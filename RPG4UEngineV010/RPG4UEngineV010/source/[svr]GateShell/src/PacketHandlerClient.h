#pragma once


#include "PacketHandlerDefine.h"

class OnlineUser;
class UserSession;

/////////////////////////////////////////////
#define PACKETHANDLER_CLIENT_DECL(protocol)\
							PACKETHANDLER_DECL(protocol)

#define PACKETHANDLER_CLIENT_IMPL(protocol,cat)\
							PACKETHANDLER_SERVER_BEGIN(PacketHandlerClient, protocol, cat,UNKNOWN_SHELL);\
							UserSession*	pUserSession		= (UserSession*)lpParam;\
							pUserSession;

#define PACKETHANDLER_CLIENT_CONN(protocol)		PACKETHANDLER_CLIENT_IMPL(protocol,CG_CONNECTION)
#define PACKETHANDLER_CLIENT_CHARINFO(protocol)	PACKETHANDLER_CLIENT_IMPL(protocol,CG_CHARINFO)
#define PACKETHANDLER_CLIENT_SYNC(protocol)		PACKETHANDLER_CLIENT_IMPL(protocol,CG_SYNC)
#define PACKETHANDLER_CLIENT_PARTY(protocol)		PACKETHANDLER_CLIENT_IMPL(protocol,CG_PARTY)
#define PACKETHANDLER_CLIENT_GM(protocol)			PACKETHANDLER_CLIENT_IMPL(protocol,CG_GM)
#define PACKETHANDLER_CLIENT_ZONE(protocol)		PACKETHANDLER_CLIENT_IMPL(protocol,CG_ZONE)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PacketHandlerClient
{
public:
	//////////////////////////////////////////////////////////////
	PACKETHANDLER_CLIENT_DECL( CG_CONNECTION_HEARTBEAT_SYN );

	////////////////////////////////////////////////////////////////
	PACKETHANDLER_CLIENT_DECL( CG_PREPARE_WORLD_CONNECT_SYN );


	////////////////////////////////////////////////////////////////
	// DBP
	PACKETHANDLER_CLIENT_DECL( CG_CONNECTION_REENTERSERVER_SYN );
	PACKETHANDLER_CLIENT_DECL( CG_CHARINFO_CREATE_SYN );
	PACKETHANDLER_CLIENT_DECL( CG_CHARINFO_DESTROY_SYN );
	////////////////////////////////////////////////////////////////


	////////////////////////////////////////////////////////////////
	PACKETHANDLER_CLIENT_DECL( CG_SYNC_PLAYER_ENTER_SYN );

	////////////////////////////////////////////////////////////////
	PACKETHANDLER_CLIENT_DECL( CG_CONNECTION_ENTERVILLAGEREQ_SYN );
	PACKETHANDLER_CLIENT_DECL( CG_CONNECTION_BACKTOCHARSELECT_SYN );

	PACKETHANDLER_CLIENT_DECL( CG_PARTY_INVITE_REQUEST_SYN );	
	PACKETHANDLER_CLIENT_DECL( CG_PARTY_INVITE_RESPONSE_ACK );	
	PACKETHANDLER_CLIENT_DECL( CG_PARTY_INVITE_RESPONSE_NAK );	


	////////////////////////////////////////////////////////////////
	PACKETHANDLER_CLIENT_DECL( CG_GM_CHANNELINFO_SYN );
	PACKETHANDLER_CLIENT_DECL( CG_GM_WARP_SYN );

};








/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerClient.cpp
创建日期：2009年6月16日
最后更新：2009年6月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerClient.h"
#include <PacketStruct_Gate_Game.h>
#include <PacketStruct_Gate_DBP.h>
#include <PacketStruct_ClientGameS.h>
#include <PacketStruct_Gate_Guild.h>
#include <PacketStruct_Gate_Chat.h>
#include <Protocol_Gate_Chat.h>
#include <ResultCode.h>
#include <ThreadableLogger.h>
#include "OnlineUser.h"
#include "UserManager.h"
#include "LobbyRoom.h"
#include "HuntingRoom.h"
#include "MissionRoom.h"
#include "CharSelector.h"
#include "PVPRoom.h"
#include "Village.h"
#include "HeartbeatHandler.h"
#include "GateShell.h"
#include "ServerSessionManagerInstance.h"
#include "IServerSession.h"
#include "ZoneManager.h"
#include "PacketHandlerManager.h"
#include "ServerInfoParser.h"
#include <EncodeMD5.h>
#include "PacketCryptManager.h"
#include "PacketHandlerClientDefine.h"


using namespace RC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CONN( CG_CONNECTION_HEARTBEAT_SYN )
{
	OnlineUser *pUser = (OnlineUser*)pUserSession;

	if( !pUser->GetHeartbeatHandler()->CheckValid() )
	{
		LOGINFO( "[CG_CONNECTION_HEARTBEAT_SYN] Warning: SpeedHacker!! UserID(%s)  CharName(%s)"
				, (LPCTSTR)pUser->GetUserID()
				, pUser->GetSelectedCharName() );
		LOGHACK	( "UserID(%s)  CharName(%s)\n"
					, (LPCTSTR)pUser->GetUserID()
					, pUser->GetSelectedCharName() );
		//pUser->Disconnect();
	}
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CONN( CG_PREPARE_WORLD_CONNECT_SYN )
{
	MSG_CG_PREPARE_WORLD_CONNECT_SYN * pRecvMsg = (MSG_CG_PREPARE_WORLD_CONNECT_SYN *)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	///////////////////////////////////////
	if( !theGateShell.IsWorldConnected() )
	{
		MSG_CG_PREPARE_WORLD_CONNECT_NAK msgNAK;
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	///////////////////////////////////////
	MSG_AW_USER_RESTORE_CMD	msgCMD;

	msgCMD.dwAuthUserID	= pUser->GetAuthID();
	msgCMD.dwUserGUID		= pUser->GetUserGUID();
	msgCMD.dwZoneKey		= pUser->GetZoneKey();
	msgCMD.byZoneType		= pUser->GetZoneType();
	msgCMD.m_dwKey			= pUser->GetUserKey();

	__ZERO( msgCMD.szCharName);
	if( msgCMD.byZoneType != ZONETYPE_CHARSELECT )
	{
		msgCMD.CharGuid = pUser->GetSelectedCharGuid();
		memcpy( msgCMD.szCharName, pUser->GetSelectedCharName(), sizeof(TCHAR)*MAX_CHARNAME_LENGTH );
	}
	else
	{
		msgCMD.CharGuid = 0;
	}
	memcpy( msgCMD.szClientIP, pUser->GetClientIP(), sizeof(TCHAR)*MAX_IP_LENGTH );

	theGateShell.SendToWorld( &msgCMD, sizeof(MSG_AW_USER_RESTORE_CMD) );

	LOGINFO	( "[Guid:%u] OnlineUser Restore To World[%u,%u]\n"
				,pUser->GetUserGUID()
				,msgCMD.byZoneType
				,msgCMD.dwZoneKey );


}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_SYNC( CG_SYNC_PLAYER_ENTER_SYN )
{
	MSG_CG_SYNC_PLAYER_ENTER_SYN * pRecvMsg = (MSG_CG_SYNC_PLAYER_ENTER_SYN *)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	MSG_CG_SYNC_PLAYER_ENTER_NAK msgNAK;


	///////////////////////////////////////
	if( pUser->IsBeginTransaction() )
	{
		msgNAK.m_dwErrorCode = _RC_RM(ALREADY_DOING_TRANSACTION);
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	///////////////////////////////////////
	switch(pUser->GetZoneState())
	{
	case ZONESTATE_PRE_ENTER_VILLAGE:
	case ZONESTATE_PRE_ENTER_LOBBY:
	case ZONESTATE_PRE_ENTER_HUNTING:
	case ZONESTATE_PRE_ENTER_MISSION:
	case ZONESTATE_PRE_ENTER_PVP:
		{
			MSG_AG_SYNC_PLAYER_ENTER_SYN msg;
			msg.m_dwCheckSum = pRecvMsg->m_dwCheckSum;
			//////////////////////////////////////
			pUser->SendToLinkedShell( &msg, sizeof(msg) );
		}
		break;

	case ZONESTATE_ATVILLAGE:
	case ZONESTATE_ATLOBBY:
	case ZONESTATE_ATHUNTING:
	case ZONESTATE_ATMISSION:
	case ZONESTATE_ATPVP:
		{
			MSG_AG_SYNC_PLAYER_WARP_SYN msg;
			msg.m_dwCheckSum = pRecvMsg->m_dwCheckSum;
			pUser->SendToLinkedShell( &msg, sizeof(msg) );
		}
		break;

	default:
		{
			msgNAK.m_dwErrorCode = _RC_RM(INVALID_ROOM_TYPE)	;
			pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
			return FALSE;
		}
	}
	
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CONN( CG_CONNECTION_REENTERSERVER_SYN )
{
	MSG_CG_CONNECTION_REENTERSERVER_SYN * pRecvMsg = (MSG_CG_CONNECTION_REENTERSERVER_SYN *)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	MSG_CG_CONNECTION_ENTERSERVER_NAK msgNAK;

	////////////////////////////////////////////
	if( pUser->IsBeginTransaction() )
	{
		msgNAK.m_dwErrorCode = _RC_RM(ALREADY_DOING_TRANSACTION);
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	pUser->BeginTransaction( TK_CHARACTER_LIST );

	////////////////////////////////////////////
	MSG_AD_CHARINFO_CHARLISTREQ_SYN	msg;
	msg.m_dwKey			= pUser->GetUserKey();
	msg.m_dwUserGUID	= pUser->GetUserGUID();

	if( FALSE == pUser->SendToDBProxy( &msg, sizeof(msg) ) )
	{
		msgNAK.m_dwErrorCode = RC_DATABASE_FAILED;
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );

		LOGINFO	("[ CG_CONNECTION_REENTERSERVER_SYN [U:%d] DBProxy Failed\n"
					,pUser->GetUserKey() );
		pUser->Disconnect();

		pUser->EndTransaction();
		return FALSE;
	}
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CHARINFO( CG_CHARINFO_CREATE_SYN )
{
	MSG_CG_CHARINFO_CREATE_SYN * pRecvMsg = (MSG_CG_CHARINFO_CREATE_SYN *)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	MSG_CG_CHARINFO_CREATE_NAK msgNAK;

	////////////////////////////////////////////////////
	if( pUser->IsBeginTransaction() )
	{
		msgNAK.m_dwErrorCode	= _RC_RM(ALREADY_DOING_TRANSACTION);
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	if(	pUser->GetState() < USER_STATE_CHARLIST 
		|| pUser->GetCreatedCharAmount() >= MAX_CHARACTER_SLOT_NUM )
	{
		msgNAK.m_dwErrorCode	= RC_CHARINFO_STATEINVALID;//56;
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}


	///////////////////////////////////////////////////////////////
	pUser->BeginTransaction( TK_CHARACTER_CREATION );

	MSG_AD_CHARINFO_CREATE_SYN msg;
	msg.m_dwKey			= pUser->GetUserKey();
	msg.m_dwUserGUID	= pUser->GetUserGUID();
	msg.m_byClass		= pRecvMsg->m_byClass;
	msg.m_byHeight		= pRecvMsg->m_byHeight;
	msg.m_byFace		= pRecvMsg->m_byFace;
	msg.m_byHair		= pRecvMsg->m_byHair;
	msg.m_byHairColor	= pRecvMsg->m_byHairColor;
	msg.m_bySex			= pRecvMsg->m_bySex;

	lstrcpyn( msg.m_szUserID	, pUser->GetUserID()		, MAX_ID_LENGTH );
	lstrcpyn( msg.m_szCharName	,pRecvMsg->m_szCharName	, MAX_CHARNAME_LENGTH );

	if( FALSE == pUser->SendToDBProxy( &msg, sizeof(msg) ) )
	{
		msgNAK.m_dwErrorCode	= RC_DATABASE_FAILED;
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		pUser->EndTransaction();
		return FALSE;
	}
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CHARINFO( CG_CHARINFO_DESTROY_SYN )
{
	MSG_CG_CHARINFO_DESTROY_SYN * pRecvMsg = (MSG_CG_CHARINFO_DESTROY_SYN *)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	MSG_CG_CHARINFO_DESTROY_NAK	msgNAK;
	BOOL									bEndTransaction(FALSE);

	//////////////////////////////////////
	if( pUser->IsBeginTransaction() )
	{
		msgNAK.m_dwErrorCode	= _RC_RM(ALREADY_DOING_TRANSACTION);
		goto laFailed;
	}

	BYTE byCharIndex = pRecvMsg->m_SelectedSlotIndex;

	if(	pUser->GetState() < USER_STATE_CHARLIST
		|| pUser->GetCreatedCharAmount() == 0 
		||	byCharIndex >= MAX_CHARACTER_SLOT_NUM 
		||	!pUser->IsValidSlot(byCharIndex) )
	{
		msgNAK.m_dwErrorCode	= RC_INVALID_SLOT;
		goto laFailed;
	}

	//////////////////////////////////////
	//if(	theServerInfoParser.GetServerInfo()->bEnableLoginShell) 
	{
		TCHAR szPassWD[MAX_PASSWORD_LENGTH];
		//检测密码合法性
		
		thePacketCryptManager.PacketDecode((BYTE* )pRecvMsg->szPasswd
                         ,MAX_PASSWORD_LENGTH
								 ,(BYTE* )szPassWD
								 ,pUser->GetAuthSequence());

		if( pUser->GetCharPassword() != (LPCTSTR)EncodeMD5::GetMD5(szPassWD) )
		{
			msgNAK.m_dwErrorCode	= RC_INVALID_SECURITY;
			goto laFailed;
		}
	}

	//////////////////////////////////////
	pUser->BeginTransaction( TK_CHARACTER_DESTRUCTION );


	//////////////////////////////////////
	{
	MSG_AD_CHARINFO_DESTROY_SYN msg;
	msg.m_dwKey					= pUser->GetUserKey();
	msg.m_SelectedSlotIndex = byCharIndex;
	msg.m_dwUserGUID			= pUser->GetUserGUID();
	msg.m_dwCharGUID			= pUser->GetCharGUID(byCharIndex);

	if(! pUser->SendToDBProxy( &msg, sizeof(msg) ) )
	{
		msgNAK.m_dwErrorCode	= RC_DATABASE_FAILED;
		bEndTransaction		= TRUE;
		goto laFailed;
	}
	}
	goto laOK;

laFailed:
	{
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		if(bEndTransaction)
			pUser->EndTransaction();
		return FALSE;
	};
laOK:{}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CONN( CG_CONNECTION_BACKTOCHARSELECT_SYN )
{
	MSG_CG_CONNECTION_BACKTOCHARSELECT_SYN * pRecvMsg = (MSG_CG_CONNECTION_BACKTOCHARSELECT_SYN *)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	MSG_CG_CONNECTION_BACKTOCHARSELECT_NAK msgNAK;

	//////////////////////////////////////////////////////////
	if( pUser->IsBeginTransaction() )
	{
		msgNAK.m_dwErrorCode = _RC_RM(ALREADY_DOING_TRANSACTION);
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	//////////////////////////////////////////////////////////
	if( !(pUser->GetZoneState() & ZONESTATE_ATZONE) )
	{
		msgNAK.m_dwErrorCode = _RC_RM(BEFOREENTERZONE);
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	//////////////////////////////////////////////////////////
	IZone * pZone = theZoneManager.FindZone( pUser->GetZoneKey() );
	if( !pZone )
		return FALSE;

	pZone->LeaveUser( pUser, eSEND_SYN );

	pUser->BeginTransaction( TK_BACKTOSELECT );

}
PACKETHANDLER_SERVER_END();


//选择人物之后进入游戏
PACKETHANDLER_CLIENT_CONN( CG_CONNECTION_ENTERVILLAGEREQ_SYN )
{
	HANDLE_INIT();
	OnlineUser *	pPreUser;
	OnlineUser *	pUser;
	BYTE				byCharIndex;
	Village *		pZone;
	CharSelector *	pCharSelect;
	MSG_CG_CONNECTION_ENTERVILLAGEREQ_SYN *	pRecvMsg;
	

	//////////////////////////////
	pRecvMsg = (MSG_CG_CONNECTION_ENTERVILLAGEREQ_SYN *)pMsg;
	pUser		= (OnlineUser *)pUserSession;


	//////////////////////////////
	HANDLE_FAILED	(pUser->IsBeginTransaction()
						,RC_CONNECTION_ROOM_ALREADY_DOING_TRANSACTION);


	byCharIndex = pRecvMsg->m_bySelectedCharIndex;

	//选择当前人物
	pUser->SelectCharacter( byCharIndex );

	/////////////////////////////////
	HANDLE_FAILED	(	(	pUser->GetState() < USER_STATE_CHARLIST 
							|| byCharIndex >= MAX_CHARACTER_SLOT_NUM 
							|| !pUser->IsValidSlot(byCharIndex) )
						,RC_CHARSELECT_OUTOFRANGEINDEX);


	/////////////////////////////////
	HANDLE_FAILED	(!theServerSessionManagerInstance.GetFieldServer()
						,RC_CONNECTION_DONOTEXIST_LINKSERVER);

	HANDLE_FAILED	(pUser->GetZoneState() != ZONESTATE_ATCHARSELECT
						, RC_CONNECTION_ZONESTATE_WRONG);


	/////////////////////////////////
	pPreUser = (OnlineUser *)theUserManager.FindUserName(pUser->GetSelectedCharName());
	if( pPreUser )
	{
		LOGINFO(  "[HANDLER_FROMCLIENT_IMPL( CG_CONNECTION_ENTERVILLAGEREQ_SYN )] (%s)已经在线.", pUser->GetSelectedCharName() );
		pPreUser->Disconnect();

	/////////////////////////////////
		HANDLE_DISCONNECT	(TRUE	
								, RC_CONNECTION_DONOTEXIST_LINKSERVER);
	}


	/////////////////////////////////
	//查找战区
	pZone = theZoneManager.FindVillage( pUser->GetSelectedCharMapCode() );
	if( !pZone )
	{	
		LOGINFO( "地图不存在.[%d]\n", pUser->GetSelectedCharMapCode() );
		HANDLE_FAILED(TRUE, RC_CONNECTION_MAP_NOTFOUND);
	}


	/////////////////////////////////
	//退出CharSelector场景
	theUserManager.AddUserName( pUser );
	pCharSelect = theZoneManager.FindCharSelector(1);//pUser->GetZoneKey());
	pCharSelect->LeaveUser( pUser, eSEND_NONE );

	//////////////////////////////////////////////////
	// 通知ChatShell，有玩家进来了
	{
	MSG_AW_USER_SET_CHARNAME_SYN wmsg;
	wmsg.m_dwKey		= pUser->GetUserKey();
	wmsg.dwCharGuid		= pUser->GetCharGUID( byCharIndex );
	strncpy( wmsg.szCharName, pUser->GetSelectedCharName(), MAX_CHARNAME_LENGTH );
	pUser->SendToChatShell( &wmsg, sizeof(wmsg) );
	}


	//////////////////////////////////////////////////
	// 通知GuildShell，有玩家进来了
	if( 0 != pUser->GetSelectedCharGuildGuid() )
	{
		MSG_AZ_GUILD_LOGIN_CHAR_CMD msgCMD;
		msgCMD.m_GuildGuid		= pUser->GetSelectedCharGuildGuid();
		msgCMD.m_CharGuid		= pUser->GetSelectedCharGuid();
		pUser->SendToGuildShell( &msgCMD, sizeof(msgCMD) );
	}


	//////////////////////////////////////////
	HANDLE_SUCCED()
	{
		///////////////////////////////////////
		//成功处理
		pUser->BeginTransaction( TK_JOIN_VILLAGE );
		pZone->ReadyUser( pUser, eSEND_SYN );
	}

	//////////////////////////////////////////
	HANDLE_ERROR(MSG_CG_CONNECTION_ENTERVILLAGEREQ_NAK)
	{
	}
	HANDLE_END();
//laFailed:
	/////////////////////////////////////////////
	//处理

	//MSG_CG_CONNECTION_ENTERVILLAGEREQ_NAK msgNAK;
	//msgNAK.m_dwErrorCode	= dwError;
	//pUser->SendPacket( &msgNAK, sizeof(msgNAK) );


}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_PARTY( CG_PARTY_INVITE_REQUEST_SYN )
{
	MSG_CG_PARTY_INVITE_REQUEST_SYN* pRecvMsg = (MSG_CG_PARTY_INVITE_REQUEST_SYN*)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	////////////////////////////////////////////////////////
	StringHandle	szCharName;
	szCharName.Set(pRecvMsg->m_szCharName, MAX_CHARNAME_LENGTH);


	////////////////////////////////////////////////////////
	OnlineUser *pOtherUser;
	pOtherUser = (OnlineUser *)theUserManager.FindUserName( szCharName );
	if( !pOtherUser )
	{
		MSG_CG_PARTY_INVITE_REQUEST_SERVER_NAK NakMsg;
		NakMsg.m_byCategory	= CG_PARTY;
		NakMsg.m_byProtocol	= CG_PARTY_INVITE_REQUEST_SERVER_NAK;
		NakMsg.m_byErrorCode = RC_PARTY_PLAYER_NOTEXIST;
		pUser->SendPacket( &NakMsg, sizeof(NakMsg) );
		return FALSE;
	}

	////////////////////////////////////////////////////////
	MSG_AG_PARTY_GATE2BATTLE_INVITE_REQUEST_SYN SendMsg;
	SendMsg.m_dwOtherUserKey = pOtherUser->GetUserKey();
	pUser->SendToLinkedShell( &SendMsg, sizeof(SendMsg) );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_PARTY( CG_PARTY_INVITE_RESPONSE_ACK )
{
	MSG_CG_PARTY_INVITE_RESPONSE_ACK* pRecvMsg = (MSG_CG_PARTY_INVITE_RESPONSE_ACK*)pMsg;
	OnlineUser *pUser = (OnlineUser *)pUserSession;

	////////////////////////////////////////////////////////////////////////
	MSG_AG_PARTY_GATE2FIELD_INVITE_RESPONSE_ACK FieldMsg;
	pUser->SendToLinkedShell( &FieldMsg, sizeof(FieldMsg) );

	////////////////////////////////////////////////////////////////////////
	OnlineUser *pMaster = (OnlineUser *)theUserManager.GetUser( pRecvMsg->m_dwMasterUserKey );
	if( pMaster )
	{
		MSG_AG_PARTY_GATE2BATTLE_INVITE_RESPONSE_ACK BattleMsg;
		BattleMsg.m_dwOtherUserKey = pUser->GetUserKey();
		pMaster->SendToLinkedShell( &BattleMsg, sizeof(BattleMsg) );
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_PARTY( CG_PARTY_INVITE_RESPONSE_NAK )
{
	MSG_CG_PARTY_INVITE_RESPONSE_NAK* pRecvMsg = (MSG_CG_PARTY_INVITE_RESPONSE_NAK*)pMsg;
	OnlineUser *pUser = (OnlineUser *)pUserSession;

	////////////////////////////////////////////////////////////////////////
	MSG_AG_PARTY_GATE2FIELD_INVITE_RESPONSE_NAK FieldMsg;
	pUser->SendToLinkedShell( &FieldMsg, sizeof(FieldMsg) );

	////////////////////////////////////////////////////////////////////////
	OnlineUser *pMaster = (OnlineUser *)theUserManager.GetUser( pRecvMsg->m_dwMasterUserKey );
	if( pMaster )
	{
		MSG_AG_PARTY_GATE2BATTLE_INVITE_RESPONSE_NAK BattleMsg;
		BattleMsg.m_byErrorCode = RC_PARTY_REJECT_INVITEMENT;
		pMaster->SendToLinkedShell( &BattleMsg, sizeof(BattleMsg) );
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_GM( CG_GM_CHANNELINFO_SYN )
{
	MSG_CG_GM_CHANNELINFO_SYN * pRecvMsg = (MSG_CG_GM_CHANNELINFO_SYN*)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;

	MSG_CG_GM_CHANNELINFO_ACK msgACK;
	msgACK.m_dwChannelUserNum	= theUserManager.GetUserAmount();
	msgACK.m_wHuntingRoomCount = (WORD)theZoneManager.GetHuntingRoomAmount();
	msgACK.m_wMissionRoomCount = (WORD)theZoneManager.GetMissionRoomAmount();

	pUser->SendPacket( &msgACK, sizeof(msgACK) );
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_GM( CG_GM_WARP_SYN )
{
	MSG_CG_GM_WARP_SYN* pRecvMsg = (MSG_CG_GM_WARP_SYN*)pMsg;
	OnlineUser * pUser = (OnlineUser *)pUserSession;
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

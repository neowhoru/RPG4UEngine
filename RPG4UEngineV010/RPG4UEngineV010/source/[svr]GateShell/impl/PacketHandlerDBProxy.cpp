/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerDBProxy.cpp
创建日期：2009年6月16日
最后更新：2009年6月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerDBProxy.h"
#include "PacketHandlerManager.h"

#include <PacketStruct_ClientGameS.h>
#include <PacketStruct_Gate_DBP.h>
#include <PacketStruct_ClientAgent.h>

#include "GateShell.h"
#include "UserManager.h"
#include "OnlineUser.h"

#define CHECK_USER(id)	(OnlineUser *)theUserManager.GetUser( id );	\
								if( !pUser )\
									return FALSE;\


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_DBRPOXY( AD_CHARINFO_CHARLISTREQ_ACK )
{
	MSG_AD_CHARINFO_CHARLISTREQ_ACK * pRecvMsg = (MSG_AD_CHARINFO_CHARLISTREQ_ACK *)pMsg;

	/////////////////////////////////////////
	OnlineUser *	pUser;
	BYTE				nListNum;
	
	pUser				= CHECK_USER( pRecvMsg->m_dwKey );
	nListNum			= pRecvMsg->m_byCount;

	pUser->SetCreatedCharAmount( nListNum );

	ASSERT( pUser->IsBeginTransaction() );

	/////////////////////////////////////////
	//分块发送
	//	1. 保存人物数量
	//		1.1.只存在1人物
	//	2.人物数量=0
	//		2.1.只存在1人物
	sRESULT_MSG			arResultMsgs[MAX_CHARACTER_LIST_NUM];
	BYTE *				arMsgs		[MAX_CHARACTER_LIST_NUM+1];
	WORD					arSizes		[MAX_CHARACTER_LIST_NUM+1];


	/////////////////////////////////////////
	MSG_CG_CONNECTION_ENTERSERVER_ACK	msg;
	sPLAYER_SERVER_PART *					pCharInfo;
	BYTE											bySlotIndex;

	////////////////////////////////
	//计算0人物数量
	msg.m_byCount	= nListNum;

	arSizes[0]		= (WORD)msg.GetSize(0);
	arMsgs[0]		= (BYTE*)&msg;


	pCharInfo = (sPLAYER_SERVER_PART*)( (BYTE*)(pRecvMsg) + pRecvMsg->GetSize() );
	for( BYTE i = 0  ; i < nListNum ; ++i )
	{
		////////////////////////////////////
		bySlotIndex = pCharInfo->m_bySlot;

		pUser->SetOccupiedSlotAt( bySlotIndex, TRUE );
		pUser->SerializeCharacterInfo( bySlotIndex, *pCharInfo, SERIALIZE_STORE );

		////////////////////////////////////
		sCHARACTER_CLIENTPART&	charInfo	= (sCHARACTER_CLIENTPART&)arResultMsgs[i].m_byMsg[0];
		ASSERT( sizeof(sCHARACTER_CLIENTPART) < sRESULT_MSG::MAX_BUFFER_SIZE );


		charInfo.m_bySlot			= pCharInfo->m_bySlot;
		charInfo.m_byHeight		= pCharInfo->m_byHeight;
		charInfo.m_byFace			= pCharInfo->m_byFace;
		charInfo.m_byHair			= pCharInfo->m_byHair;
		charInfo.m_byHairColor	= pCharInfo->m_byHairColor;
		charInfo.m_bySex			= pCharInfo->m_bySex;
		charInfo.m_byClass		= pCharInfo->m_byClass;
		charInfo.m_LV				= pCharInfo->m_LV;
		charInfo.m_dwRegion		= pCharInfo->m_dwRegion;
		charInfo.m_wX				= pCharInfo->m_wX;
		charInfo.m_wY				= pCharInfo->m_wY;
		charInfo.m_wZ				= pCharInfo->m_wZ;

		memcpy(charInfo.m_szCharName
				,pCharInfo->m_szCharName
				,MAX_CHARNAME_LENGTH*sizeof(TCHAR) );

		memcpy( &charInfo.m_EquipItemInfo
				, &pCharInfo->m_EquipItemInfo
				, pCharInfo->m_EquipItemInfo.GetSize() );

		//////////////////////////////
		arResultMsgs[i].m_wSize	= charInfo.GetSize();
		arMsgs	[1+i]	= arResultMsgs[i].m_byMsg;
		arSizes	[1+i]	= arResultMsgs[i].m_wSize;

		////////////////////////////////////////
		int bufSize = pCharInfo->GetSize();
		pCharInfo = (sPLAYER_SERVER_PART *)((BYTE*)(pCharInfo) + bufSize);
	}

	///////////////////////////////////////////////////////////
	pUser->SetState( USER_STATE_CHARLIST );

	pUser->Send	(nListNum + 1
					,&arMsgs[0]
					,&arSizes[0]);

	pUser->EndTransaction();

}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_DBRPOXY( AD_CHARINFO_CHARLISTREQ_NAK )
{
	MSG_AD_CHARINFO_CHARLISTREQ_NAK *	pRecvMsg = (MSG_AD_CHARINFO_CHARLISTREQ_NAK *)pMsg;
	OnlineUser *								pUser		= CHECK_USER( pRecvMsg->m_dwKey );

	MSG_CG_CONNECTION_ENTERSERVER_NAK msg;
	msg.m_dwErrorCode = pRecvMsg->m_dwErrorCode;
	pUser->SendPacket( &msg, sizeof(msg) );

	LOGINFO( " AD_CHARINFO_CHARLISTREQ_NAK  [U:%d] CharList Failed.\n", pRecvMsg->m_dwKey );

	pUser->EndTransaction();
	pUser->Disconnect();
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_DBRPOXY( AD_CHARINFO_CREATE_ACK )
{
	MSG_AD_CHARINFO_CREATE_ACK *	pRecvMsg = (MSG_AD_CHARINFO_CREATE_ACK *)pMsg;
	OnlineUser *						pUser		= CHECK_USER( pRecvMsg->m_dwKey );

	ASSERT( pUser->GetCreatedCharAmount() < MAX_CHARACTER_SLOT_NUM );

	pUser->SetOccupiedSlotAt		( pRecvMsg->m_CharInfo.m_bySlot, TRUE );
	pUser->SetCreatedCharAmount	( pUser->GetCreatedCharAmount() + 1 );
	pUser->SerializeCharacterInfo	(pRecvMsg->m_CharInfo.m_bySlot
                                 ,pRecvMsg->m_CharInfo
											,SERIALIZE_STORE);
	pUser->EndTransaction();


	/////////////////////////////////////////////////////////
	MSG_CG_CHARINFO_CREATE_ACK msg;
	sPLAYER_SERVER_PART&			charPart = pRecvMsg->m_CharInfo;

	msg.m_CharInfo.m_bySlot			= charPart.m_bySlot;
	msg.m_CharInfo.m_byHeight		= charPart.m_byHeight;
	msg.m_CharInfo.m_byFace			= charPart.m_byFace;
	msg.m_CharInfo.m_byHair			= charPart.m_byHair;
	msg.m_CharInfo.m_byHairColor	= charPart.m_byHairColor;
	msg.m_CharInfo.m_bySex			= charPart.m_bySex;
	msg.m_CharInfo.m_byClass		= charPart.m_byClass;
	msg.m_CharInfo.m_LV				= charPart.m_LV;
	msg.m_CharInfo.m_dwRegion		= charPart.m_dwRegion;
	msg.m_CharInfo.m_wX				= charPart.m_wX;
	msg.m_CharInfo.m_wY				= charPart.m_wY;
	msg.m_CharInfo.m_wZ				= charPart.m_wZ;

	memcpy(msg.m_CharInfo.m_szCharName		, charPart.m_szCharName		, MAX_CHARNAME_LENGTH*sizeof(TCHAR) );
	memcpy(&msg.m_CharInfo.m_EquipItemInfo, &charPart.m_EquipItemInfo	, charPart.m_EquipItemInfo.GetSize() );


	GAMELOG.LogCharacter((LPCTSTR)pUser->GetUserID()
                         ,pUser->GetSelectedCharName()
								 ,theGateShell.GetServerGUID()
								 ,TRUE); 

	LOGINFO	(" AD_CHARINFO_CREATE_ACK  ID = %s, NAME = %s\n"
				,(LPCTSTR)pUser->GetUserID( )
				,pUser->GetSelectedCharName() );

	pUser->SendPacket( &msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_DBRPOXY( AD_CHARINFO_CREATE_NAK )
{
	MSG_AD_CHARINFO_CREATE_NAK *	pRecvMsg = (MSG_AD_CHARINFO_CREATE_NAK *)pMsg;
	OnlineUser *						pUser		= CHECK_USER( pRecvMsg->m_dwKey );

	pUser->EndTransaction();

	MSG_CG_CHARINFO_CREATE_NAK msg;
	msg.m_dwErrorCode = pRecvMsg->m_dwErrorCode;
	pUser->SendPacket( &msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_DBRPOXY( AD_CHARINFO_DESTROY_ACK )
{
	MSG_AD_CHARINFO_DESTROY_ACK * pRecvMsg = (MSG_AD_CHARINFO_DESTROY_ACK *)pMsg;
	OnlineUser *						pUser		= CHECK_USER( pRecvMsg->m_dwKey );

	ASSERT( pUser->GetCreatedCharAmount() > 0 );
	pUser->SetCreatedCharAmount( pUser->GetCreatedCharAmount() - 1 );
	pUser->SetOccupiedSlotAt	( pRecvMsg->m_SelectedSlotIndex, FALSE );

	pUser->EndTransaction();

	////////////////////////////////////////////////////
	GAMELOG.LogCharacter((LPCTSTR)pUser->GetUserID()
                         ,pUser->GetSelectedCharName()
								 ,theGateShell.GetServerGUID()
								 ,FALSE); 

	LOGINFO	("[ AD_CHARINFO_DESTROY_ACK ] ID = %s, NAME = %s\n"
				,pUser->GetUserID( )
				,pUser->GetSelectedCharName() );

	MSG_CG_CHARINFO_DESTROY_ACK msg;
	pUser->SendPacket( &msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_DBRPOXY( AD_CHARINFO_DESTROY_NAK )
{
	MSG_AD_CHARINFO_DESTROY_NAK * pRecvMsg = (MSG_AD_CHARINFO_DESTROY_NAK *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	pUser->EndTransaction();

	MSG_CG_CHARINFO_DESTROY_NAK msg;
	msg.m_dwErrorCode = pRecvMsg->m_dwErrorCode;
	pUser->SendPacket( &msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

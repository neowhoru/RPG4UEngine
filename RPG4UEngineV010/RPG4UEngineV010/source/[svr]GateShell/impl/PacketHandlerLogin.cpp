/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerLogin.cpp
创建日期：2009年6月16日
最后更新：2009年6月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerLogin.h"
#include "IServerSession.h"
#include "OnlineUser.h"
#include "UserManager.h"
#include <PacketStruct_Gate_Login.h>
#include <Protocol_Gate_Login.h>
#include <PacketStruct_ClientGameS.h>
#include <Protocol_ClientAgent.h>
#include <ResultCode.h>
#include "ConnectionTimer.h"
#include "ConnectionTimerManager.h"
#include "TimeKeeper.h"
#include "GateShell.h"
#include "PacketHandlerManager.h"
#include "ServerInfoParser.h"


#ifdef _DEBUG
	const DWORD USER_AUTH_EXPIRE_DELAY = 30000*1000;		// 30秒登录验证检测
#else
	const DWORD USER_AUTH_EXPIRE_DELAY = 30*1000;		// 30秒登录验证检测
#endif

PACKETHANDLER_SERVER_AUTHAGENT(AU_LOGIN_NEWCLIENT_SYN)
{
	MSG_AU_LOGIN_NEWCLIENT_SYN *	pRecvMsg = (MSG_AU_LOGIN_NEWCLIENT_SYN*)pMsg;
	ConnectionTimer *					pUserInfo;

	//LOGMSG( LOG_FULL,  "[RECV] S2S_ASK_NEWCLIENT: authID=%d", pRecvMsg->dwAuthUserID );

	// 1.	theTimeKeeper 时间控制器，检查2个:同样UserGUID,同样AuthID
	// 2.	theUserManager则作用户检查((UserGUID)
	//<= 这样做好
	// AuthAgent基于AuthID为首先
	// 其它两个在后


	///////////////////////////////////////////////////
	MSG_AU_LOGIN_NEWCLIENT_ACK msg;
	msg.dwAuthUserID	= pRecvMsg->dwAuthUserID;			//AuthServer发给的识别码
	msg.byResult		= 0;										//OK (0) ; ERROR (CODE)
	msg.wSvrCode		= pRecvMsg->wSvrCode;				//服务器编码
	strncpy(msg.szID, pRecvMsg->szID, MAX_ID_LENGTH);		//帐号

	
	///////////////////////////////////////////////////
	pUserInfo = dynamic_cast<ConnectionTimer*>( theTimeKeeper.PeekTimer( pRecvMsg->dwAuthUserID ) );
	if( pUserInfo )
	{
		pUserInfo->Close();

		msg.byResult = 1;
		pServerSession->Send( (BYTE*)&msg, sizeof(msg) );
		return FALSE;
	}

	///////////////////////////////////////////////////
	pUserInfo = dynamic_cast<ConnectionTimer*>( theTimeKeeper.PeekTimerBy( pRecvMsg->dwUserGuid ) );
	if( pUserInfo )
	{
		pUserInfo->Close();

		msg.byResult = 1;
		pServerSession->Send( (BYTE*)&msg, sizeof(msg) );
		return FALSE;
	}

	///////////////////////////////////////////////////
	UserSession * pExistUser;
	
	pExistUser = theUserManager.GetUser( pRecvMsg->dwUserGuid );
	if( pExistUser )
	{
		LOGMSG( LOG_FULL,  "[OnAU_LOGIN_NEWCLIENT_SYN] [U:%d] Existed\n", pExistUser->GetUserKey() );

		pExistUser->SetLogoutType(LOGOUT_ETC);
		pExistUser->Disconnect();

		msg.byResult = 1;
		pServerSession->Send( (BYTE*)&msg, sizeof(msg) );
		return FALSE;
	}

	///////////////////////////////////////////////////
	// 帐号信息
	// 客户端IP
	// billing信息没创建
	ConnectionTimer* pTimeout;
	
	pTimeout = theConnectionTimerManager.AllocTimeout();

	pTimeout->SetUserGUID		( pRecvMsg->dwUserGuid );		// DB用户GUID
	pTimeout->SetAuthUserID		( pRecvMsg->dwAuthUserID );	// AuthServer识别码
	pTimeout->SetUserID			( pRecvMsg->szID );				// 用户帐号
	pTimeout->SetClientIP		( pRecvMsg->szClientIP );		// 客户端IP
	pTimeout->SetBillingType	( pRecvMsg->byBillingType);		// 
	pTimeout->SetBillingInfo	( pRecvMsg->szBillingInfo);		// 
	pTimeout->SetReservedValue	( pRecvMsg->dwReservedValue);	// 保留值
	pTimeout->SetSerialKey		( pRecvMsg->szSerialKey	);		// 序列号
	pTimeout->SetLastSSNKey		( pRecvMsg->szMD5Key		);
	pTimeout->SetAuthSequence	( pRecvMsg->dwAuthSequence);
	pTimeout->SetCharPassword	( pRecvMsg->szCharPassword);

	pTimeout->Start( USER_AUTH_EXPIRE_DELAY );
	theTimeKeeper.PushTimer( pTimeout->GetAuthUserID(), pTimeout );

	// OK
	msg.byResult = 0;
	pServerSession->Send( (BYTE*)&msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AUTHAGENT(AU_LOGIN_PC_LOGOUT_SYN)
{
	MSG_AU_LOGIN_PC_LOGOUT_SYN *	pRecvMsg = (MSG_AU_LOGIN_PC_LOGOUT_SYN*)pMsg;
	OnlineUser *						pUser = (OnlineUser *)theUserManager.GetUserByID( pRecvMsg->dwAuthUserID );

	////////////////////////////////////////////////////
	if( pUser != NULL )
	{		
		LOGMSG(LOG_FULL
				,"[OnAU_LOGIN_PC_LOGOUT_SYN ] ID:%s [Key:%d,AuthID:%d]"
				,pRecvMsg->szID
				,pUser->GetUserKey()
				,pUser->GetAuthID()
				);

		// client Disconnect -> OnDisconnect()
		MSG_CG_CONNECTION_DISCONNECT_CMD discMsg;
		discMsg.m_dwErrorCode	= RC::RC_CONNECTION_REQUESTFROMGAMESERVER;
		pUser->SendPacket( &discMsg, sizeof(discMsg) );

		pUser->SetLogoutType( LOGOUT_AUTH_REQUEST );
		pUser->Disconnect();
		return TRUE;
	}

	////////////////////////////////////////////////////
	ConnectionTimer * pUserInfo;
	BYTE					byLogOutType = LOGOUT_AUTH_REQUEST;
	
	pUserInfo = dynamic_cast<ConnectionTimer*>( theTimeKeeper.PeekTimer( pRecvMsg->dwAuthUserID ) );
	if( !pUserInfo )
		pUserInfo = dynamic_cast<ConnectionTimer*>( theTimeKeeper.PeekTimerBy( pRecvMsg->dwUserGUID ) );



	////////////////////////////////////////////////////
	if( pUserInfo )
	{
		pUserInfo->Close();
	}
	else
	{
		LOGMSG( LOG_FULL,  "[PacketHandlerLogin::OnAU_LOGIN_PC_LOGOUT_SYN]  Logout.[ToAuthID:%d, ID=%s]"
				, pRecvMsg->dwAuthUserID
				, pRecvMsg->szID );
		byLogOutType = LOGOUT_USER_NOT_FOUND;
	}

	////////////////////////////////////////////////////
	MSG_AU_LOGIN_PC_LOGOUT_CMD msg;

	lstrcpyn(msg.szAccountID, pRecvMsg->szID, MAX_ID_LENGTH);
	msg.dwAuthUserID	= pRecvMsg->dwAuthUserID;
	msg.wSvrCode		= theServerShell.GetChannelID();
	msg.byLogoutType	= byLogOutType;
	pServerSession->Send( (BYTE*)&msg, (WORD)sizeof(msg) );
}
PACKETHANDLER_SERVER_END();



PACKETHANDLER_SERVER_AUTHAGENT(AU_LOGIN_LOGIN2GAME_STATUS_CMD)
{
	MSG_AU_LOGIN_LOGIN2GAME_STATUS_CMD * pRecvMsg = (MSG_AU_LOGIN_LOGIN2GAME_STATUS_CMD*)pMsg;

	// Agent在线状态更新到AuthAgent
	MSG_AU_LOGIN_GAME2LOGIN_STATUS_CMD sendMsg;
	sendMsg.dwTimeStamp	= pRecvMsg->dwTimeStamp;
	sendMsg.wUserCount	= (WORD)theUserManager.GetUserAmount();

	pServerSession->Send( (BYTE*)&sendMsg, sizeof(sendMsg) );
}
PACKETHANDLER_SERVER_END();
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGame.cpp
创建日期：2009年6月16日
最后更新：2009年6月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerGame.h"
#include "IServerSession.h"
#include <PacketStruct_Gate_Game.h>
#include <PacketStruct_ClientGameS.h>
#include <PacketStruct_Gate_Chat.h>
#include <PacketStruct_Gate_Guild.h>
#include "OnlineUser.h"
#include "UserManager.h"
#include "HuntingRoom.h"
#include "MissionRoom.h"
#include "CharSelector.h"
#include "Village.h"
#include "LobbyRoom.h"
#include "ZoneManager.h"
#include "GateShell.h"
#include "PacketHandlerManager.h"
#include "Protocol_Server.h"

#define CHECK_USER(id)		(OnlineUser *)theUserManager.GetUser( id );	\
						if( !pUser )	\
						{ \
							LOGINFO( "Invalid OnlineUser.[ToID:%d]\n", id );	\
							return FALSE;	\
						}

using namespace RC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace util
{
void BackToCharSelect(OnlineUser* pUser)
{
	theUserManager.RemoveUserName( pUser );
	GAMELOG.LogCharDisConnect( pUser->GetUserID(), pUser->GetSelectedCharName(), theGateShell.GetServerGUID() );

	CharSelector * pCharSelect = theZoneManager.FindCharSelector(1);
	ASSERT( pCharSelect );
	if( pCharSelect )
		pCharSelect->EnterUser( pUser );
	
	//Send to Guild Server
	if( 0 != pUser->GetSelectedCharGuildGuid() )
	{
		MSG_AZ_GUILD_LOGOUT_CHAR_CMD msgCMD;
		msgCMD.m_GuildGuid	= pUser->GetSelectedCharGuildGuid();
		msgCMD.m_CharGuid		= pUser->GetSelectedCharGuid();
		pUser->SendToGuildShell( &msgCMD, sizeof(msgCMD) );
	}
}
};//namespace util


PACKETHANDLER_GAMESERVER_COMM(SVR_SERVER_HEARTBEAT_CMD)
{
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CONN(AG_CONNECTION_UNREGISTER_CMD)
{
	MSG_AG_CONNECTION_UNREGISTER_CMD *	pRecvMsg = (MSG_AG_CONNECTION_UNREGISTER_CMD *)pMsg;
	OnlineUser *								pUser = CHECK_USER( pRecvMsg->m_dwKey );

	LOGMSG( LOG_FULL,  "OnAG_CONNECTION_UNREGISTER_CMD [ID:%d]", pRecvMsg->m_dwKey );

	MSG_CG_CONNECTION_DISCONNECT_CMD msg;
	msg.m_dwErrorCode	= RC_CONNECTION_REQUESTFROMGAMESERVER;
	pUser->SendPacket( &msg, sizeof(msg) );

	pUser->Disconnect();
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_SYNC(AG_SYNC_PLAYER_ENTER_ACK )
{
	MSG_AG_SYNC_PLAYER_ENTER_ACK * pRecvMsg = (MSG_AG_SYNC_PLAYER_ENTER_ACK *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	///////////////////////////////////////////////
	MSG_CG_SYNC_PLAYER_ENTER_NAK msgNAK;
	IZone * pZone = theZoneManager.FindZone( pUser->GetZoneKey() );
	if( !pZone )
	{
		msgNAK.m_dwErrorCode = _RC_SYN(FIELD_IS_NULL)	;
		pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	///////////////////////////////////////////////
	MSG_CG_SYNC_PLAYER_ENTER_ACK msg;
	msg.m_fPos[0] = pRecvMsg->m_fPos[0];
	msg.m_fPos[1] = pRecvMsg->m_fPos[1];
	msg.m_fPos[2] = pRecvMsg->m_fPos[2];
	pUser->SendPacket( &msg, sizeof(msg) );

	pZone->EnterUser( pUser );


}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_SYNC(AG_SYNC_PLAYER_ENTER_NAK )
{
	MSG_AG_SYNC_PLAYER_ENTER_NAK * pRecvMsg = (MSG_AG_SYNC_PLAYER_ENTER_NAK *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	MSG_CG_SYNC_PLAYER_ENTER_NAK msgNAK;
	msgNAK.m_dwErrorCode	= pRecvMsg->m_dwErrorCode;										//! 烙矫
	pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_SYNC(AG_SYNC_PLAYER_WARP_ACK )
{
	MSG_AG_SYNC_PLAYER_WARP_ACK * pRecvMsg = (MSG_AG_SYNC_PLAYER_WARP_ACK *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	///////////////////////////////////////////////
	MSG_CG_SYNC_PLAYER_ENTER_ACK msg;
	msg.m_fPos[0] = pRecvMsg->m_fPos[0];
	msg.m_fPos[1] = pRecvMsg->m_fPos[1];
	msg.m_fPos[2] = pRecvMsg->m_fPos[2];
	pUser->SendPacket( &msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_SYNC(AG_SYNC_PLAYER_WARP_NAK )
{
	MSG_AG_SYNC_PLAYER_WARP_NAK * pRecvMsg = (MSG_AG_SYNC_PLAYER_WARP_NAK *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	MSG_CG_SYNC_PLAYER_ENTER_NAK msgNAK;
	msgNAK.m_dwErrorCode	= pRecvMsg->m_dwErrorCode;	
	pUser->SendPacket( &msgNAK, sizeof(msgNAK) );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_PARTY(AG_PARTY_BATTLE2GATE_INVITE_REQUEST_SYN )
{
	MSG_AG_PARTY_BATTLE2GATE_INVITE_REQUEST_SYN* pRecvMsg = (MSG_AG_PARTY_BATTLE2GATE_INVITE_REQUEST_SYN*)pMsg;

	//////////////////////////////////////////
	OnlineUser *pUser		= CHECK_USER( pRecvMsg->m_dwOtherUserKey );
	OnlineUser *pMaster	= (OnlineUser *)theUserManager.GetUser( pRecvMsg->m_dwKey );
	if( !pMaster )
		return FALSE;

	if( !pUser )
	{
		//////////////////////////////////////////
		MSG_CG_PARTY_INVITE_REQUEST_SERVER_NAK msgNAK;
		msgNAK.m_byErrorCode = RC::RC_PARTY_PLAYER_NOTEXIST;
		pMaster->SendPacket( &msgNAK, sizeof(msgNAK) );
		return FALSE;
	}


	//////////////////////////////////////////
	MSG_AG_PARTY_GATE2FIELD_INVITE_REQUEST_SYN msgSYN;
	msgSYN.m_dwMasterUserKey = pMaster->GetUserKey();
	lstrcpyn(msgSYN.m_szMasterName
          ,pMaster->GetSelectedCharName()
			 ,MAX_CHARNAME_LENGTH);
	pUser->SendToLinkedShell( &msgSYN, sizeof(msgSYN) );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_PARTY(AG_PARTY_FIELD2GATE_INVITE_RESPONSE_NAK )
{
	MSG_AG_PARTY_FIELD2GATE_INVITE_RESPONSE_NAK* pRecvMsg = (MSG_AG_PARTY_FIELD2GATE_INVITE_RESPONSE_NAK*)pMsg;
	OnlineUser *											pUser = CHECK_USER( pRecvMsg->m_dwMasterUserKey );
	if( !pUser )
		return FALSE;

	///////////////////////////////////////////////
	MSG_AG_PARTY_GATE2BATTLE_INVITE_RESPONSE_NAK msgNAK;
	msgNAK.m_byErrorCode = pRecvMsg->m_byErrorCode;
	pUser->SendToLinkedShell( &msgNAK, sizeof(msgNAK) );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_PARTY(AG_PARTY_BATTLE2GATE_JOINROOM_CMD )
{
	MSG_AG_PARTY_BATTLE2GATE_JOINROOM_CMD* pRecvMsg = (MSG_AG_PARTY_BATTLE2GATE_JOINROOM_CMD*)pMsg;
	OnlineUser *									pUser		= CHECK_USER( pRecvMsg->m_dwOtherUserKey );
	if( !pUser )
		return FALSE;

	///////////////////////////////////////////////
	IZone * pZone = theZoneManager.FindZone( pRecvMsg->m_RoomKey );
	if( !pZone )
		return FALSE;

	if( pZone->GetType() != pRecvMsg->m_byRoomType )
		return FALSE;

	pZone->OnRoomJoinCmd(pUser);
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ZONE(AG_ZONE_ROOM_JOIN_NAK )
{
	MSG_AG_ZONE_ROOM_JOIN_NAK * pRecvMsg = (MSG_AG_ZONE_ROOM_JOIN_NAK *)pMsg;

	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );
	ASSERT( pUser->IsBeginTransaction() );

	LOGMSG( LOG_FULL,  "AG_ZONE_ROOM_JOIN_NAK Error!!:进入地图失败!!!!!!" );

	SWITCH( pUser->GetTransactionState() )

	CASE( TK_JOIN_VILLAGE )
	{
		pUser->EndTransaction();
		//退出之前尝试进去的地图
		Village * pZone = theZoneManager.FindVillage( pUser->GetSelectedCharMapCode() );
		if( pZone )
		{
			//不发送到GameShell
			pZone->LeaveUser(pUser, eSEND_NONE);//eSEND_SYN);
		}

		//重新进入CharSelector
		util::BackToCharSelect(pUser);
		//theUserManager.RemoveUserName( pUser );
		//CharSelector * pCharSelect = theZoneManager.FindCharSelect(1);
		//pCharSelect->EnterUser( pUser );

		MSG_CG_CONNECTION_ENTERVILLAGEREQ_NAK msg;
		msg.m_dwErrorCode = pRecvMsg->m_dwErrorCode;
		pUser->SendPacket( &msg, sizeof(msg) );
	}

	DEFAULT
		LOGMSG( LOG_FULL,  "AG_ZONE_ROOM_JOIN_ACK 未识别状态." );
	SWITCH_END



	////////////////////////////////////////////////////

}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ZONE(AG_ZONE_ROOM_JOIN_ACK )
{
	MSG_AG_ZONE_ROOM_JOIN_ACK * pRecvMsg = (MSG_AG_ZONE_ROOM_JOIN_ACK *)pMsg;

	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );
	if( !pUser->IsBeginTransaction() )
	{
		// Game ACK TK_END
		LOGMSG( LOG_FULL,  "AG_ZONE_ROOM_JOIN_ACK事务未准备." );
		return FALSE;
	}

	////////////////////////////////////////
	pUser->EndTransaction();

	if(!pUser->OnRoomJoin(pRecvMsg))
	{
		MSG_CG_CONNECTION_ENTERVILLAGEREQ_ACK msg;
		msg.m_dwPlayerKey = pUser->GetPlayerKey();
		pUser->SendPacket( &msg, sizeof(msg) );
	}

}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ZONE(AG_ZONE_ROOM_LEAVE_ACK )
{
	MSG_AG_ZONE_ROOM_LEAVE_ACK * pRecvMsg = (MSG_AG_ZONE_ROOM_LEAVE_ACK *)pMsg;

	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );
	if( !pUser->IsBeginTransaction() )
	{
		LOGMSG( LOG_FULL,  "AG_ZONE_ROOM_LEAVE_ACK failed." );
		return FALSE;
	}

	//////////////////////////////////////////////////////
	eZONE_TYPE RoomType = ZONETYPE_MAX;

	if(pUser->GetTransactionState() == TK_BACKTOSELECT )
	{
		pUser->EndTransaction();
		util::BackToCharSelect(pUser);
		//theUserManager.RemoveUserName( pUser );

		MSG_CG_CONNECTION_BACKTOCHARSELECT_ACK msg;
		pUser->SendPacket( &msg, sizeof(msg) );

		return FALSE;
	}

	/////////////////////////////////////////////
	if(!pUser->OnRoomLeave(RoomType))
		return FALSE;


	///////////////////////////////////////////////
	IZone * pZone = theZoneManager.FindZone(pUser->GetTargetRoomKey());
	if( !pZone ) 
		return FALSE;

	pZone->ReadyUser( pUser, eSEND_SYN );

	//////////////////////////////////////////////
	if( pZone->GetType() != RoomType )
	{
		LOGMSG( LOG_CRITICAL,  "MSG_AG_ZONE_ROOM_LEAVE_ACK invalid type!\n" );
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ZONE(AG_ZONE_ROOM_LEAVE_NAK )
{
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ZONE(AG_ZONE_VILLAGE_MOVE_NAK )
{
	MSG_AG_ZONE_VILLAGE_MOVE_NAK *	pRecvMsg = (MSG_AG_ZONE_VILLAGE_MOVE_NAK *)pMsg;
	OnlineUser *							pUser = CHECK_USER( pRecvMsg->m_dwKey );

	///////////////////////////////////////////////
	MSG_CG_ZONE_VILLAGE_MOVE_NAK msg;
	msg.m_byErrorCode = pRecvMsg->m_byErrorCode;
	pUser->SendPacket( &msg, sizeof(msg) );
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ZONE(AG_ZONE_VILLAGE_MOVE_ACK )
{
	MSG_AG_ZONE_VILLAGE_MOVE_ACK * pRecvMsg = (MSG_AG_ZONE_VILLAGE_MOVE_ACK *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	///////////////////////////////////////////////
	MSG_CG_ZONE_VILLAGE_MOVE_NAK msg;
	if( pUser->IsBeginTransaction() )
	{
		msg.m_byErrorCode = RC::RC_ROOM_ALREADY_DOING_TRANSACTION;
		pUser->SendPacket( &msg, sizeof(msg) );
		return FALSE;
	}

	Village * pToVillage = theZoneManager.FindVillage( pRecvMsg->m_VillageMapCode );
	if( !pToVillage )
	{
		msg.m_byErrorCode = RC::RC_ROOM_NOTEXISTVILLAGE;
		pUser->SendPacket( &msg, sizeof(msg) );
		return FALSE;
	}

	///////////////////////////////////////////////
	RC::eROOM_RESULT rt;
	rt = theZoneManager.MoveZone(ZONETYPE_VILLAGE
                               ,pUser->GetZoneKey()
										 ,ZONETYPE_VILLAGE
										 ,pToVillage->GetKey()
										 ,pUser);
	if( RC::RC_ROOM_SUCCESS == rt )
	{
		MSG_CG_ZONE_VILLAGE_MOVE_ACK msg;
		pUser->SendPacket( &msg, sizeof(msg) );
	}
	else
	{
		msg.m_byErrorCode = rt;
		pUser->SendPacket( &msg, sizeof(msg) );
	}
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_STATUS( AG_STATUS_LEVEL_UP_CMD )
{
	MSG_AG_STATUS_LEVEL_UP_CMD * pRecvMsg = (MSG_AG_STATUS_LEVEL_UP_CMD *)pMsg;
	OnlineUser * pUser = CHECK_USER( pRecvMsg->m_dwKey );

	pUser->SetCharLevel( pUser->GetSelectedIndex(), pRecvMsg->m_Level );

	///////////////////////////////////////////////
	MSG_CG_STATUS_LEVEL_UP_CMD amsg;
	amsg.m_Level				= pRecvMsg->m_Level;
	amsg.m_wRemainStat		= pRecvMsg->m_wRemainStat;
	amsg.m_wRemainSkill		= pRecvMsg->m_wRemainSkill;	
	amsg.m_dwCurHP				= pRecvMsg->m_dwCurHP;	
	amsg.m_dwCurMP				= pRecvMsg->m_dwCurMP;	
	pUser->SendPacket( &amsg, sizeof(amsg) );
}
PACKETHANDLER_SERVER_END();




PACKETHANDLER_GAMESERVER_PARTY(AG_PARTY_BATTLE2GATE_CHANGE_MASTER_CMD )
{
	MSG_AG_PARTY_BATTLE2GATE_CHANGE_MASTER_CMD* pRecvMsg = (MSG_AG_PARTY_BATTLE2GATE_CHANGE_MASTER_CMD*)pMsg;
	OnlineUser *pMaster;
	
	pMaster = (OnlineUser *)theUserManager.GetUser( pRecvMsg->m_dwMasterUserKey );
	if( !pMaster )	
		return FALSE;

	IZone * pZone = theZoneManager.FindZone( pMaster->GetZoneKey() );
	if( !pZone )	return FALSE;

	switch( pZone->GetType() )
	{
	case ZONETYPE_HUNTING:
	case ZONETYPE_MISSION:
	case ZONETYPE_PVP:
		{
			IRoom * pRoom = (IRoom *)pZone;
			pRoom->ChangeToMaster( pMaster );
		}
		break;
	}
}
PACKETHANDLER_SERVER_END();



















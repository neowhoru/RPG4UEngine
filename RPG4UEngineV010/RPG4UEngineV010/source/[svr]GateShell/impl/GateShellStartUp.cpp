/*////////////////////////////////////////////////////////////////////////
文 件 名：GateShellStartUp.cpp
创建日期：2007年9月21日
最后更新：2007年9月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GateShellStartUp.h"
#include "ServerInfoParser.h"
#include "ServerShell.h"
#include "GateShell.h"
#include "PathInfoParser.h"

#include "ServerSessionFactoryManager.h"
#include "LoginSession.h"
//#include "GateSession.h"
#include "BattleSession.h"
#include "DBProxySession.h"
#include "FieldSession.h"
#include "GuildSession.h"
#include "MasterSession.h"
#include "ChatSession.h"
#include "TempServerSession.h"
#include "TempServerSession.h"

#include "MapInfoParser.h"
#include "LimitedMapPortParser.h"
#include "RequireFieldLevelInfoParser.h"

#include "ServerCommands.h"
#include "GameCommandManager.h"

#include "UserFactoryManager.h"
#include "UserManager.h"
//#include "AuthAgentServerInfoParser.h"

#include <ZoneFactoryManager.h>

#include "PathInfoParser.h"
#include "MediaPathManager.h"
#include "PacketCryptManager.h"
#include "ZoneManager.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GateShellStartUp::GateShellStartUp()
{
}
GateShellStartUp::~GateShellStartUp()
{
}


BOOL GateShellStartUp::LoadGameSetting()	
{
	//ServerInfoParser info;
	__BOOL_SUPER(	LoadGameSetting()		);	
	return TRUE;
} 

BOOL GateShellStartUp::LoadGameInfo() 		
{
	////////////////////////////////////////////
	//日志输出
	//if( !theAuthAgentServerInfoParser.LoadFrom( SETTING_FILE ) )
	//{
	//	LOGINFO( "LoginShell.ini parsing failed.\n" );
	//	return FALSE;
	//}

	__BOOL_SUPER(	LoadGameInfo() 		);	

	////////////////////////////////////////////////
	__INITINST2(thePacketCryptManager,"data\\Enc1.dat" , "data\\Dec2.dat");


	return TRUE;
} 

BOOL GateShellStartUp::LoadGameRes() 	
{
	__BOOL_SUPER(	LoadGameRes() 		);

	///////////////////////////////////////////////////
	__INITINST(theMapInfoParser,  100);//opt.m_wMapInfo );
	__LOAD3	( theMapInfoParser	
				,_MEDIAPATH(ePATH_DATAINFO)
				, "MapInfo(World).info"
				, "MapInfo(Field).info"  );


	///////////////////////////////////////////////////
	__LOADSVR( theLimitedMapPortParser,  "LimitedMapPort.txt"  );

	///////////////////////////////////////////////////
	theRequireFieldLevelInfoParser.Init( 100 );
	__INIT(theRequireFieldLevelInfoParser.Load( "data\\RequireFieldLevelInfo.info" ) );


	return TRUE;
} 

BOOL GateShellStartUp::InitGameEngine()
{
	DWORD* arSizes = theServerInfoParser.GetSessionPoolSizes(theServerShell.GetServerType());
	__VERIFY_PTR(arSizes,_T("Invalid PoolSize"));

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	REG_SERVERSESSIONFACTORY2(	LoginSession 		,LOGIN_SHELL);
	//REG_SERVERSESSIONFACTORY2(GateSession 			,GATE_SHELL  	);
	REG_SERVERSESSIONFACTORY2(	DBProxySession 		,PLAYER_DBPROXY  	);
	REG_SERVERSESSIONFACTORY2(	BattleSession 			,BATTLE_SHELL 	);
	REG_SERVERSESSIONFACTORY2(	FieldSession 			,FIELD_SHELL 		);
	REG_SERVERSESSIONFACTORY2(	GuildSession 			,GUILD_SHELL 		);
	REG_SERVERSESSIONFACTORY2(	MasterSession 			,MASTER_SHELL  	);
	REG_SERVERSESSIONFACTORY2(	ChatSession 			,CHAT_SHELL 		);
	REG_SERVERSESSIONFACTORY2(	TempServerSession 			,TEMP_SHELL  		);


	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


	//theUserFactoryManager.InitAllFactories();



	theZoneManager.Init();


	////////////////////////////////////
	__BOOL_SUPER(	InitGameEngine()	);

	////////////////////////////////////
	REG_GAMECOMMAND(Offline);


	return TRUE;
} 

BOOL GateShellStartUp::InitGameFunc()		
{
	__BOOL_SUPER(	InitGameFunc()			);
	return TRUE;
} 


void GateShellStartUp::ReleaseGameFunc()
{
	_SUPER::ReleaseGameFunc();
}

void GateShellStartUp::ReleaseGameEngine()
{
	//__END(theUserFactoryManager.ReleaseAllFactories());
	__END(theZoneManager.Release());

	_SUPER::ReleaseGameEngine();
}

void GateShellStartUp::DestroyGameRes()
{
	theMapInfoParser.Release();
	theLimitedMapPortParser.Release();
	theRequireFieldLevelInfoParser.Release();

	_SUPER::DestroyGameRes();
}

void GateShellStartUp::DestroyGameInfo()
{
	thePacketCryptManager.Release();


	_SUPER::DestroyGameInfo();
}

void GateShellStartUp::DestroyGameSetting()
{
	_SUPER::DestroyGameSetting();
}


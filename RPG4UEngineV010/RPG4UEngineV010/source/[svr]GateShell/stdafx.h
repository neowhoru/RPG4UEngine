#ifndef __STDAFX_H__
#define __STDAFX_H__


#pragma once


#define WIN32_LEAN_AND_MEAN
#define __NEW_WORLD_SCRIPT

#define _USE_GAME
#include <ServerIIInclude.h>
#include <ServerBaseGateInc.h>
#include <ServerMainGateInc.h>

//#include <oledb.h>


#include "ConstDefine.h"
#include "PacketStruct_Base.h"
#include <TSingleton.h>
#include <TLinkedList.h>
#include <CriticalSection.h>
#include <TMemoryPoolFactory.h>
#include <ResultCode.h>
#include <AssertUtil.h>
#include <MiscUtil.h>
//#include <SimpleModulus.h>

#include "ServerGroupLog.h"
#include "GateShellLog.h"






#endif // __STDAFX_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：QueryStruct.h
创建日期：2009年6月18日
最后更新：2009年6月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __QUERYSTRUCT_H__
#define __QUERYSTRUCT_H__
#pragma once

#include "QueryDefine.h"
#include "DatabaseProxyDefine.h"

#pragma pack(push, 1)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_SELECTEVENT : public sRESULT_QUERY
{
	INT	m_ItemSeq;
	BYTE	m_ItemCode;
	BYTE	m_ItemCount;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_SELECTWAREHOUSE : public sRESULT_QUERY
{
	union
	{
		BYTE	m_byaWarehouseItem[MAX_WAREHOUSESTREAM_SIZE];
	struct
	{
		BYTE	m_pWarehouse0[MAX_WAREHOUSESTREAM_PAGESIZE];
		BYTE	m_pWarehouse1[MAX_WAREHOUSESTREAM_PAGESIZE];
		BYTE	m_pWarehouse2[MAX_WAREHOUSESTREAM_PAGESIZE];
		BYTE	m_pWarehouse3[MAX_WAREHOUSESTREAM_PAGESIZE];
	};
	};
	MONEY		m_Money;
	DWORD		m_dwPassword;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_SELECTCHAR: public sRESULT_QUERY
{
	USERGUID	m_UserGuid;
	BYTE		m_bySlot;
	BYTE		m_byClass;
	CHARGUID	m_CharGuid;
	TCHAR		m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE		m_byHeight;
	BYTE		m_byFace;
	BYTE		m_byHair;
	BYTE		m_byHairColor;
	BYTE		m_bySex;
	LEVELTYPE	m_LV;
	SHORT		m_sStrength;
	SHORT		m_sDexterity;
	SHORT		m_sVitality;
	SHORT		m_sInteligence;
	SHORT		m_sSpirit;
	SHORT		m_sLearn;
	SHORT		m_sCredit;
	SHORT		m_sSkillStat1;
	SHORT		m_sSkillStat2;
	UPTYPE		m_UserPoint;
	INT			m_iExp;
	INT			m_iMaxHP;
	INT			m_iHP;
	INT			m_iMaxMP;
	INT			m_iMP;

	MONEY		m_Money;
	INT			m_iRemainStat;
	INT			m_iRemainSkill;
	INT			m_iSelectStyleCode;
	BYTE		m_byPKState;
	BYTE		m_byCharState;
	TIMESTAMP_STRUCT		m_StateTime;//INT64		m_StateTime
	INT						m_iRegion;
	SHORT		m_sX;
	SHORT		m_sY;
	SHORT		m_sZ;
	TCHAR		m_szTitleID[MAX_TITLE_LENGTH];
	TIMESTAMP_STRUCT		m_TitleTime;//INT64		m_TitleTime
	BYTE		m_byInvisibleOpt;
	BOOL		m_bInventoryLock;
	BYTE		m_pTempInventoryItem[MAX_TEMPINVENTORYITEMSTREAM_SIZE];
	BYTE		m_pEquipItem[MAX_EQUIPITEMSTREAM_SIZE];

	union
	{
		BYTE		m_pInventoryItem	[MAX_INVENTORYITEMSTREAM_PAGESIZE * MAX_DB_INVENTORY_PAGE_NUM];
	struct
	{
		BYTE		m_pInventory0		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory1		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory2		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory3		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory4		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
	};
	};

	BYTE		m_pSkill[MAX_SKILLSTREAM_SIZE];
	BYTE		m_pQuick[MAX_QUICKSTREAM_SIZE];
	BYTE		m_pStyle[MAX_STYLESTREAM_SIZE];
	BYTE		m_pQuest[MAX_QUESTSTREAM_SIZE];
	BYTE		m_pMission[MAX_MISSIONSTREAM_SIZE];
	INT		m_iPlayLimitedTime;

	//-----------PVP---------------------------------
	INT			m_iPVPPoint;
	INT			m_iPVPScore;
	BYTE			m_byPVPGrade;
	INT			m_iPVPDraw;
	INT			m_iPVPSeries;
	INT			m_iPVPKill;
	INT			m_iPVPDie;
	INT			m_iPVPMaxKill;
	INT			m_iPVPMaxDie;

	//------ GUILD--------------------------------
	TCHAR			m_szGuildName[MAX_GUILDNAME_LENGTH];
	GUILDGUID	m_GuildGuid;
	BYTE			m_GuildPosition;
	TCHAR			m_szGuildNickName[MAX_CHARNAME_LENGTH];

	//DWORD		m_Sts;
};





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPARAM_UPDATEWAREHOUSE: public sPARAM_QUERY
{
	INT			m_Result;
	union
	{
	BYTE		m_pWarehouseItem[MAX_WAREHOUSESTREAM_SIZE];
	struct
	{
		BYTE	m_pWarehouse0[MAX_WAREHOUSESTREAM_PAGESIZE];
		BYTE	m_pWarehouse1[MAX_WAREHOUSESTREAM_PAGESIZE];
		BYTE	m_pWarehouse2[MAX_WAREHOUSESTREAM_PAGESIZE];
		BYTE	m_pWarehouse3[MAX_WAREHOUSESTREAM_PAGESIZE];
	};
	};
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPARAM_UPDATEITEMS: public sPARAM_QUERY
{
	enum
	{ 
		PAGE_NUM = 4,
	};

	INT			m_Result;
	union
	{
		BYTE		m_pItems	[MAX_DB_INVENTORYITEMSTREAM_PAGESIZE * PAGE_NUM];
	struct
	{
		BYTE		m_pItems0		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pItems1		[MAX_INVENTORYITEMSTREAM_PAGESIZE]; //47passed
		BYTE		m_pItems2		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pItems3		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
	};
	};
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPARAM_UPDATECHAR: public sPARAM_QUERY
{
	INT			m_Result;
	BYTE			m_pEquipItem	[MAX_EQUIPITEMSTREAM_SIZE];
	BYTE			m_pTempInventoryItem[MAX_TEMPINVENTORYITEMSTREAM_SIZE];
	union
	{
		BYTE		m_pInventoryItem	[MAX_DB_INVENTORYITEMSTREAM_PAGESIZE * MAX_DB_INVENTORY_PAGE_NUM];
	struct
	{
		BYTE		m_pInventory0		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory1		[MAX_INVENTORYITEMSTREAM_PAGESIZE]; //47passed
		BYTE		m_pInventory2		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory3		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		BYTE		m_pInventory4		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
	};
	};
	BYTE		m_pSkill[MAX_SKILLSTREAM_SIZE];
	BYTE		m_pQuick[MAX_QUICKSTREAM_SIZE];
	BYTE		m_pStyle[MAX_STYLESTREAM_SIZE];
	BYTE		m_pQuest[MAX_QUESTSTREAM_SIZE];
	BYTE		m_pMission[MAX_MISSIONSTREAM_SIZE];
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_CREATEITEMSERIAL: public sRESULT_QUERY
{
	enum
	{ 
		DBSERIAL_SPACE_SIZE = 1000
	};
	DBSERIAL	m_dwCurSerial;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_LOGINUSER : public sRESULT_QUERY
{
	USERGUID	m_UserGuid;
	TCHAR		m_szPassword[MAX_MD5_BUF_LENGTH];
	BYTE		m_bySts;
	TCHAR		m_szCharPwd	[MAX_MD5_BUF_LENGTH];
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_SELECTUSER : public sRESULT_QUERY
{
	USERGUID	m_UserGuid;
	CHARGUID	m_CharGuid;
	BYTE		m_bySlot;
	TCHAR		m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE		m_byHeight;
	BYTE		m_byFace;
	BYTE		m_byHair;
	BYTE		m_byHairColor;
	BYTE		m_bySex;
	BYTE		m_byClass;
	LEVELTYPE	m_LV;
	DWORD		m_dwRegion;
	WORD		m_wX;
	WORD		m_wY;
	WORD		m_wZ;
	BYTE		m_pEquipItem[MAX_EQUIPITEMSTREAM_SIZE];
	GUILDGUID	m_GuildGuid;
	BYTE		m_GuildPosition;
	TCHAR		m_szGuildNick[MAX_CHARNAME_LENGTH];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_CREATECHAR  : public sRESULT_QUERY
{
	USERGUID	m_UserGuid;
	CHARGUID	m_CharGuid;
	BYTE		m_bySlot;
	TCHAR		m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE		m_byHeight;
	BYTE		m_byFace;
	BYTE		m_byHair;
	BYTE		m_byHairColor;//leo sex
	BYTE		m_bySex;//leo
	BYTE		m_byClass;
	LEVELTYPE	m_LV;
	INT			m_iRegion;
	SHORT		m_sX;
	SHORT		m_sY;
	SHORT		m_sZ;
	BYTE		m_pEquipItem[MAX_EQUIPITEMSTREAM_SIZE];
};




#pragma pack(pop)

#endif //__QUERYSTRUCT_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：DBUser.h
创建日期：2009年6月17日
最后更新：2009年6月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __DBUSER_H__
#define __DBUSER_H__
#pragma once


#include "CommonDefine.h"
#include <Timer.h>
#include "DBUserDefine.h"
#include "BitHandle.h"


struct sPLAYERINFO_BASE;
struct sPLAYER_SERVER_PART;
class IServerSession;
class Query;
enum eDBP_SAVE_STATE;
enum eDBP_LOAD_STATE;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEDBP_API DBUser
{
public:
	DBUser(void);
	virtual ~DBUser(void);


public:
	virtual	DWORD	GetFactoryType(){return DBUSER_TYPE_DEFAULT;}

public:
	virtual BOOL		Init		();
	virtual VOID		Release	();
	virtual VOID		Update	();

	virtual BOOL		_Reset()=0;
	virtual BOOL		UpdateExpired	()=0;
	virtual VOID		UpdateDataToDB	()=0;
	virtual VOID		StartCacheExpiredTimer	()=0;
	virtual VOID		StopCacheExpiredTimer	()=0;
	


public:


	//////////////////////////////////////////////////
	DBUSER_PTY_DECL(Style				,STYLE_STREAM	,m_arStyles				,	StylePart.Code		,MAX_STYLE_SLOT_NUM				,MAX_STYLESTREAM_SIZE					);
	DBUSER_PTY_DECL(Quick				,QUICK_STREAM	,m_arQuicks				,	QuickPart.Code		,MAX_QUICK_SLOT_NUM				,MAX_QUICKSTREAM_SIZE					);
	DBUSER_PTY_DECL(Skill				,SKILL_STREAM	,m_arSkills				,	SkillPart.wCode	,MAX_SKILL_SLOT_NUM				,MAX_SKILLSTREAM_SIZE					);
	DBUSER_PTY_DECL(EquipItem			,ITEMOPT_STREAM,m_arEquips				,	ItemPart.dwSerial	,MAX_EQUIPMENT_SLOT_NUM			,MAX_EQUIPITEMSTREAM_SIZE				);
	DBUSER_PTY_DECL(InventoryItem		,ITEMOPT_STREAM,m_arInventories		,	ItemPart.dwSerial	,MAX_INVENTORY_SLOT_NUM			,MAX_INVENTORYITEMSTREAM_SIZE			);
	DBUSER_PTY_DECL(TempInventoryItem,ITEMOPT_STREAM,m_arTempInventories	,	ItemPart.dwSerial	,MAX_TEMPINVENTORY_SLOT_NUM	,MAX_TEMPINVENTORYITEMSTREAM_SIZE	);



	//////////////////////////////////////////////////
	virtual BOOL	ExistWarehouseItem		( SLOTPOS pos );
	virtual VOID	SetWarehouseItem			( SLOTPOS pos, ITEMOPT_STREAM * IN pStream );
	virtual VOID	GetWarehouseItem			( SLOTPOS pos, ITEMOPT_STREAM * OUT pStream );
	virtual VOID	ClearWarehouseItems		();
	virtual VOID	SerializeWarehouseItems	(BYTE *     pWarehouseStream
														,WORD       wSize
														,eSERIALIZE eType = SERIALIZE_LOAD
														,INT			nStartPage=0);



	virtual VOID		ClearQuests			( SLOTIDX slotIdx );
	virtual VOID		ClearMissions		( SLOTIDX slotIdx );
	
	//////////////////////////
	virtual VOID		SerializeCharInfo	(SLOTIDX            slotIdx
                                       ,sPLAYERINFO_BASE & rCharInfo
													,eSERIALIZE eType = SERIALIZE_LOAD);

	virtual void		SerializeData		(BYTE *      pStream
													 ,BYTE*		 pBuffer
													 ,WORD       wSize
													 ,eSERIALIZE eType);


	//////////////////////////////////////////////////
	DBUSER_SERIAL_DECL(Quest				);
	DBUSER_SERIAL_DECL(Mission				);
	DBUSER_SERIAL_DECL(Style				);
	DBUSER_SERIAL_DECL(Quick				);
	DBUSER_SERIAL_DECL(Skill				);
	DBUSER_SERIAL_DECL(EquipItem			);
	DBUSER_SERIAL_DECL(InventoryItem		);
	DBUSER_SERIAL_DECL(TempInventoryItem);




public:
	virtual VOID			SendAllToGameShell		( IServerSession * pServerSession );
	virtual VOID			MakeUpdateCharacterQuery( Query *	pQuery
																, Query*			pQuery2=NULL
																, Query*			pQuery3=NULL)=0;
	virtual VOID			MakeUpdateWarehouseQuery( Query *	pQuery 
																, Query*			pQuery2=NULL
																, Query*			pQuery3=NULL
																, Query*			pQuery4=NULL)=0;

	///////////////////////////////////////////////////////////
public:
	CHARGUID					GetSelectedCharGuid		();
	sPLAYERINFO_BASE &	GetSelectedCharInfo		();

	VOID						SetOccupiedSlot			( SLOTIDX slotIdx, BOOL val );
	BOOL						IsValidSlot					( SLOTIDX slotIdx ) ;
	
	USERGUID					GetUserGUID					() ;

	///////////////////////////////////////////////////////////
public:
	BitHandle						m_PacketFlags;
	sDBUSER_TOTAL_INFO			m_TotalInfos[MAX_CHARACTER_LIST_NUM];

protected:
	VG_DWORD_PROPERTY				(UserKey);
	VG_DWORD_PROPERTY				(ServerSessionIndex);

	VG_TYPE_PROPERTY				(WarehouseMoney, MONEY);
	VG_BOOL_PROPERTY				(WarehouseModified);
	BYTE								m_pWarehouseStream[MAX_WAREHOUSESTREAM_SIZE];

	VG_TYPE_PROPERTY				(CacheState,	eDBP_CACHE_STATE);
	VG_TYPE_PROPERTY				(State,			eDBUSER_STATE);

	VG_TYPE_PROPERTY				(DBSaveState		, eDBP_SAVE_STATE);
	VG_TYPE_PROPERTY				(DBLoadState		, eDBP_LOAD_STATE);
	VG_TYPE_PROPERTY				(SelectedSlotIndex, BYTE);

	///////////////////////////////////////////////////////////
	BOOL								m_arSlotOccupied[MAX_CHARACTER_LIST_NUM];
	/// SKILL
	util::Timer						m_DBUpdateTimer;
	util::Timer						m_CacheExpiredTimer;

};


#include "DBUser.inl"


#endif //__DBUSER_H__
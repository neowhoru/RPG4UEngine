/*////////////////////////////////////////////////////////////////////////
文 件 名：IZone.h
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __IZONE_H__
#define __IZONE_H__
#pragma once



//=============================================================================================================================
#include <CommonDefine.h>
#include <IZoneDefine.h>
#include <THashTable.h>
#include "ZoneStruct.h"
#include "ResultCode.h"


class User;
class IServerSession;
using namespace RC;
using namespace util;

typedef THashTable<User*>					UserTable;
typedef THashTable<User *>::iterator	UserTableIt;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGATE_API IZone
{
public:
	IZone(void);
	IZone( DWORD dwPoolSize );
	virtual ~IZone(void);
	
public:
	virtual eZONE_TYPE	GetType() = 0;
	virtual DWORD			GetFactoryType(){return GetType();}

public:
	virtual BOOL	Create	();
	virtual BOOL	Create	(KEYTYPE          key
                           ,CODETYPE         MapCode
									,IServerSession * pServerSession);
	virtual VOID	Destroy	();

	virtual BOOL			Process			() { return TRUE; }
	virtual eROOM_RESULT CanJoin			( User * /*pUser*/, eZONE_OPEN_STATE /*RoomPublic*/, LPCTSTR /*szRoomPwd*/ ) { return RC_ROOM_SUCCESS; }
	virtual DWORD			GetUserAmount	();

public:
	virtual VOID	ReadyUser(User * pUser
                           ,eSEND_TYPE   type)		;
	virtual VOID	EnterUser(User * pUser );
	virtual VOID	LeaveUser(User * pUser
                           ,eSEND_TYPE   type)		;
	virtual VOID	SendToAll(MSG_BASE *    pMsg
                           ,WORD          wSize
									,DWORD			dwExceptUserKey = 0);

	virtual VOID	DestroyUser(User * pUser );

	
	template<class OPR>	VOID	Foreach		( OPR & op );
	template<class OPR>	BOOL	SearchUser	( OPR & op );

public:
	
	// virtual
	virtual void		OnRoomJoinCmd		(User* ){}
	virtual VOID		_OnCreate(){}
	virtual VOID		_OnDestroy(){}
	virtual VOID		_OnReadyUser( User * /*pUser*/ ){}
	virtual VOID		_OnEnterUser( User * /*pUser*/ ){}
	virtual VOID		_OnLeaveUser( User * /*pUser*/ ){}

	virtual eROOM_RESULT _CanCreate	( User * /*pUser*/ ) { return RC_ROOM_SUCCESS; }
	virtual eROOM_RESULT _CanJoin		( User * /*pUser*/ ) { return RC_ROOM_SUCCESS; }

	// User Hash
public:

	UserTable &		GetUsers		();
	DWORD				GetUserNum	();
	User *			GetFirstUser();
	User *			FindUser		( DWORD playerKey )	;
	VOID				RemoveUser	( DWORD playerKey )		;
	VOID				AddUser		( User * pUser, DWORD playerKey )	;


	// session
	VOID	SendToLinkedShell( MSG_OBJECT_BASE * pMsg, WORD wSize );

protected:
	VG_TYPE_PROPERTY		(MapCode, CODETYPE);
	VG_TYPE_PROPERTY		(Key, KEYTYPE);
	VG_PTR_PROPERTY		(LinkedShell,IServerSession);

	VG_BOOL_PROPERTY		(ReusedKey);
	VG_BOOL_PROPERTY		(ReusedServerSession);

private:
	UserTable				m_Users;
};


#include "IZone.inl"


#endif //__IZONE_H__
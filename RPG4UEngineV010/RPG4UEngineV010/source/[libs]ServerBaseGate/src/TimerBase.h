
#pragma once

#include <Timer.h>

class _SERVERBASEGATE_API TimerBase
{
	friend class TimeKeeper;
public:
	TimerBase();
	virtual ~TimerBase();

public:
	enum eTIME_STATE 
	{ 
		 STATE_NONE
		,STATE_START
		,STATE_END
		,STATE_STOP
	};

public:
	VOID				Start	( DWORD dwCurrentTime );
	VOID				Stop	();
	VOID				Resume();
	VOID				Reset	();
	VOID				Close	();

public:
	virtual VOID		OnStart(){}
	virtual VOID		OnExpired(){}
	
private:
	VOID				_CalcElapsedTime	();
	VOID				_Update				();

private:
	DWORD					m_dwCurrentTime;
	DWORD					m_dwLastTime;
	DWORD					m_dwElapsedTime;
	DWORD					m_dwState;
};

/*////////////////////////////////////////////////////////////////////////
文 件 名：UserDefine.h
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __USERDEFINE_H__
#define __USERDEFINE_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD				   USER_KEY;	//ID类型的Factory
typedef DWORD			      USER_TYPE;
#define USER_NULL				((USER_TYPE)-1)



enum eTRANSACTION_KIND 
{
	 TK_END								= _BIT( 0 )
	,TK_BEGIN							= _BIT( 1 )
	////////////////////////////////
	,TK_CREATEHUNTING					= ( TK_BEGIN | _BIT(2 ) )
	,TK_JOINHUNTING					= ( TK_BEGIN | _BIT(3 ) )
	,TK_LEAVEHUNTING					= ( TK_BEGIN | _BIT(4 ) )
	////////////////////////////////
	,TK_BACKTOSELECT					= ( TK_BEGIN | _BIT(5 ) )
	////////////////////////////////
	,TK_CREATELOBBY					= ( TK_BEGIN | _BIT(6 ) )
	,TK_JOINLOBBY						= ( TK_BEGIN | _BIT(7 ) )
	,TK_LEAVELOBBY						= ( TK_BEGIN | _BIT(8 ) )
	////////////////////////////////
	,TK_CREATEMISSION					= ( TK_BEGIN | _BIT(9 ) )
	,TK_JOINMISSION					= ( TK_BEGIN | _BIT(10) )
	,TK_LEAVEMISSION					= ( TK_BEGIN | _BIT(11) )
	////////////////////////////////
	,TK_CREATEPVP						= ( TK_BEGIN | _BIT(12) )
	,TK_JOINPVP							= ( TK_BEGIN | _BIT(13) )
	,TK_LEAVEPVP						= ( TK_BEGIN | _BIT(14) )
	////////////////////////////////
	,TK_LEAVE_LOBBY_FOR_HUNTING	= ( TK_BEGIN | _BIT(15) )
	,TK_LEAVE_LOBBY_FOR_MISSION	= ( TK_BEGIN | _BIT(16) )
	,TK_LEAVE_LOBBY_FOR_PVP			= ( TK_BEGIN | _BIT(17) )
	,TK_LEAVE_HUNTING_FOR_VILLAGE	= ( TK_BEGIN | _BIT(18) )
	,TK_LEAVE_MISSION_FOR_VILLAGE	= ( TK_BEGIN | _BIT(19) )
	,TK_LEAVE_PVP_FOR_VILLAGE		= ( TK_BEGIN | _BIT(20) )
	,TK_LEAVE_HUNTING_FOR_HUNTING	= ( TK_BEGIN | _BIT(21) )
	,TK_JOIN_VILLAGE					= ( TK_BEGIN | _BIT(22) )
	,TK_CHARACTER_LIST				= ( TK_BEGIN | _BIT(23) )
	,TK_CHARACTER_CREATION			= ( TK_BEGIN | _BIT(24) )
	,TK_CHARACTER_DESTRUCTION		= ( TK_BEGIN | _BIT(25) )
};

enum eUSER_STATE	 
{	
	 USER_STATE_POOL				
	,USER_STATE_INIT				
	,USER_STATE_CHARLIST
};



#endif //__USERDEFINE_H__
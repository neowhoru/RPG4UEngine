/*////////////////////////////////////////////////////////////////////////
文 件 名：ZoneManager.h
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ZONEMANAGER_H__
#define __ZONEMANAGER_H__
#pragma once

#include <ZoneManagerDefine.h>
#include <KeyGenerator.h>
#include <THashTable.h>
#include <IZoneDefine.h>
#include <ResultCode.h>

class User;
class IZone;
class IRoom;
class IServerSession;
class Village;
class LobbyRoom;
class HuntingRoom;
class MissionRoom;
class PVPRoom;
class CharSelector;

using namespace RC;
using namespace util;

enum ePARTY_INDEX
{
	 PARTY_INDEX_NONE	= 0
	,PARTY_INDEX_A		= 1
	,PARTY_INDEX_B		= 2
	,PARTY_INDEX_MAX
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGATE_API ZoneManager
{
	friend class IZone;
public:
	ZoneManager(void);
	virtual ~ZoneManager(void);

public:
	virtual BOOL Init		();
	virtual VOID Release	();

public:
	//实例创建管理
	virtual IZone*    AllocZone(ZONE_TYPE type /* =ZONE_NULL*/)	;
	virtual void		FreeZone	(IZone* pZone)	;

public:
// ---------------------------------------------------
	virtual VOID			 CreateCharSelect	()=0;
	virtual eROOM_RESULT CreateVillage		(CODETYPE MapCode
														,IZone ** ppVillage)=0		;
	virtual eROOM_RESULT CreateLobbyRoom	(KEYTYPE             VillageKey
														,User *        pMasterUser
														,CODETYPE            MapCode
														,eZONE_TYPE          eLobbyType
														,eZONE_OPEN_STATE    ePublic
														,LPCTSTR             szRoomTitle
														,LPCTSTR             szRoomPWD
														,sROOMINFO_BASE & IN RoomInfo
														,LobbyRoom *& OUT    pLobbyRoom)=0;

	virtual eROOM_RESULT CreateHuntingRelay(KEYTYPE                   PrevHuntingKey
														,User *              pMasterUser
														,CODETYPE                  MapCode
														,eZONE_OPEN_STATE          ePublic
														,LPCTSTR                   szRoomPWD
														,sROOMINFO_BASE & IN       RoomInfo
														,sROOMINFO_ADDITIONAL & IN AdditionalRoomInfo
														,HuntingRoom *& OUT        pHuntingRoom)=0;

	virtual eROOM_RESULT CreateHuntingRoom	(KEYTYPE            LobbyKey
														,User *       pMasterUser
														,HuntingRoom *& OUT pHuntingRoom)=0;
	virtual eROOM_RESULT CreateMissionRoom	(KEYTYPE            LobbyKey
														,User *       pMasterUser
														,MissionRoom *& OUT pMissionRoom)=0;
	virtual eROOM_RESULT CreatePVPRoom		(KEYTYPE        LobbyKey
														,User *   pMasterUser
														,PVPRoom *& OUT pPVPRoom)=0;


	///////////////////////////////////////////////////////
public:

	virtual eROOM_RESULT CreateRoom	(eZONE_TYPE			  zoneType
                                    ,KEYTYPE            LobbyKey
												,User *       pMasterUser
												,IRoom*&				  pRoomRet);
												 
	virtual eROOM_RESULT MoveZone	(eZONE_TYPE       FromZoneType
                                 ,KEYTYPE          FromZoneKey
											,eZONE_TYPE       ToZoneType
											,KEYTYPE          ToZoneKey
											,User *     pUser
											,eZONE_OPEN_STATE RoomPublic=ZONEOPEN_PUBLIC
											,LPCTSTR          szRoomPWD=NULL)=0;

	///////////////////////////////////////////////////////
protected:
	virtual BOOL			_PrevCreateRoom	(IServerSession*&	pServerSession)=0;

	virtual eROOM_RESULT MoveZone	(IZone *  pFromZone
                                 ,IZone *  pToZone
											,User *     pUser
											,eZONE_OPEN_STATE RoomPublic=ZONEOPEN_PUBLIC
											,LPCTSTR          szRoomPWD=NULL)=0;

public:
	// ----------------------------------
	virtual eROOM_RESULT KickUserAtLobby	(User *      pMaster
                                          ,DWORD             TargetPlayerKey
														,User *& OUT pTargetUser)=0;
	virtual eROOM_RESULT ToggleReadyInLobby(User * pUser
                                          ,BOOL         bReady)		=0;
	virtual eROOM_RESULT ChangeTeamInLobby	(User * pUser
                                          ,ePARTY_INDEX team)		=0;

	//////////////////////////////////////////
	virtual eROOM_RESULT ChangeMapInLobby	(User * pMaster
                                          ,CODETYPE     MapCode)	=0	;
	virtual eROOM_RESULT ConfigHuntingRoomInLobby(User *              pMaster
																,sROOMINFO_ADDITIONAL & IN AdditionalInfo)	=0	;
	virtual eROOM_RESULT ConfigPVPRoomInLobby		(User *             pMaster
																,sPVPINFO_ADDITIONAL & IN AdditionalPVPInfo)	=0	;
	virtual eROOM_RESULT SetMaxSlotNumInLobby	(User * pMaster
															,BYTE         MaxUser)=0	;

	//////////////////////////////////////////
	virtual VOID GetMissionLobbyRoomList	(User *            pUser
                                 ,BYTE                    ReqCount
											,BYTE                    nMaxCount
											,BYTE & OUT              nCount
											,sROOMINFO_MISSION * OUT pRoomInfoArray)=0;
	virtual VOID GetHuntingLobbyRoomList	(User *            pUser
                                 ,BYTE                    ReqCount
											,BYTE                    nMaxCount
											,BYTE & OUT              nCount
											,sROOMINFO_HUNTING * OUT pRoomInfoArray)=0;
	virtual VOID GetPVPLobbyRoomList		(User *        pUser
                                 ,BYTE                ReqCount
											,BYTE                nMaxCount
											,BYTE & OUT          nCount
											,sROOMINFO_PVP * OUT pRoomInfoArray)=0;
	virtual VOID GetHuntingRoomList			(User *            pUser
                                 ,BYTE                    ReqCount
											,BYTE                    nMaxCount
											,BYTE & OUT              nCount
											,sROOMINFO_HUNTING * OUT pRoomInfoArray)=0;

	virtual IRoom * GetRecommendRoom	(User * pUser
                                    ,eZONE_TYPE   RoomType)		=0;


	// --------------------------------------------------------------------------------
	virtual VOID Process	()=0;
	virtual VOID Display	()=0;
	virtual VOID DisplayerZonePoolInfo		()=0;
	virtual VOID DestroyZone					( IZone * pZone )=0;

	virtual DWORD GetHuntingRoomAmount	()=0;
	virtual DWORD GetMissionRoomAmount	()=0;

public:
	DECL_ZONE_OPR	(Zone				,IZone	);
	DECL_ZONE_OPR	(Village			,Village 		);
	DECL_ZONE_OPR	(CharSelector		,CharSelector		);
	DECL_ZONE_OPR	(Lobby			,LobbyRoom		);
	DECL_ZONE_OPR	(Hunting			,HuntingRoom	);
	DECL_ZONE_OPR	(Mission			,MissionRoom	);
	DECL_ZONE_OPR	(PVP				,PVPRoom 		);
	DECL_ZONE_OPR	(HuntingLobby	,LobbyRoom		);
	DECL_ZONE_OPR	(MissionLobby	,LobbyRoom		);
	DECL_ZONE_OPR	(PVPLobby		,LobbyRoom	)	;



	DECL_ZONE_COUNT(Village				);
	DECL_ZONE_COUNT(HuntingLobbyRoom	);
	DECL_ZONE_COUNT(MissionLobbyRoom	);
	DECL_ZONE_COUNT(PVPLobbyRoom		);
	DECL_ZONE_COUNT(HuntingRoom		);
	DECL_ZONE_COUNT(MissionRoom		);
	DECL_ZONE_COUNT(PVPRoom				);



protected:
	virtual KEYTYPE		_AllocKey	()				;
	virtual VOID			_FreeKey		( KEYTYPE Key);

	virtual VOID			_AddRoom		( IZone * pRoom )=0;
	virtual VOID			_RemoveRoom	( IZone * pRoom )=0;
	
protected:
	KeyGenerator						m_KeyGenerater;


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(ZoneManager , _SERVERBASEGATE_API);
//extern API_NULL ZoneManager& singleton::GetZoneManager();
#define theZoneManager  singleton::GetZoneManager()


#include "ZoneManager.inl"

#endif //__ZONEMANAGER_H__
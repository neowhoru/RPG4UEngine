#ifndef __LOGOUT_USER_FACTORY_H__
#define __LOGOUT_USER_FACTORY_H__

#pragma once

#include <ServerStruct.h>

class LogoutUser;

using namespace util;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGATE_API LogoutUserManager 
{
public:
	LogoutUserManager();
	virtual ~LogoutUserManager();

public:
	virtual BOOL				Init		( DWORD dwUserSIze );
	virtual VOID				Release	();

public:
	virtual LogoutUser*		Alloc		();
	virtual VOID				Free		(LogoutUser* pLogoutUser );

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(LogoutUserManager , _SERVERBASEGATE_API);
//extern API_NULL LogoutUserManager& singleton::GetLogoutUserManager();
#define theLogoutUserManager  singleton::GetLogoutUserManager()


#endif // __LOGOUT_USER_FACTORY_H__
#ifndef MESSAGE_H
#define MESSAGE_H

#pragma  once


#include "GlobalInstanceSingletonDefine.h"
#include <TMemoryPoolFactory.h>

using namespace util;

#define MAX_QUEST_MESSAGE		1000
#define MAX_GENERAL_MESSAGE	1000
#define MAX_TIP_MESSAGE			1000
#define EXTRA_MESSAGE			1000

#define MESSAGE_TOTAL_MAX		(MAX_QUEST_MESSAGE+MAX_GENERAL_MESSAGE+MAX_TIP_MESSAGE+EXTRA_MESSAGE)



enum eMESSAGE_TYPE
{
	MESSAGE_GENERAL = 0,
	MESSAGE_TIP,
	MESSAGE_QUEST,

	MESSAGE_MAX,
};

typedef struct _MESSAGE_INFO
{
	size_t	MessageLen;
	TCHAR *	pszMessage;
	int		iTime;
} MESSAGE_INFO;

typedef hash_map<DWORD, MESSAGE_INFO *>	MESSAGE_INFO_HASH;
typedef MESSAGE_INFO_HASH::iterator			MESSAGE_INFO_ITR;
typedef	pair<DWORD, MESSAGE_INFO *>		MESSAGE_INFO_PAIR;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API Message
{
public:
	Message(void);
	virtual ~Message(void);

public:
	void Init(DWORD dwMaxSize);
	BOOL Load( char * pQuestFileName );
	void Release();

	MESSAGE_INFO * GetMessage( DWORD dwID );

protected:
	void UnLoad();

	BOOL LoadTip(char * pszFileName);
	BOOL LoadGeneral(char * pszFileName);
	BOOL LoadQuest(char * pszFileName);
	BOOL LoadQuestMessage(char * pszFileName );

private:
	DWORD									m_dwMaxSize;
	MESSAGE_INFO_HASH						m_MessageHashmap;
	MESSAGE_INFO_HASH						m_QuestMessageHashmap;
	MESSAGE_INFO_HASH						m_TipMessageHashmap;



	MemoryPoolFactory<MESSAGE_INFO> *m_pMessagePool;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(Message , _RESOURCELIB_API );
//extern _RESOURCELIB_API  Message& singleton::GetMessage();
#define theMessage  singleton::GetMessage()

#endif // MESSAGE_H

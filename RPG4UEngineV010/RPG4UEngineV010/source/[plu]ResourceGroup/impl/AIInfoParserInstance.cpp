/*////////////////////////////////////////////////////////////////////////
文 件 名：AIInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "AIInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(AIInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
AIInfoParserInstance::AIInfoParserInstance()
{
}


//----------------------------------------------------------------------------
AIInfoParserInstance::~AIInfoParserInstance()
{
}




BOOL AIInfoParserInstance::Load( LPCSTR pszFileName, BOOL bReload )
{

	util::TableTextFile sr;

	__VERIFY	( sr.OpenFile( pszFileName, TRUE) 
				, "File Open Error : AIInfoParser.txt");
	if( !bReload ) 
		m_FileName = ( pszFileName);


	sAI_INFO_UNIT*		pUnit;
	sAI_INFO*			pLastInfo	(NULL);
	DWORD					dwUnitCode;
	DWORD					dwAICode;
	WORD					wAIType;
	LPCTSTR				szComment;
	VRSTR					sName;
	/////////////////////////////////////////////
	//	AI编号	注释	状态编号	状态	静止型1	静止型2	静止型3	游荡型1	游荡型2	游荡型3	归家型1	归家型2	归家型3	巡逻型1	巡逻型2	巡逻型3	搜索型1	搜索型2	搜索型3	被动1	被动2	被动3	就近攻击1	就近攻击2	就近攻击3	低血攻击1	低血攻击2	低血攻击3	低级攻击1	低级攻击2	低级攻击3	高魔攻击1	高魔攻击2	高魔攻击3	单人攻击1	单人攻击2	单人攻击3
	for(;sr.GotoNextLine();)
	{
		sr.GetInt();

		sr >> dwUnitCode;
		sr >> dwAICode;

				if(dwAICode != 0)
				{
					sAI_INFO	info={dwAICode,0};
					pLastInfo	= _Add(info);
					//m_mpAIInfos.insert(AIInfoMapPair(dwAICode,info));
					//pLastInfo	= &m_mpAIInfos[dwAICode];
				}

				if(pLastInfo == NULL)
				{
					LOGINFO("invalid aicode %d\n", dwAICode);
					continue;
				}

		sr >> szComment;
				//////////////////////////////////////
		sr >> wAIType;
				if(wAIType >= MAX_AI_TYPE)
				{
					LOGINFO("invalid aitype %d,%d\n", dwAICode,wAIType);
					continue;
				}
		sr >> sName;

			//////////////////////////////////////
			pUnit	= &pLastInfo->m_Units[wAIType];
			pUnit->m_Code		= dwUnitCode;
			pUnit->m_AIType	= wAIType;
			pUnit->m_Name		= sName;

			_Add(pUnit);

		sr >> pUnit->m_Rate;
			sr(MAX_AI_STATE);
		sr >> pUnit->m_Idles	;
		sr >> pUnit->m_Wanders;
		sr >> pUnit->m_Backs	;
		sr >> pUnit->m_Patrols;
		sr >> pUnit->m_Searchs;

		sr >> pUnit->m_Passives	;
		sr >> pUnit->m_Nearests	;
		sr >> pUnit->m_LowHPs	;
		sr >> pUnit->m_LowLVs	;
		sr >> pUnit->m_HighMPs	;
		sr >> pUnit->m_Singles	;
	}

	sr.CloseFile();


	return TRUE;

}


/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "RegenParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"
#include "worldconst.h"
#include <MonsterGroupParser.h>

using namespace std;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(RegenParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
RegenParserInstance::RegenParserInstance()
{
}


//----------------------------------------------------------------------------
RegenParserInstance::~RegenParserInstance()
{
}



BOOL RegenParserInstance::Load( LPCSTR szFileName, BOOL bReload )
{
	if( !bReload )
		m_FileName = szFileName;

	//ASSERT( m_pRegenInfos != NULL );
	//ASSERT( m_pRandRegenInfos != NULL );



	util::TableTextFile sr;

	if ( !sr.OpenFile( m_FileName ) )
	{
		ASSERT( !"File Open Error : RegenParser.txt" );
		return (FALSE);
	}

	BOOL				bAdd ;
	DWORD				dwID;
	sREGEN_INFO *	pInfo;
	//int				i;

	while( sr.GotoNextLine())
	{
		bAdd = FALSE;
		//////////////////////////////
		sr.GetInt();

		sr	>> dwID;

		pInfo = GetRegenInfo( dwID );
		if( !pInfo ) 
		{
			pInfo = AllocUnit();//new sREGEN_INFO;
			bAdd = TRUE;
		}
		else
		{
			ASSERT(bReload && "非法重读..." );
		}
		pInfo->dwID	= dwID;

		sr	>>  pInfo->szDesc;
		sr	>>  pInfo->dwMapCode	;
		sr	>>  pInfo->dwFieldIndex;
		sr	>>  pInfo->nLandX;
		sr	>>  pInfo->nLandY;

		sr >> pInfo->tagLocation;
		sr >> pInfo->vLocationPos.x;
		sr >> pInfo->vLocationPos.y;
		sr >> pInfo->vLocationPos.z;

		sr	>>  pInfo->wMonsterType;
		sr	>>  pInfo->fRegenRate;
		sr	>>  pInfo->bRandomArea;

				sr(3);	//for( i = 0; i < 3; ++i )		sr	>>  pInfo->dwGroupID[i];
		sr	>> pInfo->dwGroupID ;
				//sr(0);
		
				sr(MAX_REGEN_MONSTER_KIND);
		sr	>> pInfo->dwMonCode
			>> pInfo->dwMaxNum;
				sr(0);

		//for( i = 0; i < MAX_REGEN_MONSTER_KIND; ++i )
		//{
		//	sr	>>  pInfo->dwMonCode[i];
		//	sr	>>  pInfo->dwMaxNum[i];
		//}//for( i = 0; i < MAX_REGEN_MONSTER_KIND; ++i )
		for(INT n=0;n<MAX_REGEN_MONSTER_KIND; n++)
		{
			if(pInfo->dwMonCode[n] == 0)
				continue;
			for(INT m=n+1;m<MAX_REGEN_MONSTER_KIND; m++)
			{
				if(pInfo->dwMonCode[m] == 0)
					continue;
				if(pInfo->dwMonCode[m] != pInfo->dwMonCode[n])
					continue;
				pInfo->dwMaxNum[n]	+= pInfo->dwMaxNum[m];
				pInfo->dwMaxNum[m]	= 0;
				pInfo->dwMonCode[m]	= 0;
			}
		}

		if( bAdd )
		{
			if( pInfo->bRandomArea )
				_AddRandom( pInfo );
			else
				_Add( pInfo );
		}//if( bAdd )
	}//while( sr.GotoNextLine())

	sr.CloseFile();

	return TRUE;

}


class RegenParserInstanceGetNearRegenPosOpr
{
public:
	vector<sREGEN_INFO*>		m_arRegens;
	DWORD							m_dwMonsterCode;
	MAPCODE						m_landCode;
	FIELDID						m_fieldCode;
	RegenParserInstanceGetNearRegenPosOpr(DWORD	dwMonsterCode,MAPCODE landCode,FIELDID fieldCode)
		:m_dwMonsterCode(dwMonsterCode),m_landCode(landCode),m_fieldCode(fieldCode){
	}
	void operator()(sREGEN_INFO *pRegenInfo)
	{
		if(pRegenInfo->dwMapCode != m_landCode)
			return;
		if(	m_fieldCode != INVALID_FIELDID 
			&& m_fieldCode != (FIELD_ID)pRegenInfo->dwFieldIndex)
			return;

		//////////////////////////////////////////
		for(INT n=0; n<MAX_REGEN_MONSTER_KIND; n++)
		{
			if(	pRegenInfo->dwMonCode[n] == m_dwMonsterCode
				&& pRegenInfo->dwMaxNum[n] > 0)
				goto laOK;
		}

		//////////////////////////////////////////
		for(INT n=0; n<MAX_GROUP; n++)
		{
			sMONSTERGROUP_INFO*	pGroup;
			pGroup = theMonsterGroupParser.GetGroupInfo(pRegenInfo->dwGroupID[n]);
			if(!pGroup)
				continue;
			if(pGroup->dwLeaderCode == m_dwMonsterCode)
				goto laOK;

			//////////////////////////////////////////
			for(INT m=0; m<MAX_FOLLOWER_NUM; m++)
			{
				sFOLLOW_INFO& follow = pGroup->arFollowInfos[m];
				if(	follow.dwMonCode	 == m_dwMonsterCode
					&& follow.wNum > 0)
					goto laOK;
			}
		}

		////////////////////////////
		return;

		////////////////////////////
laOK:
		m_arRegens.push_back(pRegenInfo);
	}
};

BOOL RegenParserInstance::GetNearRegenPos	(DWORD	dwMonsterCode,MAPCODE landCode,FIELDID fieldCode,Vector3D& vResult)
{
	__CHECK(landCode != INVALID_MAPCODE);

	RegenParserInstanceGetNearRegenPosOpr opr(dwMonsterCode, landCode, fieldCode);
	ForEach(opr);

	__CHECK(opr.m_arRegens.size() > 0);

	////////////////////////////////////////////
	float				fDist;
	float				fDistMin	= 0;
	sREGEN_INFO*	pResult	= opr.m_arRegens[0];
	Vector3D			vPos;


	vPos.x	= opr.m_arRegens[0]->vLocationPos.x * TILE_3DSIZE;
	vPos.y	= opr.m_arRegens[0]->vLocationPos.y * TILE_3DSIZE;
	vPos.z	= opr.m_arRegens[0]->vLocationPos.z;

	fDistMin	= (vResult - vPos).Length2();

	for(UINT n=1; n<opr.m_arRegens.size();n++)
	{
		vPos.x	= opr.m_arRegens[n]->vLocationPos.x * TILE_3DSIZE;
		vPos.y	= opr.m_arRegens[n]->vLocationPos.y * TILE_3DSIZE;
		vPos.z	= opr.m_arRegens[n]->vLocationPos.z;

		fDist = (vResult - vPos).Length2();
		if(fDist < fDistMin)
		{
			fDistMin = fDist;
			pResult	= opr.m_arRegens[n];
		}
	}

	vResult.x	= pResult->vLocationPos.x * TILE_3DSIZE;
	vResult.y	= pResult->vLocationPos.y * TILE_3DSIZE;
	vResult.z	= pResult->vLocationPos.z;

	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：SeriesInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SeriesInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SeriesInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
SeriesInfoParserInstance::SeriesInfoParserInstance()
{
}


//----------------------------------------------------------------------------
SeriesInfoParserInstance::~SeriesInfoParserInstance()
{
}


BOOL SeriesInfoParserInstance::Load	(LPCSTR pszRelationInfoFileName
                              ,LPCSTR pszAffectInfoFileName
										,BOOL		bReload)
{

	LoadRelationInfo	(pszRelationInfoFileName , bReload );
	LoadAffectInfo		(pszAffectInfoFileName , bReload );

	return TRUE;
}


BOOL SeriesInfoParserInstance::LoadRelationInfo(LPCSTR pszFileName,BOOL   bReload)
{

	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszFileName, TRUE) );

	if( !bReload ) 
		m_sSeriesInfoFileName = _VI(pszFileName);

	__ZERO(m_arRelations);


	/////////////////////////////////////
	BYTE		nCodeIndex;
	BYTE		nRelation;
	VRSTR		sName;
	BYTE		nDest;
	CODETYPE	vitemCode;
	for(;sr.GotoNextLine();)
	{
				sr.GetInt();
		sr >> nCodeIndex;
		sr >> sName;
		sr >> vitemCode;
				sr.GetString();	///Comment
		sr >> nRelation;
		sr >> nDest;

		if(nCodeIndex < 0 || nCodeIndex>= ATTACKKIND_MAX)
		{
			assert(!"Series Info not invalid\n");
			LOGINFO("Series Info %d not invalid \n", nCodeIndex);
			continue;
		}
		if(nRelation < 0 || nRelation>= SERIES_RELATION_MAX)
		{
			assert(!"Series relation Info not invalid\n");
			LOGINFO("Series relation Info %d not invalid \n", nCodeIndex);
			continue;
		}

		if(nDest < 0 || nDest >= ATTACKKIND_MAX)
		{
			assert(!"Series Info not invalid\n");
			LOGINFO("Series Info %d not invalid \n", nCodeIndex);
			continue;
		}

		//if(m_arRelations[nCodeIndex].m_Series != 0 && !bReload)
		//{
		//	assert(!"Series Info existed\n");
		//	LOGINFO("Series Info %d  existed\n", nCodeIndex);
		//	continue;
		//}

		m_arRelations[nCodeIndex].m_Series	= nCodeIndex;
		if(sName  != _VNULL())
		{
			m_arRelations[nCodeIndex].m_Name			= sName;
			m_arRelations[nCodeIndex].m_VItemCode	= vitemCode;
		}
		m_arRelations[nCodeIndex].Relations[nRelation].m_Relation	= nRelation;
		m_arRelations[nCodeIndex].Relations[nRelation].m_SeriesDest	= nDest;

		switch(nRelation)
		{
		case SERIES_RELATION_OPPRESS:
		{
			m_arRelations[nDest].Relations[SERIES_RELATION_OPPRESSED].m_Relation		= SERIES_RELATION_OPPRESSED;
			m_arRelations[nDest].Relations[SERIES_RELATION_OPPRESSED].m_SeriesDest	= nCodeIndex;
		}break;
		case SERIES_RELATION_REGEN:
		{
			m_arRelations[nDest].Relations[SERIES_RELATION_REGENED].m_Relation	= SERIES_RELATION_REGENED;
			m_arRelations[nDest].Relations[SERIES_RELATION_REGENED].m_SeriesDest	= nCodeIndex;
		}break;
		}
	}


	/////////////////////////////////////
	for(BYTE n=0; n<ATTACKKIND_MAX; n++)
	{
		sSERIES_RELATION& kind = m_arRelations[n];
		for(BYTE m=0; m<SERIES_RELATION_MAX; m++)
		{
			if(	kind.Relations[m].m_Relation   == 0
				&&	kind.Relations[m].m_SeriesDest == 0)
			{
				kind.Relations[m].m_Relation		= m;
				kind.Relations[m].m_SeriesDest	= n;
			}
		}
	}

	return TRUE;
}


BOOL SeriesInfoParserInstance::LoadAffectInfo(LPCSTR pszFileName,BOOL   bReload)
{

	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszFileName, TRUE) );

	if( !bReload ) 
		m_sAffectInfoFileName = _VI(pszFileName);

	__ZERO(m_arAffects);

	BYTE nCodeIndex;
	for(;sr.GotoNextLine();)
	{
				sr.GetInt();
		sr >> nCodeIndex;

		if(nCodeIndex < 0 || nCodeIndex>= SERIES_RELATION_MAX)
		{
			assert(!"Series Affect Info not invalid\n");
			LOGINFO("Series Affect Info %d not invalid \n", nCodeIndex);
			continue;
		}

		if(m_arAffects[nCodeIndex].m_Relation != 0 && !bReload)
		{
			assert(!"Series Affect Info existed\n");
			LOGINFO("Series Affect Info %d  existed\n", nCodeIndex);
			continue;
		}

		m_arAffects[nCodeIndex].m_Relation = nCodeIndex;

		sr >> m_arAffects[nCodeIndex].m_Name;
				sr.GetString();		/// comment
		sr >> m_arAffects[nCodeIndex].m_AffectItemMake;

		sr >> m_arAffects[nCodeIndex].SELF.m_AttackMinRate;
		sr >> m_arAffects[nCodeIndex].SELF.m_AttackMaxRate;
		sr >> m_arAffects[nCodeIndex].DEST.m_AttackMinRate;
		sr >> m_arAffects[nCodeIndex].DEST.m_AttackMaxRate;

		sr >> m_arAffects[nCodeIndex].SELF.m_DefenceRate;	
		sr >> m_arAffects[nCodeIndex].DEST.m_DefenceRate;	

		sr >> m_arAffects[nCodeIndex].SELF.m_StateRate;	
		sr >> m_arAffects[nCodeIndex].DEST.m_StateRate;	

		sr >> m_arAffects[nCodeIndex].SELF.m_AttrRate;		
		sr >> m_arAffects[nCodeIndex].DEST.m_AttrRate;		

		sr >> m_arAffects[nCodeIndex].SELF.m_SkillRate;	
		sr >> m_arAffects[nCodeIndex].DEST.m_SkillRate;	

		sr >> m_arAffects[nCodeIndex].SELF.m_DropRate;		
		sr >> m_arAffects[nCodeIndex].DEST.m_DropRate;		
	}

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：StateInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "StateInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(StateInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
StateInfoParserInstance::StateInfoParserInstance()
{
}


//----------------------------------------------------------------------------
StateInfoParserInstance::~StateInfoParserInstance()
{
}





BOOL StateInfoParserInstance::Load( LPCSTR pszFileName, BOOL bReload )
{
	//ASSERT( m_pStateInfoHashTable != NULL );


	util::TableTextFile sr;
	__VERIFY( sr.OpenFile( pszFileName, TRUE), "File Open Error : StateInfo.info" );

	if( !bReload ) 
		 m_FileName = (pszFileName);

	for(;sr.GotoNextLine();)
	{
		WORD dwID;

		sr.GetNextNumber();	//跳过第1列...

		sr >> dwID;

		sSTATEINFO_BASE * pInfo = GetStateInfo( dwID );
		if( !pInfo ) 
		{
			pInfo = AllocUnit();//new sSTATEINFO_BASE;
			_Add( pInfo, dwID );
		}
		else
		{
			ASSERT(bReload  && "Reload Failed..." );
		}

		pInfo->m_wStateID = dwID;
		

		sr >> pInfo->m_sStateName;
		sr >> pInfo->m_VStateCode;
		sr >> pInfo->m_VItemCode;
				sr.GetNextString();	///跳过注释列

		sr >> pInfo->m_byType;

		sr >> pInfo->m_EffectTag.m_Text;
				if (pInfo->m_EffectTag.m_Text[0] == 0)
					pInfo->m_EffectTag.m_ID = IDTYPE_NULL;

		sr >> pInfo->m_byEffectPos;
		sr >> pInfo->m_sTipFormat;
	}

	sr.CloseFile();


	return TRUE;
}

BOOL StateInfoParserInstance::LoadVState( LPCSTR pszFileName, BOOL bReload )
{
	//ASSERT( m_pVStateInfoHashTable != NULL );


	util::TableTextFile sr;
	__VERIFY( sr.OpenFile(pszFileName , TRUE), "File Open Error : VStateInfo.info" );

	if( !bReload ) 
		 m_sVFileName = _VI( pszFileName);

	for(;sr.GotoNextLine();)
	{
		WORD dwID;

		sr.GetNextNumber();	//跳过第1列...

		sr >> dwID;

		sVSTATEINFO_BASE * pInfo = GetVStateInfo( dwID );
		if( !pInfo ) 
		{
			pInfo = AllocVState(TRUE);//new sVSTATEINFO_BASE;
			_Add( pInfo, dwID );
		}
		else
		{
			ASSERT(bReload  && "Reload Failed..." );
		}

		pInfo->m_VStateCode = dwID;
		

		sr >> pInfo->m_sStateName;
		sr >> pInfo->m_StateLV;		
		sr >> pInfo->m_EffectCode;		
		sr >> pInfo->m_dwDuration;	
		sr >> pInfo->m_dwFuncPeriod;
		sr >> pInfo->m_byUseColor;	
		sr >> pInfo->m_dwColor;		
		sr >> pInfo->m_fGrowSize;

		//sEFFECT_INFO* pEffect = theEffectResourceList.GetEffectInfo((LPCSTR)pInfo->m_sEffect);
		//if(pEffect)
		//	LOGINFO("effect%s:	%d\n",(LPCSTR)pInfo->m_sStateName,pEffect->dwCode);
		//else
		//	LOGINFO("effect%s:	\n",(LPCSTR)pInfo->m_sStateName);
	}

	sr.CloseFile();


	return TRUE;
}



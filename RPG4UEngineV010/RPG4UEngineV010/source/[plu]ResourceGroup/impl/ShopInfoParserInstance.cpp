/*////////////////////////////////////////////////////////////////////////
文 件 名：ShopInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ShopInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"
#include <ItemInfoParser.H>



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ShopInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
ShopInfoParserInstance::ShopInfoParserInstance()
{
}


//----------------------------------------------------------------------------
ShopInfoParserInstance::~ShopInfoParserInstance()
{
}



BOOL ShopInfoParserInstance::Load( LPCSTR szFileName, BOOL bReload )
{

	//ASSERT( m_pShopInfoHashTable != NULL );

	util::TableTextFile sr;

	__VERIFY	( sr.OpenFile( szFileName, TRUE) 
				, "File Open Error : ShopInfoList.txt");
	if( !bReload ) 
		m_FileName = ( szFileName);

	DWORD		dwID;
	DWORD		dwShopID;
	TCHAR		pszName[MAX_SHOPNAME_LENGTH];
	BYTE		tab_index;

	for(;sr.GotoNextLine();)
	{
		sr.GetInt();

		sr >> dwID;
		sr >> dwShopID;
		sr >> pszName;
		sr >> tab_index;

		ShopDetailInfo * pInfo = GetShopList( dwShopID );
		if( !pInfo ) 
		{
			pInfo = AllocUnit(FALSE);//new ShopDetailInfo;
			pInfo->Init( dwShopID, pszName );
			_Add(pInfo);
			//m_pShopInfoHashTable->Add( pInfo, pInfo->GetShopListID() );
		}
		else if( bReload )
		{
			pInfo->DestroyItem( tab_index );
		}

		SLOTCODE			ItemCode;
		LPCSTR			szText;
		VRSTR				itemName;
		DURATYPE			ItemNum;
		sITEMINFO_BASE*	pItemInfo;

		for(BYTE i = 0 ; i < ShopDetailInfo::MAX_SELL_ITEM ; ++i )
		{
			itemName = NULL;
			ItemCode	= INVALID_CODETYPE;

			sr >> szText;
					if(szText[0] == 0)		continue;
					if( szText[0] >= '0' && szText[0] <= '9' )
					{
						ItemCode = (SLOTCODE)atoi(szText);
						//if(ItemCode	== INVALID_CODETYPE)	continue;
					}
					else
					{
						itemName	= _V(szText);
						//if(itemName == NULL)		continue;
					}

			sr >> ItemNum;
					if(ItemNum == 0 )		continue;


			if(itemName)
				pItemInfo	= theItemInfoParser.GetItemInfo((LPCSTR)itemName);
			else if(ItemCode	!= INVALID_CODETYPE)
				pItemInfo	= theItemInfoParser.GetItemInfo(ItemCode);
			else
				continue;

			ASSERT( NULL != pItemInfo );
			if(pItemInfo == NULL)
			{
				LOGINFO("Shop Item %s not existed\n", itemName);
				continue;
			}
			ASSERT(	ItemNum != 0 
					&& ItemNum <= pItemInfo->m_byDupNum );

			pInfo->AddItem( tab_index, i, pItemInfo->m_Code, ItemNum );
		}
	}

	sr.CloseFile();


	return TRUE;

}




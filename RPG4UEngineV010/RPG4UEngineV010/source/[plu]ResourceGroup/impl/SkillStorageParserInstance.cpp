/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillStorageParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillStorageParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SkillStorageParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
SkillStorageParserInstance::SkillStorageParserInstance()
{
}


//----------------------------------------------------------------------------
SkillStorageParserInstance::~SkillStorageParserInstance()
{
}



BOOL SkillStorageParserInstance::Load( LPCSTR szFileName, BOOL bReload )
{

    //ASSERT( m_pSkillStorages != NULL );


    util::TableTextFile sr;
    if ( !sr.OpenFile( szFileName, TRUE) )
    {
        ASSERT( !"File Open Error : SkillInfo.txt" );
        return (FALSE);
    }

    if( !bReload )
         m_FileName = (szFileName);

    DWORD	dwID			= 0;
	 BYTE		nClass		= 0;
	 BYTE		nSKillType	(0);

//		职业	技能类型	技能数	技能1	技能2	技能3	技能4	技能5	技能6	技能7	技能8	技能9	技能10	技能11	技能12	技能13	技能14	技能15	技能16	技能17	技能18	技能19	技能20	技能21	技能22	技能23	技能24	技能25	技能26	技能27	技能28	技能29	技能30	技能31	技能32	技能33	技能34	技能35	技能36	技能37	技能38	技能39	技能40	技能41	技能42	技能43	技能44	技能45	技能46	技能47	技能48	技能49	技能50
    for(;sr.GotoNextLine();)
    {
		 sr.GetNextNumber();	///跳过第1列空格
		 sr >> dwID;
		 sr >> nClass;
		 sr >> nSKillType;
			
		  if(nClass < PLAYERTYPE_BASE || nClass >= PLAYERTYPE_MAX)
		  {
			  LOGINFO("SkillInventoryStore %d char type not invalid \n",dwID);
			  continue;
		  }

		  if(nSKillType >= SKILL_KIND_MAX)
		  {
			  LOGINFO("SkillInventoryStore %d skill type not invalid \n",dwID);
			  continue;
		  }
		  
        sSKILL_STORAGEINFO * pInfo = GetSkillStorage( dwID );
        if( !pInfo ) 
        {
            pInfo = AllocUnit();//sSKILL_STORAGEINFO;
            _Add( pInfo, dwID );
        }
        else
        {
            ASSERT(bReload && "Reload failed." );
        }

		  
		  __ZERO(pInfo->m_SkillIDs);

        pInfo->m_InfoID					= dwID;
		  pInfo->m_RequirePlayerType	= nClass;
		  pInfo->m_SkillType				= nSKillType;

        //sr >> pInfo->m_SkillTab2;
        sr >> pInfo->m_SkillCount;

        if (pInfo->m_SkillCount > MAX_SKILL_STORAGE_NUM)
            pInfo->m_SkillCount = MAX_SKILL_STORAGE_NUM;

				sr(pInfo->m_SkillCount);
		sr >> pInfo->m_SkillIDs;
				sr(0);


        //dwID++;
    }


    sr.CloseFile();

    return TRUE;
}




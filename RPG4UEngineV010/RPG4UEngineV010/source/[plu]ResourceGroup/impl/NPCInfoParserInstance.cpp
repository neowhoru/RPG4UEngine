/*////////////////////////////////////////////////////////////////////////
文 件 名：NPCInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NPCInfoParserInstance.h"
#include "StructBase.h"
#include "StructBase.h"
#include "MapInfoParser.h"
#include <DataTypeDefine.h>
#include "TableTextFile.h"
#include "StringManager.h"

using namespace quest;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(NPCInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
NPCInfoParserInstance::NPCInfoParserInstance()
{
}


//----------------------------------------------------------------------------
NPCInfoParserInstance::~NPCInfoParserInstance()
{
}


BOOL NPCInfoParserInstance::LoadExtra( LPCSTR pszFileName, BOOL bReload )
{

	//ASSERT( m_pNPCInfoHashTable != NULL );
	
	util::TableTextFile sr;

	__CHECK ( sr.OpenFile(pszFileName , TRUE) );

	if( !bReload )
		 m_pszExtraFileName = _VI( pszFileName);

	CODETYPE				npcCode;
	CODETYPE				mapID;
	FIELDID				fieldID;
	BYTE					byNPCTYPE;
	CODETYPE				monsterCode;
	sNPCINFO_FUNC *	pExtraInfo;
	VRSTR					sName;

	for(;sr.GotoNextLine();)
	{
		sr.GetInt();	//空列

		sr >> npcCode;
		sr >> sName;

		sr >> mapID;
		sr >> fieldID;
		
		sr >> byNPCTYPE;
		sr >> monsterCode;

		pExtraInfo = GetExtraInfo( npcCode );
		if( !pExtraInfo  ) 
		{
			pExtraInfo = AllocExtra(TRUE);//new sNPCINFO_FUNC;
			pExtraInfo->m_ExtraCode    = npcCode;
			_Add(pExtraInfo);
			//m_pExtraNPCInfoHashTable->Add( pExtraInfo, npcCode );
		}
		else
		{
			assert(bReload && "Invalid reload");
		}

		pExtraInfo->m_ExtraCode    = npcCode;
		pExtraInfo->m_Name			= sName;
		pExtraInfo->m_MapCode      = mapID;
		pExtraInfo->m_FieldID		= fieldID;
		pExtraInfo->m_eNPCTYPE		= (eNPC_FUNC_TYPE)byNPCTYPE;
		pExtraInfo->m_NPCCODE		= monsterCode;

		//sr >>  pExtraInfo->m_fNPCPos[0];
		//sr >>  pExtraInfo->m_fNPCPos[1];
		//sr >>  pExtraInfo->m_fNPCPos[2];

		sr >>  pExtraInfo->m_LocalNPC;
		sr >>  pExtraInfo->m_ClientScriptCode;
		sr >>  pExtraInfo->m_ServerScriptCode;

		sr >>  pExtraInfo->m_vPos.x;
		sr >>  pExtraInfo->m_vPos.y;
		sr >>  pExtraInfo->m_vPos.z;

		sr >>  pExtraInfo->m_nAngle;
		sr >>  pExtraInfo->m_dwMoveDelayMin;
		sr >>  pExtraInfo->m_dwMoveDelayMax;

		sr >> pExtraInfo->m_iMoveType;
		if(pExtraInfo->m_iMoveType == eNPCMOVETYPE_PATROL)
		{
			sr >> pExtraInfo->m_nStepNum;
					pExtraInfo->m_nStepNum = min(pExtraInfo->m_nStepNum,MAX_ROUTE_SIZE-1);
			for(INT n=0; n<pExtraInfo->m_nStepNum; n++)
			{
				sr >> pExtraInfo->m_arMoveNodes[n].x;
				sr >> pExtraInfo->m_arMoveNodes[n].y;
				sr >> pExtraInfo->m_arMoveNodes[n].z;
			}
		}
		else if(pExtraInfo->m_iMoveType == eNPCMOVETYPE_RANDOMMOVE)
		{
			sr >> pExtraInfo->m_fRange;
			if(pExtraInfo->m_fRange <= 0)
			{
				LOGINFO("NpcFuncInfo %d range <=0 \n",npcCode);
				pExtraInfo->m_fRange = 2.0f;
				
			}
		}

		//sr >> pExtraInfo->m_szWalkPath;
	}

	sr.CloseFile();


	return TRUE;

}

BOOL NPCInfoParserInstance::LoadQuestInfo( LPCSTR pszFileName, BOOL bReload )
{

	//ASSERT( m_pNPCInfoHashTable != NULL );
	
	util::TableTextFile sr;

	__CHECK ( sr.OpenFile(pszFileName , TRUE) );

	if( !bReload )
		m_pszQuestFileName  =_VI( pszFileName);

	CODETYPE				npcCode;
	NPC_QUESTINFO*		pQuestInfo;
	sQUEST_RELATEINFO	questInfo;
	map<CODETYPE,INT>	questMap;

	for(;sr.GotoNextLine();)
	{
		sr.GetInt();	//空列

		sr >> npcCode;
		sr >> questInfo.m_QuestID;
		sr >> questInfo.m_State;
		sr >> questInfo.m_Entrance;
		sr >> questInfo.m_Desc;


		pQuestInfo = GetQuestInfo( npcCode );
		if( !pQuestInfo  ) 
		{
			pQuestInfo = AllocNpcQuest(FALSE);//new NPC_QUESTINFO;
			//m_pQuestInfoHashTable->Add( pQuestInfo, npcCode );
			pQuestInfo->m_Code		= npcCode;
			//pQuestInfo->m_Desc		= _VI(szDesc);
			_Add(pQuestInfo);
		}
		_Push(pQuestInfo,questInfo);
		//pQuestInfo->m_QuestInfos.push_back(questInfo);

		if(questMap.find(questInfo.m_QuestID) == questMap.end())
		{
			questMap[questInfo.m_QuestID]	= 1;
			_Push(pQuestInfo,questInfo.m_QuestID);
			//pQuestInfo->m_QuestIDs.push_back(questInfo.m_QuestID);
		}

	}

	sr.CloseFile();

	return TRUE;

}


BOOL NPCInfoParserInstance::LoadVNpc( LPCSTR pszFileName, BOOL bReload/* = FALSE*/ )
{

	//ASSERT( m_pVNPCInfoHashTable != NULL );

	//
	TableTextFile sr;
	__VERIFY	(sr.OpenFile( pszFileName , TRUE)
				,"File Open Error : SCMonsterInfoList.txt");

	if( !bReload )
		 m_pszVNpcFileName = _VI( pszFileName);

	MONSTERCODE			vMonsterCode;
	sVNPCINFO_BASE *	pInfo;

	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		sr >> vMonsterCode;
				pInfo = GetVMonsterInfo( vMonsterCode );
				if( !pInfo ) 
				{
					pInfo = AllocVNpc(TRUE);//new sVNPCINFO_BASE;
					//m_pVNPCInfoHashTable->Add( pInfo, vMonsterCode );
					pInfo->m_vMonsterCode = vMonsterCode;
					_Add(pInfo);
				}
				else
				{
					if( FALSE == bReload )
					{
						ASSERT( !"非法Reload." );
					}
				}

				pInfo->m_vMonsterCode = vMonsterCode;


		sr >> pInfo->m_sMonsterName;
		sr >> pInfo->m_resModelID;
		//sr >> pInfo->m_MonsterGrade;	//eNPC_GRADE
		sr >> pInfo->m_wSoundTempl;
		sr >> pInfo->m_MonsterType;	//见 eNPC_GRADE
		//sr >> pInfo->m_fMoveSpeed;	

				sr(ATTACK_SEQUENCE_MAX);
		sr >> pInfo->m_SkillCode;
		//sr >> pInfo->m_SkillLevel;
				sr(0);

		sr >> pInfo->m_fBodySize;	
		sr >> pInfo->m_sSkin;
		sr >> pInfo->m_fAlpha;		
				sr(ELEMENTKIND_MAX);
		sr >> pInfo->m_ElementSizes	;
		sr >> pInfo->m_ElementSkins	;
		sr >> pInfo->m_ElementAlphas	;
				sr(0);
		//sr >> pInfo->m_sDeathScript;
		//sr >> pInfo->m_sAIScript;	
		sr >> pInfo->m_byWeaponSound;
		sr >> pInfo->m_byArmourTexture;
		//sr >> pInfo->m_fRun;			
		//sr >> pInfo->m_fWalk;			
		sr >> pInfo->m_nFootNum;		
		sr >> pInfo->m_dwIdleSoundPeriod;		
		sr >> pInfo->m_byPlayBattleVoiceRate;		
		sr >> pInfo->m_byPlayIdleVoiceRate;	
		sr >> pInfo->m_byPlayHurtVoiceRate;	
		sr >> pInfo->m_byPlayDieVoiceRate;	

	}
	sr.CloseFile();

	return TRUE;
}


BOOL NPCInfoParserInstance::Load( LPCSTR pszFileName, BOOL bReload/* = FALSE*/ )
{

	//ASSERT( m_pNPCInfoHashTable != NULL );

	util::TableTextFile sr;

	if ( !sr.OpenFile(pszFileName,TRUE) )
	{
		ASSERT( !"File Open Error : SCMonsterInfoList.txt" );
		return (FALSE);
	}

	if( !bReload ) 
		 m_FileName = (pszFileName);


	MONSTERCODE			monsterCode;
	sNPCINFO_BASE *	pInfo;
	TAG_CODE				tagMoveArea;

	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		sr >> monsterCode;
				pInfo = GetMonsterInfo(monsterCode);//m_pNPCInfoHashTable->GetData( monsterCode );
				if( !pInfo ) 
				{
					pInfo = AllocUnit();//new sNPCINFO_BASE;
					//m_pNPCInfoHashTable->Add( pInfo, monsterCode );
					pInfo->m_MonsterCode = monsterCode;
					_Add(pInfo);
				}
				else
				{
					if( FALSE == bReload )
					{
						ASSERT( !"非法Reload." );
					}
				}

				pInfo->m_MonsterCode = monsterCode;
		
		sr >> pInfo->m_vMonsterCode;

		sr >> pInfo->m_NpcName;

		sr >> pInfo->m_Level;
		sr >> pInfo->m_DisplayLevel;
		sr >> pInfo->m_dwMaxHP;
		sr >> pInfo->m_dwMaxMP;
		sr >> pInfo->m_byGrade;
		sr >> pInfo->m_wSize;

		//sr >> pInfo->m_wAttProperty;
		sr >> pInfo->m_AIInfoCode;
		sr >> pInfo->m_AIInfoRate;
		sr >> pInfo->m_byCriticalRatio;

		sr >> pInfo->m_dwMinAttackPower;
		sr >> pInfo->m_dwMaxAttackPower;
		sr >> pInfo->m_dwPhyDef;
		sr >> pInfo->m_dwMagicDef;
		sr >> pInfo->m_wClass;

		sr >> pInfo->m_bySeries;
				//pInfo->m_wAttProperty = pInfo->m_bySeries;

			   sr(ELEMENTKIND_MAX);
		sr >> pInfo->m_wElementDef;
		sr >> pInfo->m_wElementAttack;
				sr(0);

		sr >> pInfo->m_wPhyAttRate;
		sr >> pInfo->m_wPhyAttAvoid;
		sr >> pInfo->m_wAttType;
		sr >> pInfo->m_wArmorType;
		sr >> pInfo->m_fAttRange;
		sr >> pInfo->m_fViewRange;
		sr >> pInfo->m_fMoveRange;

		sr >> pInfo->m_wStandMin;
		sr >> pInfo->m_wStandMax;
		sr >> pInfo->m_fWalkSpeed;
		sr >> pInfo->m_fRunSpeed;

				sr(ATTACK_SEQUENCE_MAX);
		sr >> pInfo->m_wAttSpeed;
		//sr >> pInfo->m_wProjectileCode;
				sr(0);
		
		sr >> pInfo->m_bJump;

		sr >> pInfo->m_SpawnAniCode.m_Text;
		sr >> pInfo->m_dwSpawnTime;
	
		sr >> pInfo->m_wAttitude;
		sr >> pInfo->m_wMoveAttitude;
		sr >> tagMoveArea;		pInfo->m_dwMoveAreaID = tagMoveArea.m_ID;

		sr >> pInfo->m_wSkillUpdateTime;
		//
		for( int i = 0; i < 2; ++i )
		{
			sr >> pInfo->m_SpecialCondition[i].byCondition;
			sr >> pInfo->m_SpecialCondition[i].wConditionParam;
			sr >> pInfo->m_SpecialCondition[i].wActionType;
			sr >> pInfo->m_SpecialCondition[i].wActionParam;
			sr >> pInfo->m_SpecialCondition[i].byActionRate;
		}

		//
		for( int i = 0; i < 2; ++i )
		{
			sr >> pInfo->m_ResistanceCondition[i].m_wStateCode;
			sr >> pInfo->m_ResistanceCondition[i].m_byRatio;
			sr >> pInfo->m_ResistanceCondition[i].m_EffectTag;
		}

		sr >> pInfo->m_byChangeTargetRatio;


		sr >> pInfo->m_byReviveCondition;
		sr >> pInfo->m_wReviveConditionParam;
		sr >> pInfo->m_byReviveRate;
		sr >> pInfo->m_wReviveCode;
		sr >> pInfo->m_wReviveDelay;

		sr >> pInfo->m_byHealCondition;
		sr >> pInfo->m_wHealConditionParam;
		sr >> pInfo->m_byHealRate;
		sr >> pInfo->m_wHealCode;
		sr >> pInfo->m_wHealDelay;

		sr >> pInfo->m_bySummonCondition;
		sr >> pInfo->m_wSummonConditionParam;
		sr >> pInfo->m_bySummonRate;
		sr >> pInfo->m_wSummonCode;
		sr >> pInfo->m_wSummonDelay;

		sr >> pInfo->m_bySkillUsePossible;
		for( int i = 0; i < 3; ++i )
		{
			sr >> pInfo->m_wSkillCode[i];
			sr >> pInfo->m_bySkillRate[i];
			sr >> pInfo->m_wSkillDelay[i];
		}

		//  m_wSkillUpdateTime
		if(	pInfo->m_wReviveCode 
			|| pInfo->m_wHealCode 
			|| pInfo->m_wSummonCode 
			||	pInfo->m_wSkillCode[0] 
			|| pInfo->m_wSkillCode[1] 
			|| pInfo->m_wSkillCode[2] )
		{
			if( pInfo->m_wSkillUpdateTime == 0 )
			{
				LOGINFO	( "[NPCInfoParser] monsterCode[%d] m_wSkillUpdateTime is 0! \n"
							, pInfo->m_MonsterCode );
			}
		}

		//sr >> pInfo->m_wHateSkill;
		//sr >> pInfo->m_wRevengeSkill;
		//sr >> pInfo->m_byRevengeRate;

		sr >> pInfo->m_byItemDropRate;
		for( int i = 0; i < 3; ++i )
		{
			sr >> pInfo->m_ItemCode[i];
			sr >> pInfo->m_wDropItemRate[i];
		}




		/////////////////////////////////////////////////
		//注册名字映射
		if(GetMonsterInfo((LPCTSTR)pInfo->m_NpcName))
		{
			LOGINFO("NpcInfo  [%s %d] Name repeat\n", (LPCSTR)pInfo->m_NpcName, pInfo->m_MonsterCode);
		}
		else
		{
			_AddNameNpc(pInfo);
		}

	}//for(;sr.GotoNextLine();)


	sr.CloseFile();

	return TRUE;
}



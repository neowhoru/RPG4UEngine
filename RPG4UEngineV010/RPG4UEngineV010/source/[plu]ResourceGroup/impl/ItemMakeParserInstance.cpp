/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemMakeParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemMakeParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"
#include "ItemInfoParser.h"
#include "FormatString.h"
#include "RateInfoParser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define __CHECKINFO(f,min,max)\
		assert((f) >=(min) && (f) < (max));\
		if((f) < (min) || (f) >= (max))\
		{\
			LOGINFO("%d Value %d exceed [" #min ",%d]\n",(f),(max));\
			continue;\
		}

////////////////////////////////////////////////
#define CHECK_RATE(rate)\
	if(theRateInfoParser.GetRateInfo(pInfoLV->m_##rate) == NULL)\
		{\
			assert(0);\
			LOGINFO("ItemMakeInfo Invalid Rate [" #rate "] at %d\n",Code);\
			continue;\
		}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemMakeParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
ItemMakeParserInstance::ItemMakeParserInstance()
{
}


//----------------------------------------------------------------------------
ItemMakeParserInstance::~ItemMakeParserInstance()
{
}


BOOL ItemMakeParserInstance::Load( LPCSTR pszFileName, BOOL bReload )
{
	//ASSERT( m_pMakeInfoHashTable != NULL );


	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszFileName, TRUE) );

	if( !bReload ) 
		 m_FileName = (pszFileName);

	BYTE						byLevel;
	DWORD						dwItemType;
	SLOTCODE					Code;
	sITEMMAKE *				pInfo;
	sITEMMAKELEVEL*		pInfoLV;
	sITEMMAKE_GROUP*		pGroup;
	BOOL						bRegGroup;
	sITEMINFO_BASE*		pItemInfo;
	char						arSequences[SERIES_RELATION_MAX];
	VRSTR						sName;
	VRSTR						sGroup;

	//编号	Item类型	类型名称	打造级别	群组	打造目标	金钱耗量	主材	矿石	木材	皮料	骨材	筋材	玉石	素类	肉类	液料	辅助物
	for(;sr.GotoNextLine();)
	{
		bRegGroup = FALSE;
		__ZERO_ARRAY(arSequences);

		sr.GetInt();
		sr >> Code;
		sr >> dwItemType;
		if(dwItemType == 0)
			continue;

		sr >> sName ;
		sr >> sGroup;

		pInfo = GetMakeInfo((eITEM_TYPE) dwItemType );
		if( !pInfo ) 
		{
			pInfo = AllocMake(TRUE);//new sITEMMAKE;
			pInfo->m_ItemType = dwItemType;
			_Add(pInfo);
			//m_pMakeInfoHashTable->Add( pInfo, dwItemType );
			//__ZERO_PTR(pInfo);

			bRegGroup = TRUE;

			pInfo->m_ItemType = dwItemType;

			pInfo->m_sName	= sName ;
			pInfo->m_sGroup= sGroup;
		}
		else
		{
		}

		sr >> byLevel;

		__CHECKINFO(byLevel,1 , MAX_ITEM_LEVEL+1);

		pInfoLV = &pInfo->m_arMakeLVs[byLevel-1];

		pInfoLV->m_Code		= Code;
		pInfoLV->m_sName		= sName ;
		pInfoLV->m_sGroup		= sGroup;
		pInfoLV->m_byItemLV	= byLevel;

		_Add(pInfoLV);

		sr >> pInfoLV->m_ItemCode;			//目标物品
				sr.GetString();				//注释
		sr >> pInfoLV->m_Money;				//金钱耗量
		sr >> pInfoLV->m_byMainIndex;		//主材

				sr(MATERIALTYPE_MAX);
		sr >> pInfoLV->m_arMaterialNum;	//原料
				sr(0);


		sr >> arSequences;//					///< 追回顺序
		sr >> pInfoLV->m_byRank;				///< Rank级别
		sr >> pInfoLV->m_RankRateID;			///< Rank值段率
		sr >> pInfoLV->m_RegenRateID;			///< 相生值段率
		sr >> pInfoLV->m_OppressRateID;		///< 相克值段率
		sr >> pInfoLV->m_byLevel;				///< 等级增量
		sr >> pInfoLV->m_LevelRateID;			///< 等级值段率
		sr >> pInfoLV->m_wPhyAttackMin;		///< 物理小攻
		sr >> pInfoLV->m_wPhyAttackMax;		///< 物理大攻
		sr >> pInfoLV->m_PhyAttackRateID;	///< 物理攻值段率
		sr >> pInfoLV->m_wMagicAttackMin;	///< 物理小攻
		sr >> pInfoLV->m_wMagicAttackMax;	///< 物理大攻
		sr >> pInfoLV->m_MagicAttackRateID;	///< 物理攻值段率

		sr >> pInfoLV->m_AttackSpeed;				///< 攻速增量
		sr >> pInfoLV->m_AttackSpeedRateID;		///< 攻速值段率
		//sr >> pInfoLV->m_MoveSpeed;				///< 移速增量
		//sr >> pInfoLV->m_MoveSpeedRateID;		///< 移速值段率
		sr >> pInfoLV->m_AttSuccessRatio;		///< 命中增量
		sr >> pInfoLV->m_AttSuccessRatioRateID;///< 命中值段率
		//sr >> pInfoLV->m_AvoidRatio;				///< 闪避增量
		//sr >> pInfoLV->m_AvoidRatioRateID;		///< 闪避值段率

				sr(sITEMMAKELEVEL::POTENTIAL_NUM);
		sr >> pInfoLV->m_Potential;			///< 潜隐属性 参考随机属性表
		sr >> pInfoLV->m_PotentialRateID;	///< 
				sr(0);

				
		////////////////////////////////////////////
		//处理 追加顺序
		//static char szSeqs[]={'A','B','C','D',0};
		BYTE	arStates[SERIES_RELATION_MAX] ={0};
		for(INT n=0; n<SERIES_RELATION_MAX; n++)
		{
			if(arSequences[n] == 0)
				break;
			UINT uState = arSequences[n] - 'A';
			assert(uState < SERIES_RELATION_MAX);
			if(uState >= SERIES_RELATION_MAX || arStates[uState] != 0)
			{
				LOGINFO("ItemMake type %d Sequene info %d not the character set [ABCD]  or repeat the same one\n",dwItemType,byLevel);
				break;
			}
			arStates[uState] =  1;
		}

		INT nCounter(0);
		memcpy(pInfoLV->m_Sequenes,arSequences,SERIES_RELATION_MAX);
		nCounter = strlen(arSequences);
		for(CHAR n=0; n < SERIES_RELATION_MAX; n++)
		{
			if(arStates[n]==1)
				continue;
			pInfoLV->m_Sequenes[nCounter++] = 'A' + n;
		}
		pInfoLV->m_Sequenes[nCounter] = 0;


		////////////////////////////////////////////
		//检测RateID
		CHECK_RATE(RankRateID);
		CHECK_RATE(RegenRateID);
		CHECK_RATE(OppressRateID);
		CHECK_RATE(LevelRateID);
		CHECK_RATE(PhyAttackRateID);
		CHECK_RATE(MagicAttackRateID);
		CHECK_RATE(PotentialRateID[0]);
		CHECK_RATE(PotentialRateID[1]);


		///////////////////////////////////
		if(	pInfoLV->m_Potential[0] < RANK_9 
			|| pInfoLV->m_Potential[0] >= RANK_MAX
			|| pInfoLV->m_Potential[1] < RANK_9 
			|| pInfoLV->m_Potential[1] >= RANK_MAX)
		{
			pInfoLV->m_Potential[0] = RANK_9;
			pInfoLV->m_Potential[1] = RANK_9;
			assert(!"Make item Potential rank not exceed rank range");
			LOGINFO	("Make item %d Potential rank not exceed rank range[%d,%d)\n"
						,Code 
						,RANK_9
						,RANK_MAX);
			continue;
		}

		///////////////////////////////////
		//检测目标物品
		pItemInfo = theItemInfoParser.GetItemInfo(pInfoLV->m_ItemCode);
		assert(pItemInfo);
		if(pItemInfo == NULL || pItemInfo->m_LV != byLevel)
		{
			assert(!"Make item Level/ItemType not match");
			LOGINFO	("Make item %d not exist or Level/ItemType not match[%d,%d]\n"
						, pInfoLV->m_ItemCode
						,byLevel
						,pItemInfo->m_LV);
			continue;
		}

		///////////////////////////////////
		//检测目标类型
		if(pItemInfo->m_wType != dwItemType)
		{
			assert(!"Make item ItemType not match");
			LOGINFO	("Make item %d not exist or Level/ItemType not match[%d,%d]\n"
						, pInfoLV->m_ItemCode
						,dwItemType
						,pItemInfo->m_wType);
			continue;
		}


		///////////////////////////////////
		//检测主材
		__CHECKINFO(pInfoLV->m_byMainIndex,1 , MATERIALTYPE_MAX);

		if(pInfoLV->m_arMaterialNum[pInfoLV->m_byMainIndex] == 0)
		{
			assert(!"Main material Slot is NULL\n");
			LOGINFO	("Make item %d Main material Slot %d is NULL\n"
						, pInfoLV->m_ItemCode
						,pInfoLV->m_byMainIndex);
			continue;
		}

		///////////////////////////////////
		//检测原料表
		for(INT n=0; n<MATERIALTYPE_MAX; n++)
		{
			if(pInfoLV->m_arMaterialNum[n] > MATERIAL_CONTAINER_ROW)
			{
				assert(!"Main material MaterialNum is too large\n");
				LOGINFO	("Main material %d MaterialNum is too large %d [0,%d]\n"
							,pInfoLV->m_ItemCode
							,pInfoLV->m_arMaterialNum[n]
							,MATERIAL_CONTAINER_ROW);
				pInfoLV->m_arMaterialNum[n] = MATERIAL_CONTAINER_ROW;
			}
		}


		//////////////////////////////////////
		//生成群组管理
		if(bRegGroup)
		{
			//if(!theGlobalString.IsBlank(pInfo->m_sGroup))
			if(!StringManager::IsBlank(pInfo->m_sGroup))
			{
				///加到群组信息中....
				//m_pMakeGroupTable
				pGroup = GetMakeInfoGroup(pInfo->m_sGroup);
				//pGroup = m_pMakeGroupTable->GetData((DWORD)pInfo->m_sGroup);
				if(pGroup == NULL)
				{
					pGroup = AllocGroup(FALSE);//new sITEMMAKE_GROUP;
					pGroup->sName	= pInfo->m_sGroup;
					_Add(pGroup,(DWORD)pInfo->m_sGroup);
					//m_pMakeGroupTable->Add( pGroup, (DWORD)pInfo->m_sGroup );

					//pGroup->itemMakeTable.Initialize(m_dwMakeInfoSize);
				}
				_Push(pGroup,pInfo, dwItemType);
				//pGroup->itemMakeTable.Add(pInfo, dwItemType);
			}
		}

	}
	sr.CloseFile();
	
	return TRUE;
}

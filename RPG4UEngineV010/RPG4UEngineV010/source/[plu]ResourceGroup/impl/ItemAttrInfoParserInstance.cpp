/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemAttrInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemAttrInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"
#include "RateInfoParser.h"
#include "ItemInfoParser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemAttrInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
ItemAttrInfoParserInstance::ItemAttrInfoParserInstance()
{
}


//----------------------------------------------------------------------------
ItemAttrInfoParserInstance::~ItemAttrInfoParserInstance()
{
}



BOOL ItemAttrInfoParserInstance::Load	(LPCSTR	pszRandAttrInfoFileName
													,LPCSTR	pszDoubleAttrInfoFileName
													,LPCSTR	pszRandAttrTableFileName
													,BOOL		bReload)
{

	LoadRandAttrTable	(pszRandAttrTableFileName , bReload );
	LoadDoubleAttrInfo(pszDoubleAttrInfoFileName , bReload );
	

	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszRandAttrInfoFileName, TRUE) );

	if( !bReload ) 
		m_pszRandAttrInfoFileName = _VI(pszRandAttrInfoFileName);

	INT							nCode;
	sITEM_RANDATTR_INFO*		pInfo;

	for(;sr.GotoNextLine();)
	{
		////	Code	注释	Level	浮动范围	type	主属性	主属性值	浮动范围	
		//		随机1机率	随机2机率	随机属性类1	随机属性类2	
		//		[0]	(0,20)	[20,40)	[40,60)	[60,80)	[80,100)

		sr.GetInt();
		sr >> nCode;

		pInfo = (sITEM_RANDATTR_INFO*)GetDataUnit( nCode );
		if( !pInfo ) 
		{
			pInfo = AllocUnit();//new sITEM_RANDATTR_INFO;
			pInfo->m_nAttrCode = nCode;
			_Add(pInfo);
			//m_pRandAttrInfoHashTable->Add( pInfo, nCode );
			pInfo->m_SocketType	= eSOCKET_TYPE_RANDOM;
		}
		else
		{
			assert(bReload);
		}

		pInfo->m_nAttrCode = nCode;
				sr.GetString();//comment
		sr	>> pInfo->m_SocketItemCode;				
		sr	>> pInfo->m_Level;				
		sr	>> pInfo->m_byInLevel;		
		sr	>> pInfo->m_byMainValueType;
		sr	>> pInfo->m_nMainOption;		
		sr	>> pInfo->m_nMainValue;		
		sr	>> pInfo->m_nMainIncValue;	

		sr	>> pInfo->m_nRandRate;	
		sr	>> pInfo->m_nRandRate2;	
		sr	>> pInfo->m_nRandIndex;	
		sr	>> pInfo->m_nRandIndex2;

		//		sr(RAND_SECTION_MAX);
		//sr	>> pInfo->m_arRandSectors;
		//		sr(0);

		sr	>> pInfo->m_nRateInfoID;

		//////////////////////////////////////////////////////
		if(theRateInfoParser.GetRateInfo(pInfo->m_nRateInfoID) == NULL)
		{
			ASSERT(!"没找着RateInfo信息\n");
			LOGINFO("RandAttrInfo %d 没找着Rate信息%d\n",nCode,pInfo->m_nRateInfoID);
		}


		//////////////////////////////////////////////////////
		if(theItemInfoParser.GetItemInfo(pInfo->m_SocketItemCode) == NULL)
		{
			ASSERT(!"没找着Item信息\n");
			LOGINFO("DoubleInfo %d 没找着Item信息%d\n",nCode,pInfo->m_SocketItemCode);
		}

		//////////////////////////////////////////////////////
		if(GetRandAttrType(pInfo->m_nRandIndex) == NULL)
		{
			ASSERT(!"RandIndex没找着RandAttrType信息\n");
			LOGINFO("RandInfo %d 没找着RandAttrType信息%d\n",nCode,pInfo->m_nRandIndex);
		}

		if(GetRandAttrType(pInfo->m_nRandIndex2) == NULL)
		{
			ASSERT(!"RandIndex2没找着RandAttrType信息\n");
			LOGINFO("RandInfo2 %d 没找着RandAttrType信息%d\n",nCode,pInfo->m_nRandIndex2);
		}


		//////////////////////////////////////////////////////
		//INT nRate	=0;
		//for(INT n=0; n<RAND_SECTION_MAX; n++)
		//{
		//	nRate += pInfo->m_arRandSectors[n];
		//	assert(nRate <= 100);
		//	if(nRate > 100)
		//	{
		//		LOGINFO(" %d Section rate too large > 100 \n",nCode);
		//		nRate = 100;
		//	}
		//	pInfo->m_arRandSectors[n] = nRate;
		//}

	}
	sr.CloseFile();


	return TRUE;
}


BOOL ItemAttrInfoParserInstance::LoadOptions(LPCSTR pszFileName,BOOL   bReload)
{

	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszFileName, TRUE) );

	if( !bReload ) 
		 m_pszOptionInfoFileName = _VI( pszFileName);

	__ZERO(m_arOptionInfos);

	INT nOptionIndex;
	for(;sr.GotoNextLine();)
	{
				sr.GetInt();
		sr >> nOptionIndex;

		if(nOptionIndex < 0 || nOptionIndex>= MAX_ITEM_OPTIONS)
		{
			assert(!"Option Info not invalid\n");
			LOGINFO("Option Info %d not invalid \n", nOptionIndex);
			continue;
		}

		if(m_arOptionInfos[nOptionIndex].m_OptionIndex != 0 && !bReload)
		{
			assert(!"Option Info existed\n");
			LOGINFO("Option Info %d  existed\n", nOptionIndex);
			continue;
		}

		m_arOptionInfos[nOptionIndex].m_OptionIndex = nOptionIndex;

		sr >> m_arOptionInfos[nOptionIndex].m_OptionName;
				sr.GetString();//comment
	}

	return TRUE;
}


BOOL ItemAttrInfoParserInstance::LoadRandAttrTable	(LPCSTR szFileName, BOOL bReload)
{
	//ASSERT( m_pRandAttrTypeHashTable != NULL );


	util::TableTextFile sr;
	__CHECK(sr.OpenFile( szFileName, TRUE) );

	if( !bReload ) 
		 m_pszRandAttrTableFileName = _VI(szFileName);

	INT							nCode;
	INT							nType;
	//INT							nGroup;
	VRSTR							sGroup;
	BYTE							byGroupRate;
	sITEM_RANDATTR_TYPE*		pInfo;
	sITEM_RANDATTR_GROUP*	pGroup;
	sITEM_RANDATTR_UNIT*		pUnit;
	INT							nGroupRate(0);

	for(;sr.GotoNextLine();)
	{
		////	code	Type	序号	注释	等级	type	随机属性	值	范围	机率	组	
		///	[0]	(0,20)	[20,40)	[40,60)	[60,80)	[80,100)

		sr.GetInt();
		sr >> nCode;
		sr >> nType;
		sr >> sGroup;
		//sr >> nGroup;
		sr >> byGroupRate;

		pUnit = GetRandAttrUnit(nCode);//m_pRandAttrUnitHashTable->GetData(nCode);
		if( !pUnit ) 
		{
			pInfo = GetRandAttrType(nType);//m_pRandAttrTypeHashTable->GetData( nType );
			if( !pInfo ) 
			{
				pInfo = AllocAttrType(FALSE);//new sITEM_RANDATTR_TYPE;
				_AddType( pInfo, nType );
				//pInfo->m_SocketType	= eSOCKET_TYPE_DOUBLE;
				nGroupRate				= 0;

				//pInfo->m_mpGroups.Initialize(m_dwRandAttrGroupSize);
			}
			
			pGroup = pInfo->m_mpGroups.GetData((DWORD)sGroup);//nGroup);
			if( !pGroup ) 
			{
				pGroup = AllocAttrGroup(FALSE);//new sITEM_RANDATTR_GROUP;
				//pInfo->m_mpGroups.Add( pGroup, (DWORD)sGroup);//nGroup );
				//pInfo->m_arGroups.push_back( pGroup);
				_Push(pInfo,pGroup,sGroup);
				
				//pGroup->m_nGroupIndex	=  nGroup;
				pGroup->m_sGroupName		=  sGroup;
				pGroup->m_byRate			=  byGroupRate;
				nGroupRate					+= byGroupRate;
				assert(nGroupRate <= 100);
				if(nGroupRate > 100)
				{
					LOGINFO("GROUP %s rate too large > 100 \n", (LPCSTR)sGroup);//nGroup);
					nGroupRate = 100;
				}
				_Push(pInfo,nGroupRate);
				//pInfo->m_arRates.push_back(nGroupRate);
			}

			pUnit = AllocRandAttrUnit(TRUE);//new sITEM_RANDATTR_UNIT;
			_Push(pGroup,pUnit);
			//pGroup->m_arUnits.push_back(pUnit);
			_Add(pUnit);
		}
		else
		{
			assert(bReload);
		}



		pUnit->m_nAttrCode	= nCode;
		//pUnit->m_nRandKind	= nGroup;
		sr	>> pUnit->m_nRandIndex;	
				sr.GetString();	///comment

		sr	>> pUnit->m_Level;			
		sr	>> pUnit->m_byValueType;	
		sr	>> pUnit->m_nOptionIndex;
		sr	>> pUnit->m_nOptionValue;
		sr	>> pUnit->m_nIncValueMax;

		sr	>> pUnit->m_nRateInfoID;

		//////////////////////////////////////////////////////
		if(theRateInfoParser.GetRateInfo(pUnit->m_nRateInfoID) == NULL)
		{
			ASSERT(!"没找着RateInfo信息\n");
			LOGINFO("RandAttrTable %d 没找着Rate信息%d\n",nCode,pUnit->m_nRateInfoID);
		}

		//		sr(RAND_SECTION_MAX);
		//sr	>> pUnit->m_arRandSectors;
		//		sr(0);

		//INT nRate=0;
		//for(INT n=0; n<RAND_SECTION_MAX; n++)
		//{
		//	nRate += pUnit->m_arRandSectors[n];
		//	assert(nRate <= 100);
		//	if(nRate > 100)
		//	{
		//		LOGINFO(" %d Section rate too large > 100 \n",nCode);
		//		nRate = 100;
		//	}
		//	pUnit->m_arRandSectors[n] = nRate;
		//}
	}
	sr.CloseFile();


	return TRUE;
}


BOOL ItemAttrInfoParserInstance::LoadDoubleAttrInfo	(LPCSTR	pszDoubleAttrInfoFileName
															,BOOL		bReload)
{
	//ASSERT( m_pDoubleAttrInfoHashTable != NULL );



	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszDoubleAttrInfoFileName, TRUE) );

	if( !bReload ) 
		 m_pszDoubleAttrInfoFileName = _VI(pszDoubleAttrInfoFileName);

	INT								nCode;
	sITEM_DOUBLE_ATTR_INFO*		pInfo;

	for(;sr.GotoNextLine();)
	{
		// Code	注释	Level	浮动范围	type	
		///攻属性	攻属性值	浮动范围	攻属性2值	浮动范围	
		// type	防属性	防属性值	浮动范围	[0]	(0,20)	[20,40)	[40,60)	[60,80)	[80,100)

		sr.GetInt();
		sr >> nCode;

		pInfo = GetDoubleAttrInfo( nCode );
		if( !pInfo ) 
		{
			pInfo = AllocDoubleAttrInfo(TRUE);//new sITEM_DOUBLE_ATTR_INFO;
			pInfo->m_nAttrCode = nCode;
			_Add( pInfo );
			pInfo->m_SocketType	= eSOCKET_TYPE_DOUBLE;
		}
		else
		{
			assert(bReload);
		}

		pInfo->m_nAttrCode = nCode;
				sr.GetString();//comment
		sr	>> pInfo->m_SocketItemCode;				
		sr	>> pInfo->m_Level;					
		sr	>> pInfo->m_byInLevel;		
		sr	>> pInfo->m_byAttValueType;	
		sr	>> pInfo->m_nAttOption;			
		sr	>> pInfo->m_nAttValue;		
		sr	>> pInfo->m_nAttIncValue;	
						 
		sr	>> pInfo->m_nAttOption2;		
		sr	>> pInfo->m_nAttValue2;		
		sr	>> pInfo->m_nAttIncValue2;	
						 
		sr	>> pInfo->m_byDefValueType;	
		sr	>> pInfo->m_nDefOption;		
		sr	>> pInfo->m_nDefValue;		
		sr	>> pInfo->m_nDefIncValue;	

		//		sr(RAND_SECTION_MAX);
		//sr	>> pInfo->m_arRandSectors;
		//		sr(0);

		sr	>> pInfo->m_nRateInfoID;

		//////////////////////////////////////////////////////
		if(theRateInfoParser.GetRateInfo(pInfo->m_nRateInfoID) == NULL)
		{
			ASSERT(!"没找着RateInfo信息\n");
			LOGINFO("DoubleInfo %d 没找着Rate信息%d\n",nCode,pInfo->m_nRateInfoID);
		}

		/////////////////////////////////////////
		if(theItemInfoParser.GetItemInfo(pInfo->m_SocketItemCode) == NULL)
		{
			ASSERT(!"没找着Item信息\n");
			LOGINFO("DoubleInfo %d 没找着Item信息%d\n",nCode,pInfo->m_SocketItemCode);
		}

		//////////////////////////////////////
		//检测机率数据
		//INT nRate=0;
		//for(INT n=0; n<RAND_SECTION_MAX; n++)
		//{
		//	nRate += pInfo->m_arRandSectors[n];
		//	assert(nRate <= 100);
		//	if(nRate > 100)
		//	{
		//		LOGINFO(" %d Section rate too large > 100 \n",nCode);
		//		nRate = 100;
		//	}
		//	pInfo->m_arRandSectors[n] = nRate;
		//}
	}
	sr.CloseFile();


	return TRUE;
}


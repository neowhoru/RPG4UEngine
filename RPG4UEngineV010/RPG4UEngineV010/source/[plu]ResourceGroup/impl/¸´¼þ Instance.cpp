/*////////////////////////////////////////////////////////////////////////
文 件 名：EnchantInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "EnchantInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(EnchantInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
EnchantInfoParserInstance::EnchantInfoParserInstance()
{
}


//----------------------------------------------------------------------------
EnchantInfoParserInstance::~EnchantInfoParserInstance()
{
}



BOOL EnchantInfoParserInstance::Load( LPCSTR szFileName, BOOL bReload )
{
	if( !bReload )
		m_FileName = (szFileName);

	util::TableTextFile sr;
	__CHECK(sr.OpenFile( (LPCSTR)m_FileName, TRUE));


	sLEVEL_MATERIALS	material;
	BYTE&					ItemType = material.m_ItemType;
	LEVELTYPE&			ItemLV	= material.m_ItemLV;
	BYTE					byEnchant;

	for(;sr.GotoNextLine();)
	{

		sr.GetInt();
		memset( &material, 0, sizeof(sLEVEL_MATERIALS) );

		sr >> material.m_Code;
		sr >> material.m_sName;
		sr >> material.m_ItemType;
		sr >> material.m_ItemLV;
		sr >> byEnchant;

		ASSERT( byEnchant <= MAX_ENCHANT_GRADE );
		material.m_Enchant = byEnchant;

		sr >> material.m_byNeedComposite;


		for( int i = 0 ; i < MAX_ENCHANT_MATERIAL_NUM ; ++i )
		{
			sr >> material.m_Material[i].m_MaterialItemCode;
			sr >> material.m_Material[i].m_MaterialItemNum;
		}
		sr >> material.m_Money;
		sr >> material.m_byMinRate;
		sr >> material.m_fCostTimes;

		if(material.m_byMinRate < eSUCCEEDRATE_25 || material.m_byMinRate >= eSUCCEEDRATE_NUM)
		{
			LOGINFO("EnchantInfo %d Min Rate %d Exceed [0,%d]"
					 ,material.m_Code
					 ,material.m_byMinRate
					 ,eSUCCEEDRATE_NUM);
			material.m_byMinRate = eSUCCEEDRATE_100;
		}

		sENCHANT_MATERIALS * pMaterial = NULL;
		assert(ItemType >= 0 && ItemType < ENCHANTMATERIAL_MAX);

		pMaterial = FindEnchantMatInfo( ItemLV, (eENCHANT_MATERIAL_TYPE)ItemType );


		if( !pMaterial ) 
		{
			//pMaterial = AllocUnit();
			//memset( pMaterial, 0, sizeof(sENCHANT_MATERIALS) );

			memcpy(&pMaterial->m_EnchantMaterial[byEnchant]
               ,&material
					,sizeof(sLEVEL_MATERIALS));

			AddItemInfo(pMaterial, ItemLV,(eENCHANT_MATERIAL_TYPE)ItemType);
			//m_arEnchantHash[ItemType].Add(pMaterial, ItemLV);
		}
		else
		{
			memcpy( &pMaterial->m_EnchantMaterial[byEnchant]
					, &material
					, sizeof(sLEVEL_MATERIALS) );
		}

		////////////////////////////////
		_Add(&pMaterial->m_EnchantMaterial[byEnchant]);
	}
	sr.CloseFile();

	return TRUE;
}




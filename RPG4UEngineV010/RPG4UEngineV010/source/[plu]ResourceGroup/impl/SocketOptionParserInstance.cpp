/*////////////////////////////////////////////////////////////////////////
文 件 名：SocketOptionParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SocketOptionParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SocketOptionParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
SocketOptionParserInstance::SocketOptionParserInstance()
{
}


//----------------------------------------------------------------------------
SocketOptionParserInstance::~SocketOptionParserInstance()
{
}



BOOL SocketOptionParserInstance::Load( LPCSTR pszFileName, BOOL bReload )
{


	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszFileName, TRUE) );

	if( !bReload )
		 m_FileName = (pszFileName);

	memset( m_SocketOption, 0, sizeof(sSOCKET_OPTION)*MAX_ITEM_OPTION_NUM );


	CODETYPE					Code;
	sSOCKETITEM_OPTION * pInfo;
	DWORD						dwData;

	for(;sr.GotoNextLine();)
	{
		sr.GetInt();	///空列

		sr >> Code;

		pInfo = GetItemOption(Code);//m_OptionParams.GetData( Code );
		if( !pInfo ) 
		{
			pInfo = AllocUnit();//new sSOCKETITEM_OPTION;
			pInfo->m_SocketItemCode = Code;
			_Add(pInfo);
			//m_OptionParams.Add( pInfo, Code );
		}
		else
		{
			assert(bReload);
		}

		// sSOCKETITEM_OPTION 信息注册
		pInfo->m_SocketItemCode = Code;

		sr >> pInfo->m_AttrIndex;
		sr >> pInfo->m_sAttrName;
		sr >> pInfo->m_NCode;
		sr >> dwData; pInfo->m_SocketLevel = (eSOCKET_LEVEL)dwData;
		sr >> dwData; pInfo->m_NumericType = (eNUMERIC_TYPE)dwData;
		sr >> pInfo->m_byValue;

		////////////////////////////////////////
		//SocketItemOption信息 映射到 SocketOption索引中

		ASSERT( pInfo->m_SocketLevel == SOCKETLV_LOW || pInfo->m_SocketLevel == SOCKETLV_HIGH );

		sSOCKET_OPTION&	socketOption	= m_SocketOption[pInfo->m_AttrIndex];

		socketOption.m_AttrIndex							= pInfo->m_AttrIndex;
		socketOption.m_SocketItemCode						= pInfo->m_SocketItemCode;
		socketOption.m_sAttrName							= pInfo->m_sAttrName;
		socketOption.m_NCode									= pInfo->m_NCode;
		socketOption.m_NumericType							= pInfo->m_NumericType;
		socketOption.m_iValue[pInfo->m_SocketLevel]	= pInfo->m_byValue;
	}

	sr.CloseFile();



	return TRUE;
}

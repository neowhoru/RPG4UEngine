/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemInfoParserInstance.h"
#include "StructBase.h"
#include "ConstArray.h"
#include "CommonDefine.h"
#include "TableTextFile.h"
#include "StructBase.h"
#include "SkeletonDefine.h"

using namespace skeleton;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
ItemInfoParserInstance::ItemInfoParserInstance()
{
}


//----------------------------------------------------------------------------
ItemInfoParserInstance::~ItemInfoParserInstance()
{
}



BOOL ItemInfoParserInstance::LoadVItemInfo( LPCSTR pszFileName, BOOL bReload )
{	
	//__VERIFY_PTR(m_pItemTypes,_T("Invalid m_pItemTypes"));

	__UNUSED(bReload);

	util::TableTextFile sr;
	__CHECK(sr.OpenFile(pszFileName, TRUE));



	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		CODETYPE				itemCode;
		sr >> itemCode;

		sVITEMINFO_BASE *	pInfo = GetVItemInfo( itemCode );
		if( !pInfo ) 
		{
			pInfo = AllocVItem(TRUE);//new sVITEMINFO_BASE;
			pInfo->m_Code = itemCode;
			_Add(pInfo);
			//m_pVItemInfos->Add( pInfo, itemCode );
		}
		else
		{
			ASSERT(FALSE != bReload  && !"Reload Failed." );
		}
		pInfo->m_Code = itemCode;

		sr >> pInfo->m_sName;			
		sr >> pInfo->m_DescID;			
		//sr >> pInfo->m_dwCost;		
		//sr >> pInfo->m_byGrade;	

			pInfo->m_dwType = GetVItemTypeValue(sr.GetString());		

		sr >> pInfo->m_IconID;	
		sr >> pInfo->m_BigIconID;	
		sr >> pInfo->m_byShowMode;	
		sr >> pInfo->m_ModelID;		
		//sr >> pInfo->m_byRareRate;	
		sr >> pInfo->m_byShowEffect;
		//sr >> pInfo->m_byCanMove;	
		//sr >> pInfo->m_byCanDiscard;
		//sr >> pInfo->m_byCanDestroy;
		//sr >> pInfo->m_byCanTrade;	
		//sr >> pInfo->m_byCanBuySell;
		//sr >> pInfo->m_byCanStore;	
		//sr >> pInfo->m_byCanLock;	
		//sr >> pInfo->m_wWeight;		

		//以下依据Item类型分读
		switch(pInfo->m_dwType)
		{
		//case VITEMTYPE_RESTORE:
		case VITEMTYPE_WEAPON:
			sr >> pInfo->WEAPON.m_RHModelID;
					//pInfo->WEAPON.m_byEquipPos = (BYTE)GetEquipPosValue(sr.GetString());
			//sr >> pInfo->WEAPON.m_byWeaponSound;
			sr >> pInfo->WEAPON.m_byTwoHand;
			sr >> pInfo->WEAPON.m_LHModelID;		
			//sr >> pInfo->WEAPON.m_ScabbardModelID;

			break;

		case VITEMTYPE_ARMOUR:
			sr >> pInfo->ARMOUR.m_ArmourModelID;		
					//pInfo->ARMOUR.m_byEquipPos = (BYTE)GetEquipPosValue(sr.GetString());
			//sr >> pInfo->ARMOUR.m_byArmourTexture;
			//sr.GetInt();
			sr.GetInt();
			sr.GetInt();
			sr >> pInfo->ARMOUR.m_byArmourTexture;
			sr >> pInfo->ARMOUR.m_byCape;
			sr >> pInfo->ARMOUR.m_byHeadWear;
			break;

		case VITEMTYPE_STONE:
			sr.GetInt();	// Weapon
			sr.GetString();
			sr.GetString();
			sr.GetInt();
			sr.GetInt();
			sr.GetInt();

			sr.GetInt();	// armour
			sr.GetInt();

			sr.GetInt();	//gem
			sr.GetInt();

			sr.GetInt();	//reel
			sr.GetInt();

			//sr >> pInfo->STONE.m_wStoneType;
			break;

		case VITEMTYPE_GEM:
		case VITEMTYPE_CARD:
		case VITEMTYPE_CRYSTAL:
			sr.GetInt();	// Weapon
			sr.GetString();
			sr.GetString();
			sr.GetInt();
			sr.GetInt();
			sr.GetInt();

			sr.GetInt();	// armour
			sr.GetInt();

			//gem
			//sr >> pInfo->GEM.m_byCanInlay;
			//sr >> pInfo->GEM.m_byCanEnhance;
			break;

		case VITEMTYPE_REEL:
			sr.GetInt();	// Weapon
			sr.GetString();
			sr.GetString();
			sr.GetInt();
			sr.GetInt();
			sr.GetInt();

			sr.GetInt();	// armour
			sr.GetInt();

			sr.GetInt();	//gem
			sr.GetInt();

			//reel
			//sr >> pInfo->REEL.m_StoneID;
			//sr >> pInfo->REEL.m_SkillID;

			//...
			break;

		//case VITEMTYPE_MATERIAL:
		//	break;

		}





	}//for(;sr.GotoNextLine();)





	return TRUE;
}


BOOL ItemInfoParserInstance::Load( LPCSTR  pszFileName, BOOL bReload )
{	
	bReload;
	util::TableTextFile sr;
	__CHECK(sr.OpenFile( pszFileName, TRUE));


	sITEMINFO_BASE *	pPreInfo = NULL;
	CODETYPE				Code;
	sITEMINFO_BASE *	pInfo;
	//sVITEMINFO_BASE *	pVInfo;

	//m_sFilePath	= _VI(pszFileName);

	for(;sr.GotoNextLine();)
	{
				sr.GetInt();	///空列
		sr >> Code;

		if(Code == 0)
			continue;
		pInfo = GetItemInfo( Code );
		if( !pInfo ) 
		{
			pInfo = AllocUnit();//new sITEMINFO_BASE;
			pInfo->m_Code = (SLOTCODE)Code;
			_Add( pInfo);
		}
		else
		{
			ASSERT( bReload );
		}
		pInfo->m_Code = (SLOTCODE)Code;


		sr >>  pInfo->m_ItemName;
		sr >>  pInfo->m_VItemCode;

		sr >>  pInfo->m_wType;
		sr >>  pInfo->m_LV;
				 if(pInfo->m_LV < 0 || pInfo->m_LV > MAX_ITEM_LEVEL)
				 {
					 LOGINFO	("ItemInfo %s Level %d exceed [0,%d]\n"
								,(LPCSTR)pInfo->m_ItemName
								,pInfo->m_LV
								,MAX_ITEM_LEVEL);
				 }

		sr >>  pInfo->m_Dura;
		sr >>  pInfo->m_DropLV;
		sr >>  pInfo->m_byIsDrop;
		sr >>  pInfo->m_byMaterialType;

		sr >>  pInfo->m_LimitEqLevel;
		sr >>  pInfo->m_wLimitStr;
		sr >>  pInfo->m_wLimitDex;
		sr >>  pInfo->m_wLimitInt;
		sr >>  pInfo->m_wLimitVit;
		sr >>  pInfo->m_wLimitSkil1;
		sr >>  pInfo->m_wLimitSkil2;

		sr >>  pInfo->m_wMinDamage;
		sr >>  pInfo->m_wMaxDamage;

		sr >>  pInfo->m_wPhyAttMinDamgage;
		sr >>  pInfo->m_wPhyAttMaxDamgage;
		sr >>  pInfo->m_wRangeAttMinDamgage;
		sr >>  pInfo->m_wRangeAttMaxDamgage;
		sr >>  pInfo->m_wPhyAttRate;
		sr >>  pInfo->m_wPhyAttSpeed;
		sr >>  pInfo->m_wPhyDef;
		sr >>  pInfo->m_wPhyAvoid;
		sr >>  pInfo->m_wAttType;
		sr >>  pInfo->m_wAttRange;
		sr >>  pInfo->m_wDefType;

		sr >>  pInfo->m_bySeries;
		sr >>  pInfo->m_wMagicAttMinDamgage;
		sr >>  pInfo->m_wMagicAttMaxDamgage;
		sr >>  pInfo->m_wMagicAttSpeed;
		sr >>  pInfo->m_wMagicDef;

				 sr(ELEMENTKIND_MAX);
		sr >>  pInfo->m_wElementDef;
		sr >>  pInfo->m_wElementAttack;
				 sr(0);

		sr >>  pInfo->m_nMoveSpeed	;
		sr >>  pInfo->m_wEquipClass;
				 //if(pInfo->m_wEquipClass == 0)
					// pInfo->m_wEquipClass = 0xffffffff;
#ifdef USE_OLD
		sr >>  pInfo->m_wEqClass1;
		sr >>  pInfo->m_wEqClass2;
		sr >>  pInfo->m_wEqClass3;
		sr >>  pInfo->m_wEqClass4;
		sr >>  pInfo->m_wEqClass5;
#endif
		sr >>  pInfo->m_wEqPos;
		sr >>  pInfo->m_wMaxRank;
		sr >>  pInfo->m_SocketNum;

		for(INT o=0; o<ITEM_OPTION_MAX; o++)
		{
			sr >>  pInfo->m_arOptions[o].m_wOptionKind;
			sr >>  pInfo->m_arOptions[o].m_wOption;									
		}
		//sr >>  pInfo->m_wOptionKind1;
		//sr >>  pInfo->m_wOption1;									
		//sr >>  pInfo->m_wOptionKind2;
		//sr >>  pInfo->m_wOption2;

		//sr >>  pInfo->m_wMissile;
		//sr >>  pInfo->m_bHeadType;

		//		if (pInfo->m_bHeadType)
		//			pInfo->m_bHeadType--;

		//sr >>  pInfo->m_wCustomize;


		sr >>  pInfo->m_byDupNum;									
		sr >>  pInfo->m_byWasteType;
		sr >>  pInfo->m_wHealHP;
		sr >>  pInfo->m_wHealHPTime;

		sr >>  pInfo->m_wTimes;
		sr >>  pInfo->m_dCoolTime;


		/////////////////////////////////////////////////
		//注册名字映射
		//pVInfo = GetVItemInfo(pInfo->m_VItemCode);
		//if(!pVInfo)
		//{
		//	assert(pVInfo);
		//	LOGINFO("VItemInfo [%s %d] not exist\n", pInfo->m_pszName, pInfo->m_Code);
		//}
		//else
		{
			if(GetItemInfo((LPCTSTR)pInfo->m_ItemName))
			{
				LOGINFO("ItemInfo  [%s %d] Name repeat\n", (LPCSTR)pInfo->m_ItemName, pInfo->m_Code);
			}
			else
			{
				_AddNameItem(pInfo);
			}
		}

		//////////////////////////////////////////////////
		//注册冷却时间
		_AddCoolTime(pInfo);
		
		//sr >>  pInfo->m_wSpecialEffect;
		//sr >>  pInfo->m_MaterialCode;

		pPreInfo = pInfo;
	}
	sr.CloseFile();

	m_bLoaded = TRUE;

	return TRUE;
}


BOOL ItemInfoParserInstance::LoadType( LPCSTR pszFileName, BOOL bReload )
{	
	bReload;
	//__VERIFY_PTR(m_pItemTypes,_T("Invalid m_pItemTypes"));

	util::TableTextFile sr;
	__CHECK(sr.OpenFile(pszFileName, TRUE));

	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		WORD		itemType;
		sr >> itemType;

		sITEMTYPE_BASE *	pInfo = GetItemType( (eITEM_TYPE)itemType );
		if( !pInfo ) 
		{
			pInfo = AllocItemType(TRUE);//new sITEMTYPE_BASE;
			pInfo->m_wType = itemType;
			_Add(pInfo);
			//m_pItemTypes->Add( pInfo, itemType );
		}
		else
		{
			ASSERT(FALSE != bReload  && !"Reload Failed." );
		}
		pInfo->m_wType = itemType;

		sr >> pInfo->m_sName;			
		sr >> pInfo->m_sGroup;	
		sr.GetString();			//标识
	}
	sr.CloseFile();
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillInfoParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillInfoParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"
#include "CommonDefine.h"
#include "FormatString.h"
#include "SeriesInfoParser.h"
#include "ConstTextRes.h"
#include "TextResManager.h"
#include "StateInfoParser.h"
#include "ScriptWord.h"
#include "NPCInfoParser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SkillInfoParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
SkillInfoParserInstance::SkillInfoParserInstance()
{
}


//----------------------------------------------------------------------------
SkillInfoParserInstance::~SkillInfoParserInstance()
{
}



BOOL SkillInfoParserInstance::Load	(LPCSTR pszSkillFileName
                           ,LPCSTR pszStyleFileName
									,LPCSTR pszVSkillFileName
									,BOOL   bReload)
{
	if(pszVSkillFileName != NULL)
		__BOOL_CALL( _LoadVSkill	( pszVSkillFileName, bReload ));

	__BOOL_CALL( _Load			( pszSkillFileName, bReload ));
	__BOOL_CALL( _LoadStyle		( pszStyleFileName, bReload ));

	return TRUE;
}

BOOL SkillInfoParserInstance::_LoadStyle( LPCSTR pszStyleFileName, BOOL bReload )
{

	//ASSERT( m_pSkillInfoHashTable != NULL );


	util::TableTextFile sr;
	__VERIFY	(sr.OpenFile(pszStyleFileName, TRUE )
				,FMSTR("SkillInfoParserInstance::_LoadStyle %s Failed\n",pszStyleFileName ));

	if( !bReload )
		m_sStyleFileName = _VI(pszStyleFileName);


	SLOTCODE				StyleCode;
	sSKILLINFO_COMMON *	pRootInfo;
	sSTYLEINFO_BASE *	pInfo;
	//sVSKILLINFO_BASE *	pVInfo;

	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		sr >> StyleCode;

		pRootInfo = GetInfo( StyleCode );
		if( pRootInfo )
		{
			ASSERT( pRootInfo->IsStyle() );
		}

		pInfo = static_cast<sSTYLEINFO_BASE *>(pRootInfo);
		if( !pInfo ) 
		{
			pInfo = AllocStyle();//new sSTYLEINFO_BASE;
			_Add( pInfo, StyleCode );
		}
		else
		{
			ASSERT(bReload && "Reload Failed.\n" );
		}


		pInfo->m_SkillCode	= StyleCode;
		pInfo->m_byType		= sSKILLINFO_COMMON::STYLE_TYPE;

		sr >> pInfo->m_SkillClassCode;
		sr	>> pInfo->m_SkillName;
		sr >> pInfo->m_VSkillCode;
		sr >> pInfo->m_VItemCode;

		//sr >> pInfo->m_wSkillattribute;

		sr >> pInfo->m_wRequireLV;
		sr >> pInfo->m_wSkillLV;
		sr >> pInfo->m_wMaxLV;
		sr >> pInfo->m_wOverLV;

				//sr(sSTYLEINFO_BASE::OVERSTAT_NUM);
		//sr >> pInfo->m_wOverStatclass	;
		//sr >> pInfo->m_wOverstat;
				//sr(0);

		sr >> pInfo->m_byRequireSkillPoint		;
		sr >> pInfo->m_dwClassDefine			;
		sr >> pInfo->m_WeaponDefines			;
		//sr >> pInfo->m_byAttType				;
		sr >> pInfo->m_fAttRange				;
		//sr >> pInfo->m_byMoveAttack			;
		//sr >> pInfo->m_byStyleCheck			;



				sr(ATTACK_SEQUENCE_MAX);
		sr >> pInfo->m_arAttackActions;
		sr >> pInfo->m_dwAttackTime;
		sr >> pInfo->m_iAddDamage;
		sr >> pInfo->m_fDamagePercent;
				sr(0);

		sr >> pInfo->m_byAttRangeform			;
		sr >> pInfo->m_fStyleArea				;
		sr >> pInfo->m_fThirdDelay				;

		sr >> pInfo->m_iAttackRate				;
		sr >> pInfo->m_iAvoidRate				;
		sr >> pInfo->m_iAttackSpeed			;
		sr >> pInfo->m_iBonusDefence			;
		sr >> pInfo->m_wCriticalBonus			;
		sr >> pInfo->m_fDefenseIgnore			;

		sr >> pInfo->m_fPierceRate				;
		sr >> pInfo->m_fPierceRange			;
		sr >> pInfo->m_fStunRate				;
		sr >> pInfo->m_wStunTime				;
		sr >> pInfo->m_fKnockBackRate			;
		sr >> pInfo->m_fKnockBackRange		;
		sr >> pInfo->m_fDownRate				;
		//sr >> pInfo->m_fDelayReduce			;
		//sr >> pInfo->m_fDelayOccur				;

		sr >> pInfo->m_wHPAbsorb				;
		sr >> pInfo->m_fHPAbsorbPer			;
		sr >> pInfo->m_wMPAbsorb				;
		sr >> pInfo->m_fMPAbsorbPer			;
		//sr >> pInfo->m_wMaxTargetNo			;


		if(pInfo->m_wOverLV < pInfo->m_wMaxLV)
			pInfo->m_wOverLV = pInfo->m_wMaxLV;

		if(pInfo->m_wOverLV < pInfo->m_wSkillLV )
		{
			LOGINFO("StyleInfo OverLV too small,must bigger SkillLV\n");
			pInfo->m_wOverLV = pInfo->m_wSkillLV;
		}
		//if(pInfo->m_wMaxLV < pInfo->m_wSkillLV)
		//{
		//	pInfo->m_wMaxLV = pInfo->m_wSkillLV;
		//	LOGINFO("StyleInfo  MaxLV too small,must bigger SkillLV\n");
		//}

		/////////////////////////////////////////////////
		//注册名字映射
		//pVInfo = GetVSkillInfo(pInfo->m_VSkillCode);
		//if(!pVInfo)
		//{
		//	//assert(pVInfo);
		//	LOGINFO("StyleInfo [%s %d] not exist\n", (LPCSTR)pInfo->m_SkillName, pInfo->m_SkillCode);
		//}
		//else
		{
			if(GetInfo((LPCTSTR)pInfo->m_SkillName))
			{
				LOGINFO("ItemInfo  [%s %d] Name repeat\n",(LPCSTR)pInfo->m_SkillName, pInfo->m_SkillCode);
			}
			else
			{
				_AddNameSkill(pInfo);
			}
		}
	}

	sr.CloseFile();

	return TRUE;
}




BOOL SkillInfoParserInstance::_Load( LPCSTR pszSkillFileName, BOOL bReload )
{

	//ASSERT( m_pSkillInfoHashTable != NULL );

	SLOTCODE		ClassCodeCheck = 0;

	util::TableTextFile sr;

	__VERIFY	(sr.OpenFile(pszSkillFileName , TRUE )
				,FMSTR("SkillInfoParserInstance::Load %s Failed\n",pszSkillFileName));

	if( !bReload )
		 m_FileName = (pszSkillFileName);

	SLOTCODE				SkillCode;
	sSKILLINFO_COMMON *	pRootInfo;
	SkillDetailInfo * pInfo;
	//sVSKILLINFO_BASE *	pVInfo;

	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		sr >> SkillCode;

		 pRootInfo = GetInfo( SkillCode );
		if( pRootInfo )
			ASSERT( pRootInfo->IsSkill() );

		pInfo = static_cast<SkillDetailInfo *>(pRootInfo);
		if( !pInfo ) 
		{
			pInfo = AllocDetail(FALSE);//new SkillDetailInfo;
			_Add( pInfo, SkillCode );
		}
		else
		{
			ASSERT(bReload && "Reload failed." );
		}

		pInfo->m_SkillCode	= SkillCode;
		pInfo->m_byType		= sSKILLINFO_COMMON::SKILL_TYPE;

		sr >> pInfo->m_SkillClassCode;
		sr >> pInfo->m_SkillName;
				sr.GetString();	///忽略描述
		sr >> pInfo->m_VSkillCode;
		sr >> pInfo->m_VItemCode;

				sr(sSKILLINFO_BASE::WEAPONDEFINE_NUM);
		sr >> pInfo->m_WeaponDefines;
				sr(0);


		sr	>> pInfo->m_byNeedStop;

		sr >> pInfo->m_wFlyingLifeTime;


		//sr >> pInfo->m_wSkillAttribute;
		sr >> pInfo->m_wRequireLV;
		sr >> pInfo->m_wSkillLV;
		sr >> pInfo->m_wMaxLV;
		sr >> pInfo->m_wOverLV;

		//		sr(sSKILLINFO_BASE::OVERSTAT_NUM);
		////sr >> pInfo->m_wOverStatclass;
		//sr >> pInfo->m_wOverstat;
		//		sr(0);

		sr >> pInfo->m_bySkillType;

		sr >> pInfo->m_bySkillUserType;
		sr >> pInfo->m_dwClassDefine;

		sr >> pInfo->m_bySkillStatType;

				sr(sSKILLINFO_BASE::OVERSTAT_NUM);
		sr >> pInfo->m_wRequireSkillStat;
				sr(0);


		sr >> pInfo->m_byRequireSkillPoint;
		sr >> pInfo->m_byTarget;
		sr >> pInfo->m_wHPSpend;
		sr >> pInfo->m_wMPSpend;
		sr >> pInfo->m_wSkillCasting;
		sr >> pInfo->m_wCoolTime;
		sr >> pInfo->m_wSkillRange;
		sr >> pInfo->m_byAttRangeform;
		sr >> pInfo->m_wSkillArea;
		sr >> pInfo->m_byMaxTargetNo;

		///技能效力处理.....
		sABILITYINFO_BASE AbilityInfo;
		for(INT i = 0; i < MAX_ABILITY_NUM; ++i )
		{
			memset( &AbilityInfo, 0, sizeof(sABILITYINFO_BASE) );

			sr >> AbilityInfo.m_wAbilityID		;
			sr >> AbilityInfo.m_byRangeType	;
			sr >> AbilityInfo.m_wSuccessRate	;
			sr >> AbilityInfo.m_iOption1		;
			sr >> AbilityInfo.m_iOption2		;

				sr(sABILITYINFO_BASE::PARAM_NUM);
			sr >> AbilityInfo.m_iParam	;
				sr(0);

			sr >> AbilityInfo.m_wStateID		;


			pInfo->AddAbilityInfo( AbilityInfo );

			if( ClassCodeCheck == pInfo->m_SkillClassCode )	
				continue;


			if(	AbilityInfo.m_byRangeType	== SKILL_ABILITY_TARGETAREA_ENEMY 
				||	AbilityInfo.m_byRangeType	== SKILL_ABILITY_TARGETAREA_FRIEND
				||	AbilityInfo.m_byRangeType	== SKILL_ABILITY_MYAREA_ENEMY
				||	AbilityInfo.m_byRangeType	== SKILL_ABILITY_MYAREA_FRIEND )
			{
				if(	pInfo->m_byAttRangeform		!= SKILLAREA_FOWARD_120
					&&	pInfo->m_byAttRangeform		!= SKILLAREA_FOWARD_160 
					&&	pInfo->m_byAttRangeform		!= SKILLAREA_FOWARD_360 
					&&	pInfo->m_byAttRangeform		!= SKILLAREA_PIERCE		
					&&	AbilityInfo.m_wAbilityID	!= ABILITY_BUFF_RANGE_DAMAGE )
				{
					DISPMSG	( "[SkillInfoParser] SkillCode[%d] Invalid m_byAttRangeform1! \n"
								, pInfo->m_SkillCode );
					ClassCodeCheck = pInfo->m_SkillClassCode;
				}
			}

			else if( AbilityInfo.m_byRangeType == SKILL_ABILITY_FIELD )
			{
				if( pInfo->m_byTarget != SKILL_TARGET_AREA )
				{
					DISPMSG( "[SkillInfoParser] SkillCode[%d] Invalid m_byTarget1! \n", pInfo->m_SkillCode );
					ClassCodeCheck = pInfo->m_SkillClassCode;
				}

				//if( pInfo->m_byAttRangeform != SKILLAREA_AREA_POSITION )
				//{
				//	DISPMSG( "[SkillInfoParser] SkillCode[%d] Invalid m_byAttRangeform2! \n", pInfo->m_SkillCode );
				//	ClassCodeCheck = pInfo->m_SkillClassCode;
				//}
			}

			else if( AbilityInfo.m_byRangeType == SKILL_ABILITY_CORPSE_FRIEND )
			{
				if(	pInfo->m_byTarget != SKILL_TARGET_FRIEND_CORPSE 
					&&	pInfo->m_byTarget != SKILL_TARGET_AREA )
				{
					DISPMSG	( "[SkillInfoParser] SkillCode[%d] Invalid m_byTarget2! \n"
								, pInfo->m_SkillCode );
					ClassCodeCheck = pInfo->m_SkillClassCode;
				}
			}

			else if( AbilityInfo.m_byRangeType == SKILL_ABILITY_CORPSE_FRIEND )
			{
				if( pInfo->m_byTarget != SKILL_TARGET_SUMMON )
				{
					DISPMSG	( "[SkillInfoParser] SkillCode[%d] Invalid m_byTarget3! \n"
								, pInfo->m_SkillCode );
					ClassCodeCheck = pInfo->m_SkillClassCode;
				}
			}
		}



		if(pInfo->m_wOverLV < pInfo->m_wMaxLV)
			pInfo->m_wOverLV = pInfo->m_wMaxLV;

		if(pInfo->m_wOverLV < pInfo->m_wSkillLV )
		{
			LOGINFO("StyleInfo %s OverLV too small,must bigger SkillLV\n",(LPCSTR)pInfo->m_SkillName);
			pInfo->m_wOverLV = pInfo->m_wSkillLV;
		}
		//if(pInfo->m_wMaxLV < pInfo->m_wSkillLV)
		//{
		//	pInfo->m_wMaxLV = pInfo->m_wSkillLV;
		//	LOGINFO("SkillInfo %s MaxLV too small,must bigger SkillLV\n",(LPCSTR)pInfo->m_SkillName);
		//}


		/////////////////////////////////////////////////
		//注册名字映射
		//pVInfo = GetVSkillInfo(pInfo->m_VSkillCode);
		//if(!pVInfo)
		//{
		//	//assert(pVInfo);
		//	LOGINFO("StyleInfo [%s %d] not exist\n", (LPCSTR)pInfo->m_SkillName, pInfo->m_SkillCode);
		//}
		//else
		{
			if(GetInfo((LPCTSTR)pInfo->m_SkillName))
			{
				LOGINFO("SkillInfo  [%s %d] Name repeat\n", (LPCSTR)pInfo->m_SkillName, pInfo->m_SkillCode);
			}
			else
			{
				_AddNameSkill(pInfo);
			}
		}


		///冷却时间注册.....
		_AddCoolTime(pInfo);
	}


	sr.CloseFile();

	return TRUE;
}


BOOL SkillInfoParserInstance::_LoadVSkill( LPCSTR pszVSkillFileName, BOOL bReload )
{

	//ASSERT( m_pVSkillInfoHashTable != NULL );

	//SLOTCODE		ClassCodeCheck = 0;

	util::TableTextFile	sr;

	__VERIFY	(sr.OpenFile(pszVSkillFileName , TRUE )
				,FMSTR("SkillInfoParserInstance::Load %s Failed\n",pszVSkillFileName));

	if( !bReload )
		m_sVSkillFileName = _VI(pszVSkillFileName);

	SLOTCODE				SkillCode;
	sVSKILLINFO_BASE *	pInfo;
	INT					n;

	for(;sr.GotoNextLine();)
	{
		sr.GetNextNumber();	///跳过第1列空格
		sr >> SkillCode;

		 pInfo = GetVSkillInfo( SkillCode );

		if( !pInfo ) 
		{
			pInfo = AllocVSkill(TRUE);//new sVSKILLINFO_BASE;
			_Add( pInfo, SkillCode );
		}
		else
		{
			ASSERT(bReload && "Reload failed." );
		}

		pInfo->m_Code	= SkillCode;


		sr >> pInfo->m_sName;	
		sr >> pInfo->m_SkillLV;
				sr(sVSKILLINFO_BASE::ACTION_NUM);
		sr >> pInfo->m_arActions;
				sr(0);

		//sr >> pInfo->m_byFirstActionRate;
		sr >> pInfo->m_dwActionTimeMin;	
		sr >> pInfo->m_dwActionTimeMax;	
		sr >> pInfo->m_dwPrepareTime;		
		sr >> pInfo->m_sIntonateAction;	

				sr(sVSKILLINFO_BASE::ATTACK_NUM);
		sr >> pInfo->m_arIntonateEffect;
		sr >> pInfo->m_arAttackerEffect;
		sr >> pInfo->m_arSkillEffect;
		sr >> pInfo->m_arTargetEffect;
				sr(0);

		//sr >> pInfo->m_sTargetHitAction;
		//sr >> pInfo->m_ToSelfStatusID;	
		//sr >> pInfo->m_ToSelfStatusLV;	
		//sr >> pInfo->m_ToDestStatusID;	
		//sr >> pInfo->m_ToDestStatusLV;	
		//sr >> pInfo->m_wStatusRate;		

		//sr >> pInfo->m_bySkillType;		
		//sr >> pInfo->m_wHoldTime;		
		//sr >> pInfo->m_FSkillRange;		
		sr >> pInfo->m_byHoldSkill;		
		sr >> pInfo->m_sHoldAction;		
		sr >> pInfo->m_HoldEffectID;	
		//sr >> pInfo->m_byNeedWeapon;	
		//sr >> pInfo->m_wNeedWeaponType;
		//sr >> pInfo->m_byLeftHand;		
		sr >> pInfo->m_byEmission;		
		sr >> pInfo->m_byAssault;		
		//sr >> pInfo->m_byHideStatus;	
		//sr >> pInfo->m_bySummon;			
		//sr >> pInfo->m_SummonMonster;	
		//sr >> pInfo->m_wEffectDuration;


		/// 处理ActionName NULL数据
		LPCSTR szText = *pInfo->m_arActions[0];
		if(szText[0] == 0)
			LOGINFO("VSkill %s %d 第1个动作无效\n",pInfo->m_sName,pInfo->m_Code);
		for(n=1; n<sVSKILLINFO_BASE::ACTION_NUM; n++)
		{
			szText = *pInfo->m_arActions[n];
			if(szText[0]==0)
				pInfo->m_arActions[n] = pInfo->m_arActions[0];
		}


		//////////////////////////////////////////////////////////////
		//sEFFECT_INFO* pEffect;
		//LOGINFO("%s	",(LPCSTR)pInfo->m_sName);
		//for(INT aa=0;aa<sVSKILLINFO_BASE::ATTACK_NUM;aa++)
		//{
		//	pEffect = theEffectResourceList.GetEffectInfo((LPCSTR)pInfo->m_arIntonateEffect[aa]);
		//	if(pEffect)			LOGINFO("%d	",pEffect->dwCode);
		//	else					LOGINFO("	");
		//}
		//for(INT aa=0;aa<sVSKILLINFO_BASE::ATTACK_NUM;aa++)
		//{
		//	pEffect = theEffectResourceList.GetEffectInfo((LPCSTR)pInfo->m_arAttackerEffect[aa]);
		//	if(pEffect)			LOGINFO("%d	",pEffect->dwCode);
		//	else					LOGINFO("	");
		//}
		//for(INT aa=0;aa<sVSKILLINFO_BASE::ATTACK_NUM;aa++)
		//{
		//	pEffect = theEffectResourceList.GetEffectInfo((LPCSTR)pInfo->m_arSkillEffect[aa]);
		//	if(pEffect)			LOGINFO("%d	",pEffect->dwCode);
		//	else					LOGINFO("	");
		//}
		//for(INT aa=0;aa<sVSKILLINFO_BASE::ATTACK_NUM;aa++)
		//{
		//	pEffect = theEffectResourceList.GetEffectInfo((LPCSTR)pInfo->m_arTargetEffect[aa]);
		//	if(pEffect)			LOGINFO("%d	",pEffect->dwCode);
		//	else					LOGINFO("	");
		//}
		//LOGINFO("\n");

	}

	sr.CloseFile();

	return TRUE;
}






eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrType(sABILITYINFO_BASE& ability)
{
	switch( ability.m_wAbilityID )
	{
	case ABILITY_MAX_HP_INCREASE:					return ATTRIBUTE_MAX_HP;
	case ABILITY_CUR_HP_INCREASE:					return ATTRIBUTE_CUR_HP;
	case ABILITY_RECOVER_HP_INCREASE:				return ATTRIBUTE_RECOVERY_HP;

	case ABILITY_MAX_MP_INCREASE:					return ATTRIBUTE_MAX_MP;
	case ABILITY_CUR_MP_INCREASE:					return ATTRIBUTE_CUR_MP;
	case ABILITY_RECOVER_MP_INCREASE:				return ATTRIBUTE_RECOVERY_MP;

	case ABILITY_PHYSICAL_ATTACKPOWER_INCREASE:
	case ABILITY_MAGIC_ATTACKPOWER_INCREASE:		return GetAttrTypeForAttackpower( (eATTACK_KIND)ability.m_iOption1 );
	case ABILITY_ATTACKPOWER_BY_ARMORTYPE:		return GetAttrTypeForBonusDamage( (eARMOR_TYPE)ability.m_iOption1, ability.m_iParam[0] );

	case ABILITY_PHYSICAL_DEFENSE_INCREASE:
	case ABILITY_MAGIC_DEFENSE_INCREASE:			return GetAttrTypeForDefence( (eATTACK_KIND)ability.m_iOption1 );
	case ABILITY_DEFENSE_BY_ATTACKTYPE:			return GetAttrTypeForBonusDefence( (eATTACK_KIND)ability.m_iOption1 );

	case ABILITY_STAT_INCREASE:						return GetAttrTypeForStat( (eBASE_ATTRIBTUE)ability.m_iOption1 );

	case ABILITY_PHYSICAL_ATTACKRATE_INCREASE:	return ATTRIBUTE_PHYSICAL_ATTACK_SUCCESS_RATIO;
	case ABILITY_PHYSICAL_AVOIDRATE_INCREASE:	return ATTRIBUTE_PHYSICAL_ATTACK_BLOCK_RATIO;

	case ABILITY_MOVE_SPEED_INCREASE:				return ATTRIBUTE_MOVE_SPEED;
	case ABILITY_PHYSICAL_SPEED_INCREASE:			return ATTRIBUTE_ATTACK_SPEED;
	case ABILITY_MAGIC_SPEED_INCREASE:				return ATTRIBUTE_ATTACK_SPEED;

	case ABILITY_DAMAGE_DECREASE:					return GetAttrTypeForReduceDamage( (eATTACK_KIND)ability.m_iOption1 );

	case ABILITY_SKILLRANGE_INCREASE:				return ATTRIBUTE_SKILL_ATTACK_RANGE;

	case ABILITY_CRITICAL_RATIO_INCREASE:			return ATTRIBUTE_ADD_ALL_CRITICAL_RATIO;
	case ABILITY_CRITICAL_DAMAGE_INCREASE:		return GetAttrTypeForCriticalDamage( ability.m_iParam[0] );

	case ABILITY_LOWHP_ATTACKPOWER_CHANGE:		return GetAttrTypeForAttackpower	( (eATTACK_KIND)ability.m_iOption1 );
	case ABILITY_LOWHP_DEFENSE_CHANGE:				return GetAttrTypeForDefence		( (eATTACK_KIND)ability.m_iOption1 );

	default:									return ATTRIBUTE_TYPE_INVALID;
	}
}

eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForAttackpower(  eATTACK_KIND attackType )
{
	switch( attackType )
	{
	case ATTACKKIND_ALL_OPTION:			return ATTRIBUTE_OPTION_ALL_ATTACK_POWER;
	case ATTACKKIND_MELEE:				return ATTRIBUTE_BASE_MELEE_MIN_ATTACK_POWER;
	case ATTACKKIND_RANGE:				return ATTRIBUTE_BASE_RANGE_MIN_ATTACK_POWER;
	case ATTACKKIND_TRAP:					return ATTRIBUTE_BASE_TRAP_MIN_ATTACK_POWER;
	case ATTACKKIND_PHY4:					return ATTRIBUTE_BASE_PHY4_MIN_ATTACK_POWER;
	case ATTACKKIND_PHY5:					return ATTRIBUTE_BASE_PHY5_MIN_ATTACK_POWER;

	case ATTACKKIND_GOLD:					return ATTRIBUTE_MAGICAL_GOLD_ATTACK_POWER;
	case ATTACKKIND_WATER:				return ATTRIBUTE_MAGICAL_WATER_ATTACK_POWER;
	case ATTACKKIND_WOOD:					return ATTRIBUTE_MAGICAL_WOOD_ATTACK_POWER;
	case ATTACKKIND_FIRE:					return ATTRIBUTE_MAGICAL_FIRE_ATTACK_POWER;
	case ATTACKKIND_EARTH:				return ATTRIBUTE_MAGICAL_EARTH_ATTACK_POWER;
	case ATTACKKIND_WIND:					return ATTRIBUTE_MAGICAL_WIND_ATTACK_POWER;
	case ATTACKKIND_DIVINE:				return ATTRIBUTE_MAGICAL_DIVINE_ATTACK_POWER;
	case ATTACKKIND_DARKNESS:			return ATTRIBUTE_MAGICAL_DARKNESS_ATTACK_POWER;

	case ATTACKKIND_PHYSICAL_OPTION:	return ATTRIBUTE_OPTION_PHYSICAL_ATTACK_POWER;
	case ATTACKKIND_MAGIC_OPTION:		return ATTRIBUTE_OPTION_MAGICAL_ATTACK_POWER;
	case ATTACKKIND_ALL_MAGIC:			return ATTRIBUTE_MAGICAL_ALL_ATTACK_POWER;
	default:							
		return ATTRIBUTE_BASE_MELEE_MIN_ATTACK_POWER;
	}
}



eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForDefence(  eATTACK_KIND attackType )
{
	switch( attackType )
	{
	case ATTACKKIND_ALL_OPTION:			return ATTRIBUTE_OPTION_ALL_DEFENSE_POWER;
	case ATTACKKIND_MELEE:				return ATTRIBUTE_BASE_MELEE_DEFENSE_POWER;
	case ATTACKKIND_RANGE:				return ATTRIBUTE_BASE_RANGE_DEFENSE_POWER;
	case ATTACKKIND_TRAP:					return ATTRIBUTE_BASE_TRAP_DEFENSE_POWER;
	case ATTACKKIND_PHY4:					return ATTRIBUTE_BASE_PHY4_DEFENSE_POWER;
	case ATTACKKIND_PHY5:					return ATTRIBUTE_BASE_PHY5_DEFENSE_POWER;

	case ATTACKKIND_GOLD:					return ATTRIBUTE_MAGICAL_GOLD_DEFENSE_POWER;
	case ATTACKKIND_WATER:				return ATTRIBUTE_MAGICAL_WATER_DEFENSE_POWER;
	case ATTACKKIND_WOOD:					return ATTRIBUTE_MAGICAL_WOOD_DEFENSE_POWER;
	case ATTACKKIND_FIRE:					return ATTRIBUTE_MAGICAL_FIRE_DEFENSE_POWER;
	case ATTACKKIND_EARTH:				return ATTRIBUTE_MAGICAL_EARTH_DEFENSE_POWER;
	case ATTACKKIND_WIND:					return ATTRIBUTE_MAGICAL_WIND_DEFENSE_POWER;
	case ATTACKKIND_DIVINE:				return ATTRIBUTE_MAGICAL_DIVINE_DEFENSE_POWER;
	case ATTACKKIND_DARKNESS:			return ATTRIBUTE_MAGICAL_DARKNESS_DEFENSE_POWER;

	case ATTACKKIND_PHYSICAL_OPTION:	return ATTRIBUTE_OPTION_PHYSICAL_DEFENSE_POWER;
	case ATTACKKIND_MAGIC_OPTION:		return ATTRIBUTE_OPTION_MAGICAL_DEFENSE_POWER;
	case ATTACKKIND_ALL_MAGIC:			return ATTRIBUTE_MAGICAL_ALL_DEFENSE_POWER;
	default:							
		return ATTRIBUTE_BASE_MELEE_DEFENSE_POWER;
	}
}

eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForBonusDefence(  eATTACK_KIND attackType )
{
	switch( attackType )
	{
	case ATTACKKIND_ALL_OPTION:			return ATTRIBUTE_ADD_ALL_DEFENSE_POWER;
	case ATTACKKIND_MELEE:				return ATTRIBUTE_ADD_MELEE_DEFENSE_POWER;
	case ATTACKKIND_RANGE:				return ATTRIBUTE_ADD_RANGE_DEFENSE_POWER;
	case ATTACKKIND_TRAP:					return ATTRIBUTE_ADD_TRAP_DEFENSE_POWER;
	case ATTACKKIND_PHY4:					return ATTRIBUTE_ADD_PHY4_DEFENSE_POWER;
	case ATTACKKIND_PHY5:					return ATTRIBUTE_ADD_PHY5_DEFENSE_POWER;

	case ATTACKKIND_GOLD:					return ATTRIBUTE_ADD_GOLD_DEFENSE_POWER;
	case ATTACKKIND_WATER:				return ATTRIBUTE_ADD_WATER_DEFENSE_POWER;
	case ATTACKKIND_WOOD:					return ATTRIBUTE_ADD_WOOD_DEFENSE_POWER;
	case ATTACKKIND_FIRE:					return ATTRIBUTE_ADD_FIRE_DEFENSE_POWER;
	case ATTACKKIND_EARTH:				return ATTRIBUTE_ADD_EARTH_DEFENSE_POWER;
	case ATTACKKIND_WIND:					return ATTRIBUTE_ADD_WIND_DEFENSE_POWER;
	case ATTACKKIND_DIVINE:				return ATTRIBUTE_ADD_DIVINE_DEFENSE_POWER;
	case ATTACKKIND_DARKNESS:			return ATTRIBUTE_ADD_DARKNESS_DEFENSE_POWER;

	case ATTACKKIND_PHYSICAL_OPTION:	return ATTRIBUTE_ADD_PHYSICAL_DEFENSE_POWER;
	case ATTACKKIND_MAGIC_OPTION:		return ATTRIBUTE_ADD_MAGICAL_DEFENSE_POWER;
	case ATTACKKIND_ALL_MAGIC:			return ATTRIBUTE_ADD_MAGICAL_ALL_DEFENSE_POWER;
	default:							
		return ATTRIBUTE_ADD_MELEE_DEFENSE_POWER;
	}
}

eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForStat(  eBASE_ATTRIBTUE statType )
{
	switch( statType )
	{
	case eBASE_ATTR_STR:				return ATTRIBUTE_STR;
	case eBASE_ATTR_DEX:				return ATTRIBUTE_DEX;
	case eBASE_ATTR_VIT:				return ATTRIBUTE_VIT;
	case eBASE_ATTR_SPR:				return ATTRIBUTE_SPR;
	case eBASE_ATTR_INT:				return ATTRIBUTE_INT;
	case eBASE_ATTR_LRN:				return ATTRIBUTE_LRN;
	case eBASE_ATTR_CRE:				return ATTRIBUTE_CRE;
	default:							
		return ATTRIBUTE_STR;
	}
}

eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForBonusDamage(  eARMOR_TYPE armorType, int iValueType )
{
	if( iValueType == VALUE_TYPE_VALUE )
	{
		switch( armorType )
		{
		case eARMOR_HARD:					return ATTRIBUTE_ADD_ARMOR_HARD_DAMAGE;
		case eARMOR_MEDIUM:				return ATTRIBUTE_ADD_ARMOR_MEDIUM_DAMAGE;
		case eARMOR_SOFT:					return ATTRIBUTE_ADD_ARMOR_SOFT_DAMAGE;
		case eARMOR_SIEGE:				return ATTRIBUTE_ADD_ARMOR_SIEGE_DAMAGE;
		case eARMOR_UNARMOR:				return ATTRIBUTE_ADD_ARMOR_UNARMOR_DAMAGE;
		default:								return ATTRIBUTE_ADD_ARMOR_HARD_DAMAGE;
		}
	}
	else if( iValueType == VALUE_TYPE_RATIO_VALUE )
	{
		switch( armorType )
		{
		case eARMOR_HARD:					return ATTRIBUTE_ADD_RATIO_ARMOR_HARD_DAMAGE;
		case eARMOR_MEDIUM:				return ATTRIBUTE_ADD_RATIO_ARMOR_MEDIUM_DAMAGE;
		case eARMOR_SOFT:					return ATTRIBUTE_ADD_RATIO_ARMOR_SOFT_DAMAGE;
		case eARMOR_SIEGE:				return ATTRIBUTE_ADD_RATIO_ARMOR_SIEGE_DAMAGE;
		case eARMOR_UNARMOR:				return ATTRIBUTE_ADD_RATIO_ARMOR_UNARMOR_DAMAGE;
		default:								return ATTRIBUTE_ADD_RATIO_ARMOR_HARD_DAMAGE;
		}
	}
	else
	{
		DISPMSG( "sABILITYINFO_BASE : [GetAttrTypeForBonusDamage] Invalid ValueType[%d]! \n", iValueType );
		return ATTRIBUTE_ADD_ARMOR_HARD_DAMAGE;
	}
}

eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForReduceDamage(  eATTACK_KIND attackType )
{
	switch( attackType )
	{
	case ATTACKKIND_ALL_OPTION:			return ATTRIBUTE_DEL_ALL_DAMAGE;
	case ATTACKKIND_MELEE:				return ATTRIBUTE_DEL_MELEE_DAMAGE;
	case ATTACKKIND_RANGE:				return ATTRIBUTE_DEL_RANGE_DAMAGE;
	case ATTACKKIND_TRAP:					return ATTRIBUTE_DEL_TRAP_DAMAGE;
	case ATTACKKIND_PHY4:					return ATTRIBUTE_DEL_PHY4_DAMAGE;
	case ATTACKKIND_PHY5:					return ATTRIBUTE_DEL_PHY5_DAMAGE;

	case ATTACKKIND_GOLD:					return ATTRIBUTE_DEL_GOLD_DAMAGE;
	case ATTACKKIND_WATER:				return ATTRIBUTE_DEL_WATER_DAMAGE;
	case ATTACKKIND_WOOD:					return ATTRIBUTE_DEL_WOOD_DAMAGE;
	case ATTACKKIND_FIRE:					return ATTRIBUTE_DEL_FIRE_DAMAGE;
	case ATTACKKIND_EARTH:				return ATTRIBUTE_DEL_EARTH_DAMAGE;
	case ATTACKKIND_WIND:					return ATTRIBUTE_DEL_WIND_DAMAGE;
	case ATTACKKIND_DIVINE:				return ATTRIBUTE_DEL_DIVINE_DAMAGE;
	case ATTACKKIND_DARKNESS:			return ATTRIBUTE_DEL_DARKNESS_DAMAGE;

	case ATTACKKIND_PHYSICAL_OPTION:	return ATTRIBUTE_DEL_PHYSICAL_DAMAGE;
	case ATTACKKIND_MAGIC_OPTION:		return ATTRIBUTE_DEL_MAGICAL_DAMAGE;
	case ATTACKKIND_ALL_MAGIC:			return ATTRIBUTE_DEL_MAGICAL_ALL_DAMAGE;
	default:							
		return ATTRIBUTE_DEL_MELEE_DAMAGE;
	}
}

eATTRIBUTE_TYPE SkillInfoParserInstance::GetAttrTypeForCriticalDamage(  int ValueType )
{
	if( ValueType == VALUE_TYPE_VALUE )					
		return ATTRIBUTE_ADD_CRITICAL_DAMAGE;
	else
		return ATTRIBUTE_ADD_CRITICAL_DAMAGE_RATIO;
}


void	SkillInfoParserInstance::GetDamageText	(StringHandle&        sText
															,sABILITYINFO_BASE* pAbility
															,LPCTSTR            szTipFormat
															,DWORD					dwAttackKind)
{
	int					iValueType  = pAbility->m_iParam[0];
	float					fValue		= (float)pAbility->m_iParam[1];
	LPCTSTR				szPercent	= _T("");
	sSERIES_RELATION*	pInfo			= NULL;
	

	switch(iValueType)
	{
	case VALUE_TYPE_PERCENT_PER_MAX:
	case VALUE_TYPE_PERCENT_PER_CUR:	
		fValue		= fValue / 1000.0f;	
		szPercent	= _T("%");
		break;
	}

	if(dwAttackKind == INVALID_DWORD_ID)
	{
		// [%s]- %2.2f%% 
		sText.Format(szTipFormat
						,_STRING(iValueType + TEXTRES_VALUE_TYPE_VALUE -1)
						,fValue
						,szPercent);
	}
	else
	{
		pInfo = theSeriesInfoParser.GetSeriesInfo	((eATTACK_KIND)dwAttackKind);
		// [%s][%s]- %2.2f%% 
		sText.Format(szTipFormat
						,pInfo?(LPCTSTR)pInfo->m_Name : _T("")
						,_STRING(iValueType + VALUE_TYPE_VALUE -1)
						,fValue
						,szPercent);
	}
}

void SkillInfoParserInstance::GetAbilityDesc			(StringHandle&       sResult
																	,sABILITYINFO_BASE*	pAbility
																	,LPCTSTR					szContentFormat)
{
	__CHECK2_PTR(pAbility,);

	LPCTSTR					szName;
	LPCTSTR					szTipFormat;
	LPCTSTR					szStateFormat;
	StringHandle			sText;
	StringHandle			sState;
	sSERIES_RELATION*			pInfo;
	sSTATEINFO_BASE*			pState(NULL);
	sABILITY_OPTION_INFO*	pOption;
	DWORD							dwStateID(0);

	if(szContentFormat== NULL || szContentFormat[0] == 0)
		szContentFormat	= _T("%s [%d%%,%s] %s %s");


	//pDetail->SetFirst();
	////////////////////////////////////////////
	//for(; ; )
	//{
		//pAbility	= pDetail->GetNext();
		//if(!pAbility)
		//	break;

	if(pAbility->m_wAbilityID == 0)
		return;

	pOption		= theSkillInfoParser.GetAbilityOption(pAbility->m_wAbilityID);
	if(!pOption)
		return;

	szName		= (LPCTSTR)pOption->m_AbilityName;
	szTipFormat	= (LPCTSTR)pOption->m_TipFormat;
	dwStateID	= pAbility->m_wStateID;

	/////////////////////////////////////////
	//效力处理
	switch(pAbility->m_wAbilityID)
	{
	/////////////////////////////////////////
	case ABILITY_RESURRECTION:
		{
			float fRecoverExp = pAbility->m_iOption2 / 1000.0f;
			float fRecoverHP  = pAbility->m_iParam[0] / 1000.0f;
			float fRecoverMP  = pAbility->m_iParam[1] / 1000.0f;

			// 经验=%2.1f HP=%2.1f MP=%2.1f
			sText.Format(szTipFormat
							,fRecoverExp
							,fRecoverHP 
							,fRecoverMP );
		}break;

	/////////////////////////////////////////
	case ABILITY_BONUS_DAMAGE_PER_SP:
		{
			//伤害力+%d 伤害比+%2d%%
			sText.Format(szTipFormat
							,pAbility->m_iParam[0]
							,pAbility->m_iParam[1] );
		}break;

	case ABILITY_BONUS_DAMAGE_PER_FIGHTING_ENERGY:
		{
			int iDecreseCount = pAbility->m_iOption2;
			//伤害力+%d%s 伤害比+%2.2f%%
			sText.Format(szTipFormat
							,pAbility->m_iParam[0]
							,iDecreseCount?_T("") :_T("%") 
							,pAbility->m_iParam[1] / 1000.0f);
		}break;

	/////////////////////////////////////////
	case ABILITY_BUFF_RANGE_DAMAGE:
		{
			if( pAbility->m_iOption2 == 3 )
			{
				eATTACK_KIND	eAttackType			= (eATTACK_KIND)pAbility->m_iOption1;
				float				fRadius				= pAbility->m_iParam[0] / 10.f;
				int				iDamage				= pAbility->m_iParam[1];
				int				iApplicationTime	= pAbility->m_iParam[2];
				int				iPeriodicTime		= pAbility->m_iParam[3];

				pInfo		= theSeriesInfoParser.GetSeriesInfo	(eAttackType);
				// [%s]伤害力+%d 范围=%2.1f 有效时间=%dms 周期=%dms
				sText.Format(szTipFormat
								,pInfo?(LPCTSTR)pInfo->m_Name : _T("")
								,fRadius				
								,iDamage				
								,iApplicationTime	
								,iPeriodicTime		);
			}
		}
		break;

	case ABILITY_WEAPON_MASTERY:
		{
			int nSetValue	= pAbility->m_iParam[0];
			int nSetValue2	= pAbility->m_iParam[1];
			// 攻击 +%d (+%d)
			sText.Format	(szTipFormat
								,nSetValue	
								,nSetValue2);
		}break;

	/////////////////////////////////////////
	case ABILITY_BONUS_DAMAGE_PER_STATUS:
	case ABILITY_DAMAGE:
		{
			pInfo		= theSeriesInfoParser.GetSeriesInfo	((eATTACK_KIND)pAbility->m_iOption1);
			// [%s]SkillAttackPower- %d (+%2.2f%%) 
			sText.Format(szTipFormat
							,pInfo?(LPCTSTR)pInfo->m_Name : _T("")
							,pAbility->m_iParam[0]
							,pAbility->m_iParam[1] / 1000.0f);
		}
		break;

	/////////////////////////////////////////
	case ABILITY_DAMAGE_PER_TIME:
			dwStateID	= CHARSTATE_PERIODIC_DAMAGE;
		break;

	/////////////////////////////////////////
	case ABILITY_KNOCKBACK:
		{
			dwStateID	= CHARSTATE_THRUST;
			// Length- %2.1f 
			sText.Format(szTipFormat
							,(float)pAbility->m_iParam[1] / 10.0f);
		}break;

	/////////////////////////////////////////
	case ABILITY_FIGHTING_ENERGY_NUM_INCREASE:
	case ABILITY_AGGROPOINT_INCREASE:
		{
			// %d 
			sText.Format(szTipFormat
							,pAbility->m_iParam[1]);
		}break;

	/////////////////////////////////////////
	case ABILITY_TARGET_CHANGE:
		{
			// %2.1f%% 
			sText.Format(szTipFormat
							,(float)pAbility->m_iParam[1]/10.f);
		}break;

	/////////////////////////////////////////
		case ABILITY_EXHAUST_HP:
		case ABILITY_EXHAUST_MP:
		case ABILITY_ATTACK_DAMAGE_HP_ABSORPTION:
		case ABILITY_ATTACK_DAMAGE_MP_ABSORPTION:
		case ABILITY_ATTACKED_DAMAGE_HP_ABSORPTION:
		case ABILITY_ATTACKED_DAMAGE_MP_ABSORPTION:
		{
			GetDamageText(sText,pAbility,szTipFormat);
		}break;

	/////////////////////////////////////////
	case ABILITY_MAX_HP_INCREASE:					
	case ABILITY_CUR_HP_INCREASE:					
	case ABILITY_RECOVER_HP_INCREASE:			

	case ABILITY_MAX_MP_INCREASE:					
	case ABILITY_CUR_MP_INCREASE:					
	case ABILITY_RECOVER_MP_INCREASE:			

	case ABILITY_PHYSICAL_ATTACKPOWER_INCREASE:
	case ABILITY_PHYSICAL_ATTACKRATE_INCREASE:	
	case ABILITY_PHYSICAL_AVOIDRATE_INCREASE:		

	case ABILITY_MOVE_SPEED_INCREASE:				
	case ABILITY_PHYSICAL_SPEED_INCREASE:			
	case ABILITY_MAGIC_SPEED_INCREASE:				
	case ABILITY_SKILLRANGE_INCREASE:			

	case ABILITY_CRITICAL_RATIO_INCREASE:		
		{
			GetDamageText(sText,pAbility,szTipFormat);
		}break;


		//////////////////////////////////////////////////////////
	case ABILITY_CRITICAL_DAMAGE_INCREASE:	//		return GetAttrTypeForCriticalDamage( ability.m_iParam[0] );
	case ABILITY_ATTACKPOWER_BY_ARMORTYPE:	//		return GetAttrTypeForBonusDamage( (eARMOR_TYPE)ability.m_iOption1, ability.m_iParam[0] );
	case ABILITY_MAGIC_ATTACKPOWER_INCREASE:	

	case ABILITY_PHYSICAL_DEFENSE_INCREASE:
	case ABILITY_MAGIC_DEFENSE_INCREASE:	
	case ABILITY_DEFENSE_BY_ATTACKTYPE:		

	case ABILITY_STAT_INCREASE:				

	case ABILITY_DAMAGE_DECREASE:				
	case ABILITY_LOWHP_ATTACKPOWER_CHANGE:	
	case ABILITY_LOWHP_DEFENSE_CHANGE:	
		{
			//option1
			GetDamageText(sText,pAbility,szTipFormat);
		}break;

	case ABILITY_SUMMON:
	    {
			sNPCINFO_BASE*	pNpcInfo = theNPCInfoParser.GetMonsterInfo(pAbility->m_iOption2);
			if(pNpcInfo)
			{
				float fTime  = (float)pAbility->m_iParam[2] / 1000.0f;
				// %s[%d只,生命%g秒]
				sText.Format(szTipFormat
								,(LPCTSTR)pNpcInfo->m_NpcName
								,pAbility->m_iParam[0] 
								,fTime );
			}
	    }break;	

	/////////////////////////////////////////
	default:
		sText = _T("");
		break;
	}

	////////////////////////////////////////
	//状态处理
	pState			= theStateInfoParser.GetStateInfo	(dwStateID);
	szStateFormat	= pState?(LPCTSTR)pState->m_sTipFormat : _T("");
	switch(dwStateID)
	{
	case CHARSTATE_BLIND:
		{
			sState.Format(szStateFormat);
		}break;

	case CHARSTATE_MAGIC_SHIELD:
		{
			int	iShieldHP		= pAbility->m_iParam[1];
			int	iDecreaseMP		= pAbility->m_iOption2;
			float fAbsorbRatio	= pAbility->m_iParam[0] / 1000.f;
			// HP消耗减少%d MP消耗%d 吸伤率%2.1f%%
			sState.Format	(szStateFormat
								,iShieldHP	
								,iDecreaseMP	
								,fAbsorbRatio);
		}break;

	case CHARSTATE_SP_BONUS:
		{
			int nSetValue	= pAbility->m_iParam[0];
			int nValuePer	= pAbility->m_iParam[1];
			// 攻击 +%d (+%d%%)
			sState.Format	(szStateFormat
								,nSetValue	
								,nValuePer);
		}break;

	case CHARSTATE_STUN:
	case CHARSTATE_DOWN:
	case CHARSTATE_DELAY:	
	case CHARSTATE_FROZEN:
		{
			// 时间%dms
			sState.Format	(szStateFormat
								,pAbility->m_iParam[2]);
		}break;


	case CHARSTATE_REFLECT_DAMAGE:	
	case CHARSTATE_REFLECT_SLOW:
	case CHARSTATE_REFLECT_FROZEN:
	case CHARSTATE_REFLECT_SLOWDOWN:
	case CHARSTATE_REFLECT_STUN:	
	case CHARSTATE_REFLECT_FEAR:
		{
			float fAbsorbRatio = pAbility->m_iOption1 / 1000.0f;
			//m_iOption2 = pAbility->m_iOption2;
			// [%s][%s]伤害力+%d%s;反伤率%2.1f%%
			ScriptWord		words(szStateFormat,';');
			StringHandle	sText2;
			GetDamageText(sState,pAbility,words[0]);
			sText2.Format	(words[1]
								,fAbsorbRatio);
			sState += sText2;
		}break;

	case CHARSTATE_STAT_LOWHP_ATTACK_DECREASE:
	case CHARSTATE_STAT_LOWHP_DEFENSE_DECREASE:
	case CHARSTATE_STAT_LOWHP_ATTACK_INCREASE:	
	case CHARSTATE_STAT_LOWHP_DEFENSE_INCREASE:
		{
			GetDamageText(sState,pAbility,szStateFormat,pAbility->m_iOption1);
		}break;

	case CHARSTATE_STAT_DAMAGE_ADD:
		{
			GetDamageText(sState,pAbility,szStateFormat);
		}break;

	case CHARSTATE_CURE:
		{
			sSTATEINFO_BASE*	arSubStates[3];
			arSubStates[0] = theStateInfoParser.GetStateInfo(pAbility->m_iOption2 );
			arSubStates[1] = theStateInfoParser.GetStateInfo(pAbility->m_iParam[0]);
			arSubStates[2] = theStateInfoParser.GetStateInfo(pAbility->m_iParam[1]);
			//解除状态 %s %s %s
			sState.Format(szStateFormat
							,arSubStates[0]?(LPCTSTR)arSubStates[0]->m_sStateName : _T("")
							,arSubStates[1]?(LPCTSTR)arSubStates[1]->m_sStateName : _T("")
							,arSubStates[2]?(LPCTSTR)arSubStates[2]->m_sStateName : _T("")
							);
		}break;

	case CHARSTATE_POISON:
	case CHARSTATE_WOUND:
	case CHARSTATE_FIRE_WOUND:
	case CHARSTATE_PERIODIC_DAMAGE:
		{
			pInfo		= theSeriesInfoParser.GetSeriesInfo	((eATTACK_KIND)pAbility->m_iOption1);
			// [%s]PERIODIC_DAMAGE+ %d
			sState.Format(szStateFormat
							,pInfo?(LPCTSTR)pInfo->m_Name : _T("")
							,pAbility->m_iParam[1]);
		}break;

	//case CHARSTATE_THRUST:

	default:
		sState = _T("");
		break;
	}

	////////////////////////////////////////
	sText += sState;


	////////////////////////////////////////
	StringHandle	sStateName;
	if(pState)
		sStateName.Format(_T("<%s>"), (LPCTSTR)pState->m_sStateName);

	sResult	= FMSTR	(szContentFormat
							,szName
							,pAbility->m_wSuccessRate/10
							,GetSkillAbilityRangeLabel(pAbility->m_byRangeType)
							,(LPCTSTR)sText
							,(LPCTSTR)sStateName);

}

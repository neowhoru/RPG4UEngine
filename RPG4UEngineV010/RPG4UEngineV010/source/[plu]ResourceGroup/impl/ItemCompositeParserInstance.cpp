/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemCompositeParserInstance.cpp
创建日期：2008年4月15日
最后更新：2008年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemCompositeParserInstance.h"
#include "StructBase.h"
#include "TableTextFile.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemCompositeParserInstance, ()  , gamemain::eInstPrioGameFunc);


//----------------------------------------------------------------------------
ItemCompositeParserInstance::ItemCompositeParserInstance()
{
}


//----------------------------------------------------------------------------
ItemCompositeParserInstance::~ItemCompositeParserInstance()
{
}

//
//BOOL ItemCompositeParserInstance::Load( LPCSTR pszFileName, LPCSTR pszResultFileName, BOOL bReload /*= FALSE */)
//{
//	//ASSERT( m_pCompositeInfoHashTable != NULL );
//
//	_LoadCompositeResult( pszResultFileName, bReload );
//	
//
//	util::TableTextFile sr;
//	__CHECK(sr.OpenFile( pszFileName, TRUE) );
//
//	if( !bReload )
//		 m_FileName = (pszFileName);
//
//
//	SLOTCODE							Code;
//	sITEMCOMPOSITE *				pInfo;
//	sITEMCOMPOSITE_GROUP*		pGroup;
//	sITEMCOMPOSITE_SUBGROUP*	pSubGroup;
//
//	for(;sr.GotoNextLine();)
//	{
//		sr.GetInt();
//		sr >> Code;
//
//		pInfo = (sITEMCOMPOSITE*)GetDataUnit( Code );
//		if( !pInfo ) 
//		{
//			pInfo = AllocUnit();//new sITEMCOMPOSITE;
//			pInfo->m_Code = Code;
//			_Add( pInfo);
//		}
//		else
//		{
//			assert(bReload);
//		}
//		pInfo->m_Code = Code;
//
//		sr >> pInfo->m_sName;
//		sr >> pInfo->m_sGroup;
//		sr >> pInfo->m_sSubGroup;
//
//
//		sr >> pInfo->m_vItemID;
//		sr >> pInfo->m_ResultCode;
//
//		if (0 != pInfo->m_ResultCode) 
//		{
//			ASSERT( GetCompositeResultInfo(pInfo->m_ResultCode) && "not found result...");
//		}	
//
//		sr >> pInfo->m_byMatCount;
//		
//		for (int i= 0; i < sITEMCOMPOSITE::MAX_COMPOSITE_MATERIAL; i++)
//		{
//			sr >>	pInfo->m_sCompositeMaterials[i].m_dwCompositeMaterialCode;
//			sr	>> pInfo->m_sCompositeMaterials[i].m_bySpendMaterialNum;
//		}
//		
//		sr >> pInfo->m_Money;
//
//		//if(!theGlobalString.IsBlank(pInfo->m_sGroup))
//		if(!StringManager::IsBlank(pInfo->m_sGroup))
//		{
//			///加到群组信息中....
//			//m_pCompositeGroupTable
//			pGroup = m_pCompositeGroupTable->GetData((DWORD)pInfo->m_sGroup);
//			if(pGroup == NULL)
//			{
//				pGroup = AllocGroup();//new sITEMCOMPOSITE_GROUP;
//				pGroup->sName	= pInfo->m_sGroup;
//				m_pCompositeGroupTable->Add( pGroup, (DWORD)pInfo->m_sGroup );
//				pGroup->subGroupTable.Initialize(m_dwCompositeGroupSize);
//			}
//
//			pSubGroup = pGroup->subGroupTable.GetData((DWORD)pInfo->m_sSubGroup);
//			if(pSubGroup == NULL)
//			{
//				pSubGroup = AllocSubGroup();//new sITEMCOMPOSITE_SUBGROUP;
//				pSubGroup->sName	= pInfo->m_sSubGroup;
//				pGroup->subGroupTable.Add( pSubGroup, (DWORD)pInfo->m_sSubGroup );
//				pSubGroup->itemCompositeTable.Initialize(m_dwCompositeInfoSize);
//			}
//
//			pSubGroup->itemCompositeTable.Add(pInfo, Code);
//		}
//
//
//	}
//	sr.CloseFile();
//	
//
//	return TRUE;
//}
//
//
//BOOL ItemCompositeParserInstance::_LoadCompositeResult( LPCSTR pszResultFileName, BOOL bReload ) 
//{
//
//	util::TableTextFile sr;
//
//	__CHECK(sr.OpenFile( pszResultFileName, TRUE) );
//
//	if( !bReload ) 
//		 m_sResultFileName = _VI(pszResultFileName);
//
//
//	CODETYPE						ResultKey;
//	sITEMCOMPOSITERESULT *	pInfo;
//
//	for(;sr.GotoNextLine();)
//	{
//				sr.GetInt();//
//		sr >> ResultKey;
//
//		pInfo = GetCompositeResultInfo( ResultKey );
//		if( !pInfo ) 
//		{
//			pInfo = new sITEMCOMPOSITERESULT;
//			pInfo->m_ResultCode = ResultKey;
//			_Add( pInfo );
//		}
//		else
//		{
//			assert(bReload);
//		}
//
//		pInfo->m_ResultCode = ResultKey;
//			sr.GetString();///注释
//		sr >> pInfo->m_MainResult;
//		sr >> pInfo->m_byMainRate;
//
//		if(pInfo->m_MainResult != 0)
//		{
//				sr(sITEMCOMPOSITERESULT::MAX_COMPOSITE_RESULT_NUM);
//			sr >> pInfo->m_ResultItemCode;
//				sr(0);
//
//			///数据校验
//			for (int i= 0; i < sITEMCOMPOSITERESULT::MAX_COMPOSITE_RESULT_NUM; i++)
//			{
//				if(pInfo->m_ResultItemCode[i] == 0)
//				{
//					LPCSTR szText = FMSTR("ItemCompositeParser::_LoadCompositeResult %d m_ResultItemCode Is Null\n",ResultKey);
//					LOGINFO(szText);
//					MessageBox(g_hWndMain,szText,"",MB_OK);
//				}
//			}
//		}
//		else
//		{
//				sr(SOCKET_MAX);
//			sr >> pInfo->SOCKET.m_arDamageRates;
//			sr >> pInfo->SOCKET.m_arRetainRates;
//				sr(0);
//		}
//	}
//	sr.CloseFile();
//
//	return TRUE;
//}

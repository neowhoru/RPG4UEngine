/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManager.h
创建日期：2008年6月19日
最后更新：2008年6月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TESTINGITEMMANAGER_H__
#define __TESTINGITEMMANAGER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "ResultCode.h"
#include "ItemManagerDeclare.h"

using namespace RC;
using namespace std;

class ItemSlotContainer;
class BaseContainer;
class ItemSlot;
struct sITEMMAKELEVEL;
_NAMESPACEU(Hero,object);
_NAMESPACEU(Player,object);

namespace testing
{
struct COMPOSITEMATERIAL
{
	int		nMaterialCode;
	WORD		wCntMaterial;
};

class GameLog 
{
public:
	void WriteEnchantStart( Player* /*pPlayer*/, int /*nServerCode*/, vector<COMPOSITEMATERIAL> /*vecMatInfo*/ ){};
	void WriteEnchantResult( Player* /*pPlayer*/, int /*nServerCode*/, BOOL /*bSuccess*/, ItemSlot* /*pItemSlot*/, vector<COMPOSITEMATERIAL> /*vecMatInfo*/ ){}
	DWORD GetServerGUID(){return 0;}
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class EmptyItemCheck
{
	CODETYPE m_code;
	SLOTPOS  m_max_num;
public:
	EmptyItemCheck(CODETYPE code, SLOTPOS max_num):
	  m_max_num(max_num),m_code(code){}
	  ~EmptyItemCheck(){}

	  BOOL operator()( BaseContainer * /*pContainer*/, BaseSlot & /*slotDat*/ )
	  {
		  return TRUE;
	  }
};

class DeleteItem
{
	CODETYPE m_code;
	SLOTPOS  m_max_num;
public:
	DeleteItem(CODETYPE code, SLOTPOS max_num):
	  m_max_num(max_num),m_code(code){}
	  ~DeleteItem(){}

	BOOL operator()( BaseContainer * /*pContainer*/, BaseSlot & /*slotDat*/ )
	{
		return TRUE;
	}
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ItemManagerTesting : public ItemManager
{
public:
	BOOL ValidPos( SLOTINDEX atIndex, SLOTPOS atPos, BOOL bEmptyCheck = TRUE);
	ItemSlotContainer * GetItemSlotContainer( SLOTINDEX Index );

	BOOL						IsPossibleFunction( eITEM_FUNCTIONAL_TYPE /*type*/ ) { return TRUE; }
	BOOL						ValidState(){return TRUE;}

	RC::eITEM_RESULT		MakeItem			(SLOTCODE					destItemCode
                                       ,DWORD						dwMaterialNums
													,BYTE							byMaterialRow
													,SLOTPOS*					parMaterialPos
													,LPCSTR						szItemName
													,sTOTALINFO_INVENTORY&	resultItems);

	RC::eITEM_RESULT		Enchant			(SLOTPOS			TargetPos
                                       ,BYTE & OUT		ResultEnchant)		;
	RC::eITEM_RESULT		EnchantEx		(SLOTPOS			TargetPos
                                       ,BYTE				SuccessRateIndex
													,BYTE & OUT		ResultEnchant);

	RC::eITEM_RESULT		ExtractSocket	(MATERIALTYPE               Type
                                       ,SLOTPOS                    TargetPos
													,DWORD							 dwSocketState
													,DWORD&							 dwSocketResult
													,sTOTALINFO_INVENTORY * OUT pTotalInfo);

	RC::eITEM_RESULT		FillSocket(MATERIALTYPE  Type,SLOTPOS SocketItemPos[], SLOTPOS TargetPos );




	RC::eITEM_RESULT		Lose( SLOTCODE ItemCode, BOOL bAllOfItem );
	VOID						Lose( BaseContainer * pContainer, SLOTPOS pos, BYTE num );
	VOID						Lose( SLOTINDEX slotIdx, SLOTPOS pos, BYTE num );

	RC::eITEM_RESULT		Obtain( SLOTCODE ItemCode, SLOTPOS num, sTOTALINFO_INVENTORY * OUT pTotalInfo );
	RC::eITEM_RESULT		Obtain( ItemSlot & IN slot, SLOTPOS num, sTOTALINFO_INVENTORY * OUT pTotalInfo );

	BOOL						RegenItem	(ItemSlot&		itemSlot
												,sITEMMAKELEVEL*	pMakeLevel
												,DWORD				dwRegenNum=0
												,DWORD				dwOppressNum=0);



	VOID					CanInsertItem( BaseContainer * pContainer, SLOTPOS MAX_DUP_NUM, SLOTPOS & INOUT need_num, SLOTPOS * OUT PosArray, SLOTPOS & OUT PosNum );


	BOOL					IsExistItemDup	(SLOTINDEX         idx
                                    ,SLOTCODE        code
												,BYTE            num
												,SLOTPOS * OUT   pPosArray
												,SLOTPOS & INOUT posNum);

	BOOL					IsExistItem		(SLOTINDEX         idx
                                    ,SLOTCODE        code
												,DURATYPE        dur
												,SLOTPOS * OUT   pPosArray
												,SLOTPOS * INOUT pPosNum);
												//,SLOTPOS & INOUT posNum);

	BOOL					IsExistItem( SLOTINDEX Index, SLOTCODE code );


public:
	template <class FUNCTOR>
	BOOL ForeachSlot( SLOTINDEX /*idx*/, FUNCTOR & /*opr*/ )
	{
		return TRUE;
	}

public:
	VG_PTR_PROPERTY(Player, Player);
};


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//GLOBALINST_SINGLETON_DECL(GameLog , API_NULL);
GLOBALINST_SINGLETON_DECL2(testing::GameLog , GameLog , API_NULL);
#define GAMELOG			(&singleton::GetGameLog())
#define g_pGameServer	(&singleton::GetGameLog())
#define theGameLog		singleton::GetGameLog()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(testing::ItemManagerTesting , TestingItemManager , API_NULL);
//extern API_NULL ItemManager& singleton::GetItemManager();
#define theTestingItemManager  singleton::GetTestingItemManager()
//#define theTestingItemManager  (*testing::ItemManager::Instance())


#endif //__GAMEINPLAYINGTESTINGITEMMANAGER_H__
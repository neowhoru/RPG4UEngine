/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenLocationInstance.h
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	怪物生成点
		1.记录当前点生成alive怪数量
		2.记录当前点怪物群
		3.记录当前点区域索引

版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENLOCATIONINSTANCE_H__
#define __REGENLOCATIONINSTANCE_H__
#pragma once

#include "CommonDefine.h"
#include "RegenLocation.h"

struct sREGEN_INFO;
class ObjectGroup;
class NPC;

class RegenLocationInstance : public RegenLocation
{
public:
	RegenLocationInstance();
	~RegenLocationInstance();

public:
	VOID				Init				(sREGEN_INFO* pRegenInfo
                                 ,DWORD       dwAreaKey=0)		;
	VOID				Release			();

	//活动NPC相关操作
	VOID				OnCreateNPC		(DWORD dwNPCCode, DWORD dwNum=1 );
	VOID				OnRemoveNPC		(DWORD dwNPCCode, DWORD dwNum=1 );

	DWORD				GetRandomArea	();

	ObjectGroup*	JoinGroup		( NPC* pNPC );

};//class RegenLocationInstance



#endif //__REGENLOCATIONINSTANCE_H__
#pragma once

#include <RatioRandomizer.h>
#include <ConstItem.h>

struct SOCKET_RATIO
{
	BYTE Ratio[SOCKET_MAX];
};

class RatioManager
{
public:
	RatioManager(void);
	~RatioManager(void);

	VOID Init();
	VOID Release();

	enum { MAX_ENCHANT_SELECTER_NUM = 6 };
	RatioRandomizer m_EnchantRatioSelecter[MAX_ENCHANT_SELECTER_NUM];

	SOCKET_RATIO m_SocketRatio[RANK_5-RANK_8];

};

extern RatioManager g_Ratio;
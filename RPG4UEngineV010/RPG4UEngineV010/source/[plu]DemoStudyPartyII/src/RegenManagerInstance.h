/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenManagerInstance.h
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	怪物生成管理


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENMANAGERINSTANCE_H__
#define __REGENMANAGERINSTANCE_H__
#pragma once


#include "RegenLocationDefine.h"
#include "RegenManager.h"
#include "RegenManagerFactoryManager.h"

class		Field;
struct	sREGEN_INFO;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class RegenManagerInstance : public RegenManager
{
public:
	RegenManagerInstance	(  );
	virtual ~RegenManagerInstance	();

public:
	virtual DWORD		GetFactoryType(){ return REGEN_TYPE_DEFAULT;}

public:
	virtual VOID		Init		(DWORD dwMapCode
                              ,DWORD dwFieldIndex
										,WORD  wMonType
										,DWORD dwRegenPeriod=0);
	virtual VOID		Release	();
	virtual VOID		Update	();
	//virtual void		OnInitRandomRegen();

public:
	virtual RegenLocation*	AllocLocation	();
	virtual void				FreeLocation	(RegenLocation*);

public:
	virtual VOID		CreateNPC(DWORD          dwCode
                              ,DWORD          dwNumberOfNPCs
										,Vector3D*      pvPos
										,RegenLocation* pHomeLocation
										,DWORD          dwGroupID);

	virtual Vector3D		GetRandomPosInTile	(DWORD dwTile);
	virtual ObjectGroup*	FindGroup				(DWORD dwAreaID, DWORD dwGroupID );
	virtual void			OnSendBossRegen		(DWORD dwBossCode,Vector3C& vPos);

protected:
	virtual DWORD		GetRegenAmount	(RegenLocation* pLocation
												,DWORD          dwNPCCode
												,DWORD          dwMaxNum);
	virtual VOID		ProcessRegen	(BOOL				 bSpawnAll=FALSE );

};//class RegenManagerInstance

REGENMANAGER_FACTORY_DECLARE(RegenManagerInstance);

#endif //__REGENMANAGERINSTANCE_H__
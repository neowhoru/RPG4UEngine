#include "StdAfx.h"
#include "RatioManager.h"

RatioManager g_Ratio;

RatioManager::RatioManager(void)
{
}

RatioManager::~RatioManager(void)
{
	Release();
}

VOID RatioManager::Release()
{
}

VOID RatioManager::Init()
{

	for( int i = 0 ; i < MAX_ENCHANT_SELECTER_NUM ; ++i )
		m_EnchantRatioSelecter[i].Clear();

	BOOL rt = m_EnchantRatioSelecter[0].AddRatio( 1, 30, 100 ); ASSERT(rt);
	rt = m_EnchantRatioSelecter[1].AddRatio( 1, 45, 100 ); ASSERT(rt);
	rt = m_EnchantRatioSelecter[2].AddRatio( 1, 60, 100 ); ASSERT(rt);
	rt = m_EnchantRatioSelecter[3].AddRatio( 1, 70, 100 ); ASSERT(rt);
	rt = m_EnchantRatioSelecter[4].AddRatio( 1, 80, 100 ); ASSERT(rt);
	rt = m_EnchantRatioSelecter[5].AddRatio( 1, 90, 100 ); ASSERT(rt);



	m_SocketRatio[RANK_8-RANK_8].Ratio[SOCKET_1] = 70;
	m_SocketRatio[RANK_8-RANK_8].Ratio[SOCKET_2] = 0;
	m_SocketRatio[RANK_8-RANK_8].Ratio[SOCKET_3] = 0;

	m_SocketRatio[RANK_7-RANK_8].Ratio[SOCKET_1] = 70;
	m_SocketRatio[RANK_7-RANK_8].Ratio[SOCKET_2] = 10;
	m_SocketRatio[RANK_7-RANK_8].Ratio[SOCKET_3] = 0;

	m_SocketRatio[RANK_6-RANK_8].Ratio[SOCKET_1] = 70;
	m_SocketRatio[RANK_6-RANK_8].Ratio[SOCKET_2] = 10;
	m_SocketRatio[RANK_6-RANK_8].Ratio[SOCKET_3] = 5;
}
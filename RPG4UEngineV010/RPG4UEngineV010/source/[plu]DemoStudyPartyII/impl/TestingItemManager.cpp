/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManager.cpp
创建日期：2008年6月19日
最后更新：2008年6月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "TestingItemManager.h"
#include "ItemSlotContainer.h"
#include "SocketOptionParser.h"
#include "ItemSlot.h"
#include "ItemCompositeParser.h"
#include "Hero.h"
#include "BattleUtil.h"
#include "EnchantInfoParser.h"
#include "MiscUtil.h"
#include "RatioManager.h"
#include "ObjectManager.h"
#include "ItemInfoParser.h"
#include "ItemAttrInfoParser.h"
#include "ItemMakeParser.h"
#include "RateInfoParser.h"
#include "ItemAttrInfoParser.h"
#include "RankOptionParser.h"
#include "SeriesInfoParser.h"
#include "PacketStruct_ClientGameS.h"
//#include "ItemManager.h"

#pragma warning(disable:4244)
#pragma warning(disable:4345)

#define ItemManager  ItemManagerTesting
//using namespace singleton;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//GLOBALINST_SINGLETON_IMPL(GameLog, ()  , gamemain::eInstPrioGameFunc);
//GLOBALINST_SINGLETON_IMPL(ItemManager, ()  , gamemain::eInstPrioGameFunc);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace testing;
GLOBALINST_SINGLETON_IMPL2(ItemManager,TestingItemManager,	()  , gamemain::eInstPrioGameFunc);
GLOBALINST_SINGLETON_IMPL2(GameLog,		GameLog ,				()  , gamemain::eInstPrioGameFunc);


using namespace util;


namespace testing
{ 



BOOL ItemManager::ValidPos( SLOTINDEX atIndex, SLOTPOS atPos, BOOL bEmptyCheck )
{
	if( atIndex == SI_SKILL ) return FALSE;

	if( atIndex >= SI_MAX )
		return FALSE;

	BaseContainer * pAtContainer	= GetItemSlotContainer( atIndex );

	if( pAtContainer == NULL )
		return FALSE;

	if( !pAtContainer->ValidState() )
			return FALSE;

	if( !pAtContainer->ValidPos(atPos) )
		return FALSE;

	if( pAtContainer->IsLocked(atPos) )
		return FALSE;

	if( bEmptyCheck && pAtContainer->IsEmpty( atPos ) )
		return FALSE;

	return TRUE;
}

ItemSlotContainer * ItemManager::GetItemSlotContainer( SLOTINDEX Index )
{
	ASSERT( Index < SI_MAX );
	ASSERT( Index != SI_SKILL );
	return (ItemSlotContainer *)::theHero.GetSlotContainer(Index);
}


RC::eITEM_RESULT ItemManager::MakeItem	(SLOTCODE					destItemCode
													,DWORD						dwMaterialNums
													,BYTE							byMaterialRow
													,SLOTPOS*					parMaterialPos
													,LPCSTR						szItemName
													,sTOTALINFO_INVENTORY&	resultItems)
{
	SLOTPOS				mainPos(INVALID_POSTYPE);
	ItemSlot			itemResult;
	sITEMINFO_BASE*		pItemInfo;
	sITEMMAKE*			pMakeInfo;
	sITEMMAKELEVEL*	pLevelInfo;
	UINT					nColNum;
	SLOTPOS				arPos	[eITEMENCHANT_MAX_SLOT];
	UINT					arNums[MATERIALTYPE_MAX];

	BaseContainer * pInventory;


	__CHECK2_PTR(byMaterialRow > 0, RC::RC_ITEM_MAKE_INVALID_ROWNUM);

	nColNum		= eITEMENCHANT_MAX_SLOT/byMaterialRow;

	pItemInfo	= theItemInfoParser.GetItemInfo(destItemCode);
	__CHECK2_PTR(pItemInfo, RC::RC_ITEM_NOINFO);


	pMakeInfo = theItemMakeParser.GetMakeInfo((eITEM_TYPE)pItemInfo->m_wType);
	__CHECK2_PTR(pItemInfo, RC::RC_ITEM_MAKE_NOINFO);

	__CHECK2_RANGE2(pItemInfo->m_LV, 1, MAX_ITEM_LEVEL
						,RC::RC_ITEM_MAKE_INVALID_ITEMLEVEL);

	pLevelInfo = &pMakeInfo->m_arMakeLVs[pItemInfo->m_LV-1];

	__ZERO_ARRAY(arPos);
	__ZERO_ARRAY(arNums);

	pInventory = GetItemSlotContainer( SI_INVENTORY );


	////////////////////////////////////////////////////
	///检测金钱
	if( m_pPlayer->GetMoney() < pLevelInfo->m_Money )
		return RC::RC_ITEM_HAVENOTMONEY;


	/////////////////////////////////////////////////////
	//数据转换到 二维数组中
	INT	nMaterialNum(0);
	INT	nShiftNum(0);
	INT	nIndex(0);
	for(UINT nCol=0; nCol<nColNum;nCol++)
	{
		nMaterialNum	= ( (dwMaterialNums >> nShiftNum) & MATERIAL_ROW_MASK );
		nShiftNum		+= MATERIAL_ROW_SHIFT;
		arNums[nCol]	= nMaterialNum;
		for(INT	nRow=0; nRow<nMaterialNum; nRow++)
		{
			SLOTPOS		atPos;
			atPos				= (SLOTPOS)(nRow + nCol * byMaterialRow);
			arPos[atPos]	= parMaterialPos[nIndex];
			nIndex++;
		}
	}

	/////////////////////////////////////////////////////
	//计算原料位置信息
	INT	arMakeMaterialStates[MATERIALTYPE_MAX];
	INT	nMainCol(0);
	if(!theItemMakeParser.CalcMaterialSlotInfo(pLevelInfo
															,arMakeMaterialStates
															,nColNum
															,byMaterialRow))
	{
		return RC::RC_ITEM_MAKE_INVALID_MATERIALSLOTINFO;
	}


	/////////////////////////////////////////////////////
	//检测信息的合法性
	//  原料类型与所在位置是否匹配
	//	 原料是否为合成原料
	/// 检测背包是否存在指定材料

	for(UINT nCol=0; nCol< nColNum; nCol++)
	{
		INT typeIndex;
		typeIndex = arMakeMaterialStates[nCol];

		if(typeIndex <= 0)
			continue;

		///检测传来的原料数量，与配置中数量是否匹配
		__CHECK2	(pLevelInfo->m_arMaterialNum[typeIndex] == arNums[nCol]
					,RC::RC_ITEM_MAKE_INVALID_MATERIALSLOTINFO);

		///检测原料类型
		for(UINT nRow=0; nRow<arNums[nCol]; nRow++)
		{
			SLOTPOS	atPos;
			atPos = arPos[nRow + nCol * byMaterialRow];
			
			///检测是否背包存在此材料
			__CHECK2	(!pInventory->IsEmpty(atPos)
						,RC::RC_ITEM_EMPTYSLOT);

			ItemSlot& itemSlot = (ItemSlot&) pInventory->GetSlot(atPos);
			pItemInfo				= itemSlot.GetItemInfo();
			__CHECK2_PTR	(pItemInfo ,RC::RC_ITEM_NOINFO);

			///检测物品类型
			__CHECK2	(pItemInfo->m_wType == ITEMTYPE_MATERIAL
						,RC::RC_ITEM_MAKE_INVALID_MATERIALSLOTINFO);

			//检测原料类型
			__CHECK2	(pItemInfo->m_byMaterialType == typeIndex
						,RC::RC_ITEM_MAKE_INVALID_MATERIALSLOTINFO);
		}

		///设定主材
		if(typeIndex == pLevelInfo->m_byMainIndex)
		{
			nMainCol = nCol;
			mainPos	= arPos[nCol * byMaterialRow];
		}
	}

	__CHECK2(mainPos != INVALID_POSTYPE, RC::RC_ITEM_INVALIDPOS);


	ItemSlot& mainItem = (ItemSlot&)pInventory->GetSlot(mainPos);

	///////////////////////////////////////////////////////
	//依据目标，生成新物品
	itemResult.SetCode(destItemCode);
	itemResult.SetNum	(1);

	///////////////////////////////////////////////////////
	//依据目标，生成打造属性

	//设置为主材系别
	itemResult.SetSeries		(mainItem.GetSeries());
	itemResult.SetEnchant	(mainItem.GetEnchant());

	///////////////////////////////////////////////////////
	///累积各原料属性
	//DWORD					dwRegen(0);
	//DWORD					dwOppress(0);
	vector<SLOTPOS>	arRegen;
	vector<SLOTPOS>	arOppress;
	vector<SLOTPOS>	arNormal;

	for(UINT nCol=0; nCol< nColNum; nCol++)
	{
		for(UINT nRow=0; nRow<arNums[nCol]; nRow++)
		{
			SLOTPOS	atPos;
			atPos = arPos[nRow + nCol * byMaterialRow];

			ItemSlot&	materialItem = (ItemSlot&) pInventory->GetSlot(atPos);

			if(theSeriesInfoParser.CanRegenTo((eATTACK_KIND)materialItem.GetSeries(), (eATTACK_KIND)mainItem.GetSeries()) )
			{
				arRegen.push_back(atPos);
				//dwRegen++;
			}
			else if(theSeriesInfoParser.CanOppressTo((eATTACK_KIND)materialItem.GetSeries(), (eATTACK_KIND)mainItem.GetSeries()))
			{
				arOppress.push_back(atPos);
				//dwOppress++;
			}
			///将各副原料属性追加到目标物品上 
			else if(atPos != mainItem.GetPos())
			{
				arNormal.push_back(atPos);
				//itemResult.AppendItem(materialItem, ITEM_APPEND_ALL);
			}
		}
	}

	///////////////////////////////////////////////
	//属性追加 处理
	//A主材B普通C相生D相克
	for(INT n=SERIES_RELATION_MAX-1; n>=0; n--)
	{
		vector<SLOTPOS>* pArray = NULL;
		switch(pLevelInfo->m_Sequenes[n])
		{
		case 'A':	///主材处理
			{
				itemResult.AppendItem(mainItem, (DWORD)ITEM_APPEND_ALL);
			}break;
		case 'B':		pArray = &arNormal;	break;
		case 'C':		pArray = &arRegen;	break;
		case 'D':		pArray = &arOppress;	break;
		}

		if(pArray)
		{
			for(UINT n=0; n<pArray->size(); n++)
			{
				ItemSlot&	materialItem = (ItemSlot&) pInventory->GetSlot((*pArray)[n]);
				itemResult.AppendItem(materialItem, (DWORD)ITEM_APPEND_ALL);
			}
		}
	}


	//依据目标，生成打造属性
	if(!RegenItem	(itemResult ,pLevelInfo,arRegen.size(),arOppress.size()))
		return RC::RC_ITEM_MAKE_FAILED;


	/////////////////////////////////////////////////////////////
	///扣除手续费
	BOOL ret = m_pPlayer->MinusMoney( pLevelInfo->m_Money );
	__UNUSED(ret);
	ASSERT(ret);

	/////////////////////////////////////////////////////////////
	//扣除原料
	for(UINT nCol=0; nCol< nColNum; nCol++)
	{
		///检测原料类型
		for(UINT nRow=0; nRow<arNums[nCol]; nRow++)
		{
			SLOTPOS	atPos;
			//atPos = nRow + nCol * byMaterialRow;
			atPos = arPos[nRow + nCol * byMaterialRow];

			Lose(pInventory, atPos, 1);
		}
	}

	itemResult.SetName(szItemName);


	///////////////////////////////////////////////////////
	//获得物品
	//pInventory->InsertSlot(mainPos, itemResult);
	RC::eITEM_RESULT rt = Obtain(itemResult , 1, &resultItems );
	if( rt != RC::RC_ITEM_SUCCESS )
		return RC::RC_ITEM_ISNOTEMPTYSLOT;



	return RC::RC_ITEM_MAKE_SUCCESS;
}


RC::eITEM_RESULT ItemManager::Enchant( SLOTPOS TargetPos, BYTE & OUT ResultEnchant )
{
	if( !IsPossibleFunction( ITEM_FUNCTYPE_ENCHANT ) )
		return RC::RC_ITEM_UNUSABLE_FUNCTION;
	if( !ValidState() ) 
		return RC::RC_ITEM_INVALIDSTATE;
	if( !ValidPos(  SI_INVENTORY, TargetPos ) )
		return RC::RC_ITEM_INVALIDPOS;

	BaseContainer * pInventory = GetItemSlotContainer( SI_INVENTORY );
	ItemSlot & rTargetSlot = (ItemSlot &)pInventory->GetSlot(TargetPos);
	if( !rTargetSlot.GetItemInfo()->IsWeapon() &&
		!rTargetSlot.GetItemInfo()->IsArmor() )
	{
		return RC::RC_ITEM_ITEMCODENOTEQUAL;
	}

	BYTE Enchant = rTargetSlot.GetEnchant();
	MATERIALTYPE eCode;
	switch( Enchant )
	{
	case 0: case 1: case 2: case 3: case 4: case 5: { eCode = ITEMCOMPOSITE_LOW_ENCHANT; } break;
	case 6: case 7: case 8:							{ eCode = ITEMCOMPOSITE_MEDIUM_ENCHANT; } break;
	case 9: case 10: case 11:						{ eCode = ITEMCOMPOSITE_HIGH_ENCHANT; } break;
	default:										{  ASSERT( NULL );	return RC::RC_ITEM_INVALIDSTATE; }
	}



	sITEMCOMPOSITE * pCInfo = theItemCompositeParser.GetCompositeInfo(eCode);
	ASSERT( pCInfo );

	if( m_pPlayer->GetMoney() < pCInfo->m_Money )
		return RC::RC_ITEM_HAVENOTMONEY;

	sITEMCOMPOSITEMATERIAL *  pMaterials = pCInfo->m_sCompositeMaterials;

	vector<COMPOSITEMATERIAL> vecMatInfo;

	for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
	{
		if( pMaterials[i].m_dwCompositeMaterialCode == 0 ) continue;

		EmptyItemCheck EmptyCheck( pMaterials[i].m_dwCompositeMaterialCode, pMaterials[i].m_bySpendMaterialNum );
		if( !ForeachSlot( SI_INVENTORY, EmptyCheck ) )
			return RC::RC_ITEM_ENCHANT_HAVENOTMATERIALS;

		COMPOSITEMATERIAL info;
		info.nMaterialCode = pMaterials[i].m_dwCompositeMaterialCode;
		info.wCntMaterial = pMaterials[i].m_bySpendMaterialNum;
		vecMatInfo.push_back( info );
	}

	GAMELOG->WriteEnchantStart( m_pPlayer, g_pGameServer->GetServerGUID(), vecMatInfo );

	for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
	{
		if( pMaterials[i].m_dwCompositeMaterialCode == 0 ) continue;

		DeleteItem DeleteOperator( pMaterials[i].m_dwCompositeMaterialCode, pMaterials[i].m_bySpendMaterialNum );
		BOOL rt = ForeachSlot( SI_INVENTORY, DeleteOperator );
		__UNUSED(rt);
		ASSERT(rt);
	}

	BOOL ret = m_pPlayer->MinusMoney( pCInfo->m_Money );
	__UNUSED(ret);
	ASSERT(ret);

	BYTE RatioIndex = Enchant;
	if( RatioIndex >= 6 )
	{
		RatioIndex -= 6;
		ASSERT( RatioIndex < RatioManager::MAX_ENCHANT_SELECTER_NUM );
		DWORD key = g_Ratio.m_EnchantRatioSelecter[RatioIndex].RandomizedKey();
		if( key == 1 )
		{
			if( Enchant == 6 || Enchant == 7 || Enchant == 8 )
			{
				rTargetSlot.SetEnchant( Enchant - 1 );
				ResultEnchant = Enchant - 1;

				//////////////////////////////////////////////////////////////////////////
				GAMELOG->WriteEnchantResult( m_pPlayer, g_pGameServer->GetServerGUID(), FALSE, &rTargetSlot, vecMatInfo );

				return RC::RC_ITEM_ENCHANT_DOWN_FAILED;
			}
			else
			{
				pInventory->DeleteSlot( TargetPos, NULL );
				return RC::RC_ITEM_ENCHANT_CRACK_FAILED;
			}
			
		}
	}

	rTargetSlot.SetEnchant( Enchant + 1 );
	ResultEnchant = Enchant + 1;

	//////////////////////////////////////////////////////////////////////////
	GAMELOG->WriteEnchantResult( m_pPlayer, g_pGameServer->GetServerGUID(), TRUE, &rTargetSlot, vecMatInfo );

	
	return RC::RC_ITEM_ENCHANT_SUCCESS;
}


RC::eITEM_RESULT ItemManager::EnchantEx( SLOTPOS TargetPos, BYTE SuccessRateIndex , BYTE & OUT ResultEnchant)
{
	if( !IsPossibleFunction( ITEM_FUNCTYPE_ENCHANT ) )
		return RC::RC_ITEM_UNUSABLE_FUNCTION;

	if( !ValidState() ) 
		return RC::RC_ITEM_INVALIDSTATE;
	if( !ValidPos(  SI_INVENTORY, TargetPos ) )
		return RC::RC_ITEM_INVALIDPOS;


	BaseContainer * pInventory = GetItemSlotContainer( SI_INVENTORY );
	ItemSlot & rTargetSlot = (ItemSlot &)pInventory->GetSlot(TargetPos);
	if( !rTargetSlot.GetItemInfo()->IsWeapon() &&
		!rTargetSlot.GetItemInfo()->IsArmor() )
	{
		return RC::RC_ITEM_ITEMCODENOTEQUAL;
	}

	//////////////////////////////////////
	//检测装备类型
	sENCHANT_MATERIALS * pNeedMaterial = NULL;

	pNeedMaterial = theEnchantInfoParser.FindEnchantMatInfo( rTargetSlot.GetItemInfo());

	if( !pNeedMaterial )
	{
		return RC::RC_ITEM_ENCHANT_INVALID_ITEMLEVEL;
	}

	INT				SuccessRate				= 100;
	//FLOAT				NeedMaterialProduct	= 0.0f;
	BYTE				Enchant					= rTargetSlot.GetEnchant();
	eENCHANTLEVEL	enchantLevel			= rTargetSlot.GetEnchantLV();
	MATERIALTYPE	eCode;


	switch( enchantLevel )
	{
	case ENCHANT_LOW:		{ eCode = ITEMCOMPOSITE_LOW_ENCHANT; }		break;
	case ENCHANT_MIDDLE:	{ eCode = ITEMCOMPOSITE_MEDIUM_ENCHANT; }	break;
	case ENCHANT_HIGH:	{ eCode = ITEMCOMPOSITE_HIGH_ENCHANT; }		break;
	default:					{  ASSERT( NULL );	return RC::RC_ITEM_INVALIDSTATE; }
	}

	////////////////////////////////
	//检测成功率
	// 放由 m_byMinRate检测
	//if(enchantLevel == ENCHANT_LOW && SuccessRateIndex != eSUCCEEDRATE_100)
	//	return RC::RC_ITEM_ENCHANT_INVALID_RATE_INDEX;

	if(SuccessRateIndex < eSUCCEEDRATE_25 || SuccessRateIndex >= eSUCCEEDRATE_NUM)
			return RC::RC_ITEM_ENCHANT_INVALID_RATE_INDEX;


	vector<COMPOSITEMATERIAL> vecMatInfo;


	///////////////////////////////////////////////////////
	///计算强化原料条件
	WORD						wNeedNum(0);
	sMATERIALS *			pMaterials;
	sLEVEL_MATERIALS*	pEnchantLV;
	MONEY						moneyCost(0);

	pEnchantLV	= &pNeedMaterial->m_EnchantMaterial[(Enchant+1)];
	pMaterials	= pEnchantLV->m_Material;

	if(SuccessRateIndex < pEnchantLV->m_byMinRate)
		return RC::RC_ITEM_ENCHANT_INVALID_RATE_INDEX;

	for( INT i = 0 ; i < MAX_ENCHANT_MATERIAL_NUM ; ++i )
	{
		if( 0 == pMaterials[i].m_MaterialItemCode || 0 == pMaterials[i].m_MaterialItemNum )
			continue;

		wNeedNum = 	theEnchantInfoParser.CalcMaterialNum(pEnchantLV
                                                            ,SuccessRateIndex
																				,pMaterials[i].m_MaterialItemNum);

		EmptyItemCheck EmptyCheck( pMaterials[i].m_MaterialItemCode, (SLOTPOS)wNeedNum );
		if( !ForeachSlot( SI_INVENTORY, EmptyCheck ) )
			return RC::RC_ITEM_ENCHANT_HAVENOTMATERIALS;

		COMPOSITEMATERIAL info;
		info.nMaterialCode	= pMaterials[i].m_MaterialItemCode;
		info.wCntMaterial		= (SLOTPOS)wNeedNum;//(pMaterials[i].m_MaterialItemNum*NeedMaterialProduct+0.5f);
		vecMatInfo.push_back( info );
	}

	moneyCost	= theEnchantInfoParser.CalcMoneyCost(pEnchantLV
																			,SuccessRateIndex
																			,pEnchantLV->m_Money);

	///////////////////////////////////////////////////////
	///计算Composite原料条件
	sITEMCOMPOSITE *				pCInfo(NULL);
	sITEMCOMPOSITEMATERIAL *	pCompositeMaterials(NULL);

	if(pEnchantLV->m_byNeedComposite)
	{
		pCInfo = theItemCompositeParser.GetCompositeInfo(eCode);
		ASSERT( pCInfo );
		//累积手续费
		moneyCost += pCInfo->m_Money;

		///检测Composite原料
		pCompositeMaterials = pCInfo->m_sCompositeMaterials;

		for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
		{
			if( pCompositeMaterials[i].m_dwCompositeMaterialCode == 0 )
				continue;

			EmptyItemCheck EmptyCheck( pCompositeMaterials[i].m_dwCompositeMaterialCode, pCompositeMaterials[i].m_bySpendMaterialNum );
			if( !ForeachSlot( SI_INVENTORY, EmptyCheck ) )
				return RC::RC_ITEM_ENCHANT_HAVENOTMATERIALS;

			COMPOSITEMATERIAL info;
			info.nMaterialCode	= pCompositeMaterials[i].m_dwCompositeMaterialCode;
			info.wCntMaterial		= pCompositeMaterials[i].m_bySpendMaterialNum;
			vecMatInfo.push_back( info );
		}
	}

	///检测金钱
	if( m_pPlayer->GetMoney() < moneyCost || pCompositeMaterials)
		return RC::RC_ITEM_HAVENOTMONEY;

	GAMELOG->WriteEnchantStart( m_pPlayer, g_pGameServer->GetServerGUID(), vecMatInfo );



	///////////////////////////////////////////////////////
	//原料扣除操作 强化原料
	for( SLOTPOS i = 0 ; i < MAX_ENCHANT_MATERIAL_NUM ; ++i )
	{
		if( 0 == pMaterials[i].m_MaterialItemCode || 0 == pMaterials[i].m_MaterialItemNum )
			continue;

		wNeedNum = 	theEnchantInfoParser.CalcMaterialNum(pEnchantLV
                                                            ,SuccessRateIndex
																				,pMaterials[i].m_MaterialItemNum);
		DeleteItem DeleteOperator( pMaterials[i].m_MaterialItemCode, (SLOTPOS)wNeedNum );
		BOOL rt = ForeachSlot( SI_INVENTORY, DeleteOperator );
		__UNUSED(rt);
		ASSERT(rt);
	}


	///////////////////////////////////////////////
	///Composite原料扣除
	if(pEnchantLV->m_byNeedComposite)
	{
		for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
		{
			if( pCompositeMaterials[i].m_dwCompositeMaterialCode == 0 ) continue;

			DeleteItem DeleteOperator	(pCompositeMaterials[i].m_dwCompositeMaterialCode
                                    ,pCompositeMaterials[i].m_bySpendMaterialNum);
			BOOL rt = ForeachSlot( SI_INVENTORY, DeleteOperator );
			__UNUSED(rt);
			ASSERT(rt);
		}
	}

	///手续费扣除
	BOOL ret = m_pPlayer->MinusMoney( moneyCost );
	__UNUSED(ret);
	ASSERT(ret);


	//////////////////////////////////////////////////////////
	//以下进行强化处理

	static BYTE arRate[eSUCCEEDRATE_NUM]=
	{
		25,50,75,100
	};
	SuccessRate = arRate[SuccessRateIndex];


	if( util::DrawLots(SuccessRate) )
	{
		rTargetSlot.SetEnchant( Enchant + 1 );

		ResultEnchant = Enchant + 1;
		//////////////////////////////////////////////////////////////////////////
		// 
		GAMELOG->WriteEnchantResult( m_pPlayer, g_pGameServer->GetServerGUID(), TRUE, &rTargetSlot, vecMatInfo );

		return RC::RC_ITEM_ENCHANT_SUCCESS;
	}

	//失败处理
	switch(enchantLevel)
	{
		//不处理
	case ENCHANT_LOW:
		{
		}break;

		///降Enchant
	case ENCHANT_MIDDLE:
		{
			rTargetSlot.SetEnchant( Enchant - 1 );
			ResultEnchant = Enchant - 1;
			GAMELOG->WriteEnchantResult( m_pPlayer, g_pGameServer->GetServerGUID(), FALSE, &rTargetSlot, vecMatInfo );
			return RC::RC_ITEM_ENCHANT_DOWN_FAILED;
		}break;

		//装备破碎
	case ENCHANT_HIGH:
		{
			pInventory->DeleteSlot( TargetPos, NULL );
			return RC::RC_ITEM_ENCHANT_CRACK_FAILED;
		}break;
	}

	//////////////////////////////////////////////////////////////////////////
	GAMELOG->WriteEnchantResult( m_pPlayer, g_pGameServer->GetServerGUID(), 	FALSE, &rTargetSlot, vecMatInfo );

	return RC::RC_ITEM_ENCHANT_FAILED;
}



RC::eITEM_RESULT ItemManager::FillSocket(MATERIALTYPE  Type, SLOTPOS SocketItemPos[], SLOTPOS TargetPos )
{
	if( !IsPossibleFunction( ITEM_FUNCTYPE_FILLSOCKET ) )
		return RC::RC_ITEM_UNUSABLE_FUNCTION;
	if( !ValidState() ) 
		return RC::RC_ITEM_INVALIDSTATE;

	sITEMCOMPOSITE * pCInfo = theItemCompositeParser.GetCompositeInfo((MATERIALTYPE)Type);
	if( !pCInfo )
		return RC::RC_ITEM_INVALIDSTATE;

	if( m_pPlayer->GetMoney() < pCInfo->m_Money )
		return RC::RC_ITEM_HAVENOTMONEY;


	//sITEMCOMPOSITEMATERIAL *  pMaterials = pCInfo->m_sCompositeMaterials;
	//for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
	//{
	//	if( pMaterials[i].m_dwCompositeMaterialCode == 0 ) continue;

	//	EmptyItemCheck EmptyCheck(pMaterials[i].m_dwCompositeMaterialCode,pMaterials[i].m_bySpendMaterialNum);
	//	if( !ForeachSlot( SI_INVENTORY, EmptyCheck ) )
	//		return RC::RC_ITEM_ENCHANT_HAVENOTMATERIALS;
	//}

	for(INT n=0; n<SOCKET_MAX; n++)
	{
		if(SocketItemPos[n] == INVALID_POSTYPE )
			continue;
		if( !ValidPos( SI_INVENTORY, SocketItemPos[n] ) )
			return RC::RC_ITEM_INVALIDPOS;
	}

	if( !ValidPos(  SI_INVENTORY, TargetPos ) )
		return RC::RC_ITEM_INVALIDPOS;

	BaseContainer * pInventory = GetItemSlotContainer( SI_INVENTORY );
	ItemSlot & rTargetSlot = (ItemSlot &)pInventory->GetSlot(TargetPos);


	// 物品类型检测...
	if( !rTargetSlot.GetItemInfo()->IsWeapon() &&
		rTargetSlot.GetItemInfo()->m_wType != ITEMTYPE_ARMOR &&
		rTargetSlot.GetItemInfo()->m_wType != ITEMTYPE_HELMET &&
		rTargetSlot.GetItemInfo()->m_wType != ITEMTYPE_PANTS &&
		rTargetSlot.GetItemInfo()->m_wType != ITEMTYPE_BOOTS &&
		rTargetSlot.GetItemInfo()->m_wType != ITEMTYPE_GLOVE )
	{
		return RC::RC_ITEM_INVALID_CONDITION;
	}


	// 100%成功...........

	///检测是否存在Socket有效信息，发现其中不满足，不处理
	for(INT n=0; n<SOCKET_MAX; n++)
	{
		if(SocketItemPos[n] == INVALID_POSTYPE )
			continue;
		if( rTargetSlot.GetSocketAttrIndex((eSOCKET)(n + SOCKET_1), eSOCKET_VALUE_1) != 0 )
		{
			return RC::RC_ITEM_FULLSOCKET;
		}

		ItemSlot & rSocketSlot = (ItemSlot &)pInventory->GetSlot(SocketItemPos[n]);
		if( rSocketSlot.GetItemInfo()->m_wType != ITEMTYPE_SOCKET )
		{
			return RC::RC_ITEM_INVALID_CONDITION;
		}

	}

	///最后镶嵌所有物品
	for(INT n=0; n<SOCKET_MAX; n++)
	{
		if(SocketItemPos[n] == INVALID_POSTYPE )
			continue;
		ItemSlot & rSocketSlot = (ItemSlot &)pInventory->GetSlot(SocketItemPos[n]);
		rTargetSlot.CopySocketInfo	( (eSOCKET)(n + SOCKET_1)
											, rSocketSlot
											, SOCKET_1);

		Lose( pInventory, SocketItemPos[n], 1 );
	}

	m_pPlayer->MinusMoney(pCInfo->m_Money);

	return RC::RC_ITEM_SUCCESS;
}


RC::eITEM_RESULT ItemManager::ExtractSocket	(MATERIALTYPE					Type
																	,SLOTPOS                    TargetPos
																	,DWORD							 dwSocketState
																	,DWORD&							 dwSocketResult
																	,sTOTALINFO_INVENTORY * OUT pTotalInfo)
{
	if( !IsPossibleFunction( ITEM_FUNCTYPE_EXTRACTSOCKET ) )
		return RC::RC_ITEM_UNUSABLE_FUNCTION;
	if( !ValidState() ) 
		return RC::RC_ITEM_INVALIDSTATE;
	if( !ValidPos(  SI_INVENTORY, TargetPos ) )
		return RC::RC_ITEM_INVALIDPOS;

	BaseContainer * pInventory = GetItemSlotContainer( SI_INVENTORY );
	ItemSlot & rTargetBak = (ItemSlot &)pInventory->GetSlot(TargetPos);

	ItemSlot  rTargetSlot;
	rTargetSlot.Copy(rTargetBak);



	SLOTPOS need_num = 0;
	//////////////////////////////////////////////////////
	///检测操作的Socket位置，是否有Socket信息
	BOOL bExist = TRUE;
	for( int i = 0 ; i < rTargetSlot.GetSocketNum() ; ++i )
	{
		if( dwSocketState & _BIT(i) )
		{
			if( rTargetSlot.GetSocketAttr((eSOCKET)i, eSOCKET_VALUE_1) == 0 )
			{
				bExist = FALSE;
				break;
			}
			need_num++;
		}
	}

	if( !bExist ) return RC::RC_ITEM_INVALID_CONDITION;


	///////////////////////////////////////////////////////////////
	///检测背包是否有空间
	static SLOTPOS InPos[SOCKET_MAX];
	SLOTPOS InPosNum = 0;

	CanInsertItem( pInventory, 1, need_num, InPos, InPosNum );

	if( need_num != 0 )
		return RC::RC_ITEM_NOSPACEININVENTORY;


	//////////////////////////////////////
	//检测操作条件
	sITEMCOMPOSITE * pCInfo = theItemCompositeParser.GetCompositeInfo(Type);
	ASSERT( pCInfo );

	//Money
	if( m_pPlayer->GetMoney() < pCInfo->m_Money )
		return RC::RC_ITEM_HAVENOTMONEY;

	///材料检测
	//sITEMCOMPOSITEMATERIAL *  pMaterials = pCInfo->m_sCompositeMaterials;
	//for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
	//{
	//	if( pMaterials[i].m_dwCompositeMaterialCode == 0 ) continue;

	//	EmptyItemCheck EmptyCheck(pMaterials[i].m_dwCompositeMaterialCode,pMaterials[i].m_bySpendMaterialNum);
	//	if( !ForeachSlot( SI_INVENTORY, EmptyCheck ) )
	//		return RC::RC_ITEM_ENCHANT_HAVENOTMATERIALS;
	//}


	//////////////////////////////////////////////////////////
	//开始拆除操作
	sITEMCOMPOSITERESULT * pResultInfo;

	pResultInfo = theItemCompositeParser.GetCompositeResultInfo((SLOTCODE)pCInfo->m_ResultCode);

	/// 运算主物品拆除机率
	/// 宝石拆除机率
	BYTE	byMainRate = 100;
	BYTE	arDamageRate[SOCKET_MAX];
	BYTE	arRetainRate[SOCKET_MAX];

	if(pResultInfo)
	{
		byMainRate = pResultInfo->m_byMainRate;
		for(INT n=0; n<SOCKET_MAX; n++)
		{
			arDamageRate[n] = pResultInfo->SOCKET.m_arDamageRates[n];
			arRetainRate[n] = pResultInfo->SOCKET.m_arRetainRates[n];
		}
	}
	else
	{
		for(INT n=0; n<SOCKET_MAX; n++)
		{	
			arDamageRate[n]	= 0;
			arRetainRate[n]	= 100;
		}
	}

	dwSocketResult		= 0;
	///生成宝石
	BOOL bCreateItem	= FALSE;
	BYTE byDamageLV	= 0;
	pTotalInfo->m_InvenCount		= 0;
	pTotalInfo->m_TmpInvenCount	= 0;
	INT  nInsertIndex = 0;
	for( int i = 0 ; i < rTargetSlot.GetSocketNum() ; ++i )
	{
		if( dwSocketState & _BIT(i) )
		{
			///按机率生成宝石（损坏）
			///生成完好宝石机率
			if(DrawLots(arDamageRate[i]))
			{
				bCreateItem = TRUE;
				byDamageLV  = 1;
			}
			else if(DrawLots(arRetainRate[i]))
			{
				bCreateItem = TRUE;
				byDamageLV  = 0;
			}

			if(bCreateItem)
			{
				const sITEM_ATTR_INFO *	pOption;
				ItemSlot					itemSocket;

				pOption = rTargetSlot.GetSocketAttrDesc((eSOCKET)i);

				itemSocket.SetCode			(pOption->m_SocketItemCode);
				itemSocket.SetDamageLV		(byDamageLV);
				itemSocket.CopySocketInfo	(SOCKET_1,rTargetSlot,(eSOCKET)i);

				pTotalInfo->m_Slot[pTotalInfo->m_InvenCount].m_ItemPos	= InPos[nInsertIndex];
				itemSocket.CopyOut(pTotalInfo->m_Slot[pTotalInfo->m_InvenCount].m_Stream);

				pTotalInfo->m_InvenCount++;

				rTargetSlot.SetSocketAttr((eSOCKET)i,NULL);

				pInventory->InsertSlot(InPos[nInsertIndex], itemSocket);
				nInsertIndex++;
				//RC::eITEM_RESULT rt = Obtain(itemResult , 1, pTotalInfo );
				//if( rt != RC::RC_ITEM_SUCCESS )
				//	return RC::RC_ITEM_ISNOTEMPTYSLOT;
			}
			else
			{
				dwSocketResult |= _BIT(i);
			}
		}
	}


	////////////////////////////////////////
	//扣除操作条件
	//for( SLOTPOS i = 0 ; i < pCInfo->m_byMatCount ; ++i )
	//{
	//	if( pMaterials[i].m_dwCompositeMaterialCode == 0 )
	//		continue;

	//	DeleteItem DeleteOperator(pMaterials[i].m_dwCompositeMaterialCode,pMaterials[i].m_bySpendMaterialNum);
	//	BOOL rt = ForeachSlot( SI_INVENTORY, DeleteOperator );
	//	ASSERT(rt);
	//}

	//扣除手续费
	BOOL ret = m_pPlayer->MinusMoney( pCInfo->m_Money );
	__UNUSED(ret);
	ASSERT(ret);


	rTargetBak.Copy(rTargetSlot);

	/////////////////////////////////////////////
	///更新主物品
	if(DrawLots(byMainRate))
	{
		pTotalInfo->m_Slot[pTotalInfo->m_InvenCount].m_ItemPos	= TargetPos;
		rTargetSlot.CopyOut(pTotalInfo->m_Slot[pTotalInfo->m_InvenCount].m_Stream);
		pTotalInfo->m_InvenCount++;

		return RC::RC_ITEM_EXTRACTSOCKET_SUCCESS;
	}

	///主物品破碎
	Lose(pInventory, TargetPos,1);

	return RC::RC_ITEM_EXTRACTSOCKET_FAILED;




}

RC::eITEM_RESULT		ItemManager::Lose( SLOTCODE /*ItemCode*/, BOOL /*bAllOfItem*/ )
{
	return RC::RC_ITEM_SUCCESS;
}

VOID ItemManager::Lose( BaseContainer * /*pContainer*/, SLOTPOS /*pos*/, BYTE /*num*/ )
{
}

VOID ItemManager::Lose( SLOTINDEX slotIdx, SLOTPOS pos, BYTE num )
{
	BaseContainer * pContainer = GetItemSlotContainer( slotIdx );

	ItemSlot & rSlotItem = (ItemSlot &)pContainer->GetSlot(pos);


	MSG_CG_ITEM_LOSE_ACK msg;

	msg.m_ItemPos = pos;
	if( rSlotItem.GetNum() > num )
	{
		msg.m_ItemNum = rSlotItem.GetNum() - num;
		//rSlotItem.SetNum( rSlotItem.GetNum() - num );
	}
	else
	{
		msg.m_ItemNum = 0;
		//pContainer->DeleteSlot( rSlotItem.GetPos(), NULL );
	}

	m_pPlayer->SendPacket( &msg, sizeof(msg) );
}



RC::eITEM_RESULT ItemManager::Obtain( SLOTCODE ItemCode, SLOTPOS num, sTOTALINFO_INVENTORY * OUT pTotalInfo )
{
	ItemSlot itemSlot;
	itemSlot.SetCode( ItemCode );
	return Obtain( itemSlot, num, pTotalInfo );
}


RC::eITEM_RESULT ItemManager::Obtain( ItemSlot & IN slot, SLOTPOS num, sTOTALINFO_INVENTORY * OUT pTotalInfo )
{
	if( pTotalInfo )
	{
		pTotalInfo->m_InvenCount	= 0;
		pTotalInfo->m_TmpInvenCount	= 0;
	}

	/////////////////////////////////////////////////////
	//输出物品
	//resultItems.m_InvenCount			= 1;
	//resultItems.m_TmpInvenCount		= 0;
	//resultItems.m_Slot[0].m_ItemPos	= mainPos;
	//itemResult.CopyOut(resultItems.m_Slot[0].m_Stream);



	if( !ValidState() ) 
		return RC::RC_ITEM_INVALIDSTATE;

	//if( slot.IsOverlap() )
	//	return RC::RC_ITEM_INVALIDSTATE;

	BaseContainer * pInventory = GetItemSlotContainer( SI_INVENTORY );
	BaseContainer * pTmpInventory = GetItemSlotContainer( SI_INVENTORY2 );

	//ASSERT( num > 0 );
	////ASSERT( slot.GetSerial() != TEMP_SERIALTYPE_VALUE );


	int need_num = num;
	need_num -= pInventory->GetSlotMaxSize() - pInventory->GetSlotNum();
	if( need_num > 0 )
	{
		need_num -= pTmpInventory->GetSlotMaxSize() - pTmpInventory->GetSlotNum();
		if( need_num > 0 )
			return RC::RC_ITEM_NOSPACEININVENTORY;
	}

	BYTE total = 0;


	///////////////////////////////////////////////////////
	need_num = num;
	for( SLOTPOS pos = 0 ; pos < pInventory->GetSlotMaxSize() ; ++pos )
	{
		if( pInventory->IsEmpty(pos) )
		{
			--need_num;
			if( pTotalInfo )
			{
				pTotalInfo->m_Slot[total].m_ItemPos	= pos;
				slot.CopyOut(pTotalInfo->m_Slot[total].m_Stream);
				++total;
				++pTotalInfo->m_InvenCount;
			}
			if( need_num == 0 ) break;
		}
	}

	if( need_num != 0 )
	{
		for( SLOTPOS pos = 0 ; pos < pTmpInventory->GetSlotMaxSize() ; ++pos )
		{
			if( pTmpInventory->IsEmpty(pos) )
			{
				if( pTotalInfo )
				{
					pTotalInfo->m_Slot[total].m_ItemPos	= pos;
					slot.CopyOut(pTotalInfo->m_Slot[total].m_Stream);
					++total; ++pTotalInfo->m_TmpInvenCount;
				}
				--need_num;
				if( need_num == 0 ) break;
			}
		}
	}

	ASSERT( need_num == 0 );

	return RC::RC_ITEM_SUCCESS;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _REGEN_ITEM2(RATE, FIELD,FUNC,TYPE)\
		fRate		= theRateInfoParser.GetSectorRateByID(pMakeLevel->m_##RATE##RateID);\
		fRate		+= fAddRate;\
		nValue	= (INT) ((float)pMakeLevel->##FIELD * fRate + 0.5f);

#define REGEN_ITEM(RATE, FIELD,FUNC)		_REGEN_ITEM2(RATE, FIELD,FUNC,INT)\
														itemSlot.Set##FUNC	(itemSlot.Get##FUNC() + nValue)

#define REGEN_ITEM2(RATE, FIELD,FUNC,TYPE)	_REGEN_ITEM2(RATE, FIELD,FUNC,INT)\
														itemSlot.Set##FUNC	((TYPE)(itemSlot.Get##FUNC() + nValue))
															//itemSlot.Set##FUNC	((TYPE)nValue)


BOOL ItemManager::RegenItem	(ItemSlot&     itemSlot
                              ,sITEMMAKELEVEL* pMakeLevel
										,DWORD           dwRegenNum
										,DWORD           dwOppressNum)
{
	__CHECK_PTR(pMakeLevel);

	float							fRate;
	float							fAddRate;
	INT							nValue;		
	float							fRegenRate;
	float							fOppressRate;
	INT							curRank;
	eRANK_OPTION_ITEM_TYPE	ItemType;
	sRANK_OPTION *				pRankOption;

	//DWORD	dwMaterialNum;
	///////////////////////////////////////////////////////
	//依据目标，生成打造属性
	// 相生 = 提高随机属性机率
	// 相克 = 降低随机属性机率
	//
	// 1.生成随机属性
	// 累积材料属性
	// 生成潜隐属性
	//	确定物品系属性

	//计算材料总数
	//for(INT n=0; n<MATERIALTYPE_MAX; n++)
	//	dwMaterialNum += pMakeLevel->m_arMaterialNum[n];


	///////////////////////////////////////////////////////
	// 1.相生相克机率处理

	// 1.a 生成相生属性
	fRegenRate		= 0.f;
	for(UINT n=0; n<dwRegenNum; n++)
		fRegenRate		+= theRateInfoParser.GetSectorRateByID(pMakeLevel->m_RegenRateID);

	// 1.b 生成相克属性
	fOppressRate		= 0.f;
	for(UINT n=0; n<dwOppressNum; n++)
		fOppressRate	+= theRateInfoParser.GetSectorRateByID(pMakeLevel->m_OppressRateID);
	fAddRate = fRegenRate - fOppressRate;

	///////////////////////////////////////////////////////
	// 2.生成随机属性

	ItemType = ItemRank::GetRankOptionItemType	(itemSlot.GetItemInfo()->m_wType 
																,itemSlot.GetItemInfo()->m_byMaterialType );
	// 2.a 生成Rank属性
	//REGEN_ITEM2(Rank, m_byRank, Rank, eRANK);
	fRate		= theRateInfoParser.GetSectorRateByID(pMakeLevel->m_RankRateID);
	fRate		+= fAddRate;
	nValue	= (INT) ((float)pMakeLevel->m_byRank* fRate + 0.5f);
	for(INT nRank=0; nRank<nValue; nRank++)
	{
		if( ItemType != eRANK_OPTION_ITEM_TYPE_MAX )
		{
			curRank		= itemSlot.GetRank() + 1;
			pRankOption = theRankOptionParser.GetRankOptionRandom( ItemType, (eRANK)curRank );
			if(pRankOption )
			{
				itemSlot.SetRank		((eRANK)curRank);
				itemSlot.SetRankAttr	((eRANK)curRank, pRankOption, 0, TRUE );
			}
		}
	}


	//itemSlot.SetRank( nValue);

	// 2.b 生成LimitLV属性
	REGEN_ITEM(Level, m_byLevel, ModLimitLevel);

	///////////////////
	// 2.c 武器则 生成Attack属性
	REGEN_ITEM(PhyAttack, m_wPhyAttackMin,				ModAttackMin);
	REGEN_ITEM(PhyAttack, m_wPhyAttackMax,				ModAttackMax);
	REGEN_ITEM(AttackSpeed, m_AttackSpeed,				ModSpeed);
	REGEN_ITEM(AttSuccessRatio, m_AttSuccessRatio,	ModAttackRatio);

	if(itemSlot.GetItemInfo()->IsWeapon())
	{
		//PhyAttackMin
		//PhyAttackMax
		//REGEN_ITEM(PhyAttack, m_wPhyAttackMin,				ModPhyAttackMin);
		//REGEN_ITEM(PhyAttack, m_wPhyAttackMax,				ModPhyAttackMax);
		//魔法攻击
		//REGEN_ITEM(MagicAttack, m_wMagicAttackMin,		ModMagicAttackMin);
		//REGEN_ITEM(MagicAttack, m_wMagicAttackMax,		ModMagicAttackMax);

		//攻速
		//REGEN_ITEM(AttackSpeed, m_AttackSpeed,			ModAttackSpeed);
		//命中
		//REGEN_ITEM(AttSuccessRatio, m_AttSuccessRatio,	ModAttSuccessRatio);

		itemSlot.SetModAttackSpeed		(itemSlot.GetModAttackSpeed());
		itemSlot.SetModAttSuccessRatio(itemSlot.GetModAttackRatio());

		itemSlot.SetModPhyAttackMin	(itemSlot.GetModAttackMin());
		itemSlot.SetModPhyAttackMax	(itemSlot.GetModAttackMax());
		itemSlot.SetModMagicAttackMin	(itemSlot.GetModAttackMin());
		itemSlot.SetModMagicAttackMax	(itemSlot.GetModAttackMax());
		itemSlot.SetModRangeAttackMin	(itemSlot.GetModAttackMin());
		itemSlot.SetModRangeAttackMax	(itemSlot.GetModAttackMax());
	}

	else if(itemSlot.GetItemInfo()->IsArmor())
	{
		//PhyDef
		REGEN_ITEM(PhyAttack, m_wPhyAttackMin,				ModDefense);
		//REGEN_ITEM(PhyAttack, m_wPhyAttackMin,			ModPhyDef);
		//PhyAttackMax
		//REGEN_ITEM(PhyAttack, m_wPhyAttackMax,				SetModPhyAttackMax);
		//MagicDef
		//REGEN_ITEM(MagicAttack, m_wMagicAttackMin,		ModMagicDef);
		//REGEN_ITEM(MagicAttack, m_wMagicAttackMax,		SetModMagicAttackMax);
		itemSlot.SetModPhyDef	(itemSlot.GetModDefense());
		itemSlot.SetModMagicDef	(itemSlot.GetModDefense());

		itemSlot.SetModMoveSpeed		(itemSlot.GetModAttackSpeed());
		itemSlot.SetModAttBlockRatio	(itemSlot.GetModAttackRatio());
		//移速
		//REGEN_ITEM(AttackSpeed, m_AttackSpeed,				ModMoveSpeed);
		//命中
		//REGEN_ITEM(AttSuccessRatio, m_AttSuccessRatio,	ModAttBlockRatio);
	}

	///其它非武器与防具时，则有机会生成
	else
	{
		itemSlot.SetModAttackSpeed		(itemSlot.GetModAttackSpeed());
		itemSlot.SetModAttSuccessRatio(itemSlot.GetModAttackRatio());
		itemSlot.SetModMoveSpeed		(itemSlot.GetModAttackSpeed());
		itemSlot.SetModAttBlockRatio	(itemSlot.GetModAttackRatio());
		//攻速
		//REGEN_ITEM(AttackSpeed, m_AttackSpeed,				ModAttackSpeed);
		//命中
		//REGEN_ITEM(AttSuccessRatio, m_AttSuccessRatio,	ModAttSuccessRatio);

		//移速
		//REGEN_ITEM(AttackSpeed, m_AttackSpeed,				ModMoveSpeed);
		//命中
		//REGEN_ITEM(AttSuccessRatio, m_AttSuccessRatio,	ModAttBlockRatio);
	}

	/////////////////////////////////
	// 3 生成潜隐属性
	for(INT n=0; n<sITEMMAKELEVEL::POTENTIAL_NUM; n++)
	{
		fRate		= theRateInfoParser.GetSectorRateByID(pMakeLevel->m_PotentialRateID[n]);
		fRate		+= fAddRate;
		if(fRate > 0.f)
		{
			if( ItemType != eRANK_OPTION_ITEM_TYPE_MAX )
			{
				curRank		= pMakeLevel->m_Potential[n];

				pRankOption = theRankOptionParser.GetRankOptionRandom( ItemType, (eRANK)curRank );
				if(pRankOption )
				{
					ePOTENTIAL poten;
					poten		= (ePOTENTIAL)(itemSlot.GetPotential() + 1);

					itemSlot.SetPotential		(poten);
					itemSlot.SetPotentialAttr	(poten, pRankOption, 0, TRUE );
				}
			}
		}
	}

	

	return TRUE;
}


VOID ItemManager::CanInsertItem	(BaseContainer * pContainer
                                 ,SLOTPOS         MAX_DUP_NUM
											,SLOTPOS & INOUT needNumRet
											,SLOTPOS * OUT   PosArray
											,SLOTPOS & OUT   PosNum)
{
	INT need_num = needNumRet;
	if( need_num == 0 )
		return;
	for( SLOTPOS pos = 0 ; pos < pContainer->GetSlotMaxSize() ; ++pos )
	{
		if( pContainer->IsEmpty(pos) )
		{
			PosArray[PosNum++] = pos;
			if( need_num > MAX_DUP_NUM)
				need_num -= MAX_DUP_NUM;
			else
				need_num = 0;

			if( need_num == 0 )
				break;
		}
	}
	needNumRet = (SLOTPOS)need_num;

}

BOOL ItemManager::IsExistItemDup	(SLOTINDEX         idx
                                 ,SLOTCODE        code
											,BYTE            num
											,SLOTPOS * OUT   pPosArray
											,SLOTPOS & INOUT posNum)
{
	SLOTPOS maxPos = posNum;
	BaseContainer * pContainer = GetItemSlotContainer( idx );
	posNum = 0;
	for( SLOTPOS i = 0 ; i < pContainer->GetSlotMaxSize() ; ++i )
	{
		if( !pContainer->IsEmpty(i) )
		{
			BaseSlot & slot = pContainer->GetSlot(i);
			if( slot.GetCode() == code )
			{
				if( maxPos < posNum ) return FALSE;
				pPosArray[posNum++] = i;
				if( posNum == num ) return TRUE;
			}
		}
	}
	return FALSE;
}

BOOL ItemManager::IsExistItem	(SLOTINDEX         idx
                              ,SLOTCODE        code
										,DURATYPE        dupNum
										,SLOTPOS * OUT   pPosArray
										,SLOTPOS * INOUT pPosNum)
										//,SLOTPOS & INOUT posNum)
{
	INT		InsufficientNum	= dupNum;
	SLOTPOS	maxPos(0);

	if(pPosNum)
	{
		maxPos	= *pPosNum;
		*pPosNum	= 0;
	}

	BaseContainer * pContainer = GetItemSlotContainer( idx );
	for( SLOTPOS i = 0 ; i < pContainer->GetSlotMaxSize() ; ++i )
	{
		if( !pContainer->IsEmpty(i) )
		{
			BaseSlot & slot = pContainer->GetSlot(i);
			if( slot.GetCode() == code )
			{
				if(pPosNum)
				{
					if( maxPos < *pPosNum ) return FALSE;
					pPosArray[(*pPosNum)++] = i;
				}
				ASSERT( slot.GetNum()!= 0 );

				if( InsufficientNum <= slot.GetNum() ) return TRUE;

				InsufficientNum -= slot.GetNum();
			}
		}
	}

	return FALSE;
}

BOOL ItemManager::IsExistItem( SLOTINDEX Index, SLOTCODE code )
{
	BaseContainer * pContainer = GetItemSlotContainer(Index);
	for( SLOTPOS pos = 0 ; pos < pContainer->GetSlotMaxSize() ; ++pos )
	{
		const BaseSlot & slotDat = pContainer->GetSlot(pos);
		if( slotDat.GetCode() == code )
		{
			return TRUE;
		}
	}

	return FALSE;
}


};//namespace test

#include "stdafx.h"
#include "RegenLocationInstance.h"
#include "RegenParser.h"
//#include "ObjectGroupManager.h"


RegenLocationInstance::RegenLocationInstance()
{
}

RegenLocationInstance::~RegenLocationInstance()
{
}

VOID RegenLocationInstance::Init( sREGEN_INFO *pRegenInfo, DWORD dwAreaKey )
{
	SetRegenInfo( pRegenInfo );
	m_dwAreaKey = dwAreaKey;

	for( int i = 0; i < MAX_REGEN_MONSTER_KIND; ++i )
	{
		if(pRegenInfo->dwMonCode[i] > 0)
			AddAliveNPC(pRegenInfo->dwMonCode[i],0);
	}
}

VOID RegenLocationInstance::Release()
{
	ClearAlives();

	m_pRegenInfo = NULL;
}



VOID RegenLocationInstance::OnCreateNPC( DWORD dwNPCCode, DWORD dwNum )
{
	_SUPER::OnCreateNPC(dwNPCCode,  dwNum);
}

VOID RegenLocationInstance::OnRemoveNPC( DWORD dwNPCCode, DWORD dwNum )
{
	_SUPER::OnRemoveNPC(dwNPCCode,  dwNum);
}

ObjectGroup* RegenLocationInstance::JoinGroup( NPC* /*pNPC*/ )
{
	//for( int i = 0; i < MAX_OBJECTGROUP_PER_AREA; ++i )
	//{
	//	if( !m_ObjectGroups[i] )
	//		continue;
	//	if( m_ObjectGroups[i]->AddObject(NPC_OBJECT
 //                                     ,pNPC
	//											  ,pNPC->GetBaseInfo()->m_MonsterCode ))
	//	{
	//		return m_ObjectGroups[i];
	//	}
	//}

	return NULL;
}

DWORD RegenLocationInstance::GetRandomArea()
{
	int iMaxIndex		= GetAreaAmount() - 1;
	int iSelectIndex	= math::Random( 0, iMaxIndex );
	return GetAreaAt(iSelectIndex);
}

















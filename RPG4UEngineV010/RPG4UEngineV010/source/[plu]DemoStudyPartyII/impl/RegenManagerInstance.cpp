/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenManagerInstance.cpp
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "RegenManagerInstance.h"
#include "RegenParser.h"
#include "RegenLocationInstance.h"
#include "ObjectSystem.h"
#include "NPCInfoParser.h"
#include "BattleUtil.h"
#include "Monster.h"
#include "TileWorld.h"
#include "ObjectManager.h"
#include "GameInPlayingTestingData.h"
#include "RegenManagerInstance.inc.h"

using namespace util;
using namespace testing;
const DWORD	REGEN_DELAY		= 15*1000;


RegenManagerInstance::RegenManagerInstance(  )
{
}


RegenManagerInstance::~RegenManagerInstance()
{
}


VOID RegenManagerInstance::Release()
{
	_SUPER::Release();
}


VOID RegenManagerInstance::Init(DWORD dwMapCode
                       ,DWORD dwFieldIndex
							  ,WORD  wMonType
							  ,DWORD dwRegenPeriod)
{
	if(dwRegenPeriod == 0)
	{
		dwRegenPeriod = REGEN_DELAY;
	}

	//普通生成初始化
	RegenInit	RegenOpr(this
                       ,dwMapCode
							  ,dwFieldIndex
							  ,wMonType);
	theTileMap.ForEachSpecialArea(RegenOpr);

	//随机生成初始化
	RandomRegenInit RandomRegenOpr(this
                                 ,dwMapCode
											,dwFieldIndex
											,wMonType
											,NULL);//m_pField->GetFieldInfo());
	theRegenParser.ForEachRandRegen(RandomRegenOpr);

	_SUPER::Init(dwMapCode
               ,dwFieldIndex
					,wMonType);
}


RegenLocation*	RegenManagerInstance::AllocLocation	()
{
	return new RegenLocationInstance;
}

void RegenManagerInstance::FreeLocation	(RegenLocation* pLocation)
{
	SAFE_DELETE(pLocation);
}

//VOID RegenManagerInstance::OnInitRandomRegen()
//{
//}

VOID RegenManagerInstance::Update()
{
	if( !m_RegenTimer.IsExpired() )
		return;

	ProcessRegen();
}

VOID RegenManagerInstance::CreateNPC( DWORD dwCode, DWORD dwNumberOfNPCs, Vector3D *pvPos, RegenLocation *pHomeLocation, DWORD dwGroupID )
{
	sNPCINFO_BASE* pBaseNPCInfo = theNPCInfoParser.GetMonsterInfo( dwCode );
	if( !pBaseNPCInfo )
	{
		LOGINFO( "[RegenManagerInstance::CreateNPC] NPC生成失败.(%d)", dwCode );
		return;
	}

	//生成怪物团体
	ObjectGroup *pObjectGroup = NULL;
	if( dwGroupID )
	{
		//pObjectGroup = m_pField->AllocGroup(pHomeLocation->GetRegenInfo()->tagLocation.m_ID
                                         //,dwGroupID)			;
	}

	SLocationInfo	info={0};
	//生成指定数量的怪物
	Monster *pMonster;
	for( DWORD i = 0; i < dwNumberOfNPCs; ++i )
	{

		info.vRegenPos				= GetRandomPosInTile( pHomeLocation->GetRandomArea() );
		info.MonsterRegenCode	= dwCode;
		theGameInPlayingTestingData.ProcessMonster(info);
		//pMonster = (Monster*)theObjectManager.Add	(dwMonsterID
		//														,MONSTER_OBJECT
		//														,dwCode);
		//pMonster = (Monster*)theObjectSystem.AllocObject( MONSTER_OBJECT );

		//pMonster->SetBaseInfo( pBaseNPCInfo );

		//m_pField->EnterField( pMonster, pvPos );

		//
		//pMonster->SetRegenLocation( pHomeLocation );

		//若是团体怪，则加入团体中
		if( pObjectGroup )
		{
			pObjectGroup->AddObject( MONSTER_OBJECT, pMonster, dwCode );
			pMonster->JoinGroup( pObjectGroup );
		}
		else
		{
			//生成点增加怪物数量
			pHomeLocation->OnCreateNPC( pMonster->GetBaseInfo()->m_MonsterCode );
		}
	}
}

Vector3D RegenManagerInstance::GetRandomPosInTile(DWORD dwTile)
{
	if(!m_pField)
		return Vector3D::ZERO;
	return  theTileMap.GetRandomPosInTile( dwTile );
}

ObjectGroup*	RegenManagerInstance::FindGroup(DWORD dwAreaID, DWORD dwGroupID )
{
	if(!m_pField)
		return NULL;
	return m_pField->FindGroup(dwAreaID, dwGroupID );
}


DWORD RegenManagerInstance::GetRegenAmount(RegenLocation* pLocation
                                         ,DWORD          dwNPCCode
													  ,DWORD          dwMaxNum)
{
	return _SUPER::GetRegenAmount(pLocation
                                ,dwNPCCode
										  ,dwMaxNum);
}

void	RegenManagerInstance::OnSendBossRegen(DWORD dwBossCode,Vector3C& vPos)
{
	MSG_CG_SYNC_BOSS_MONSTER_ENTER_BRD brdMsg;
	brdMsg.m_dwBossCode	= dwBossCode;
	brdMsg.m_vCurPos		= vPos;
	m_pField->SendToAll( &brdMsg, sizeof(brdMsg) );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class RegenManagerInstanceProcessRegenOpr
{
public:
	sREGEN_INFO*&				m_pRegenInfo;	//怪物生成资源表
	RegenLocation*&			m_pLocation;	//当前生成点
	Field*						m_pField;
	RegenManagerInstance*	m_pRegenMan;
	BOOL							bSpawnAll;

	BOOL operator()(RegenLocation	*pLocation)
	{
		m_pLocation = pLocation;
		m_pRegenInfo = m_pLocation->GetRegenInfo();

		/// 1.获取随机区域怪物生成
		BOOL bBossRegen = FALSE;
		SpecialArea *pBaseAreaInfo = m_pField->GetFieldInfo()->GetAreaInfo( m_pLocation->GetAreaKey() );
		if( !pBaseAreaInfo )
		{
			bBossRegen = TRUE;
			pBaseAreaInfo = m_pField->GetFieldInfo()->GetAreaInfo( m_pLocation->GetRandomArea() );
			if( !pBaseAreaInfo )
			{
				LOGMSG( LOG_FULL
						,  "[RegenManagerInstance::ProcessRegen] Regen.txt 配置信息有错... 区域号未找到%s\n"
						, TAG2STR(m_pLocation->GetRandomArea()));
				return TRUE;
			}
		}

		if( pBaseAreaInfo ->m_bvData.m_eBvType != BVT_PATH_TILE_INDEX )
		{
			LOGMSG( LOG_FULL,  "[RegenManagerInstance::ProcessRegen] Regen AreaType is not Tile! " );
			return TRUE;
		}

		DWORD dwAreaTileCnt = pBaseAreaInfo->m_bvData.m_PathTileIndex.m_nCount;
		if( dwAreaTileCnt )
		{
			TILEINDEX *pdwTileIndex = (TILEINDEX*)pBaseAreaInfo->m_bvData.m_PathTileIndex.m_pdwData;

			//生成怪物群
			m_pRegenMan->_ProcessRegenGroup( bBossRegen, dwAreaTileCnt, pdwTileIndex );

			//生成单只怪物
			m_pRegenMan->_ProcessRegenSingle( bSpawnAll, dwAreaTileCnt, pdwTileIndex );
		}
		return TRUE;
	}
};

VOID RegenManagerInstance::ProcessRegen( BOOL bSpawnAll )
{
	RegenManagerInstanceProcessRegenOpr opr=	{m_pRegenInfo
															,m_pLocation
															,m_pField
															,this
															,bSpawnAll	};
	ForEachLocation(opr);
}























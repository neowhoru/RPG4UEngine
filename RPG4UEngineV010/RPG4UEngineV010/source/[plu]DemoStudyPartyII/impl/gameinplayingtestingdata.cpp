#include "stdafx.h"
#include "GameInPlayingTestingData.h"
#include "GameInPlaying.h"
#include "ScriptManager.h"
#include "V3DGameWorld.h"
#include "MapViewManager.h"
#include "PathGameMap.h"
#include "V3DAshesFloatManager.h"
#include "VHero.h"
#include "RenderUtil.h"
#include "GameMainUtil.h"
#include "MapViewManager.h"
#include "VObjectManager.h"
#include "RenderUtil.h"
#include "GameMainUtil.h"
#include "ScreenTipManager.h"
#include "VObject.h"
#include "VHeroActionInput.h"
#include "VItem.h"
#include "IV3DRenderer.h"
#include "IV3DEngine.h"
#include "GameUIManager.h"
#include "ObjectManager.h"
#include "HeroData.h"
#include "VHeroInputLayer.h"
#include "Hero.h"
#include "CharActionInfo.h"
#include "Monster.h"
#include "ApplicationBase.h"
#include "CloneManager.h"
#include "Clone.h"
#include "MathInc.h"
#include "V3DHugeModelManager.h"
#include "TileWorldClient.h"
#include "ObjectManager.h"
#include "TableTextFile.h"
#include "SkillInfoParser.h"
#include "HeroActionInput.h"
#include "BaseContainer.h"
#include "ItemInfoParser.h"
#include "SkillEffectManager.h"
#include "SkillEffect.h"
#include "StructInPacket.h"
#include "FormularManager.h"
#include "PacketStruct_ClientGameS.h"
#include "ItemSlotContainer.h"
#include "ItemSlot.h"
#include "Item.h"
#include "ResultCode.h"
#include "Player.h"
#include "WareHouseDialog.h"
#include "ShopInfoParser.h"
#include "ItemCompositeParser.h"
#include "TestingItemManager.h"
#include "ItemManager.h"
#include "TestingPlayerManager.h"
//#include "GameInScriptManager.h"
#include "GameInDropManager.h"
#include "ItemLogManager.h"
#include "ApplicationSetting.h"
#include "PathInfoParser.h"
#include "ResPublishManager.h"
#include "TipLogManager.h"
#include "InputLayer.h"
#include "NetworkLayer.h"
#include "GameUtil.h"
#include "HeroParty.h"
#include "GameParameter.h"
#include "TradeDialog.h"
#include "HeroTipLayer.h"
#include "PacketStruct_ClientGameS_Player.h"
#include "ShopInfoParser.h"
#include "ConstTextRes.h"
#include "TextResmanager.h"
#include "MapNPC.h"
#include "PacketStruct_ClientGameS_Event.h"
#include "BattleUtil.h"


//#define	TEST_ALLDEAD


#ifndef NONE_MONSTERTESTING


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace input;
using namespace util;
using namespace testing;
using namespace ::singleton;
GLOBALINST_SINGLETON_IMPL(GameInPlayingTestingData, ()  , gamemain::eInstPrioGameFunc);


const DWORD BASE_MAPX			= 4 * 128;
const DWORD BASE_MAPY			= 4 * 128;
const DWORD TILE_GEGEN_RANGE	= 3;

#ifdef TEST_ALLDEAD
const DWORD REGEN_INTERVAL		= 0xfffffff;
#else
const DWORD REGEN_INTERVAL		= 5000;
#endif

static D3DVECTOR	 gs_Areas[]=
{
	 {(75 + BASE_MAPX) * TILE_3DSIZE	,(96 + BASE_MAPX) * TILE_3DSIZE, 11}
	,{(89 + BASE_MAPX) * TILE_3DSIZE	,(103 + BASE_MAPX) * TILE_3DSIZE, 12}
};

const int	MONSTER_INFO_BASE	= 1;
const DWORD ATTACK_DELAY		= 300;



GameInPlayingTestingData::GameInPlayingTestingData()
{
	ACTION_TIME_DIFF			= 0;	/// 50ms时间差异
	WALK_TIME_LENGTH			= 3000;	/// 50ms时间差异
	MOVE_RATE					= 30;	/// 
	g_nCurrentMon				= 0;	/// 
	m_dwRegenBaseID			= 100;
	m_nAttackMode				= 2;
	m_dwDelayAttackTimer		= 0;
}


//System函数
BOOL GameInPlayingTestingData::Init()							
{
	//__BOOL_SUPER(Init());
	__INIT(LoadRegenInfo("Demo\\StudyPartyIII\\RegenInfo.info") );

	return TRUE;
}

void GameInPlayingTestingData::Release()	
{
	//_SUPER::Release();
	m_arLocations.clear();
	assert(m_arPlayerLocations.size() == 0);

}

BOOL GameInPlayingTestingData::RequestWorkingData()	
{

	theHero.SetItemManager( &theTestingItemManager);
	theTestingItemManager.SetPlayer( &theHero);

	//theHeroData.AddEffectInfosTo(&theHero);
	CreateAllPlayers();

	theHeroParty.ResetPartyTo(NULL);

	return TRUE;
}

void GameInPlayingTestingData::ReleaseWorkingData()	
{
	for(UINT n=0; n<m_arLocations.size(); n++)
	{
		theObjectManager.Delete(m_arLocations[n].MonsterCode);
	}
	//m_arLocations.clear(); 这里不用清除...


	for(UINT n=0; n<m_arPlayerLocations.size(); n++)
	{
		theObjectManager.Delete(m_arPlayerLocations[n].MonsterCode);
	}
	m_arPlayerLocations.clear();

	//_SUPER::ReleaseWorkingData();
}


BOOL GameInPlayingTestingData::FrameMove(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(FrameMove(dwTick));

	ProcessRegen();
	ProcessSkillEffects();
	//CreateMonsters();


	///////////////////////////////////////
	//testing
	///P切换攻击状态
	//if (	theInputLayer.IsKeyUp(KEYC_O))
	//{
	//	static INT nCount=0;
	//	OUTPUTTIPF("%d",nCount++);
	//}

//#ifdef _DEBUG
	if (	theInputLayer.IsKeyUp(KEYC_J))
	{
		Object*	pObject;
		pObject = theObjectManager.GetObject("猪八戒");
		if(pObject && pObject->IsCharacter())
		{
			Character*	pChr = (Character*)pObject;
			//pChar->GetVCharacter()->Jump();

			////////////////////////////////////////
			PLAYER_ACTION action;
			ZeroMemory(&action,sizeof(action));

			action.ActionID						= ACTION_JUMP;
			//action.STOP.vCurPos.wvPos			= vCurPos;
			//action.STOP.vCurPos.m_TileIndex	= -1;

			if (theGeneralGameParam.IsServerDebug()) 
			{
				pChr->DoAction(&action);	
				pChr->ForceStateChange(g_CurTime);	
			}
			else
			{
				pChr->PutAction(&action);
			}

		}
		//theVHero.Jump();
	}
//#endif

	///////////////////////////////////////
	//testing
	///P切换攻击状态
	if (	theInputLayer.IsKeyUp(KEYC_P))
	{
		Character*		pChar;
		pChar = (Character*)theObjectManager.GetObject(theHeroActionInput.GetCurrentTarget());
		if(pChar && pChar->IsCharacter())
			theHeroData.AddEffectInfosTo(pChar);
		else
			theHeroData.AddEffectInfosTo(&theHero);
	}



	if(theInputLayer.IsKeyDown(KEYC_NUMPAD7))
	{
		gTrueTime.WriteLog();
		static DWORD	s_dwConter=0;
		OUTPUTCHATF("%u.性能剖析输出\n",s_dwConter++);

#ifdef TEST_ALLDEAD
		Monster*	pMonster;

		MSG_CG_BATTLE_PLAYER_ATTACK_SYN	msgAttack;


	//if(theGeneralGameParam.IsEnableNetwork())
	//{
		//if( FALSE == pHero->SendPacket(&msgAttack
		//										,sizeof(MSG_CG_BATTLE_PLAYER_ATTACK_SYN)
		//										,TRUE)	)
		static DWORD	s_nCounter=100;

		for(UINT n=0;n < m_arLocations.size(); n++ )
		{
			SLocationInfo& info = m_arLocations[n];

			msgAttack.dwTargetKey		= info.MonsterCode;		

			pMonster		= (Monster*)theObjectManager.GetObject(info.MonsterCode);
			if(!pMonster)
				continue;

			msgAttack.byAttackType		= 0;//theHero.GetAttackSequence();	
			msgAttack.vCurPos				= pMonster->GetPosition();			
			msgAttack.vDestPos			= pMonster->GetPosition();
			msgAttack.StyleCode			= (SLOTCODE)60100;		
			msgAttack.dwClientSerial	= s_nCounter++;
			ProcessBattlePacket(&msgAttack,&theHero);
		}
#endif

		//theItemLogManager.SetEnableLog(TRUE);
		//theItemLogManager.PushMoney(123 );
		//theItemLogManager.PushExp(-123 );
		//theItemLogManager.PushSkillExp(123 );
		//theItemLogManager.SetEnableLog(FALSE);

		//static int m_nVMId = -1;
		//LOGINFO("******* 测试脚本\n");

		//LPCSTR szScriptName = "Script\\DoTesting.s";
		//if ( m_nVMId == -1 )
		//{
		//	LOGINFO( "准备运行脚本:%s", szScriptName );
		//	m_nVMId = GetScriptManager().CreateVM( SCRIPTTYPE_NPC, szScriptName, -1 );
		//}

		//if ( m_nVMId != -1 )
		//{
		//	GetScriptManager().StartupVM( SCRIPTTYPE_NPC, m_nVMId, &theHero );
		//}
		//GetScriptManager().StartupVM( SCRIPTTYPE_NPCDEAD, m_nVMId, pPlayer );
	}



	static DWORD sdwAttackAuto = g_CurTime;


	if(theInputLayer.IsKeyUp(KEYC_F12) && (theInputLayer.IsPressKey(KEYC_LCONTROL) || theInputLayer.IsPressKey(KEYC_RCONTROL)))
	{
		static DWORD sTimer = 0;
		if(g_CurTime - sTimer > 30*1000)
		{
			sTimer = g_CurTime;
			LOGTIP("开始发布资源\n",eTIP_TAKE,NULL);
			theResPublishManager.BeginPublish(TRUE);
			LOGTIP("开始发布结束\n",eTIP_TAKE,NULL);
		}
		else
		{
			LOGTIP("发布资源时间未到\n",eTIP_NORMAL,NULL);
		}
	}

	if(theInputLayer.IsKeyDown(KEYC_NUMPAD0))
	{
		m_nAttackMode = 0;
		LOGINFO("******* PAUSE继续增加攻击测试\n");
	}

	if(theInputLayer.IsKeyDown(KEYC_NUMPAD1))
	{
		m_nAttackMode = 1;
		LOGINFO("******BEGIN增加攻击测试 %d\n",m_nAttackMode);
	}
	if(theInputLayer.IsKeyDown(KEYC_NUMPAD2))
	{
		m_nAttackMode = 2;
		LOGINFO("******BEGIN增加攻击测试 %d\n",m_nAttackMode);
	}


	if(theInputLayer.IsKeyDown(KEYC_NUMPAD4))
	{
		ACTION_TIME_DIFF += 100;
		if(ACTION_TIME_DIFF >= 2000)
			ACTION_TIME_DIFF = 2000;
		//LOGINFO("******ACTION_TIME_DIFF + 100 = %d ,  MOVE_RATE : %d\n",ACTION_TIME_DIFF,MOVE_RATE );
	}

	if(theInputLayer.IsKeyDown(KEYC_NUMPAD6))
	{
		ACTION_TIME_DIFF -= 100;
		if(ACTION_TIME_DIFF < -2000)
			ACTION_TIME_DIFF = -2000;
		//LOGINFO("******ACTION_TIME_DIFF - 100 = %d ,  MOVE_RATE : %d\n",ACTION_TIME_DIFF,MOVE_RATE );
	}

	if(theInputLayer.IsKeyDown(KEYC_NUMPAD5))
	{
		MOVE_RATE = rand()%100;
		//LOGINFO("******ACTION_TIME_DIFF = %d ,  MOVE_RATE : %d\n",ACTION_TIME_DIFF,MOVE_RATE );
	}

	if(theInputLayer.IsKeyDown(KEYC_NUMPAD8))
	{
		static DWORD dwStatusID = 5;
		SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	
		pEffect->dwSkillID		= 0;
		pEffect->dwStatusID		= dwStatusID++;	
		pEffect->iRemainTime		= (rand()%15 + 10)* 1000;
		theHero.AddSkillEffect(&pEffect);
		if(dwStatusID >= 11)
			dwStatusID = 1;
	}


					 //if( theHeroActionInput.GetCurrentTarget() == 0 )
					 //{
						// theHeroActionInput.SetCurrentTarget( pRecvPacket->dwAttackerKey );
					 //}

	//Character*	pHeroTarget = (Character*)theObjectManager.GetObject(theHeroActionInput.GetCurrentTarget());
	//if(pHeroTarget && pHeroTarget->IsKindOfObject(MONSTER_OBJECT))
	//{
	//	MonsterDoAttack((Monster*)pHeroTarget,&theHero);
	//}
	//else
	//	pHeroTarget = NULL;


	if(	g_CurTime - sdwAttackAuto >= ATTACK_DELAY 
		&& (m_nAttackMode/* || theHeroActionInput.GetCurrentTarget() != 0*/ )	)
	{
		sdwAttackAuto	= g_CurTime;

		for(UINT n=0; n<m_arLocations.size(); n++)
		{
			if(theHeroActionInput.GetCurrentTarget() == m_arLocations[n].MonsterCode)
				MonsterDoAttack(m_arLocations[n],&theHero);

			if(m_nAttackMode == 2)
				MonsterDoAttack(m_arLocations[n], NULL);

			//pChr->ProcessAttackResult(attackresult.dwAttackSerial,TRUE);
		}
	}

	if(theInputLayer.IsKeyUp(KEYC_NUMPAD9))
	{
		for(UINT n=0; n<m_arLocations.size(); n++)
		{
			Monster* pMonster = (Monster*)theObjectManager.GetObject(m_arLocations[n].MonsterCode);//pMonster;
			if(pMonster == NULL)
				continue;
			Vector3D vPos	= pMonster->GetPosition();
			Vector3D vDest = theVHero.GetPosition();
			vDest.x +=  rand()%10;
			vDest.y +=  rand()%10;
			pMonster->SetNextState( STATE_MOVE,g_CurTime );
			pMonster->Move(vPos,vDest,(BYTE)((n&1)?MOVETYPE_RUN:MOVETYPE_WALK),FALSE);
		}
	}

	if(ExistTheHero())
	{
		static DWORD sAutoSaveTimer	= g_CurTime;
		if(g_CurTime - sAutoSaveTimer >= theApplicationSetting.m_AutoSaveDataDelay)
		{
			sAutoSaveTimer	= g_CurTime;
			theHeroData.SaveGameData( GAMEDATA_PATH );
			theHeroData.SaveQuestData( GAMEQUEST_PATH );
		}
	}

	return TRUE;
}


BOOL GameInPlayingTestingData::Render(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(Render(dwTick));

	//if(g_BaseSetting.m_ShowDebugInfo )
	//{
	//	for(int r=0;r<sizeof(gs_Areas)/sizeof(D3DVECTOR);r+=2)
	//	{
	//			theV3DGraphicDDraw.DrawBox3D	(gs_Areas[r]
	//														,gs_Areas[r+1]
	//														,0xffff22ff);
	//	}
	//}

	return TRUE;
}

BOOL GameInPlayingTestingData::LoadRegenInfo(LPCSTR szFile)
{
	TableTextFile	info;
	__CHECK	( info.OpenFile(szFile, TRUE) );

	m_arRegenInfos.clear();

	CODETYPE codeRegen;
	for(;info.GotoNextLine();)
	{
		info.GetNextNumber();	///跳过空列
		codeRegen = 0;
		info >> codeRegen;
		if(codeRegen == 0)
			continue;
		m_arRegenInfos.push_back(SRegenInfo());
		SRegenInfo& regen = m_arRegenInfos.back();

		regen.RegenCode = codeRegen;

		info.GetString();//注释
		info >> regen.fRegenX;
		info >> regen.fRegenY;
		info >> regen.bNearHero;
		info >> regen.MonsterCode;
		info >> regen.nRegenRate;
		info >> regen.dwRegenNum; 

		info >> regen.dwStateCode;       
		info >> regen.dwStateDuration;       


		info >> regen.byCharType;     

		if(regen.byCharType == 0)
		{
			info >>	regen.dropInfo.byDropRate;
			info >>	regen.dropInfo.dwMoneyMin;
			info >>	regen.dropInfo.dwMoneyMax;
			info >>	regen.dropInfo.byItemRate;

						info(DROP_ITEM_MAX);
			info >>	regen.dropInfo.arItemCodes;
						info(0);

			for(UINT n=0; n<regen.dwRegenNum;n++)
			{
				m_arLocations.push_back(SLocationInfo());
				SLocationInfo& location = m_arLocations.back();
				__ZERO(location);
				location.MonsterRegenCode	= regen.MonsterCode;
				location.vRegenPos.x			= regen.fRegenX;
				location.vRegenPos.y			= regen.fRegenY;
				location.vRegenPos.z			= 0;
				location.bNearHero			= regen.bNearHero;
				location.dwStateCode    	= regen.dwStateCode;       
				location.dwStateDuration	= regen.dwStateDuration;       
				location.dropInfo				= regen.dropInfo;
				location.MonsterCode			= INVALID_PLAYERKEY;

			}
			continue;
		}

		info >> regen.sName;
		info >> regen.bySex;
		info >> regen.byFace;
		info >> regen.byHairColor;
		info >> regen.wLevel;
		info >> regen.wMaxHP;  

	}
	return TRUE;
}

BOOL GameInPlayingTestingData::ProcessRegen()
{
	static DWORD sdwTimer = 0;
	if(sdwTimer > 0 && g_CurTime - sdwTimer < REGEN_INTERVAL)
		return FALSE;
	sdwTimer = g_CurTime;

	//TEST_ALLDEAD
	for(UINT n=0;n < m_arLocations.size(); n++ )
	{
		SLocationInfo& info = m_arLocations[n];
		ProcessMonster(info);
	}

	return TRUE;
}


BOOL GameInPlayingTestingData::ProcessStatus()
{
	Object*			pObject;
	Character*		pChar;
	ObjectMapIt	it;
	//SKILL_EFFECT*	pEffect;
	vector<MSG_CG_STATUS_REMOVE_BRD>		arStatus;

	for(it=theObjectManager.GetBegin(); it != theObjectManager.GetEnd();it++)
	{
		pObject = it->second;
		if(!pObject->IsKindOfObject(CHARACTER_OBJECT))
			continue;
		pChar = (Character*)pObject;
	}
	return TRUE;
}



BOOL GameInPlayingTestingData::ProcessSkillEffects()
{
	Object*			pObject;
	Character*		pChar;
	ObjectMapIt	it;
	SKILL_EFFECT*	pEffect;
	vector<MSG_CG_STATUS_REMOVE_BRD>		arStatus;

	for(it=theObjectManager.GetBegin(); it != theObjectManager.GetEnd();it++)
	{
		pObject = it->second;
		if(!pObject->IsKindOfObject(CHARACTER_OBJECT))
			continue;
		pChar = (Character*)pObject;

		pEffect = pChar->GetFirstSkillEffect();
		for(;pEffect;pEffect=pChar->GetNextSkillEffect())
		{
			//if(pEffect->iRemainTime <= 0)
			if(pEffect->IsTimeOut())
			{
				arStatus.push_back(MSG_CG_STATUS_REMOVE_BRD());
				MSG_CG_STATUS_REMOVE_BRD& BrdMsg = arStatus.back();
				BrdMsg.m_byCategory		= CG_STATUS;
				BrdMsg.m_byProtocol		= CG_STATUS_REMOVE_BRD;
				BrdMsg.m_dwTargetKey		= pChar->GetObjectKey();
				BrdMsg.m_dwStatusCode	= pEffect->GetStatusID();
			}
		}
	}

	for(UINT n=0; n<arStatus.size(); n++)
	{
		theNetworkLayer.ParsePacket(&arStatus[n]);
	}
	return TRUE;
}


BOOL GameInPlayingTestingData::MonsterDoAttack(SLocationInfo& info,Character* pTarget)
{
	Monster* pChr;

	pChr = (Monster*)theObjectManager.GetObject(info.MonsterCode);//pMonster;
	if(	pChr == NULL 
		|| !pChr->IsMonster()
		|| pChr->IsDead())
		return FALSE;


	sNPCINFO_BASE* pInfo = pChr->GetMonsterInfo();
	if(pInfo->m_wAttitude <=  1)
		return FALSE;

	//矿石 树木
	if(pInfo->m_dwMaxAttackPower == 0)
		return FALSE;

	Vector3D vPos;
	Vector3D vDest;
	DWORD		dwAttakType;
	BOOL		bChangeMove		= FALSE;

	static BOOL	 bAddMove	= FALSE;
	static BOOL  bMoveMode	= FALSE;
	static DWORD dwActionTime;
	static DWORD sdwAttackNO	= 0;


	//if(pChr->IsMoving())
	//	vPos			= pChr->GetPathExplorer()->GetTargetPos();
	//else
	vPos			= pChr->GetPosition();


	///////////////////////////////////////////
	if(pTarget == NULL)
	{
		const	DWORD	PLAYER_MAX	= 100;
		DWORD		arPlayers[PLAYER_MAX];
		DWORD	dwCount = ObjectUtil::GetAttackTargetWithinRange(vPos,2*TILE_3DSIZE,arPlayers,0,HERO_OBJECT,PLAYER_MAX);
		//if(dwCount == 0)
		//	return FALSE;
		if(dwCount)
			pTarget = (Character*)theObjectManager.GetObject(arPlayers[0]);
		//if(pTarget == NULL)
		//	return FALSE;
	}


	dwAttakType = g_CurTime%3;
	if(pTarget == NULL || dwAttakType >= 3)
	{
		vDest.x = (info.vRegenPos.x	+ math::Random(TILE_GEGEN_RANGE * 2))*TILE_3DSIZE -TILE_GEGEN_RANGE/2*TILE_3DSIZE;
		vDest.y = (info.vRegenPos.y	+ math::Random(TILE_GEGEN_RANGE * 2))*TILE_3DSIZE -TILE_GEGEN_RANGE/2*TILE_3DSIZE;
		vDest.z	= theGameWorld.GetHeightAt(10.f,1000.f,vDest.x,vDest.y);

		//bMoveMode = TRUE;
		bChangeMove		= TRUE;
	}
	else
	{
		vDest				= pTarget->GetPosition();
		bChangeMove		= FALSE;
	}

	//////////////////////////////////////////////////
	if(!bMoveMode)
	{
		//sNPCINFO_BASE *	pInfo = pChr->GetMonsterInfo();
		dwActionTime	= pInfo->m_wAttSpeed[dwAttakType] + ACTION_TIME_DIFF ;	///模拟服务端与客户端时间差异
		if(dwActionTime < 0)
			dwActionTime = 10;

		dwActionTime = (DWORD)(dwActionTime/pChr->GetAttackSpeedModifier());

		if(	pInfo->m_wMoveAttitude  != 1)
		{
			if(!pChr->IsMoving() && bChangeMove)
			{
				//{
				if(g_CurTime -  info.dwLastTime < dwActionTime)
					return FALSE;
				info.dwLastTime = g_CurTime;

				//}
				if(util::DrawLots(MOVE_RATE))
				{
					bMoveMode		= TRUE;
					bAddMove			= TRUE;
					dwActionTime	= WALK_TIME_LENGTH + ACTION_TIME_DIFF;
				}
				//else
			}
		}
	}

	///////////////////////////////////////////////
	if(!bAddMove)
	{
		if(g_CurTime -  info.dwLastTime < dwActionTime)
			return FALSE;

		bMoveMode		= FALSE;

		info.dwLastTime = g_CurTime;
	}



	PLAYER_ACTION action;
	ZeroMemory(&action,sizeof(action));

	////////////////////////////////////////////////
	if(!bAddMove)
	{
		if(pTarget == NULL)
			return FALSE;
		pChr->StopMove();

		sdwAttackNO		= pChr->GetNextAttackSerial();
		pChr->AddNextAttackSerial();

		DWORD	dwDamage	= math::RandomRange(pInfo->m_dwMinAttackPower,pInfo->m_dwMaxAttackPower);//pTarget->GetMaxMP();
		if(dwDamage > pTarget->GetHP())
			dwDamage = pTarget->GetHP();

		MSG_CG_BATTLE_MONSTER_ATTACK_CMD	msg;
		msg.m_dwObjectKey			= pChr->GetObjectKey();
		msg.m_dwTargetObjectKey	= pTarget->GetObjectKey();
		msg.m_AttackType		= (BYTE)dwAttakType;
		msg.m_vCurPos				= vPos;
		msg.m_wDamage				= (DAMAGETYPE)dwDamage;
		msg.m_wStandDelay			= 0;
		msg.m_wTargetHP			= (HPTYPE)(pTarget->GetHP() - dwDamage);
		msg.m_byEffect				= (g_CurTime&4)?0: ATTACK_ADDITIONAL_EFFECT_CRITICAL;				

		ProcessBattlePacket(&msg,pChr);
		//action.ActionID = ACTION_ATTACK;
		//action.ATTACK.byAttackType		= (BYTE)dwAttakType;	
		//action.ATTACK.dwTargetID		= pTarget->GetObjectKey();			
		//action.ATTACK.vCurPos.wvPos	= vPos; 
		//action.ATTACK.vCurPos.m_TileIndex	= -1;
		//action.ATTACK.vDestPos.wvPos	= vDest;
		//action.ATTACK.vDestPos.m_TileIndex	= -1;
		//action.ATTACK.dwAttackSerial	= sdwAttackNO;
		//action.ATTACK.bAttackMove		= FALSE;
		//action.ATTACK.dwAttackStyle	= 9999;

		////pChr->DoAction(&action);
		////pChr->ForceStateChange(g_CurTime);	

		//pChr->PutAction(&action);


		//ATTACK_RESULT attackresult;
		//ZeroMemory(&attackresult,sizeof(attackresult));
		//attackresult.dwAttackSerial = sdwAttackNO;
		//attackresult.dwTimeSerial   = 0;
		//attackresult.dwTimeStart    = g_CurTime;
		//attackresult.dwGroupId      = 1;
		//attackresult.dwWeaponSoundKind			= (eWEAPONSOUNDKIND)0;
		//attackresult.TargetInfo.dwTargetID		= pTarget->GetObjectKey();
		//attackresult.TargetInfo.wDamage			= rand()%1000;
		//attackresult.TargetInfo.dwTargetHP		= 1454;
		//attackresult.TargetInfo.dwAdditionalEffect = (g_CurTime&4)?0: ATTACK_ADDITIONAL_EFFECT_CRITICAL;
		//attackresult.TargetInfo.bNoDamage      = FALSE;
		//attackresult.dwSkillID						= 0;

		//pChr->AddAttackResult(&attackresult);

		//LOGINFO	(" *** Add Attack AttackSerial:%d \n"
		//			//,sdwCounter
		//			,sdwAttackNO);
	
		//sdwAttackNO ++;
	}
	else
	{
		static int sMoveNum = 0;

		bAddMove = FALSE;
		pChr->ReleaseMoveTarget();

		pChr->SetNextState( STATE_MOVE,g_CurTime );
		pChr->Move(pChr->GetPosition(), vDest, MOVETYPE_WALK,FALSE);

		/*
		action.ActionID				= ACTION_MOVE;
		action.MOVE.bThrust			= TRUE;
		action.MOVE.byState			= MOVETYPE_RUN;
		action.MOVE.vCurPos.wvPos	= pChr->GetPosition();						
		action.MOVE.vCurPos.m_TileIndex	= theTileMap.GetTileToStand(action.MOVE.vCurPos.wvPos);;						
	//	//action.MOVE.vCurPos.wvPos.x	= theHero.GetPosition().x + (rand()%3 )*TILE_3DSIZE;
	//	//action.MOVE.vCurPos.wvPos.y	= theHero.GetPosition().y + (rand()%3  )*TILE_3DSIZE;
	//	//action.MOVE.vCurPos.wvPos.z	= theHero.GetPosition().z + 1.0f;
	//	action.MOVE.vCurPos.wvPos	= theHero.GetPosition();
	//	action.MOVE.vCurPos.m_TileIndex	= -1;

	//	action.MOVE.vDestPos.wvPos	= theHero.GetPosition();
	//action.MOVE.vDestPos.wvPos.x	= pTarget->GetPosition().x + (rand()%2  )*TILE_3DSIZE;
	//action.MOVE.vDestPos.wvPos.y	= pTarget->GetPosition().y + (rand()%2  )*TILE_3DSIZE;
	//action.MOVE.vDestPos.wvPos.z	= theGameWorld.GetHeightAt(pTarget->GetPosition().z
	//																,100
	//																,action.MOVE.vDestPos.wvPos.x
	//																,action.MOVE.vDestPos.wvPos.y
	//																,0);
	action.MOVE.vDestPos.wvPos	= vDest;
	//	action.MOVE.vDestPos.m_TileIndex	= -1;

		LOGINFO	(" *** Add Move  at [%d]  \n"
					//,sdwCounter
					,sMoveNum);

		sMoveNum++;
		pChr->PutAction(&action);
		*/
	}
	return TRUE;
}

BOOL GameInPlayingTestingData::ProcessMonster(SLocationInfo& info)
{
	DWORD		dwMonsterID;
	Monster*	pMonster;


	dwMonsterID = info.MonsterCode;
	pMonster		= (Monster*)theObjectManager.GetObject(dwMonsterID);
	if(pMonster != NULL)
	{
		if(pMonster->GetHP() <= 0)
		{
			if(!info.bDropItemed)
			{
				theGameInDropManager.MonsterDeath(dwMonsterID, theHero.GetObjectKey());
				CreateItem(info);
			}
		}
		return TRUE;
	}

	if(dwMonsterID == INVALID_PLAYERKEY)
		dwMonsterID	= m_dwRegenBaseID++;

	return CreateMonster(info,dwMonsterID);
}

BOOL GameInPlayingTestingData::CreateMonster(SLocationInfo& info,DWORD dwMonsterID)
{
	///////////////////////////////////////////
	//leo testing 怪物生成测试
	
	Monster*	pMonster;
	Vector3D vPos = theHero.GetPosition();
	Vector3D vPos2;
	Vector3D vTo;

	vPos2 = vPos;

	pMonster = (Monster*)theObjectManager.Add	(dwMonsterID
															,MONSTER_OBJECT
															,info.MonsterRegenCode);
	if(pMonster)
	{
		info.bDropItemed	= FALSE;
		if(info.bNearHero)
		{
			vPos2.x				= vPos.x + info.vRegenPos.x*TILE_3DSIZE;
			vPos2.y				= vPos.y + info.vRegenPos.y*TILE_3DSIZE;
			info.vRegenPos.x	= vPos2.x / TILE_3DSIZE;
			info.vRegenPos.y	= vPos2.y / TILE_3DSIZE;
			info.bNearHero	= FALSE;
		}
		else
		{
			vPos2.x = info.vRegenPos.x*TILE_3DSIZE;
			vPos2.y = info.vRegenPos.y*TILE_3DSIZE;
		}
		vPos2.z	= theGameWorld.GetHeightAt(10.f,1000.f,vPos2.x,vPos2.y);

		//pMonster->SetPosition(vPos2 );

		vTo.x = vPos2.x	+ rand() % (int)(TILE_GEGEN_RANGE*TILE_3DSIZE) -TILE_GEGEN_RANGE/2*TILE_3DSIZE;
		vTo.y = vPos2.y	+ rand() % (int)(TILE_GEGEN_RANGE*TILE_3DSIZE) -TILE_GEGEN_RANGE/2*TILE_3DSIZE;
		vTo.z	= theGameWorld.GetHeightAt(10.f,1000.f,vTo.x,vTo.y);

		pMonster->SetHP(pMonster->GetMaxHP());
		pMonster->SetPosition(vTo );

		//VCharacter* pVChar = (VCharacter*)pMonster->GetVObject();
		//pVChar->SetCreatureSound( (char*)pMonster->GetVMonsterInfo()->m_sSoundTempl );

		info.MonsterCode	= pMonster->GetObjectKey();

		if(info.dwStateCode)
		{
			if(info.bNearHero || (g_CurTime&1) )
			{
				SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	
				pEffect->dwSkillID		= 0;
				pEffect->dwStatusID		= info.dwStateCode;	
				pEffect->iRemainTime		= info.dwStateDuration;
				pMonster->AddSkillEffect(&pEffect);
			}
		}
		
	}

	//pMonster->Move(vPos2,vTo,MOVETYPE_RUN,FALSE );


	return TRUE;
}


BOOL GameInPlayingTestingData::CreateAllPlayers()
{
	for(UINT n=0; n<m_arRegenInfos.size();n++)
	{
		SRegenInfo& info = m_arRegenInfos[n];
		if(info.byCharType == 0)
			continue;

		CreatePlayer(info);
	}
	return TRUE;
}

BOOL GameInPlayingTestingData::CreatePlayer(SRegenInfo& info)
{
	Vector3D vPos = theHero.GetPosition();
	Vector3D vPos2;
	vPos2 = vPos;

	if(info.bNearHero)
	{
		vPos2.x = vPos.x + info.fRegenX*TILE_3DSIZE;
		vPos2.y = vPos.y + info.fRegenY*TILE_3DSIZE;
	}
	else
	{
		vPos2.x = info.fRegenX*TILE_3DSIZE;
		vPos2.y = info.fRegenY*TILE_3DSIZE;
	}

	vPos2.x	+= rand() % (int)(TILE_GEGEN_RANGE*TILE_3DSIZE) -TILE_GEGEN_RANGE/2*TILE_3DSIZE;
	vPos2.y	+= rand() % (int)(TILE_GEGEN_RANGE*TILE_3DSIZE) -TILE_GEGEN_RANGE/2*TILE_3DSIZE;

	sPLAYERINFO_RENDER playerInfo;
	__ZERO(playerInfo);
	playerInfo.m_wPlayerKey			= (WORD)m_dwRegenBaseID++;
	playerInfo.m_byClass				= info.byCharType;
	playerInfo.m_bySex				= info.bySex;
	playerInfo.m_wMoveSpeedRatio	= 100;
	playerInfo.m_wAttSpeedRatio	= 100;
	playerInfo.m_byFace				= info.byFace;
	playerInfo.m_byHairColor		= info.byHairColor;
	playerInfo.m_byHeight			= 9;
	playerInfo.m_LV					= info.wLevel;
	playerInfo.m_HP					= info.wMaxHP;
	playerInfo.m_MaxHP				= info.wMaxHP;
	strcpy(playerInfo.m_szName,	(LPCSTR)info.sName);

	Player*	pPlayer		= (Player*)theObjectManager.Add(playerInfo.m_wPlayerKey
													, PLAYER_OBJECT
													, playerInfo.m_byClass);

	pPlayer->SetPosition(vPos2);
	pPlayer->SetShowName(TRUE);
	pPlayer->SetDirection(vPos);	 
	pPlayer->SetPlayerInfo( &playerInfo );

	pPlayer->SetItemManager( &theTestingItemManager);


	m_arLocations.push_back(SLocationInfo());
	SLocationInfo& location = m_arLocations.back();
	location.MonsterCode		= pPlayer->GetObjectKey();
	location.vRegenPos.x		= vPos2.x;
	location.vRegenPos.y		= vPos2.y;
	location.vRegenPos.z		= 0;
	location.bNearHero		= info.bNearHero;


	if(info.dwStateCode )
	{
		SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	
		pEffect->dwSkillID		= 0;
		pEffect->dwStatusID		= info.dwStateCode;	
		pEffect->iRemainTime		= info.dwStateDuration;
		pPlayer->AddSkillEffect(&pEffect);
	}

	//////////////////////////////////////////
	if(theShopInfoParser.GetShopList(playerInfo.m_wPlayerKey))
	{
		StringHandle	sTitle;
		sTitle.Format(_STRING(TEXTRES_VENDOR_TITLE),pPlayer->GetName());

		pPlayer->SetVendorTitle(sTitle);
		pPlayer->SetBehaveState(PLAYER_BEHAVE_VENDOR_ESTABLISHER);
	}


	return TRUE;
}


BOOL GameInPlayingTestingData::CreateItem(SLocationInfo& info)
{
	info.bDropItemed = TRUE;

	if(rand()%100 >= info.dropInfo.byDropRate)
		return FALSE;

	Object*	pTarget = theObjectManager.GetObject(info.MonsterCode);

	Vector3D vPos = theHero.GetPosition();
	Vector3D vPos2;

	if(pTarget)
		vPos2 = pTarget->GetPosition();
	else
		vPos2 = vPos;

	int nRand;
	nRand		=  rand() % (int)(TILE_GEGEN_RANGE);
	vPos2.x	= vPos2.x + nRand -TILE_GEGEN_RANGE/2;
	nRand		=  rand() % (int)(TILE_GEGEN_RANGE);
	vPos2.y	= vPos2.y + nRand -TILE_GEGEN_RANGE/2;


	/////////////////////////////////
	/// 生成地上物品对象
	MSG_CG_SYNC_FIELDITEM_ENTER_BRD itemMsg;


	itemMsg.m_dwFromMonsterKey		= info.MonsterCode;
	sITEMINFO_RENDER & RenderInfo = itemMsg.m_ItemRenderInfo;

	RenderInfo.m_dwObjectKey		= m_dwRegenBaseID++;
	RenderInfo.m_dwOwnerPlayerKey	= theHero.GetObjectKey();

	
	if(rand()%100 < info.dropInfo.byItemRate)
	{
		DWORD dwIndex = rand()%DROP_ITEM_MAX;
		if(info.dropInfo.arItemCodes[dwIndex] == 0)
			return TRUE;
		if(::theItemInfoParser.GetItemInfo(info.dropInfo.arItemCodes[dwIndex]) == NULL)
			return TRUE;

		ItemSlot	item;
		item.Clear();
		item.SetCode((SLOTCODE)info.dropInfo.arItemCodes[dwIndex]);
		item.SetNum(1);
		//item.SetRank();

		RenderInfo.m_byFieldItemType = sITEMINFO_RENDER::eFIELDITEM_ITEM;
		item.CopyOut(RenderInfo.m_ItemStream);
	}
	else
	{
		RenderInfo.m_byFieldItemType	= sITEMINFO_RENDER::eFIELDITEM_MONEY;
		RenderInfo.m_Money				= math::RandomRange(info.dropInfo.dwMoneyMin, info.dropInfo.dwMoneyMax);
		if(RenderInfo.m_Money == 0)
			return TRUE;
	}

	RenderInfo.m_fPos[0]		= vPos2.x;
	RenderInfo.m_fPos[1]		= vPos2.y;
	RenderInfo.m_fPos[2]		= vPos2.z;

	theNetworkLayer.ParsePacket(&itemMsg);


	return TRUE;
}



BOOL GameInPlayingTestingData::ParsePacket(MSG_BASE*  pMsgBase
                                          ,DWORD      /*dwSize*/
														,Character* pChar)
{

	MSG_OBJECT_BASE* pMsgForward = (MSG_OBJECT_BASE*)pMsgBase;
	pMsgForward->m_dwKey = pChar->GetObjectKey();

	switch(pMsgBase->m_byCategory)
	{
	case CG_STATUS:
		ProcessStatusPacket(pMsgForward,pChar);
		return TRUE;

	case CG_TRADE:
		return ProcessTradePacket(pMsgForward,pChar);

	case CG_EVENT:
		return ProcessEventPacket(pMsgForward,pChar);

	case CG_VENDOR:
		return ProcessVendorPacket(pMsgForward,pChar);

	case CG_PARTY:
		return ProcessPartyPacket(pMsgForward,pChar);

	case CG_WAREHOUSE:
		return ProcessWareHousePacket(pMsgForward,pChar);

	case CG_SKILL:
		return ProcessSkillPacket(pMsgForward,pChar);

	case CG_ITEM:
		return ProcessItemPacket(pMsgForward,pChar);

	case CG_SYNC:
		return ProcessSyncPacket(pMsgForward,pChar);

	case CG_BATTLE:
		return ProcessBattlePacket(pMsgForward,pChar);


	case CG_STYLE:
		return ProcessStylePacket(pMsgForward,pChar);


	default:
		assert(!"未处理的本地Packet");
		break;
	}
	return FALSE;
}


void GameInPlayingTestingData::ProcessStatusPacket	(MSG_OBJECT_BASE* pMsg
																	,Character*        pChar)
{
	__UNUSED(pChar);
	assert(pMsg->m_byCategory == CG_STATUS && pChar);

	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	//Player*	pPlayer = (Player*)pChar;

	switch(pMsg->m_byProtocol)
	{
	case CG_STATUS_RESURRECTION_SYN:
		{
			//MSG_CG_WAREHOUSE_START_ACK msg;
			//msg.m_WarehouseMoney	= theHeroData.GetWareHouseMoney();// + theHero.GetMoney();
			//msg.m_ItemInfo			= theHeroData.GetWareHouseData();
			//theNetworkLayer.ParsePacket(&msg);

			MSG_CG_STATUS_RESURRECTION_SYN * pRecvMsg = (MSG_CG_STATUS_RESURRECTION_SYN *)pMsg;

			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			if( !pPlayer ) return;

			if( pPlayer->OnResurrection( 0.f, 1.f, 1.f ) )
			{

				MSG_CG_STATUS_RESURRECTION_BRD sendMsg;
				sendMsg.m_dwObjectKey	= pPlayer->GetObjectKey();
				sendMsg.m_dwHP				= pPlayer->GetHP();
				sendMsg.m_dwMP				= pPlayer->GetMP();
				sendMsg.m_vCurPos		= pPlayer->GetPosition();

				pPlayer->SendPacketAround( &sendMsg, sizeof(sendMsg) );
			}
			else
			{
				MSG_CG_STATUS_RESURRECTION_NAK NakMsg;
				NakMsg.m_dwErrorCode	= 0;	

				pPlayer->SendPacket( &NakMsg, sizeof(NakMsg) );
			}

		}
		break;
	}
}

BOOL GameInPlayingTestingData::ProcessTradePacket	(MSG_OBJECT_BASE* pMsgBase
																	,Character*        pChar)
{
	assert(pMsgBase->m_byCategory == CG_TRADE && pChar);

	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	Player*	pPlayer = (Player*)pChar;

	switch(pMsgBase->m_byProtocol)
	{
		/////////////////////////////////////////////////
		case CG_TRADE_REQ_SYN:
		{
			//MSG_CG_TRADE_REQ_SYN*	pRecv = (MSG_CG_TRADE_REQ_SYN*)pMsgBase;
			//pRecv->m_dwTargetPlayerKey;

			MSG_CG_TRADE_REQ_CMD cmsg;
			cmsg.m_dwPlayerKey = theHeroActionInput.GetCurrentTarget();//pPlayer->GetObjectKey();
			theNetworkLayer.ParsePacket(&cmsg);
		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_RES_SYN:
		{
			MSG_CG_TRADE_RES_SYN*	pRecvMsg = (MSG_CG_TRADE_RES_SYN*)pMsgBase;

			MSG_CG_TRADE_RES_CMD cmsg;
			if( pRecvMsg->m_Type == MSG_CG_TRADE_RES_SYN::ACCEPT )
			{
				//MSG_CG_TRADE_RES_ACK amsg;
				//theNetworkLayer.ParsePacket(&amsg);
				//pPlayer->SendPacket( &amsg, sizeof(amsg) );

				cmsg.m_Type = MSG_CG_TRADE_RES_CMD::ACCEPT;
			}
			else
			{
				cmsg.m_Type = MSG_CG_TRADE_RES_CMD::REFUSE;
			}
			theNetworkLayer.ParsePacket(&cmsg);
			//pRequestPlayer->SendPacket( &cmsg, sizeof(cmsg) );
		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_PUT_SYN:
		{
			MSG_CG_TRADE_PUT_SYN*	pRecvMsg = (MSG_CG_TRADE_PUT_SYN*)pMsgBase;
			//pRecv->m_dwTargetPlayerKey;
			BaseContainer*	pContainer;
			pContainer = pPlayer->GetSlotContainer(SI_INVENTORY);

			ItemSlot& slot = (ItemSlot&) pContainer->GetSlot(pRecvMsg->m_OrgPos);
			if(slot.GetCode() == 0)
			{
				MSG_CG_TRADE_PUT_NAK nmsg;
				nmsg.m_dwErrorCode = _RC_TRADE(PUT_FAIELD);
				theNetworkLayer.ParsePacket(&nmsg);
				return FALSE;
			}

			/////////////////////////////////
			MSG_CG_TRADE_PUT_CMD cmsg;

			cmsg.m_TradePos	= pRecvMsg->m_TradePos;
			slot.CopyOut(cmsg.m_ItemStream);
			theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			MSG_CG_TRADE_PUT_ACK amsg;
			amsg.m_OrgPos		= pRecvMsg->m_OrgPos;
			amsg.m_TradePos	= pRecvMsg->m_TradePos;
			theNetworkLayer.ParsePacket( &amsg );

		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_PUT_MONEY_SYN:
		{
			MSG_CG_TRADE_PUT_MONEY_SYN*	pRecvMsg = (MSG_CG_TRADE_PUT_MONEY_SYN*)pMsgBase;

			/////////////////////////////////
			MSG_CG_TRADE_PUT_MONEY_CMD cmsg;

			cmsg.m_money	= pRecvMsg->m_money;
			theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			MSG_CG_TRADE_PUT_MONEY_ACK amsg;
			amsg.m_money		= pRecvMsg->m_money;
			theNetworkLayer.ParsePacket( &amsg );

		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_PROPOSAL_SYN:
		{
			//MSG_CG_TRADE_PROPOSAL_MONEY_SYN*	pRecvMsg = (MSG_CG_TRADE_PROPOSAL_MONEY_SYN*)pMsgBase;

			/////////////////////////////////
			MSG_CG_TRADE_PROPOSAL_CMD cmsg;

			theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			MSG_CG_TRADE_PROPOSAL_ACK amsg;
			theNetworkLayer.ParsePacket( &amsg );

		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_MODIFY_SYN:
		{
			//MSG_CG_TRADE_PROPOSAL_MONEY_SYN*	pRecvMsg = (MSG_CG_TRADE_PROPOSAL_MONEY_SYN*)pMsgBase;

			/////////////////////////////////
			MSG_CG_TRADE_MODIFY_CMD cmsg;

			theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			MSG_CG_TRADE_MODIFY_ACK amsg;
			theNetworkLayer.ParsePacket( &amsg );

		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_ACCEPT_SYN:
		{

			MSG_CG_TRADE_TRADE_BRD bmsg1;
			//MSG_CG_TRADE_TRADE_BRD bmsg2;

			bmsg1.m_Money	= theHero.GetMoney() - theTradeDialog.GetMoney1();
			bmsg1.m_TradeInfo.m_InvenCount		= 0;
			bmsg1.m_TradeInfo.m_TmpInvenCount	= 0;

			//BaseContainer*	pContainer;
			//pContainer	= theItemManager.GetContainer(SI_TRADE);

			/////////////////////////////////////////////
			//for (SLOTPOS i=0; i<MAX_TRADE_SLOT_NUM; ++i)
			//{
			//	DummyItemSlot& rSlot = (DummyItemSlot&)pContainer->GetSlot(i);

			//	if (	rSlot.GetCode() == 0 
			//		|| rSlot.GetOrgSlot() == NULL)
			//		continue;
			//	sITEM_SLOTEX& slot = bmsg1.m_TradeInfo.m_Slot[bmsg1.m_TradeInfo.m_InvenCount++];
			//	rSlot.GetOrgSlot()->CopyOut(slot.m_Stream);
			//	slot.m_ItemPos	= rSlot.GetFromPosition() + MAX_INVENTORY_SLOT_NUM_PERPAGE;
			//}


			theNetworkLayer.ParsePacket(&bmsg1);

			/////////////////////////////////
			//MSG_CG_TRADE_ACCEPT_CMD cmsg;

			//theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			//MSG_CG_TRADE_ACCEPT_ACK amsg;
			//theNetworkLayer.ParsePacket( &amsg );

		}
		break;

		/////////////////////////////////////////////////
		case CG_TRADE_SWAP_SYN:
		{
			MSG_CG_TRADE_SWAP_SYN*	pRecvMsg = (MSG_CG_TRADE_SWAP_SYN*)pMsgBase;

			/////////////////////////////////
			MSG_CG_TRADE_SWAP_CMD cmsg;

			cmsg.m_FromPos	= pRecvMsg->m_FromPos;
			cmsg.m_ToPos	= pRecvMsg->m_ToPos	;
			theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			MSG_CG_TRADE_SWAP_ACK amsg;
			amsg.m_FromPos	= pRecvMsg->m_FromPos;
			amsg.m_ToPos	= pRecvMsg->m_ToPos	;
			theNetworkLayer.ParsePacket( &amsg );

		}
		break;
		/////////////////////////////////////////////////
		case CG_TRADE_GET_SYN:
		{
			MSG_CG_TRADE_GET_SYN*	pRecvMsg = (MSG_CG_TRADE_GET_SYN*)pMsgBase;

			/////////////////////////////////
			MSG_CG_TRADE_GET_CMD cmsg;

			cmsg.m_TradePos	= pRecvMsg->m_TradePos;
			//slot.CopyOut(cmsg.m_ItemStream);
			theNetworkLayer.ParsePacket(&cmsg);

			/////////////////////////////////
			MSG_CG_TRADE_GET_ACK amsg;
			//amsg.m_OrgPos		= pRecvMsg->m_OrgPos;
			amsg.m_TradePos	= pRecvMsg->m_TradePos;
			theNetworkLayer.ParsePacket( &amsg );

		}
		break;
	}
	return TRUE;

}

BOOL GameInPlayingTestingData::ProcessEventPacket	(MSG_OBJECT_BASE* pMsgBase
																	,Character*        pChar)
{
	pChar;
	//const	DWORD PRICE_GOODS	= 1000;
	assert(pMsgBase->m_byCategory == CG_EVENT && pChar);

	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	//Player*	pPlayer = (Player*)pChar;

	switch(pMsgBase->m_byProtocol)
	{
		case CG_EVENT_QUESTDATA_CHANGE_SYN:
		{
			MSG_CG_EVENT_QUESTDATA_CHANGE_SYN*	pRecv = (MSG_CG_EVENT_QUESTDATA_CHANGE_SYN*)pMsgBase;


			/////////////////////////////////
			MSG_CG_EVENT_QUESTDATA_CHANGE_CMD amsg;
			amsg.m_QuestID	= pRecv->m_QuestID;
			amsg.m_Data		= pRecv->m_Data	;
			amsg.m_Action	= pRecv->m_Action;
			theNetworkLayer.ParsePacket(&amsg);

		}
		break;
	}
	return TRUE;
}

BOOL GameInPlayingTestingData::ProcessVendorPacket	(MSG_OBJECT_BASE* pMsgBase
																	,Character*        pChar)
{
	const	DWORD PRICE_GOODS	= 1000;
	assert(pMsgBase->m_byCategory == CG_VENDOR && pChar);

	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	Player*	pPlayer = (Player*)pChar;

	switch(pMsgBase->m_byProtocol)
	{
		case CG_VENDOR_VIEW_START_SYN:
		{
			MSG_CG_VENDOR_VIEW_START_SYN*	pRecvMsg = (MSG_CG_VENDOR_VIEW_START_SYN*)pMsgBase;
			MSG_CG_VENDOR_VIEW_START_ACK	amsg;
			ShopDetailInfo*					pShop;
			sTOTALINFO_VENDOR&				items = amsg.m_ItemInfo;

			Player*	pVendor = (Player*)theObjectManager.GetObject(pRecvMsg->m_dwPlayerKey);
			if(!pVendor || !pVendor->IsPlayer())
				return FALSE;

			pShop = theShopInfoParser.GetShopList(pRecvMsg->m_dwPlayerKey);
			if(!pShop)
				return FALSE;

			items.m_Count = 0;
			//pShop->GetItemAt();
			for ( SLOTPOS j = 0; j < ShopDetailInfo::MAX_SELL_ITEM; ++j )
			{
				sSHOP_ITEM * pShopItem = pShop->GetItemAt(0, (BYTE)j);
				if(pShopItem->m_SellItemCode == INVALID_CODETYPE)
					continue;
				sITEMINFO_BASE * pBaseItemInfo = theItemInfoParser.GetItemInfo( pShopItem->m_SellItemCode );
				if(!pBaseItemInfo)
					continue;


				ItemSlot	itemSlot;
				itemSlot.SetCode	( pShopItem->m_SellItemCode );
				itemSlot.SetNum	( pShopItem->m_SellItemNum );
				if(!itemSlot.GetItemInfo())
					continue;

				sVENDOR_ITEM_SLOTEX&	slotAt = items.m_Slot[items.m_Count++];
				itemSlot.CopyOut(slotAt.m_Stream.m_Stream);
				slotAt.m_Money					= PRICE_GOODS;
				slotAt.m_Stream.m_ItemPos	= j;
			}
			lstrcpyn(amsg.m_pszTitle,pVendor->GetVendorTitle(),MAX_VENDOR_TITLE_LENGTH);
			theNetworkLayer.ParsePacket(&amsg);

		}break;

		case CG_VENDOR_VIEW_END_SYN:
		{
			//MSG_CG_VENDOR_END_SYN*	pRecvMsg = (MSG_CG_VENDOR_END_SYN*)pMsgBase;

			MSG_CG_VENDOR_VIEW_END_ACK amsg;
			theNetworkLayer.ParsePacket(&amsg);
		}break;

		case CG_VENDOR_BUY_SYN:
		{
			MSG_CG_VENDOR_BUY_SYN*	pRecvMsg = (MSG_CG_VENDOR_BUY_SYN*)pMsgBase;

			MSG_CG_VENDOR_BUY_ACK amsg;

			ShopDetailInfo*	pShop;
			MapNPC*				pNpc;

			pNpc = (MapNPC*)theObjectManager.GetObject(theHeroActionInput.GetCurrentTarget());
			if(!pNpc->IsMapNpc())
				return FALSE;

			pShop = theShopInfoParser.GetShopList(pNpc->GetFuncInfo()->m_ExtraCode);
			if(!pShop)
				return FALSE;

			sSHOP_ITEM * pShopItem = pShop->GetItemAt(0, (BYTE)pRecvMsg->m_VendorPos);
			if(pShopItem->m_SellItemCode == INVALID_CODETYPE)
				return FALSE;
			sITEMINFO_BASE * pBaseItemInfo = theItemInfoParser.GetItemInfo( pShopItem->m_SellItemCode );
			if(!pBaseItemInfo)
				return FALSE;

			amsg.m_InventoryTotalInfo.m_InvenCount	= 1;

			sITEM_SLOTEX&	slotAt	= amsg.m_InventoryTotalInfo.m_Slot[0];
			BaseContainer*	pInven	= theItemManager.GetContainer(SI_INVENTORY);

			ItemSlot	itemSlot;
			itemSlot.SetCode	( pShopItem->m_SellItemCode );
			itemSlot.SetNum	( pShopItem->m_SellItemNum );
			itemSlot.CopyOut(slotAt.m_Stream);
			if(!pInven->GetEmptyPos(slotAt.m_ItemPos))
				return FALSE;

			amsg.m_Money = pPlayer->GetMoney() - PRICE_GOODS;
			theNetworkLayer.ParsePacket(&amsg);
			//pPlayer->SendPacket(&amsg,amsg.GetSize());

			////////////////////////////////////////
			//MSG_CG_VENDOR_BUY_CMD cmsg;
			//cmsg.m_dwPlayerKey	= pPlayer->GetObjectKey();
			//cmsg.m_VendorPos	= pRecvMsg->m_VendorPos;
			//cmsg.m_Money		= pEstablisher->GetMoney();
			//theNetworkLayer.ParsePacket(&cmsg);

			////////////////////////////////////////
			MSG_CG_VENDOR_BUY_BRD	bmsg;
			bmsg.m_VendorPos	= pRecvMsg->m_VendorPos;
			theNetworkLayer.ParsePacket(&bmsg);

		}break;


		/////////////////////////////////////////////////
		case CG_VENDOR_START_SYN:
		{
			MSG_CG_VENDOR_START_SYN*	pRecvMsg = (MSG_CG_VENDOR_START_SYN*)pMsgBase;

			MSG_CG_VENDOR_START_ACK amsg;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_START_BRD bmsg;
			bmsg.m_dwPlayerKey = pPlayer->GetObjectKey();
			memcpy( bmsg.m_pszTitle, pRecvMsg->m_pszTitle, sizeof(TCHAR)*MAX_VENDOR_TITLE_LENGTH );
			bmsg.m_pszTitle[MAX_VENDOR_TITLE_LENGTH] = '\0';
			theNetworkLayer.ParsePacket(&bmsg);

		}break;

		case CG_VENDOR_END_SYN:
		{
			//MSG_CG_VENDOR_END_SYN*	pRecvMsg = (MSG_CG_VENDOR_END_SYN*)pMsgBase;

			MSG_CG_VENDOR_END_ACK amsg;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_END_BRD bmsg;
			bmsg.m_dwPlayerKey = pPlayer->GetObjectKey();
			theNetworkLayer.ParsePacket(&bmsg);

		}break;

		case CG_VENDOR_MODIFY_SYN:
		{
			MSG_CG_VENDOR_MODIFY_SYN*	pRecv = (MSG_CG_VENDOR_MODIFY_SYN*)pMsgBase;

			BaseContainer*	pInven = theItemManager.GetContainer(SI_INVENTORY);
			ItemSlot& slot = (ItemSlot&)pInven->GetSlot(pRecv->m_ItemSlot.m_OrgPos);

			MSG_CG_VENDOR_MODIFY_ACK amsg;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_MODIFY_BRD bmsg;
			//bmsg.m_dwPlayerKey		= pPlayer->GetObjectKey();
			bmsg.m_VendorPos			= pRecv->m_ItemSlot.m_VendorPos;
			bmsg.m_ItemSlot.m_Money	= pRecv->m_ItemSlot.m_Money;

			bmsg.m_ItemSlot.m_Stream.m_ItemPos	= slot.GetPos();
			slot.CopyOut(bmsg.m_ItemSlot.m_Stream.m_Stream);
			theNetworkLayer.ParsePacket(&bmsg);

		}break;


		case CG_VENDOR_RENAME_SYN:
		{
			MSG_CG_VENDOR_RENAME_SYN*	pRecvMsg = (MSG_CG_VENDOR_RENAME_SYN*)pMsgBase;

			MSG_CG_VENDOR_RENAME_ACK amsg;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_RENAME_BRD bmsg;
			bmsg.m_dwPlayerKey = pPlayer->GetObjectKey();
			memcpy( bmsg.m_pszTitle, pRecvMsg->m_pszTitle, sizeof(TCHAR)*MAX_VENDOR_TITLE_LENGTH );
			bmsg.m_pszTitle[MAX_VENDOR_TITLE_LENGTH] = '\0';
			theNetworkLayer.ParsePacket(&bmsg);

		}break;

		case CG_VENDOR_SWAP_SYN:
		{
			MSG_CG_VENDOR_SWAP_SYN*	pRecvMsg = (MSG_CG_VENDOR_SWAP_SYN*)pMsgBase;

			MSG_CG_VENDOR_SWAP_ACK amsg;
			amsg.m_FromPos	= pRecvMsg->m_FromPos;
			amsg.m_ToPos	= pRecvMsg->m_ToPos	;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_SWAP_BRD bmsg;
			bmsg.m_dwPlayerKey = pPlayer->GetObjectKey();
			bmsg.m_FromPos	= pRecvMsg->m_FromPos;
			bmsg.m_ToPos	= pRecvMsg->m_ToPos	;
			theNetworkLayer.ParsePacket(&bmsg);

		}break;

		case CG_VENDOR_DELETE_SYN:
		{
			MSG_CG_VENDOR_DELETE_SYN*	pRecvMsg = (MSG_CG_VENDOR_DELETE_SYN*)pMsgBase;

			MSG_CG_VENDOR_DELETE_ACK amsg;
			amsg.m_VendorPos = pRecvMsg->m_VendorPos;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_DELETE_BRD bmsg;
			bmsg.m_VendorPos = pRecvMsg->m_VendorPos;
			theNetworkLayer.ParsePacket(&bmsg);

		}break;

		case CG_VENDOR_INSERT_SYN:
		{
			MSG_CG_VENDOR_INSERT_SYN*	pRecv = (MSG_CG_VENDOR_INSERT_SYN*)pMsgBase;
			//pRecv->m_dwTargetPlayerKey;
			BaseContainer*	pInven = theItemManager.GetContainer(SI_INVENTORY);
			ItemSlot& slot = (ItemSlot&)pInven->GetSlot(pRecv->m_ItemSlot.m_OrgPos);

			/////////////////////////////////
			MSG_CG_VENDOR_INSERT_ACK amsg;
			theNetworkLayer.ParsePacket(&amsg);

			/////////////////////////////////
			MSG_CG_VENDOR_INSERT_BRD bmsg;
			bmsg.m_VendorPos			= pRecv->m_ItemSlot.m_VendorPos;
			bmsg.m_ItemSlot.m_Money	= pRecv->m_ItemSlot.m_Money;

			bmsg.m_ItemSlot.m_Stream.m_ItemPos	= slot.GetPos();
			slot.CopyOut(bmsg.m_ItemSlot.m_Stream.m_Stream);
			theNetworkLayer.ParsePacket(&bmsg);
		}
		break;

	}
	return TRUE;

}

BOOL GameInPlayingTestingData::ProcessPartyPacket	(MSG_OBJECT_BASE* pMsgBase
																	,Character*        pChar)
{
	assert(pMsgBase->m_byCategory == CG_PARTY && pChar);

	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	//Player*	pPlayer = (Player*)pChar;

	switch(pMsgBase->m_byProtocol)
	{
		case CG_PARTY_FORCED_EXPULSION_SYN:
		{
			MSG_CG_PARTY_FORCED_EXPULSION_SYN*	pRecv = (MSG_CG_PARTY_FORCED_EXPULSION_SYN*)pMsgBase;

			MSG_CG_PARTY_LEAVE_START_BRD AckMsg;
			AckMsg.m_dwObjKey = pRecv->m_dwObjKey;

			theNetworkLayer.ParsePacket(&AckMsg);
		}
		break;

		case CG_PARTY_LEAVE_REQUEST_SYN:
			{
			MSG_CG_PARTY_LEAVE_START_BRD AckMsg;
			AckMsg.m_dwObjKey = pChar->GetObjectKey();

			theNetworkLayer.ParsePacket(&AckMsg);
		}
		break;

		case CG_PARTY_INVITE_REQUEST_SYN:
		{
			MSG_CG_PARTY_INVITE_RESPONSE_SERVER_SYN msg;

			msg.m_dwMasterUserKey = theGeneralGameParam.GetUserID();
			strncpy(msg.m_szMasterName
					 ,pChar->GetName()
					 ,MAX_CHARNAME_LENGTH);

			theNetworkLayer.ParsePacket(&msg);
		}
		break;

	case CG_PARTY_INVITE_RESPONSE_NAK:
		{
			MSG_CG_PARTY_INVITE_REQUEST_SERVER_NAK NakMsg;
			NakMsg.m_byErrorCode = RC_PARTY_REJECT_INVITEMENT;
			theNetworkLayer.ParsePacket(&NakMsg);
		}
		break;

	case CG_PARTY_INVITE_RESPONSE_ACK:
		{
			//////////////////////////////////////
			//MSG_CG_PARTY_MEMBER_INFO_CMD PartyInfoMsg;
			//PartyInfoMsg.m_dwTargetKeyOfParty	= 0;
			//PartyInfoMsg.m_byMemberNum				= 0;
			//pParty->GetMemberInfoAll( &PartyInfoMsg.m_MemberInfo[0] );

			//theNetworkLayer.ParsePacket(&PartyInfoMsg);


			//////////////////////////////////////
			MSG_CG_PARTY_MEMBER_JOIN_BRD PartyJoinMsg;
			PartyJoinMsg.m_NewMemberInfo.m_dwObjKey = pChar->GetObjectKey();
			strncpy(PartyJoinMsg.m_NewMemberInfo.m_szCharName
                ,pChar->GetName()
					 ,MAX_CHARNAME_LENGTH);
			PartyJoinMsg.m_NewMemberInfo.m_wLevel	= pChar->GetLevel();
			PartyJoinMsg.m_NewMemberInfo.m_dwHP		= pChar->GetHP();
			PartyJoinMsg.m_NewMemberInfo.m_dwHPMax	= pChar->GetMaxHP();
			PartyJoinMsg.m_NewMemberInfo.m_dwMP		= pChar->GetMP();
			PartyJoinMsg.m_NewMemberInfo.m_dwMPMax	= pChar->GetMaxMP();
			PartyJoinMsg.m_NewMemberInfo.m_bMaster = FALSE;

			theNetworkLayer.ParsePacket(&PartyJoinMsg);

		}
		break;


	}
	return TRUE;

}

BOOL GameInPlayingTestingData::ProcessWareHousePacket	(MSG_OBJECT_BASE* pMsgBase
																		,Character*        pChar)
{
	assert(pMsgBase->m_byCategory == CG_WAREHOUSE && pChar);

	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	Player*	pPlayer = (Player*)pChar;

	switch(pMsgBase->m_byProtocol)
	{
	case CG_WAREHOUSE_START_SYN:
		{
			MSG_CG_WAREHOUSE_START_ACK msg;
			msg.m_WarehouseMoney	= theHeroData.GetWareHouseMoney();// + theHero.GetMoney();
			msg.m_ItemInfo			= theHeroData.GetWareHouseData();
			theNetworkLayer.ParsePacket(&msg);
		}
		break;

	case CG_WAREHOUSE_MONEY_SYN:
		{
			MSG_CG_WAREHOUSE_MONEY_SYN* pRecvMsg = (MSG_CG_WAREHOUSE_MONEY_SYN*)pMsgBase;
			BOOL rt = FALSE;
			MONEY money = theHero.GetMoney();
			if( pRecvMsg->m_byType == MSG_CG_WAREHOUSE_MONEY_SYN::PUTMONEY )
			{
				if( pRecvMsg->m_Money <= money )
				{
					money -= pRecvMsg->m_Money;
					theHeroData.SetWareHouseMoney(theHeroData.GetWareHouseMoney() + pRecvMsg->m_Money);
					rt = TRUE;
				}
			}
			else if( pRecvMsg->m_byType == MSG_CG_WAREHOUSE_MONEY_SYN::GETMONEY )
			{
				if(theHeroData.GetWareHouseMoney() >= pRecvMsg->m_Money)
				{
					money += pRecvMsg->m_Money;
					theHeroData.SetWareHouseMoney(theHeroData.GetWareHouseMoney() - pRecvMsg->m_Money);
					rt = TRUE;
				}
				//pPlayer->PlusMoney( pRecvMsg->m_Money );
			}

			if( TRUE == rt )
			{
				MSG_CG_WAREHOUSE_MONEY_ACK msg;
				msg.m_InventoryMoney = money;
				msg.m_WarehouseMoney = theHeroData.GetWareHouseMoney();
				theNetworkLayer.ParsePacket(&msg);
			}
			else
			{
				MSG_CG_WAREHOUSE_MONEY_NAK nmsg;
				nmsg.m_dwErrorCode = RC::RC_WAREHOUSE_INSUFFICIENT_MONEY;
				theNetworkLayer.ParsePacket(&nmsg);
				return TRUE;
			}
		}
		break;

	case CG_WAREHOUSE_END_SYN:
		{
			WareHouseDialog* pBank = (WareHouseDialog*) pPlayer->GetSlotContainer(SI_WAREHOUSE);
			//pBank->SerializeItemInfo(&, SERIALIZE_STORE);
			sTOTALINFO_WAREHOUSE& ItemInfo = theHeroData.GetWareHouseData();

			ItemInfo.m_Count = 0;
			sITEM_SLOTEX * pSlot = ItemInfo.m_Slot;
			SLOTPOS total = 0;
			for(SLOTPOS i=0;i<pBank->GetSlotMaxSize();++i)
			{
				if( !pBank->IsEmpty(i) )
				{
					pSlot[total].m_ItemPos = i;
					pBank->SerializeItemStream(pSlot[total].m_ItemPos, &pSlot[total].m_Stream, SERIALIZE_LOAD);
					++total;
					++ItemInfo.m_Count;
				}
			}

			MSG_CG_WAREHOUSE_END_ACK msg;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;
	}
	return FALSE;
}

BOOL GameInPlayingTestingData::ProcessSkillPacket	(MSG_OBJECT_BASE* pMsgBase
                                                   ,Character*        pChar)
{
	assert(pMsgBase->m_byCategory == CG_SKILL && pChar);

	switch(pMsgBase->m_byProtocol)
	{
	case CG_SKILL_ACTION_SYN:
		{
			const	DWORD	TARGET_MAX	= 100;
			MSG_CG_SKILL_ACTION_SYN& syn = *(MSG_CG_SKILL_ACTION_SYN*)pMsgBase;
			INT	nHP			= 0;
			INT	nMP			= 0;
			BOOL	bCanAttack	= FALSE;
			DWORD arTargets[TARGET_MAX]	= {0};
			DWORD dwTargetNum		= 0;
			DWORD dwTargetMax		= 0;

			BYTE					szBuf[2048]={0};
			Character*			pTarget;
			SkillDetailInfo*	pBaseSkillInfo;
			DWORD					dwActionMsgSize;


			pBaseSkillInfo = theSkillInfoParser.GetSkillInfo(syn.m_SkillCode);

			if(pBaseSkillInfo->m_byTarget == SKILL_TARGET_AREA)
			{
				dwTargetNum = ObjectUtil::GetAttackTargetWithinRange	(syn.m_MainTargetPos
																					,pBaseSkillInfo->m_wSkillArea/10.f
																					,arTargets
																					,0
																					,MONSTER_OBJECT
																					,TARGET_MAX);
			}
			else
			{
				arTargets[0]	= syn.m_dwMainTargetKey;
				dwTargetNum		= 1;
			}
			dwTargetMax			= dwTargetNum;


			if(	pBaseSkillInfo->m_SkillClassCode == SKILLCODE_TELEPORT
				||	pBaseSkillInfo->m_byTarget == SKILL_TARGET_SUMMON)
			{
				arTargets[0]	= syn.m_dwMainTargetKey;
				dwTargetNum		= 1;
				dwTargetMax		= 1;
				//sABILITYINFO_BASE*		pAbilityInfo	= NULL ;
				//INT						nIndex = 0;
				//pBaseSkillInfo->SetFirst();
				//while( pAbilityInfo = pBaseSkillInfo->GetNext() )
				//{
				//	//pResults->m_wAbilityIndex	= nIndex++;
				//	switch(pAbilityInfo->m_wAbilityID)
				//	{
				//	case ABILITY_TELEPORT:
				//		{
				//			MSG_CG_MAP_TELEPORT_BRD msg;
				//			msg.m_byCategory = CG_MAP;
				//			msg.m_byProtocol = CG_MAP_TELEPORT_BRD;
				//			msg.m_dwObjectKey = pChar->GetObjectKey();
				//			msg.vPos				= syn.m_MainTargetPos;
				//			theNetworkLayer.ParsePacket(&msg);
				//		}
				//		break;
				//	}
				//}
				//return TRUE;
			}



			nHP = -pBaseSkillInfo->m_wHPSpend;
			nMP = -pBaseSkillInfo->m_wMPSpend;

			MSG_SKILL_ACTION_BASE& actionBase = *(MSG_SKILL_ACTION_BASE*)szBuf;
			actionBase.m_byCategory				= CG_SKILL;
			actionBase.m_SkillCode				= syn.m_SkillCode;
			actionBase.m_dwClientSerial		= syn.m_dwClientSerial;
			actionBase.m_dwAttackerKey			= pChar->GetObjectKey();
			actionBase.m_dwPrimaryTargetKey	= arTargets[0];//syn.m_dwMainTargetKey;
			actionBase.m_MainTargetPos		= syn.m_MainTargetPos;

			if( !pBaseSkillInfo->m_wSkillCasting && !pBaseSkillInfo->m_wFlyingLifeTime )
			{
				MSG_CG_SKILL_ACTION_INSTANT_RESULT_BRD& actionMsg = *(MSG_CG_SKILL_ACTION_INSTANT_RESULT_BRD*)szBuf;
				dwActionMsgSize						= actionMsg.GetSize();

				actionMsg.m_byProtocol				= CG_SKILL_ACTION_INSTANT_RESULT_BRD;
				actionMsg.m_vCurPos					= syn.m_vCurPos;
				actionMsg.m_vDestPos				= syn.m_vDestPos;
				actionMsg.m_dwAttackerHP			= pChar->GetHP();
				actionMsg.m_dwAttackerMP			= pChar->GetMP() - pBaseSkillInfo->m_wMPSpend;
				actionMsg.m_byNumberOfTargets		= dwTargetNum;
				actionMsg.m_byNumberOfFieldEffect= 0;
				bCanAttack								= TRUE;
			}
			else
			{
				if(m_dwDelayAttackTimer == 0)
				{
					m_dwDelayAttackTimer					= g_CurTime + pBaseSkillInfo->m_wSkillCasting;
					bCanAttack								= FALSE;
					MSG_CG_SKILL_ACTION_DELAY_START_BRD& actionMsg = *(MSG_CG_SKILL_ACTION_DELAY_START_BRD*)szBuf;
					actionMsg.m_byProtocol				= CG_SKILL_ACTION_DELAY_START_BRD;
					dwActionMsgSize						= actionMsg.GetSize();
					actionMsg.m_vCurPos					= syn.m_vCurPos;
					actionMsg.m_vDestPos				= syn.m_vDestPos;
				}
				else
				{
					m_dwDelayAttackTimer					= 0;
					MSG_CG_SKILL_ACTION_DELAY_RESULT_BRD& actionMsg = *(MSG_CG_SKILL_ACTION_DELAY_RESULT_BRD*)szBuf;
					dwActionMsgSize						= actionMsg.GetSize();
					actionMsg.m_byProtocol				= CG_SKILL_ACTION_DELAY_RESULT_BRD;
					actionMsg.m_dwAttackerHP			= pChar->GetHP();
					actionMsg.m_dwAttackerMP			= pChar->GetMP() - pBaseSkillInfo->m_wMPSpend;
					actionMsg.m_byNumberOfTargets		= dwTargetNum;
					actionMsg.m_byNumberOfFieldEffect= 0;
					bCanAttack								= TRUE;
				}
			}


			BOOL bKilled = FALSE;
			if(bCanAttack)
			{
				//DWORD dwExp = 0;
				BYTE* szMsg = szBuf + dwActionMsgSize;
				for(UINT n=0; n<dwTargetMax; n++)
				{
					UINT						uNum		= pBaseSkillInfo->GetAbilityNum();
					SKILL_RESULT_BASE&	resBase	= *(SKILL_RESULT_BASE*)(szMsg);
					DWORD 					wDamage			= 0;
					DWORD 					wPerDamage		= 0;
					DWORD						dwTargetHP;

					pTarget			= (Character*)theObjectManager.GetObject(arTargets[n]);

					dwTargetHP		= pTarget->GetHP();

					resBase.m_dwTargetKey	= arTargets[n];//syn.m_dwMainTargetKey;
					resBase.m_EffectFlag	= 0;
					resBase.m_byAbilityNum	= uNum;

					SKILL_RESULT_CODE*	pResults			= (SKILL_RESULT_CODE*)((DWORD)(&resBase) + resBase.GetSize());
					sABILITYINFO_BASE*		pAbilityInfo	= NULL ;
					INT						nIndex = 0;


					//////////////////////////////////////////
					///其它效力处理
					pBaseSkillInfo->SetFirst();
					while( pAbilityInfo = pBaseSkillInfo->GetNext() )
					{
						pResults->m_wAbilityIndex	= (WORD)(nIndex++);
						switch(pAbilityInfo->m_wAbilityID)
						{
						case ABILITY_DAMAGE:
							{
								SKILL_RESULT_DAMAGE*	pDamages = (SKILL_RESULT_DAMAGE*)(pResults);

								wDamage		+= wDamage * 2 +pAbilityInfo->m_iParam[0];
								wPerDamage	+= wPerDamage * 2 + pAbilityInfo->m_iParam[1];

								wDamage		+= (DWORD)((float)( wPerDamage / 1000.0f ) * wDamage);
								wDamage		+= rand() % 100;

								if(wDamage >  dwTargetHP)
									wDamage = dwTargetHP;
								dwTargetHP						-= wDamage;
								if(wDamage && dwTargetHP == 0)
									bKilled = TRUE;

								pDamages->m_wDamage			= (WORD)wDamage;
								pDamages->m_dwTargetHP		= dwTargetHP;
								pDamages->m_byEffect			= (BYTE)(wDamage%3 == 0?SKILL_EFFECT_CRITICAL:0);

								pDamages++;
								pResults = (SKILL_RESULT_CODE*)pDamages;
							}
							break;
							///仇恨值
						case ABILITY_AGGROPOINT_INCREASE:
							{
								pTarget->SetTargetID(pChar->GetObjectKey());
							}
							break;

						case ABILITY_SUMMON:
							{
								//MSG_CG_SYNC_MONSTER_ENTER_BRD  summonAdd;

								MSG_CG_SUMMON_SKILL_RESULT_BRD BrdMsg;

								BOOL	bPlayerSkill	= pChar->IsKindOfObject(PLAYER_OBJECT) ;
								//BYTE	bySummonType	= pAbilityInfo->m_iOption1;	
								DWORD dwSummonID		= pAbilityInfo->m_iOption2;	
								//DWORD dwExpireTime	= pAbilityInfo->m_iParam[2];

								BrdMsg.m_dwSummonerObjKey = pChar->GetObjectKey();
								BrdMsg.m_bySummonedNum		=	(BYTE)pAbilityInfo->m_iParam[0];

								if( BrdMsg.m_bySummonedNum && BrdMsg.m_bySummonedNum <= MAX_SUMMONED_NUM )
								{
									BYTE szSummon[2048]={0};
									BYTE* szSummonCur = szSummon;

									Vector3D	vPos = pChar->GetPosition();
									///创建宠物
									for(INT n=0; n<BrdMsg.m_bySummonedNum;n++)
									{
										MSG_CG_SYNC_MONSTER_ENTER_BRD *	pRecvPacket = (MSG_CG_SYNC_MONSTER_ENTER_BRD *)szSummonCur;
										sMONSTERINFO_RENDER *				pRenderInfo = (sMONSTERINFO_RENDER*)( (BYTE*)(pRecvPacket) + sizeof(MSG_CG_SYNC_MONSTER_ENTER_BRD) );

										pRecvPacket->m_byCategory		= CG_SYNC;
										pRecvPacket->m_byProtocol		= CG_SYNC_MONSTER_ENTER_BRD;
										pRenderInfo->m_dwObjectKey		= m_dwRegenBaseID++;
										pRenderInfo->m_dwMonsterCode	= (MONSTERCODE)dwSummonID;
										pRenderInfo->m_vPos.x			= vPos.x + rand()%4-2;
										pRenderInfo->m_vPos.y			= vPos.y + rand()%4-2;
										pRenderInfo->m_vPos.z			= vPos.z;
										pRenderInfo->m_dwHP				= 200;
										pRenderInfo->m_wMoveSpeedRatio= 100;
										pRenderInfo->m_wAttSpeedRatio	= 100;
										pRenderInfo->m_byCount			= 0;
										
										//pRenderInfo++;
										//szSummonCur = (BYTE*)pRenderInfo;

										theNetworkLayer.ParsePacket(pRecvPacket);
										BrdMsg.m_dwSummonedObjKey[n] = pRenderInfo->m_dwObjectKey;
									}
									if( bPlayerSkill )
									{
										BrdMsg.m_byCurCommand = SUMMON_COMMAND_DELEGATE_ATTACK;
									}

									theNetworkLayer.ParsePacket(&BrdMsg);

								}



							}
							break;

							///瞬移处理
						case ABILITY_TELEPORT:
							{
								SKILL_RESULT_POSITION*	pStatus = (SKILL_RESULT_POSITION*)(pResults);
								pStatus->m_vCurPos	= pChar->GetPosition();
								pStatus->m_vDestPos	= syn.m_MainTargetPos;


								MSG_CG_MAP_TELEPORT_BRD msg;
								msg.m_byCategory = CG_MAP;
								msg.m_byProtocol = CG_MAP_TELEPORT_BRD;
								msg.m_dwObjectKey = pChar->GetObjectKey();
								msg.vPos				= syn.m_MainTargetPos;
								theNetworkLayer.ParsePacket(&msg);


								pStatus++;
								pResults = (SKILL_RESULT_CODE*)pStatus;
							}
							break;



						case ABILITY_ABNORMAL_STATUS:
							{
								SKILL_RESULT_STUN*	pStatus = (SKILL_RESULT_STUN*)(pResults);
								pStatus->m_vCurPos	= pTarget->GetPosition();
								pStatus++;
								pResults = (SKILL_RESULT_CODE*)pStatus;
							}
							break;

						case ABILITY_KNOCKBACK:
							{
								Vector3D	vPos3D;
								vPos3D = pTarget->GetPosition() - pChar->GetPosition();
								if(vPos3D.Length() )
									vPos3D.Normalize();
								vPos3D	*= 2.0;

								SKILL_RESULT_POSITION*	pStatus = (SKILL_RESULT_POSITION*)(pResults);
								pStatus->m_vCurPos	= pTarget->GetPosition();
								pStatus->m_vDestPos	= pTarget->GetPosition() + vPos3D;

								pTarget->Move	(pStatus->m_vCurPos
													,pStatus->m_vDestPos
													,MOVETYPE_RUN
													,TRUE);

								pStatus++;
								pResults = (SKILL_RESULT_CODE*)pStatus;
							}
							break;

						default:
							LOGINFO	("%s(%d) 不支持的Ability[%d]\n"
										,__FILE__,__LINE__
										,pAbilityInfo->m_wAbilityID);
							break;
						}
					}

				if(bKilled)
					OnAddExp	(pChar
								,(DWORD)(pTarget->GetMaxHP()/2 + pTarget->GetMaxHP()/2*math::RandomUnit()));

					szMsg = (BYTE*)pResults;
				}


			}

			theNetworkLayer.ParsePacket(&actionBase);
			if(bCanAttack)
			{
				OnRecover(pChar,nHP,nMP);
			}



		}
		break;
	default:
		assert(!"未处理的本地Packet");
		return FALSE;
		break;
	}
	return TRUE;
}

void GameInPlayingTestingData::OnAddExp	(Character* pChar, DWORD dwExp)
{
	Character* pTarget = (Character*)pChar->GetTargetObject();
	//增加经验值
	MSG_CG_STATUS_EXP_CMD sendMsg;
	sendMsg.m_byCategory			= CG_STATUS;
	sendMsg.m_byProtocol			= CG_STATUS_EXP_CMD;
	sendMsg.m_dwTargetObjKey	= pTarget?pTarget->GetObjectKey() : pChar->GetObjectKey();
	sendMsg.m_dwExp				= dwExp + pChar->GetExp();
	theNetworkLayer.ParsePacket(&sendMsg);

	OnLevelUp(pChar);
}

void GameInPlayingTestingData::OnLevelUp	(Character* pChar)
{
	if( pChar->GetExp() < pChar->GetNextExp() )
		return;
	assert(pChar->IsKindOfObject(PLAYER_OBJECT));
	Player*	pPlayer = (Player*)pChar;
	INT nNextLevel = pChar->GetLevel() +1;

	DWORD bonusStat = theFormularManager.GetStatPointPerLevel();

	DWORD dwRemainStat = pPlayer->GetCharInfo()->m_dwRemainStat  + bonusStat;

	// Skill Bonus!
	DWORD bonusSkill		= theFormularManager.GetSkillPoint( (LEVELTYPE)nNextLevel);
	DWORD dwRemainSkill	= pPlayer->GetCharInfo()->m_dwRemainSkill  + bonusSkill;

	//pPlayer->SetNextExp();

	pChar->SetHP( pChar->GetMaxHP() );
	pChar->SetMP( pChar->GetMaxMP() );

	MSG_CG_STATUS_LEVEL_UP_CMD cmsg;
	cmsg.m_Level				= (LEVELTYPE)nNextLevel;
	cmsg.m_wRemainStat			= (WORD)dwRemainStat;
	cmsg.m_wRemainSkill			= (WORD)dwRemainSkill;
	cmsg.m_dwCurHP				= pChar->GetMaxHP();
	cmsg.m_dwCurMP				= pChar->GetMaxMP();
	theNetworkLayer.ParsePacket( &cmsg );

	//MSG_CG_STATUS_LEVEL_UP_BRD sendMsg;
	//sendMsg.m_dwObjectKey		= pChar->GetObjectKey();
	//sendMsg.m_Level				= nNextLevel;
	//sendMsg.m_wRemainStat		= (WORD)dwRemainStat;
	//sendMsg.m_wRemainSkill		= (WORD)dwRemainSkill;
	//sendMsg.m_dwCurHP				= pChar->GetHP();
	//sendMsg.m_dwCurMP				= pChar->GetMP();

	//theNetworkLayer.ParsePacket(&sendMsg);
}


BOOL GameInPlayingTestingData::ProcessSyncPacket	(MSG_OBJECT_BASE* pMsgBase
                                                   ,Character*        pChar)
{
	__UNUSED(pChar);
	assert(pMsgBase->m_byCategory == CG_SYNC && pChar);

	switch(pMsgBase->m_byProtocol)
	{
	case CG_SYNC_MONSTER_LEAVE_BRD:
		{
			theNetworkLayer.ParsePacket(pMsgBase);
		}
		break;
	case CG_SYNC_KBMOVE_SYN:
		{
			MSG_CG_SYNC_KBMOVE_SYN *pRecvMsg = (MSG_CG_SYNC_KBMOVE_SYN*)pMsgBase;

			MSG_CG_SYNC_KBMOVE_BRD sendMsg;
			sendMsg.m_wPlayerKey	= (WORD)pChar->GetObjectKey();
			sendMsg.vCurPos		= pRecvMsg->vCurPos;
			sendMsg.m_wAngle		= pRecvMsg->m_wAngle;
			sendMsg.m_byMoveState= pRecvMsg->m_byMoveState;

			theNetworkLayer.ParsePacket(&sendMsg);
		}
		break;
	default:
		assert(!"未处理的本地Packet");
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL GameInPlayingTestingData::ProcessItemPacket	(MSG_OBJECT_BASE* pMsgBase
                                                   ,Character*        pChar)
{
	assert(	pMsgBase->m_byCategory == CG_ITEM 
			&& pChar 
			&& pChar->IsKindOfObject(PLAYER_OBJECT));

	MSG_OBJECT_BASE* pMsg = pMsgBase;

	//Hero* pPlayer = (Hero*)pChar;
	//Player* pPlayer = (Player*)pChar;
	switch(pMsgBase->m_byProtocol)
	{
	//CG_ITEM_OBTAIN_ACK,
	//CG_ITEM_OBTAIN_NAK,
	//CG_ITEM_LOSE_ACK,

	case CG_ITEM_RANKUP_SYN:
		assert(0);
		break;

	case CG_ITEM_ENCHANTUP_SYN:
		{
		}
		break;

	case CG_ITEM_MAKE_SYN:
		{
			RC::eITEM_RESULT			rt;
			MSG_CG_ITEM_MAKE_SYN *	pRecvMsg = (MSG_CG_ITEM_MAKE_SYN *)pMsg;

			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			ASSERT( pPlayer );
			if( pPlayer )
			{
				MSG_CG_ITEM_MAKE_ACK		msg;
				rt = pPlayer->GetItemManager()->MakeItem	(pRecvMsg->m_DestItemCode
																		,pRecvMsg->m_dwMaterialNums
																		,pRecvMsg->m_byMaterialRow
																		,pRecvMsg->m_arMaterialPos
																		,pRecvMsg->m_szItemName
																		,msg.m_ItemInfo);
				//BYTE					Enchant = 0;
				switch( rt )
				{
				case RC::RC_ITEM_MAKE_SUCCESS:
					{
						msg.m_DestItemCode	= pRecvMsg->m_DestItemCode;
						msg.m_Money				= pPlayer->GetMoney();
						pPlayer->SendPacket(&msg,sizeof(msg));
						//theNetworkLayer.ParsePacket(&msg);
					}
					break;

				default:
					{
						MSG_CG_ITEM_MAKE_NAK msg;
						msg.m_byErrorCode	= (BYTE)rt;
						pPlayer->SendPacket(&msg,sizeof(msg));
						//theNetworkLayer.ParsePacket(&msg);
					}
				}
			}
		}
		break;


	case CG_ITEM_ENCHANT_SYN:
		{
			MSG_CG_ITEM_ENCHANT_SYN * pRecvMsg = (MSG_CG_ITEM_ENCHANT_SYN *)pMsg;

			//Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			//ASSERT( pPlayer );
			//if( !pPlayer ) return ;

			//RC::eITEM_RESULT rt = pPlayer->GetItemManager()->EnchantEx( pRecvMsg->m_TargetPos, pRecvMsg->m_SuccessRateIndex );
			BYTE					Enchant = 0;
			RC::eITEM_RESULT	rt;
			rt= theTestingItemManager.EnchantEx	(pRecvMsg->m_TargetPos
                                             ,pRecvMsg->m_SuccessRateIndex
															,Enchant);
			switch( rt )
			{
			case RC::RC_ITEM_ENCHANT_SUCCESS:
				{
					MSG_CG_ITEM_ENCHANT_SUCCESS_ACK msg;
					msg.m_TargetItemEnchant = Enchant;
					//msg.m_TargetPos		= pRecvMsg->m_TargetPos;
					//pPlayer->SendPacket(&msg,sizeof(msg),TRUE);
					theNetworkLayer.ParsePacket(&msg);
				}
				break;

			case RC::RC_ITEM_ENCHANT_FAILED:
				{
					MSG_CG_ITEM_ENCHANT_FAILED_ACK msg;
					msg.m_TargetItemEnchant		= Enchant;
					//msg.m_TargetPos		= pRecvMsg->m_TargetPos;
					//pPlayer->SendPacket(&msg,sizeof(msg),TRUE);
					theNetworkLayer.ParsePacket(&msg);
				}
				break;

			case RC::RC_ITEM_ENCHANT_DOWN_FAILED:
				{
					MSG_CG_ITEM_ENCHANT_FAILED_ACK msg;
					//msg.m_TargetPos		= pRecvMsg->m_TargetPos;
					msg.m_TargetItemEnchant = Enchant;
					//pPlayer->SendPacket(&msg,sizeof(msg));
					theNetworkLayer.ParsePacket(&msg);
				}
				break;
			case RC::RC_ITEM_ENCHANT_CRACK_FAILED:
				{
					MSG_CG_ITEM_ENCHANT_FAILED_ACK msg;
					//msg.m_TargetPos		= pRecvMsg->m_TargetPos;
					msg.m_TargetItemEnchant = 0;
					//pPlayer->SendPacket(&msg,sizeof(msg));
					theNetworkLayer.ParsePacket(&msg);
				}
				break;

			default:
				{
					MSG_CG_ITEM_ENCHANT_NAK msg;
					msg.m_byErrorCode	= (BYTE)rt;
					//pPlayer->SendPacket(&msg,sizeof(msg),TRUE);
					theNetworkLayer.ParsePacket(&msg);
				}
			}
		}
		break;

	case CG_ITEM_SOCKET_EXTRACT_SYN:
		{
			MSG_CG_ITEM_SOCKET_EXTRACT_SYN * pRecvMsg = (MSG_CG_ITEM_SOCKET_EXTRACT_SYN *)pMsgBase;
			RC::eITEM_RESULT rt;
			
			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			ASSERT( pPlayer );
			if( pPlayer )
			{
				MSG_CG_ITEM_SOCKET_EXTRACT_SUCCESS_ACK amsg;

				rt = theTestingItemManager.ExtractSocket	(pRecvMsg->m_type
																		,pRecvMsg->m_TargetPos
																		,pRecvMsg->dwSocketState
																		,amsg.dwSocketResult
																		,&amsg.m_ItemInfo);
				switch( rt )
				{
				case RC::RC_ITEM_EXTRACTSOCKET_SUCCESS:
					{
						amsg.m_TargetPos		= pRecvMsg->m_TargetPos;
						amsg.m_type				= pRecvMsg->m_type;
						amsg.dwSocketState	= pRecvMsg->dwSocketState;
						amsg.m_Money			= pPlayer->GetMoney();
						//pPlayer->SendPacket(&amsg, amsg.GetSize());
						theNetworkLayer.ParsePacket(&amsg);
					}
					break;

				case RC::RC_ITEM_EXTRACTSOCKET_FAILED:
					{
						MSG_CG_ITEM_SOCKET_EXTRACT_FAILED_ACK msg;
						msg.m_ItemInfo			= amsg.m_ItemInfo;
						msg.dwSocketResult	= amsg.dwSocketResult;
						msg.m_TargetPos		= pRecvMsg->m_TargetPos;
						msg.m_type				= pRecvMsg->m_type;
						msg.dwSocketState		= pRecvMsg->dwSocketState;
						msg.m_Money				= pPlayer->GetMoney();
						//pPlayer->SendPacket(&msg, msg.GetSize());
						theNetworkLayer.ParsePacket(&msg);
					}
					break;
				default:
					{
						MSG_CG_ITEM_SOCKET_EXTRACT_NAK msg;
						msg.m_dwErrorCode		= rt;
						//pPlayer->SendPacket(&msg,sizeof(msg));
						theNetworkLayer.ParsePacket(&msg);
					}
				}
			}

		}
		break;

	case CG_ITEM_SOCKET_FILL_SYN:
		{
			MSG_CG_ITEM_SOCKET_FILL_SYN * pRecvMsg = (MSG_CG_ITEM_SOCKET_FILL_SYN *)pMsgBase;

			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			ASSERT( pPlayer );
			if( pPlayer )
			{
				RC::eITEM_RESULT rt;
				rt = theTestingItemManager.FillSocket	(pRecvMsg->m_type
																  ,pRecvMsg->m_arSocketItemPos
																  ,pRecvMsg->m_TargetPos);
				switch( rt )
				{
				case RC::RC_ITEM_SUCCESS:
					{
						ItemSlotContainer*	pContainer;
						MSG_CG_ITEM_SOCKET_FILL_ACK msg;

						for(INT n=0; n<SOCKET_MAX; n++)
							msg.m_arSocketItemPos[n]	= pRecvMsg->m_arSocketItemPos[n];

						pContainer					= pPlayer->GetItemManager()->GetItemSlotContainer(SI_INVENTORY);
						//pContainer					= (ItemSlotContainer*)theItemManager.GetContainer(SI_INVENTORY);

						msg.m_TargetPos			= pRecvMsg->m_TargetPos;
						msg.m_Money					= theHero.GetMoney();
						ItemSlot & slotDat		= (ItemSlot & )pContainer->GetSlot( msg.m_TargetPos );

						slotDat.CopyOut(msg.m_TargetItemStream);
						theNetworkLayer.ParsePacket(&msg);
					}
					break;
				default:
					{
						MSG_CG_ITEM_SOCKET_FILL_NAK msg;
						msg.m_dwErrorCode	= rt;
						theNetworkLayer.ParsePacket(&msg);
					}
				}
			}

		}break;

	case CG_ITEM_ACCESSORY_CREATE_SYN:
		{
			MSG_CG_ITEM_ACCESSORY_CREATE_SYN&	syn = (MSG_CG_ITEM_ACCESSORY_CREATE_SYN&)*pMsgBase;
			MSG_CG_ITEM_ACCESSORY_CREATE_ACK		msg;
		
			msg.m_type				= syn.m_type;
			msg.m_SocketItemPos	= syn.m_SocketItemPos;

			sITEMCOMPOSITE * pCInfo = theItemCompositeParser.GetCompositeInfo((MATERIALTYPE)syn.m_type);
			if( !pCInfo )
				return RC::RC_ITEM_INVALIDSTATE;

			sITEMCOMPOSITERESULT * pResultInfo;
			pResultInfo = theItemCompositeParser.GetCompositeResultInfo((SLOTCODE)pCInfo->m_ResultCode);
			ASSERT( pResultInfo );

			SLOTCODE	itemResult;
			if(rand()%100 < pResultInfo->m_byMainRate)
			{
				itemResult = pResultInfo->m_MainResult;
				msg.m_byResultPos		= sITEMCOMPOSITERESULT::RESULT_MAIN;
			}
			else
			{
				int seed = math::RandomRange(0, sITEMCOMPOSITERESULT::MAX_COMPOSITE_RESULT_NUM-1);
				ASSERT( pResultInfo->m_ResultItemCode[seed] != 0 );
				itemResult = pResultInfo->m_ResultItemCode[seed];
				msg.m_byResultPos		= (BYTE)(seed + sITEMCOMPOSITERESULT::RESULT_SUBBASE);
			}


			SLOTPOS atPos;
			BaseContainer * pInventory = theHero.GetSlotContainer( SI_INVENTORY );
			if(!pInventory->GetEmptyPos(atPos))
			{
				MSG_CG_ITEM_ACCESSORY_CREATE_NAK	nmsg;
				nmsg.m_dwErrorCode = RC::RC_ITEM_HAVENOTSPACE;
				theNetworkLayer.ParsePacket(&nmsg);
				//goto laBuyFailed;
				return TRUE;
			}

			ItemSlot slot;
			slot.SetCode(itemResult);

			msg.m_ItemInfo.m_InvenCount		= 1;
			msg.m_ItemInfo.m_TmpInvenCount	= 0;
			msg.m_ItemInfo.m_Slot[0].m_ItemPos	= atPos;
			slot.CopyOut(msg.m_ItemInfo.m_Slot[0].m_Stream);

			msg.m_Money			= theHero.GetMoney() - pCInfo->m_Money;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;


	case CG_ITEM_SELL_SYN:
		{
			MSG_CG_ITEM_SELL_SYN& syn = (MSG_CG_ITEM_SELL_SYN&)*pMsgBase;
			MSG_CG_ITEM_SELL_ACK msg;

			ItemSlotContainer*	pContainer	= (ItemSlotContainer*)theHero.GetSlotContainer(syn.m_atIndex);
			if(!pContainer->IsEmpty(syn.m_atPos))
			{
				ItemSlot&			rcItem			= (ItemSlot&)pContainer->GetSlot((SLOTPOS)syn.m_atPos);
				msg.m_byCategory	= CG_ITEM;
				msg.m_byProtocol	= CG_ITEM_SELL_ACK;
				msg.m_atIndex		= syn.m_atIndex;
				msg.m_atPos			= syn.m_atPos;
				msg.m_Money			= theHero.GetMoney() + rcItem.GetPriceForSell();

				theNetworkLayer.ParsePacket(&msg);
			}
			else
			{
				MSG_CG_ITEM_SELL_NAK msg;
				msg.m_byCategory	= CG_ITEM;
				msg.m_byProtocol	= CG_ITEM_SELL_NAK;
				msg.m_dwErrorCode	= RC::RC_ITEM_INVALIDPOS;
				theNetworkLayer.ParsePacket(&msg);
			}

		}break;

	case CG_ITEM_BUY_SYN:
		{
			MSG_CG_ITEM_BUY_SYN& syn = (MSG_CG_ITEM_BUY_SYN&)*pMsgBase;
			MSG_CG_ITEM_BUY_ACK msg;
			MSG_CG_ITEM_BUY_NAK nmsg;
			ItemSlot slot;

			BaseContainer* pContainer = theHero.GetSlotContainer(SI_INVENTORY);
			//syn.m_dwShopListID, syn.m_ShopTabIndex, syn.m_ShopItemIndex

			ShopDetailInfo * pShopInfo = theShopInfoParser.GetShopList(syn.m_dwShopListID);
			if( !pShopInfo )
			{
				nmsg.m_dwErrorCode = RC::RC_ITEM_INVALIDSHOPLISTID;
				goto laBuyFailed;
			}
			if( syn.m_ShopTabIndex >= ShopDetailInfo::MAX_PAGE_NUM ||
				syn.m_ShopItemIndex >= ShopDetailInfo::MAX_SELL_ITEM )
			{
				nmsg.m_dwErrorCode = RC::RC_ITEM_OUTOFSHOPITEMINDEX;
				goto laBuyFailed;
			}

			sSHOP_ITEM * pShopItem = pShopInfo->GetItemAt( syn.m_ShopTabIndex, syn.m_ShopItemIndex );
			if( !pShopItem )
			{
				nmsg.m_dwErrorCode = RC::RC_ITEM_OUTOFSHOPITEMINDEX;
				goto laBuyFailed;
			}

			SLOTCODE buyItemCode = pShopItem->m_SellItemCode;
			sITEMINFO_BASE * pInfo = theItemInfoParser.GetItemInfo( buyItemCode );
			if( !pInfo )
			{
				nmsg.m_dwErrorCode = RC::RC_ITEM_OUTOFSHOPITEMINDEX;
				goto laBuyFailed;
			}


			//////////////////////////////////////
			//计算价格
			DURATYPE num = pShopItem->m_SellItemNum;
			slot.SetCode(buyItemCode);
			MONEY PriceOfGoods = slot.GetPriceForBuy()*num;

			if( theHero.GetMoney() < PriceOfGoods )
			{
				nmsg.m_dwErrorCode = RC::RC_ITEM_HAVENOTMONEY;
				goto laBuyFailed;
			}

			SLOTPOS atPos;
			if(!pContainer->GetEmptyPos(atPos))
			{
				nmsg.m_dwErrorCode = RC::RC_ITEM_HAVENOTSPACE;
				goto laBuyFailed;
			}

			msg.m_Money			= theHero.GetMoney() - PriceOfGoods;
			msg.m_TotalInfo.m_InvenCount		= 1;
			msg.m_TotalInfo.m_TmpInvenCount	= 0;
			msg.m_TotalInfo.m_Slot[0].m_ItemPos	= atPos;
			slot.CopyOut(msg.m_TotalInfo.m_Slot[0].m_Stream);

			theNetworkLayer.ParsePacket(&msg);
			break;
laBuyFailed:
			theNetworkLayer.ParsePacket(&nmsg);

		}break;


	case CG_ITEM_QUICK_LINKITEM_SYN:
		{
			MSG_CG_ITEM_QUICK_LINKITEM_SYN& syn = *(MSG_CG_ITEM_QUICK_LINKITEM_SYN*)pMsgBase;
			MSG_CG_ITEM_QUICK_LINKITEM_ACK msg;
			msg.m_OrgPos	= syn.m_OrgPos;
			msg.m_ToPos		= syn.m_ToPos;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;

	case CG_ITEM_QUICK_LINKSKILL_SYN:
		{
			MSG_CG_ITEM_QUICK_LINKSKILL_SYN& syn = *(MSG_CG_ITEM_QUICK_LINKSKILL_SYN*)pMsgBase;
			MSG_CG_ITEM_QUICK_LINKSKILL_ACK msg;
			msg.m_SkillCode= syn.m_SkillCode;
			msg.m_ToPos		= syn.m_ToPos;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;

	case CG_ITEM_QUICK_UNLINK_SYN:
		{
			MSG_CG_ITEM_QUICK_UNLINK_SYN& syn = *(MSG_CG_ITEM_QUICK_UNLINK_SYN*)pMsgBase;
			MSG_CG_ITEM_QUICK_UNLINK_ACK msg;
			msg.m_atPos	= syn.m_atPos;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;

	case CG_ITEM_QUICK_MOVE_SYN:
		{
			MSG_CG_ITEM_QUICK_MOVE_SYN& syn = *(MSG_CG_ITEM_QUICK_MOVE_SYN*)pMsgBase;
			MSG_CG_ITEM_QUICK_MOVE_ACK msg;
			msg.m_fromPos	= syn.m_fromPos;
			msg.m_toPos		= syn.m_toPos;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;


	case CG_ITEM_DROP_MONEY_SYN:
		assert(0);
		break;
	case CG_ITEM_PICK_MONEY_SYN:
		{
			MSG_CG_ITEM_PICK_MONEY_SYN& syn = *(MSG_CG_ITEM_PICK_MONEY_SYN*)pMsgBase;
			Item* pItem = (Item*)theObjectManager.GetObject(syn.m_dwFieldItemObjectKey);
			if(pItem && pItem->IsKindOfObject(ITEM_OBJECT))
			{
				if(pItem->IsMoney())
				{
					MSG_CG_ITEM_PICK_MONEY_ACK msg;
					msg.m_Money		= theHero.GetMoney() + pItem->GetMoney();
					theNetworkLayer.ParsePacket(&msg);
		
					/// 删除地上物品
					MSG_CG_SYNC_FIELDITEM_LEAVE_BRD BrdMsg;
					BrdMsg.m_byCount = 1;
					BrdMsg.m_dwObjectKey[0] = pItem->GetObjectKey();
					theNetworkLayer.ParsePacket(&BrdMsg);
				}
			}
		}
		break;

	case CG_ITEM_PICK_SYN:
		{
			MSG_CG_ITEM_PICK_SYN&	syn		= *(MSG_CG_ITEM_PICK_SYN*)pMsgBase;

			MSG_CG_ITEM_PICK_SYN*	pRecvMsg = (MSG_CG_ITEM_PICK_SYN*)pMsg;

			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			ASSERT( pPlayer );
			if( pPlayer )
			{
				Item* pItem = (Item*)theObjectManager.GetObject(syn.m_dwFieldItemObjectKey);
				if(pItem && pItem->IsKindOfObject(ITEM_OBJECT))
				{
					if(!pItem->IsMoney())
					{
						MSG_CG_ITEM_PICK_ACK msg;

						ItemSlotContainer* pContainer =  (ItemSlotContainer*)pPlayer->GetSlotContainer(SI_INVENTORY);
						assert(pContainer);
						SLOTPOS atPos;
						if(pContainer->GetEmptyPos(atPos))
						{
							msg.m_ItemInfo.m_InvenCount		= 1;
							msg.m_ItemInfo.m_TmpInvenCount	= 0;
							msg.m_ItemInfo.m_Slot[0].m_ItemPos	= atPos;
							pItem->GetItem().CopyOut(msg.m_ItemInfo.m_Slot[0].m_Stream);

							theNetworkLayer.ParsePacket(&msg);

							/// 删除地上物品
							MSG_CG_SYNC_FIELDITEM_LEAVE_BRD BrdMsg;
							BrdMsg.m_byCount = 1;
							BrdMsg.m_dwObjectKey[0] = pItem->GetObjectKey();
							theNetworkLayer.ParsePacket(&BrdMsg);

						}
						else
						{
							MSG_CG_ITEM_PICK_NAK msg;
							msg.m_dwErrorCode				= RC::RC_ITEM_HAVENOTSPACE;
							msg.m_dwFieldItemObjectKey	= syn.m_dwFieldItemObjectKey;
							theNetworkLayer.ParsePacket(&msg);
						}
					}
					else
					{
						assert(0);
					}
				}
			}
		}
		break;

	case CG_ITEM_DROP_SYN:
		{
			MSG_CG_ITEM_DROP_SYN& syn = *(MSG_CG_ITEM_DROP_SYN*)pMsgBase;

			MSG_CG_ITEM_DROP_SYN*	pRecvMsg = (MSG_CG_ITEM_DROP_SYN*)pMsg;

			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			ASSERT( pPlayer );
			if( pPlayer )
			{
				/////////////////////////////////
				/// 生成地上物品对象
				MSG_CG_SYNC_FIELDITEM_ENTER_BRD itemMsg;

				//assert(syn.m_atIndex == SI_INVENTORY);

				ItemSlotContainer * pAtContainer	= (ItemSlotContainer *)pPlayer->GetSlotContainer( syn.m_atIndex );
				ItemSlot&			item					= (ItemSlot&)pAtContainer->GetSlot(syn.m_atPos);

				itemMsg.m_dwFromMonsterKey		= 0;
				//CreateItem(SRegenInfo& info);
				sITEMINFO_RENDER & RenderInfo = itemMsg.m_ItemRenderInfo;

				RenderInfo.m_dwObjectKey		= m_dwRegenBaseID++;
				RenderInfo.m_dwOwnerPlayerKey	= pChar->GetObjectKey();
				//if( GetObjectType() == MONEY_OBJECT )
				//{
				//	RenderInfo.m_byFieldItemType = sITEMINFO_RENDER::eFIELDITEM_MONEY;
				//	RenderInfo.m_Money = GetMoney();
				//}
				//else
				{
					RenderInfo.m_byFieldItemType = sITEMINFO_RENDER::eFIELDITEM_ITEM;
					item.CopyOut(RenderInfo.m_ItemStream);
				}

				Vector3D vec =	pChar->GetPosition();
				RenderInfo.m_fPos[0]		= vec.x + (rand()%10 - 5);
				RenderInfo.m_fPos[1]		= vec.y + (rand()%10 - 5);
				RenderInfo.m_fPos[2]		= vec.z;

				theNetworkLayer.ParsePacket(&itemMsg);

				///响应Drop操作
				MSG_CG_ITEM_DROP_ACK msg;
				msg.m_byCategory	= CG_ITEM;
				msg.m_byProtocol	= CG_ITEM_DROP_ACK;
				msg.m_atIndex	= syn.m_atIndex;
				msg.m_atPos		= syn.m_atPos;
				theNetworkLayer.ParsePacket(&msg);

			}
		}
		break;

	case CG_ITEM_COMBINE_SYN:
		{
			MSG_CG_ITEM_COMBINE_SYN& syn = *(MSG_CG_ITEM_COMBINE_SYN*)pMsgBase;
			MSG_CG_ITEM_COMBINE_ACK msg;
			msg.m_byCategory	= CG_ITEM;
			msg.m_byProtocol	= CG_ITEM_COMBINE_ACK;
			msg.m_fromIndex	= syn.m_fromIndex;
			msg.m_toIndex		= syn.m_toIndex;
			msg.m_fromPos		= syn.m_fromPos;
			msg.m_toPos			= syn.m_toPos;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;



	case CG_ITEM_MOVE_SYN:
		{
			MSG_CG_ITEM_MOVE_SYN& syn = *(MSG_CG_ITEM_MOVE_SYN*)pMsgBase;
			MSG_CG_ITEM_MOVE_ACK msg;
			msg.m_fromIndex	= syn.m_fromIndex;
			msg.m_toIndex		= syn.m_toIndex;
			msg.m_fromPos		= syn.m_fromPos;
			msg.m_toPos			= syn.m_toPos;;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;

	case CG_ITEM_USE_SYN:
		{
			MSG_CG_ITEM_USE_SYN& syn = *(MSG_CG_ITEM_USE_SYN*)pMsgBase;
			MSG_CG_ITEM_USE_SYN* pRecvMsg = (MSG_CG_ITEM_USE_SYN*)pMsg;

			Player * pPlayer = thePlayerManager.FindPlayer( pRecvMsg->m_dwKey );
			ASSERT( pPlayer );
			if( pPlayer ) 
			{

					BaseContainer * pAtContainer	= pPlayer->GetSlotContainer( syn.m_atIndex );
					if( pAtContainer->IsEmpty( syn.m_atPos ) )
						return FALSE;
					BaseSlot & rFromSlot				= pAtContainer->GetSlot( syn.m_atPos );			//< 1. 
					sITEMINFO_BASE * pInfo = theItemInfoParser.GetItemInfo( rFromSlot.GetCode() );

					MSG_CG_ITEM_USE_ACK  msg;
					msg.m_byCategory	= CG_ITEM;
					msg.m_byProtocol	= CG_ITEM_USE_ACK;
					msg.m_atIndex		= syn.m_atIndex;
					msg.m_atPos			= syn.m_atPos;
					theNetworkLayer.ParsePacket(&msg);

					if( pInfo->IsCanUseWaste())
					{
						DWORD dwMP(0);
						DWORD dwHP(0);
						if(pInfo->m_byWasteType == ITEMWASTE_HPPOTION)
							dwHP = pInfo->m_wHealHP;
						else if(pInfo->m_byWasteType == ITEMWASTE_MPPOTION)
							dwMP = pInfo->m_wHealHP;
						else if(pInfo->m_byWasteType == ITEMWASTE_HPMPPOTION)
						{
							dwHP = pInfo->m_wHealHP;
							dwMP = pInfo->m_wHealHP;
						}
						OnRecover(pChar,dwHP,dwMP);
					}
			}
		}
		break;
	default:
		assert(!"未处理的本地Packet");
		return FALSE;
		break;
	}
	return TRUE;
}

BOOL GameInPlayingTestingData::OnRecover(Character* pChar,int recoverHP, int recoverMP)
{
	///触发属性变化
	INT nHP = pChar->GetHP() + recoverHP;
	INT nMP = pChar->GetMP() + recoverMP;
	if(nHP < 0)		nHP = 0;
	if((UINT)nHP > pChar->GetMaxHP())		nHP = pChar->GetMaxHP();
	if(nMP < 0)		nMP = 0;
	if((UINT)nMP > pChar->GetMaxMP())		nMP = pChar->GetMaxMP();

	MSG_CG_STATUS_RECOVER_ATTR_BRD sendMsg;
	sendMsg.m_byCategory		= CG_STATUS;
	sendMsg.m_byProtocol		= CG_STATUS_RECOVER_ATTR_BRD;
	sendMsg.m_dwObjectKey	= pChar->GetObjectKey();
	sendMsg.m_dwTargetHP		= nHP;
	sendMsg.m_dwTargetMP		= nMP;

	theNetworkLayer.ParsePacket(&sendMsg);
	return TRUE;
}


BOOL GameInPlayingTestingData::ProcessStylePacket	(MSG_OBJECT_BASE* pMsgBase
                                                   ,Character*        pChar)
{
	assert(pMsgBase->m_byCategory == CG_STYLE && pChar);

	switch(pMsgBase->m_byProtocol)
	{
	case CG_STYLE_SELECT_STYLE_SYN:
		{
			assert(pChar->IsKindOfObject(PLAYER_OBJECT));
			Player* pPlayer = (Player*)pChar;
			MSG_CG_STYLE_SELECT_STYLE_SYN&	syn = *(MSG_CG_STYLE_SELECT_STYLE_SYN*)pMsgBase;

			MSG_CG_STYLE_SELECT_STYLE_BRD msg;
			msg.m_dwObjectKey		= pPlayer->GetObjectKey();
			msg.m_CurStyleCode	= (SLOTCODE)pPlayer->GetCurrentAttackStyle();
			msg.m_NewStyleCode	= syn.m_NewStyleCode;
			theNetworkLayer.ParsePacket(&msg);
		}
		break;
	case CG_STYLE_LINK_SYN:
	case CG_STYLE_LINKMOVE_SYN:
	case CG_STYLE_UNLINK_SYN:
	default:
		assert(!"未处理的本地Packet");
		return FALSE;
		break;
	}
	return TRUE;
}


BOOL GameInPlayingTestingData::ProcessBattlePacket	(MSG_OBJECT_BASE* pMsgBase
                                                   ,Character*        pChar)
{
	assert(pMsgBase->m_byCategory == CG_BATTLE && pChar);

	switch(pMsgBase->m_byProtocol)
	{
	case CG_BATTLE_MONSTER_ATTACK_CMD:
			theNetworkLayer.ParsePacket(pMsgBase);
		break;
	case CG_BATTLE_PLAYER_ATTACK_SYN:
		{
			MSG_CG_BATTLE_PLAYER_ATTACK_SYN& syn = *(MSG_CG_BATTLE_PLAYER_ATTACK_SYN*)pMsgBase;
			MSG_CG_BATTLE_PLAYER_ATTACK_BRD  msg;
			Character*								pTarget;
			DWORD										wDamage(0);
			sSTYLEINFO_BASE*						pStyle;
			INT										nLVDiff;
			BOOL										bCalcDamage(TRUE);
			

			pTarget	= (Character*)theObjectManager.GetObject(syn.dwTargetKey);
			if(!pTarget || pTarget->IsDead())
				return FALSE;

			pStyle	= theSkillInfoParser.GetStyleInfo(syn.StyleCode);

			if(pTarget->IsKindOfObject(MONSTER_OBJECT))
			{
				Monster*			pMonster = (Monster*)pTarget;
				sNPCINFO_BASE*	pNPCInfo	= pMonster->GetMonsterInfo();
				if(pNPCInfo && !pNPCInfo->CanAttack())
				{	
					wDamage		= pTarget->GetHP();
					bCalcDamage	= FALSE;
				}
			}

			if(bCalcDamage)
			{
				nLVDiff	= pTarget->GetLevel() - pChar->GetLevel();
				wDamage	= (DWORD)(math::RandomUnit() * pTarget->GetMaxHP()/3);
				if(nLVDiff > 50)
					wDamage = (DWORD)(math::RandomUnit() * pTarget->GetMaxHP()/50);
				else if(nLVDiff > 5)
					wDamage -= (DWORD)(wDamage * nLVDiff/50);
				else if(nLVDiff < -5)
					wDamage	= (DWORD)(math::RandomUnit() * pTarget->GetMaxHP()/(-nLVDiff));

				wDamage	= (DWORD)(( wDamage * ( 1 + pStyle->m_fDamagePercent[syn.byAttackType] ) ) 
										+ pStyle->m_iAddDamage[syn.byAttackType]);
				if(wDamage > pTarget->GetHP())
					wDamage = pTarget->GetHP();
			}

#ifdef TEST_ALLDEAD
			wDamage	= pTarget->GetHP();
#endif

			//if(wDamage)
			{
				msg.dwAttackerKey	= pChar->GetObjectKey();
				msg.byAttackType	= syn.byAttackType;
				msg.StyleCode		= syn.StyleCode;
				msg.dwClientSerial= syn.dwClientSerial;
				msg.vCurPos		= syn.vCurPos;
				msg.vDestPos		= syn.vDestPos;
				msg.dwTargetKey	= syn.dwTargetKey;
				msg.wDamage			= (DAMAGETYPE)wDamage;
				msg.dwTargetHP		= pTarget->GetHP() - wDamage;
				msg.byEffect		= 0;
				if(wDamage >= pTarget->GetMaxHP() /2)
					msg.byEffect		|= ATTACK_ADDITIONAL_EFFECT_CRITICAL;

				theNetworkLayer.ParsePacket(&msg);

				if(msg.dwTargetHP == 0)
					OnAddExp(pChar,(DWORD)(pTarget->GetMaxHP()/2 + pTarget->GetMaxHP()/2*math::RandomUnit()));
			}
		}
		break;
	default:
		assert(!"未处理的本地Packet");
		return FALSE;
		break;
	}

	return TRUE;
}


#endif

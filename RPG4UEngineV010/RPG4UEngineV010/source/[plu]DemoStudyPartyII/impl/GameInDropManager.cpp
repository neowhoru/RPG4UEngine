/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInDropManager.cpp
创建日期：2008年7月4日
最后更新：2008年7月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameInDropManager.h"
#include "Player.h"
#include "Monster.h"
#include "QuestItemDropInfoParser.h"
#include "QuestManager.h"
#include "GameInPlayingTestingData.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "TipLogManager.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace testing;
GLOBALINST_SINGLETON_IMPL2(DropManager,GameInDropManager, ()  , gamemain::eInstPrioGameFunc);


#define TEAM_TASK_VAR_DIST	60.0f

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

namespace testing
{ 

DropManager::DropManager(void)
{
}
DropManager::~DropManager(void)
{
}

BOOL DropManager::Init()
{
	return TRUE;
}




BOOL DropManager::CanDropQuestItem	(Player*  pPlayer
                                    ,CODETYPE questID
												,DWORD    questStep
												,SLOTCODE itemCode)
{
	DWORD dwDropMax;

	// 如果不是此任务状态则不掉
	if ( pPlayer->GetQuestState( questID ) != questStep )
	{
		return FALSE;
	}

	dwDropMax = theQuestItemDropInfoParser.GetItemDropMax(itemCode);

	// 物品已经满了
	return !::theItemManager.IsExistItem(SI_INVENTORY, itemCode, dwDropMax);
}


BOOL DropManager::MonsterDeath( DWORD dwMonsterKey, DWORD dwPlayerID )
{
	Monster*						pMonster;
	Player*						pPlayer;
	sQUESTITEM_DROPINFO*		pDropInfo;
	sQUESTITEM_DROPUNIT*		pDropUnit;
	//DWORD							dwRate;

	pMonster = (Monster*)theObjectManager.GetObject(dwMonsterKey);
	pPlayer  = (Player*)theObjectManager.GetObject(dwPlayerID);

	__CHECK(pMonster && pMonster->IsKindOfObject(MONSTER_OBJECT));
	__CHECK(pPlayer  && pPlayer->IsKindOfObject(PLAYER_OBJECT));


	pDropInfo = theQuestItemDropInfoParser.GetQuestItemDropInfo(pMonster->GetMonsterInfo()->m_MonsterCode);
	__CHECK_PTR(pDropInfo);

	for ( UINT i=0; i<pDropInfo->m_DropUnits.size(); i++ )
	{
		pDropUnit = &pDropInfo->m_DropUnits[i];

		// 有任务物品
		if ( pDropUnit->m_ItemCode != INVALID_DWORD_ID )
		{
			// 是否需要掉落此任务物品
			if ( CanDropQuestItem	(pPlayer
                                 ,pDropUnit->m_QuestID
											,pDropUnit->m_QuestStep
											,(SLOTCODE)pDropUnit->m_ItemCode)== FALSE )
			{
				continue;
			}

			// 按掉落机率掉落
			SLocationInfo info;
			info.MonsterCode				= dwMonsterKey;
			info.dropInfo.byDropRate	= (BYTE)pDropUnit->m_DropRate;
			info.dropInfo.byItemRate	= 100;
			for(INT nIndex=0;nIndex<DROP_ITEM_MAX; nIndex++)
				info.dropInfo.arItemCodes[nIndex]	= pDropUnit->m_ItemCode;

			theGameInPlayingTestingData.CreateItem(info);

			// 掉落物品
			//DropItem	(pDropUnit->m_ItemCode
         //       ,1
			//			,pMonster->GetMapID()
			//			,pMonster->GetFloatX()
			//			,pMonster->GetFloatY()
			//			,TRUE
			//			,pPlayer->GetTeamID()
			//			,pPlayer->GetID());
		}
		UpdateQuestVarInfo( pDropUnit, pMonster, pPlayer );
	}
	return TRUE;
}

BOOL DropManager::UpdateQuestVarInfo	(sQUESTITEM_DROPUNIT*    pDropUnit
													,Monster*					 /*pMonster*/
													,Player*						 pPlayer)
{
	// 是否有任务关联记数变量
	if ( pDropUnit->m_VarID == INVALID_DWORD_ID )
		return FALSE;

	quest::sQUEST_INFO*	pQuest;
	DWORD				questStep;
	DWORD				dwValue;

	
	pQuest = theQuestManager.LoadQuest( pDropUnit->m_QuestID );
	if( !pQuest )
		return FALSE;


	questStep = pPlayer->GetQuestState( pDropUnit->m_QuestID );
	if ( questStep == pDropUnit->m_QuestStep )
	{
		dwValue = pPlayer->GetVar( pDropUnit->m_VarID );
		if (dwValue < pDropUnit->m_DestValue )
		{
			pPlayer->SetVar( pDropUnit->m_VarID, dwValue+1 );

			LOGTIPF	("[%s]%s(%d//%d)"
						,dwValue+1 < pDropUnit->m_DestValue? eTIP_NORMAL : eTIP_QUEST
						,NULL
						,INVALID_SOUNDINDEX
						,pQuest->m_Name
						,(LPCSTR)pDropUnit->m_ShowName
						,dwValue+1
						,pDropUnit->m_DestValue
						);
			//pPlayer->ShowScreenText(TRUE
   //                             ,1000
			//							  ,0xff00ff00
			//							  ,FMSTR("任务[%s]" ,pQuest->m_Name) );

			//pPlayer->ShowScreenText(FALSE
   //                             ,1000
			//							  ,0xff00ff00
			//							  ,FMSTR("%s(%d/%d)"
			//									  ,(LPCSTR)pDropUnit->m_ShowName
			//									  ,dwValue+1
			//									  ,pDropUnit->m_DestValue)
			//							  );
		}
	}

	return TRUE;
}



};//namespace testing

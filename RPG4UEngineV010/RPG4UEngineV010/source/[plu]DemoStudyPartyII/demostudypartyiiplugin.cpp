// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "LogSystem.h"
#include "PathInfoParser.h"

#include "GameInPlayingTestingData.h"
#include "DemoSystemManager.h"



/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_DEMOSTUDYPARTYII_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{
	//__INIT(theGameInPlayingTestingData.Init() );

	if(theDemoSystemManager.RegDemoSystem(&theGameInPlayingTestingData))
	{
		theDemoSystemManager.ActivateDemoSystem(&theGameInPlayingTestingData);
		//__INIT(theGameInPlayingTestingData.LoadRegenInfo("Demo\\StudyPartyII\\RegenInfo.txt") );
	}

	LOGINFO("Plugin DemoStudyPartyII insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_DEMOSTUDYPARTYII_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	theDemoSystemManager.ActivateDefaultDemoSystem();
	theDemoSystemManager.UnregDemoSystem(&theGameInPlayingTestingData);
	//__END(theGameInPlayingTestingData.Release());

	return gamemain::ePluginOK;
}

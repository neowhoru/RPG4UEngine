#include "stdafx.h"
#include "DBUserManager.h"
#include "GameSession.h"
#include "PacketHandlerManager.h"
#include "PacketStruct_Base.h"
#include "DBUser.h"
#include "ServerUtil.h"
#include "PacketHandlerManager.h"

GameSession::GameSession()
{
	m_PacketHandlerType	= GAME_SHELL;
}

GameSession::~GameSession()
{
}

VOID GameSession::OnRecv( BYTE * pMsg, WORD wSize )
{
	_SUPER::OnRecv(pMsg, wSize);
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class DisconnectRelatedGameOpr
{
	DWORD m_dwIndex;
public:
	DisconnectRelatedGameOpr( DWORD idx ):m_dwIndex(idx){}

	////////////////////////////////////////////////////////////////////////
	VOID operator()( DBUser * pUser )
	{
		// Link///////////////////
		if( pUser->GetServerSessionIndex() == m_dwIndex )
		{
			LOGMSG(LOG_FULL,  "[(%u),U:(%u)]", m_dwIndex, pUser->GetUserKey() );
			pUser->UpdateDataToDB();
			theDBUserManager.UserNormalToCache( pUser->GetUserKey() );
		}
	}
};

VOID GameSession::OnDisconnect()
{
	DisconnectRelatedGameOpr op(GetSessionIndex());
	_RemoveRelatedUsers(op);
	_SUPER::OnDisconnect();
}



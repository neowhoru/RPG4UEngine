/*////////////////////////////////////////////////////////////////////////
文 件 名：DBUserImpl.cpp
创建日期：2009年3月31日
最后更新：2009年3月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "DBUserImpl.h"
#include "StructBase.h"
#include "QueryForUser.h"
#include "DBProxyShell.h"
#include <ServerStruct.h>
#include <PacketStruct_Server.h>
#include <PacketStruct_DBP_Game.h>
#include "DBUserManager.h"
#include <StructInPacket.h>
#include "IServerSession.h"
#include <MiscUtil.h>
#include <DBServerUtil.h>
#include <ConstInfoSql.h>
#include <DatabaseProxyQuery.h>
#include <ServerSetting.h>
#include <LocalDBData.h>
#include <QueryStruct.h>

DBUserImpl::DBUserImpl(void)
{
}

DBUserImpl::~DBUserImpl(void)
{	
}

BOOL DBUserImpl::Init()
{
	_Reset();
	
	for( int i = 0 ; i < MAX_CHARACTER_SLOT_NUM ; ++i )
		m_arSlotOccupied[i] = FALSE;

	SetDBLoadState	( DBLOAD_NONE );
	SetState			( DBUSER_STATE_ALLOC );

	SetSelectedSlotIndex(0);
	return TRUE;
}


BOOL DBUserImpl::_Reset()
{
	SetWarehouseModified( FALSE );

	m_DBUpdateTimer.SetTimer( DBUPDATE_DELAY_LENGTH );

	m_dwServerSessionIndex = 0;

	m_PacketFlags = 0;
	return TRUE;
}


VOID DBUserImpl::Release()
{
	SetState( DBUSER_STATE_POOL );

	SetCacheState( DBCACHE_NONE );
}







VOID DBUserImpl::UpdateDataToDB()
{
	if( GetDBLoadState() == DBLOAD_ACTIONED )
		theDBProxyShell.RunUpdateCharacterInfo( this );
}


VOID DBUserImpl::Update()
{
	if( m_DBUpdateTimer.IsExpired() )
	{
		UpdateDataToDB();
	}
}


BOOL DBUserImpl::UpdateExpired()
{
	if( m_CacheExpiredTimer.IsExpired() )
	{
		return FALSE;
	}

	return TRUE;
}


// Destroy
VOID DBUserImpl::StartCacheExpiredTimer() 
{ 
	m_CacheExpiredTimer.SetTimer( MAX_CACHE_TIMEOUT_LENGTH );
	SetCacheState( DBCACHE_WORKING );
}


VOID DBUserImpl::StopCacheExpiredTimer()
{
	m_CacheExpiredTimer.DisableCheckTime();	
	SetCacheState( DBCACHE_NONE );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define	_COMMON_PROCESS(NAME,MAX,PTR,POS,COUNT)\
	COUNT=0;\
	for(SLOTPOS i=0;i<MAX;++i)\
	{\
	if( Exist##NAME(GetSelectedSlotIndex(), i) )\
		{\
			PTR[nTotal].POS = i;\
			Get##NAME(GetSelectedSlotIndex(), i, &PTR[nTotal].m_Stream);\
			++nTotal;\
			++COUNT;\
		}\
	}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define	ITEM_PROCESS(NAME,MAX,COUNT)	_COMMON_PROCESS(NAME,MAX,pSlot,m_ItemPos,msg.m_ItemInfo.COUNT)

#define	UNIT_PROCESS(Name,NAME)\
						MSG_DG_CHARINFO_##NAME##_CMD		msg##Name;\
						msg##Name.m_dwKey					= dwUserKey;\
						s##NAME##_SLOT * p##Name##Slot	= msg##Name.m_##Name##Info.m_Slot;\
						nTotal = 0;\
						_COMMON_PROCESS(Name,MAX_##NAME##_SLOT_NUM,p##Name##Slot,m_##Name##Pos,msg##Name.m_##Name##Info.m_Count);\
						pServerSession->SendPacket( &msg##Name, msg##Name.GetSize() )



VOID DBUserImpl::SendAllToGameShell( IServerSession * pServerSession )
{
	///////////////////////////////////
	//把人物信息发送到	Game Server
	//	CharItem
	//	CharSkill
	//	CharQuick
	//	CharStyle
	//	CharQuest


	///////////////////////////////////
	// DG_CHARINFO_CHAR_ITEM_CMD
	ASSERT(GetUserKey() != 0);
	DWORD dwUserKey = GetUserKey();

	////////////////////////////////////////////
	MSG_DG_CHARINFO_CHAR_ITEM_CMD msg;
	msg.m_dwKey = dwUserKey;
	SerializeCharInfo(GetSelectedSlotIndex()
                    ,msg.m_CharacterInfo
						  ,SERIALIZE_LOAD);



	sITEM_SLOTEX * pSlot = msg.m_ItemInfo.m_Slot;
	SLOTPOS			nTotal = 0;

	//////////////////////////////////////////
	ITEM_PROCESS(EquipItem	,MAX_EQUIPMENT_SLOT_NUM	,m_EquipCount);


	//////////////////////////////////////////
	ITEM_PROCESS(InventoryItem	,MAX_INVENTORY_SLOT_NUM	,m_InvenCount);


	//////////////////////////////////////////
	ITEM_PROCESS(TempInventoryItem	,MAX_TEMPINVENTORY_SLOT_NUM	,m_TmpInvenCount);


	//////////////////////////////////////////
	pServerSession->SendPacket( &msg, msg.GetSize() );



	//////////////////////////////////////////
	// DG_CHARINFO_SKILL_CMD
	UNIT_PROCESS(Skill, SKILL);




	//////////////////////////////////////////
	// DG_CHARINFO_QUICK_CMD
	UNIT_PROCESS(Quick, QUICK);




	//////////////////////////////////////////
	// DG_CHARINFO_STYLE_CMD
	UNIT_PROCESS(Style, STYLE);




	//////////////////////////////////////////
	// DG_CHARINFO_QUEST_CMD
	MSG_DG_CHARINFO_QUEST_CMD msgQuest;
	msgQuest.m_dwKey = dwUserKey;
	SerializeQuests	(GetSelectedSlotIndex()
                     ,msgQuest.m_pQuestStream
							,MAX_QUESTSTREAM_SIZE
							,SERIALIZE_LOAD);
	SerializeMissions	(GetSelectedSlotIndex()
                     ,msgQuest.m_pMissionStream
							,MAX_MISSIONSTREAM_SIZE
							,SERIALIZE_LOAD);
	pServerSession->SendPacket( &msgQuest, sizeof(msgQuest) );

}



VOID DBUserImpl::MakeUpdateWarehouseQuery	(Query * pQuery 
														, Query*			/*pQuery2*/
														, Query*			/*pQuery3*/
														, Query*			/*pQuery4*/)
{
	StringHandle	sSql;

	if( GetWarehouseMoney() > LLONG_MAX )
		SetWarehouseMoney( LLONG_MAX );

	//sSql.Format("{?=call Warehouse_Update (%u, ?, %u) }"
	//			, GetUserGUID()
	//			, GetWarehouseMoney() );

	pQuery->SetQueryF	(SQL_UPDATEWAREHOUSE 
							,GetUserGUID()
							,GetWarehouseMoney() );


	pQuery->SetUserKey( GetUserKey() );
	//pQuery2->SetUserKey( GetUserKey() );
	//pQuery3->SetUserKey( GetUserKey() );
	//pQuery4->SetUserKey( GetUserKey() );


	//pQuery2->SetQueryF(SQL_UPDATEITEMS
	//						,GetUserGUID()
	//						,0
	//						,UpdateItemsQuery::PAGE_WAREHOUSE_11);

	//pQuery3->SetQueryF(SQL_UPDATEITEMS
	//						,GetUserGUID()
	//						,0
	//						,UpdateItemsQuery::PAGE_WAREHOUSE_12);

	//pQuery4->SetQueryF(SQL_UPDATEITEMS
	//						,GetUserGUID()
	//						,0
	//						,UpdateItemsQuery::PAGE_WAREHOUSE_13);


	///////////////////////////////////////////////////
	INT nStart(0);
	sPARAM_UPDATEWAREHOUSE*	pParam = (sPARAM_UPDATEWAREHOUSE*)pQuery->GetQueryParam();
	if(!pParam)
		return;

	SerializeWarehouseItems	(pParam->m_pWarehouseItem
                           ,MAX_WAREHOUSESTREAM_SIZE
									,SERIALIZE_LOAD
									,nStart);
	//nStart += 3;
	//SerializeWarehouseItems	(pQuery2->pCharacterParam[0].m_pItems
 //                          ,MAX_INVENTORYITEMSTREAM_PAGESIZE*3
	//								,SERIALIZE_LOAD
	//								,nStart);

	//nStart += 3;
	//SerializeWarehouseItems	(pQuery3->pCharacterParam[0].m_pItems
 //                          ,MAX_INVENTORYITEMSTREAM_PAGESIZE*3
	//								,SERIALIZE_LOAD
	//								,nStart);
	//nStart += 3;
	//SerializeWarehouseItems	(pQuery4->pCharacterParam[0].m_pItems
 //                          ,MAX_INVENTORYITEMSTREAM_PAGESIZE*1
	//								,SERIALIZE_LOAD
	//								,nStart);


	LOGMSG(LOG_FULL,  "[DBUserImpl::MakeUpdateWarehouseQuery] %s", (LPCTSTR)sSql );
}

VOID DBUserImpl::MakeUpdateCharacterQuery	(Query * pQuery
													, Query*			/*pQuery2*/
													, Query*			/*pQuery3*/)
{
	BYTE CharSlotIndex = GetSelectedSlotIndex();
	
	ASSERT( CharSlotIndex < MAX_CHARACTER_LIST_NUM );

	sPLAYERINFO_BASE & Info = m_TotalInfos[CharSlotIndex].m_pCharInfo;

	Info.m_Money	= BOUND_CLAMP( (MONEY)0, Info.m_Money, (MONEY)LLONG_MAX );
	Info.m_dwExp	= BOUND_CLAMP( (DWORD)0, Info.m_dwExp, (DWORD)INT_MAX );
	Info.m_LV		= BOUND_CLAMP( (LEVELTYPE)0, Info.m_LV, (LEVELTYPE)SHRT_MAX );

	//ASSERT( sQuery.Length() < 255 );
	//if( sQuery.Length() >= 255 ) return;
	//pQuery->SetQuery( sQuery );



	pQuery->SetQueryF	(SQL_UPDATECHAR
							,Info.m_CharGuid		,Info.m_LV			,Info.m_sStrength	,Info.m_sDexterity	,Info.m_sVitality
							,Info.m_sInteligence	,Info.m_sSpirit	,Info.m_sLearn		,Info.m_sCredit		,Info.m_sSkillStat1
							,Info.m_sSkillStat2	,Info.m_UserPoint	,Info.m_dwExp		,Info.m_dwMaxHP		,Info.m_dwHP
							,Info.m_dwMaxMP		,Info.m_dwMP		,Info.m_Money		,Info.m_dwRemainStat	,Info.m_dwRemainSkill
							,Info.m_wSelectStyleCode,	Info.m_byPKState,	Info.m_byCharState,	DBServerUtil::TimeStampText(Info.m_StateTime ),Info.m_dwRegion
							,Info.m_sLocationX,			Info.m_sLocationY,Info.m_sLocationZ, Info.m_szTitleID,	DBServerUtil::TimeStampText(Info.m_TitleTime ),Info.m_byInvisibleOptFlag
							,Info.m_bInventoryLock
							,Info.m_PlayLimitedTime,	Info.m_dwPVPPoint,		Info.m_dwPVPScore,	Info.m_byPVPGrade,	Info.m_dwPVPTotalDraw
							,Info.m_dwPVPMaxSeries,		Info.m_dwPVPTotalKill,	Info.m_dwPVPTotalDie,Info.m_dwPVPMaxKill,	Info.m_dwPVPMaxDie
							);
	pQuery->SetUserKey( GetUserKey() );


	//////////////////////////////////////////////////////
	//Char_UpdateItems(%u,%u,%d,?,?,?,?)
	//pQuery2->SetUserKey( GetUserKey() );
	//pQuery2->SetQueryF(SQL_UPDATEITEMS
	//						,Info.m_UserGuid
	//						,Info.m_CharGuid
	//						,UpdateItemsQuery::PAGE_INVENTORY_3_4);

	//pQuery3->SetUserKey( GetUserKey() );
	//pQuery3->SetQueryF(SQL_UPDATEITEMS
	//						,Info.m_UserGuid
	//						,Info.m_CharGuid
	//						,UpdateItemsQuery::PAGE_INVENTORY_TMP);


	sPARAM_UPDATECHAR*	pParam = (sPARAM_UPDATECHAR*)pQuery->GetQueryParam();

	if(!pParam)
		return;
		
	//////////////////////////////////////////////////////
	SerializeEquipItems				(CharSlotIndex
                                 ,pParam->m_pEquipItem
											,MAX_EQUIPITEMSTREAM_SIZE
											,SERIALIZE_LOAD);
	SerializeInventoryItems			(CharSlotIndex
                                 ,pParam->m_pInventoryItem
											,MAX_INVENTORYITEMSTREAM_SIZE
											,SERIALIZE_LOAD);

	//////////////////////////////////////////////////////
	//SerializeInventoryItems			(CharSlotIndex
 //                                ,pQuery2->pCharacterParam[0].m_pItems
	//										,MAX_INVENTORYITEMSTREAM_PAGESIZE*2 //4、5页
	//										,SERIALIZE_LOAD
	//										,3);

	//////////////////////////////////////////////////////
	SerializeTempInventoryItems	(CharSlotIndex
                                 ,pParam->m_pTempInventoryItem
											,MAX_TEMPINVENTORYITEMSTREAM_SIZE
											,SERIALIZE_LOAD);


	//////////////////////////////////////////////////////
	SerializeSkills		( CharSlotIndex, pParam->m_pSkill,		MAX_SKILLSTREAM_SIZE, SERIALIZE_LOAD );
	SerializeQuests		( CharSlotIndex, pParam->m_pQuest,		MAX_QUESTSTREAM_SIZE, SERIALIZE_LOAD );
	SerializeMissions		( CharSlotIndex, pParam->m_pMission,	MAX_MISSIONSTREAM_SIZE, SERIALIZE_LOAD );
	SerializeQuicks		( CharSlotIndex, pParam->m_pQuick,		MAX_QUICKSTREAM_SIZE, SERIALIZE_LOAD );
	SerializeStyles		( CharSlotIndex, pParam->m_pStyle,		MAX_STYLESTREAM_SIZE, SERIALIZE_LOAD );
}


#include "stdafx.h"
#include "ServerSession.h"
#include "ServerUtil.h"
#include "ServerSessionManager.h"
#include "DBProxyShell.h"
#include "DBUserManager.h"
#include <DatabaseSystem.h>
#include <ServerInfoParser.h>
#include <Protocol_Server.h>
#include <PacketStruct_Server.h>

VOID CallBackMessage		( LPCTSTR  szMessage );
VOID CallBackQueryResult( DWORD dwIndex, Query * pData );


ServerSession::ServerSession()
:m_pDatabase(NULL)
{
	m_bAutoTryConnect		= TRUE;

	m_dwHeartBeatDelay	= _HEARTBEAT_DELAY_BETWEEN_SERVER;
	__ZERO(m_arMsgBufs);
	__ZERO(m_arResultMsgs);
	__ZERO(m_arMsgSizes);
}

ServerSession::~ServerSession()
{
}


BOOL ServerSession::Init()
{
	__BOOL_SUPER(Init());

	sSERVER_INFOC*	pEnv;
	pEnv = theServerInfoParser.GetServerInfo();

	//////////////////////////////////////////////////////////////
	m_pDatabase			= theDatabaseSystem.AllocDatabase();
	__CHECK_PTR(m_pDatabase);

	sDATABASE_DESC db_desc;
	//db_desc.databaseType				= DBCIT_OLEDB;
	//db_desc.databaseType					= (eDATABASE_TYPE)pEnv->SQLSERVER_INFO.nSQLServerType		;//DBCIT_ODBC;
	//db_desc.dwQueryPoolSize				= pEnv->SQLSERVER_INFO.nSQLServerPoolSize	;//2000;
	//db_desc.dwMaxQueryThreadNum		= pEnv->SQLSERVER_INFO.nSQLServerThreadNum;//1;
	//db_desc.szDatabaseIP					= pEnv->SQLSERVER_INFO.szSQLServerIP;


	//db_desc.szDatabaseName				= pEnv->SQLSERVER_INFO.szSQLDBName;
	//db_desc.szUserName					= pEnv->SQLSERVER_INFO.szSQLUserName;
	//db_desc.szUserPwd						= pEnv->SQLSERVER_INFO.szSQLUserPwd;

	theDBProxyShell.GetDBServerInfo(db_desc);

	db_desc.procErrorText				= CallBackMessage;
	db_desc.procQueryResult				= CallBackQueryResult;

	if( !m_pDatabase->Initialize( db_desc ) )
	{
		LOGINFO ( "Database���ӳ�����...\n" );
	}
	return TRUE;
}


VOID ServerSession::RunQuery( Query * pQuery ) 
{ 
	if(m_pDatabase)	
		m_pDatabase->RunQuery( pQuery ); 
}

VOID ServerSession::DBResult( BYTE cate, BYTE ptcl, Query * pData )
{
	MSG_DBRESULT_BASE msg;
	msg.m_byCategory	= cate;
	msg.m_byProtocol	= ptcl;
	msg.m_pData			= pData;
	ParsePacket(&msg, sizeof(MSG_DBRESULT_BASE) );
}

VOID ServerSession::Release()
{
	if(m_pDatabase)
	{
		theDatabaseSystem.FreeDatabase(m_pDatabase);
		m_pDatabase = NULL;
	}

	_SUPER::Release();
}

VOID ServerSession::Update()
{
	if(m_pDatabase)
		m_pDatabase->Update();

	_SUPER::Update();

}

VOID ServerSession::OnConnect( BOOL bSuccess, DWORD dwNetworkIndex )
{
	_SUPER::OnConnect(bSuccess, dwNetworkIndex);

}

VOID ServerSession::OnRecv		( BYTE *pMsg, WORD wSize )
{
	_SUPER::OnRecv(pMsg, wSize);
}

VOID ServerSession::OnAccept( DWORD dwNetworkIndex )
{
	_SUPER::OnAccept(dwNetworkIndex);

}

DWORD ServerSession::GetQueryAmount()
{
	if(m_pDatabase)
		return m_pDatabase->GetQueryAmount();
	return 0;
}

VOID ServerSession::OnDisconnect()
{


	_SUPER::OnDisconnect();
	
}







/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID CallBackMessage( LPCTSTR  szMessage )
{
	LOGINFO(_T("[DB]:%s\n"), szMessage );
}

VOID CallBackQueryResult( DWORD dwIndex, Query * pData )
{
	BYTE byPacketCategory	= (BYTE)LOWORD( dwIndex );
	BYTE byPacketType		= (BYTE)HIWORD( dwIndex );
	((ServerSession *)pData->GetObjectPtr())->DBResult( byPacketCategory, byPacketType, pData );	
}
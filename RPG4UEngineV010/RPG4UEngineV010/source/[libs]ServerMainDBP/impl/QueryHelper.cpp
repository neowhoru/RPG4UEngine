/*////////////////////////////////////////////////////////////////////////
文 件 名：QueryHelper.cpp
创建日期：2007年6月5日
最后更新：2007年6月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "QueryHelper.h"
#include "DBUser.h"
#include "LocalDBData.h"
#include "QueryStruct.h"
#include "DBServerUtil.h"
#include "ServerSetting.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
QueryHelper::QueryHelper()
{
}
QueryHelper::~QueryHelper()
{
}


VOID QueryHelper::GhostCharQueryTo	(DBUser*						pUser
										,BYTE							slotIdx
										,sRESULT_SELECTCHAR *	pCharInfo)
{
	ASSERT( slotIdx < MAX_CHARACTER_LIST_NUM );

	////////////////////////////////
	sPLAYERINFO_BASE&	playerTo	= pUser->m_TotalInfos[slotIdx].m_pCharInfo;

	////////////////////////////////

	ASSERT( pCharInfo->m_iSelectStyleCode <= USHRT_MAX );

	////////////////////////////////
	memcpy( playerTo.m_szCharName	, pCharInfo->m_szCharName	, MAX_CHARNAME_LENGTH );
	playerTo.m_UserGuid				= pCharInfo->m_UserGuid;
	playerTo.m_CharGuid				= pCharInfo->m_CharGuid;
	playerTo.m_bySlot					= pCharInfo->m_bySlot;
	playerTo.m_byClassCode			= pCharInfo->m_byClass;
	playerTo.m_LV						= pCharInfo->m_LV;
	playerTo.m_dwRegion				= pCharInfo->m_iRegion;
	playerTo.m_sLocationX			= pCharInfo->m_sX;
	playerTo.m_sLocationY			= pCharInfo->m_sY;
	playerTo.m_sLocationZ			= pCharInfo->m_sZ;

	memcpy( playerTo.m_szTitleID	, pCharInfo->m_szTitleID	
			, MAX_TITLE_LENGTH );
	
	playerTo.m_dwExp					= pCharInfo->m_iExp;
	playerTo.m_dwMaxHP				= pCharInfo->m_iMaxHP;
	playerTo.m_dwHP					= pCharInfo->m_iHP;
	playerTo.m_dwMaxMP				= pCharInfo->m_iMaxMP;
	playerTo.m_dwMP					= pCharInfo->m_iMP;
	playerTo.m_Money					= pCharInfo->m_Money;
	playerTo.m_dwRemainStat			= pCharInfo->m_iRemainStat;
	playerTo.m_dwRemainSkill		= pCharInfo->m_iRemainSkill;
	playerTo.m_wSelectStyleCode	= pCharInfo->m_iSelectStyleCode;	
	playerTo.m_byPKState				= pCharInfo->m_byPKState;
	playerTo.m_byCharState			= pCharInfo->m_byCharState;
	playerTo.m_StateTime				= DBServerUtil::TimeStamp(pCharInfo->m_StateTime); //leo 
	//playerTo.m_StateTime			= pCharInfo->m_StateTime;

	playerTo.m_TitleTime				= DBServerUtil::TimeStamp(pCharInfo->m_TitleTime);//leo
	//playerTo.m_TitleTime			= pCharInfo->m_TitleTime;

	playerTo.m_bInventoryLock		= pCharInfo->m_bInventoryLock;
	playerTo.m_sStrength				= pCharInfo->m_sStrength;
	playerTo.m_sDexterity			= pCharInfo->m_sDexterity;
	playerTo.m_sVitality				= pCharInfo->m_sVitality;
	playerTo.m_sInteligence			= pCharInfo->m_sInteligence;
	playerTo.m_sSpirit				= pCharInfo->m_sSpirit;
	playerTo.m_sLearn					= pCharInfo->m_sLearn;
	playerTo.m_sCredit				= pCharInfo->m_sCredit;
	playerTo.m_sSkillStat1			= pCharInfo->m_sSkillStat1;
	playerTo.m_sSkillStat2			= pCharInfo->m_sSkillStat2;

	///
	playerTo.m_byHeight				= pCharInfo->m_byHeight;
	playerTo.m_byFace					= pCharInfo->m_byFace;
	playerTo.m_byHair					= pCharInfo->m_byHair;
	playerTo.m_byHairColor			= pCharInfo->m_byHairColor;//leo sex
	playerTo.m_bySex					= pCharInfo->m_bySex;//leo sex

	playerTo.m_PlayLimitedTime		= pCharInfo->m_iPlayLimitedTime;

	// PVP
	playerTo.m_dwPVPPoint			= pCharInfo->m_iPVPPoint;
	playerTo.m_dwPVPScore			= pCharInfo->m_iPVPScore;
	playerTo.m_byPVPGrade			= pCharInfo->m_byPVPGrade;
	//playerTo.m_dwPVPRanking		= pCharInfo->m_iPVPRanking;
	playerTo.m_dwPVPMaxSeries		= pCharInfo->m_iPVPSeries;
	playerTo.m_dwPVPMaxKill			= pCharInfo->m_iPVPMaxKill;
	playerTo.m_dwPVPMaxDie			= pCharInfo->m_iPVPMaxDie;
	playerTo.m_dwPVPTotalKill		= pCharInfo->m_iPVPKill;
	playerTo.m_dwPVPTotalDie		= pCharInfo->m_iPVPDie;
	playerTo.m_dwPVPTotalDraw		= pCharInfo->m_iPVPDraw;

	// 
	playerTo.m_UserPoint				= pCharInfo->m_UserPoint;
	playerTo.m_byInvisibleOptFlag	= pCharInfo->m_byInvisibleOpt;

	//帮派信息
	playerTo.m_GuildGuid				= pCharInfo->m_GuildGuid;
	playerTo.m_GuildPosition		= pCharInfo->m_GuildPosition;

	memcpy( playerTo.m_szGuildNickName,	pCharInfo->m_szGuildNickName	, sizeof(TCHAR)*MAX_CHARNAME_LENGTH );
	memcpy( playerTo.m_szGuildName,		pCharInfo->m_szGuildName		, sizeof(TCHAR)*MAX_GUILDNAME_LENGTH );

	///////////////////////////////////////////
	pUser->SerializeEquipItems			( slotIdx, pCharInfo->m_pEquipItem			, MAX_EQUIPITEMSTREAM_SIZE, SERIALIZE_STORE );
	pUser->SerializeInventoryItems		( slotIdx, pCharInfo->m_pInventoryItem		, MAX_INVENTORYITEMSTREAM_SIZE, SERIALIZE_STORE );
	pUser->SerializeTempInventoryItems( slotIdx, pCharInfo->m_pTempInventoryItem, MAX_TEMPINVENTORYITEMSTREAM_SIZE, SERIALIZE_STORE );

	pUser->SerializeSkills		( slotIdx, pCharInfo->m_pSkill	, MAX_SKILLSTREAM_SIZE	, SERIALIZE_STORE );
	pUser->SerializeQuests		( slotIdx, pCharInfo->m_pQuest	, MAX_QUESTSTREAM_SIZE	, SERIALIZE_STORE );
	pUser->SerializeMissions	( slotIdx, pCharInfo->m_pMission	, MAX_MISSIONSTREAM_SIZE, SERIALIZE_STORE );
	pUser->SerializeQuicks		( slotIdx, pCharInfo->m_pQuick	, MAX_QUICKSTREAM_SIZE	, SERIALIZE_STORE );
	pUser->SerializeStyles		( slotIdx, pCharInfo->m_pStyle	, MAX_STYLESTREAM_SIZE	, SERIALIZE_STORE );


	if(theServerSetting.m_bLocalPlayerData)
	{
		theLocalDBData.SerializeEquipItems			(pUser->m_TotalInfos[slotIdx].m_arEquips			, MAX_EQUIPITEMSTREAM_SIZE);
		theLocalDBData.SerializeInventoryItems		(pUser->m_TotalInfos[slotIdx].m_arInventories	, MAX_INVENTORYITEMSTREAM_SIZE);
		theLocalDBData.SerializeTempInventoryItems(pUser->m_TotalInfos[slotIdx].m_arTempInventories, MAX_TEMPINVENTORYITEMSTREAM_SIZE);

		theLocalDBData.SerializeSkills		(pUser->m_TotalInfos[slotIdx].m_arSkills	, MAX_SKILLSTREAM_SIZE	);
		//theLocalDBData.SerializeQuests		(pUser->m_TotalInfos[slotIdx].m_arQuests	, MAX_QUESTSTREAM_SIZE	);
		//theLocalDBData.SerializeMissions	(pUser->m_TotalInfos[slotIdx].m_arMissions, MAX_MISSIONSTREAM_SIZE);
		theLocalDBData.SerializeQuicks		(pUser->m_TotalInfos[slotIdx].m_arQuicks	, MAX_QUICKSTREAM_SIZE	);
		theLocalDBData.SerializeStyles		(pUser->m_TotalInfos[slotIdx].m_arStyles	, MAX_STYLESTREAM_SIZE	);
	}

}

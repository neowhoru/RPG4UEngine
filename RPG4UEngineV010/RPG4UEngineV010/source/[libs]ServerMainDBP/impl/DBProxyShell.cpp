/*////////////////////////////////////////////////////////////////////////
文 件 名：DBProxyShell.cpp
创建日期：2009年3月30日
最后更新：2009年3月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <NetworkGroup.h>
#include <ISessionHandle.h>
#include <conio.h>
#include "DBProxyShell.h"
#include "DatabaseProxyQuery.h"
#include "ServerSessionManager.h"
#include "ServerSession.h"
#include "DBUserManager.h"
#include "DBUSer.h"
#include "DBSerialGenerator.h"
#include "DatabaseSystem.h"
#include <QueryStruct.h>

#include <DatabaseDefine.h>
#include <Version.h>
#include <BuildVersion.h>
////#include <ServerGlobal.h>
#include <ServerSetting.h>
#include <ServerInfoParser.h>
#include <ConstInfoSql.h>

#include "DBProxyShell.inc.h"

//#include "TestingDB.h"


#define		USE_STATIC_LOG
const DWORD	SLEEP_IDLE		=	10;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_IMPL(DBProxyShell);




DBProxyShell::DBProxyShell()
{
GLOBALINST_SINGLETON_PTR_INIT(DBProxyShell);
	m_pDatabase				= NULL;
	m_pDBSerialGenerator	= NULL;

}

DBProxyShell::~DBProxyShell()
{
GLOBALINST_SINGLETON_PTR_FREE(DBProxyShell);
}

//-------------------------------------------------------------------------------------------------
BOOL DBProxyShell::Init()
{
	//sSERVER_INFOC*		pEnv;



	/////////////////////////////////////////
	__BOOL_SUPER(Init() );


	/////////////////////////////////////////
	//pEnv	= theServerInfoParser.GetServerInfo();


	theDBUserManager.Init( 1000 );



	//////////////////////////////////
	m_pDBSerialGenerator = new DBSerialGenerator;	





	//////////////////////////////////////////////////////////////
	m_pDatabase			= theDatabaseSystem.AllocDatabase();
	__CHECK_PTR(m_pDatabase);

	sDATABASE_DESC dbInfo;
	GetDBServerInfo(dbInfo);

	//dbInfo.databaseType			= DBCIT_OLEDB;
	dbInfo.procErrorText				= CallBackMessage1;
	dbInfo.procQueryResult			= CallBackQueryResult1;

	if( !m_pDatabase->Initialize( dbInfo ) )
	{
		DWORD dwErr = GetLastError();
		LOGINFO	("数据库连接出错\n(数据源:%s,IP:%s,ModuleType:%d,PoolSize:%d,ThreadNum:%d,User:%s,Pwd:%s)...\n请检测数据源(%s)是否正确配置\n"
					,dbInfo.szDatabaseName
					,dbInfo.szDatabaseIP
					,dbInfo.databaseType			
					,dbInfo.dwQueryPoolSize		
					,dbInfo.dwMaxQueryThreadNum
					,dbInfo.szUserName		
					,dbInfo.szUserPwd
					,dbInfo.szDatabaseName
					);
		//return FALSE;
	}




	sNETWORK_INFO desc[1];


	desc[0].fnCreateAcceptedSession		= CreateAcceptedObject;
	desc[0].fnDestroyAcceptedSession		= DestroyAcceptedObject;
	desc[0].fnDestroyWorkingSession		= DestroyConnectedObject;


	if( !_InitNetworkCS( desc, NETWORK_SERVER ) )
		return FALSE;




	/////////////////////////////////////////////////



	LOGMSG( LOG_FULL, "DBProxy Server Start " );



	///////////////////////////////////////////
	if(!IsUserDBProxy())
		RunSelectSerialQuery();

	return TRUE;
}



VOID DBProxyShell::Release()
{
	/////////////////////////////////////
	DISPRELEASE( "  Release DBInfo...\n" );
	SAFE_DELETE(m_pDBSerialGenerator);

	if(m_pDatabase)
	{
		theDatabaseSystem.FreeDatabase(m_pDatabase);
		m_pDatabase = NULL;
	}


	/////////////////////////////////////
	DISPRELEASE( "  Release Users...\n" );
	theDBUserManager.Release();



	/////////////////////////////////////



	/////////////////////////////////////
	DISPRELEASE( "  Release Other Datas...\n" );




	/////////////////////////////////////
	_SUPER::Release();

	DISPRELEASE( "  Release Finished... !\n" );
}




//DWORD DBProxyShell::ConnectToServer( ISessionHandle * pSessionHandle, char * pszIP, WORD wPort )
//{
//	return m_pNetworkGroup->Connect( IOHANDLER_SERVER_IDX, pSessionHandle, pszIP, wPort );
//}

VOID DBProxyShell::Run(DWORD dwDelay)
{

	StartListen	();

	_SUPER::Run(dwDelay);


}


BOOL DBProxyShell::FrameMove(DWORD dwTick)
{
	__BOOL_SUPER(FrameMove(dwTick));

#ifndef NONE_TESTING
	theTestingSession.Update();
#endif

	if(m_pDatabase)
		m_pDatabase->Update();
	theDBUserManager.UpdateUsers();




	return TRUE;
}

VOID DBProxyShell::Shutdown()
{
	LOGINFO("Shutdow now, waiting for database disconnect...");
	_SUPER::Shutdown();

	//testing
	if(!IsUserDBProxy())
		RunUpdateSerialQuery();
}

VOID DBProxyShell::Testing(LPCTSTR szParam)
{
#ifndef NONE_TESTING
	if(szParam[0] >= '0' && szParam[0] <= '9')
		theTestingSession.TestingSql(szParam[0] - '0');
#endif
}




/*
class CalcQueryCount
{
	DWORD	m_totalNum;
public: 
	CalcQueryCount():m_totalNum(0){}
	~CalcQueryCount(){}
	DWORD	GetTotalCount() { return m_totalNum; }
	VOID operator()( ServerSession * pSession )
	{
		if( pSession->IsDBInited() )
		{
			m_totalNum += pSession->GetQueryAmount();
		}
	}
};
*/
VOID DBProxyShell::DisplayServerInfo()
{
	//	CalcQueryCount op;
	//	ServerSessionManager::GetInstance().Foreach(op);



	_SUPER::DisplayServerInfo();

	DISPMSG( "DBUser Connections		: %u \n", theDBUserManager.GetPlayerAmount() );
	DISPMSG( "Cached DBUser Connections	: %u \n", theDBUserManager.GetCachedPlayerAmount() );
}




DBSERIAL DBProxyShell::AllocSerial()
{
	if( m_pDBSerialGenerator->NeedQuerySerial() )
		RunSelectSerialQuery();
	return m_pDBSerialGenerator->AllocSerial();
}





//--------------------------------------------------------------------------------------------------------
// Global DB Query
//--------------------------------------------------------------------------------------------------------
VOID DBProxyShell::RunSelectSerialQuery()
{
	if(!m_pDatabase)
		return;

	Query * pQuery = SAFE_ALLOCQUERY(CreateItemSerialQuery);
	if(!pQuery)
		return;

	pQuery->SetIndex		( QUERY_SELECTDBSERIAL );
	pQuery->SetObjectPtr( this );
	pQuery->SetQueryF		( SQL_CREATEITEMSEQ//_T("ItemSeq_Create %u")
								, sRESULT_CREATEITEMSERIAL::DBSERIAL_SPACE_SIZE );

	m_pDatabase->RunQuery( pQuery );

}


VOID DBProxyShell::RunUpdateSerialQuery()
{
	if(!m_pDatabase)
		return;

	Query * pQuery = SAFE_ALLOCQUERY(UpdateItemSerialQuery);
	if(!pQuery)
		return;

	pQuery->SetIndex		( QUERY_UPDATEDBSERIAL );
	pQuery->SetObjectPtr( this );
	pQuery->SetQueryF		( SQL_UPDATEITEMSEQ//_T("ItemSeq_Update %u")
								, m_pDBSerialGenerator->GetCurrentSerial() );

	m_pDatabase->RunQuery( pQuery );	
}



VOID DBProxyShell::RunUpdateWarehouseQuery( DBUser * pUser )
{	
	if(!m_pDatabase)
		return;

	if( pUser->IsWarehouseModified() )
	{
		Query		* pQuery  = SAFE_ALLOCQUERY(UpdateWarehouseQuery);
		//UpdateItemsQuery		* pQuery2 = SAFE_ALLOCQUERY(UpdateItemsQuery);
		//UpdateItemsQuery		* pQuery3 = SAFE_ALLOCQUERY(UpdateItemsQuery);
		//UpdateItemsQuery		* pQuery4 = SAFE_ALLOCQUERY(UpdateItemsQuery);

		pUser->MakeUpdateWarehouseQuery(pQuery);//,pQuery2,pQuery3,pQuery4);

		pQuery->SetIndex( QUERY_UPDATEWAREHOUSE );
		pQuery->SetObjectPtr( this );

		//pQuery2->SetIndex( QUERY_UPDATEWAREHOUSE2 );
		//pQuery2->SetObjectPtr( this );

		//pQuery3->SetIndex( QUERY_UPDATEWAREHOUSE3 );
		//pQuery3->SetObjectPtr( this );

		//pQuery4->SetIndex( QUERY_UPDATEWAREHOUSE3 );
		//pQuery4->SetObjectPtr( this );

		m_pDatabase->RunQuery( pQuery  );
		//m_pDatabase->RunQuery( pQuery2 );
		//m_pDatabase->RunQuery( pQuery3 );
		//m_pDatabase->RunQuery( pQuery4 );
		pUser->SetWarehouseModified(FALSE);
	}
}

VOID DBProxyShell::RunUpdateCharacterInfo( DBUser * pUser )
{
	if(!m_pDatabase)
		return;
	Query  * pQuery = SAFE_ALLOCQUERY(UpdateCharacterQuery);
	//UpdateItemsQuery		 * pQuery2 = SAFE_ALLOCQUERY(UpdateItemsQuery);
	//UpdateItemsQuery		 * pQuery3 = SAFE_ALLOCQUERY(UpdateItemsQuery);
	pUser->MakeUpdateCharacterQuery( pQuery);// ,pQuery2, pQuery3);

	pQuery->SetIndex( QUERY_UPDATECHARACTER );
	pQuery->SetObjectPtr( this );

	//pQuery2->SetIndex( QUERY_UPDATECHARACTER2 );
	//pQuery2->SetObjectPtr( this );

	//pQuery3->SetIndex( QUERY_UPDATECHARACTER2 );
	//pQuery3->SetObjectPtr( this );

	m_pDatabase->RunQuery( pQuery );

}

VOID DBProxyShell::DBResult( DWORD dwIndex, Query * pData )
{
	if(!pData)
		return;

	Query *				pQData			= (Query *)pData;
	sRESULT_QUERY*		pQueryResult	= pQData->GetQueryResult();

	if(!pQueryResult)
		goto laEnd;

	/////////////////////////////////////////////
	switch( dwIndex )
	{
	case 99:
		{
			//TestQuery * pQData = (TestQuery *)pData;
		}
		break;

		//////////////////////////////////////////////////
	case QUERY_SELECTDBSERIAL:
		{
			sRESULT_CREATEITEMSERIAL*	pResult = (sRESULT_CREATEITEMSERIAL*)pQueryResult;
			m_pDBSerialGenerator->SetSerial	(pResult->m_dwCurSerial
														,sRESULT_CREATEITEMSERIAL::DBSERIAL_SPACE_SIZE );

			LOGINFO	("设置序列号: (当前:%u,封顶:%u) [QUERY_SELECTDBSERIAL]\n"
						, pResult->m_dwCurSerial
						, pResult->m_dwCurSerial + sRESULT_CREATEITEMSERIAL::DBSERIAL_SPACE_SIZE );
		}
		break;

		//////////////////////////////////////////////////
	case QUERY_UPDATEDBSERIAL:
		{
			LOGINFO	( "更新序列号: (%u) [QUERY_UPDATEDBSERIAL]\n"
						, m_pDBSerialGenerator->GetCurrentSerial() );
			SetRunState( RUNSTATE_TOSHUTDOWN );
		}
		break;

	case QUERY_UPDATEWAREHOUSE:
		{
			if(!pQData->IsResultSucceed() )
			{
				LOGINFO	( "更新仓库:[U:%u][%d] [QUERY_UPDATEWAREHOUSE] failed\n"
							, pQData->GetUserKey()
							, pQData->GetResult() );
			}
		}
		break;

		//////////////////////////////////////////////////
	case QUERY_UPDATEWAREHOUSE2:
	case QUERY_UPDATEWAREHOUSE3:
	case QUERY_UPDATECHARACTER2:
		{
			//UpdateItemsQuery * pQData = (UpdateItemsQuery *)pData;
			//if( UpdateItemsQuery::QUERY_RESULT_SUCCEEDED != pQData->pCharacterParam[0].m_Result )
			//{
			//	LOGINFO	( "更新物品组:[U:%u][%d] [QUERY_UPDATECHARACTER2]\n"
			//				, pQData->GetUserKey()
			//				, pQData->pCharacterParam[0].m_Result );
			//}
			//else
			//{
			//}
			//QUERYFREE( UpdateItemsQuery, pQData );
		}
		break;


	case QUERY_UPDATECHARACTER:
		{
			if(!pQData->IsResultSucceed() )
			{
				LOGINFO	( "更新人物信息：[U:%u][%d]:[QUERY_UPDATECHARACTER]\n"
							, pQData->GetUserKey()
							, pQData->GetResult() );
				//LOGMSG(LOG_FULL,  "[DBProxyShell::DBResult] %s\n", pQData->GetQuery() );
			}
			else
			{
				//LOGMSG(LOG_FULL,  "[U:%d][己傍]:某腐磐沥焊历厘己傍", pQData->GetUserKey() );
			}
		}
		break;
	}

laEnd:
	SAFE_FREEQUERY(pQData);
}


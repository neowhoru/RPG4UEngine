/*////////////////////////////////////////////////////////////////////////
文 件 名：LoginSession.h
创建日期：2006年8月6日
最后更新：2006年8月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __LOGINESSION_H__
#define __LOGINESSION_H__
#pragma once


#include "serversession.h"
#include "ServerSessionFactoryManager.h"


class _SERVERMAINDBP_API LoginSession : public ServerSession
{
public:
	LoginSession();
	virtual ~LoginSession();

public:
	eSERVER_TYPE		GetServerType() { return LOGIN_SHELL;	}

protected:
	VOID		OnRecv		( BYTE * pMsg, WORD wSize );
	 VOID		OnDisconnect();
};


SERVERSESSION_FACTORY_DECLARE(LoginSession);

//#pragma pack(pop)


#endif //__LOGINESSION_H__
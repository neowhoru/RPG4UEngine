/*////////////////////////////////////////////////////////////////////////
文 件 名：ActiveUser.h
创建日期：2007年2月16日
最后更新：2007年2月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __ACTIVEUSER_H__
#define __ACTIVEUSER_H__
#pragma once


#include "User.h"
#include "UserFactoryManager.h"


class _SERVERMAINLOGIN_API ActiveUser : public User
{
public:
	ActiveUser(void);
	~ActiveUser(void);

	virtual BOOL			Init();
	virtual VOID			Release();
	virtual VOID			Update();
	virtual VOID			OnRedirect();

protected:
	virtual VOID			OnAccept( DWORD dwNetworkIndex );
	virtual VOID			OnRecv( BYTE *pMsg, WORD wSize );
};

USER_FACTORY_DECLARE(ActiveUser);


#endif //__ACTIVEUSER_H__
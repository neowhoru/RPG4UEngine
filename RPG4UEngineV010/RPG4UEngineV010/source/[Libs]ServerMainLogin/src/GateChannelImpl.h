/*////////////////////////////////////////////////////////////////////////
文 件 名：GateChannelImpl.h
创建日期：2007年12月6日
最后更新：2007年12月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __GATECHANNELIMPL_H__
#define __GATECHANNELIMPL_H__
#pragma once

#include <GateChannel.h>
#include <GateChannelFactoryManager.h>

class GateSession;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class GateChannelImpl : public GateChannel
{
public:
	GateChannelImpl(void);

	virtual DWORD GetFactoryType() {return GATECHANNEL_DEFAULT;}

public:
	void Init			();
	void Release		();
	void DisplayInfo	();
	void Update			();

public:
	void OnGetGameStatus		(DWORD dwTimeStamp, DWORD dwUserCount);

	BOOL SendToGateServer	(VOID* pMsg, WORD wSize);
	DWORD GetServerStatus	();
};

GATECHANNEL_FACTORY_DECLARE(GateChannelImpl);

#endif //__GATECHANNELIMPL_H__
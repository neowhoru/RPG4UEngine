/*////////////////////////////////////////////////////////////////////////
文 件 名：TestUserSession.h
创建日期：2007年12月21日
最后更新：2007年12月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	用来模拟客户端的登录系列的业务逻辑协议发送流程

版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TESTUSERSESSION_H__
#define __TESTUSERSESSION_H__
#pragma once

#ifndef NONE_TESUSER

#include "ISessionHandle.h"


class TestUserSession : public ISessionHandle
{
public:
	TestUserSession();
	~TestUserSession();

	virtual VOID				Init();
	virtual VOID				Release();
	virtual VOID				Update();

protected:
	virtual VOID				OnAccept( DWORD dwNetworkIndex );
	virtual VOID				OnConnect( BOOL bSuccess, DWORD dwNetworkIndex );
	virtual VOID				OnDisconnect();
	virtual	VOID				OnRecv( BYTE *pMsg, WORD wSize );

private:
	VOID						SendServerInfoSyn();
	DWORD	 m_dwEncKey;
};

#endif

#endif //__TESTUSERSESSION_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ServerSessionManagerInstance.h
创建日期：2007年12月1日
最后更新：2007年12月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __SERVERSESSIONMANAGERINSTANCE_H__
#define __SERVERSESSIONMANAGERINSTANCE_H__
#pragma once



#pragma once


#include <GlobalInstanceSingletonDefine.h>
#include <CommonDefine.h>
#include <ServerSessionManager.h>

using namespace util;
using namespace std;

class ServerSession;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERMAINLOGIN_API ServerSessionManagerInstance : public ServerSessionManager
{
public:
	ServerSessionManagerInstance();
	~ServerSessionManagerInstance();

public:
	virtual		BOOL		OnAddServer		( IServerSession * );
	virtual		BOOL		OnRemoveServer	( IServerSession * );

private:
	VG_PTR_GET_PROPERTY			(MasterShell,	IServerSession);
	VG_PTR_GET_PROPERTY			(UserDBProxy,	IServerSession);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ServerSessionManagerInstance , API_NULL);
//extern API_NULL ServerSessionManagerInstance& singleton::GetServerSessionManager();
#define theServerSessionManagerInstance  singleton::GetServerSessionManagerInstance()



#endif //__SERVERSESSIONMANAGERINSTANCE_H__
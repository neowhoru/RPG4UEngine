/*////////////////////////////////////////////////////////////////////////
文 件 名：TempUser.h
创建日期：2007年2月19日
最后更新：2007年2月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TEMPUSER_H__
#define __TEMPUSER_H__
#pragma once

#include "User.h"
#include "UserFactoryManager.h"

class _SERVERMAINLOGIN_API TempUser : public User
{
public:
	TempUser(void);
	~TempUser(void);

	virtual BOOL		Init();

protected:
	virtual VOID		OnAccept( DWORD dwNetworkIndex );
	virtual VOID		OnRecv( BYTE *pMsg, WORD wSize );

private:
	BOOL				m_bFistTime;
};

USER_FACTORY_DECLARE(TempUser);

#endif //__TEMPUSER_H__
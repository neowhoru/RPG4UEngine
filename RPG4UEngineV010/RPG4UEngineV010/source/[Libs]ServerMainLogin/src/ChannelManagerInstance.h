/*////////////////////////////////////////////////////////////////////////
文 件 名：ChannelManagerInstance.h
创建日期：2007年12月8日
最后更新：2007年12月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHANNELMANAGERINSTANCE_H__
#define __CHANNELMANAGERINSTANCE_H__
#pragma once

#include "ChannelManager.h"




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChannelManagerInstance : public ChannelManager
{
public:
	ChannelManagerInstance(void);
	virtual ~ChannelManagerInstance(void);

public:
	virtual VOID				Init		();
	virtual VOID				Release	();
	virtual VOID				Update	();


	virtual VOID				DisplayChannelInfo();
	virtual void				SyncInfoToUsers	();
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ChannelManagerInstance , API_NULL);
//extern API_NULL ChannelManagerInstance& singleton::GetChannelManagerInstance();
#define theChannelManagerInstance  singleton::GetChannelManagerInstance()


#endif //__CHANNELMANAGERINSTANCE_H__
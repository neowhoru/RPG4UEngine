/*////////////////////////////////////////////////////////////////////////
文 件 名：UserManagerInstance.h
创建日期：2007年2月5日
最后更新：2007年2月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __USERMANAGERINSTANCE_H__
#define __USERMANAGERINSTANCE_H__
#pragma once

#include <GlobalInstanceSingletonDefine.h>
#include <UserManager.h>


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UserManagerInstance : public UserManager
{
public:
	UserManagerInstance(void);
	virtual ~UserManagerInstance(void);

public:
	virtual VOID		Init		();
	virtual VOID		Release	();

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(UserManagerInstance , API_NULL);
//extern API_NULL UserManagerInstance& singleton::GetUserManagerInstance();
#define theUserManagerInstance  singleton::GetUserManagerInstance()



#endif //__USERMANAGERINSTANCE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：WaitingUserManagerInstance.h
创建日期：2007年2月12日
最后更新：2007年2月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __WAITINGUSERMANAGERINSTANCE_H__
#define __WAITINGUSERMANAGERINSTANCE_H__
#pragma once


#include <WaitingUserManager.h>

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class WaitingUserManagerInstance : public WaitingUserManager
{
public:
	WaitingUserManagerInstance(void);
	virtual ~WaitingUserManagerInstance(void);

public:
	virtual VOID		Init			();
	virtual VOID		Release		();
	virtual VOID		Update		();

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(WaitingUserManagerInstance , API_NULL);
//extern API_NULL WaitingUserManagerInstance& singleton::GetWaitingUserManagerInstance();
#define theWaitingUserManagerInstance  singleton::GetWaitingUserManagerInstance()


#endif //__WAITINGUSERMANAGERINSTANCE_H__
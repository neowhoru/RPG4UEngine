/*////////////////////////////////////////////////////////////////////////
文 件 名：GateSession.h
创建日期：2007年12月1日
最后更新：2007年12月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	负责与GateShell通信的Session连接

版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __GATESESSION_H__
#define __GATESESSION_H__
#pragma once



#include "CommonDefine.h"
#include "ServerSession.h"
#include "ServerSessionFactoryManager.h"

class GateChannel;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERMAINLOGIN_API GateSession : public ServerSession
{
public:
	GateSession();
	~GateSession();

public:
	virtual eSERVER_TYPE		GetServerType() { return GATE_SHELL; }

public:
	virtual BOOL				Init			();
	virtual VOID				Release		();
	virtual VOID				Update		();
	virtual VOID				OnRedirect	();

protected:
	virtual VOID				OnConnect		( BOOL bSuccess, DWORD dwNetworkIndex );
	virtual VOID				OnDisconnect	();
	virtual	VOID				OnRecv			(BYTE *pMsg, WORD wSize );


protected:
	VG_PTR_PROPERTY			(Channel, GateChannel);
};


SERVERSESSION_FACTORY_DECLARE(GateSession);



#endif //__GATESESSION_H__
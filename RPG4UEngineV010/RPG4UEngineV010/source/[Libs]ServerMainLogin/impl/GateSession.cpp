/*////////////////////////////////////////////////////////////////////////
文 件 名：GateSession.cpp
创建日期：2007年12月1日
最后更新：2007年12月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketStruct_Base.h"
#include <PacketStruct_Gate_Login.h>
#include <Protocol_Gate_Login.h>
#include "ConstDefine.h"
#include "GateSession.h"
#include "LoginShell.h"
#include "PacketHandlerManager.h"
#include "ChannelManager.h"



GateSession::GateSession()
:m_pChannel(NULL)
{
}

GateSession::~GateSession()
{
}

BOOL GateSession::Init()
{
	return _SUPER::Init();

	//m_dwChannelID = 0;
}

VOID GateSession::Release()
{
	_SUPER::Release();
}

VOID GateSession::Update()
{
	_SUPER::Update();
}

VOID GateSession::OnRedirect()
{
	MessageOut(LOG_CRITICAL,   "Gate server connected at.(%s)", GetIP() );	
}

VOID GateSession::OnConnect( BOOL bSuccess, DWORD dwNetworkIndex )
{
	_SUPER::OnConnect( bSuccess, dwNetworkIndex );
}

VOID GateSession::OnDisconnect()
{
	_SUPER::OnDisconnect();

	theChannelManager.RemoveChannel( m_pChannel );

	MessageOut(LOG_CRITICAL,   "Gate server disconnected." );
}

VOID GateSession::OnRecv( BYTE *pMsg, WORD wSize )
{
	_SUPER::OnRecv( pMsg,  wSize );

}

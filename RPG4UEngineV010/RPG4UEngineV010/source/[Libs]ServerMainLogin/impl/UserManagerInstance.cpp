/*////////////////////////////////////////////////////////////////////////
文 件 名：UserManagerInstance.cpp
创建日期：2007年2月5日
最后更新：2007年2月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "UserManagerInstance.h"
#include "User.h"
#include "KeyUserList.h"
#include "StructBase.h"
#include "UserFactoryManager.h"
#include "LoginShellInfo.h"
#include "ActiveUser.h"
#include "TempUser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UserManagerInstance, ()  , gamemain::eInstPrioGameFunc);


UserManagerInstance::UserManagerInstance(void)
{
}

UserManagerInstance::~UserManagerInstance(void)
{
}

VOID UserManagerInstance::Init()
{
	////////////////////////////////////
	sIOHANDLER_INFO *pDesc;
	pDesc = theLoginShellInfo.GetClientIoDesc();

	REG_USERFACTORY	(ActiveUser,	ACTIVE_USER , pDesc->dwMaxAcceptSession);
	REG_USERFACTORY	(TempUser,		TEMP_USER	, pDesc->dwMaxAcceptSession);

	_SUPER::Init();
}

VOID UserManagerInstance::Release()
{
	_SUPER::Release();
}




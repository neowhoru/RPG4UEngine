/*////////////////////////////////////////////////////////////////////////
文 件 名：ServerSessionManagerInstance.cpp
创建日期：2007年12月1日
最后更新：2007年12月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ServerSessionManagerInstance.h"
#include "ServerSession.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ServerSessionManagerInstance, ()  , gamemain::eInstPrioGameFunc);


ServerSessionManagerInstance::ServerSessionManagerInstance()
{
	m_pMasterShell	= NULL;
	m_pUserDBProxy = NULL;
}

ServerSessionManagerInstance::~ServerSessionManagerInstance()
{
}


BOOL ServerSessionManagerInstance::OnAddServer( IServerSession * pServer )
{
	switch( pServer->GetServerType() )
	{
	case MASTER_SHELL:
		m_pMasterShell	= pServer;	
		break;

	case USER_DBPROXY:
		m_pUserDBProxy = pServer;
		break;
	}
	return TRUE;
}


BOOL ServerSessionManagerInstance::OnRemoveServer( IServerSession * pServer )
{
	switch( pServer->GetServerType() )
	{
	case MASTER_SHELL:
		m_pMasterShell = NULL;	
		break;
	case USER_DBPROXY:
		m_pUserDBProxy = NULL;	
		break;
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：ActiveUser.cpp
创建日期：2007年2月16日
最后更新：2007年2月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "ActiveUser.h"
#include "PacketHandlerManager.h"
#include "UserManager.h"
#include "EncodeMD5.h"
#include "TimeHelper.h"

//OnRecv
#include "PacketStruct_Base.h"

ActiveUser::ActiveUser(void)
{
	m_UserType		= ACTIVE_USER;
}

ActiveUser::~ActiveUser(void)
{
}

BOOL ActiveUser::Init()
{
	return _SUPER::Init();
}

VOID ActiveUser::Release()
{
	_SUPER::Release();
}

VOID ActiveUser::Update()
{
	_SUPER::Update();
}

VOID ActiveUser::OnRedirect()
{
	_SUPER::OnRedirect();


	StringHandle	sKey;
	StringHandle	sTime;

	TimeHelper::GetTime(TIME_FORMAT_MS,sTime);
	sKey.Format	(_T("%d#%s")
					,GetAuthSequence()
					,(LPCTSTR)sTime);

	sKey = EncodeMD5::GetMD5(sKey);


	SetSerialKey	((TBYTE*)sKey.GetBuffer(), sKey.Length());
	SetMD5Key		((TBYTE*)sKey.GetBuffer(), sKey.Length());

	//#pragma message(__FILE__  "(51) 生成 m_szSerialKey 序列信息  " )
}


VOID ActiveUser::OnAccept(DWORD dwNetworkIndex)
{
	ASSERT( !"此函数不作业务逻辑!" );
}

VOID ActiveUser::OnRecv( BYTE *pMsg, WORD wSize )
{
	MSG_BASE *pRecvMsg = (MSG_BASE*)pMsg;


	if( !ParsePacket((MSG_BASE*)pMsg, wSize ) )
	{
		//LOGINFO	("[%s][Guid:%u] Client Packet Parse Error !(C:%u)(P:%u)\n"
		//			,GetAccount()
		//			,GetGUID()
		//			,pRecvMsg->m_byCategory
		//			,pRecvMsg->m_byProtocol);

		//Logout();
	}
}
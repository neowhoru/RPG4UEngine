/*////////////////////////////////////////////////////////////////////////
文 件 名：TestUserSession.cpp
创建日期：2007年12月21日
最后更新：2007年12月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"

#ifndef NONE_TESUSER

#include <PacketStruct_ClientLogin.h>
#include <Protocol_ClientLogin.h>
#include "ConstDefine.h"
#include "TestUserSession.h"
#include "LoginShell.h"
#include "PacketHandlerManager.h"
#include "Version.h"
#include "PacketCryptManager.h"

#pragma message(__FILE__  "(23)  TestUserSession " )

TestUserSession::TestUserSession()
{
}

TestUserSession::~TestUserSession()
{
}


VOID TestUserSession::Init()
{
	//ServerSession::Init();
}

VOID TestUserSession::Release()
{
	//ServerSession::Release();
}

VOID TestUserSession::Update()
{
	//ServerSession::Update();
}

VOID TestUserSession::OnAccept( DWORD dwNetworkIndex )
{
	//ServerSession::OnAccept( dwNetworkIndex );
}

VOID TestUserSession::OnConnect( BOOL bSuccess, DWORD dwNetworkIndex )
{
	//ServerSession::OnConnect( bSuccess, dwNetworkIndex );
	if(bSuccess)
	{
	}
}

VOID TestUserSession::OnDisconnect()
{
	//ServerSession::OnDisconnect();
	MessageOut(LOG_CRITICAL,   "TestUserSession Disconnected." );
}

VOID TestUserSession::OnRecv( BYTE *pMsg, WORD wSize )
{
	MSG_BASE *pBaseMsg = (MSG_BASE*)pMsg;

	if(pBaseMsg->m_byCategory == CL_LOGIN)
	{
		switch(pBaseMsg->m_byProtocol)
		{
		case CL_LOGIN_USER_READY_CMD:
			{
				MSG_CL_LOGIN_USER_READY_CMD* pRecvMsg = (MSG_CL_LOGIN_USER_READY_CMD*)pBaseMsg;
				m_dwEncKey		= pRecvMsg->dwEncKey;
				MSG_CL_LOGIN_VERSION_CHECK_SYN  sync;

				sync.byHighVersion	= VERSION_C2S_HIGH_NO;
				sync.byMiddleVersion	= VERSION_C2S_MIDDLE_NO;
				sync.byLowVersion		= VERSION_C2S_LOW_NO;
				strcpy(sync.szLocalIP,	GetIP());
				sync.dwEncKey		= m_dwEncKey;
				Send((BYTE*)&sync,sizeof(sync));
			}
			break;
		case CL_LOGIN_VERSION_CHECK_ACK:
			{
				MSG_CL_LOGIN_VERSION_CHECK_ACK *pRecvPacket = (MSG_CL_LOGIN_VERSION_CHECK_ACK *)pMsg;
				BYTE    byResult = pRecvPacket->byResult;

				if (byResult == 0)
				{
					MSG_CL_LOGIN_ACCOUNT_LOGIN_SYN	msg;
					msg.dwAuthUserID	= m_dwEncKey;
					strcpy(msg.szID, "leo");
					strcpy(msg.szPasswd, "320");

					thePacketCryptManager.PacketEncode( (unsigned char *)msg.szPasswd,
											  MAX_PASSWORD_LENGTH,
											  (unsigned char *)msg.szPasswd,
											  m_dwEncKey);

					Send((BYTE*)&msg,sizeof(msg));
				}
			}
			break;

		case CL_LOGIN_ACCOUNT_LOGIN_ACK:
			{
				MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK *pRecvPacket = (MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK *)pMsg;
				BYTE		byResult	=  pRecvPacket->byResult;				
				char*		szInfo	=	pRecvPacket->szInfo;	
				if(byResult == MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_OK)
				{
					MSG_CL_LOGIN_GATESTATUS_LIST_SYN	msg;
					Send((BYTE*)&msg,sizeof(msg));
				}
			}
			break;

		case CL_LOGIN_GATESTATUS_LIST_ACK:
			{
				MSG_CL_LOGIN_GATESTATUS_LIST_ACK* pMsgRecv = (MSG_CL_LOGIN_GATESTATUS_LIST_ACK*)pMsg;
				BYTE bySvrCnt = pMsgRecv->bySvrCnt;
				BYTE* szServerStatusList = pMsgRecv->szServerStatusList;

				MSG_CL_LOGIN_GATE_SELECT_SYN	msg;
				msg.bySelectIndex	= 0;
				Send((BYTE*)&msg,sizeof(msg));
			}
			break;

		case CL_LOGIN_GATENAME_LIST_ACK:
			{
				MSG_CL_LOGIN_GATENAME_LIST_ACK* pMsgRecv = (MSG_CL_LOGIN_GATENAME_LIST_ACK*)pMsg;
				//char* s1 = pMsgRecv->ServerNameList[0].szServerName;
				//BYTE  gidx = pMsgRecv->ServerNameList[0].byGroupIdx;
				//BYTE  num = pMsgRecv->byServerCnt;
				//char* sg1 = pMsgRecv->szGroupNameList[0];
				//BYTE  gnum = pMsgRecv->byGroupCnt;
				int n=0;
			}
			break;

		case CL_LOGIN_GATE_SELECT_ACK:
			{
				MSG_CL_LOGIN_GATE_SELECT_ACK* pMsgRecv = (MSG_CL_LOGIN_GATE_SELECT_ACK*)pMsg;
				int n=0;
			}
			break;
		default:
			{
				assert(0 && "没处理的协议....");
			}
			break;
		}
	}
	//thePacketHandler.ParsePacket_TU( this, (MSG_BASE*)pMsg, wSize );
}

VOID TestUserSession::SendServerInfoSyn()
{
}

#endif
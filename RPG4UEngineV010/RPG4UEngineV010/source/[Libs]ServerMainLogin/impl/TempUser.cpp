/*////////////////////////////////////////////////////////////////////////
文 件 名：TempUser.cpp
创建日期：2007年2月19日
最后更新：2007年2月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "TempUser.h"
#include "UserManager.h"
#include "WaitingUserManager.h"
#include "GateChannel.h"
#include "ChannelManager.h"
#include "Version.h"
#include <Protocol_ClientLogin.h>
#include <PacketStruct_ClientLogin.h>

#define	SERVER_READY_INFO		"Welcome to RPG4U Login Server"

TempUser::TempUser(void)
{
	m_UserType		= TEMP_USER;
}

TempUser::~TempUser(void)
{
}

BOOL TempUser::Init()
{

	_SUPER::Init();
	m_bFistTime = TRUE;
	return TRUE;
}

VOID TempUser::OnAccept(DWORD dwNetworkIndex)
{
	_SUPER::OnAccept(dwNetworkIndex);

	static  DWORD sdwAuthSequence = 0;
	if(sdwAuthSequence == 0)
		sdwAuthSequence = 1;

	SetAuthID(sdwAuthSequence);
	SetAuthSequence(sdwAuthSequence);
	sdwAuthSequence++;

	//这里首先向客户端发送 CL_LOGIN_USER_READY_CMD 协议
	MSG_CL_LOGIN_USER_READY_CMD		msgReady;
	strncpy(msgReady.szServerInfo, SERVER_READY_INFO ,MAX_INFO_LENGTH-1);
	msgReady.dwEncKey		= GetAuthSequence();

	Send((BYTE*)&msgReady,sizeof(msgReady));
}


VOID TempUser::OnRecv( BYTE *pMsg, WORD wSize )
{
	assert( m_bFistTime );
	m_bFistTime = FALSE;

	MSG_CL_LOGIN_VERSION_CHECK_SYN *pRecvMsg = (MSG_CL_LOGIN_VERSION_CHECK_SYN*)pMsg;

	MSG_CL_LOGIN_VERSION_CHECK_ACK msgResult;
	msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_UNKNOWN;


	if(	pRecvMsg->m_byCategory != CL_LOGIN 
		|| pRecvMsg->m_byProtocol != CL_LOGIN_VERSION_CHECK_SYN )
	{
		msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_PROTOCOLINVALID;
		LOGINFO	( "TempUser::OnRecv (%d)(%d)非CL_LOGIN_VERSION_CHECK_SYN协议，不处理...\n"
					, pRecvMsg->m_byCategory
					, pRecvMsg->m_byProtocol );
		goto laFailed;
	}

	if(theUserManager.FindUser(GetAuthID()) )
	{
		msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_AUTH_REPEAT;
		LOGINFO	( "TempUser::OnRecv 该序列号已经处理了(%d)(%d)\n"
					, pRecvMsg->m_byCategory
					, pRecvMsg->m_byProtocol );
		goto laFailed;
	}


	if(	pRecvMsg->dwEncKey != GetAuthSequence())//		网络序列号不匹配
	{
		msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_SEQUENCEINVALID;
		LOGINFO	( "TempUser::OnRecv 验证码不匹配(%d)(%d)\n"
					, pRecvMsg->m_byCategory
					, pRecvMsg->m_byProtocol );
		goto laFailed;
	}
																						//以下情况，都导致验证不通过
	if(	pRecvMsg->byHighVersion		!= VERSION_C2S_HIGH_NO ||		//		高版本号不同
			pRecvMsg->byMiddleVersion	!= VERSION_C2S_MIDDLE_NO ||	//		中版本号不同
			pRecvMsg->byLowVersion		<  VERSION_C2S_LOW_NO)			//		低版本号过低
	
	{
		msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_VERSION_NOTMATCH;
		LOGINFO	( "版本不对(%d)(%d)\n"
					, pRecvMsg->m_byCategory
					, pRecvMsg->m_byProtocol );
		goto laFailed;
	}

	if(	inet_addr(pRecvMsg->szLocalIP) != inet_addr( GetIP()))		//		登录IP与监听IP不同 
	{
		msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_IP_NOTMATCH;
		LOGINFO	( "IP不匹配(%d)(%d)\n"
					, pRecvMsg->m_byCategory
					, pRecvMsg->m_byProtocol );
		goto laFailed;
	}


	//User *pActiveUser = theWaitingUserManager.FindUserWithAuthID( pRecvMsg->dwAuthUserID );
	User *pActiveUser;
	pActiveUser = theUserManager.AllocUser( ACTIVE_USER );
	if( !pActiveUser )
	{
		LOGINFO( "创建用户出错\n" );
		goto laFailed;
	}

	//拷贝序列号
	pActiveUser->SetAuthID(GetAuthID());
	pActiveUser->SetAuthSequence(GetAuthSequence());


	//验证成功
	msgResult.byResult = MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_OK;
	Send((BYTE*)&msgResult,sizeof(msgResult));


	//theWaitingUserManager.RemoveUser( pRecvMsg->dwAuthUserID );

	Redirect( pActiveUser );
	pActiveUser->OnRedirect();


	theUserManager.AddUser(pActiveUser);

	LOGINFO( "[ActiveUser]新用户[%d]连接成功\n", GetAuthSequence() );

	//eZONE_TYPE	eZoneType = pActiveUser->GetStatus();
	//pActiveUser->SetStatus( ZONETYPE_MAX );

	// TempUser去除
	theUserManager.FreeUser( this );
	return;

laFailed:
	Send((BYTE*)&msgResult,sizeof(msgResult));
	Disconnect();
}
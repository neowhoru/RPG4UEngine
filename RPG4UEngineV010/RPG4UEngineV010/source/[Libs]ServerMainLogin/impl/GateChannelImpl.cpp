/*////////////////////////////////////////////////////////////////////////
文 件 名：GateChannelImpl.cpp
创建日期：2007年12月6日
最后更新：2007年12月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "GateChannelImpl.h"
#include "ChannelManager.h"
#include "GateSession.h"
#include "LoginShellInfo.h"
#include <PacketStruct_Gate_Login.h>

GateChannelImpl::GateChannelImpl(void)
{
}


void GateChannelImpl::Init()
{
	m_GateStatusTimer.SetTimer(theLoginShellInfo.GetGateStatusUpdateDelay());
	_SUPER::Init();
}


void GateChannelImpl::Release()
{
	_SUPER::Release();
}

void GateChannelImpl::Update()
{
	if(m_GateStatusTimer.IsExpiredOnce(TRUE))
	{
		MSG_AU_LOGIN_LOGIN2GAME_STATUS_CMD msg;
		msg.dwTimeStamp	= GetTickCount();
		m_pGateSession->Send((BYTE*)&msg, sizeof(msg));
		//m_GateStatusTimer.DisableCheckTime();
	}
}

void GateChannelImpl::DisplayInfo()
{
	DISPMSG	(_T("GateChannelImpl[%d#] => %s User Count:%d/%d\n")
				,GetChannelID()
				,GetChannelName()
				,GetUserCount()
				,GetMaxUserCount());
}


void GateChannelImpl::OnGetGameStatus(DWORD dwTimeStamp, DWORD dwUserCount)
{
	dwTimeStamp;
	SetUserCount(dwUserCount);
	m_GateStatusTimer.Reset();
}


BOOL GateChannelImpl::SendToGateServer(VOID* pMsg, WORD wSize)
{
	__VERIFY_PTR(m_pGateSession, "Invalid m_pGateSession");

	return m_pGateSession->Send((BYTE*)pMsg, wSize);
}


DWORD GateChannelImpl::GetServerStatus()
{

	//	以下为BYTE中，数值意义：
	//	0 - 3   闲状态
	//	4 - 6   中状态
	//	7 - 8   繁忙状态
	//	9		  满状态
	//	其它	  则不处理
	
	if(GetMaxUserCount() == 0)
		return 0;
	DWORD dwRet = (DWORD)((float)GetUserCount()/(float)GetMaxUserCount() *  MSG_CL_LOGIN_GATESTATUS_LIST_ACK::STATUS_VALUE_AMOUNT);

#ifdef _DEBUG
#pragma message(__FILE__  "(92) GateChannelImpl..GetServerStatus DEUBG版本时，底数为1 " )
	if(dwRet == 0)
		dwRet = 1;
#endif

	return dwRet;
}

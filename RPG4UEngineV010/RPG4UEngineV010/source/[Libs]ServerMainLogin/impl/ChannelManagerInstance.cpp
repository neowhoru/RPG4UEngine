/*////////////////////////////////////////////////////////////////////////
文 件 名：ChannelManagerInstance.cpp
创建日期：2007年12月8日
最后更新：2007年12月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "ChannelManagerInstance.h"
#include "GateChannelImpl.h"
#include "LoginShellInfo.h"
#include "User.h"
#include "UserManager.h"
#include <GateChannelFactoryManager.h>



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ChannelManagerInstance, ()  , gamemain::eInstPrioGameFunc);



ChannelManagerInstance::ChannelManagerInstance(void)
{
}

ChannelManagerInstance::~ChannelManagerInstance(void)
{
}

VOID ChannelManagerInstance::Init()
{
	const INT	POOL_SIZE	= 5;
	REG_GATECHANNELFACTORY(GateChannelImpl, GATECHANNEL_DEFAULT, POOL_SIZE);

	////////////////////////////////
	_SUPER::Init();
	m_StatisticsTimer.SetTimer( theLoginShellInfo.GetStatisticsDelay() );
}

VOID ChannelManagerInstance::Release()
{	
	_SUPER::Release();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChannelManagerInstanceUpdateOpr
{
public:
	void operator()(GateChannel *pChannel)
	{
		pChannel->Update();
	}
};

VOID ChannelManagerInstance::Update()
{
	if( m_StatisticsTimer.IsExpired() )
	{
		ChannelManagerInstanceUpdateOpr	opr;
		ForEach(opr);
	}
}





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class SyncInfoToUsersOpr
{
public:
	BOOL operator()(User& user)
	{
		/////////////////////////////////////////
		//再派发状态数据
	   MSG_CL_LOGIN_GATENAME_LIST_SYN sync;
		user.ParsePacket(&sync,sizeof(sync));
		return TRUE;
	}
};

void ChannelManagerInstance::SyncInfoToUsers()
{
	SyncInfoToUsersOpr	opr;
	theUserManager.ForEach(opr);
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChannelManagerInstanceDisplayChannelInfoOpr
{
public:
	void operator()(GateChannel *pChannel)
	{
		pChannel->DisplayInfo();
	}
};

VOID ChannelManagerInstance::DisplayChannelInfo()
{
	ChannelManagerInstanceDisplayChannelInfoOpr	opr;
	ForEach(opr);
}
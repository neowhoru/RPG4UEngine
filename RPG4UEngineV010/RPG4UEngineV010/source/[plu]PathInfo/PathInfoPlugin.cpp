// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "LogSystem.h"
#include "PathInfoParser.h"
#include "ConstArray.h"
#include "SkeletonDefine.h"
#include "ConstV3DWorld.h"
#include "PathInfoDefine.h"

#include "MapPathToken.h"
#include "PlayerPathToken.h"
#include "NormalPathToken.h"


using namespace skeleton;


#define REG_LABEL(Name,Num)\
	thePathInfoParser.Register(eTYPELABEL_##Name	,	Get##Name##Labels()	,		Num)

#define REG_TOKEN(Name,Type)\
	static Name##PathToken s##Name##Token;\
	thePathInfoParser.Register(Type,	&s##Name##Token)


/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_PATHINFO_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{
	REG_LABEL(CharType		, PLAYERTYPE_MAX);
	REG_LABEL(SexType			, SEX_MAX);
	REG_LABEL(VItemType		, VITEMTYPE_MAX);
	REG_LABEL(EquipName		, eMaxPart);
	REG_LABEL(EquipPos		, eMaxPart);
	REG_LABEL(V3DExt			, eV3DEXT_MAX);
	REG_LABEL(V3DFileTitle	, eV3DEXT_MAX);
	REG_LABEL(PathInfo		, ePATHINFO_MAX);
	REG_LABEL(TipType			, eTIP_MAX);


	REG_TOKEN(PlayerHUD,			ePATHINFO_PLAYERHUD);
	REG_TOKEN(CharSkeleton,		ePATHINFO_CHARSKELETON);
	REG_TOKEN(EquipModel,		ePATHINFO_EQUIPMODEL);

	REG_TOKEN(MapFile,			ePATHINFO_MAPFILE);
	REG_TOKEN(MapPath,			ePATHINFO_MAPPATH);
	REG_TOKEN(MapName,			ePATHINFO_MAPNAME);
	REG_TOKEN(LandPath,			ePATHINFO_LANDPATH);
	REG_TOKEN(LandData,			ePATHINFO_LANDDATA);
	REG_TOKEN(MapViewFile,		ePATHINFO_MAPVIEWFILE);
	REG_TOKEN(MapViewPath,		ePATHINFO_MAPVIEWPATH);

	LOGINFO("Plugin PathInfo insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_PATHINFO_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	return gamemain::ePluginOK;
}

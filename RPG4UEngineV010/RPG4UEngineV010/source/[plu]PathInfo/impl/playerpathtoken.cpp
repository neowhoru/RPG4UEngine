/*////////////////////////////////////////////////////////////////////////
文 件 名：MapPathToken.cpp
创建日期：2007年9月10日
最后更新：2007年9月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PathInfoParser.h"
#include "PlayerPathToken.h"
#include "MediaPathManager.h"
#include "PathInfoDefine.h"
#include "EquipInfoParser.h"
#include "SkeletonDefine.h"

using namespace skeleton;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PATHTOKEN_IMPL(PlayerHUD)
{
	INT    nProfession	=	token.m_Params[0].m_Value	;
	INT    nSex				=	token.m_Params[1].m_Value	;

	LPCSTR szPath;
	szPath = PATH_FMT	(GetPathInfoLabel(ePATHINFO_PLAYERHUD)//"%s%s_%s.tga" 
							,_MEDIAPATH(ePATH_NPCUI)
							,GetCharTypeLabel((ePLAYER_TYPE)nProfession)
							,GetSexTypeLabel((eSEX_TYPE)nSex)
							);
	return szPath;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PATHTOKEN_IMPL(CharSkeleton)
{
	INT    nProfession	=	token.m_Params[0].m_Value	;
	INT    nSex				=	token.m_Params[1].m_Value	;

	LPCSTR		szPath;
	LPCSTR		szProfession;

	szProfession	=  GetCharTypeLabel((ePLAYER_TYPE)nProfession);

	//	Equip\职业[sex]\CharSkeleton.mesh
	szPath = PATH_FMT	(GetPathInfoLabel(ePATHINFO_CHARSKELETON)//"%s%s[%d]\\CharSkeleton.mesh"
							, _MEDIAPATH(ePATH_EQUIP)
							, szProfession
							, nSex );
	return szPath;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PATHTOKEN_IMPL(EquipModel)
{
	INT		nProfession		=	token.m_Params[0].m_Value	;
	INT		nSex				=	token.m_Params[1].m_Value	;
	DWORD		nEquipId			=	token.m_Params[2].m_Value	;
	int		nType				=	token.m_Params[3].m_Value	;
	int		nColor			=	token.m_Params[4].m_Value	;


	sEQUIPINFO*	pEquipInfo;
	LPCSTR		szProfession;
	LPCSTR		szEquipType	;
	LPCSTR		szPath;



	szProfession	=  GetCharTypeLabel((ePLAYER_TYPE)nProfession);
	szEquipType		=  GetEquipNameLabel(nType);

	if(nEquipId == 0)
	{
		//Avatar\职业[sex]\类型\ 
		szPath = PATH_FMT	(GetPathInfoLabel(ePATHINFO_EQUIPMODEL_DIR)//"%s%s[%d]\\%s\\  
								, _MEDIAPATH(ePATH_EQUIP)
								, szProfession
								, nSex
								, szEquipType
								);
		return szPath;
	}

	pEquipInfo = theEquipInfoParser.GetEquipInfo(nEquipId);
	__CHECK2_PTR(pEquipInfo, "");

	nProfession -= PLAYERTYPE_BASE;


	if( nColor != -1 )
	{
		__CHECK2_RANGE(nColor-1,pEquipInfo->m_SubNum,"");

		//Avatar\职业[sex]\类型\主名称\主名称[分序号].mesh
		szPath = PATH_FMT	(GetPathInfoLabel(ePATHINFO_EQUIPMODEL)//"%s%s[%d]\\%s\\%s\\%s[%d].mesh"
								, _MEDIAPATH(ePATH_EQUIP)
								, szProfession
								, nSex
								, szEquipType
								, (LPCSTR)pEquipInfo->m_CharTypeTitles[nProfession][nSex]
								, (LPCSTR)pEquipInfo->m_CharTypeTitles[nProfession][nSex]
								, nColor);
	}
	else
	{
		//Avatar\职业[sex]\类型\主名称\主名称.mesh
		szPath = PATH_FMT	(GetPathInfoLabel(ePATHINFO_EQUIPMODEL2)//"%s%s[%d]\\%s\\%s\\%s.mesh"
								, _MEDIAPATH(ePATH_EQUIP)
								, szProfession
								, nSex
								, szEquipType
								, (LPCSTR)pEquipInfo->m_CharTypeTitles[nProfession][nSex]
								, (LPCSTR)pEquipInfo->m_CharTypeTitles[nProfession][nSex]
								);
	}

	return szPath;
}

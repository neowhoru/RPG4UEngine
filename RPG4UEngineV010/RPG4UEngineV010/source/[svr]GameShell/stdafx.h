
#pragma once


#define WIN32_LEAN_AND_MEAN
#define _USING_SIMPLE_CONSOLE
#define __NEW_WORLD_SCRIPT

#define _USE_GAME
#include <ServerIIInclude.h>

#include <ServerBaseGameInc.h>
#include <ServerMainGameInc.h>

#include <Timer.h>

#include "ServerBaseDefine.h"
#include "ConstDefine.h"
#include <ResultCode.h>

#include "WorldConst.h"

#include <AssertUtil.h>

#include <Protocol_ClientGameS.h>
#include <PacketStruct_ClientGameS.h>

#include <DataTypeDefine.h>
#include "StructBase.h"
#include <ISessionHandle.h>
#include "TileWorldInc.h"

#include "PlayerManager.h"
#include "Field.h"
//#include "State.h"
#include "GameLog.h"
#include "MiscUtil.h"
#include "ServerGroupLog.h"





//#pragma warning(pop)

// GameShell.cpp : 콘솔 응용 프로그램에 대한 진입점을 정의합니다.
//

#include "stdafx.h"
#include "GameServerStartUp.h"
#include "GameServerRunning.h"
#include "ServerInfoParser.h"

#pragma message(__FILE__ "(11) _SERVER_MAIN = " _PROJNAME " [GameServerStartUp,ServerRunning]")
_SERVER_MAIN	(_PROJNAME
					,GameServerStartUp
					,GameServerRunning
					,ServerInfoParser
					,DEFAULT_SETTING);




#ifndef __BATTLE_SERVER_H__
#define __BATTLE_SERVER_H__


#pragma once

#include "GameShell.h"

class FieldSession;
class MasterSession;

class BattleServer : public GameShell
{
public:
	BattleServer();
	virtual ~BattleServer();

public:
	eSERVER_TYPE		GetServerType(){return BATTLE_SHELL;};

public:
	virtual BYTE		GetWarServerType();
	virtual BOOL		_PrevInit		();

protected:



};


#endif // __BATTLE_SERVER_H__
#ifndef __FILED_SERVER_H__
#define __FILED_SERVER_H__

#pragma once

#include "GameShell.h"

class Village;
class BattleSession;

class FieldServer : public GameShell
{
public:
	FieldServer(void);
	virtual ~FieldServer(void);

public:
	eSERVER_TYPE	GetServerType(){return FIELD_SHELL;}

protected:
	virtual BOOL		_PrevInit		();



};

#endif // __FILED_SERVER_H__
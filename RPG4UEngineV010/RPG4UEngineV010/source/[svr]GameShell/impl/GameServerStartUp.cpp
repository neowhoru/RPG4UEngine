/*////////////////////////////////////////////////////////////////////////
文 件 名：GameServerStartUp.cpp
创建日期：2007年9月21日
最后更新：2007年9月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameServerStartUp.h"
#include "ServerInfoParser.h"
#include "ServerShell.h"
#include "GameShell.h"

#include "ServerSessionFactoryManager.h"
//#include "AccountDBProxySession.h"
//#include "AuthAgentServerSession.h"
#include "GateSession.h"
#include "BattleSession.h"
#include "FieldSession.h"
#include "DBProxySession.h"
#include "GuildSession.h"
#include "MasterSession.h"
//#include "WorldServerSession.h"
#include "TempServerSession.h"

//#include "ServerCommands.h"
#include "GameCommandManager.h"

#include "MediaPathManager.h"
#include <ItemInfoParser.h>
#include <NPCInfoParser.h>
#include <AIInfoParser.h>
#include <MapInfoParser.h>
#include <ShopInfoParser.h>
#include "PacketStruct_Base.h"
#include <Protocol.h>
#include "Player.h"
#include "ServerSession.h"
#include "MasterSession.h"
#include "DBProxySession.h"
#include "ObjectSystem.h"
#include "PlayerManager.h"
#include "MapSystem.h"
#include "ServerUtil.h"
#include "DropManager.h"
#include "ObjectGroupManager.h"
#include "SkillInfoParser.h"
#include "TriggerParser.h"
#include "RegenParser.h"
#include "SkillSystem.h"
#include "MonsterGroupParser.h"
#include "PartyManager.h"
#include "AbilityManager.h"
#include "StateInfoParser.h"
#include <ItemCompositeParser.h>
#include "ItemMakeParser.h"
#include "ItemAttrInfoParser.h"
#include "TradeManager.h"
#include "SummonManager.h"
//#include <SimpleModulus.h>
#include "ratiomanager.h"
#include <MissionRewardParser.h>
#include "FormulaRatioParser.h"
#include "SummonManager.h"
#include "ZoneManager.h"
#include <GMInfoParser.h>
//#include <ServerGlobal.h>
#include <Version.h>
#include <BuildVersion.h>
#include "StyleQuickRegInfoParser.h"
#include "AIParamParser.h"
#include "ServerOptionParser.h"
#include <LimitedMapPortParser.h>
#include <EnchantInfoParser.h>
#include "GuildManager.h"
#include <RankOptionParser.h>
#include <SocketOptionParser.h>
#include "StatusSystem.h"
#include "CoolTimerFactory.h"
#include "TileWorld.h"
#include "RateInfoParser.h"
#include "QuestItemDropInfoParser.h"
#include "AttributeAffectorManager.h"
#include "AttributeManager.h"
#include "NumericValueParser.h"
#include "SlotSystem.h"
#include "PathInfoParser.h"
#include "SkillDependenceParser.h"
#include "SkillDefaultParser.h"
#include "SeriesInfoParser.h"
#include "ScriptResourceList.h"
#include "QuestManager.h"
#include "ScriptManager.h"
#include "PacketCryptManager.h"
#include "AIStateSystem.h"
#include "AIState.h"
#include "EffectFactory.h"
#include "TargetFinderManager.h"
#include "DataPumpSystem.h"
#include "MoneyDropParser.h"
#include "ItemDropParser.h"
#include "MonsterItemDrop.h"
#include "GroupItemDrop.h"
#include "DropItemGradeParser.h"


#ifdef _DEBUG
#define GAMEOPTION_PATH		_T("setting\\GameOption.info")
#elif _PRERELEASE
#define GAMEOPTION_PATH		_T("Setting\\GameOptionD.info")	
#else
#define GAMEOPTION_PATH		_T("setting\\GameOptionD.info")
#endif


#ifdef _DEBUG
#else
#endif

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameServerStartUp::GameServerStartUp()
{
#ifdef USE_OLD
	m_hDllNumerical					= NULL;
#endif
}
GameServerStartUp::~GameServerStartUp()
{
}


BOOL GameServerStartUp::ReloadData()	
{
	__BOOL_SUPER(	ReloadData()		);	

	//theMapInfoParser.Reload();
	//theTriggerParser.Reload();


	////////////////////////////////////////////////
	theRateInfoParser.Reload();
	//theScriptResourceList.Reload();
	theQuestItemDropInfoParser.Reload();

	theItemInfoParser.Reload("datainfo\\ItemInfo(Weapon).info");
	theItemInfoParser.Reload("datainfo\\ItemInfo(Armor).info");
	theItemInfoParser.Reload("datainfo\\ItemInfo(Waste).info");
	theItemInfoParser.Reload("datainfo\\ItemInfo(Supply).info");

	theSeriesInfoParser.Reload();
	theNPCInfoParser.Reload();
//	CharInfoParser::Instance()->Reload();
	theShopInfoParser.Reload();
	theSkillInfoParser.Reload();
	theRegenParser.Reload();
	theMonsterGroupParser.Reload();
	theStateInfoParser.Reload();
	//ItemOptionParser::Instance()->Reload();
	theItemCompositeParser.Reload();
	theItemMakeParser.Reload();
	theItemAttrInfoParser.Reload();
	//RareItemDropParser::Instance()->Reload();
	theItemDropParser.Reload();
	theMoneyDropParser.Reload();
	theRareItemDropParser.Reload();
	theMonsterItemDrop.Reload();
	theGroupItemDrop.Reload();
	theDropItemGradeParser.Reload();

	theMissionRewardParser.Reload();
	theFormulaRatioParser.Reload();
	theStyleQuickRegInfoParser.Reload();
	theAIParamParser.Reload();
	theServerOptionParser.Reload();
	theEnchantInfoParser.Reload();
//	theGMInfoParser.Reload(); =>

	theRatioManager.Release();
	theRatioManager.Init();

	theDropManager.Reload();

	return TRUE;
} 

BOOL GameServerStartUp::LoadGameSetting()	
{
	__BOOL_SUPER(	LoadGameSetting()		);	


	return TRUE;
} 

BOOL GameServerStartUp::LoadGameInfo() 		
{
	////////////////////////////////////////////
	//日志输出
	//if( !theAuthAgentServerInfoParser.LoadFrom( SETTING_FILE ) )
	//{
	//	LOGINFO( "LoginShell.ini parsing failed.\n" );
	//	return FALSE;
	//}

	__BOOL_SUPER(	LoadGameInfo() 		);	


	/////////////////////////////////////////////////////////
	theServerOptionParser.Load	( GAMEOPTION_PATH
										, (eSERVER_OPTION_TYPE)theGameShell.GetWarServerType() );


	////////////////////////////////////////////////
	//密钥初始化...........
	__INITINST2(thePacketCryptManager,"data\\Enc1.dat" , "data\\Dec2.dat");


	return TRUE;
} 

BOOL GameServerStartUp::LoadGameRes() 	
{
	__BOOL_SUPER(	LoadGameRes() 		);

	//////////////////////////////////////
	sSERVER_OPTION& opt = theServerOptionParser.GetServerOption();


	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	StringHandle	sPath,sPath2,sPath3,sPath4,sPath5,sPath6;


	//////////////////////////////////////////////////////////
	__INITINST(theScriptResourceList, MAX_SCRIPT_INFO);
	__LOADINFO(theScriptResourceList, "ResInfo(Script).info");

	//////////////////////////////////////////////////////////
	sPath		= _PATH_DATAINFO("Script\\LevelUp.txt");
	sPath2	= _PATH_DATAINFO("Script\\UseItem.txt");
	sPath3	= _PATH_DATAINFO("Script\\Time.txt");
	sPath4	= _PATH_DATAINFO("Script\\ScriptEvent.txt");
	sPath5	= _PATH_DATAINFO("Script\\KeywordDefine.txt");

	//Script配置
	__INITINST(theScriptManager, );
	__INIT(theScriptManager.InitScripts	(sPath 	
													,sPath2
													,sPath3
													,sPath4
													,sPath5) );

	//////////////////////////////////////////////////////////
	__INITINST2	(theQuestManager,MAX_QUEST_POOLSIZE,MAX_QUEST_SIZE);
	__LOADINFO	(theQuestManager, "ResInfo(Quest).info");


	//////////////////////////////////////////////////////////
	__INITINST	(theRateInfoParser, opt.m_wRateInfo );
	__LOADBASE	(theRateInfoParser, "RateInfo.txt" );

	//////////////////////////////////////////////////////////
	__INITINST	(theQuestItemDropInfoParser, opt.m_wQuestItemDropInfo );
	__LOADSVR	(theQuestItemDropInfoParser, "QuestItemDropInfo.info"  );


	//////////////////////////////////////////////////////////
	sPath		= _PATH_DATAINFO("BaseInfo\\SeriesRelationInfo.info");
	sPath2	= _PATH_DATAINFO("BaseInfo\\SeriesAffectInfo.info");

	__INITINST(theSeriesInfoParser,  );
	__LOAD2	 (theSeriesInfoParser, sPath ,sPath2);

	//////////////////////////////////////////////////////////
	__INITINST(theItemInfoParser, opt.m_wItemInfo );

	//__LOADFUNC(theItemInfoParser.LoadType,			"BaseInfo\\ItemType.txt" );
	//__LOADFUNC(theItemInfoParser.LoadVItemInfo,	"VItemInfo.info" );


	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Weapon).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Armor).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Waste).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Supply).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo.info")	);


	//////////////////////////////////////////////////////////
	__LOAD2( theGMInfoParser, "data\\GMInfo.info", "data\\SuperUserList.info"  );


	__INITINST(theAIInfoParser, 100);
	__LOADINFO(theAIInfoParser,	"TipInfo(AI).info"		);

	//////////////////////////////////////////////////////////
	__INITINST3	(theNPCInfoParser
					,opt.m_wNPCInfo
					,opt.m_wExtraNPCInfo
					,opt.m_wQuestInfo );

	__LOADINFO(theNPCInfoParser,						"NPCInfo.info"		);
	__LOADFUNC(theNPCInfoParser.LoadVNpc,			"VNPCInfo.info"		);
	__LOADFUNC(theNPCInfoParser.LoadExtra,			"NpcFuncInfo.info"	);
	__LOADFUNC(theNPCInfoParser.LoadQuestInfo,	"NpcQuestInfo.info");


	//////////////////////////////////////////////////////////
	__INITINST(theMapInfoParser,  opt.m_wMapInfo );
	__LOAD3	( theMapInfoParser	
				,_MEDIAPATH(ePATH_DATAINFO)
				, "MapInfo(World).info"
				, "MapInfo(Field).info"  );

	__INITINST(theShopInfoParser, opt.m_wShopInfo );
	__LOADINFO(theShopInfoParser,	"ShopInfo.info" );


	//////////////////////////////////////
	sPath		= _PATH_DATAINFO("SkillInfo.info");
	sPath2	= _PATH_DATAINFO("StyleInfo.info");
	__INITINST	(theSkillInfoParser,  opt.m_wSkillInfo );
	__LOAD3		(theSkillInfoParser, sPath	,sPath2 ,NULL );

	__INITINST	(theSkillDependenceParser,  );
	__LOADBASE	(theSkillDependenceParser, "SkillDependence.info" );

	__INITINST	(theSkillDefaultParser,  );
	__LOADBASE	(theSkillDefaultParser, "SkillDefault.info" );


	//////////////////////////////////////
	__INITINST(theRegenParser,opt.m_wRegenInfo );
	__LOADSVR( theRegenParser,"RegenMonster.info"  );

	__INITINST(theMonsterGroupParser, opt.m_wGroupInfo );
	__LOADSVR( theMonsterGroupParser, "RegenGroup.info"  );


	//////////////////////////////////////
	__INITINST(theStateInfoParser,  opt.m_wStateInfo  );
	__LOADINFO(theStateInfoParser,	"StateInfo.info");
	//__LOADFUNC(theStateInfoParser.LoadVState,	"VStateInfo.info");

	//////////////////////////////////////
	sPath		= _PATH_DATAINFO("BaseInfo\\ItemCompositeInfo.info");
	sPath2	= _PATH_DATAINFO("BaseInfo\\ItemCompositeResultInfo.info");
	__INITINST2	(theItemCompositeParser
					, opt.m_wItemCompInfo
					, opt.m_wItemCompResultInfo );
	__LOAD2(theItemCompositeParser, sPath	,sPath2  );


	//////////////////////////////////////


	__INITINST(theItemMakeParser	,opt.m_wItemMakeInfo );
	__LOADBASE(theItemMakeParser	,"ItemMakeInfo.info" );

	//////////////////////////////////////////////
	__INITINST3	(theItemAttrInfoParser
					,opt.m_wItemRandAttrInfo
					,opt.m_wItemDoubleAttrInfo
					,opt.m_wItemRandAttrTable );

	sPath		= _PATH_DATAINFO("BaseInfo\\ItemRandAttrInfo.info" );
	sPath2	= _PATH_DATAINFO("BaseInfo\\ItemDoubleAttrInfo.info");
	sPath3	= _PATH_DATAINFO("BaseInfo\\ItemRandAttrTable.info" );
	__LOAD3		(theItemAttrInfoParser,sPath,sPath2 ,sPath3);

	__LOADFUNC	(theItemAttrInfoParser.LoadOptions
					,"BaseInfo\\ItemOptionInfo.info"); 



	//////////////////////////////////////
	__INIT		(theRankOptionParser.Init(100));
	__LOADBASE	(theRankOptionParser,"RankOptionInfo.info");

	__INIT		(theSocketOptionParser.Init());
	__LOADBASE	(theSocketOptionParser, "SocketOptionInfo.info");


	__INITINST	(theMissionRewardParser, opt.m_wMissionRewardInfo);
	__LOADSVR	(theMissionRewardParser, "MissionRewardInfo.txt"  );

	theFormulaRatioParser.Init(opt.m_wFormulaRatioInfo);
	__INIT( theFormulaRatioParser.Load( "data\\FormulaRatio.info" ) );


	/////////////////////////////////////
	__INITINST(theStyleQuickRegInfoParser, opt.m_wStyleQuickInfo);
	__LOADBASE( theStyleQuickRegInfoParser, "StyleQuickRegistInfo.txt"  );


	__LOADSVR( theAIParamParser, "AIParameters.info"  );


	__LOADSVR( theLimitedMapPortParser,  "LimitedMapPort.txt"  );


	/////////////////////////////////////
	__INITINST(theEnchantInfoParser, 100);
	__LOADBASE(theEnchantInfoParser,"EnchantInfo.txt" );


	__INITINST	(theItemDropParser, 100	);
	__LOADSVR(theItemDropParser, "ItemDropInfo.info"	);

	__INITINST	(theMoneyDropParser, 20	);
	__LOADSVR(theMoneyDropParser, "MoneyDropInfo.info"	);

	__INITINST	(theRareItemDropParser, 50	);
	__LOADSVR(theRareItemDropParser, "RareItemDropInfo.info"	);

	__INITINST	(theMonsterItemDrop,	);
	__LOADSVR(theMonsterItemDrop, "MonsterItemDropInfo.info"	);

	__INITINST	(theGroupItemDrop,	);
	__LOADSVR(theGroupItemDrop, "GroupItemDropInfo.info"	);

	__INITINST	(theDropItemGradeParser,	);
	__LOADSVR(theDropItemGradeParser, "ItemDropPerGrade.info"	);


	__INITINST	(theDropManager,	);


	//LOG_OK;






	return TRUE;
} 

BOOL GameServerStartUp::InitGameEngine()
{
	DWORD* arSizes = theServerInfoParser.GetSessionPoolSizes(theServerShell.GetServerType());
	__VERIFY_PTR(arSizes,_T("Invalid PoolSize"));

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//REG_SERVERSESSIONFACTORY2(	AccountDBProxySession 		,USER_DBPROXY	);
	//REG_SERVERSESSIONFACTORY2(	AuthAgentServerSession 		,LOGIN_SHELL);
	REG_SERVERSESSIONFACTORY2	(	GateSession 			,GATE_SHELL  	);
	REG_SERVERSESSIONFACTORY2	(	BattleSession 			,BATTLE_SHELL 	);
	REG_SERVERSESSIONFACTORY2	(	FieldSession 			,FIELD_SHELL 		);
	REG_SERVERSESSIONFACTORY2	(	DBProxySession 		,PLAYER_DBPROXY  	);
	REG_SERVERSESSIONFACTORY2	(	GuildSession 			,GUILD_SHELL 		);
	REG_SERVERSESSIONFACTORY2	(	MasterSession 			,MASTER_SHELL  	);
	//REG_SERVERSESSIONFACTORY2(	WorldServerSession 			,CHAT_SHELL 		);
	REG_SERVERSESSIONFACTORY2	(	TempServerSession 			,TEMP_SHELL  		);



	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	//sIOHANDLER_INFO *pDesc;
	//pDesc = theServerInfoParser.GetClientIoDesc();

	//REG_USERFACTORY(ActiveUser,	ACTIVE_USER , pDesc->dwMaxAcceptSession);
	//REG_USERFACTORY(TempUser,		TEMP_USER	, pDesc->dwMaxAcceptSession);


	//theUserFactoryManager.InitAllFactories();


	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@


	////////////////////////////////////
	__BOOL_SUPER(	InitGameEngine()	);



	/////////////////////////////////////////////////


	return TRUE;
} 

BOOL GameServerStartUp::InitGameFunc()		
{
	__BOOL_SUPER(	InitGameFunc()			);

	//////////////////////////////////////
	sSERVER_OPTION& opt = theServerOptionParser.GetServerOption();


	///////////////////////////////////////////////
	__INIT(theTileWorld.Init());
	__INIT(theSlotSystem.Init());

	///////////////////////////////////////////////
	////////////////////////////////////////////////////////////////
	sSERVER_INFOC * pServerEnv = theServerInfoParser.GetServerInfo();

	// Object Pool
	theCoolTimerFactory.Init( opt.m_wCoolTimerCharPool*300 );

	/// Object Pool
	DISPMSG(  "Creating object factories...\n" );
	theDataPumpSystem.Init(opt.m_wPlayerPool); // 须在theObjectSystem之前

	theObjectSystem.Init(opt.m_wPlayerPool
                        ,opt.m_wNPCPool
								,opt.m_wItemPool
								,opt.m_wMapObjectPool
								,opt.m_wMapNPCPool);

	theAIStateSystem.Init(opt.m_wNPCPool * AISTATE_MAX);
	theEffectFactory.Init(opt.m_wEffectPool);
	theTargetFinderManager.Init(opt.m_wTargetFinderPool);

	theSkillSystem.Init( opt.m_wPlayerSkillPool, opt.m_wNPCSkillPool );
	theAbilityManager.Init( opt.m_wAbilityPool );
	thePartyManager.Init( opt.m_wPartyPool );
	theSummonManager.Init( opt.m_wSummonPool );
	theStatusSystem.Init( opt.m_wStatusPool );
	//LOG_OK;

	////////////////////////////////////////////////
	DISPMSG(  "Initializing map allocator...\n" );
	theMapSystem.Init	( opt.m_wMapPool
								, opt.m_wSectorPool
								, VILLAGE_SECTOR_SIZE
								, ROOM_SECTOR_SIZE );
	//LOG_OK;


	////////////////////////////////////////////////
	DISPMSG(  "Initializing zone manager & drop formular...\n" );
	//
	theGuildManager.Init	(opt.m_wGuildPool		
								,opt.m_wGuildMemberPool);
	theZoneManager	.Init	(opt.m_wRoomPool	
								,opt.m_wPVPRoomPool
								,opt.m_wLobbyPool	
								,opt.m_wVillagePool);

	theTradeManager.Init( opt.m_wTradePool );
	theRatioManager.Init();

	theAttributeManager.Init();
	theAttributeAffectorManager.Init();

	theNumericValueParser.Init();
	theNumericValueParser.LoadNormal	( _PATH_DATAINFO("BaseInfo\\NumericValueInfo.txt") );
	theNumericValueParser.LoadItem	( _PATH_DATAINFO("BaseInfo\\NumericValueInfo(Item).txt") );


	return TRUE;
} 


void GameServerStartUp::ReleaseGameFunc()
{

	////////////////////////////////////////////////
	theZoneManager.Release();
	theGuildManager.Release();

	//资源信息



	////////////////////////////////////////////////
	theMapSystem.Release();
	theTradeManager.Release();
	theRatioManager.Release();
	theAttributeAffectorManager.Release();
	theAttributeManager.Release();
	theNumericValueParser.Release();



	////////////////////////////////////////////////
	//资源信息
	theServerOptionParser.Release();	

	theItemInfoParser.Release();	
	theSeriesInfoParser.Release();	
	theGMInfoParser.Release();
	theNPCInfoParser.Release();	
	theAIInfoParser.Release();	
	theMapInfoParser.Release();	
	theShopInfoParser.Release();	
	theSkillDependenceParser.Release();	
	theSkillDefaultParser.Release();	
	theSkillInfoParser.Release();	
	theRegenParser.Release();	
	theMonsterGroupParser.Release();	
	theStateInfoParser.Release();		
	theItemCompositeParser.Release();
	theItemAttrInfoParser.Release();		
	theItemMakeParser.Release();			

	theRankOptionParser.Release();
	theSocketOptionParser.Release();		

	theMissionRewardParser.Release();	
	theFormulaRatioParser.Release();	
	theStyleQuickRegInfoParser.Release();	
	theAIParamParser.Release();		

	theLimitedMapPortParser.Release();
	theEnchantInfoParser.Release();
	theDropManager.Release();
	theItemDropParser.Release();
	theMoneyDropParser.Release();
	theRareItemDropParser.Release();
	theMonsterItemDrop.Release();
	theGroupItemDrop.Release();
	theDropItemGradeParser.Release();

	theRateInfoParser.Release();	
	theQuestManager		.Release();
	theScriptManager		.Release();
	theScriptResourceList.Release();

	theQuestItemDropInfoParser.Release();	//	theQuestItemDropInfoParser.DestroyInstance();

	theAIStateSystem.Release();
	theEffectFactory.Release();
	theTargetFinderManager.Release();
	theObjectSystem.Release();
	theDataPumpSystem.Release();// // 须在theObjectSystem之后
	theSkillSystem.Release();	
	thePartyManager.Release();
	theAbilityManager.Release();
	theSummonManager.Release();
	theStatusSystem.Release();
	theCoolTimerFactory.Release();		

	///////////////////////////////////////////////////
	__END(theSlotSystem.Release());
	__END(theTileWorld.Release());


	_SUPER::ReleaseGameFunc();
}

void GameServerStartUp::ReleaseGameEngine()
{
	//__END(theUserFactoryManager.ReleaseAllFactories());
	//__END(theZoneFactoryManager.ReleaseAllFactories());

	_SUPER::ReleaseGameEngine();
}

void GameServerStartUp::DestroyGameRes()
{
	//__END( theViewPortFactory.Release() );

	//__END( theMapInfoParser.Release() );
	//theLimitedMapPortParser.Release();
	//theRequireFieldLevelInfoParser.Release();

	_SUPER::DestroyGameRes();
}

void GameServerStartUp::DestroyGameInfo()
{

	thePacketCryptManager.Release();


#ifdef USE_OLD
	if( m_hDllNumerical )
	{
		FreeLibrary( m_hDllNumerical );
	}
#endif

	_SUPER::DestroyGameInfo();
}

void GameServerStartUp::DestroyGameSetting()
{
	_SUPER::DestroyGameSetting();
}


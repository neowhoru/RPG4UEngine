/*////////////////////////////////////////////////////////////////////////
文 件 名：InventoryItemTransaction.cpp
创建日期：2006年11月25日
最后更新：2006年11月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "InventoryItemTransaction.h"
#include "MouseHandler.h"
#include "ItemManager.h"
#include "ConstSlot.h"
#include "HeroActionInput.h"
#include "ItemSlot.h"
#include "BaseContainer.h"
#include "ItemCompoundDialog.h"
#include "HeroTipLayer.h"
#include "DummyItemSlot.h"
#include "SlotHandle.h"
#include "ConstTextRes.h"
#include "TradeDialog.h"
#include "VendorDialog.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
InventoryDefaultTransaction::InventoryDefaultTransaction()
{
	m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo		= INVALID_SLOTINDEX;
}

BOOL InventoryDefaultTransaction::DoTransaction(SLOTPOS /*posFrom*/, SLOTPOS /*posTo*/)
{
	theMouseHandler.CancelHandlingItem();
	return TRUE;
}

BOOL InventoryDefaultTransaction::DoDropTransaction(SLOTPOS posFrom)
{
	//uiVendorSell* vendorSell =
	//    static_cast<uiVendorSell *>(g_InterfaceManager.GetUserInterface(UI_VENDORSELL));

	//if (vendorSell)
	//{
	//    if (vendorSell->IsVendedItem(/*theMouseHandler.*/GetMouseItem()->m_fromPos))
	//    {
	//        TCHAR szMessage[MAX_UITEXT_LENGTH];
	//        _stprintf (szMessage, "泅犁 惑痢俊 蔼阑 瘤沥茄 惑怕涝聪促. 滚府矫摆嚼聪鳖?");
	//        OUTPUTCHAT(OUTPUTMSG_CHAT, szMessage, CHAT_TYPE_SYSTEM );
	//        return TRUE;
	//    }
	//}
	//theMouseHandler.CancelHandlingItem();
	theItemManagerNet.SendDropItemMsg	(m_SlotIdxFrom,posFrom);
	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
	->Self
/*////////////////////////////////////////////////////////////////////////
InventoryItemTransaction::InventoryItemTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo		= SI_INVENTORY;
}

BOOL InventoryItemTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	if(posFrom == posTo)
	{
		theMouseHandler.CancelHandlingItem();
		return TRUE;
	}
	BaseContainer * pFromContainer = 	theItemManager.GetContainer(m_SlotIdxFrom); 
	BaseContainer * pToContainer	=	theItemManager.GetContainer(m_SlotIdxTo); 
	BaseSlot & rFromSlot	= pFromContainer->GetSlot(posFrom);
	BaseSlot & rToSlot		= pToContainer->GetSlot(posTo);

	/// 同类物品组合处理
	if (	rFromSlot.GetCode() 
		&& rToSlot.GetCode() 
		&& (rFromSlot.GetCode() == rToSlot.GetCode()) )
	{
		ItemSlot & rFromItemSlot = (ItemSlot & )rFromSlot;
		ItemSlot & rToItemSlot   = (ItemSlot & )rToSlot;

		if ( rFromItemSlot.GetMaxNum() != rToItemSlot.GetNum() )
		{
			theItemManagerNet.SendItemCombineMsg	(m_SlotIdxFrom
                                             ,m_SlotIdxTo
															,posFrom
															,posTo);
		}
		else
		{
			//theMouseHandler.CancelHandlingItem();
			theItemManagerNet.SendItemMoveMsg	(m_SlotIdxFrom
														,m_SlotIdxTo
														,posFrom
														,posTo);
		}
	}

	/// 物品移动
	else
	{
		theItemManagerNet.SendItemMoveMsg	(m_SlotIdxFrom
                                       ,m_SlotIdxTo
													,posFrom
													,posTo);
	}
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/// 背包 => 向NPC卖物品处理
/*////////////////////////////////////////////////////////////////////////
InventoryNpcStoreTransaction::InventoryNpcStoreTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo		= SI_NPCSHOP;
}

BOOL InventoryNpcStoreTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS /*posTo*/)
{
	theItemManagerNet.SendSellMsg(m_SlotIdxFrom, posFrom);
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
//  => 快速栏
/*////////////////////////////////////////////////////////////////////////
InventoryQuickTransaction::InventoryQuickTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo		= SI_QUICK;
}

BOOL InventoryQuickTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	BOOL bQuick;
	bQuick = theItemManager.CanLinkQuick	(SI_QUICK
                                          ,posTo
														,m_pSlotFrom);
	if (bQuick)
	{
		theItemManagerNet.SendQuickItemLinkMsg(posFrom ,posTo);
	}
	else
	{
		theMouseHandler.CancelHandlingItem();
		return FALSE;
	}
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/// 背包 => Trade
/*////////////////////////////////////////////////////////////////////////
InventoryTradeTransaction::InventoryTradeTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo		= SI_TRADE;
}

BOOL InventoryTradeTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	if(m_SlotIdxFrom != SI_INVENTORY)
	{
		OUTPUTTIP(TEXTRES_TRADE_ITEM_FROM_INVENTORY);
		return FALSE;
	}

	theTradeDialog.PutItem(posTo,posFrom);

	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
/// 背包 => VendorSell
/*////////////////////////////////////////////////////////////////////////
InventoryVendorSellTransaction::InventoryVendorSellTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo		= SI_VENDOR_SELL;
}

BOOL InventoryVendorSellTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	theVendorDialog.VendingInsert(posTo, posFrom);

return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
/// 背包 => Equipment
/*////////////////////////////////////////////////////////////////////////
InventoryEquipmentTransaction::InventoryEquipmentTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo			= SI_EQUIPMENT;
}

BOOL InventoryEquipmentTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	BOOL bEquipRet = theItemManager.CanEquip	(m_SlotIdxFrom
                                             ,posFrom
															,posTo);

	if ( TRUE == bEquipRet )
	{
		theItemManagerNet.SendItemMoveMsg	(m_SlotIdxFrom
                                       ,m_SlotIdxTo
													,posFrom
													,posTo);
	}
	else
	{
		theHeroActionInput.PlayLimitUseSound();
		theMouseHandler.CancelHandlingItem();
		return FALSE;
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/// 背包 => WareHouse
/*////////////////////////////////////////////////////////////////////////
InventoryWareHouseTransaction::InventoryWareHouseTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo			= SI_WAREHOUSE;
}

BOOL InventoryWareHouseTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	return _SUPER::DoTransaction( posFrom,  posTo);
}



/*////////////////////////////////////////////////////////////////////////
/// 背包 => EchantTarget
/*////////////////////////////////////////////////////////////////////////
InventoryEnchantTargetTransaction::InventoryEnchantTargetTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo			= SI_ENCHANT_TARGET;
}

BOOL InventoryEnchantTargetTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	BaseContainer * pFromContainer = 	theItemManager.GetContainer(m_SlotIdxFrom); 
	ItemSlot &	rFromSlot	= (ItemSlot&)pFromContainer->GetSlot(posFrom);

	if(!rFromSlot.WithDura())
	{
		OUTPUTTOP(TEXTRES_NOT_WEAPON_OR_ARMOR);
		theMouseHandler.CancelHandlingItem();
		return TRUE;
	}

	theItemCompoundDialog.SetTargetInfo(m_SlotIdxFrom, posFrom);

	return _DoDummyTransaction( posFrom,  posTo);
}

BOOL InventoryEnchantTargetTransaction::_DoDummyTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	BaseContainer * pFromContainer = 	theItemManager.GetContainer(m_SlotIdxFrom); 
	BaseContainer * pToContainer	=	theItemManager.GetContainer(m_SlotIdxTo); 

	ItemSlot &	rFromSlot	= (ItemSlot&)pFromContainer->GetSlot(posFrom);
	//BaseSlot &			rToSlot		= pToContainer->GetSlot(posTo);

	BOOL				bRollback(TRUE);

	//DummyItemSlot	dummyItem;
	SlotHandle			dummyHandle(SLOTT_ITEMDUMMY);

	if((BOOL)dummyHandle && !pToContainer->IsEmpty(posTo))
	{
		DummyItemSlot&	dummyItem = (DummyItemSlot&)*dummyHandle;
		bRollback	= FALSE;
		pToContainer->DeleteSlot(posTo,dummyHandle);
		//theMouseHandler.CancelHandlingItem();
		theMouseHandler.BeginHandlingItem	(dummyItem.GetFromContainerIdx()
												,dummyItem.GetFromPosition()
												,dummyItem.GetOrgSlot()->GetSlotType()
												,dummyItem.GetOrgSlot()->GetCode()
												,TRUE);
	}

	pToContainer->InsertSlot(posTo,rFromSlot);

	//if(pToContainer->IsEmpty(posTo))
	//	pToContainer->InsertSlot(posTo,rFromSlot);
	//else
	//	pToContainer->UpdateSlot(posTo,rFromSlot);

	pFromContainer->SetSlotState(posFrom, SLOT_UISTATE_DISABLE);

	if(bRollback)
		theMouseHandler.CancelHandlingItem();

	return TRUE;
	//return _SUPER::DoTransaction( posFrom,  posTo);
}


/*////////////////////////////////////////////////////////////////////////
/// 背包 => EchantSocket
/*////////////////////////////////////////////////////////////////////////
InventoryEnchantSocketTransaction::InventoryEnchantSocketTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo			= SI_ENCHANT_SOCKET;
}

BOOL InventoryEnchantSocketTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	BaseContainer * pFromContainer = 	theItemManager.GetContainer(m_SlotIdxFrom); 
	ItemSlot &	rFromSlot	= (ItemSlot&)pFromContainer->GetSlot(posFrom);

	if(rFromSlot.GetItemInfo()->m_wType != ITEMTYPE_SOCKET)
	{
		OUTPUTTOP(TEXTRES_ITEM_NOT_SOCKET_ITEM);
		//theMouseHandler.CancelHandlingItem();
		return TRUE;
	}

	if(rFromSlot.GetDamageLV())
	{
		OUTPUTTOP(TEXTRES_ITEM_NOT_ACCEPT_DAMAGED);
		//theMouseHandler.CancelHandlingItem();
		return TRUE;
	}


	return _DoDummyTransaction( posFrom,  posTo);
}


/*////////////////////////////////////////////////////////////////////////
/// 背包 => SI_ENCHANT 原料容器
/*////////////////////////////////////////////////////////////////////////
InventoryEnchantMaterialTransaction::InventoryEnchantMaterialTransaction()
{
	//m_SlotIdxFrom	= SI_INVENTORY;
	m_SlotIdxTo			= SI_ENCHANT;
}

BOOL InventoryEnchantMaterialTransaction::DoTransaction(SLOTPOS posFrom, SLOTPOS posTo)
{
	BaseContainer * pFromContainer = 	theItemManager.GetContainer(m_SlotIdxFrom); 
	//BaseContainer * pToContainer	= 	theItemManager.GetContainer(m_SlotIdxTo); 
	ItemSlot &	rFromSlot	= (ItemSlot&)pFromContainer->GetSlot(posFrom);

	sITEMINFO_BASE*	pItemInfo;

	pItemInfo	= rFromSlot.GetItemInfo();


	/// 非原料不处理
	if(pItemInfo->m_wType != ITEMTYPE_MATERIAL)
	{
		OUTPUTTOP(TEXTRES_ITEM_NOT_MAKE_MATERIAL);
		//theMouseHandler.CancelHandlingItem();
		return TRUE;
	}

	if(rFromSlot.GetDamageLV())
	{
		OUTPUTTOP(TEXTRES_ITEM_NOT_ACCEPT_DAMAGED);
		//theMouseHandler.CancelHandlingItem();
		return TRUE;
	}

	if(!theItemCompoundDialog.CheckMakeMaterialAt((eMATERIAL_TYPE)pItemInfo->m_byMaterialType, posTo))
	{
		OUTPUTTOP(TEXTRES_ITEM_MATERIAL_NOTMATCH);
		//theMouseHandler.CancelHandlingItem();
		return TRUE;
	}

	return _DoDummyTransaction( posFrom,  posTo);
}




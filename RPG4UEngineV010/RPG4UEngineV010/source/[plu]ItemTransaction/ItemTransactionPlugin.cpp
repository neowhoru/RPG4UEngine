// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "LogSystem.h"
#include "ConstSlot.h"

#include "QuickItemTransaction.h"
#include "ItemTransactionManager.h"
#include "NpcStoreItemTransaction.h"
#include "InventoryItemTransaction.h"
#include "TempItemTransaction.h"
#include "EventItemTransaction.h"
#include "EquipItemTransaction.h"
#include "StyleItemTransaction.h"
#include "WareHouseItemTransaction.h"
#include "SkillItemTransaction.h"
#include "TradeItemTransaction.h"
#include "EnchantItemTransaction.h"
#include "RankUpItemTransaction.h"
#include "VendorSellItemTransaction.h"
#include "VendorBuyItemTransaction.h"
#include "EnchantTargetItemTransaction.h"
#include "EnchantSocketItemTransaction.h"




#define REG_TRANSACTION(x,y,c)\
	static c s##ItemTran##c;\
	theItemTransactionManager.RegisterItemTransaction(x,y,&s##ItemTran##c)

#define UNREG_TRANSACTION(x,y,p)\
	theItemTransactionManager.UnregisterItemTransaction(x,y)



/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_ITEMTRANSACTION_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{

	REG_TRANSACTION(SI_QUICK,				INVALID_SLOTINDEX,				QuickDefaultTransaction);
	REG_TRANSACTION(SI_QUICK,				SI_QUICK,			QuickItemTransaction);
											
	REG_TRANSACTION(SI_NPCSHOP,			INVALID_SLOTINDEX,				NpcStoreDefaultTransaction);
	REG_TRANSACTION(SI_NPCSHOP,			SI_NPCSHOP,		NpcStoreItemTransaction);
	REG_TRANSACTION(SI_NPCSHOP,			SI_INVENTORY,		NpcStoreInventoryTransaction);
											
	REG_TRANSACTION(SI_INVENTORY,			INVALID_SLOTINDEX,				InventoryDefaultTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_INVENTORY,		InventoryItemTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_NPCSHOP,		InventoryNpcStoreTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_QUICK,			InventoryQuickTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_TRADE,			InventoryTradeTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_VENDOR_SELL,	InventoryVendorSellTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_EQUIPMENT,		InventoryEquipmentTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_WAREHOUSE,		InventoryWareHouseTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_ENCHANT_TARGET,InventoryEnchantTargetTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_ENCHANT_SOCKET,InventoryEnchantSocketTransaction);
	REG_TRANSACTION(SI_INVENTORY,			SI_ENCHANT,			InventoryEnchantMaterialTransaction);

	REG_TRANSACTION(SI_INVENTORY2,	INVALID_SLOTINDEX,				TempDefaultTransaction);
	REG_TRANSACTION(SI_INVENTORY2,	SI_INVENTORY2,	TempItemTransaction);
	REG_TRANSACTION(SI_INVENTORY2,	SI_INVENTORY,		TempInventoryTransaction);

	REG_TRANSACTION(SI_EVENT_INVENTORY,	INVALID_SLOTINDEX,				EventDefaultTransaction);
	REG_TRANSACTION(SI_EVENT_INVENTORY,	SI_EVENT_INVENTORY,EventItemTransaction);
	REG_TRANSACTION(SI_EVENT_INVENTORY,	SI_INVENTORY,		EventInventoryTransaction);

	REG_TRANSACTION(SI_EQUIPMENT,			INVALID_SLOTINDEX,				EquipDefaultTransaction);
	REG_TRANSACTION(SI_EQUIPMENT,			SI_EQUIPMENT,		EquipItemTransaction);
	REG_TRANSACTION(INVALID_SLOTINDEX,				SI_EQUIPMENT,		EquipReceiveTransaction);
	REG_TRANSACTION(SI_EQUIPMENT,			SI_INVENTORY,		EquipInventoryTransaction);

	REG_TRANSACTION(SI_STYLE,				INVALID_SLOTINDEX,				StyleDefaultTransaction);
	REG_TRANSACTION(SI_STYLE,				SI_STYLE,			StyleItemTransaction);
	REG_TRANSACTION(INVALID_SLOTINDEX,				SI_STYLE,			StyleReceiveTransaction);

	REG_TRANSACTION(SI_WAREHOUSE,			INVALID_SLOTINDEX,				WareHouseDefaultTransaction);
	REG_TRANSACTION(SI_WAREHOUSE,			SI_WAREHOUSE,		WareHouseItemTransaction);
	REG_TRANSACTION(SI_WAREHOUSE,			SI_INVENTORY,		WareHouseInventoryTransaction);

	REG_TRANSACTION(SI_TRADE,				INVALID_SLOTINDEX,				TradeDefaultTransaction);
	REG_TRANSACTION(SI_TRADE,				SI_TRADE,			TradeItemTransaction);
	REG_TRANSACTION(SI_TRADE,				SI_INVENTORY,		TradeInventoryTransaction);

	REG_TRANSACTION(SI_ENCHANT,			INVALID_SLOTINDEX,				EnchantDefaultTransaction);
	REG_TRANSACTION(SI_ENCHANT,			SI_ENCHANT,			EnchantItemTransaction);
	REG_TRANSACTION(SI_ENCHANT,			SI_INVENTORY,		EnchantInventoryTransaction);

	REG_TRANSACTION(SI_ENCHANT_TARGET,	INVALID_SLOTINDEX,				EnchantTargetDefaultTransaction);
	REG_TRANSACTION(SI_ENCHANT_TARGET,	SI_ENCHANT_TARGET,EnchantTargetItemTransaction);
	REG_TRANSACTION(SI_ENCHANT_TARGET,	SI_INVENTORY,		EnchantTargetInventoryTransaction);

	REG_TRANSACTION(SI_ENCHANT_SOCKET,	INVALID_SLOTINDEX,				EnchantSocketDefaultTransaction);
	REG_TRANSACTION(SI_ENCHANT_SOCKET,	SI_ENCHANT_SOCKET,EnchantSocketItemTransaction);
	REG_TRANSACTION(SI_ENCHANT_SOCKET,	SI_INVENTORY,		EnchantSocketInventoryTransaction);

	REG_TRANSACTION(SI_RANKUP,				INVALID_SLOTINDEX,				RankUpDefaultTransaction);
	REG_TRANSACTION(SI_RANKUP,				SI_RANKUP,			RankUpItemTransaction);
	REG_TRANSACTION(SI_RANKUP,				SI_INVENTORY,		RankUpInventoryTransaction);

	REG_TRANSACTION(SI_VENDOR_SELL,		INVALID_SLOTINDEX,				VendorSellDefaultTransaction);
	REG_TRANSACTION(SI_VENDOR_SELL,		SI_VENDOR_SELL,	VendorSellItemTransaction);
	REG_TRANSACTION(SI_VENDOR_SELL,		SI_INVENTORY,		VendorSellInventoryTransaction);

	REG_TRANSACTION(SI_VENDOR_BUY,		INVALID_SLOTINDEX,				VendorBuyDefaultTransaction);
	REG_TRANSACTION(SI_VENDOR_BUY,		SI_VENDOR_BUY,		VendorBuyItemTransaction);
	REG_TRANSACTION(SI_VENDOR_BUY,		SI_INVENTORY,		VendorBuyInventoryTransaction);

	REG_TRANSACTION(SI_SKILL,				INVALID_SLOTINDEX,				SkillDefaultTransaction);
	REG_TRANSACTION(SI_SKILL,				SI_SKILL,			SkillItemTransaction);
	REG_TRANSACTION(INVALID_SLOTINDEX,				SI_SKILL,			SkillReceiveTransaction);
	REG_TRANSACTION(SI_SKILL,				SI_QUICK,			SkillQuickTransaction);
	REG_TRANSACTION(SI_SKILL,				SI_STYLE,			SkillStyleTransaction);


	LOGINFO("Plugin ItemTransaction insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_ITEMTRANSACTION_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	UNREG_TRANSACTION(SI_QUICK,				INVALID_SLOTINDEX,				QuickDefaultTransaction);
	UNREG_TRANSACTION(SI_QUICK,				SI_QUICK,			QuickItemTransaction);
											
	UNREG_TRANSACTION(SI_NPCSHOP,			INVALID_SLOTINDEX,				NpcStoreDefaultTransaction);
	UNREG_TRANSACTION(SI_NPCSHOP,			SI_NPCSHOP,		NpcStoreItemTransaction);
	UNREG_TRANSACTION(SI_NPCSHOP,			SI_INVENTORY,		NpcStoreInventoryTransaction);
											
	UNREG_TRANSACTION(SI_INVENTORY,			INVALID_SLOTINDEX,				InventoryDefaultTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_INVENTORY,		InventoryItemTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_NPCSHOP,		InventoryNpcStoreTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_QUICK,			InventoryQuickTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_TRADE,			InventoryTradeTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_VENDOR_SELL,	InventoryVendorSellTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_EQUIPMENT,		InventoryEquipmentTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_WAREHOUSE,		InventoryWareHouseTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_ENCHANT_TARGET,InventoryEnchantTargetTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_ENCHANT_SOCKET,InventoryEnchantSocketTransaction);
	UNREG_TRANSACTION(SI_INVENTORY,			SI_ENCHANT,			InventoryEnchantMaterialTransaction);

	UNREG_TRANSACTION(SI_INVENTORY2,		INVALID_SLOTINDEX,				TempDefaultTransaction);
	UNREG_TRANSACTION(SI_INVENTORY2,		SI_INVENTORY2,	TempItemTransaction);
	UNREG_TRANSACTION(SI_INVENTORY2,		SI_INVENTORY,		TempInventoryTransaction);

	UNREG_TRANSACTION(SI_EVENT_INVENTORY,	INVALID_SLOTINDEX,				EventDefaultTransaction);
	UNREG_TRANSACTION(SI_EVENT_INVENTORY,	SI_EVENT_INVENTORY,EventItemTransaction);
	UNREG_TRANSACTION(SI_EVENT_INVENTORY,	SI_INVENTORY,		EventInventoryTransaction);

	UNREG_TRANSACTION(SI_EQUIPMENT,			INVALID_SLOTINDEX,				EquipDefaultTransaction);
	UNREG_TRANSACTION(SI_EQUIPMENT,			SI_EQUIPMENT,		EquipItemTransaction);
	UNREG_TRANSACTION(INVALID_SLOTINDEX,					SI_EQUIPMENT,		EquipReceiveTransaction);
	UNREG_TRANSACTION(SI_EQUIPMENT,			SI_INVENTORY,		EquipInventoryTransaction);

	UNREG_TRANSACTION(SI_STYLE,				INVALID_SLOTINDEX,				StyleDefaultTransaction);
	UNREG_TRANSACTION(SI_STYLE,				SI_STYLE,			StyleItemTransaction);
	UNREG_TRANSACTION(INVALID_SLOTINDEX,					SI_STYLE,			StyleReceiveTransaction);

	UNREG_TRANSACTION(SI_WAREHOUSE,			INVALID_SLOTINDEX,				WareHouseDefaultTransaction);
	UNREG_TRANSACTION(SI_WAREHOUSE,			SI_WAREHOUSE,		WareHouseItemTransaction);
	UNREG_TRANSACTION(SI_WAREHOUSE,			SI_INVENTORY,		WareHouseInventoryTransaction);

	UNREG_TRANSACTION(SI_TRADE,				INVALID_SLOTINDEX,				TradeDefaultTransaction);
	UNREG_TRANSACTION(SI_TRADE,				SI_TRADE,			TradeItemTransaction);
	UNREG_TRANSACTION(SI_TRADE,				SI_INVENTORY,		TradeInventoryTransaction);

	UNREG_TRANSACTION(SI_ENCHANT,				INVALID_SLOTINDEX,				EnchantDefaultTransaction);
	UNREG_TRANSACTION(SI_ENCHANT,				SI_ENCHANT,			EnchantItemTransaction);
	UNREG_TRANSACTION(SI_ENCHANT,				SI_INVENTORY,		EnchantInventoryTransaction);

	UNREG_TRANSACTION(SI_ENCHANT_TARGET,	INVALID_SLOTINDEX,				EnchantTargetDefaultTransaction);
	UNREG_TRANSACTION(SI_ENCHANT_TARGET,	SI_ENCHANT_TARGET,EnchantTargetItemTransaction);
	UNREG_TRANSACTION(SI_ENCHANT_TARGET,	SI_INVENTORY,		EnchantTargetInventoryTransaction);

	UNREG_TRANSACTION(SI_ENCHANT_SOCKET,	INVALID_SLOTINDEX,				EnchantSocketDefaultTransaction);
	UNREG_TRANSACTION(SI_ENCHANT_SOCKET,	SI_ENCHANT_SOCKET,EnchantSocketItemTransaction);
	UNREG_TRANSACTION(SI_ENCHANT_SOCKET,	SI_INVENTORY,		EnchantSocketInventoryTransaction);


	UNREG_TRANSACTION(SI_RANKUP,				INVALID_SLOTINDEX,				RankUpDefaultTransaction);
	UNREG_TRANSACTION(SI_RANKUP,				SI_RANKUP,			RankUpItemTransaction);
	UNREG_TRANSACTION(SI_RANKUP,				SI_INVENTORY,		RankUpInventoryTransaction);

	UNREG_TRANSACTION(SI_VENDOR_SELL,		INVALID_SLOTINDEX,				VendorSellDefaultTransaction);
	UNREG_TRANSACTION(SI_VENDOR_SELL,		SI_VENDOR_SELL,	VendorSellItemTransaction);
	UNREG_TRANSACTION(SI_VENDOR_SELL,		SI_INVENTORY,		VendorSellInventoryTransaction);

	UNREG_TRANSACTION(SI_VENDOR_BUY,			INVALID_SLOTINDEX,				VendorBuyDefaultTransaction);
	UNREG_TRANSACTION(SI_VENDOR_BUY,			SI_VENDOR_BUY,		VendorBuyItemTransaction);
	UNREG_TRANSACTION(SI_VENDOR_BUY,			SI_INVENTORY,		VendorBuyInventoryTransaction);

	UNREG_TRANSACTION(SI_SKILL,				INVALID_SLOTINDEX,				SkillDefaultTransaction);
	UNREG_TRANSACTION(SI_SKILL,				SI_SKILL,			SkillItemTransaction);
	UNREG_TRANSACTION(INVALID_SLOTINDEX,					SI_SKILL,			SkillReceiveTransaction);
	UNREG_TRANSACTION(SI_SKILL,				SI_QUICK,			SkillQuickTransaction);
	UNREG_TRANSACTION(SI_SKILL,				SI_STYLE,			SkillStyleTransaction);

	return gamemain::ePluginOK;
}

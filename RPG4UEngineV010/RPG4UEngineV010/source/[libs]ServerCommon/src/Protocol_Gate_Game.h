/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_Gate_Game.h
创建日期：2009年6月15日
最后更新：2009年6月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PROTOCOL_GATE_GAME_H__
#define __PROTOCOL_GATE_GAME_H__
#pragma once

#include "Protocol_Define.h"
#include <Protocol.h>
#include "Protocol_ClientAgent.h"

#define _AG_CAT(NAME)		_PROTOCOL_CATEGORY(AG,NAME)
#define _AG_NIL(NAME)		_PROTOCOL_NIL		(AG,NAME)
#define _AG_SYN(NAME)		_PROTOCOL_SYN		(AG,NAME)
#define _AG_ACK(NAME)		_PROTOCOL_ACK		(AG,NAME)
#define _AG_BRD(NAME)		_PROTOCOL_BRD		(AG,NAME)
#define _AG_SYN_AN(NAME)	_PROTOCOL_SYN_AN	(AG,NAME)
#define _AG_SYN_N(NAME)		_PROTOCOL_SYN_N	(AG,NAME)
#define _AG_SYN_B(NAME)		_PROTOCOL_SYN_B	(AG,NAME)
#define _AG_SYN_BN(NAME)	_PROTOCOL_SYN_BN	(AG,NAME)


#define _AGSYNC_SYN_AN(NAME)		_PROTOCOL_SYN_AN	(AG_SYNC,NAME)
#define _AGCONN_CMD(NAME)			_PROTOCOL_CMD		(AG_CONNECTION,NAME)
#define _AGZONE_SYN_AN(NAME)		_PROTOCOL_SYN_AN	(AG_ZONE,NAME)
#define _AGZONE_SYN_ANC(NAME)		_PROTOCOL_SYN_ANC	(AG_ZONE,NAME)
#define _AGZONE_CMD(NAME)			_PROTOCOL_CMD		(AG_ZONE,NAME)

#define _AGPARTY_SYN(NAME)			_PROTOCOL_SYN		(AG_PARTY,NAME)
#define _AGPARTY_NAK(NAME)			_PROTOCOL_NAK		(AG_PARTY,NAME)
#define _AGPARTY_ACK(NAME)			_PROTOCOL_ACK		(AG_PARTY,NAME)
#define _AGPARTY_CMD(NAME)			_PROTOCOL_CMD		(AG_PARTY,NAME)

#define _AGSTATUS_CMD(NAME)		_PROTOCOL_CMD		(AG_STATUS,NAME)
#define _AGGM_CMD(NAME)				_PROTOCOL_CMD		(AG_GM,NAME)

#define _AGGUILD_ACK(NAME)			_PROTOCOL_ACK		(AG_GUILD,NAME)
#define _AGGUILD_CMD(NAME)			_PROTOCOL_CMD		(AG_GUILD,NAME)




///////////////////////////////////////
enum eAG_CATEGORY
{
	 _AG_BASEVALUE				= PROTOCOL_RANGE(AG)
	,_AG_CAT(CONNECTION)		= _AG_BASEVALUE
	,_AG_CAT(CHARINFO)
	,_AG_CAT(SYNC)
	,_AG_CAT(ZONE)
	,_AG_CAT(PARTY)
	,_AG_CAT(STATUS)
	,_AG_CAT(GM)
	,_AG_CAT(GUILD)
	,_AG_CAT(MAX)
	,_AG_MAXVALUE				= _AG_CAT(MAX)
};
PROTOCOL_CHECK(AG);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAG_SYNC_PROTOCOL
{
	 _AGSYNC_SYN_AN(PLAYER_ENTER)
	,_AGSYNC_SYN_AN(PLAYER_WARP)

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAG_CONNECTION_PROTOCOL
{
	 _AGCONN_CMD(DISCONNECT)
	,_AGCONN_CMD(UNREGISTER)


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAG_CHARINFO_PROTOCOL
{
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAG_ZONE_PROTOCOL
{
	 _AGZONE_CMD(LOBBY_CREATE	)
	,_AGZONE_CMD(HUNTING_CREATE)
	,_AGZONE_CMD(MISSION_CREATE)
	,_AGZONE_CMD(VILLAGE_CREATE)
	,_AGZONE_CMD(PVP_CREATE		)
	,_AGZONE_CMD(ROOM_DESTROY	)
					 


	,_AGZONE_SYN_ANC(ROOM_JOIN)
	,_AGZONE_SYN_ANC(ROOM_LEAVE)


	,_AGZONE_CMD	(MASTER_CHANGED)
	,_AGZONE_CMD	(MISSION_LEAVE	)
	,_AGZONE_CMD	(PVP_INFO		)
	,_AGZONE_SYN_AN(VILLAGE_MOVE	)


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAG_PARTY_PROTOCOL
{
	 _AGPARTY_SYN(GATE2BATTLE_INVITE_REQUEST	)
	,_AGPARTY_SYN(BATTLE2GATE_INVITE_REQUEST	)
	,_AGPARTY_SYN(GATE2FIELD_INVITE_REQUEST	)

	,_AGPARTY_ACK(GATE2FIELD_INVITE_RESPONSE	)
	,_AGPARTY_ACK(GATE2BATTLE_INVITE_RESPONSE)
	,_AGPARTY_CMD(BATTLE2GATE_JOINROOM			)

	,_AGPARTY_NAK(GATE2FIELD_INVITE_RESPONSE	)
	,_AGPARTY_NAK(FIELD2GATE_INVITE_RESPONSE	)
	,_AGPARTY_NAK(GATE2BATTLE_INVITE_RESPONSE)

	,_AGPARTY_CMD(BATTLE2GATE_CHANGE_MASTER	)

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAG_STATUS_PROTOCOL
{
	_AGSTATUS_CMD(LEVEL_UP)

};

enum eAG_GM_PROTOCOL
{
	_AGGM_CMD(TELEPORT)

};

enum eAG_GUILD_PROTOCOL
{
	 _AGGUILD_ACK(CREATE			)
	,_AGGUILD_ACK(DESTROY		)
	,_AGGUILD_CMD(JOIN_SUCCESS	)
	,_AGGUILD_ACK(WITHDRAW		)

};




#endif //__PROTOCOL_GATE_GAME_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_Gate_Chat.h
创建日期：2009年6月15日
最后更新：2009年6月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __Protocol_Gate_Chat_H__
#define __Protocol_Gate_Chat_H__
#pragma once

#include "Protocol_Define.h"
#include <Protocol.h>

#define _AW_CAT(NAME)		_PROTOCOL_CATEGORY(AW,NAME)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAW_CATEGORY
{
	 _AW_CAT(CONNECTION)		= PROTOCOL_RANGE(AW)
	,_AW_CAT(USER)
	,_AW_CAT(MAX)
};
PROTOCOL_CHECK(AW);

#define _AWCONN_SYN(NAME)		_PROTOCOL_SYN		(AW_CONNECTION,NAME)
#define _AWCONN_SYN_AN(NAME)	_PROTOCOL_SYN_AN	(AW_CONNECTION,NAME)
#define _AWUSER_SYN(NAME)		_PROTOCOL_SYN		(AW_USER,NAME)
#define _AWUSER_SYN_C(NAME)	_PROTOCOL_SYN_C	(AW_USER,NAME)
#define _AWUSER_CMD(NAME)		_PROTOCOL_CMD		(AW_USER,NAME)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAW_CONNECTION_PROTOCOL
{
	 _AWCONN_SYN	(AGENTSERVER_INFO)
	,_AWCONN_SYN_AN(PREPARE_NEW_USER)


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eAW_USER_STATUS_PROTOCOL
{
	 _AWUSER_SYN	(SET_CHARNAME	)
	,_AWUSER_SYN	(UNSET_CHARNAME)
	,_AWUSER_SYN	(ENTER_ZONE)
	,_AWUSER_SYN	(LEAVE_ZONE)
	,_AWUSER_CMD	(LOGOUT)
	,_AWUSER_SYN_C	(RESTORE)


};



#endif //__Protocol_Gate_Chat_H__
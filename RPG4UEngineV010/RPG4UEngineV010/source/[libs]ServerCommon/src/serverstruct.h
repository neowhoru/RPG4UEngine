#ifndef __SERVER_STRUCT_H__
#define __SERVER_STRUCT_H__

#include "StructBase.h"
#include <structInPacket.h>



#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct SERVER_KEY
{
	DWORD dwKey;

	SERVER_KEY(DWORD key = 0 ) { dwKey = key; }
	operator DWORD() const;

	DWORD Set				(BYTE worldID
                        ,BYTE channelID
								,BYTE serverType
								,BYTE serverID);
	BYTE	GetWorldID		()	const	;
	BYTE	GetChannelID	()	const	;
	BYTE	GetServerType	()	const	;
	BYTE	GetServerSubID	()	const	;
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPLAYER_SERVER_PART
{
	CHARGUID		m_CharGuid;
	BYTE			m_bySlot;
	TCHAR			m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE			m_byHeight;
	BYTE			m_byFace;
	BYTE			m_byHair;
	BYTE			m_byHairColor;
	BYTE			m_bySex;
	BYTE			m_byClass;
	LEVELTYPE	m_LV;
	DWORD			m_dwRegion;
	WORD			m_wX;
	WORD			m_wY;
	WORD			m_wZ;

	GUILDGUID	m_GuildGuid		;
	BYTE			m_GuildPosition;
	TCHAR			m_szGuildNick	[MAX_CHARNAME_LENGTH];

	sEQUIP_ITEM_INFO	m_EquipItemInfo;


	/////////////////////////////////////////////////
	int GetSize()
	{
		return	sizeof(sPLAYER_SERVER_PART) 
				-	(MAX_EQUIPMENT_SLOT_NUM - m_EquipItemInfo.m_Count)*sizeof(sITEM_SLOTEX);
	}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sLOGOUT_INFO
{
	DWORD			dwAuthUserID;							// 
	TCHAR			szAccountID	[MAX_ID_LENGTH];		// 
	TCHAR			szCharName	[MAX_CHARNAME_LENGTH];	// 
	WORD			wSvrCode		;						// 
	WORD			charLV		;						// 
	BYTE			charType		;						// 
	BYTE			byLogoutType;					// 
	TCHAR			szLogoutTime[MAX_TIMEDATA_SIZE];
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSERVERLOG_INFO
{
	BYTE	byLogOption		;
	BYTE	byLogFileLevel	;
	TCHAR	szLogFilePath	[MAX_LOGPATH];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sIOHANDLER_INFO
{
	TCHAR	szIP[MAX_IP_LENGTH]	;
	WORD	wPort						;
	DWORD	dwMaxAcceptSession	;
	DWORD	dwMaxConnectSession	;
	DWORD	dwSendBufferSize		;
	DWORD	dwRecvBufferSize		;
	DWORD	dwTimeOut				;
	DWORD	dwMaxAcceptThreads	;
	DWORD	dwMaxIOThreads			;
	DWORD	dwMaxConnectThreads	;
	DWORD	dwMaxPacketSize		;
};
typedef const sIOHANDLER_INFO  sIOHANDLER_INFOC;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSQLSERVER_INFO
{
	INT		nSQLServerType			;
	INT		nSQLServerThreadNum	;
	INT		nSQLServerPoolSize	;
	TCHAR		szSQLServerIP	[ MAX_IP_LENGTH ];
	TCHAR		szSQLDBName		[ MAX_DBNAME_LENGTH ];
	TCHAR		szSQLUserName	[ MAX_DBNAME_LENGTH ];
	TCHAR		szSQLUserPwd	[ MAX_DBNAME_LENGTH ];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSERVER_INFO
{
	VRSTR		m_sGroupFile	;	//群组信息文件
	VRSTR		m_sPoolSizeFile;	//群组信息文件

	BOOL		bUseServerGroupFile;
	BOOL		bEnableGMFile		;
	BYTE		byFieldServerType	;
	BYTE		byBattleServerType;
	BOOL		bVillageRenderInfo;


	DWORD		dwGateUserMax;
	DWORD		dwGateZonePoolSize;

	/////////////////////////////////////
	sIOHANDLER_INFO ioHandlerClient;
	sIOHANDLER_INFO ioHandlerServer;



	BOOL		bEnableLoginShell;
	DWORD		dwTestUserGUID;
	TCHAR		szUserAccount[ MAX_NAME_LENGTH ]; //leo


	sSQLSERVER_INFO	sqlServerGame;
	sSQLSERVER_INFO	sqlServerUser;

	TCHAR					szLogPath			[MAX_LOGPATH];
	BYTE					byLogOption;

	DWORD					dwStatisticsDelay;

};

typedef const sSERVER_INFO sSERVER_INFOC;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sAIPARAM_INFO
{
	WORD		m_AggroTime;					// 
	WORD		m_BattleRecordUpdateDelay;	// 
	BYTE		m_DamageReduceRatio;		// 
	WORD		m_MinDamagePT;				// 
	WORD		m_FirstAttackPower;		// 
	WORD		m_NearDistancePT;			// 
	WORD		m_LowLevelPT;				// 
	WORD		m_LowHPPoint;				// 
	WORD		m_DamagePT;					// 

	//////////////////////////////
	WORD		m_PatrolPeriodMin;			// 
	WORD		m_PatrolPeriodMax;			// 
	WORD		m_SearchPeriod;				// 
	WORD		m_SearchAreaPeriod;		// 
	WORD		m_RetreatPeriod;			// 
	WORD		m_TrackPeriod;				// 
	WORD		m_ReturnPeriod;				// 
	WORD		m_DeadWaitDelay;				// 
	WORD		m_IdlePeriodMin;				// 
	WORD		m_IdlePeriodMax;				// 
	WORD		m_KnockdownPeriod;			// 
	WORD		m_RunawayTime;				// 
	WORD		m_ThrustTime;				// 

	//////////////////////////////
	float		m_MoveDistanceMin;			// ..)
	float		m_MoveDistanceMax;
	float		m_NpcRegenRange;		// 
	float		m_MovableDistanceMin;		// 
	float		m_NpcWanderRange;
	float		m_GroupFollowRangeMin;	// 
	float		m_GroupFollowRangeMax;

	//////////////////////////////
	float		m_JumpHeightMin;	
	float		m_JumpHeightMax;
	float		m_JumpLengthMin;
	float		m_JumpLengthMax;

	//////////////////////////////
	float		m_FallapartMinAttackRange;	//
	WORD		m_FallapartPeriod;

	//////////////////////////////
	float		m_RetreatDistanceMin;			//
	float		m_RetreatDistanceLimitRatio;	//

	//////////////////////////////
	float		m_HelpSightRatio;					//
	float		m_HelpNeedHPPercent;				//

	//////////////////////////////
	float		m_RangeTolerance;				// 
	WORD		m_SearchRotate;			// 
	BYTE		m_MaxObserversPerPlayer;	// 
	WORD		m_GroupAIMsgDelayMin;	// 
	WORD		m_GroupAIMsgDelayMax;
	WORD		m_TrackInnerAngle;			// 
	DWORD		m_NpcRegenPeriod;					// 
	WORD		m_PlayingOverTime;			// 

	//////////////////////////////
	float		m_EtherSplashDamageRatio;	// 
	float		m_StyleSplashDamageRatio;	// 

	float		m_NpcMeetDistance;		//
	float		m_PlayerTradeDistance;		//
	//////////////////////////////
	float		m_SummonFollowDistance;		// 
	float		m_SummonFollowDistanceMax;// 
	WORD		m_SummonChangeMovingTime;// 

	///////////////////////////
	// HP, MP
	WORD		m_PlayerHPRegenPeriod;		//
	WORD		m_PlayerMPRegenPeriod;		//
	WORD		m_NpcHPMPRegenPeriod;		//

	WORD		m_wScriptRunPeriod;
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSERVER_OPTION
{
	BYTE		m_byIndex;			
	VRSTR		m_sServerName;
	DWORD		m_dwCoolTimeMargin;
	DWORD		m_dwPlayerDBUpdateDelay;
	DWORD		m_dwPlayerPlayingDelay ;
	DWORD		m_dwPlayerSnapshopDelay;
	WORD		m_wTargetFinderPool;
	WORD		m_wGuildMemberPool;
	WORD		m_wGuildPool		;
	WORD		m_wEffectPool;
	WORD		m_wPlayerPool;
	WORD		m_wNPCPool;
	WORD		m_wItemPool;
	WORD		m_wMapNPCPool;
	WORD		m_wMapObjectPool;
	WORD		m_wPlayerSkillPool;
	WORD		m_wNPCSkillPool;
	WORD		m_wAbilityPool;
	WORD		m_wPartyPool;
	WORD		m_wSummonPool;
	WORD		m_wStatusPool;
	WORD		m_wCoolTimerCharPool;
	WORD		m_wSectorPool;
	WORD		m_wMapPool;	
	WORD		m_wTradePool	;
	WORD		m_wRoomPool		;
	WORD		m_wPVPRoomPool	;
	WORD		m_wLobbyPool	;
	WORD		m_wVillagePool	;

	// 配置信息
	WORD		m_wRateInfo;
	WORD		m_wQuestItemDropInfo;
	WORD		m_wItemMakeInfo;
	WORD		m_wItemRandAttrTable;
	WORD		m_wItemRandAttrInfo;
	WORD		m_wItemDoubleAttrInfo;

	WORD		m_wItemFreeDelay		;
	WORD		m_wNPCInfo;
	WORD		m_wItemDestroyDelay	;
	WORD		m_wItemInfo;
	WORD		m_wExtraNPCInfo;
	WORD		m_wMapInfo;
	WORD		m_wQuestInfo	;
	WORD		m_wShopInfo;
	WORD		m_wSkillInfo;
	WORD		m_wRegenInfo;
	WORD		m_wGroupInfo;
	WORD		m_wStateInfo;
	WORD		m_wItemCompInfo;
	WORD		m_wItemCompResultInfo;
	WORD		m_wMissionRewardInfo;
	WORD		m_wStyleQuickInfo;
	WORD		m_wFormulaRatioInfo;
};

/////////////////////////////////////////////////////
struct sBATTLEZONE_INFO
{
	BYTE			byWorldID;
	BYTE			byChannelID;
	DWORD			dwVillageUserCount;	
	DWORD			dwLobbyZoneCount;		
	DWORD			dwLobbyUserCount;		
	DWORD			dwHuntingZoneCount;	
	DWORD			dwHuntingUserCount;	
	DWORD			dwMissionZoneCount;	
	DWORD			dwMissionUserCount;	
	DWORD			dwQuestZoneCount;		
	DWORD			dwQuestUserCount;		
	DWORD			dwPVPZoneCount;		
	DWORD			dwPVPUserCount;		
	DWORD			dwEventZoneCount;		
	DWORD			dwEventUserCount;		
	DWORD			dwGuildZoneCount;		
	DWORD			dwGuildUserCount;		
	DWORD			dwSiegeZoneCount;		
	DWORD			dwSiegeUserCount;		
};


struct sSERVERIP_INFO
{
	SERVER_KEY	ServerKey;
	DWORD			dwServerGUID;
	char			szIP[MAX_IP_LENGTH];
	WORD			wPort;
	char			szInnerIP[MAX_IP_LENGTH];
	WORD			wInnerPort;
};
typedef	sSERVERIP_INFO*		LPSERVERIP_INFO;
typedef	const sSERVERIP_INFO sSERVERIP_INFOC;


#pragma pack(pop)


#include "ServerStruct.inl"

#endif // __SERVER_STRUCT_H__
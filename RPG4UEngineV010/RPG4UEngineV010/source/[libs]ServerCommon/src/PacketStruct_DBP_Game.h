/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_DBP_Game.h
创建日期：2009年6月14日
最后更新：2009年6月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_DBP_GAME_H__
#define __PACKETSTRUCT_DBP_GAME_H__
#pragma once



#include "StructBase.h"
#include "PacketStruct_Base.h"
#include <ConstSlot.h>
#include <Protocol_DBP_Game.h>



#include "PacketStruct_Define.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define MSGPACKET_DG_BEGIN( c, p )			MSGPACKET_BEGIN(DG_##c,	DG_##p)
#define MSGPACKET_DG_END						MSGPACKET_END

#define MSGOBJECT_DG_BEGIN(c,p)				MSGPACKET_OBJECT_BEGIN(DG_##c,DG_##p)
#define MSGOBJECT_DG_END						MSGPACKET_OBJECT_END

#define MSGPACKET_DG_SUPER_BEGIN(c, p,s)	MSGPACKET_SUPER_BEGIN(DG_##c,DG_##p,DG_##s)
#define MSGPACKET_DG_SUPER_END				MSGPACKET_SUPER_END

#define SIZE_DG_MSG(p)							SIZE_MSG(DG_##p)



#define MSGOBJECT_DG_CONN(p)				MSGOBJECT_DG_BEGIN(CONNECTION,CONNECTION_##p)
#define MSGOBJECT_DG_CHARINFO(p)			MSGOBJECT_DG_BEGIN(CHARINFO,CHARINFO_##p)
#define MSGOBJECT_DG_ITEM(p)				MSGOBJECT_DG_BEGIN(ITEM,ITEM_##p)
#define MSGOBJECT_DG_WAREHOUSE(p)		MSGOBJECT_DG_BEGIN(WAREHOUSE,WAREHOUSE_##p)
#define MSGOBJECT_DG_EVENT(p)				MSGOBJECT_DG_BEGIN(EVENT,EVENT_##p)
#define SIZE_DG_CONN(p)						SIZE_MSG(DG_CONNECTION_##p)
#define SIZE_DG_CHARINFO(p)				SIZE_MSG(DG_CHARINFO_##p)
#define SIZE_DG_ITEM(p)						SIZE_MSG(DG_ITEM_##p)
#define SIZE_DG_WAREHOUSE(p)				SIZE_MSG(DG_WAREHOUSE_##p)
#define SIZE_DG_EVENT(p)					SIZE_MSG(DG_EVENT_##p)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#pragma pack(push,1)



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CONN(RECOVER_SYN )
	enum		{ MAX_RECOVER_INFO_NUM = 150, };
	WORD		m_byCount;

	struct RECOVER_INFO
	{
		DWORD	m_dwKey;
		BYTE	m_SelectedSlotIndex;
		DWORD	m_dwCharGUID;
	}m_RecoverInfos[MAX_RECOVER_INFO_NUM];

	int GetSize()
	{
		return (SIZE_DG_CONN(RECOVER_SYN) - sizeof(RECOVER_INFO)*(MAX_RECOVER_INFO_NUM-m_byCount));
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CONN(RECOVER_ACK )
	enum		{ MAX_RECOVER_INFO_NUM = 150, };
	WORD		m_byCount;
	struct RECOVER_INFO
	{
		DWORD	m_dwKey;
	}m_RecoverInfos[MAX_RECOVER_INFO_NUM];
	int GetSize()
	{
		return (SIZE_DG_CONN(RECOVER_ACK) - sizeof(RECOVER_INFO)*(MAX_RECOVER_INFO_NUM-m_byCount));
	}
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CONN(RECOVER_NAK )
	enum		{ MAX_RECOVER_INFO_NUM = 150, };
	WORD		m_byCount;
	struct RECOVER_INFO
	{
		DWORD	m_dwKey;
	}m_RecoverInfos[MAX_RECOVER_INFO_NUM];
	int GetSize()
	{
		return (SIZE_DG_CONN(RECOVER_NAK) - sizeof(RECOVER_INFO)*(MAX_RECOVER_INFO_NUM-m_byCount));
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(ALL_REQ_SYN )
	enum	{ ENTER_VILLAGE
			, JOIN_VILLAGE
			, JOIN_MISSION
			, CREATE_HUNTING
			, JOIN_HUNTING
			};
	BYTE		m_SelectedSlotIndex;
	BYTE		m_byType;
	DWORD		m_dwCharGUID;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(ALL_REQ_ACK )
	enum { ENTER_VILLAGE, JOIN_VILLAGE, JOIN_MISSION, CREATE_HUNTING, JOIN_HUNTING, };
	BYTE m_byType;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(ALL_REQ_NAK )
	enum { ENTER_VILLAGE, JOIN_VILLAGE, JOIN_MISSION, CREATE_HUNTING, JOIN_HUNTING, };
	BYTE m_byType;
	DWORD m_dwErrorCode;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(CHAR_ITEM_CMD )
	sPLAYERINFO_BASE	m_CharacterInfo;
	sTOTALINFO_ITEM m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_DG_CHARINFO(CHAR_ITEM_CMD) - (sTOTALINFO_ITEM::MAX_SLOT_NUM-m_ItemInfo.m_EquipCount-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(SKILL_CMD )
	sTOTALINFO_SKILL m_SkillInfo;
	int GetSize()
	{
		return ( SIZE_DG_CHARINFO(SKILL_CMD) - (sTOTALINFO_SKILL::MAX_SLOT_NUM-m_SkillInfo.m_Count)*sizeof(sSKILL_SLOT) );
	}
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(QUICK_CMD )
	sTOTALINFO_QUICK m_QuickInfo;
	int GetSize()
	{
		return ( SIZE_DG_CHARINFO(QUICK_CMD) - (sTOTALINFO_QUICK::MAX_SLOT_NUM-m_QuickInfo.m_Count)*sizeof(sQUICK_SLOT) );
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(STYLE_CMD )
	sTOTALINFO_STYLE m_StyleInfo;
	int GetSize()
	{
		return ( SIZE_DG_CHARINFO(STYLE_CMD) - (sTOTALINFO_STYLE::MAX_SLOT_NUM-m_StyleInfo.m_Count)*sizeof(sSTYLE_SLOT) );
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(QUEST_CMD )
	BYTE m_pQuestStream	[MAX_QUESTSTREAM_SIZE];
	BYTE m_pMissionStream[MAX_MISSIONSTREAM_SIZE];
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(WAREHOUSE_CMD )
	MONEY					m_WarehouseMoney;
	sTOTALINFO_WAREHOUSE	m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_DG_CHARINFO(WAREHOUSE_CMD) - (sTOTALINFO_WAREHOUSE::MAX_SLOT_NUM-m_ItemInfo.m_Count)*sizeof(sITEM_SLOTEX) );
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(UDPATE_CMD )
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(UDPATE_SYN )
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(UDPATE_ACK )
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_CHARINFO(UDPATE_NAK )
	enum { UNREGISTER, JOIN_HUNTING, LEAVE_HUNTING, LEAVE_MISSION, LEAVE_LOBBY };
	BYTE	m_byType;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_ITEM(SERIAL_SYN )
	SLOTIDX		m_SlotIndex;
	SLOTPOS		m_ItemPos;
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_ITEM(SERIAL_ACK )
	SLOTIDX		m_SlotIndex;
	SLOTPOS		m_ItemPos;
	DBSERIAL		m_DBSerial;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_ITEM(WAREHOUSEITEMALL_SYN )
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_ITEM(WAREHOUSEITEMALL_ACK )
	MONEY							m_Money;
	sTOTALINFO_WAREHOUSE		m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_DG_ITEM(WAREHOUSEITEMALL_ACK) - (sTOTALINFO_WAREHOUSE::MAX_SLOT_NUM-m_ItemInfo.m_Count)*sizeof(sITEM_SLOTEX) );
	}
MSGPACKET_DG_END;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_ITEM(WAREHOUSEITEMALL_NAK )
	DWORD m_dwErrorCode;
MSGPACKET_DG_END;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_WAREHOUSE(START_SYN )
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_WAREHOUSE(START_ACK )
	MONEY							m_WarehouseMoney;
	sTOTALINFO_WAREHOUSE		m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_DG_WAREHOUSE(START_ACK) - (sTOTALINFO_WAREHOUSE::MAX_SLOT_NUM-m_ItemInfo.m_Count)*sizeof(sITEM_SLOTEX) );
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_WAREHOUSE(START_NAK )
	DWORD m_dwErrorCode;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_WAREHOUSE(END_SYN )
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_WAREHOUSE(END_ACK )
MSGPACKET_DG_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_WAREHOUSE(END_NAK )
	DWORD m_dwErrorCode;
MSGPACKET_DG_END;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_EVENT(SELECT_SYN )
	USERGUID	m_UserGuid;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_EVENT(SELECT_ACK )
	sTOTALINFO_EVENT_ITEM_EX m_TotalInfo;
	int GetSize()
	{
		return ( SIZE_DG_EVENT(SELECT_ACK) - (sTOTALINFO_EVENT_ITEM_EX::MAX_SLOT_NUM-m_TotalInfo.m_Count)*sizeof(sEVENT_SLOTEX));
	}
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_EVENT(SELECT_NAK )
	BYTE		m_byErrorCode;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_EVENT(UPDATE_SYN )
	USERGUID	m_UserGuid;
	DWORD		m_ItemSeq;
	SLOTPOS		m_ItemPos;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_EVENT(UPDATE_ACK )
	SLOTPOS		m_ItemPos;
MSGPACKET_DG_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_DG_EVENT(UPDATE_NAK )
	BYTE		m_byErrorCode;
MSGPACKET_DG_END;


#pragma pack(pop)




#endif //__PACKETSTRUCT_DBP_GAME_H__
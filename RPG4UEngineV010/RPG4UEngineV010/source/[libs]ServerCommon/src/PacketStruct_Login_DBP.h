/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_Login_DBP.h
创建日期：2007年9月30日
最后更新：2007年9月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_LOGIN_DBP_H__
#define __PACKETSTRUCT_LOGIN_DBP_H__
#pragma once


#include <ServerStruct.h>
#include "PacketStruct_Base.h"
#include <Protocol_Login_DBP.h>

#include "PacketStruct_Define.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define MSGPACKET_TU_BEGIN( c, p )			MSGPACKET_BEGIN(TU_##c,	TU_##p)
#define MSGPACKET_TU_END						MSGPACKET_END

#define MSGOBJECT_TU_BEGIN(c,p)				MSGPACKET_OBJECT_BEGIN(TU_##c,TU_##p)
#define MSGOBJECT_TU_END						MSGPACKET_OBJECT_END

#define MSGPACKET_TU_SUPER_BEGIN(c, p,s)	MSGPACKET_SUPER_BEGIN(TU_##c,TU_##p,TU_##s)
#define MSGPACKET_TU_SUPER_END				MSGPACKET_SUPER_END

#define SIZE_TU_MSG(p)							SIZE_MSG(TU_##p)

#define MSGOBJECT_TU_USER(p)					MSGOBJECT_TU_BEGIN(USER,USER_##p)
#define SIZE_TU_USER(p)							SIZE_MSG(TU_USER_##p)


#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_TU_USER(LOGIN_SYN)
	TCHAR		szID[MAX_ID_LENGTH];
MSGPACKET_TU_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_TU_USER(LOGIN_ACK)
	USERGUID dwUserGUID;
	CHAR		szPassWD		[MAX_MD5_BUF_LENGTH];
	CHAR		szCharPassWD[MAX_MD5_BUF_LENGTH];
	BYTE		bySts;
MSGPACKET_TU_END;



#pragma pack(pop)


#endif //__PACKETSTRUCT_LOGIN_DBP_H__
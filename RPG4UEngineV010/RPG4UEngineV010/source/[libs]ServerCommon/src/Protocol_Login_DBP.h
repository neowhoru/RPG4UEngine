/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_Login_DBP.h
创建日期：2007年9月26日
最后更新：2007年9月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

 *  前缀
		C : Client
		G : Game Server
		M : Master Server
		D : DBP Server

 * 后缀
		SYN - Server同步协议，ACK协议接受处理，NAK协议拒绝处理
		ACK - SYN协议接受处理
		NAK - SYN协议拒绝处理
		CMD - Server指令，无须返回响应
		BRD - Server向外广播消息
		DBR - DBProxy数据库代理查询回应

 * 协议命名规则：
		前缀_分类_协议_后缀
		譬如) CG_CONNECTION_REGISTER_SYN

版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __Protocol_Login_DBP_H__
#define __Protocol_Login_DBP_H__
#pragma once

#include "Protocol_Define.h"
#include <Protocol.h>

#define _TU_CAT(NAME)		_PROTOCOL_CATEGORY(TU,NAME)

#define _TUCONN_CMD(NAME)		_PROTOCOL_CMD		(TU_CONNECTION,NAME)
#define _TUUSER_SYN_AND(NAME)	_PROTOCOL_SYN_AND	(TU_USER,NAME)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eTU_CATEGORY
{
	 _TU_CAT(CONNECTION)		= PROTOCOL_RANGE(TU)
	,_TU_CAT(USER)
	,_TU_CAT(MAX)
};

PROTOCOL_CHECK(TU);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eTU_CONNECTION_PROTOCOL
{
	_TUCONN_CMD(SERVER_INFO)
};

enum eTU_USER_PROTOCOL
{
	_TUUSER_SYN_AND(LOGIN)


};



#endif //__Protocol_Login_DBP_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_DBP_Chat.h
创建日期：2009年6月14日
最后更新：2009年6月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_DBP_CHAT_H__
#define __PACKETSTRUCT_DBP_CHAT_H__
#pragma once

#pragma once

#include <ServerStruct.h>
#include "PacketStruct_Base.h"

#include <Protocol_DBP_Chat.h>


#include "PacketStruct_Define.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define MSGPACKET_DW_BEGIN( c, p )			MSGPACKET_BEGIN(DW_##c,	DW_##p)
#define MSGPACKET_DW_END						MSGPACKET_END

#define MSGOBJECT_DW_BEGIN(c,p)				MSGPACKET_OBJECT_BEGIN(DW_##c,DW_##p)
#define MSGOBJECT_DW_END						MSGPACKET_OBJECT_END

#define MSGPACKET_DW_SUPER_BEGIN(c, p,s)	MSGPACKET_SUPER_BEGIN(DW_##c,DW_##p,DW_##s)
#define MSGPACKET_DW_SUPER_END				MSGPACKET_SUPER_END

#define SIZE_DW_MSG(p)							SIZE_MSG(DW_##p)

#define MSGPACKET_DW_CONN(p)				MSGPACKET_DW_BEGIN(CONNECTION,CONNECTION_##p)
#define MSGPACKET_DW_FRIEND(p)			MSGPACKET_DW_BEGIN(FRIEND,FRIEND_##p)
#define SIZE_DW_FRIEND(p)					SIZE_MSG(DW_FRIEND_##p)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_CONN(SERVER_INFO_CMD)
	SERVER_KEY	m_ServerKey;
	DWORD			m_dwServerGUID;
	char			m_szDBProxyIP[MAX_IP_LENGTH];
	WORD			m_DBProxyPort;
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(GETLIST_SYN)
	CHARGUID		m_CharGuid;
	WORD GetSize() { return SIZE_DW_FRIEND(GETLIST_SYN); }
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(GETLIST_ACK)
	CHARGUID				m_CharGuid;		
	BYTE					m_byFriendNum;	
	sFRIEND_INFO_DB	m_FriendInfos[MAX_FRIENDBLOCK_STAT_NUM];

	WORD GetSize()
	{
		return ( SIZE_DW_FRIEND(GETLIST_ACK) - ( sizeof(sFRIEND_INFO_DB) * (MAX_FRIENDBLOCK_STAT_NUM - m_byFriendNum) ) );
	}
MSGPACKET_DW_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(GETLIST_NAK)
	enum
	{
		ERR_DEFAULT
	};

	CHARGUID				m_CharGuid;
	BYTE					m_byResult;
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(ADD_SYN)
	CHARGUID				m_CharGuidFrom;								
	TCHAR					m_szCharNameTo[MAX_CHARNAME_LENGTH];
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(ADD_ACK)
	CHARGUID				m_CharGuidFrom;
	sFRIEND_INFO_DB	m_ToFriendInfo;
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(ADD_NAK)
	enum
	{
		ERR_DEFAULT		= 0,	
		NO_USER			= 1,	
		ALREADY_FRIEND	= 2,	
		BLOCKED_USER	,	
	};

	CHARGUID				m_CharGuidFrom;
	BYTE					m_byResult;
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(DEL_SYN	)
	CHARGUID		m_CharGuidFrom;	
	CHARGUID		m_CharGuidTo;		
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(DEL_ACK)
	CHARGUID		m_CharGuidFrom;	
	CHARGUID		m_CharGuidTo;		
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(DEL_NAK)
	enum
	{
		ERR_DEFAULT,		
		NOT_FRIEND_USER,	
	};

	CHARGUID				m_CharGuidFrom;
	BYTE					m_byResult;
MSGPACKET_DW_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(BLOCK_SYN)
	CHARGUID		m_CharGuidFrom;								
	TCHAR			m_szCharNameTo[MAX_CHARNAME_LENGTH];	
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(BLOCK_ACK)
	CHARGUID				m_CharGuidFrom;	
	sBLOCK_INFO_BASE	m_BlockInfo;
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(BLOCK_NAK)
	CHARGUID			m_CharGuidFrom;
	BYTE				m_byResult;
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(BLOCK_FREE_SYN)
	CHARGUID		m_CharGuidFrom;								
	TCHAR			m_szCharNameTo[MAX_CHARNAME_LENGTH];	
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(BLOCK_FREE_ACK)
	CHARGUID		m_CharGuidFrom;								
	TCHAR			m_szCharNameTo[MAX_CHARNAME_LENGTH];	
MSGPACKET_DW_END;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_DW_FRIEND(BLOCK_FREE_NAK)
	enum
	{
		ERR_DEFAULT		=0,	
		NO_USER			=1,	
		NOT_BLOCKED,	
	};

	CHARGUID		m_CharGuidFrom;
	BYTE			m_byResult;
MSGPACKET_DW_END;

#pragma pack(pop)


#endif //__PACKETSTRUCT_DBP_CHAT_H__


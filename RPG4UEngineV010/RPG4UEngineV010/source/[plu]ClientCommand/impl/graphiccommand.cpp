/*////////////////////////////////////////////////////////////////////////
文 件 名：GraphicCommand.cpp
创建日期：2006年5月16日
最后更新：2006年5月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GraphicCommand.h"
#include "HeroTipLayer.h"
#include "ScriptWord.h"
#include "V3DGameWorld.h"
#include "ApplicationSetting.h"
#include "VHero.h"
#include "VObjectManager.h"
#include "BaseSetting.h"
#include "V3DMaterialEffectManager.h"
#include "StdCharSkeleton.h"
#include "VCharacterAnimationCtrl.h"
#include "ConstTextRes.h"

using namespace vobject;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetBspSeek,0)
{
	//INT nChunk = atoi( words.GetWord( 1 ) );
	//FLOAT fDistance = nChunk * 128.0f * TILE_RES * SCALE_MESH;
	//g_V3DConfig.SetBspRenderDistance( fDistance ); 
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowGrass,0)
{
	int iCheck = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetRenderGrass( iCheck );
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowWater,0)
{
	int iCheck = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetRenderWater( iCheck );
}
END_GAMECOMMAND()




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetSkyBox,1)
{
	theApplicationSetting.m_EnableRenderSkyBox = atoi( words.GetWord( 1 ) );
	if( words.GetWordCount() >= 3 )
		theApplicationSetting.m_SkyBoxScale = (float)atof( words.GetWord( 2 ) ); 
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetTerrainLayerNum,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	V3DGameMap* pLevel = theGameWorld.GetHeroPlotMap();
	if( pLevel )
	{
		pLevel->GetTerrain()->SetMaxLayer( nParam );

	}

	OUTPUTCHATF (  "当前渲染地表层数 %d\n", nParam );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowObjects,1)
{
	int nParam = atoi( words.GetWord( 1 ) );

	g_V3DConfig.SetRenderObjs( nParam );

	BOOL bRender = 	g_V3DConfig.IsRenderObjs();
	OUTPUTCHATF ( " 显示Scence %s", bRender?"是":"否" );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowTerrain,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetRenderTerrain( nParam );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowWorld,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetRenderWorld( nParam );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(EnableShadow,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetRenderShadows( nParam );

	BOOL bRender = 	g_V3DConfig.IsRenderShadows();

	OUTPUTCHATF ( " 显示Shadow %s", bRender?"是":"否" );
}
END_GAMECOMMAND()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowFloorTree,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetShowModelFloorTree (nParam);
	OUTPUTCHATF(  "显示模型地板树 : %d\n", nParam );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(LoadMaterialEffect,1)
{
	theVHero.LoadMtlEffect( words.GetWord( 1 ) );
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowModelBBox,1)
{
	int nParam =  (words.GetNumber( 1 ) != 0);
	g_BaseSetting.m_ShowModelBBox = nParam;
	g_V3DConfig.SetRenderBBox( nParam );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowObjectShadow,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	theApplicationSetting.m_bUseObjectShadow = nParam;
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowObjectBox,1)
{
	int nParam = atoi( words.GetWord( 1 ) );
	g_V3DConfig.SetShowObjectBBox (nParam);
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(LoadMatlEffect,2)
{
	int nPartID = GetStdCharSkeleton().GetPartIdByPartName( words.GetWord(1) );
	if( nPartID == -1 )
		return FALSE;
	int nEffectID = theV3DMaterialEffectManager.AddEffect( words.GetWord( 2 ) );
	if( nEffectID == -1 )
		return FALSE;
	VCharacterAnimationCtrl* pAnim  = theVHero.GetAnim();
	if( pAnim->ApplyEquipMaterialEffect( nPartID, nEffectID ) )
	{
		OUTPUTCHAT(	"装载材质效果成功!!" );
	}
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ShowWireFrame,1)
{
	int nParam = words.GetNumber( 1 )  != 0;
	g_V3DConfig.SetAlwayseWireFrameRendering(nParam);
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetFarPlane,1)
{
	INT nFarPlane = atoi( words.GetWord(1) );
	theApplicationSetting.m_FarPlane = theGameWorld.ChangeFarPlane( nFarPlane );
	theApplicationSetting.m_FarFog   = theApplicationSetting.m_FarPlane *0.8f;

	//FLOAT fDistance = (nFarPlane) * 128.0f * TILE_RES * SCALE_MESH;
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(UseVB,1)
{
		INT nUseVB = atoi( words.GetWord(1) );
		g_V3DConfig.SetUseVBIB( nUseVB );
}
END_GAMECOMMAND()




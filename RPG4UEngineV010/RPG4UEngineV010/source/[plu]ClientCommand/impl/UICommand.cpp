/*////////////////////////////////////////////////////////////////////////
文 件 名：UICommand.cpp
创建日期：2006年5月16日
最后更新：2006年5月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "UICommand.h"
#include "HeroTipLayer.h"
#include "ScriptWord.h"
#include "BaseSetting.h"
#include "ApplicationSetting.h"
#include "MapViewManager.h"
#include "V3DGameWorld.h"
#include "VUCtrlManager.h"
#include "MathFormular.h"
#include "VHero.h"
#include "ObjectUtil.h"
#include "SkillSpecialEffectManager.h"
#include "ConstTextRes.h"

using namespace vobject;
using namespace util;
using namespace math;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(EnableUIRender,1)
{
	g_BaseSetting.m_EnableRenderUI	= (  words.GetNumber( 1 ) != 0 );
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetMiniMap,2)
{
	theApplicationSetting.m_MiniMapSight = atoi( words.GetWord( 1 ) );
	int nWinSize  = atoi( words.GetWord( 2 ) );
	if( nWinSize > 0 )
		theApplicationSetting.m_MiniMapWinSize = nWinSize;

	OUTPUTCHATF("设置小地图视野(%ld),渲染窗口大小(%ld,%ld)", 
				theApplicationSetting.m_MiniMapSight,
				theApplicationSetting.m_MiniMapWinSize,
				theApplicationSetting.m_MiniMapWinSize );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(AddMiniMark,3)
{
	mapinfo::sMAP_MARK mark;

	__CHECK(theGameWorld.GetHeroPlotMap());

	mark.x = (INT)(theGameWorld.GetHeroPlotMap()->GetTerrain()->GetMapWidth()*Random());
	mark.y = (INT)(theGameWorld.GetHeroPlotMap()->GetTerrain()->GetMapHeight()*Random());
	mark.dwBornTime = base::GetRunTime();
	mark.dwLife = 2000;

	if( words.GetWordCount() > 1 )
		mark.x = atoi( words.GetWord(1) );
	if( words.GetWordCount() > 2 )
		mark.y = atoi( words.GetWord(2) );
	if( words.GetWordCount() > 3 )
		mark.dwLife = atoi( words.GetWord(3 ) );


	mark.bFlicker = TRUE;

	theMapViewManager.AddMark( &mark );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(FindAtMiniMap,1)
{
	if( words.IsNumber( 1 ) )
	{
		theMapViewManager.MarkNpcPosition( atoi( words.GetWord(1) ) );
	}
	else
		theMapViewManager.MarkNpcPositionBy( words.GetWord( 1 ) );
}
END_GAMECOMMAND()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(FlyText,0)
{
		int xEffect = 0,yEffect = 0;
		ObjectUtil::World3DToScreen2D( theVHero.GetPosition(),xEffect,yEffect );
		g_pFlyTextEffect->Create	(sseffect::POPUPTEXT_MISS
                                 ,xEffect
											,yEffect
											,0);
}
END_GAMECOMMAND()




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ReloadUI,0)
{
	theUICtrlManager.ReloadUI();
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(UIShowDetail,1)
{
	g_BaseSetting.m_UIShowDetail = (words.GetNumber(1) != 0);
	OUTPUTCHATF("UIShowDetail: %d\n", g_BaseSetting.m_UIShowDetail);
}
END_GAMECOMMAND()




/*////////////////////////////////////////////////////////////////////////
文 件 名：PlayerCommand.cpp
创建日期：2006年5月16日
最后更新：2006年5月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PlayerCommand.h"
#include "HeroTipLayer.h"
#include "ScriptWord.h"
#include "SceneLogicFlow.h"
#include "Hero.h"
#include "ObjectManager.h"
#include "VHero.h"
#include "VObjectManager.h"
#include "VCharacterAnimationCtrl.h"
#include "VHeroActionInput.h"
#include "V3DGameWorld.h"
#include "MeshResManager.h"
#include "StdCharSkeleton.h"
#include "GameEffectManager.h"
#include "IGameEffect.h"
#include "Avatar.h"
#include "ConstTextRes.h"

using namespace vobject;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(GotoMap,3)
{
	int iMapID = -1;
		if(! words.IsNumber( 1 ) )
			return FALSE;

	iMapID	= atoi( words.GetWord( 1 ) );
	int x		= atoi( words.GetWord( 2 ) );//*MAP_SIZE_BYTILE;
	int y		= atoi( words.GetWord( 3 ) );//*MAP_SIZE_BYTILE;

	theSceneLogicFlow.GotoVillageScene((MAPCODE)iMapID,x,y);
}
END_GAMECOMMAND()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(LowerAnim,6)
{
	theVHero.GetAnim()->PlayLowerAnim	(words.GetWord(1)
                                       ,PLAYMODE_NORMAL
													,atoi( words.GetWord( 2 ) )
													,1
													,ANIMATION_TIMEOUT
													,atoi( words.GetWord( 3 ) )
													,words.GetWord(4)
													,PLAYMODE_NORMAL
													,atoi( words.GetWord( 5 ) ));
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(UpperAnim,4)
{
	theVHero.GetAnim()->PlayUpperAnim	(words.GetWord(1)
                                       ,PLAYMODE_NORMAL
													,atoi( words.GetWord( 2 ) )
													,1
													,ANIMATION_TIMEOUT
													,atoi( words.GetWord( 3 ) ));
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(Mount,3)
{
	if( atoi( words.GetWord( 1 ) ) == 0 )
	{
		theVHero.DisMount();
	}
	else
	{
		theVHero.MountMonster( atoi( words.GetWord( 2 ) ) );
	}
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(Dismount,0)
{
	theVHero.DisDrome();
	theVHero.DisMount();
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ChangeMoveSpeed, 1)
{
	VObject* pObject = theVObjectManager.GetMouseTargetObject(); 
	if(  pObject && pObject->IsCharacter() )
	{
		((VCharacter*)pObject)->SetRunStep( (float)words.GetFloat(1) );
	}
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetJumpSpeed,1)
{
	theVHero.SetStartJumpingSpeed(  (float)words.GetFloat( 1) );
}
END_GAMECOMMAND()





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(SetWalkHeight,1)
{
	//int nParam = atoi( words.GetWord( 1 ) );
	OUTPUTCHATF( "设置可行走 高度\n" );
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(AddItem,1)
{
	assert(0 && " BEGIN_GAMECOMMAND[AddItem ");
	#pragma message(__FILE__  "(373) BEGIN_GAMECOMMAND[AddItem  " )
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(AddPlayer,1)
{
//const DWORD REGEN_INTERVAL		= 5000;
const DWORD TILE_GEGEN_RANGE	= 6;

	Vector3D vPos = theHero.GetPosition();
	Vector3D vPos2;
	vPos2 = vPos;

	//BOOL	bNearHero;
	INT	nRegenX,
			nRegenY;
	INT	nCharType;
	INT	nSex;
	INT	nFace;
	INT	nHairColor;
	INT	nMaxHP;
	INT	nLevel;
	LPCSTR szName;

	szName		= words.GetWord(1);
	nRegenX		= words.GetNumber(2);
	nRegenY		= words.GetNumber(3);
	nCharType	= words.GetNumber(4);
	nSex			= words.GetNumber(5);
	nFace			= words.GetNumber(6);
	nHairColor	= words.GetNumber(7);
	nMaxHP		= words.GetNumber(8);
	nLevel		= words.GetNumber(8);

	if(nRegenX == 0 || nRegenY == 0)
	{
		vPos2.x = vPos.x + TILE_3DSIZE;
		vPos2.y = vPos.y + TILE_3DSIZE;
	}
	else
	{
		vPos2.x = nRegenX*TILE_3DSIZE;
		vPos2.y = nRegenY*TILE_3DSIZE;
	}

	vPos2.x	+= rand() % (int)(TILE_GEGEN_RANGE*TILE_3DSIZE) -TILE_GEGEN_RANGE/2*TILE_3DSIZE;
	vPos2.y	+= rand() % (int)(TILE_GEGEN_RANGE*TILE_3DSIZE) -TILE_GEGEN_RANGE/2*TILE_3DSIZE;

	sPLAYERINFO_RENDER playerInfo;
	__ZERO(playerInfo);

	playerInfo.m_wPlayerKey			= (WORD)theObjectManager.GenerateKeyAtSingleMode(NPC_OBJECT);
	playerInfo.m_byClass				= nCharType;
	playerInfo.m_bySex				= nSex;
	playerInfo.m_wMoveSpeedRatio	= 100;
	playerInfo.m_wAttSpeedRatio	= 100;
	playerInfo.m_byFace				= nFace;
	playerInfo.m_byHairColor		= (BYTE)nHairColor;
	playerInfo.m_byHeight			= 9;
	playerInfo.m_LV					= (LEVELTYPE)nLevel;
	playerInfo.m_HP					= (HPTYPE)nMaxHP;
	playerInfo.m_MaxHP				= (HPTYPE)nMaxHP;
	strcpy(playerInfo.m_szName,	(LPCSTR)szName);

	Player*	pPlayer		= (Player*)theObjectManager.Add(playerInfo.m_wPlayerKey
													, PLAYER_OBJECT
													, playerInfo.m_byClass);

	pPlayer->SetPosition(vPos2);
	pPlayer->SetShowName(TRUE);
	pPlayer->SetDirection(vPos);	 
	pPlayer->SetPlayerInfo( &playerInfo );

	//pPlayer->SetItemManager( &theTestingItemManager);


	//m_arLocations.push_back(SLocationInfo());
	//SLocationInfo& location = m_arLocations.back();
	//location.MonsterCode		= pPlayer->GetObjectKey();
	//location.vRegenPos.x		= vPos2.x;
	//location.vRegenPos.y		= vPos2.y;
	//location.vRegenPos.z		= 0;
	//location.bNearHero		= info.bNearHero;


	//if(info.dwStateCode )
	//{
	//	SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	
	//	pEffect->dwSkillID		= 0;
	//	pEffect->dwStatusID		= info.dwStateCode;	
	//	pEffect->iRemainTime		= info.dwStateDuration;
	//	pPlayer->AddSkillEffect(&pEffect);
	//}
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ReloadMesh,1)
{
	sMESH_RECORD* r = theMeshResManager.CreateMesh( words[1] );
	if( r )
	{
		theVHero.GetAnim()->ReloadMesh( r->nID );
	}
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ChangePart,1)
{
	int nPartID = GetStdCharSkeleton().GetPartIdByPartName( words[1] );
	sMESH_RECORD* r = theMeshResManager.CreateMesh( words[1] );
	if( r )
	{
		theVHero.GetAnim()->MountEquip( nPartID, r->nID );

		Avatar *pAvatar = theVHero.GetAnim()->GetAvatar();
		if(pAvatar)
			pAvatar->SetEquipFlag( avatar::AVATAR_LEFT_BACK | avatar::AVATAR_RIGHT_BACK );
	}
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ToggleFight,0)
{
	__CHECK( singleton::ExistVHero() );

	if( !theVHero.HasFightFlag(FIGHTFLAG_FIGHTING) )
	{
		theVHero.AddFightFlag(FIGHTFLAG_FIGHTING );
		Avatar *pAvatar = theVHero.GetAnim()->GetAvatar();
		if(pAvatar)
			pAvatar->SetEquipFlag( avatar::AVATAR_LEFT_HAND | avatar::AVATAR_RIGHT_HAND );
	}
	else
	{
		theVHero.ClearFightFlag(FIGHTFLAG_FIGHTING );
		Avatar *pAvatar = theVHero.GetAnim()->GetAvatar();
		if(pAvatar)
			pAvatar->SetEquipFlag( avatar::AVATAR_LEFT_BACK | avatar::AVATAR_RIGHT_BACK );
	}
}
END_GAMECOMMAND()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(DumpSkinInfo,0)
{
	VObject* pObject = theVObjectManager.GetMouseTargetObject(); 
	if(  pObject )
	{
		MeshConfigAvatarSettingArray* pSettings = &pObject->GetAnim()->GetMeshConfig()->m_AvatarSettingInfos;
		for( UINT i = 0; i < pSettings->m_AvatarInfos.size(); i++ )
		{
			MeshConfigAvatarSetting* s = pSettings->m_AvatarInfos[i];
			LOGINFO( "skin:%s", s->m_NameParam.GetString() );
		}
	}
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ChangeSkin,1)
{
	VObject* pObject = theVObjectManager.GetMouseTargetObject(); 
	if(  pObject )
	{
		pObject->GetAnim()->SetAvatarSetting( words.GetWord(1));
	}
}
END_GAMECOMMAND()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(ClearHeroEffect,0)
{
	//theVHero.GetCharEffectContainer()->ClearEffect( 0 );
}
END_GAMECOMMAND()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(PlayEffect,1)
{
	VObject* pTarget = theVObjectManager.GetMouseTargetObject();
	if(pTarget->IsCharacter())
		theVHero.PlayEffect	((char*)words.GetWord(1 )
                           ,(VCharacter*)pTarget)		;
}
END_GAMECOMMAND()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(CastEffect,1)
{
	IGameEffect* pEffect = theGameEffectManager.CreateEffect(GAMEEFFECT_MESH, TRUE);
	if(!pEffect)
		return FALSE;

	pEffect->LoadSetting	( FMSTR("data\\effect\\%s", words.GetWord(1))
								, TRUE );

	theVHero.CastModelEffect( pEffect );
}
END_GAMECOMMAND()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_GAMECOMMAND(LoadDaySetting,1)
{
	const char *szFile = words.GetWord(1);
	V3DGameMap* pLevle = theGameWorld.GetHeroPlotMap();
	if( pLevle )
	{
		theGameWorld.LoadDayNightInfo( (char*)szFile );
	}
}
END_GAMECOMMAND()






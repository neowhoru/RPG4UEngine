// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "GameCommandManager.h"
#include "LogSystem.h"
#include "ApplicationDefine.h"
#include "NormalCommand.h"
#include "PlayerCommand.h"
#include "StateCommand.h"
#include "UICommand.h"
#include "GraphicCommand.h"




#define REG_GAMECOMMAND(NAME)\
		static GameCommand##NAME s##GameCommand##NAME;\
		theGameCommandManager.RegisterCommand(&s##GameCommand##NAME);

/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_CLIENTCOMMAND_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{
	REG_GAMECOMMAND(Help);
	REG_GAMECOMMAND(Exit);

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	REG_GAMECOMMAND(EnableUIRender);
	REG_GAMECOMMAND(SetMiniMap);
	REG_GAMECOMMAND(AddMiniMark);
	REG_GAMECOMMAND(FindAtMiniMap);
	REG_GAMECOMMAND(FlyText);
	REG_GAMECOMMAND(ReloadUI);
	REG_GAMECOMMAND(UIShowDetail);


	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	REG_GAMECOMMAND(ShowDebugInfo);
	REG_GAMECOMMAND(SetClockFactor);
	REG_GAMECOMMAND(ShowCollision);
	REG_GAMECOMMAND(ShowRange);
	REG_GAMECOMMAND(EnableEffect);
	REG_GAMECOMMAND(EnableCameraCollision);
	REG_GAMECOMMAND(ShowFPS);

	REG_GAMECOMMAND(ShowWalk);
	REG_GAMECOMMAND(ShowMouseTile);

	REG_GAMECOMMAND(ShowSnow);
	REG_GAMECOMMAND(ShowFog);
	REG_GAMECOMMAND(ShowPlayer);
	REG_GAMECOMMAND(ShowHero);

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////

	REG_GAMECOMMAND(GotoMap);
	REG_GAMECOMMAND(LowerAnim);
	REG_GAMECOMMAND(UpperAnim);
	REG_GAMECOMMAND(Mount);
	REG_GAMECOMMAND(Dismount);
	REG_GAMECOMMAND(ChangeMoveSpeed);
	REG_GAMECOMMAND(SetJumpSpeed);

	REG_GAMECOMMAND(SetWalkHeight);


	REG_GAMECOMMAND(AddItem);
	REG_GAMECOMMAND(AddPlayer);
	REG_GAMECOMMAND(ReloadMesh);
	REG_GAMECOMMAND(ChangePart);
	REG_GAMECOMMAND(ToggleFight);

	REG_GAMECOMMAND(DumpSkinInfo);
	REG_GAMECOMMAND(ChangeSkin);

	REG_GAMECOMMAND(ClearHeroEffect);
	REG_GAMECOMMAND(PlayEffect);
	REG_GAMECOMMAND(CastEffect);
	REG_GAMECOMMAND(LoadDaySetting);

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	REG_GAMECOMMAND(SetBspSeek);
	REG_GAMECOMMAND(ShowGrass);
	REG_GAMECOMMAND(ShowWater);
	REG_GAMECOMMAND(SetSkyBox);
	REG_GAMECOMMAND(SetTerrainLayerNum);

	REG_GAMECOMMAND(ShowObjects);
	REG_GAMECOMMAND(ShowTerrain);
	REG_GAMECOMMAND(ShowWorld);
	REG_GAMECOMMAND(EnableShadow);
	REG_GAMECOMMAND(ShowFloorTree);
	REG_GAMECOMMAND(LoadMaterialEffect);



	REG_GAMECOMMAND(ShowModelBBox);
	REG_GAMECOMMAND(ShowObjectShadow);
	REG_GAMECOMMAND(ShowObjectBox);


	REG_GAMECOMMAND(LoadMatlEffect);
	REG_GAMECOMMAND(ShowWireFrame);
	REG_GAMECOMMAND(SetFarPlane);
	REG_GAMECOMMAND(UseVB);


	LOGINFO("Plugin ClientCommand insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_CLIENTCOMMAND_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	return gamemain::ePluginOK;
}

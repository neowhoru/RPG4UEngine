/*////////////////////////////////////////////////////////////////////////
文 件 名：NPC.cpp
创建日期：2008年3月28日
最后更新：2008年3月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Protocol_ClientGameS.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS.h"
#include "varpacket.h"
#include "NPC.h"
#include <NPCInfoParser.h>
#include "AppearanceManager.h"
#include "SoundEffect.h"
#include "GameParameter.h"
#include "SkillInfoParser.h"
#include "StateInfoParser.h"
#include "Hero.h"
#include "CharSoundInfoParser.h"
#include "WeaponSoundInfoParser.h"
#include "MonsterVoiceInfoParser.h"
#include "TextResManager.h"
#include "MathLib.h"
#include "BattleUtil.h"
#include "ObjectUtil.h"
#include "AIInfoParser.h"
#include "ConstCharacter.h"
#include "ConstTextRes.h"

namespace object
{ 
const DWORD STAND_ANIMATION_MINIMUM_DELAY		= 4500;
const DWORD STAND_ANIMATION_DELAY_VARIATION	= 6500;
const DWORD ATTACK_ADDITIONAL_EFFECTs			= ATTACK_ADDITIONAL_EFFECT_KNOCKBACK | ATTACK_ADDITIONAL_EFFECT_DOWN | ATTACK_ADDITIONAL_EFFECT_AIR;
const float RANGED_THRESHOLD						= 7.0f;

#define MONSTER_VOICE_RANGE		(theGeneralGameParam.GetDistanceVoiceRange() * theGeneralGameParam.GetDistanceVoiceRange())

//------------------------------------------------------------------------------
NPC::NPC( void )
{
	SetObjectType(NPC_OBJECT);

	m_fMoveSpeed				= 0.001f;
	m_fAttackSpeedModifier	= 1.0f;
	m_fMoveSpeedModifier		= 1.0f;
	m_DelaySkillSerial.clear();
	m_pVNPCInfo					= NULL;
	m_bEnableIdleSound		= TRUE;
}

//------------------------------------------------------------------------------
NPC::~NPC( void )
{
}

//------------------------------------------------------------------------------
BOOL NPC::Create( DWORD dwObjectKey, DWORD dwParam )
{
	m_ObjectCookie.SetObjectKey( dwObjectKey );
	//-----------------------------------------------------------------------	
	// 关联怪物信息
	//-----------------------------------------------------------------------
	SetMonsterInfo( theNPCInfoParser.GetMonsterInfo( dwParam ) );

	APPEARANCEINFO appearInfo;
	ZeroMemory(&appearInfo,sizeof(appearInfo));
	appearInfo.ObjectType		= APPEARANCEINFO::NPC;
	appearInfo.pMonsterInfo		= GetMonsterInfo();
	appearInfo.pVMonsterInfo	= GetVMonsterInfo();

	theAppearanceManager.SetAppearance(&appearInfo,this,TRUE,FALSE);

	BOOL rt;
	rt = Character::Create(dwObjectKey, dwParam);
	__UNUSED(rt);
	ASSERT(rt);
	//	初设为( IDLE 状态 )
	InitState(STATE_IDLE,g_CurTime);


	//LPCSTR szName = _STRING(appearInfo.pMonsterInfo->m_NCode);
	SetName( (LPCTSTR)appearInfo.pMonsterInfo->m_NpcName );

	m_dwSummonMasterID	=0;
	m_iAlarmVoice			= 0;
	m_iWaitSoundDelay		= 0;


	return TRUE;
}

//------------------------------------------------------------------------------
void NPC::SetSummonMasterID(const DWORD & dwID)
{
	m_dwSummonMasterID=dwID;

	//if(theHero.GetObjectKey()==m_dwSummonMasterID)
	//{
	//	SummonTargetDialog * pSummonTargetDlg = (SummonTargetDialog * )g_InterfaceManager.GetDialog(InterfaceManager::DIALOG_SUMMON_TARGET);

	//	if (pSummonTargetDlg)
	//	{
	//		pSummonTargetDlg->SetTargetKey( GetObjectKey() );
	//		pSummonTargetDlg->ShowWindow(TRUE);

	//	}

	//	SummonSkillContainer * pSummonSkillContainer = (SummonSkillContainer * )g_InterfaceManager.GetDialog(InterfaceManager::DIALOG_SUMMON_SKILL_CONTAINER);
	//	
	//	if(pSummonSkillContainer)
	//	{
	//		pSummonSkillContainer->SetTargetKey( GetObjectKey() );
	//		pSummonSkillContainer->ShowWindow(TRUE);

	//		//SkillSlot skillSlot;
	//		//skillSlot.SetCode(154);

	//		sNPCINFO_BASE *pInfo = theNPCInfoParser.GetMonsterInfo(m_NPCInfo.m_MonsterCode);
	//		
	//		if(pInfo)
	//		{
	//			for(int i=0;i<3;i++)
	//			{
	//				SkillPaneSlot skillSlot;
	//				
	//				if(pInfo->m_wSkillCode[i]!=0)
	//				{
	//					skillSlot.SetSlotType(ST_SKILL);
	//					skillSlot.SetCode(pInfo->m_wSkillCode[i]);

	//					pSummonSkillContainer->InsertSlot(i,skillSlot);
	//				}
	//			}
	//		}
	//	}
	//}
}

//------------------------------------------------------------------------------
void NPC::Destroy( void )
{
	//if(m_dwSummonMasterID)
	//{
	//	if(g_pHero&&theHero.GetObjectKey()==m_dwSummonMasterID)
	//	{
	//		theHero.RemoveSummonId(this->GetObjectKey());

	//		
	//		if(theHero.GetSummonId()==INVALID_DWORD_ID)
	//		{
	//			SummonTargetDialog * pSummonTargetDlg = (SummonTargetDialog * )g_InterfaceManager.GetDialog(InterfaceManager::DIALOG_SUMMON_TARGET);
	//			if( pSummonTargetDlg )
	//			{
	//				pSummonTargetDlg->SetTargetKey( 0 );
	//				pSummonTargetDlg->ShowWindow(FALSE);
	//			}

	//			SummonSkillContainer * pSummonSkillContainer = (SummonSkillContainer * )g_InterfaceManager.GetDialog(InterfaceManager::DIALOG_SUMMON_SKILL_CONTAINER);

	//			if(pSummonSkillContainer)
	//			{
	//				pSummonSkillContainer->SetTargetKey( 0 );
	//				pSummonSkillContainer->ShowWindow(FALSE);
	//				pSummonSkillContainer->Clear();
	//			}
	//		}
	//	}
	//}

	//------------------------------------------
	Character::Destroy();
	m_DelaySkillSerial.clear();
}
 


//------------------------------------------------------------------------------
void NPC::SetMonsterInfo(sNPCINFO_BASE *pInfo)
{
	assert(pInfo);

	if (pInfo) 
	{
		m_NPCInfo		= *pInfo;
		m_pVNPCInfo		= theNPCInfoParser.GetVMonsterInfo(pInfo->m_vMonsterCode);
		assert(m_pVNPCInfo);


		if(m_pVNPCInfo->m_dwIdleSoundPeriod == 0)
			m_bEnableIdleSound = FALSE;
		else
		{
			m_bEnableIdleSound	= TRUE;
			m_iWaitSoundDelay		= m_pVNPCInfo->m_dwIdleSoundPeriod;
		}
	}
}


//------------------------------------------------------------------------------
void NPC::SetCondition(BYTE byCondition)
{
	Character::SetCondition(byCondition);
	
	//switch(byCondition)
	//{
	//case CHAR_CONDITION_HELP:
	//	{
	//	}
	//	break;
	//}
}


DWORD	NPC::GetCharSoundCode()
{
	sVNPCINFO_BASE* pVNpc;
	pVNpc = theNPCInfoParser.GetVMonsterInfo(m_NPCInfo.m_vMonsterCode);
	assert(pVNpc);
	return pVNpc->m_wSoundTempl;
}


//------------------------------------------------------------------------------
BOOL NPC::Process(DWORD dwTick)
{
	
	PROFILE_PROC_BEGIN(_T("ProcessMonster1"));
   ProcessVoice(dwTick);
	ProcessStandAnimationDelay(dwTick);


	BOOL bRet = Character::Process(dwTick);
	PROFILE_PROC_END(_T("ProcessMonster1"));
	

	return bRet;
}



void NPC::SetNpcFunc(eNPC_FUNC_TYPE /*funcType*/)
{
}

eNPC_FUNC_TYPE	NPC::GetNpcFunc()
{
	return NPC_FUNC_MONSTER;
}
//------------------------------------------------------------------------------
void NPC::SetName(const char * pszName)
{
	m_NPCInfo.m_NpcName	= _VI(pszName);
	//m_strObjName			= m_NPCInfo.m_NpcName;
	_SUPER::SetName(pszName);
}


//------------------------------------------------------------------------------
void  NPC::ChangeHPRatio(float fRatio)
{
	sNPCINFO_BASE *pInfo;
	
	pInfo = theNPCInfoParser.GetMonsterInfo(m_NPCInfo.m_MonsterCode);
	__CHECK2_PTR(pInfo,);
	ASSERT(pInfo);

	DWORD dwMaxHP = (DWORD)( pInfo->m_dwMaxHP * fRatio );		
	BOOL bChangeHP = FALSE;;
	if (m_dwHP > 0) 
	{
		bChangeHP = TRUE;
	}

	m_NPCInfo.m_dwMaxHP = dwMaxHP;

	if (bChangeHP)
	{
		SetHP(dwMaxHP);
	}
}




void NPC::SetStandAnimationDelay()
{
	m_dwStandAnimDelay	= STAND_ANIMATION_MINIMUM_DELAY
								+ rand() % STAND_ANIMATION_DELAY_VARIATION;
}

void NPC::DisplayAIInfo(DWORD dwAIType, DWORD dwIndex, DWORD dwIndex2)
{
	DWORD*	pAttitudes	= NULL;
	DWORD*	pMoves		= NULL;

	if(dwIndex >= MAX_AI_STATE || dwIndex2 >= MAX_AI_STATE)
		return;

	if(!theAIInfoParser.GetInfo(m_NPCInfo.m_AIInfoCode
                              ,dwAIType
										,m_NPCInfo.m_wAttitude
										,m_NPCInfo.m_wMoveAttitude
										,pAttitudes
										,pMoves	)		)
		return;

	if(pAttitudes[dwIndex] == 0 && pMoves[dwIndex2] == 0)
		return;

	//////////////////////////////////////////////

	LPCTSTR			szFormat = _STRING(TEXTRES_EXTRA_AIINFO);
	StringHandle	sText;
	LPCTSTR			szText1 = _STRING(pAttitudes[dwIndex]);
	StringHandle	sText1;

	if(szText1[0])
		sText1.Format(_T("(%s)\n"), szText1);

	sText.Format(szFormat
					,(LPCTSTR)sText1
					,_STRING(pMoves[dwIndex2]));
	//////////////////////////////////////////////
	DisplayChatMessage(sText);
}


void NPC::ProcessStandAnimationDelay(DWORD dwTick)
{
	if (m_dwStandAnimDelay < dwTick)
	{
		m_dwStandAnimDelay = 0;

	}
	else {
		m_dwStandAnimDelay -= dwTick;
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void NPC::OnDamage	(DWORD                dwAdditionalEffect
                        ,BOOL                 bRight
								,eWEAPONSOUNDKIND     eSoundKind
								,eARMOUR_TEX_TYPE		 eArmourTex
								,DWORD                dwSkillCode)
{
	Character::OnDamage	(dwAdditionalEffect
                        ,bRight
								,eSoundKind
								,eArmourTex
								,dwSkillCode);

	SetAttackStandTime(ATTACK_STAND_TIME);


	if (dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECT_TOGROUND)
	{
		DownToGround();
	}

	if(util::DrawLots(m_pVNPCInfo->m_byPlayHurtVoiceRate))
		PlayHurtVoice();
	else	if(util::DrawLots(theGeneralGameParam.GetPlayDamageSoundRate()) )
	{
		PlayDamageSound(eSoundKind,eArmourTex);
	}

	CreateDamageEffect	(dwAdditionalEffect & (ATTACK_ADDITIONAL_EFFECT_CRITICAL)
                        ,bRight)		;

	if ( !(dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECTs) ) 
	{
		switch (GetCurrentState())
		{
		case STATE_SKILL:
		case STATE_DEATH:
		case STATE_DOWN:
			break;

		case STATE_ATTACK:
			{
				if (!(rand() % 5))
					PlayDamageAnimation(bRight);
			}
			break;

		default:
			{				 				
				if (!(rand() % 3))
					PlayDamageAnimation(bRight);
			}
			break;
		}							
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
eWEAPONSOUNDKIND	NPC::GetWeaponSoundKind()
{
	sVNPCINFO_BASE* pVInfo;
	pVInfo = theNPCInfoParser.GetVMonsterInfo(GetMonsterInfo()->m_vMonsterCode);
	assert(pVInfo);

	return (eWEAPONSOUNDKIND)pVInfo->m_byWeaponSound;
}


eARMOUR_TEX_TYPE	NPC::GetArmourTexture()
{
	sVNPCINFO_BASE* pVInfo;
	pVInfo = theNPCInfoParser.GetVMonsterInfo(GetMonsterInfo()->m_vMonsterCode);
	assert(pVInfo);

	return (eARMOUR_TEX_TYPE)pVInfo->m_byArmourTexture;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CODETYPE NPC::GetCurrentVSkill()
{
	if(m_CurrentAction.ActionID == ACTION_ATTACK)
	{
		return 	m_pVNPCInfo->m_SkillCode[GetAttackSequence()];	
	}
	return _SUPER::GetCurrentVSkill();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD NPC::GetAttackSpeed()const
{
	if(m_CurrentAction.ActionID == ACTION_ATTACK)
	{
		DWORD dwActionDelay;
		//DWORD dwPhysicalAttackSpeed;

		//GetAttSpeedRatio
		if( m_fAttackSpeedModifier != 0.f  )
			dwActionDelay = (DWORD)(m_NPCInfo.m_wAttSpeed[GetAttackSequence()] / m_fAttackSpeedModifier);
		else
			dwActionDelay = 1000;

		return dwActionDelay;
	}
	assert(0 && "未处理...");
	return INVALID_DWORD_ID;
}


float NPC::GetAttackSpeedModifier()const
{
	return m_fAttackSpeedModifier;

}
float NPC::GetMoveSpeedModifier()const
{
	return m_fMoveSpeedModifier;

}

void NPC::UpdateSkillAttributes()
{
	//m_fAttackSpeedModifier = 1.0f;
	//m_fMoveSpeedModifier   = 1.0f;

	//SKILL_EFFECT_LIST_IT iter;
	//iter = m_SkillEffectList.begin();

	//while (iter != m_SkillEffectList.end())
	//{
	//	SKILL_EFFECT *pEffect = (*iter);

	//	SkillDetailInfo *pInfo = theSkillInfoParser.GetSkillInfo(pEffect->dwSkillID);

	//	DWORD dwAbilityID = pEffect->dwAbilityID;

	//	if (pInfo)
	//	{
	//		sABILITYINFO_BASE *pAbilityInfo = pInfo->GetAbilityInfo(dwAbilityID);
	//		if (pAbilityInfo)
	//		{
	//			eATTRIBUTE_TYPE eType = pAbilityInfo->GetAttrType();
	//			
	//			if( eType == ATTRIBUTE_PHYSICAL_ATTACK_SPEED )
	//			{
	//				float fRate = (pAbilityInfo->m_iParam[1] / 10.0f);
	//				m_fAttackSpeedModifier *= fRate;
	//			}
	//			else if (eType == ATTRIBUTE_MOVE_SPEED)
	//			{
	//				float fRate = (pAbilityInfo->m_iParam[1] / 10.0f);
	//				m_fMoveSpeedModifier *= fRate;
	//			}
	//		}
	//	}
	//	
	//	iter++;
	//}
}


/*////////////////////////////////////////////////////////////////////////
远程攻击
/*////////////////////////////////////////////////////////////////////////
BOOL NPC::IsNormalRangedAttack()
{
	if (GetMonsterInfo()->m_fAttRange >= RANGED_THRESHOLD)
	{
		return TRUE;
	}
	else
	{
		return FALSE;
	}
}


int   NPC::AddSkillEffect(SKILL_EFFECT *pEffect)
{
	int iID = Character::AddSkillEffect(pEffect);

	UpdateSkillAttributes();
	return iID;

}

void  NPC::DeleteSkillEffect(int iEffectID)
{
	Character::DeleteSkillEffect(iEffectID);

	UpdateSkillAttributes();


	return;
}


float NPC::GetMeshHeight(float fExtra)
{
	if(m_fMeshHeight == 0.f)
		m_fMeshHeight = _SUPER::GetMeshHeight(fExtra);
	return m_fMeshHeight;
}


void NPC::SetAttackSpeedModifier(float fAttSpeed)
{
	m_fAttackSpeedModifier = fAttSpeed;
}

void NPC::SetMaxHP( DWORD iHP )
{
	m_NPCInfo.m_dwMaxHP = iHP;
	Character::SetMaxHP(iHP);
}

void NPC::SetMaxMP( DWORD iMP )
{
	m_NPCInfo.m_dwMaxMP = iMP;
    Character::SetMaxHP(iMP);
}

void NPC::SetAttackSpeedRatio(int iSpeed)
{
   SetAttackSpeedModifier( iSpeed /100.f );				
   Character::SetAttackSpeedRatio(iSpeed);
}

void NPC::SetMoveSpeedRatio(int iSpeed)
{	
   SetMoveSpeedModifier( iSpeed /100.f );
   Character::SetMoveSpeedRatio(iSpeed);
}






void     NPC::AddDelaySkillSerial(DWORD dwDelaySkill)
{
	m_DelaySkillSerial.push_back(dwDelaySkill);

}

DWORD    NPC::GetDelaySkillSerialCount()
{

	return m_DelaySkillSerial.size();

}


DWORD    NPC::GetDelaySkillSerial()
{
	if (m_DelaySkillSerial.size()) 
	{
		DWORD ret = m_DelaySkillSerial.front();
		m_DelaySkillSerial.pop_front();
		return ret;
	}

	return 0;

}





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	NPC::PlayIdleVoice()
{
	if(	m_NPCInfo.m_AIInfoCode 
		&& m_NPCInfo.m_AIInfoRate
		&& theGeneralGameParam.IsEnableNetwork())
		return;

	ObjectUtil::ProcessMonsterVoice(this
                               ,offsetof(sMONSTER_VOICEINFO,m_Idle)
										 ,m_iVoiceHandle);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	NPC::PlayBattleVoice()
{
	if(	m_NPCInfo.m_AIInfoCode 
		&& m_NPCInfo.m_AIInfoRate
		&& theGeneralGameParam.IsEnableNetwork())
		return;
	ObjectUtil::ProcessMonsterVoice	(this
											,offsetof(sMONSTER_VOICEINFO,m_Battle)
											,m_iVoiceHandle);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	NPC::PlaySkillVoice()
{
	if(	m_NPCInfo.m_AIInfoCode 
		&& m_NPCInfo.m_AIInfoRate
		&& theGeneralGameParam.IsEnableNetwork())
		return;
	ObjectUtil::ProcessMonsterVoice	(this
											,offsetof(sMONSTER_VOICEINFO,m_Skill)
											,m_iVoiceHandle);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	NPC::PlayHurtVoice()
{
	if(	m_NPCInfo.m_AIInfoCode 
		&& m_NPCInfo.m_AIInfoRate
		&& theGeneralGameParam.IsEnableNetwork())
		return;
	ObjectUtil::ProcessMonsterVoice	(this
											,offsetof(sMONSTER_VOICEINFO,m_Hurt)
											,m_iVoiceHandle);

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	NPC::PlayDieVoice()
{
	if(	m_NPCInfo.m_AIInfoCode 
		&& m_NPCInfo.m_AIInfoRate
		&& theGeneralGameParam.IsEnableNetwork())
		return;
	ObjectUtil::ProcessMonsterVoice	(this
											,offsetof(sMONSTER_VOICEINFO,m_Die)
											,m_iVoiceHandle);

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	NPC::PlayStateVoice()
{
	if(	m_NPCInfo.m_AIInfoCode 
		&& m_NPCInfo.m_AIInfoRate
		&& theGeneralGameParam.IsEnableNetwork())
		return;
	ObjectUtil::ProcessMonsterVoice	(this
											,offsetof(sMONSTER_VOICEINFO,m_State)
											,m_iVoiceHandle);

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void  NPC::OnUseSkill()
{
	PlaySkillVoice();
	Character::OnUseSkill();
}

void NPC::OnDead()
{
	if(util::DrawLots(m_pVNPCInfo->m_byPlayDieVoiceRate))
		PlayDieVoice();

	//DestoryEffect();
	Character::OnDead();

}


//----------------------------------------------------------------------------
BOOL NPC::CanUseSkill(DWORD /*skillID*/,BOOL /*bShowFailMessage*/)
{
	return TRUE;
#ifdef NO_DISABLE
	BOOL bCheck = FALSE;

	__CHECK(CanUseSkillLimitClassDefine(skillID, bShowFailMessage));

	__CHECK(CanUseSkillLimitMP(skillID, bShowFailMessage));

	__CHECK(CanUseSkillLimitHP(skillID, bShowFailMessage));

	__CHECK(CanUseSkillLimitCoolTime(skillID, bShowFailMessage));

	return TRUE;
#endif
}
//----------------------------------------------------------------------------
BOOL NPC::CanUseSkillLimitClassDefine(DWORD skillID,BOOL /*bShowFailMessage*/ /*= FALSE*/)
{

	sSKILLINFO_BASE *	pInfo;
	sNPCINFO_BASE *	pNpcInfo;

	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillID);
	__CHECK_PTR(pInfo);

	pNpcInfo=GetMonsterInfo();
	__CHECK_PTR(pNpcInfo);

	__CHECK(pInfo->m_dwClassDefine == pNpcInfo->m_MonsterCode);

	return TRUE;
}

void NPC::OnStartRun()
{
	if(util::DrawLots(m_pVNPCInfo->m_byPlayBattleVoiceRate) )
	{
		PlayBattleVoice();
		//m_iAlarmVoice = 1;
	}
	Character::OnStartRun();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void NPC::ProcessVoice(DWORD dwTick)
{
	if(!m_bEnableIdleSound)
		return;

	m_iWaitSoundDelay -= dwTick;

	if (m_iWaitSoundDelay < 0)
		m_iWaitSoundDelay = 0;

	if (m_iWaitSoundDelay)
		return;

	///////////////////////////////////////////
	switch (GetCurrentState())
	{
	case STATE_IDLE:
			break;

	case STATE_MOVE:
		{			
			if (GetMoveState() == MOVETYPE_RUN)
				return;
			break;
		}

	default:
			return;
	}
	
	sMONSTER_VOICEINFO *pInfo;

	//////////////////////////////////////////////
	__CHECK2(singleton::ExistHero(),);

	pInfo = theMonsterVoiceInfoParser.GetVoiceInfo(m_NPCInfo.m_MonsterCode);
	__CHECK2_PTR(pInfo,);


	////////////////////////////////////////////////////
	float fSightRange = GetMonsterInfo()->m_fViewRange;//30.0f;
	
	if ( GetRangeWithHero() <= fSightRange * fSightRange )
	{
		if (m_iWaitSoundDelay == 0)
		{
			if(util::DrawLots(m_pVNPCInfo->m_byPlayIdleVoiceRate))
				PlayIdleVoice();
			else
				PlayIdleSound();

			m_iWaitSoundDelay	=	(DWORD)((float)m_pVNPCInfo->m_dwIdleSoundPeriod * 0.7f);
			m_iWaitSoundDelay	+= (DWORD)math::RandomUnit() *( m_pVNPCInfo->m_dwIdleSoundPeriod/2);
		}
	}
}




DWORD	NPC::GetInfoID()const
{
	return GetMonsterInfo()->m_MonsterCode;
}

eATTACK_KIND	NPC::GetAttackKind	(eATTACK_KIND /*defaultKind*/) const
{
	return (eATTACK_KIND)GetMonsterInfo()->m_bySeries;
}

};//namespace object

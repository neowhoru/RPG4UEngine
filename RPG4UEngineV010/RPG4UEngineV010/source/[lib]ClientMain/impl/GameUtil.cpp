/*////////////////////////////////////////////////////////////////////////
文 件 名：GameUtil.cpp
创建日期：2008年2月13日
最后更新：2008年2月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameUtil.h"
#include "PacketInclude.h"
#include "WeaponSoundInfoParser.h"
#include "TileWorldClient.h"
#include "Object.h"
#include "character.h"
#include "Hero.h"
#include "ObjectManager.h"
#include "Monster.h"
#include "HeroActionInput.h"
#include "MouseHandler.h"
#include "mapObject.h"
#include "VHeroActionInput.h"
#include "VObject.h"
#include "PartyManager.h"
#include "CursorHandler.h"
#include "VUICursorManager.h"
#include "ScriptWord.h"
#include "GameCommandManager.h"
#include "SkillEffectManager.h"
#include "VHeroInputLayer.h"


using namespace math;
using namespace cursor;

Vector3D GameUtil::GetSidePos(Character *pTarget,float fAngle)
{
	Vector3D vTarget	= pTarget->GetPosition();
	Vector3D vPos		= theHero.GetPosition();
	Vector3D vDir		= (vPos - vTarget);

	vDir.z = 0.0f;
	vDir.Normalize();
	

	Vector3D vNewDir;
	Vector3D vNewPos;
	int		iNearTile[3];
	int		iTile;
	int		iNearCount;

	vNewDir.x = vDir.x * cos(fAngle) + vDir.y * -sin(fAngle);
	vNewDir.y = vDir.x * sin(fAngle) + vDir.y * cos(fAngle);
	vNewDir.z = 0.0f;


	////////////////////////////////////////
	vNewPos = vTarget + vNewDir;

	theTileMap.PathHandleReset(g_pPathExplorer,vNewPos);

	iTile = pTarget->GetPathExplorer()->GetTile();

	/////////////////////
	ZeroMemory(iNearTile,sizeof(iNearTile));
	iNearCount = theTileMap.GetNearNodes(iTile,iNearTile,3);

	if (g_pPathExplorer->GetTile() == iTile)
	{		
		return vNewPos;
	}


	////////////////////////////////////////
	if (iNearCount > 0)
	{
		for (int a = 0; a < iNearCount; a++)
		{
			if (iNearTile[a] == iTile) 
			{
				if ( !(theTileMap.GetAttribute(iNearTile[a]) & PTA_NO_WALK) )
					return vNewPos;
			}
		}
	}

	return vTarget;
}

//////////////////////////////////////////////////////////////////
Vector3D GameUtil::FindAttackTargetPos(Character *pTarget)
{

	int	iNumber;
	int	iTargetCount;


	iNumber			= thePartyManager.GetPartyNumber();
	iTargetCount	= pTarget->GetTargetingCount();

	/// 怪物没攻击者
	if (iNumber < 2 ||  iTargetCount == 0) 
		return pTarget->GetPosition();

	/// 怪物有多个攻击者，则按队伍位置，调整自己的位置
	int index = thePartyManager.GetPartyIndex(theHero.GetObjectKey());

	/////////////////////////////////////////////
	float fAngle = math::cPI_V3;
	switch(index)
	{
	case 0:	return pTarget->GetPosition();

	case 2:	fAngle = -math::cPI_V3;		break;
	case 3:	fAngle = math::cPI_V5;		break;
	case 4:	fAngle = -math::cPI_V5;		break;
	}

	return GetSidePos(pTarget,fAngle);				
}


//------------------------------------------------------------------------------
Character* GameUtil::GetPickedCharacter(Ray * /*pRay*/,DWORD /*dwPickingOption*/)
{
	VObject* pSelObject = theVObjectManager.GetMouseTargetObject();
	if(pSelObject == NULL)
		return NULL;
	if(!pSelObject->IsCharacter())
		return NULL;

	return  (Character*)pSelObject->GetObject();


}

//------------------------------------------------------------------------------
Item * GameUtil::GetPickedItem(Ray* /*pRay*/)
{
	VObject* pSelObject = theVObjectManager.GetMouseTargetObject();
	if(pSelObject == NULL)
		return NULL;
	if(!pSelObject->IsItem())
		return NULL;

	return  (Item*)pSelObject->GetObject();
//

}


//----------------------------------------------------------------------------
MapObject *	GameUtil::GetPickedMapUnitObject(Ray* /*pRay*/)
{
	VObject* pSelObject = theVObjectManager.GetMouseTargetObject();
	if(pSelObject == NULL)
		return NULL;
	if(!pSelObject->IsMapObject())
		return NULL;

	return  (MapObject*)pSelObject->GetObject();


}


//------------------------------------------------------------------------------
BOOL GameUtil::GetClickedPosition(Vector3D* OUT pPosition)
{
	__CHECK_PTR(pPosition);

	//POINT		ptMouse;

	//GetCursorPos( &ptMouse );
	//ScreenToClient( g_hWndMain, &ptMouse );

	if(theVHeroInputLayer.PickFromTerrain(*pPosition))
	{
		return TRUE;
	}
	return FALSE;

}



//------------------------------------------------------------------------------
eRELATION_TYPE GameUtil::GetRelationOfHero(Character* pObject)
{
	ePLAYER_BEHAVE_STATE behaveState;
	Player *					pPlayer;
	Monster *				pMonster;


	////////////////////////////////
	if (pObject->IsMapNpc())
		return RELATION_ASSISTANT;

	////////////////////////////////
	else if (pObject->IsKindOfObject( MERCHANT_OBJECT ))
		return RELATION_VENDOR;

	////////////////////////////////
	else if (pObject->IsPlayer())
	{
		pPlayer = (Player *)pObject;

		behaveState = pPlayer->GetBehaveState();

		if (behaveState == PLAYER_BEHAVE_VENDOR_ESTABLISHER)
			return RELATION_VENDOR;

		///////////////////////////
		if ( !g_bPkMode )
		{
			if(pPlayer->IsHero())
				return RELATION_FRIEND;

			else if ( 0 == theHero.GetTeam() )
				return RELATION_ENEMY;

			else if (	theHero.GetTeam() > 0
					&&		pPlayer->GetTeam() != theHero.GetTeam() )
				return RELATION_ENEMY;

			return RELATION_FRIEND;
		}
		else
		{
			if(pPlayer->IsHero())
				return RELATION_FRIEND;
			return RELATION_ENEMY;
		}
	}

	////////////////////////////////////////////
	else if (pObject->IsMonster())
	{
		pMonster = (Monster *)pObject;

		/////////////////////////
		if(pMonster->GetSummonMasterID() == 0)
			return RELATION_ENEMY;

		pPlayer = (Player *)theObjectManager.GetObject(pMonster->GetSummonMasterID());

		if(!pPlayer || !pPlayer->IsPlayer())
			return RELATION_ENEMY;

		return GetRelationOfHero(pPlayer);
	}
	return RELATION_NONE;



}

//------------------------------------------------------------------------------
BOOL GameUtil::IsMove(Vector3D vPos,Vector3D vDest,BOOL bCheckMakePath)
{
	TILEINDEX iTileFocussing;

	__VERIFY_PTR(g_pPathExplorer, "g_pPathExplorer is NULL");

	g_pPathExplorer->Stop();

	theTileMap.PathHandleReset(g_pPathExplorer,vPos);	

	iTileFocussing = theTileMap.GetTileToStand( vDest, NULL, -1.0f, 100.0f );
	__CHECK(iTileFocussing >= 0);

	__CHECK(!(theTileMap.GetAttribute(iTileFocussing) & (PTA_ONLY_JUMP | PTA_NO_WALK)) );

	if(bCheckMakePath)
	{
		WORD wAttr = PTA_ONLY_JUMP | PTA_NO_WALK;

		__CHECK(theTileMap.PathHandleFindPath	(g_pPathExplorer
														,vDest
														,iTileFocussing
														,wAttr
														,theGeneralGameParam.GetDistanceSeekRange() ));
	}

	return TRUE;
}




/////////////////////////////////////////////////////////////////
BOOL GameUtil::UpdateCursorForTarget(Object * pObject)
{
	__CHECK(singleton::ExistHero());
	__CHECK(!theMouseHandler.IsExistItemAtHand());

	__CHECK_PTR(pObject);


	//////////////////////////////////////////////////
	if (pObject->IsKindOfObject(MAP_OBJECT))
	{
		MapObject * pMapObject = (MapObject *)pObject;
		if (pMapObject->IsTriggerObj())
		{
			if (pMapObject->IsTriggerOperating())
				theCursorHandler.ChangeTo( CURSORTYPE_ACTION_SWITCH_HL );

			else
				theCursorHandler.ChangeTo( CURSORTYPE_ACTION_SWITCH );

			return TRUE;
		}
	}
	///////////////////////////////////////////////////
	else if (pObject->IsKindOfObject(ITEM_OBJECT))
	{
		theCursorHandler.ChangeTo( CURSORTYPE_OPEN_BOX );
		return TRUE;
	}

	///////////////////////////////////////////////////
	else
	{
		Character * pCharCmp = static_cast<Character *>(pObject);
		ASSERT(pCharCmp);

		///////////////////////////
		switch (GetRelationOfHero(pCharCmp))
		{
		case RELATION_ENEMY:
			{
				if (theHeroActionInput.IsAttackSkillArea())
					theCursorHandler.ChangeTo( CURSORTYPE_MAGIC_READY_HL );

				else if (theHero.GetTargetObject() 
						&&	theHero.GetTargetObject() == pObject)
				{
					if(pCharCmp->IsAlive())
						theCursorHandler.ChangeTo( CURSORTYPE_ATTACK_TARGET_HL );
					else
						theCursorHandler.ChangeTo( CURSORTYPE_DEFAULT );
				}
				else
				{
					 if(pCharCmp->IsAlive())
						theCursorHandler.ChangeTo( CURSORTYPE_ATTACK_TARGET );
					else
						theCursorHandler.ChangeTo( CURSORTYPE_DEFAULT );
				}

				return TRUE;
			}
			break;

		case RELATION_VENDOR:
		case RELATION_ASSISTANT:
			{
				theCursorHandler.ChangeTo( CURSORTYPE_CONVERSATION );
				return TRUE;
			}
			break;

		//case RELATION_FRIEND:
		//case RELATION_NONE:
		}
	}
	return FALSE;
}




BOOL GameUtil::TryGameCommond( const char* pszCommand )
{
	if( pszCommand == NULL )
		return FALSE;

	if( pszCommand[0] == 0 )
		return FALSE;

	theGameUIManager.UIOnDataInput(gameui::eUIChatInfoBox, (void*)pszCommand);

	ScriptWord words;
	if( words.Parse( pszCommand ) == 0 )
		return FALSE;


	if(theGameCommandManager.ExecuteCommand(words))
		return TRUE;

	///////////////////////////////////////////
	MSG_CG_GM_STRING_CMD_SYN	msgGM;
	lstrcpyn(msgGM.m_szStringCmd,pszCommand,MSG_CG_GM_STRING_CMD_SYN::MAX_STRING_CMD_LENGTH);
	theHero.SendPacket(&msgGM,sizeof(msgGM));

	return TRUE;
	
}


//------------------------------------------------------------------------------
BOOL GameUtil::SpawnMonster( DWORD dwCode, DWORD dwCount )
{
	if( theGeneralGameParam.IsEnableNetwork() )
	{
		MSG_CG_GM_MONSTER_CREATE_SYN	SendPacket2;
		SendPacket2.m_dwMonsterCode	= (MONSTERCODE)dwCode;
		SendPacket2.m_byMonsterCnt		= (BYTE)dwCount;
		Vector3D vvPos = theHero.GetPosition();
		SendPacket2.m_fPos[0] = vvPos.x;
		SendPacket2.m_fPos[1] = vvPos.y;
		SendPacket2.m_fPos[2] = vvPos.z + 1.0f;

		if( !theNetworkLayer.SendPacket( CK_GAMESERVER, &SendPacket2, sizeof(MSG_CG_GM_MONSTER_CREATE_SYN) ) )
		{
			//COLOR	Color = COLOR_RGBA( 255, 80, 80, 255 );
			ASSERT( !"MSG_CG_GM_MONSTER_CREATE_SYN - BattleScene" );
		}
	}
	else
	{
		Vector3D vvPos = theHero.GetPosition();

		Monster	*pMonster = (Monster*)theObjectManager.Add(
			theObjectManager.GenerateKeyAtSingleMode(MONSTER_OBJECT), MONSTER_OBJECT, dwCode );

		pMonster->SetPosition( vvPos );

		PLAYER_ACTION idleaction;
		sNPCINFO_BASE *pInfo=theNPCInfoParser.GetMonsterInfo( dwCode );

		if(pInfo)
		{
			idleaction.SPAWN.dwEndTime=pInfo->m_dwSpawnTime+g_CurTime;
		}

		pMonster->SetCurrentAction(idleaction);
		pMonster->SetNextState(STATE_SPAWN,g_CurTime);		


		//	pMonster->SetAnimation( "ID01", TRUE );

		Vector3D vTemp;
		vTemp.x = 56.0f;
		vTemp.y = 57.0f;
		vTemp.z = 0.0f;
		pMonster->SetDirection(vTemp);
		pMonster->SetHP(pMonster->GetMonsterInfo()->m_dwMaxHP);


		SKILL_EFFECT* pEffect	= theSkillEffectManager.CreateSkillEffect();	
		(*pEffect)->dwSkillID	= 0;
		(*pEffect)->dwStatusID	= 3;	
		(*pEffect)->iRemainTime	=1000;
		pMonster->AddSkillEffect(pEffect);

		//(*pEffect) = new SKILL_EFFECT;		
		(*pEffect)->dwSkillID	= 0;
		(*pEffect)->dwStatusID	= 5;	
		(*pEffect)->iRemainTime	=1000;
		pMonster->AddSkillEffect(pEffect);

		//(*pEffect) = new SKILL_EFFECT;		
		(*pEffect)->dwSkillID	= 0;
		(*pEffect)->dwStatusID	= 7;	
		(*pEffect)->iRemainTime	=1000;
		pMonster->AddSkillEffect(pEffect);

	}

	return TRUE;
}

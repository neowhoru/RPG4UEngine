/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInVarInterface.cpp
创建日期：2007年5月20日
最后更新：2007年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Player.h"
#include "GameInVarInterface.h"
#include "ScriptKeywordManager.h"
#include "IScriptCompiler.h"
//#include "TDataBuffer.h"
#include "ScriptWord.h"
#include "GameInScriptHandler.h"
#include "ScriptStructDefine.h"
#include "ObjectManager.h"
#include "HeroTipLayer.h"
#include "Hero.h"
#include "HeroActionInput.h"
#include "MapNPC.h"
#include "TipLogManager.h"
#include "TipLogManagerInstanceDefine.h"

#include "GameInVarInterface.inc.h"

using namespace quest;

BOOL g_bScriptCompilerError = FALSE;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(GameInVarInterface, ()  , gamemain::eInstPrioGameFunc);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameInVarInterface::GameInVarInterface()
{
	m_dwTempValue = 0;
}



void GameInVarInterface::SetTeamVar	(int   varIndex
                                    ,int   oprType
												,void* pParams
												,int   nParamCount)
{
	//Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::SetTeamVar error" );
	//	return;
	//}
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( pPlayer->GetTeamID() );
	//if ( pTeam )
	//{
	//	for ( int n=0; n<pTeam->GetTeamNumber(); n++ )
	//	{
	//		m_dwCharacterID = pTeam->GetTeamMemberID( n );
	//		SetVar( varIndex, oprType, pParams, nParamCount );
	//	}
	//}
	//else
	{
		SetVar( varIndex, oprType, pParams, nParamCount );
	}
	
}

void GameInVarInterface::SetVar(int   varIndex
                               ,int   oprType
										 ,void* pvParams
										 ,int   nParamCount)
{
	__UNUSED(nParamCount);
	__CHECK2_PTR(pvParams,);


	SetVarParam*	pParams = (SetVarParam*)pvParams;
	Character*		pChar;

	pChar				= (Character*)theObjectManager.GetObject(m_dwCharacterID);

	__VERIFY2	(pChar && pChar->IsKindOfObject(CHARACTER_OBJECT)
					,FMSTR("SetVar()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,);


	///玩家变量，须是玩家
	if ( varIndex < MAX_PLAYERVAR_SIZE && !pChar->IsKindOfObject(PLAYER_OBJECT))
		return;


	GameInScriptHandler script(pChar);

	switch( varIndex )
	{
	case KEYW_LEVEL:
		{
			assert( nParamCount == 1 );
			pChar->SetLevel((LEVELTYPE)(INT) pParams[0] );
		}break;

	case KEYW_POS:
		{
			assert( nParamCount == 3 );
			Vector3D vPos;
			vPos.Set( pParams[0], pParams[1],0.f);
			pChar->SetPosition(vPos);
			pChar->SetAngle( pParams[2]);
		}
		break;

	case KEYW_NAME:
		{
			assert( nParamCount == 1 );
			pChar->SetName( pParams[0] );
		}break;

	//case KEYW_HEADERNAME:
	//	break;
	//case KEYW_TARGETNAME:
	//	break;

	case KEYW_MAP:
		{
			assert(0);
			assert( nParamCount == 1 );
			//pChar->SetMapID( pParams[0] );
		}break;

	case KEYW_MODEL:
		{
			assert(0);
			assert( nParamCount == 1 );
			//pChar->SetModelID((short) pParams[0] );
		}
		break;

	case KEYW_PROFESSION:
		{
			assert( nParamCount == 1 );
			script.SetProfession( pParams[0] );
		}break;

	case KEYW_SEX:
		{
			assert( nParamCount == 1 );
			script.SetSex( pParams[0] );
		}break;

	case KEYW_MONEY:
		{
			assert( nParamCount == 1 );
			script.Money( pParams[0], oprType );
		}break;

	case KEYW_EXP:
		{
			assert( nParamCount == 1 );
			script.SetExp( pParams[0], oprType );
		}break;

	case KEYW_SKILLEXP:
		{
			assert( nParamCount == 1 );
			script.SetSkillExp( pParams[0], oprType );
		}break;

	case KEYW_VIT:
		{
			assert( nParamCount == 1 );
			script.SetVIT( pParams[0], oprType );
		}break;

	case KEYW_STR:
		{
			assert( nParamCount == 1 );
			script.SetSTR( pParams[0], oprType );
		}break;

	case KEYW_DEX:
		{
			assert( nParamCount == 1 );
			script.SetDEX( pParams[0], oprType );
		}break;


	case KEYW_INT:
		{
		assert( nParamCount == 1 );
			script.SetINT( pParams[0], oprType );
		}break;



	case KEYW_SPR:
		{
			assert( nParamCount == 1 );
			script.SetSPR( pParams[0], oprType );
		}break;


	case KEYW_HP:
		{
			assert( nParamCount == 1 );
			script.SetHP( pParams[0], oprType );
		}break;

	case KEYW_MP:
		{
			assert( nParamCount == 1 );
			script.SetMP( pParams[0], oprType );
		}break;


	case KEYW_SKILL:
		{
			//assert( nParamCount == 1 );
			//script.Skill( pParams[0], oprType );
		}break;

	case KEYW_REPUTATION:
		{
			assert( nParamCount == 1 );
			script.SetReputation( pParams[0], oprType );
		}break;


	default:
		{
			pChar->SetVar( varIndex, pParams[0], oprType );
		}break;
	}
}


LPCSTR GameInVarInterface::GetLabelOfVar( int varIndex )
{
	Character*			pChar;

	pChar				= (Character*)theObjectManager.GetObject(m_dwCharacterID);

	__VERIFY2	(pChar && pChar->IsKindOfObject(CHARACTER_OBJECT)
					,FMSTR("GetLabelOfVar()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,NULL);

	///玩家变量，须是玩家
	if ( varIndex < MAX_PLAYERVAR_SIZE && !pChar->IsKindOfObject(PLAYER_OBJECT))
		return NULL;


	switch( varIndex )
	{
	case KEYW_NAME:
		{
			return pChar->GetName();
		}break;


	case KEYW_HEADERNAME:
		{
			//PlayerTeam * pTeam = NULL;
			//pTeam = GetPlayerTeamManager().GetTeam( pChar->GetTeamID() );
			//if ( pTeam )
			//{
			//	int nHeaderId = pTeam->GetTeamHeaderID();
			//	Player* pHeader = thePlayerManager.FindPlayer( nHeaderId );
			//	if ( pHeader )
			//		return pHeader->GetPlayerName();
			//}
			return "";
		}
		break;

	case KEYW_TARGETNAME:
		{
			//Player*			pDestPlayer;

			//pDestPlayer			= thePlayerManager.FindPlayer(pChar->GetDstPlayerId());
			//__VERIFY2_PTR	(pDestPlayer
			//					,FMSTR("GetLabelOfVar ()脚本错误，char:%ld 无效.\n",pChar->GetDstPlayerId() )
			//					,"");

			//return pDestPlayer->GetPlayerName();
			return "";
		}
		break;
	}

	static char szVar[32];
	itoa( GetVar(varIndex), szVar, 10 );
	return szVar;
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int GameInVarInterface::GetVar( int varIndex, int nPlayerId )
{
	int nID;
	
	if ( nPlayerId != -1 )
		nID = nPlayerId;
	else
		nID = m_dwCharacterID;

	
	Character*			pChar;

	pChar				= (Character*)theObjectManager.GetObject(m_dwCharacterID);

	__VERIFY2	(pChar && pChar->IsKindOfObject(CHARACTER_OBJECT)
					,FMSTR("GetLabelOfVar()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,0);

	///玩家变量，须是玩家
	if ( varIndex < MAX_PLAYERVAR_SIZE && !pChar->IsKindOfObject(PLAYER_OBJECT))
		return 0;



	GameInScriptHandler script(pChar);

	switch( varIndex )
	{
	case KEYW_MONEY:			return (int)pChar->GetMoney();
	case KEYW_LEVEL:			return pChar->GetLevel();
	case KEYW_PROFESSION:	return script.GetProfession();
	case KEYW_SEX:				return script.GetSex();
	case KEYW_EXP:				return script.GetExp();
	case KEYW_SKILLEXP:		return script.GetSkillExp();
	case KEYW_VIT:				return script.GetVIT();

	case KEYW_STR:				return script.GetSTR();
	case KEYW_DEX:				return script.GetDEX();
	case KEYW_INT:				return script.GetINT();
	case KEYW_SPR:				return script.GetSPR();
	case KEYW_HP:				return script.GetHP();
	case KEYW_MP:				return script.GetMP();
	case KEYW_REPUTATION:		return script.GetReputation();
	default:						return pChar->GetVar( varIndex );
	}
	//return 0;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::ShowDialog( LPCSTR pszText, BOOL bEndDialog )
{
	//__CHECK2(m_dwCharacterID != -1, );

	//theGameUIManager.UIDisplayChatInfo(pszText);
	//OUTPUTCHAT(pszText);

	//Character*			pChar;

	//pChar				= (Character*)theObjectManager.GetObject(m_dwCharacterID);

	//__VERIFY2	(pChar && pChar->IsKindOfObject(CHARACTER_OBJECT)
	//				,FMSTR("ShowDialog脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
	//				,);

	theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
											,gameui::eToggleDialogText
											,(LPARAM)pszText);

	theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
											,gameui::eShowDialogNext
											,(LPARAM)!bEndDialog);

	//MSG_EXESCRIPT_ACK msg;
	//msg.NpcId = (GameCharID)m_dwTargetNpcID;
	//msg.SetString( pszText );
	//if ( iButtonFLag == 0 )
	//	msg.bShowNext = TRUE;
	//else
	//	msg.bShowNext = FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::ShowAsk( KEYWORD varID, LPCSTR pszText )
{
	__CHECK2(m_dwCharacterID != -1, );

	if(varID != -1)
	{
		Character*			pChar;

		pChar				= (Character*)theObjectManager.GetObject(m_dwCharacterID);

		__VERIFY2	(pChar && pChar->IsKindOfObject(CHARACTER_OBJECT)
						,FMSTR("ShowAsk脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
						,);

		if( !pChar->RegisterVar( varID ) )
		{
			LOGINFO( "ShowAsk脚本错误，CharacterID id:%ld ask failed.\n", m_dwCharacterID );
			return;
		}
	}

	theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
											,gameui::eShowQuestionText
											,(LPARAM)pszText);

	//MSG_EXESCRIPT_ACK msg;
	//msg.dwAckType |= MSG_EXESCRIPT_ACKStruct::eAskQuestion;
	//msg.stNpcId = (GameCharID)m_dwTargetNpcID;
	//msg.SetString( pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Tell( LPCSTR pszText )
{
	__CHECK2(m_dwCharacterID != -1, );

	Character*			pChar;

	pChar				= (Character*)theObjectManager.GetObject(m_dwTargetNpcID);

	__VERIFY2	(pChar && pChar->IsKindOfObject(CHARACTER_OBJECT)
					,FMSTR("Tell脚本错误，CharacterID id:%ld 无效\n", m_dwTargetNpcID)
					,);

	pChar->DisplayChatMessage(pszText);

    //MSG_CHAT_REQStruct Msg;
    //Msg.SetString( pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Talk( DWORD /*time*/, DWORD /*color*/, LPCSTR pszText )
{
	__CHECK2(m_dwCharacterID != -1, );

	if ( !pszText || strlen(pszText) <= 0 )
		return;
	//MSG_SHOWTEXT msg;
	//msg.bTile = FALSE;
	//msg.byAlign = MsgShowText::eAlignMiddle;
	//msg.dwTime = time;
	//msg.dwColor = color;
	//strcpy( msg.szText, MsgShowText::STRING_LEGTH, pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::TalkInShout( DWORD /*time*/, DWORD /*color*/, LPCSTR pszText )
{
	__CHECK2(m_dwCharacterID != -1, );
	if ( !pszText || strlen(pszText) <= 0 )
		return;
	//MSG_SHOWTEXT msg;
	//msg.bTile = FALSE;
	//msg.byAlign = MsgShowText::eAlignMiddle;
	//msg.dwTime = time;
	//msg.dwColor = color;
	//strcpy( msg.szText, MsgShowText::STRING_LEGTH, pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::TalkInSector	(int         /*nMapId*/
                                    ,int         /*nX*/
												,int         /*nY*/
												,DWORD       /*time*/
												,DWORD       /*color*/
												,LPCSTR pszText)
{
	__CHECK2(m_dwCharacterID != -1, );
	if ( !pszText || strlen(pszText) <= 0 )
		return;
	//MSG_SHOWTEXT msg;
	//msg.bTile = FALSE;
	//msg.byAlign = MsgShowText::eAlignMiddle;
	//msg.dwTime = time;
	//msg.dwColor = color;
	//strcpys( msg.szText, MsgShowText::STRING_LEGTH, pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Broadcast( LPCSTR /*pszText*/ )
{
  __CHECK2(m_dwCharacterID != -1, );
    
   //MSG_CHAT_REQ Msg(gameui::CHAT_TYPE_BULL);
	//Msg.header.stID = (GameCharID)m_dwCharacterID;
	//Msg.SetString( pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::JumpToMap( int nMapId, int x, int y )
{
	__CHECK2(m_dwCharacterID != -1, );
	Player* pPlayer;
	
	pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if( !pPlayer  )
	{
		LOGINFO( "脚本执行错误，CharacterID id:%ld 无效\n", m_dwCharacterID );
		return;
	}
	//GameInScriptHandler script((Player*)pPlayer);
	//script.JumpToMap( nMapId, x, y );
	theSceneLogicFlow.GotoVillageScene((MAPCODE)nMapId ,x	 ,y);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::OpenShop ( DWORD npcID )
{
	if(singleton::ExistHero())
		theHeroActionInput.ProcessNpcMeet(npcID);

	//MSG_OPENSHOP_ACK msg;
	//msg.header.stID = pPlayer->GetID();
	//msg.m_RewardType = pShop->m_RewardType;
	//msg.nCount = pShop->nItemCount;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::DoCommand( LPCSTR /*command*/ )
{
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Show	(short /*stImageId*/
                              ,int   /*x*/
										,int   /*y*/
										,DWORD /*dwLife*/
										,DWORD /*dwFlag*/
										,DWORD /*dwFlagTime*/)
{
	
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::RemoveAllEquip( )
{
	__CHECK2(m_dwCharacterID != -1, );
	
	Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if( !pPlayer )
	{
		LOGINFO( "脚本执行错误，CharacterID id:%ld 无效\n", m_dwCharacterID );
		return;
	}

	GameInScriptHandler script((Player*)pPlayer);
	script.RemoveAllEquip( );
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::RemoveAllItem( )
{
	__CHECK2(m_dwCharacterID != -1, );

	Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if( !pPlayer )
	{
		LOGINFO( "脚本执行错误，CharacterID id:%ld 无效\n", m_dwCharacterID );
		return;
	}

	GameInScriptHandler script((Player*)pPlayer);
	script.RemoveAllItem( );
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::SetBornPoint( DWORD dwMapID, int iPosX, int iPosY )
{
	__CHECK2(m_dwCharacterID != -1, );

	Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if( !pPlayer )
	{
		LOGINFO( "脚本执行错误，CharacterID id:%ld 无效\n", m_dwCharacterID );
		return;
	}
	
	//SetBornPoint	
	GameInScriptHandler script((Player*)pPlayer);
	script.SetBornPoint( dwMapID, iPosX, iPosY );
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::AddItem( LPCSTR pszItemName, int nItemCount )
{
	int iCharID = GetCharacterID();
	Player* pPlayer = (Player*)thePlayerManager.FindPlayer( iCharID );
	{
		GameInScriptHandler script(pPlayer);
		script.AddItem( pszItemName, nItemCount );
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::DeleteItem( LPCSTR pszItemName, int nItemCount )
{
	int iCharID = GetCharacterID();
	Player* pPlayer = (Player*)thePlayerManager.FindPlayer( iCharID );
	{
		GameInScriptHandler script(pPlayer);
		script.DeleteItem( pszItemName, nItemCount );
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameInVarInterface::ExistItem(LPCSTR  pItemName, int nItemCount )
{
	__CHECK2(m_dwCharacterID != -1, 0);
	Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if ( !pPlayer )
	{
		LOGINFO("错误:HaveThisItem");
		return 0;
	}

	GameInScriptHandler script((Player*)pPlayer);
	return script.ExistItem(pItemName, nItemCount);

	
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameInVarInterface::IsItemInEquip( LPCSTR pItemName )
{
	sITEMINFO_BASE* pItemInfo;

	pItemInfo = theItemInfoParser.GetItemInfo(pItemName);
	__CHECK_PTR(pItemInfo);

	return theItemManager.IsExistItem(SI_EQUIPMENT,pItemInfo->m_Code,1);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameInVarInterface::IsSkillActived( WORD skillItemID,WORD skillLevel )
{
	__CHECK(m_dwCharacterID != -1 );
	
	Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if ( !pPlayer )
		return FALSE;

	GameInScriptHandler script((Player*)pPlayer);
	return script.IsSkillActived( skillItemID,skillLevel );
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int	GameInVarInterface::GetMapCodeByName( LPCSTR /*pszMapName*/ )
{
	return -1;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::TeashSkill( const std::vector<int>& /*vectorSkill*/ )
{
	//msg.nNum = vectorSkill.size();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::AddSkill( int /*nSkillId*/ )
{

	//__CHECK2(m_dwCharacterID != -1, );
	//
	//int iCharID = GetCharacterID();
	//Player* pPlayer = (Player*)thePlayerManager.FindPlayer( iCharID );
	//if ( !pPlayer )
	//	return;
	//if( pPlayer->LearnSkill( nSkillId, 0 ) == FALSE )
	//{
	//	LOGINFO( "脚本学习技能失败 ID=%d", nSkillId );
	//}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD GameInVarInterface::Quest_CancelQuest( int nQuestID )
{
	
	int iCharID = GetCharacterID();
	Player* pPlayer = thePlayerManager.FindPlayer( iCharID );

	if( pPlayer)// && pPlayer->IsPlayer() )
	{
		GameInScriptHandler script((Player*)pPlayer);
		return script.CancelQuest( nQuestID );
	}
	return 0;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD GameInVarInterface::Quest_DoneQuest( int nQuestID )
{
	
	int iCharID = GetCharacterID();
	Player* pPlayer = thePlayerManager.FindPlayer( iCharID );

	if( pPlayer )//&& pPlayer->IsPlayer() )
	{
		GameInScriptHandler script((Player*)pPlayer);
		return script.DoneQuest( nQuestID );
	}
	return 0;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Quest_SetQuestState( int nQuestID, int nState )
{
	
	int iCharID = GetCharacterID();
	Player* pPlayer = thePlayerManager.FindPlayer( iCharID );

	if( pPlayer )//&& pPlayer->IsPlayer() )
	{
		GameInScriptHandler script((Player*)pPlayer);
		script.SetQuestState( nQuestID, nState );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Quest_GetQuestState( int nQuestID, int nVarID )
{
	
	int iCharID = GetCharacterID();
	Player* pPlayer = thePlayerManager.FindPlayer( iCharID );

	if( pPlayer )//&& pPlayer->IsPlayer() )
	{
		GameInScriptHandler script((Player*)pPlayer);
		//pPlayer->SetVar( varIndex, script.GetQuestState( nQuestId ) );
		script.GetQuestState( nQuestID, nVarID );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Quest_RelateQuest( int nQuestID, int nState, int nEnter )
{
	__CHECK2(m_dwCharacterID != -1, );


	MapNPC*			pNpc;

	pNpc				= (MapNPC*)theObjectManager.GetObject(m_dwCharacterID);

	__VERIFY2	(pNpc && pNpc->IsKindOfObject(NPC_OBJECT)
					,FMSTR("Quest_RelateQuest()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,);

	sQUEST_RELATEINFO info;
	info.m_QuestID		= nQuestID;
	info.m_State		= nState;
	info.m_Entrance	= nEnter;
	pNpc->AddRelateQuestInfo( &info );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::Quest_CanGetNewQuest( int nVarID )
{
	int iCharID = GetCharacterID();
	Player* pPlayer = thePlayerManager.FindPlayer( iCharID );

	if( pPlayer)// && pPlayer->IsPlayer() )
	{
		GameInScriptHandler script((Player*)pPlayer);
		script.CanGetNewQuest( nVarID );
	}
	
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	GameInVarInterface::Printf( LPCSTR string, ... )
{
	if( !string )
		return;

	va_list	va;
	static char data[1024];
	va_start( va, string );
	vsnprintf( (char*)data,1024, string, va );
	va_end( va );

	LOGINFO(data);
}


void GameInVarInterface::RefreshMonster	(int    /*nMapId*/
                                          ,int    /*x*/
														,int    /*y*/
														,LPCSTR /*szArea*/)
{
}

int GameInVarInterface::GetCharCount( int /*nMapId*/ )
{
	theObjectManager.GetPlayerCount();
	return 0;
	
}

void GameInVarInterface::PlayBGMusic( LPCSTR /*szFileName*/, DWORD /*dwParam*/ )
{
	//MSG_SCRIPT_PLAYERMUSIC_CMDStruct msg;
	//msg.header.stID = pPlayer->GetID();
	//msg.stParameter = stParameter;
	//msg.SetFileName( szFileName );
}

void GameInVarInterface::MakeItem	(BYTE byCreatNewOrOld
                                    ,int  nRate
												,int  nIdBegin[]
												,int  nIdEnd[]
												,int  nCount)
{
	__UNUSED(byCreatNewOrOld);
	__UNUSED(nRate);
	__UNUSED(nIdBegin);
	__UNUSED(nIdEnd);
	__UNUSED(nCount);
}

void GameInVarInterface::ShowQuestDialog(KEYWORD varID , LPCSTR szInfo )
{
	Player* pPlayer;
	pPlayer = (Player*)thePlayerManager.FindPlayer( m_dwCharacterID );
	if( !pPlayer )
	{
		LOGINFO( "GameInVarInterface::ShowQuestDialog(%d) error", varID );
		return;
	}
	pPlayer->SetScriptTargetID( m_dwTargetNpcID );

	//MSG_SCRIPT_SHOWQUESTDLG_CMD  msg;
	//msg.header.stID = pPlayer->GetID();
	//msg.stNpcId = m_dwTargetNpcID;
	//StringUtil::SafeNCpy( msg.szInfo, szInfo, MSG_SCRIPT_SHOWQUESTDLG_CMDStruct::eInfoLengthMax );

	if(varID != -1)
	{
		if( !pPlayer->RegisterVar( varID ) )
		{
			LOGINFO( "GameInVarInterface::ShowQuestDialog(%d) error", varID );
		}
	}

	theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
											,gameui::eShowQuestList
											,(LPARAM)szInfo);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameInVarInterface::UseSkill( int nSkillId, int nSkillLevel )
{
	theHeroActionInput.DoSkillProcessing(nSkillId+nSkillLevel);
	//pDst->TryAttack(&target, pSkill, nSkillId, nSkillLevel );
}

void GameInVarInterface::PopNpcList()
{
	//MSG_POPMAPNPCLIST_CMDStruct pop;
	//theServer.SendMsgToSingle( &pop, pDst );
}

int GameInVarInterface::GetWorldTime( int /*nTimeType*/ )
{
	return 0;
}

int GameInVarInterface::GetBirthday( int /*nTimeType*/ )
{
	return 0;
}

void GameInVarInterface::ShowBank()
{
	//MSG_UPDATEBANK msg;
	//msg.dwCurMoney = pBank->dwCurMoney;
	//msg.dwMaxMoney = pBank->dwMaxMoney;
	//msg.dwPlayerMoney = pPlayer->GetMoney();
}



void GameInVarInterface::NpcMoveNext()
{
}

void GameInVarInterface::NpcStopMove()
{
}

void GameInVarInterface::NpcStartMove()
{
}

void GameInVarInterface::NpcPlayAnim	(LPCSTR /*szName*/
                                       ,int    /*nTimes*/
													,LPCSTR /*szEndName*/
													,BOOL   /*bForView*/)
{
	//MSG_NPCPLAYANIM msg;
	//msg.nNpcId = GetTargetNpcID();
	//StringUtil::SafeNCpy( msg.szActionName, szName, MsgNpcPlayAnim::eActionNameLength );
	//StringUtil::SafeNCpy( msg.szEndActionName, szEndName, MsgNpcPlayAnim::eActionNameLength );
	//msg.nLoopTimes = nTimes;
}

BOOL GameInVarInterface::CanLearnSkill( int /*nSkillId*/, int /*nSkillLevel*/ )
{
	return TRUE;
}

void GameInVarInterface::DoRide( int /*nID*/ )
{
	//Player* pPlayer = (Player*)thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::CanLearnSkill error" );
	//	return;
	//}
	//pPlayer->DoRide( nID );
}

int GameInVarInterface::GetDromeID()
{
	return -1;
	//Player* pPlayer = (Player*)thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::GetDromeID error" );
	//	return -1;
	//}
	//return pPlayer->GetDromeID();
}

void GameInVarInterface::SetDialogName( LPCSTR /*szName*/ )
{
	//MSG_SETDIALOGNAME_CMD msg;
	//strncpy( msg.szName, szName, MSG_SETDIALOGNAME_CMDStruct::NAME_MAXLENGTH );
}

void GameInVarInterface::SetHotkey( int /*nIndex*/, int /*nID*/ )
{
	//MSG_SCRIPT_SETHOTKEY_CMD msg;
	//msg.ustIndex = nIndex;
	//msg.nID = nID;
}

BOOL GameInVarInterface::DumpInfoToDB( LPCSTR /*szInfo*/ )
{
   return TRUE;
} 

int GameInVarInterface::GetTeamPlayerCount()
{
	return 1;
	//Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::GetTeamPlayerCount error" );
	//	return 1;
	//}
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( pPlayer->GetTeamID() );
	//if ( pTeam )
	//		return pTeam->GetTeamNumber();
	//	return 1;
}

int GameInVarInterface::GetTeamPlayerLevel( int /*nType*/ )
{
	return 0;
	//Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::GetTeamPlayerCount error" );
	//	return 0;
	//}
	//int nLevel = pPlayer->GetLevel();
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( pPlayer->GetTeamID() );
	//if ( pTeam )
	//{
	//	for ( int n=0; n<pTeam->GetTeamNumber(); n++ )
	//	{
	//		DWORD dwID = pTeam->GetTeamMemberID( n );
	//		Player* pTeamPlayer = thePlayerManager.FindPlayer( dwID );
	//		assert( pTeamPlayer );
	//		if( !pTeamPlayer )
	//			continue;
	//		switch( nType ) 
	//		{
	//		case 0:
	//			{
	//				if ( pTeamPlayer->GetLevel() < nLevel )
	//					nLevel = pTeamPlayer->GetLevel();
	//			}
	//			break;
	//		case 1:
	//			{
	//				if ( pTeamPlayer->GetLevel() > nLevel )
	//					nLevel = pTeamPlayer->GetLevel();
	//			}
	//			break;
	//		default:
	//			assert(FALSE);
	//			break;
	//		}
	//	}
	//}
	//return nLevel;
}

int GameInVarInterface::GetTeamPlayerID( UINT /*nIndex*/ )
{
	//Player* pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::GetTeamPlayerID error" );
	//	return -1;
	//}
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( pPlayer->GetTeamID() );
	//if ( pTeam )
	//{
	//	if ( nIndex < (WORD)pTeam->GetTeamNumber() )
	//	{
	//		return pTeam->GetTeamMemberID( nIndex );
	//	}
	//}
	return -1;
	
}


void GameInVarInterface::SetTargetPlayer( DWORD /*dwID*/)
{
	//Player* pPlayer = (Player*)thePlayerManager.FindPlayer( m_dwCharacterID );
	//if( !pPlayer )
	//{
	//	LOGINFO( "GameInVarInterface::SetTargetPlayer error" );
	//	return;
	//}
	//pPlayer->SetTargetPlayer( nID );
}

void GameInVarInterface::CreateMonster	(DWORD /*dwMapId*/
                                       ,float /*fPosX*/
													,float /*fPosY*/
													,float /*fRadius*/
													,float /*fBodySize*/
													,int   /*nMonsterIndex*/
													,int   /*nMonsterNum*/)
{
}


int GameInVarInterface::GetEmptySlotCount()
{
	
	Player* pPlayer = (Player*)thePlayerManager.FindPlayer( m_dwCharacterID );
	if ( !pPlayer )
	{
		LOGINFO("错误:GetEmptySlotCount");
		return 0;
	}

	GameInScriptHandler script((Player*)pPlayer);
	return script.GetEmptySlotCount();
	
}

void GameInVarInterface::CreateGuild()
{
    //Player* pPlayer = (Player*)thePlayerManager.FindPlayer( m_dwCharacterID );
    //if ( !pPlayer )
    //{
    //    LOGINFO("错误:CreateGuild");
    //    return;
    //}
    //pPlayer->CreateGuild();
}

void	GameInVarInterface::LogTip(LPCSTR      szTip,LPCSTR      szTip2,eTIP_TYPE tipType)
{
	LOGTIP( szTip, tipType, NULL,INVALID_SOUNDINDEX,FALSE,szTip2);
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemValidation.cpp
创建日期：2008年9月12日
最后更新：2008年9月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ItemValidation.h"
#include "ConstTextRes.h"

ItemValidation::ItemValidation(ItemSlot& itemSlotRef)
:m_ItemSlot(itemSlotRef)
{

}


//----------------------------------------------------------------------------
BOOL ItemValidation::CanEquipLimit( ) const
{
	__CHECK(singleton::ExistHero());

	__CHECK(ValidLimitStr(  ));
	__CHECK(ValidLimitDex(  ) );
	__CHECK(ValidLimitInt(  ) );

	__CHECK(ValidLimitLevel(  ));

	__CHECK(ValidLimitSkillExp1(  ));
	__CHECK(ValidLimitSkillExp2(  ));

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL ItemValidation::ValidLimitStr(  ) const 
{
	PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();			
	DWORD LimitStr = m_ItemSlot.GetLimitSTR( theHero.GetClass() );

	if( 0 == LimitStr )
		return TRUE;



	if ( (DWORD)pCharInfo->GetBaseSTR() < LimitStr )
	{
		OUTPUTCHATF	(TEXTRES_ITEM_EQUIP_LIMIT_STR
						,(LPCSTR)m_ItemSlot.GetItemInfo()->m_ItemName
						,pCharInfo->GetBaseSTR() 
						,LimitStr);
		return FALSE;
	}

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL ItemValidation::ValidLimitDex(  ) const 
{

	PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();
	DWORD LimitDex = m_ItemSlot.GetLimitDEX(theHero.GetClass());
	if( 0 == LimitDex )
		return TRUE;



	if( (DWORD)pCharInfo->GetBaseDEX() < LimitDex ) 
	{
		OUTPUTCHATF	(TEXTRES_ITEM_EQUIP_LIMIT_DEX
						,(LPCSTR)m_ItemSlot.GetItemInfo()->m_ItemName
						,pCharInfo->GetBaseDEX()
						,LimitDex);
	
		return FALSE;
	}

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL ItemValidation::ValidLimitInt(  ) const 
{	

	PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();			
	DWORD LimitInt = m_ItemSlot.GetLimitINT(theHero.GetClass());
	if( 0 == LimitInt )
		return TRUE;


	if( (DWORD)pCharInfo->GetBaseINT() < LimitInt ) 
	{
		OUTPUTCHATF	(TEXTRES_ITEM_EQUIP_LIMIT_INT
						,(LPCSTR)m_ItemSlot.GetItemInfo()->m_ItemName
						,pCharInfo->GetBaseINT()
						,LimitInt);
		return FALSE;
	}

	return TRUE;

}


//----------------------------------------------------------------------------
BOOL ItemValidation::ValidLimitLevel(  ) const
{	
	//PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();			
	//sPLAYERINFO_BASE *		pPlayInfo = theHero.GetCharInfo();

	sITEMINFO_BASE *		pInfo = theItemInfoParser.GetItemInfo( m_ItemSlot.GetCode() );
	if( !pInfo ) return FALSE;

	DWORD LimitLV	= m_ItemSlot.GetLimitLevel();	

	if( theHero.GetLevel() < LimitLV ) 
	{
		return FALSE;
	}

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL ItemValidation::ValidLimitSkillExp1(  ) const
{
	PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();			
	//sPLAYERINFO_BASE *		pPlayInfo = theHero.GetCharInfo();

	sITEMINFO_BASE *		pInfo = theItemInfoParser.GetItemInfo( m_ItemSlot.GetCode() );
	if( !pInfo ) return FALSE;

	DWORD LimitSkill = pInfo->m_wLimitSkil1;
	if( 0 == LimitSkill ) return TRUE;

	if( m_ItemSlot.GetItemInfo()->IsWeapon() )
	{
		LimitSkill = theFormularManager.CalcLimitStatWeapon((WORD) LimitSkill, m_ItemSlot.GetEnchant(), m_ItemSlot.GetRank(), m_ItemSlot.GetItemInfo()->m_LV );
	}
	else if( m_ItemSlot.GetItemInfo()->IsArmor() )
	{
		LimitSkill = theFormularManager.CalcLimitStatArmor((WORD) LimitSkill, m_ItemSlot.GetEnchant(), m_ItemSlot.GetRank(), m_ItemSlot.GetItemInfo()->m_LV );
	}
	else
	{
		DEBUG_CODE( if(LimitSkill!=0) DISPMSG("invalid ValidLimitStr check(%s)\n", (LPCSTR)pInfo->m_ItemName ); );
	}

	if( (DWORD)pCharInfo->GetExperty1() < LimitSkill ) 
	{
		return FALSE;
	}

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL ItemValidation::ValidLimitSkillExp2(  ) const
{
	PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();			
	//sPLAYERINFO_BASE *	pPlayInfo = theHero.GetCharInfo();

	sITEMINFO_BASE * pInfo = theItemInfoParser.GetItemInfo( m_ItemSlot.GetCode() );
	if( !pInfo ) return FALSE;

	DWORD LimitSkill = pInfo->m_wLimitSkil2;
	if( 0 == LimitSkill ) return TRUE;

	if( m_ItemSlot.GetItemInfo()->IsWeapon() )
	{
		LimitSkill = theFormularManager.CalcLimitStatWeapon((WORD) LimitSkill, m_ItemSlot.GetEnchant(), m_ItemSlot.GetRank(), m_ItemSlot.GetItemInfo()->m_LV );
	}
	else if( m_ItemSlot.GetItemInfo()->IsArmor() )
	{
		LimitSkill = theFormularManager.CalcLimitStatArmor((WORD) LimitSkill, m_ItemSlot.GetEnchant(), m_ItemSlot.GetRank(), m_ItemSlot.GetItemInfo()->m_LV );
	}
	else
	{
		DEBUG_CODE( if(LimitSkill!=0) DISPMSG("invalid ValidLimitStr check(%s) \n", (LPCSTR)pInfo->m_ItemName ); );
	}

	if((DWORD) pCharInfo->GetExperty2() < LimitSkill ) 
	{
		return FALSE;
	}

	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：VNpc.cpp
创建日期：2007年4月13日
最后更新：2007年4月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VNpc.h"
#include "VObjectManager.h"
#include "IGameEffect.h"
#include "IGameEffectContainer.h"
#include "GameEffectManager.h"
#include "SoundLayer.h"
#include "GameMainUtil.h"
#include "VCharacterAnimationCtrl.h"
#include "GameClientApplication.h"
#include "Map.h"
#include "MapNPC.h"
#include "ApplicationSetting.h"
#include "RenderUtil.h"
#include "HeroQuestManager.h"
#include "PathInfoParser.h"
#include "GameParameter.h"

using namespace gamemain;
using namespace object;
namespace vobject
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VNpc::VNpc()
{
	m_dwObjectType			= NPC_OBJECT;
	m_nNpcTipsType			= eNpcTips_None;
	m_dwMonsterType		= INVALID_DWORD_ID;
	m_nStateUpdateTimer	= 0;
}
VNpc::~VNpc()
{
}


VMapNpc::VMapNpc()
{
	m_dwObjectType			= MAPNPC_OBJECT;
	m_dwShowNameFlag		= OBJNAME_SHOWNPC;
	//m_nNpcTipsType			= eNpcTips_None;
	//m_nStateUpdateTimer	= 0;
}
VMapNpc::~VMapNpc()
{
}


BOOL VMapNpc::InitSericeIcon	(DWORD attackKind)
{
	MapNPC*			pNPC = (MapNPC*)m_pObject;
	sNPCINFO_FUNC*	pFunc;
	pFunc = pNPC->GetFuncInfo();

	if(pFunc && pFunc->m_eNPCTYPE != NPC_FUNC_NONE)
	{
		attackKind = PREFIXKIND_NPC_FUNC + pFunc->m_eNPCTYPE;
	}
	return _SUPER::InitSericeIcon	(attackKind);
}



//void	VNpc::Destroy()
//{
//	_SUPER::Destroy();
//}

void	VNpc::Reset()
{
	_SUPER::Reset();
	m_nNpcTipsType		= eNpcTips_None;
}

BOOL VNpc::OnMeshLoading()
{
	BOOL bRet = FALSE;
	if( m_nModelId !=-1 )
	{
		if( m_bTwoHalfBody )
		{
			m_pAnim->SetNeedInitUpperBodyAndLowerBody(TRUE);
		}
		bRet = m_pAnim->ReloadMesh( m_nModelId );
	}
	return bRet;
}

void	VNpc::Update( DWORD dwTick )
{
	_SUPER::Update(dwTick);
}

void	VNpc::UpdateManual( DWORD dwTime)
{
	//m_nStateUpdateTimer -= dwTick;
	if(dwTime - m_nStateUpdateTimer >= theGeneralGameParam.GetQuestStateUpdatePeriod() )
	{
		m_nStateUpdateTimer = dwTime;
		//m_nStateUpdateTimer = theGeneralGameParam.GetQuestStateUpdatePeriod();
		UpdateQuestState(g_FrameCostTime);
	}
	_SUPER::UpdateManual(dwTime);
}

void	VNpc::UpdateQuestState( DWORD /*dwTick*/ )
{

	int	nNpcTipsType	(eNpcTips_None);
	BOOL	bUpdateEffect	(FALSE);

	///////////////////////////////////////////
	float	fRange	= theVObjectManager.GetFarFromHero()*2;
	if(GetDistanceToRole() <= fRange)
	{
		nNpcTipsType = theHeroQuestManager.UpdateNpcTipsType( GetID() );
	}


	/////////////////////////////////////////////////
	// 如果状态改变了
	if(	m_nNpcTipsType != nNpcTipsType 
		//||	m_pCharEffectContainer && !m_pCharEffectContainer->GetEffect(gameeffect::eNpcTip)
		)
		bUpdateEffect = TRUE;


	/////////////////////////////////////////////////
	if(bUpdateEffect)
	{
		//清除原来的
		if( m_pCharEffectContainer )
		{
			m_pCharEffectContainer->ClearEffect( gameeffect::eNpcTip );
		}

		SetNpcTipsType( nNpcTipsType );

		// 启动特效
		// 没有tips
		if(  m_nNpcTipsType != eNpcTips_None )
		{
			{
				//theApplicationSetting
				EFFECTINDEX effectIndex = INVALID_EFFECTINDEX;
				switch( m_nNpcTipsType )
				{
				case eNpcTips_HaveQuest:
					effectIndex = theApplicationSetting.m_szHaveQuestEffect;
					break;
				case eNpcTips_HaveQuestNotNow:
					effectIndex = theApplicationSetting.m_szHaveQuestNotNowEffect;
					break;
				case eNpcTips_QuestDone:
					effectIndex = theApplicationSetting.m_szQuestDoneEffect;
					break;
				case eNpcTips_QuestNotDone:
					effectIndex = theApplicationSetting.m_szQuestNotDoneEffect;
					break;
				}

				CastModelEffect(effectIndex , NULL, gameeffect::eNpcTip );
			}
		}
	}
}

BOOL	VNpc::RenderInfo( DWORD dwTick, DWORD dwColor , float fTransparent, float fPlayerTransparent  )
{
	__BOOL_SUPER(RenderInfo(  dwTick,  dwColor ,  fTransparent,  fPlayerTransparent  ));

	/// 以下绘画的小图提示，

	return TRUE;
}



//BOOL VNpc::_DoStatusMove()
//{
//	__BOOL_SUPER(_DoStatusMove());
//	return TRUE;
//}
//

void VNpc::SetNPCType(DWORD /*dwType*/)
{
}

};//namespace vobject


/*////////////////////////////////////////////////////////////////////////
文 件 名：CharacterScene.cpp
创建日期：2008年3月25日
最后更新：2008年3月25日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "CharacterScene.h"
#include "CharacterScenePacketHandler.h"
#include "LoadScene.h"
#include "LoginScene.h"
#include "PacketInclude.h"
#include "StructInPacket.h"
#include "Camera.h"
#include "CommonDefine.h"
#include "StringHandle.h"
#include "Version.h"
#include "KeyQueueManager.h"
#include "GameClientApplication.h"
#include "VPlayer.h"
#include "VHeroActionInput.h"
#include "VHero.h"
#include "CharacterCreator.h"
#include "ApplicationSetting.h"
#include "GameParameter.h"
#include "BGMusicInfoParser.h"
#include "ResourceManagerDefine.h"
#include "LoginScenePacketHandler.h"
#include "Map.h"
#include "SceneLogicFlow.h"
#include "VUCtrlManager.h"
#include "NetworkLayer.h"
#include "V3DCamera.h"
#include "V3DGameWorld.h"
#include "V3DScene.h"
#include "V3DSceneAnimation.h"
#include "CharActionInfo.h"
#include "PacketStruct_ClientGameS_Connection.h"
#include "StructInPacket.h"
#include "PacketCryptManager.h"
#include "HeroData.h"

//using namespace gameaction;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(CharacterScene, ()  , gamemain::eInstPrioGameFunc);

using namespace math;
using namespace input;
using namespace vobject;

#define MOVE_TIMELENGTH				5000		//移动时间

#define PERFECT                 100.0f 
#define DOUBLE_CLICK_TIME       500    





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CharacterScene::CharacterScene(void)
{
	m_dwFreshTipGroup			= TIPGROUP_CHARSELECT;
	m_dwMapHandle				= WH_TYPE_CHARSELECT;
	m_dwUIListener				= gameui::eUISceneCHARSELECT;
	m_SceneType					= scene::SCENE_TYPE_CHARSELECT;
	m_bLockPlayer				= FALSE;
	m_bLockCameraDistance	= TRUE;
}

CharacterScene::~CharacterScene(void)
{
	Release();
}



BOOL CharacterScene::Init()
{
	if(!_SUPER::Init())
		return FALSE;
	//UseGameWorld();
	__INIT(theCharacterCreator.Init());

	return TRUE;
}	


void CharacterScene::Release()
{
	__END(theCharacterCreator.Release());
	//SAFE_DELETE(m_pCamera);
	//m_pScene			= NULL;//不可删除
}			


void CharacterScene::Activate()
{
}

BOOL CharacterScene::OnChangeFrameworkIn()
{
	//assert(!singleton::ExistVHero() && "本场景不需要VHero对象，须销毁此对象，否则有些业务逻辑将看作GameInPlaying处理");

	if(!_SUPER::OnChangeFrameworkIn())
		return FALSE;

	__BOOL_CALL(theCharacterCreator.RequestWorkingData());

	m_nShowPlayerInfoBak = g_V3DConfig.GetShowInfoLV();
	g_V3DConfig.SetShowInfoLV(OBJNAME_SHOWPLAYER);

	m_dwSelectObjID		= NONE_SELECT;
	m_dwSelectCharacter	= NONE_SELECT;
	m_dwCurDelObjID		= NONE_SELECT;

	theCursorHandler.ForceChange( cursor::CURSORTYPE_DEFAULT );

	m_CharList.clear();
	InitSlotForPositionAndAni();

	theSoundBGM.PlayBGM	(MAP_TYPE_DEFAULT
								,theMap.GetCurrentFieldID()
								,ZONETYPE_CHARSELECT);

	if( theGeneralGameParam.IsEnableNetwork() )
	{
		if(!InitNet())
			return FALSE;
	}
	else 
	{
		if(!InitLocal())
			return FALSE;
	} 

	m_fOriginalFarClip = theCamera.GetFarClip();
	m_bCreateCharacter	= FALSE;;

	theKeyQueueManager.Reset();
	theKeyQueueManager.SetKey(KEYC_ESCAPE);
	theKeyQueueManager.SetDefaultFun(NULL);
	
	return TRUE;
}


BOOL CharacterScene::OnChangeFrameworkOut()
{
	if(!_SUPER::OnChangeFrameworkOut())
		return FALSE;

	theCharacterCreator.ReleaseWorkingData();

	g_V3DConfig.SetShowInfoLV(m_nShowPlayerInfoBak);


	//theGameWorld.ReleaseWorkingData();
	//theGameWorld.Shutdown();	

	//SAFE_DELETE(m_pCamera);
	//m_pScene		= NULL;//不可删除


	CharListInfoListIt itr;

	for (itr=  m_CharList.begin(); itr!= m_CharList.end() ;)
	{
		delete static_cast<CharListInfo *>(*itr);
		m_CharList.erase(itr++);
	}

	m_CharList.clear();
	m_StatusArray.Clear();

	return TRUE;
}

void CharacterScene::InitSlotForPositionAndAni()
{
	m_arPosInfos[0].objKey	=	NONE_SELECT;
	m_arPosInfos[1].objKey	=	NONE_SELECT;
	m_arPosInfos[2].objKey	=	NONE_SELECT;
	m_arPosInfos[3].objKey	=	NONE_SELECT;
	m_arPosInfos[4].objKey	=	NONE_SELECT;

	m_Animations[0]	=	info::Idle;
	m_Animations[1]	=	info::Idle;
	m_Animations[2]	=	info::Idle;
	m_Animations[3]	=	info::Idle;
	m_Animations[4]	=	info::Idle;

	if(theClientSetting.IsLoginAuto())
		return;


	MapHandle* pWorld = theMap.GetActiveMapHandle();
	assert(pWorld);

	V3DScene* pScene = pWorld->GetScene();
	assert(pScene);
	if(pScene)
	{
		INT					n;
		V3DSceneObject*	pObject;
		for(n=0; n<MAX_CHAR_NUM; n++)
		{
			pObject = pScene->FindSceneObject(FMSTR("pos%d",n+1));
			if(!pObject)
			{
				MessageBox(NULL,FMSTR("CharacterScene场景中没有[pos%d]的辅助物",n+1),"找不到辅助物",MB_OK);
				GetGameClientApplication().Exit();
				return;
			}

			m_arPosInfos[n].vPosition = pObject->GetPosition();

			pObject = pScene->FindSceneObject(FMSTR("lookat%d",n+1));
			if(!pObject)
			{
				MessageBox(NULL,FMSTR("CharacterScene场景中没有[lookat%d]的辅助物",n+1),"找不到辅助物",MB_OK);
				GetGameClientApplication().Exit();
				return;
			}

			m_arPosInfos[n].vLookAt = pObject->GetPosition();
		}
	}


}

void CharacterScene::ParsePacket( MSG_BASE * pMsg )
{
	if(!theCharacterScenePacketHandler.NetworkProc(pMsg))
		SceneBase::ParsePacket(pMsg);
}



void	CharacterScene::OnReconnect()
{
	if(theGeneralGameParam.IsNetSimple())
	{
		//OnChangeFrameworkOut();
		//OnChangeFrameworkIn();
		//theObjectManager.Destroy();
		//theObjectManager.Destroy();
		theCharacterScenePacketHandler.SetConnected(FALSE);
		//theObjectManager.

			ObjectMapIt	itr = theObjectManager.GetBegin();

			while( theObjectManager.GetEnd() != itr )
			{
				Object *pObj = (Object *)itr->second;
				itr++;
				if(pObj->IsPlayer())
				{
					Player *pPlayer=(Player *)pObj;
					theObjectManager.Delete(pPlayer->GetObjectKey());
				}
			}

		CharListInfoListIt it;
		for (it=  m_CharList.begin(); it!= m_CharList.end() ;)
		{
			delete static_cast<CharListInfo *>(*it);
			m_CharList.erase(it++);
		}
		m_CharList.clear();
		m_StatusArray.Clear();

		m_bLockPlayer = FALSE;
		m_dwSelectObjID		= NONE_SELECT;
		m_dwSelectCharacter	= NONE_SELECT;
		m_dwCurDelObjID		= NONE_SELECT;
		m_dwPacketStatus		= 0;

		InitNet();
	}
}


BOOL CharacterScene::InitNet()
{
	if (theCharacterScenePacketHandler.IsConnected())
	{
		MSG_CG_CONNECTION_REENTERSERVER_SYN		msgSend;

		if( FALSE == theNetworkLayer.SendPacket(CK_GAMESERVER, &msgSend, sizeof(msgSend) ) )
		{
			LOGINFO(_T("MSG_CG_CONNECTION_REENTERSERVER_SYN - CharacterScene"));
			return FALSE;
		}        
	}


	if(!theCharacterScenePacketHandler.IsConnected())
	{
		if(theGeneralGameParam.IsNetSimple())
		{
			int iConnectRet = theNetwork.Connect(CK_GAMESERVER, 
		                                       theGeneralGameParam.GetGameServerIP(), 
		                                       theGeneralGameParam.GetGameServerPort());
			__UNUSED(iConnectRet);
			assert(iConnectRet && "GameServer...连接失败...");
		}

		if(!IsSendCharEnterPacket())
		{
			BIT_ADD(m_dwPacketStatus,PACKET_STATUS_CHARSCENE_CONECT);

			MSG_CG_CONNECTION_ENTERSERVER_SYN	ConnectRegPacket;
			ConnectRegPacket.m_byCategory		= CG_CONNECTION;
			ConnectRegPacket.m_byProtocol		= CG_CONNECTION_ENTERSERVER_SYN;
			//ConnectRegPacket.m_dwUserGUID = theGeneralGameParam.GetUserID();

			ConnectRegPacket.m_dwAuthID	  = theLoginScenePacketHandler.GetAuthUserID();
			strcpy(ConnectRegPacket.m_szID, theLoginScenePacketHandler.GetAccount());

			memcpy ((BYTE *)ConnectRegPacket.m_szSerialKey,
							theLoginScenePacketHandler.GetSerialKey(),
							MAX_AUTH_SERIAL_LENGTH);

			ConnectRegPacket.m_byHighVerNo		= VERSION_C2S_HIGH_NO;
			ConnectRegPacket.m_byMiddleVerNo		= VERSION_C2S_MIDDLE_NO;
			ConnectRegPacket.m_byLowVerNo			= VERSION_C2S_LOW_NO;


			__VERIFY	(theNetworkLayer.SendPacket	(CK_GAMESERVER
                                          ,&ConnectRegPacket
														,sizeof(ConnectRegPacket))
						," x-- MSG_CA_CHARINFO_CHARLIST_SYN - CharacterScene SendPacket Failed!... \n");

		}           
	}


	theNetworkLayer.OnReconnectToChatServer();
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL CharacterScene::InitLocal()
{
	#pragma message(__FILE__  "(373) CharacterScene..InitLocal  " )

	CharListInfo * pCharListInfo = new CharListInfo;
	pCharListInfo->dwID = theObjectManager.GenerateKeyAtSingleMode(PLAYER_OBJECT);
	strcpy(pCharListInfo->CharInfo.m_szCharName,"人物1");

	if(CreateCharacter(PLAYERTYPE_WARRIOR,pCharListInfo->CharInfo.m_szCharName,0,pCharListInfo)==FALSE)
		delete pCharListInfo;

	//theGameUIManager.UITriggerFunc();

	return TRUE;
}


void CharacterScene::OnDisconnect		( )
{
	theCharacterScenePacketHandler.SetConnected(FALSE);
	m_dwPacketStatus	= 0;
}


int  CharacterScene::GetEmptySlot()
{
	for(int i=0;i<MAX_CHAR_NUM;i++)
	{
		if(m_arPosInfos[i].objKey == NONE_SELECT)
			return i;
	}
	return -1;
}




void CharacterScene::ProcessKeyInput( DWORD /*dwTick*/ )
{
	if(theInputLayer.IsKeyUp(KEYC_U))
	{
		g_V3DConfig.SetShowDummy(!g_V3DConfig.IsShowDummy());
	}


}

Character* CharacterScene::GetPickedChar(const RAY& /*ray*/)
{
	VObject* pSelObject = theVObjectManager.GetMouseTargetObject();
	if(pSelObject == NULL)
		return NULL;
	if(!pSelObject->IsPlayer())
		return NULL;

	return  (Character*)pSelObject->GetObject();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	CharacterScene::MoveFront(Player * pPlayer)
{
	DWORD dwIndex = pPlayer->GetCharInfo()->m_bySlot;
	pPlayer->MoveTo(m_arPosInfos[dwIndex].vLookAt);

	AddStatus		(pPlayer->GetObjectKey(),	CHARACTER_STATUS_MOVE_FRONT);
	RemoveStatus	(pPlayer->GetObjectKey(),	CHARACTER_STATUS_MOVE_BACK);
	RemoveStatus	(pPlayer->GetObjectKey(),	CHARACTER_STATUS_SELECT_IDLE);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	CharacterScene::MoveBack	(Player * pPlayer)
{
	if(pPlayer == NULL)
		pPlayer = (Player*)theObjectManager.GetObject(m_dwSelectObjID);
	if(pPlayer == NULL)
		return;

	DWORD dwIndex = pPlayer->GetCharInfo()->m_bySlot;
	pPlayer->MoveTo(m_arPosInfos[dwIndex].vPosition);

	AddStatus		(pPlayer->GetObjectKey(),	CHARACTER_STATUS_MOVE_BACK);
	RemoveStatus	(pPlayer->GetObjectKey(),	CHARACTER_STATUS_MOVE_FRONT);
	RemoveStatus	(pPlayer->GetObjectKey(),	CHARACTER_STATUS_SELECT_IDLE);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL	CharacterScene::ShowCharacter(float fAlpha,int nSlot)
{
	Object* pObject;
	if(nSlot == -1 )
	{
		for(int i=0;i<MAX_CHAR_NUM;i++)
		{
			if(m_arPosInfos[i].objKey != NONE_SELECT)
			{
				pObject = theObjectManager.GetObject(m_arPosInfos[i].objKey);
				if(pObject)
					pObject->SetAlpha(fAlpha);
			}
		}
	}
	else if(nSlot < MAX_CHAR_NUM)
	{
			if(m_arPosInfos[nSlot].objKey != NONE_SELECT)
			{
				pObject = theObjectManager.GetObject(m_arPosInfos[nSlot].objKey);
				if(pObject)
					pObject->SetAlpha(fAlpha);
			}
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	CharacterScene::SetLockPlayer(BOOL bSet)
{
	m_bLockPlayer = bSet;
	if(bSet)
	{
		ShowCharacter(0.6f);
	}
	else
	{
		ShowCharacter(1.0f);
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL	CharacterScene::DoCreateCharacter()
{
	int i;
	int nEmpty = -1;
	for(i=0;i<MAX_CHAR_NUM;i++)
	{
		if(m_arPosInfos[i].objKey == NONE_SELECT)
		{
			nEmpty = i;
			break;
		}
	}
	if(nEmpty == -1)
		return FALSE;

	//ShowCharacter(FALSE,-1);

	return theCharacterCreator.Open(nEmpty);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL CharacterScene::DoEnterWorld()
{
	Player *pPlayer = (Player*)theObjectManager.GetObject(m_dwSelectObjID);
	if(pPlayer == NULL)
		return FALSE;

	_SelectCharacter(pPlayer);
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL CharacterScene::DoDeleteCharacter(LPCSTR szVerify)
{
	if(m_dwSelectObjID == NONE_SELECT)
		return FALSE;

	__CHECK(SendDeleteCharPacket(szVerify) );
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::_OnEnterWorldFromServer(Player* pPlayer)
{
	float	fSign;
	AddStatus(pPlayer->GetObjectKey(),CHARACTER_STATUS_SELECT);
	fSign					= theGameRunning.GetCurrentTick() % 2 ? -1.f : 1.f;
	m_fAngleCurrent	= pPlayer->GetAngle() + fSign * cPI_TWO;
	m_fAngleStep		= fSign * cPI_TWO * 0.01f;	//1s转完
	pPlayer->SetVisible(FALSE);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::SelectCharacter(CharListInfo* pPlayerInfo)
{
	if(!pPlayerInfo)
	{
		_SelectCharacter(NULL,FALSE);
		return;
	}
	Player *pPlayer = (Player*)theObjectManager.GetObject(pPlayerInfo->dwID);
	//if(pPlayer == NULL)
	//	return;
	_SelectCharacter(pPlayer,FALSE);
}


//
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::_CheckAutoEnterWorld()
{
	if(!theClientSetting.IsLoginAuto())
		return;

	///////////////////////////////////////////////////////
	int nAutoSlot = theClientSetting.GetCharSelectSlot();
	if(nAutoSlot < 0)
		return;

	//nAutoSlot--;
		//GetSelectCharOId
	Player *			pOtherPlayer;
	CharListInfoListIt	itr				= m_CharList.begin();
	CharListInfo * pCharListInfo	= NULL;
	for (; itr != m_CharList.end(); ++itr)
	{
		pCharListInfo	= static_cast<CharListInfo *>(*itr);
		if(pCharListInfo->CharInfo.m_bySlot == nAutoSlot)
		{
			pOtherPlayer		= (Player*)theObjectManager.GetObject(pCharListInfo->dwID);
			_SelectCharacter(pOtherPlayer);//选两次...
			AddStatus(pCharListInfo->dwID, CHARACTER_STATUS_SELECT_IDLE);
			_SelectCharacter(pOtherPlayer);
			break;
		}
	}
}

void CharacterScene::_OnGetCharacterList(MSG_CG_CONNECTION_ENTERSERVER_ACK* pMsgHandler)
{
	sCHARACTER_CLIENTPART* pCharInfo;
	pCharInfo = (sCHARACTER_CLIENTPART*)( (BYTE*)(pMsgHandler) + pMsgHandler->GetSize(0) );

	for( int i = 0; i < pMsgHandler->m_byCount; ++i )
	{
		CreateCharacter(* pCharInfo);

		int bufSize = pCharInfo->GetSize();
		pCharInfo = (sCHARACTER_CLIENTPART *)((BYTE*)(pCharInfo) + bufSize);
		//CreateCharacter( pMsgHandler->m_CharacterList[i]);
	}

	theCharacterScenePacketHandler.SetConnected(TRUE);
	//theLoginScene.Disconnect();
	//持久连接，暂不断开

	///////////////////////////////////////////////////
	///////////////////////////////////////////////////////
	if(theClientSetting.IsLoginAuto())
	{
		int nAutoSlot = theClientSetting.GetCharSelectSlot();
		if(nAutoSlot <= 0 )
		{
			if(pMsgHandler->m_byCount == 0)
			{
				LOGINFO("none character\n");
				return;
			}
			sCHARACTER_CLIENTPART* pCharInfo;
			pCharInfo = (sCHARACTER_CLIENTPART*)( (BYTE*)(pMsgHandler) + pMsgHandler->GetSize(0) );
			nAutoSlot = pCharInfo->m_bySlot;
		}
		m_dwSelectCharacter = nAutoSlot;
		SendCharSelectPacket();
		return;
	}


	///////////////////////////////////////////////////
	theCursorHandler.ForceChange( cursor::CURSORTYPE_DEFAULT );

	if (!theNetwork.IsConnected(CK_CHATSERVER))
	{
		//MSG_CG_PREPARE_WORLD_CONNECT_SYN msgSend;
		//theNetworkLayer.SendPacket(CK_GAMESERVER, &msgSend, sizeof(msgSend) );
		//g_pApplication->OnReconnectToChatServer();
	}



	_CheckAutoEnterWorld();
}

////////////////////////////////////////////////////////////////////////
void CharacterScene::_OnRemoveCharacterBy(MSG_CG_CHARINFO_DESTROY_ACK* /*pMsgHandler*/)
{
	SelectCharacter(NULL);
	RemovePacketStatus(PACKET_STATUS_CHARSCENE_DELCHAR);

	RemovePacketStatus(PACKET_STATUS_CHARSCENE_DELCHAR);
	if(GetCurDelObjID() != NONE_SELECT)
	{
		//CharListInfoList&		m_CharList		= GetCharList();
		CharListInfoListIt	itr				= m_CharList.begin();
		CharListInfo * pCharListInfo	= NULL;

		for (; itr != m_CharList.end(); ++itr)
		{
			pCharListInfo = static_cast<CharListInfo *>(*itr);

			if(pCharListInfo->CharInfo.m_bySlot == GetCurDelObjID() )
			{
				DeleteCharacter(pCharListInfo);
				m_CharList.erase(itr);

				if(GetSelectCharacter() == GetCurDelObjID() )
					SetSelectCharacter(NONE_SELECT);

				SetCurDelObjID(NONE_SELECT);
				break;
			}
		}
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::_SelectCharacter(Character * pChar,BOOL bNotifyUI)
{
	if(m_bLockPlayer)
		return;

	if(	pChar == NULL)
	{
		MoveBack(NULL);
		return;
	}


	if(pChar->GetObjectKey() == m_dwSelectObjID)
	{
		
		//LOGINFO("CharacterScene::_SelectCharacter 2\n");
		sCHARLIST_OBJSTATUS *pGet=GetStatus(pChar->GetObjectKey());
		if(pGet)// && BIT_CHECK(pGet->objStatus, CHARACTER_STATUS_SELECT_IDLE) )
		{
			SetLockPlayer(TRUE);
			SendCharSelectPacket();
		}
		return;
	}
		//m_dwSelectCharacter = NONE_SELECT;

	//LOGINFO("CharacterScene::_SelectCharacter\n");


	Player *			pPlayer			= static_cast<Player *>(pChar);
	Player *			pOtherPlayer;
	CharListInfoListIt	itr				= m_CharList.begin();
	CharListInfo * pCharListInfo	= NULL;
	DWORD				dwPrevSelectObjID;

	m_dwSelectCharacter = NONE_SELECT;

	for (; itr != m_CharList.end(); ++itr)
	{
		pCharListInfo	= static_cast<CharListInfo *>(*itr);
		pOtherPlayer	= (Player*)theObjectManager.GetObject(pCharListInfo->dwID);

		if(pOtherPlayer->GetObjectKey() != pPlayer->GetObjectKey())
			continue;

		m_dwSelectCharacter	= pPlayer->GetCharInfo()->m_bySlot;

		//之前选中的退回
		if(m_dwSelectObjID != pPlayer->GetObjectKey())
		{
			MoveBack(NULL);
			MoveFront(pPlayer);
		}


		dwPrevSelectObjID		= m_dwSelectObjID;
		m_dwSelectObjID		= pPlayer->GetObjectKey();

		if(bNotifyUI)
			theGameUIManager.UITriggerFunc((gameui::EGameUIType)m_dwUIListener
													,gameui::eCharSelectSelectPlayer
													,(LPARAM)pCharListInfo)			;

		if( theGeneralGameParam.IsEnableNetwork() == FALSE )
		{
			sHERO_INFO	info;
			ZeroMemory(&info,sizeof(info));
			info.m_CharInfo.m_byClassCode = (BYTE)pChar->GetClass();

			((Player *)pChar)->SetMoveSpeedRatio(100);
			((Player *)pChar)->SetAttackSpeedRatio(100);
			theHeroData.SetHeroInfo( &info );
			//BattleScene::SetHeroInfo( &info );
		}
		break;
	}//for
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::_ProcessCharInfoRemove(Player * pPlayer)
{
	RemoveStatus(pPlayer->GetObjectKey(), CHARACTER_STATUS_REMOVE);
	int size=m_StatusArray.Size();
	for(int i=0;i<size;i++)
	{
		if(m_StatusArray[i].objKey == pPlayer->GetObjectKey())
		{
			m_StatusArray.Erase(i);
			break;
		}
	}

	theObjectManager.Delete(pPlayer->GetObjectKey());
}

void CharacterScene::ProcessMouse(DWORD /*dwTick*/)
{
	if(theUICtrlManager.ExistModalFrame())
		return;

	Character * pChar;
	theInputLayer.SetUIMouseInput(FALSE);

#ifdef CREATE_CHARACTER_TEST
	if (m_bCreateCharacter) 
	{
		theCharacterSceneCreate.ProcessCreateCharacterMouse();
		return;
	}
#endif


	if(theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_DOWN) )
	{
		//if(!theInputLayer.IsUIMouseInput())
		{
			pChar=GetPickedChar(g_MouseRay);
			if(pChar)
				_SelectCharacter(pChar);
		}
	}
}

BOOL CharacterScene::AddStatus(DWORD objKey,eCHARACTER_STATUS status)
{
	for(int i=0;i<m_StatusArray.Size();i++)
	{
		if(m_StatusArray[i].objKey==objKey)
		{
			BIT_ADD(m_StatusArray[i].objStatus,status);
			return TRUE;
		}
	}
	return FALSE;
}


BOOL CharacterScene::RemoveStatus(DWORD objKey,eCHARACTER_STATUS status)
{
	for(int i=0;i<m_StatusArray.Size();i++)
	{
		if(m_StatusArray[i].objKey==objKey)
		{
			BIT_REMOVE(m_StatusArray[i].objStatus,status);
			return TRUE;
		}
	}
	return FALSE;

}


BOOL CharacterScene::CheckStatus(DWORD objKey,eCHARACTER_STATUS status)
{
	for(int i=0;i<m_StatusArray.Size();i++)
	{
		if(m_StatusArray[i].objKey==objKey)
		{
			return BIT_CHECK(m_StatusArray[i].objStatus,status);
		}
	}
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL CharacterScene::SendCharSelectPacket()
{
	if(m_dwSelectCharacter==NONE_SELECT)
		return FALSE;

	//LOGINFO("CharacterScene::SendCharSelectPacket\n");

	if (BIT_CHECK(m_dwPacketStatus,PACKET_STATUS_CHARSCENE_SELCHAR)==FALSE)
	{
		BIT_ADD(m_dwPacketStatus,PACKET_STATUS_CHARSCENE_SELCHAR);

		MSG_CG_CONNECTION_ENTERVILLAGEREQ_SYN		msgCharSelectSync;
		msgCharSelectSync.m_bySelectedCharIndex = (BYTE)m_dwSelectCharacter;
		theNetworkLayer.SendGamePacket(&msgCharSelectSync
                                    ,sizeof(msgCharSelectSync))			;

		if(theGeneralGameParam.IsEnableNetwork()==FALSE)
		{           
			AddStatus(m_dwSelectObjID,CHARACTER_STATUS_SELECT);
		}
	}
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::ProcessUnits(DWORD dwTick)
{
	ObjectMapIt	itr = theObjectManager.GetBegin();

	while( theObjectManager.GetEnd() != itr )
	{
		Object *pObj = (Object *)itr->second;
		if(pObj->IsPlayer())
		{
			Player *pPlayer=(Player *)pObj;

			ProcessState(pPlayer,dwTick);

			pPlayer->ProcessInterpolation( dwTick );
			//float rt=
			pPlayer->ProcessAnimation( dwTick );

			//if(	rt >= PERFECT 
			//	&& m_dwSelectCharacter == pPlayer->GetCharInfo()->m_bySlot 
			//	&& IsSendSelectCharPacket() )
			//{
			//	DWORD dwMapID;
			//	if (theGeneralGameParam.IsEnableNetwork())
			//		dwMapID		= pPlayer->GetCharInfo()->m_dwRegion;
			//	else
			//		dwMapID		= theGeneralGameParam.GetStartLandID();

			//	theLoadScene.SetGoingToMap(dwMapID,pPlayer->GetCharInfo()->m_sLocationX, pPlayer->GetCharInfo()->m_sLocationY);
			//	theLoadScene.SetNextSceneAfterLoaded(SCENE_TYPE_VILLAGE);
			//	singleton::GetFrameworkSystem().ChangeScene(SCENE_TYPE_LOAD);
			//}
		}
		++itr;
	}//while


	Player* pPlayer;
	for(int n=m_arPlayerRemove.size()-1; n>=0;n--)
	{
		pPlayer = m_arPlayerRemove[n];
		if(!pPlayer)
			continue;

		if(pPlayer->IsVisible(FALSE))
			continue;
		sCHARLIST_OBJSTATUS *pGet=GetStatus(pPlayer->GetObjectKey());
		if(pGet)
		{
			if(BIT_CHECK(pGet->objStatus, CHARACTER_STATUS_REMOVE))
			{
			}
		}
		_ProcessCharInfoRemove(m_arPlayerRemove[n]);
		//pGet->objStatus	= CHARACTER_STATUS_NONE;
		m_arPlayerRemove.erase(m_arPlayerRemove.begin() + n);
	}

}


sCHARLIST_OBJSTATUS * CharacterScene::GetStatus(DWORD objKey)
{
	int size = m_StatusArray.Size();

	for(int i=0;i<size;i++)
	{
		if(m_StatusArray[i].objKey == objKey)
		{
			return &m_StatusArray[i];
		}
	}
	return NULL;
}



void CharacterScene::_OnEnterWorldAnimEnd( Player * pPlayer )
{
	if(IsSendSelectCharPacket() )
	{
		DWORD dwMapID;
		//DWORD dwFieldID;
		if (theGeneralGameParam.IsEnableNetwork())
		{
			dwMapID		= pPlayer->GetCharInfo()->m_dwRegion;
			//dwFieldID	= pPlayer->GetCharInfo()->m_dwRegion;
		}
		else
		{
			if(theGeneralGameParam.IsLocalHeroData())
				dwMapID			= theHeroData.GetHeroInfo().m_CharInfo.m_dwRegion;
			else
				dwMapID		= theGeneralGameParam.GetStartMapID();
			//dwMapID		= theGeneralGameParam.GetStartFieldID();
		}

		theSceneLogicFlow.GotoVillageScene((MAPCODE)dwMapID
													 ,pPlayer->GetCharInfo()->m_sLocationX
													 ,pPlayer->GetCharInfo()->m_sLocationY);
		//theLoadScene.SetGoingToMap(dwMapID,
		//						pPlayer->GetCharInfo()->m_sLocationX, 
		//						pPlayer->GetCharInfo()->m_sLocationY);
		//theLoadScene.SetNextSceneAfterLoaded(SCENE_TYPE_VILLAGE);
		//singleton::GetFrameworkSystem().ChangeScene(SCENE_TYPE_LOAD);
	}
}

void CharacterScene::_OnEnterWorldFailed(  )
{
	RemovePacketStatus(PACKET_STATUS_CHARSCENE_SELCHAR);
	SetLockPlayer		(FALSE);
	SelectCharacter	(NULL);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::ProcessState( Player * pPlayer,DWORD dwTick )
{
	sCHARLIST_OBJSTATUS *pGet=GetStatus(pPlayer->GetObjectKey());

	if(pGet)
	{
		if(BIT_CHECK(pGet->objStatus, CHARACTER_STATUS_SELECT))
		{
			m_fAngleCurrent += m_fAngleStep;
			pPlayer->SetAngle(m_fAngleCurrent);
			if(!pPlayer->IsVisible())
			{
				//pPlayer->PlayAnimation(info::Attack1,1);
				_OnEnterWorldAnimEnd(pPlayer);
				pGet->objStatus	= CHARACTER_STATUS_NONE;
			}
			//return;
		}

		if(BIT_CHECK(pGet->objStatus, CHARACTER_STATUS_MOVE_FRONT))
		{
			if(BIT_CHECK(pGet->objStatus, CHARACTER_STATUS_SELECT_IDLE) == FALSE)
			{
				int twRemainder=0;
				//pPlayer->PlayAnimation(info::Walk);

				pGet->twTime+=dwTick;

				if(pGet->twTime >= MOVE_TIMELENGTH)
				{
					twRemainder = pGet->twTime - MOVE_TIMELENGTH;
					pGet->twTime= MOVE_TIMELENGTH;

					RemoveStatus(pPlayer->GetObjectKey(),	CHARACTER_STATUS_MOVE_FRONT);
					AddStatus(pPlayer->GetObjectKey(),		CHARACTER_STATUS_SELECT_IDLE);
				}

				//DWORD dwIndex = pPlayer->GetCharInfo()->m_bySlot;
				//pPlayer->MoveTo(m_arPosInfos[dwIndex].vLookAt);
				//Vector3D curPos= pPlayer->GetVisiblePos();
				//Vector3D dir	= pPlayer->GetDirection();
				//Vector3D curPos= pPlayer->GetPosition();
				//Vector3D dir	= m_arPosInfos[dwIndex].vLookAt - curPos;

				//dir.x = dir.x*WALK_SPEED*(dwTick-twRemainder);
				//dir.y = dir.y*WALK_SPEED*(dwTick-twRemainder);

				//curPos += dir;
				//pPlayer->SetPosition(curPos);
			}
			else 
				RemoveStatus(pPlayer->GetObjectKey(),CHARACTER_STATUS_MOVE_FRONT);
		}

		else if(BIT_CHECK(pGet->objStatus,CHARACTER_STATUS_MOVE_BACK))
		{
			int twRemainder=0;
			//pPlayer->PlayAnimation(info::WalkBackWards);

			//DWORD dwIndex = pPlayer->GetCharInfo()->m_bySlot;
			//pPlayer->MoveTo(m_arPosInfos[dwIndex].vLookAt);

			pGet->twTime	-= dwTick;
			// Vector3D vCurDistance=pGet->startPos-curPos;

			if(pGet->twTime <= 0)
			{
				twRemainder	= pGet->twTime;
				pGet->twTime= 0;

				if(pPlayer->GetObjectKey() == m_dwSelectObjID)
					m_dwSelectObjID = VOBJID_NULL;

				//MoveFront(pPlayer);
				RemoveStatus(pPlayer->GetObjectKey(),		CHARACTER_STATUS_MOVE_BACK);
				AddStatus	(pPlayer->GetObjectKey(),		CHARACTER_STATUS_IDLE);

				//if(pPlayer->GetCharInfo()->m_bySlot == 0)
				//{		 
				//	CAMERA_EXTRA_INFO * camExtra = theMap.GetExtraCameraInfo();
				//	Vector3D wzCameraPos;
				//	wzCameraPos.Set( 10.0f, -20.0f, 0.0f);
				//	pPlayer->SetDirection(wzCameraPos,&m_arPosInfos[0].vPosition);
				//}
			}

			//Vector3D curPos= pPlayer->GetPosition();
			//Vector3D dir	= m_arPosInfos[dwIndex].vLookAt - curPos;
			////Vector3D curPos	= pPlayer->GetVisiblePos();
			////Vector3D dir		= pPlayer->GetDirection();

			//dir.x		= dir.x*WALK_SPEED*(dwTick+twRemainder);
			//dir.y		= dir.y*WALK_SPEED*(dwTick+twRemainder);

			//curPos	-= dir;
			//pPlayer->SetPosition(curPos);
		}

		if(BIT_CHECK(pGet->objStatus,CHARACTER_STATUS_IDLE))
		{
			//pPlayer->PlayAnimation( m_Animations[pPlayer->GetCharInfo()->m_bySlot] );	
			//pPlayer->SetAnimation( m_Animations[pPlayer->GetCharInfo()->m_bySlot], TRUE );	
			//BIT_REMOVE(pGet->objStatus,CHARACTER_STATUS_IDLE);
			pGet->objStatus	= CHARACTER_STATUS_NONE;
			pGet->twTime		= 0;
		}

		//if(BIT_CHECK(pGet->objStatus,CHARACTER_STATUS_SELECT_IDLE))
		//{
		//	//pPlayer->PlayAnimation( info::walk_attack);	
		//	//pGet->objStatus = CHARACTER_STATUS_NONE;
		//	pGet->objStatus = CHARACTER_STATUS_SELECT_IDLE;
		//}
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void CharacterScene::ProcessCamera(const sMOUSE_INFO& /*info*/)
{
#pragma message(__FILE__  "(905) CharacterScene.ProcessCamera " )




}


BOOL CharacterScene::FrameMove( DWORD dwTick )
{
	//g_CurTime = theGameRunning.GetCurrentTick();
	if(theClientSetting.IsLoginAuto())
		return TRUE;

	if(!_SUPER::FrameMove(dwTick ))
		return FALSE;

	__BOOL_CALL(theCharacterCreator.FrameMove(dwTick));


	if (m_bCreateCharacter)
	{
//		theCharacterSceneCreate.ProcessScrollInterface(dwTick);
	}

	ProcessKeyInput( dwTick );
	ProcessMouse( dwTick );
	ProcessUnits( dwTick );

	//ProcessCamera( theInput.GetMouserState() );


	return TRUE;
}



BOOL CharacterScene::Render( DWORD dwTick )
{
	if(theClientSetting.IsLoginAuto())
		return TRUE;

	if(!_SUPER::Render(dwTick ))
		return FALSE;

	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CharListInfo * CharacterScene::GetCurSelectCharacterInfo( )
{
	CharListInfoListIt itr = m_CharList.begin();
	CharListInfo * pCharListInfo = NULL;

	for (; itr != m_CharList.end(); ++itr)
	{
		pCharListInfo = static_cast<CharListInfo *>(*itr);
		if (pCharListInfo && 
			pCharListInfo->CharInfo.m_bySlot == (BYTE)m_dwSelectCharacter)
		{
			return pCharListInfo;
		}
	}
	return NULL;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL CharacterScene::CreateCharacter(const sCHARACTER_CLIENTPART& info)
{
	CharListInfo * pCharListInfo	= new CharListInfo;
	pCharListInfo->CharInfo			= info;
	pCharListInfo->dwID				= theObjectManager.GenerateKeyAtSingleMode(PLAYER_OBJECT);

	if(theClientSetting.IsLoginAuto())
	{
		m_CharList.push_back( pCharListInfo );
		return TRUE;
	}

	if(! CreateCharacter(info.m_byClass
                  ,info.m_szCharName
						,info.m_bySlot
						,pCharListInfo))
	{
		delete pCharListInfo;
	}

	theCharacterCreator.Close(TRUE);
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL CharacterScene::CreateCharacter(BYTE classCode,LPCTSTR szName,DWORD slot,CharListInfo * pCharListInfo)
{
	int size=m_CharList.size();

	if(size>=MAX_CHAR_NUM)
		return FALSE;

	Vector3D wzCameraPos;


	DWORD		dwObjectKey			= pCharListInfo->dwID;
	Player*	pPlayer				= (Player*)theObjectManager.Add(dwObjectKey, PLAYER_OBJECT, classCode);
	Vector3D pos					= m_arPosInfos[slot].vPosition;
	Vector3D vLookat				= m_arPosInfos[slot].vLookAt;
	m_arPosInfos[slot].objKey= dwObjectKey;

	__CHECK_PTR(pPlayer);

	//if(slot==0)
	//{
	//	Vector3D tempCamPos = wzCameraPos;
	//	tempCamPos.x	= 10.0f;
	//	tempCamPos.y	= -20.0f;
	//	tempCamPos.z	= 0.0f;
	//	pPlayer->SetDirection(tempCamPos,&pos,TRUE);	 
	//}
	//else
	//{
	//	pPlayer->SetDirection(wzCameraPos,&pos,TRUE);	 
	//}

	VObject*		pVObject = pPlayer->GetVObject();

	pVObject->ModShowFlag(VOBJ_SHOW_NOCAMERACHECK,0);
	pVObject->SetPathHandler(NULL);
	pPlayer->SetPosition(pos);
	//pPlayer->SetAngle(RandomAngleRadian() );
	pPlayer->SetDirection(vLookat, &pos, TRUE);	 

	///pPlayer->SetColor(COLOR_RGBA(128,128,128,255));
	pPlayer->SetShowName(TRUE);


	// BoundingVolume * pVolume=pPlayer->GetBoundingVolume();
	//pVolume->m_Sphere.m_fRadius-=0.2f;

	if(theGeneralGameParam.IsEnableNetwork() == FALSE)
	{
		sCHARACTER_CLIENTPART part;
		ZeroMemory(&part,sizeof(part));

		part.m_byClass = classCode;
		part.m_bySlot	= (BYTE)slot;
		strcpy(part.m_szCharName,szName);

		pPlayer->SetClientPartInfo(&part);
		pPlayer->SetName(szName);

		pCharListInfo->CharInfo	= part;
		pCharListInfo->dwID		= dwObjectKey;

	}
	else
	{
		pPlayer->SetClientPartInfo(&pCharListInfo->CharInfo);
		pPlayer->GetCharInfo()->m_bySlot = pCharListInfo->CharInfo.m_bySlot;
	}

	//////////////////////////////////
	//载入Mesh
	pPlayer->Process(0);


	//pPlayer->SetAnimation(m_Animations[slot], TRUE);
	pPlayer->PlayAnimation(m_Animations[slot]);
	pPlayer->SetScale(theApplicationSetting.m_UISelectorScale);

	if(theCharacterCreator.IsWorking())
		pPlayer->SetAlpha(1.0f, TRUE);

	//进入Idle状态
	sCHARLIST_OBJSTATUS temp;
	temp.objKey		= pPlayer->GetObjectKey();       
	temp.objStatus = CHARACTER_STATUS_NONE;
	temp.startPos	= pos;
	temp.twTime		= 0;
	BIT_ADD(temp.objStatus , CHARACTER_STATUS_IDLE);

	m_StatusArray.Append(temp);

	AddCharID(pCharListInfo);

	return TRUE;
}


//------------------------------------------------------------------------------
void CharacterScene::AddCharID( CharListInfo * pCharListInfo )
{
	m_CharList.push_back( pCharListInfo );
	theGameUIManager.UITriggerFunc((gameui::EGameUIType)m_dwUIListener,
								gameui::eCharSelectAddPlayer,
								(LPARAM)pCharListInfo);
}

//------------------------------------------------------------------------------
BOOL CharacterScene::SendDeleteCharPacket(const char *pString)
{   
	__CHECK_STR(pString);

	CharListInfoListIt itr = m_CharList.begin();
	CharListInfo * pCharListInfo = NULL;
	//StringHandle str=pString;

	for (; itr != m_CharList.end(); ++itr)
	{
		pCharListInfo = static_cast<CharListInfo *>(*itr);

		if(pCharListInfo->CharInfo.m_bySlot==m_dwSelectCharacter)
		{
			m_dwSelectCharacter=NONE_SELECT;
			if( theGeneralGameParam.IsEnableNetwork() )
			{
				if(!IsSendDelCharPacket())
				{
					BIT_ADD(m_dwPacketStatus,	PACKET_STATUS_CHARSCENE_DELCHAR);
					m_dwCurDelObjID	= pCharListInfo->CharInfo.m_bySlot;

					MSG_CG_CHARINFO_DESTROY_SYN msgSend;


					__ZERO(msgSend.szPasswd);
					thePacketCryptManager.PacketEncode( (unsigned char *)pString,
											  MAX_PASSWORD_LENGTH,
											  (unsigned char *)msgSend.szPasswd,
											  theLoginScenePacketHandler.GetEncKey() );

					msgSend.m_SelectedSlotIndex	=pCharListInfo->CharInfo.m_bySlot;


					__VERIFY	(theNetworkLayer.SendPacket(CK_GAMESERVER
                                              ,&msgSend
															 ,sizeof(msgSend))
								,"net failed");
				}
			}
			else
			{
				DeleteCharacter(pCharListInfo);
				m_CharList.erase(itr);
			}
			break;
		}
	}
	return TRUE;
}

//------------------------------------------------------------------------------
VOID CharacterScene::DeleteCharacter(CharListInfo * pCharListInfo)
{
	if(!pCharListInfo)
		return;
	theGameUIManager.UITriggerFunc((gameui::EGameUIType)m_dwUIListener
											,gameui::eCharSelectRemovePlayer
											,(LPARAM)pCharListInfo)		;

	DWORD ObjKey = pCharListInfo->dwID;

	for(int i=0;i<MAX_CHAR_NUM;i++)
	{
		if(m_arPosInfos[i].objKey == ObjKey)
		{
			m_arPosInfos[i].objKey = NONE_SELECT;
			break;
		}
	}

	Object * pObject = theObjectManager.GetObject(ObjKey);
	if(pObject)
	{
		pObject->SetVisible(FALSE);
		AddStatus(pObject->GetObjectKey(),CHARACTER_STATUS_REMOVE);

		m_arPlayerRemove.push_back((Player*)pObject);
	}

	//theObjectManager.Delete(ObjKey);

	//int size=m_StatusArray.Size();
	//for(i=0;i<size;i++)
	//{
	//	if(m_StatusArray[i].objKey == ObjKey)
	//	{
	//		m_StatusArray.Erase(i);
	//		break;
	//	}
	//}
}


BOOL   CharacterScene::SendCreateCharacter()
{
	if(IsSendCreateCharPacket())
		return TRUE;
	BIT_ADD(m_dwPacketStatus, PACKET_STATUS_CHARSCENE_CREATECHAR);

	return theCharacterCreator.SendCreateCharacter();
}





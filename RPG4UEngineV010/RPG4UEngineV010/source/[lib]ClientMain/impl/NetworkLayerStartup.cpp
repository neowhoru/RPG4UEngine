/*////////////////////////////////////////////////////////////////////////
文 件 名：NetworkLayer.cpp
创建日期：2006年5月23日
最后更新：2006年5月23日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NetworkLayer.h"
#include "GameParameter.h"

#define NETWORKLAYER_TIMER		1
#define NETWORKLAYER_DELAY		theGeneralGameParam.GetNetDialogDelay()
#define IDD_DIALOG_NETINIT		theGeneralGameParam.GetNetDialogResID()

namespace networkinit
{
enum{NETWORKING_OK,NETWORKING_FAILED};

HINSTANCE	g_hInstanceNet					= NULL;
TCHAR			g_szTitleNet[]					= _T("网络连接初始化");					
TCHAR			g_szWndClassNet[]				= _T("NetDialogInit");	

TCHAR			g_szSystemCheckTitle[]		= _T(	"网络连接检测");
TCHAR			g_szSystemCheckMessage[]	= _T(	"    尝试过连接验证服务器，但没能连接成功。\n"
															"请检测setting\\Login.ini中[PARAM]两字段\n"
															"	LOGIN_SERVER_IP    = 	\n"
															"	LOGIN_SERVER_PORT  = 	\n"
															"是否指向正确的验证服务器IP及端口。"
													);
TCHAR			g_szTitleNoneRes[]			= _T(	"没发现NetDialog的资源");
TCHAR			g_szMsgNoneRes[]				= _T(	"    没发现NetDialog的的对话框资源。\n"
															"请检测setting\\General.ini中[PARAM]中字段\n"
															"	NetDialogResID	= 	nID(对话框资源ID)\n"
															"是否为正确的对话框资源ID，须是在启动程序中定义的DIALOG资源。"
													);




ATOM			NetDialogRegisterClass();
BOOL			NetDialogInitInstance();
BOOL			NetDialogInit();

LRESULT CALLBACK	WndProc(HWND, UINT, WPARAM, LPARAM);
LRESULT CALLBACK SystemProc(HWND hDlg, UINT message, WPARAM wParam, LPARAM lParam)
	{	return theNetworkLayer.ProcStartUp(hDlg, message, wParam, lParam);	}

};
using namespace networkinit;


BOOL NetworkLayer::InitStartUp(HINSTANCE hInstance)
{

	if (theGeneralGameParam.IsEnableNetwork())
    {
		 if(!theGeneralGameParam.IsNetCheck())
		 {	 
			 //不检测网络，则直接初始化
			 Init();
			 return TRUE;
		 }

		g_hInstanceNet = hInstance;

		 //打开网络检测窗口
		 HRSRC hNetDlg = FindResource(NULL,MAKEINTRESOURCE(IDD_DIALOG_NETINIT),RT_DIALOG);
		 if(hNetDlg == NULL)
		 {
			 MessageBox(NULL,g_szMsgNoneRes,g_szTitleNoneRes,MB_OK);
			 return FALSE;
		 }

        NetDialogInit();
        DialogBox	(g_hInstanceNet
                  ,MAKEINTRESOURCE(IDD_DIALOG_NETINIT)
						,NULL
						,(DLGPROC)SystemProc);

        MSG msg;
        while (GetMessage(&msg, NULL, 0, 0)) 
        {
            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        if (msg.wParam != NETWORKING_OK)
        {
            Release();
            return FALSE;
        }
    }
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
LRESULT NetworkLayer::ProcStartUp(HWND hDlg, UINT message, WPARAM wParam, LPARAM /*lParam*/)
{
	switch (message)
	{
	case WM_INITDIALOG:
		{
			RECT rect,rect2;
			GetClientRect(hDlg, &rect);
			GetClientRect(GetDesktopWindow(), &rect2);

			int w = rect.right-rect.left;
			int h = rect.bottom-rect.top;
			SetWindowPos(hDlg
                     ,NULL
							,(rect2.right -   w)/2
							,(rect2.bottom -  h)/2
							,0
							,0
							,SWP_SHOWWINDOW | SWP_NOSIZE);

			SetWindowText(hDlg, g_szSystemCheckTitle);

			SetTimer	(hDlg
                  ,NETWORKLAYER_TIMER
						,NETWORKLAYER_DELAY
						,(TIMERPROC) NULL);		
		}
		return TRUE;

	case WM_COMMAND:
		if (LOWORD(wParam) == IDOK || LOWORD(wParam) == IDCANCEL) 
		{
			EndDialog(hDlg, LOWORD(wParam));
			PostQuitMessage(NETWORKING_FAILED);
			return TRUE;
		}
		break;

	case WM_TIMER:
		{
			switch(wParam)
			{
			case NETWORKLAYER_TIMER:
				{
					KillTimer(hDlg, 1);

					//初始化网络连接
					Init();

					if (!ConnectToLoginServer())
					{
						MessageBox(hDlg, g_szSystemCheckMessage, g_szSystemCheckTitle, 0);
						EndDialog(hDlg, LOWORD(wParam));
						PostQuitMessage(NETWORKING_FAILED);
					}
					else
					{
						theNetwork.Disconnect(CK_LOGINSERVER);
						EndDialog(hDlg, LOWORD(wParam));
						PostQuitMessage(NETWORKING_OK);
					}
				}
				break;
			}
		}
		break;
	}
	return FALSE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace networkinit
{
BOOL NetDialogInit()
{
    NetDialogRegisterClass();

    if (!NetDialogInitInstance ()) 
    {
        return FALSE;
    }

    return TRUE;
}

ATOM NetDialogRegisterClass()
{
    WNDCLASSEX wcex;

    wcex.cbSize = sizeof(WNDCLASSEX); 

    wcex.style				= CS_HREDRAW | CS_VREDRAW;
    wcex.lpfnWndProc		= (WNDPROC)WndProc;
    wcex.cbClsExtra		= 0;
    wcex.cbWndExtra		= 0;
    wcex.hInstance		= g_hInstanceNet;
    wcex.hIcon				= NULL;//LoadIcon(g_hInstanceNet, (LPCTSTR)IDI_SUNICON);
    wcex.hCursor			= LoadCursor(NULL, IDC_ARROW);
    wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
    wcex.lpszMenuName	= NULL;
    wcex.lpszClassName	= g_szWndClassNet;
    wcex.hIconSm			= NULL;//LoadIcon(wcex.hInstance, (LPCTSTR)IDI_SUNICON);

    return RegisterClassEx(&wcex);
}


BOOL NetDialogInitInstance()
{
    HWND hWnd;

    hWnd = CreateWindow(g_szWndClassNet, 
					g_szTitleNet,
					WS_OVERLAPPEDWINDOW,
					CW_USEDEFAULT,
					0,
					CW_USEDEFAULT,
					0,
					NULL,
					NULL,
					g_hInstanceNet,
					NULL);

    if (!hWnd)
    {
        return FALSE;
    }

    g_hWndMain = hWnd;

	//HIMC hIMC = ImmGetContext(g_hWndMain);
	//if (hIMC)
	//{
	//	ImmReleaseContext(g_hWndMain,hIMC);
	//	ImmAssociateContext(g_hWndMain,NULL);
	//}

    //ShowWindow(hWnd, nCmdShow);
    //UpdateWindow(hWnd);
    return TRUE;
}

LRESULT CALLBACK WndProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    int wmId, wmEvent;
    PAINTSTRUCT ps;
    HDC hdc;

    switch (message) 
    {
    case WM_COMMAND:
        wmId    = LOWORD(wParam); 
        wmEvent = HIWORD(wParam); 
        //switch (wmId)
        {
        //default:
            return DefWindowProc(hWnd, message, wParam, lParam);
        }
        break;
    case WM_PAINT:
        hdc = BeginPaint(hWnd, &ps);
        EndPaint(hWnd, &ps);
        break;
    case WM_DESTROY:
        PostQuitMessage(NETWORKING_FAILED);
        break;
    default:
        return DefWindowProc(hWnd, message, wParam, lParam);
    }
    return 0;
}
};

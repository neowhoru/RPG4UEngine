/*////////////////////////////////////////////////////////////////////////
文 件 名：NetworkSystem.cpp
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NetworkSystem.h"
#include "GameClientSocket.h"
#include "PacketStruct_ClientWorldS.h"
#include "GameParameter.h"
#include "ConstTextRes.h"
#include "PacketInclude.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(NetworkSystem, ()  , gamemain::eInstPrioNetwork);


#define WM_SOCKET_NETWORKSYSTEM		( WM_USER + 1000)



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NetworkSystem::NetworkSystem()
{
	__ZERO(m_Connections);

}

NetworkSystem::~NetworkSystem()
{
	Release();
}


BOOL NetworkSystem::Init	(DWORD              /*dwMaxConnectionNum*/
                           ,NETPROC_RECV       netProcRecv
									,NETPROC_DISCONNECT netProcDisconnect
									,NETPROC_REPORT     /*netProcReport*/)
{
	GameClientSocket::SocketStartup();
	
	m_netProcRecv					= netProcRecv;
	m_netProcDisconnect			= netProcDisconnect;

	SetEnableSendHeartBeat(FALSE);
	SetEnableSendChatHeartBeat(FALSE);
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID NetworkSystem::Release()
{

	SetEnableSendHeartBeat(FALSE);
	SetEnableSendChatHeartBeat(FALSE);

	if( m_Connections )
	{
		for ( int iChannel = 0; iChannel < CK_MAX; ++iChannel) 
		{
			if ( m_Connections[iChannel] )
			{
				m_Connections[iChannel]->Disconnect();
				delete m_Connections[iChannel];
				m_Connections[iChannel] = NULL;
			}
		}
	}
	
	GameClientSocket::SocketShutdown();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void		NetworkSystem::UpdateTimer			( eCONNECTION_KIND connIndex )
{
	if( m_Connections[connIndex] )
		m_Connections[connIndex]->UpdateTimer(); 
}

DWORD		NetworkSystem::GetSendBytePerSec	( eCONNECTION_KIND connIndex )
{
	if( m_Connections[connIndex] )
		return m_Connections[connIndex]->GetSendBytePerSec();
	return 0; 
}

DWORD		NetworkSystem::GetRecvBytePerSec	( eCONNECTION_KIND connIndex ) 
{
	if( m_Connections[connIndex] )
		return m_Connections[connIndex]->GetRecvBytePerSec();
	return 0; 
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID NetworkSystem::Update()
{



	////////////////////////////////////////////
	if (	m_tmHeartBeat.IsExpired()
		&&	IsConnected(CK_GAMESERVER)) 
	{
		MSG_CG_CONNECTION_HEARTBEAT_SYN	SendPacket;
		theNetworkLayer.SendGamePacket	(&SendPacket,sizeof(SendPacket));
	}

	////////////////////////////////////////////
	if (	m_tmChatHeartBeat.IsExpired()
		&& IsConnected(CK_CHATSERVER) )
	{
		MSG_CW_HEARTBEAT msgBeat;
		theNetworkLayer.SendPacket(CK_CHATSERVER
                            ,&msgBeat
									 ,sizeof(msgBeat));
	}



	////////////////////////////////////////////
	WORD wSize;

	for( int iChannel = 0 ; iChannel < CK_MAX ; ++iChannel )
	{
		if(!m_Connections[iChannel])
			continue;

		BYTE * pbyMsg = NULL;
		while (pbyMsg = m_Connections[iChannel]->GetFirstPacket() )
		{
			wSize = *(WORD*)pbyMsg;

			m_netProcRecv( iChannel, pbyMsg + sizeof(wSize) );

			m_Connections[iChannel]->RemoveFirstPacket( sizeof(wSize) + wSize );
		}
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL NetworkSystem::Connect(eCONNECTION_KIND connIndex
                           ,char *           pszIPAddress
									,PORTTYPE         usPort)
{
	if( m_Connections[connIndex] == NULL )
	{
		m_Connections[connIndex] = new GameClientSocket;
		m_Connections[connIndex]->Create();
		m_Connections[connIndex]->SetPacketSize(theGeneralGameParam.GetSendPacketSize()
															,theGeneralGameParam.GetSendPacketMaxSize());
	}

	if(m_Connections[connIndex]->IsConneted())
		return TRUE;

	ASSERT( connIndex < CK_MAX );
	assert(g_hWndMain);
	BOOL bConnected;
	bConnected = m_Connections[connIndex]->Connect(pszIPAddress
                                                 ,usPort
																 ,g_hWndMain
																 ,WM_SOCKET_NETWORKSYSTEM + connIndex);

	if(bConnected)
	{
		SetEnableSendHeartBeat(FALSE);

	}
	else
	{
		//delete m_Connections[connIndex];
		//m_Connections[connIndex] = NULL;
	}

	return bConnected;
}


int	NetworkSystem::GetBackDataCount() 
{
	//int num=0;

	if(m_Connections[CK_GAMESERVER])
		return  m_Connections[CK_GAMESERVER]->GetBackDataCount();

	return 0;
}


BOOL NetworkSystem::IsConnected( eCONNECTION_KIND connIndex )
{
    if (m_Connections[connIndex])
        return m_Connections[connIndex]->IsConneted();
    return FALSE;
}

BOOL NetworkSystem::Disconnect( eCONNECTION_KIND connIndex, BOOL isCallfn )
{
	SetEnableSendHeartBeat(FALSE);


	if( m_Connections[connIndex] )
	{
		ASSERT( connIndex < CK_MAX );
		BOOL ret = m_Connections[connIndex]->Disconnect();
		if (isCallfn)
			m_netProcDisconnect( connIndex );
		return ret;
	}
	return FALSE;
}

BOOL NetworkSystem::Send( eCONNECTION_KIND connIndex, BYTE * pBuf, DWORD dwLength )
{
	ASSERT( connIndex < CK_MAX );

	if (m_Connections[connIndex])
		return m_Connections[connIndex]->Send( pBuf, dwLength );
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sADDRESS
{
    BYTE   addr[32];
    UINT   driver;
    BYTE   valid;
} ;

struct hostent;
struct sockaddr;
struct sockaddr_in;
char* NetworkSystem::GetLocalAddress(eCONNECTION_KIND connIndex, char *string)
{
    SOCKET sock = m_Connections[connIndex]->GetSocket();

    sADDRESS		addr;
    int				len;

	 __ZERO(addr);

    ((sockaddr_in *)&addr)->sin_family = AF_INET;
    addr.valid = 1;
    len			= (int)sizeof(sockaddr_in);

	 if(getsockname(sock, (sockaddr *)&addr, &len) == SOCKET_ERROR)
    {
        addr.valid = 0;
        return NULL;
    }

	 ///////////////////////////////////////
    UINT uAddress;

    hostent  *local;
    char      buff[256];

    if(gethostname(buff, 256) == SOCKET_ERROR)
        return NULL;

	 buff[256 - 1] = '\0';
    local = gethostbyname(buff);
    if(!local)
        uAddress = (UINT)htonl(0x7f000001);
    uAddress = *(UINT *)local->h_addr_list[0];


    if(((sockaddr_in *)&addr)->sin_addr.s_addr == INADDR_ANY)
    {
        ((sockaddr_in *)&addr)->sin_addr.s_addr = uAddress;
    }

    DWORD   uladdr;
    WORD  usport;

    uladdr = ntohl(((sockaddr_in *)&addr)->sin_addr.s_addr);
    usport = ntohs(((sockaddr_in *)&addr)->sin_port);

	sprintf	(string
            ,"%lu.%lu.%lu.%lu"
				,(uladdr >> 24) & 0xff
				,(uladdr >> 16) & 0xff
				,(uladdr >> 8) &  0xff
				,uladdr &         0xff);


    return string;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL NetworkSystem::WindowProc( HWND /*hWnd*/, UINT iMessage, WPARAM /*wParam*/, LPARAM lParam)
{
	if ( WM_SOCKET_NETWORKSYSTEM <= iMessage && iMessage < WM_SOCKET_NETWORKSYSTEM + CK_MAX)
	{
		int iChannel = iMessage - WM_SOCKET_NETWORKSYSTEM;

		switch ( WSAGETSELECTEVENT( lParam))
		{
			//////////////////////////////////////
		case FD_READ:
			if (m_Connections[iChannel])
			{
				BOOL bRet;
				bRet = m_Connections[iChannel]->Receive();
				if(!bRet)
					theGameUIManager.UIMessageBox(TEXTRES_DisconnecttedReason);
			}
			break;
			//////////////////////////////////////
		case FD_WRITE:
			if (m_Connections[iChannel]) 
			{
				m_Connections[iChannel]->Send_FDWrite();
			}
			break;

			//////////////////////////////////////
		case FD_CLOSE:
			{
				if (m_Connections[iChannel])
				{
					m_Connections[iChannel]->Disconnect();
				}
				m_netProcDisconnect( iChannel );
			}
			break;

			//////////////////////////////////////
		case FD_CONNECT:
			break;
		}
	}

	return ( FALSE);
}



void   NetworkSystem::SetEnableSendHeartBeat(BOOL bEnable)
{

	if(!bEnable)
	{
		m_tmHeartBeat.DisableCheckTime();
	}
	else
	{
		const DWORD dwTickBeat = theGeneralGameParam.GetTickHeartBeat();

		m_tmHeartBeat.SetTimer(dwTickBeat);


	}
}

void   NetworkSystem::SetEnableSendChatHeartBeat(BOOL bEnable)
{
	if(!bEnable)
	{
		m_tmChatHeartBeat.DisableCheckTime();
	}
	else
	{
		const DWORD dwTickBeat = theGeneralGameParam.GetTickHeartBeat();

		m_tmChatHeartBeat.SetTimer(dwTickBeat);


	}
}


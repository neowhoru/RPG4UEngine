/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillStorageManager.cpp
创建日期：2008年5月20日
最后更新：2008年5月20日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillStorageManager.h"
#include "SkillInfoParser.h"
#include "SkillStorageParser.h"
#include "SkillPaneSlot.h"
#include "SlotKeyGenerator.h"
#include "SlotUIListener.h"
#include "UISkillContainer.h"
#include "PlayerAttributes.h"
#include "PacketInclude.h"
#include "SkillDependenceParser.h"
#include "SkillStorageOpr.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SkillStorageManager, ()  , gamemain::eInstPrioGameFunc);


//------------------------------------------------------------------------------
SkillStorageManager::SkillStorageManager()
{
	m_nSkillCategory			= SKILL_KIND_ACTIVE1;
	m_PlayerType				= PLAYERTYPE_WARRIOR;
	m_pUISkillActive1			= NULL;
	m_pUISkillActive2			= NULL;
	m_pUISkillPassive			= NULL;
	m_pUISkillStyle			= NULL;
	m_pUISkillAction			= NULL;
	m_pUISkillEmotion			= NULL;
	m_pUISkillActive2Shot	= NULL;
	m_pSkillStorage			= NULL;
}

//------------------------------------------------------------------------------
SkillStorageManager::~SkillStorageManager()
{
	Release();
}

//------------------------------------------------------------------------------
BOOL SkillStorageManager::Init(ePLAYER_TYPE	charType)
{
	__CHECK_RANGE2(charType, PLAYERTYPE_BASE, PLAYERTYPE_MAX);

	m_PlayerType						= charType;




	for(INT n=0; n< SKILL_KIND_MAX; n++)
	{
		m_arContainers[n] = new UISkillContainer;
		m_arContainers[n]->SetSlotIndex		(SI_SKILL);
		m_arContainers[n]->SetSubSlotIdx		((SLOTINDEX)n);
	}

	m_pUISkillSlotContainer = m_pUISkillActive1;
	m_pSkillStorage			= &m_arSkillInfoActive1;

	LoadFromInventoryStore();
	return TRUE;
}

//------------------------------------------------------------------------------
void SkillStorageManager::Release()
{
	if(m_arContainers[0])
	{
		for(INT n=0; n< SKILL_KIND_MAX; n++)
		{
			m_arContainers[n]->Release();
			SAFE_DELETE(m_arContainers[n]	);
		}
	}

}


//------------------------------------------------------------------------------
void SkillStorageManager::Clear()
{
	m_arSkillInfoActive1.clear();
	//if (m_PlayerType == PLAYERTYPE_STABBER)
	m_pUISkillActive2Shot->Clear();
	//else
	m_pUISkillActive2->Clear();

	m_arSkillInfoPassive.clear();
	m_arSkillInfoStyle.clear();
	m_arSkillInfoEmotion.clear();
	m_arSkillInfoAction.clear();
}


class LoadFromInventoryStoreOpr
{
public:
	SkillStorageManager*	m_pSkillManager;
	BOOL operator()(sSKILL_STORAGEINFO&	info)
	{
		///////////////////////////////////////////////
		if (info.m_RequirePlayerType != m_pSkillManager->GetPlayerType())
			return TRUE;

		SkillStorageOpr	opr(info.m_SkillType, m_pSkillManager);

		opr.LoadInfo(&info);
		return TRUE;

	}
};
//------------------------------------------------------------------------------
void SkillStorageManager::LoadFromInventoryStore()
{
	if (m_bOnceInitSkillInfos)
		return;

	Clear();

	LoadFromInventoryStoreOpr	opr={this};
	theSkillStorageParser.ForEach2(opr);
}

void SkillStorageManager::ResetLevelUpInfo()
{
	ResetLevelUpInfo_Active1	();
	ResetLevelUpInfo_Active2	();
	ResetLevelUpInfo_Passive	();
	ResetLevelUpInfo_Style		();
}

//------------------------------------------------------------------------------


void SkillStorageManager::_SetPageSize(sSKILL_STORAGEINFO * pSkillInven)
{
	SkillStorageArray*	pArray(NULL);

	pArray = _GetPage(pSkillInven);
	////////////////////////////////////////////
	if(pArray)
	{
		assert(pArray->size() == 0);
		pArray->resize(pSkillInven->m_SkillCount);
	}
}

//------------------------------------------------------------------------------
SkillStorageArray* SkillStorageManager::_GetPage	(sSKILL_STORAGEINFO * pSkillInven)
{
	if(!pSkillInven)
		return NULL;

	SkillStorageOpr	opr(pSkillInven->m_SkillType, this);
	return opr.m_parInfos;


}


SkillStorageInfo* SkillStorageManager::_SetPageSkill	(sSKILL_STORAGEINFO * pSkillInven
																		,UINT   nAt
																		,const SkillStorageInfo& skillInfo)
{
	SkillStorageArray*	pArray(NULL);

	pArray = _GetPage(pSkillInven);
	////////////////////////////////////////////
	if(pArray)
	{
		__CHECK2_RANGE(nAt,pArray->size(),NULL);
		(*pArray)[nAt] = skillInfo;
		return  &(*pArray)[nAt];
	}
	return NULL;
}






//------------------------------------------------------------------------------
void SkillStorageManager::SlotUpdate(const SKILL_STREAM& stream)
{
	sSKILLINFO_COMMON *	pInfo;
	SkillPaneSlot			postSlot(stream);

	__CHECK2(postSlot.GetCode() != INVALID_CODETYPE,; );

	pInfo = theSkillInfoParser.GetInfo(postSlot.GetCode());
	__CHECK2_PTR(pInfo,);

	///////////////////////////////////////////
	SlotUpdate_Active1	(postSlot, pInfo);
	SlotUpdate_Active2	(postSlot, pInfo);
	SlotUpdate_Passive	(postSlot, pInfo);
	SlotUpdate_Style		(postSlot, pInfo);


}

//------------------------------------------------------------------------------
void SkillStorageManager::SetSkillInfos(const sTOTALINFO_SKILL& skillInfos, BOOL bAppend)
{
	sSKILLINFO_COMMON *pInfo;

	if (m_bOnceInitSkillInfos && !bAppend)
		return;

	m_bOnceInitSkillInfos = TRUE;

	const sSKILL_SLOT* pSlot = skillInfos.m_Slot;
	SLOTPOS		posFirst = 0;
	SLOTPOS		posTotal = skillInfos.m_Count;
	CODETYPE		skillCode;

	////////////////////////////////////////////
	for(SLOTPOS i=posFirst; i<posTotal; ++i)
	{
		SkillPaneSlot postSlot(pSlot[i].m_Stream);

		skillCode = postSlot.GetCode();
		if (skillCode == INVALID_CODETYPE)
			continue;

		
		pInfo = theSkillInfoParser.GetInfo((SLOTCODE)skillCode);
		if (!pInfo)
			return;

		/////////////////////////////////////////////
		sSKILLINFO_COMMON* pPostSkillInfo;
		pPostSkillInfo = 	theSkillInfoParser.GetSkillInfo((SLOTCODE)skillCode);

		if (!pPostSkillInfo)
			continue;

		SetSkillInfos_Active1(postSlot, (SkillDetailInfo*)pPostSkillInfo);
		SetSkillInfos_Active2(postSlot, (SkillDetailInfo*)pPostSkillInfo);

		SetSkillInfos_Passive(postSlot, (SkillDetailInfo*)pPostSkillInfo);
		SetSkillInfos_Style(postSlot, (sSTYLEINFO_BASE*)pPostSkillInfo);

#ifdef USE_DISALE
		if (pInfo->IsSkill())
		{
			SkillDetailInfo* pPostSkillInfo;
			pPostSkillInfo = 	theSkillInfoParser.GetSkillInfo((SLOTCODE)skillCode);

			if (!pPostSkillInfo)
				continue;

			/////////////
			BOOL bProcessed;

			if (	pPostSkillInfo->m_bySkillType == SKILL_TYPE_ACTIVE
				|| pPostSkillInfo->m_bySkillType == SKILL_TYPE_ACTIVE_INSTANT
				|| pPostSkillInfo->m_bySkillType == SKILL_TYPE_ACTIVE_DELAYED )
			{
				if (!SetSkillInfos_Active1(postSlot, pPostSkillInfo))
					SetSkillInfos_Active2(postSlot, pPostSkillInfo);
			}
			else if (pPostSkillInfo->m_bySkillType == SKILL_TYPE_PASSIVE)
			{
				SetSkillInfos_Passive(postSlot, pPostSkillInfo);
			}
		}
		////////////////////////////////////
		else if (pInfo->IsStyle())
		{
			sSTYLEINFO_BASE* pPostStyleInfo;
			pPostStyleInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)skillCode);

			if (pPostStyleInfo)
			{
				SetSkillInfos_Style(postSlot, pPostStyleInfo);
			}
		}
#endif
	}
}


//------------------------------------------------------------------------------
BOOL SkillStorageManager::IsStyleActivated(DWORD dwCode)
{
	UINT i;

	for (i=0; i<m_arSkillInfoStyle.size(); ++i)
	{
		if (m_arSkillInfoStyle[i].pCurrSkillInfo->m_SkillCode == dwCode)
		{
			return (m_arSkillInfoStyle[i].state == SLOT_UISTATE_ACTIVATED);
		}
	}

	SkillStorageInfo*	pStorage;
	pStorage = GetSkillStorage((SLOTCODE)dwCode,FALSE);
	__CHECK(pStorage);

	return pStorage->state == SLOT_UISTATE_ACTIVATED;
}


//------------------------------------------------------------------------------
SkillSlot * SkillStorageManager::GetSkillSlot(CODETYPE code, BOOL bCheckClassCode)
{
	SLOTPOS					maxPos;
	SLOTPOS					pos;
	INT						type;
	SkillPaneContainer*	pContainer;

	__CHECK2_PTR(m_arContainers[0], NULL);

	///////////////////////////////////////////////
	for (type = 0; type < SKILL_KIND_MAX; ++type)
	{
		pContainer = m_arContainers[type];

		maxPos = pContainer->GetSlotMaxSize();
		///////////////////////////////////////////
		for (pos=0; pos<maxPos; ++pos)
		{
			if (pContainer->IsEmpty(pos))
				continue;

			BaseSlot & slotDat = pContainer->GetSlot(pos);

			if(bCheckClassCode )
			{
				SkillSlot& skillSlot = (SkillSlot&)slotDat;
				if(skillSlot.GetSkillClassCode() == code)
					return (SkillSlot *)&slotDat;
			}
			else
			{
				if ( code == slotDat.GetCode() )
					return (SkillSlot *)&slotDat;
			}
		}
	}
	return NULL;
}

//------------------------------------------------------------------------------
SkillStorageInfo * SkillStorageManager::GetSkillStorage(SLOTCODE code, BOOL bCheckClassCode)
{
	///////////////////////////////////////////////
	SkillStorageInfo*	pStorage;

	for (UINT type = 0; type < SKILL_KIND_MAX; ++type)
	{
		SkillStorageOpr	opr(m_nSkillCategory, this);
		pStorage = opr.GetSkillStorage(code, bCheckClassCode);
		if(pStorage)
			return pStorage;
	}

	return NULL;
}


SkillStorageInfo* SkillStorageManager::GetSkillStorageAt	(SLOTPOS	 pos)
{
	__CHECK2(pos != INVALID_SLOTPOS,NULL);
	__CHECK2_PTR(m_pUISkillSlotContainer,NULL);

	SkillSlot & slotDat = (SkillSlot&)m_pUISkillSlotContainer->GetSlot(pos);
	__CHECK(slotDat.GetCode() != INVALID_SLOTCODE);

	///////////////////////////////////
	SkillStorageInfo*	pStorage;
	pStorage = (SkillStorageInfo*)slotDat.GetUserData();
	if(pStorage)
		return pStorage;

	///////////////////////////////////
	SkillStorageOpr	opr(m_nSkillCategory, this);
	pStorage = opr.GetSkillStorage((SLOTCODE)slotDat.GetSkillClassCode(), TRUE);

	//__CHECK_PTR(pStorage);
	//__CHECK(pStorage->state	== SLOT_UISTATE_ACTIVATED);

	return pStorage;
}

BOOL SkillStorageManager::CheckSkillCanUsing(SLOTPOS	 pos)
{
	__CHECK(singleton::ExistHero());

	//__CHECK(pos != INVALID_SLOTPOS);
	//__CHECK_PTR(m_pUISkillSlotContainer);

	//SkillSlot & slotDat = (SkillSlot&)m_pUISkillSlotContainer->GetSlot(pos);
	//__CHECK(slotDat.GetCode() != INVALID_SLOTCODE);


	///////////////////////////////////////
	SkillStorageInfo*	pStorage;
	//SkillStorageOpr	opr(m_nSkillCategory, this);
	//pStorage = opr.GetSkillStorage((SLOTCODE)slotDat.GetSkillClassCode());
	pStorage	= GetSkillStorageAt(pos);

	__CHECK_PTR(pStorage);
	__CHECK(pStorage->state	== SLOT_UISTATE_ACTIVATED);


	///////////////////////////////////////
	sPLAYERINFO_BASE*		pInfo			= theHero.GetCharInfo();
	PlayerAttributes *	pCharInfo	= theHero.GetPlayerAttributes();
	sSKILLINFO_COMMON*	pSkill		= pStorage->pCurrSkillInfo;

	__CHECK(theHero.GetLevel()			>= pSkill->m_wRequireLV);

	if(pSkill->m_dwClassDefine != -1)
	{
		__CHECK(pInfo->m_byClassCode		== pSkill->m_dwClassDefine);
	}
	
	if(pSkill->IsSkill())
	{
		SkillDetailInfo*	pSkill2 = (SkillDetailInfo*)pSkill;
		switch(m_nSkillCategory)
		{
		case SKILL_KIND_ACTIVE1:
			__CHECK(pCharInfo->GetExperty1()	>= pSkill2->m_wRequireSkillStat[0]);
			break;
		case SKILL_KIND_ACTIVE2:
			__CHECK(pCharInfo->GetExperty2()	>= pSkill2->m_wRequireSkillStat[1]);
			break;
		}
	}

	return TRUE;
}


SLOTCODE SkillStorageManager::GetLevelUpSkillCode(SLOTPOS pos)
{
	__CHECK2(singleton::ExistHero(),INVALID_SLOTCODE);


	__CHECK2(pos != INVALID_SLOTPOS,INVALID_SLOTCODE);
	__CHECK2_PTR(m_pUISkillSlotContainer,INVALID_SLOTCODE);

	SkillSlot & slotDat = (SkillSlot&)m_pUISkillSlotContainer->GetSlot(pos);

	return slotDat.GetCode();

	/////////////////////////////////////////
	//SkillStorageInfo*	pStorage;
	//pStorage	= GetSkillStorageAt(pos);

	//__CHECK_PTR(pStorage);

	/////////////////////////////////////////
	//__CHECK(CheckSkillValid(pStorage->pCurrSkillInfo, m_nSkillCategory ));

	//skillCodeRet	= pStorage->pCurrSkillInfo->m_SkillCode;
	//return TRUE;
}


BOOL SkillStorageManager::CheckSkillDependence	(CODETYPE code)
{
	sSKILL_DEPENDENCEC*	pDenpend;
	pDenpend = theSkillDependenceParser.GetDependence(code);
	if(!pDenpend)
		return TRUE;

	//SkillSlot*			pSkillSlot;
	SLOTCODE				dwSkill;
	SkillStorageInfo*	pStorage;

	SkillStorageOpr	opr(m_nSkillCategory,this);

	for(INT n=0; n<MAX_SKILL_DEPENDENCE;n++)
	{
		dwSkill = pDenpend->m_arDependences[n];
		if(dwSkill == 0)
			continue;
		if(theSkillInfoParser.GetInfo(dwSkill) == NULL)
			continue;

		//pSkillSlot = GetSkillSlot(dwSkill, TRUE);
		//if(pSkillSlot == NULL)
		//	return FALSE;

		pStorage = opr.GetSkillStorage(dwSkill, TRUE);
		if(pStorage == NULL)
			return FALSE;
		if(pStorage->state != SLOT_UISTATE_ACTIVATED)
			return FALSE;
	}
	return TRUE;
}


BOOL SkillStorageManager::CheckSkillValid		(sSKILLINFO_COMMON*	pSkill,DWORD /*dwSkillKind*/)
{
	__CHECK(singleton::ExistHero());
	__CHECK_PTR(pSkill);

	sPLAYERINFO_BASE*	pInfo = theHero.GetCharInfo();
	PlayerAttributes *pCharInfo = theHero.GetPlayerAttributes();

	__CHECK(pInfo->m_dwRemainSkill	>= pSkill->m_byRequireSkillPoint);
	__CHECK(theHero.GetLevel()			>= pSkill->m_wRequireLV);

	if(pSkill->m_dwClassDefine != -1)
	{
		__CHECK(pInfo->m_byClassCode		== pSkill->m_dwClassDefine);
	}
	
	if(pSkill->IsSkill())
	{
		SkillDetailInfo*	pSkill2 = (SkillDetailInfo*)pSkill;
		//switch(dwSkillKind)
		//{
		//case SKILL_KIND_ACTIVE1:
			__CHECK(pCharInfo->GetExperty1()	>= pSkill2->m_wRequireSkillStat[0]);
		//	break;
		//case SKILL_KIND_ACTIVE2:
			__CHECK(pCharInfo->GetExperty2()	>= pSkill2->m_wRequireSkillStat[1]);
		//	break;
		//}
	}

	__CHECK(CheckSkillDependence(pSkill->m_SkillClassCode));
	return TRUE;
}

BOOL SkillStorageManager::CheckNextSkillValid	(sSKILLINFO_COMMON*	pSkill,DWORD /*dwSkillKind*/)
{
	__CHECK(singleton::ExistHero());
	__CHECK_PTR(pSkill);

	//sPLAYERINFO_BASE*	pInfo			= theHero.GetCharInfo();
	//PlayerAttributes *pCharInfo	= theHero.GetPlayerAttributes();

	__CHECK(pSkill->m_wSkillLV <= pSkill->m_wMaxLV);

	return TRUE;
}

BOOL SkillStorageManager::CanLearnSkill(sSKILLINFO_COMMON*	pSkill,DWORD dwSkillKind)
{
	__CHECK(CheckSkillValid	(pSkill, dwSkillKind) );
	return TRUE;
}

BOOL SkillStorageManager::CanLevelUpSkill(sSKILLINFO_COMMON*	pSkill,DWORD dwSkillKind)
{
	__CHECK(CheckNextSkillValid(pSkill, dwSkillKind) );
	__CHECK(CheckSkillValid		(pSkill, dwSkillKind) );

	return TRUE;
}

BOOL SkillStorageManager::CheckSkillSlotState(SkillStorageInfo& info,DWORD dwSkillKind)
{
	__CHECK(singleton::ExistHero());
	//已经学会
	if(info.state == SLOT_UISTATE_ACTIVATED)
	{
		return CanLevelUpSkill(info.pNextSkillInfo,dwSkillKind);
	}
	return CanLearnSkill(info.pCurrSkillInfo,dwSkillKind);
}



//------------------------------------------------------------------------------
BOOL SkillStorageManager::SetUISkillContainer(SkillPaneContainer* pContainer)
{
	int i;
	for (i=0; i<SKILL_KIND_MAX; ++i)
	{
		if(pContainer == m_arContainers[i])
		{
			m_pUISkillSlotContainer = pContainer;
			return TRUE;
		}
	}
	assert(!"非SkillContainer...");
	return FALSE;
}

//------------------------------------------------------------------------------
BOOL SkillStorageManager::SetUISkillContainer(eSKILL_CATEGORY containerType)
{
	__CHECK(containerType >= 0 && containerType< SKILL_KIND_MAX);

	m_nSkillCategory			= containerType;
	m_pUISkillSlotContainer = m_arContainers[containerType];

	SkillStorageOpr	opr(containerType,this);
	m_pSkillStorage			= opr.m_parInfos;
	return TRUE;
}



//------------------------------------------------------------------------------
void SkillStorageManager::SendSelectSkillPointMsg(MSG_CG_SKILL_SELECT_SKILLPOINT_SYN& sync)
{


	theNetworkLayer.SendPacket(CK_GAMESERVER, &sync, sizeof (sync));
}




//------------------------------------------------------------------------------
void SkillStorageManager::NetworkProc( MSG_BASE * pMsg )
{
	switch( pMsg->m_byCategory )
	{
	case CG_SKILL:
		{
			switch (pMsg->m_byProtocol)
			{
				/////////////////////////////////////////////
			case CG_SKILL_SELECT_SKILLPOINT_ACK:
				{
					MSG_CG_SKILL_SELECT_SKILLPOINT_ACK *	pMsgRecv;
					sPLAYERINFO_BASE *							pPlayerInfo;

					pMsgRecv = (MSG_CG_SKILL_SELECT_SKILLPOINT_ACK *)pMsg;

					pPlayerInfo = theHero.GetCharInfo();
					pPlayerInfo->m_dwRemainSkill = pMsgRecv->m_dwRemainSkillPoint;

					SlotUpdate(pMsgRecv->m_NewSkillSlot.m_Stream);
					theGameUIManager.UIRefresh(gameui::eUISkill);
					theGameUIManager.UIRefresh(gameui::eUIActionUI);
				}
				break;

			case CG_SKILL_SELECT_SKILLPOINT_NAK:
				{
					MSG_CG_SKILL_SELECT_SKILLPOINT_NAK *pMsgRecv = (MSG_CG_SKILL_SELECT_SKILLPOINT_NAK *)pMsg;
					theHero.ProcessPlayerError(pMsgRecv->m_byErrorCode,CG_SKILL);
				}
				break;
			}
		}
		break;
	}
}

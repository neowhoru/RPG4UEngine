/*////////////////////////////////////////////////////////////////////////
文 件 名：MapLoadingHandle.cpp
创建日期：2007年10月19日
最后更新：2007年10月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "MapLoadingHandle.h"
#include "V3DGameWorld.h"
#include "ClientMain.h"
#include "VHero.h"
#include "MathInc.h"
#include "Map.h"

const float LAODING_FIHISHED_RATE	= 0.3f;
const float LOADING_STEPRATE			= 0.1f;
const float LOADING_STEP_NUMBER		= 50.0f;
const float LOADING_STEPNORMAL		= 1.0f/5000.f;
const float LOADING_STEPFAST			= 1.0f/30.f;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MapLoadingHandle::MapLoadingHandle()
{
	m_loadingThread.SetLoadingHandle(this);
	m_fProgressDisplay = 0.0f;
	m_fProgressFromMap = 0.0f;
	//m_fJumpToX = 0.0f ;
	//m_fJumpToY = 0.0f;

	m_fStepNormal		= LOADING_STEPNORMAL;
	m_fStepBackup		= LOADING_STEPNORMAL;
	m_bChangeSpeed		= FALSE;	

	m_MapID				= (MAPCODE)-1;
	m_FieldID			= (FIELDID)-1;
	m_szLandName[0]	= 0;
	m_nPosX				= 0 ;
	m_nPosY				= 0 ;
	m_pPathGameMap		= NULL;
	m_pMap				= NULL;
}

MapLoadingHandle::~MapLoadingHandle()
{
}

BOOL MapLoadingHandle::FrameMove(DWORD /*dwTick*/)
{
	float fDist			= (m_fProgressFromMap - m_fProgressDisplay);
	if(fDist != 0.f)
	{
		float fSpeed		= fDist/LOADING_STEP_NUMBER;
		//已经完成，加快脚本步
		if(m_fProgressFromMap == 1.0f)
		{
			if(m_fProgressDisplay >= m_fProgressFromMap)
			{
				m_fProgressDisplay	= 1.0f;
				m_fStepNormal			= 0.f;
				return TRUE;
			}
			else
			{
				if(!m_bChangeSpeed)
				{
					m_bChangeSpeed	= TRUE;
					//float fGap			= fSpeed - fDist * LAODING_FIHISHED_RATE;
					m_fStepBackup		= fDist * LAODING_FIHISHED_RATE;
				}
				if(m_fStepBackup > 0)
					m_fStepNormal	+=  m_fStepBackup * LOADING_STEPRATE;
			}
		}
		else
		{
			if( m_fProgressDisplay < m_fProgressFromMap )
			{	
				float fGap			= fSpeed - m_fStepNormal;
				m_fStepNormal		+=  fGap * LOADING_STEPRATE;
			}
			else
			{
				m_fStepNormal		-=  fSpeed * LOADING_STEPRATE;
			}
		}

		if(fDist <= m_fStepNormal)
			m_fProgressDisplay = m_fProgressFromMap;
		else
			m_fProgressDisplay += m_fStepNormal;
	}

	return TRUE;
}


void MapLoadingHandle::InitMapLoading(Map* pMap)
{
	m_fProgressFromMap = 0.0f;
	m_fProgressDisplay = 0.0f;
	m_bChangeSpeed		 = FALSE;
	m_pMap				 = pMap;	
}

void MapLoadingHandle::BeginMapLoading()
{
	m_loadingThread.StartThread();
}


void MapLoadingHandle::LoadMap( LPCSTR szLand, int nSizeX, int nSizeY)
{
	SetLandName(szLand);
	m_nPosX			= nSizeX;
	m_nPosY			= nSizeY;
	m_MapID			= (MAPCODE)-1;
}

void MapLoadingHandle::LoadMap(MAPCODE  dwMapId
                              ,FIELDID  dwFieldId
										,int      x
										,int      y
										,PathGameMap* pMap
										,int      /*nGameMapId*/)
{
	m_MapID		= dwMapId;
	m_FieldID	= dwFieldId;
	m_nPosX		= x;
	m_nPosY		= y;
	m_pPathGameMap	= pMap;
}

BOOL MapLoadingHandle::LoadMapProcess()
{
	assert(m_pMap);
	m_fProgressDisplay = 0;

	Vector3D	vStart((REAL)m_nPosX, (REAL)m_nPosY, 0);

	if(m_MapID	== (MAPCODE)-1)
	{
		m_pMap->LoadLand	(m_szLandName , vStart );
	}
	else
	{
		m_pMap->Load	( m_MapID,m_FieldID, vStart );
	}
	return TRUE;
}

void MapLoadingHandle::OnReadyLoading(UINT nMax)
{
	util::CriticalSectionHandle	sync_cs(m_mapLoadingLocker);
	m_nProgressMax	=	nMax;
}

void MapLoadingHandle::OnProcessLoading(UINT nProgress)
{
	util::CriticalSectionHandle	sync_cs(m_mapLoadingLocker);
	m_fProgressFromMap	= (float)nProgress/(float)m_nProgressMax;
}

void MapLoadingHandle::OnEndLoading()
{
	util::CriticalSectionHandle	sync_cs(m_mapLoadingLocker);
	m_fProgressFromMap = 1.0f;
	theMap.OnLoadingFinished();
}

BOOL MapLoadingHandle::IsLoadingFinished()
{
	if(m_fProgressDisplay == m_fProgressFromMap &&  m_fProgressFromMap == 1.0f)
		return TRUE;
	return FALSE;
}


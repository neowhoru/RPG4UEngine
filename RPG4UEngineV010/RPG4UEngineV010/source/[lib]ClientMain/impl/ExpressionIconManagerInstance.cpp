
#include "stdafx.h"
#include "ExpressionIconManagerInstance.h"
#include "V3DInclude.h"
#include "TableTextFile.h"
#include "UIPictureCtrl.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ExpressionIconManagerInstance, ()  , gamemain::eInstPrioGameFunc);


CONSTARRAY_IMPL(ExpType,eEXPTYPE_MAX)
{
	 ""			//
	,"Exclaim"	//(吃惊)
	,"Nothing"	//(不理人)
	,"How"		//(无奈)
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ExpressionIconManagerInstance::ExpressionIconManagerInstance()
{
	m_pExpressionFrame = NULL;


}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ExpressionIconManagerInstance::~ExpressionIconManagerInstance()
{
	assert(m_pExpressionFrame == NULL);
	//Release();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void ExpressionIconManagerInstance::Release()
{
	SAFE_DELETE(m_pExpressionFrame);
	for(int n=0; n<eEXPTYPE_MAX; n++)
	{
		for( ExpInfoListIt it=m_Datas[n].begin(); it!=m_Datas[n].end(); it++ )
		{
			SAFE_DELETE( (*it).pPic );
		}
		m_Datas[n].clear();
	}
}



void ExpressionIconManagerInstance::DisplayExpression	(INT   nID
																		,INT   nX
																		,INT   nY
																		,DWORD dwStartTime)
{
	if (nID < 0)
		return;

	RECT rcDst=	{nX+3
					,nY+5
					,nX+EXPRESSION_TILE_SIZE+3
					,nY+EXPRESSION_TILE_SIZE+5
					};

	for( int n=0; n<eEXPTYPE_MAX; n++ )
	{
		for( ExpInfoListIt it=m_Datas[n].begin(); it!=m_Datas[n].end(); it++ )
		{
			sEXP_INFO&	expInfo = (*it);
			if ( expInfo.nID != nID )
				continue;
			_StepIcon(expInfo ,dwStartTime,rcDst);
			return;
		}
	}

	/////////////////////////////////////////
	// 如果没有找到,则new
	sEXP_INFO infoDat;
	infoDat.nID			= nID;
	infoDat.nSize		= EXPRESSION_TILE_SIZE;
	infoDat.nCurFrame	= 0;
	infoDat.pPic		= NULL;

	for( ExpPicListIt itPic=m_PicNames.begin(); itPic!=m_PicNames.end(); itPic++ )
	{
		sEXP_PIC& expPic = (*itPic);
		if ( expPic.nID != nID )
			continue;
		
		_StepPic(expPic, infoDat);
		m_Datas[expPic.enType].push_back( infoDat );
		return;
	}

	m_Datas[0].push_back( infoDat );
}


BOOL	ExpressionIconManagerInstance::_StepPic	(sEXP_PIC&  expPic, sEXP_INFO& infoDat)
{
	infoDat.nPlayType	= expPic.nPlayType;
	infoDat.pPic		= new UIPicture;

	if ( infoDat.pPic->CreateTexture((LPCSTR) expPic.strName ) == -1 )
	{
		delete infoDat.pPic;
		infoDat.pPic = NULL;
		assert(false&&"找不到文件!");
	}
	else
	{
		infoDat.pPic->SetRealPicSize( EXPRESSION_PIC_SIZE, EXPRESSION_PIC_SIZE );
	}

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL ExpressionIconManagerInstance::Load( LPCSTR szFileName )
{
	util::TableTextFile	ini;

	__CHECK(ini.OpenFile(szFileName, TRUE));
	sEXP_PIC	stName;
	LPCSTR		szType;

	for(;ini.GotoNextLine();)
	{
		////	表情ID	类型	文件名	显示类型
		ini.GetInt();
		ini >> stName.nID;
		ini >> szType;
				stName.enType = GetExpTypeValue(szType);

		ini >> stName.strName;
		ini >> stName.nPlayType;

		m_PicNames.push_back( stName );
	}

	return TRUE;

}




/*////////////////////////////////////////////////////////////////////////
文 件 名：tooltipmanager.cpp
创建日期：2007年8月9日
最后更新：2007年8月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ToolTipManagerInstance.h"
#include "ItemSlot.h"
#include "CursorHandler.h"
#include "ItemInfoParser.h"
#include "QuickPaneSlot.h"
#include "ItemManager.h"
#include "SkillInfoParser.h"
#include "BaseContainer.h"
#include "StyleSlot.h"
#include "Hero.h"
#include "MouseHandler.h"
#include "HeroTipLayer.h"
#include "StateInfoParser.h"
#include "ObjectManager.h"
#include "SkillEffect.h"
#include "TextResManager.h"
#include "ColorManager.h"
#include "ItemSlot.h"
#include "FormularManager.h"
#include "PlayerAttributes.h"
#include "SkillSlot.h"
#include "MacroTextParser.h"
#include "DataChunker.h"
#include "TableTextFile.h"
#include "ConstTextRes.h"

const DWORD			NAME_LINEHEIGHT	= 24 + 2;
const DWORD			TITLE_LINEHEIGHT	= 15 + 2;
const DWORD			NORMAL_LINEHEIGHT = 13 + 2;
const DWORD			SPACE_LINEHEIGHT	= 13;

const IDTYPE		FONTTYPE_8	= 0;
const IDTYPE		FONTTYPE_10	= 0;
const IDTYPE		FONTTYPE_12	= 0;
const IDTYPE		FONTTYPE_14	= 0;

using namespace std;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
LPCSTR ToolTipManagerInstance::GetItemDesc( CODETYPE ItemCode )
{
	sVITEMINFO_BASE*	pVItem = theItemInfoParser.GetVItemInfo(ItemCode);
	if(pVItem)
	{
		return (LPCSTR)theTextResManager.GetDesc(pVItem->m_DescID);
	}
	return (LPCSTR)_V("");
}


BOOL ToolTipManagerInstance::GetItemDesc( CODETYPE ItemCode, TCHAR * OUT szNormalText, int bufsize )
{
	__UNUSED(bufsize);
	ASSERT(bufsize <= MAX_UITEXT_LENGTH && bufsize > 0);
	szNormalText[0] = 0;
	sVITEMINFO_BASE*	pVItem = theItemInfoParser.GetVItemInfo(ItemCode);
	if(pVItem)
	{
		LPCSTR szDesc = theTextResManager.GetDesc(pVItem->m_DescID);
		if(szDesc && szDesc[0])
			strcpy( szNormalText, szDesc );
	}
	return TRUE;
}


LPCSTR ToolTipManagerInstance::GetItemName( CODETYPE ItemCode )
{
	sITEMINFO_BASE*	pItem = theItemInfoParser.GetItemInfo(ItemCode);
	if(pItem)
	{
		return (LPCSTR)pItem->m_ItemName;
	}
	return (LPCSTR)_V("");
}


BOOL ToolTipManagerInstance::GetItemName( CODETYPE ItemCode, TCHAR * OUT szNormalText, int bufsize )
{
	__UNUSED(bufsize);
	ASSERT(bufsize <= MAX_UITEXT_LENGTH && bufsize > 0);
	szNormalText[0] = 0;
	sITEMINFO_BASE*	pItem = theItemInfoParser.GetItemInfo(ItemCode);
	if(pItem)
	{
		strcpy( szNormalText, (LPCSTR)pItem->m_ItemName );
	}

	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void  ToolTipManagerInstance::BeginSlotRegister(const BaseSlot *pSlot)
{
	if (!pSlot)
	{
		ASSERT(0);
		return;
	}
	
	switch(pSlot->GetSlotType())
	{
		//////////////////////////////////////
	case ST_ITEM:
		{
			ItemSlot* pItemSlot = (ItemSlot*)pSlot;
			_RegisterByToken(pItemSlot);
			//if(pItemSlot->IsShopItem())
			//	RegisterItemslotInfoTooltipForShop(pSlot);
			//else
			//	RegisterItemslotInfoTooltip(pSlot);
		}
		break;

		//////////////////////////////////////
	case ST_ITEMHANDLE:
		{
			ItemSlot* pItemSlot = (ItemSlot*)pSlot;
			_RegisterByToken(pItemSlot);
		}
		break;

		//////////////////////////////////////
	case ST_SKILL:
		{
			SkillSlot* pSkillSlot = (SkillSlot*)pSlot;
			_RegisterByToken(pSkillSlot);

		}
		break;

		//////////////////////////////////////
	case ST_STYLE_QUICK:
		{
			theCursorHandler.ChangeTo( cursor::CURSORTYPE_USE );
			_RegisterStyleQuickSlot(pSlot);
		}
		break;

		//////////////////////////////////////
	case ST_QUICK:
		{
			theCursorHandler.ChangeTo( cursor::CURSORTYPE_USE );
			_RegisterQuickSlot(pSlot);	
		}
		break;

	}
}










void ToolTipManagerInstance::_RegisterQuickSlot(const BaseSlot *pSlot)
{
	ASSERT(pSlot);
	ASSERT(ST_QUICK == pSlot->GetSlotType());

	if (!pSlot)
		return;

	QuickPaneSlot * pQuickSlot = (QuickPaneSlot * )pSlot;


	switch (pQuickSlot->GetOrgSlotType())
	{
	case ST_ITEM:
		{	
			BaseContainer * pFromContainer = theItemManager.GetContainer(pQuickSlot->GetOrgSlotIndex());
			if (!pFromContainer)
			{
				ASSERT(pFromContainer);
				return;
			}

			BaseSlot & slotDat = pFromContainer->GetSlot(pQuickSlot->GetOrgPos());
			if (slotDat.GetCode() != pQuickSlot->GetOrgCode())
				return;

			switch (pQuickSlot->GetStatus())
			{
			case eQUICK_ACTIVATE:
				{
					_RegisterByToken(&slotDat);
					//RegisterItemslotInfoTooltipForQuick(&slotDat);
				}
				break;
			case eQUICK_DEACTIVATE:
				{
					_RegisterByToken(&slotDat);
					//RegisterItemslotInfoTooltipForQuickInDeactive(pQuickSlot);
				}
				break;
			}		
		}
		break;

	case ST_SKILL:
		{
			_RegisterByToken(pQuickSlot->GetOrgSlot());

		}
		break;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void ToolTipManagerInstance::_RegisterStyleQuickSlot(const BaseSlot *pSlot)
{
	ASSERT(pSlot);
	ASSERT(ST_STYLE_QUICK == pSlot->GetSlotType());

	if (!pSlot)
	{
		return;
	}

	StyleSlot * pStyleSlot = (StyleSlot * )pSlot;

	switch (pStyleSlot->GetOrgSlotType())
	{
	case ST_SKILL:
		{
			_RegisterByToken(pStyleSlot->GetOrgSlot());

		}
		break;

	default:
		ASSERT(!"not support type.");
		break;
	}
}


void ToolTipManagerInstance::_RegisterStyleSlot( const BaseSlot *pSlot )
{
    if(!pSlot)
	{
		ASSERT( pSlot );
        return;
	}

	ASSERT( ST_SKILL == pSlot->GetSlotType() );

	sSTYLEINFO_BASE * pStyleInfo = 
		(sSTYLEINFO_BASE *)theSkillInfoParser.GetStyleInfo(pSlot->GetCode());

	if (!pStyleInfo)
	{
		ASSERT (pStyleInfo);
        return;
	}

    DWORD _dwFlags = 0;

    // %s %d
    _dwFlags =  (	TOOLTIP_STYLE_INFO_NAME |
						TOOLTIP_STYLE_INFO_EXT_EFFECT0 | 
						TOOLTIP_STYLE_INFO_DESC0);

    _ProcessStyleTip(_dwFlags, pStyleInfo);
}




void ToolTipManagerInstance::_RegisterSkillSlot(const BaseSlot *pSlot)
{
    if(!pSlot)
        return;

    InitToolTip();

    SkillDetailInfo* pSkillInfo =
        (SkillDetailInfo *)theSkillInfoParser.GetSkillInfo(pSlot->GetCode());

    if (!pSkillInfo)
        return;


    DWORD _dwFlags = 0;

    ASSERT (pSkillInfo);

    // %s %d格式
    _dwFlags |= (	TOOLTIP_INFO_NAME 
                |	TOOLTIP_INFO_EXT_EFFECT0 
                |	TOOLTIP_INFO_DESC0);

	if( pSkillInfo->m_wSkillLV >= pSkillInfo->m_wMaxLV )
	{
		_dwFlags |= TOOLTIP_INFO_MAX_SKILLLEVEL;
	}

    _ProcessSkillTip(_dwFlags, pSkillInfo);
}




void ToolTipManagerInstance::BeginSkillEffectRegister(SkillEffect *pImageRef)
{
	if(!pImageRef)
		return;

	//ClearTokens();
	TCHAR szFormatText[512]	={0};
	TCHAR szNormalText[512]	={0};
	TCHAR szParam[32]			={0};
	TCHAR szParam2[32]		={0};


	InitToolTip();

	COLOR		clrNameColor		= _COLOR( COLOR_NAME);
	//COLOR		clrTitleColor		= _COLOR( COLOR_TITLE);
	//COLOR		clrNormalColor		= _COLOR( COLOR_NORMAL);
	//COLOR		clrSpaceColor		= _COLOR( COLOR_SPACE);
	//COLOR		clrErrorColor		= _COLOR( COLOR_ERROR);


	SKILL_EFFECTDAT&	effectInfo	= pImageRef->GetEffectData();
	sSTATEINFO_BASE *	pInfo;
	
	pInfo = theStateInfoParser.GetStateInfo(effectInfo.dwStatusID);
	__CHECK2_PTR(pInfo,);

	Character *			pChr			= NULL;
	SKILL_EFFECTDAT *	pEffect		= NULL;
	DWORD					dwRemainTime	= 0;
	DWORD 				dwRemainSecond = 0;
	DWORD 				dwRemainMinute = 0;
	int   				iParam			= 0;


	pChr = (Character *)pImageRef->GetOwner();
	if (pChr)
		pEffect = pChr->GetSkillEffectData(effectInfo.iEffectID);
			
	if (pEffect) 
	{
		dwRemainTime	= pEffect->iRemainTime / 1000;
		dwRemainSecond = dwRemainTime % 60;
		dwRemainMinute = dwRemainTime / 60;
		iParam			= pEffect->AbilityInfo.m_iParam[1];
	}


	GetItemName(pInfo->m_VItemCode,szNormalText,512);
	AddToolTip	(szNormalText
               ,-1
					,NAME_LINEHEIGHT
					,FONTTYPE_14
					,clrNameColor
					,TOOLTIP_BG_COLOR);
	

	GetItemDesc	(pInfo->m_VItemCode,szFormatText,512);
	__ZERO(szNormalText);

	//////////////////////////////////////////////
	switch(effectInfo.dwStatusID) 
	{
	 default:
	 case CHARSTATE_CHAOS:
	 case CHARSTATE_BLIND:
	 case CHARSTATE_DOWN:
	 case CHARSTATE_DELAY:
	 case CHARSTATE_SEALING:
	 case CHARSTATE_STUN:
	 case CHARSTATE_STONE:
	 case CHARSTATE_SLEEP:
	 case CHARSTATE_FROZEN:
	 case CHARSTATE_SEQUELA:
	 case CHARSTATE_REFLECT_FEAR:
	 case CHARSTATE_PROTECTION:
	 case CHARSTATE_HOLDING:
	 case CHARSTATE_TRANSFORMATION:
	 case CHARSTATE_STEEL :
	 case CHARSTATE_TRANSPARENT:
	 case CHARSTATE_FEAR:
	 case CHARSTATE_BLUR:
		 {
			StrCopy(szNormalText,szFormatText);
		 }
		 break;

	 // Option 1
	 case CHARSTATE_REFLECT_STUN:
	 case CHARSTATE_REFLECT_FROZEN:
	 case CHARSTATE_REFLECT_DAMAGE:
		 {
			 Sprintf(szParam,_T("%ld "),pEffect->AbilityInfo.m_iOption1);
			 Sprintf(szNormalText,szFormatText,szParam);
		 }
		 break;


     // Parameter1
	 case CHARSTATE_REFLECT_SLOW:
	 case CHARSTATE_REFLECT_SLOWDOWN:
		 {
			 Sprintf(szParam,_T("%ld "),pEffect->AbilityInfo.m_iParam[0]);
			 Sprintf(szNormalText,szFormatText,szParam);

		 }
		 break;


	 // Parameter2 Param1
	 case CHARSTATE_POISON :
	 case CHARSTATE_WOUND :
	 case CHARSTATE_FIRE_WOUND :
	 case CHARSTATE_FETTER :
	 case CHARSTATE_SLOWDOWN :
	 case CHARSTATE_HIGH_SPIRIT :
	 case CHARSTATE_SPEEDING :
	 case CHARSTATE_INCREASE_SKILLRANGE :
	 case CHARSTATE_PRECISION :
	 case CHARSTATE_BUF_RANGE_DAMAGE:
		 {
			 Sprintf(szParam,_T("%ld "),pEffect->AbilityInfo.m_iParam[1]);
			 Sprintf(szNormalText,szFormatText,szParam);

		 }
		 break;	


	 // Parameter2 Param1
	 case CHARSTATE_PANIC:
	 case CHARSTATE_LOW_SPIRIT :
	 case CHARSTATE_WEAKENING :
	 case CHARSTATE_ATTACK_FAIL:
	 case CHARSTATE_FRUSTRATION:
	 case CHARSTATE_HP_REDUCE:
	 case CHARSTATE_MP_REDUCE:
	 case CHARSTATE_LOW_STRENGTH:
	 case CHARSTATE_DICLINE:
	 case CHARSTATE_MAGIC_EXPOSE:
	 case CHARSTATE_ABSORB:
	 case CHARSTATE_HPRATE_REDUCE:
	 case CHARSTATE_MPRATE_REDUCE:
	 case CHARSTATE_ANGER:
	 case CHARSTATE_DEFENSE:
	 case CHARSTATE_FIGHTING:
	 case CHARSTATE_BALANCE:
	 case CHARSTATE_VITAL_POWER:
	 case CHARSTATE_MEDITATION:
	 case CHARSTATE_CONCENTRATION:
	 case CHARSTATE_HP_INCREASE:
	 case CHARSTATE_MP_INCREASE:
	 case CHARSTATE_HPRATE_INCREASE:
	 case CHARSTATE_MPRATE_INCREASE:
	 case CHARSTATE_MAGIC_DEFENSE:		 
	 case CHARSTATE_STAT_STR:
	 case CHARSTATE_STAT_DEX:
	 case CHARSTATE_STAT_VIT:
	 case CHARSTATE_STAT_INT:
	 case CHARSTATE_STAT_SPI:
	 case CHARSTATE_MAGIC_ATTACK_INCREASE:
	 case CHARSTATE_MAGIC_ATTACK_DECRESE:
	 case CHARSTATE_STAT_STR_DECRESE:
	 case CHARSTATE_STAT_DEX_DECRESE:
	 case CHARSTATE_STAT_VIT_DECRESE:
	 case CHARSTATE_STAT_INT_DECRESE:
	 case CHARSTATE_STAT_SPI_DECRESE:
		 {
			 if (pEffect->AbilityInfo.m_iParam[0] == 1) 
			 {
				Sprintf(szParam,_T("%ld "),pEffect->AbilityInfo.m_iParam[1]);
			 }
			 else
			 {
				 Sprintf(szParam,_T("%ld%% "),pEffect->AbilityInfo.m_iParam[1]);

			 }
			 Sprintf(szNormalText,szFormatText,szParam);

		 }
		 break;

	 case CHARSTATE_SP_BONUS:
	 // Parameter1,Parameter2
		 {
			 Sprintf(szParam,_T("%ld "),pEffect->AbilityInfo.m_iParam[0]);
			 Sprintf(szParam2,_T("%ld "),pEffect->AbilityInfo.m_iParam[1]);
			 Sprintf(szNormalText,szFormatText,szParam,szParam2);
		 }
		 break;

	// Parameter2,Option2
	 case CHARSTATE_MAGIC_SHIELD:	    
		 {
			 Sprintf(szParam,_T("%ld "),pEffect->AbilityInfo.m_iParam[1]);
			 Sprintf(szParam2,_T("%ld "),pEffect->AbilityInfo.m_iOption2);
			 Sprintf(szNormalText,szFormatText,szParam,szParam2);
		 }
		 break;
	}

	////////////////////////////////////////////////
	AddToolTipML(szNormalText,_tcslen(szNormalText));



	//	格式 : %d %d
	TCHAR		szMsgText[MAX_UITEXT_LENGTH];
	LPCSTR	szTime,
				szMin,
				szSec;

	szTime	= _STRING( TEXTRES_REMAIN_TIME_TEXT);
	szMin		= _STRING( TEXTRES_MINUTE_TEXT);
	szSec		= _STRING( TEXTRES_SECOND_TEXT);

	if (dwRemainMinute > 0)
	{
		Sprintf	(szMsgText
					,_T("%s : %d%s %d%s")
					,szTime
					,dwRemainMinute
					,szMin
					,dwRemainSecond
					,szSec);
		AddToolTip( szMsgText );
	}
	else if (dwRemainTime > 0)
	{
		Sprintf( szMsgText, _T("%s : %d%s"), szTime, dwRemainSecond, szSec );
		AddToolTip( szMsgText );
		}
}



//------------------------------------------------------------------------------
void ToolTipManagerInstance::_ProcessSkillTip	(DWORD            dwFlags
																,SkillDetailInfo* pPrevSkillInfo
																,SkillDetailInfo* pNextSkillInfo
																,BOOL             bActivated)
{
	if( !singleton::ExistHero() )
		return;

	if (!pPrevSkillInfo)
	{
		ASSERT (" (pPrevSkillInfo == NULL)");
		return;
	}

	InitToolTip();


	int		i = 0;
	BOOL		bCheck = FALSE;


	DWORD		dwExtraStyle = 0;

	TCHAR		szNormalText[MAX_UITEXT_LENGTH], 
				szMsgText[MAX_UITEXT_LENGTH], 
				szTextBuf[MAX_UITEXT_LENGTH];

	COLOR		clrNameColor		= _COLOR( COLOR_NAME);
	COLOR		clrTitleColor		= _COLOR( COLOR_TITLE);
	COLOR		clrNormalColor		= _COLOR( COLOR_NORMAL);
	//COLOR		clrSpaceColor		= _COLOR( COLOR_SPACE);
	//COLOR		clrErrorColor		= _COLOR( COLOR_ERROR);

	//---------------------------------------------------------------------------------------------
	//		1. 技能名称
	if( dwFlags & TOOLTIP_INFO_NAME )
	{
		GetItemName( pPrevSkillInfo->m_VItemCode, szTextBuf, MAX_UITEXT_LENGTH );
		AddToolTip	(szTextBuf
                  ,-1
						,NAME_LINEHEIGHT
						,FONTTYPE_14
						,clrNameColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);

		if(	pPrevSkillInfo->m_bySkillUserType != SKILLUSER_ACTION 
			&& pPrevSkillInfo->m_bySkillUserType != SKILLUSER_EMOTICON )
		{
			BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DONOT_CHANGE_LINE );
			BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DT_RIGHT );

			//	TOOLTIP_INFO_MAX_SKILLLEVEL
			if( dwFlags & TOOLTIP_INFO_MAX_SKILLLEVEL )
			{
				GetUIText( TEXTRES_MAX_SKILL_LEVEL, szMsgText, MAX_UITEXT_LENGTH );
				AddToolTip(szMsgText
                      ,-1
							 ,1
							 ,FONTTYPE_14
							 ,clrNameColor
							 ,TOOLTIP_BG_COLOR
							 ,dwExtraStyle);
			}
			else
			{
				GetUIText( TEXTRES_LEVEL, szNormalText, MAX_UITEXT_LENGTH );
				Sprintf( szMsgText, _T("%d%s"), pPrevSkillInfo->m_wSkillLV, szNormalText );
				AddToolTip(szMsgText
                      ,-1
							 ,1
							 ,FONTTYPE_14
							 ,clrNameColor
							 ,TOOLTIP_BG_COLOR
							 ,dwExtraStyle);
			}
		}
	}
	else if (dwFlags & TOOLTIP_INFO_NAME_DISABLE)
	{
		GetItemName ( pPrevSkillInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH );
		//GetUIText( pPrevSkillInfo->m_dwSkillNcode, szMsgText, MAX_UITEXT_LENGTH );
		AddToolTip(szMsgText
                ,-1
					 ,NAME_LINEHEIGHT
					 ,FONTTYPE_14
					 ,clrNameColor
					 ,TOOLTIP_BG_COLOR
					 ,dwExtraStyle);
	}



	//---------------------------------------------------------------------------------------------
	//		2. 武器要求

	if(	pPrevSkillInfo->m_bySkillUserType != SKILLUSER_ACTION 
		&& pPrevSkillInfo->m_bySkillUserType != SKILLUSER_EMOTICON )
	{
		dwExtraStyle = 0;
		const int	iWeaponLimit = TEXTRES_WEAPON_DEFINE;//70230;
		GetUIText( iWeaponLimit, szMsgText, MAX_UITEXT_LENGTH );
		AddToolTip(szMsgText
                ,-1
					 ,NORMAL_LINEHEIGHT
					 ,FONTTYPE_10
					 ,clrTitleColor
					 ,TOOLTIP_BG_COLOR
					 ,dwExtraStyle);

		//	武器类型
		//BOOL	bSetName = FALSE;
		bCheck = FALSE;

		const int	iWeaponName = TEXTRES_EQUIP_SingleSword_0;
		szMsgText[0] = NULL;

		for( i = 0 ; i < pPrevSkillInfo->WEAPONDEFINE_NUM ; ++i )
		{
			if(	pPrevSkillInfo->m_WeaponDefines[i] <= 0  )
				continue;
			dwExtraStyle = TOOLTIP_EXTRA_INFO_DT_RIGHT;

			if(!bCheck)
				BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DONOT_CHANGE_LINE );

			GetUIText( (iWeaponName + pPrevSkillInfo->m_WeaponDefines[i]), szMsgText, MAX_UITEXT_LENGTH );

			bCheck = TRUE;
			AddToolTip		(szMsgText
                        ,-1
								,NORMAL_LINEHEIGHT
								,FONTTYPE_10
								,clrTitleColor
								,TOOLTIP_BG_COLOR
								,dwExtraStyle);
		}

		if( bCheck == FALSE )
		{
			dwExtraStyle = TOOLTIP_EXTRA_INFO_DT_RIGHT | TOOLTIP_EXTRA_INFO_DONOT_CHANGE_LINE;
			GetUIText( TEXTRES_NONE_TEXT, szMsgText, MAX_UITEXT_LENGTH );
			AddToolTip		(szMsgText
                        ,-1
								,NORMAL_LINEHEIGHT
								,FONTTYPE_10
								,clrTitleColor
								,TOOLTIP_BG_COLOR
								,dwExtraStyle);
		}


	}



	//---------------------------------------------------------------------------------------------
	//		4. MP要求
	dwExtraStyle = 0;


	if(	pPrevSkillInfo->m_bySkillUserType != SKILLUSER_ACTION 
		&& pPrevSkillInfo->m_bySkillUserType != SKILLUSER_EMOTICON )
	{
		const int		iSkillAttribute = TEXTRES_SKILL_ATTRIBUTE_0;//11235;
		GetUIText( iSkillAttribute, szNormalText, MAX_UITEXT_LENGTH);
		//GetUIText( pPrevSkillInfo->m_wSkillAttribute, szTextBuf, MAX_UITEXT_LENGTH);
		Sprintf(szMsgText, _T("%s : %s"), szNormalText, szTextBuf );
		AddToolTip(szMsgText
                ,-1
					 ,NORMAL_LINEHEIGHT
					 ,FONTTYPE_10
					 ,clrNameColor
					 ,TOOLTIP_BG_COLOR
					 ,dwExtraStyle);



		//////////////////////////////////////////
		if( pPrevSkillInfo->m_bySkillType != SKILL_TYPE_PASSIVE )
		{
			const int	iCastingTime = TEXTRES_SKILL_CASTTIME;//11155; text : %d
			float			fCoolTime = 0;
			if( pPrevSkillInfo->m_wSkillCasting > 0 )
			{
				fCoolTime = ((float)pPrevSkillInfo->m_wSkillCasting / 1000.0f);
				GetUIText( iCastingTime, szNormalText, MAX_UITEXT_LENGTH);
				Sprintf	(szMsgText, szNormalText, (int)(fCoolTime/60), fmod(fCoolTime,60.f) );
				AddToolTip(szMsgText
                      ,-1
							 ,NORMAL_LINEHEIGHT
							 ,FONTTYPE_10
							 ,clrNormalColor
							 ,TOOLTIP_BG_COLOR
							 ,dwExtraStyle);
			}


			const int		iCoolTime = TEXTRES_SKILL_COOLTIME;//70231;	//	Text : %d s
			fCoolTime = 0;
			if( pPrevSkillInfo->m_wCoolTime > 0 )
			{
				fCoolTime = ((float)pPrevSkillInfo->m_wCoolTime / 1000.0f);
			}

			GetUIText( iCoolTime, szNormalText, MAX_UITEXT_LENGTH);
			Sprintf(szMsgText, szNormalText, (int)(fCoolTime/60), fmod(fCoolTime,60.f) );
			AddToolTip(szMsgText
                   ,-1
						 ,NORMAL_LINEHEIGHT
						 ,FONTTYPE_10
						 ,clrNormalColor
						 ,TOOLTIP_BG_COLOR
						 ,dwExtraStyle);

			const int		iMPSpend = TEXTRES_SKILL_MPSPEND;
			GetUIText( iMPSpend, szNormalText, MAX_UITEXT_LENGTH);
			Sprintf(szMsgText, szNormalText, pPrevSkillInfo->m_wMPSpend );
			AddToolTip	(szMsgText
                     ,-1
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrNormalColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
		}
	}




	//---------------------------------------------------------------------------------------------
	//		6. 技能描述
	//---------------------------------------------------------------------------------------------
	if (dwFlags & TOOLTIP_INFO_DESC0)
	{
		GetItemDesc(pPrevSkillInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH);
		//GetUIText(pPrevSkillInfo->m_dwSkillDescCode, szMsgText, MAX_UITEXT_LENGTH);

		AddToolTipML(szMsgText
                  ,_tcslen(szMsgText)
						,TITLE_LINEHEIGHT
						,FONTTYPE_12
						,clrNormalColor
						,TOOLTIP_BG_COLOR
						,0);
	}
	else if (dwFlags & TOOLTIP_INFO_DESC1)
	{
		if (pNextSkillInfo)
		{

			GetItemDesc(pNextSkillInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH);
			//GetUIText(pNextSkillInfo->m_dwSkillDescCode, szMsgText, MAX_UITEXT_LENGTH);

			AddToolTipML(szMsgText
                     ,_tcslen(szMsgText)
							,TITLE_LINEHEIGHT
							,FONTTYPE_12
							,clrNormalColor
							,TOOLTIP_BG_COLOR
							,0);
		}
	}


	//---------------------------------------------------------------------------------------------
	//		8. 
	bool				bSpaceLine = false;
	const int		SKILLATTACK_DAMAGE = 1;
	const int		SKILLDAMAGE_PARAM1 = 0;

	if( pPrevSkillInfo && bActivated )
	{
		pPrevSkillInfo->SetFirst();
		sABILITYINFO_BASE	*pAbilityInfo = pPrevSkillInfo->GetNext();

		while( pAbilityInfo != NULL )
		{
			if( pAbilityInfo->m_wAbilityID == SKILLATTACK_DAMAGE )
			{
				//---------------------------------------------------------------------------------------------

				bSpaceLine = true;

				GetUIText( TEXTRES_SKILL_CURRENTDAMAGE, szNormalText, MAX_UITEXT_LENGTH );
				Sprintf( szMsgText, szNormalText, pAbilityInfo->m_iParam[SKILLDAMAGE_PARAM1] );
				AddToolTip(szMsgText
                      ,_tcslen(szMsgText)
							 ,NORMAL_LINEHEIGHT
							 ,FONTTYPE_10
							 ,clrTitleColor
							 ,TOOLTIP_BG_COLOR
							 ,0);

				break;
			}

			pAbilityInfo = pPrevSkillInfo->GetNext();
		}
	}

	//---------------------------------------------------------------------------------------------
	if( bActivated )
	{
		if(	pNextSkillInfo 
			&& pPrevSkillInfo 
			&& pPrevSkillInfo->m_wSkillLV < pPrevSkillInfo->m_wMaxLV )
		{
			pNextSkillInfo->SetFirst();
			sABILITYINFO_BASE	*pNextAbilityInfo = pNextSkillInfo->GetNext();

			while( pNextAbilityInfo != NULL )
			{
				if( pNextAbilityInfo->m_wAbilityID == SKILLATTACK_DAMAGE )
				{
					if( bSpaceLine == false )
					{
						//---------------------------------------------------------------------------------------------
					}

					GetUIText( TEXTRES_SKILL_NEXTDAMAGE, szNormalText, MAX_UITEXT_LENGTH );
					Sprintf( szMsgText, szNormalText, pNextAbilityInfo->m_iParam[SKILLDAMAGE_PARAM1] );
					AddToolTip	(szMsgText
                           ,_tcslen(szMsgText)
									,NORMAL_LINEHEIGHT
									,FONTTYPE_10
									,clrNormalColor
									,TOOLTIP_BG_COLOR
									,0);

					break;
				}

				pNextAbilityInfo = pNextSkillInfo->GetNext();
			}


		}
	}
	else
	{
		if( pPrevSkillInfo )
		{
			pPrevSkillInfo->SetFirst();
			sABILITYINFO_BASE	*pAbilityInfo = pPrevSkillInfo->GetNext();

			while( pAbilityInfo != NULL )
			{
				if( pAbilityInfo->m_wAbilityID == SKILLATTACK_DAMAGE )
				{
					if( bSpaceLine == false )
					{
						//---------------------------------------------------------------------------------------------
					}


					GetUIText( TEXTRES_SKILL_NEXTDAMAGE, szNormalText, MAX_UITEXT_LENGTH );
					Sprintf( szMsgText, szNormalText, pAbilityInfo->m_iParam[SKILLDAMAGE_PARAM1] );
					AddToolTip	(szMsgText
                           ,_tcslen(szMsgText)
									,NORMAL_LINEHEIGHT
									,FONTTYPE_10
									,clrTitleColor
									,TOOLTIP_BG_COLOR
									,0);

					break;
				}

				pAbilityInfo = pPrevSkillInfo->GetNext();
			}
		}
	}




	//---------------------------------------------------------------------------------------------
	//		10. 
	//---------------------------------------------------------------------------------------------
	dwExtraStyle = 0;
	if (dwFlags & TOOLTIP_INFO_EXT_EFFECT0)
	{
		GetUIText( TEXTRES_EXT_EFFECT_STRING, szNormalText, MAX_UITEXT_LENGTH );
		Sprintf( szMsgText, _T("%s: "), szNormalText );
		string strStates = szMsgText;

		std::vector<int>    arStateCodes;   //
		arStateCodes.clear();

		sABILITYINFO_BASE * pAbilityInfo = NULL ;
		pPrevSkillInfo->SetFirst();
		while( pAbilityInfo = pPrevSkillInfo->GetNext() )
		{
			if (pAbilityInfo->m_wStateID)
			{
				arStateCodes.push_back(pAbilityInfo->m_wStateID);
			}
		}


		int nStart, nEnd;

		nStart	= 0;
		nEnd		= arStateCodes.size();
		//bool bInsertComma = false; 
		bool bNonStates = true; 

		if (nEnd)
		{
			TCHAR	szStateName[MAX_UITEXT_LENGTH];

			for (int i=0, j=0; i<nEnd; ++i)
			{
				int code = arStateCodes[i];
				sSTATEINFO_BASE *bs = theStateInfoParser.GetStateInfo(code);
				if (bs)
				{
					if (j)
					{
						strStates += _T(", ");
					}

					GetItemName(bs->m_VItemCode,szStateName, MAX_UITEXT_LENGTH);
					//GetUIText( bs->m_dwNamecode, szStateName, MAX_UITEXT_LENGTH );
					strStates += szStateName;

					if (bNonStates)
						bNonStates = false;

					j++;
				}
			}
		}


		if (!bNonStates)
		{
			AddToolTipML((TCHAR* )strStates.c_str()
                     ,_tcslen(strStates.c_str())
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,0);
		}

	}
	else if (dwFlags & TOOLTIP_INFO_EXT_EFFECT1)
	{
		if (pNextSkillInfo)
		{
			GetUIText( TEXTRES_EXT_EFFECT_STRING, szNormalText, MAX_UITEXT_LENGTH );
			Sprintf( szMsgText, _T("%s: "), szNormalText );
			string strStates = szMsgText;

			std::vector<int>    arStateCodes;   //
			arStateCodes.clear();

			sABILITYINFO_BASE * pAbilityInfo = NULL ;
			pNextSkillInfo->SetFirst();
			while( pAbilityInfo = pNextSkillInfo->GetNext() )
			{
				arStateCodes.push_back(pAbilityInfo->m_wStateID);
			}

			int nStart, nEnd;

			nStart = 0;
			nEnd = arStateCodes.size();
			//bool bInsertComma = false; 
			bool bNonStates = true;

			if (nEnd)
			{
				for (int i=0, j=0; i<nEnd; ++i)
				{
					int code = arStateCodes[i];
					sSTATEINFO_BASE *bs = theStateInfoParser.GetStateInfo(code);
					if (bs)
					{
						if (j)
						{
							strStates += _T(", ");
						}

						strStates += (LPCSTR)bs->m_sStateName;

						if (bNonStates)
							bNonStates = false;

						j++;
					}
				}
			}

			if (bNonStates)
			{
				GetUIText( TEXTRES_NONE_TEXT, szNormalText, MAX_UITEXT_LENGTH );
				strStates += szNormalText;
			}

			AddToolTipML((TCHAR *)strStates.c_str(),
				_tcslen(strStates.c_str()),
				NORMAL_LINEHEIGHT,
				FONTTYPE_10,
				clrTitleColor,
				TOOLTIP_BG_COLOR,
				0);
		}

	}	//	10.



	//---------------------------------------------------------------------------------------------
	//		12. 升级属性
	dwExtraStyle = 0;
	//TEXTRES_SKILL_REQUIRESKILLLEVEL		;	// Text: %d
	//TEXTRES_SKILL_REQUIRESKILLSTAT		;	//	Text %s : %d
	//TEXTRES_SKILL_NEXTREQUIRESKILLSTAT	;	//	Text %s : %d
	//TEXTRES_SKILL_NEXTREQUIRESKILLLEVEL;	//	Text %d     

	//int			iSkillTypeIndex = 0;
	const int	arSkillTypes[PLAYERJOB_ALL][2] = 
	{
		{ TEXTRES_CLASS_ACTIVE_NONE,			TEXTRES_CLASS_ACTIVE_NONE },		//	PLAYERJOB_NONE
		{ TEXTRES_CLASS_ACTIVE1_WARRIOR,     TEXTRES_CLASS_ACTIVE2_WARRIOR },	//	PLAYERJOB_WARRIOR
		{ TEXTRES_CLASS_ACTIVE1_PALADIN,     TEXTRES_CLASS_ACTIVE2_PALADIN },	//	PLAYERJOB_PALADIN
		{ TEXTRES_CLASS_ACTIVE2_POWWOW,		TEXTRES_CLASS_ACTIVE2_POWWOW },	//	PLAYERJOB_POWWOW
		{ TEXTRES_CLASS_ACTIVE1_ASSASSIN,    TEXTRES_CLASS_ACTIVE2_ASSASSIN },	//	PLAYERJOB_STABBER
		{ TEXTRES_CLASS_ACTIVE1_NECROMANCER, TEXTRES_CLASS_ACTIVE2_NECROMANCER },	//	PLAYERJOB_NECROMANCER
	};


	if (dwFlags & TOOLTIP_INFO_REQUIRED_SKILLSTAT0)
	{
		//---------------------------------------------------------------------------------------------
		GetUIText( arSkillTypes[theHero.GetClass()][0], szTextBuf, MAX_UITEXT_LENGTH );
		//	text %s : %d
		GetUIText( TEXTRES_SKILL_REQUIRESKILLSTAT, szNormalText, MAX_UITEXT_LENGTH );
		Sprintf( szMsgText, szNormalText, szTextBuf, pPrevSkillInfo->m_wRequireSkillStat[0] );
		AddToolTip	(szMsgText
                  ,-1
						,NORMAL_LINEHEIGHT
						,FONTTYPE_10
						,clrTitleColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);
	}
	else if (dwFlags & TOOLTIP_INFO_REQUIRED_SKILLSTAT1)
	{
		//---------------------------------------------------------------------------------------------

		GetUIText( arSkillTypes[theHero.GetClass()][1], szTextBuf, MAX_UITEXT_LENGTH );
		//	text %s : %d
		GetUIText( TEXTRES_SKILL_REQUIRESKILLSTAT, szNormalText, MAX_UITEXT_LENGTH );
		Sprintf	( szMsgText, szNormalText, szTextBuf, pPrevSkillInfo->m_wRequireSkillStat[1]);
		AddToolTip(szMsgText
                ,-1
					 ,NORMAL_LINEHEIGHT
					 ,FONTTYPE_10
					 ,clrTitleColor
					 ,TOOLTIP_BG_COLOR
					 ,dwExtraStyle);
	}
	else if (dwFlags & TOOLTIP_INFO_REQUIRED_SKILLLEVEL)
	{
		//---------------------------------------------------------------------------------------------

		//	text: %d
		GetUIText( TEXTRES_SKILL_REQUIRESKILLLEVEL, szNormalText, MAX_UITEXT_LENGTH );
		Sprintf( szMsgText, szNormalText, pPrevSkillInfo->m_wRequireLV);
		AddToolTip	(szMsgText
                  ,-1
						,NORMAL_LINEHEIGHT
						,FONTTYPE_10
						,clrTitleColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);
	}
	else if (dwFlags & TOOLTIP_INFO_NEXT_SKILLSTAT0)
	{
		if (pNextSkillInfo)
		{
			//---------------------------------------------------------------------------------------------
			//		11. 

			////////
			GetUIText( arSkillTypes[theHero.GetClass()][0], szTextBuf, MAX_UITEXT_LENGTH );
			//	text %s : %d
			GetUIText( TEXTRES_SKILL_NEXTREQUIRESKILLSTAT, szNormalText, MAX_UITEXT_LENGTH );
			Sprintf( szMsgText, szNormalText, szTextBuf, pNextSkillInfo->m_wRequireSkillStat[0]);
			AddToolTip	(szMsgText
                     ,-1
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
		}
	}
	else if (dwFlags & TOOLTIP_INFO_NEXT_SKILLSTAT1)
	{
		if (pNextSkillInfo)
		{
			//---------------------------------------------------------------------------------------------

			///////////////////////
			GetUIText( arSkillTypes[theHero.GetClass()][1], szTextBuf, MAX_UITEXT_LENGTH );
			//	text %s : %d
			GetUIText( TEXTRES_SKILL_NEXTREQUIRESKILLSTAT, szNormalText, MAX_UITEXT_LENGTH );
			Sprintf( szMsgText, szNormalText, szTextBuf, pNextSkillInfo->m_wRequireSkillStat[1]);
			AddToolTip	(szMsgText
                     ,-1
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
		}
	}
	else if (dwFlags & TOOLTIP_INFO_NEXT_SKILLLEVEL)
	{
		if (pNextSkillInfo)
		{
			//---------------------------------------------------------------------------------------------

			//	text: %d
			GetUIText( TEXTRES_SKILL_NEXTREQUIRESKILLLEVEL, szNormalText, MAX_UITEXT_LENGTH );
			Sprintf( szMsgText, szNormalText, pNextSkillInfo->m_wRequireLV);
			AddToolTip	(szMsgText
                     ,-1
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
		}
	}


	////////////////////////////////////////////////////////////////
	dwExtraStyle = 0;
	if (dwFlags & TOOLTIP_INFO_REQUIRED_SKILLPOINT)
	{
		//	text: %d
		GetUIText( TEXTRES_SKILL_REQUIRESKILLPOINT, szNormalText, MAX_UITEXT_LENGTH );
		Sprintf( szMsgText, szNormalText, pPrevSkillInfo->m_byRequireSkillPoint);
		AddToolTip	(szMsgText
                  ,-1
						,NORMAL_LINEHEIGHT
						,FONTTYPE_10
						,clrTitleColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);
	}
	else if (dwFlags & TOOLTIP_INFO_NEXT_SKILLPOINT)
	{
		if (pNextSkillInfo)
		{
			//	text: %d
			GetUIText( TEXTRES_SKILL_NEXTREQUIRESKILLPOINT, szNormalText, MAX_UITEXT_LENGTH );
			Sprintf( szMsgText, szNormalText, pNextSkillInfo->m_byRequireSkillPoint);
			AddToolTip	(szMsgText
                     ,-1
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
		}
	}

}


//----------------------------------------------------------------------------
void ToolTipManagerInstance::_ProcessEmotionTip(DWORD dwFlags, SkillDetailInfo* pSkillInfo)
{
	TCHAR	szMsgText[MAX_UITEXT_LENGTH],
			szTextBuf[MAX_UITEXT_LENGTH];

	if( !singleton::ExistHero() )
		return;


	if ( !pSkillInfo )
		return;


	DWORD		dwExtraStyle = 0;

	COLOR		clrNameColor		= _COLOR( COLOR_NAME);
	//COLOR		clrTitleColor		= _COLOR( COLOR_TITLE);
	COLOR		clrNormalColor		= _COLOR( COLOR_NORMAL);
	//COLOR		clrSpaceColor		= _COLOR( COLOR_SPACE);
	//COLOR		clrErrorColor		= _COLOR( COLOR_ERROR);

	if( dwFlags & TOOLTIP_INFO_NAME )
    {
		GetItemName( pSkillInfo->m_VItemCode, szTextBuf, MAX_UITEXT_LENGTH );
		AddToolTip(	szTextBuf,
								-1,
								NAME_LINEHEIGHT,
								FONTTYPE_14,
								clrNameColor,
								TOOLTIP_BG_COLOR,
								dwExtraStyle);
    }

	//---------------------------------------------------------------------------------------------
	if (dwFlags & TOOLTIP_INFO_DESC0)
	{
		GetItemDesc(pSkillInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH);
		//GetUIText(pSkillInfo->m_dwSkillDescCode, szMsgText, MAX_UITEXT_LENGTH);

		AddToolTipML (szMsgText
                   ,_tcslen(szMsgText)
						 ,TITLE_LINEHEIGHT
						 ,FONTTYPE_12
						 ,clrNormalColor
						 ,TOOLTIP_BG_COLOR
						 ,0);
	}

}

//----------------------------------------------------------------------------
void ToolTipManagerInstance::_ProcessActionTip(DWORD dwFlags, SkillDetailInfo* pSkillInfo)
{
	TCHAR	szMsgText[MAX_UITEXT_LENGTH],
			szTextBuf	[MAX_UITEXT_LENGTH];

	if( !singleton::ExistHero() )
		return;


	if ( !pSkillInfo )
		return;


	DWORD			dwExtraStyle = 0;
	COLOR		clrNameColor			= _COLOR( COLOR_NAME);
	//COLOR		clrTitleColor		= _COLOR( COLOR_TITLE);
	COLOR		clrNormalColor		= _COLOR( COLOR_NORMAL);
	//COLOR		clrSpaceColor		= _COLOR( COLOR_SPACE);
	//COLOR		clrErrorColor		= _COLOR( COLOR_ERROR);


	if( dwFlags & TOOLTIP_INFO_NAME )
    {
		GetItemName ( pSkillInfo->m_VItemCode, szTextBuf, MAX_UITEXT_LENGTH );
		AddToolTip		(szTextBuf
							,-1
							,NAME_LINEHEIGHT
							,FONTTYPE_14
							,clrNameColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
    }

	//---------------------------------------------------------------------------------------------
	if (dwFlags & TOOLTIP_INFO_DESC0)
	{

		GetItemDesc (pSkillInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH);

		AddToolTipML	(	szMsgText, _tcslen(szMsgText),
							TITLE_LINEHEIGHT,
							FONTTYPE_12,
							clrNormalColor,
							TOOLTIP_BG_COLOR,
							0);
	}

}

//------------------------------------------------------------------------------
void ToolTipManagerInstance::_ProcessStyleTip	(DWORD            dwFlags
                                                ,sSTYLEINFO_BASE* prevStyleInfo
																,sSTYLEINFO_BASE* pNextStyleInfo)
{
	if( !singleton::ExistHero() )
		return;


	if (!prevStyleInfo)
    {
        ASSERT (". (prevStyleInfo == NULL)");
        return;
    }


	InitToolTip();

	
	DWORD		dwExtraStyle		= 0;
	COLOR		clrNameColor		= _COLOR( COLOR_NAME);
	COLOR		clrTitleColor		= _COLOR( COLOR_TITLE);
	COLOR		clrNormalColor		= _COLOR( COLOR_NORMAL);
	//COLOR		clrSpaceColor		= _COLOR( COLOR_SPACE);
	//COLOR		clrErrorColor		= _COLOR( COLOR_ERROR);


    TCHAR	szNormalText[MAX_UITEXT_LENGTH], szMsgText[MAX_UITEXT_LENGTH];


	//---------------------------------------------------------------------------------------------
	if (dwFlags & TOOLTIP_STYLE_INFO_NAME)
	{
		GetItemName ( prevStyleInfo->m_VItemCode, szNormalText, MAX_UITEXT_LENGTH );
		//GetUIText( prevStyleInfo->m_dwSkillNcode, szNormalText, MAX_UITEXT_LENGTH );
		AddToolTip	(szNormalText
                  ,-1
						,NAME_LINEHEIGHT
						,FONTTYPE_14
						,clrNameColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);

		BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DONOT_CHANGE_LINE );
		BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DT_RIGHT );

		GetUIText( TEXTRES_LEVEL, szNormalText, MAX_UITEXT_LENGTH );	
		Sprintf( szMsgText, _T("%d%s"), prevStyleInfo->m_wSkillLV, szNormalText );
		AddToolTip	(szMsgText
                  ,-1
						,1
						,FONTTYPE_14
						,clrNameColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);

	}
	else if (dwFlags & TOOLTIP_STYLE_INFO_NAME_DISABLE)
	{
		GetItemName ( prevStyleInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH );
		AddToolTip(szMsgText,
			-1,
			NAME_LINEHEIGHT,
			FONTTYPE_14,
			clrNameColor,
			TOOLTIP_BG_COLOR,
			dwExtraStyle);
	}




	//---------------------------------------------------------------------------------------------
	dwExtraStyle = 0;
	const int		iWeaponLimit = TEXTRES_WEAPON_DEFINE;
	GetUIText( iWeaponLimit, szMsgText, MAX_UITEXT_LENGTH );
	AddToolTip	(szMsgText
               ,-1
					,NORMAL_LINEHEIGHT
					,FONTTYPE_10
					,clrTitleColor
					,TOOLTIP_BG_COLOR
					,dwExtraStyle);

	//
	BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DONOT_CHANGE_LINE );
	BIT_ADD( dwExtraStyle, TOOLTIP_EXTRA_INFO_DT_RIGHT );
	const int	iWeaponName = TEXTRES_EQUIP_SingleSword_0;

	if( prevStyleInfo->m_WeaponDefines > 0)// && prevStyleInfo->m_WeaponDefines < ITEMTYPE_BOW )
	{
		GetUIText( (iWeaponName + prevStyleInfo->m_WeaponDefines), szMsgText, MAX_UITEXT_LENGTH );
	}
	else
	{
		GetUIText( TEXTRES_NONE_TEXT, szMsgText, MAX_UITEXT_LENGTH );
	}

	AddToolTip	(szMsgText
               ,-1
					,NORMAL_LINEHEIGHT
					,FONTTYPE_10
					,clrTitleColor
					,TOOLTIP_BG_COLOR
					,dwExtraStyle);




	//---------------------------------------------------------------------------------------------


	//---------------------------------------------------------------------------------------------
	//		4. 
	//---------------------------------------------------------------------------------------------
	dwExtraStyle = 0;
	if (dwFlags & TOOLTIP_STYLE_INFO_DESC0)
	{
		GetItemDesc (prevStyleInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH);
		//GetUIText(prevStyleInfo->m_dwSkillDescCode, szMsgText, MAX_UITEXT_LENGTH);

		AddToolTipML(szMsgText
                  ,_tcslen(szMsgText)
						,TITLE_LINEHEIGHT
						,FONTTYPE_12
						,clrNormalColor
						,TOOLTIP_BG_COLOR
						,dwExtraStyle);

	}
	else if (dwFlags & TOOLTIP_STYLE_INFO_DESC1)
	{
		if (pNextStyleInfo)
		{
			GetItemDesc (pNextStyleInfo->m_VItemCode, szMsgText, MAX_UITEXT_LENGTH);
			//GetUIText(pNextStyleInfo->m_dwSkillDescCode, szMsgText, MAX_UITEXT_LENGTH);

			AddToolTipML(szMsgText
                     ,_tcslen(szMsgText)
							,TITLE_LINEHEIGHT
							,FONTTYPE_12
							,clrNormalColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);

		}
	}



	//---------------------------------------------------------------------------------------------



	//---------------------------------------------------------------------------------------------
	//		6. 
	//---------------------------------------------------------------------------------------------
	dwExtraStyle = 0;
	if (dwFlags & TOOLTIP_STYLE_INFO_EXT_EFFECT0)
	{
		GetUIText( TEXTRES_EXT_EFFECT_STRING, szNormalText, MAX_UITEXT_LENGTH );
		Sprintf( szMsgText, _T("%s: "), szNormalText );
		string strStates = szMsgText;
		GetUIText( TEXTRES_NONE_TEXT, szNormalText, MAX_UITEXT_LENGTH );
		strStates += szNormalText;
		AddToolTipML	((TCHAR* )strStates.c_str()
                     ,_tcslen(strStates.c_str())
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);

	}
	else if (dwFlags & TOOLTIP_STYLE_INFO_EXT_EFFECT1)
	{
		if (pNextStyleInfo)
		{
			GetUIText( TEXTRES_EXT_EFFECT_STRING, szNormalText, MAX_UITEXT_LENGTH );
			Sprintf( szMsgText, _T("%s: "), szNormalText );
			string strStates = szMsgText;
			GetUIText( TEXTRES_NONE_TEXT, szNormalText, MAX_UITEXT_LENGTH );
			strStates += szNormalText;
			AddToolTipML	((TCHAR* )strStates.c_str()
                        ,_tcslen(strStates.c_str())
								,NORMAL_LINEHEIGHT
								,FONTTYPE_10
								,clrTitleColor
								,TOOLTIP_BG_COLOR
								,dwExtraStyle);

		}
	}



	//---------------------------------------------------------------------------------------------
	//		8. 
	//---------------------------------------------------------------------------------------------
    dwExtraStyle = 0;
	 //TEXTRES_SKILL_REQUIRESKILLLEVEL;			//	 : %d
	 //TEXTRES_SKILL_NEXTREQUIRESKILLLEVEL;		//	text : %d

    if (dwFlags & TOOLTIP_STYLE_INFO_REQUIRED_LEVEL)
    {
		//---------------------------------------------------------------------------------------------

		//	text : %d
		GetUIText( TEXTRES_SKILL_REQUIRESKILLLEVEL, szNormalText, MAX_UITEXT_LENGTH );
        Sprintf( szMsgText, szNormalText, prevStyleInfo->m_wRequireLV);
        AddToolTip(szMsgText,
			-1,
			NORMAL_LINEHEIGHT,
			FONTTYPE_10,
			clrTitleColor,
			TOOLTIP_BG_COLOR,
			dwExtraStyle);
    }
    else if (dwFlags & TOOLTIP_STYLE_INFO_NEXT_LEVEL)
    {
        if (pNextStyleInfo)
        {
			//---------------------------------------------------------------------------------------------


			//	text : %d
			GetUIText( TEXTRES_SKILL_NEXTREQUIRESKILLLEVEL, szNormalText, MAX_UITEXT_LENGTH );
            Sprintf( szMsgText, szNormalText, pNextStyleInfo->m_wRequireLV);
            AddToolTip	(szMsgText
                        ,-1
								,NORMAL_LINEHEIGHT
								,FONTTYPE_10
								,clrTitleColor
								,TOOLTIP_BG_COLOR
								,dwExtraStyle);
        }
    }

	//	text
    if (dwFlags & TOOLTIP_STYLE_INFO_REQUIRED_SKILLPOINT)
    {
		//	text : %d
		GetUIText( TEXTRES_SKILL_REQUIRESKILLPOINT, szNormalText, MAX_UITEXT_LENGTH );
        Sprintf( szMsgText, szNormalText, prevStyleInfo->m_byRequireSkillPoint);
        AddToolTip	(szMsgText
                     ,-1
							,NORMAL_LINEHEIGHT
							,FONTTYPE_10
							,clrTitleColor
							,TOOLTIP_BG_COLOR
							,dwExtraStyle);
    }
    else if (dwFlags & TOOLTIP_STYLE_INFO_NEXT_SKILLPOINT)
    {
        if (pNextStyleInfo)
        {
			//	text : %d
			GetUIText	(TEXTRES_SKILL_NEXTREQUIRESKILLPOINT
                     ,szNormalText
							,MAX_UITEXT_LENGTH);
            Sprintf( szMsgText, szNormalText, pNextStyleInfo->m_byRequireSkillPoint);
            AddToolTip(szMsgText
                      ,-1
							 ,NORMAL_LINEHEIGHT
							 ,FONTTYPE_10
							 ,clrTitleColor
							 ,TOOLTIP_BG_COLOR
							 ,dwExtraStyle);
        }
    }

}



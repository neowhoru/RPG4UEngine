/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInfoLoader.cpp
创建日期：2007年5月19日
最后更新：2007年5月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameInfoLoader.h"
#include "GraphicResourceList.h"
#include "SoundResourceList.h"
#include "WeaponSoundInfoParser.h"
#include "PlayerHeightInfoParser.h"
#include "NPCInfoParser.h"
#include "MonsterVoiceInfoParser.h"
#include "CharSoundInfoParser.h"
#include "SkillStorageParser.h"
#include "StyleQuickRegInfoParser.h"
#include "ItemSoundInfoParser.h"
#include "HeroVoiceInfoParser.h"
#include "NPCVoiceInfoParser.h"
#include "StateInfoParser.h"
#include "ItemCompositeParser.h"
#include "SocketOptionParser.h"
#include "RankOptionParser.h"
#include "ShopInfoParser.h"
#include "EnvironmentInfoParser.h"
#include "MapInfoParser.h"
#include "EnchantInfoParser.h"
#include "ItemMakeParser.h"
#include "ItemAttrInfoParser.h"
#include "RateInfoParser.h"
#include "QuestItemDropInfoParser.h"
#include "SeriesInfoParser.h"
#include "ScriptResourceList.h"
#include "HorseInfoParser.h"
#include "ExpressionIconManager.h"
#include "CharInfoParser.h"
#include "GameAvatarSetting.h"
#include "EffectResourceList.h"
#include "GameParameter.h"
#include "ResPublishManager.h"
#include "TextLogger.h"
#include "NumericValueParser.h"
#include "DemoSystemManager.h"
#include "BGMusicInfoParser.h"
#include "HeroData.h"
#include "SkillDependenceParser.h"
#include "SkillDefaultParser.h"
#include "AIInfoParser.h"
#include "RegenParser.h"
#include "MonsterGroupParser.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameInfoLoader, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameInfoLoader::GameInfoLoader()
{
}
GameInfoLoader::~GameInfoLoader()
{
}


void InitSound()
{
   theSound.SetUseSoundLibrary(FALSE);
	


	theSound.SetUseEffectSound	(theApplicationSetting.m_bEnableSoundEffect );
	theSound.SetUseBGMSound		(theApplicationSetting.m_bEnableBGMusic);

	if (	theApplicationSetting.m_bEnableSoundEffect 
		||	theApplicationSetting.m_bEnableBGMusic )
	{
		theSound.SetUseSoundLibrary(TRUE);
	}
}




BOOL GameInfoLoader::StartUp()
{
	StringHandle sPath;
	StringHandle sPath2;
	StringHandle sPath3;

	/////////////////////////////////////////////////////////
	//初始化
	//

	LOGINFO( __FUNCTION__ "\n");


	/////////////////////////////////////////////////////////

	__INIT( theCursorManager.LoadCursors( _PATH_DATAINFO("ResInfo(Cursor).info")) );
	__INIT( theColorManager.LoadColors(_PATH_DATAINFO("ConstInfo(Color).info")) );

	__INIT(theToolTipManager.Init());
	__INIT(theToolTipManager.LoadToolTipSetting(_PATH_DATAINFO("BaseInfo\\ToolTipTemplate.info")) );


	InitSound();


	/////////////////////////////////////////////////////////
	if(theGeneralGameParam.IsLocalHeroData())
	{
		if(!theHeroData.LoadGameData( GAMEDATA_PATH ))
		{
			__INIT(theHeroData.LoadLocalData		(_PATH_DATAINFO("local\\HeroData.info")) );
			theHeroData.LoadItemData		(_PATH_DATAINFO("local\\HeroItemData.info"));
		}
		theHeroData.LoadItemData		(_PATH_DATAINFO("local\\HeroStatusData.info")) ;
		theHeroData.LoadQuestData		(GAMEQUEST_PATH);

		//__INIT(theHeroData.LoadItemOption	(_PATH_DATAINFO("local\\HeroItemOption.txt")) );
		if(theGeneralGameParam.IsLocalHeroData())
		{
			__INIT(theDemoSystemManager.Init() );
		}
		//__INIT(theDemoSystemManager.LoadRegenInfo(_PATH_DATAINFO("local\\RegenInfo.txt")) );
	}

	/////////////////////////////////////////////////////////
	__INIT(GetGameAvatarSetting().LoadCfgFile( _PATH_DATAINFO("BaseInfo\\AppearanceInfo.txt") ) );
	__INIT(theCharInfoParser.LoadCharInfo	(_PATH_DATAINFO("BaseInfo\\CharInfo.txt") ) );
	__INIT(theCharInfoParser.LoadEquipInfo	(_PATH_DATAINFO("BaseInfo\\DefaultEquipInfo.txt") ) );


	__INIT(theGraphicResourceList.Init(MAX_GRAPHIC_INFO));
	theGraphicResourceList.Load(_PATH_DATAINFO("ResInfo(Graphic).info"));

	__INIT(theEffectResourceList.Init(MAX_EFFECT_INFO));
	theEffectResourceList.Load(_PATH_DATAINFO("ResInfo(Effect).info"));

	__INITINST(theScriptResourceList, MAX_SCRIPT_INFO);
	__LOADINFO(theScriptResourceList, "ResInfo(Script).info");

	__INIT(theHorseInfoParser.Init(MAX_HORSE_INFO));
	theHorseInfoParser.Load(_PATH_DATAINFO("HorseInfo.info"));


	__INIT(theSoundResourceList.Init(MAX_ITEM_SOUND_INFO));
	theSoundResourceList.Load(_PATH_DATAINFO("ResInfo(Sound).info"));


	///////////////////////////////////////////////////////////////////////////////

	__INIT(theTipLogManager.Init(_PATH_DATAINFO("TipLogInfo.info")) );

	__INIT(theNumericValueParser.Init() );
	theNumericValueParser.LoadNormal	( _PATH_DATAINFO("BaseInfo\\NumericValueInfo.txt") );
	theNumericValueParser.LoadItem	( _PATH_DATAINFO("BaseInfo\\NumericValueInfo(Item).txt") );

	__INIT(theRateInfoParser.Init(MAX_RATE_INFO));
	__LOADBASE	(theRateInfoParser,"RateInfo.txt" );

	__INIT(theQuestItemDropInfoParser.Init(MAX_QUESTITEM_DROPINFO));
	__LOADINFO	(theQuestItemDropInfoParser, _T("server\\QuestItemDropInfo.info")  );

	sPath		= _PATH_DATAINFO("BaseInfo\\SeriesRelationInfo.info");
	sPath2	= _PATH_DATAINFO("BaseInfo\\SeriesAffectInfo.info");
	__INIT(theSeriesInfoParser.Init	());
	theSeriesInfoParser.Load	(sPath ,sPath2);

	///////////////////////////////////////////////////////////////////////////////
	__INIT(theItemInfoParser.Init( MAX_ITEM_INFO, MAX_ITEM_TYPE ));

	__LOADFUNC(theItemInfoParser.LoadType,			 "BaseInfo\\ItemType.txt" );
	__LOADFUNC(theItemInfoParser.LoadVItemInfo,	"VItemInfo.info" );

	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Weapon).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Armor).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Waste).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo(Supply).info")	);
	theItemInfoParser.Load(_PATH_DATAINFO( "ItemInfo.info")	);


	__INITINST	(theMapInfoParser,  MAX_MAP_INFO );
	__LOAD3		( theMapInfoParser	
					,_MEDIAPATH(ePATH_DATAINFO)
					, "MapInfo(World).info"
					, "MapInfo(Field).info"  );

	__INITINST(theAIInfoParser, 100);
	__LOADINFO(theAIInfoParser,	"TipInfo(AI).info"		);


	//////////////////////////////////////////////////////////////
	__INITINST3(theNPCInfoParser, MAX_NPC_INFO, 100, 30 );
	__LOADINFO(theNPCInfoParser,						"NPCInfo.info"		);
	__LOADFUNC(theNPCInfoParser.LoadVNpc,			"VNPCInfo.info"		);
	__LOADFUNC(theNPCInfoParser.LoadExtra,			"NpcFuncInfo.info"	);
	__LOADFUNC(theNPCInfoParser.LoadQuestInfo,	"NpcQuestInfo.info");


	sPath		= _PATH_DATAINFO("SkillInfo.info");
	sPath2	= _PATH_DATAINFO("StyleInfo.info");
	sPath3	= _PATH_DATAINFO("VSkillInfo.info");
	__INITINST	(theSkillInfoParser,  MAX_SKILL_INFO );
	__LOAD3		(theSkillInfoParser, sPath	,sPath2 ,sPath3 );

	__LOADFUNC	(theSkillInfoParser.LoadOptions
					,"BaseInfo\\AbilityOptionInfo.info"); 


	__INITINST	(theSkillDefaultParser,  );
	__LOADBASE	(theSkillDefaultParser, "SkillDefault.info" );

	__INITINST	(theSkillDependenceParser,  );
	__LOADBASE	(theSkillDependenceParser, "SkillDependence.info" );

   __INITINST(theSkillStorageParser, MAX_SKILL_INVENTORY_STORE_INFO);
   __LOADINFO(theSkillStorageParser, "SkillInventoryStore.info" );

	// 
	__INITINST(theStateInfoParser,				100 );
	__LOADINFO(theStateInfoParser,				"StateInfo.info");
	__LOADFUNC(theStateInfoParser.LoadVState,	"VStateInfo.info");

	__INIT(theShopInfoParser.Init(MAX_SHOP_INFO));
	theShopInfoParser.Load(_PATH_DATAINFO("ShopInfo.info"));

	sPath		= _PATH_DATAINFO("BaseInfo\\ItemCompositeInfo.info");
	sPath2	= _PATH_DATAINFO("BaseInfo\\ItemCompositeResultInfo.info");
	__INITINST2	(theItemCompositeParser
					, MAX_ITEM_COMPOSITE_INFO
					, MAX_ITEM_COMPOSITE_INFO );
	__LOAD2(theItemCompositeParser, sPath	,sPath2  );


	__INITINST(theItemMakeParser	,MAX_ITEM_MAKE_INFO );
	__LOADBASE(theItemMakeParser	,"ItemMakeInfo.info" );


	/////////////////////////////////////
	sPath		= _PATH_DATAINFO("BaseInfo\\ItemRandAttrInfo.info" );
	sPath2	= _PATH_DATAINFO("BaseInfo\\ItemDoubleAttrInfo.info");
	sPath3	= _PATH_DATAINFO("BaseInfo\\ItemRandAttrTable.info" );
	__INITINST3	(theItemAttrInfoParser
					,MAX_ITEM_RANDATTR_INFO
					,MAX_ITEM_DOUBLEATTR_INFO
					,MAX_ITEM_RANDATTR_TABLE );
	__LOAD3	(theItemAttrInfoParser,sPath,sPath2 ,sPath3);

	__LOADFUNC	(theItemAttrInfoParser.LoadOptions
					,"BaseInfo\\ItemOptionInfo.info"); 


	__INIT		(theSocketOptionParser.Init());
	__LOADBASE	(theSocketOptionParser, "SocketOptionInfo.info");

	__INIT		(theRankOptionParser.Init(100));
	__LOADBASE	(theRankOptionParser,"RankOptionInfo.info");
	

	__INIT(theWeaponSoundInfoParser.Init(100));
	theWeaponSoundInfoParser.Load(_PATH_DATAINFO("SoundInfo(Weapon).info"));

	__INIT(theCharSoundInfoParser.Init(10000));
	theCharSoundInfoParser.Load(_PATH_DATAINFO("SoundInfo(Char).info"));

	__INIT(theBGMusicInfoParser.Init(MAX_BGM) );
	theBGMusicInfoParser.Load(_PATH_DATAINFO("MusicInfo(BG).info") );
	//CharInfoParser::Init(100,PACK_FILE_NAME);
	//CharInfoParser::Load(_PATH_DATAINFO("InitCharInfoList.txt");


	__INIT(theStyleQuickRegInfoParser.Init(MAX_STYLE_QUICK_REGISTER));
	theStyleQuickRegInfoParser.Load( _PATH_DATAINFO("BaseInfo\\StyleQuickRegistInfo.txt") );


	__INIT(theEnvironmentInfoParser.Init(100));
	theEnvironmentInfoParser.Load(_PATH_DATAINFO("BaseInfo\\MapInfo(WorldExtra).info"));

	__INIT(theHeroVoiceInfoParser.Init(10));
	theHeroVoiceInfoParser.Load(_PATH_DATAINFO("TipInfo(Hero).info"));

	__INIT(theNPCVoiceInfoParser.Init(100));
	theNPCVoiceInfoParser.Load(_PATH_DATAINFO("TipInfo(Npc).info"));

	__INIT(theMonsterVoiceInfoParser.Init(100));
	theMonsterVoiceInfoParser.Load(_PATH_DATAINFO("TipInfo(Monster).info"));


	__INIT(thePlayerHeightInfoParser.Init(100));
	thePlayerHeightInfoParser.Load(_PATH_DATAINFO("BaseInfo\\CharHeightInfo.txt"));

	
	__INIT(theItemSoundInfoParser.Init(MAX_ITEM_SOUND_INFO));
	theItemSoundInfoParser.Load(_PATH_DATAINFO("SoundInfo(Item).txt"));

	//EventInfoParser::Init(100,PACK_FILE_NAME);
	//EventInfoParser::Load(_PATH_DATAINFO("EventInfo.txt");

	//#if defined ( _NPC_PORTAL )
	//theLimitedMapPortParser.Init(MAX_NPC_PORTAL_INFO, PACK_FILE_NAME);
	//theLimitedMapPortParser.Load(_PATH_DATAINFO("LimitedMapPort.txt");
	//#endif

	__INITINST(theRegenParser,100 );
	__LOADSVR( theRegenParser,"RegenMonster.info"  );

	__INITINST(theMonsterGroupParser, 100 );
	__LOADSVR( theMonsterGroupParser, "RegenGroup.info"  );


	__INITINST(theEnchantInfoParser,100 );
	__LOADBASE(theEnchantInfoParser, "EnchantInfo.txt" );

	__INIT(theExpressionIconManager.Load( _PATH_DATAINFO("BaseInfo\\ExpressionInfo.txt" )));

	__INIT(theFreshmanTipManager.Init(_PATH_DATAINFO("TipInfo(Freshman).info" )));

	theFreshmanTipManager.RequestWorkingData();

	return TRUE;
}


void GameInfoLoader::Shutdown()
{
	//__END(ResourceManager				::Release());

	theFreshmanTipManager.ReleaseWorkingData();

	__END(theFreshmanTipManager		.Release());
	__END(theExpressionIconManager	.Release());
	__END(theStateInfoParser			.Release());
	__END(theNPCInfoParser				.Release());
	__END(theAIInfoParser				.Release());
	__END(theItemInfoParser				.Release());
	//__END(theItemManager				.Release());

	__END(theMapInfoParser				.Release());
	__END(theShopInfoParser				.Release());
   __END(theSkillStorageParser		.Release());
	__END(theSkillDependenceParser	.Release());
	__END(theSkillDefaultParser		.Release());
	__END(theSkillInfoParser			.Release());

	//__END(theTriggerParser			.Release());
	//__END(theMissionRewardParser	.Release());


	__END(theItemAttrInfoParser		.Release());
	__END(theItemMakeParser				.Release());
	__END(theItemCompositeParser		.Release());
	__END(theSocketOptionParser		.Release());
	__END(theRankOptionParser			.Release());


	__END(theWeaponSoundInfoParser	.Release());
	__END(theCharSoundInfoParser		.Release());

	//__END(theRequireFieldLevelInfoParser.Release());
	__END(theBGMusicInfoParser		.Release());
	//__END(CharInfoParser::Release());

#ifdef _USE_AUTO_STYLE_QUICK 
	__END(theStyleQuickRegInfoParser.Release());
#endif



	__END(theEnvironmentInfoParser	.Release());
	__END(theHeroVoiceInfoParser		.Release());
	__END(theNPCVoiceInfoParser		.Release());
	//__END(theHeroVariationInfoParser	.Release());
	__END(thePlayerHeightInfoParser		.Release());
	__END(theMonsterVoiceInfoParser	.Release());
	__END(theItemSoundInfoParser		.Release());

	//EventInfoParser::Release();
	//#if defined ( _NPC_PORTAL )
	//theLimitedMapPortParser.Release();
	//#endif

	__END(theRegenParser					.Release());
	__END(theMonsterGroupParser		.Release());
	__END(theEnchantInfoParser			.Release());
	__END(theSeriesInfoParser			.Release());
	__END(theQuestItemDropInfoParser	.Release());
	__END(theRateInfoParser				.Release());

	__END(theNumericValueParser		.Release	());
	__END(theTipLogManager				.Release());

	__END(theGraphicResourceList		.Release());
	__END(theEffectResourceList		.Release());
	__END(theHorseInfoParser			.Release());
	__END(theScriptResourceList		.Release());
	__END(theSoundResourceList			.Release());

	//GraphicResourceList::DestroyInstance();

	if(theGeneralGameParam.IsLocalHeroData())
		__END(theDemoSystemManager		.Release() );

	__END( theToolTipManager			.Release() );
	__END( theGameOption					.Release() );
}

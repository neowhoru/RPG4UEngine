/*////////////////////////////////////////////////////////////////////////
文 件 名：WareHouseDialog.cpp
创建日期：2008年6月8日
最后更新：2008年6月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "WareHouseDialog.h"
#include "SlotUIListener.h"
#include "MouseHandler.h"
#include "ObjectManager.h"
#include "SlotKeyGenerator.h"
#include "Mouse.h"
#include "TempInventoryContainer.h"
#include "ConstTextRes.h"
#include "PacketInclude.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_IMPL(WareHouseDialog);

//----------------------------------------------------------------------------
WareHouseDialog::WareHouseDialog(void)
{
	GLOBALINST_SINGLETON_PTR_INIT(WareHouseDialog);

	m_bCanHideOrginIcon = TRUE;
	m_pSlotListener		= NULL;
	m_Money					= 0;
	m_bStarted				= FALSE;
	m_bExtraBank			= FALSE;
	m_pTempInventory		= NULL;
}


//----------------------------------------------------------------------------
WareHouseDialog::~WareHouseDialog(void) 
{
	GLOBALINST_SINGLETON_PTR_FREE(WareHouseDialog);
	assert(m_pTempInventory == NULL && "Must Release manual");
	//ItemSlotContainer::Release();
}


//----------------------------------------------------------------------------
BOOL WareHouseDialog::Init(SLOTPOS MaxSlotSize, SLOTINDEX SlotIdx  )
{
	assert(m_pTempInventory == NULL);
	__BOOL_SUPER(Init( MaxSlotSize, SlotIdx ) );

	SetMoney(0);

	m_pSlotListener	= NULL;
	m_bStarted			= FALSE;
	m_dwPacketStatus		= 0;

	m_pTempInventory = new TempInventoryContainer;
	m_pTempInventory->Init();
	return TRUE;
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::Release()
{
	_SUPER::Release();
	m_pSlotListener = NULL;

	SAFE_DELETE(m_pTempInventory);
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::FlushSlotListener()
{
	if (!m_pSlotListener)
		return;


	for (SLOTPOS i = 0; i < GetSlotMaxSize(); i++)
	{
		if( !IsEmpty(i) )
			DeleteSlot(i, NULL);
	}
}	

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID	WareHouseDialog::FlushSlotListenerTempInven()
{
	if (!m_pSlotListener && m_pTempInventory)
		return;


	for (SLOTPOS i = 0; i < m_pTempInventory->GetSlotMaxSize(); i++)
	{
		if( !m_pTempInventory->IsEmpty(i) )
		{
			BaseSlot & FromSlot = m_pTempInventory->GetSlot(i);
			if(m_pSlotListener)
				m_pSlotListener->OnSlotRemove( FromSlot);//.GetSerial() );
		}
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void WareHouseDialog::SendMoneyPacket	(MONEY money, BOOL bDeposit)
{
	if(BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_MONEY ))
		return;

	BIT_ADD( m_dwPacketStatus, PACKET_STATUS_MONEY );

	MSG_CG_WAREHOUSE_MONEY_SYN		warehouseMoneySyn;

	warehouseMoneySyn.m_byType	= bDeposit	? (BYTE)MSG_CG_WAREHOUSE_MONEY_SYN::PUTMONEY 
														: (BYTE)MSG_CG_WAREHOUSE_MONEY_SYN::GETMONEY;
	warehouseMoneySyn.m_Money	= money;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&warehouseMoneySyn
									,sizeof(warehouseMoneySyn)
									,&theHero);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID WareHouseDialog::SendWareHouseStart()
{
	if (m_bStarted)
	{
		OUTPUTTIP(TEXTRES_WAREHOUSE_ALREADY_OPEN);
		return;
	}

#ifdef  _DEBUG
	OUTPUTTIP(TEXTRES_SEND_WAREHOUSE_START);
#endif

	if ( BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_START ) )
		return;

	BIT_ADD( m_dwPacketStatus, PACKET_STATUS_START );

	MSG_CG_WAREHOUSE_START_SYN		SendPacket;
	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID WareHouseDialog::SendWareHouseEnd()
{
	if (!m_bStarted)
		return;

	if ( BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_END ) )
	{
		OUTPUTTIP(TEXTRES_SEND_WAREHOUSE_END);
		return;
	}

	BIT_ADD( m_dwPacketStatus, PACKET_STATUS_END );

	MSG_CG_WAREHOUSE_END_SYN SendEndPacket;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendEndPacket
									,sizeof(SendEndPacket)
									,&theHero);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void WareHouseDialog::CloseWindow()
{
	SendWareHouseEnd();
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::ShowWindow(BOOL bShow)
{
	theGameUIManager.UISetData	(gameui::eUIWareHouse
										,gameui::eSetVisible
										,bShow);

	if(bShow)
	{
		if (singleton::ExistHero())
		{
			m_pTempInventory = 
				(TempInventoryContainer *)theHero.GetSlotContainer(SI_INVENTORY2);
			ASSERT(m_pTempInventory);
		}

		theKeyQueueManager.PushBack(ESC_PROC_DIALOG_BANK
											,GlobalUtil::DoBank)			;
	}
	else
	{
		SetMoney(0);
		m_bStarted = FALSE;

		m_dwPacketStatus = 0;

		theKeyQueueManager.DeleteMsg(ESC_PROC_DIALOG_BANK);

		theMouseHandler.RemoveMouseState(eMOUSE_WITHDRAW);
		theMouseHandler.RemoveMouseState(eMOUSE_DEPOSIT);
		//theMouseHandler.SetMouseState(eMOUSE_NONE);
		SwitchTempInventory(FALSE);

	}
}



//----------------------------------------------------------------------------
VOID WareHouseDialog::NetworkProc( MSG_BASE * pMsg )
{
	switch ( pMsg->m_byProtocol )
	{
		///////////////////////////////////////////
	case CG_WAREHOUSE_START_ACK:
		{
			MSG_CG_WAREHOUSE_START_ACK * pRecvMsg = (MSG_CG_WAREHOUSE_START_ACK *)pMsg;
			SetBankTotalInfo( pRecvMsg->m_ItemInfo, pRecvMsg->m_WarehouseMoney, TRUE );	
			SetMoney(pRecvMsg->m_WarehouseMoney);
			m_bStarted = TRUE;
			BIT_REMOVE( m_dwPacketStatus, PACKET_STATUS_START );

			ShowWindow(TRUE);
		}
		break;

	case CG_WAREHOUSE_START_CMD:
		{
			MSG_CG_WAREHOUSE_START_CMD * pRecvMsg = (MSG_CG_WAREHOUSE_START_CMD *)pMsg;
			SetBankTotalInfo	( pRecvMsg->m_ItemInfo
									, 0 
									, pRecvMsg->m_PageIndex != 0);	
			//m_bStarted = TRUE;
			//BIT_REMOVE( m_dwPacketStatus, PACKET_STATUS_START );
			//ShowWindow(TRUE);
		}
		break;


		///////////////////////////////////////////
	case CG_WAREHOUSE_START_NAK:
		{
			MSG_CG_WAREHOUSE_START_NAK * pRecvMsg = (MSG_CG_WAREHOUSE_START_NAK *)pMsg;

			m_bStarted = FALSE;
			BIT_REMOVE( m_dwPacketStatus, PACKET_STATUS_START );
			
			theItemManager.ProcessItemError(pRecvMsg->m_dwErrorCode);
			ShowWindow(FALSE);
		}
		break;

		///////////////////////////////////////////
	case CG_WAREHOUSE_MONEY_ACK:
		{
			MSG_CG_WAREHOUSE_MONEY_ACK * pRecvMsg = (MSG_CG_WAREHOUSE_MONEY_ACK *)pMsg;

			theHero.SetMoney(pRecvMsg->m_InventoryMoney);
			SetMoney( pRecvMsg->m_WarehouseMoney );

			BIT_REMOVE( m_dwPacketStatus, PACKET_STATUS_MONEY );

		}
		break;

		///////////////////////////////////////////
	case CG_WAREHOUSE_MONEY_NAK:
		{
			MSG_CG_WAREHOUSE_MONEY_NAK * pRecvMsg = (MSG_CG_WAREHOUSE_MONEY_NAK *)pMsg;

			theItemManager.ProcessItemError(pRecvMsg->m_dwErrorCode);

		}
		break;

		///////////////////////////////////////////
	case CG_WAREHOUSE_END_ACK:
		{
			ShowWindow(FALSE);
			m_bStarted = FALSE;

			BIT_REMOVE( m_dwPacketStatus, PACKET_STATUS_END );
		}
		break;

		///////////////////////////////////////////
	case CG_WAREHOUSE_END_NAK:
		{
			MSG_CG_WAREHOUSE_END_NAK * pRecvMsg = (MSG_CG_WAREHOUSE_END_NAK *)pMsg;

			theItemManager.ProcessItemError( pRecvMsg->m_dwErrorCode );

			BIT_REMOVE( m_dwPacketStatus, PACKET_STATUS_END );
			m_bStarted = FALSE;
		}
		break;

		///////////////////////////////////////////
	default:
		{
			ASSERT(!"Invlid protocol");
		}
	}
}



//----------------------------------------------------------------------------
VOID WareHouseDialog::Update()
{
	if(m_pSlotListener)
		m_pSlotListener->Update();
}




//----------------------------------------------------------------------------
BOOL WareHouseDialog::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )
{
	SERIALTYPE dwSerial = theSlotKeyGenerator.GetKey();
	slotDat.SetSerial(dwSerial);

	BaseContainer::InsertSlot( posIndex, slotDat );

	BaseSlot & rRealSlot = GetSlot(posIndex);

	if(m_pSlotListener)
		m_pSlotListener->OnSlotAdd(rRealSlot);

	UpdateSlotListener();

	return TRUE;
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	if (IsEmpty(posIndex))
		return;

	ItemSlot & rItemSlot = (ItemSlot & )GetSlot(posIndex);
	SERIALTYPE	 serial = rItemSlot.GetSerial();

	rItemSlot.SetSerial(0);
	if(m_pSlotListener)
		m_pSlotListener->OnSlotRemove( rItemSlot);

	theSlotKeyGenerator.Restore(serial);

	BaseContainer::DeleteSlot(posIndex, pSlotOut );

	//UpdateSlotListener();
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::UpdateSlotListener()
{
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::SetMoney(MONEY Money)
{
	m_Money = Money;
	theGameUIManager.UISetData	(gameui::eUIWareHouse
										,gameui::eSetMoney
										,(LPARAM)&m_Money);
}
	


//----------------------------------------------------------------------------
VOID WareHouseDialog::SetBankTotalInfo( sTOTALINFO_WAREHOUSE & ItemInfo, MONEY money,BOOL bAppend )
{
	sITEM_SLOTEX * pSlot = ItemInfo.m_Slot;
	SLOTPOS start = 0;
	SLOTPOS total = ItemInfo.m_Count;

	ItemSlot slotItemTemp;

	if(!bAppend)
		FlushSlotListener();
	
	for(SLOTPOS i=start;i<total;++i)
	{
		slotItemTemp.Copy( pSlot[i].m_Stream );
		InsertSlot(pSlot[i].m_ItemPos, slotItemTemp);
	}

	if(bAppend)
		m_Money += money;
	else
		SetMoney(money);
}



//----------------------------------------------------------------------------
VOID WareHouseDialog::InitContainer()
{
	if(m_pSlotListener)
		m_pSlotListener->Init( GetSlotMaxSize() );
}


//----------------------------------------------------------------------------
VOID WareHouseDialog::ReleaseContainer()
{
	FlushSlotListenerTempInven();
	FlushSlotListener();
}



//----------------------------------------------------------------------------
VOID WareHouseDialog::SwitchTempInventory(BOOL bFlag)
{
	if (bFlag)
	{
		m_bExtraBank = TRUE;
	}
	else
	{
		theMouseHandler.CancelHandlingItem();
		m_bExtraBank = FALSE;
	}

	UpdateSlotListener();
}




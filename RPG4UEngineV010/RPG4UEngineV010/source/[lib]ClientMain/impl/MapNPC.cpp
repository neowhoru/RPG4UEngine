/*////////////////////////////////////////////////////////////////////////
文 件 名：MapNPC.cpp
创建日期：2008年3月28日
最后更新：2008年3月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "MapNPC.h"
#include "VNpc.h"
#include "NPCInfoParser.h"
#include "AppearanceManager.h"
#include "NPCVoiceInfoParser.h"
#include "SoundEffect.h"
#include "SceneStruct.h"
#include "TextResManager.h"
#include "PathHandler.h"
#include "MathInc.h"
#include "ScriptManager.h"
#include "ScriptResourceList.h"
#include "WorldConst.h"
#include "ObjectUtil.h"
#include "MapNPCMovement.h"
#include "ScriptManager.h"
#include "IQuestCharacter.h"

using namespace scene;
using namespace math;

#define NPC_SWING_DEFAULT_DELAY_TERM	5000	
#define NPC_SWING_RANDOM_DELAY_TERM		13000	

namespace object
{ 

//------------------------------------------------------------------------------
MapNPC::MapNPC( void )
{
	m_ObjectCookie.SetObjectType(MAPNPC_OBJECT);
	m_bEnableIdleSound	= TRUE;
	m_pPlayerMeet			= NULL;
	m_bReadyPlayMeet		= FALSE;
	m_iWaitSoundDelay		= 0;
	m_pMovement				= NULL;

	__ZERO(m_FuncInfo);
	m_FuncInfo.m_LocalNPC= FALSE;
}

//------------------------------------------------------------------------------
MapNPC::~MapNPC( void )
{
	SAFE_DELETE(m_pMovement);
}

//------------------------------------------------------------------------------
BOOL MapNPC::Create( DWORD dwObjectKey, DWORD dwParam) 
{

	__BOOL_SUPER(Create(dwObjectKey, dwParam));

	m_iVoiceKind = eNPCVOICEKIND_NONE;
	InitState(STATE_NPC_IDLE_STATE,g_CurTime);

	m_iDelaySwingTick		= 0;
	m_DialogSlotIdx		= INVALID_SLOTINDEX;

	//////////////////////////////
	//m_pMovement->InitMoveInfo();


	return TRUE;
}


BOOL MapNPC::InitializeStates()
{
	m_StateArray[STATE_IDLE]				= theObjectSystem.AllocState(STATE_IDLE);
	m_StateArray[STATE_SPAWN]				= NULL;
	m_StateArray[STATE_MOVE]				= theObjectSystem.AllocState(STATE_MOVE);
	m_StateArray[STATE_KEYBOARDMOVE]		= NULL;
	m_StateArray[STATE_ATTACK]				= NULL;
	m_StateArray[STATE_JUMP]				= NULL;
	m_StateArray[STATE_DEATH]				= NULL;
	m_StateArray[STATE_ANIMATION]			= NULL;
	m_StateArray[STATE_DAMAGE]				= NULL;
	m_StateArray[STATE_AIR]					= NULL;
	m_StateArray[STATE_DOWN]				= NULL;
	m_StateArray[STATE_GETUP]				= NULL;
	m_StateArray[STATE_EVENTJUMP]			= NULL;
	m_StateArray[STATE_SKILL]				= NULL;
	m_StateArray[STATE_TRIGGER_STATE]	= NULL;
	m_StateArray[STATE_ITEM_DROP_STATE]	= NULL;

	m_StateArray[STATE_NPC_IDLE_STATE]		= theObjectSystem.AllocState(STATE_NPC_IDLE_STATE);
	m_StateArray[STATE_NPC_SWING_STATE]		= theObjectSystem.AllocState(STATE_NPC_SWING_STATE);
	m_StateArray[STATE_NPC_RESPONSE_STATE]	= theObjectSystem.AllocState(STATE_NPC_RESPONSE_STATE);

	m_pNextSkillState						= NULL;
	OnInitializeState();

	return TRUE;
}

void MapNPC::OnInitializeState()
{
}


//------------------------------------------------------------------------------
void MapNPC::Destroy( void )
{
	theObjectManager.RemoveFuncNpc(m_FuncInfo.m_ExtraCode);
	_SUPER::Destroy();
}



//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
BOOL MapNPC::Process( DWORD dwTick )
{

	if(m_pPlayerMeet != NULL)
	{
		float		fDISTANCE_NPC_MEET	= theGeneralGameParam.GetDistanceNpcMeet() * 2;
		Vector3D vDiff						= GetPosition() - theHero.GetPosition();
		float		distance					= vDiff.Length2();

		if ( distance > (fDISTANCE_NPC_MEET * fDISTANCE_NPC_MEET))
		{
			BaseContainer*	pContainer;
			
			if(m_DialogSlotIdx != INVALID_SLOTINDEX)
			{
				pContainer = theItemManager.GetContainer(m_DialogSlotIdx) ;
				if(pContainer && pContainer->GetSlotListener())
					pContainer->GetSlotListener()->CloseContainer();
			}
			m_DialogSlotIdx	= INVALID_SLOTINDEX;
			m_pPlayerMeet		= NULL;
		}
	}
	else
	{
		ProcessNPCMove(dwTick);
	}

	ProcessNPCVoice(dwTick);
	
	return _SUPER::Process(dwTick);
}


void MapNPC::SetNpcFunc(eNPC_FUNC_TYPE funcType)
{
	m_FuncInfo.m_eNPCTYPE = funcType;
}


eNPC_FUNC_TYPE	MapNPC::GetNpcFunc()
{
	return m_FuncInfo.m_eNPCTYPE;
}

//------------------------------------------------------------------------------
void MapNPC::SetFuncInfo( sNPCINFO_FUNC *	pExtraInfo, BOOL bForceLocal)
{
	theObjectManager.AddFuncNpc(pExtraInfo->m_ExtraCode,GetObjectKey());

	/////////////////////////////////
	m_FuncInfo					= *pExtraInfo;
	if(bForceLocal)
		m_FuncInfo.m_LocalNPC= TRUE;

	/////////////////////////////////////////
	if(m_FuncInfo.m_LocalNPC)
	{
		if(m_FuncInfo.m_iMoveType != 0 && m_pMovement == NULL)
		{
			m_pMovement				= new	MapNPCMovement(this);
			m_pMovement->InitMoveInfo();
		}

		if(m_pMovement)
			m_pMovement->SetFuncInfo(&m_FuncInfo);
	}


	/////////////////////////////////////////
	KeepQuestScript(0,QUESTCHARACTER_NPC);

	/////////////////////////////////////////
	if(m_pQuestChar)
	{
		NPC_QUESTINFO*		pQuestInfo;

		pQuestInfo	= theNPCInfoParser.GetQuestInfo(pExtraInfo->m_NPCCODE);

		if(m_FuncInfo.m_LocalNPC)
			m_pQuestChar->AddQuestScript	(m_FuncInfo.m_ServerScriptCode);
		m_pQuestChar->AddQuestScript	(m_FuncInfo.m_ClientScriptCode);

		m_pQuestChar->InitQuestInfo	(pQuestInfo);
	}

	//////////////////////////////////////
	VMapNpc*	pVNPC = (VMapNpc*)m_pVObject;
	pVNPC->InitSericeIcon(0);


}




void MapNPC::InitDelaySwingTick() 
{
	m_iDelaySwingTick = 0; 
}

void MapNPC::SetDelaySwingTick(DWORD dwTick)
{
	m_iDelaySwingTick = CalDelaySwingTick(dwTick);
}

int MapNPC::ProcessDelaySwingTick(DWORD dwTick)
{

	if (m_pMovement && m_pMovement->GetMoveType())
		return 1;

	if(!m_FuncInfo.m_LocalNPC)
		return 1;

	return m_iDelaySwingTick -= dwTick;
}

DWORD MapNPC::CalDelaySwingTick(DWORD /*dwTick*/)
{
	return		NPC_SWING_DEFAULT_DELAY_TERM 
				+	Random(NPC_SWING_RANDOM_DELAY_TERM);	
}




void MapNPC::StopMove( void )
{
	_SUPER::StopMove();

	if(m_pMovement)
		m_pMovement->StopMove();
}


void MapNPC::ProcessNPCMove(DWORD dwTick)
{
	if(!m_FuncInfo.m_LocalNPC)
		return;

	if(m_pMovement)
	{
		if (GetCurrentState() != STATE_MOVE)
			m_pMovement->ProcessMove(dwTick);
	}
}

void MapNPC::OnNextRoutePos()
{
	PLAYER_ACTION act;

	ZeroMemory(&act,sizeof(act));

	act.ActionID						= ACTION_MOVE;
	act.MOVE.bThrust					= FALSE;
	act.MOVE.byState					= MOVETYPE_RUN;
	act.MOVE.vCurPos.wvPos			= GetPosition();
	act.MOVE.vCurPos.m_TileIndex	= GetPathExplorer()->GetTile();
	act.MOVE.vDestPos.wvPos			= m_pMovement->GetNextRoutePos();
	act.MOVE.vDestPos.m_TileIndex = -1;

	PutAction(&act);
}

////////////////////////////////////////////////////
BOOL MapNPC::ProcessNPCVoice(DWORD dwTick)
{
	if(m_bReadyPlayMeet)
	{
		m_iMeetSoundDelay -= dwTick;
		if (m_iMeetSoundDelay <= 0)
		{
			m_bReadyPlayMeet = FALSE;
			PlayMeetSound();
			return TRUE;
		}
	}

	m_iWaitSoundDelay -= dwTick;

	if (m_iWaitSoundDelay < 0)
		m_iWaitSoundDelay = 0;


	__CHECK(m_iWaitSoundDelay == 0);

	__CHECK(singleton::ExistHero());

	__CHECK(theGameOption.GetNPCVoice());


	sNPC_VOICEINFO *pInfo;
	
	pInfo = theNPCVoiceInfoParser.GetVoiceInfo(m_NPCInfo.m_MonsterCode);
	__CHECK_PTR(pInfo);


	float					fLenght2;

	fLenght2				= GetRangeWithHero();
	float fSightRange	= GetMonsterInfo()->m_fViewRange;


	if (	GetVMonsterInfo()->m_dwIdleSoundPeriod )
	{
		if(fLenght2 <= fSightRange * fSightRange )
		{
			if (m_iWaitSoundDelay == 0)
			{
				if(rand()%100 < GetVMonsterInfo()->m_byPlayIdleVoiceRate)
					PlayWaitSound();

				m_iWaitSoundDelay = GetVMonsterInfo()->m_dwIdleSoundPeriod 
										+ math::RandomRange	(theGeneralGameParam.GetNpcWaitSoundDelayVariable() ); 
			}
		}
		else 
		{
			if(m_iVoiceKind == eNPCVOICEKIND_MEET)
			{
				PlayLeaveSound();
			}
			else if(m_iVoiceKind == eNPCVOICEKIND_WAIT)
			{
				m_iVoiceKind = eNPCVOICEKIND_NONE;
				/// 加长下次播音时间
				m_iWaitSoundDelay = GetVMonsterInfo()->m_dwIdleSoundPeriod * 2 
										+ theGeneralGameParam.GetNpcWaitSoundDelayVariable(); 
			}
		}
	}

	
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void MapNPC::PlayLeaveSound()
{
	if( theGameOption.GetNPCVoice() == FALSE )
		return;

	m_iVoiceKind = eNPCVOICEKIND_LEAVE;

	ObjectUtil::ProcessNpcVoice(this
										,offsetof(sNPC_VOICEINFO,m_Leave)
										,m_iVoiceHandle);

}

void MapNPC::PlayMeetSound()
{
	if( theGameOption.GetNPCVoice() == FALSE )
		return;


	if (	m_iVoiceKind != eNPCVOICEKIND_NONE
		&&	m_iVoiceKind != eNPCVOICEKIND_WAIT)
		return;

	m_iVoiceKind = eNPCVOICEKIND_MEET;

	ObjectUtil::ProcessNpcVoice(this
										,offsetof(sNPC_VOICEINFO,m_Meet)
										,m_iVoiceHandle);


}

void MapNPC::PlayWaitSound()
{
	if( theGameOption.GetNPCVoice() == FALSE )
		return;

	if (	m_iVoiceKind != eNPCVOICEKIND_NONE
		&&	m_iVoiceKind != eNPCVOICEKIND_LEAVE)
		return;

	m_iVoiceKind = eNPCVOICEKIND_WAIT;

	if(ObjectUtil::ProcessNpcVoice(this
										,offsetof(sNPC_VOICEINFO,m_Wait)
										,m_iVoiceHandle))
	{
		SetDirection(theHero.GetPosition());
		StopMove();
	}


}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

void MapNPC::ProcessPlayerMeeting(Object* pPlayer)
{
	OnMeetPlayer(pPlayer);
}


void MapNPC::OnMeetPlayer(Object* pPlayer)
{
	__CHECK2(pPlayer && pPlayer->IsPlayer(),;);

	if(m_iVoiceKind == eNPCVOICEKIND_WAIT)
	{
		m_iMeetSoundDelay = m_iWaitSoundDelay + 1000;
	}
	else
	{
		m_iMeetSoundDelay = 100;
	}

	m_bReadyPlayMeet	= TRUE;
	m_pPlayerMeet		= pPlayer;

	if(m_pPlayerMeet)
	{
		//PlayMeetSound();
		StopMove();
		SetDirection(m_pPlayerMeet->GetPosition());
		m_pPlayerMeet->SetDirection(GetPosition());
	}
}



BOOL MapNPC::AddRelateQuestInfo( sQUEST_RELATEINFO* pInfo )
{
	if(m_pQuestChar)
		return m_pQuestChar->AddRelateQuestInfo(pInfo );
	return FALSE;
}


sQUEST_RELATEINFO* MapNPC::GetRelateQuestInfo( int n )
{
	if(m_pQuestChar)
		return m_pQuestChar->GetRelateQuestInfo(n );
	return NULL;
}

UINT MapNPC::GetRelateQuestInfoCount()
{
	if(m_pQuestChar)
		return m_pQuestChar->GetRelateQuestInfoCount( );
	return 0;
}




};//namespace object

//------------------------------------------------------------------------------
//  uiSkillEmotion_func.cpp
//  (C) 2005 ckbang
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "UISkillEmotion.h"
#include "SlotUIListener.h"
#include "SkillPaneSlot.h"
#include "SkillInfoParser.h"

#include "SkillStorageParser.h"
#include "ObjectManager.h"
#include "SlotKeyGenerator.h"
#include "Hero.h"
#include "PlayerAttributes.h"
#include "QuickContainer.h"
#include "ItemManager.h"
//#include "SkillStorageManager/SkillStorageManager.h"


UISkillEmotion::UISkillEmotion()
{
	m_pSlotListener = NULL;
}

UISkillEmotion::~UISkillEmotion()
{
    //SAFE_RELEASENDELETE(m_pSlotListener);
	m_pSlotListener = NULL;
}

BOOL UISkillEmotion::Init(INT nSlotNum)
{
	SkillPaneContainer::Init(nSlotNum);
	return TRUE;
}

void UISkillEmotion::Release()
{
	_SUPER::Release();
}


//------------------------------------------------------------------------------
/** ITEM UNIT RENDERER
*/
void
UISkillEmotion::InitSlotListener()
{


	 if(m_pSlotListener)
      m_pSlotListener->Init(GetSlotMaxSize() );
}

//------------------------------------------------------------------------------
/** ITEM UNIT RENDERER
*/
void
UISkillEmotion:://ResetItemRenderer()
{
    //SAFE_RELEASENDELETE(m_pSlotListener);
	m_pSlotListener = NULL;

    InitSlotListener();
}

//------------------------------------------------------------------------------
/** ITEM UNIT RENDERER
*/
//SlotUIListener * 
//UISkillEmotion::GetItemUnitRender() const
//{
//    return m_pSlotListener;
//}

//------------------------------------------------------------------------------
/**
    working for slot clean

    called where to Hero::ReleaseContainer().
*/
void
UISkillEmotion::FlushSlotListener()
{
    SkillPaneSlot FromSlot;

    for (int i = 0; i < GetSlotMaxSize(); i++)
    {
        if( !IsEmpty(i) )
		{
			// 시리얼을 지워준다.
			SERIALTYPE serial = GetSlot(i).GetSerial();
			theSlotKeyGenerator.Restore(serial);
			
            DeleteSlot(i, &FromSlot);
		}
    }

}

//------------------------------------------------------------------------------
/**
*/
BOOL
UISkillEmotion::InsertSlot( SLOTPOS posIndex, BaseSlot & IN slotDat )
{
    SkillSlotContainer::InsertSlot(posIndex, slotDat);

    SkillSlot& slot =
        (SkillSlot &)GetSlot(posIndex);

	 if(m_pSlotListener)
    m_pSlotListener->OnSlotAdd(
        slot.GetSerial(),
        slot.GetCode(),
        &slot );

    return TRUE;
}

//------------------------------------------------------------------------------
/**
*/
void
UISkillEmotion::DeleteSlot(SLOTPOS posIndex, BaseSlot * pSlotOut )
{
    SkillSlotContainer::DeleteSlot(posIndex, pSlotOut);

    if (!m_pSlotListener)
        return;

    if (0 != pSlotOut->GetSerial())
    {
        if (m_pSlotListener)
            m_pSlotListener->OnSlotRemove( pSlotOut->GetSerial() );
    }
}

//------------------------------------------------------------------------------
/**
*/
void
UISkillEmotion::Clear()
{
    FlushSlotListener();
    m_dwRemainPoint = 0;
    InitSlotListener();
}

//------------------------------------------------------------------------------
//  EOF
//------------------------------------------------------------------------------

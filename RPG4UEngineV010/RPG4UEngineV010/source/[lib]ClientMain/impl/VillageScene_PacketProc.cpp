/*////////////////////////////////////////////////////////////////////////
文 件 名：VillageScene_PacketProc.cpp
创建日期：2008年4月4日
最后更新：2008年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VillageScene.h"
#include "Protocol_ClientGameS.h"
#include "PacketStruct_ClientGameS.h"
#include "MapNPC.h"
#include "ObjectManager.h"
#include "hero.h"
#include "HeroData.h"
#include "NPCInfoParser.h"
#include "LoadScene.h"
#include "UserSlot.h"
#include "StringUtil.h"
#include "HeroActionInput.h"
//#include "GameInVarInterface.h"


//////////////////////////////////////////////////////////////////////////
//------------------------------------------------------------------------------
void VillageScene::ParsePacket( MSG_BASE * pMsg )
{
	switch( pMsg->m_byCategory )
	{	
	//case CG_EVENT:
	//	theGameInVarInterface.OnRecvCG_EVENT(pMsg);
	//	break;
	case CG_SYNC:	OnRecvCG_SYNC(pMsg);						break;
	case CG_ZONE:	OnRecvCG_ZONE( pMsg );					break;
	case CG_PARTY:	OnRecvCG_PARTY( pMsg );					break;
	default:			BattleScene::ParsePacket( pMsg );	break;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL VillageScene::NeedToParsePacket(MSG_BASE * pMsg)
{
	switch( pMsg->m_byCategory )
	{	
		/////////////////////////////////////////////
	case CG_SYNC:
		{
			if (IsLobby())
			{
				if (IsLobbyInPending())
				{
					//OnRecvCG_SYNC_LobbyPending( pMsg );
					switch(pMsg->m_byProtocol)
					{
					case CG_SYNC_PLAYER_ENTER_ACK:
					case CG_SYNC_PLAYER_ENTER_NAK:
						return TRUE;
					}
					return FALSE;
				}
				else
				{
					//OnRecvCG_SYNC_Lobby( pMsg );
					switch(pMsg->m_byProtocol)
					{
					case CG_SYNC_PLAYER_ENTER_ACK:
					case CG_SYNC_PLAYER_ENTER_NAK:
					case CG_SYNC_ALLPLAYERS_CMD:
					case CG_SYNC_ALLPLAYERS_EQUIPINFO_CMD:
						return TRUE;
					//case CG_SYNC_MOVE_BRD:
					//case CG_SYNC_MOVE_THRUST_BRD:
					//case CG_SYNC_KBMOVE_BRD:
					//case CG_SYNC_NPC_JUMP_BRD:
					//case CG_SYNC_TARGET_MOVE_BRD:
					}
					return FALSE;
				}
			}
			else
			{
				if (IsLobbyOutPending())
				{
					//OnRecvCG_SYNC_LobbyOutPending( pMsg );
					switch(pMsg->m_byProtocol)
					{
					case CG_SYNC_PLAYER_ENTER_ACK:
					case CG_SYNC_PLAYER_ENTER_NAK:
						return TRUE;
					}
					return FALSE;
				} 
				else
				{
					//OnRecvCG_SYNC_Normal( pMsg );
					//switch(pMsg->m_byProtocol)
					//{
					//case :
					//	return FALSE;
					//}
				}
			}
		}
		break;

		/////////////////////////////////////////////
	case CG_ZONE:
		{
			switch(pMsg->m_byProtocol)
			{
			case CG_ZONE_MISSION_LEAVE_ACK:
			case CG_ZONE_MISSION_LEAVE_NAK:
			case CG_ZONE_HUNTING_LEAVE_ACK:
			case CG_ZONE_HUNTING_LEAVE_NAK:
			case CG_ZONE_PVP_LEAVE_ACK:
			case CG_ZONE_PVP_LEAVE_NAK:
				return FALSE;
			}
			return TRUE;
		}
		break;

	/////////////////////////////////////////////
	//case CG_PARTY:
	//	break;
	}

	return TRUE;
}


//------------------------------------------------------------------------------
void VillageScene::OnRecvCG_SYNC( MSG_BASE * pMsg )
{
	if (IsLobby())
	{
		if (IsLobbyInPending())
			OnRecvCG_SYNC_LobbyPending( pMsg );

		else
			OnRecvCG_SYNC_Lobby( pMsg );
	}
	else
	{
		if (IsLobbyOutPending())
			OnRecvCG_SYNC_LobbyOutPending( pMsg );

		else
			OnRecvCG_SYNC_Normal( pMsg );
	}
}


//------------------------------------------------------------------------------
void VillageScene::OnRecvCG_SYNC_Normal( MSG_BASE * pMsg )
{
	BattleScene::OnRecvCG_SYNC(pMsg);

	switch (pMsg->m_byProtocol)
	{
	case CG_SYNC_PLAYER_ENTER_ACK:
		{
			m_bWaitEnterSync		= FALSE;
			m_iPrevSectorIndex	= -1;
			m_bComebackFromLobby = FALSE;
			//BattleScene::OnRecvCG_SYNC(pMsg);	
		}
		break;

	default:
		break;
	}

	//BattleScene::OnRecvCG_SYNC(pMsg);
//#endif
}


//------------------------------------------------------------------------------
void VillageScene::OnRecvCG_SYNC_Lobby( MSG_BASE * pMsg )
{
	__VERIFY2(IsLobby(),"Should  Is Lobby",);

	switch (pMsg->m_byProtocol)
	{
		////////////////////////////////////
	case CG_SYNC_MOVE_BRD:
	case CG_SYNC_MOVE_THRUST_BRD:
	case CG_SYNC_KBMOVE_BRD:
	case CG_SYNC_NPC_JUMP_BRD:
	case CG_SYNC_TARGET_MOVE_BRD:
		break;

		////////////////////////////////////
	case CG_SYNC_PLAYER_ENTER_BRD:
		{
			MSG_CG_SYNC_PLAYER_ENTER_BRD *pRecvPacket;
			sPLAYERINFO_RENDER *				pPlayerInfo;
			
			pRecvPacket = (MSG_CG_SYNC_PLAYER_ENTER_BRD *)pMsg;

			pPlayerInfo = (sPLAYERINFO_RENDER*)( (BYTE*)(pRecvPacket) + SIZE_CG_MSG(SYNC_PLAYER_ENTER_BRD) );

			//////////////////////////////////////
			if (singleton::ExistHero())
			{
				if (theHero.GetObjectKey() == pPlayerInfo->m_wPlayerKey)
				{
					theHero.SetCurrentAttackStyle(pPlayerInfo->m_SelectStyleCode,FALSE);
					break;
				}
			}

			//////////////////////////////////
			Player*						pPlayer;
			sGUILDINFO_RENDER *		pGuildInfo;
			sEQUIP_ITEM_INFO_BASE *	pEquipInfo;
			sITEM_SLOT *				pItemSlot;
			sVENDORINFO_RENDER*		pVendorInfo;
			
			pPlayer = (Player*)theObjectManager.GetObject(pPlayerInfo->m_wPlayerKey);

			if (!pPlayer)
				pPlayer = (Player*)theObjectManager.Add	(pPlayerInfo->m_wPlayerKey
                                                      ,PLAYER_OBJECT
																		,pPlayerInfo->m_byClass);

			ASSERT(pPlayer);

			///////////////////
			Vector3D		vPos( pPlayerInfo->m_fPos );
			pPlayer->SetPosition(vPos );
			pPlayer->SetPlayerInfo( pPlayerInfo ,FALSE);

			///////////////////
			pGuildInfo = (sGUILDINFO_RENDER*)		( (BYTE*)(pPlayerInfo) + pPlayerInfo->GetSize() );
			pPlayer->SetGuildInfo(pGuildInfo);

			pVendorInfo = (sVENDORINFO_RENDER*)		( (BYTE*)(pGuildInfo) + pGuildInfo->GetSize() );
			pPlayer->SetVendorInfo(pVendorInfo);

			//////////////////////
			pEquipInfo	= (sEQUIP_ITEM_INFO_BASE*)	( (BYTE*)(pVendorInfo) + pVendorInfo->GetSize() );
			pItemSlot	= (sITEM_SLOT*)				( (BYTE*)(pEquipInfo) + sizeof(sEQUIP_ITEM_INFO_BASE) );
			pPlayer->SetEquipItemBaseInfo( pEquipInfo->m_Count, pItemSlot );


			//if (theLobbyDialog.GetLobbyMasterPlayerKey() != pPlayerInfo->m_wPlayerKey )
			//{
			//	UserSlot EnterUser;
			//	EnterUser.SetPlayerKey( pPlayer->GetObjectKey() );
			//	theLobbyDialog.AddPlayer( EnterUser );
			//}
		}
		break;

		///////////////////////////////////////////
	case CG_SYNC_PLAYER_LEAVE_BRD:
		{
			Player *								pPlayer;
			MSG_CG_SYNC_PLAYER_LEAVE_BRD *pRecvPacket = (MSG_CG_SYNC_PLAYER_LEAVE_BRD *)pMsg;

			for( int i = 0; i < pRecvPacket->m_byCount; ++i )
			{
				if( pRecvPacket->m_dwObjectKey[i] == theHero.GetObjectKey() )	
				{
					ASSERT(0);
					continue;
				}
				pPlayer = static_cast<Player *>( theObjectManager.GetObject(pRecvPacket->m_dwObjectKey[i]) );
				if( !pPlayer )
					continue;

				pPlayer->ProcessRemainActionResults();
				theObjectManager.Delete( pRecvPacket->m_dwObjectKey[i] );

				//theLobbyDialog.LeavePlayer( pRecvPacket->m_dwObjectKey[i] );
			}
		}
		break;


		///////////////////////////////////////////////
	case CG_SYNC_ALLPLAYERS_CMD:
		{
			BattleScene::OnRecvCG_SYNC(pMsg);

			//MSG_CG_SYNC_ALLPLAYERS_CMD *pRecvPacket = (MSG_CG_SYNC_ALLPLAYERS_CMD *)pMsg;

			//BYTE * pPacket = ( (BYTE*)(pRecvPacket) + sizeof(MSG_CG_SYNC_ALLPLAYERS_CMD) );

			//sPLAYERINFO_RENDER *pPlayerInfo = NULL;
			//sGUILDINFO_RENDER *pGuildInfo = NULL;
     
			//for (int i = 0; i < pRecvPacket->m_byCount; i++)
			//{
			//	pPlayerInfo = (sPLAYERINFO_RENDER *)pPacket; 
			//	pGuildInfo = (sGUILDINFO_RENDER *)( (BYTE*)(pPacket) + pPlayerInfo->GetSize() );

			//	if (pReadyDlg->GetLobbyMasterPlayerKey() == pPlayerInfo->m_dwObjectKey)
			//	{
			//	}
			//	else
			//	{
			//		UserSlot EnterUser;
			//		EnterUser.SetPlayerKey( pPlayerInfo->m_dwObjectKey );
			//		pReadyDlg->AddPlayer( EnterUser );
			//	}

			//	pPacket += GetPlayerSize(pPacket);
			//}

			MSG_CG_ZONE_LOBBY_REQUEST_INFO_SYN Sendpacket;
			theNetworkLayer.SendPacket(CK_GAMESERVER, &Sendpacket, sizeof(Sendpacket));
		}
		break;

	case CG_SYNC_ALLPLAYERS_EQUIPINFO_CMD:
		{
			BattleScene::OnRecvCG_SYNC(pMsg);
		}
		break;

	}
}


//----------------------------------------------------------------------------
void VillageScene::OnRecvCG_SYNC_LobbyPending( MSG_BASE * pMsg )
{
	switch (pMsg->m_byProtocol)
	{
	case CG_SYNC_PLAYER_ENTER_ACK:
		{
			MSG_CG_SYNC_PLAYER_ENTER_ACK	*	pRecvPacket;
			Player *									pPlayer;
			
			pRecvPacket = (MSG_CG_SYNC_PLAYER_ENTER_ACK *)pMsg;

			pPlayer = static_cast<Player *>( theObjectManager.GetObject(theHero.GetObjectKey()) );
			if ( NULL == pPlayer || !pPlayer->IsHero())
			{
				ASSERT(!"无效Hero!!");
				return;
			}

			ASSERT( singleton::ExistHero() );

			//if (pReadyDlg->GetLobbyMasterPlayerKey() != theHero.GetObjectKey())
			//{
			//	UserSlot EnterUser;
			//	EnterUser.SetPlayerKey( theHero.GetObjectKey() );
			//	BOOL bRet = pReadyDlg->AddPlayer( EnterUser );	
			//	ASSERT(bRet);
			//}

			// 
			m_bComebackFromLobby = TRUE;		
			m_bPlayerAck			= TRUE;
			m_bDrawWait				= FALSE;

			SetLobby(TRUE);
			SetLobbyInPending(FALSE);
			
			theHero.StopMove();
			theHero.ResetMoveFlag();

			theHeroActionInput.SwitchAutoMove(FALSE);
			theHeroActionInput.SetFollowState( FALSE );
		}
		break;

		//////////////////////////////////////////////////
	case CG_SYNC_PLAYER_ENTER_NAK:
		{
			//MSG_CG_SYNC_PLAYER_ENTER_NAK * pRecvMsg = (MSG_CG_SYNC_PLAYER_ENTER_NAK *)pMsg;

			//SetLobby(FALSE);
			SetLobbyInPending(FALSE);
		}
		break;

	default:
		{
			//OnRecvCG_SYNC_Normal( pMsg );
		}
		break;
	}
}


//----------------------------------------------------------------------------
void VillageScene::OnRecvCG_SYNC_LobbyOutPending(MSG_BASE * pMsg)
{
	switch (pMsg->m_byProtocol)
	{
		case CG_SYNC_PLAYER_ENTER_ACK:
			{
				SetLobbyOutPending(FALSE);
				SetLobby(FALSE);

				OnRecvCG_SYNC_Normal(pMsg);
			}
			break;

		case CG_SYNC_PLAYER_ENTER_NAK:
			{
				OnRecvCG_SYNC_Normal(pMsg);
			}
			break;

		default:
			{
				//OnRecvCG_SYNC_Lobby(pMsg);
			}
			break;
	}	
}


//------------------------------------------------------------------------------
void VillageScene::OnRecvCG_PARTY( MSG_BASE * /*pMsg*/ )
{
	if (IsLobby())	return;
    //theLobbyDialog.NetworkProc(pMsg);
}

//----------------------------------------------------------------------------
void VillageScene::OnRecvCG_ZONE( MSG_BASE * pMsg )
{
	switch (pMsg->m_byProtocol)
	{



#if defined ( _PVP )
	case CG_ZONE_LIST_PVPLOBBY_ACK:
	case CG_ZONE_LIST_PVPLOBBY_NAK:
#endif
	case CG_ZONE_LIST_MISSIONLOBBY_ACK:
	case CG_ZONE_LIST_MISSIONLOBBY_NAK:
	case CG_ZONE_LIST_HUNTINGLOBBY_ACK:
	case CG_ZONE_LIST_HUNTINGLOBBY_NAK:
	case CG_ZONE_LIST_HUNTING_ACK:
	case CG_ZONE_LIST_HUNTING_NAK:
	case CG_ZONE_LIST_REFRESH_HUNTINGLOBBY_ACK:
	case CG_ZONE_LIST_REFRESH_MISSIONLOBBY_ACK:
	case CG_ZONE_LIST_REFRESH_NONE_ACK:
	case CG_ZONE_LIST_REFRESH_NAK:
	case CG_ZONE_HUNTING_ROOM_FROM_CHARID_ACK:
	case CG_ZONE_PVPLOBBY_ROOM_FROM_CHARID_ACK:
	case CG_ZONE_FIND_ROOM_FROM_CHARID_NAK:
	case CG_ZONE_MISSIONLOBBY_ROOM_FROM_CHARID_ACK:
	case CG_ZONE_HUNTINGLOBBY_ROOM_FROM_CHARID_ACK:
		{
			//theBattleZoneList.NetworkProc(pMsg);
			//theBattleZoneCreate.NetworkProc(pMsg);
		}
		break;



#if defined ( _PVP )
	case CG_ZONE_LOBBY_CHANGE_TEAM_ACK:
	case CG_ZONE_LOBBY_CHANGE_TEAM_NAK:
	case CG_ZONE_LOBBY_CHANGE_TEAM_BRD:
//	case CG_ZONE_LOBBY_PVP_CONFIG_ACK:
	case CG_ZONE_LOBBY_PVP_CONFIG_NAK:
	case CG_ZONE_LOBBY_PVP_CONFIG_BRD:
	case CG_ZONE_PVP_CREATE_NAK:
	case CG_ZONE_PVP_JOIN_ACK:
	case CG_ZONE_PVP_JOIN_NAK:
#endif

	case CG_ZONE_LOBBY_READY_ACK:
	case CG_ZONE_LOBBY_READY_NAK:
	case CG_ZONE_LOBBY_READY_BRD:				//< 

	case CG_ZONE_LOBBY_READY_CANCEL_ACK:
	case CG_ZONE_LOBBY_READY_CANCEL_NAK:
	case CG_ZONE_LOBBY_READY_CANCEL_BRD:

	case CG_ZONE_LOBBY_NONBLOCKSLOTNUM_ACK:
	case CG_ZONE_LOBBY_NONBLOCKSLOTNUM_NAK:
	case CG_ZONE_LOBBY_NONBLOCKSLOTNUM_BRD:
	case CG_ZONE_LOBBY_CHANGE_MAP_ACK:
	case CG_ZONE_LOBBY_CHANGE_MAP_NAK:
	case CG_ZONE_LOBBY_CHANGE_MAP_BRD:

	case CG_ZONE_LOBBY_KICKPLAYER_ACK:
	case CG_ZONE_LOBBY_KICKPLAYER_NAK:

	case CG_ZONE_LOBBY_HUNTING_CONFIG_ACK:
	case CG_ZONE_LOBBY_HUNTING_CONFIG_NAK:
	case CG_ZONE_LOBBY_HUNTING_CONFIG_BRD:	

	//CG_ZONE_HUNTING_CREATE_ACK,	
	case CG_ZONE_HUNTING_CREATE_NAK:
	//CG_ZONE_MISSION_CREATE_ACK,		
	case CG_ZONE_MISSION_CREATE_NAK:

	//	case CG_ZONE_HUNTING_JOIN_ACK:
//	case CG_ZONE_HUNTING_JOIN_NAK:
	//CG_ZONE_MISSION_JOIN_SYN,	
	case CG_ZONE_MISSION_JOIN_ACK:
	case CG_ZONE_MISSION_JOIN_NAK:
	case CG_ZONE_LOBBY_LEAVE_ACK:
	case CG_ZONE_LOBBY_LEAVE_NAK:
	case CG_ZONE_MASTER_CHANGED_BRD:
	case CG_ZONE_LOBBY_PVP_REQUEST_INFO_ACK:
	case CG_ZONE_LOBBY_REQUEST_INFO_NAK:
	case CG_ZONE_LOBBY_PVP_INFO_BRD:
	case CG_ZONE_LOBBY_HUNTING_REQUEST_INFO_ACK:
	case CG_ZONE_LOBBY_MISSION_REQUEST_INFO_ACK:
		{
			//if (IsLobby())
			//	theLobbyDialog.NetworkProc( pMsg )
		}
		break;



	default:
		{
			ASSERT(!"Invalid protocol");
		}
		break;
	}
}

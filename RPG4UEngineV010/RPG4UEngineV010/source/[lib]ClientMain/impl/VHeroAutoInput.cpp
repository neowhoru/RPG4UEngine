/*////////////////////////////////////////////////////////////////////////
文 件 名：VHeroAutoInput.cpp
创建日期：2006年11月2日
最后更新：2006年11月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VHeroAutoInput.h"
#include "VHeroActionInput.h"
#include "VUCtrlManager.h"
#include "GameClientApplication.h"
#include "SkillSpecialEffectManager.h"
#include "ISkillSpecialEffect.h"
#include "VHero.h"
#include "VObjectManager.h"
#include "Hero.h"
#include "GameParameter.h"
#include "PacketInclude.h"


using namespace input;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace vobject;
GLOBALINST_SINGLETON_IMPL(VHeroAutoInput, ()  , gamemain::eInstPrioGameFunc);


namespace vobject
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VHeroAutoInput::VHeroAutoInput()
{
}
VHeroAutoInput::~VHeroAutoInput()
{
}

//BOOL VHeroAutoInput::FrameMove(DWORD dwTick)
//{
//
//	return TRUE;
//}







void VHeroAutoInput::MoveHeroTo( const VECTOR3D& vDest )
{
	theHero.SetNextState( STATE_MOVE,g_CurTime );
	if(!theHero.Move(theHero.GetPosition()
                   ,vDest
						 ,MOVETYPE_RUN
						 ,theInputLayer.IsKeyPress(KEYC_RCONTROL)
						 ,-1
						 ,-1))
		return;

//m_bHeroActionAtServerSync

	MSG_CG_SYNC_MOVE_SYN	SendPacket;
	ZeroMemory(&SendPacket,sizeof(SendPacket));
	SendPacket.m_dwKey = theGeneralGameParam.GetUserID();
	SendPacket.m_byCategory = CG_SYNC;
	SendPacket.m_byProtocol = CG_SYNC_MOVE_SYN;

	theHero.MakePathPacket( &SendPacket );

	theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(MSG_CG_SYNC_MOVE_SYN) );

	theGameUIManager.SetMovePointer(TRUE,vDest);
}


// 移动主角到目标
void VHeroAutoInput::MoveHeroTo( VObject* pTargetPlayer, BOOL bClose, float fCorrect, BOOL bPick )
{
	float fXOff = 0;
	float fYOff = 0;
	float fDist = 0;
	float f = 0;
	float fRange = 0;

	fRange = 0;

	Vector3D	vDisc = pTargetPlayer->GetPosition() - theVHero.GetPosition();
	fDist	= vDisc.Length();

	if( fDist == 0 )
		return;

	fRange += pTargetPlayer->GetBodySize();
	if( !bPick )
	{
		if( !bClose )
		{
			fRange += theVHero.GetBodySize();
		}
	}

	if( !bClose )
	{
		fRange += theVHero.GetWeaponAttackRange();
	}

	float fDir = atan2f(fYOff,fXOff);
	theVHero.SetDirectionLower( fDir );
	theVHero.SetDirection(fDir);

	if( fDist > fRange )
	{
		f = fRange/fDist;
		if( f < 1 )
		{
			g_InputData.m_TargetPos.x = theVHero.GetPosition().x + fXOff*((fDist-fRange+fCorrect)/fDist);
			g_InputData.m_TargetPos.y = theVHero.GetPosition().y + fYOff*((fDist-fRange+fCorrect)/fDist);
			g_InputData.m_TargetPos.z = 0;
			theVHero.SetMoving( TRUE );
			g_InputData.m_HeroCounterMarch = FALSE;

			theGameUIManager.SetMovePointer(FALSE);
			return;
		}
	}
}

//
void VHeroAutoInput::TurnHeroTo( VObject* pTargetPlayer )
{
	if(pTargetPlayer->IsHero() )
		return;

	Vector3D vDir = pTargetPlayer->GetPosition() - theVHero.GetPosition();
	vDir.Normalize();

	theVHero.SetTurnDirection( vDir );

	float fDir = atan2f(vDir.y, vDir.x);
	theVHero.SetDirectionLower(fDir);
	theVHero.SetDirection(fDir);
	theVHero.SetDirection(fDir, FALSE);
}






};//namespace vobject


/*////////////////////////////////////////////////////////////////////////
文 件 名：VMonster.cpp
创建日期：2007年4月14日
最后更新：2007年4月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VMonster.h"
#include "VCharacterAnimationCtrl.h"
#include "Monster.h"
#include "VUCtrlPicture.h"
#include "GraphicResourceList.h"
#include "ResourceManager.h"
#include "SeriesInfoParser.h"

namespace vobject
{
const DWORD SOUND_TIMER	= 15000;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VMonster::VMonster()
{
	m_dwObjectType		= MONSTER_OBJECT;
	//m_dwMonsterID	= INVALID_DWORD_ID;
	m_pMonster			= NULL;
	m_dwShowNameFlag	= OBJNAME_SHOWMONSTER;

}
VMonster::~VMonster()
{
}

void	VMonster::OnCreate(Object* pObject)
{
	_SUPER::OnCreate(pObject);

	if(!m_pMonster)
		return;

	/////////////////////////////////////////////////
	//RESOURCE_INFO*	pInfo;
	DWORD dwSeries = m_pMonster->GetMonsterInfo()->m_bySeries;

	InitSericeIcon(dwSeries);
}

void	VMonster::SetObject(Object* pObject)
{
	_SUPER::SetObject(pObject);
	m_pMonster = (Monster*)pObject;

}


BOOL	VMonster::OnMeshLoading()
{
	BOOL bRet = FALSE;
	if( m_nModelId !=-1 )
	{
		if( m_bTwoHalfBody )
		{
			m_pAnim->SetNeedInitUpperBodyAndLowerBody(TRUE);
		}
		bRet = m_pAnim->ReloadMesh( m_nModelId );
	}



	/////////////////////////////////////////////////

	return bRet;
}


BOOL	VMonster::CanRotateAnim()
{
	return GetMonsterType() != NPC_GRADE_BUILDING;
}

BOOL	VMonster::CanShowPopupText()
{
	if(  GetMonsterType() == NPC_GRADE_MINE
      || GetMonsterType() == NPC_GRADE_METERIAL
		|| GetMonsterType() == NPC_GRADE_TREASURE
		|| GetMonsterType() == NPC_GRADE_BUILDING )
	{
		return FALSE;
	}
	return TRUE;
}


void VMonster::SetNPCType(DWORD dwType)
{
	eOBJECT_TYPE type = (eOBJECT_TYPE)GetObjectType();
	switch(dwType)
	{
	case NPC_GRADE_GENERAL	: type = MONSTER_OBJECT; break;
	case NPC_GRADE_LEADER	: type = MONSTER_LEADER; break;
	case NPC_GRADE_BOSS		:	type = MONSTER_BOSS; break;
	case NPC_GRADE_ELITE		:	type = MONSTER_GOD; break;
	case NPC_GRADE_MINE		:	type = COLLECTION_MINE; break;
	case NPC_GRADE_METERIAL :  type = COLLECTION_WOOD; break;
	case NPC_GRADE_TREASURE	:	type = COLLECTION_TREASURE; break;
	case NPC_GRADE_BUILDING	:	type = BUILDING_OBJECT; break;
	}
	SetObjectType(type);
	m_dwMonsterType = dwType;
}


BOOL   VMonster::CanCollection()
{
	return  GetMonsterType() == NPC_GRADE_MINE
			|| GetMonsterType() == NPC_GRADE_METERIAL
			|| GetMonsterType() == NPC_GRADE_TREASURE;
}


COLOR	VMonster::GetNameColor()
{
	INT	nLevelDiff;

	//COLOR_LEVEL_MINUS_5	
	//COLOR_LEVEL_MINUS_4	
	//COLOR_LEVEL_MINUS_3	
	//COLOR_LEVEL_MINUS_2	
	//COLOR_LEVEL_MINUS_1	
	//COLOR_LEVEL_DIFF_0  
	//COLOR_LEVEL_PLUG_1  
	//COLOR_LEVEL_PLUG_2	
	//COLOR_LEVEL_PLUG_3	
	//COLOR_LEVEL_PLUG_4	
	//COLOR_LEVEL_PLUG_5	
	const	INT	LEVEP_STEP	= 5;
	INT			nColorIndex	= COLOR_LEVEL_DIFF_0;

	if(!m_pMonster)
		return _SUPER::GetNameColor();

	nLevelDiff	=  m_pMonster->GetMonsterInfo()->m_DisplayLevel - theHero.GetLevel();
	nLevelDiff	+= math::Sign(nLevelDiff) * ((LEVEP_STEP+1)/2);
	nLevelDiff  /= LEVEP_STEP; 
	
	nColorIndex	= nLevelDiff + COLOR_LEVEL_DIFF_0;
	nColorIndex	= math::Clamp(nColorIndex,(INT)COLOR_LEVEL_MINUS_5, (INT)COLOR_LEVEL_PLUG_5);

	return _COLOR(nColorIndex);
}


//void	VMonster::Destroy()
//{
//	_SUPER::Destroy();
//}

void	VMonster::Reset()
{
	_SUPER::Reset();
}

void	VMonster::Update( DWORD dwTick )
{
	_SUPER::Update(dwTick);
	


}

void   VMonster::UpdateManual( DWORD dwTime )
{
	if(! m_bMounting )
	{

	}

	_SUPER::UpdateManual(  dwTime);
}

BOOL	VMonster::RenderInfo( DWORD dwTick, DWORD dwColor , float fTransparent, float fPlayerTransparent)
{
	__BOOL_SUPER(RenderInfo( dwTick,  dwColor ,  fTransparent,  fPlayerTransparent));
	return TRUE;
}





//BOOL VMonster::_DoStatusMove()
//{
//	__BOOL_SUPER(_DoStatusMove());
//	return TRUE;
//}


};//namespace vobject


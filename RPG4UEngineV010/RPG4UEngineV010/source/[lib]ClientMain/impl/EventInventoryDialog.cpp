/*////////////////////////////////////////////////////////////////////////
文 件 名：EventInventoryDialog.cpp
创建日期：2008年5月13日
最后更新：2008年5月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "EventInventoryDialog.h"
#include "SlotUIListener.h"
#include "slotkeygenerator.h"
#include "PacketInclude.h"
#include "PacketStruct_ClientGameS_Event.h"


#define EVENT_INVENTORY_REFRESH_DELAY	1000


//----------------------------------------------------------------------------
EventInventoryDialog::EventInventoryDialog(void)
{
}

//----------------------------------------------------------------------------
EventInventoryDialog::~EventInventoryDialog(void)
{
	assert(m_pPressRefreshDelay == NULL && "Should call release manual");
	SAFE_DELETE(m_pPressRefreshDelay);
}

//----------------------------------------------------------------------------
BOOL EventInventoryDialog::Init( SLOTPOS MaxSlotSize, SLOTINDEX SlotIdx )
{
	__BOOL_SUPER(Init(MaxSlotSize, SlotIdx));
	//ItemSlotContainer::Init(MAX_EVENT_INVENTORY_SLOT_NUM, SI_EVENT_INVENTORY);

	m_pSlotListener = NULL;

	m_pPressRefreshDelay = new util::Timer;
	return TRUE;
}


//----------------------------------------------------------------------------
VOID	EventInventoryDialog::Release()
{
	_SUPER::Release();
	SAFE_DELETE(m_pPressRefreshDelay);
}



//----------------------------------------------------------------------------
VOID	EventInventoryDialog::NetworkProc( MSG_BASE * /*pMsg*/ )
{

}





//----------------------------------------------------------------------------
VOID EventInventoryDialog::ShowWindow(BOOL /*val*/)
{

}




//----------------------------------------------------------------------------
BOOL EventInventoryDialog::InsertSlot( SLOTPOS /*posIndex*/, BaseSlot & IN /*slotDat*/ )
{


	return TRUE;
}

//----------------------------------------------------------------------------
VOID EventInventoryDialog::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	ASSERT( FALSE == IsEmpty(posIndex) );


	BaseSlot & slotDat = GetSlot(posIndex);
	m_pSlotListener->OnSlotRemove( slotDat.GetSerial() );

	theSlotKeyGenerator.Restore(slotDat.GetSerial());

	BaseContainer::DeleteSlot(posIndex, pSlotOut );

	UpdateSlotListener();
}

//----------------------------------------------------------------------------
VOID EventInventoryDialog::UpdateSlot( SLOTPOS /*posIndex*/, BaseSlot & IN /*slotDat*/ )
{
}

//VOID EventInventoryDialog::OnLbuttonClickSlot()
//{
	//MouseHandler::ItemTransaction( SI_EVENT_INVENTORY, GetRealSlotPos(pos),);
//}


//----------------------------------------------------------------------------
VOID	EventInventoryDialog::UpdateSlotListener()
{

}



//----------------------------------------------------------------------------
VOID EventInventoryDialog::FlushSlotListener()
{
	for (SLOTPOS i = 0; i < GetSlotMaxSize(); i++)
	{
		if( !IsEmpty(i) )
			DeleteSlot(i, NULL);
	}

}


//----------------------------------------------------------------------------
VOID EventInventoryDialog::SendItemMoveMsg( SLOTPOS FromPos )
{
	if ( !BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_MOVE_ITEM ) )
	{
		BIT_ADD( m_dwPacketStatus, PACKET_STATUS_MOVE_ITEM);
		
		
		ASSERT( FromPos <= GetSlotMaxSize() );

		m_iPendingMovePos		= FromPos;

		MSG_CG_EVENT_MOVE_ITEM_TO_INVEN_SYN	SendPacket;
		SendPacket.m_ItemPos = FromPos;

		theNetworkLayer.SendPacket	(CK_GAMESERVER
                              ,&SendPacket
										,sizeof(SendPacket));
	}
}




//----------------------------------------------------------------------------
VOID EventInventoryDialog::SendRequestFetchEventItem()
{
	if ( !BIT_CHECK(m_dwPacketStatus, PACKET_STATUS_REFRESH) )
	{
		BIT_ADD(m_dwPacketStatus, PACKET_STATUS_REFRESH);
		m_pPressRefreshDelay->Reset();

		MSG_CG_EVENT_SELECT_SYN		SendPacket;
		theNetworkLayer.SendPacket	(CK_GAMESERVER
                              ,&SendPacket
										,sizeof(SendPacket));
	}
}
/*////////////////////////////////////////////////////////////////////////
文 件 名：NetworkLayer.cpp
创建日期：2006年5月23日
最后更新：2006年5月23日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NetworkLayer.h"
#include "PacketInclude.h"
#include "ApplicationBase.h"
#include "NetworkSystem.h"
#include "SystemBaseListener.h"
#include "PacketHandlerManager.h"
#include "NetworkSystemManager.h"
#include "LoginScene.h"
#include "ConstTextRes.h"
#include "CharacterScene.h"
#include "DemoSystemManager.h"

#include "NetworkLayer.inc.h"

using namespace util;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(NetworkLayer, ()  , gamemain::eInstPrioNetworkInput);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NetworkLayer::NetworkLayer()
{
	m_bLoginServerIP	= FALSE;
	m_bDisconnectting	= FALSE;
	m_bChangeServer	= FALSE;
	m_pNetwork			= &theNetworkClient;
	m_nChatTryCount	= 0;
}

NetworkLayer::~NetworkLayer()
{
}



static NetworkLayerListener	gs_networkListener;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL  NetworkLayer::Init()
{
	if(theGeneralGameParam.IsEnableNetwork())
	{
		//监听
		theNetworkSystemManager.AddListener(&gs_networkListener);
		theApplication.AddListener(this);

		///注册
		theNetworkSystemManager.RegNetworkSystem(&theNetworkClient);
		theNetworkSystemManager.ActivateNetworkSystem(&theNetworkClient);


		m_pNetwork->Init( 2, ProcNetworkRecv, ProcNetworkDisconnect, ProcNetworkError );
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void  NetworkLayer::Release()
{
	// socket close

	if( theGeneralGameParam.IsEnableNetwork())
	{
		if(m_pNetwork->IsConnected(CK_GAMESERVER))
			m_pNetwork->Disconnect(CK_GAMESERVER);

		if(m_pNetwork->IsConnected(CK_CHATSERVER))
			m_pNetwork->Disconnect(CK_CHATSERVER);

		//MSG_CA_CONNECTION_UNREGISTER_SYN		SendPacket;

		//m_pNetwork->Send( CK_GAMESERVER, &SendPacket, sizeof(MSG_CG_CONNECTION_UNREGISTER_SYN) );

		/// 1.断开网络
		m_pNetwork->Disconnect( CK_GAMESERVER );
		theApplication.RemoveListener(this);
		m_pNetwork->Release();

		//2.反注册 [须在后调用]
		theNetworkSystemManager.ActivateDefaultNetwork();
		theNetworkSystemManager.UnregNetworkSystem(&theNetworkClient);

		//3.撤消监听
		theNetworkSystemManager.RemoveListener(&gs_networkListener);
	}

	//theGeneralGameParam.DestroyInstance();
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL NetworkLayer::FrameMove	(DWORD dwTick)
{
	if( theGeneralGameParam.IsEnableNetwork() )
	{
		m_pNetwork->Update();
		if( theGeneralGameParam.GetSpecialMode() )
		{
			for( int i = 0 ; i < CK_MAX ; ++i )
			{
				m_pNetwork->UpdateTimer( (eCONNECTION_KIND)i );
			}
		}
	}

	CheckReconnectChatServer	(dwTick);
	CheckReconnectLoginServer	(dwTick);


	//const DWORD dwTickBeat = theGeneralGameParam.GetTickHeartBeat();
	//
	//if (m_dwHeartBeatTimer >= dwTickBeat)
	//{
	//	if (m_pNetwork->IsEnableSendChatHeartBeat() &&
	//		m_pNetwork->IsConnected(CK_CHATSERVER) )
	//	{
	//		SendToChatHeartbeatSyn();
	//	}

	//	if (m_pNetwork->IsEnableSendHeartBeat()
	//		&& m_pNetwork->IsConnected(CK_GAMESERVER)) 
	//	{
	//		MSG_CG_CONNECTION_HEARTBEAT_SYN	SendPacket;
	//		theNetworkLayer.SendPacket( CK_GAMESERVER, &SendPacket, sizeof(SendPacket) );
	//	}		

	//	m_dwHeartBeatTimer -= dwTickBeat;
	//}
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL  NetworkLayer::ConnectToLoginServer()
{
	if(!theGeneralGameParam.IsEnableNetwork())
		return TRUE;

	if (m_pNetwork->IsConnected(CK_LOGINSERVER))
		return TRUE;

	int			iConnectRet = FALSE;
	INT			i(0);
	int			tryCounts		= 0;  //   
	vector<int> arTryIndex;
	INT			nLoginIPCount(0);


	{
		////////////////////////////////////

		nLoginIPCount = theLoginGameParam.GetLoginIPNum();
		////////////////////////////////////
		while (	tryCounts < nLoginIPCount
				&& iConnectRet != TRUE )
		{
			INT nRandomIdx;
			
			nRandomIdx = math::Random(nLoginIPCount);

			if (nRandomIdx >= nLoginIPCount)
				continue;

			BOOL bTried = FALSE;

			//////////////////////////////////////////
			for (i=0; i<(INT)arTryIndex.size(); ++i)
			{
				if (arTryIndex[i] == nRandomIdx)
				{
					bTried = TRUE;
					break;
				}
			}

			////////////////////////////////////////////////
			if (bTried)
				continue;

			////////////////////////////////////////////////
			arTryIndex.push_back(nRandomIdx);


			sLOGINIP_INFO*	pIPInfo;
			pIPInfo = theLoginGameParam.GetLoginIP(nRandomIdx);
			if(!pIPInfo)
				continue;


			if (!m_bLoginServerIP)
			{
				theLoginGameParam.SetSelectedServiceLoginServerIP	(pIPInfo->szIP);
				theLoginGameParam.SetSelectedServiceLoginServerPort(pIPInfo->port);
			}

			iConnectRet = m_pNetwork->Connect(CK_LOGINSERVER
                                         ,theLoginGameParam.GetSelectedServiceLoginServerIP()
													  ,theLoginGameParam.GetSelectedServiceLoginServerPort());


			m_bLoginServerIP = (iConnectRet != 0);

			tryCounts ++;
			//Sleep(1);

			//justTryCounts ++;
			//if (justTryCounts > MAX_AUTH_SEREVR_CHOICE)
			//    break;
		}
	}

	/////////////////////////////////////
	if( nLoginIPCount == 0 )
	{
		iConnectRet = m_pNetwork->Connect	(CK_LOGINSERVER
													,theGeneralGameParam.GetLoginServerIP()
													,theGeneralGameParam.GetLoginServerPort());
	}


	/////////////////////////////////////
	if( ! iConnectRet )
	{
		//Sleep(1000);
		OUTPUTTIP(TEXTRES_ConnectFail);
		theGameUIManager.UIShowMessageBox(TEXTRES_LoginServerFailedAndTry,
										TEXTRES_UI_ask,
										MB_YESNO,
										TRUE,
										ProcDisconnecttedLogin);
	}
	else
	{
	}

	return iConnectRet;
}


void NetworkLayer::OnWinMsg( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	m_pNetwork->WindowProc(hWnd,  uMsg,  wParam,  lParam);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID NetworkLayer::NetworkRecv( DWORD dwIndex, BYTE * pData )
{
	MSG_BASE*	pMsg = (MSG_BASE*)pData;

#ifdef USE_NETWORK_RECV_LOG
	LOGINFO	("RECV:%d#,Cat:%d,Pro:%d\n"
				,dwIndex
				,pMsg->m_byCategory
				,pMsg->m_byProtocol );
#endif

	ParsePacket(pMsg, dwIndex);
}


//------------------------------------------------------------------
BOOL NetworkLayer::SendPacket	(eCONNECTION_KIND idx
                              ,void *            pBuf
										,int               iLength
										,Character*        pSender)
{
	if( theGeneralGameParam.IsEnableNetwork() )
	{
		
		if ( theNetwork.IsConnected(idx) )
		{
#if USE_NETWORK_SEND_LOG
			MSG_BASE * pPacket = (MSG_BASE * )pBuf;

			char szText[0xff];

			Sprintf(szText, "[SEND] Idx : %d, Catagory : %d, Protocol : %d \n", 
				idx, pPacket->m_byCategory, pPacket->m_byProtocol);

			OutputDebugString(szText);
#endif

			return theNetwork.Send(idx, (BYTE*)pBuf, iLength);
		}
		return FALSE;
	}
	else
	{
		__CHECK(theGeneralGameParam.IsLocalHeroData());

		if(pSender)
		{
			return theDemoSystemManager.ParsePacket	((MSG_BASE*)pBuf
																	,iLength
																	,pSender);
		}
		return TRUE;
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL NetworkLayer::ParsePacket(MSG_BASE* pMsg, DWORD dwIndex)
{
	__CHECK_PTR(pMsg);

	switch( dwIndex )
	{
	case CK_GAMESERVER:
		{
			if(!thePacketHandlerManager.ParsePacket(dwIndex,pMsg ))
				theFrameworkSystem.ParsePacket( pMsg );
		}
		break;

	case CK_CHATSERVER:
		{
			if(!thePacketHandlerManager.ParsePacket(dwIndex,pMsg ))
				theFrameworkSystem.ParsePacket( pMsg );
		}
		break;

	case CK_LOGINSERVER:
		{
			if(!thePacketHandlerManager.ParsePacket(dwIndex,pMsg ))
				theFrameworkSystem.ParsePacket (pMsg);
		}
		break;
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID NetworkLayer::NetworkDisconnect( DWORD dwIndex )
{
	switch( dwIndex )
	{
		//////////////////////////////////
	case CK_GAMESERVER:
		{
			theCharacterScene.OnDisconnect();
			if (	!IsChangeServer()
				&&	!m_bDisconnectting)
			{
			   OUTPUTTIP(TEXTRES_ConnectFail);
				theGameUIManager.UIShowMessageBox(TEXTRES_GAMESERVER_DISCONNECT_MSG,
												TEXTRES_UI_ask,
												MB_YESNO,
												TRUE,
												ProcDisconnectted);
			}
			m_bDisconnectting	= TRUE;
		}
		break;

		//////////////////////////////////
	case CK_CHATSERVER:
		{
			if (!IsChangeServer())
			{
				OnReconnectToChatServer();
			}
		}
		break;

		//////////////////////////////////
	case CK_LOGINSERVER:
		{
			theLoginScene.OnDisconnect();

			//在未连接GameServer之前
			if(	!m_pNetwork->IsConnected(CK_GAMESERVER) 
				&& !m_bDisconnectting)
			{
				OUTPUTTIP(TEXTRES_Disconnectted);

				theGameUIManager.UIShowMessageBox(TEXTRES_DisconnecttedAndRetry,
												TEXTRES_UI_ask,
												MB_YESNO,
												TRUE,
												ProcDisconnecttedLogin);
			}
			m_bDisconnectting	= TRUE;


		}
		break;
	}	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID NetworkLayer::NetworkError( char * pszMsg )
{
	FILE * fp = fopen( "NetworkErrorLog.txt", "a+t" );
	fwrite( pszMsg, strlen(pszMsg), 1, fp );
	fclose( fp );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL NetworkLayer::OnReconnectToChatServer()
{

	__CHECK(m_pNetwork->IsConnected(CK_GAMESERVER));

	__CHECK(!m_pNetwork->IsConnected(CK_CHATSERVER));


	m_nChatTryCount = 0;

	m_pNetwork->SetEnableSendChatHeartBeat(FALSE);

	m_ChatReconnectTimer.SetTimer( theGeneralGameParam.GetWorldServerReconnectDelay() );
	OUTPUTTIP(TEXTRES_WORLDSERVER_RECONNECT_MSG);

	return TRUE;
}


void NetworkLayer::AfterReconnectToChatServer()
{
	m_ChatReconnectTimer.DisableCheckTime();

}

void NetworkLayer::CheckReconnectChatServer(DWORD /*dwTick*/)
{

	if (!m_ChatReconnectTimer.IsCheckTime())
		return;

	if (!m_pNetwork->IsConnected(CK_GAMESERVER))
		return;

	if (m_pNetwork->IsConnected(CK_CHATSERVER))
	{
		AfterReconnectToChatServer();
		return;
	}

	////////////////////////////////////////////
	if(m_ChatReconnectTimer.IsExpired())
	{
		MSG_CG_PREPARE_WORLD_CONNECT_SYN SendPacket;
		theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket) );

		m_nChatTryCount++;
	}


}


void NetworkLayer::ReconnectToChatServer()
{
	m_pNetwork->Disconnect(CK_CHATSERVER,TRUE);
}

void NetworkLayer::CheckReconnectLoginServer(DWORD /*dwTick*/)
{
	if(m_LoginReconnectTimer.IsExpiredOnce())
	{
		ConnectToLoginServer();
		//theGameUIManager.UIOnProcess	(gameui::eUIProgress);
	}
}

BOOL NetworkLayer::ReconnectToLoginServer()
{
	if (m_pNetwork->IsConnected(CK_LOGINSERVER))
		return TRUE;
		//m_pNetwork->Disconnect(CK_LOGINSERVER);

	OUTPUTTOP(_STRING(TEXTRES_NowTryConnecting));
	m_LoginReconnectTimer.SetTimer(theGeneralGameParam.GetLoginReconnectDelay());

	theGameUIManager.UIShowByTime	(gameui::eUIProgress
											,g_CurTime
                                ,theGeneralGameParam.GetLoginReconnectDelay() + 200
										  ,true
										  ,1
										  ,true
										  ,true
										  ,_STRING(TEXTRES_USERLOGINING));

	return FALSE;
}

void NetworkLayer::Reconnect()
{
	theFrameworkSystem.OnReconnect();
}


bool NetworkLayer::ProcDisconnectted( const bool bPressYesButton, void * /*pData*/ )
{
	if(bPressYesButton)
	{
		theNetworkLayer.SetDisconnectting(FALSE);
		theSceneLogicFlow.GotoLoginScene();
	}
	else
	{
		theApplication.Exit();
	}
	return bPressYesButton;
}

bool NetworkLayer::ProcDisconnecttedLogin( const bool bPressYesButton, void * /*pData*/ )
{
	if(bPressYesButton)
	{
		theNetworkLayer.SetDisconnectting(FALSE);
		theNetworkLayer.Reconnect();
		//theNetworkLayer.ConnectToLoginServer();
	}
	else
	{
		theApplication.Exit();
	}
	return bPressYesButton;
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemLinkHandler.cpp
创建日期：2008年5月31日
最后更新：2008年5月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemLinkHandler.h"
#include "ItemManager.h"
#include "BaseContainer.h"
#include "QuickPaneSlot.h"
#include "SlotKeyGenerator.h"
#include "SkillStorageManager.h"
#include "SkillSlot.h"
#include "DummyItemSlot.h"
#include "DummySlotContainer.h"


//------------------------------------------------------------------------
VOID ItemLinkHandler::LinkItemToQuick	(SLOTINDEX  atIndex
														,SLOTPOS  OrgPos
														,SLOTPOS  toQuickPos
														,SLOTCODE OrgCode)
{
	//if (!singleton::ExistHero())
	//	return;

	BaseContainer * pFromContainer	= theItemManager.GetContainer( atIndex );
	BaseContainer * pToContainer		= theItemManager.GetContainer( SI_QUICK );
   ASSERT (pFromContainer);
   ASSERT (pToContainer);
	

	///////////////////////////////////////////////////////
	QuickPaneSlot		slotQuickTemp;
	sITEMINFO_BASE *	pItemInfo;
	SLOTPOS				FromPos	= OrgPos;
	BOOL					bRet		= TRUE;

	slotQuickTemp.Clear();
	
	pItemInfo = theItemInfoParser.GetItemInfo( OrgCode );
	__VERIFY2_PTR(pItemInfo, "无效的ItemInfo.",);



	//////////////////////////////////////////////
	if ( FromPos == INVALID_QUICKSLOT) 
	{
		bRet = FALSE;
	}
	else
	{
		if ( pItemInfo->IsCanEquip() )
		{
			if (pFromContainer->IsEmpty(FromPos))
			{
				bRet = FALSE;	
			}
			else
			{
				BaseSlot & slotDat = pFromContainer->GetSlot(OrgPos);
				if (slotDat.GetCode() != OrgCode)
				{
					bRet = FALSE;
				}
			}
		}
		else
		{
			if (pFromContainer->IsEmpty(FromPos))
			{
				bRet = theItemManager.LocateItemAtFirst(OrgCode , FromPos);	
			}
			else
			{
				BaseSlot & slotDat = pFromContainer->GetSlot(OrgPos);
				if (slotDat.GetCode() != OrgCode)
				{
					bRet = theItemManager.LocateItemAtFirst(OrgCode , FromPos);	
				}
			}
		}
	}

	//////////////////////////////////////////////////////////////
	DWORD dwSerial = theSlotKeyGenerator.GetKey();

	if(!bRet)
	{
		slotQuickTemp.SetOrgSerial		( dwSerial );
		slotQuickTemp.SetOrgCode		( OrgCode );
		slotQuickTemp.SetOrgSlotIndex	( atIndex );
		slotQuickTemp.SetOrgPos			(OrgPos);
		slotQuickTemp.SetOrgSlotType	( ST_ITEM );
		slotQuickTemp.SetOverlapCount(0);

		ASSERT(OrgCode != 0);
		slotQuickTemp.SetStatus			( eQUICK_DEACTIVATE );
	}
	else
	{
		BaseSlot & slotFrom  = pFromContainer->GetSlot( FromPos );

		slotQuickTemp.SetOrgSerial		( dwSerial );
		slotQuickTemp.SetOrgCode		( slotFrom.GetCode() );
		slotQuickTemp.SetOrgSlotIndex	( atIndex );
		slotQuickTemp.SetOrgPos			( OrgPos );					//
		slotQuickTemp.SetOrgSlotType	( ST_ITEM );
		slotQuickTemp.SetOrgSlot		( &slotFrom );
		slotQuickTemp.SetStatus			( eQUICK_ACTIVATE );

		ItemSlot & slotItemFrom = (ItemSlot &)slotFrom;
		if (slotItemFrom.IsOverlap())
		{
			slotQuickTemp.SetOverlapCount((WORD) theItemManager.GetItemToltalAmount(slotFrom.GetCode()) );
		}
		else
		{
			slotQuickTemp.SetOverlapCount( 0 );
		}
	}

		
	pToContainer->DeleteSlot( toQuickPos, NULL );
	pToContainer->InsertSlot( toQuickPos, slotQuickTemp );
}


//---------------------------------------
VOID ItemLinkHandler::LinkSkillToQuick(SLOTINDEX /*atIndex*/
                                          ,SLOTCODE  OrgCode
														,SLOTPOS   toQuickPos)
{

	sSKILLINFO_BASE * pSkillInfo;

	pSkillInfo = (sSKILLINFO_BASE *)theSkillInfoParser.GetSkillInfo(OrgCode);

	if (pSkillInfo)
	{
		BaseContainer *	pToContainer;
		SkillSlot *			pOrgSkillSlot;
		DWORD					dwSerial;
		
		pToContainer	 = theItemManager.GetContainer( SI_QUICK );
		ASSERT (pToContainer);

		pOrgSkillSlot = theSkillStorageManager.GetSkillSlot(OrgCode);

		__CHECK2_PTR(pOrgSkillSlot,);


		////////////////////////////////
		QuickPaneSlot slotQuickTemp;
		slotQuickTemp.Clear();
		slotQuickTemp.SetOrgSlotIndex( SI_SKILL );	

		dwSerial = theSlotKeyGenerator.GetKey();
		ASSERT( OrgCode == pOrgSkillSlot->GetCode() && "物品代码不一样!!" );	
      
		slotQuickTemp.SetOrgCode		( pOrgSkillSlot->GetCode() );
		slotQuickTemp.SetOrgSlotType	( ST_SKILL );
		slotQuickTemp.SetOrgSerial		( dwSerial );	
		slotQuickTemp.SetOrgSlot		( pOrgSkillSlot );
		
		if (pToContainer->IsEmpty(toQuickPos))
		{
			pToContainer->InsertSlot( toQuickPos, slotQuickTemp );
		}
		else
		{
			pToContainer->DeleteSlot( toQuickPos, NULL );
			pToContainer->InsertSlot(toQuickPos, slotQuickTemp);
		}
	}
	else
	{
		ASSERT(0);
	}
}


//---------------------------------------
VOID ItemLinkHandler::LinkStyle(SLOTCODE OrgCode, SLOTPOS toQuickPos )
{
	sSKILLINFO_COMMON *pInfo;
	
	pInfo = theSkillInfoParser.GetInfo(OrgCode);
	__CHECK2_PTR(pInfo,);

	sSKILLINFO_BASE * pSkillInfo = NULL;
	sSTYLEINFO_BASE * pStyleInfo = NULL;
		

	if (pInfo->IsSkill())
		pSkillInfo = (sSKILLINFO_BASE *)pInfo;

	else
		pStyleInfo = (sSTYLEINFO_BASE *)pInfo;


	if (pStyleInfo)
	{
		BaseContainer *	pToContainer;
		SkillSlot *			pOrgSkillSlot;
		StyleContainer *	pStyleDlg;
		
		pToContainer = theItemManager.GetContainer( SI_STYLE );
		ASSERT (pToContainer);

		pOrgSkillSlot = theSkillStorageManager.GetSkillSlot(OrgCode);

	__CHECK2_PTR(pOrgSkillSlot,);


		pStyleDlg = (StyleContainer *)theItemManager.GetContainer(SI_STYLE);

		if ( pStyleDlg->IsExistSameCodeItem( pOrgSkillSlot->GetCode() ) )
		{
			ASSERT(0);
			return;
		}

		////////////////////////////////////////////////
		StyleSlot	slotStyleTemp;
		slotStyleTemp.Clear();
		slotStyleTemp.SetOrgCode		( OrgCode );
		slotStyleTemp.SetOrgSlotType	( ST_SKILL );
		slotStyleTemp.SetOrgSlot		( pOrgSkillSlot );
		
		if (pToContainer->IsEmpty(toQuickPos))
		{
			pToContainer->InsertSlot( toQuickPos, slotStyleTemp );
		}
		else
		{
			pToContainer->DeleteSlot( toQuickPos, NULL );
			pToContainer->InsertSlot(toQuickPos, slotStyleTemp);
		}
	}
	else
	{
		ASSERT(0);
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemLinkHandler::LinkDummyItem	(SLOTINDEX fromIdx
                                          ,SLOTINDEX toIdx
														,SLOTPOS   fromPos
														,SLOTPOS   toPos)
{
	BaseContainer *		pFromContainer	= theItemManager.GetContainer( fromIdx );
	DummySlotContainer *	pToContainer	= (DummySlotContainer * )theItemManager.GetContainer( toIdx );


	ItemSlot &	FromItemSlot = (ItemSlot &)pFromContainer->GetSlot(fromPos);

	SlotHandle	dummyItemSlot(SLOTT_ITEMDUMMY);

	if((BOOL)dummyItemSlot)
	{
		DummyItemSlot&	 toDummyItemSlot = (DummyItemSlot&)(BaseSlot&)dummyItemSlot;

		toDummyItemSlot.SetOrgSlot( &FromItemSlot );
		toDummyItemSlot.SetFromContainerIdx(fromIdx);
		toDummyItemSlot.SetFromPosition(fromPos);
		
		pToContainer->DeleteSlot( toPos, NULL );
		pFromContainer->InsertSlot( fromPos, toDummyItemSlot );

		theItemManager. PlaySlotSound(SLOTSOUND_PUT, &FromItemSlot);
	}
}


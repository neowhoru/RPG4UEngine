/*////////////////////////////////////////////////////////////////////////
文 件 名：HeroParty.cpp
创建日期：2009年5月19日
最后更新：2009年5月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "HeroParty.h"
#include "Hero.h"
#include "BattleScene.h"
#include "ConstTextRes.h"
#include "PacketStruct_ClientGameS_Player.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(HeroParty, ()  , gamemain::eInstPrioGameFunc);



//CG_PARTY_MEMBER_INFO_CMD
HeroParty::HeroParty()
{
	m_dwDistributionType = 0;
	m_dwMasterRequesting	= 0;
	m_dwMasterPlayer		= 0;
}

HeroParty::~HeroParty()
{
}

void HeroParty::Invite(DWORD dwObjectKey)
{
	Object*	pObject;
	pObject = theObjectManager.GetObject(dwObjectKey);
	if(!pObject || !pObject->IsPlayer())
		return;

	MSG_CG_PARTY_INVITE_REQUEST_SYN sendMsg;
	lstrcpy	(sendMsg.m_szCharName,pObject->GetName());

	theHero.SendPacket(&sendMsg, sizeof(sendMsg),TRUE);
}

void HeroParty::KickOff(DWORD dwObjectKey)
{
	if(!GetMemberBy(dwObjectKey))
		return;
	if(!IsHeroMaster())
		return;

	MSG_CG_PARTY_FORCED_EXPULSION_SYN sendMsg;
	sendMsg.m_dwObjKey	= dwObjectKey;

	theHero.SendPacket(&sendMsg, sizeof(sendMsg),TRUE);
}

void HeroParty::OnKickOff(DWORD dwObjectKey)
{
	if(dwObjectKey == theHero.GetObjectKey())
	{
		DestroyParty();
	}
	else
	{
		RemoveMember(dwObjectKey);
	}
}

void HeroParty::LeaveHero()
{
	MSG_CG_PARTY_LEAVE_REQUEST_SYN sendMsg;
	theHero.SendPacket(&sendMsg, sizeof(sendMsg),TRUE);
}

void HeroParty::Disband()
{
	MSG_CG_PARTY_DESTROY_PARTY_SYN sendMsg;
	theHero.SendPacket(&sendMsg, sizeof(sendMsg),TRUE);
}


BOOL HeroParty::ResetPartyTo	(MSG_CG_PARTY_MEMBER_INFO_CMD* pMsg)
{
	m_Members.clear();
	m_MemberMap.clear();

	m_dwMasterPlayer	= theHero.GetObjectKey();
	if(pMsg)
	{
		//////////////////////////////////////
		for(UINT n=0; n<pMsg->m_byMemberNum; n++)
		{
			AddMember(pMsg->m_MemberInfo[n]);
		}
	}

	theHero.AddUpdateFlag(CHAR_UPDATE_PARTY);

	return TRUE;
}

void HeroParty::DestroyParty()
{
	ResetPartyTo(NULL);
	BattleScene::BackToTheVillageSyn();
}

void HeroParty::SelectTarget		(DWORD /*dwObjectKey*/)
{
	//strTargetMonsterName = pMonster->GetName();
	//Set_PartyLeaderTargetKey(msg->m_dwObjectKey);
	//Set_AssistPartyLeader(true);
}

BOOL HeroParty::AddMember(PARTY_MEMBER_STREAM& info)
{
	if(info.m_dwObjKey == theHero.GetObjectKey())
		return FALSE;

	Player*					pPlayer;
	sPARTY_MEMBER_INFO*	pInfo;

	pPlayer	= (Player*)theObjectManager.GetObject(info.m_dwObjKey);
	pInfo		= GetMemberBy(info.m_dwObjKey);

	//////////////////////////////////////////////
	if(!pInfo)
	{
		m_Members.resize(m_Members.size() + 1);
		pInfo = &m_Members.back();
		m_MemberMap.insert(PartyMemberPair(info.m_dwObjKey, pInfo));
	}

	//////////////////////////////////////////////
	pInfo->m_bMaster	= info.m_bMaster	;
	pInfo->m_dwObjKey	= info.m_dwObjKey	;
	pInfo->m_Level		= info.m_wLevel	;
	pInfo->m_dwHP		= info.m_dwHP		;
	pInfo->m_dwHPMax	= info.m_dwHPMax	;
	pInfo->m_dwMP		= info.m_dwMP		;
	pInfo->m_dwMPMax	= info.m_dwMPMax	;
	pInfo->m_CharName	= _VI(info.m_szCharName);

	pPlayer->SetMaxHP	(info.m_dwHPMax);
	pPlayer->SetMaxMP	(info.m_dwMPMax);
	pPlayer->SetHP		(info.m_dwHP);
	pPlayer->SetMP		(info.m_dwMP);

	if(info.m_bMaster)
		m_dwMasterPlayer	= info.m_dwObjKey;

	//////////////////////////////////////////////
	theHero.AddUpdateFlag(CHAR_UPDATE_PARTY);
	return TRUE;
}


void HeroParty::RemoveMember	(DWORD dwObjectKey)
{
	if(dwObjectKey == theHero.GetObjectKey())
		return;

	PartyMemberMapIt it;
	it = m_MemberMap.find(dwObjectKey);
	if(it == m_MemberMap.end())
		return;

	sPARTY_MEMBER_INFO*	pInfo		= it->second;
	sPARTY_MEMBER_INFO*	pFront	= &m_Members.front();

	INT nIndex = pInfo - pFront;
	m_Members.erase( m_Members.begin() + nIndex );

	///////////////////////////////
	m_MemberMap.erase(it);

	theHero.AddUpdateFlag(CHAR_UPDATE_PARTY);
}


BOOL HeroParty::ChangeMaster	(DWORD dwObjectKey)
{
	MSG_CG_PARTY_CHANGE_MASTER_SYN	msg;
	msg.m_dwObjKey	= dwObjectKey;
	theHero.SendPacket(&msg,sizeof(msg));

	return TRUE;
}

BOOL HeroParty::OnChangeMaster	(DWORD dwObjectKey)
{
	Player*	pPlayer;
	pPlayer = (Player*)theObjectManager.GetObject(dwObjectKey);

	OUTPUTTIPF	(TEXTRES_PARTY_CHANGE_MASTER
					,pPlayer?pPlayer->GetName() : _T(""));

	for(UINT n=0; n<m_Members.size(); n++)
	{
		m_Members[n].m_bMaster = (m_Members[n].m_dwObjKey == dwObjectKey);
	}

	m_dwMasterPlayer	= dwObjectKey;

	theHero.AddUpdateFlag(CHAR_UPDATE_PARTY);
	return TRUE;
}


void HeroParty::ChangeDistribution(DWORD dwType)
{
	m_dwDistributionType = dwType;
	theHero.AddUpdateFlag(CHAR_UPDATE_PARTY);
}

void HeroParty::ProcessError	(DWORD dwError)
{
	theHero.ProcessPlayerError(dwError, CG_PARTY);
}

void HeroParty::ProcAskInviting(INT   nTipIndex,sTIP_LOG* /*pLogInfo*/)
{
	//////////////////////////////////////////////
	Player*		pPlayer;

	if(theGeneralGameParam.IsEnableNetwork())
	{
		pPlayer = &theHero;
	}
	else
	{
		pPlayer = (Player*)theObjectManager.GetObject(theHeroActionInput.GetCurrentTarget());
		if(!pPlayer || !pPlayer->IsPlayer())
			return;
	}

	//////////////////////////////////////////////
	if(nTipIndex == 0)
	{
		MSG_CG_PARTY_INVITE_RESPONSE_ACK sendMsg;
		sendMsg.m_dwMasterUserKey = theHeroParty.GetMasterRequesting();

		theNetworkLayer.SendGamePacket(&sendMsg, sizeof(sendMsg),pPlayer);
	}
	else
	{
		OUTPUTTIPF(TEXTRES_PARTY_YOU_REJECT_INVITEMENT, (LPCTSTR)theHeroParty.m_sMasterRequestName);

		MSG_CG_PARTY_INVITE_RESPONSE_NAK sendMsg;
		sendMsg.m_dwMasterUserKey = theHeroParty.GetMasterRequesting();
		theNetworkLayer.SendGamePacket(&sendMsg, sizeof(sendMsg),pPlayer);
	}
}

void HeroParty::AskInviting(LPCTSTR szMasterName
                           ,DWORD   dwMasterUserKey)
{

	m_sMasterRequestName = szMasterName;
	//OUTPUTTIPF(TEXTRES_PARTY_INVITE_REQUEST_PLAYER, szMasterName);

	/////////////////////////////////////////
	StringHandle	sText;
	StringHandle	sText2;
	sText.Format	(_STRING(TEXTRES_INVITE_REQUEST_TIP ),   szMasterName);
	sText2.Format	(_STRING(TEXTRES_INVITE_REQUEST_TIP2),  szMasterName);




	m_dwMasterRequesting	= dwMasterUserKey;

	
	TIPGROUP	(sText ,eTIP_TEAM,ProcAskInviting);
	TIPGROUP	(sText2,eTIP_TEAM,ProcAskInviting, TRUE, TRUE);

	//theGameUIManager.UIShowMessageBox(sText
	//											,_STRING(TEXTRES_Prompt)
	//											,MB_YESNO
	//											,TRUE
	//											,ProcAskInviting);
}


BOOL HeroParty::IsHeroMaster	()
{
	return	GetMemberCount() > 0 
		&&		m_dwMasterPlayer == theHero.GetObjectKey();
}


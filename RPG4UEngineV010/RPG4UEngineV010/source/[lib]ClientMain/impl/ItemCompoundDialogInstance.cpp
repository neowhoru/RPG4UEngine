/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemCompoundDialogInstance.h
创建日期：2008年6月15日
最后更新：2008年6月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ItemCompoundDialogInstance.h"
#include "PacketInclude.h"
#include "SlotUIListener.h"
#include "DummyItemSlot.h"
#include "ItemCompositeParser.h"
#include "KeyQueueManager.h"
#include "EnchantInfoParser.h"
#include "ItemMakeParser.h"
#include "ItemAttrInfoParser.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemCompoundDialogInstance, ()  , gamemain::eInstPrioGameFunc);



//------------------------------------------------------------------------------
ItemCompoundDialogInstance::ItemCompoundDialogInstance(void)
{


}


//------------------------------------------------------------------------------
ItemCompoundDialogInstance::~ItemCompoundDialogInstance(void)
{
}


//------------------------------------------------------------------------------
BOOL	ItemCompoundDialogInstance::Init( )
{
	m_CurrentCompositeType = ITEMCOMPOSITE_NONE;
	__CHECK(InitContainer() );

	return TRUE;
}

//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::Release()
{
	ReleaseContainer();
}


//------------------------------------------------------------------------------
BOOL ItemCompoundDialogInstance::InitContainer()
{
	//ItemSlotContainer::Init	(eITEMENCHANT_MAX_SLOT,			SI_ENCHANT );
	//_SUPER::Init	(eITEMENCHANT_MAX_SLOT,			SI_ENCHANT );

	ContainerHandle	target	(SLOTT_ITEMHANDLE);
	ContainerHandle	subResult(SLOTT_ITEMHANDLE);
	ContainerHandle	socket	(SLOTT_ITEMHANDLE);
	ContainerHandle	main		(SLOTT_ITEMHANDLE);

	__CHECK_PTR(&target	);
	__CHECK_PTR(&subResult);
	__CHECK_PTR(&socket	);
	__CHECK_PTR(&main		);

	m_pTargetContainer		= target.Detach();
	m_pSubResultContainer	= subResult.Detach();
	m_pSocketContainer		= socket.Detach();
	m_pMainContainer			= main.Detach();

	m_pMainContainer			->Init(eITEMENCHANT_MAX_SLOT,					SI_ENCHANT);
	m_pTargetContainer		->Init(eITEMENCHANT_TARGET_MAX_SLOT,		SI_ENCHANT_TARGET);
	m_pSubResultContainer	->Init(eITEMENCHANT_SUBRESULT_MAX_SLOT,	SI_ENCHANT_SUBRESULT);
	m_pSocketContainer		->Init(eITEMENCHANT_SOCKET_MAX_SLOT,		SI_ENCHANT_SOCKET);

	return TRUE;
}


//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::ReleaseContainer()
{
	FlushSlotListener();
	if(m_pTargetContainer)
	{
		m_pMainContainer		->ClearAll();
		m_pTargetContainer	->ClearAll();
		m_pSocketContainer	->ClearAll();
		m_pSubResultContainer->ClearAll();

		m_pMainContainer		->Release();
		m_pTargetContainer	->Release();
		m_pSubResultContainer->Release();
		m_pSocketContainer	->Release();

		ContainerHandle	main		(m_pMainContainer	);
		ContainerHandle	target	(m_pTargetContainer	);
		ContainerHandle	subResult(m_pSubResultContainer);
		ContainerHandle	socket	(m_pSocketContainer	);

		m_pMainContainer			= NULL;
		m_pTargetContainer		= NULL;
		m_pSubResultContainer	= NULL;
		m_pSocketContainer		= NULL;
	}

}


//------------------------------------------------------------------------------
VOID	ItemCompoundDialogInstance::ShowWindow(BOOL val)
{
	m_bStarted	= val;
	if (val)
	{

		//ReTry(m_CurrentCompositeType);

		theKeyQueueManager.PushBack	(ESC_PROC_DIALOG_ENCHANT
											,GlobalUtil::DoShowWindow
											,(WPARAM)(ContainerDialogBase*)this);

		theGameUIManager.UIRefresh	(m_GameUIID);

	}
	else
	{
		FlushSlotListener();
		if(m_pTargetContainer)
		{
			m_pTargetContainer->ClearAll();
			m_pSocketContainer->ClearAll();
			m_pSubResultContainer->ClearAll();
		}

		theKeyQueueManager.DeleteMsg(ESC_PROC_DIALOG_ENCHANT);
	}

	theGameUIManager.UISetData	(m_GameUIID
										,gameui::eSetVisible
										,val);

}


void ItemCompoundDialogInstance::CloseWindow()
{
	ShowWindow(FALSE);
}

//------------------------------------------------------------------------------
void ItemCompoundDialogInstance::InitEnchantInfo()
{
	m_bValidEnchant			= FALSE;
	m_FromTargetPos			= 0;
	m_FromTargetSlotIdx		= INVALID_SLOTINDEX; 
	m_CurrentCompositeType	= ITEMCOMPOSITE_NONE;
}



//------------------------------------------------------------------------------
void ItemCompoundDialogInstance::ActivateMaterials(BOOL bActivate)
{

		DWORD dwState;

	SLOTPOS i = (SLOTPOS)m_nMaterialPosBase;
	for (; i < m_pMainContainer->GetSlotMaxSize(); i++)
	{
		if (m_pMainContainer->IsEmpty(i))
			continue;

		BaseSlot& slot = m_pMainContainer->GetSlot(i);

		dwState	= (bActivate)	? SLOT_UISTATE_ACTIVATED 
										: SLOT_UISTATE_DEACTIVATED;

		m_pMainContainer->GetSlotListener()->OnSlotStateChange(slot,dwState);
	}

}


BOOL ItemCompoundDialogInstance::ExistSocketItem(BOOL bCheckNew)
{
	for(SLOTPOS pos=0; pos<m_pSocketContainer->GetSlotMaxSize(); pos++)
	{
		if(!m_pSocketContainer->IsEmpty(pos))
		{
			if(!bCheckNew)
				return TRUE;

			DummyItemSlot& rDummy = (DummyItemSlot&)m_pSocketContainer->GetSlot(pos);
			if(rDummy.GetFromContainerIdx() != INVALID_SLOTINDEX) ///说明有新增加镶嵌物品
				return TRUE;
		}
	}
	return FALSE;
}

BOOL ItemCompoundDialogInstance::ExistEmptySocket	()
{
	__CHECK_PTR(m_pTargetContainer);
	DummyItemSlot& rTargetSlot = (DummyItemSlot&)m_pTargetContainer->GetSlot(0);

	eSOCKET EmptySocket = SOCKET_1;
	if( !rTargetSlot.ExistEmptySocket(EmptySocket) )
	{
		return FALSE;
	}
	return TRUE;
}

BOOL ItemCompoundDialogInstance::FillSocketState	(DWORD& dwSocketState)
{
	__CHECK_PTR(m_pTargetContainer);

	dwSocketState = 0;
	DummyItemSlot& rDummySlot = (DummyItemSlot&)m_pTargetContainer->GetSlot(0);
	for(SLOTPOS pos=0; pos<SOCKET_MAX; pos++)
	{
		if(m_pSocketContainer->IsEmpty(pos))
		{
			if(rDummySlot.GetSocketAttrDesc((eSOCKET)(pos + SOCKET_1)) )
				dwSocketState |= _BIT(pos);
		}
	}
	return dwSocketState != 0;
}

BOOL ItemCompoundDialogInstance::FillSocketItems(SLOTPOS arSocketItemPos[])
{
	BOOL bFilled = FALSE;
	for(SLOTPOS pos=0; pos<SOCKET_MAX; pos++)
	{
		arSocketItemPos[pos] = INVALID_POSTYPE;
		if(!m_pSocketContainer->IsEmpty(pos))
		{
			DummyItemSlot& rDummy = (DummyItemSlot&)m_pSocketContainer->GetSlot(pos);
			if(rDummy.GetFromContainerIdx() != INVALID_SLOTINDEX)
			{
				arSocketItemPos[pos] = rDummy.GetFromPosition();
				bFilled = TRUE;
			}
		}
	}
	return bFilled;
}

BOOL ItemCompoundDialogInstance::ExistTargetItem()
{
	__CHECK_PTR(m_pTargetContainer);
	for(SLOTPOS pos=0; pos<m_pTargetContainer->GetSlotMaxSize(); pos++)
	{
		if(!m_pTargetContainer->IsEmpty(pos))
			return TRUE;
	}
	return FALSE;
}


BOOL ItemCompoundDialogInstance::ExistSubResultItem()
{
	for(SLOTPOS pos=0; pos<m_pSubResultContainer->GetSlotMaxSize(); pos++)
	{
		if(!m_pSubResultContainer->IsEmpty(pos))
			return TRUE;
	}
	return FALSE;
}

BOOL ItemCompoundDialogInstance::CheckValidMaterails(eCOMPOSITE_CHECK check)
{
	switch (m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_ITEM_MAKE:
		{
			return ExistMakeMaterialInfo(check);
		}break;

	case ITEMCOMPOSITE_LOW_ENCHANT:
	case ITEMCOMPOSITE_MEDIUM_ENCHANT:
	case ITEMCOMPOSITE_HIGH_ENCHANT:
		return ExistEnchantMaterials(check);
		break;
	}

	return ExistCompositeMaterials(m_CurrentCompositeType, SI_INVENTORY,check );
}




VOID ItemCompoundDialogInstance::RefreshEnchantInfo()
{
	__CHECK2_PTR(m_pTargetContainer,;);

	switch (m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_LOW_ENCHANT:
	case ITEMCOMPOSITE_MEDIUM_ENCHANT:
	case ITEMCOMPOSITE_HIGH_ENCHANT:
		break;
	default:
		assert(0);
		break;
	}

	//ClearAll();

	if(m_pTargetContainer->IsEmpty(0))
		return;

	ItemSlot & slotDat = (ItemSlot &)m_pTargetContainer->GetSlot(0);
	
	m_EnchantLevel = slotDat.GetEnchantLV();

	AutoRegisterEnchantMaterials();
}





INT* ItemCompoundDialogInstance::RefreshMakeMaterialContainer(sITEMMAKELEVEL* pMakeLevel,INT nMaterialCol,INT nMaterialRow)
{
	__CHECK2_PTR(pMakeLevel,NULL);
	__CHECK2(nMaterialCol < MATERIALTYPE_MAX, NULL);

	m_pItemMakeLevel			= pMakeLevel;
	m_nMaterailContainerCol	= nMaterialCol;
	m_nMaterailContainerRow	= nMaterialRow;

	if(theItemMakeParser.CalcMaterialSlotInfo	(pMakeLevel
															,m_arMakeMaterialStates
															,nMaterialCol
															,nMaterialRow))
	{
		return m_arMakeMaterialStates;
	}
	return NULL;

	//INT		m_arMakeMaterialStates[nMaterialCol];
	//BYTE*		pMaterials	=	m_pItemMakeLevel->m_arMaterialNum;
	//INT		nIndexTail;
	//INT		nMaterialBase = MATERIALTYPE_NONE + 1;
	//
	///// 1.将m_arMaterialNum前段填满 m_arMakeMaterialStates
	///// 2.遍历检测 m_arMaterialNum 后段未处理信息
	/////	2.1未处理信息依次追加到 arIndex 最后非空内容处理，如果
	/////		2.1.a m_arMakeMaterialStates最后位置就是有内容时，则把arIndex最后空位置开始到现有位置的内容都前移1格

	//for(INT n=0; n<nMaterialCol; n++)
	//{	
	//	INT nIndex = n + nMaterialBase;
	//	if(nIndex >= MATERIALTYPE_MAX)
	//		m_arMakeMaterialStates[n] = -1;
	//	else if(pMaterials[nIndex])
	//		m_arMakeMaterialStates[n] = nIndex;
	//	else
	//		m_arMakeMaterialStates[n] = -1;
	//}

	//nIndexTail = nMaterialCol-1;
	//for(INT nLast=nIndexTail + nMaterialBase; nLast < MATERIALTYPE_MAX; nLast++)
	//{
	//	if(pMaterials[nLast] == 0)
	//		continue;
	//	///检测空位置
	//	INT nIndex;
	//	for(nIndex=nIndexTail; nIndex >= 0; nIndex--)
	//	{
	//		if(m_arMakeMaterialStates[nIndex] == -1)
	//			break;
	//	}
	//	///最后位置是空的
	//	if(nIndex == nIndexTail)
	//	{
	//		m_arMakeMaterialStates[nIndex] = nLast;//pMaterials[nLast];
	//	}
	//	///否则，(后面内容向前移动1格
	//	else if(nIndex >= 0)
	//	{
	//		for(INT nMove=nIndex+1; nMove < nMaterialCol; nMove++)
	//		{
	//			m_arMakeMaterialStates[nMove-1] = m_arMakeMaterialStates[nMove];
	//		}
	//		m_arMakeMaterialStates[nIndexTail] = nLast;//pMaterials[nLast];
	//	}
	//	else
	//	{
	//		assert(!"not empty pos\n");
	//		return NULL;
	//	}
	//}

	//return m_arMakeMaterialStates;
}


BOOL ItemCompoundDialogInstance::CheckMakeMaterialAt(eMATERIAL_TYPE type, SLOTPOS atPos)
{
	__CHECK_PTR(m_pItemMakeLevel);
	__CHECK(m_nMaterailContainerCol > 0 && m_nMaterailContainerCol < MATERIALTYPE_MAX);

	INT nMaterialType;

	nMaterialType = atPos/ m_nMaterailContainerRow;

	return m_arMakeMaterialStates[nMaterialType] == type;
}



BOOL ItemCompoundDialogInstance::OpenByNpc(DWORD dwNpcType)
{
	InitEnchantInfo();

	switch(dwNpcType)
	{
	case NPC_FUNC_ITEMINLAY:	
		{
			m_GameUIID				= gameui::eUIItemInlay;
			m_EnchantType				= eITEMENCHANT_INLAY;
			m_CurrentCompositeType	= ITEMCOMPOSITE_SOCKET_FILL;
		}break;

	case NPC_FUNC_ITEMMAKE:
		{
			m_GameUIID				= gameui::eUIItemMake;
			m_EnchantType				= eITEMENCHANT_MAKE;
			m_CurrentCompositeType	= ITEMCOMPOSITE_ITEM_MAKE;
		}break;

	case NPC_FUNC_ITEMENCHANT:
		{
			m_GameUIID				= gameui::eUIItemEnchant;
			m_EnchantType				= eITEMENCHANT_ENCHANT;
			m_CurrentCompositeType	= ITEMCOMPOSITE_LOW_ENCHANT;
		}break;

	case NPC_FUNC_ITEMCOMPOUND:
	default:
		{
			m_GameUIID				= gameui::eUIItemCompound;
			m_EnchantType				= eITEMENCHANT_COMPOUND;
			m_CurrentCompositeType	= ITEMCOMPOSITE_ITEM_COMPOUND;
		}break;
	}
	return TRUE;
}

//------------------------------------------------------------------------------
BOOL ItemCompoundDialogInstance::Enchant()
{

	switch (m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_ITEM_MAKE:
		{
			__CHECK_PTR(m_pItemMakeLevel);

			if ( !BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_ENCHANT) )
			{
				BIT_ADD( m_dwPacketStatus, PACKET_STATUS_ENCHANT);

				MSG_CG_ITEM_MAKE_SYN		SendPacket;
				SendPacket.m_DestItemCode	= (SLOTCODE)m_pItemMakeLevel->m_ItemCode;
				SendPacket.m_byMaterialRow	= (BYTE)m_nMaterailContainerRow;
				strncpy(SendPacket.m_szItemName,m_szItemName,MAX_ITEMNAME2_LENGTH);
				//SendPacket.						= m_pItemMakeLevel->m_ItemCode;
				if(!FillMakeMaterialInfo(SendPacket.m_dwMaterialNums,SendPacket.m_arMaterialPos))
					return FALSE;

				theNetworkLayer.SendPacket(CK_GAMESERVER
                                  ,&SendPacket
											 ,SendPacket.GetSize()
											 ,&theHero);
				return TRUE;
			}
			return FALSE;
		}break;

	case ITEMCOMPOSITE_LOW_ENCHANT:
	case ITEMCOMPOSITE_MEDIUM_ENCHANT:
	case ITEMCOMPOSITE_HIGH_ENCHANT:
		{
			if ( !BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_ENCHANT) )
			{
				BIT_ADD( m_dwPacketStatus, PACKET_STATUS_ENCHANT);

				MSG_CG_ITEM_ENCHANT_SYN	SendPacket;
				SendPacket.m_SuccessRateIndex = (BYTE)m_SucceedType;
				SendPacket.m_TargetPos			= m_FromTargetPos;
				m_PendingSucceedType				= m_SucceedType;
				m_PendingTargetPos				= m_FromTargetPos;

				theNetworkLayer.SendPacket( CK_GAMESERVER, &SendPacket, sizeof(SendPacket),&theHero);
				return TRUE;
			}
			return FALSE;
		}
		break;
	//case ITEMCOMPOSITE_HIGH_ENCHANT:
	//	{
	//		if ( !BIT_CHECK( m_dwPacketStatus, PACKET_STATUS_ENCHANT) )
	//		{
	//			BIT_ADD( m_dwPacketStatus, PACKET_STATUS_ENCHANT);

	//			MSG_CG_ITEM_ENCHANTUP_SYN	SendPacket;
	//			SendPacket.m_SuccessRateIndex = m_SucceedType;
	//			SendPacket.m_TargetPos			= m_FromTargetPos;
	//			m_PendingSucceedType				= m_SucceedType;
	//			m_PendingTargetPos				= m_FromTargetPos;

	//			theNetworkLayer.SendPacket( CK_GAMESERVER, &SendPacket, sizeof(SendPacket),&theHero);
	//		}
	//		return TRUE;
	//	}
	//	break;


	case ITEMCOMPOSITE_RANK_DOWN:
	case ITEMCOMPOSITE_ADD_SOCKET:		
			return FALSE;

		///拆除宝石
	case ITEMCOMPOSITE_EXTRACT_SOCKET:
		{
			MSG_CG_ITEM_SOCKET_EXTRACT_SYN SendPacket;
			SendPacket.m_TargetPos	= m_FromTargetPos;
			SendPacket.m_type			= m_CurrentCompositeType;
			if(!FillSocketState(SendPacket.dwSocketState))
				return FALSE;

			theNetworkLayer.SendPacket	(CK_GAMESERVER
                                 ,&SendPacket
											,sizeof(SendPacket)
											,&theHero);
			return TRUE;
		}
		break;

		///镶嵌宝石
	case ITEMCOMPOSITE_SOCKET_FILL:
		{
			///由UI调用
			//__CHECK( ExistTargetItem() );
			//__CHECK( ExistSocketItem(TRUE) );
			//BIT_ADD(m_PacketStatus, FILL_SOCKET);

			MSG_CG_ITEM_SOCKET_FILL_SYN ItemSocketFillSyn;
			ItemSocketFillSyn.m_TargetPos		= m_FromTargetPos;
			ItemSocketFillSyn.m_type			= m_CurrentCompositeType;

			if(!FillSocketItems(ItemSocketFillSyn.m_arSocketItemPos))
				return FALSE;

			theNetworkLayer.SendPacket	(CK_GAMESERVER
                                 ,&ItemSocketFillSyn
											,sizeof(ItemSocketFillSyn)
											,&theHero);	
			return TRUE;
		}
		break;

		///制造戒指与项链
	case ITEMCOMPOSITE_LOW_RING:
	case ITEMCOMPOSITE_HIGH_RING:
	case ITEMCOMPOSITE_LOW_NECKLACE:
	case ITEMCOMPOSITE_HIGH_NECKLACE:
	default:
		{	
			MSG_CG_ITEM_ACCESSORY_CREATE_SYN SendPacket;

			SendPacket.m_type				= m_CurrentCompositeType;
			SendPacket.m_SocketItemPos = m_FromTargetPos;

			theNetworkLayer.SendPacket	(CK_GAMESERVER
                                 ,&SendPacket
											,sizeof(SendPacket)
											,&theHero);

			return TRUE;
		}
		break;

	case ITEMCOMPOSITE_NONE:
		{
			assert(0);
		}break;
	}
	return FALSE;
}


//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::ReTry(MATERIALTYPE eCompositeType)
{
	BOOL bUpdateSockets		= FALSE;
	//BOOL bCompositeMaterial = FALSE;
	m_CurrentCompositeType = eCompositeType;

	m_pSocketContainer->ClearAll();

	FlushSlotListener();

	m_nMaterialPosBase = ITEMCOMPOUND_MATERAIL_BASE;//1;



	///////////////////////////////////////////////
	//强化操作类型
	switch (m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_ITEM_MAKE:
		{
			bUpdateSockets			= FALSE;
		}break;
	case ITEMCOMPOSITE_LOW_ENCHANT:
	case ITEMCOMPOSITE_MEDIUM_ENCHANT:
	case ITEMCOMPOSITE_HIGH_ENCHANT:
		{
			RefreshEnchantInfo();
			bUpdateSockets			= TRUE;
			//bCompositeMaterial	= FALSE;
		}
		break;
		case ITEMCOMPOSITE_EXTRACT_SOCKET:
		case ITEMCOMPOSITE_SOCKET_FILL:
		case ITEMCOMPOSITE_ADD_SOCKET:
			{
				AutoRegisterCompositeMaterials( m_CurrentCompositeType );
				m_pSubResultContainer->ClearAll();
				bUpdateSockets			= TRUE;
				//bCompositeMaterial	= TRUE;
			}break;
	default:
		{
			AutoRegisterCompositeMaterials( m_CurrentCompositeType );
			bUpdateSockets			= FALSE;
			//bCompositeMaterial	= TRUE;
		}
		break;
	}


	if(bUpdateSockets && m_FromTargetSlotIdx != INVALID_SLOTINDEX)
	{
		if(!m_pTargetContainer->IsEmpty(0))
		{
			AutoRegisterCompositeSockets(m_CurrentCompositeType
												 ,m_FromTargetPos
												 ,m_FromTargetSlotIdx);
		}
	}
	
}



//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::Exit()
{
	InitEnchantInfo();
	CloseWindow();
}


//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::SetRequiredMoney(MONEY Money)
{
	theGameUIManager.UISetData(m_GameUIID
		,gameui::eSetMoney
		,(DWORD)&Money);
}


MONEY ItemCompoundDialogInstance::GetRequiredMoney()
{
	__CHECK2_PTR(m_pTargetContainer,0);

	MONEY						moneyCost(0);

	sITEMCOMPOSITE * pInfo = theItemCompositeParser.GetCompositeInfo(m_CurrentCompositeType);
	
	__CHECK2_PTR(pInfo,moneyCost);

	switch (m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_ITEM_MAKE:
		{
			__CHECK2_PTR(m_pItemMakeLevel, 0);
			return m_pItemMakeLevel->m_Money;
			//if(!m_pTargetContainer->IsEmpty(0))
			//{
			//	sITEMMAKE*			pItemMake;
			//	sITEMMAKELEVEL*	pItemLevel;
			//	sITEMINFO_BASE*		pItemInfo;
			//	ItemSlot & rItemSlot = (ItemSlot &)m_pTargetContainer->GetSlot(0);
			//	
			//	pItemInfo	= rItemSlot.GetItemInfo();
			//	assert(pItemInfo);
			//	pItemMake	= theItemMakeParser.GetMakeInfo((eITEM_TYPE)pItemInfo->m_wType);
			//	if(pItemMake)
			//	{
			//		assert(pItemInfo->m_LV > 0 && pItemInfo->m_LV <= MAX_ITEM_LEVEL);
			//		pItemLevel = &pItemMake->m_arMakeLVs[pItemInfo->m_LV-1];
			//		return pItemLevel->m_Money;
			//	}
			//}
		}break;
	case ITEMCOMPOSITE_LOW_ENCHANT:
	case ITEMCOMPOSITE_MEDIUM_ENCHANT:
	case ITEMCOMPOSITE_HIGH_ENCHANT:
		{
			sENCHANT_MATERIALS*	pEnchantMats;
			sLEVEL_MATERIALS*	pEnchantLV;

			if(!m_pTargetContainer->IsEmpty(0))
			{
				ItemSlot & rItemSlot = (ItemSlot &)m_pTargetContainer->GetSlot(0);

				pEnchantMats = theEnchantInfoParser.FindEnchantMatInfo(rItemSlot.GetItemInfo());
				if(pEnchantMats)
				{
					pEnchantLV = &pEnchantMats->m_EnchantMaterial[rItemSlot.GetEnchant()+1];
					moneyCost	+= theEnchantInfoParser.CalcMoneyCost(pEnchantLV
																				,m_SucceedType
																				,pEnchantLV->m_Money);

					if(pEnchantLV->m_byNeedComposite)
						moneyCost += pInfo->m_Money;
				}
			}
			else
			{
				moneyCost = pInfo->m_Money;
			}
		}
		break;
	default:
		{
			moneyCost = pInfo->m_Money;
		};
	}

	return moneyCost;
}

void ItemCompoundDialogInstance::SetSucceedType(eSUCCEEDRATE rateType)
{
	eSUCCEEDRATE minRate;
	minRate = GetMinSucceedRate();

	if(rateType < minRate)
		rateType = minRate;
	else if(rateType > eSUCCEEDRATE_100)
		rateType = eSUCCEEDRATE_100;

	m_SucceedType = rateType;
}

eSUCCEEDRATE ItemCompoundDialogInstance::GetMinSucceedRate()
{
	__CHECK2_PTR(m_pTargetContainer,eSUCCEEDRATE_100);

	sITEMCOMPOSITE * pInfo = theItemCompositeParser.GetCompositeInfo(m_CurrentCompositeType);
	
	__CHECK2_PTR(pInfo,eSUCCEEDRATE_100);

	switch (m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_ITEM_MAKE:
		{
		}break;
	case ITEMCOMPOSITE_LOW_ENCHANT:
	case ITEMCOMPOSITE_MEDIUM_ENCHANT:
	case ITEMCOMPOSITE_HIGH_ENCHANT:
		{
			sENCHANT_MATERIALS*	pEnchantMats;
			sLEVEL_MATERIALS*	pEnchantLV;

			if(!m_pTargetContainer->IsEmpty(0))
			{
				ItemSlot & rItemSlot = (ItemSlot &)m_pTargetContainer->GetSlot(0);

				pEnchantMats = theEnchantInfoParser.FindEnchantMatInfo(rItemSlot.GetItemInfo());
				if(pEnchantMats)
				{
					pEnchantLV = &pEnchantMats->m_EnchantMaterial[rItemSlot.GetEnchant()+1];
					return (eSUCCEEDRATE)pEnchantLV->m_byMinRate;
				}
			}
		}
		break;
	}

	return eSUCCEEDRATE_100;
}

//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::SetTargetInfo(SLOTINDEX fromSlotIdx, SLOTPOS fromPos)
{
	m_FromTargetSlotIdx	= fromSlotIdx;
	m_FromTargetPos		= fromPos;
}

VOID ItemCompoundDialogInstance::ClearTarget()
{
	__CHECK2_PTR(m_pTargetContainer,;);

	m_FromTargetSlotIdx	= INVALID_SLOTINDEX;
	m_FromTargetPos		= INVALID_POSTYPE;

	m_pTargetContainer->ClearAll();
	
	switch(m_CurrentCompositeType)
	{
	case ITEMCOMPOSITE_EXTRACT_SOCKET:
	case ITEMCOMPOSITE_SOCKET_FILL:
	case ITEMCOMPOSITE_ADD_SOCKET:
		{
			m_pSocketContainer->ClearAll();
		}
		break;
	}

	theGameUIManager.UITriggerFunc(m_GameUIID
											,gameui::eOnClearTarget);
}





//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::NetworkProc( MSG_BASE * /*pMsg*/ )
{

}



//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::Update()
{
	if(m_pMainContainer->GetSlotListener())
		m_pMainContainer->GetSlotListener()->Update();
}


//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::FlushSlotListener()
{

	m_pMainContainer->ClearAll();

}


//------------------------------------------------------------------------------
BOOL ItemCompoundDialogInstance::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )
{
	__CHECK(m_pMainContainer->InsertSlot(  posIndex,  slotDat ));

	return TRUE;
}


//------------------------------------------------------------------------------
VOID ItemCompoundDialogInstance::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	m_pMainContainer->DeleteSlot(  posIndex,  pSlotOut );
}




/*////////////////////////////////////////////////////////////////////////
文 件 名：VHeroInputLayer.cpp
创建日期：2007年1月10日
最后更新：2007年1月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VHeroInputLayer.h"
#include "VHero.h"
#include "VHeroActionInput.h"
#include "VHeroAutoInput.h"
#include "InputLayer.h"
#include "V3DGameWorld.h"
#include "GameUIManager.h"
#include "Map.h"
#include "GameMainUtil.h"
#include "ApplicationBase.h"
#include "VUCtrlManager.h"
#include "VUCtrlIconDragManager.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace vobject;
using namespace gamemain;
GLOBALINST_SINGLETON_IMPL(VHeroInputLayer, ()  , gamemain::eInstPrioGameFunc);

namespace vobject
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VHeroInputLayer::VHeroInputLayer()
{
	m_MouseRay.m_Ray.m_Origin(0.f);
	m_MouseRay.m_Ray.m_Direction(0.f);
	m_MouseRay.m_vCross(0.f);
	m_MouseRay.m_vNormal(0.f);
	m_MouseRay.m_bResult	= FALSE;
	m_RayTimer.SetTimer(300);//1000ms

}
VHeroInputLayer::~VHeroInputLayer()
{
}
//虚函数
BOOL VHeroInputLayer::Init()							
{
	//__BOOL_SUPER(Init());
	return TRUE;
}

void VHeroInputLayer::Release()	
{
	//_SUPER::Release();
}

BOOL VHeroInputLayer::RequestWorkingData()	
{
	//__BOOL_SUPER(RequestWorkingData());
	return TRUE;
}

void VHeroInputLayer::ReleaseWorkingData()	
{
	//_SUPER::ReleaseWorkingData();
}

BOOL VHeroInputLayer::FrameMove(DWORD dwTick)
{
	if(m_RayTimer.IsExpired())
		ProcessMouseRay();

	ProcessInput( dwTick );
	return TRUE;
}

BOOL VHeroInputLayer::Render(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(Render(dwTick));
	return TRUE;
}


void VHeroInputLayer::OnWinMsg	(HWND   /*hWnd*/
                                 ,UINT   uMsg
											,WPARAM /*wParam*/
											,LPARAM /*lParam*/)
{
	switch( uMsg )
	{
	case WM_KEYDOWN:
		{
			//if( !theGameUIManager.UIIsEditInputFocus(gameui::eUIChatInfoBox))
			//{
			//}
		}
		break;

	case WM_KEYUP:
		{
			//if( !theGameUIManager.UIIsEditInputFocus(gameui::eUIChatInfoBox))
			//{
			//}
		}
		break;


	case WM_KILLFOCUS:
		{
			//ReleaseCapture();
			//ClipCursor( NULL );
		}
		break;

	case WM_SETFOCUS:
		{
			//ReleaseCapture();
			//ClipCursor( NULL );
		}
		break;
	default:
		break;
	}

}


//BOOL	VHeroInputLayer::CanHeroCastRayTo(VObject* pVObject)
//{
//	return TRUE;
//}

BOOL VHeroInputLayer::ProcessMouseRay( )
{
	//return TRUE;
	m_MouseRay.m_bResult	= FALSE;

	/////////////////////////////////////
	__CHECK(theGameWorld.GetHeroPlotMap() );

	m_MouseRay.m_bResult = theMap.PickRayAtScreen(m_MouseRay.m_Ray
																,theVUCtrlManager.m_MousePos)		;

	__CHECK(m_MouseRay.m_bResult );


	/////////////////////////////////////
	return theGameWorld.GetWorldCastRay(m_MouseRay.m_Ray,m_MouseRay.m_vCross);
}

BOOL VHeroInputLayer::PickFromTerrain(Vector3D& vCross)
{
	__CHECK(ProcessMouseRay() );
	__CHECK(m_MouseRay.m_bResult );
	vCross	= m_MouseRay.m_vCross;
	return TRUE;





}


BOOL VHeroInputLayer::ProcessInput( DWORD /*dwTick*/ )
{
	//if( theGameWorld.IsChanging() )
	//	return FALSE;

	//if( !g_V3DConfig.IsRenderWorld() )
	//	return FALSE;

	//V3DCamera* pCamera = theMap.GetCamera();
	//if(!pCamera)
	//	return FALSE;

	return true;
}




void VHeroInputLayer::ProcessKeyInput(DWORD /*dwTick*/)
{

}

void VHeroInputLayer::ProcessMouseInput(DWORD /*dwTick*/)
{
	

}





void VHeroInputLayer::_UpdateInput(DWORD /*dwTick*/)
{

}

BOOL	VHeroInputLayer::_ProcessMouseInput(DWORD /*dwType*/, DWORD /*dwData*/)
{

	return TRUE;
}

BOOL VHeroInputLayer::_ProcessMouseEvent( HWND /*hWnd*/, UINT /*uMsg*/ , BOOL /*MouseLButtonPress*/ )
{
	return FALSE;
}


};//namespace vobject


/*////////////////////////////////////////////////////////////////////////
文 件 名：VendorDialogInstance.cpp
创建日期：2009年5月25日
最后更新：2009年5月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VendorDialogInstance.h"
#include "BaseContainer.h"
#include "SlotHandle.h"
#include "Hero.h"
#include "ConstTextRes.h"
#include "GameConst.h"
#include "PacketInclude.h"
#include "DummyItemSlot.h"
#include "ItemManager.h"
#include "MouseHandler.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(VendorDialogInstance, ()  , gamemain::eInstPrioGameFunc);


//------------------------------------------------------------------------------
VendorDialogInstance::VendorDialogInstance()
{
	//m_pTradeContainer	= this;
}


//------------------------------------------------------------------------------
VendorDialogInstance::~VendorDialogInstance()
{
}


//void VendorDialogInstance::_FreeSlot(BaseSlot* pSlot)
//{
//	//SAFE_DELETE(pSlot);
//	theSlotSystem.DestroyBaseSlot(pSlot);
//}
//
//BaseSlot* VendorDialogInstance::_AllocSlot()
//{
//	//return new DummyItemSlot;
//	return theSlotSystem.CreateBaseSlot(SLOTT_TRADE);
//}

//------------------------------------------------------------------------------
BOOL	VendorDialogInstance::Init( )
{
	__CHECK(InitContainer() );

	return TRUE;
}

//------------------------------------------------------------------------------
VOID VendorDialogInstance::Release()
{
	ReleaseContainer();
}


//------------------------------------------------------------------------------
BOOL VendorDialogInstance::InitContainer()
{
	ContainerHandle	trade		(SLOTT_ITEMHANDLE);
	__CHECK_PTR(&trade);
	m_pTradeContainer		= trade.Detach();

	m_pTradeContainer		->Init(MAX_PERSONAL_VENDOR_SLOT_NUM,	SI_VENDOR_SELL);

	return TRUE;
}


//------------------------------------------------------------------------------
VOID VendorDialogInstance::ReleaseContainer()
{
	FlushSlotListener();
	if(m_pTradeContainer)
	{
		m_pTradeContainer	->ClearAll();

		m_pTradeContainer	->Release();

		ContainerHandle	trade		(m_pTradeContainer	);
		m_pTradeContainer		= NULL;
	}

}

//------------------------------------------------------------------------------
VOID VendorDialogInstance::FlushSlotListener()
{
	if(!m_pTradeContainer)
		return;
	m_pTradeContainer->ClearAll();
}

//------------------------------------------------------------------------------
VOID VendorDialogInstance::Update()
{
	if(!m_pTradeContainer)
		return;

	if (theHero.GetBehaveState() == PLAYER_BEHAVE_IDLE)
	{
		CloseWindow();
		return;
	}

	if(m_pTradeContainer->GetSlotListener())
		m_pTradeContainer->GetSlotListener()->Update();

    //updateVendingMsgs();
    //updateVendingBtns();
    //updateVendingCurrentItemInfos();

	if(IsVendorView())
	{
		if (!m_pTradeContainer->GetSlotNum())
		{
			VendingEnd();
		}
	}

}


//------------------------------------------------------------------------------
VOID	VendorDialogInstance::ShowWindow(BOOL val)
{
	m_bStarted	= val;
	if (val)
	{
		///////////////////////////////////////////////
		if(!m_bVendorView)
		{
			if (	theHero.GetBehaveState() != PLAYER_BEHAVE_IDLE
				&&	theHero.GetBehaveState() != PLAYER_BEHAVE_VENDOR_ESTABLISHER)
			{
				OUTPUTTIP(TEXTRES_PLAYER_BEHAVE_NOT_IDLE);
				return;
			}

			theHero.SetBehaveState(PLAYER_BEHAVE_VENDOR_ESTABLISHER);
		}

		///////////////////////////////////////////////
		theGameUIManager.UISetVisible	(gameui::eUIInventory,TRUE);

		theKeyQueueManager.PushBack(ESC_PROC_DIALOG_VENDOR
											,GlobalUtil::DoShowWindow
											,(WPARAM)this);

		theGameUIManager.UIRefresh	(gameui::eUIPrivateShop);
	}
	else
	{
		//////////////////////////////////////////
		if(!m_bVendorWorking || m_bVendorView)
		{
			FlushSlotListener();
			if(m_pTradeContainer)
			{
				m_pTradeContainer->ClearAll();
			}
	      theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
		}

		theKeyQueueManager.DeleteMsg(ESC_PROC_DIALOG_VENDOR);

		//////////////////////////////////////
		if(m_bVendorView)
		{
			theMouseHandler.RemoveMouseState(eMOUSE_BUY);
			VendingEnd();
		}
	}

	theGameUIManager.UISetVisible	(gameui::eUIPrivateShop,val);

}


void VendorDialogInstance::CloseWindow()
{
	///////////////////////////////////////
	if(m_bStarted)
	{
		theGameUIManager.UISetVisible(gameui::eUIInventory,FALSE);
	}
	ShowWindow(FALSE);

	if(m_bVendorView || !m_bVendorWorking)
	{
		ResetTrade();
	}
}


VOID	VendorDialogInstance::ProcessError	(DWORD dwError	, BOOL /*bReset*/)
{
	OffPending();
	//if(bReset)
	//	theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
	theHero.ProcessPlayerError(dwError, CG_VENDOR);

	if(theMouseHandler.IsExistItemAtHand())
		theMouseHandler.CancelHandlingItem();
}

VOID	VendorDialogInstance::ProcessErrorTip	(DWORD dwErrorTip)
{
	//theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
	OffPending();
	OUTPUTTIP(dwErrorTip);

	if(theMouseHandler.IsExistItemAtHand())
		theMouseHandler.CancelHandlingItem();
}

//------------------------------------------------------------------------------
BOOL VendorDialogInstance::InsertSlot(SLOTPOS		posIndex
                                     ,BaseSlot &	slotDat)
{
	BaseContainer*	pContainer = m_pTradeContainer;

	__CHECK(pContainer->InsertSlot(  posIndex,  slotDat ));

	 //if(pContainer->GetSlotListener())
	 //{
		// BaseSlot& slot = pContainer->GetSlot(posIndex);
		// pContainer->GetSlotListener()->OnSlotAdd(slot);
	 //}

	return TRUE;
}

//------------------------------------------------------------------------------
BOOL VendorDialogInstance::InsertSlot(SLOTPOS		posIndex
                                     ,BaseSlot &	slotDat
												 ,MONEY			money
												 ,SLOTPOS		fromPos
												 )
{

	__CHECK(m_pTradeContainer->InsertSlot(  posIndex,  slotDat ));

	DummyItemSlot& itemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(posIndex);
	itemSlot.SetExtraData	(money);

	if(fromPos != INVALID_POSTYPE)
		itemSlot.SetFromPosition(fromPos);

	//////////////////////////////////////////
	if(m_pTradeContainer->GetSlotListener())
		m_pTradeContainer->GetSlotListener()->OnSlotAdd(itemSlot);

	return TRUE;
}

//------------------------------------------------------------------------------
VOID VendorDialogInstance::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut)
{
	BaseContainer*	pContainer = m_pTradeContainer;

	 //if(pContainer->GetSlotListener())
	 //{
		// BaseSlot& slot = pContainer->GetSlot(posIndex);
		// pContainer->GetSlotListener()->OnSlotRemove(slot);
	 //}
	pContainer->DeleteSlot(  posIndex,  pSlotOut );
}

VOID VendorDialogInstance::NetworkProc( MSG_BASE * /*pMsg*/ )
{
}




//------------------------------------------------------------------------------
void VendorDialogInstance::SetCurrentItemSlotInfo(DURATYPE curAmounts, MONEY curMoney, POSTYPE curPos)
{
    m_curAmouts   = curAmounts;
    m_curMoney    = curMoney;
    m_curPos      = curPos;
}



VOID	VendorDialogInstance::OnVendorEnd( BOOL bFailed)
{
	OffPending();
	if(bFailed)
	{
		OUTPUTTIP(TEXTRES_VENDOR_END_NAK);
	}
	else
	{
		OnVendorViewEnd(theHero.GetObjectKey());

		m_bVendorWorking	= FALSE;
		OUTPUTTIPF(TEXTRES_VENDOR_END_BRD, (LPCTSTR)m_VendorMessage);
		CloseWindow();
	}
}

VOID	VendorDialogInstance::OnVendorRename()
{
	OffPending();
	m_VendorMessage = m_PendingTitle;

	OnVendorViewStart(theHero.GetObjectKey(), NULL);

	//theGameUIManager.UIRefresh(gameui::eUIPrivateShop);

}

VOID	VendorDialogInstance::OnVendorStart( BOOL bFailed)
{
	OffPending();
	if(bFailed)
	{
		m_bVendorWorking	= FALSE;
		OUTPUTTIP(TEXTRES_VENDOR_START_NAK);
		CloseWindow();
	}
	else
	{
		////////////////////////////////
		m_bVendorView		= FALSE;
		m_bVendorWorking	= TRUE;

		OUTPUTTIPF(TEXTRES_VENDOR_START_BRD, (LPCTSTR)m_VendorMessage);

		OnVendorViewStart(theHero.GetObjectKey(), NULL);
	}

	theGameUIManager.UIRefresh	(gameui::eUIPrivateShop);
}

VOID	VendorDialogInstance::OnVendorViewStart(DWORD dwPlayerKey, LPCTSTR szTitle)
{
	OffPending();

	///////////////////////////////////
	if(szTitle == NULL)
		szTitle				= m_VendorMessage;
	else
		m_VendorMessage	= szTitle;


	Player *pPlayer = (Player *)theObjectManager.GetObject(dwPlayerKey);
	if (pPlayer)
	{
		ASSERT (pPlayer->IsKindOfObject(PLAYER_OBJECT));

		pPlayer->SetBehaveState	(PLAYER_BEHAVE_VENDOR_ESTABLISHER);
		pPlayer->SetVendorTitle	(szTitle);
		//OUTPUTCHAT();
		theGameUIManager.UIRefresh	(gameui::eUIPrivateShop);
	}
}

VOID	VendorDialogInstance::OnVendorViewEnd	(DWORD dwPlayerKey)
{
	Player *pPlayer = (Player *)theObjectManager.GetObject(dwPlayerKey);
	if (pPlayer)
	{
		ASSERT (pPlayer->IsPlayer());

		pPlayer->SetBehaveState(PLAYER_BEHAVE_IDLE);
		pPlayer->UpdateAppearance();
	}

	if (dwPlayerKey == m_dwSalerKey)
	{
		CloseWindow();
	}
}


VOID	VendorDialogInstance::OnViewVendorStart	(LPCTSTR szTitle,sTOTALINFO_VENDOR& items)
{
	OffPending();
	//ShowInterfaceWithSound(TRUE);

	for (int i=0; i<items.m_Count; ++i)
	{
		sVENDOR_ITEM_SLOTEX&	vendorSlot	= items.m_Slot[i];
		sITEM_SLOTEX&			itemDat		= vendorSlot.m_Stream;

		ItemSlot itSlot(itemDat.m_Stream);

		InsertSlot(vendorSlot.m_Stream.m_ItemPos	//itemDat.m_ItemPos
                ,itSlot
					 ,vendorSlot.m_Money
					 //,itemDat.m_ItemPos	
					 );
	}

	theHero.SetBehaveState(PLAYER_BEHAVE_VENDOR_OBSERVER);

	m_VendorMessage = szTitle;
	m_dwSalerKey	= theHeroActionInput.GetCurrentTarget();
	ShowWindow(TRUE);
}


void VendorDialogInstance::OnViewVendorEnd()
{
	OffPending();
	//ShowInterfaceWithSound(FALSE);
  theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
}

VOID	VendorDialogInstance::OnViewVendorBuy	(MONEY money,sTOTALINFO_INVENTORY& items)
{
	OffPending();

	/////////////////////////
	MONEY		priceMoney = theHero.GetMoney() - money;
	theHero.SetMoney(money);

	m_TotalMoneyBuy		+= priceMoney;

	SLOTPOS			i;
	BaseContainer*	pContainer;
	pContainer = theItemManager.GetContainer(SI_INVENTORY);


	for (i=0; i<items.m_InvenCount; ++i)
	{
		sITEM_SLOTEX& slot = items.m_Slot[i];

		ItemSlot scSlot(slot.m_Stream);
		pContainer->InsertSlot(slot.m_ItemPos, scSlot);
	}

	m_curAmouts = 0;
	m_curMoney	= 0;

	/////////////////////////////////////////
	Player*	pPlayer = (Player*)theObjectManager.GetObject(GetSalerKey());
	if(pPlayer && pPlayer->IsPlayer())
	{
		StringHandle	sText;
		ItemSlot&		itemSlot = (ItemSlot&)pContainer->GetSlot(items.m_Slot[0].m_ItemPos);

		if(itemSlot.GetItemInfo())
		{
			//买入%s[%I64u元,%s]
			sText.Format(_STRING(TEXTRES_VENDOR_BUY_GOODS)
							,(LPCTSTR)itemSlot.GetItemInfo()->m_ItemName
							,priceMoney
							,(LPCTSTR)pPlayer->GetVendorTitle()
							);
			theGameUIManager.UITriggerFunc(gameui::eUIPrivateShop
													,gameui::eAddTipLog
													,(LPARAM)(LPCTSTR)sText);
		}
	}
	theGameUIManager.UIRefresh	(gameui::eUIPrivateShop);
}

VOID	VendorDialogInstance::OnViewVendorOtherBuy	(SLOTPOS pos)
{
	DeleteSlot(pos,NULL);
}

void VendorDialogInstance::OnPlayerBuyGoods(DWORD dwPlayer,SLOTPOS pos, MONEY money)
{
	BaseContainer*	pContainer;

	//////////////////////////////////
	MONEY		priceMoney = money - theHero.GetMoney();

	theHero.SetMoney(money);

	m_TotalMoneySell		+= priceMoney;


	//////////////////////////////////
	pContainer = theItemManager.GetContainer(SI_INVENTORY);

	//////////////////////////////////
	DummyItemSlot& itemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(pos);


	/////////////////////////////////////////
	Player*	pPlayer = (Player*)theObjectManager.GetObject(dwPlayer);
	if(pPlayer && pPlayer->IsPlayer())
	{
		StringHandle	sText;
		//ItemSlot&		itemSlot = pContainer->GetSlot(items.m_Slot[0].m_ItemPos);

		if(itemSlot.GetItemInfo())
		{
			//买走%s[%I65u元,%s]
			sText.Format(_STRING(TEXTRES_VENDOR_PLAYER_BUY)
							,(LPCTSTR)itemSlot.GetItemInfo()->m_ItemName
							,priceMoney
							,(LPCTSTR)pPlayer->GetName()
							);
			theGameUIManager.UITriggerFunc(gameui::eUIPrivateShop
													,gameui::eAddTipLog
													,(LPARAM)(LPCTSTR)sText);
		}
	}


	//////////////////////////////////
	pContainer->DeleteSlot(itemSlot.GetFromPosition(), NULL);

	DeleteSlot(pos, NULL);

	m_curAmouts = 0;
	m_curMoney = 0;

	theGameUIManager.UIRefresh	(gameui::eUIPrivateShop);
}

void VendorDialogInstance::OnVendorModifyReq		(SLOTPOS /*pos*/,BOOL bAck)
{
	if(bAck)
	{
		if (m_pTradeContainer->GetSlotNum())
		{
			//OpenItemModifyPopupAfterVendingStarted(m_PendingModifyPos);
			//m_bStarted = false;
		}
	}
}

VOID	VendorDialogInstance::OnVendorModify		( )
{
	ItemModify(INVALID_SLOTPOS,m_PendingModifyItemSlot.m_Money);
	//////////////////////////////////

	//DummyItemSlot& uiItemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(m_PendingModifyItemSlot.m_VendorPos);
	//uiItemSlot.SetExtraData(m_PendingModifyItemSlot.m_Money);

	//m_curMoney    = m_PendingModifyItemSlot.m_Money;
	//if (uiItemSlot.GetItemInfo()->IsCanEquip())
	//{
	//	m_curAmouts   = 1;
	//}
	//else
	//{
	//	m_curAmouts   = m_PendingModifyItemSlot.m_Dura;
	//}
	//m_curPos      = m_PendingModifyItemSlot.m_VendorPos;

	//m_bStarted = true;
}

VOID	VendorDialogInstance::OnVendorViewModify(SLOTPOS pos,sVENDOR_ITEM_SLOTEX& item )
{
	if(!m_bVendorView)
		return;

	if (!m_pTradeContainer->IsEmpty(pos))
	{
		DeleteSlot(pos, NULL);
	}

	ItemSlot itemSlot(item.m_Stream.m_Stream);
	InsertSlot(pos, itemSlot, item.m_Money);
}


VOID	VendorDialogInstance::OnVendorInsert( )
{
	BaseContainer*	pInven;
	pInven = theItemManager.GetContainer(SI_INVENTORY);

	if(!m_pTradeContainer->IsEmpty(m_PendingModifyItemSlot.m_VendorPos))
		DeleteSlot(m_PendingModifyItemSlot.m_VendorPos, NULL);

	ItemSlot& itemSlot = (ItemSlot&)pInven->GetSlot(m_PendingModifyItemSlot.m_OrgPos);
	InsertSlot(m_PendingModifyItemSlot.m_VendorPos
             ,itemSlot
				 ,m_PendingModifyItemSlot.m_Money
				 ,m_PendingModifyItemSlot.m_OrgPos
				 );


	DummyItemSlot& uiItemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(m_PendingModifyItemSlot.m_VendorPos);
	//uiItemSlot.SetExtraData(m_PendingModifyItemSlot.m_Money);

	m_curMoney    = m_PendingModifyItemSlot.m_Money;
	if (uiItemSlot.GetItemInfo()->IsCanEquip())
	{
		m_curAmouts   = 1;
	}
	else
	{
		m_curAmouts   = m_PendingModifyItemSlot.m_Dura;
	}
	m_curPos      = m_PendingModifyItemSlot.m_VendorPos;

	///////////////////////////////////
	if(theMouseHandler.IsExistItemAtHand())
		theMouseHandler.CancelHandlingItem();
}


VOID	VendorDialogInstance::OnVendorViewInsert(SLOTPOS pos,sVENDOR_ITEM_SLOTEX& item )
{
	if(!m_bVendorView)
		return;
	OnVendorViewModify(pos,item);
}


//------------------------------------------------------------------------------
void VendorDialogInstance::OnVendorViewDelete(POSTYPE pos)
{
	if(!m_bVendorView)
		return;
   OnVendorDelete(pos);
}

//------------------------------------------------------------------------------
void VendorDialogInstance::OnVendorDelete(POSTYPE pos)
{
	OffPending();
	DeleteSlot(pos, NULL);
	m_curAmouts	= 0;
	m_curMoney	= 0;
	m_curPos		= 0;

	if(theMouseHandler.IsExistItemAtHand())
		theMouseHandler.CancelHandlingItem();
}

//------------------------------------------------------------------------------
void VendorDialogInstance::VendorDelete(POSTYPE pos)
{
	if(m_bVendorView || !m_bStarted)
	{
		if(theMouseHandler.IsExistItemAtHand())
			theMouseHandler.CancelHandlingItem();
		return;
	}

	if(!m_bVendorWorking)
	{
		OnVendorDelete(pos);
		return;
	}

	if(IsPending())
		return;
	OnPending();

	MSG_CG_VENDOR_DELETE_SYN sendMsg;
	sendMsg.m_VendorPos     = pos;

	theHero.SendPacket(  &sendMsg, sizeof(sendMsg), TRUE );
}


void	VendorDialogInstance::VendingSwap			(SLOTPOS toPos, SLOTPOS fromPos)
{
	if(m_bVendorView)
		return;

	/////////////////////////////////////
	if(toPos == fromPos)
	{
		OpenModifyDialog(toPos);
		return;
	}


	/////////////////////////////////////
	if(!m_bVendorWorking)
	{
		OnVendorSwap(fromPos, toPos);
		return;
	}

	/////////////////////////////////////
	if (IsPending())
		return;
	OnPending();


	/////////////////////////////////////
	MSG_CG_VENDOR_SWAP_SYN sendMsg;
	sendMsg.m_FromPos	= fromPos;
	sendMsg.m_ToPos	= toPos;

	theHero.SendPacket( &sendMsg, sizeof(sendMsg), TRUE );
}

VOID	VendorDialogInstance::OnVendorSwap		(SLOTPOS fromPos, SLOTPOS  toPos)
{
	OffPending();
	theItemManager.MoveItem	(SI_VENDOR_SELL
									,SI_VENDOR_SELL
									,fromPos
									,toPos);

    if(theMouseHandler.IsExistItemAtHand())
       theMouseHandler.CancelHandlingItem();
}

VOID	VendorDialogInstance::OnVendorSwapView	(SLOTPOS fromPos, SLOTPOS  toPos)
{
	if(!m_bVendorView)
		return;
	OnVendorSwap(fromPos,toPos);
}


//------------------------------------------------------------------------------
void VendorDialogInstance::VendingEnd()
{
	if (IsPending())
		return;
	OnPending();

	if(m_bVendorView)
	{
		MSG_CG_VENDOR_VIEW_END_SYN sendMsg;

		theHero.SendPacket( &sendMsg, sizeof(sendMsg), TRUE );
	}
	else
	{
		MSG_CG_VENDOR_END_SYN sendMsg;

		theHero.SendPacket( &sendMsg, sizeof(sendMsg), TRUE );
	}
}


void VendorDialogInstance::VendingRename(LPCTSTR szTitle)
{
	if(m_bVendorView)
		return;

	if(!m_bVendorWorking)
	{
		m_VendorMessage = szTitle;
		return;
	}

	if (IsPending())
		return;
	OnPending();

	//////////////////////////////////
	m_PendingTitle	=	szTitle;

	//////////////////////////////////
	MSG_CG_VENDOR_RENAME_SYN sendMsg;

	__ZERO(sendMsg.m_pszTitle);
	lstrcpyn(sendMsg.m_pszTitle, szTitle, MAX_VENDOR_TITLE_LENGTH);

	theHero.SendPacket( &sendMsg, sendMsg.GetSize() , TRUE);

}

void VendorDialogInstance::OpenVendor(BOOL bView)
{
	if (theFramework.GetType() != SCENE_TYPE_VILLAGE )
	{
		OUTPUTTIP(TEXTRES_VENDOR_JUST_VILLAGE );
		return;
	}

	///////////////////////////////////////
	if(bView)
	{
		if(m_bVendorWorking)
		{
			OUTPUTTIP(TEXTRES_PLAYER_BEHAVE_NOT_IDLE);
			return;
		}
		///////////////////////////////////////
	}

	///////////////////////////////////////
	m_bVendorView = bView ;


	///////////////////////////////////////
	if(bView)
	{
		MSG_CG_VENDOR_VIEW_START_SYN	msg;
		msg.m_dwPlayerKey	= theHeroActionInput.GetCurrentTarget();
		theHero.SendPacket(&msg,sizeof(msg),TRUE);
	}
	else
	{
		ShowWindow(TRUE);
	}
}

void VendorDialogInstance::VendingStart()
{
	if (IsPending())
		return;
	OnPending();

	////////////////////////////////////////
	MSG_CG_VENDOR_START_SYN sendMsg;

	sTOTALINFO_ESTABLISHER_VENDOR&  venderInfos = sendMsg.m_ItemInfo;
	
	////////////////////////////////////////
	venderInfos.m_Count = 0;

	for (SLOTPOS i = 0; i < m_pTradeContainer->GetSlotMaxSize(); i++)
	{
		DummyItemSlot& rSlot = (DummyItemSlot&)m_pTradeContainer->GetSlot(i);

		if (rSlot.GetCode()== 0)
			continue;

		sVENDOR_ITEM_SLOT& vitSlot = venderInfos.m_Slot[venderInfos.m_Count];
		vitSlot.m_Money		= rSlot.GetExtraData();
		vitSlot.m_Dura			= rSlot.GetNum();
		vitSlot.m_VendorPos	= i;
		vitSlot.m_OrgPos		= rSlot.GetFromPosition();

		venderInfos.m_Count++;
	}

	__ZERO(sendMsg.m_pszTitle);
	lstrcpyn(sendMsg.m_pszTitle, m_VendorMessage, MAX_VENDOR_TITLE_LENGTH);

	theHero.SendPacket( &sendMsg, sendMsg.GetSize() , TRUE);

}


BOOL VendorDialogInstance::ProcPricesInsert( LPCSTR  szInputData, void * pData )
{
	VendorDialogInstance*	pVendor = (VendorDialogInstance*)pData;

	MONEY money	= (MONEY)_atoi64(szInputData);
	pVendor->_VendingInsert(money);
	return true;
}


void VendorDialogInstance::VendingInsert(SLOTPOS toPos, SLOTPOS fromPos)
{
	if(m_bVendorView || !m_bStarted)
	{
		if(theMouseHandler.IsExistItemAtHand())
			theMouseHandler.CancelHandlingItem();
		return;
	}


	//////////////////////////////////////////////////
	BaseContainer*	pInven	= theItemManager.GetContainer(SI_INVENTORY);
	ItemSlot&		slot		= (ItemSlot&)pInven->GetSlot(fromPos);

	////////////////////////////////////////////////
	if(toPos == INVALID_POSTYPE)
	{
		if(!m_pTradeContainer->GetEmptyPos(toPos))
		{
			OUTPUTTIP(TEXTRES_VENDOR_NONE_SPACE);
			return;
		}
	}

	//////////////////////////////////////////////////
	if(m_bVendorWorking)
	{
		if(!m_pTradeContainer->IsEmpty(toPos))
		{
			OUTPUTTIP(TEXTRES_VENDOR_SLOT_NOTEMPTY);
			OUTPUTTOP(TEXTRES_VENDOR_SLOT_NOTEMPTY);
			return;
		}
	}

	m_PendingModifyItemSlot.m_OrgPos		= fromPos;
	m_PendingModifyItemSlot.m_VendorPos	= toPos;
	m_PendingModifyItemSlot.m_Dura		= slot.GetNum();

	theGameUIManager.UIShowInputBox	(TEXTRES_Sell_Price
												,TRUE
												,true
												,ProcPricesInsert
												,this);




}

void VendorDialogInstance::_VendingInsert(MONEY money)
{
	m_PendingModifyItemSlot.m_Money	= money;
	if(m_bVendorWorking)
	{
		MSG_CG_VENDOR_INSERT_SYN	msg;
		msg.m_ItemSlot	= m_PendingModifyItemSlot;
		theHero.SendPacket(&msg,sizeof(msg),TRUE);
	}
	else
	{
		OnVendorInsert();
	}
}

void VendorDialogInstance::VendingBuy(SLOTPOS pos)
{
	if(!m_bVendorView)
		return;

	if(m_pTradeContainer->IsEmpty(pos))
		return;

	/////////////////////////////////////////
	DummyItemSlot&	slot = (DummyItemSlot&)m_pTradeContainer->GetSlot(pos);
	if(slot.GetExtraData() <= 0)
	{
		OUTPUTTIP(TEXTRES_VENDOR_JUST_SHOW);
		return;
	}

	/////////////////////////////////////////
	if(theHero.GetMoney() < (MONEY)slot.GetExtraData() )
	{
		OUTPUTTIP(TEXTRES_NOT_ENOUGH_MONEY);
		return;
	}

	/////////////////////////////////////////
	MSG_CG_VENDOR_BUY_SYN	msg;
	msg.m_VendorPos	= pos;
	theHero.SendPacket(&msg,sizeof(msg),TRUE);
}


//------------------------------------------------------------------------------
void VendorDialogInstance::ItemUp(SLOTIDX /*fromSlotIdx*/
                                 ,SLOTIDX /*toSlotIdx*/
											,POSTYPE fromPos
											,POSTYPE toPos
											,MONEY   m)
{
    DummyItemSlot& itemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(toPos);
    itemSlot.SetExtraData		(m);
    itemSlot.SetFromPosition	(fromPos);

    m_curMoney    = m;

	 //////////////////////////////////////////
    if (itemSlot.GetItemInfo()->IsCanEquip())
    {
        m_curAmouts   = 1;
    }
    else
    {
        m_curAmouts   = itemSlot.GetNum();
    }
    m_curPos      = toPos;

    if (theMouseHandler.IsExistItemAtHand())
    {
        theMouseHandler.CancelHandlingItem();
    }
}

//------------------------------------------------------------------------------
void VendorDialogInstance::ItemUpCancel(SLOTIDX /*fromSlotIdx*/
                                       ,SLOTIDX /*toSlotIdx*/
													,POSTYPE /*fromPos*/
													,POSTYPE toPos)
{
	DeleteSlot(toPos, NULL);
    //m_pTradeContainer->DeleteSlot(toPos, NULL);

    if (theMouseHandler.IsExistItemAtHand())
    {
        theMouseHandler.CancelHandlingItem();
    }
}

//------------------------------------------------------------------------------
//void VendorDialogInstance::QueryItemModify(POSTYPE /*pos*/)
//{
//    //MSG_CG_VENDOR_MODIFY_REQ_SYN sendMsg;
//    //sendMsg.m_dwKey         = theGeneralGameParam.GetUserID();
//    //sendMsg.m_byCategory    = CG_VENDOR;
//    //sendMsg.m_byProtocol    = CG_VENDOR_MODIFY_REQ_SYN;
//    //sendMsg.m_VendorPos     = pos;
//
//    //theHero.SendPacket(  &sendMsg, sizeof (sendMsg), TRUE);
//}


BOOL VendorDialogInstance::ProcPricesModify( LPCSTR  szInputData, void * pData )
{
	VendorDialogInstance*	pVendor = (VendorDialogInstance*)pData;

	MONEY money	= (MONEY)_atoi64(szInputData);
	pVendor->ItemModify(INVALID_SLOTPOS,money,true);
	return true;
}

//------------------------------------------------------------------------------
void VendorDialogInstance::OpenModifyDialog(POSTYPE pos)
{
	if(m_pTradeContainer->IsEmpty(pos))
		return;

	DummyItemSlot&	slot = (DummyItemSlot&)m_pTradeContainer->GetSlot(pos);

	m_PendingModifyItemSlot.m_OrgPos		= slot.GetFromPosition();
	m_PendingModifyItemSlot.m_VendorPos	= pos;
	m_PendingModifyItemSlot.m_Dura		= slot.GetNum();
	m_PendingModifyItemSlot.m_Money		= slot.GetExtraData();

	theGameUIManager.UIShowInputBox	(TEXTRES_Sell_Price
												,TRUE
												,true
												,ProcPricesModify
												,this);
}

//------------------------------------------------------------------------------
void VendorDialogInstance::ItemModify(POSTYPE pos, MONEY money, bool isNetSend)
{
	if(pos == INVALID_SLOTPOS)
		pos = m_PendingModifyItemSlot.m_VendorPos;


	if (isNetSend && m_bVendorWorking)
	{
		DummyItemSlot& uiItemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(pos);
		//uiItemSlot.SetMoney(money);

		//m_curMoney    = money;
		//m_curAmouts   = uiItemSlot.GetNum();
		//m_curPos      = pos;

		//sVENDOR_ITEM_SLOT m_ItemSlot;

		m_PendingModifyItemSlot.m_Dura		= uiItemSlot.GetNum();
		m_PendingModifyItemSlot.m_Money		= money;
		m_PendingModifyItemSlot.m_OrgPos		= uiItemSlot.GetFromPosition();
		m_PendingModifyItemSlot.m_VendorPos	= pos;

		MSG_CG_VENDOR_MODIFY_SYN sendMsg;
		sendMsg.m_ItemSlot      = m_PendingModifyItemSlot;

		theHero.SendPacket( &sendMsg, sizeof(sendMsg), TRUE );
	}
	else
	{
		DummyItemSlot& uiItemSlot = (DummyItemSlot &)m_pTradeContainer->GetSlot(pos);
		uiItemSlot.SetExtraData(money);

		m_curMoney    = money;
		if (uiItemSlot.GetItemInfo()->IsCanEquip())
		{
			m_curAmouts   = 1;
		}
		else
		{
			m_curAmouts   = uiItemSlot.GetNum();
		}
		m_curPos      = pos;

		if(m_pTradeContainer->GetSlotListener())
			m_pTradeContainer->GetSlotListener()->OnSlotAdd(uiItemSlot);
	}
	if(theMouseHandler.IsExistItemAtHand())
		theMouseHandler.CancelHandlingItem();
}

//------------------------------------------------------------------------------
void VendorDialogInstance::ItemModifyCancel()
{
}

//------------------------------------------------------------------------------
void VendorDialogInstance::QueryItemModifyRequest(POSTYPE pos)
{
    m_PendingModifyPos = pos;

    MSG_CG_VENDOR_MODIFY_REQ_SYN sendMsg;
    sendMsg.m_VendorPos    = m_PendingModifyPos;

    theHero.SendPacket( &sendMsg, sizeof(sendMsg), TRUE );
}

//------------------------------------------------------------------------------
bool VendorDialogInstance::IsVendedItem(POSTYPE FromPos)
{
	BaseContainer*	pInven;

	pInven = theItemManager.GetContainer(SI_INVENTORY);

	if (!pInven)
		return false;

	// check already uploaded
	for (SLOTPOS i=0; i<m_pTradeContainer->GetSlotMaxSize(); ++i)
	{
		if (m_pTradeContainer->IsEmpty(i))
			continue;

		BaseSlot& invenSlot	= pInven->GetSlot(FromPos);
		BaseSlot& fromSlot	= m_pTradeContainer->GetSlot(i);
		if( invenSlot.GetSerial() == fromSlot.GetSerial() )
			return true;
	}

	return false;
}



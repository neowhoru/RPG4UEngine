/*////////////////////////////////////////////////////////////////////////
文 件 名：GameStartUp.cpp
创建日期：2006年9月26日
最后更新：2006年9月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameStartUp.h"
#include "SceneLogicFlow.h"
#include "GameInfoLoader.h"
#include "GameResLoader.h"
#include "Map.h"
#include "GameInPlaying.h"
#include "V3DGameWorld.h"
#include "V3DConfig.h"
#include "ApplicationSetting.h"
#include "SoundLayer.h"
#include "V3DDecal.h"
#include "GameMainUtil.h"
#include "ScreenTipManager.h"
#include "GameEffectManager.h"
#include "PathGameMap.h"
#include "V3DTerrain.h"
#include "GameInScriptManager.h"
#include "GameCommandSystem.h"
#include "GameMainUtil.h"
#include "MediaPathManager.h"
#include "GameWorldResManagerDefine.h"
#include "SkeletonDefine.h"
#include "ResPublishManager.h"
#include "AttributeManager.h"
#include "AttributeAffectorManager.h"
#include "V3DPlatformProbe.h"
#include "HeroSoundListener.h"
#include "SlotSystem.h"
#include "V3DHugeModelManager.h"
#include "V3DTerrainLODManager.h"
#include "RenderSystemManager.h"
#include "TextLogger.h"
#include "ApplicationStartUp.h"
#include "V3DSceneObjectManager.h"
#include "ScriptWord.h"
#include "GameParameter.h"
#include "StudioPathManager.h"



using namespace gamemain;
using namespace skeleton;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameStartUp, ()  , gamemain::eInstPrioGameFunc);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameStartUp::GameStartUp()
:m_SettingType(SETTING_LOCAL)
{
}
GameStartUp::~GameStartUp()
{
}

BOOL GameStartUp::InitDependence(HINSTANCE /*hInstance*/)
{
	return theApplicationStartUp.InitDependence();


}

static LPCTSTR	g_arSettings[]=
{
#ifdef _DEBUG
	 _T("GeneralD.ini")
	,_T("GeneralD.Net.ini")
#else 
	 _T("General.ini")
	,_T("General.Net.ini")
#endif
};


BOOL GameStartUp::InitStartUp(HINSTANCE hInstance)
{
	_ParseCmdLine	();

	GlobalUtil::InitRandom();

	__CHECK(InitDependence(hInstance));

	///优先级次之
	theApplicationSetting.LoadSetting();

	//InitFileLog( (LPCSTR)theApplicationSetting.m_LogFile );


	__INIT(theGeneralGameParam.Load( _PATH_SETTING(g_arSettings[m_SettingType]) ));

	__INIT(theLoginGameParam.Load( _PATH_SETTING("LoginServers.txt") ));
	__INIT(theGameOption.Load( _PATH_SETTING("GameOption.ini")  ));



	//初始化网络连接
	if(!theNetworkLayer.InitStartUp(hInstance))
		return FALSE;

	//读取程序配置级信息
	return TRUE;
}



void GameStartUp::_ParseCmdLine()
{
	m_SettingType	= SETTING_LOCAL;

	if(m_CmdLine.IsBlank())
		return;

	//GameServerIP				= 192.168.77.188			//可由命令行指定： -GameIP:192.168.77.188:20002
	//GameServerPort				= 20002
	//ChatServerIP				= 192.168.77.188			//可由命令行指定： -ChatIP:192.168.77.188:20004
	//ChatServerPort				= 20004
	//LoginServerIP				= 192.168.77.188			//可由命令行指定： -LoginIP:192.168.77.188:6001
	//LoginServerPort			= 6001

	ScriptWord	cmds;
	if( !cmds.Parse( m_CmdLine )  )
		return;

	for(INT n=0; n<cmds.GetWordCount(); n++)
	{
		if(StrStrI(cmds[n], _T("-GameIP:")) )
		{
			ScriptWord	params(cmds[n],':');
			if(params.GetWordCount() >= 3)
			{
				theGeneralGameParam.SetGameServerIP		(params[1]);
				theGeneralGameParam.SetGameServerPort	((PORTTYPE)params.GetNumber(2));
			}
		}
		else if(StrStrI(cmds[n], _T("-ChatIP:")))
		{
			ScriptWord	params(cmds[n],':');
			if(params.GetWordCount() >= 3)
			{
				theGeneralGameParam.SetChatServerIP		(params[1]);
				theGeneralGameParam.SetChatServerPort	((PORTTYPE)params.GetNumber(2));
			}
		}
		else if(StrStrI(cmds[n], _T("-LoginIP:")))
		{
			ScriptWord	params(cmds[n],':');
			if(params.GetWordCount() >= 3)
			{
				if(theLoginGameParam.GetLoginIPNum() == 0)
				{
					theGeneralGameParam.SetLoginServerIP	(params[1]);
					theGeneralGameParam.SetLoginServerPort	((PORTTYPE)params.GetNumber(2));
				}
				else
				{
					theLoginGameParam.PushLoginIP(params[1], (PORTTYPE)params.GetNumber(2));
				}
			}
		}
		else if(StrStrI(cmds[n], _T("-Zone:")))
		{
			ScriptWord	params(cmds[n],':');
			theLoginGameParam.SetServerZoneID((PORTTYPE)params.GetNumber(1));
		}
		else if(StrStrI(cmds[n], _T("-Online")))
		{
			m_SettingType	= SETTING_ONLINE;
		}
	}

}



void GameStartUp::AfterShutdown()
{
	theNetworkLayer.Release();


	////////////////////////////////////////
	RenderSystem* pRenderSystem = GetActiveRender();
	if(pRenderSystem)
	{
		//隐藏活动的RenderSystem
		theRenderSystemManager.ActivateDefaultRender();
		pRenderSystem->Destroy();
	}

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameStartUp::StartUp()
{
	if(!PrevStartUp())
		return FALSE;

	//1.读取各类配置信息
	if(!LoadGameSetting())
		return FALSE;

	//2.读取各类资源信息
	if(!LoadGameInfo())
		return FALSE;

	//读取各类资源数据
	if(!LoadGameRes())
		return FALSE;

	//初始化各类底层引擎
	if(!InitGameEngine())
		return FALSE;

	//初始化各类游戏功能管理
	if(!InitGameFunc())
		return FALSE;


	return AfterStartUp();
}

void GameStartUp::Shutdown()
{
	PrevShutdown();

	ReleaseGameFunc();

	ReleaseGameEngine();

	DestroyGameRes();
	DestroyGameInfo();
	DestroyGameSetting();

	AfterShutdown();
}


BOOL GameStartUp::PrevStartUp()
{
	//主窗口创建成功后，就创建RenderSystem
	RenderSystem* pRenderSystem = GetActiveRender();
	assert(pRenderSystem);
	
	pRenderSystem->Create(theApplication.GetInstance(), g_hWndMain);//theApplication.GetMainWnd());
	pRenderSystem->InitDisplay(FALSE);	//窗口模式

	return TRUE;
}

BOOL GameStartUp::AfterStartUp()
{
	//最后启动Scene 业务逻辑
	if(!singleton::GetSceneLogicFlow().StartUp())
		return FALSE;

	return TRUE;
}


void GameStartUp::PrevShutdown()
{
	gTrueTime.WriteLog();

	theV3DMapLoadManager.SetMainThreadExit(TRUE);
	Sleep(10);

	singleton::GetSceneLogicFlow().Shutdown();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameStartUp::LoadGameSetting()
{
	return TRUE;
}


BOOL GameStartUp::LoadGameInfo()
{
	if(!theGameInfoLoader.StartUp())
		return FALSE;

	return TRUE;
}


BOOL GameStartUp::LoadGameRes()
{
	if(!theGameResLoader.StartUp())
		return FALSE;
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameStartUp::InitGameEngine()
{
		string sPath;
		string sPath2;
		string sPath3;
		string sPath4;
		string sPath5;

	assert(g_hWndMain);
	//这里初始化各类底层引擎
	// .running 先初始化，初始化时钟
	// .framework
	//	.input ...
	//	.3d引擎
	// .ui
	//	.network ...

	__INIT(theGameRunning.Init());

	__INIT(theGameCommandSystem.Init());

	// .framework
	__INIT(singleton::GetFrameworkSystem().Init());

	__INIT(theTileWorld.Init());
	theGameRunning.AddListener(&theTileWorld);

	// Map
	__INIT(theTerrainLODManager.Init());
	__INIT(theGameWorld.StartUp());
	__INIT(theMap.Init(&theGameWorld));
	//__INIT(theMap.Init());

	//	.input ...
	__INIT(theInputLayer.Init(g_hWndMain));
	__INIT(theInputLayer.Create(g_hWndMain));

	// .sound
	__INIT(theSoundLayer.Init());
	/*__INIT*/(theSoundLayer.InitAudioSystem(g_hWndMain));
	theSoundLayer.PauseMusic(FALSE);
	theSoundLayer.PauseSound(FALSE);

	theSound.SetHeroListener(&theHeroSoundListener);


	//	.3d引擎
	__INIT(theV3DPlatformProbe.Init());
		if( theV3DPlatformProbe.GetMemoryType() == MEMORYTYPE_LOW )
		{
			theApplicationSetting.m_FarPlane = theGameWorld.ChangeFarPlane( 5 );
			theApplicationSetting.m_FarFog   = theApplicationSetting.m_FarPlane *0.8f;	
		}
		//g_V3DConfig.m_bUseSkinMesh = theApplicationSetting.m_bUseSkinMeshShader;
		the3DEngine.SetMultiThreadLoadTexture(TRUE);



	sPath		= _PATH_DATAINFO("Script\\LevelUp.txt");
	sPath2	= _PATH_DATAINFO("Script\\UseItem.txt");
	sPath3	= _PATH_DATAINFO("Script\\Time.txt");
	sPath4	= _PATH_DATAINFO("Script\\ScriptEvent.txt");
	sPath5	= _PATH_DATAINFO("Script\\KeywordDefine.txt");

	//Script配置
	__INIT(theScriptManager.Init() );
	__INIT(theScriptManager.InitScripts	(sPath.c_str()	
													,sPath2.c_str()
													,sPath3.c_str()
													,sPath4.c_str()
													,sPath5.c_str()) );


	//脚本配置
	__INITINST2	(theQuestManager,MAX_QUEST_POOLSIZE,MAX_QUEST_SIZE);
	__LOADINFO	(theQuestManager, "ResInfo(Quest).info");


	__INIT(theResPublishManager.Init());
	__INIT(theResPublishManager.LoadSetting(_PATH_PUBLISH("ResPublishSetting.txt" )));

	sPath		= _PATH_DATAINFO("BaseInfo\\ResPublishInfo.info" );
	sPath2	= _PATH_DATAINFO("BaseInfo\\DirPublishInfo.info" );
	__INIT(theResPublishManager.Load(sPath.c_str(), sPath2.c_str()));


	//REG_PUBLISH(ScriptLevelUp	);


	__INIT(theStudioPathManager.Init());

	// .ui
	__INIT(theGameUIManager.Init());

	//	.network ...
	//在theNetworkLayer.InitStartUp()中已经初始化
	//theNetworkLayer.Init();


	return TRUE;
}

BOOL GameStartUp::InitGameFunc()
{
#pragma message(__FILE__  "(281) g_pfnGetDecal初始化...需要调整  " )
	g_pfnGetDecal = RenderUtil::GetTerrainDecal;

	/////////////////////////////
	__INIT(theScreenTipManager.Init());

	__INIT(theSlotSystem.Init());

	//这里初始化各类游戏功能管理
	//__INIT(theTileWorld.Init());
	//theGameRunning.AddListener(&theTileWorld);

	__INIT(theSkillSpecialEffectManager.Init());
	theSkillSpecialEffectManager.InitGlobalEffect	(theApplicationSetting.m_TargetLockEffectPassivity
																	,theApplicationSetting.m_TargetLockEffectInitiative);
	__INIT(theGameEffectManager.Init());

	//vobject
	__INIT(theVObjectSystem.Init());

	//object
	__INIT(theObjectSystem.Init());

	__INIT(theV3DSceneObjectManager.Init());

	__INIT(theAttributeManager.Init());
	__INIT(theAttributeAffectorManager.Init());

	//__INIT(theItemSystem.Init());

	  thePathGameMap.SetSize(128, 128);
	  thePathGameMap.SetCellSize(MAPTILESIZE);

	__INIT(theGameInPlaying.Init());
	

	//__INIT(theCamera.Create( CAMERA_DEFAULT_NEAR_CLIP, CAMERA_DEFAULT_FAR_CLIP ,CAMERA_DEFAULT_FOV));


	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameStartUp::ReleaseGameFunc()
{
	__END(theGameWorld.Shutdown());

	//__END(theCamera.Destroy());

	__END(theGameInPlaying.Release());

	//Item
	//__END(theItemSystem.Release());

	//object
	__END(theObjectSystem.Release());
	//vobject
	__END(theVObjectSystem.Release());

	///////////////////////////////////////////
	__END(theAttributeAffectorManager.Release());
	__END(theAttributeManager.Release());

	__END(theV3DSceneObjectManager.Release());



	__END(theGameEffectManager.Release());
	__END(theSkillSpecialEffectManager.Release());

	theGameRunning.RemoveListener(&theTileWorld);
	//__END(theTileWorld.Release());

	__END(theSlotSystem.Release());

	__END(theScreenTipManager.Release());
}

void GameStartUp::ReleaseGameEngine()
{

	__END(singleton::GetGameRunning().Release());

	//在AfterShutdown调用
	//theNetworkLayer.Release();

	// .ui
	__END(theGameUIManager.Release());

	__END(theStudioPathManager.Release());

	__END(theResPublishManager.Release());

	__END(theQuestManager.Release());

	__END(theScriptManager.Release());


	if(singleton::ExistIV3DEngine())
		the3DTextureManager.EndThreadTextureLoading();

	__END(theV3DPlatformProbe.Release());

	__END(theInputLayer.Release());
	__END(theSoundLayer.Release());

	//__END(theMap.Release());
	__END(theMap.Release());
	__END(theTerrainLODManager.Release());

	__END(theTileWorld.Release());

	__END(theGameCommandSystem.Release());

	__END(singleton::GetFrameworkSystem().Release());
}

void GameStartUp::DestroyGameRes()
{
	__END(theGameResLoader.Shutdown());
}

void GameStartUp::DestroyGameInfo()
{
	__END(theGameInfoLoader.Shutdown());
}

void GameStartUp::DestroyGameSetting()
{
	//theGeneralGameParam.Destroy();
	//theGeneralGameParam.Destroy();
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：gameuiicondraglistener.cpp
创建日期：2006年9月10日
最后更新：2006年9月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameIconDragListener.h"
#include "MouseHandler.h"
#include "BaseContainer.h"
#include "VUIIconMouseTip.h"
#include "VUCtrlIconDragManager.h"
#include "MouseHandler.h"
#include "QuickPaneSlot.h"
#include "ItemManager.h"
#include "SkillStorageManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameIconDragListener, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameIconDragListener::GameIconDragListener()
{
	m_eVItemShowMode = VITEMSHOW_AUTO;
}
GameIconDragListener::~GameIconDragListener()
{
}

BOOL 	GameIconDragListener::Init	(  DWORD /*dwNum*/ )
{
	m_itemModel.Init(10);
	return TRUE;
}

VOID	GameIconDragListener::Release()
{
	m_itemModel.Release();
}


BOOL	GameIconDragListener::PrevInstall()
{
	return TRUE;
}

void	GameIconDragListener::OnInstallTo(BaseContainer* /*pContainer*/)
{
}

VOID	GameIconDragListener::FlushSlotListener()
{
}

VUCtrlIconDrag *GameIconDragListener::GetIconDragCtrl(SLOTPOS /*atPos*/)
{
	return NULL;
}


VOID	GameIconDragListener::Update(SLOTPOS /*pos*/, UINT /*nUpdateNum*/)
{
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID	GameIconDragListener::OnSlotAdd(const BaseSlot&		slotDat )
{
	BaseContainer*		pContainer;
	SlotUIListener*		pListener;

	pContainer	=	theItemManager.GetContainer(slotDat.GetSlotIndex() );
	pListener	=	pContainer->GetSlotListener();
	if(pListener)
	{
		VUCtrlIconDrag*		pListCtrl;
		IconDragListImg*		pItemImg;
		//IconDragListImg*		pHandleImg;
		pListCtrl = pListener->GetIconDragCtrl(slotDat.GetPos());
		if(pListCtrl)
		{
			SLOTPOS	atPos;
			atPos		= (SLOTPOS)(slotDat.GetPos() - pListCtrl->GetSlotStartIndex() );
			pItemImg = pListCtrl->GetItemAt(atPos);
			theIconDragManager.SwapDrag(pListCtrl, pItemImg);

			UpdateMouseInfo(pContainer, atPos);
		}
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID	GameIconDragListener::OnSlotRemove(const BaseSlot&		/*slotDat*/ )
{
	theIconMouseTip.Clear();
	theIconDragManager.ClearDrag();
}

void	GameIconDragListener::OnSlotAdd(SERIALTYPE			/*serial*/
													,CODETYPE			/*code*/
													,const BaseSlot *	/*pSlot*/)
{
	//BaseContainer* pContainerFrom = static_cast<BaseContainer*>(pContainerFromPtr);
	assert(0);
}

VOID	GameIconDragListener::OnSlotRemove(SERIALTYPE	/*serial*/ )
{
	assert(0);
}

VOID	GameIconDragListener::OnSlotStateChange	(const BaseSlot&		/*slotDat*/, DWORD /*dwState*/ )
{
}


void GameIconDragListener::OnInstall()
{
}

void GameIconDragListener::OnUninstall()
{
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameIconDragListener::OnDragFrom	(void* pContainerFromPtr,	INT nDragAt)
{
	BaseContainer* pContainerFrom = static_cast<BaseContainer*>(pContainerFromPtr);
	__VERIFY_PTR(pContainerFrom, "非法BaseContainer指针");

	SLOTINDEX slotIdx = pContainerFrom->GetSlotIndex();

	nDragAt += pContainerFrom->GetPageSlotBase();

	if(slotIdx == SI_SKILL)
	{
		theSkillStorageManager.SetUISkillContainer((eSKILL_CATEGORY)pContainerFrom->GetSubSlotIdx());
	}
	__CHECK( theMouseHandler.DoItemHandling(slotIdx,(SLOTPOS)nDragAt) );


	return TRUE;
	//return UpdateMouseInfo(pContainerFrom, nDragAt);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameIconDragListener::OnDropTo	(void* pContainerToPtr,		INT nDropAt)
{
	BaseContainer* pContainerTo = static_cast<BaseContainer*>(pContainerToPtr);
	__VERIFY_PTR(pContainerTo, "非法BaseContainer指针，请调用ListImg的SetContainer关联容器指针");

	SLOTINDEX slotIdx = pContainerTo->GetSlotIndex();

	nDropAt += pContainerTo->GetPageSlotBase();

	theMouseHandler.DoItemHandling(slotIdx,(SLOTPOS)nDropAt);
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameIconDragListener::RenderDraggingModel(DWORD dwTick, const RECT& rect)
{
	switch(m_eVItemShowMode)
	{
	case VITEMSHOW_AUTO:	
	case VITEMSHOW_MODEL:	
		break;
	default:
		return FALSE;
	}

	__CHECK(m_itemModel.IsModelCanRender());


	if(theMouseHandler.GetMouseState() == eMOUSE_NONE )
	{
		m_itemModel.SetViewRect	(rect.left
										,rect.top
										,(rect.right-rect.left)*2
										,(rect.bottom-rect.top)*2
										,TRUE);
		m_itemModel.Render(dwTick);
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameIconDragListener::CanShowIcon(DWORD dwShow)
{
	switch(m_eVItemShowMode)
	{
	case VITEMSHOW_AUTO:
	case VITEMSHOW_ICON:
		if(!(dwShow & eCANSHOW_ICON))	return FALSE;
		break;

	case VITEMSHOW_ICONBIG:	
		if(!(dwShow & eCANSHOW_ICONBIG))		return FALSE;
		break;

	default:
		return FALSE;
	}


	__CHECK(!m_itemModel.IsModelCanRender());
	__CHECK(theMouseHandler.GetMouseState() == eMOUSE_NONE);
	return TRUE;
}

BOOL GameIconDragListener::CanShowCursor()
{
	switch(m_eVItemShowMode)
	{
	case VITEMSHOW_HIDE:	
		return FALSE;
	}

	__CHECK(theMouseHandler.GetMouseState() == eMOUSE_NONE);
	return TRUE;
}

BOOL GameIconDragListener::CanShowToolTip()
{
	if(theMouseHandler.GetMouseState() != eMOUSE_NONE)
		return TRUE;

	return theIconDragManager.GetCurItem() == NULL;
	//return !CanShowIcon();
}

BOOL GameIconDragListener::CanHideOrginIcon()
{
	mouse::MouseItem&	mouseItem = theMouseHandler.GetMouseItem();

	BaseContainer* pContainer = theItemManager.GetContainer(mouseItem.m_fromSlotIdx);
	return pContainer && pContainer->IsCanHideOrginIcon();

	//if(theMouseHandler.GetMouseState() != eMOUSE_NONE)
	//	return TRUE;

	//return theIconDragManager.GetCurItem() == NULL;
	//return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameIconDragListener::UpdateMouseInfo	(BaseContainer* pContainerFrom,	INT nDragAt)
{
	SLOTINDEX	slotIdx	= pContainerFrom->GetSlotIndex();
	DWORD		dwItemID	= INVALID_DWORD_ID;

	m_eVItemShowMode = VITEMSHOW_AUTO;

	if(theMouseHandler.GetMouseState() == eMOUSE_NONE)
	{
		switch(slotIdx)
		{
		case SI_QUICK:
			{
				QuickPaneSlot&  quickSlot = (QuickPaneSlot&)pContainerFrom->GetSlot((SLOTPOS)nDragAt);
				if(quickSlot.GetOrgSlotType() == ST_ITEM)
				{
					dwItemID = quickSlot.GetOrgCode();
				}
			}
			break;
		case SI_SKILL:
		case SI_STYLE:
			break;

		default:
			{
				BaseSlot&  slot = pContainerFrom->GetSlot((SLOTPOS)nDragAt);
				switch(slot.GetSlotType())
				{
				case ST_ITEM:
				case ST_ITEMHANDLE:
					{
						dwItemID = slot.GetCode();
					}break;
				}
			}
			break;
		}

		if(dwItemID != -1)
		{
			//sITEMINFO_BASE*  pItem = theItemInfoParser.GetItemInfo(dwItemID);
			//if(pItem && pItem->m_wType != ITEMTYPE_MATERIAL)
			{
				sVITEMINFO_BASE* pVItem = theItemInfoParser.GetVItemInfoByItem(dwItemID);
				if(pVItem)
				{
					m_eVItemShowMode = (eVITEM_SHOW_TYPE)pVItem->m_byShowMode;
					switch(m_eVItemShowMode)
					{
					case VITEMSHOW_AUTO:	
					case VITEMSHOW_MODEL:	
						{
							dwItemID		= pVItem->m_ModelID;	
						}break;

					//case VITEMSHOW_ICON:	
					//	{
					//	}break;
					//case VITEMSHOW_ICONBIG:
					//	{
					//	}break;
					//case VITEMSHOW_HIDE:
					//	{
					//	}break;
					}
					
				}
			}
		}
	}


	m_itemModel.ReloadMesh(dwItemID);

	return TRUE;
}

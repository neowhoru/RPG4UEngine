/*////////////////////////////////////////////////////////////////////////
文 件 名：vheroinputdata.cpp
创建日期：2007年10月13日
最后更新：2007年10月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VHeroInputData.h"

namespace vobject
{ 


VHeroInputData g_InputData;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VHeroInputData::VHeroInputData()
{
	m_IsJumping				= FALSE; //主角是否起跳
	m_IsAttackLockTarget	= FALSE;

	
	//m_TargetPos;			//主角的目标位置
	//m_TargetDirection; //主角的目标方向
	//m_JumpingDir;		//主角的跳跃方向
	//m_CurDirection;		//主角的当前方向
	//m_TargetPos;			//主角的目标位置
	//m_TargetDirection; //主角的目标方向
	//m_SlipJumpingDir;	//主角下滑跳跃的方向

	m_MovingByDir			= FALSE;	//主角是否以方向移动
	m_HeroCounterMarch	= FALSE;	//主角是否在倒退移动
	m_AutoAttackTarget	= FALSE;
	m_AutoRunForward		= FALSE;
	m_MouseCtrlMode		= TRUE;	//鼠标移动模式
	m_LockCameraMode		= TRUE;	//锁定摄象机模式

	m_DirectionCorrect	= 0;
	m_RotateZCorrect		= 0.0f;	//主角的转向休整
	m_LookUpAngle			= math::cPI*0.54f;
	m_SlipStartTime		= 0;		//主角开始下滑的时间
	m_JumpingRotate		= 0.0f;
	m_IsSlipJumping		= FALSE; //主角在下划跳跃
	m_IsSpacePressed		= FALSE;
	m_SpacePressTime		= 0;
	m_JumpStartTime		= 0; //主角的起跳时间

}

VHeroInputData::~VHeroInputData()
{
}




};//namespace vobject

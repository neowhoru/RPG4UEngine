/*////////////////////////////////////////////////////////////////////////
文 件 名：MouseHandler.cpp
创建日期：2008年5月31日
最后更新：2008年5月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Hero.h"
#include "MouseHandler.h"
#include "SlotUIListener.h"
#include "Mouse.h"
#include "QuickPaneSlot.h"
#include "StyleSlot.h"
#include "SkillSlot.h"
#include "ConstTextRes.h"

#include "ShopDialog.h"
#include "InventoryDialog.h"
#include "QuickContainer.h"
#include "StyleContainer.h"
#include "EventInventoryDialog.h"
#include "HeroEquipmentContainer.h"
#include "dummyItemSlot.h"
#include "ItemCompoundDialog.h"
#include "SlotUIListener.h"
#include "ItemCompositeParser.h"
#include "CursorHandler.h"
#include "ItemTransactionManager.h"
#include "WareHouseDialog.h"
#include "ItemManagerNet.h"
#include "TradeDialog.h"
#include "VendorDialog.h"


using namespace mouse;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(MouseHandler, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MouseHandler::MouseHandler()
{
	memset( &m_MouseItem, 0, sizeof( MouseItem ) );
	m_eTradeState	= eMOUSE_NONE;
	m_pSlotListener= NULL;
}

MouseHandler::~MouseHandler()
{
	m_pSlotListener = NULL;
	//ReleaseSlotListener();
}


VOID MouseHandler::InitSlotListener(SlotUIListener *  pSlotUIListener )
{
	__VERIFY2_PTR(pSlotUIListener, "不能为NULL",;);
	if(pSlotUIListener == m_pSlotListener)
		return;

	//if(m_pSlotListener)
	//	m_pSlotListener->OnUninstall();

	m_pSlotListener = pSlotUIListener;
	//m_pSlotListener->OnInstall();
}


VOID MouseHandler::ReleaseSlotListener(SlotUIListener *  pSlotUIListener)
{
	if(m_pSlotListener)
	{
	__VERIFY2(pSlotUIListener == m_pSlotListener
				, "必须为之前安装的pSlotUIListener"
				,;);

	//	m_pSlotListener->OnUninstall();
	}

	m_pSlotListener = NULL;
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID MouseHandler::CancelHandlingItem()
{
	if( IsExistItemAtHand() )
	{
		_RefreshContainer(m_MouseItem.m_fromSlotIdx);	

		////////////////////////////////////////////
		DWORD dwSound = INVALID_DWORD_ID;

		if ( m_MouseItem.m_pFromSlot->GetSlotType() == ST_QUICK ) 
			dwSound = SLOTSOUND_LINK;
		else if (m_MouseItem.m_fromSlotIdx == SI_EQUIPMENT)
			dwSound = SLOTSOUND_LINK;
		else
			dwSound = SLOTSOUND_PUT;

		if(dwSound != INVALID_DWORD_ID)
			theItemManager.PlaySlotSound((eSLOT_SOUND_TYPE)dwSound
                                     ,m_MouseItem.m_pFromSlot)				;
		

		////////////////////////////////////
		EndHandlingItem	(m_MouseItem.m_fromSlotIdx
								,m_MouseItem.m_fromPos
								,m_MouseItem.m_slotType
								,m_MouseItem.m_pFromSlot->GetCode()
								,FALSE);

		if(m_pSlotListener)
			m_pSlotListener->OnSlotRemove( *m_MouseItem.m_pFromSlot );

		_InitMouseItem();
	}

	theKeyQueueManager.DeleteMsg	(ESC_PROC_DRAG_ITEM);

}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID MouseHandler::ClearItemHandling()
{
	if (m_MouseItem.m_pFromSlot)
	{
		m_MouseItem.m_pFromBaseItemInfo = theItemInfoParser.GetItemInfo( m_MouseItem.m_pFromSlot->GetCode() );
		//EQUIP_OFF_DEFAULT	
		//EQUIP_ON_DEFAULT	
		//theSoundEffect.Play(EQUIP_ON_DEFAULT	);

		EndHandlingItem	(m_MouseItem.m_fromSlotIdx
								,m_MouseItem.m_fromPos
								,m_MouseItem.m_slotType
								,m_MouseItem.m_pFromSlot->GetCode()
								,FALSE);
	}


	if(m_pSlotListener)
		m_pSlotListener->OnSlotRemove( *m_MouseItem.m_pFromSlot );


	_InitMouseItem();

	m_MouseItem.m_pFromSlot				= NULL;
	m_MouseItem.m_pFromBaseItemInfo	= NULL;
}


//----------------------------------------------------------------------------
VOID MouseHandler::EndHandlingItem	(SLOTINDEX      toSlotIdx
												,SLOTPOS        toPos
												,eSLOTTYPE      /*slotType*/
												,SLOTCODE       /*code*/
												,BOOL				 bSwapIfExit)
{
	__UNUSED(bSwapIfExit);

	BaseContainer * pContainerPrev	(NULL);

	pContainerPrev	= theItemManager.GetContainer( toSlotIdx );
	__CHECK2_PTR(pContainerPrev,;);

	//BaseSlot&	toSlot = pContainerPrev->GetSlot(toPos);

	if( pContainerPrev->IsCanHideOrginIcon())
	{
		pContainerPrev->SetSlotState	(toPos
												,SLOT_UISTATE_INVISIBLE
												,SLOT_STATE_REMOVE);
	}

	//if(m_pSlotListener)
	//	m_pSlotListener->OnSlotAdd	(toSlot);
}

VOID MouseHandler::BeginHandlingItem(SLOTINDEX      fromSlotIdx
												,SLOTPOS        fromPos
												,eSLOTTYPE      slotType
												,SLOTCODE       /*code*/
												,BOOL				 bSwapIfExit)
{
	if(bSwapIfExit)
	{
		if( IsExistItemAtHand() )
		{
			//_InitMouseItem();
			//if(m_pSlotListener)
			//	m_pSlotListener->OnSlotRemove( DEFAULT_MOUSE_ITEM_SERIAL );
		}
	}

	BaseContainer * pAtContainer	(NULL);

	m_MouseItem.m_fromSlotIdx		= fromSlotIdx;
	m_MouseItem.m_fromPos			= fromPos;
	m_MouseItem.m_slotType			= slotType;


	pAtContainer	= theItemManager.GetContainer( fromSlotIdx );
	if( !pAtContainer->IsEmpty( fromPos ) ) 
	{
		m_MouseItem.m_pFromSlot = &pAtContainer->GetSlot( fromPos );
     	theItemManager.PlaySlotSound	(SLOTSOUND_PICK
                                    ,m_MouseItem.m_pFromSlot);

		if(pAtContainer->IsCanHideOrginIcon())
		{	
			pAtContainer->SetSlotState		(fromPos
													,SLOT_UISTATE_INVISIBLE
													,SLOT_STATE_ADD);
		}
	}


	if(m_pSlotListener)
		m_pSlotListener->OnSlotAdd	(*m_MouseItem.m_pFromSlot);
	if(!bSwapIfExit)
	{
		theKeyQueueManager.PushBack	(ESC_PROC_DRAG_ITEM
												,GlobalUtil::DoDragItem);
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MouseHandler::DoItemDrop()
{
	if(theItemTransactionManager.DoDropTransaction(m_MouseItem.m_fromSlotIdx
																 ,m_MouseItem.m_fromPos))
	{
		return TRUE;
	}

	CancelHandlingItem();
	return TRUE;
}


//----------------------------------------------------------------------------
BOOL MouseHandler::DoItemHandling	(SLOTINDEX SlotIdx
                                    ,SLOTPOS posTo)
{
	//////////////////////////////////////////////
	/// 鼠标上物品的处理
	if( IsExistItemAtHand() )
	{ 
		//BaseContainer * pFromContainer	= 	theItemManager.GetContainer(m_MouseItem.m_fromSlotIdx); 
		BaseContainer * pToContainer		=	theItemManager.GetContainer(SlotIdx); 

		if ( posTo >= pToContainer->GetSlotMaxSize() )
		{
			CancelHandlingItem();
			return FALSE;
		}

		/// tran
		if(theItemTransactionManager.DoItemTransaction(m_MouseItem.m_fromSlotIdx
                                                    ,m_MouseItem.m_fromPos
																	 ,SlotIdx
																	 ,posTo
																	 ,m_MouseItem.m_pFromSlot))
		{
			return TRUE;
		}
	}//if( IsExistItemAtHand() )

	//////////////////////////////////////////////
	/// 鼠标上没物品的处理
	else
	{
		BaseContainer * pAtContainer = theItemManager.GetContainer(SlotIdx);
		ASSERT(pAtContainer);

		BaseContainer * pContainerDlg = theItemManager.GetContainer(SlotIdx);
		ASSERT(pContainerDlg);

		if ( posTo >= pAtContainer->GetSlotMaxSize() )
			return FALSE;

		if ( pAtContainer->IsEmpty(posTo))
			return FALSE;

		BaseSlot &		slotDat				= pAtContainer->GetSlot(posTo);
		eSLOTTYPE	eTempSlotType	= slotDat.GetSlotType();

		//////////////////////////////////////////////////
		///宝石拆除
		if (GetMouseState() == eMOUSE_SOCKET_REMOVE)
		{
			switch( eTempSlotType  )
			{
			case ST_ITEMHANDLE:
				{
					switch( SlotIdx  )
					{
						/// 点击Socket槽时，把宝石转移到SubResult中
					case SI_ENCHANT_SOCKET:
						{
							theItemCompoundDialog.RemoveSocketItem(posTo);
							return TRUE;
						}break;

						/// 点击subResult槽时，把宝石转移到Socket中
					case SI_ENCHANT_TARGET:
						{
							theItemCompoundDialog.ReTry(theItemCompoundDialog.GetCurrentCompositeType());
							return TRUE;
						}break;

					case SI_ENCHANT_SUBRESULT:
						{
							theItemCompoundDialog.RestoreSocketItem(posTo);
							return TRUE;
						}break;
					}//switch( SlotIdx  )
				}break;
			}//switch( eTempSlotType  )
			return FALSE;
		}

		//////////////////////////////////////////////////
		///其它正常模式

		switch( eTempSlotType  )
		{
		case ST_ITEMHANDLE:
			{
				switch(SlotIdx)
				{
					/// 摆摊=>
					case SI_VENDOR_SELL:
					case SI_VENDOR_BUY:
					{
						if (GetMouseState() == eMOUSE_VENDOR_BUY)
						{
							theVendorDialog.VendingBuy(posTo);
						}
						else if(theHero.GetBehaveState() == PLAYER_BEHAVE_VENDOR_OBSERVER)
						{
							if(IsExistItemAtHand())
								CancelHandlingItem();
							return FALSE;
						}
					}break;
				}//switch(SlotIdx)
			}break;

		case ST_ITEM:
			{
				switch(SlotIdx)
				{
				/// 买状态下，NPC商店 =>
				case SI_NPCSHOP:
				{
					if (GetMouseState() == eMOUSE_BUY)
					{
						ItemSlot & rSlot1		= (ItemSlot &)pAtContainer->GetSlot(posTo);				
						ShopDialog * pShopDlg	= static_cast<ShopDialog *>(pContainerDlg);

						DWORD			dwShopID;
						ShopDetailInfo *	pShopInfo;
						sSHOP_ITEM * pShopItem;

						dwShopID		= (DWORD)pShopDlg->GetShopID();
						pShopInfo	= (ShopDetailInfo *)theShopInfoParser.GetShopList( dwShopID );
						pShopItem	= pShopInfo->GetItem(rSlot1.GetCode());

						__VERIFY_PTR(pShopItem, "ShopItem not found!!");
						
                        
						theItemManagerNet.SendBuyMsg(pShopDlg ->GetShopID()
                                             ,rSlot1.GetCode()
															,(BYTE)pShopDlg->GetCurrentPage()
															,pShopDlg->GetSlotPosRelative(posTo)
															);

						return TRUE;
					}
				}break;

				//////////////////////////////////////////////
				/// 背包物品=> 
				case SI_EQUIPMENT:
				{
					switch(GetMouseState())
					{
					//////////////////////////////////////////////
					/// 修理状态
					case eMOUSE_REPAIR:
						theItemManagerNet.SendRepairMsg(SlotIdx, posTo);
						return TRUE;
					}
				}break;

				//////////////////////////////////////////////
				/// 背包物品=> 
				case SI_INVENTORY:
				{
					switch(GetMouseState())
					{
					//////////////////////////////////////////////
					/// 修理状态
					case eMOUSE_REPAIR:
						theItemManagerNet.SendRepairMsg(SlotIdx, posTo);
						return TRUE;

					//////////////////////////////////////////////
					/// 卖状态
					case eMOUSE_SELL:
						theItemManagerNet.SendSellMsg(SlotIdx, posTo);
						return TRUE;

					//////////////////////////////////////////////
					///存储状态
					case eMOUSE_DEPOSIT:
						{
							SLOTPOS	EmptyPos = INVALID_POSTYPE;
							BOOL		bRet		= theItemManager.GetEmptySlotPos(SI_WAREHOUSE, EmptyPos);
							if (bRet)
							{
								theItemManagerNet.SendItemMoveMsg(SI_INVENTORY
																			,SI_WAREHOUSE
																			,posTo
																			,EmptyPos);
							}
						}return TRUE;
					}
				}break;

				//////////////////////////////////////////////
				/// 交易 =>
				case SI_TRADE:
				{
					if (m_MouseItem.m_slotType == ST_NONE)
					{
						return TRUE;
					}

					if (m_MouseItem.m_fromPos >= 10)
					{
						return TRUE;
					}
				}break;

				/// 仓库 =>
				case SI_WAREHOUSE:
				{
					/// 取物状态
					if (GetMouseState() == eMOUSE_WITHDRAW)
					{
						SLOTPOS EmptyPos = INVALID_POSTYPE;
						BOOL bRet = theItemManager.GetEmptySlotPos(SI_INVENTORY, EmptyPos);
						if (bRet)
						{
							theItemManagerNet.SendItemMoveMsg(SI_WAREHOUSE
                                                   ,SI_INVENTORY
																	,posTo
																	,EmptyPos);
						}
						return TRUE;
					}
				}break;

			}//switch
			}break;

			/// 快速栏
		case ST_QUICK:
			{
				QuickPaneSlot & rQuickSlot = (QuickPaneSlot & )slotDat;

				if( !pAtContainer->IsEmpty( posTo ) )
				{
					BeginHandlingItem(SlotIdx
										 ,posTo
										 ,eTempSlotType
										 ,rQuickSlot.GetOrgCode());

					return TRUE;
				}
			}
			break;

			////////////////////////////////////////////
			/// 心法栏
		case ST_STYLE_QUICK:
			{
#if !defined ( _USE_AUTO_STYLE_QUICK )
				StyleSlot & rStyleSlot = (StyleSlot &)slotDat;

				if( !pAtContainer->IsEmpty( posTo ) )
				{
					BeginHandlingItem	(SlotIdx
                              ,posTo
										,eTempSlotType
										,rStyleSlot.GetOrgCode());

					return TRUE;
				}
#else
				return FALSE;
#endif //_USE_AUTO_STYLE_QUICK 
				
			}
			break;

			////////////////////////////////////////////////
			///技能栏
		case ST_SKILL:
			{
				DWORD code=slotDat.GetCode();
				sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)code);
				if(pInfo && pInfo->m_bySkillUserType == SKILLUSER_SUMMONED)
				{
					return FALSE;
				}
			}
			break;

		default:
			ASSERT(!"not support type!!");
			return FALSE;
			break;
		}


//laSetCursor:
		if( !pAtContainer->IsEmpty( posTo ) )
		{
			//BaseSlot & slotDat = pAtContainer->GetSlot(posTo);
			BeginHandlingItem	(SlotIdx
									 ,posTo
									 ,eTempSlotType
									 ,pAtContainer->GetSlot(posTo).GetCode());
		}
	}
//#endif

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
	物品使用处理
/*////////////////////////////////////////////////////////////////////////
BOOL MouseHandler::DoItemUsing( SLOTINDEX SlotIdx, SLOTPOS posIndex)
{
	BaseContainer * pAtContainer;
	
	pAtContainer = theItemManager.GetContainer(SlotIdx);
	__VERIFY_PTR(pAtContainer,"");

	__CHECK(!theHero.IsDead());


	//////////////////////////////////////////
	switch (SlotIdx)
	{
	case SI_INVENTORY:
		{	
			ItemSlot & rItemSlot = (ItemSlot &)pAtContainer->GetSlot(posIndex);
		
			//////////////////////////////////////////
			if(theTradeDialog.IsStarted())
			{
				theTradeDialog.PutItem(INVALID_POSTYPE,posIndex);
				return TRUE;
			}

			//////////////////////////////////////////
			if(theVendorDialog.IsStarted())
			{
				theVendorDialog.VendingInsert(INVALID_POSTYPE,posIndex);
				return TRUE;
			}


			//////////////////////////////////////////
			if(rItemSlot.GetDamageLV())
			{
				OUTPUTCHAT(TEXTRES_ITEM_NOT_ACCEPT_DAMAGED);
				return TRUE;
			}

			///////////////////////////////////////
			/// 装备上装备处理...
			if (rItemSlot.GetItemInfo()->IsCanEquip())
			{
				BOOL bCanEquip;
				bCanEquip = theItemManager.CanEquip	(SlotIdx
                                                ,posIndex
																,(SLOTPOS)rItemSlot.GetItemInfo()->m_wEqPos);
				if (bCanEquip)
				{
					SLOTPOS EQPos = INVALID_POSTYPE;
					
					__CHECK(theItemManager.GetEquipPosition(rItemSlot, EQPos));

					theItemManagerNet.SendItemMoveMsg(SI_INVENTORY
                                                ,SI_EQUIPMENT
																,posIndex
																,EQPos);
				}
				else
				{
					theHeroActionInput.PlayLimitUseSound();;
				}

				return TRUE;
			}

			///////////////////////////////////////////////////////
			/// 药品使用处理
			else
			{
				sITEMINFO_BASE * pItemInfo;
				pItemInfo = theItemInfoParser.GetItemInfo(rItemSlot.GetCode());
				__VERIFY_PTR(pItemInfo,"Item is NULL");

				__CHECK(CheckItemUsing(pItemInfo));
				__CHECK(theHero.CanUseItem(pItemInfo->m_byWasteType,TRUE));

				theItemManagerNet.SendItemUseMsg(SlotIdx, posIndex);
			}
		}
		break;

		///技能使用处理
	case SI_SKILL:
		{
			SkillSlot & rSkillSlot = (SkillSlot &)pAtContainer->GetSlot(posIndex);	
			if(rSkillSlot.IsSKill())
				theHeroActionInput.DoSkillProcessing(rSkillSlot.GetCode());
			else
				theHeroActionInput.UseStyle( rSkillSlot.GetCode() );
		}
		break;

		////////////////////////////////////////////////////
		/// 快速栏物品使用
	case SI_QUICK:
		{
			QuickContainer *	pAtContainer;
			SLOTPOS				posQuickReal;

			pAtContainer = (QuickContainer * )theItemManager.GetContainer( SI_QUICK);
			posQuickReal = (SLOTPOS)pAtContainer->GetCurrentPage();

			posQuickReal = posQuickReal + posIndex;
			
			if (posQuickReal > pAtContainer->GetSlotMaxSize() )
				return FALSE;

			if( pAtContainer->IsEmpty( posQuickReal ) ) 
				return FALSE;

			QuickPaneSlot & slotFrom = (QuickPaneSlot &)pAtContainer->GetSlot( posQuickReal );
			SLOTPOS OrgPos		= 0;        

			///无效物品不处理
			if (eQUICK_ACTIVATE !=  slotFrom.GetStatus())
				return FALSE;

			///////////////////////////////////////////////
			switch (slotFrom.GetOrgSlotType())
			{
			case ST_ITEM:
				{
					//////////////////////////////////////////
					if(	theTradeDialog.IsStarted()
						||	theVendorDialog.IsStarted())
					{
						OUTPUTTIP(TEXTRES_TRADE_ITEM_CANNT_USING);
						return FALSE;
					}

					/// 物品链接时，物品须来自背包
					if ( SI_INVENTORY != slotFrom.GetOrgSlotIndex() )
						return FALSE;
		
					sITEMINFO_BASE * pItemInfo;
					
					pItemInfo = (sITEMINFO_BASE *)theItemInfoParser.GetItemInfo(slotFrom.GetOrgCode());
					if( !pItemInfo )
						break;

					if(pItemInfo->IsCanEquip())
					{
						BaseContainer * pInvenContainer = theItemManager.GetContainer(SI_INVENTORY);

						ItemSlot & rItemSlot = (ItemSlot &)pInvenContainer->GetSlot(slotFrom.GetOrgPos());

						SLOTPOS EQPos = 255;
						BOOL bCorrectPos = theItemManager.GetEquipPosition(rItemSlot, EQPos);
						if ( !bCorrectPos )
							return FALSE;

						BOOL bCanEquip = theItemManager.CanEquip(SI_INVENTORY
                                                          ,slotFrom.GetOrgPos()
																			 ,EQPos);
						if (!bCanEquip)
						{
							theHeroActionInput.PlayLimitUseSound();
							return FALSE;
						}

						if (SI_INVENTORY != slotFrom.GetOrgSlotIndex() )
							return FALSE;

						theItemManagerNet.SendItemMoveMsg	(SI_INVENTORY
                                                   ,SI_EQUIPMENT
																	,slotFrom.GetOrgPos()
																	,EQPos);							
						return TRUE;
					}

					///消耗品处理，则处理背包中首次发现的物品
					else if(pItemInfo->IsCanUseWaste())
					{
						__CHECK(CheckItemUsing(pItemInfo));

						BOOL bRet = theItemManager.LocateItemAtFirst(slotFrom.GetOrgCode(), OrgPos);
						if (bRet)
						{
							sITEMINFO_BASE * pItemInfo;
							
							pItemInfo = (sITEMINFO_BASE *)theItemInfoParser.GetItemInfo(slotFrom.GetOrgCode());

							if(	pItemInfo
								&&	theHero.CanUseItem(pItemInfo->m_byWasteType,TRUE))
							{
								theItemManagerNet.SendItemUseMsg(SI_INVENTORY, OrgPos);
							}
						}
					}
				}
				break;

				/// 使用技能
			case ST_SKILL:
				{
					sSKILLINFO_COMMON* pSkill;
					
					pSkill = theSkillInfoParser.GetInfo(slotFrom.GetOrgCode());
					if(pSkill->IsStyle())
						return theHeroActionInput.UseStyle(slotFrom.GetCode());
					return theHeroActionInput.DoSkillProcessing(slotFrom.GetOrgCode());
				}
				break;
			}
		}
		break;

		///////////////////////////////////////////////////
		///心法栏处理
	case SI_STYLE:
		{
			if (!singleton::ExistHero())
				return FALSE;

			if (theHero.IsDead())
				return FALSE;
			
			StyleContainer *	pAtContainer;
			SLOTPOS				posStyleReal;
			
			pAtContainer = (StyleContainer * )theItemManager.GetContainer( SI_STYLE);
			posStyleReal = (SLOTPOS)pAtContainer->GetCurrentPage();

			posStyleReal = posStyleReal + posIndex;
			
			if (posStyleReal > pAtContainer->GetSlotMaxSize() )
				return FALSE;

			if( pAtContainer->IsEmpty( posStyleReal ) ) 
				return FALSE;

			StyleSlot & slotFrom = (StyleSlot &) pAtContainer->GetSlot( posStyleReal );
			//SLOTPOS OrgPos		= 0;        

			ASSERT(slotFrom.GetOrgSlotType() == ST_SKILL);

			SkillSlot * pSkillSlot;
			pSkillSlot = (SkillSlot *)theItemManager.GetSkillSlot( slotFrom.GetOrgCode() );

			__VERIFY_PTR(pSkillSlot,"invalid skill slot.!!");

			theHeroActionInput.UseStyle(pSkillSlot->GetCode());
		}
		break;
	}

	return TRUE;
}






BOOL MouseHandler::DoItemRankUp( SLOTINDEX /*SlotIdx*/, SLOTPOS /*posTo*/, RECT * /*pRect*/)
{
	return FALSE;



}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MouseHandler::UpdateCursor()
{
	eCURSOR_TYPE eCursor = CURSORTYPE_DEFAULT;
	
	switch (m_eTradeState)
	{
	case eMOUSE_VENDOR_BUY:		eCursor = cursor::CURSORTYPE_BUY;				break;
	case eMOUSE_BUY:				eCursor = cursor::CURSORTYPE_BUY;				break;
	case eMOUSE_SELL:				eCursor = cursor::CURSORTYPE_SELL;			break;
	case eMOUSE_REPAIR:			eCursor = cursor::CURSORTYPE_REPAIR;			break;
	case eMOUSE_ENCHANT:
	case eMOUSE_SOCKET_FILL:	eCursor = cursor::CURSORTYPE_ITEMMAKE;		break;
	case eMOUSE_SOCKET_REMOVE:	eCursor = cursor::CURSORTYPE_REMOVE_GEM;	break;

	default:
		break;
	}

	if(eCursor != CURSORTYPE_DEFAULT)
		theCursorHandler.ChangeTo(eCursor, TRUE);
	else
		theCursorHandler.ChangeTo(CURSORTYPE_DEFAULT, TRUE);

	return eCursor != CURSORTYPE_DEFAULT;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MouseHandler::CheckItemUsing(sITEMINFO_BASE*	pItemInfo)
{
	__CHECK_PTR(pItemInfo);

	INT nTextID	= -1;
	//BOOL bHPFulled = FALSE;
	//BOOL bMPFulled = FALSE;
	INT  nHPMPFulled = 0;


	switch(pItemInfo->m_byWasteType)
	{
	case ITEMWASTE_HPPOTION:
	case ITEMWASTE_HPRATE_POTION:
		if(theHero.GetHP() >= theHero.GetMaxHP()) nTextID = TEXTRES_PLAYER_HP_FULL;
		break;
	case ITEMWASTE_HPMPPOTION:
	case ITEMWASTE_HPMPRATE_POTION:
		if(theHero.GetHP() >= theHero.GetMaxHP()) nHPMPFulled++;
		break;
	}

	switch(pItemInfo->m_byWasteType)
	{
	case ITEMWASTE_MPPOTION:
	case ITEMWASTE_MPRATE_POTION:
		if(theHero.GetMP() >= theHero.GetMaxMP())nTextID = TEXTRES_PLAYER_MP_FULL;
		break;
	case ITEMWASTE_HPMPPOTION:
	case ITEMWASTE_HPMPRATE_POTION:
		if(theHero.GetMP() >= theHero.GetMaxMP()) nHPMPFulled++;
		break;
	}

	if( nHPMPFulled > 1)	nTextID = TEXTRES_PLAYER_MP_FULL;
	if( nTextID  != -1 )
	{
		OUTPUTTIP(nTextID);
		return FALSE;
	}
	return TRUE;
}


//----------------------------------------------------------------------------
VOID MouseHandler::RemoveMouseState(MOUSE_STATE eShop)
{
	if (GetMouseState() == eShop)
	{
		SetMouseState( eMOUSE_NONE );
	}
}


//----------------------------------------------------------------------------
VOID MouseHandler::_InitMouseItem()
{
	m_MouseItem.m_fromSlotIdx			= 0;
	m_MouseItem.m_fromPos				= 0;
	m_MouseItem.m_pFromSlot				= NULL;
	m_MouseItem.m_pFromBaseItemInfo	= NULL;
}

//----------------------------------------------------------------------------
VOID MouseHandler::_RefreshContainer(SLOTINDEX SlotIdx)
{
	BaseContainer * pContainer;
	pContainer = 	(BaseContainer *)theItemManager.GetContainer(SlotIdx);

	if (!pContainer)
		return;
	
	pContainer->UpdateSlotListener();;
}
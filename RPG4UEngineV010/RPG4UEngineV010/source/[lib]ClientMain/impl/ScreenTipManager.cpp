/*////////////////////////////////////////////////////////////////////////
文 件 名：ScreenTipManager.cpp
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	屏幕文字显示管理


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ScreenTipManager.h"
#include "VUIFontDDraw.h"
#include "ApplicationSetting.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ScreenTipManager, ()  , gamemain::eInstPrioGameDisplay);


#define TEXT_FADE_RATE		theApplicationSetting.m_ScreenTextFadeDelay//2000
#define TILE_TEXT_Y			70
#define INFO_TEXT_BEGIN_Y	100

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ScreenTipManager::ScreenTipManager(void)
{
	for ( int i=0; i<SCREEN_TEXT_MAX; i++ )
	{
		m_Texts[i].nFont = 0;
	}
}

ScreenTipManager::~ScreenTipManager(void)
{
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL ScreenTipManager::Init()
{
	int nFontTile = uidraw::CreateFont((LPCSTR) theApplicationSetting.m_szFontName, theApplicationSetting.m_nTitleFontSize );
	int nFontInfo = uidraw::CreateFont((LPCSTR) theApplicationSetting.m_szFontName, theApplicationSetting.m_nFontSize );

	m_Texts[SCREEN_TITLE].nFont = nFontTile;
	for ( int n=SCREEN_TEXT_BEGIN; n<SCREEN_TEXT_MAX; n++ )
	{
		m_Texts[n].nFont = nFontInfo;
	}
	return TRUE;
}

void ScreenTipManager::Release()
{
}

BOOL ScreenTipManager::FrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL ScreenTipManager::Render(DWORD /*dwTick*/)
{
	sSCREEN_TIP *pText = NULL;
	for ( int i=0; i<SCREEN_TEXT_MAX; i++ )
	{
		pText = &m_Texts[i];
		DWORD dwTime = base::GetRunTime() - pText->dwStartTime;

		switch( pText->enShowState ) 
		{
		case eSHOW_NORMAL:
			if ( dwTime >= pText->dwShowTime )
			{
				pText->enShowState = eSHOW_HIDDEN;
			}
			break;

		case eSHOW_FADEIN:
			{
				if ( dwTime >= TEXT_FADE_RATE )
				{
					pText->enShowState	= eSHOW_FADENORMAL;
					pText->dwColor			|= 0xff000000;
					break;
				}
				int	nFade		= TEXT_FADE_RATE - dwTime;
				BYTE	alpha		= (BYTE)(0xFF - 0xFF*nFade/TEXT_FADE_RATE);
				pText->dwColor = COLOR_WITH_A(pText->dwColor, alpha);
			}
			break;

		case eSHOW_FADENORMAL:
			if ( dwTime >= TEXT_FADE_RATE + pText->dwShowTime )
			{
				pText->enShowState = eSHOW_FADEOUT;
			}
			break;

			///////////////////////////////////////
		case eSHOW_FADEOUT:
			{
				if ( dwTime >= TEXT_FADE_RATE*2 + pText->dwShowTime )
				{
					pText->enShowState = eSHOW_HIDDEN;
					break;
				}
				int	nFade = TEXT_FADE_RATE*2 + pText->dwShowTime - dwTime;
				BYTE	alpha = (BYTE)(0xFF*nFade/TEXT_FADE_RATE);

				pText->dwColor = COLOR_WITH_A(pText->dwColor, alpha);
			}
			break;

		case eSHOW_HIDDEN:
			continue;

		default:
			assert( FALSE );
			continue;
		}

		assert(pText->nFont);
			
		uidraw::DrawTextARGB	(pText->nX
                           ,pText->nY
									,pText->strText.c_str()
									,pText->dwColor
									,pText->nFont);
	}

	return TRUE;
}


void ScreenTipManager::SetTile	(LPCSTR szText
                                 ,COLOR  color
											,DWORD  time)
{
	if ( !szText )
		return;

	INT nPosY(TILE_TEXT_Y);
	theGameUIManager.UIGetData(gameui::eUIFrameTop,gameui::eGetScreenTitlePos,&nPosY);

	sSCREEN_TIP* pText = &m_Texts[SCREEN_TITLE];
	_SetText	(pText
            ,0
				,nPosY
				,theApplicationSetting.m_nTitleFontSize
				,time
				,color
				,szText
				,eAlignTop
				,eSHOW_FADEIN);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void ScreenTipManager::SetInfo	(LPCSTR szText
                                 ,COLOR  color
											,DWORD  time)
{
	int nLen = strlen( szText );
	string strText;

	for ( int n=0; n<nLen; n++ )
	{
		BOOL bNewLine = FALSE;
		if ( n+1 < nLen )
		{
			if ( szText[n] == '\\' && szText[n+1] == 'n')
			{
				bNewLine = TRUE;
				n++;
			}
		}

		if ( szText[n] == '\n' )
			bNewLine = TRUE;

		if (	bNewLine
			&&	strText.size() > 0 )
		{
			AddInfo( strText.c_str(), color, time );
			strText.clear();
		}
		else
		{
			strText += szText[n];
		}
	}
	if ( strText.size() > 0 )
	{
		AddInfo( strText.c_str(), color, time );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void ScreenTipManager::AddInfo	(LPCSTR szText
                                 ,COLOR  color
											,DWORD  time
											,int    nInfoIndex)
{
	if ( !szText )
		return;

	int nShowIndex = -1;
	if ( nInfoIndex != -1 )
	{
		nShowIndex = nInfoIndex;
	}
	else
	{
		for ( int n=SCREEN_TEXT_BEGIN; n<SCREEN_TEXT_MAX; n++ )
		{
			sSCREEN_TIP* pText = &m_Texts[n];
			if ( pText->enShowState == eSHOW_HIDDEN )
			{
				nShowIndex = n;
				break;
			}
		}
	}

	//////////////////////////////////////////////
	if ( nShowIndex <  SCREEN_TEXT_BEGIN 
		||nShowIndex >= SCREEN_TEXT_MAX )
	{
		nShowIndex = SCREEN_TEXT_BEGIN;
	}

	//////////////////////////////////////////////
	INT nPosY(INFO_TEXT_BEGIN_Y);
	theGameUIManager.UIGetData	(gameui::eUIFrameTop
										,gameui::eGetScreenTextPos
										,&nPosY);

	nPosY += theApplicationSetting.m_nFontSize * nShowIndex;

	sSCREEN_TIP* pText = &m_Texts[nShowIndex];
	_SetText	(pText
            ,0
				,nPosY
				,theApplicationSetting.m_nFontSize
				,time
				,color
				,szText
				,eAlignTop
				,eSHOW_FADEIN);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void ScreenTipManager::_SetText	(sSCREEN_TIP*      pText
                                 ,int         x
											,int         y
											,int         nFontSize
											,DWORD       time
											,DWORD       color
											,LPCSTR      szText
											,eALIGN_MODE eAlign
											,eSHOW_STATE eShowState)
{
	assert( pText );
	assert( szText );

	if( !szText )
		return;

	time				+= theApplicationSetting.m_ScreenTextDelay;

	int nStrLen		= strlen( szText );
	int nStrWidth	= nStrLen*nFontSize/2;
	int nStrHeight = nFontSize;

	pText->enShowState	= eShowState;
	pText->dwShowTime		= time;
	pText->dwColor			= color;
	pText->strText			= szText;
	pText->dwStartTime	= base::GetRunTime();

	switch( eAlign ) 
	{
	case eAlignLeftTop:
		pText->nX = x;
		pText->nY = y;
		break;
	case eAlignTop:
		pText->nX = SCREEN_WIDTH/2 - nStrWidth/2 + x;
		pText->nY = y;
		break;
	case eAlignRightTop:
		pText->nX = SCREEN_WIDTH - nStrWidth + x;
		pText->nY = y;
		break;
	case eAlignLeft:
		pText->nY = SCREEN_HEIGHT/2 - nStrHeight/2 + x;
		pText->nY = y;
		break;
	case eAlignMiddle:
		pText->nX = SCREEN_WIDTH/2 - nStrWidth/2 + x;
		pText->nY = SCREEN_HEIGHT/2 - nStrHeight/2 + y;
		break;
	case eAlignRight:
		pText->nX = SCREEN_WIDTH - nStrWidth + x;
		pText->nY = SCREEN_HEIGHT/2 - nStrHeight/2 + y;
		break;
	case eAlignLeftBottom:
		pText->nX = x;
		pText->nY = SCREEN_HEIGHT - nStrHeight + y;
		break;
	case eAlignBottom:
		pText->nX = SCREEN_WIDTH/2 - nStrWidth/2 + x;
		pText->nY = SCREEN_HEIGHT - nStrHeight + y;
		break;
	case eAlignRightBottom:
		pText->nX = SCREEN_WIDTH - nStrWidth + x;
		pText->nY = SCREEN_HEIGHT - nStrHeight + y;
		break;
	default:
		assert( FALSE );
		pText->nX = x;
		pText->nY = y;
		break;
	}
}


#include "stdafx.h"
#include ".\quickcontainer.h"
#include "SlotUIListener.h"
#include "MouseHandler.h"
#include "QuickPaneSlot.h"
#include "ItemManager.h"
#include <Protocol_ClientGameS.h>
#include "InventoryDialog.h"
#include "Hero.h"
#include "HeroEquipmentContainer.h"
#include "HeroActionInput.h"
#include "PlayerCookie.h"
#include "mouse.h"

#include "ObjectManager.h"
#include "SlotKeyGenerator.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_IMPL(QuickContainer);



//----------------------------------------------------------------------------
QuickContainer::QuickContainer()
{
	GLOBALINST_SINGLETON_PTR_INIT(QuickContainer);
}


//----------------------------------------------------------------------------
QuickContainer::~QuickContainer()
{
	GLOBALINST_SINGLETON_PTR_FREE(QuickContainer);
	ASSERT( NULL == m_pSlotListener );
}


//----------------------------------------------------------------------------
VOID QuickContainer::Release()
{
	_SUPER::Release();
	m_pSlotListener = NULL;
}


//----------------------------------------------------------------------------
BOOL QuickContainer::Init(SLOTPOS			MaxSlotSize	  
								 , SLOTINDEX		idx)	

{
	__BOOL_SUPER(Init(MaxSlotSize,idx));
	return TRUE;
}


//----------------------------------------------------------------------------
VOID QuickContainer::InitSlotListener()
{
	if(m_pSlotListener)
		m_pSlotListener->Init(GetSlotMaxSize() );
}


//----------------------------------------------------------------------------
BOOL QuickContainer::IsExistSameSerialItem(SERIALTYPE Serial)
{
	for (SLOTPOS i = 0; i < GetSlotMaxSize(); ++i)
	{
		if (IsEmpty(i))
			continue;

		QuickPaneSlot & slotQuick = (QuickPaneSlot &)GetSlot(i);

		if ( slotQuick.GetOrgSerial() == Serial )
			return TRUE;
	}

	return FALSE;
}


//----------------------------------------------------------------------------
BOOL QuickContainer::IsExistSameCodeItem( CODETYPE Code )
{
	for (SLOTPOS i = 0; i < GetSlotMaxSize(); ++i)
	{	
		if (IsEmpty(i))
			continue;

		QuickPaneSlot & slotQuick = (QuickPaneSlot &) GetSlot(i);

		if ( slotQuick.GetOrgCode() == Code )
			return TRUE;
	}

	return FALSE;

}


//----------------------------------------------------------------------------
SLOTPOS QuickContainer::GetPosByCode( CODETYPE Code )
{
	for (SLOTPOS i = 0; i < GetSlotMaxSize(); ++i)
	{	
		if (IsEmpty(i))
			continue;

		QuickPaneSlot & slotQuick = (QuickPaneSlot &) GetSlot(i);

		if ( slotQuick.GetOrgCode() == Code )
			return i;
	}

	return INVALID_QUICKSLOT;
}


//----------------------------------------------------------------------------
BOOL QuickContainer::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )
{ 
	__BOOL_SUPER(InsertSlot( posIndex, slotDat ));
	
	QuickPaneSlot & slotQuick = (QuickPaneSlot & )(GetSlot(posIndex));
	
	ASSERT( ST_QUICK == slotDat.GetSlotType() );

	if(m_pSlotListener)
		m_pSlotListener->OnSlotAdd(slotQuick);

	return TRUE;
}



//----------------------------------------------------------------------------
VOID  QuickContainer::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	QuickPaneSlot & slotDat = (QuickPaneSlot &)GetSlot(posIndex);
	
	if (slotDat.GetOrgSerial())
	{
		theSlotKeyGenerator.Restore( slotDat.GetOrgSerial() );
		if(m_pSlotListener)
			m_pSlotListener->OnSlotRemove( slotDat);
	}

	_SUPER::DeleteSlot( posIndex, pSlotOut );
}


VOID QuickContainer::UpdateSlot( SLOTPOS posIndex, BaseSlot & IN slotDat ) //leo quick
{
	BaseContainer::UpdateSlot( posIndex, slotDat );
	if(m_pSlotListener)
		m_pSlotListener->Update(posIndex);
}


//----------------------------------------------------------------------------
VOID QuickContainer::NetworkProc( MSG_BASE * /*pMsg*/ )
{

}


//----------------------------------------------------------------------------
VOID	 QuickContainer::ShowWindow(BOOL /*val*/)
{

}




VOID QuickContainer::FlushSlotListener()
{
	if (!m_pSlotListener)
		return;

	m_pSlotListener->FlushSlotListener();

	for (SLOTPOS i = 0; i < GetSlotMaxSize(); i++)
	{
		if( !IsEmpty(i) )
			DeleteSlot(i, NULL);
	}

}


VOID QuickContainer::UpdateSlotListener()
{
	if(m_pSlotListener)
		m_pSlotListener->Update();
}



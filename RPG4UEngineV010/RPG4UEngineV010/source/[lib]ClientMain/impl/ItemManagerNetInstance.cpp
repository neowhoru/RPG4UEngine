/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManagerNetInstance.cpp
创建日期：2006年5月13日
最后更新：2006年5月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemManagerNetInstance.h"
#include "ShopInfoParser.h"
#include "PacketInclude.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemManagerNetInstance, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemManagerNetInstance::ItemManagerNetInstance()
{
}
ItemManagerNetInstance::~ItemManagerNetInstance()
{
}




//////////////////////////////////////////////////////////////////////////
// Send Packet
VOID ItemManagerNetInstance::SendItemMoveMsg( SLOTINDEX fromIdx, SLOTINDEX toIdx, SLOTPOS fromPos, SLOTPOS toPos )
{
	MSG_CG_ITEM_MOVE_SYN msg;
	//msg.m_byCategory	= CG_ITEM;
	//msg.m_byProtocol	= CG_ITEM_MOVE_SYN;

	msg.m_dwKey			= theGeneralGameParam.GetUserID();

	msg.m_fromIndex	= fromIdx;
	msg.m_toIndex		= toIdx;
	msg.m_fromPos		= fromPos;
	msg.m_toPos			= toPos;
	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&msg
									,sizeof(msg)
									,&theHero);
}


VOID ItemManagerNetInstance::SendSellMsg( SLOTINDEX atIndex, SLOTPOS atPos )
{
	MSG_CG_ITEM_SELL_SYN ItemSellSyn;

	ItemSellSyn.m_atIndex	= atIndex;
	ItemSellSyn.m_atPos		= atPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&ItemSellSyn
									,sizeof(MSG_CG_ITEM_SELL_SYN)
									,&theHero);
}

VOID ItemManagerNetInstance::SendRepairMsg( SLOTINDEX atIndex, SLOTPOS atPos )
{
	MSG_CG_ITEM_REPAIR_SYN msgSyn;

	msgSyn.m_atIndex	= atIndex;
	msgSyn.m_atPos		= atPos;

	theHero.SendPacket	(&msgSyn
								,sizeof(msgSyn));
}

VOID ItemManagerNetInstance::SendRepairEquipsMsg(  )
{
	MSG_CG_ITEM_REPAIR_EQUIPS_SYN msgSyn;

	theHero.SendPacket	(&msgSyn
								,sizeof(msgSyn));
}

VOID ItemManagerNetInstance::SendBuyMsg(DWORD    dwShopListID
                                       ,SLOTCODE /*ItemCode*/
													,BYTE     byTabIndex
													,SLOTPOS  ItemPos)
{
	//ShopDetailInfo * pShopInfo = theShopInfoParser.GetShopList(dwShopListID);

	ASSERT(byTabIndex < ShopDetailInfo::MAX_PAGE_NUM);
	ASSERT(ItemPos < ShopDetailInfo::MAX_SELL_ITEM);
	
	MSG_CG_ITEM_BUY_SYN ItemBuySyn;

	ItemBuySyn.m_dwShopListID		= dwShopListID;
	ItemBuySyn.m_ShopTabIndex		= byTabIndex;
	ItemBuySyn.m_ShopItemIndex		= ItemPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&ItemBuySyn
									,sizeof(MSG_CG_ITEM_BUY_SYN)
									,&theHero);

}

VOID ItemManagerNetInstance::SendPickupItem(DWORD ItemObjectKey)
{
	MSG_CG_ITEM_PICK_SYN SendPacket;


	SendPacket.m_dwKey = theGeneralGameParam.GetUserID();
	SendPacket.m_dwFieldItemObjectKey = ItemObjectKey;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(MSG_CG_ITEM_PICK_SYN)
									,&theHero);

}

VOID ItemManagerNetInstance::SendPickupMoney(DWORD ItemObjectKey)
{
	MSG_CG_ITEM_PICK_MONEY_SYN SendPacket;


	SendPacket.m_dwKey = theGeneralGameParam.GetUserID();

	SendPacket.m_dwFieldItemObjectKey = ItemObjectKey;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(MSG_CG_ITEM_PICK_MONEY_SYN)
									,&theHero);
}


VOID ItemManagerNetInstance::SendDropItemMsg( SLOTINDEX FromIdx, SLOTPOS FromPos)
{
	MSG_CG_ITEM_DROP_SYN		SendPacket;

	SendPacket.m_dwKey		= theGeneralGameParam.GetUserID();

	SendPacket.m_atIndex		= FromIdx;
	SendPacket.m_atPos		= FromPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(MSG_CG_ITEM_DROP_SYN)
									,&theHero);
}



VOID ItemManagerNetInstance::SendQuickItemLinkMsg( SLOTPOS atPos, SLOTPOS ToPos)
{
	MSG_CG_ITEM_QUICK_LINKITEM_SYN SendPacket;

   SendPacket.m_OrgPos = atPos;
	SendPacket.m_ToPos	= ToPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);

	//MSG_CG_ITEM_QUICKADD_SYN	SendPacket;

	//SendPacket.m_dwKey		= theGeneralGameParam.GetUserID();

	//SendPacket.m_atIndex	= atIndex;
	//SendPacket.m_atPos		= atPos;
	//SendPacket.m_toQuickPos = ToQuickPos;

	//theNetworkLayer.SendPacket( CK_GAMESERVER, &SendPacket, sizeof(MSG_CG_ITEM_QUICKADD_SYN) );
}

//---------------------------------------------------------------
VOID ItemManagerNetInstance::SendQuickSkillLinkMsg( SLOTCODE OrgCode, SLOTPOS ToPos)
{
	MSG_CG_ITEM_QUICK_LINKSKILL_SYN SendPacket;

   SendPacket.m_SkillCode = OrgCode;
	SendPacket.m_ToPos		= ToPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}


VOID ItemManagerNetInstance::SendSimplexMoveLinkMsg	(SLOTPOS fromPos, SLOTPOS ToQuickPos)
{
	MSG_CG_ITEM_QUICK_MOVE_SYN			SendPacket;

	SendPacket.m_fromPos		= fromPos;
	SendPacket.m_toPos		= ToQuickPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}

VOID ItemManagerNetInstance::SendQuickItemRemoveMsg( SLOTPOS posIndex)
{
	MSG_CG_ITEM_QUICK_UNLINK_SYN Sendpacket;

	Sendpacket.m_atPos = posIndex;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&Sendpacket
									,sizeof(Sendpacket)
									,&theHero);
}

////////////////////////////////////////////////////////////////////////////////

VOID ItemManagerNetInstance::SendQuickStyleLinkMsg(SLOTCODE OrgCode, SLOTPOS ToPos)
{
	MSG_CG_STYLE_LINK_SYN SendPacket;

	SendPacket.m_StyleCode	= OrgCode;
	SendPacket.m_ToPos		= ToPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}


VOID 
ItemManagerNetInstance::SendQuickStyleMoveLinkMsg	(SLOTPOS fromPos, SLOTPOS ToPos)
{
	MSG_CG_STYLE_LINKMOVE_SYN SendPacket;

	SendPacket.m_fromPos = fromPos;
	SendPacket.m_toPos	= ToPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}


VOID ItemManagerNetInstance::SendQuickStyleRemoveMsg(SLOTPOS posIndex )
{
	MSG_CG_STYLE_UNLINK_SYN		SendPacket;

	SendPacket.m_atPos = posIndex;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}


//////////////////////////////////////////////////////////////////////////
VOID ItemManagerNetInstance::SendWareHouseItemAllSyn()
{
	MSG_CG_WAREHOUSE_START_SYN			SendPacket;
	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}


VOID ItemManagerNetInstance::SendItemUseMsg(SLOTINDEX	atIndex, SLOTPOS atPos)
{
	MSG_CG_ITEM_USE_SYN SendPacket;

	SendPacket.m_dwKey		= theGeneralGameParam.GetUserID();
	SendPacket.m_atIndex		= atIndex;
	SendPacket.m_atPos		= atPos;

	theNetworkLayer.SendPacket(CK_GAMESERVER
                         ,&SendPacket
								 ,sizeof(SendPacket)
								 ,&theHero);

}

VOID ItemManagerNetInstance::SendItemCombineMsg( SLOTINDEX fromIndex, SLOTINDEX toIndex, SLOTPOS fromPos, SLOTPOS toPos )
{
	MSG_CG_ITEM_COMBINE_SYN SendPacket;

	SendPacket.m_dwKey		= theGeneralGameParam.GetUserID();

	SendPacket.m_fromIndex	= fromIndex;
	SendPacket.m_toIndex		= toIndex;
	SendPacket.m_fromPos		= fromPos;
	SendPacket.m_toPos		= toPos;

	theNetworkLayer.SendPacket	(CK_GAMESERVER
                           ,&SendPacket
									,sizeof(SendPacket)
									,&theHero);
}



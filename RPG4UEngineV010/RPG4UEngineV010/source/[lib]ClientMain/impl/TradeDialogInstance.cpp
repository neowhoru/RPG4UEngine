/*////////////////////////////////////////////////////////////////////////
文 件 名：TradeDialogInstance.cpp
创建日期：2007年12月19日
最后更新：2007年12月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "TradeDialogInstance.h"
#include "PacketInclude.h"
#include "ConstTextRes.h"
#include "MouseHandler.h"
#include "DummyItemSlot.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(TradeDialogInstance, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
TradeDialogInstance::TradeDialogInstance()
{
}
TradeDialogInstance::~TradeDialogInstance()
{
}


//------------------------------------------------------------------------------
BOOL	TradeDialogInstance::Init( )
{
	__CHECK(InitContainer() );

	return TRUE;
}

//------------------------------------------------------------------------------
VOID TradeDialogInstance::Release()
{
	ReleaseContainer();
}


//------------------------------------------------------------------------------
BOOL TradeDialogInstance::InitContainer()
{
	//ItemSlotContainer::Init	(eITEMENCHANT_MAX_SLOT,			SI_ENCHANT );
	//_SUPER::Init	(eITEMENCHANT_MAX_SLOT,			SI_ENCHANT );

	ContainerHandle	viewer	(SLOTT_ITEM);//HANDLE);
	ContainerHandle	trade		(SLOTT_ITEMHANDLE);

	__CHECK_PTR(&viewer	);
	__CHECK_PTR(&trade		);

	m_pViewContainer		= viewer.Detach();
	m_pTradeContainer		= trade.Detach();

	m_pTradeContainer		->Init(MAX_TRADE_SLOT_NUM,		SI_TRADE);
	m_pViewContainer		->Init(MAX_TRADE_SLOT_NUM,		SI_TRADE2);

	return TRUE;
}


//------------------------------------------------------------------------------
VOID TradeDialogInstance::ReleaseContainer()
{
	FlushSlotListener();
	if(m_pViewContainer)
	{
		m_pTradeContainer	->ClearAll();
		m_pViewContainer	->ClearAll();

		m_pTradeContainer	->Release();
		m_pViewContainer	->Release();

		ContainerHandle	trade		(m_pTradeContainer	);
		ContainerHandle	viewer	(m_pViewContainer	);

		m_pTradeContainer		= NULL;
		m_pViewContainer		= NULL;
	}

}

//------------------------------------------------------------------------------
VOID TradeDialogInstance::FlushSlotListener()
{
	if(!m_pTradeContainer)
		return;
	m_pTradeContainer->ClearAll();
	m_pViewContainer->ClearAll();
}

//------------------------------------------------------------------------------
VOID TradeDialogInstance::Update()
{
	if(!m_pTradeContainer)
		return;

	if(m_pTradeContainer->GetSlotListener())
		m_pTradeContainer->GetSlotListener()->Update();

	if(m_pViewContainer->GetSlotListener())
		m_pViewContainer->GetSlotListener()->Update();
}


//------------------------------------------------------------------------------
VOID	TradeDialogInstance::ShowWindow(BOOL val)
{
	m_bStarted	= val;
	if (val)
	{
		//ResetTrade();
		//ReTry(m_CurrentCompositeType);

		theKeyQueueManager.PushBack(ESC_PROC_DIALOG_TRADE
											,GlobalUtil::DoShowWindow
											,(WPARAM)this);

		theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);

		theGameUIManager.UISetVisible	(gameui::eUIInventory,TRUE);
	}
	else
	{
		FlushSlotListener();
		if(m_pViewContainer)
		{
			m_pViewContainer->ClearAll();
			m_pTradeContainer->ClearAll();
		}

		theKeyQueueManager.DeleteMsg(ESC_PROC_DIALOG_TRADE);
	}

	theGameUIManager.UISetVisible	(gameui::eUIBargaining,val);

}


void TradeDialogInstance::CloseWindow()
{
	///////////////////////////////////////
	if(m_bStarted)
	{
		theGameUIManager.UISetVisible(gameui::eUIInventory,FALSE);
	}
	ShowWindow(FALSE);
	ResetTrade();
}


VOID	TradeDialogInstance::RequestTrade	(DWORD dwObjectKey)
{
	ResetTrade();

	/////////////////////////////////////////
	m_pPlayerTrading = (Player*)theObjectManager.GetObject(dwObjectKey);

	if(	m_pPlayerTrading == NULL 
		|| !m_pPlayerTrading->IsPlayer())
	{
		OUTPUTTIP(TEXTRES_NOT_SELECTED_PLAYER );
		return;
	}

	/////////////////////////////////////////
	Vector3D vDisc = theHero.GetPosition() - m_pPlayerTrading->GetPosition();

	const float fDist = theGeneralGameParam.GetDistanceTrading();
	if( vDisc.Length2() > fDist * fDist )
	{
		OUTPUTTIP(TEXTRES_TRADE_TOOFAR);
		return;
	}


	/////////////////////////////////////////
	MSG_CG_TRADE_REQ_SYN sendMsg;
	sendMsg.m_dwTargetPlayerKey = dwObjectKey;

	theHero.SendPacket(&sendMsg, sizeof(sendMsg), TRUE);
}


void TradeDialogInstance::ProcAskTrading(INT   nTipIndex,sTIP_LOG* /*pLogInfo*/)
{
	if(!theTradeDialog.GetPlayerTrading())
		return;


	/////////////////////////////////////////
	Vector3D vDisc = theHero.GetPosition() - theTradeDialog.GetPlayerTrading()->GetPosition();

	const float fDist = theGeneralGameParam.GetDistanceTrading();
	if( vDisc.Length2() > fDist * fDist )
	{
		OUTPUTTIP(TEXTRES_TRADE_TOOFAR);
		nTipIndex = 0;
	}


	/////////////////////////////////////////
	MSG_CG_TRADE_RES_SYN sendMsg;
	sendMsg.m_dwPlayerKey   = theTradeDialog.GetPlayerTrading()->GetObjectKey();

	if (nTipIndex == 0)
	{
		theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
		sendMsg.m_Type = sendMsg.REFUSE;
	}
	else
	{
		//theHero.SetBehaveState(PLAYER_BEHAVE_TRADE);
		sendMsg.m_Type = sendMsg.ACCEPT;
	}
	theHero.SendPacket(&sendMsg,sizeof(sendMsg),TRUE);

}


VOID	TradeDialogInstance::OnRequestTrade	(DWORD dwReqPlayerKey)
{
	ResetTrade();

	m_pPlayerTrading = (Player*)theObjectManager.GetObject(dwReqPlayerKey);
	if(!m_pPlayerTrading || !m_pPlayerTrading->IsPlayer())
		return;

	StringHandle	sText;
	StringHandle	sText2;
	sText.Format	(_STRING(TEXTRES_TRADE_REQUEST_TIP_ACCEPT),  m_pPlayerTrading->GetName());
	sText2.Format	(_STRING(TEXTRES_TRADE_REQUEST_TIP_REJECT),  m_pPlayerTrading->GetName());

	TIPGROUP	(sText2	,eTIP_TEAM,ProcAskTrading, FALSE, TRUE);
	TIPGROUP	(sText	,eTIP_TEAM,ProcAskTrading, TRUE);
   //PopupTradeReply(msg->m_dwPlayerKey);
	//theHero.SetBehaveState(PLAYER_BEHAVE_TRADE);
}


VOID	TradeDialogInstance::OnResponseTrade	()
{
   //PopupTradeReply(msg->m_dwPlayerKey);
	theHero.SetBehaveState(PLAYER_BEHAVE_TRADE);
	//TradeStart
	ShowWindow(TRUE);
}


VOID	TradeDialogInstance::CancelTrade()
{									
	///////////////////////////////////////
	if(theGeneralGameParam.IsEnableNetwork())
	{
		MSG_CG_TRADE_CANCEL_SYN		msg;
		theHero.SendPacket(&msg,sizeof(msg),TRUE);
	}
	else
	{
		OnCancelTrade();
	}
}

VOID	TradeDialogInstance::OnCancelTrade()
{									
	theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
	CloseWindow();
}

VOID	TradeDialogInstance::UnlockTrade		(BOOL bRedraw)
{
	m_bLockTrade1	= FALSE;
	//theGameUIManager.UIRefresh(gameui::eUIBargaining);
	if(bRedraw)
		theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
}

VOID	TradeDialogInstance::LockTrade		(BOOL bRedraw)
{
	m_bLockTrade1	= TRUE;
	//theGameUIManager.UIRefresh(gameui::eUIBargaining);
	if(bRedraw)
		theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
}

BOOL	TradeDialogInstance::PutItem	(POSTYPE tradePos, POSTYPE invenPos)
{
    if (m_dwStatus1 == TRADE_STATUS_ACCEPT)
    {
        theMouseHandler.CancelHandlingItem();
        return false;
    }

	 ////////////////////////////////////////////////
	 if(tradePos == INVALID_POSTYPE)
	 {
		 BaseContainer*	pContainer = theItemManager.GetContainer(SI_TRADE);
		 if(!pContainer->GetEmptyPos(tradePos))
		 {
			 OUTPUTTIP(TEXTRES_TRADE_PLAYER1_HAVENOTSPACE);
			 return FALSE;
		 }
	 }


	 ////////////////////////////////////////////////
    if (m_dwStatus1 != TRADE_STATUS_IDLE)
    {
        m_bModifyPending			= true;
        m_uiFunctionIndex			= TRADE_MOD_PENDING_PUT;
        m_modpendingPut.pos		= invenPos;
        m_modpendingPut.posTrade	= tradePos;

        ModifyTrade();
        return true;
    }
    else
    {
        PutItemSending (tradePos, invenPos);
    }

    return true;
}

void TradeDialogInstance::PutItemSending(POSTYPE tradePos, POSTYPE invenPos)
{
    LockTrade(FALSE);

    MSG_CG_TRADE_PUT_SYN sendMsg;
    sendMsg.m_OrgPos		= invenPos;
    sendMsg.m_TradePos	= tradePos;
   theHero.SendPacket(&sendMsg, sizeof(sendMsg), TRUE );
}


VOID	TradeDialogInstance::OnTradePut1		(POSTYPE tradePos, POSTYPE invenPos)
{
    UnlockTrade();

	BaseContainer * pFromContainer	= 	theItemManager.GetContainer(SI_INVENTORY); 
	BaseContainer * pToContainer		=	theItemManager.GetContainer(SI_TRADE); 

	ItemSlot &	rFromSlot	= (ItemSlot&)pFromContainer->GetSlot(invenPos);

	BOOL				bRollback(TRUE);

	SlotHandle			dummyHandle(SLOTT_ITEMDUMMY);

	if((BOOL)dummyHandle && !pToContainer->IsEmpty(tradePos))
	{
		DummyItemSlot&	dummyItem = (DummyItemSlot&)*dummyHandle;
		bRollback	= FALSE;
		pToContainer->DeleteSlot(tradePos,dummyHandle);
		//theMouseHandler.CancelHandlingItem();
		theMouseHandler.BeginHandlingItem	(dummyItem.GetFromContainerIdx()
												,dummyItem.GetFromPosition()
												,dummyItem.GetOrgSlot()->GetSlotType()
												,dummyItem.GetOrgSlot()->GetCode()
												,TRUE);
	}

	pToContainer->InsertSlot(tradePos,rFromSlot);

	//if(pToContainer->IsEmpty(tradePos))
	//	pToContainer->InsertSlot(tradePos,rFromSlot);
	//else
	//	pToContainer->UpdateSlot(tradePos,rFromSlot);

	pFromContainer->SetSlotState(invenPos, SLOT_UISTATE_DISABLE);

	if(bRollback)
		theMouseHandler.CancelHandlingItem();

}

VOID	TradeDialogInstance::OnTradePut2		(POSTYPE tradePos, ITEMOPT_STREAM&	item)
{
	BaseContainer * pToContainer		=	theItemManager.GetContainer(SI_TRADE2); 

	ItemSlot	itemSlot(item);
	if(!pToContainer->IsEmpty(tradePos))
		pToContainer->DeleteSlot(tradePos,NULL);
	InsertSlot(tradePos, itemSlot, TRUE);
}

BOOL	TradeDialogInstance::GetItem	(POSTYPE tradePos)
{
	if (m_dwStatus1 == TRADE_STATUS_ACCEPT)
	{
		theMouseHandler.CancelHandlingItem();
		return false;
	}

	if (m_dwStatus1 != TRADE_STATUS_IDLE)
	{
		m_bModifyPending				= true;
		m_uiFunctionIndex				= TRADE_MOD_PENDING_GET;
		m_modpendingGet.pos			= tradePos;
		m_modpendingGet.posTrade	= tradePos;

		ModifyTrade();
		return true;
	}
	else
	{
		GetItemSending (tradePos);
	}
	return TRUE;
}

void TradeDialogInstance::GetItemSending(POSTYPE tradePos)
{
    LockTrade(FALSE);

    MSG_CG_TRADE_GET_SYN sendMsg;
    //sendMsg.m_OrgPos		= invenPos;
    sendMsg.m_TradePos	= tradePos;
   theHero.SendPacket(&sendMsg, sizeof(sendMsg), TRUE );
}

VOID	TradeDialogInstance::OnTradeGet1		(POSTYPE tradePos)
{
	UnlockTrade();

	DeleteSlot(tradePos,NULL,FALSE);
	theMouseHandler.CancelHandlingItem();
}

VOID	TradeDialogInstance::OnTradeGet2		(POSTYPE tradePos)
{
	DeleteSlot(tradePos,NULL,TRUE);
}

BOOL	TradeDialogInstance::SwapItem(POSTYPE fromPos, POSTYPE	toPos)
{
    if (m_dwStatus1 == TRADE_STATUS_ACCEPT)
    {
        theMouseHandler.CancelHandlingItem();
        return false;
    }

	 ////////////////////////////////////////////////
    if (m_dwStatus1 != TRADE_STATUS_IDLE)
    {
        m_bModifyPending			= true;
        m_uiFunctionIndex			= TRADE_MOD_PENDING_SWAP;
        m_modpendingSwap.fromPos	= fromPos;
        m_modpendingSwap.toPos	= toPos;

        ModifyTrade();
        return true;
    }
    else
    {
        SwapItemSending (fromPos, 	toPos);
    }

    return true;
}

BOOL	TradeDialogInstance::SwapItemSending(POSTYPE fromPos, POSTYPE	toPos)
{
	if(fromPos == toPos)
	{
		theMouseHandler.CancelHandlingItem();
		return FALSE;
	}

	MSG_CG_TRADE_SWAP_SYN	msgSyn;
	msgSyn.m_FromPos	= fromPos;
	msgSyn.m_ToPos		= toPos;
	theHero.SendPacket(&msgSyn, sizeof(msgSyn), TRUE);
	return TRUE;
}

VOID	TradeDialogInstance::OnTradeSwap1	(POSTYPE fromPos, POSTYPE	toPos)
{
	theTradeDialog.UnlockTrade();

	theItemManager.MoveItem(SI_TRADE,SI_TRADE,fromPos, toPos);

	if(m_pTradeContainer->GetSlotListener())
		m_pTradeContainer->GetSlotListener()->Update();
	theMouseHandler.CancelHandlingItem();
}

VOID	TradeDialogInstance::OnTradeSwap2	(POSTYPE fromPos, POSTYPE	toPos)
{
	theItemManager.MoveItem(SI_TRADE2,SI_TRADE2,fromPos, toPos);

	if(m_pViewContainer->GetSlotListener())
		m_pViewContainer->GetSlotListener()->Update();
}


BOOL	TradeDialogInstance::PutMoney		(MONEY money)
{
	if (m_dwStatus1 == TRADE_STATUS_ACCEPT)
	{
		theMouseHandler.CancelHandlingItem();
		return false;
	}

	if (m_dwStatus1 != TRADE_STATUS_IDLE)
	{
		m_bModifyPending				= true;
		m_uiFunctionIndex				= TRADE_MOD_PENDING_PUTMONEY;
		m_modpendingPutMoney.money	= money;

		ModifyTrade();
		return true;
	}
	else
	{
		PutMoneySending (money);
	}
	return TRUE;
}

void	TradeDialogInstance::PutMoneySending(MONEY money)
{
	MSG_CG_TRADE_PUT_MONEY_SYN	msgSyn;
	msgSyn.m_money = money;
	theHero.SendPacket(&msgSyn,sizeof(msgSyn), TRUE);
}

VOID	TradeDialogInstance::OnMoneyPut1		(MONEY money)
{
	m_Money1	= money;
	//theGameUIManager.UIRefresh(gameui::eUIBargaining);
}

VOID	TradeDialogInstance::OnMoneyPut2		(MONEY money)
{
	m_Money2	= money;
	theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
	//theGameUIManager.UIRefresh(gameui::eUIBargaining);
}

VOID	TradeDialogInstance::OnMoneyGet1		(MONEY /*money*/)
{
}

VOID	TradeDialogInstance::OnMoneyGet2		(MONEY /*money*/)
{
}

VOID	TradeDialogInstance::Proposal		()
{
	//OUTPUTTIP(TEXTRES_TRADE_PROPOSAL_ACK);
	if(	m_pTradeContainer->GetSlotNum() == 0 
		&&	m_Money1 == 0
		&& m_Money2	== 0
		&& m_pViewContainer->GetSlotNum() == 0 )
	{
		OUTPUTTIP(TEXTRES_TRADE_NONE_MONEY_ITEM);
		OUTPUTTOP(TEXTRES_TRADE_NONE_MONEY_ITEM);
		return;
	}

	MSG_CG_TRADE_PROPOSAL_SYN	msgSyn;
	theHero.SendPacket(&msgSyn, sizeof(msgSyn), TRUE);
}

VOID	TradeDialogInstance::OnProposal1		()
{
	OUTPUTTIP(TEXTRES_TRADE_PROPOSAL_ACK);
   m_dwStatus1 = TRADE_STATUS_PROPOSAL;
	theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
	//theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
}

VOID	TradeDialogInstance::OnProposal2		()
{
	OUTPUTTIP(TEXTRES_TRADE_PROPOSAL_CMD);
   m_dwStatus2 = TRADE_STATUS_PROPOSAL;
	theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
}

VOID	TradeDialogInstance::ModifyTrade		()
{
	//OUTPUTTIP(TEXTRES_TRADE_MODIFY_ACK);
	MSG_CG_TRADE_MODIFY_SYN	msgSyn;
	theHero.SendPacket(&msgSyn, sizeof(msgSyn), TRUE);
}

VOID	TradeDialogInstance::OnModify1		()
{
   m_dwStatus1 = TRADE_STATUS_IDLE;
	m_dwStatus2 = TRADE_STATUS_IDLE;
	OUTPUTTIP(TEXTRES_TRADE_MODIFY_ACK);
	theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);

	ProcessPending();
}

VOID	TradeDialogInstance::OnModify2		()
{
   m_dwStatus1 = TRADE_STATUS_IDLE;
	m_dwStatus2 = TRADE_STATUS_IDLE;
  // if(m_dwStatus1 == TRADE_STATUS_ACCEPT)
		//m_dwStatus1 = TRADE_STATUS_PROPOSAL;

	OUTPUTTIP(TEXTRES_TRADE_MODIFY_CMD);
	theHero.AddUpdateFlag(CHAR_UPDATE_TRADE);
}


VOID	TradeDialogInstance::ProcessPending()
{
    if (!m_bModifyPending)
		 return;

	 switch (m_uiFunctionIndex)
	 {
	 case TRADE_MOD_PENDING_UNKNOWN:
		 break;

	 case TRADE_MOD_PENDING_GET:
		 {
			 GetItemSending(m_modpendingGet.pos);
		 }
		 break;

	 case TRADE_MOD_PENDING_PUT:
		 {
			 PutItemSending(m_modpendingPut.posTrade,m_modpendingPut.pos);
		 }
		 break;

	 case TRADE_MOD_PENDING_PUTMONEY:
		 {
			 PutMoneySending(m_modpendingPutMoney.money);
		 }
		 break;

	 case TRADE_MOD_PENDING_SWAP:
		 {
			 SwapItemSending(m_modpendingSwap.fromPos,m_modpendingSwap.toPos);
		 }
		 break;
	 }

	 m_bModifyPending = false;
}

VOID	TradeDialogInstance::Accept()
{
	if(	m_dwStatus1 >= TRADE_STATUS_PROPOSAL
		&&	m_dwStatus2 >= TRADE_STATUS_PROPOSAL)
	{
		LockTrade();
		MSG_CG_TRADE_ACCEPT_SYN sendMsg;
		theHero.SendPacket( &sendMsg, sizeof (sendMsg), TRUE );
	}
	else
	{
		OUTPUTTIP(TEXTRES_TRADE_INVALID_STATE);
	}
}


VOID	TradeDialogInstance::OnAccept1		()
{
	m_dwStatus1	= TRADE_STATUS_ACCEPT;
	UnlockTrade();
	OUTPUTTIP(TEXTRES_TRADE_ACCEPT_ACK);
}

VOID	TradeDialogInstance::OnAccept2		()
{
	m_dwStatus2	= TRADE_STATUS_ACCEPT;
	UnlockTrade();
	OUTPUTTIP(TEXTRES_TRADE_ACCEPT_CMD);
}

VOID	TradeDialogInstance::OnTradeFinished(MONEY money, sTOTALINFO_TRADE& tradeInfo)
{
	OUTPUTTIP(TEXTRES_TRADE_TRADE_BRD);
	OUTPUTTOP(TEXTRES_TRADE_TRADE_BRD);

	//////////////////////////////////////
	theHero.SetMoney(money);
	BaseContainer*	pContainer;
	pContainer	= theItemManager.GetContainer(SI_INVENTORY);

	/////////////////////////////////////////////
	for (SLOTPOS i=0; i<MAX_TRADE_SLOT_NUM; ++i)
	{
		DummyItemSlot& rSlot = (DummyItemSlot&)m_pTradeContainer->GetSlot(i);

		if (	rSlot.GetCode() == 0 
			|| rSlot.GetOrgSlot() == NULL)
			continue;

		pContainer	= theItemManager.GetContainer(rSlot.GetFromContainerIdx());

		pContainer->DeleteSlot(rSlot.GetFromPosition(), NULL);
	}

	/////////////////////////////////////////////
	for (INT i=0; i<tradeInfo.m_InvenCount; ++i)
	{
		sITEM_SLOTEX& slot = tradeInfo.m_Slot[i];

		ItemSlot itemSlot(slot.m_Stream);
		pContainer->InsertSlot(slot.m_ItemPos, itemSlot);
	}


	//////////////////////////////////////
	theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);

	CloseWindow();
}


VOID	TradeDialogInstance::ProcessError	(DWORD dwError	, BOOL bReset)
{
	if(bReset)
		theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
	theHero.ProcessPlayerError(dwError, CG_TRADE);
}

VOID	TradeDialogInstance::ProcessErrorTip	(DWORD dwErrorTip)
{
	theHero.SetBehaveState(PLAYER_BEHAVE_IDLE);
	OUTPUTTIP(dwErrorTip);
}

//------------------------------------------------------------------------------
BOOL TradeDialogInstance::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat ,BOOL		 bViewContainer)
{
	BaseContainer*	pContainer = (bViewContainer? m_pViewContainer: m_pTradeContainer);

	__CHECK(pContainer->InsertSlot(  posIndex,  slotDat ));

	 if(pContainer->GetSlotListener())
	 {
		 BaseSlot& slot = pContainer->GetSlot(posIndex);
		 pContainer->GetSlotListener()->OnSlotAdd(slot);
	 }

	return TRUE;
}


//------------------------------------------------------------------------------
VOID TradeDialogInstance::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut,BOOL		 bViewContainer )
{
	BaseContainer*	pContainer = (bViewContainer? m_pViewContainer: m_pTradeContainer);

	 if(pContainer->GetSlotListener())
	 {
		 BaseSlot& slot = pContainer->GetSlot(posIndex);
		 pContainer->GetSlotListener()->OnSlotRemove(slot);
	 }
	pContainer->DeleteSlot(  posIndex,  pSlotOut );
}

VOID TradeDialogInstance::NetworkProc( MSG_BASE * /*pMsg*/ )
{
}

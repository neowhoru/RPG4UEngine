/*////////////////////////////////////////////////////////////////////////
文 件 名：LoginScene.cpp
创建日期：2008年3月21日
最后更新：2008年3月21日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "PacketInclude.h"
#include "LoginScene.h"
#include "GameClientApplication.h"
#include "ApplicationSetting.h"
#include "IV3DEngine.h"
#include "SoundLayer.h"
#include "LoginScenePacketHandler.h"
#include "PacketStruct_ClientLogin.h"
#include "KeyQueueManager.h"
#include "Map.h"
#include "NetworkLayer.h"
#include "BGMusicInfoParser.h"
#include "PacketInclude.h"

using namespace input;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(LoginScene, ()  , gamemain::eInstPrioGameFunc);

//GLOBALINST_LOCAL(LoginScenePacketHandler, LoginPacketHandler  , eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
LoginScene::LoginScene()
	:m_bConnectedToAuthServer(false)
	,m_bPendingConfrim(false)
	,m_bReconnect(false)
	,m_bReconnectTimer(false)
	,m_dwReconnectTick(0)
	,m_dwReconnectTickOver(0)
{
	m_LoginStep					= LSTEP_NONE;
	m_dwStepStatus				= 0;
	m_dwFreshTipGroup			= TIPGROUP_LOGIN;
	m_dwMapHandle				= WH_TYPE_LOGIN;
	m_dwUIListener				= gameui::eUISceneLOGIN;
	m_SceneType					= scene::SCENE_TYPE_LOGIN;
	m_bLockCameraDistance	= TRUE;
}

LoginScene::~LoginScene()
{
}


BOOL LoginScene::Init()
{
	//BOOL	bRet = TRUE;

	if(!_SUPER::Init())
		return FALSE;

	//UserGameWorld();

	theLoginScenePacketHandler.Init();
	//return bRet;
	return TRUE;
}

void LoginScene::Release()
{
	theLoginScenePacketHandler.Release();
	Disconnect();

	_SUPER::Release();
}


BOOL LoginScene::OnChangeFrameworkIn()
{
	if(!_SUPER::OnChangeFrameworkIn())
		return FALSE;

	m_bPendingConfrim		= false;
	m_bReconnect			= false;
	m_bReconnectTimer		= false;
	m_dwReconnectTick		= 0;
	m_dwReconnectTickOver= 0;

	ChangeStep();



	///////////////////////////////////////
	theCursorHandler.ForceChange( cursor::CURSORTYPE_DEFAULT );
	if(theGeneralGameParam.IsEnableNetwork())
	{
		Connect();
	}



	theSoundBGM.PlayBGM	(MAP_TYPE_DEFAULT
								,theMap.GetCurrentFieldID()
								,ZONETYPE_LOBBY);

	//theSoundLayer.StopMusic();
	//theSoundLayer.PlayMusic((LPCSTR)theApplicationSetting.m_sLoginSceneMusic
 //                         ,TRUE
	//							  ,FALSE);

	theKeyQueueManager.Reset();
	theKeyQueueManager.SetKey(KEYC_ESCAPE);
	theKeyQueueManager.SetDefaultFun(GlobalUtil::DoLogin);
	return TRUE;
}

BOOL LoginScene::OnChangeFrameworkOut()
{
	if(!_SUPER::OnChangeFrameworkOut())
		return FALSE;

	theSoundLayer.StopMusic();
	//theGameWorld.ReleaseWorkingData();
	//theGameWorld.Shutdown();	

	//SAFE_DELETE(m_pCamera);
	//m_pScene		= NULL;//不可删除

	//在人物列表获取成功后
	//Disconnect();

	return TRUE;
}

void LoginScene::Activate()
{
}

BOOL LoginScene::FrameMove( DWORD dwTick )
{
	_SUPER::FrameMove(dwTick);
	theLoginScenePacketHandler.FrameMove(dwTick);


	//ProcessConnection(dwTick);
	return TRUE;
}



BOOL LoginScene::Render( DWORD dwTick )
{
	if(!_SUPER::Render(dwTick))
		return FALSE;


	return TRUE;
}



void LoginScene::ProcessConnection(DWORD dwTick)
{
	if (m_bReconnectTimer)
	{
		m_dwReconnectTick += dwTick;

		if (m_dwReconnectTick > m_dwReconnectTickOver)
		{
			if (!Connect())
				m_bReconnect			= true;
			else
				m_bReconnectTimer	= false;
		}
	}

	if (m_bReconnect)
	{
		m_dwReconnectTick		= dwTick;
		m_dwReconnectTickOver = dwTick + (1*1000);
		m_bReconnect				= false;
		m_bReconnectTimer		= true;
	}
}


BOOL LoginScene::Connect()
{
	return Reconnect();
}


void LoginScene::Disconnect()
{
	if (theNetwork.IsConnected(CK_LOGINSERVER))
		theNetwork.Disconnect(CK_LOGINSERVER);
	else
		OnDisconnect();
}


void LoginScene::OnDisconnect()
{
	m_bConnectedToAuthServer	= false;
	m_bPendingConfrim				= false;

	theLoginScenePacketHandler.SetNetworkPending(FALSE);

	theLoginScenePacketHandler.OnDisconnect();

	ChangeStep(LSTEP_NOCONNECT);
}


void LoginScene::OnReconnect()
{
	Reconnect();
}

BOOL LoginScene::Reconnect()
{
	//Disconnect();
	if(theNetworkLayer.ReconnectToLoginServer())
	{
		ChangeStep();
	}
	m_bReconnect = true;
	return TRUE;
}

void LoginScene::ChangeStep(eLOGIN_STEP step)
{
	if(!theNetwork.IsConnected(CK_LOGINSERVER))
		step = LSTEP_NOCONNECT;
	else if(theLoginScenePacketHandler.IsAuthFinished())
		step = LSTEP_SERVERLIST;

	//if(m_LoginStep == step)
	//	return;

	BOOL	bShowAccountInput	(FALSE);
	BOOL	bShowServerList	(FALSE);

	////////////////////////////////////////
	switch(step)
	{
	default:
	case LSTEP_NOCONNECT:
		break;
	case LSTEP_ACCOUNT_INPUT:
		{
			bShowAccountInput	= TRUE;
			bShowServerList	= FALSE;
		}break;
	case LSTEP_SERVERLIST:
		{
			bShowAccountInput	= FALSE;
			bShowServerList	= TRUE;
		}break;
	}

	m_LoginStep	= step;

	//////////////////////////////////////
	theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN
											,gameui::eShowAccountInput
											,bShowAccountInput)		;

	theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN
											,gameui::eShowServerList
											,bShowServerList)		;
}

UINT LoginScene::GetGroupInfoSize()
{
	return theLoginScenePacketHandler.GetGroupInfos().size();
}

const SServerGroupInfo* LoginScene::GetGroupInfoAt(INT nAt)
{
	if(nAt < (INT)theLoginScenePacketHandler.GetGroupInfos().size())
		return &theLoginScenePacketHandler.GetGroupInfos()[nAt];
	return NULL;
}


BYTE* LoginScene::GetSerialKey()
{
	return theLoginScenePacketHandler.GetSerialKey();
}

BOOL LoginScene::SendLoginInfo(LPCSTR szAccount, LPCSTR szPasswd)
{
	return theLoginScenePacketHandler.ExecLoginQuery(szAccount, szPasswd);
}
BOOL LoginScene::SelectChannelAt(const SServerChannelInfo& channelInfo)
{
	return theLoginScenePacketHandler.ExecSelectChannel(channelInfo);
}


BOOL LoginScene::CheckSendPacket(int size, char *packet)
{
	if (!m_bPendingConfrim)
	{
		ASSERT( !"未确定数据发送" );
		return FALSE;
	}

	if (!theNetworkLayer.SendPacket(CK_LOGINSERVER, packet, size))
	{
		ASSERT( !"数据发送失败..." );
		Reconnect();
		return FALSE;
	}
	return TRUE;
}


void LoginScene::ParsePacket( MSG_BASE * pMsg )
{
	_SUPER::ParsePacket(pMsg);

	switch( pMsg->m_byCategory )
	{
	case CL_LOGIN:
		{
			//#pragma message(__FILE__  "(297) 此处向 UI 派发 Msg... " )
			//theGameUIManager.UIOnMessage(gameui::eUISceneLOGIN, pMsg);
			theLoginScenePacketHandler.NetworkProc(pMsg);
		}
		break;

	case CG_CONNECTION:
		{
			OnRecvCG_CONNECTION( pMsg );
			return;
		}
		break;
	}
}



/*////////////////////////////////////////////////////////////////////////
文 件 名：VHeroActionInput.cpp
创建日期：2006年11月20日
最后更新：2006年11月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VHeroActionInput.h"
#include "VHeroAutoInput.h"
#include "VObjectManager.h"
#include "VHero.h"
#include "GameUIManager.h"
#include "GameClientApplication.h"
#include "V3DCamera.h"
#include "VPlayer.h"
#include "VUCtrlManager.h"
#include "GameUIManager.h"
#include "VHero.h"
#include "PathGameMap.h"
#include "VMonster.h"
//#include "MonsterType.h"
#include "Map.h"
#include "V3DGameWorld.h"
#include "MapViewManager.h"
#include "GameInPlaying.h"
#include "PacketStruct_ClientGameS_Sync.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace vobject;
GLOBALINST_SINGLETON_IMPL(VHeroActionInput, ()  , gamemain::eInstPrioGameFunc);


namespace vobject
{


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VHeroActionInput::VHeroActionInput()
{


}
VHeroActionInput::~VHeroActionInput()
{
}








void VHeroActionInput::BeginJumping		()
{
	if(	!g_InputData.m_IsJumping 
		&& !g_InputData.m_IsSpacePressed 
		&& !theVHero.IsInWater() 
		//&& theVHero.GetFightStatusDat().IsCanMove() 
		&& g_CurTime - g_InputData.m_SpacePressTime > 618 )
		// && g_CurTime - g_InputData.m_JumpStartTime > 1200 )
	{
		Vector3D vDir(0,0,1.f);

		FLOAT fDistToCollision = 0.0f;

		theVHero.GetCollisionDistance( theVHero.GetPosition(), vDir, &fDistToCollision );
		if( fDistToCollision!=0.0f && fDistToCollision < 80.0f*SCALE_MESH*2 )
		{
			return;
		}


		g_InputData.m_SpacePressTime = g_CurTime;

		g_InputData.m_IsSpacePressed		= TRUE;
		g_InputData.m_IsJumping				= TRUE;
		g_InputData.m_JumpingDir			= g_InputData.m_TargetDirection;
		g_InputData.m_JumpingRotate		= g_InputData.m_RotateZCorrect;
		g_InputData.m_JumpStartTime			= g_CurTime + 100;

		if( theVHero.IsMounting() )
		{
			if( theVHero.IsMoving() )
			{
				if( theVHero.IsCountermarch() )
				{
					g_InputData.m_IsJumping = FALSE;
					g_InputData.m_JumpingDir = g_InputData.m_TargetDirection;
					g_InputData.m_JumpingRotate = g_InputData.m_RotateZCorrect;
					g_InputData.m_JumpStartTime = g_CurTime + 100;
				}
			}
			else
			{
				g_InputData.m_IsJumping = FALSE;
				g_InputData.m_JumpingDir = g_InputData.m_TargetDirection;
				g_InputData.m_JumpingRotate = g_InputData.m_RotateZCorrect;
				g_InputData.m_JumpStartTime = g_CurTime + 100;
			}
		}

		theVHero.Jump();

		////////////////
		//if( theVHero.IsIntonating() )
		//{
		//	Msg_IntonateInterrupt msg;

		//	if( -1 == GetNetworkInput().SendMsg( &msg ) )
		//	{
		//		//断线
		//		UI_MessageBox::Show( "Disconnectted!" );
		//	}
		//	OnMsgIntonateInterrupt();
		//}

		//theVHero.SwitchJumpAnim();

		//UI_Progress::ShowByTime( base::GetRunTime(), 1, true, 1, true, true, "打  断" );
	}
}


void VHeroActionInput::SetJumpingState	(BOOL bState)
{
	g_InputData.m_IsSpacePressed = bState;
}





// 角色的跳跃信息
void	VHeroActionInput::SendJumpMsg()
{
	MSG_CG_SYNC_KBMOVE_SYN Packet;

	Packet.m_byMoveState = KEYBOARDMOVE_JUMP;
	Packet.vCurPos			= theHero.GetPosition();
	Packet.m_wAngle		= (WORD) (theHero.GetAngle() * 180.0f / MATH_PI);
	Packet.m_TileIndex	= theHero.GetPathExplorer()->GetTile();

	theHero.SendPacket		(&Packet,sizeof(Packet),TRUE);
}

// 发送角色拔出武器的消息
void VHeroActionInput::SendDrawWeapon()
{
}

// 发送角色收起武器的消息
void VHeroActionInput::SendDrawInWeapon()
{
}

void VHeroActionInput::DisDrome()
{
}


};//namespace vobject


#include "stdafx.h"
#include "TempInventoryContainer.h"
#include "objectManager.h"
#include "inventoryDialog.h"
#include "SlotUIListener.h"
#include "SlotKeyGenerator.h"
#include "WareHouseDialog.h"


TempInventoryContainer::TempInventoryContainer(void)
{
}


TempInventoryContainer::~TempInventoryContainer(void)
{
	Release();
}


BOOL TempInventoryContainer::Init(SLOTPOS   MaxSlotSize	
											 ,SLOTINDEX SlotIdx)		
{
	//ItemSlotContainer::Init(MAX_TEMPINVENTORY_SLOT_NUM, SI_INVENTORY2);
	__BOOL_SUPER(Init(   MaxSlotSize	, SlotIdx));
	return TRUE;
}


VOID TempInventoryContainer::Release()
{
	_SUPER::Release();
}


BOOL TempInventoryContainer::InsertSlot( SLOTPOS posIndex, BaseSlot & IN slotDat )
{
	ASSERT(IsEmpty(posIndex));

	SERIALTYPE dwSerial = theSlotKeyGenerator.GetKey();
	slotDat.SetSerial(dwSerial);

	__CHECK(BaseContainer::InsertSlot(posIndex, slotDat));

	if(singleton::ExistWareHouseDialog())
		theWareHouseDialog.UpdateSlotListener();

	return TRUE;
}


VOID TempInventoryContainer::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	SERIALTYPE serial = BaseContainer::GetSlot(posIndex).GetSerial();

	theSlotKeyGenerator.Restore(serial);

	_SUPER::DeleteSlot(posIndex, pSlotOut);

	if(singleton::ExistWareHouseDialog())
		theWareHouseDialog.UpdateSlotListener();
}


//----------------------------------------------------------------------------
VOID	TempInventoryContainer::FlushSlots()
{
	for (SLOTPOS i = 0; i < MAX_TEMPINVENTORY_SLOT_NUM; i++)
	{
		if (!IsEmpty(i))
			DeleteSlot(i, NULL);
	}
}
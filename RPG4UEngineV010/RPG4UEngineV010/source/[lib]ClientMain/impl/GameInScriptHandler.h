/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInScriptHandler.h
创建日期：2007年8月2日
最后更新：2007年8月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEINSCRIPTHANDLER_H__
#define __GAMEINSCRIPTHANDLER_H__
#pragma once

#include "Player.h"

_NAMESPACEU(Character,	object);
_NAMESPACEU(Player,		object);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class  GameInScriptHandler
{
public:
	GameInScriptHandler(Character* pChar)
		:m_pCharacter(pChar)
		,m_pPlayer(NULL)
	{
		assert(m_pCharacter);
		if(m_pCharacter->IsKindOfObject(PLAYER_OBJECT))
			m_pPlayer = (Player*)m_pCharacter;
	}
	//~GameInScriptHandler();

public:

#ifndef NONE_NEWGAMEINSCRIPT
/*////////////////////////////////////////////////////////////////////////
Script
/*////////////////////////////////////////////////////////////////////////
public:
	void	RemoveAllEquip	( );				//去除所有装备
	void	SetReputation	( int n, int oprType );
	void	Money				( int nMoney, int oprType );
	void	RemoveAllItem	( );				//去除所有道具
	void	SetExp			( int nExp, int oprType );		// 经验
	void	SetSkillExp		( int nExp, int oprType );// 技能经验

	void	SetVIT	( int attrVal, int oprType );		// 体质
	void	SetSTR	( int attrVal, int oprType );		// 力量
	void	SetDEX	( int attrVal, int oprType );		// 敏捷
	void	SetINT	( int attrVal, int oprType );		// 智力
	void	SetSPR	( int attrVal, int oprType );		// 魅力
	void	SetHP		( int attrVal, int oprType );
	void	SetMP		( int attrVal, int oprType );

	void	SetProfession	( int nProfession );			// 职业
	int	GetProfession	(void);			
	void	SetSex			( int /*nSex*/ ){ }
	int	GetSex			();					



	void	LearnSkill	( LPCSTR szSkillName, INT nLevel );		// 技能
	void	SetBornPoint( DWORD dwMapID, int iPosX, int iPosY );//设置出生点

	int	GetExp		( );		// 经验
	int	GetSkillExp	( );	// 技能经验
	int	GetVIT	( );		// 体质
	int	GetSTR	( );		// 力量
	int	GetDEX	( );		// 敏捷
	int	GetINT	( );		// 智力
	int	GetSPR	( );		// 魅力
	int	GetHP		( );	
	int	GetMP		( );

	int	GetReputation		(){ return 0; }
	BOOL	ExistItem			(LPCSTR szItemName, DWORD dwItemCount);
	BOOL	DeleteItem			(LPCSTR szItemName, DWORD dwItemCount);
	void	AddItem				(LPCSTR szItemName, DWORD dwItemCount);
	int	GetEmptySlotCount	();
	BOOL	IsSkillActived		(SLOTCODE skillCode,LEVELTYPE skillLevel );


	// 任务增强
	DWORD	CancelQuest		( CODETYPE questID , BOOL bDiscard=TRUE);
	DWORD	DoneQuest		( CODETYPE questID );
	void	SetQuestState	( CODETYPE questID, DWORD		state );
	void	GetQuestState	( CODETYPE questID, KEYWORD	varID);
	BOOL	CheckQuestDone	( CODETYPE questID );
	void	CanGetNewQuest	( KEYWORD  varID );

	void	AddMoney			( DWORD dwMoney, int nType );

	void	JumpToMap			( DWORD dwMapID,	INT x, INT y );
#endif

protected:
	VG_PTR_PROPERTY(Character,		Character);
	VG_PTR_PROPERTY(Player,			Player);
};


#include "GameInScriptHandler.inl"
#endif //__GAMEINSCRIPTHANDLER_H__
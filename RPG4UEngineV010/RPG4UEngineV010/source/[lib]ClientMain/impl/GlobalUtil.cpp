/*////////////////////////////////////////////////////////////////////////
文 件 名：GlobalUtil.cpp
创建日期：2008年2月13日
最后更新：2008年2月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GlobalUtil.h"
#include <time.h>
#include "BGMusicInfoParser.h"
#include "LoginScene.h"
#include "ChatDefine.h"
#include "WareHouseDialog.h"
#include "DemoSystemManager.h"
#include "RenderSystemManager.h"
#include "PacketInclude.h"






void GlobalUtil::InitRandom()
{
	srand( GetTickCount() );
}





BOOL GlobalUtil::ChangeFullScreen()
{
	static DWORD	arFrequences[]=
	{
		 185
		,85 
		,80 
		,72 
		,70 
		,60 
	};

	///////////////////////////////////////////
	DEVMODE	dm={0};
	LONG		lResult;
	BOOL		bChangeOk		= FALSE;

	dm.dmSize			= sizeof( DEVMODE );
	dm.dmPelsWidth		= SCREEN_WIDTH;
	dm.dmPelsHeight	= SCREEN_HEIGHT;
	dm.dmBitsPerPel	= 16;
	dm.dmFields			= DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT|DM_DISPLAYFREQUENCY;

	/////////////////////////////////////
	for( UINT i = 0; i < _countof(arFrequences); i++ )
	{
		dm.dmDisplayFrequency = arFrequences[i];
		lResult= ::ChangeDisplaySettings( &dm, CDS_FULLSCREEN );
		if( lResult == DISP_CHANGE_SUCCESSFUL )
		{
			bChangeOk = TRUE;
			break;
		}
	}


	/////////////////////////////////////
	// 如果没有找到合适的全屏显示模式
	// 则用窗口模式
	if( !bChangeOk )
	{
		MessageBox( 0, "Sorry, You can only play this game with window mode!", "", MB_OK );
		//对不起，您的显示器没有合适的全屏模式，只能在窗口模式下进行游戏!", "", MB_OK );
		//GetGameClientApplication().m_bFullScreen = FALSE;
	}
	return bChangeOk;
}


void GlobalUtil::ToggleRenderUI()
{
	g_BaseSetting.m_EnableRenderUI = !g_BaseSetting.m_EnableRenderUI;
}

void GlobalUtil::PrintScreenBmp()
{
	DWORD		time	= base::GetRunTime();
	LPCSTR	szDir = _MEDIAPATH(ePATH_SCREENSHOT);

	CreateDirectory(szDir,NULL);
	GetActiveRender()->PrintScreenAsBmp(FMSTR("%sShot[%08ld].bmp",szDir, time));
}


//----------------------------------------------------------------------------
void GlobalUtil::DeadConfirmInHunting(BOOL bYes)
{
	if (bYes)
	{
		MSG_CG_STATUS_RESURRECTION_SYN	SendResurrectSyn;
		theNetworkLayer.SendGamePacket( &SendResurrectSyn
												, sizeof(SendResurrectSyn)
												, &theHero);
	}
	else
	{
		MSG_CG_ZONE_HUNTING_LEAVE_SYN	SendLeaveSyn;
		theNetworkLayer.SendGamePacket( &SendLeaveSyn
												, sizeof(SendLeaveSyn)
												, &theHero);
	}
}


//----------------------------------------------------------------------------
void GlobalUtil::DeadVerifyInMission(void)
{
	MSG_CG_ZONE_MISSION_LEAVE_SYN	SendLeaveSyn;
	theNetworkLayer.SendGamePacket( &SendLeaveSyn, sizeof(SendLeaveSyn));
}


void GlobalUtil::SystemExitConfirm(BOOL bYes)
{
	if (bYes)
	{
		::SendMessage(g_hWndMain,WM_CLOSE,0,0);
	}
	else
	{
	}
}



void GlobalUtil::DoSystemDialog(DWORD /*wParam*/,DWORD /*lParam*/)
{
	theGameUIManager.UITriggerFunc(gameui::eUIMain
											,gameui::eToggleSystemMenu);
}


void GlobalUtil::DoAutoAttack(DWORD /*wParam*/,DWORD /*lParam*/)
{
	theHeroActionInput.SwitchAutoAttack(FALSE,FALSE);
}


void GlobalUtil::DoAutoRun(DWORD /*wParam*/,DWORD /*lParam*/)
{
	theHeroActionInput.SwitchAutoMove(FALSE,FALSE);
}

void GlobalUtil::DoAreaSkill(DWORD /*wParam*/,DWORD /*lParam*/)
{
	theHeroActionInput.SwitchAreaSkill(FALSE);
}

void GlobalUtil::DoTarget(DWORD /*wParam*/,DWORD /*lParam*/)
{
	theHeroActionInput.SetCurrentTarget(0);
	
}

void GlobalUtil::DoLogin(DWORD /*wParam*/,DWORD /*lParam*/)
{
	//theGameUIManager.HideUI(UI_LOGIN_BACK);
	//theGameUIManager.HideUI(UI_LOGIN_ACCOUNT);
	//theGameUIManager.HideUI(UI_LOGIN_SERVER_LIST);

	//theGameUIManager.ShowUI(UI_LOGIN_BACK);
	//theGameUIManager.ShowUI(UI_LOGIN_ACCOUNT);
	//theLoginScene.Reconnect();
}


void GlobalUtil::DoDragItem(DWORD /*wParam*/,DWORD /*lParam*/)
{
	theMouseHandler.CancelHandlingItem();
}


void GlobalUtil::DoShowWindow(DWORD wParam,DWORD /*lParam*/)
{
	ContainerDialogBase* pDlg= (ContainerDialogBase*)wParam;

	if(pDlg)
		pDlg->CloseWindow();
}




void GlobalUtil::DoBank(DWORD /*wParam*/,DWORD /*lParam*/)
{
	WareHouseDialog* pWareHouse;
	pWareHouse = (WareHouseDialog*)theHero.GetSlotContainer(SI_WAREHOUSE);
	if(pWareHouse)
	{
		pWareHouse->SendWareHouseEnd();
	}
}

void GlobalUtil::DoTrade(DWORD /*wParam*/,DWORD /*lParam*/)
{
	//uiTrade* pDlg = (uiTrade *)theGameUIManager.GetDialog(InterfaceManager::DIALOG_TRADE);

	//if (pDlg)
	//{
	//	tradeMan->SendTradeCancel();
	//	pDlg->ShowInterface(FALSE);
	//}
}




/*////////////////////////////////////////////////////////////////////////
文 件 名：HeroActionInputInstance.cpp
创建日期：2008年5月21日
最后更新：2008年5月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "HeroActionInputInstance.h"
#include "Monster.h"
#include "Item.h"
#include "MouseHandler.h"
#include <SkillInfoParser.h>
#include "MapNPC.h"
#include "SummonSkillContainer.h"
#include "MapObject.h"
#include "MapViewManager.h"
#include "GlobalUtil.h"
#include "VHero.h"
#include "WareHouseDialog.h"
#include "ShopDialog.h"
#include "ItemCompoundDialog.h"
#include "ApplicationSetting.h"
#include "WorldConst.h"

#include "QuickPaneSlot.h"
#include "Camera.h"
#include "WareHouseDialog.h"
#include "HeroVoiceInfoParser.h"
#include "SoundEffect.h"
#include "quickcontainer.h"
#include "HeroSoundLayer.h"
#include "HeroMouseInputLayer.h"
#include "HeroAssistantLayer.h"
#include "HeroQueueActionLayer.h"
#include "HeroKeyboardInputLayer.h"
#include "HeroArcherKeyboardInputLayer.h"
#include "HeroNecromancerKeyboardInputLayer.h"
#include "HeroTipLayer.h"
#include "SceneDefine.h"
#include "TileWorldClient.h"
#include "SkillStorageManager.h"
#include "TipLogManager.h"
#include "ConstTextRes.h"
#include "PacketInclude.h"

using namespace tile;
using namespace input;
using namespace scene;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(HeroActionInputInstance, ()  , gamemain::eInstPrioPlayerRole);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------
HeroActionInputInstance::HeroActionInputInstance(void)
{


	InstallKeyboardLayer(NULL,PLAYERTYPE_MAX);
}

//------------------------------------------------------------------------------
HeroActionInputInstance::~HeroActionInputInstance(void)
{
}


BOOL HeroActionInputInstance::Init()
{
	__BOOL_SUPER(Init());
	theSkillQueueManager.ClearAll();

	return TRUE;
}

void HeroActionInputInstance::Release()
{
	_SUPER::Release();
}




//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::FrameMove(DWORD dwTick)
{
	m_pSoundLayer->ProcessVoiceDelay(dwTick);
	
	if (	!singleton::ExistHero() ) 
		return TRUE;


	if(m_pKeyboardLayer)
	{
		m_pKeyboardLayer->ProcessQuickKey();
		m_pKeyboardLayer->ProcessOptionKey();
	}

	m_iMoveDelay -= dwTick;
	m_iMoveDelay = math::Max( 0, m_iMoveDelay);

	if(m_pAssistantLayer)
		m_pAssistantLayer->ProcessFollow();


	if (theApplicationSetting.m_ShowAdvancedMiniMap) 
		return TRUE;

	ProcessCamera();

	/// 连击处理
	ProcessComboDelay(dwTick);

	ProcessTargeting();
   ProcessPicking();

	/////////////////////////////////////////
	if (singleton::ExistHero())
	{
		if (theHero.CannotMove())
		{
			theHero.ResetMoveFlag();
			SwitchAutoMove(FALSE);
			SetFollowState( FALSE );
		}

		if(theHero.IsLockInput())
		{
			if(m_pMouseLayer)
				m_pMouseLayer->ProcessMouse();
			ProcessCursor();
			return TRUE;
		}
	}


	//////////////////////////////////////////
	if(!IsShootingMode() )
	{
		theHero.LockOn(0);

		if(m_pAssistantLayer)
			m_pAssistantLayer->ProcessAutoAttack();

		if(m_pKeyboardLayer)
		{
			m_pKeyboardLayer->ProcessKeyboardAttack();

			if(m_iMoveDelay<=0)
			{
				m_bHeroKeyboardMove = m_pKeyboardLayer->ProcessKeyboardMove();
			}
		}//if(m_pKeyboardLayer)
		
		///////////////////////////////
		if(m_pMouseLayer)
			m_pMouseLayer->ProcessMouse();


      ProcessAssistTarget();
		ProcessQueueAction();


		///////////////////////////////////////////////
		if( m_iMoveDelay <= 0 && m_bAutoMove )
		{
			InitAction();

			if (IsIdleState() || theHero.IsCurrentState(STATE_MOVE) )
			{
				theHero.StopMove();
				theHero.SetNextState(STATE_KEYBOARDMOVE,g_CurTime);
			}
			theHero.AddMoveFlag( KEYBOARDMOVE_FOWARD );
		}

	}//if(!IsShootingMode() )
	else
	{
		if(m_pAssistantLayer)
			m_pAssistantLayer->ProcessAutoAttack();

		if(m_iMoveDelay<=0)
		{
			if(m_pKeyboardLayer)
				m_pKeyboardLayer->ProcessKeyboardMove();
		}

		if(m_pMouseLayer)
			m_pMouseLayer->ProcessStabberMouse();


		//////////////////////////////////////////////
      ProcessAssistTarget();
		ProcessQueueAction();

		if( m_iMoveDelay <= 0 && m_bAutoMove )
		{
			InitAction();

			if (IsIdleState() || theHero.IsCurrentState(STATE_MOVE))
			{
				theHero.StopMove();
				theHero.SetNextState(STATE_KEYBOARDMOVE, g_CurTime);
			}
			theHero.AddMoveFlag( KEYBOARDMOVE_FOWARD );
		}


		if (GetCurrentTarget())
		{
			Character *pTarget = (Character *)theObjectManager.GetObject( GetCurrentTarget() );
			if (pTarget)
			{
				Vector3D vTarget			= pTarget->GetPosition();
				Vector3D vPos				= theHero.GetPosition();
				Vector3D vDiff				= vTarget - vPos;
				float		fTargetAngle	= vDiff.GetAngleByACos();
				float		fAngle			= theHero.GetAngle();

				float fDiff				= abs(fTargetAngle - fAngle);
				float fAnotherDiff	= MATH_PI * 2.0f - fDiff;

				// 45度
				if (min(fDiff,fAnotherDiff) > MATH_PI / 4.0f )
				{
					SwitchAutoAttack(FALSE);
					//SetCurrentTarget( 0 );
					theHero.LockOn( 0 );
				}
				else
				{
					theHero.LockOn( GetCurrentTarget() );
				}
			}
		}
		//	theHero.LockOn( GetCurrentTarget() );
	}	



	///////////////////////////////////////////////////////////////
	Vector3D				pickPos;	
	

	if(	m_bAreaSkill == FALSE
		||	GameUtil::GetClickedPosition(&pickPos) == FALSE)
	{
		m_bMouseInRange	= FALSE;
		theGameUIManager.SetAreaPointer(FALSE);
	}
	else
	{
		sSKILLINFO_BASE *	pInfo;
		Vector3D				vDistance;
	   float					fDistance;
		
		pInfo			= theSkillInfoParser.GetSkillInfo((SLOTCODE)m_dwCurrentSkillID);
		vDistance	= pickPos - theHero.GetPosition();
		fDistance	= vDistance.Length();

		m_vDest	= pickPos; 

		if( fDistance <= (float)pInfo->m_wSkillRange * SCALE_SKILLRANGE_FACTOR)
		{
			m_bMouseInRange	= TRUE;
			theGameUIManager.SetAreaPointer	(TRUE
                                          ,m_vDest
														,pInfo->m_wSkillArea/10
														,COLOR_RGBA(255, 0,0, 255));		
		}
		else
		{
			m_bMouseInRange	= FALSE;
			theGameUIManager.SetAreaPointer	(FALSE);	
		}

	}


	ProcessCursor();

	EndCamera();
	return TRUE;
}


//------------------------------------------------------------------------------
float HeroActionInputInstance::GetActionDistance(PLAYER_ACTION action)
{
	switch (action.ActionID)
	{
	case ACTION_ATTACK:
			return GetAttackActionDistance();

	case ACTION_PLAYERMEET:
	case ACTION_NPCMEET:
	case ACTION_VENDORMEET:
			return theGeneralGameParam.GetDistanceNpcMeet();

	case ACTION_GETITEM:
			return theGeneralGameParam.GetDistanceNpcMeet();

	case ACTION_STABBERSHOT:
			return GetAttackActionDistance();

	case ACTION_SKILL:
		{
			sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)action.SKILL.dwSkillID);
			ASSERT(pInfo);
			if (pInfo->m_byTarget == SKILL_TARGET_ME)
				return math::cINFINITE;

			return ((float)pInfo->m_wSkillRange * SCALE_SKILLRANGE_FACTOR);
		}
		break;
	}
	return 0.0f;
}


//------------------------------------------------------------------------------
void HeroActionInputInstance::SetComboReset()
{
	m_iComboCount = 0;
	m_iComboDelay = 0;

	if(singleton::ExistHero())
		theHero.SetComboCount(0);
	
	//SwitchAutoAttack(FALSE);
	SwitchAutoMove(FALSE);
	SetFollowState( FALSE );
}

//------------------------------------------------------------------------------
void HeroActionInputInstance::SwitchAreaSkill(BOOL bFlag,BOOL bCheck)
{
	if(bCheck && m_bAreaSkill != bFlag)
	{
		if(bFlag)
		{
			theKeyQueueManager.PushBack	(ESC_PROC_AREA_POINTER
													,GlobalUtil::DoAreaSkill);
		}
		else
		{
			theSkillQueueManager.PopFront();
			theKeyQueueManager.DeleteMsg(ESC_PROC_AREA_POINTER);
		}
	}

	m_bAreaSkill = bFlag;
}


//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::DoSkillProcessing(DWORD dwSkillid)
{
	sSKILLINFO_BASE *pInfo;
	
	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillid);

	__CHECK_PTR(pInfo);

	BOOL bRet=FALSE;

	switch(pInfo->m_bySkillUserType)
	{
	case SKILLUSER_PLAYER:
		bRet = PushSkill(dwSkillid);
		break;

	case SKILLUSER_SUMMONED:
		bRet = UseSummonSkill(dwSkillid);
		break;

	case SKILLUSER_ACTION:
		if(m_pAssistantLayer)
			bRet = m_pAssistantLayer->ProcessUserAction( dwSkillid );
		break;

	case SKILLUSER_EMOTICON:
		if(m_pAssistantLayer)
			bRet = m_pAssistantLayer->ProcessUserEmotion( dwSkillid );
		break;

	default:
		ASSERT(!"未能处理的技能类型");
		break;
	}

	return bRet;
}

//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::UseSummonSkill(DWORD dwSkillid)
{
	DWORD					dwID;
	Monster *			pMon;
	DWORD					dwTargetId;
	sSKILLINFO_BASE *	pInfo;
	Character *			pTarget;
	

	dwID	= theHero.GetSummonId();

	if(dwID == INVALID_DWORD_ID)
	{
		OUTPUTCHAT(TEXTRES_SUMMON_MONSTER);	
		return FALSE;
	}

	pMon			= (Monster *)theObjectManager.GetObject(dwID);
	__CHECK_PTR	(pMon);

	dwTargetId	= GetCurrentTarget();

	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillid);

	__VERIFY_PTR(pInfo, "Skill invalid!");
	
	__CHECK(pMon->IsMonster());


	if(	pInfo->m_byTarget == SKILL_TARGET_ME
		||	pInfo->m_byTarget == SKILL_TARGET_REACHABLE_ME)
	{
		dwTargetId=dwID;
	}

	
	pTarget=(Character *)theObjectManager.GetObject(dwTargetId);

	if(	pInfo->m_byTarget != SKILL_TARGET_ME
		&&	pInfo->m_byTarget != SKILL_TARGET_REACHABLE_ME)
	{
		if (!CanBeSkillTarget(dwSkillid,pTarget))
		{				
			OUTPUTCHAT(TEXTRES_SKILL_FAIL_NOT_PROPER_TARGET);	
			return FALSE;
		}
	}

	if (	pTarget
		&&	pMon
		&&	pMon->CanUseSkill(dwSkillid,TRUE))
	{
		MSG_CG_SUMMON_SKILL_ACTION_SYN	Packet;


		Packet.m_dwSummonedObjKey=dwID;	
		Packet.m_SkillCode		= (SLOTCODE)dwSkillid;		
		Packet.m_dwClientSerial	= pMon->GetCurrentAttackSerial();	
		Packet.m_vCurPos			= pMon->GetPosition();			
		Packet.m_vDestPos			= pMon->GetPosition();		
		Packet.m_dwMainTargetKey= dwTargetId;	
		Packet.m_MainTargetPos= pTarget->GetPosition();	

		__CHECK(theHero.SendPacket( &Packet, sizeof(MSG_CG_SUMMON_SKILL_ACTION_SYN) ) );

		return TRUE;
	}

	return FALSE;
}

//----------------------------------------------------------------------------
BOOL HeroActionInputInstance::UseStyle(DWORD dwSkillid)
{
	sSTYLEINFO_BASE * pStyleInfo;
	
	pStyleInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)dwSkillid);
	__CHECK_PTR	(pStyleInfo);


	if(theHero.GetCurrentAttackStyle() == dwSkillid )
	{
		OUTPUTTIP(TEXTRES_NAMECODE_SET_STYLE_SUCCESS);
		return TRUE;
	}

	__CHECK(theHero.CheckAttackStyle(dwSkillid, TRUE));

	MSG_CG_STYLE_SELECT_STYLE_SYN		packet;
	packet.m_NewStyleCode			= (SLOTCODE)dwSkillid;

	return theHero.SendPacket(&packet,sizeof(packet));
}


//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::PushSkill(DWORD dwSkillid)
{
	BOOL bRet;
	
	bRet = theSkillQueueManager.PushBack(dwSkillid);

	if(!bRet)
	{
		if(m_bAreaSkill) 
		{
			m_bAreaSkill = FALSE;
			theKeyQueueManager.DeleteMsg(ESC_PROC_AREA_POINTER);
		}
	}

	return bRet;
}



//------------------------------------------------------------------------------
DWORD HeroActionInputInstance::GetSkillInfo(DWORD dwClassId,BOOL & bActive,float &fDistance)
{
	int nSlotNum;
	
	nSlotNum = theSkillStorageManager.GetSkillNumActive1();

	for(INT j=0; j<nSlotNum; j++)
	{
		const SkillStorageInfo *pInfo = theSkillStorageManager.GetSkillActive1(j);

		if(!pInfo || !pInfo->pCurrSkillInfo)
			continue;

		if(dwClassId != pInfo->pCurrSkillInfo->m_SkillClassCode)
			continue;

		if(pInfo->state == SLOT_UISTATE_ACTIVATED)
			bActive=TRUE;
		else 
			bActive=FALSE;	

		if (theGeneralGameParam.GetSpecialMode()) 
			bActive=TRUE;

		if(pInfo->pCurrSkillInfo->IsSkill())
			fDistance = (float)((sSKILLINFO_BASE*)pInfo->pCurrSkillInfo)->m_wSkillRange * SCALE_SKILLRANGE_FACTOR;

		return pInfo->pCurrSkillInfo->m_SkillCode;
	}//for(int j=0; j<nSlotNum; j++)


	///////////////////////////////////////////////////
	nSlotNum = theSkillStorageManager.GetSkillNumActive2();
	for(int j=0; j<nSlotNum; j++)
	{
		const SkillStorageInfo *pInfo = theSkillStorageManager.GetSkillActive2(j);

		if(!pInfo || !pInfo->pCurrSkillInfo)
			continue;

		if(dwClassId != pInfo->pCurrSkillInfo->m_SkillClassCode)
			continue;

		if(pInfo->state == SLOT_UISTATE_ACTIVATED)
			bActive=TRUE;
		else 
			bActive=FALSE;	

		//if (theGeneralGameParam.GetSpecialMode()) 
		//	//bActive=TRUE;

		if(pInfo->pCurrSkillInfo->IsSkill())
			fDistance = (float)((sSKILLINFO_BASE*)pInfo->pCurrSkillInfo)->m_wSkillRange * SCALE_SKILLRANGE_FACTOR;

		return pInfo->pCurrSkillInfo->m_SkillCode;
	}//for(int j=0; j<nSlotNum; j++)
	return 0;
}



//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::CanUseSpecialMove()
{
	if (theGeneralGameParam.GetSpecialMode()) 
		return TRUE;

	return TRUE;
}



//------------------------------------------------------------------------------
Vector3D HeroActionInputInstance::GetCanMovePos(const VECTOR3D& vDest)
{
	Vector3D vSrc;
	Vector3D vTemp;
	float		fRate=0.0f;
	
	vSrc = theHero.GetPosition();


	////////////////////////////////////
	for(int i=0;i<10;i++)
	{
		vTemp =  vDest* (1.0f - fRate) +  vSrc* (fRate);
		
		if(GameUtil::IsMove(vSrc,vTemp))
		{
			return vTemp;
		}

		fRate+=0.1f;
	}

	return vSrc;
}



//------------------------------------------------------------------------------
VOID HeroActionInputInstance::InitAction()
{
	ZeroMemory(&m_QueueAction,sizeof(m_QueueAction));	
	m_QueueAction.ActionID = ACTION_IDLE;

	if(!IsShootingMode()  )
	{
		SwitchAutoAttack(FALSE);	
	}

	m_bKeyboardQueueAttack = FALSE;
}





//------------------------------------------------------------------------------
DWORD HeroActionInputInstance::GetCurrentTarget()
{
	return m_dwSelectedTarget;
}


//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::MoveToAction(const VECTOR3D& vDest,int /*iTileNum*/)
{
	if (!theGameOption.GetAutoMove())
	{
		if (m_QueueAction.ActionID == ACTION_ATTACK)
			return FALSE;
	}
	//m_bHeroActionAtServerSync

	if( IsIdleState() || theHero.IsCurrentState(STATE_MOVE)  )
	{
		STATE_TYPE eOldNextState = theHero.GetNextState();

		if(theHero.SetNextState( STATE_MOVE,g_CurTime ))
		{
			BOOL bMoveResult = FALSE;
			
			if (!theHero.CannotMove())
			{
				bMoveResult = theHero.Move	(theHero.GetPosition()
                                       ,vDest
													,MOVETYPE_RUN
													,FALSE
													,-1
													,-1);
			}
			
			if (bMoveResult)
			{
				if (	m_QueueAction.ActionID == ACTION_ATTACK 
					&& !theHero.IsNormalRangedAttack())
				{
					MSG_CG_SYNC_TARGET_MOVE_SYN SendPacket;
					SendPacket.m_dwKey			= theGeneralGameParam.GetUserID();
					SendPacket.m_dwTargetKey	= m_QueueAction.ATTACK.dwTargetID;

					if( theHero.MakePathPacket( &SendPacket ) == FALSE)
					{
						ASSERT(!"寻路失败...");
					}
					else
					{
						theHero.SendPacket( &SendPacket, sizeof(MSG_CG_SYNC_TARGET_MOVE_SYN) );
					}
				}
				else
				{
					MSG_CG_SYNC_MOVE_SYN	SendPacket;
					SendPacket.m_dwKey		= theGeneralGameParam.GetUserID();

					if( theHero.MakePathPacket( &SendPacket ) == FALSE)
					{
						ASSERT(!"寻路失败");
					}
					else
					{
						theHero.SendPacket( &SendPacket, sizeof(MSG_CG_SYNC_MOVE_SYN) );
					}
				}
				return TRUE;
			}
			else
			{
				m_QueueAction.ActionID = ACTION_IDLE;
				if (eOldNextState != STATE_KEYBOARDMOVE)
				{
					theHero.SetNextState( STATE_IDLE,g_CurTime );
				}
				else
				{
					theHero.SetNextState( eOldNextState,g_CurTime );
				}
				//return TRUE;
			}
		}
	}

	return FALSE;
}


//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::IsShootingMode()
{
	if (theHero.GetWeaponKind() == ITEMTYPE_CROSSBOW )
	{
		//return TRUE;
	}
	
	return FALSE;	
}

//------------------------------------------------------------------------------
void HeroActionInputInstance::SetCurrentTarget(DWORD dwID,BOOL bCheck)
{
	if(bCheck)
	{
		if(dwID)
		{
			if(m_dwSelectedTarget == 0)
			{
				theKeyQueueManager.PushBack(ESC_PROC_TARGET
													,GlobalUtil::DoTarget);
			}
		}
		else
		{
			//if(m_dwSelectedTarget!=dwID)
			{
				g_InputData.m_IsAttackLockTarget = FALSE;
				theKeyQueueManager.DeleteMsg(ESC_PROC_TARGET);
			}
		}
	}

	///////////////////////////////////////////
	Character *pChar;

	m_dwSelectedTarget = dwID;
	
	pChar = (Character *)theObjectManager.GetObject( m_dwSelectedTarget );
	if( pChar )
	{
		m_TargetType = GameUtil::GetRelationOfHero( pChar );
	}
	else
	{
		m_TargetType = RELATION_NONE;
	}


	if( singleton::ExistHero() )
	{
		theHero.m_TabTargetInfo.dwObjectKey = dwID;
		if( IsShootingMode() )
		{
			theHero.LockOn( m_dwSelectedTarget );
		}
	}

	if(bCheck)
	{
		theGameUIManager.UISetData	(gameui::eUITarget
											,gameui::eSetPlayerID
											,m_dwSelectedTarget);
	}
}


//////////////////////////////////////////////////////////////////
void	HeroActionInputInstance::RefreshCurrentTarget	()
{
	if(m_dwSelectedTarget == 0)
		return;
	theGameUIManager.UIRefresh	(gameui::eUITarget);
}


//////////////////////////////////////////////////////////////////
BOOL HeroActionInputInstance::ProcessCamera()
{
	BOOL bUpdate		= FALSE;
	//BOOL bEndCamera	= FALSE;

	const		sMOUSE_INFO& info	= theInputLayer.GetMouseBuffer();

	if(	theGeneralGameParam.GetSpecialMode()
		&&	theGeneralGameParam.IsFreeCameraState())
	{
		if(m_pMouseLayer)
			bUpdate |= m_pMouseLayer->ProcessCamera(info,TRUE);
		if(m_pKeyboardLayer)
			bUpdate |= m_pKeyboardLayer->ProcessCamera(TRUE);

		//if(bUpdate)
		{
			theCamera.SetCamera( m_vFreeCameraPos, FALSE );
			theCamera.UpdateListeners();
		}
	}
	else
	{
		Vector3D vDirection = theHero.GetDirection();	
		theCamera.SetAngle( &vDirection );

		if (IsCameraDetech())
		{
			theCamera.SetYaw(theHero.GetAngle());
			theCamera.Rotate(0,-50);	
			SetCameraDetech(FALSE);
		}

		if(m_pMouseLayer)
		{
			if(m_pMouseLayer->ProcessCamera(info))
			{
				bUpdate	|= theInputLayer.HaveMouseState();
			}
		}

		if(m_pKeyboardLayer)
			bUpdate |= m_pKeyboardLayer->ProcessCamera();

		if(!bUpdate && theHero.IsMoving() )
		{
			if( theGeneralGameParam.GetCameraRotation() )
			{
				theCamera.InterpolateAngle();
				bUpdate = TRUE;
			}
		}


		//if(bUpdate)
		{


			#pragma message(__FILE__  "(899)  theCamera.SetCamera " )

		}
	}


	//////////////////////////////////////////////////
	if(	bUpdate 
		&& singleton::ExistHero())
	{
		if(theCamera.IsCloseToLookAt(theGeneralGameParam.GetCameraAlphaDistance()/**SCALE_MESH*/ ))
		{
			theHero.SetAlpha(0.f);
		}
		else
		{
			theHero.SetAlpha(1.f);
		}
	}

	return bUpdate;
}

///////////////////////////////////////////////////////////////////////
void HeroActionInputInstance::EndCamera()
{
	///这个用来处理淡出动画
	//theCamera.Rotate(0,0);
	//theCamera.SetRotateState( FALSE );
	//theCamera.RotateAngleCheck( 0 );
	//theCamera.SetStartRotateCheck( FALSE );

	//theCamera.SetCamera( &(theHero.GetVisiblePos()), TRUE );	
	//theCamera.UpdateListeners();
}



///////////////////////////////////////////////////////////////////////
BOOL HeroActionInputInstance::UseSpecialMoveSkill( DWORD /*dwSkillid*/)
{
	sSKILLINFO_BASE *pInfo=NULL;

	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)m_dwCurrentSkillID);

	__CHECK_PTR(pInfo);

	__CHECK (!theHero.CannotMove());


	Vector3D vDistance = m_vDest - theHero.GetPosition();
	float		fDistance = vDistance.Length();

	__CHECK (fDistance <= (float)pInfo->m_wSkillRange * SCALE_SKILLRANGE_FACTOR);

	__CHECK (GameUtil::IsMove(theHero.GetPosition(),m_vDest));

	__CHECK (theHero.CanUseSkill(m_dwCurrentSkillID,TRUE));


	////////////////////////////////////////////////////
	PLAYER_ACTION skillaction;
	ZeroMemory(&skillaction,sizeof(skillaction));

	skillaction.ActionID				= ACTION_SKILL;
	skillaction.SKILL.dwSkillID	= m_dwCurrentSkillID;
	skillaction.SKILL.dwTargetID	= GetCurrentTarget();
	skillaction.SKILL.vTargePos	= m_vDest;

	m_QueueAction = skillaction;

	if (theHero.CanDoQueueAction(&m_QueueAction)) 
	{
		if (IsShootingMode())
		{
			__CHECK (theHero.GetStabberShotDelay() <= 30);
		}

		theHero.DoAction(&m_QueueAction);
	}

	return TRUE;
}


BOOL HeroActionInputInstance::UseAreaSkill()
{
	sSKILLINFO_BASE *pInfo=NULL;

	if(!m_bPicking) 
		return  TRUE;


	__VERIFY(m_bAreaSkill, "Must in AreaSkill mode");

	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)m_dwCurrentSkillID);

	__VERIFY_PTR(pInfo, "Skill is NULL");


	if (pInfo->m_SkillClassCode == SKILLCODE_TELEPORT)
	{
		__CHECK(!theHero.CannotMove());
	}


	if(GameUtil::IsMove(theHero.GetPosition(),m_vDest) == FALSE)
	{
		OUTPUTCHAT(TEXTRES_INVALIDAREA);
		return TRUE;
	}



	/////////////////////////////////////////////////////
	Vector3D vDistance = m_vDest - theHero.GetPosition();
	float		fDistance = vDistance.Length();

	if( fDistance > (float)pInfo->m_wSkillRange * SCALE_SKILLRANGE_FACTOR)
	{
		OUTPUTCHAT(TEXTRES_INVALIDDISTANCE );
		return TRUE;
	}


	/////////////////////////////////////////////////////
	if (!theHero.CanUseSkill(m_dwCurrentSkillID,TRUE))
	{
		OUTPUTCHAT(TEXTRES_INVALIDSKILL );
		return FALSE;
	}


	/////////////////////////////////////////////////////
	PLAYER_ACTION skillaction;
	ZeroMemory(&skillaction,sizeof(skillaction));
	skillaction.ActionID				= ACTION_SKILL;
	skillaction.SKILL.dwSkillID	= m_dwCurrentSkillID;
	skillaction.SKILL.dwTargetID	= GetCurrentTarget();
	skillaction.SKILL.vTargePos	= m_vDest;
	
	m_QueueAction = skillaction;
		
	return TRUE;
}






BOOL HeroActionInputInstance::UseSkill(DWORD skillid)
{
	__CHECK(!IsCannotAttack());


	if (singleton::ExistHero())
	{
		//if(theHero.GetCurrentState()==STATE_SKILL)
		//	return	TRUE;
		__CHECK(!theHero.IsLockInput());

		__CHECK(!theHero.CannotUseSkill());
	}


	if( m_bAutoMove == TRUE )
	{
		if( singleton::ExistHero() )
		{
			SwitchAutoMove			(FALSE);
			SetFollowState			( FALSE );
			theHero.StopMove		();
			theHero.StopAtServer	();
		}
	}

	sSKILLINFO_BASE *pInfo;
	
	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillid);
	__VERIFY_PTR(pInfo
					, FMSTR("技能不可为NULL %d",skillid));


	////////////////////////////////////
	/// @不可移动时，不能发技能
	switch(pInfo->m_byTarget)
	{
	case SKILL_TARGET_REACHABLE_ENEMY:
	case SKILL_TARGET_REACHABLE_FRIEND:
	case SKILL_TARGET_REACHABLE_ME:
		{
			if (theHero.CannotMove())
				return FALSE;
		}
		break;
	}

	///////////////////////////////////////////////
	/// @不可移动时，不能使用瞬移
	if (pInfo->m_SkillClassCode == SKILLCODE_TELEPORT)
	{
		__CHECK(!theHero.CannotMove());
	}

	__CHECK(theHero.CanUseSkill(skillid,TRUE));


	///////////////////////////////////////////////
	Character *	pTarget		= NULL;
	DWORD			dwTargetID	= GetCurrentTarget();

	/// @ 选择攻击目标
	if (pInfo->m_byTarget == SKILL_TARGET_FRIEND)
	{
		if (dwTargetID == 0)
			dwTargetID = theHero.GetObjectKey();
	}
	else if(pInfo->m_byTarget == SKILL_TARGET_SUMMON)
	{
		dwTargetID =0;
	}

	pTarget = (Character *)theObjectManager.GetObject(dwTargetID);

	//SwitchAreaSkill(FALSE);
	m_dwCurrentSkillID	=	skillid;


	////////////////////////////////////
	/// @准备技能动作
	PLAYER_ACTION skillaction;
	ZeroMemory(&skillaction,sizeof(skillaction));
	skillaction.ActionID				= ACTION_SKILL;
	skillaction.SKILL.dwSkillID	= m_dwCurrentSkillID;
	skillaction.SKILL.dwTargetID	= dwTargetID;


	////////////////////////////////////
	/// @处理 NonStopSkill技能
	if (pInfo->IsNonStopSkill())
	{
		/// @发送技能消息
		MSG_CG_SKILL_ACTION_SYN	Packet;
		Packet.m_SkillCode		= (SLOTCODE)skillid;

		Packet.m_vCurPos			= theHero.GetPosition();	
		Packet.m_vDestPos			= theHero.GetPosition();		
		Packet.m_dwClientSerial = theHero.GetCurrentAttackSerial();


		/// 处理技能相关信息
		if (pInfo->m_byAttRangeform == SKILL_TARGET_NONE)
		{
			Packet.m_dwMainTargetKey = theHero.GetObjectKey();
			Packet.m_MainTargetPos = theHero.GetPosition();
		}
		else
		{
			if (pInfo->m_byTarget == SKILL_TARGET_ME)
			{
				Packet.m_dwMainTargetKey = theHero.GetObjectKey();
				Packet.m_MainTargetPos = theHero.GetPosition();
			}
			else if( pInfo->m_byTarget ==SKILL_TARGET_AREA)
			{
				ASSERT("Target  Nonstop not support  SKILL_TARGET_AREA!");
			}
			else
			{
				if (pTarget)
				{

					Packet.m_dwMainTargetKey = pTarget->GetObjectKey();					
					Packet.m_MainTargetPos = pTarget->GetPosition();
				}
				else
				{
					return FALSE;
				}

				if (pInfo->m_byAttRangeform == SKILLAREA_PIERCE)
				{

					Vector3D vDir	= theHero.GetDirection();
					Vector3D vDest = theHero.GetPosition() + vDir * (pInfo->m_wSkillRange * SCALE_SKILLRANGE_FACTOR);

					Packet.m_MainTargetPos = vDest;
				}
			}
		}


		theHero.SendPacket(&Packet,sizeof(MSG_CG_SKILL_ACTION_SYN));	
		return FALSE;
	}//if (pInfo->IsNonStopSkill())


	//////////////////////////////////////
	// @处理自身技能 或召唤技能
	if (	pInfo->m_byTarget == SKILL_TARGET_ME
		||	pInfo->m_byTarget == SKILL_TARGET_SUMMON)
	{
		m_QueueAction = skillaction;
		return TRUE;
	}

	//////////////////////////////////////
	// @处理普通技能
	/// .检测是否存在目标
	if (!pTarget) 
	{				
		OUTPUTCHAT(TEXTRES_SKILL_FAIL_NO_TARGET);
		m_pSoundLayer->PlayNoTargetSound();
		return FALSE;
	}

	/// .检测目标是否可被攻击
	if (!CanBeSkillTarget(m_dwCurrentSkillID,pTarget))
	{				
		OUTPUTCHAT(TEXTRES_SKILL_FAIL_NOT_PROPER_TARGET);	
		return FALSE;
	}

	m_QueueAction = skillaction;

	Vector3D vDest			= pTarget->GetPosition();
	Vector3D vDistance	= vDest - theHero.GetPosition();

	float fDistance = vDistance.Length();

	/// .检测目标是否在技能范围内
	if( fDistance > GetActionDistance(m_QueueAction) )
	{
		if (theGameOption.GetAutoMove())
		{
			MoveToAction(vDest,pTarget->GetPathExplorer()->GetTile());		
		}
		else
		{
			OUTPUTCHAT( TEXTRES_TOO_FAR);
			m_pSoundLayer->PlayTargetTooFarSound();
			return FALSE;
		}
	}
	return TRUE;	
}


////////////////////////////////////////////////////////////
BOOL    HeroActionInputInstance::CanBeSkillTarget(DWORD dwSkillID,Character *pTarget)
{
	__CHECK_PTR(pTarget);

	sSKILLINFO_BASE *pInfo;
	
	pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillID);
	__VERIFY_PTR(pInfo,"Skill Is NULL");


	switch(pInfo->m_byTarget)
	{	
		///////////////////////////////////////////////
	case SKILL_TARGET_ENEMY    :
	case SKILL_TARGET_REACHABLE_ENEMY	:
		{
			__CHECK(pTarget->CanBeAttackTarget());

			eRELATION_TYPE type = GameUtil::GetRelationOfHero(pTarget);

			return (type == RELATION_ENEMY);
		}
		break;

		///////////////////////////////////////////////
	case SKILL_TARGET_FRIEND   :
	case SKILL_TARGET_REACHABLE_FRIEND :
		{
			__CHECK(pTarget->CanBeAttackTarget());

			eRELATION_TYPE type = GameUtil::GetRelationOfHero(pTarget);

			return (type == RELATION_FRIEND);
		}
		break;	

		///////////////////////////////////////////////
	case SKILL_TARGET_FRIEND_CORPSE :
		{
			__CHECK(pTarget->IsDead());

			eRELATION_TYPE type = GameUtil::GetRelationOfHero(pTarget);

			return (type == RELATION_FRIEND);
		}
		break;	

		///////////////////////////////////////////////
	case SKILL_TARGET_ME       :
	case SKILL_TARGET_REACHABLE_ME:
	case SKILL_TARGET_AREA     :
		{
			ASSERT(!"CanBeSkillTarget() not support!");
			return FALSE;
		}
		break;

		///////////////////////////////////////////////
	default:
		{
			ASSERT(!"CanBeSkillTarget() not support");
			return FALSE;
		}
		break;
	}
	//return FALSE;
}


BOOL	HeroActionInputInstance::IsAttackSkillArea()
{
    return m_bAreaSkill;
}

//------------------------------------------------------------------------------
void HeroActionInputInstance::ProcessAssistTarget()
{
}


float HeroActionInputInstance::GetAttackActionDistance()
{

	const float SWIPE_RANGE = 0.2f;

	if (!theHero.IsNormalRangedAttack())
	{
		return theHero.GetAttackRange() + SWIPE_RANGE;
	}
	else
	{
		return theHero.GetAttackRange();
	}
	
}


void HeroActionInputInstance::ProcessComboDelay(DWORD dwTick)
{
	if (m_iComboDelay > 0)
	{
		if (!theHero.IsCurrentState(STATE_ATTACK)  )
		{
			m_iComboDelay -= dwTick;
		}
		if (m_iComboDelay <= 0)
		{
			m_iComboDelay = 0;
			m_iComboCount = 0;
			theHero.SetComboCount(0);
		}
	}

	if (m_iKeyboardQueueAttackDelay > 0)
	{
		m_iKeyboardQueueAttackDelay -= dwTick;

		if (m_iKeyboardQueueAttackDelay <= 0)
		{
			m_iKeyboardQueueAttackDelay = 0;
			m_bKeyboardQueueAttack = FALSE;
		}
	}
}


/////////////////////////////////////////////////////////////////
void HeroActionInputInstance::ProcessTargeting()
{
	/// 处理目标
	if ( GetCurrentTarget())
	{
		Character *pTarget;
		
		pTarget = (Character *)theObjectManager.GetObject( GetCurrentTarget());
		if (!pTarget)
		{
			SetCurrentTarget(0);
		}
		else 
		{
			if (pTarget->IsMonster()) 
			{
				if (pTarget->IsCurrentState(STATE_DEATH) )
				{
					SetCurrentTarget(0);
				}
			}
		}
	}

}


//----------------------------------------------------------------------------
void HeroActionInputInstance::ProcessCursor()
{
	if (theHeroActionInput.IsAttackSkillArea())
	{
		theCursorHandler.ChangeTo	(m_bMouseInRange? cursor::CURSORTYPE_MAGIC_READY : cursor::CURSORTYPE_MAGIC_INVALID);
		return;
	}

	if(theMouseHandler.UpdateCursor())
		return;

	///////////////////////////////////////////////////
	if ( !theInputLayer.IsUIMouseInput() )
	{
		Object *			pPickedObject;
		eCURSOR_TYPE	cursorType	 = CURSORTYPE_DEFAULT;
		
		pPickedObject = theHeroActionInput.GetPickedObject();

		if (pPickedObject)
		{
			BOOL bResult = GameUtil::UpdateCursorForTarget( pPickedObject );
			if ( bResult )
				return;

			cursorType = cursor::CURSORTYPE_CANCLE;
		}
		else
		{
			if ( !theHeroActionInput.IsCanMoveToPickedPos() )
				cursorType = cursor::CURSORTYPE_CANT_GOTO_TARGET;
			else
				cursorType = cursor::CURSORTYPE_CANCLE;
		}

		theCursorHandler.ChangeTo( cursorType );
	}
}


//----------------------------------------------------------------------------
void HeroActionInputInstance::ProcessPicking()
{
	if(m_pMouseLayer)
		m_dwPickedTarget = 	m_pMouseLayer->ProcessPicking();
}


Character *HeroActionInputInstance::GetPickedCharacter()
{
	Object *pObject = theObjectManager.GetObject(m_dwPickedTarget);
	if (pObject)
	{
		if (pObject->IsKindOfObject(CHARACTER_OBJECT))
		{
			return (Character *)pObject;
		}
	}

	return NULL;
	
}

Item *HeroActionInputInstance::GetPickedItem()
{
	Object *pObject = theObjectManager.GetObject(m_dwPickedTarget);
	if (pObject) 
	{
		if (pObject->IsKindOfObject(ITEM_OBJECT))
		{
			return (Item *)pObject;
		}
	}

	return NULL;
	
}

MapObject *HeroActionInputInstance::GetPickedMapObject()
{
	Object *pObject = theObjectManager.GetObject(m_dwPickedTarget);
	if (pObject)
	{
		if (pObject->IsKindOfObject(MAP_OBJECT))
		{
			return (MapObject *)pObject;
		}
	}

	return NULL;
}


Object *HeroActionInputInstance::GetPickedObject()
{
	Object *pObject = theObjectManager.GetObject(m_dwPickedTarget);

	return pObject;
}


void HeroActionInputInstance::StabberShot()
{
	if (singleton::ExistHero()) 
	{
		if(	theHero.GetCurrentState()	!= STATE_SPECIALMOVE
			&&	theHero.GetNextState()	!= STATE_SPECIALMOVE
			&&	theSkillQueueManager.Size()==0)
		{
			if (theHero.GetBulletCount() > 0)
			{
				theHero.DecreaseBulletCount();

				MSG_CG_BATTLE_VKR_ATTACK_SYN packet;
				packet.dwTargetKey		=  GetCurrentTarget();
				packet.dwClientSerial	= theHero.GetNextAttackSerial();
				theHero.SendPacket(&packet,sizeof(packet));

				theHero.StabberShot(packet.dwClientSerial);
			}
			else
			{
				StabberReload();			
			}
		}
	}
}

void HeroActionInputInstance::StabberReload()
{
	if (!IsShootingMode())
		return;

	BOOL	PlayAnimation = TRUE;
	switch(theHero.GetCurrentState())
	{
	case STATE_SIT:
	case STATE_EMOTICON:
		PlayAnimation = FALSE;
		break;

	case STATE_MOVE:
	case STATE_IDLE:
	case STATE_KEYBOARDMOVE:
		PlayAnimation = TRUE;
		break;

	default:
		return;
		break;
	}

	if (theHero.GetStabberShotDelay() > (STABBER_SHOT_DELAY_MARGIN - 5))
		return;

	MSG_CG_BATTLE_VKR_RELOAD_SYN packet;
	theHero.SendPacket(&packet,sizeof(packet));

	theHero.StabberReload( PlayAnimation );
	theHero.SetBulletCount(20);
}


void HeroActionInputInstance::ToggleAdvancedMap()
{
	theApplicationSetting.m_ShowAdvancedMiniMap = !theApplicationSetting.m_ShowAdvancedMiniMap;
	if( theApplicationSetting.m_ShowAdvancedMiniMap )
	{
		theMapViewManager.RealeaseSecondLV();
		theHero.StopMove();
	}
	else
		theMapViewManager.SetMaxMapLevel(TRUE);
}


void HeroActionInputInstance::ToggleAttackState()
{
	static DWORD dwToggleTimer = 0;
	const DWORD TOGGLE_ATTACK_DELAY	= 400;
	if( g_CurTime - dwToggleTimer < TOGGLE_ATTACK_DELAY )
		return;

	dwToggleTimer = g_CurTime;

	if( theVHero.HasFightFlag(FIGHTFLAG_FIGHTING) )
	{
		if( theVHero.ClearFightFlag( FIGHTFLAG_FIGHTING ) )
		{
			LOGINFO("发送收起武器信息\n");
			//MsgDrawInWeapon msg;
			//msg.header.stID = theVHero.GetID();
			//SendDrawInWeapon();
		}
	}
	else
	{
		if( !theVHero.IsMounting() )
		{
			if( theVHero.AddFightFlag( FIGHTFLAG_FIGHTING ) )
			{
				LOGINFO("发送拔出武器信息\n");
				//MsgDrawWeapon msg;
				//msg.header.stID = theVHero.GetID();
				//SendDrawWeapon();
			}
		}
	}
}



DWORD HeroActionInputInstance::FindNearestTarget()
{
	assert(0 && " HeroActionInputInstance..FindNearestTarget ");
#pragma message(__FILE__  "(6718) HeroActionInputInstance..FindNearestTarget  " )

	DWORD dwTarget = 0;
	//DWORD dwPickingOption = PICKING_IGNORE_CORPSE;

	//if (m_bIgnorePlayer)
	//{
	//	dwPickingOption |= PICKING_IGNORE_FRIEND;
	//}

	//Ray ray;

	//ray.m_Origin = theHero.GetVisiblePos();
	//ray.m_Origin.z += 1;
	//ray.m_Direction = theHero.GetDirection();

	//if (g_pMap)
	//{
	//	Character *pChr = theMap.GetPickedCharacter(&ray,dwPickingOption);
	//	if (pChr)
	//	{
	//		dwTarget = pChr->GetObjectKey();
	//	}

	//}
	return dwTarget;
}


BOOL HeroActionInputInstance::AttackNearestTarget()
{
	if( theHero.IsDead() && m_bAutoAttack )
		return FALSE;

	__CHECK (!IsCannotAttack());

	switch(theHero.GetCurrentState())
	{
	case STATE_IDLE:
	case STATE_SIT:
	case STATE_EMOTICON:
	case STATE_MOVE:
	case STATE_KEYBOARDMOVE:
		break;
	case STATE_ATTACK:
		{
			m_bKeyboardQueueAttack			= TRUE;
			m_iKeyboardQueueAttackDelay	= 100;
			return FALSE;
		}
		break;
	default:
		return FALSE;
		break;

	}
	
	if ( m_bAreaSkill == FALSE )
	{
		PLAYER_ACTION action;
		ZeroMemory(&action,sizeof(action));
		

		DWORD dwTarget = FindNearestTarget();		
					
		if( dwTarget )
		{
			Character *pTarget;
			
			pTarget = (Character *)theObjectManager.GetObject(dwTarget);

			action.ActionID				= ACTION_ATTACK;
			action.ATTACK.dwTargetID	= dwTarget;

			m_QueueAction					= action;		
			m_bKeyboardQueueAttack		= FALSE;

			///////////////////////////////////////////
			Vector3D vTarget = pTarget->GetPosition();
			Vector3D vOffset = theHero.GetPosition() - vTarget;

			float fDistance = vOffset.Length();

			if( fDistance > GetActionDistance(m_QueueAction) )
			{
				MoveToAction(vTarget,pTarget->GetPathExplorer()->GetTile());					
			}
			return TRUE;
		}
	}
	return FALSE;
}

void    HeroActionInputInstance::SwitchCanntAttack(BOOL bFlag)
{
	m_bCannotAttack = bFlag;

	if (m_bCannotAttack)
	{
		SwitchAutoAttack(FALSE);
	}
}


//------------------------------------------------------------------------------
DWORD HeroActionInputInstance::IncreaseComboCount()
{
	m_iComboCount++;

	DWORD dwCurStyle = theHero.GetCurrentAttackStyle();

	sSTYLEINFO_BASE *pInfo;
	
	pInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)dwCurStyle);

	if(pInfo==NULL || theHero.IsNormalRangedAttack())
	{
		if (m_iComboCount >= 2)
		{
			m_iComboCount = 0;
		}
	}
	//else if(pInfo->m_byStyleCheck==FALSE)
	//{
	//	if (m_iComboCount >= 2)
	//	{
	//		m_iComboCount = 0;

	//	}
	//}
	else
	{
		if (m_iComboCount >= 3)
		{
			m_iComboCount = 0;
		}
	}

	return m_iComboCount;
}


//------------------------------------------------------------------------------
void HeroActionInputInstance::SwitchAutoAttack(BOOL bFlag,BOOL bCheck)
{
	if(bCheck && m_bAutoAttack!=bFlag)
	{
		if(bFlag)
		{
			//OUTPUTTIP(TEXTRES_PLAYER_AUTOATTACK_ON);
			theKeyQueueManager.PushBack(ESC_PROC_AUTO_ATTACK
												,GlobalUtil::DoAutoAttack);
		}
		else
		{
			//OUTPUTTIP(TEXTRES_PLAYER_AUTOATTACK_OFF);
			theKeyQueueManager.DeleteMsg(ESC_PROC_AUTO_ATTACK);
		}
	}

	m_bAutoAttack = bFlag;
	//LOGINFO("SwitchAutoAttack %d\n",bFlag);
	if( m_bAutoAttack )
	{
		SetFollowState( FALSE );
	}
}


//------------------------------------------------------------------------------
void HeroActionInputInstance::SwitchAutoMove(BOOL bFlag,BOOL bCheck)
{
	if(bCheck && m_bAutoMove!=bFlag)
	{
		if(bFlag)
		{
			OUTPUTTIP(TEXTRES_PLAYER_AUTORUN_ON);
			theKeyQueueManager.PushBack(ESC_PROC_AUTO_RUN, GlobalUtil::DoAutoRun);
		}
		else
		{
			OUTPUTTIP(TEXTRES_PLAYER_AUTORUN_OFF);
			theKeyQueueManager.DeleteMsg(ESC_PROC_AUTO_RUN);
		}
	}

	m_bAutoMove	= bFlag;
}


//------------------------------------------------------------------------------
BOOL HeroActionInputInstance::IsIdleState( void )
{
	//if( g_pHero )
	{
		return(	theHero.IsCurrentState(STATE_IDLE )
				|| theHero.IsCurrentState(STATE_SIT ) 
				|| theHero.IsCurrentState(STATE_EMOTICON)  );
	}

	//return FALSE;
}



/*////////////////////////////////////////////////////////////////////////
文 件 名：.cpp
创建日期：2006年11月15日
最后更新：2006年11月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUILuaFuns.h"
#include "LogSystem.h"
#include "GameUIManager.h"
#include "GameMainUtil.h"
#include "GraphicSetting.h"
#include "VUILayoutManager.h"
#include "VUCtrlObject.h"
#include "VUIObjectManager.h"
#include "VUCtrlFrame.h"
#include "GameClientLuaFuns.h"
#include "VHero.h"
#include "Hero.h"
#include "PlayerAttributes.h"
#include "HeroActionInput.h"
#include "ObjectManager.h"
#include "PacketInclude.h"


using namespace vobject;
#define USE_GAMECLIENT



//////////////////////////////////////////////////////////////////////////
//UI
#ifdef USE_GAMECLIENT
namespace gamemain
{

/////////////////////////////////////////////////////
int UISetVisible		(lua_State * L);
int UIGetPosition		(lua_State * L);
int UIIsVisible		(lua_State * L);
int UISetPosition		(lua_State * L);
int UISetImage			(lua_State * L );
int UIMsgBox			(lua_State * L );
int UITipBox			(lua_State * L );
int UISwitchChatBox	(lua_State * L );
int UIGetChatType		(lua_State * L);

/////////////////////////////////////////////////////
int GetTargetInfo			(lua_State *	L);
int RunGameCommand		(lua_State *	L );
int SendChatText			(lua_State *	L );
int ProcessKeyEvent		(lua_State *	L );
int GetHeroAttr			(lua_State *	L );
#endif


#define _LUA_FUNC(Name)			{#Name,		Name	}

/////////////////////////////////////////////////////
sLUA_SCRIPT_FUNCTION g_LuaScriptFunctions[] = 
{
#ifdef USE_GAMECLIENT
	 _LUA_FUNC(UISetVisible		)	
	,_LUA_FUNC(UISetPosition	)
	,_LUA_FUNC(UIGetPosition	)
	,_LUA_FUNC(UIIsVisible		)
	,_LUA_FUNC(UISetImage		)
	,_LUA_FUNC(UIMsgBox			)
	,_LUA_FUNC(UITipBox			)
	,_LUA_FUNC(UISwitchChatBox	)
	,_LUA_FUNC(GetTargetInfo	)
	,_LUA_FUNC(UIGetChatType	)
	,_LUA_FUNC(RunGameCommand	)
	,_LUA_FUNC(SendChatText		)
	,_LUA_FUNC(ProcessKeyEvent	)
	,_LUA_FUNC(GetHeroAttr	)
#endif

};



#ifdef USE_GAMECLIENT

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL InitGameClientLuaFuns()
{
	LauAppendScriptFunctions(g_LuaScriptFunctions, sizeof(g_LuaScriptFunctions)/sizeof(sLUA_SCRIPT_FUNCTION));
	return TRUE;
}

#endif




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

#ifdef USE_GAMECLIENT

//////////////////////////////////////////////////////////////////////////
//	获得Hero基本属性
int GetHeroAttr( lua_State *L )
{
	__CHECK2(singleton::ExistHero(), 0);

	int nRetVal = 0;
	PlayerAttributes*	pAttr = theHero.GetPlayerAttributes();

	LPCSTR szData = lua_tostring(L, 1);
	string szBuffer = szData;

	if( szBuffer == "DEX" )
	{
		nRetVal = pAttr->GetDEX();
	}
	else if( szBuffer == "CRE" )
	{
		nRetVal = pAttr->GetCRE();
	}
	else if( szBuffer == "INT" )
	{
		nRetVal = pAttr->GetINT();
	}
	else if( szBuffer == "VIT" )
	{
		nRetVal = pAttr->GetVIT();
	}
	else if( szBuffer == "STR" )
	{
		nRetVal = pAttr->GetSTR();
	}
	else if( szBuffer == "LEVEL" )
	{
		nRetVal = theHero.GetLevel();
	}
	else if( szBuffer == "SEX" )
	{
		nRetVal = theHero.GetSex();
	}
	else if( szBuffer == "PLAYERTYPE" )
	{
		nRetVal = theHero.GetClass();
	}
	lua_pushnumber( L, nRetVal );

	return 1;
}


////////////////////////////////////////////////////////////////////////
 
int UITipBox ( lua_State *L )
{
	LPCSTR szData = lua_tostring(L, 1);
	if( szData )
	{
		theHero.DisplayChatMessage(szData);
	}

	return 0;
}


////////////////////////////////////////////////////////////////////////
//SendMsg
int SendChatText( lua_State *L )
{
	__UNUSED(L);
#pragma message(__FILE__  "(179) 这里调用Chat SendChatText  " )

	int		nType		= (int)lua_tonumber(L, 1);
	LPCSTR	szData	= lua_tostring(L, 2);

	__UNUSED(nType);


	//if(nRet == -1)
	//switch(nType)
	//{
	//}
	BOOL bRet(FALSE);

	///////////////////////////////////
	switch(nType)
	{
	case gameui::CHAT_TYPE_NORMAL:
		{
			MSG_CW_CHAT_VILLAGE_SYN	msgPublic;
			msgPublic.m_ChatPlayer	= theHero.GetObjectKey();
			msgPublic.m_byMsgLength	= (BYTE)(lstrlen(szData)+1 );
			lstrcpyn	(msgPublic.m_pszChatMsg
						,szData
						,MSG_CW_CHAT_VILLAGE_SYN::_MAX_CHATMSG_SIZE);

			bRet = theNetworkLayer.SendChatPacket(&msgPublic, sizeof(msgPublic));
		}break;
	}

	///////////////////////////////////
//#ifndef _DEBUG
	if(bRet || !theGeneralGameParam.IsEnableNetwork())
//#endif
	{
		OUTPUTCHAT	(szData
                  ,&theHero
						,NULL
						,gameui::CHAT_TYPE_NORMAL);

		//theVHero.DisplayChat(szData);
		//theGameUIManager.UIDisplayChatInfo(szData
  //                                      ,_COLOR(COLOR_CHAT_SHOUT)
		//											 ,nType
		//											 ,theVHero.GetName());
		//theGameUIManager.UIDisplayChatInfo("未处理Net连接 (SendChatText)...\n");
	}

	return 0;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int UIGetChatType( lua_State * L)
{
	int nType(0);
	theGameUIManager.UIGetData	(gameui::eUIChatInfoBox
										,gameui::eGetCurrentChannel
										,&nType);
	lua_pushnumber( L, nType );
	return 1;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int RunGameCommand(  lua_State * L )
{
	string sText;
	sText.substr();
	if (lua_gettop(L) != 1)
		return 0;

	LPCSTR szComp = lua_tostring(L, 1);
	if( szComp )
		GameUtil::TryGameCommond( szComp );

	return 1;
}

////////////////////////////////////////////////////////////////////////
 //获得 LockObj的信息
 //return Type, Name, level...
int GetTargetInfo( lua_State * L)
{
	int nType = 0;
	int nLevel = 0;
	char szName[256] = {0};

	DWORD		dwTarget = theHeroActionInput.GetCurrentTarget();
	Object*	pObject	= theObjectManager.GetObject(dwTarget);
	if( pObject )
	{
		nType		= 0;
		nLevel	= 0;
		strncpy( szName, pObject->GetName(),  256 );
		if(pObject->IsKindOfObject(CHARACTER_OBJECT))
		{
			nLevel	= ((Character*)pObject)->GetLevel();
		}
	}
	else
	{
		strcpy( szName, "NULL" );
	}

	lua_pushnumber( L, nType );
	lua_pushstring( L, szName );
	lua_pushnumber( L, nLevel );
	return 3;
}


int UISwitchChatBox(lua_State *L )
{
	__UNUSED(L);

	theGameUIManager.UITriggerFunc(gameui::eUIChatInfoBox
											,gameui::eAutoSwitchChat);
	return 1;
}


////////////////////////////////////////////////////////////////////////
 //Param : nType, visable
 //return x , y
int UISetVisible(lua_State * L)
{
	int		nType;
	LPCTSTR	szType;
	BOOL		bVisible;

	szType	= lua_tostring(L, 1);
	bVisible	= (int)lua_tonumber(L, 2);
	nType		= GetGameUITypeValue(szType);

	theGameUIManager.UISetVisible((gameui::EGameUIType)nType,bVisible);


	return 0;
}

//////////////////////////////////////////////////////////////////////////
// Param : nType
// return x , y
int UIGetPosition(lua_State * L)
{
	int x = 0,
		y = 0;
	int nType = (int)lua_tonumber(L, 1);
	VUCtrlObject* pObject;
	
	pObject = theUIObjectManager.GetUIObject( (eUIOBJ_TYPE)nType );
	if( pObject )
	{
		RECT rt = {0};
		pObject->GetGlobalRect(&rt);
		x = rt.left;
		y = rt.top;
	}

	lua_pushnumber( L, x );
	lua_pushnumber( L, y );
	return 2;
}

//////////////////////////////////////////////////////////////////////////
// Param : nType
// return bVis
int UIIsVisible(lua_State * L)
{
	BOOL		bVisible = 0;

	int		nType;
	LPCTSTR	szType;
	szType	= lua_tostring(L, 1);
	nType		= GetGameUITypeValue(szType);

	bVisible = theGameUIManager.UIIsVisible((gameui::EGameUIType)nType);
	lua_pushnumber( L, bVisible );


	return 1;
}


////////////////////////////////////////////////////////////////////////
 //Param : nType, x, y
//
int UISetPosition(lua_State * L)
{
	if (lua_gettop(L) != 3)
		return 0;

	int nType	= (int)lua_tonumber(L, 1);
	int x			= (int)lua_tonumber(L, 2);
	int y			= (int)lua_tonumber(L, 3);
	if( nType < 0 || nType > UIOBJ_MAX )
		return 0;

	printf( "%d %d", x, y );

	VUCtrlObject* pObject;
	
	pObject = theUIObjectManager.GetUIObject( (eUIOBJ_TYPE)nType );
	if( pObject )
	{
		pObject->SetPos( x, y );
	}
	return 0;
}
////////////////////////////////////////////////////////////////////////
	//UI sMAP_ICONINFO oper
 // Param :  nType, string szPath

int UISetImage( lua_State * L )
{
	if (lua_gettop(L) != 2 )
		return 0;

	int		nType		= (int)lua_tonumber(L, 1);
	LPCSTR	szPath	= lua_tostring( L, 2 );

	VUCtrlObject* pObject;
	
	pObject = theUIObjectManager.GetUIObject( (eUIOBJ_TYPE)nType );
	if( pObject )
	{
		pObject->SetPicInfo( szPath );
	}
	return 2;
}

//////////////////////////////////////////////////////////////////////////
// UI MessageBox
// Param : 
int UIMsgBox( lua_State * L )
{
	if (lua_gettop(L) != 2 )
		return 0;

	int		nType	= (int)lua_tonumber(L, 1);
	LPCSTR	szMsg = lua_tostring( L, 2 );

	__UNUSED(nType);

	if( szMsg )
	{
		theGameUIManager.UIMessageBox(szMsg);
	}

	return 0;
}

int ProcessKeyEvent( lua_State *L )
{
	int nType = (int)lua_tonumber(L, 1);
	__UNUSED(nType);
	//GetGameClientApplication().OnChangeControlType(nType);
	return 0;
}

};
#endif

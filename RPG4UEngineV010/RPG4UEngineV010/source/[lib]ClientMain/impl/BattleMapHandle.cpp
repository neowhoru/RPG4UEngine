/*////////////////////////////////////////////////////////////////////////
文 件 名：BattleMapHandle.cpp
创建日期：2006年10月25日
最后更新：2006年10月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BattleMapHandle.h"
#include "VObjectManager.h"
#include "VHero.h"
#include "V3DGameWorld.h"
#include "V3DConfig.h"
#include "V3DScene.h"
#include "VObjectManager.h"
#include "VObject.h"
#include "VHeroAutoInput.h"
#include "ISkillSpecialEffect.h"
#include "VUCtrlManager.h"
#include "VUICursorManager.h"
#include "ObjectUtil.h"
#include "VObjectSystem.h"
#include "ObjectSystem.h"
#include "PathGameMap.h"
#include "GameEffectManager.h"
#include "VCharacterAnimationCtrl.h"
#include "ApplicationSetting.h"
#include "RenderSystem.h"
#include "RenderSystemManager.h"
#include "VItem.h"
#include "VMonster.h"
#include "VHeroActionInput.h"
#include "VHeroAutoInput.h"
#include "HeroActionInput.h"
#include "Camera.h"
#include "V3DTerrainLightManager.h"
#include "V3DTerrainRender.h"


namespace vobject
{
extern Vector3 g_arCollisionVertices[];
extern Vector3 g_CollisionRay[];
};

using namespace vobject;
using namespace gamemain;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BattleMapHandle::BattleMapHandle()
{
	m_bRenderTask	= TRUE;
	UseGameWorld();
}
BattleMapHandle::~BattleMapHandle()
{
}

WH_TYPE BattleMapHandle::GetSystemType()			
{
	return WH_TYPE_BATTLE;
}

BOOL BattleMapHandle::Init()			
{
	__BOOL_SUPER(Init());
	return TRUE;
}

void BattleMapHandle::Release()		
{
	_SUPER::Release();
}

BOOL BattleMapHandle::RequestWorkingData()	
{
	__BOOL_SUPER(RequestWorkingData());

	return TRUE;
}

void BattleMapHandle::ReleaseWorkingData()
{
	_SUPER::ReleaseWorkingData();
}


BOOL BattleMapHandle::FrameMove(DWORD dwTick)
{
	//__BOOL_SUPER(FrameMove(dwTick));
   theCamera.FrameMove(dwTick);
	BuildSceneFrustum();

	__BOOL_CALL (RunGameMain(g_CurTime));

	theObjectSystem.FrameMove(dwTick);
	theVObjectSystem.FrameMove(dwTick);

	return TRUE;
}


BOOL BattleMapHandle::ReadyRender(DWORD dwTick)	
{
	//__BOOL_SUPER(Render(dwTick)); 不调用SUPER
	m_pRenderer = the3DEngine.GetRenderer();

	m_pRenderer->SetViewport(&GetViewPortMain());

	//////////////////////////////////////////
	//用雾色清除背景
	sFOG_WORLRD_INFO *pFog;
	
	


	/////////////////////////////////////////////////
	BOOL bUpDateNight = FALSE;
	bUpDateNight = UpdateFog( g_V3DConfig.IsShowFog() );

	//if( !bUpDateNight )
	if( bUpDateNight )
	{
		V3DGameMap* pLevel = theGameWorld.GetHeroPlotMap();
		if( pLevel )
		{
			V3DScene* pScene = pLevel->GetScene();
			//m_pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );
			if( pScene )
				pScene->UpdateDayLightRender();
		}			
	}

	/////////////////////////////////////////////////
	pFog= &theGameWorld.GetFogData();
	m_pRenderer->Clear	(0L
                        ,D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER
								,COLOR_RGBA(pFog->iFogColorR,pFog->iFogColorG,pFog->iFogColorB,255)
								,1.0f
								,0L);

	/////////////////////////////////////////////////
	RenderEnvironment(dwTick);


	return TRUE;
}


BOOL BattleMapHandle::Render(DWORD dwTick)	
{
	//__BOOL_SUPER(Render(dwTick));

	//RenderCollision(dwTick);
	__BOOL_CALL (RenderGameMain(g_CurTime));

#pragma message(__FILE__  "(133)  theObjectSystem.Render 有待调整位置 " )

	//theObjectSystem.Render(dwTick); 目前不需Render
	//theVObjectSystem.Render(dwTick); 在DisplayGameMain调用了

	RenderCollision(dwTick);
	return TRUE;
}

BOOL BattleMapHandle::UpdateFog(BOOL bEnabled)
{
	BOOL bUpDateNight = FALSE;
	if(bEnabled)
	{
		m_pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );
		if( theGameWorld.UpdateDayNightAnimation() )
		{
			bUpDateNight = TRUE;
		}
		//m_pRenderer->SetRenderState( D3DRS_FOGEND,    FtoDW(theApplicationSetting.m_FarFog) );
	}
	else
	{
		m_pRenderer->SetRenderState( D3DRS_FOGENABLE, FALSE );
	}
	return bUpDateNight;
}


INT  BattleMapHandle::AttachMapData()
{
	return ATTACH_OK;
}

void BattleMapHandle::BuildSceneFrustum()
{
	_SUPER::BuildSceneFrustum();

}


BOOL BattleMapHandle::RenderEnvironment(DWORD dwTick)
{
	if(	theApplicationSetting.m_EnableRenderSkyBox
		&& theGameWorld.GetHeroPlotMap() )
	{
		float fFov		= theCamera.GetFOV();
		float fNear		= theCamera.GetNearClip();
		float fFar		= theCamera.GetFarClip();
		float fAspect0	= theCamera.GetAspect();
		float fAspect	= (float)SCREEN_WIDTH / (float)SCREEN_HEIGHT;

		//m_pRenderer->SetRenderState( D3DRS_FOGENABLE,		FALSE );
		m_pRenderer->SetRenderState( D3DRS_ZWRITEENABLE,	FALSE );

		theCamera.SetProjection(1.0f,30000.0f,theApplicationSetting.m_CamerFOV,fAspect);

		//m_pRenderer->SetRenderState( D3DRS_FOGENABLE,	 FALSE );
		//m_pRenderer->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );

		//渲染天空...
		Vector3 vTilePos = Vector3(0.f);
		Vector3 heroPos(0.f);

		GetWorldPos(vTilePos, heroPos);
		//if(singleton::ExistVHero())
		//	heroPos = theVHero.GetPosition();
		heroPos.z = theGameWorld.GetSkyBoxHeight() * SCALE_MESH;
		theGameWorld.RenderSky(dwTick, heroPos );


		//m_pRenderer->SetRenderState( D3DRS_FOGENABLE,		TRUE );
		m_pRenderer->SetRenderState( D3DRS_ZWRITEENABLE,	TRUE );

		theCamera.SetProjection(fNear,fFar,fFov,fAspect0);
	}	
	return TRUE;
}

BOOL BattleMapHandle::GetWorldPos	(Vector3& vTilePos,Vector3& heroPos)
{
	__CHECK(	singleton::ExistHero());
	__CHECK(	theHero.IsEnterWorld());

	vTilePos.x	= (float)theVHero.GetTilePos().x; 
	vTilePos.y	= (float)theVHero.GetTilePos().y; 
	heroPos		= theVHero.GetPosition();

	return TRUE;
}

BOOL BattleMapHandle::OnUpdateWorldPos	()
{
	__CHECK_PTR( theGameWorld.GetHeroPlotMap() );
	__CHECK(	g_V3DConfig.IsRenderWorld() );

	V3DFrustum* pFrustum = (V3DFrustum*)GetSceneFrustum();

	///////////////////////////////////
	Ray  ray;
	ray.m_Origin		=  GetCamera()->GetEyePt();
	ray.m_Direction	=  GetCamera()->GetLookatPt();
	ray.m_Direction	-= GetCamera()->GetEyePt();
	ray.m_Direction.Normalize();

	///////////////////////////////////
	Vector3 vTilePos = Vector3(0.f);
	Vector3 heroPos(0.f);

	GetWorldPos(vTilePos, heroPos);
	//if(singleton::ExistVHero())

	//地图更新
	if( theGameWorld.Update(g_CurTime
                     ,heroPos
							,vTilePos
							,ray
							,pFrustum))	
	{
		V3DTerrain *pTerrain = theGameWorld.GetHeroPlotMap()->GetTerrain();
		if( pTerrain )
		{
			//ON_FUNC_PROFILER(pTerrain Error);
			thePathGameMap.SetBaseTileXY ( pTerrain->GetBaseTileX(), pTerrain->GetBaseTileY() );
			thePathGameMap.SelectWalkMask( pTerrain->GetWalkMask(),  pTerrain );				
			;
		}
	}

	return TRUE;
}

BOOL BattleMapHandle::RunGameMain( DWORD /*dwTick*/ )
{
	TRUETIMEFUNC();


	//渲染2D场景
	

	OnUpdateWorldPos();

	//////ON_FUNC_PROFILER( GetAdvancedMiniMap Update Error );
	//{
	//	theMapViewManager.Update();
	//	// 更新players
	//	theVObjectManager.Update( dwTick, &thePathGameMap, m_SceneFrustum );
	//}
	////;

	//ON_FUNC_PROFILER(test0.2);
	{
		TRUETIMEBLOCK( "BattleMapHandle::RunGameMain EffectUpate" );

		theGameEffectManager.Update( g_CurTime );

		//设定锁定的npc的效果
		//ON_FUNC_PROFILER(test0.3);
		g_pLockPassivityTargetEffect->SetVisible(FALSE);
		g_pLockInitiativeTargetEffect->SetVisible(FALSE);

		//
		VObject* pLockObject;
		
		pLockObject = theVObjectManager.FindByID(theHeroActionInput.GetCurrentTarget());
		if (pLockObject)
		{
			//if (!pLockObject->IsDeadOver())
			{
				Vector3 vPos = pLockObject->GetPosition();
				float fBodySize = pLockObject->GetBodySize();
				float fRadius = 0.0f;
				
				if( pLockObject->GetAnim()
					&& pLockObject->GetAnim()->GetMeshConfig()
					)
				{
						fRadius = pLockObject->GetAnim()->GetMeshConfig()->m_BodyRadiusInfo.m_RadiusParam.GetNumber()*0.0333f;
				}


				if( g_InputData.m_IsAttackLockTarget )
				{
					g_pLockInitiativeTargetEffect->SetFollowPlayer( FALSE ); 
					g_pLockInitiativeTargetEffect->SetPos( vPos.x, vPos.y, vPos.z );
					g_pLockInitiativeTargetEffect->SetScale(fRadius*fBodySize);
					g_pLockInitiativeTargetEffect->SetVisible( TRUE );
				}
				else
				{
					g_pLockPassivityTargetEffect->SetFollowPlayer( FALSE ); 
					g_pLockPassivityTargetEffect->SetPos( vPos.x, vPos.y, vPos.z );
					g_pLockPassivityTargetEffect->SetScale(fRadius*fBodySize);
					g_pLockPassivityTargetEffect->SetVisible( TRUE );
				}
			}
		}
		g_pLockPassivityTargetEffect->Update();
		g_pLockInitiativeTargetEffect->Update();
		g_pFlyTextEffect->Update();
		;
	}

	return TRUE;
}

BOOL BattleMapHandle::RenderGameMain( DWORD dwTick )
{
	TRUETIMEFUNC();

	the3DRenderer.SetEyePoint( (float*)GetCamera()->GetEyePt() );
	
	{
		DisplayGameMain( dwTick );
	}

	if( !theApplicationSetting.m_ShowAdvancedMiniMap )
	{
#ifndef OFF_TRUETIME
		TRUETIMEBLOCK( "BattleMapHandle::RenderGameMain GameEffectManager" );
#endif // OFF_TRUETIME

		m_pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );	
		theSkillSpecialEffectManager.Render(dwTick);
		//m_pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );	

		theGameEffectManager.Render();
	}

	return TRUE;
}



BOOL BattleMapHandle::DisplayGameMain( DWORD dwTick )
{
	TRUETIMEFUNC();
	//BEGINFUNCPERLOG( "BattleMapHandle::DisplayGameMain" );

	IV3DRenderer* m_pRenderer = the3DEngine.GetRenderer();

	/////////////////////////////////////////
	//ON_FUNC_PROFILER(test1);
	if( g_V3DConfig.IsUseSkinMesh() )
	{
		GetActiveRender()->InitSkinMeshShader(GetGameSun()
                                           ,GetCamera()->GetViewMatrix()
														 ,GetCamera()->GetProjMatrix());
	}


	/////////////////////////////////////////
	//ON_FUNC_PROFILER(test2);

	if( theGameWorld.GetHeroPlotMap() )
	{
		//ON_FUNC_PROFILER(test2.1);
		if(m_bRenderTask)
			theGameWorld.PushRenderTask();


		/////////////////////////////////////////
		//TRUETIMEBLOCK( "BattleMapHandle::DisplayGameMain > theGameWorld.Render" );
		theGameWorld.Render(dwTick,m_bRenderTask,TRUE);



		/////////////////////////////////////////
		//if(!m_bRenderTask)
		{
			IV3DRenderer*	RI		= the3DEngine.GetRenderer();
			if(RI)
			{
				RI->SetRenderState( D3DRS_ZBIAS,2 );
				V3DTerrainRender::Flush();
				RI->Flush( RENDER_SORT_FAR_Z );
			}
		}


		/////////////////////////////////////////
		{
			//渲染对象
			//TRUETIMEBLOCK( "DisplayGameMain::Players" );
			m_pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );
			//theGameWorld.RenderScene(dwTick,FALSE);

			theVObjectManager.Render(dwTick);

		}
		
		////////////////////////////////////////////
		theGameWorld.RenderGrass(dwTick);

		if(theGeneralGameParam.IsShowSpecialArea())
		{
			theTileMap.RenderSpecialAreas(dwTick , 0x7f1fff7f);
		}

		theGameWorld.RenderWater(dwTick);		


		////////////////////////////////////////////
		//ON_FUNC_PROFILER(test2.3);
		{
			//TRUETIMEBLOCK( "theGameWorld::RenderAlpha" );
			m_pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );	
			theGameWorld.RenderAlpha(dwTick);

			//if(m_bRenderTask)
				theGameWorld.PopRender();

			//theGameWorld.RenderGrass(dwTick);
			//theGameWorld.RenderWater(dwTick);		

		}
		;




		////////////////////////////////
		//渲染鼠标提示
		//ON_FUNC_PROFILER(test2.6);
		if( theApplicationSetting.m_ShowMouseTile )
		{
			Vector3 vStart;
			Vector3 vDir;

			GetCamera()->CalcPickRay( 
				theUICtrlManager.m_MousePos.x, 
				theUICtrlManager.m_MousePos.y,
				vStart,
				vDir, 
				SCREEN_WIDTH, 
				SCREEN_HEIGHT );

		//	if( pScene )
			{
				float fDistToModelFloor = 0.0f;
				sBSP_INTERSECT* pIntersect;
				
				pIntersect =	theGameWorld.GetNearestIntersect( vStart, vDir );
				if( pIntersect )
				{
					fDistToModelFloor = (pIntersect->pos-vStart).Length();

					Vector3 c = pIntersect->pos;

					theV3DGraphicDDraw.DrawBox3D(c-Vector3(10,10,10)*SCALE_TERRAIN
														 ,c+Vector3(10,10,10)*SCALE_TERRAIN
														 ,0xffffffff);

					//PrintDebugInfo( 0, "hehe" );
				}
			}
		}

		/////////////////////////////////////////////
		//渲染视线  阻挡面
		if(g_BaseSetting.m_ShowDebugInfo)
		{
			theV3DGraphicDDraw.FillTriangle	(g_arCollisionVertices[0]
														,g_arCollisionVertices[1]
														,g_arCollisionVertices[2]
														,0xFF0000FF);
			theV3DGraphicDDraw.DrawLine3D	(g_CollisionRay[0]
													,g_CollisionRay[1]
													,0xFFFF0000);
		}


		/////////////////////////////////////////////
		m_pRenderer->SetRenderState( D3DRS_ZBIAS,1 );
	}
	m_pRenderer->SetRenderState( D3DRS_ZBIAS,0 );



	//ENDFUNCPERLOG( "BattleMapHandle::DisplayGameMain" );

	return TRUE;
}


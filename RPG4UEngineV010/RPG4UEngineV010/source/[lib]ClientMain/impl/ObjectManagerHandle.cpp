/*////////////////////////////////////////////////////////////////////////
文 件 名：ObjectManagerHandle.cpp
创建日期：2007年10月16日
最后更新：2007年10月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ObjectManagerHandle.h"
#include "ObjectFactoryManager.h"
#include "MapNPC.h"
#include "PartyManager.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ObjectManagerHandle, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ObjectManagerHandle::ObjectManagerHandle()
{
}
ObjectManagerHandle::~ObjectManagerHandle()
{
}


BOOL	ObjectManagerHandle::EnterPlayer	(sPLAYERINFO_VILLAGE*	pVillageInfo
													,BOOL							bNormalEnter
													,sEQUIP_ITEM_INFO_BASE*	pEquipInfo
													,sITEM_SLOT *				pItemSlot
													,sGUILDINFO_RENDER *		pGuildInfo
													,sVENDORINFO_RENDER *	pVendorInfo)
{
	sPLAYERINFO_RENDER *	pRenderInfo(NULL);
	Player*					pPlayer;
	
	if(bNormalEnter)
		pRenderInfo = (sPLAYERINFO_RENDER*) pVillageInfo;

	/////////////////////////////////////////////////
	if (theHero.GetObjectKey() == pVillageInfo->m_wPlayerKey)
	{
		if(pRenderInfo)
			theHero.SetCurrentAttackStyle(pRenderInfo->m_SelectStyleCode,FALSE);
		return TRUE;
	}


	pPlayer = (Player*)theObjectManager.Add(pVillageInfo->m_wPlayerKey
                                          ,PLAYER_OBJECT
														,pVillageInfo->m_byClass);
	if( !pPlayer )
		return FALSE;



	////////////////////////////////
	Vector3D vPos( pVillageInfo->m_fPos);

	if(pRenderInfo)
	{
		pPlayer->SetPlayerInfo( pRenderInfo , FALSE);
		pPlayer->SetCurrentAttackStyle(pRenderInfo->m_SelectStyleCode,FALSE);
	}
	else
	{
		pPlayer->SetPlayerVillageInfo( pVillageInfo , FALSE);
	}

	pPlayer->SetPosition	(vPos );
	pPlayer->SetName		(pVillageInfo->m_szName);				
	pPlayer->SetGMGrade	(pVillageInfo->m_byGMGrade );
	pPlayer->HideHelmet	(pVillageInfo->m_byHelmetOption,FALSE);


	//////////////////////////
	
	if(pGuildInfo)
		pPlayer->SetGuildInfo(pGuildInfo);

	if(pVendorInfo)
		pPlayer->SetVendorInfo(pVendorInfo);

	if(pEquipInfo && pItemSlot)
		pPlayer->SetEquipItemBaseInfo( pEquipInfo->m_Count, pItemSlot );

	return TRUE;
}


BOOL	ObjectManagerHandle::EnterNPC(sMONSTERINFO_RENDER& info
                                 ,sSTATE_INFO*         pStateInfo
											,WORD*                pwAngle)
{
	NPC	*					pMonster;
	sNPCINFO_BASE *		pNpcInfo;
	eOBJECT_TYPE			dwObjectType;
	

	//if(info.m_byType < NPC_FUNC_MAX)
	if(info.m_dwNpcFuncCode != 0)
		dwObjectType	= MAPNPC_OBJECT;
	else
		dwObjectType	= MONSTER_OBJECT;

	/////////////////////////////////////////
	pMonster = (NPC*)theObjectManager.GetObject(info.m_dwObjectKey);
	if(pMonster && pMonster->GetObjectType() != dwObjectType)
	{
		pMonster = (NPC*)theObjectManager.Delete(info.m_dwObjectKey);
		pMonster	= NULL;
	}

	/////////////////////////////////////////
	if(!pMonster)
	{
		pMonster = (NPC*)theObjectManager.Add	(info.m_dwObjectKey
															,dwObjectType
															,info.m_dwMonsterCode);
	}
	
	if( !pMonster )
		return FALSE;

	//////////////////////////////////////////////////
	//pMonster->SetNpcFunc((eNPC_FUNC_TYPE)info.m_byType);

	LOGINFO("CG_SYNC_MONSTER_ENTER_BRD [%s]\n",pMonster->GetName());

	int	iNumber		= thePartyManager.GetPartyNumber();
	float fMultiplier = theFormularManager.CalcHPIncreaseRatioAsParty(iNumber);

	pMonster->ChangeHPRatio(fMultiplier);
	pMonster->SetHP(info.m_dwHP);

	pNpcInfo = theNPCInfoParser.GetMonsterInfo(info.m_dwMonsterCode);

	if(pNpcInfo)
	{
		pMonster->SetMP(pNpcInfo->m_dwMaxMP);
	}

	if(info.m_dwNpcFuncCode != 0)
	{
		MapNPC*			pNPC			= (MapNPC*)pMonster;
		sNPCINFO_FUNC*	pFuncInfo;

		pFuncInfo = theNPCInfoParser.GetExtraInfo(info.m_dwNpcFuncCode);
		if(pFuncInfo)
			pNPC->SetFuncInfo(pFuncInfo);
	}


	///////////////////////////////////////////////////
	if(pwAngle)
		pMonster->SetAngle( math::Deg2Rad((float)*pwAngle) );

	pMonster->SetPosition(Vector3D(info.m_vPos) );
	pMonster->SetAttackSpeedModifier( info.m_wAttSpeedRatio/100.f );
	pMonster->SetMoveSpeedModifier( info.m_wMoveSpeedRatio/100.f );

	///////////////////////////////////////////////////
	// sSTATE_INFO 
	if(pStateInfo)
	{
		//pStateInfo = (sSTATE_INFO*)( (BYTE*)pRenderInfo + sizeof(sMONSTERINFO_RENDER) );
		for( int j = 0; j < info.m_byCount; ++j )
		{
			pStateInfo[j];
		}
	}

	/// 设置怪物出生时间 
	if(pNpcInfo && pNpcInfo->m_dwSpawnTime != 0)
	{
		PLAYER_ACTION idleaction;
		idleaction.SPAWN.dwEndTime=pNpcInfo->m_dwSpawnTime+g_CurTime;
		pMonster->SetCurrentAction(idleaction);
		pMonster->SetNextState(STATE_SPAWN,g_CurTime);		
	}
	// 否则设置淡入时间
	else
	{
		pMonster->SetFadeIn(1000);
	}
	return TRUE;
}


BOOL	ObjectManagerHandle::LeaveNPC(DWORD dwObjectKey)
{
	Object *		pObject;
	NPC *			pMonster;
	DWORD			dwCloneKey;
	Clone *		pClone;

	pObject = theObjectManager.GetObject( dwObjectKey );
	__CHECK_PTR	(pObject);
	__CHECK		(pObject->IsNpc());

	pMonster = static_cast<NPC *>(pObject);
	__CHECK_PTR	(pMonster);

	///////////////////////////////////////////////////
	if(pMonster->IsDead())
	{
		LOGINFO("CG_SYNC_MONSTER_LEAVE_BRD [%s] dead\n",pObject->GetName());
		dwCloneKey	= theCloneManager.CreateClone(pMonster);
		pClone		= theCloneManager.GetClone(dwCloneKey);
		if (pClone) 
		{
			DWORD dwTime = theGeneralGameParam.GetDeadDisappearTime();

			COLOR color = COLOR_RGBA(255,255,255,255);
			pClone->SetColor				(color);
			pClone->SetDelay				(dwTime+100);
			pClone->SetDisappear			(dwTime);
			pClone->SetScaleAnimation	(dwTime,1.8f);
			pClone->SetPlayAni			(FALSE);
			pClone->Vibrate				(Vector3D::UNIT_X*0.03f,3.f);
		}
	}
	else
	{
		LOGINFO("CG_SYNC_MONSTER_LEAVE_BRD [%s]\n",pObject->GetName());
	}

	//pMonster->SetAlpha(0.f);
	//pMonster->SetHP(0);
	pMonster->ProcessRemainActionResults();
	pMonster->SetTargetID( 0 );
	theObjectManager.AddToDeadList(pMonster->GetObjectKey());

	return TRUE;
}



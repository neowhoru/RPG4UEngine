/*////////////////////////////////////////////////////////////////////////
文 件 名：Map.cpp
创建日期：2008年6月6日
最后更新：2008年6月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Map.h"
#include "Camera.h"
#include "monster.h"
#include "Item.h"
#include "Npc.h"
#include "MapInfoParser.h"
#include "AppearanceManager.h"
#include "MapObject.h"
#include "EnvironmentInfoParser.h"
#include "TileWorldClient.h"
#include "WorldConst.h"
#include "V3DGameWorld.h"
#include "V3DTerrain.h"
#include "V3DGameMap.h"
#include "MapViewManager.h"
#include "VUCtrlManager.h"
#include "NPCInfoParser.h"
#include "PathInfoParser.h"
#include "VillageMapHandle.h"
#include "LoginMapHandle.h"
#include "LoadSceneMapHandle.h"
#include "BlankMapHandle.h"
#include "CharSelectMapHandle.h"
#include "MapNPC.h"


using namespace math;
using namespace input;

const DWORD	PLOTLOADINFO_PERIOD	= 500;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(Map, ()  , gamemain::eInstPrioGameFunc);


//static MapHandle	gs_DefaultMapHandle;

Map::Map(void)
:MapHandleManager(&theBlankMapHandle)
{

	m_pVMap					= NULL;
	m_pCurFieldInfo		= NULL;




	m_bLoaded				= FALSE;
	m_pTrigger			= NULL;
	m_bRenderAreaInfo		= FALSE;
	m_iAmbientSoundHandle = -1;

	m_dwSectorSize = 10; 
	m_iShiftMinX	= 0;
	m_iShiftMinY	= 0;
	m_nLoadTimer	= PLOTLOADINFO_PERIOD;
}

Map::~Map(void)
{
}

BOOL Map::Init()
{
	return Init(&theGameWorld);
}


//System函数
BOOL Map::Init(V3DGameWorld* pVMap)							
{
	assert(pVMap);
	//__BOOL_SUPER(Init());
	__CHECK( _RegMapHandles() );

	m_iAmbientSoundHandle = -1;

	m_iTriggerGroupIndex = -1;
	m_pTrigger			= NULL;
	m_dwCurrentTrigger	= 0;

	m_dwSectorSize			= 10; 

	m_pVMap					= pVMap;

	m_pVMap->AddListener(this);
	return TRUE;
}


void Map::Release()	
{
	if(m_pVMap == NULL)
		return;

	_UnregMapHandles();
	//_SUPER::Release();
	m_pVMap->RemoveListener(this);
}

BOOL Map::RequestWorkingData()	
{
	//__BOOL_SUPER(RequestWorkingData());
	return TRUE;
}

void Map::ReleaseWorkingData()	
{
	//_SUPER::ReleaseWorkingData();
	
	UnLoad();

}

BOOL Map::FrameMove( DWORD dwTick )
{
	__BOOL_SUPER(FrameMove(  dwTick ));
	if (m_bLoaded)
	{
		ProcessMapObjects	( dwTick );
		ProcessLoadInfo	(dwTick);
	}

	return TRUE;
}



BOOL Map::Render(DWORD dwTick)
{
	__BOOL_SUPER(Render(  dwTick ));

	if (theGeneralGameParam.GetSpecialMode())
	{
		if (m_bRenderAreaInfo)
			RenderAreaInfo();
	}

	if (theGeneralGameParam.GetSpecialMode() && theInputLayer.IsKeyDown(KEYC_B) ) 
	{
		RenderSector();
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::OnLoadMapPlot(V3DGameMap* pMapPlot)
{
	//pMapPlot->GetTerrain()->ProcssShadowMarker(20,20);
	//LoadNPCsIn(pMapPlot);
	AddPlotLoadInfo(pMapPlot, ePLOTLOAD_LOAD);
}


void Map::OnUnloadMapPlot	(V3DGameMap* pMapPlot)
{
	AddPlotLoadInfo(pMapPlot, ePLOTLOAD_UNLOAD);
	//UnloadNPCsIn(pMapPlot);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::ProcessLoadInfo(DWORD dwTick)
{
	m_nLoadTimer -= dwTick;
	if(m_nLoadTimer > 0)
		return;
	m_nLoadTimer	= PLOTLOADINFO_PERIOD;

	if(singleton::ExistHero() && m_mpPlotLoadInfo.size())
	{
		PlotLoadInfoMapIt	it;
		for(it = m_mpPlotLoadInfo.begin(); it != m_mpPlotLoadInfo.end(); it++)
		{
			if(it->second.eAction == ePLOTLOAD_LOAD)
				LoadNPCsIn(it->second);
			else
				UnloadNPCsIn(it->second);
		}
		m_mpPlotLoadInfo.clear();
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::AddPlotLoadInfo(V3DGameMap* pMapPlot,ePlotLoadAction eAction)
{
	assert(pMapPlot);

	INT					nPlotX,
							nPlotY;
	DWORD 				dwKey;
	PlotLoadInfoMapIt	it;

	pMapPlot->GetWorldMapXY(nPlotX, nPlotY);

	dwKey = MAKELONG(nPlotX,nPlotY);
	it = m_mpPlotLoadInfo.find(dwKey);
	if(it != m_mpPlotLoadInfo.end())
	{
		if(	eAction == ePLOTLOAD_LOAD		&& it->second.eAction == ePLOTLOAD_UNLOAD
			||	eAction == ePLOTLOAD_UNLOAD	&& it->second.eAction == ePLOTLOAD_LOAD)
			m_mpPlotLoadInfo.erase(it);

		return;
	}


	VMapPlotLoadInfo	info ;
	V3DTerrain*	pTerrain;

	pTerrain		= pMapPlot->GetTerrain();
	__CHECK2_PTR(pTerrain,;);

	info.vPlotBBox.m_Min.Set(
                pTerrain->GetBaseX()
					,pTerrain->GetBaseY()
					,-10000);

	info.vPlotBBox.m_Max.Set(
                pTerrain->GetBaseX() + pTerrain->GetMapSizeWidth()
					,pTerrain->GetBaseY() + pTerrain->GetMapSizeHeight()
					,10000);


	info.eAction	= eAction;
	info.nPlotX		= nPlotX;
	info.nPlotY		= nPlotY;

	m_mpPlotLoadInfo[dwKey] = info;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::LoadNPCsIn(VMapPlotLoadInfo& info)
{
	sNPCINFO_EXTRA_GROUP *	pInfoSet;
	MapNPC *					pNPC;
	DWORD						dwNpcID;

	vector<sNPCINFO_FUNC*>::iterator itr;
	sNPCINFO_FUNC *						pInfo = NULL;


	pInfoSet = theNPCInfoParser.GetExtraInfoSetFromFieldID( m_MapID , m_FieldID);



	for ( itr = pInfoSet->GetBegin(); itr != pInfoSet->GetEnd(); itr ++)
	{
		pInfo = *itr;

		if(theGeneralGameParam.IsEnableNetwork() && !pInfo->m_LocalNPC)
			continue;

		ASSERT(pInfo);

		Vector3D	vPos;
		vPos.x = pInfo->m_vPos.x * MAPTILESIZE;
		vPos.y = pInfo->m_vPos.y * MAPTILESIZE;
		vPos.z = pInfo->m_vPos.z;


		///////////////////////////////////////
		vPos.z = theGameWorld.GetHeightAt(vPos.z,10000.f,vPos.x,vPos.y);

		pInfo->m_vPos.z	= vPos.z;

		///////////////////////////////////////
		if(!info.vPlotBBox.IsContain(vPos))
			continue;

		dwNpcID	= theObjectManager.GenerateKeyAtSingleMode(MAPNPC_OBJECT);
		//dwNpcID	= theObjectManager.GenerateKeyAtSingleMode(NPC_OBJECT);
		pNPC		= (MapNPC *)theObjectManager.Add	(dwNpcID//dwNpcObjectKey++
																,MAPNPC_OBJECT//NPC_OBJECT
																,pInfo->m_NPCCODE);	




		///////////////////////////////////////
		pNPC->SetPosition(vPos );
		//pNPC->SetMoveSpeed();
		pNPC->SetAngle		( math::Deg2Rad((float)pInfo->m_nAngle) );

		pNPC->SetFuncInfo(pInfo, TRUE);
	}	   
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::UnloadNPCsIn(VMapPlotLoadInfo& info)
{
	Vector3D		wzPos;
	Object*		pObject;

	ObjectMapIt	itr;
	for ( itr = theObjectManager.GetBegin(); itr != theObjectManager.GetEnd(); itr ++)
	{
		pObject = itr->second;
		/// 确保只是 MAPNPC_OBJECT  
		if(pObject->IsKindOfObject(MAPNPC_OBJECT) )//NPC_OBJECT)
		//if(pObject->GetObjectCookie().GetObjectType() != MAPNPC_OBJECT)//NPC_OBJECT)
			continue;

		/// 不在本区域内，不删除
		if(!info.vPlotBBox.IsContain(pObject->GetPosition()))
			continue;

		theObjectManager.AddToDeadList(pObject->GetObjectKey());
	}	   
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::OnLoadingFinished()
{
	m_bLoaded = TRUE;

	PlayAmbientSound();

	CalculateSectorInfo();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::_RegMapHandles()
{
	//RegSystem(&theBlankMapHandle);  默认Handle
	RegSystem(&theLoginMapHandle);
	RegSystem(&theCharSelectMapHandle);
	RegSystem(&theVillageMapHandle);
	RegSystem(&theLoadSceneMapHandle);

	ActivateDefault();

	_SUPER::ActivateSystem(WH_TYPE_BLANK);
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Map::_UnregMapHandles	()
{
	ActivateSystem((DWORD)SYSTEM_NULL);

	UnregSystem(&theBlankMapHandle);
	UnregSystem(&theLoginMapHandle);
	UnregSystem(&theCharSelectMapHandle);
	UnregSystem(&theVillageMapHandle);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::LoadBaseMapInfo(MAPCODE MapID, FIELDID FieldID)
{
	sMAPINFO_BASE * pMapInfo = theMapInfoParser.GetMapInfo(MapID);

	ASSERT(pMapInfo);

	m_pCurFieldInfo	= NULL;
	m_pMapInfo			= NULL;

	m_iFieldIndex = 0;

	for (int i = 0 ; i < 6; i++)
	{
		if ( pMapInfo->arFieldIDs[i] == FieldID)
		{
			m_pCurFieldInfo = theMapInfoParser.GetFieldInfo(FieldID);
			m_iFieldIndex = i;
			break;
		}
	}
	m_pMapInfo		= pMapInfo;

	assert(m_pCurFieldInfo && m_pMapInfo && "FieldInfo is NULL.!");


	m_MapID			= MapID;
	m_FieldID		= FieldID;



	m_iTriggerGroupIndex = -1;

	m_sFieldID = pMapInfo->arFieldNames[i];
	
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::LoadMapData()
{
	assert(m_pVMap);

	INT	nPlotX	= 0, 
			nPlotY	= 0;
	INT	nSizeX	= 1,
			nSizeY	= 1;

	//LPCSTR	szPath;
	DWORD		dwStartTime = g_CurTime;

	nPlotX = (INT)(m_vStartPos.x/MAP_SIZE_BYTILE); 
	nPlotY = (INT)(m_vStartPos.y/MAP_SIZE_BYTILE);

	nSizeX = m_pCurFieldInfo->uWidth;
	nSizeY = m_pCurFieldInfo->uHeight;

	//if( theTestMap.bTestMap )
	//	m_pVMap->SetReplacePath( szMapFilename );


	//////////////////////////////////
	// 单地块载入

	{
		m_pVMap->SetReplacePath( "" );
	}

	/// 通过VMap载入3D地图
	if(!m_pVMap->LoadWorld	((LPCTSTR)m_pCurFieldInfo->szFieldPath
                           ,nSizeX
									,nSizeY
									,nPlotX
									,nPlotY)) 
	{
		m_pVMap->ReleaseWorkingData();
		return FALSE; 
	}

	//thePathGameMap.SetCellSize( TILE_3DSIZE );
	if( m_pVMap->GetHeroPlotMap() )
	{
		V3DTerrain* pTerrain = m_pVMap->GetHeroPlotMap()->GetTerrain();
		if( pTerrain )
		{
			//thePathGameMap.SelectWalkMask( pTerrain->GetWalkMask(), pTerrain );
			
			theMapViewManager.Update();
			theMapViewManager.Create	(MAP_SIZE_BYTILE
                                       ,MAP_SIZE_BYTILE
													,m_pCurFieldInfo);
			//theMapViewManager.Create(  thePathGameMap.GetWidth(), thePathGameMap.GetHeight() );

			pTerrain->BuildLightList();
		}
	}


	///////////////////////////////////////////
	//地图载入完成
	//显示小地图
	theGameUIManager.UISetVisible	(gameui::eUIMiniMap
											,m_pCurFieldInfo->bShowMiniMap?true:false);

	if(singleton::ExistHero())
		theHero.AddUpdateFlag(INVALID_DWORD_ID);

	LOGINFO( "mapLoading finished  ,used time:%d\n", g_CurTime-dwStartTime );

	return TRUE;
}






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::LoadMapObject()
{
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::LoadCameraExtraInfo()
{
	m_fMapProjectionFar  = CAMERA_DEFAULT_FAR_CLIP;
	m_fMapProjectionNear = CAMERA_DEFAULT_NEAR_CLIP;
	m_fMapProjectionFOV  = CAMERA_DEFAULT_FOV;




	return TRUE;
}






BOOL Map::LoadTrigger()
{

	return TRUE;

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::LoadLand( LPCSTR szLandName, const Vector3D& vStartPos )
{
	assert(m_pVMap);




	__BOOL_CALL( m_pVMap->LoadWorld(szLandName
                                   ,(INT)vStartPos.x
											  ,(INT)vStartPos.y));
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::Load( MAPCODE MapID, FIELDID FieldID, const Vector3D& vStartPos )
{

	m_iAmbientSoundHandle = -1;

	UnLoad();


	m_bLoaded = FALSE;

	__BOOL_CALL (LoadBaseMapInfo(MapID, FieldID) );

	m_vStartPos = vStartPos;

	LoadMapData();
	LoadMapObject();


	LoadCameraExtraInfo();

	//////////////////////////////////////////////////////////////////////////
	// trigger info 
	//////////////////////////////////////////////////////////////////////////

	return TRUE;
}



void Map::UnLoad()
{
	StopAmbientSound();



	m_pCurFieldInfo		= NULL;
	m_iTriggerGroupIndex = -1;
	m_dwCurrentTrigger	= 0;

	if (m_pTrigger) 
	{
		assert(!"need delete m_pTrigger");
		//delete m_pTrigger;
		m_pTrigger = NULL;
	}

	m_bLoaded = FALSE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int	Map::GetCurrentFieldID() const
{ 
	if (NULL == m_pCurFieldInfo)
		return -1;
	return m_pCurFieldInfo->FieldCode; 
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
Ray* Map::GetMouseCursorRay( Ray *pRay, POINT *pptCursor)
{
	POINT ptMouseCursor;
	if ( !pptCursor)
	{
		::GetCursorPos( &ptMouseCursor);
		::ScreenToClient( g_hWndMain, &ptMouseCursor);
		//ptMouseCursor.x = theUICtrlManager.m_MousePos.x;
		//ptMouseCursor.y = theUICtrlManager.m_MousePos.y;
		pptCursor = &ptMouseCursor;
	}

	GetCamera()->CalcPickRay(pptCursor->x
                            ,pptCursor->y
									 ,(Vector3&)pRay->m_Origin
									 ,(Vector3&)pRay->m_Direction
									 ,SCREEN_WIDTH
									 ,SCREEN_HEIGHT);

	return ( pRay);


}











void Map::ProcessMapObjects( DWORD /*dwTick*/ )
{

}



SpecialArea * Map::GetMapArea( DWORD dwID )
{
	SpecialArea *pInfo = theTileMap.GetSpecialArea(dwID,FALSE);
	return pInfo;
}

SpecialArea * Map::GetMapAreaByIndex( DWORD dwIndex )
{

	SpecialArea *pInfo = theTileMap.GetSpecialAreaByIndex(dwIndex,FALSE);
	
	return pInfo;
}



BOOL Map::GetStartAreaPosition(Vector3D * pwzPos)
{
	IDTYPE id;

	id = TAG('STRT');

	SpecialArea *pArea = GetMapArea(id);

	if (!pArea)
	{
		return FALSE;
	}

	switch ( pArea->m_bvData.m_eBvType )
	{
	case BVT_AABB:	
		{
			*pwzPos = (pArea->m_bvData.m_AABB.m_Min + pArea->m_bvData.m_AABB.m_Max);
			pwzPos->x /= 2.0f;
			pwzPos->y /= 2.0f;
			pwzPos->z /= 2.0f;
		}
		break;
	case BVT_PATH_TILE_INDEX:
		{
			
			ASSERT(pArea->m_bvData.m_PathTileIndex.m_nCount > 0);
			ASSERT(pArea->m_bvData.m_PathTileIndex.m_pdwData[0] >= 0);

			*pwzPos = theTileMap.GetRandomPosInTile( pArea->m_bvData.m_PathTileIndex.m_pdwData[0] );
		}
		break;			

	default:
		ASSERT(0);
		break;
	}
	return TRUE;
}


LPCSTR Map::GetFieldName()
{
	return (LPCSTR)m_pMapInfo->sMapName;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Map::GetPlayerDirection(Vector3D * pwzDir)
{
    if (m_pCurFieldInfo)
	{
		if (pwzDir)
		{
			*pwzDir = m_ExtraCameraInfo.rTargetPos - m_ExtraCameraInfo.rPos;
			return TRUE;
		}
	}

	return FALSE;
}


BOOL Map::GetTerrainHeight(const Vector3D& vPos,Vector3D& vResult)
{
	vResult	= vPos;
	vResult.z= theGameWorld.GetHeightAt(vPos.z,38.2f*SCALE_MESH,vPos.x,vPos.y,0);

	return TRUE;

}


void Map::RenderAreaInfo()
{
#pragma message(__FILE__  "(2649) Map..RenderAreaInfo  " )

	//int areanum = theTileWorld.GetNumberOfSpecialArea();
	//for (int a = 0; a< areanum; a++)
	//{

	//	SpecialArea *pArea = theTileWorld.GetSpecialAreaByIndex(a);

	//	switch ( pArea->m_bvData.m_eBvType )
	//	{
	//	case BVT_AABB:
	//		{
	//			GetActiveRender()->RenderBoundingVolume(&pArea->m_bvData);				
	//		}
	//		break;
	//	case BVT_PATH_TILE_INDEX:
	//		{
	//			GetActiveRender()->SetMeshDrawMode(MD_WIREFRAME);
	//			m_pWorldBase->x_pPathFinder->RenderPathTile(g_pRenderer,0,(int *)pArea->m_bvData.m_PathTileIndex.m_pdwData,pArea->m_bvData.m_PathTileIndex.m_nCount);
	//			GetActiveRender()->SetMeshDrawMode(MD_POLYGON);				
	//		}
	//		break;
	//	}
	//}
}


void Map::ToggleShowAreaInfo()
{
	m_bRenderAreaInfo ^= 1;
}



void Map::RenderSector()
{
	//COLOR oldcolor = GetActiveRender()->GetDiffuseColor();
	//GetActiveRender()->SetColor(COLOR_RGBA(255,255,0,255));
	//GetActiveRender()->SetMeshDrawMode(MD_WIREFRAME);
	//ENUM_ALPHABLEND oldblend = GetActiveRender()->GetAlphaBlendMode();
	//GetActiveRender()->SetAlphaBlendMode(AB_NONE);
	//GetActiveRender()->EnableTexture(FALSE);


	//St_Camera *pInfo = GetActiveRender()->GetCameraInfo();

	//BBox3D bv = m_pWorldBase->GetTerrainSize();

	//float fMinX = bv.m_Min.x;
	//float fMinY = bv.m_Min.y;

	//DWORD dwXSize = (DWORD)(bv.m_Max.x - bv.m_Min.x);
	//DWORD dwYSize = (DWORD)(bv.m_Max.y - bv.m_Min.y);

	//int iXCount = dwXSize / m_dwSectorSize + ((dwXSize % m_dwSectorSize) ? 1 : 0);
	//int iYCount = dwYSize / m_dwSectorSize + ((dwYSize % m_dwSectorSize) ? 1 : 0);

	//BoundingVolume tempbv;

	//tempbv.m_eBvType = BVT_AABB;
	//tempbv.m_AABB.m_Min.z = bv.m_Min.z;
	//tempbv.m_AABB.m_Max.z = bv.m_Max.z;

	//if (g_pHero)
	//{
	//	Vector3D vPos = g_pHero->GetVisiblePos();

	//	tempbv.m_AABB.m_Min.z = vPos.z;
	//	tempbv.m_AABB.m_Max.z = vPos.z + 5.0f;

	//}

	//int iRenderCount = 0;

	//for (int iX = 0; iX < iXCount; iX++)
	//{
	//	for (int iY = 0; iY < iYCount; iY++)
	//	{
	//		tempbv.m_AABB.m_Min.x = fMinX + iX * m_dwSectorSize;
	//		tempbv.m_AABB.m_Min.y = fMinY + iY * m_dwSectorSize;
	//		tempbv.m_AABB.m_Max.x = fMinX + (iX + 1) * m_dwSectorSize;
	//		tempbv.m_AABB.m_Max.y = fMinY + (iY + 1) * m_dwSectorSize;

	//		if (Outside_Frustum_Bv( &pInfo->m_wfFrustum, &tempbv ) != NTCT_OUTSIDE)
	//		{
	//			GetActiveRender()->RenderBoundingVolume(&tempbv);
	//			iRenderCount++;
	//		}
	//	}
	//}
	//GetActiveRender()->SetMeshDrawMode(MD_POLYGON);
	//GetActiveRender()->SetColor(oldcolor);
	//GetActiveRender()->SetAlphaBlendMode(oldblend);
	//GetActiveRender()->EnableTexture(TRUE);

}


//----------------------------------------------------------------------------
void Map::StopAmbientSound()
{
	if (m_iAmbientSoundHandle != -1) 
	{
		//stop_ambi(m_iAmbientSoundHandle);
		m_iAmbientSoundHandle = -1;
	}
}


//----------------------------------------------------------------------------
void	Map::SetAmbientVolume(float /*fVolume*/)
{
	if (m_iAmbientSoundHandle != -1) 
	{
		//set_volume_all_ambi( fVolume);
		//SetAmbientEffectVolume(fVolume);
	}
}


//----------------------------------------------------------------------------
void Map::PlayAmbientSound()
{

}




DWORD Map::GetSectorIndex( const Vector3D& vPos ) 
{
	DWORD dwXIndex = 0;
	DWORD dwYIndex = 0;

	dwXIndex = (DWORD)( vPos.x - m_iShiftMinX ) / m_dwSectorSize;
	dwYIndex = (DWORD)( vPos.y - m_iShiftMinY ) / m_dwSectorSize;


	ASSERT( dwXIndex + m_dwSectorXNum * dwYIndex < m_dwTotalSectorNum );
	return ( dwXIndex + m_dwSectorXNum * dwYIndex );
}

void Map::CalculateSectorInfo()
{
	sMAPINFO_BASE * pMapInfo = theMapInfoParser.GetMapInfo(GetMapID());
	if ( pMapInfo )
	{
		if (ZONETYPE_VILLAGE == pMapInfo->byMKind)
		{
			m_dwSectorSize = VILLAGE_SECTOR_SIZE;
		}
		else
		{
			m_dwSectorSize = ROOM_SECTOR_SIZE;
		}
	}
	else
	{
		LOGINFO("[Map::CalculateSectorInfo] not found mapinfo[%d]\n", GetMapID());
		//ASSERT(0);
	}

	//if (theTileMap)
	{
		BBox3D box = theTileWorld.GetLandSize();

		m_iShiftMinX = (int)box.m_Min.x;
		m_iShiftMinY = (int)box.m_Min.y;

		DWORD XSize = 0, YSize = 0;
		XSize = (DWORD)(box.m_Max.x - box.m_Min.x);
		YSize = (DWORD)(box.m_Max.y - box.m_Min.y);

		BYTE byMarginX = 0, byMarginY = 0;
		XSize % m_dwSectorSize == 0 ? byMarginX = 0 : byMarginX = 1;
		YSize % m_dwSectorSize == 0 ? byMarginY = 0 : byMarginY = 1;

		m_dwSectorXNum				= XSize / m_dwSectorSize + byMarginX;
		m_dwSectorYNum				= YSize / m_dwSectorSize + byMarginY;

		m_dwTotalSectorNum			= m_dwSectorXNum * m_dwSectorYNum;
	}
}






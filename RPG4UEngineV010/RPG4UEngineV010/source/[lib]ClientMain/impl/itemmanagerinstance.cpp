/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManagerInstance.cpp
创建日期：2008年5月31日
最后更新：2008年5月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ItemManagerInstance.h"
#include "PacketInclude.h"
#include "ItemManagerNet.h"
#include "QuickPaneSlot.h"
#include "StyleSlot.h"
#include "MouseHandler.h"
#include "EquipmentContainer.h"
#include "InventoryDialog.h"
#include "ShopDialog.h"
#include "Item.h"
#include "WareHouseDialog.h"
#include "EquipmentContainer.h"
#include "DummySlotContainer.h"
#include "DummyItemSlot.h"
#include "ItemCompositeParser.h"
#include "TempInventoryContainer.h"
#include "ItemCompoundDialog.h"
#include "StyleContainer.h"
#include "ShopInfoParser.h"
#include "SkillSlot.h"
#include "QuickContainer.h"
#include "SlotKeyGenerator.h"
#include "ItemSoundInfoParser.h"
#include "EventInventoryDialog.h"
#include "SkillStorageManager.h"
#include "StateInfoParser.h"
#include "SkillStorageManager.h"
#include "SkillPaneContainer.h"
#include "ItemCompositeParser.h"
#include "ItemMakeParser.h"
#include "ItemAttrInfoParser.h"
#include "SeriesInfoParser.h"
#include "Player.h"
#include "ItemValidation.h"
#include "ItemLinkHandler.h"
#include "ConstTextRes.h"


using namespace RC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemManagerInstance, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemManagerInstance::ItemManagerInstance()
{
}

ItemManagerInstance::~ItemManagerInstance()
{
}

BOOL ItemManagerInstance::Init()
{
	return TRUE;
}


VOID ItemManagerInstance::Release()
{

}

//VOID ItemManagerInstance::Render()
//{
//}
//
//VOID ItemManagerInstance::Update()
//{
//}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BaseContainer *  ItemManagerInstance::GetContainer( SLOTINDEX ContainerIndex )
{
	return theHero.GetSlotContainer(ContainerIndex);
}


ItemSlotContainer * ItemManagerInstance::GetItemSlotContainer( SLOTINDEX Index )
{
	ASSERT( Index < SI_MAX );
	ASSERT( Index != SI_SKILL );
	return (ItemSlotContainer *)::theHero.GetSlotContainer(Index);
}


//=============================================================================================================================
BOOL ItemManagerInstance::GetItemDesc( SERIALTYPE serial, SLOTINDEX & slotIdxOut, SLOTPOS & posOut )
{
	for( SLOTINDEX i = 0 ; i < SI_MAX ; ++i )
	{
		if (!theHero.GetSlotContainer(i))
			continue;

        if (i == SI_SKILL)
        {
            SLOTPOS maxPos;
            SLOTPOS pos;

				SkillPaneContainer* pContainer;

				for (int j=0; j < SKILL_KIND_MAX; ++j)
				{
					pContainer	= theSkillStorageManager.GetUISkillContainer((eSKILL_CATEGORY)j);
					maxPos		= pContainer->GetSlotMaxSize();

					for (pos=0; pos < maxPos; ++pos)
					{
						BaseSlot & slotDat = pContainer->GetSlot(pos);
						if( serial == slotDat.GetSerial() )
						{
							slotIdxOut	= i;
							posOut		= pos;
							return TRUE;
						}
					}
				}
        }
        else
        {
            SLOTPOS maxPos;
				maxPos = theHero.GetSlotContainer(i)->GetSlotMaxSize();
            for( SLOTPOS pos = 0 ; pos < maxPos ; pos++ )
            {
                if (		i == SI_EQUIPMENT  
                    ||	i == SI_INVENTORY  
                    ||	i == SI_SKILL )
                {
                    BaseSlot & slotDat = theHero.GetSlotContainer(i)->GetSlot(pos);
                    if( serial == slotDat.GetSerial() )
                    {
                        slotIdxOut	= i;
                        posOut		= pos;
                        return TRUE;
                    }
                }

            }
        }
	}
	return FALSE;
}


//=============================================================================================================================
BOOL ItemManagerInstance::GetEmptySlotPos(SLOTINDEX slotIdxAt, SLOTPOS & OUT posOut)
{
	BaseContainer * pContainer;
	
	pContainer = GetContainer(slotIdxAt);
	__VERIFY_PTR(pContainer, "Container is NULL" );

	pContainer->GetEmptyPos( posOut );

	return TRUE;
}


//----------------------------------------------------
BOOL ItemManagerInstance::GetEquipPosition(BaseSlot & slotDat, SLOTPOS & OUT equipPosRet)
{
	__VERIFY(slotDat.GetSlotType() == ST_ITEM, "Slot must be Item");

	sITEMINFO_BASE *	pItemInfo;

	ItemSlot &	slotItem		= (ItemSlot &)slotDat;
	pItemInfo					= slotItem.GetItemInfo();

	__CHECK_PTR(pItemInfo);

	switch ( pItemInfo->m_wEqPos )
	{
	case EQUIPPOS_WEAPON:
	case EQUIPPOS_WEAPON2:
	case EQUIPPOS_ARMOR:
	case EQUIPPOS_PROTECTOR:
	case EQUIPPOS_HELMET:
	case EQUIPPOS_PANTS:
	case EQUIPPOS_BOOTS:
	case EQUIPPOS_GLOVE:
	case EQUIPPOS_BELT:
	case EQUIPPOS_SHIRTS:
	case EQUIPPOS_NECKLACE:
		{
			equipPosRet = (SLOTPOS)pItemInfo->m_wEqPos;
		}
		break;

	case EQUIPPOS_RING1:
	case EQUIPPOS_RING2:
		{
			BaseContainer * pContainer = GetContainer(SI_EQUIPMENT);
			if ( pContainer->IsEmpty( EQUIPPOS_RING2 ) )
			{
				equipPosRet = EQUIPPOS_RING2;
			}
			else
			{
				equipPosRet = EQUIPPOS_RING1;
			}
		}
		break;

	default:
		{
			ASSERT(!"未支持类型!!");
			return FALSE;
		}
		break;
	}

	return TRUE;
}

//----------------------------------------------------
DWORD ItemManagerInstance::GetItemSound(eSLOT_SOUND_TYPE eSoundType, ItemSlot * pItemSlot)
{
	__CHECK2_PTR(pItemSlot, INVALID_DWORD_ID);

	sITEMINFO_BASE * pItemInfo;
	
	pItemInfo = pItemSlot->GetItemInfo();
	__CHECK2_PTR(pItemInfo, INVALID_DWORD_ID);

	return GetItemSound(eSoundType, pItemInfo->m_wType);
}

DWORD ItemManagerInstance::GetItemSound(eSLOT_SOUND_TYPE eSoundType, DWORD dwItemType)
{
	sITEM_SOUNDINFO * pSoundInfo;
	
	pSoundInfo = theItemSoundInfoParser.GetSoundInfo( dwItemType );

	__CHECK2_PTR(pSoundInfo, INVALID_DWORD_ID);

	switch (eSoundType)
	{
	case SLOTSOUND_PICK:		return pSoundInfo->m_PickDefault;	break;
	case SLOTSOUND_PUT:		return pSoundInfo->m_PutDefault;		break;
	case SLOTSOUND_EQUIP:	return pSoundInfo->m_PutWear;			break;
	case SLOTSOUND_UNEQUIP:	return pSoundInfo->m_RemoveWear;		break;
	case SLOTSOUND_LINK:		return pSoundInfo->m_LinkQslot;		break;
	case SLOTSOUND_UNLINK:	return pSoundInfo->m_UnlinkQslot;	break;
	case SLOTSOUND_DUMP:		return pSoundInfo->m_Dump;				break;

	default:
		break;
	}
	return INVALID_DWORD_ID;
}


//----------------------------------------------------
DWORD ItemManagerInstance::GetSkillSound(eSLOT_SOUND_TYPE eSoundType, SkillSlot * pSkillSlot)
{
	__CHECK2_PTR(pSkillSlot, INVALID_DWORD_ID);

	DWORD dwSoundID = INVALID_DWORD_ID;

	switch (eSoundType)
	{
	case SLOTSOUND_PICK:		dwSoundID = UI_SOUND_SKILL_ICON_PICK;	break;
	case SLOTSOUND_PUT:		dwSoundID = UI_SOUND_SKILL_ICON_DROP;	break;
	case SLOTSOUND_EQUIP:	
	case SLOTSOUND_UNEQUIP:	
	case SLOTSOUND_LINK:		dwSoundID = UI_SOUND_SKILL_ICON_DROP;	break;
	case SLOTSOUND_UNLINK:	
	case SLOTSOUND_DUMP:
	default:
		break;
	}

	if(dwSoundID != INVALID_DWORD_ID)
		return GetItemSound(eSoundType,dwSoundID  );

	return INVALID_DWORD_ID;
}


//----------------------------------------------------
VOID ItemManagerInstance::PlaySlotSound( eSLOT_SOUND_TYPE eSoundType, BaseSlot * pSlot )
{
	__CHECK2_PTR(pSlot, ;);

	DWORD dwSoundID = 0;

	eSLOTTYPE SlotType = pSlot->GetSlotType();

	switch (SlotType) 
	{
		case ST_ITEM:
				dwSoundID = GetItemSound(eSoundType, (ItemSlot *)pSlot);
			break;

		case ST_SKILL:
				dwSoundID = GetSkillSound(eSoundType, (SkillSlot *)pSlot);
			break;

		case ST_STYLE_QUICK:
			{
				StyleSlot * pStyleSlot = (StyleSlot * )pSlot;	
				dwSoundID = GetSkillSound(eSoundType
                                     ,(SkillSlot* )pStyleSlot->GetOrgSlot());
			}
			break;

		case ST_QUICK:
			{
				QuickPaneSlot * pQuickSlot = (QuickPaneSlot *)pSlot;
				switch ( pQuickSlot->GetOrgSlotType() )
				{
				case ST_ITEM:
						dwSoundID = GetItemSound(eSoundType
                                          ,(ItemSlot* )pQuickSlot->GetOrgSlot())							;
					break;

				case ST_SKILL:
						dwSoundID = GetSkillSound(eSoundType
                                           ,(SkillSlot* )pQuickSlot->GetOrgSlot())							;
					break;
				}
			}
			break;

		case ST_ITEMHANDLE:
			{
				DummyItemSlot * pDummySlot	= (DummyItemSlot *)pSlot;			
				dwSoundID = GetItemSound(eSoundType, pDummySlot);
			}
			break;

		default:
			break;
		}

		theSoundEffect.PlayUI( dwSoundID );	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace util
{
void OutputItemInfo(eTOTALINFO_INSERT_TYPE eType, sITEMINFO_BASE * pBaseItemInfo)
{
	ASSERT(pBaseItemInfo && "Invalid Item Info!!");
	if(pBaseItemInfo)
	{
		LPCSTR szItemName = "";
		//sITEMINFO_BASE* pItem;
		
		//pItem = theItemInfoParser.GetItemInfo(pBaseItemInfo->m_VItemCode);
		//if(pVItem)
			szItemName = (LPCSTR)pBaseItemInfo->m_ItemName;

		DWORD dwID = 0;
		switch ( eType )
		{
		case TOTALINFO_INSERT_BUY:		dwID = TEXTRES_NAMECODE_BUY_ITEM;		break;
		case TOTALINFO_INSERT_PICK:	dwID = TEXTRES_NAMECODE_GET_ITEM;		break;
		case TOTALINFO_INSERT_CREATE:	dwID = TEXTRES_ITEM_CREATE;				break;
		case TOTALINFO_INSERT_REWARD:	dwID = TEXTRES_ITEM_REWARD;				break;
		case TOTALINFO_INSERT_EVENT:	dwID = TEXTRES_REWARDITEM;				break;
		}
		if(dwID)
			OUTPUTCHATF(dwID,szItemName);
	}
}
};

using namespace util;

//----------------------------------------------------
BOOL ItemManagerInstance::InsertInventoryItems(eTOTALINFO_INSERT_TYPE eType
                                              ,sTOTALINFO_INVENTORY & totalInfoDat)
{
	BaseContainer * pContainer;
	
	pContainer = GetContainer(SI_INVENTORY);
	ASSERT(pContainer); 

	// total info set
	sITEM_SLOTEX *		pSlot = totalInfoDat.m_Slot;
	SLOTPOS				start = 0;
	INT					total = totalInfoDat.m_InvenCount;

	ItemSlot				ItemBuf;
	sITEMINFO_BASE *	pBaseItemInfo = NULL;

	/////////////////////////////////////////////////
	//背包
	for(SLOTPOS i=start;i<total;++i)
	{
		ItemBuf.Clear();
		ItemBuf.Copy( pSlot[i].m_Stream );
		if (pContainer->IsEmpty(pSlot[i].m_ItemPos))
		{
			pContainer->InsertSlot(pSlot[i].m_ItemPos, ItemBuf);
		}
		else
		{
			pContainer->DeleteSlot(pSlot[i].m_ItemPos, NULL);
			pContainer->InsertSlot(pSlot[i].m_ItemPos, ItemBuf);
		}

		pBaseItemInfo = ItemBuf.GetItemInfo();

		///输出物品信息
		OutputItemInfo( eType, pBaseItemInfo);
		theItemLogManager.PushItem(ItemBuf, eITEMLOG_ADD);
		theGameUIManager.UIRefresh(gameui::eUIQuest);
	}



	/////////////////////////////////////////////////////////
	//临时背包
	pContainer = GetContainer(SI_INVENTORY2);
	start		=	totalInfoDat.m_InvenCount;
	total		+= totalInfoDat.m_TmpInvenCount;
	for(SLOTPOS i=start; i<total; ++i)
	{
		ItemBuf.Clear();
		ItemBuf.Copy( pSlot[i].m_Stream );

		if (pContainer->IsEmpty(pSlot[i].m_ItemPos))
		{
			pContainer->InsertSlot(pSlot[i].m_ItemPos, ItemBuf);
		}
		else
		{
			pContainer->DeleteSlot(pSlot[i].m_ItemPos, NULL);
			pContainer->InsertSlot(pSlot[i].m_ItemPos, ItemBuf);
		}

		pBaseItemInfo = ItemBuf.GetItemInfo();

		///输出物品信息
		OutputItemInfo( eType, pBaseItemInfo);
		theItemLogManager.PushItem(ItemBuf, eITEMLOG_ADD);
		theGameUIManager.UIRefresh(gameui::eUIQuest);
	}
	if(totalInfoDat.m_TmpInvenCount)
	{
		OUTPUTCHAT(TEXTRES_CHECK_TEMP_INVENTORY);
	}



	/////////////////////////////////////////////////////////
	// play sound 
	DWORD dwSoundID(INVALID_DWORD_ID);
	switch ( eType )
	{
	case TOTALINFO_INSERT_BUY:		dwSoundID = SLOTSOUND_PUT;		break;
	case TOTALINFO_INSERT_PICK:	dwSoundID = SLOTSOUND_PUT;		break;
	case TOTALINFO_INSERT_CREATE: dwSoundID = SLOTSOUND_PUT;		break;
	case TOTALINFO_INSERT_REWARD: dwSoundID = SLOTSOUND_PUT;		break;
	}

	if(dwSoundID != INVALID_DWORD_ID)
		PlaySlotSound((eSLOT_SOUND_TYPE)dwSoundID, &ItemBuf);

	return TRUE;
}


//----------------------------------------------------------------------
BOOL ItemManagerInstance::InsertEventItems( sTOTALINFO_EVENT_ITEM & totalInfoDat)
{
	BaseContainer * pContainer = GetContainer(SI_EVENT_INVENTORY);
	ASSERT(pContainer); 

	BOOL bRet = TRUE;


	sEVENT_SLOT *	pEventSlot	= totalInfoDat.m_Slot;
	SLOTPOS			total			= totalInfoDat.m_Count;

	ASSERT( pContainer->GetSlotMaxSize() >= sTOTALINFO_EVENT_ITEM::MAX_SLOT_NUM );

	ItemSlot itemEventTemp;

	if ( pContainer )
	{
		for ( int i = 0; i < total; ++i)
		{
			itemEventTemp.Clear();

			switch (pEventSlot[i].m_ItemCode)
			{
			case 1: itemEventTemp.SetCode(9001); break;
			case 2: itemEventTemp.SetCode(9002); break; 
			case 3: itemEventTemp.SetCode(9003); break;
			case 4: itemEventTemp.SetCode(9004); break;
			default:
				ASSERT(0);
				continue;
				break;
			}

			itemEventTemp.SetNum(pEventSlot[i].m_ItemCount);	

			pContainer->InsertSlot((SLOTPOS) i, itemEventTemp );
		}
	}

	return bRet;
}


//----------------------------------------------------------------------
BOOL ItemManagerInstance::ExistEmptySlotAllInventory()
{
	SLOTPOS EmptyPos;

	if ( GetEmptySlotPos(SI_INVENTORY, EmptyPos) )
		return TRUE;

	if ( GetEmptySlotPos(SI_INVENTORY2, EmptyPos) )
		return TRUE;

	return FALSE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::MoveItem	(SLOTINDEX fromIdx
                                    ,SLOTINDEX toIdx
												,SLOTPOS   fromPos
												,SLOTPOS   toPos)
{
	BaseContainer * pFromContainer	= theHero.GetSlotContainer( fromIdx );
	BaseContainer * pToContainer		= theHero.GetSlotContainer( toIdx );


	if( toIdx == SI_EQUIPMENT )
	{
		EquipmentContainer *pContainer=(EquipmentContainer *)pToContainer;
		if ( toPos == EQUIPPOS_WEAPON )
		{
			BaseSlot & slotDat = pContainer->GetSlot(EQUIPPOS_WEAPON);

			if (slotDat.GetCode())
			{
				theHero.UpdateStyleAttributeOut();
			}
		}
	}


	BaseSlot & rTempSlot = pFromContainer->GetSlot(fromPos);


	///////////////////////////////////////////////////////////
	SlotHandle		FromSlot;
	DWORD				dwSlotType;
	if(rTempSlot.GetSlotType() == ST_ITEMHANDLE)
		dwSlotType	= SLOTT_ITEMDUMMY;
	else
		dwSlotType	= SLOTT_ITEM;

	FromSlot.AllocHandle(dwSlotType);

	if(!FromSlot)
		return;

	//////////////////////////////////////////////
	pFromContainer->DeleteSlot( fromPos, FromSlot );

	if( TRUE == pToContainer->IsEmpty( toPos ) )
	{
		pToContainer->InsertSlot( toPos, *FromSlot );
	}
	else
	{
		ItemSlot ToSlot;
		pToContainer->DeleteSlot( toPos, &ToSlot );
		pFromContainer->InsertSlot( fromPos, ToSlot );
		pToContainer->InsertSlot( toPos, *FromSlot );
	}


	////////////////////////////////////////////////////////
	if ( SI_EQUIPMENT == fromIdx)
		PlaySlotSound(SLOTSOUND_UNEQUIP, FromSlot);

	else if (SI_EQUIPMENT == toIdx)
		PlaySlotSound(SLOTSOUND_EQUIP, FromSlot);

	else
		PlaySlotSound(SLOTSOUND_PUT, FromSlot);


	///////////////////////////////////////////////////////////////////
	if( toIdx == SI_EQUIPMENT )
	{
		if ( toPos == EQUIPPOS_WEAPON )
		{
			EquipmentContainer *pContainer=(EquipmentContainer *)pToContainer;

			if(pContainer->GetRealSlotPos(toPos)==EQUIPPOS_WEAPON)
			{
				sSTYLEINFO_BASE *pInfo;
				
				pInfo = theSkillInfoParser.GetStyleInfo( (SLOTCODE)theHero.GetCurrentAttackStyle() );
				if(pInfo)
				{
					if((DWORD)pInfo->m_WeaponDefines != theHero.GetWeaponKind())
					{
						theHero.UpdateStyleAttributeIn();
					}
				}
			}
		}//if ( toPos == EQUIPPOS_WEAPON )
	}//if( toIdx == SI_EQUIPMENT )
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::LinkDummyItem	(SLOTINDEX fromIdx
                                          ,SLOTINDEX toIdx
														,SLOTPOS   fromPos
														,SLOTPOS   toPos	)
{
	ItemLinkHandler	link;
	link.LinkDummyItem	(fromIdx
								,toIdx
								,fromPos
								,toPos	);

}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::TradeBuyItem	(SLOTINDEX /*fromIdx*/
                                       ,SLOTINDEX toIdx
													,SLOTPOS   /*fromPos*/
													,SLOTPOS   toPos)
{
	//BaseContainer * pFromContainer = theHero.GetSlotContainer( fromIdx );
	BaseContainer * pToContainer	 = theHero.GetSlotContainer( toIdx );

	ItemSlot FromSlot;


	if( FALSE == pToContainer->IsEmpty( toPos ) )
	{
		ItemSlot ToSlot;
		pToContainer->InsertSlot( toPos, ToSlot );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::TradeSellItem(SLOTINDEX fromIdx
                                       ,SLOTINDEX /*toIdx*/
													,SLOTPOS   fromPos
													,SLOTPOS   /*toPos*/)
{
	BaseContainer * pFromContainer	= theHero.GetSlotContainer( fromIdx );
	//BaseContainer * pToContainer	= theHero.GetSlotContainer( toIdx );

	ItemSlot FromSlot;
	if( FALSE == pFromContainer->IsEmpty( fromPos ) )
	{
		pFromContainer->DeleteSlot( fromPos, &FromSlot );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::MoveLinkItem(SLOTINDEX slotIdxAt
                                      ,SLOTPOS   fromPos
												  ,SLOTPOS   toQuickPos)
{
	switch (slotIdxAt)
	{
	case SI_QUICK:
		{
			_MoveLinkItem( slotIdxAt, fromPos, toQuickPos );
		}
		break;

	case SI_STYLE:
		{
			_MoveLinkStyle( slotIdxAt, fromPos, toQuickPos );
		}
		break;

	default:
		{
			ASSERT(!"Not invalid slot Container");
		}
		break;
	}	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::_MoveLinkItem(SLOTINDEX slotIdxAt
                                       ,SLOTPOS   fromQuickPos
													,SLOTPOS   toQuickPos)
{
	if (!singleton::ExistHero())
		return;

	BaseContainer * pFromContainer	= theHero.GetSlotContainer( slotIdxAt );
	BaseContainer * pToContainer	= theHero.GetSlotContainer( slotIdxAt );

	/////////////////////////////////////////////////////
	QuickPaneSlot FromSlot;
	pFromContainer->DeleteSlot( fromQuickPos, &FromSlot );
	if( TRUE == pToContainer->IsEmpty( toQuickPos ) )
	{
		pToContainer->InsertSlot( toQuickPos, FromSlot );	
	}
	else
	{
		QuickPaneSlot ToSlot;

		pToContainer->DeleteSlot( toQuickPos, &ToSlot );

		pFromContainer->InsertSlot( fromQuickPos, ToSlot );
		pToContainer->InsertSlot( toQuickPos, FromSlot );
	}

	PlaySlotSound(SLOTSOUND_LINK, (BaseSlot *)&FromSlot);
}

//---------------------------------------------------------------------
VOID ItemManagerInstance::_MoveLinkStyle(SLOTINDEX slotIdxAt
                                        ,SLOTPOS   fromQuickPos
													 ,SLOTPOS   toQuickPos)
{
	if (!singleton::ExistHero()) return;

	BaseContainer * pFromContainer	= theHero.GetSlotContainer( slotIdxAt );
	BaseContainer * pToContainer	= theHero.GetSlotContainer( slotIdxAt );

	StyleSlot FromSlot;
	pFromContainer->DeleteSlot( fromQuickPos, &FromSlot );
	if( TRUE == pToContainer->IsEmpty( toQuickPos ) )
	{
		pToContainer->InsertSlot( toQuickPos, FromSlot );	
	}
	else
	{
		StyleSlot ToSlot;

		pToContainer->DeleteSlot( toQuickPos, &ToSlot );

		pFromContainer->InsertSlot( fromQuickPos, ToSlot );
		pToContainer->InsertSlot( toQuickPos, FromSlot );
	}

	PlaySlotSound(SLOTSOUND_LINK, (BaseSlot *)&FromSlot);
}


//------------------------------------------------------------------------
VOID ItemManagerInstance::LinkItemToQuick	(SLOTINDEX  atIndex
														,SLOTPOS  OrgPos
														,SLOTPOS  toQuickPos
														,SLOTCODE OrgCode)
{
	ItemLinkHandler	link;
	link.LinkItemToQuick	(atIndex
								,OrgPos
								,toQuickPos
								,OrgCode);
}


//---------------------------------------
VOID ItemManagerInstance::LinkSkillToQuick(SLOTINDEX atIndex
                                          ,SLOTCODE  OrgCode
														,SLOTPOS   toQuickPos)
{
	ItemLinkHandler	link;
	link.LinkSkillToQuick	(atIndex
									,OrgCode
									,toQuickPos
		);
}


//---------------------------------------
VOID ItemManagerInstance::LinkStyle(SLOTCODE OrgCode
                                   ,SLOTPOS  toQuickPos)
{
	ItemLinkHandler	link;
	link.LinkStyle	(OrgCode,toQuickPos);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::CopyItem	(SLOTINDEX fromIdx
                                    ,SLOTINDEX toIdx
												,SLOTPOS   fromPos
												,SLOTPOS   toPos)
{
	BaseContainer * pFromContainer = GetContainer( fromIdx );
	BaseContainer * pToContainer	 = GetContainer( toIdx );

    ASSERT (pFromContainer);
    ASSERT (pToContainer);

	BaseSlot &  slotDat	= pFromContainer->GetSlot( fromPos);

	pToContainer->InsertSlot( toPos, slotDat );
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL ItemManagerInstance::CanEquipClass( sITEMINFO_BASE * pInfo ) const
{
	return _BIT_INDEX(pInfo->m_wEquipClass, theHero.GetClass());

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL ItemManagerInstance::CanEquipLevel( sITEMINFO_BASE * pInfo ) const
{
	__CHECK_PTR(pInfo);
	__CHECK(singleton::ExistHero());

	__CHECK(theHero.GetLevel() >= pInfo->m_LimitEqLevel);

	return TRUE;
}



//----------------------------------------------------
BOOL ItemManagerInstance::CanLinkQuick	(SLOTINDEX           /*slotIdxAt*/
                                       ,SLOTPOS             /*ToPos*/
													,const BaseSlot * IN pSlot)
{
	__CHECK_PTR(pSlot);


	if (	ST_ITEM  != pSlot->GetSlotType() 
		&&	ST_SKILL != pSlot->GetSlotType() 
		&&	ST_QUICK != pSlot->GetSlotType())
	{
		OUTPUTCHAT(TEXTRES_QUICK_LINK_ONLY_ITEM_AND_SKILL);
		return FALSE;
	}

	/////////////////////////////////////////
	if (pSlot->GetSlotType() == ST_ITEM)
	{
		ItemSlot *			pItemSlot	= (ItemSlot *)pSlot;
		sITEMINFO_BASE *	pInfo			= pItemSlot->GetItemInfo();

		__CHECK_PTR(pInfo);

		if (pInfo->m_byMaterialType == ITEMUSETYPE_NONE )
		{
			OUTPUTTIP(TEXTRES_ITEM_MATERIAL_TYPE_CAN_EQUIP);
			return FALSE;
		}
	}
	///////////////////////////////////////////////
	else	if (pSlot->GetSlotType() == ST_SKILL)
	{
		sSKILLINFO_BASE *pInfo;
		
		pInfo = (sSKILLINFO_BASE*)theSkillInfoParser.GetInfo(pSlot->GetCode());
	                    					    
		if (pInfo->IsStyle())
		{
			OUTPUTTIP(TEXTRES_STYLE_ONLY_STYLE_SLOT);
			return FALSE;
		}

		if(pInfo->m_bySkillType == SKILL_TYPE_PASSIVE)
		{
			OUTPUTTIP(TEXTRES_ITEM_PASSIVE_CANNT_LINK);
			return FALSE;
		}
	}

	return TRUE;
}


//----------------------------------------------------
BOOL ItemManagerInstance::CanLinkStyleQuick	(SLOTINDEX           /*slotIdxAt*/
                                             ,SLOTPOS             ToPos
															,const BaseSlot * IN pSlot)
{
	__CHECK_PTR(pSlot);


	if (	ST_SKILL			!= pSlot->GetSlotType()
		&&	ST_STYLE_QUICK	!= pSlot->GetSlotType())
	{
		OUTPUTCHAT(TEXTRES_STYLE_SLOT_ONLY_ITEM_AND_SKILL);
		return FALSE;
	}

	if (pSlot->GetSlotType() == ST_SKILL)
	{
		sSKILLINFO_COMMON *pInfo;
		
		pInfo = theSkillInfoParser.GetInfo(pSlot->GetCode());
	                    					    
		if (pInfo->IsSkill())
		{
			OUTPUTCHAT(TEXTRES_STYLE_NOT_QUICK_SLOT);
			return FALSE;
		}
	}

	////////////////////////////////////////////////////////
	StyleContainer * pContainer;
	
	pContainer = (StyleContainer *)GetContainer(SI_STYLE);

	__CHECK(pContainer->IsEmpty(ToPos));

	////////////////////////
	if (ST_STYLE_QUICK == pSlot->GetSlotType())
	{
		StyleSlot * pQuickSlot;
		
		pQuickSlot = (StyleSlot *)pSlot;

		/////////////////////////////
		if (	pContainer->IsExistSameCodeItem(pQuickSlot->GetOrgCode())
			||	pContainer->IsExistSameSerialItem(pQuickSlot->GetSerial()) )
		{
			OUTPUTCHAT(TEXTRES_SLOT_NOT_EMPTY);
			return FALSE;
		}
	}
	else
	{
		if (	pContainer->IsExistSameCodeItem(pSlot->GetCode() )
			||	pContainer->IsExistSameSerialItem(pSlot->GetSerial() ) )
		{
			OUTPUTCHAT(TEXTRES_SLOT_NOT_EMPTY);
			return FALSE;
		}
	}	
	return TRUE;
}


//----------------------------------------------------
BOOL ItemManagerInstance::CanEquip	(SLOTINDEX slotIdxAt
                                    ,SLOTPOS   posIndex
												,SLOTPOS   ToPos)
{
	ASSERT(SI_INVENTORY == slotIdxAt || SI_EQUIPMENT == slotIdxAt);

	__CHECK(singleton::ExistHero());

	BaseContainer * pAtContainer	= theHero.GetSlotContainer( slotIdxAt );

	if( pAtContainer->IsEmpty( posIndex ) ) 
		return FALSE;

	BaseSlot & slotFrom = pAtContainer->GetSlot( posIndex );

	__VERIFY	(slotFrom.GetSlotType() == ST_ITEM
				, "物品须为ST_ITEM类型");

	
	sITEMINFO_BASE * pInfo;
	
	pInfo = theItemInfoParser.GetItemInfo( slotFrom.GetCode() );  
	__CHECK_PTR	(pInfo);
	__CHECK		(pInfo->m_byMaterialType != ITEMUSETYPE_NONE);

	if ( !CanEquipClass( pInfo ) )
	{
		OUTPUTCHAT(TEXTRES_CAN_NOT_EQUIP_CLASS);

		return FALSE;
	}

	///////////////////////////////////////////////
	ItemValidation	valid((ItemSlot &)slotFrom);

	__CHECK ( valid.CanEquipLimit() );

	if(	pInfo->m_wEqPos == EQUIPPOS_RING1
		||	pInfo->m_wEqPos == EQUIPPOS_RING2 )
	{
		__CHECK ( ToPos == EQUIPPOS_RING1 || ToPos == EQUIPPOS_RING2 );
	}
	else 
	{
		__CHECK(pInfo->m_wEqPos == ToPos);
	}

	return TRUE;


}


/*////////////////////////////////////////////////////////////////////////
	检测物品是否存在指定容器中
/*////////////////////////////////////////////////////////////////////////
BOOL ItemManagerInstance::IsExistItem(SLOTINDEX FromSlotIdx, CODETYPE ItemCode, int iItemNum)
{
	BaseContainer * pContainer = GetContainer( FromSlotIdx );
	__CHECK_PTR(pContainer);

	__CHECK(iItemNum > 0);

	sITEMINFO_BASE * pItemInfo = theItemInfoParser.GetItemInfo( ItemCode );
	__CHECK_PTR(pItemInfo);


	BOOL bExist = FALSE;

	for ( int i = 0; i < pContainer->GetSlotMaxSize(); ++i)
	{
		if ( pContainer->IsEmpty((SLOTPOS)i) )
			continue;

		BaseSlot & slotDat = pContainer->GetSlot((SLOTPOS)i);
		if ( slotDat.GetCode() == ItemCode )
		{
			 if ( slotDat.GetNum() >= iItemNum )
			 {
				 bExist = TRUE;
				 break;
			 }
			 else
			 {
				iItemNum -= slotDat.GetNum();
			 }
		}
	}

	return bExist;
}

/*////////////////////////////////////////////////////////////////////////
	消耗物品
/*////////////////////////////////////////////////////////////////////////
BOOL ItemManagerInstance::SpendItem(SLOTINDEX FromSlotIdx, CODETYPE ItemCode, int iItemNum)
{
	BaseContainer * pContainer = GetContainer( FromSlotIdx );
	__CHECK_PTR(pContainer);

	__CHECK(iItemNum > 0);

	sITEMINFO_BASE * pItemInfo = theItemInfoParser.GetItemInfo( ItemCode );
	__CHECK_PTR(pItemInfo);


	BOOL bRet = FALSE;

	for ( int i = 0; i < pContainer->GetSlotMaxSize(); ++i)
	{
		if ( pContainer->IsEmpty((SLOTPOS)i) )
			continue;

		BaseSlot & slotDat = pContainer->GetSlot((SLOTPOS)i);
		if ( slotDat.GetCode() == ItemCode )
		{
			if ( slotDat.GetNum() < iItemNum )
			{
				iItemNum -= slotDat.GetNum();
				pContainer->DeleteSlot((SLOTPOS)i, NULL);
			}
			else if (slotDat.GetNum() == iItemNum)
			{
				pContainer->DeleteSlot((SLOTPOS)i, NULL);
				bRet = TRUE;
				break;
			}
			else
			{
				slotDat.SetNum((DURATYPE)( slotDat.GetNum() - iItemNum) );
				bRet = TRUE;

				pContainer->UpdateSlot((SLOTPOS)i,slotDat);
				break;
			}
		}
	}

	return bRet;
}




/*////////////////////////////////////////////////////////////////////////
	获取窗口中同类物品总量
/*////////////////////////////////////////////////////////////////////////
int ItemManagerInstance::GetItemToltalAmount( CODETYPE code, SLOTINDEX slotIdxAt)
{
	int iCount = 0;

	BaseContainer * pAtContainer = GetContainer(slotIdxAt);

	for (int i = 0; i < pAtContainer->GetSlotMaxSize(); ++i)
	{
		if (pAtContainer->IsEmpty((SLOTPOS)i))
			continue;

		BaseSlot & slotDat = pAtContainer->GetSlot((SLOTPOS)i);

		if ( code != slotDat.GetCode() )
			continue;

		iCount += slotDat.GetNum();
	}

	return iCount;
}

//------------------------------------------------------
BOOL	ItemManagerInstance::LocateItemAtFirst(CODETYPE      code
														 ,SLOTPOS & OUT rOutPos
														 ,SLOTINDEX     slotIdxAt)
{
	BaseContainer * pAtContainer = GetContainer(slotIdxAt); 

	for (int i = 0; i < pAtContainer->GetSlotMaxSize(); ++i)
	{
		if (pAtContainer->IsEmpty((SLOTPOS)i))
			continue;

		BaseSlot & slotDat = pAtContainer->GetSlot((SLOTPOS)i);
		if (slotDat.GetCode() == code)
		{
			rOutPos = (SLOTPOS)i;
			return TRUE;
		}
	}
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
	更新快速栏信息
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::UpdateQuickInfo()
{
	BaseContainer * pAtContainer		= theHero.GetSlotContainer( SI_QUICK);
	BaseContainer * pInvenContainer	= theHero.GetSlotContainer( SI_INVENTORY);
	// skill

	///////////////////////////////////////////////
	// quick slot 
	int i = 0;
	for (i = 0; i < pAtContainer->GetSlotMaxSize(); ++i)
	{
		if (pAtContainer->IsEmpty((SLOTPOS)i))
			continue;

		QuickPaneSlot & slotQuick = (QuickPaneSlot & )pAtContainer->GetSlot((SLOTPOS)i);

		switch ( slotQuick.GetOrgSlotType() )
		{
			/// Quick中物品更新
		case ST_ITEM:
			{
				SLOTPOS FromPos;
				sITEMINFO_BASE * pItemInfo = NULL;

				pItemInfo = theItemInfoParser.GetItemInfo( slotQuick.GetOrgCode() );
				if (!pItemInfo)
					continue;

				/////////////////////////////////////////////////////////
				if ( pItemInfo->IsCanUseWaste() )
				{
					BOOL bRet = LocateItemAtFirst(slotQuick.GetOrgCode(), FromPos);						
					if (!bRet)
					{
						// slotQuick.SetOrgPos(INVALID_QUICKSLOT);
						slotQuick.SetOverlapCount(0);
						slotQuick.SetStatus( eQUICK_DEACTIVATE );
						pAtContainer->UpdateSlot((SLOTPOS)i, slotQuick);
						continue;
					}	

					/////////////////////////////////////////////////////////
					slotQuick.SetOrgPos( FromPos );

					ItemSlot & slotItem = (ItemSlot &)pInvenContainer->GetSlot(FromPos);
					
					slotQuick.SetStatus( eQUICK_ACTIVATE );

					///获取总数量
					if (slotItem.IsOverlap()) 
					{
						int OverlapCount = GetItemToltalAmount(slotQuick.GetOrgCode());

						ASSERT(OverlapCount > 0);
						slotQuick.SetOverlapCount((WORD) OverlapCount );
						slotQuick.SetOrgSlot( &slotItem );
						pAtContainer->UpdateSlot((SLOTPOS)i, slotQuick); // leo quick
					}
				}
				/////////////////////////////////////////
				else if ( pItemInfo->IsCanEquip())
				{
					BaseContainer * pFromContainer = GetContainer(slotQuick.GetOrgSlotIndex());
					if (!pFromContainer)
					{
						slotQuick.SetStatus( eQUICK_DEACTIVATE );
						pAtContainer->UpdateSlot((SLOTPOS)i, slotQuick); // leo quick
						continue;
					}

					if ( pFromContainer->IsEmpty( slotQuick.GetOrgPos() ) )
					{
						slotQuick.SetStatus( eQUICK_DEACTIVATE );
						pAtContainer->UpdateSlot((SLOTPOS)i, slotQuick); // leo quick
						continue;
					}


					BaseSlot & slotOrigin = pFromContainer->GetSlot( slotQuick.GetOrgPos() );
					if (slotOrigin.GetCode() != slotQuick.GetOrgCode() )
					{
						slotQuick.SetStatus( eQUICK_ACTIVATE );

						///交换Quick物品链接，一般在快速栏切换装备之后，需要更新图标
						slotQuick.SetOrgPos			( slotOrigin.GetPos() );
						slotQuick.SetOrgSerial		( slotOrigin.GetSerial() );
						slotQuick.SetOrgSlotType	( slotOrigin.GetSlotType() );
						slotQuick.SetOrgSlotIndex	( slotOrigin.GetSlotIndex() );
						slotQuick.SetOrgCode			( slotOrigin.GetCode() );

						pAtContainer->UpdateSlot((SLOTPOS)i, slotQuick); // leo quick
						//slotQuick.SetStatus( eQUICK_DEACTIVATE );
						continue;
					}

					slotQuick.SetStatus( eQUICK_ACTIVATE );
					pAtContainer->UpdateSlot((SLOTPOS)i, slotQuick); // leo quick
				}
			}
			break;

		//case ST_SKILL:
		//	{
		//	}
		//	break;
		}
	}
}


//----------------------------------------------------
VOID ItemManagerInstance::UpdateQuickForEquips()
{
	BaseContainer * pAtContainer		= theHero.GetSlotContainer( SI_QUICK);
	BaseContainer * pEquipContainer	= theHero.GetSlotContainer( SI_EQUIPMENT);

	ASSERT(pAtContainer);

	if (!singleton::ExistHero())
		return;

	for (int i = 0; i < pAtContainer->GetSlotMaxSize(); i++)
	{
		if ( pAtContainer->IsEmpty((SLOTPOS)i) )
			continue;

		QuickPaneSlot & slotQuick = (QuickPaneSlot &)pAtContainer->GetSlot((SLOTPOS)i);

		if (slotQuick.GetOrgSlotType() != ST_ITEM)
			continue;

		sITEMINFO_BASE * pItemInfo = theItemInfoParser.GetItemInfo( slotQuick.GetOrgCode() );
		ASSERT(pItemInfo);
		if (!pItemInfo)
		{
			LOGINFO("UpdateQuickForEquipItem:: failed!!");
			continue;
		}

		if (!pItemInfo->IsCanEquip())
			continue;

		SLOTINDEX OrgSlotIdx	= slotQuick.GetOrgSlotIndex();
		SLOTPOS OrgPos			= slotQuick.GetOrgPos();
		BaseContainer * pOrgContainer = GetContainer( OrgSlotIdx );
		if ( !pOrgContainer )
			continue;


		/////////////////////////////////////////////////
		BaseSlot & slotDat = pOrgContainer->GetSlot( OrgPos );
		if ( slotDat.GetCode() != slotQuick.GetOrgCode() )
		{
			if ( !pEquipContainer->IsEmpty( (SLOTPOS)pItemInfo->m_wEqPos ) )
			{
				BaseSlot & slotDat = pEquipContainer->GetSlot( (SLOTPOS)pItemInfo->m_wEqPos );
				if ( slotDat.GetCode() == slotQuick.GetOrgCode() )
				{
					slotQuick.SetOrgSlotIndex	(SI_EQUIPMENT );
					slotQuick.SetOrgPos			( (SLOTPOS)pItemInfo->m_wEqPos );
					ASSERT( !pEquipContainer->IsEmpty( (SLOTPOS)pItemInfo->m_wEqPos ) );

					slotQuick.SetStatus			( eQUICK_ACTIVATE );
					continue;
				}
			}
			
			////////////////////////////////////////////////////////
			BOOL bRet;
			
			bRet = LocateItemAtFirst( slotQuick.GetOrgCode(), OrgPos );
			if (bRet)
			{
				slotQuick.SetOrgSlotIndex( SI_INVENTORY );
				slotQuick.SetOrgPos( OrgPos );
				slotQuick.SetStatus( eQUICK_ACTIVATE );
			}
			else
			{
				slotQuick.SetStatus( eQUICK_DEACTIVATE );
				slotQuick.SetOrgSlot(NULL);
			}
		}
		else
		{
			slotQuick.SetStatus( eQUICK_ACTIVATE );
		}
	}
}


//----------------------------------------------------
VOID ItemManagerInstance::UpdateQuickItems()
{
	BaseContainer * pAtContainer		= theHero.GetSlotContainer( SI_QUICK);

	ASSERT(pAtContainer);

	if (!singleton::ExistHero())
		return;

	for (int i = 0; i < pAtContainer->GetSlotMaxSize(); i++)
	{
		if ( pAtContainer->IsEmpty((SLOTPOS)i) )
		{
			continue;
		}

		QuickPaneSlot & slotQuick = (QuickPaneSlot &)pAtContainer->GetSlot((SLOTPOS)i);

		if ( ST_ITEM != slotQuick.GetOrgSlotType() )
			continue;

		BaseContainer * pFromContainer;
		
		pFromContainer = GetContainer(slotQuick.GetOrgSlotIndex());
		if (!pFromContainer)
		{
			slotQuick.SetStatus( eQUICK_DEACTIVATE );
			continue;
		}

		if ( pFromContainer->IsEmpty( slotQuick.GetOrgPos() ) )
		{
			slotQuick.SetStatus( eQUICK_DEACTIVATE );
			continue;
		}

		BaseSlot & slotOrigin = pFromContainer->GetSlot( slotQuick.GetOrgPos() );
		if (slotOrigin.GetCode() != slotQuick.GetOrgCode() )
		{
			slotQuick.SetStatus( eQUICK_DEACTIVATE );
			continue;
		}

		slotQuick.SetStatus( eQUICK_ACTIVATE );
	}
}


VOID ItemManagerInstance::EquipItem( SLOTINDEX /*slotIdxAt*/, SLOTPOS /*posIndex*/ )
{
	//	BaseContainer * pAtContainer	= theHero.GetSlotContainer( slotIdxAt );
	//	BaseSlot * pFromSlot				= const_cast<BaseSlot *>(pAtContainer->GetSlot( posIndex ));
	//
	//	sITEMINFO_BASE * pInfo = theItemInfoParser.GetItemInfo( pFromSlot->GetCode() );
	//	switch( (eEQUIP_POS_INDEX)pInfo->m_wEqPos )
	//	{
	//	case EQUIPPOS_PROTECTOR:
	//		break;
	//
	//	case EQUIPPOS_RING1:
	//		break;
	//
	//	case EQUIPPOS_RING2:
	//		break;
	//
	//	case EQUIPPOS_WEAPON1LEFT:
	//		break;
	//	case EQUIPPOS_WEAPON2LEFT:
	//		break;
	//	case EQUIPPOS_SHIRTS:
	//		break;
	//	case EQUIPPOS_BOOTS:
	//		break;
	//	case EQUIPPOS_PANTS:
	//		break;
	//	case EQUIPPOS_BELT:
	//		break;
	//	case EQUIPPOS_HELMET:
	//		break;
	//	case EQUIPPOS_ARMOR:
	//		break;
	//	case EQUIPPOS_GLOVE:
	//		break;
	//	case EQUIPPOS_NECKLACE:
	//		break;
	//	case EQUIPPOS_ARROW:
	//		break;
	//	case EQUIPPOS_CAPE:
	//		break;
	//	case EQUIPPOS_CREST:
	//		break;
	//	case EQUIPPOS_STONE1:
	//		break;
	//	case EQUIPPOS_STONE2:
	//		break;
	//	case EQUIPPOS_STONE3:
	//		break;
	//	}

	//	case EQPOS_PROTECTOR:
	//	case EQPOS_RING:
	//		break;
	//	case EQPOS_ONEWEAPON:
	//		{
	////			BaseContainer * pEquipContainer	= theHero.GetSlotContainer( SI_EQUIPMENT );
	////			SLOTPOS toPos = 0;
	////			if( pEquipContainer->IsEmpty( EQUIPPOS_WEAPONLEFT ) )
	////				toPos = EQUIPPOS_WEAPONLEFT;
	////			else if( pEquipContainer->IsEmpty( EQUIPPOS_WEAPONRIGHT ) )
	////				toPos = EQUIPPOS_WEAPONRIGHT;
	////			else
	////			{
	////				toPos = EQUIPPOS_WEAPONLEFT;
	////			}
	////			SendItemMoveMsg( slotIdxAt, SI_EQUIPMENT, posIndex, toPos );
	//
	//		}
	//		break;
	//	case EQPOS_BOTHWEAPON:
	//		{
	////			BaseContainer * pEquipContainer	= theHero.GetSlotContainer( SI_EQUIPMENT );
	////			SLOTPOS toPos = 0;
	////			if( pEquipContainer->IsEmpty( EQUIPPOS_WEAPONLEFT ) )
	////				toPos = EQUIPPOS_WEAPONLEFT;
	////			else
	////			{
	////				toPos = EQUIPPOS_WEAPONLEFT;
	////			}
	////			SendItemMoveMsg( slotIdxAt, SI_EQUIPMENT, posIndex, toPos );
	//		}
	//		break;
	//	case EQPOS_SHIRTS:
	//	case EQPOS_BOOTS:
	//	case EQPOS_PANTS:
	//	case EQPOS_BELT:
	//	case EQPOS_HELMET:
	//	case EQPOS_ARMOR:
	//	case EQPOS_GLOVE:
	//	case EQPOS_NECKLACE:
	//	case EQPOS_ARROW:
	//	case EQPOS_CAPE:
	//	case EQPOS_MARK:
	//	case EQPOS_STONE:
	//	default:
	//		{
	//			ASSERT( !"Invalid." );
	//		}
	//	}
}


VOID ItemManagerInstance::UseEquipmentItem( SLOTINDEX slotIdxAt, SLOTPOS posIndex )
{
	if (!singleton::ExistHero())		return;
	if( slotIdxAt != SI_EQUIPMENT )	return;

	BaseContainer * pAtContainer;
	BaseContainer * pToContainer;
	
	pAtContainer	= theHero.GetSlotContainer( slotIdxAt );
	__CHECK2(!pAtContainer->IsEmpty( posIndex ),);


	pToContainer	= theHero.GetSlotContainer( SI_INVENTORY );
	SLOTPOS emptyPos = 0;
	if( pToContainer->GetEmptyPos( emptyPos ) )
	{
		theItemManagerNet.SendItemMoveMsg( slotIdxAt, SI_INVENTORY, posIndex, emptyPos );
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::UseInventoryItem( SLOTINDEX slotIdxAt, SLOTPOS posIndex )
{
	BaseContainer *	pAtContainer;
	sITEMINFO_BASE *	pInfo;
	DWORD					dwItemCode;
	
	pAtContainer	= theHero.GetSlotContainer( slotIdxAt );
	__CHECK2(!pAtContainer->IsEmpty( posIndex ),);

	////////////
	BaseSlot & slotFrom		= pAtContainer->GetSlot( posIndex );
	
	pInfo		= theItemInfoParser.GetItemInfo( slotFrom.GetCode() );
	__CHECK2_PTR(pInfo,);

	dwItemCode = slotFrom.GetCode();
	if(pInfo->IsCanEquip())
	{
		EquipItem( slotIdxAt, posIndex );
	}
	else if(pInfo->IsCanUseWaste())
	{
		WORD wCount;
		
		wCount = (WORD)slotFrom.GetNum();
		if (wCount  > 1)
		{
			slotFrom.SetNum((DURATYPE)--wCount);
			pAtContainer->UpdateSlot(posIndex, slotFrom);
		}
		else
		{
			ItemSlot itemSlot;
			pAtContainer->DeleteSlot(posIndex, &itemSlot);
		}
	}
	else// if(pInfo->IsCanCompound())
	{
		assert(0);
	}


	///////////////////////////////////////////////////
	theHero.OnUseItem(dwItemCode);

	//////////////////////////////////////////////////
	//启动冷却时间
	if(pInfo)
	{	
		util::Timer *pTimer = theHero.GetPlayerCookie().GetItemCoolTimer(pInfo->m_byWasteType);
		if (pTimer) 
		{
			pTimer->SetTimer(pInfo->m_dCoolTime);
			//pTimer->Reset();
		}
	}
}


/*////////////////////////////////////////////////////////////////////////
物品掉落到地上
/*////////////////////////////////////////////////////////////////////////
BOOL ItemManagerInstance::CreateFieldItem( const sITEMINFO_RENDER * pItemInfo )
{
	sITEMINFO_RENDER::eFIELDITEMTYPE eFieldItemType;
	Vector3D			vVec;
	DWORD				dwObjectKey	= 0;

	
	eFieldItemType	= 	sITEMINFO_RENDER::eFIELDITEM_ITEM;

	dwObjectKey		= pItemInfo->m_dwObjectKey;
	eFieldItemType = (sITEMINFO_RENDER::eFIELDITEMTYPE)pItemInfo->m_byFieldItemType;


	if ( theObjectManager.GetObject(dwObjectKey) )
	{
		ASSERT(!"Already exist item ?!!");
		return FALSE;
	}


	vVec.Set(pItemInfo->m_fPos);

	Item * pItem = NULL;

	///////////////////////////////////////////////////////
	if (eFieldItemType == sITEMINFO_RENDER::eFIELDITEM_ITEM)
	{
		pItem = (Item *)theObjectManager.Add(dwObjectKey
                                          ,ITEM_OBJECT
														,pItemInfo->m_ItemStream.ItemPart.wCode);
		pItem->SetItem( (ITEMOPT_STREAM&)pItemInfo->m_ItemStream );
	}
	else
	{
		pItem = (Item *)theObjectManager.Add(dwObjectKey
                                          ,ITEM_OBJECT
														,ITEMCATEGORY_MONEY);
		pItem->SetMoney(pItemInfo->m_Money);
	}
	
	pItem->SetName("");
	pItem->SetPosition(vVec);
	pItem->SetDropType( pItemInfo->m_byFieldItemType );

	pItem->SetOwnerKey(pItemInfo->m_dwOwnerPlayerKey);

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemManagerInstance::ProcessItemError(DWORD dwErrCode)
{
	DWORD dwTextID = 0;

	#define _CASE_TEXT(c,t)		case c: dwTextID = (t); break

	//#define _SET_TEXTID(x)		dwTextID = (x)
	//	case[:b]+{[:a_]+}[:b]*\:{[:b]*}_SET_TEXTID\({[:a_\+]+}\)[^\n]*\break;

	switch ((eITEM_RESULT)dwErrCode)
	{
	_CASE_TEXT(RC_ITEM_INVALIDSTATE,							TEXTRES_RC_ITEM_INVALIDSTATE);
	_CASE_TEXT(RC_ITEM_INVALIDPOS,							TEXTRES_RC_ITEM_INVALIDPOS);
	_CASE_TEXT(RC_ITEM_EMPTYSLOT,								TEXTRES_TARGET_SLOT_IS_EMPTY);
	_CASE_TEXT(RC_ITEM_NOTEXISTITEMATFIELD,				TEXTRES_RC_ITEM_NOTEXISTITEMATFIELD);
	_CASE_TEXT(RC_ITEM_NOINFO,									TEXTRES_RC_ITEM_NOINFO);
	_CASE_TEXT(RC_ITEM_NOSPACEININVENTORY,					TEXTRES_RC_ITEM_NOSPACEININVENTORY);
	_CASE_TEXT(RC_ITEM_INVALIDSTATEOFPLAYER,				TEXTRES_RC_ITEM_INVALIDSTATEOFPLAYER);
	_CASE_TEXT(RC_ITEM_CANNT_REPAIR_DURA,					TEXTRES_RC_ITEM_CANNT_REPAIR_DURA);
	_CASE_TEXT(RC_ITEM_CANNT_REPAIR_AT_CONTAINER,		TEXTRES_RC_ITEM_CANNT_REPAIR_AT_CONTAINER);
																	
	_CASE_TEXT(RC_ITEM_INVALIDPOSFORDROPITEM,				TEXTRES_RC_ITEM_INVALIDPOSFORDROPITEM);
	_CASE_TEXT(RC_ITEM_DBP_ASYNC,								TEXTRES_RC_ITEM_DBP_ASYNC);
	_CASE_TEXT(RC_ITEM_COOLTIME_ERROR,						TEXTRES_RC_ITEM_COOLTIME_ERROR);
	_CASE_TEXT(RC_ITEM_ITEMCODENOTEQUAL,					TEXTRES_RC_ITEM_ITEMCODENOTEQUAL);
	_CASE_TEXT(RC_ITEM_ISNOTWASTEITEM,						TEXTRES_RC_ITEM_ISNOTWASTEITEM);
	_CASE_TEXT(RC_ITEM_INVALIDSHOPLISTID,					TEXTRES_RC_ITEM_INVALIDSHOPLISTID);
	_CASE_TEXT(RC_ITEM_OUTOFSHOPITEMINDEX,					TEXTRES_RC_ITEM_OUTOFSHOPITEMINDEX);
	_CASE_TEXT(RC_ITEM_HAVENOTMONEY,							TEXTRES_RC_ITEM_HAVENOTMONEY);
	_CASE_TEXT(RC_ITEM_ISNOTEMPTYSLOT,						TEXTRES_RC_ITEM_ISNOTEMPTYSLOT);
																	
	_CASE_TEXT(RC_ITEM_HAVENOTSPACE,							TEXTRES_RC_ITEM_HAVENOTSPACE);
	_CASE_TEXT(RC_ITEM_INVALIDVALUE,							TEXTRES_RC_ITEM_INVALIDVALUE);
	_CASE_TEXT(RC_ITEM_ERRORDROPMONEY,						TEXTRES_RC_ITEM_ERRORDROPMONEY);
	_CASE_TEXT(RC_ITEM_ENCHANT_DOWN_FAILED,				TEXTRES_RC_ITEM_ENCHANT_DOWN_FAILED);
	_CASE_TEXT(RC_ITEM_ENCHANT_FAILED,						TEXTRES_NOTICE_REASON_ITEM_ERROR_CODE+dwErrCode);
	_CASE_TEXT(RC_ITEM_ENCHANT_HAVENOTMATERIALS,			TEXTRES_RC_ITEM_ENCHANT_HAVENOTMATERIALS);
																	
	// rankup												
	_CASE_TEXT(RC_ITEM_INVALID_CONDITION,					TEXTRES_RC_ITEM_INVALID_CONDITION);
	_CASE_TEXT(RC_ITEM_CANNT_RANKUP_ITEM,					TEXTRES_RC_ITEM_CANNT_RANKUP_ITEM);
	_CASE_TEXT(RC_ITEM_FULLSOCKET,							TEXTRES_RC_ITEM_FULLSOCKET);
	_CASE_TEXT(RC_ITEM_EXTRACTSOCKET_FAILED,				TEXTRES_RC_ITEM_EXTRACTSOCKET_FAILED);

	_CASE_TEXT(RC_ITEM_MAKE_NOINFO,							TEXTRES_RC_ITEM_MAKE_NOINFO);
	_CASE_TEXT(RC_ITEM_MAKE_INVALID_ITEMLEVEL,			TEXTRES_RC_ITEM_MAKE_INVALID_ITEMLEVEL);
	_CASE_TEXT(RC_ITEM_MAKE_INVALID_ROWNUM,				TEXTRES_RC_ITEM_MAKE_INVALID_ROWNUM);
	_CASE_TEXT(RC_ITEM_MAKE_INVALID_MATERIALSLOTINFO,	TEXTRES_RC_ITEM_MAKE_INVALID_MATERIALSLOTINFO);

	_CASE_TEXT(RC_ITEM_PLAYER_HP_FULL,						TEXTRES_PLAYER_HP_FULL);
	_CASE_TEXT(RC_ITEM_PLAYER_MP_FULL,						TEXTRES_PLAYER_MP_FULL);
	_CASE_TEXT(RC_ITEM_PLAYER_HPMP_FULL,					TEXTRES_PLAYER_MP_FULL);

	case RC_ITEM_UNKNOWNERROR:			
	default:
		OUTPUTTIP (TEXTRES_RC_ITEM_UNKNOWNERROR);
		return;
	}

	if(dwTextID == 0)
		return;

	///内容输出...
	OUTPUTTIP(dwTextID);

	switch ((eITEM_RESULT)dwErrCode)
	{	
	case RC_ITEM_HAVENOTMONEY:
		{
			theHeroActionInput.PlayNotEnoughMoneySound();
		}
		break;
	}

}



//----------------------------------------------------
SkillSlot * ItemManagerInstance::GetSkillSlot(CODETYPE code)
{
	 return theSkillStorageManager.GetSkillSlot(code);
}


//////////////////////////////////////////////////////////////////////////
BOOL ItemManagerInstance::IsExistItemDup	(SLOTINDEX         idx
														,SLOTCODE        code
														,BYTE            num
														,SLOTPOS * OUT   pPosArray
														,SLOTPOS & INOUT posNum)
{
	SLOTPOS maxPos = posNum;
	BaseContainer * pContainer = GetItemSlotContainer( idx );
	posNum = 0;
	for( SLOTPOS i = 0 ; i < pContainer->GetSlotMaxSize() ; ++i )
	{
		if( !pContainer->IsEmpty((SLOTPOS)i) )
		{
			BaseSlot & slot = pContainer->GetSlot((SLOTPOS)i);
			if( slot.GetCode() == code )
			{
				if( maxPos < posNum ) return FALSE;
				pPosArray[posNum++] = i;
				if( posNum == num ) return TRUE;
			}
		}
	}
	return FALSE;
}


VOID ItemManagerInstance::CanInsertItem	(BaseContainer * pContainer
                                          ,SLOTPOS         MAX_DUP_NUM
														,SLOTPOS & INOUT need_num
														,SLOTPOS * OUT   PosArray
														,SLOTPOS & OUT   PosNum)
{
	if( need_num == 0 ) return;
	for( SLOTPOS pos = 0 ; pos < pContainer->GetSlotMaxSize() ; ++pos )
	{
		if( pContainer->IsEmpty(pos) )
		{
			PosArray[PosNum++] = pos;
			if( need_num > MAX_DUP_NUM)
				need_num = need_num - MAX_DUP_NUM;
			else
				need_num = 0;

			if( need_num == 0 ) break;
		}
	}
}


BOOL ItemManagerInstance::IsExistItem	(SLOTINDEX         idx
													,SLOTCODE        code
													,DURATYPE        dupNum
													,SLOTPOS * OUT   pPosArray
													,SLOTPOS * INOUT pPosNum)
{
	BYTE		InsufficientNum	= dupNum;
	SLOTPOS	maxPos(0);

	if(pPosNum)
	{
		maxPos	= *pPosNum;
		*pPosNum	= 0;
	}

	BaseContainer * pContainer = GetItemSlotContainer( idx );
	for( SLOTPOS i = 0 ; i < pContainer->GetSlotMaxSize() ; ++i )
	{
		if( !pContainer->IsEmpty((SLOTPOS)i) )
		{
			BaseSlot & slot = pContainer->GetSlot((SLOTPOS)i);
			if( slot.GetCode() == code )
			{
				if(pPosNum)
				{
					if( maxPos < *pPosNum ) return FALSE;
					pPosArray[(*pPosNum)++] = i;
				}
				ASSERT( slot.GetNum()!= 0 );

				if( InsufficientNum <= slot.GetNum() ) return TRUE;

				InsufficientNum = InsufficientNum - slot.GetNum();
			}
		}
	}

	return FALSE;
}

BOOL ItemManagerInstance::IsExistItem( SLOTINDEX Index, SLOTCODE code )
{
	BaseContainer * pContainer = GetItemSlotContainer(Index);
	for( SLOTPOS pos = 0 ; pos < pContainer->GetSlotMaxSize() ; ++pos )
	{
		const BaseSlot & slotDat = pContainer->GetSlot(pos);
		if( slotDat.GetCode() == code )
		{
			return TRUE;
		}
	}

	return FALSE;
}



RC::eITEM_RESULT		ItemManagerInstance::Lose( SLOTCODE /*ItemCode*/, BOOL /*bAllOfItem*/ )
{
	return RC::RC_ITEM_SUCCESS;
}

VOID ItemManagerInstance::Lose( BaseContainer * /*pContainer*/, SLOTPOS /*pos*/, BYTE /*num*/ )
{
}

VOID ItemManagerInstance::Lose( SLOTINDEX slotIdx, SLOTPOS pos, BYTE num )
{
	BaseContainer * pContainer = GetItemSlotContainer( slotIdx );

	ItemSlot & rSlotItem = (ItemSlot &)pContainer->GetSlot(pos);


	MSG_CG_ITEM_LOSE_ACK msg;

	msg.m_ItemPos = pos;
	if( rSlotItem.GetNum() > num )
	{
		msg.m_ItemNum = rSlotItem.GetNum() - num;
		//rSlotItem.SetNum( rSlotItem.GetNum() - num );
	}
	else
	{
		msg.m_ItemNum = 0;
		//pContainer->DeleteSlot( rSlotItem.GetPos(), NULL );
	}

	/*m_pPlayer->*/theHero.SendPacket( &msg, sizeof(msg) );
}



RC::eITEM_RESULT ItemManagerInstance::Obtain( SLOTCODE ItemCode, SLOTPOS num, sTOTALINFO_INVENTORY * OUT pTotalInfo )
{
	ItemSlot itemSlot;
	itemSlot.SetCode( ItemCode );
	return Obtain( itemSlot, num, pTotalInfo );
}


RC::eITEM_RESULT ItemManagerInstance::Obtain( ItemSlot & IN slot, SLOTPOS num, sTOTALINFO_INVENTORY * OUT pTotalInfo )
{
	if( pTotalInfo )
	{
		pTotalInfo->m_InvenCount	= 0;
		pTotalInfo->m_TmpInvenCount	= 0;
	}

	/////////////////////////////////////////////////////
	//输出物品
	//resultItems.m_InvenCount			= 1;
	//resultItems.m_TmpInvenCount		= 0;
	//resultItems.m_Slot[0].m_ItemPos	= mainPos;
	//itemResult.CopyOut(resultItems.m_Slot[0].m_Stream);



	if( !ValidState() ) 
		return RC::RC_ITEM_INVALIDSTATE;

	//if( slot.IsOverlap() )
	//	return RC::RC_ITEM_INVALIDSTATE;

	BaseContainer * pInventory = GetItemSlotContainer( SI_INVENTORY );
	BaseContainer * pTmpInventory = GetItemSlotContainer( SI_INVENTORY2 );

	//ASSERT( num > 0 );
	////ASSERT( slot.GetSerial() != TEMP_SERIALTYPE_VALUE );


	int need_num = num;
	need_num -= pInventory->GetSlotMaxSize() - pInventory->GetSlotNum();
	if( need_num > 0 )
	{
		need_num -= pTmpInventory->GetSlotMaxSize() - pTmpInventory->GetSlotNum();
		if( need_num > 0 )
			return RC::RC_ITEM_NOSPACEININVENTORY;
	}

	BYTE total = 0;


	need_num = num;
	for( SLOTPOS pos = 0 ; pos < pInventory->GetSlotMaxSize() ; ++pos )
	{
		if( pInventory->IsEmpty(pos) )
		{
			--need_num;
			if( pTotalInfo )
			{
				pTotalInfo->m_Slot[total].m_ItemPos	= pos;
				slot.CopyOut(pTotalInfo->m_Slot[total].m_Stream);
				++total;
				++pTotalInfo->m_InvenCount;
			}
			if( need_num == 0 ) break;
		}
	}

	if( need_num != 0 )
	{
		for( SLOTPOS pos = 0 ; pos < pTmpInventory->GetSlotMaxSize() ; ++pos )
		{
			if( pTmpInventory->IsEmpty(pos) )
			{
				if( pTotalInfo )
				{
					pTotalInfo->m_Slot[total].m_ItemPos	= pos;
					slot.CopyOut(pTotalInfo->m_Slot[total].m_Stream);
					++total; ++pTotalInfo->m_TmpInvenCount;
				}
				--need_num;
				if( need_num == 0 ) break;
			}
		}
	}

	ASSERT( need_num == 0 );

	return RC::RC_ITEM_SUCCESS;
}

RC::eITEM_RESULT		ItemManagerInstance::MakeItem		(SLOTCODE					destItemCode
																		 ,DWORD						dwMaterialNums
																		 ,BYTE						byMaterialRow
																		 ,SLOTPOS*					parMaterialPos
																		 ,LPCSTR						szItemName
																		 ,sTOTALINFO_INVENTORY&	resultItems		)
{
	__UNUSED(destItemCode);
	__UNUSED(dwMaterialNums);
	__UNUSED(byMaterialRow);
	__UNUSED(parMaterialPos);
	__UNUSED(szItemName);
	__UNUSED(resultItems		);

	return RC_ITEM_FAILED;
}


#include "stdafx.h"
#include "EquipmentContainer.h"	
#include "AttributeAffectorItemHandle.h"
#include "Player.h"
#include "ObjectManager.h"
#include "SlotKeyGenerator.h"

using namespace object;

BASECONTAINER_IMPL(EquipmentContainer);

EquipmentContainer::EquipmentContainer()
{
	//由调用者调用
	//Init(MAX_EQUIPMENT_SLOT_NUM, SI_EQUIPMENT);
}

EquipmentContainer::~EquipmentContainer()
{
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL EquipmentContainer::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )
{
	// 
	SERIALTYPE	dwSerial;
	Player *		pPlayer;
	
	dwSerial = theSlotKeyGenerator.GetKey();
	slotDat.SetSerial(dwSerial);

	pPlayer = (Player*)theObjectManager.GetObject(GetOwnerKey());

	__CHECK_PTR(pPlayer);

	AttributeAffectorItemHandle itemCalc(*pPlayer->GetPlayerAttributes()
                                       ,*this);
	itemCalc.Equip( slotDat );

	BaseContainer::InsertSlot( posIndex, slotDat );

	pPlayer->UpdateSkillAttributes();
	pPlayer->UpdateAppearance();

	return TRUE;

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID EquipmentContainer::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	// render unit 
	BaseSlot & slotDat = GetSlot(posIndex);

	theSlotKeyGenerator.Restore(slotDat.GetSerial());

	Player *pPlayer;
	pPlayer = (Player *)theObjectManager.GetObject(GetOwnerKey());

	__CHECK2_PTR(pPlayer,);

	
	AttributeAffectorItemHandle itemCalc( *pPlayer->GetPlayerAttributes()
													, *this );

	if (!IsEmpty(posIndex))
	{ 		
		BaseSlot & slotDat = GetSlot(posIndex);
		itemCalc.UnEquip( &slotDat );
	}

	BaseContainer::DeleteSlot( posIndex, pSlotOut );

	pPlayer->UpdateSkillAttributes();
	pPlayer->UpdateAppearance();

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID EquipmentContainer::UpdateSlot( SLOTPOS posIndex,BaseSlot & slotDat )
{
	Player *pPlayer;
	pPlayer = (Player *)theObjectManager.GetObject(GetOwnerKey());

	__CHECK2_PTR(pPlayer,);

	BaseSlot & slot = GetSlot(posIndex);
	slot.Copy(slotDat);

	pPlayer->UpdateSkillAttributes();
	pPlayer->UpdateAppearance();
}


//
SLOTPOS	EquipmentContainer::GetRealSlotPos( SLOTPOS pos )
{
	return (SLOTPOS)(GetCurrentPage() * GetSlotNumPerPage() + pos);
}





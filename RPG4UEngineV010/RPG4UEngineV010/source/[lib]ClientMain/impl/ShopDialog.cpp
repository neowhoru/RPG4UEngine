/*////////////////////////////////////////////////////////////////////////
文 件 名：ShopDialog.cpp
创建日期：2008年6月10日
最后更新：2008年6月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <BaseContainer.h>

#include "ContainerDialogBase.h"
#include "shopdialog.h"
#include "SlotUIListener.h"
#include "MouseHandler.h"
#include "ItemManager.h"
#include "ShopInfoParser.h"
#include "ItemInfoParser.h"
#include "Mouse.h"
#include <ItemSlot.h>
#include "HeroTipLayer.h"
#include "KeyQueueManager.h"
#include "ConstTextRes.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ShopDialog, ()  , gamemain::eInstPrioGameFunc);

ShopDialog::ShopDialog(void)
{
	m_pSlotListener		= NULL;
	m_TabNum					= 0;
	m_bStarted				= FALSE;
}

ShopDialog::~ShopDialog(void)
{
}


BOOL ShopDialog::Init(SLOTPOS MaxSlotSize, SLOTINDEX SlotIdx  )
{
	__BOOL_SUPER(Init( MaxSlotSize, SlotIdx  ));

	m_TabNum				= 0;
	m_eShopMethod		= SHOP_METHOD_NONE;

	//ItemSlotContainer::Init(MAX_VENDOR_SLOT_NUM, SI_NPCSHOP);
	InitContainer();

	return TRUE;
}

VOID ShopDialog::InitContainer()
{
}

VOID ShopDialog::ReleaseContainer()
{
	FlushSlotListener();
}


VOID ShopDialog::Release()
{
	ReleaseContainer();
	_SUPER::Release();
}


VOID ShopDialog::FlushSlotListener()
{
	if (!GetSlotMaxSize())
		return;

	ItemSlot fromSlot;

	for (SLOTPOS i = 0; i < GetSlotMaxSize(); i++)
	{
		if (!IsEmpty(i))
		{
			DeleteSlot(i, &fromSlot);
		}
	}
}



//----------------------------------------------------------------------------
VOID ShopDialog::NetworkProc( MSG_BASE * /*pMsg*/ )
{
}


BOOL ShopDialog::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )
{
	BaseContainer::InsertSlot( posIndex, slotDat );

	BaseSlot & rRealSlot = GetSlot(posIndex);
	if(m_pSlotListener)
		m_pSlotListener->OnSlotAdd(rRealSlot);

	UpdateSlotListener();
	return TRUE;
}


VOID ShopDialog::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	if(pSlotOut)
	{
		pSlotOut->SetSerial(0);
		if (m_pSlotListener)
			m_pSlotListener->OnSlotRemove( *pSlotOut );
	}

	BaseContainer::DeleteSlot( posIndex, pSlotOut );

	UpdateSlotListener();
}



VOID ShopDialog::Update()
{
	if(m_pSlotListener)
		m_pSlotListener->Update();
}


VOID	ShopDialog::SetCheckTrade(eSHOP_METHOD eShopMethod )
{
	m_eShopMethod = eShopMethod;
	
	DWORD dwTextID = INVALID_DWORD_ID;
	DWORD dwMouse	= INVALID_DWORD_ID;
	switch(eShopMethod)
	{
	case VENDOR_METHOD_BUY:
		{
			dwTextID = TEXTRES_SELECT_BUY_ITEM; 
			dwMouse	= eMOUSE_VENDOR_BUY;
		}break;

	case SHOP_METHOD_BUY:	
		{
			dwTextID = TEXTRES_SELECT_BUY_ITEM; 
			dwMouse	= eMOUSE_BUY;
		}break;
	case SHOP_METHOD_SELL:	
		{
			dwTextID = TEXTRES_SELECT_SELL_ITEM; 
			dwMouse	= eMOUSE_SELL;
		}break;
	case SHOP_METHOD_REPAIR:
		{
			dwTextID = TEXTRES_SELECT_REPAIR_ITEM;
			dwMouse	= eMOUSE_REPAIR;
		}break;
	}

	if(dwTextID != -1)
	{
		OUTPUTTIP(dwTextID);
	}
	theMouseHandler.SetMouseState((MOUSE_STATE) dwMouse);

}


VOID ShopDialog::RepairAll()
{
	OUTPUTTIP(TEXTRES_REPAIR_ALL_ITEM);
	theItemManagerNet.SendRepairEquipsMsg();
}


void ShopDialog::CloseWindow()
{
	if (theMouseHandler.IsMouseItemFrom(SI_NPCSHOP))
	{
		theMouseHandler.CancelHandlingItem();
		//theMouseHandler.DoItemDrop();
	}
	ShowWindow(FALSE);
}


VOID ShopDialog::ShowWindow(BOOL val)
{
	m_bStarted	=  val;

	//////////////////////////////////////////////
	theGameUIManager.UISetData	(gameui::eUIShop
										,gameui::eSetVisible
										,val);

	//////////////////////////////////////////////
	if (val)
	{
		if ( !m_pSlotListener )
			InitContainer();

		SettingShop();
		theGameUIManager.UITriggerFunc	(gameui::eUIShop
													,gameui::eChangePage
													,GetCurrentPage());

		m_eShopMethod = SHOP_METHOD_NONE;
		SetCheckTrade(SHOP_METHOD_NONE);

		UpdateSlotListener();

		theKeyQueueManager.PushBack	(ESC_PROC_DIALOG_SHOP
											,GlobalUtil::DoShowWindow
											,(DWORD)(ContainerDialogBase*)this);
	}
	else
	{
		ReleaseContainer();

		m_TabNum = (SLOTPOS)(GetCurrentPage() * ShopDetailInfo::MAX_SELL_ITEM); 

		m_eShopMethod = SHOP_METHOD_NONE;

		theMouseHandler.RemoveMouseState(eMOUSE_BUY);
		theMouseHandler.RemoveMouseState(eMOUSE_SELL);
		
		theKeyQueueManager.DeleteMsg(ESC_PROC_DIALOG_SHOP);
	}	

}



BOOL ShopDialog::_AddItem(eSHOP_TAB eShopTabIndex, SLOTPOS posIndex, sSHOP_ITEM * pShopItem)
{
	if ( INVALID_CODETYPE == pShopItem->m_SellItemCode )
		return FALSE;

	int TabNum = eShopTabIndex * ShopDetailInfo::MAX_SELL_ITEM;

	sITEMINFO_BASE * pBaseItemInfo = theItemInfoParser.GetItemInfo( pShopItem->m_SellItemCode );

	if (!pBaseItemInfo)
	{
		ASSERT(!"invalid item info.");
		return FALSE;
	}


	static DWORD SERIAL = 1000000;

	ItemSlot	itemSlot;
	itemSlot.SetSlotType(ST_ITEM);
	itemSlot.SetCode	( pShopItem->m_SellItemCode );
	itemSlot.SetNum	( pShopItem->m_SellItemNum );
	itemSlot.SetSerial(SERIAL++);
	itemSlot.SetShopItem(TRUE);
   
	InsertSlot( (SLOTPOS)(TabNum + posIndex), itemSlot);

	return TRUE;
}

BOOL ShopDialog::SettingShop()
{
	m_pShopListInfo	=  theShopInfoParser.GetShopList( GetShopID() );
	SetCurrentPage(0);

	if (!m_pShopListInfo)
		return FALSE;

	SLOTPOS			nFirstPage;


	m_pShopListInfo->SetFirstItem();


	nFirstPage	= INVALID_POSTYPE;
	for ( SLOTPOS i = 0; i < ShopDetailInfo::MAX_PAGE_NUM; ++i)
	{
		for ( int j = 0; j < ShopDetailInfo::MAX_SELL_ITEM; ++j )
		{
			sSHOP_ITEM * pShopItem = m_pShopListInfo->GetItemAt((BYTE)i, (BYTE)j);

			{
				BOOL bRet;
				bRet = _AddItem((eSHOP_TAB)i, (SLOTPOS)j, pShopItem);
				if(bRet && nFirstPage == INVALID_POSTYPE)
					nFirstPage = i;
			}
		}
	}
	if(nFirstPage != -1)
		SetCurrentPage (nFirstPage);

	UpdateSlotListener();

	return TRUE;
}


VOID ShopDialog::UpdateSlotListener()
{
}



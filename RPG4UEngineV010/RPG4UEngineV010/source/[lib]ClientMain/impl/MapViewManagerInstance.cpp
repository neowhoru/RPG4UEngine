/*////////////////////////////////////////////////////////////////////////
文 件 名：MapViewManagerInstance.cpp
创建日期：2007年11月14日
最后更新：2007年11月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "MapViewManagerInstance.h"
#include "ImageTGA.h"
#include "GameUIManager.h"
#include "VUIPictureManager.h"
#include "BitBltHandle.h"
#include "VObject.h"
#include "VHero.h"
#include "VObjectManager.h"
#include "VUCtrlManager.h"
#include "IniFile.h"
#include "V3DGameWorld.h"
#include "Hero.h"
#include "ObjectManager.h"
#include "MapNPC.h"
#include "VNpc.h"
#include "HeroQuestManager.h"
#include "ApplicationSetting.h"
#include "MediaPathManager.h"
#include "PathInfoParser.h"
#include "GameParameter.h"
#include "HeroParty.h"
#include "VUIFontDDraw.h"
#include "ObjectUtil.h"

using namespace mapinfo;
using namespace vobject;

static	SNpcInfo*	gs_pSelNpcInfo		= NULL;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(MapViewManagerInstance, ()  , gamemain::eInstPrioGameFunc);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MapViewManagerInstance::MapViewManagerInstance()
{
}

MapViewManagerInstance::~MapViewManagerInstance()
{
}


void MapViewManagerInstance::Destroy()
{
	
	if( m_nHeroArrowTexture != -1 )
	{
		the3DTextureManager.UnRegisterTexture( m_nHeroArrowTexture );
		m_nHeroArrowTexture = -1;
	}


	if( m_nMaskTextureID != -1 )
	{
		the3DTextureManager.UnRegisterTexture( m_nMaskTextureID );
		m_nMaskTextureID = -1;

	}

	for(UINT i = 0; i < MAPICON_MAX; i++ )
	{
		if( m_IconImgs[i].nTextureID != -1 )
		{
			the3DTextureManager.UnRegisterTexture( m_IconImgs[i].nTextureID );
			m_IconImgs[i].nTextureID = -1;
		}
	}

	m_nMapImageWidth	= MINIMAP_WIDTH;
	m_nMapImageHeight = MINIMAP_HEIGHT;
	m_nMapOriginW	= MINIMAP_WIDTH;
	m_nMapOriginH	= MINIMAP_HEIGHT;

	gs_pSelNpcInfo		= NULL;
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::LoadImage( int nImage, LPCSTR  szFileName )
{
	if( nImage < 0 || nImage >= MAPICON_MAX )
	{
		assert( false );
		return FALSE;
	}
	IV3DTextureManager*	pTextureMgr = the3DEngine.GetTextureManager();
	sMAP_ICONINFO*					pImage		= &m_IconImgs[nImage];

	pImage->nTextureID = pTextureMgr->RegisterTexture( szFileName, FALSE, TRUE, FALSE );
	if( pImage->nTextureID == -1 )
		return FALSE;

	SIZE ptSize = pTextureMgr->GetTexture(pImage->nTextureID)->GetSize();
	pImage->nWidth		= ptSize.cx;
	pImage->nHeight	= ptSize.cy;



	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::LoadSettings(LPCSTR szFile)
{
	IniFile					iniFile;
	IV3DTextureManager*	pTextureMgr;
	LPCSTR					szDef;
	LPCSTR					szFileName;
	BOOL						bLoadResult	= FALSE;

	__CHECK( iniFile.LoadFromFile( szFile )  );

	pTextureMgr = the3DEngine.GetTextureManager();
	szDef			= _PATH_UIICON("MiniMap\\Default.tga");
	
	static LPCSTR arFieldNames[]=
	{
		 "Player"
		,"Npc"
		,"NpcLock"
		,"Monster"
		,"Teammate"

		,"Restore"
		,"Weapon"
		,"Shield"
		,"Dress"
		,"Material"
		,"Transport"
		,"Record"
		,"Storage"
		,"Guidepost"
		,"MakeItem"
		,"NpcDirect"

		,"YellowGanTanHao"
		,"WhiteGanTanHao"
		,"YellowWenHao"
		,"WhiteWenHao"


		,"RoleArrow"
		,"NpcPos"

		,"Hero"
	};

	if(iniFile.OpenSession( "Icon" ) )
	{
		for(INT n=0; n<MAPICON_MAX; n++)
		{
			iniFile(arFieldNames[n],"") >> szFileName;
			if(szFileName[0])
				bLoadResult = LoadImage( n, _PATH_UIICON(szFileName ));

			if( !bLoadResult )
				LoadImage(n , szDef );
		}
	}


	m_nHeroArrowTexture	= the3DTextureManager.RegisterTexture( _PATH_UIICON("Mouse\\zg_arrow.tga"), FALSE, TRUE, FALSE  );
	m_nMaskTextureID		= the3DTextureManager.RegisterTexture( _PATH_UIICON("MiniMap\\MiniMapMask.tga"), FALSE, TRUE, FALSE );

	return TRUE;
}





BOOL MapViewManagerInstance::Create	(int              /*nMapOriginW*/
                              ,int              /*nMapOriginH*/
										,sFIELDINFO_BASE* pFieldInfo)
{

	LoadFullMap( pFieldInfo );

	IV3DTexture* pTex;
	pTex = the3DTextureManager.GetTexture(m_arBGTextureIDs[1][1]) ;
	__CHECK_PTR(pTex);

	SIZE ptSize = pTex->GetSize();

	m_nMapImageWidth	= ptSize.cx;
	m_nMapImageHeight = ptSize.cy;
	m_nMapOriginW	= m_nMapImageWidth;
	m_nMapOriginH	= m_nMapImageHeight;

	
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::AddMark( sMAP_MARK* pMark )
{
	for(UINT nMark = 0; nMark < MAX_MAP_MARK; nMark++ )
	{
		if( m_MarkLabels[nMark].dwLife == 0 )
		{
			m_MarkLabels[nMark] = *pMark;
			return TRUE;
		}
	}
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::DrawTextUnderImage	(RECT   rcImage
                                          ,LPCSTR pszText
														,DWORD  dwColor)
{
	
	char s[256];
	sprintf( s, "%s", pszText );
	int nLength = strlen( s )*6+4;
	int x = (rcImage.right+rcImage.left)/2;
	int y = (rcImage.bottom);;//+rcImage.top)/2;
	x -= nLength/2;
	DrawGameText(x
           ,y
			  ,s
			  ,dwColor);
	return TRUE;
	
}

BOOL MapViewManagerInstance::DrawIconImg	(RECT  /*rcRenderWindow*/
											,RECT  /*rcSight*/
											,int   nImageType
											,float  fImageX
											,float  fImageY
											,RECT* prcDst
											,int   nHeroMiniX
											,int   nHeroMiniY
											,float fScale)
{
	
	sMAP_ICONINFO* pImage = &m_IconImgs[nImageType];
	
	//V3DTerrain* pTerrain = NULL;
	//if( theGameWorld.GetHeroPlotMap() && bLocal )
	//{
	//	pTerrain = (V3DTerrain*)theGameWorld.GetHeroPlotMap()->GetTerrain();
	//	pTerrain->TilePosToLocal( fImageX, fImageY );
	//}

	fImageX *= 2.f;
	fImageY *= 2.f;

	RECT rcDst;
	{
		if(nImageType == MAPICON_HERO)
		{
			rcDst.left	= m_TargetRect.left + (m_TargetRect.right - m_TargetRect.left)/2 - ICON_RENDER_SIZE /2;
			rcDst.top	= m_TargetRect.top  + (m_TargetRect.bottom - m_TargetRect.top)/2 - ICON_RENDER_SIZE /2;
			rcDst.right = rcDst.left + ICON_RENDER_SIZE - 1;
			rcDst.bottom = rcDst.top + ICON_RENDER_SIZE - 1;
		}
		else
		{
			rcDst.left		= (LONG)(m_TargetRect.left + MAP_SIZE_BYTILE /2 + ( fImageX - nHeroMiniX) * fScale - ICON_RENDER_SIZE /2 * fScale);
			rcDst.top		= (LONG)(m_TargetRect.top  + MAP_SIZE_BYTILE /2 + ( fImageY - nHeroMiniY) * fScale - ICON_RENDER_SIZE /2 * fScale);
			rcDst.right		= rcDst.left + ICON_RENDER_SIZE - 1;
			rcDst.bottom	= rcDst.top + ICON_RENDER_SIZE - 1;
		}
		//GetImageWindowRect(  rcSight, pImage, fImageX, fImageY, rcDst );
	}


	if( prcDst )
		*prcDst = rcDst;

	RECT rcSrc =	{0, 0, pImage->nWidth, pImage->nHeight};

	//////////////////////////////////////////////////
	if( nImageType == MAPICON_HERO  )
	{
		FLOAT fDir = 0.0f;
		fDir = theVHero.GetCurrentDir();
		fDir = fDir/2;
		const float fPer = math::cPI/16;
		INT fValues = (INT)(fDir / fPer);
		INT fY		= fValues / 4;
		INT fX		= fValues % 4;
		SetRect( &rcSrc, (fX)*16, (fY)*16, (fX+1)*16, (fY+1)*16 );
	}

	return theV3DGraphicDDraw.Blt(pImage->nTextureID
                                  ,&rcDst
											 ,&rcSrc
											 ,&m_TargetRect
											 ,pImage->nWidth
											 ,pImage->nHeight
											 ,0.0f
											 ,0xffffffff
											 ,/*RENDER_Z_FUNC_LESS*/0);

	
}

BOOL MapViewManagerInstance::FrameMove(DWORD /*dwTick*/)
{
	Update();
	return TRUE;
}

void MapViewManagerInstance::Update()
{
	for(UINT x = 0; x< GAMEMAP_CACHE_COL_SIZE; x ++ )
	{
		for(UINT y = 0; y< GAMEMAP_CACHE_COL_SIZE; y ++ )
		{
			m_arBGTextureIDs[x][y] =	(short)theGameWorld.GetMiniMapTextureID( x , y );
		}
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void MapViewManagerInstance::Transfrom( int &nCovX, int &nCovY )
{
	float vx = (float)nCovX;
	float vy = (float)nCovY;
	vx /= 2688;
	vy /= 1280;
	nCovX = (int)(vx*SCREEN_WIDTH);
	nCovY = (int)(vy*SCREEN_HEIGHT);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::ReadyRender(int          nMode
                                 ,const RECT&  rcWindow
											,const POINT& ptHero
											,int         nSight)
{
	SetRenderMode	(nMode);
	SetTargetRect	(rcWindow);

	m_HeroTilePos	= ptHero;
	m_nSightRange	= nSight;

	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::Render	(DWORD dwTick)
{
	if( m_nRenderMode == mapinfo::MAPMODE_FULL )
	{
		_RenderFullMap();
		return TRUE;
	}

	/////////////////////////////////////////////////////////////
	SetMaxMapLevel(TRUE);
	gs_pSelNpcInfo = NULL;


	/////////////////////////////////////////////////////////////
	RECT	rcSight;

	rcSight.left	= m_HeroTilePos.x - m_nSightRange;
	rcSight.top		= m_HeroTilePos.y - m_nSightRange;

	if( m_HeroTilePos.x - m_nSightRange < 0 )
		rcSight.left = 0;

	if( m_HeroTilePos.x + m_nSightRange >= m_nMapOriginW )
		rcSight.left = m_nMapOriginW-m_nSightRange*2;

	if( m_HeroTilePos.y - m_nSightRange < 0 )
		rcSight.top = 0;

	if( m_HeroTilePos.y + m_nSightRange >= m_nMapOriginH )
		rcSight.top = m_nMapOriginH-m_nSightRange*2;

	rcSight.right	= rcSight.left+m_nSightRange*2;
	rcSight.bottom = rcSight.top+m_nSightRange*2;


	if( rcSight.left < 0 )					rcSight.left	= 0;
	if( rcSight.right > m_nMapOriginW )	rcSight.right	= m_nMapOriginW;
	if( rcSight.top < 0 )					rcSight.top		= 0;
	if( rcSight.bottom > m_nMapOriginH )rcSight.bottom = m_nMapOriginH;

	

	__CHECK(_CalcMiniMapInfo(dwTick) );

	/////////////////////////////////////////////////////////////
	//--渲染
	for(UINT x=0; x<3; x++ ) 
	{
		for(UINT y=0; y<3; y++ )
		{	
			if ( m_arBGTextureIDs[x][y] == -1 )
			{
				Update();
			}
			if( m_arBGTextureIDs[x][y] != -1)
			{
				RECT rcRenderWindowCopy = m_rcRenderWindow[x][y];
				RECT rcMapImageCopy		= m_rcMiniImage[x][y];//  (2.0 - m_fZoomScale);
				RECT rcWindowCopy			= m_TargetRect;

				if( !(	rcMapImageCopy.left == 0 
						&& rcMapImageCopy.top == 0 
						&& rcMapImageCopy.right == 0 
						&& rcMapImageCopy.bottom == 0) )
				{
					//渲染一 或 二 或 四张
					theV3DGraphicDDraw.Blt(m_arBGTextureIDs[x][y]
                                       ,&rcRenderWindowCopy
													,&rcMapImageCopy
													,&rcWindowCopy
													,m_nMapImageWidth
													,m_nMapImageHeight
													,0
													,0xffffffff
													,
													0);
				}
			}
		}	
	}//--End 小地图
	

	////////////////////////////////////////////////////////////////////////
	//--画 导向npc--
	_RenderNpcDirect(rcSight);

	///////////////////////////////////////////////////
	//--绘画附近玩家与怪物
	DWORD			arObjects[OBJECT_MAX];
	DWORD			dwCount;

	
	dwCount		= ObjectUtil::GetAttackTargetWithinRange	(theHero.GetPosition()
																		,theVObjectManager.GetFarFromHero()*2//theGeneralGameParam.GetAttackReadyRange()*2
																		,arObjects
																		,0
																		,PLAYER_OBJECT|MONSTER_OBJECT | MAPNPC_OBJECT
																		,OBJECT_MAX);

	ClearViewNpc();

	////////////////////////////////
	if(dwCount)
	{
		vector<Object*>	arQuests;
		vector<Object*>	arNormals;
		//int					nNpcTipsType;


		/////////////////////////////////////////
		for( UINT n=0; n<dwCount; n++ )
		{
			Object *pObject = theObjectManager.GetObject( arObjects[n] );
			//在视野范围内的
			if(!pObject )
				continue;


			if(pObject->IsKindOfObject(MAPNPC_OBJECT))
			{
				MapNPC*	pNpc			= (MapNPC*)pObject;
				VNpc*	pVNpc				= (VNpc*)pNpc->GetVObject();
				if(pVNpc->GetNpcTipsType() != eNpcTips_None)
				{
					arQuests.push_back(pObject);
					continue;
				}
			}
			arNormals.push_back(pObject);
		}

		std::sort(arQuests.begin()	,arQuests.end()	,ObjectUtil::SortByHeroDist);
		std::sort(arNormals.begin(),arNormals.end()	,ObjectUtil::SortByHeroDist);

		/////////////////////////////////////////
		for( UINT n=0; n<arNormals.size(); n++ )
		{
			DrawObjectIcon(arNormals[n], rcSight);
		}

		/////////////////////////////////////////
		for( UINT n=0; n<arQuests.size(); n++ )
		{
			DrawObjectIcon(arQuests[n], rcSight);
		}
	}
	//End of 队友


	///////////////////////////////////////////////////
	//--画主角-
	DrawIconImg	(m_rcRenderWindow[1][1]
					,rcSight
					,MAPICON_HERO
					,(float)m_nHeroMiniX
					,(float)m_nHeroMiniY);


	/*////////////////////////////////////////////////////////////////////////
	///这里统一画所有的鼠标指向说明　这样指向说明才能提到最上层
	/*////////////////////////////////////////////////////////////////////////
	_RenderMarkLabels(rcSight);


	return TRUE;
}



void MapViewManagerInstance::DrawObjectIcon(Object*	pObject,RECT& rcSight)
{
	const float fTILE_3DSIZE_INV = 1.0f/TILE_3DSIZE;

	sNPC_COORDINFO	cood;
	Vector3D			vObjPos;
	INT				nType = 0;
	RECT&				rc = cood.m_rcIcon;

	///////////////////////////////////////
	vObjPos		= pObject->GetPosition();
	cood.m_MapX	= vObjPos.x * fTILE_3DSIZE_INV;
	cood.m_MapY	= vObjPos.y * fTILE_3DSIZE_INV;

	vObjPos	-= theHero.GetPosition();
	vObjPos	*= fTILE_3DSIZE_INV;
	vObjPos.x	+= (float)m_nHeroMiniX*0.5f;
	vObjPos.y	+= (float)m_nHeroMiniY*0.5f;

	cood.m_Name	= _VI(pObject->GetName());
	cood.m_NpcID= pObject->GetObjectKey();

	////////////////////////////////////////////
	nType = MAPICON_PLAYER;
	//if(pObject->IsKindOfObject(PLAYER_OBJECT))
	//	nType = MAPICON_Player;
	if(theHeroParty.GetMemberBy(pObject->GetObjectKey()))
		nType = MAPICON_TEAMMATE;
	else if(pObject->IsKindOfObject(MONSTER_OBJECT))
		nType = MAPICON_MONSTER;

	else if(pObject->IsKindOfObject(MAPNPC_OBJECT))
	{
		MapNPC*	pNpc			= (MapNPC*)pObject;
		VNpc*	pVNpc				= (VNpc*)pNpc->GetVObject();
		int	nNpcTipsType	= pVNpc->GetNpcTipsType();//theHeroQuestManager.UpdateNpcTipsType( arObjects[n] );

		nType = MAPICON_NPC;
		if(nNpcTipsType != eNpcTips_None)
		{
			switch( nNpcTipsType )
			{
			case eNpcTips_HaveQuest:			nType = MAPICON_YELLOWGANTANHAO;		break;
			case eNpcTips_HaveQuestNotNow:	nType = MAPICON_WHITEGANTANHAO;		break;
			case eNpcTips_QuestDone:			nType = MAPICON_YELLOWWENHAO;			break;
			case eNpcTips_QuestNotDone:		nType = MAPICON_WHITEWENHAO;			break;
			}
		}
		else
		{
			switch( pNpc->GetFuncInfo()->m_eNPCTYPE )
			{
			case NPC_FUNC_STORE:			nType = MAPICON_RESTORE;			break;
			case NPC_FUNC_WEAPON:		nType = MAPICON_WEAPON;			break;
			case NPC_FUNC_SHIELD:		nType = MAPICON_SHIELD;			break;
			case NPC_FUNC_DRESS:			nType = MAPICON_DRESS;			break;
			case NPC_FUNC_MATERIAL:		nType = MAPICON_MATERIAL;		break;
			case NPC_FUNC_TRANSPORT:	nType = MAPICON_TRANSPORT;		break;
			case NPC_FUNC_RECORD:		nType = MAPICON_RECORD;			break;
			case NPC_FUNC_BANK:			nType = MAPICON_STORAGE;			break;
			case NPC_FUNC_GUIDEPOST:	nType = MAPICON_GUIDEPOST;		break;
			case NPC_FUNC_ITEMMAKE:		nType = MAPICON_ITEMMAKE;		break;
			default:							nType = MAPICON_NPC;				break;
			}
		}
	}

	DrawIconImg	(m_rcRenderWindow[1][1]
					,rcSight
					,nType
					,vObjPos.x
					,vObjPos.y
					,&rc
					,m_nHeroMiniX
					,m_nHeroMiniY
					,m_fZoomScale);

	/////////////////////////////////
	_PushViewNpc(cood);
}

void MapViewManagerInstance::SetNpcInfo(LPCSTR  szName, float mapX, float mapY)
{
	
	sNPC_COORDINFO info;
	if(szName && szName[0])
	{
		//info._szName = szName;
		//info.SetName(szName);
		info.m_Name		= _VI(szName);
		info.m_MapX		= mapX*TILE_3DSIZE;
		info.m_MapY		= mapY*TILE_3DSIZE;
	}
	_PushNpcInfo(info);
	//m_NpcInfos.push_back(info);
	
}


void MapViewManagerInstance::CreateMapButton(  LPCSTR  pszFilename1, LPCSTR  pszFilename2,RECT rtArea)
{
	
	mapinfo::sMAPBTN_INFO	btn;
	btn.m_Normal		= the3DTextureManager.RegisterTexture( pszFilename1, FALSE, TRUE, FALSE );
	btn.m_MouseOver	= the3DTextureManager.RegisterTexture( pszFilename2, FALSE, TRUE, FALSE );
	::CopyMemory(&btn.m_HotArear,&rtArea,sizeof(RECT));
	//m_ButtonInfos.push_back(btn);
	_PushButtonInfo(btn);
}

void MapViewManagerInstance::RealeaseSecondLV()
{
	
	for(UINT i=1; i<MAX_MAP_COUNTS; ++i)
	{
		if(m_MapLevels[i].size() > 0)
		{
			for(UINT nX=0; nX<m_MapLevels[i].size(); ++nX)
				the3DTextureManager.UnRegisterTexture(m_MapLevels[i][nX]);
		}
		m_MapLevels[i].clear();
	}
	
}

void MapViewManagerInstance::OnWinMsg	(HWND   hWnd
													,UINT   uMsg
													,WPARAM wParam
													,LPARAM lParam)
{
	MsgProc(hWnd,uMsg,wParam,lParam);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL MapViewManagerInstance::MsgProc( HWND /*hWnd*/, UINT msg, WPARAM /*wParam*/, LPARAM /*lParam*/ )
{
	
	switch(msg)
	{
	case WM_LBUTTONDOWN:
		{
			m_bMouseDown = true;

			if( m_bIsSecondMap )
				SecondMapBtnDown();

			else if( m_bInSecondMap )
				SecondMapBtnDown();

			if(theApplicationSetting.m_ShowAdvancedMiniMap)
				return TRUE;
		}break;

	case WM_LBUTTONUP:
		{
			m_bMouseDown = false;
		}break;

	case WM_RBUTTONDOWN:
		{
			m_nMaxMapIndex = 0;
			m_bIsSecondMap = false;
			theMapViewManager.RealeaseSecondLV();
		}break;
	default:
		break;
	}
	return FALSE;
	
}



void MapViewManagerInstance::SetOffPoint(int idx,POINT pnt)
{
	
	::CopyMemory(&m_OffsetInfos[idx],&pnt,sizeof(POINT));
	
}

int MapViewManagerInstance::CheckWherePlayerIn(POINT pPlayerPos)
{
	
	int iresult = 0;
	//int iFarFromFrame = 20;
	for ( int i = 1; i < MAX_MAP_COUNTS; i++ )
	{
		if	(
				(pPlayerPos.x >= m_OffsetInfos[i].x) && (pPlayerPos.x <= (LONG)((m_OffsetInfos[i].x + 4*MAP_SIZE_BYTILE))) &&
				(pPlayerPos.y >= m_OffsetInfos[i].y) && (pPlayerPos.y <= (LONG)((m_OffsetInfos[i].y + 3*MAP_SIZE_BYTILE)))
			)
		{
			iresult = i;
			break;
		}
	}
	return iresult;
	
}

float  MapViewManagerInstance::_GetHeroTargetDir()
{
	return theVHero.GetTargetDir();
}

void MapViewManagerInstance::SecondMapBtnDown()
{
	if( m_nMaxMapIndex && m_bMouseDown )
	{
		int mouseX = theUICtrlManager.m_MousePos.x;  //-- mouse.x
		int mouseY = theUICtrlManager.m_MousePos.y;  //-- mouse.y

		if( mouseX > SCREEN_WIDTH*0.125 && mouseX < SCREEN_WIDTH*0.875 &&
			mouseY > SCREEN_HEIGHT*0.125 && mouseY < SCREEN_HEIGHT*0.875 )
		{
			//增加 小地图的npc 指引 
			POINT pt = GetMouseGlobalPos();

			ClearNpcInfo();
			SetNpcInfo( "目的地", (float)pt.x, (float)pt.y );
		}

	}
}



BOOL MapViewManagerInstance::DrawTipInfo(sNPC_COORDINFO* pInfo)
{

	//////////////////////////////////////////
	POINT& ptMouse = theUICtrlManager.m_MousePos;
	if(!PtInRect(&m_TargetRect, ptMouse) )
		return FALSE;

	RECT	rcIcon = {pInfo->m_rcIcon.left + (ICON_RENDER_SIZE-ICON_REAL_SIZE)/2
						,pInfo->m_rcIcon.top  + (ICON_RENDER_SIZE-ICON_REAL_SIZE)/2
						,rcIcon.left  + ICON_REAL_SIZE
						,rcIcon.top   + ICON_REAL_SIZE
	};

	if(! PtInRect( &rcIcon, ptMouse ) )
		return FALSE;


	//////////////////////////////////////////
	int nFontIndex(0);
	theGameUIManager.UIGetData	(gameui::eUINpcChatDialogBox
											,gameui::eGetFontIndex
											,&nFontIndex)		;


	//////////////////////////////////////////
	//画鼠标指向
	LPCSTR szName = (LPCTSTR)pInfo->m_Name;

	int nLength = strlen( szName )*7+4;
	int nTipX = ptMouse.x;
	int nTipY = ptMouse.y-16;

	if( nTipX+nLength >= SCREEN_WIDTH )
		nTipX = SCREEN_WIDTH-nLength;
	if( nTipY+16 >= SCREEN_HEIGHT )
		nTipY = SCREEN_HEIGHT-16;
	RECT rc = {	nTipX,nTipY, nTipX+nLength,	nTipY+16 };

	theV3DGraphicDDraw.FillRect2D( rc, 0xdf000000 );
	DrawGameText	(rc.left+2
            ,rc.top+1
				,szName
				,0xffffff00
				,nFontIndex);

	return TRUE;
}

BOOL	MapViewManagerInstance::GetTargetPosAt(POINT&	pt,Vector3D& vPos)
{
	if(!PtInRect(&m_TargetRect, pt) )
		return FALSE;

	Vector3D	vObjPos((REAL)pt.x,(REAL)pt.y,0);
	
	/////////////////////////////
	vObjPos.x = (pt.x - (m_TargetRect.left + MAP_SIZE_BYTILE /2 - ICON_RENDER_SIZE /2 * m_fZoomScale))/m_fZoomScale + m_nHeroMiniX;
	vObjPos.y = (pt.y - (m_TargetRect.top  + MAP_SIZE_BYTILE /2 - ICON_RENDER_SIZE /2 * m_fZoomScale))/m_fZoomScale + m_nHeroMiniY;

	vObjPos.x	-= ICON_RENDER_SIZE/2;
	vObjPos.y	-= ICON_RENDER_SIZE/2;

	vObjPos	*= 0.5f;

	/////////////////////////////
	vObjPos.x	-= m_nHeroMiniX/2;
	vObjPos.y	-= m_nHeroMiniY/2;

	vObjPos	*= TILE_3DSIZE;
	vObjPos	+= theHero.GetPosition();
	vObjPos.z = theGameWorld.GetHeightAt(vObjPos.z,10000.f,vObjPos.x,vObjPos.y);


	/////////////////////////////
	vPos = vObjPos;

	return TRUE;
}


//void MapViewManagerInstance::_DrawNormapNpc()
//{
//}

/*////////////////////////////////////////////////////////////////////////
文 件 名：Item.cpp
创建日期：2008年3月25日
最后更新：2008年3月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Character.h"
#include "Item.h"
#include "ConstTextRes.h"


namespace object
{ 

static const int ITEM_DISAPPEAR_DELAY	= 10000;
static const int ITEM_DISAPPEAR_DELAY2 = 30000;

//extern DWORD g_CurTime;




Item::Item(void) 
	:m_bHide(FALSE)
	,m_dwOwnerKey(0)
	,m_dwDropMonsterID(0)
	,m_Money(0)
	,m_dwLifeTime(0)
	,m_bSendPickPacket(FALSE)
{
}

Item::~Item(void)
{
}

BOOL Item::InitializeStates()
{
	m_StateArray[STATE_IDLE]				= NULL;
	m_StateArray[STATE_SPAWN]				= NULL;
	m_StateArray[STATE_MOVE]				= NULL;
	m_StateArray[STATE_KEYBOARDMOVE]	= NULL;
	m_StateArray[STATE_ATTACK]	      = NULL;
	m_StateArray[STATE_JUMP]				= NULL;
	m_StateArray[STATE_DEATH]				= NULL;
	m_StateArray[STATE_ANIMATION]      = NULL;
	m_StateArray[STATE_DAMAGE]	      = NULL;
	m_StateArray[STATE_AIR]				= NULL;
	m_StateArray[STATE_DOWN]				= NULL;
	m_StateArray[STATE_GETUP]          = NULL;
	m_StateArray[STATE_EVENTJUMP]      = NULL;
	m_StateArray[STATE_SKILL]          = NULL;
	m_StateArray[STATE_TRIGGER_STATE]  = NULL;
	m_StateArray[STATE_ITEM_DROP_STATE]= theObjectSystem.AllocState(STATE_ITEM_DROP_STATE);
	//m_StateArray[STATE_ITEM_DROP_STATE] = StateItemDrop::GetInstance();
	m_pNextSkillState					 = NULL;
	OnInitializeState();

	return TRUE;
}


void Item::OnInitializeState()
{

}



BOOL Item::Create( DWORD dwObjectKey, DWORD dwParam )
{
	m_bSendPickPacket = FALSE;

	m_ObjectCookie.SetObjectType(ITEM_OBJECT);

	if(dwParam)
		SetItemInfo(theItemInfoParser.GetItemInfo(dwParam));
	else
		SetItemInfo(NULL);

	m_byDropType = 0;
	SetAngle(0.0f,TRUE);

	m_fVisibleAngleModifier = 0.0f;

	m_vVisiblePos.Set(0.0f,0.0f,0.0f);


	APPEARANCEINFO appearInfo;
	ZeroMemory(&appearInfo,sizeof(appearInfo));
	appearInfo.ObjectType	= APPEARANCEINFO::ITEM;
	appearInfo.ItemCode		= dwParam;

	
	if (appearInfo.ItemCode)
	{
		appearInfo.dwMoney	= 0;
		m_Money					= 0;
	}
	else
	{
		appearInfo.dwMoney	= 1;
		m_Money					= 1;

	}

	theAppearanceManager.SetAppearance(&appearInfo,this,TRUE,FALSE);


	BOOL bRet = Object::Create(dwObjectKey, dwParam);

	InitState(STATE_ITEM_DROP_STATE, g_CurTime);

	m_dwDropMonsterID = 0;
	m_dwLifeTime		= 0;

	m_bOpened = FALSE;

	m_Item.Clear();


	return bRet;
}


void Item::Destroy( void )
{

	Object::Destroy();
}

BOOL Item::Process( DWORD dwTick )
{
#if !defined(_DEBUG) && !defined(_PRERELEASE)
	if (!theGeneralGameParam.IsEnableNetwork())
	{
		m_dwLifeTime += dwTick;
		if (m_dwLifeTime > ITEM_DISAPPEAR_DELAY2)
			return FALSE;
	}
#endif

	if (m_bOpened)
	{
		m_dwOpenDelay += dwTick;
		if (m_dwOpenDelay > ITEM_DISAPPEAR_DELAY)
		{
			return FALSE;
		}
	}

	if (!m_bHide)
	{
		ProcessInterpolation(dwTick);
		ProcessAnimation(dwTick);

		StateProcess( g_CurTime, dwTick );
	}
	else
	{
		if (m_dwDropMonsterID)
		{
			Character *pMon = (Character *)theObjectManager.GetObject(m_dwDropMonsterID);
			if (pMon && pMon->IsMonster())
			{
				if (pMon->GetHP() <= 0)
				{
					m_bHide = FALSE;
				}
			}
			else
			{
				m_bHide = FALSE;
			}
		}
	}

	return TRUE;
}

float Item::ProcessAnimation(DWORD dwTick, GAMEEVENT * pEvent)
{


	return Object::ProcessAnimation(dwTick, pEvent);
}


BOOL Item::ProcessInterpolation( DWORD /*dwTick*/ )
{
	m_vVisiblePos = m_pPathExplorer->GetVisiblePos();	

	return TRUE;
}


BOOL Item::SetPosition( Vector3D *wvPos)
{
	ASSERT(wvPos);

	theTileMap.PathHandleReset( m_pPathExplorer, *wvPos );
	m_pVObject->SetPosition(m_pPathExplorer->GetPos());

	return TRUE;
}




IDTYPE Item::GetCurrentDropAnimation()
{
	return 0;
}

IDTYPE Item::GetCurrentIdleAnimation()
{
	return 0;
}

IDTYPE Item::GetCurrentOpenAnimation()
{
	return 0;
}


void Item::Open()
{
	m_dwOpenDelay = 0;
	m_bOpened = TRUE;
	SetAnimation(GetCurrentOpenAnimation(),FALSE);
	
}


void Item::SetItem(ITEMOPT_STREAM & ItemStream)
{
	m_Item.Copy(ItemStream);
}







void Item::SetItemInfo(sITEMINFO_BASE* pItemInfo)
{
	if(pItemInfo == NULL)
	{
		memset(&m_ItemInfo,	0,sizeof(m_ItemInfo));
		memset(&m_VItemInfo,	0,sizeof(m_VItemInfo));
		return;
	}

	sVITEMINFO_BASE* pVItemInfo;
	assert(pItemInfo);

	pVItemInfo = theItemInfoParser.GetVItemInfo(pItemInfo->m_VItemCode);
	assert(pVItemInfo);

	m_ItemInfo	= *pItemInfo;
	m_VItemInfo	= *pVItemInfo;
}

void Item::SetName(const char * /*pszName*/)
{
	LPCSTR szItemName;
	if(IsMoney())
	{
		LPCSTR szName = _STRING(TEXTRES_ITEM_MONEYNAME);
		szItemName = FMSTR( szName,  GetMoney() );
		//LOGINFO("%d %s\n",(DWORD)GetMoney(),szItemName);
	}
	else
	{
		sITEMINFO_BASE& item = GetItemInfo();

		szItemName = (LPCSTR)item.m_ItemName;
	}

	m_strObjName = _VI(szItemName);
	m_pVObject->SetName(szItemName);
}






};//namespace object

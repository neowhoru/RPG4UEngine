/*////////////////////////////////////////////////////////////////////////
文 件 名：VPlayer.cpp
创建日期：2007年2月28日
最后更新：2007年2月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VPlayer.h"
#include "VUCtrlEdit.h"
#include "VCharacterAnimationCtrl.h"
#include "GameClientApplication.h"
#include "ScreenTipManager.h"
#include "MapHandleManager.h"
#include "V3DTerrain.h"
#include "IGameEffectHelper.h"
#include "IGameEffectContainer.h"
#include "GameEffectDefine.h"
#include "V3DGameWorld.h"
#include "MathFormular.h"
#include "CharActionDefine.h"
#include "GlobalTexture.h"
#include "GameMainUtil.h"
#include "Object.h"
#include "RenderUtil.h"
#include "HeroParty.h"
#include "Player.h"

using namespace gameeffect;
using namespace gamemain;
using namespace object;
using namespace math;

namespace vobject
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VPlayer::VPlayer()
{
	m_dwObjectType				= PLAYER_OBJECT;
	m_bOpenPrivateShop		= FALSE;
	m_pVendorEditCtrl	= NULL;

	m_dwFightFlag				= 0;
	m_dwFightChangeFlag		= FALSE;
	m_dwFightChangeTime		= 0;
	m_dwFightChCurrTime		= 0;

	m_szGuildName[0]			= 0;
	m_pPlayer					= NULL;

	m_nOfficialLevel			= 0;
	m_szOfficialTitle[0]		= 0;
	m_dwShowNameFlag			= OBJNAME_SHOWPLAYER;
}
VPlayer::~VPlayer()
{
	SAFE_DELETE( m_pVendorEditCtrl );
}

//void	VPlayer::Destroy()
//{
//	_SUPER::Destroy();
//	SAFE_DELETE( m_pVendorEditCtrl );
//}


void	VPlayer::Reset()
{
	_SUPER::Reset();
}

void	VPlayer::Update( DWORD dwTick )
{
	_SUPER::Update(dwTick);

	if ( m_pVendorEditCtrl )
	{
		int nFontSize = m_pVendorEditCtrl->GetFontSize();
		int nWidth = 0, nHeight = 0;
		int nLen = strlen(m_pVendorEditCtrl->GetText());
		if ( nLen <= CHAT_INFO_WIDTH_MAX )
		{
			nWidth = nFontSize*( nLen +2)/2;
			nHeight = nFontSize*2;
		}
		else
		{
			nWidth = nFontSize*(CHAT_INFO_WIDTH_MAX+4)/2;
			nHeight = nFontSize*( nLen / CHAT_INFO_WIDTH_MAX + 1);
		}

		V3DSpaceBox* pBBox = &m_pAnim->GetWorldBBox();
		float fHeight = pBBox->GetMaxZ() + 30*SCALE_MESH;
		V3DVector vCenter( m_vPosition.x, m_vPosition.y, fHeight );
		int ix = 0, iy = 0;
		if( theMapHandleManager.GetCamera()->World3DToScreen2D( vCenter, &ix, &iy, SCREEN_WIDTH, SCREEN_HEIGHT ) )
		{
			m_pVendorEditCtrl->SetPos( ix - nWidth/2, iy - nHeight-40 );
		}
	}
}

void	VPlayer::UpdateManual(DWORD dwTime)
{
	_SUPER::UpdateManual(dwTime);
	if(m_dwFightChangeFlag )
	{
		if( g_CurTime - m_dwFightChCurrTime > m_dwFightChangeTime )
		{
			m_pAnim->UpdateHandItem( HasFightFlag(FIGHTFLAG_FIGHTING), &m_arEquips );
			m_dwFightChangeFlag = eNoChangeFight;		//更改结束
		}
	}


}


//void	VPlayer::OnCreate(Object* pObject)
//{
//	_SUPER::OnCreate(pObject);
//
//	if(!m_pMonster)
//		return;
//
//	/////////////////////////////////////////////////
//	//RESOURCE_INFO*	pInfo;
//	DWORD dwSeries = m_pMonster->GetMonsterInfo()->m_bySeries;
//
//	InitSericeIcon((eATTACK_KIND)dwSeries);
//}


BOOL	VPlayer::RenderInfo( DWORD dwTick, DWORD dwColor , float fTransparent, float fPlayerTransparent)
{
	int x = 0, y = 0;
	V3DVector vPos( m_vPosition.x, m_vPosition.y, m_vPosition.z );
	BOOL bShow = theMapHandleManager.GetCamera()->World3DToScreen2D( vPos, &x, &y, SCREEN_WIDTH, SCREEN_HEIGHT );
	if( !bShow )
		return FALSE;

	y += 8;

	__BOOL_SUPER(RenderInfo(dwTick,dwColor,fTransparent , fPlayerTransparent));

	if( m_nOfficialLevel > 0 )
	{

		RECT rcDst = 
		{
			x, y+20, x+32, y+20+16
		};
		RenderUtil::RenderPic(theGlobalTexture.GetCountryTexture( GetCountry() )
                           ,rcDst
									,0xffffffff);
		RenderUtil::RenderPic(theGlobalTexture.GetMedalTexture( m_nOfficialLevel-1 )
                           ,rcDst
									,0xffffffff);
	}

	if ( m_pVendorEditCtrl )
	{
		m_pVendorEditCtrl->Render(dwTick);
	}
	return TRUE;
}



void	VPlayer::GetShowTextInfo(StringHandle&	sText)
{
	if(m_pPlayer->GetBehaveState()== PLAYER_BEHAVE_VENDOR_ESTABLISHER)
	{
		sText = m_pPlayer->GetVendorTitle();
		return;
	}

	if(m_szGuildName[0])
		sText.Format("[%s]%s", m_szGuildName, m_szName );
	else
		sText = m_szName;
}

COLOR	VPlayer::GetNameColor()
{
	if(theHeroParty.GetMemberBy(m_pObject->GetObjectKey()))
		return _COLOR(COLOR_TEAMMATE);
	if(m_pPlayer->GetBehaveState() == PLAYER_BEHAVE_VENDOR_ESTABLISHER)
		return _COLOR(COLOR_VENDOR);

	return _SUPER::GetNameColor();
}


BOOL VPlayer::IsShowName()
{
	if(_SUPER::IsShowName())
		return TRUE;

	return FALSE;

}

void	VPlayer::SetObject		(Object* pObject)
{
	_SUPER::SetObject(pObject);
	m_pPlayer = (Player*)pObject;
}

void	VPlayer::OnLevelUp()
{
	ISkillSpecialEffect* pEffect = theSkillSpecialEffectManager.CreateEffect(PLAYEREFFECT_BYMESH);
	//pEffect->SetCreator( __FUNCTION__  );
	pEffect->Create( 15100, 5000);
	//pEffect->SetPos(m_vPosition.x,m_vPosition.y,m_vPosition.z);
	pEffect->SetFollowPlayer( this );
	theSkillSpecialEffectManager.AddEffect( pEffect );
}

BOOL VPlayer::InitSericeIcon	(DWORD attackKind)
{
	if(m_pPlayer->GetBehaveState()== PLAYER_BEHAVE_VENDOR_ESTABLISHER)
	{
		attackKind = PREFIXKIND_VENDOR;
	}
	return _SUPER::InitSericeIcon	(attackKind);
}


BOOL VPlayer::IsDetectedVendor( int x, int y )
{
	if ( !IsPlayer() )
	{
		return FALSE;
	}
	if ( !m_pVendorEditCtrl )
	{
		return FALSE;
	}
	return m_pVendorEditCtrl->PtInObject( x, y );
}


void VPlayer::SetVendorInfo( const BOOL bOpen, const char* szInfo )
{
	;
	m_bOpenPrivateShop = bOpen;
	std::string strInfo = szInfo;
//	m_strPrivateShopInfo = szInfo;
	if( bOpen == FALSE )
	{
		if ( IsHero())
		{
			theGameUIManager.UIChangeShopState(gameui::eUIPrivateShop, FALSE);
		}
		else
		{
			int nPlayerID;
			theGameUIManager.UIGetData(gameui::eUIPlayerPrivateShop,gameui::eGetPlayerID,&nPlayerID);
			if ( GetID() == (VOBJID)nPlayerID )
			{
				nPlayerID = -1;
				theGameUIManager.UISetData(gameui::eUIPlayerPrivateShop,gameui::eSetPlayerID,nPlayerID);
				theGameUIManager.UISetVisible(gameui::eUIPlayerPrivateShop, FALSE);
				char szCloseShop[128];
				sprintf( szCloseShop, "%s 关掉了商店!", GetName() );
				theGameUIManager.UIMessageBox(szCloseShop);
			}


		}
		SAFE_DELETE( m_pVendorEditCtrl );
	}
	else
	{
		RECT rc = {0,0,0,0};
		if ( !m_pVendorEditCtrl )
		{
			m_pVendorEditCtrl = new VUCtrlEdit;
			m_pVendorEditCtrl->Create( &rc);//, 0xffffffff, TRUE, TRUE );
			m_pVendorEditCtrl->SetReadOnly(TRUE);
		}
		int nFontSize = m_pVendorEditCtrl->GetFontSize();
		int nStrLen = strInfo.size();
		if ( nStrLen <= CHAT_INFO_WIDTH_MAX )
		{
			rc.left = -4;
			rc.top = -5;
			rc.right = nFontSize*(nStrLen+4)/2;
			rc.bottom = nFontSize + 5;
		}
		else
		{
			rc.left = -4;
			rc.top = -5;
			rc.right = nFontSize*(CHAT_INFO_WIDTH_MAX+4)/2;
			rc.bottom = nFontSize*nStrLen/CHAT_INFO_WIDTH_MAX + 5;
			if ( nStrLen%CHAT_INFO_WIDTH_MAX != 0 )
			{
				rc.bottom += nFontSize;
			}
		}
		m_pVendorEditCtrl->SetGlobalRect( &rc );
		m_pVendorEditCtrl->SetText( strInfo.c_str() );


		sUIINFO_PIC pPic;
		RECT rcFrame = {319, 0, 401, 20};
		RECT rcClient = {333, 7, 387, 13};
		pPic.SetPicInfo("[Base]\\UIBase.png", &rcFrame, &rcClient);
		m_pVendorEditCtrl->SetPicInfo(&pPic, TRUE);

		sUIINFO_BASE* pData = m_pVendorEditCtrl->GetData();
		for(int i=0; i<UIDRAW_MAX; ++i)
		{
			pData->m_nDrawMode[i] = DrawModeStrectch;
		}
	}
	;
}


BOOL VPlayer::AddFightFlag( eFIGHT_FLAG flag, BOOL bIsPlayAmin, BOOL /*IsReEquip*/ )
{
	if( IsDead() )
		return FALSE;

	_BIT_ADD(m_dwFightFlag, flag);
	DWORD dwTime = 300;

	switch( flag )
	{
	case FIGHTFLAG_FIGHTING:
		{
			m_dwFightChangeFlag = eChangeFightAll;

			if( bIsPlayAmin && g_CurTime - m_dwFightChCurrTime > m_dwFightChangeTime )
			{
				SwitchScabbardAnim();
				//SetActionStatus( esk_Scabbard, 1000 );

			//	if( IsHaveLHardWeapon() )
				{
					//dwTime = 500;
					if( m_pAnim->GetTriggerPointFrameID() == -1 )
					{
						dwTime = 500;
					}
					else
					{
						int nFrame = ( m_pAnim->GetTriggerPointFrameID() - m_pAnim->GetStartFrameId() );
						if( nFrame < 0 )
							dwTime = 500;
						else 
							dwTime = nFrame *33;
					}
				}
				m_dwFightChangeTime = dwTime;
				m_dwFightChCurrTime = g_CurTime;
			}
			else
			{
				return FALSE;
			}
		}
		break;
	default:
		return FALSE;
		break;
	}

	return TRUE;
	;
}

BOOL VPlayer::ClearFightFlag( eFIGHT_FLAG flag, BOOL bIsPlayAmin, BOOL /*IsReEquip*/ )
{
	if( IsDead() )
		return FALSE;

	_BIT_REMOVE(m_dwFightFlag, flag);
	DWORD dwTime = 500;

	switch( flag )
	{
		case FIGHTFLAG_FIGHTING:
		{
			m_dwFightChangeFlag = eChangeFightAll;

			if( bIsPlayAmin && g_CurTime - m_dwFightChCurrTime > m_dwFightChangeTime )
			{
				SwitchScabbardAnim();
				//SetActionStatus( esk_Scabbard, 1000 );
				//HangMoving( g_CurTime, 1000 );

			//	if( IsHaveLHardWeapon() )
				{
					//dwTime = 500;
					if( m_pAnim->GetTriggerPointFrameID() == -1 )
					{
						dwTime = 500;
					}
					else
					{
						int nFrame = ( m_pAnim->GetTriggerPointFrameID() - m_pAnim->GetStartFrameId() );
						if( nFrame < 0 )
							dwTime = 500;
						else 
							dwTime = nFrame *33;
					}
				}

				m_dwFightChangeTime = dwTime;
				m_dwFightChCurrTime = g_CurTime;
			}
			else
			{
				return FALSE;
			}
			break;
		default:
			return FALSE;
			break;
		}
	}
	return TRUE;
	;
}


BOOL   VPlayer::CheckFightingStatus()
{
	return HasFightFlag(FIGHTFLAG_FIGHTING);
}


void VPlayer::SetMoving( BOOL bMoving, BOOL bBackOff )
{
	_SUPER::SetMoving( bMoving, bBackOff );

}


//切换到跑步的动作
void VPlayer::SwitchRunAnim(DWORD dwTimeOut)
{
	//_SUPER::SwitchRunAnim(dwTimeOut) 不调用超类
	if( m_bDead )
		return;

	if(m_bAssaultMoving)
	{
		SwitchAssaultAnim( dwTimeOut);
		return;
	}

	m_fCurMoveRate = GetMoveRate();
	DWORD dwTimePerLoop = 0;
	dwTimePerLoop = (DWORD)((m_fRunStep/m_fCurMoveRate)*1000);

	static DWORD dwTick = GetTickCount();
	//DWORD dwDiff = GetTickCount() - dwTick;
	dwTick = GetTickCount();
	//LOGINFO("VPlayer::SwitchRunAnim Time:%d, Diff:%d BackOff:%d\n",dwTimePerLoop, dwDiff,m_bCountermarch);

	if( m_bMounting )
	{
		m_pAnim->PlayLowerAnim("mountrun"
                            ,PLAYMODE_NORMAL
									 ,0
									 ,MAX_ANIMATION_LOOP
									 ,ANIMATION_TIMEOUT
									 ,ANIMATION_BLENDING_TIME
									 ,"mountrun"
									 ,PLAYMODE_NORMAL);

		m_pMountAnim->PlayAnimByActionName("run"
                                        ,PLAYMODE_NORMAL
													 ,0
													 ,MAX_ANIMATION_LOOP
													 ,ANIMATION_TIMEOUT
													 ,"run"
													 ,PLAYMODE_NORMAL);
	}
	else
	{
		if( m_bHideProwl )
		{
			m_pAnim->PlayLowerAnim("prowl"
                               ,PLAYMODE_NORMAL
										 ,dwTimePerLoop
										 ,MAX_ANIMATION_LOOP
										 ,ANIMATION_TIMEOUT
										 ,ANIMATION_BLENDING_TIME
										 ,"idle"
										 ,PLAYMODE_NORMAL);
		}
		else
		{
			m_pAnim->PlayLowerAnim("run"
                               ,PLAYMODE_NORMAL
										 ,dwTimePerLoop
										 ,MAX_ANIMATION_LOOP
										 ,ANIMATION_TIMEOUT
										 ,ANIMATION_BLENDING_TIME
										 ,"idle"
										 ,PLAYMODE_NORMAL);
		}
	}
}

//切换到水中待机的动作
void VPlayer::SwitchSwimIdleAnim(DWORD /*dwTimeOut*/)
{
	if(!CanSwitchAnim())
		return;
	if(m_bMounting )
		return;

	m_pAnim->PlayLowerAnim("swimidle"
                         ,PLAYMODE_NORMAL
								 ,0
								 ,MAX_ANIMATION_LOOP
								 ,ANIMATION_TIMEOUT
								 ,400
								 ,"swimidle"
								 ,PLAYMODE_NORMAL);
}

//切换到躲避动作
void	VPlayer::SwitchDodgeAnim(DWORD /*dwTimeOut*/)
{
	if(IsDead())
		return;

	m_dwStartStopMovingTime = g_CurTime;

	if( m_pAnim->IsUpperBodyAndLowerBodyMode() )
	{
		if( IsHaveShield() )
		{
			m_pAnim->PlayUpperAnim(	"block",
				PLAYMODE_NORMAL, 
				0,
				1,
				ANIMATION_TIMEOUT,
				ANIMATION_BLENDING_TIME );
		}
		else
		{
			m_pAnim->PlayUpperAnim("dodge"
                               ,PLAYMODE_NORMAL
										 ,0
										 ,1
										 ,ANIMATION_TIMEOUT
										 ,ANIMATION_BLENDING_TIME);
		}
	}
}

//切换到游泳的动作
void VPlayer::SwitchSwimAnim(DWORD /*dwTimeOut*/)
{
	if(!CanSwitchAnim())
		return;
	if(m_bMounting )
		return;

	m_fCurMoveRate = GetMoveRate();
	DWORD dwTimePerLoop = 0;
	dwTimePerLoop = (DWORD)((m_fSwimStep/m_fCurMoveRate)*1000);

	m_pAnim->PlayLowerAnim("swim"
                         ,PLAYMODE_NORMAL
								 ,dwTimePerLoop
								 ,MAX_ANIMATION_LOOP
								 ,ANIMATION_TIMEOUT
								 ,400
								 ,"idle"
								 ,PLAYMODE_NORMAL);
}

void	VPlayer::SwitchAssaultAnim( DWORD dwTimeOut /* = ANIMATION_TIMEOUT  */)
{
	dwTimeOut;
	if( m_bDead )
		return;

	DWORD dwTimePerLoop = 0;
	dwTimePerLoop = (DWORD)((m_fRunStep/m_fCurMoveRate)*500);

	LPCSTR szAction	= "single_rush";
	DWORD  dwRush		= info::single_rush;
	if( HasFightFlag(FIGHTFLAG_FIGHTING) )
	{
		if( IsHaveWeapon() )
			szAction = "single_rush";
		if( IsDoubleHandWeapon() )
			szAction = "double_rush";
	}

	if( m_pAnim->IsUpperBodyAndLowerBodyMode() )
	{
		m_pAnim->PlayLowerAnim(szAction
                            ,PLAYMODE_NORMAL
									 ,dwTimePerLoop
									 ,MAX_ANIMATION_LOOP
									 ,ANIMATION_TIMEOUT
									 ,ANIMATION_BLENDING_TIME
									 ,"idle"
									 ,PLAYMODE_NORMAL);
	}
	else
	{
		m_pAnim->PlayAnim2(dwRush
                        ,PLAYMODE_NORMAL
								,dwTimePerLoop
								,MAX_ANIMATION_LOOP
								,ANIMATION_TIMEOUT
								,info::Idle
								,PLAYMODE_NORMAL);
	}
}

int VPlayer::GetPlayerIdleActionID( int iFightType)
{
	int nIdleActionId;
	nIdleActionId = _SUPER::GetPlayerIdleActionID(iFightType);


	if( HasFightFlag(FIGHTFLAG_FIGHTING) )
	{
		if( IsHaveWeapon() )		//if左手有武器.
		{
			nIdleActionId = GetPlayerActionID( info::MESH_ACTION_single_attack_idle );
		}
		else
		{
			nIdleActionId = info::Attack_idle;
		}

		if( IsDoubleHandWeapon() )
		{
			nIdleActionId = GetPlayerActionID( info::MESH_ACTION_double_attack_idle );
		}
	}
	return nIdleActionId;
}

LPCSTR VPlayer::GetPlayerHurtActionName( int /*iFightType*/)
{
	LPCSTR szName = "single_hurt";

	if( HasFightFlag(FIGHTFLAG_FIGHTING) )
	{
		if( IsHaveWeapon() )		//if左手有武器.
		{
			szName = "single_hurt";
		}
		if( IsDoubleHandWeapon() )
		{
			szName = "double_hurt";
		}
	}
	return szName;
}


void VPlayer::SwitchAnim(STATUS_KEY PreStatus, DWORD dwTimeOut)
{
	//死人不需要坐...
	if (GetSitStatus() && (m_status.CurStatus() != esk_Die))
	{
		SwitchSitAnim(dwTimeOut);
		return;
	}
	_SUPER::SwitchAnim( PreStatus,  dwTimeOut);
}


void VPlayer::_OnAttack(VCharacter* /*pSrcPlayer*/,VCharacter* /*pTargetPlayer*/)
{
}

void VPlayer::_OnMountMonster(BOOL bMountUp)
{
	if(bMountUp)
	{
		if( CheckFightingStatus() )
		{
			ClearFightFlag( FIGHTFLAG_FIGHTING );
		}
	}
	else
	{
	}
}


void VPlayer::_UpdateEffect(DWORD dwTime)
{
	if( !m_pCharEffectContainer )
		return;

	_SUPER::_UpdateEffect(dwTime);


	IGameEffectHelper* pEffectHelper;

	if( m_nRightHandBoneID != -1 )
	{
		pEffectHelper = m_pCharEffectContainer->GetEffectHelper( eEffectBindPart_RightHand );
		Vector3D vRightHandPos = m_pAnim->GetBonePosAtWorld( m_nRightHandBoneID );
		pEffectHelper->SetRot( m_fCurrentDir + math::cPI_V2 );
		pEffectHelper->SetPos( vRightHandPos.x, vRightHandPos.y, vRightHandPos.z );
	}

	if( m_nLeftHandBoneID != -1 )
	{
		pEffectHelper = m_pCharEffectContainer->GetEffectHelper( eEffectBindPart_LeftHand );
		Vector3D vLeftHandPos = m_pAnim->GetBonePosAtWorld( m_nLeftHandBoneID );
		pEffectHelper->SetRot( m_fCurrentDir + math::cPI_V2 );
		pEffectHelper->SetPos( vLeftHandPos.x, vLeftHandPos.y, vLeftHandPos.z );
	}

}

void VPlayer::_UpdateAnim(DWORD /*dwTime*/)
{
	if(m_bMounting )
		return;
	//_SUPER::_UpdateAnim(dwTick);  不用超类

	if(CanRotateAnim())
	{
		m_pAnim->SetRotationZ( m_fLowerBodyDir + math::cPI_V2 );
		m_pAnim->SetUpperRotation( m_fLowerBodyDir + math::cPI_V2 );
		m_pAnim->SetLowerRotation( m_fLowerBodyDir + math::cPI_V2 );
	}

	m_pAnim->SetScale( m_fScale );
	m_pAnim->SetPosition( m_vPosition.x, m_vPosition.y, m_vPosition.z );

	Matrix4 matRot;

	if( IsDead() )
	{
		Vector3 vAxis( 0, 0, 1 );
		if( !m_bInWater && !m_bInFloor && !m_bJumping && theGameWorld.GetHeroPlotMap() )
		{
			vAxis = theGameWorld.GetNormal( m_vPosition.x, m_vPosition.y );
		}
		Vector3 vUp( 0, 0, 1 );
		vAxis.Normalize( );

		BuildMatrixFromVectorToVector( (float*)vAxis, (float*)vUp, (float*)matRot );
		m_pAnim->CalcTransformMatrix( m_Transform );

		m_Transform._41 = 0;
		m_Transform._42 = 0;
		m_Transform._43 = 0;

		m_Transform *= matRot;
		//m_Transform.Multiply(m_Transform,matRot);

		m_Transform._41 = m_vPosition.x;
		m_Transform._42 = m_vPosition.y;
		m_Transform._43 = m_vPosition.z;
	}
	else
	{
		m_pAnim->CalcTransformMatrix( m_Transform );
	}

	m_pAnim->UpdateWorldBBox( m_Transform );
}

float  VPlayer::_GetUpdatePosZ(FLOAT fRoleHeight)
{
	DWORD dwLoadFlags = 0;
	return theGameWorld.GetHeightAt( m_fTerrainZ + fRoleHeight,
							20.0f*SCALE_MESH,
							m_vPosition.x,m_vPosition.y, 
							0, 
							&m_bInFloor,
							&dwLoadFlags,
							FALSE );
}

BOOL VPlayer::_DoStatusMove()
{
	__BOOL_SUPER(_DoStatusMove());
	return TRUE;


}



};//namespace vobject

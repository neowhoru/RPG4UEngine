/*////////////////////////////////////////////////////////////////////////
文 件 名：BattleScene_PacketProc.cpp
创建日期：2008年4月4日
最后更新：2008年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SceneBase.h"
#include "Player.h"
#include "Monster.h"
#include "BattleScene.h"
#include "VarPacket.h"
#include "Clone.h"
#include "WareHouseDialog.h"
#include "PacketInclude.h"
#include "EquipmentContainer.h"
#include "CloneManager.h"
#include "WeaponSoundInfoParser.h"
#include "SkillStateFactory.h"
#include "NetUtil.h"
#include "ConstTextRes.h"
#include "HeroData.h"

using namespace state;
using namespace RC;
class Attributes;
using namespace object;




//------------------------------------------------------------------------------
void BattleScene::ParsePacket( MSG_BASE * pMsg )
{

	PrevPacketHandler(pMsg);

	switch( pMsg->m_byCategory )
	{	
	//////////////////////////////////////////////////////////////
		//case CG_VENDOR:
		//        theVendorDialog.NetworkProc(pMsg);
		//    break;

		//case CG_TRADE:
		//        theTradeDialog.NetworkProc(pMsg);
		//    break;

	//////////////////////////////////////////////////////////////
	case CG_CONNECTION:		OnRecvCG_CONNECTION( pMsg );				return;
	case CG_GM:		         OnRecvCG_GM( pMsg );							break;
	case CG_CHARINFO:			OnRecvCG_CHARINFO( pMsg );					return;	
	case CG_MAP:				OnRecvCG_MAP( pMsg );						return;	
	case CG_SYNC:				OnRecvCG_SYNC( pMsg );						return;	
	case CG_ZONE:				OnRecvCG_ZONE(pMsg );						return;
	case CG_CONVERSATION:	OnRecvCG_CONVERSATION( pMsg );			return;
	case CG_ETC:				OnRecvCG_ETC( pMsg );						return;
	case CG_ITEM:				theItemManager.ParsePacket( pMsg );		return;
	case CG_TRIGGER:			OnRecvCG_TRIGER( pMsg );					return;
	case CG_PARTY:				OnRecvCG_Party( pMsg );						return;
	case CG_SUMMON:			OnRecvCG_Summon( pMsg );					return;

	case CG_BATTLE:			OnRecvCG_BATTLE( pMsg );					return;	
	case CG_STYLE:				OnRecvCG_STYLE( pMsg );						return;
	case CG_SKILL:				OnRecvCG_SKILL( pMsg );						return;	
	case CG_STATUS:			OnRecvCG_STATUS( pMsg );					return;

	case CG_WAREHOUSE:		theWareHouseDialog.NetworkProc(pMsg);	return;

	#ifdef  _EVENT_INVENTORY
	case CG_EVENT:				OnRecvCG_EVENT_INVENTORY( pMsg );		return;
	#endif // _EVENT_INVENTORY


	//////////////////////////////////////////////////////////////
	default:
			SceneBase::ParsePacket(pMsg);
		break;
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL BattleScene::NeedToParsePacket(MSG_BASE * pMsg)
{
	switch( pMsg->m_byCategory )
	{	
	case CG_ZONE:
		{
			switch(pMsg->m_byProtocol)
			{
			case CG_ZONE_MISSION_LEAVE_ACK:
			case CG_ZONE_MISSION_LEAVE_NAK:
			case CG_ZONE_HUNTING_LEAVE_ACK:
			case CG_ZONE_HUNTING_LEAVE_NAK:
			case CG_ZONE_PVP_LEAVE_ACK:
			case CG_ZONE_PVP_LEAVE_NAK:
				return TRUE;
			}
			return FALSE;
		}
		break;
	}

	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL BattleScene::PrevPacketHandler(MSG_BASE * pMsg)
{
	switch( pMsg->m_byCategory )
	{	
	case CG_BATTLE:
	case CG_STYLE:
	case CG_SKILL:
	case CG_STATUS:
			AddBattleTimeSerial();
		break;
	}
	return TRUE;
}


//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_CONNECTION( MSG_BASE * pMsg )
{
	SceneBase::OnRecvCG_CONNECTION(pMsg);
#ifdef NONE_PACKET
	switch(pMsg->m_byProtocol)
	{
	case CG_CONNECTION_BACKTOCHARSELECT_ACK:
		{

			theSceneLogicFlow.GotoCharSelectScene();
		}
		break;

	case CG_CONNECTION_BACKTOCHARSELECT_NAK:
		{
			MSG_CG_CONNECTION_BACKTOCHARSELECT_NAK * pRecvMsg = (MSG_CG_CONNECTION_BACKTOCHARSELECT_NAK *)pMsg;
			pRecvMsg->m_dwErrorCode;
		}
		break;

	default:
		{
			SceneBase::OnRecvCG_CONNECTION(pMsg);
		}
		break;
	}
#endif
}


//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_Party( MSG_BASE * /*pMsg*/ )
{
	assert(0 && " BattleScene..OnRecvCG_Party ");
#pragma message(__FILE__  "(198) BattleScene..OnRecvCG_Party  " )

	//g_InterfaceManager.GetUserInterface(UI_PARTY_LEADER)->NetworkProc(pMsg);
	//for (int i=UI_PARTY_MEMBER_BEGIN; i<UI_PARTY_MEMBER_END; ++i)
	//	g_InterfaceManager.GetUserInterface(i)->NetworkProc(pMsg);
}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_Summon( MSG_BASE * /*pMsg*/ )
{


}

//------------------------------------------------------------------------------
void	BattleScene::OnRecvCG_EVENT_INVENTORY( MSG_BASE * /*pMsg*/ )
{
	//#if defined ( _EVENT_INVENTORY )
	//	theEventInventoryDialog.NetworkProc(pMsg);
	//#endif
}


//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_ZONE( MSG_BASE * /*pMsg*/ )
{

}


//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_CHARINFO( MSG_BASE * /*pMsg*/ )
{

}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_MAP( MSG_BASE * /*pMsg*/ )
{

}





//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_SYNC( MSG_BASE * pMsg )
{

	switch( pMsg->m_byProtocol )
	{
	case CG_SYNC_PLAYER_ENTER_ACK:
		{
			MSG_CG_SYNC_PLAYER_ENTER_ACK	*pRecvPacket = (MSG_CG_SYNC_PLAYER_ENTER_ACK *)pMsg;

			Vector3D vPos3D(pRecvPacket->m_fPos);

			if(!singleton::ExistHero())
			{
				theObjectManager.Add(theHeroData.GetHeroInfo().m_dwKey
                               ,HERO_OBJECT
										 ,theHeroData.GetHeroInfo().m_CharInfo.m_byClassCode);

				theHero.SetHeroInfo(theHeroData.GetHeroInfo());
			}

			//theHero.SetPosition(vPos3D );
			theHero.JumpTo( vPos3D );

#pragma message(__FILE__  "(677)  Hero进入地图后，可通过地图相机信息，设定初始化方向 " )
			//if ( theMap.GetPlayerDirection( &vPos3D ) )
			//{
			//	theHero.SetDirection(
			//		&theMap.GetExtraCameraInfo()->rTargetPos, 
			//		&theMap.GetExtraCameraInfo()->rPos);

			//	theCamera.SetCameraPosition(&theHero.GetVisiblePos());
			//	theCamera.SetYaw( vPos3D.GetAngleByACos() ); 
			//}


			m_bWaitEnterSync	= FALSE;
			m_bPlayerAck		= TRUE;

			theCursorHandler.ForceChange( cursor::CURSORTYPE_DEFAULT );

			/// 初始化战区NPC
			//SettingNPC(theMap.GetMapID(),	theMap.GetCurrentFieldID());	


			//uiGuildMan* guildMan =
			//	static_cast<uiGuildMan *>(g_InterfaceManager.GetUserInterfaceManager(UIMAN_GUILD));
			//if (guildMan)
			//{
			//	guildMan->Clear();
			//	guildMan->SetSilentGuildInfo();
			//	guildMan->SEND_CG_GUILD_SELECT_SYN();
			//}
		}
		break;

	case CG_SYNC_PLAYER_ENTER_NAK:
		{
			//	Check The Map Version - CG_SYNC_PLAYER_ENTER_NAK - LoginScene
			m_bWaitEnterSync = TRUE;

			theGameUIManager.UIMessageBox(TEXTRES_INVALID_MAPVERSION);
		}
		break;


	case CG_SYNC_ALLPLAYERS_CMD:
		{
			MSG_CG_SYNC_ALLPLAYERS_CMD *pRecvPacket = (MSG_CG_SYNC_ALLPLAYERS_CMD *)pMsg;

			BYTE *pPacket = ( (BYTE*)(pRecvPacket) + sizeof(MSG_CG_SYNC_ALLPLAYERS_CMD) );

			sPLAYERINFO_RENDER *pRenderInfo = NULL;
			sGUILDINFO_RENDER *pGuildInfo = NULL;
			sVENDORINFO_RENDER *pVendorInfo = NULL;
        
			for (int i = 0; i < pRecvPacket->m_byCount; i++)
			{

				pRenderInfo  = (sPLAYERINFO_RENDER *)pPacket; 
				pGuildInfo   = (sGUILDINFO_RENDER *)( (BYTE*)(pPacket) + pRenderInfo->GetSize() );
				pVendorInfo  = (sVENDORINFO_RENDER *)( (BYTE*)(pGuildInfo) + pGuildInfo->GetSize() );

				if (singleton::ExistHero())
				{
					if (theHero.GetObjectKey() == pRenderInfo->m_wPlayerKey)
					{
						ASSERT(!"Hero  ALLPLAYER_CMD invalid!");
						pPacket += NetUtil::GetPlayerSize(pPacket);
						continue;
					}

				}
				
				Player* pPlayer = (Player*)theObjectManager.Add( pRenderInfo->m_wPlayerKey, PLAYER_OBJECT, pRenderInfo->m_byClass );
				if( !pPlayer )	
				{
					pPacket += NetUtil::GetPlayerSize(pPacket);
					continue;
				}

				Vector3D wvPos( pRenderInfo->m_fPos);
				pPlayer->SetPosition(wvPos );
				pPlayer->SetPlayerInfo( pRenderInfo );
				pPlayer->SetName(pRenderInfo->m_szName);
				pPlayer->SetCurrentAttackStyle(pRenderInfo->m_SelectStyleCode,FALSE);
				pPlayer->SetGMGrade( pRenderInfo->m_byGMGrade );

				if (pRenderInfo->m_byHelmetOption)
				{
					pPlayer->HideHelmet(TRUE,FALSE);

				}
				else
				{
					pPlayer->HideHelmet(FALSE,FALSE);
				}
				
            pPlayer->SetGuildInfo(pGuildInfo);
            pPlayer->SetVendorInfo(pVendorInfo);

				pPacket += NetUtil::GetPlayerSize(pPacket);

				
            }
		}
		break;



	}
}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_BATTLE( MSG_BASE * /*pMsg*/ )
{

}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_GM( MSG_BASE * /*pMsg*/ )
{

}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_STYLE( MSG_BASE * /*pMsg*/ )
{

}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_SKILL( MSG_BASE * /*pMsg*/ )
{

}

//------------------------------------------------------------------------------
void BattleScene::ShowErrorCodeSkill(DWORD code)
{
	theHero.ProcessPlayerError(code,CG_SKILL);

}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_CONVERSATION( MSG_BASE * /*pMsg*/ )
{
	//theChatDialog.NetworkProc( pMsg );
}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_ETC( MSG_BASE * /*pMsg*/ )
{
	//switch( pMsg->m_byProtocol )
	//{
	//case CG_ETC_CMD:
	//	break;
	//}
}

//------------------------------------------------------------------------------
BOOL BattleScene::ChangeStatus(DWORD dwObjKey, BYTE byConditionType)
{
	Character *pChar=(Character *)theObjectManager.GetObject(dwObjKey);
	if(pChar)
	{
		pChar->SetCondition(byConditionType);
		pChar->SetEmotion( 0 );
	}

	return TRUE;
}


//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_STATUS( MSG_BASE * /*pMsg*/ )
{

}

//------------------------------------------------------------------------------
void BattleScene::OnRecvCG_TRIGER( MSG_BASE * /*pMsg*/ )
{

}


//------------------------------------------------------------------------------
//  uiSkillPassive_func.cpp
//  (C) 2005 ckbang
//------------------------------------------------------------------------------
#include "stdafx.h"
#include "UISkillPassive.h"
#include "SlotUIListener.h"
#include "SkillPaneSlot.h"
#include "SkillInfoParser.h"

#include "SkillStorageParser.h"
#include "ObjectManager.h"
#include "SlotKeyGenerator.h"
#include "Hero.h"
#include "PlayerAttributes.h"
#include "QuickContainer.h"
#include "ItemManager.h"
//#include "SkillStorageManager.h"

UISkillPassive::UISkillPassive()
{
	m_pSlotListener = NULL;
}

UISkillPassive::~UISkillPassive()
{
    //SAFE_RELEASENDELETE(m_pSlotListener);
	m_pSlotListener = NULL;
}

BOOL UISkillPassive::Init(UINT nSlotNum)
{
	SkillPaneContainer::Init(nSlotNum);
	return TRUE;
}

void UISkillPassive::Release()
{
	_SUPER::Release();
}

//------------------------------------------------------------------------------
/** ITEM UNIT RENDERER
*/
void
UISkillPassive::InitSlotListener()
{



	 if(m_pSlotListener)
      m_pSlotListener->Init(GetSlotMaxSize() );

}

//------------------------------------------------------------------------------
/** ITEM UNIT RENDERER
*/
void
UISkillPassive:://ResetItemRenderer()
{
    //SAFE_RELEASENDELETE(m_pSlotListener);
	m_pSlotListener = NULL;

    InitSlotListener();
}

//------------------------------------------------------------------------------
/** ITEM UNIT RENDERER
*/
//SlotUIListener * 
//UISkillPassive::GetItemUnitRender() const
//{
//    return m_pSlotListener;
//}

//------------------------------------------------------------------------------
/**
working for slot clean

called where to Hero::ReleaseContainer().
*/
void
UISkillPassive::FlushSlotListener()
{
    SkillPaneSlot FromSlot;

    for (int i = 0; i < GetSlotMaxSize(); i++)
    {
        if( !IsEmpty(i) )
        {
            SERIALTYPE serial = GetSlot(i).GetSerial();
            theSlotKeyGenerator.Restore(serial);

            DeleteSlot(i, &FromSlot);
        }
    }

}

//------------------------------------------------------------------------------
/**
*/
BOOL
UISkillPassive::InsertSlot( SLOTPOS posIndex, BaseSlot & IN slotDat )
{
    SkillSlotContainer::InsertSlot(posIndex, slotDat);

    SkillSlot& slot =
        (SkillSlot &)GetSlot(posIndex);

    RECT rcSlot = {0,0, 100, 100};



	 if(m_pSlotListener)
		 m_pSlotListener->OnSlotAdd(
			  slot.GetSerial(),
			  slot.GetCode(),
			  &slot );

    return TRUE;
}

//------------------------------------------------------------------------------
/**
*/
void
UISkillPassive::DeleteSlot(SLOTPOS posIndex, BaseSlot * pSlotOut )
{
    SkillSlotContainer::DeleteSlot(posIndex, pSlotOut);

    if (!m_pSlotListener)
        return;

    if (0 != pSlotOut->GetSerial())
    {
        if (m_pSlotListener)
            m_pSlotListener->OnSlotRemove( pSlotOut->GetSerial() );
    }
}

//------------------------------------------------------------------------------
/**
*/
void
UISkillPassive::Clear()
{
    FlushSlotListener();
    m_sRequiredSkillStat1 = 0;
    m_sRequiredSkillStat2 = 0;
    m_dwRemainPoint = 0;
    m_lvStyleLevel = 0;
    InitSlotListener();


}

//------------------------------------------------------------------------------
//  EOF
//------------------------------------------------------------------------------

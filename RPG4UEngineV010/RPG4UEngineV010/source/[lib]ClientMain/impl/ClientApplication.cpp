/*////////////////////////////////////////////////////////////////////////
文 件 名：ClientApplication.cpp
创建日期：2007年11月9日
最后更新：2007年11月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ApplicationSetting.h"
#include "V3DConfig.h"
#include "GraphicSetting.h"
#include "BaseData.h"
#include "LogSystem.h"
#include "ClientApplication.h"
#include "RenderSystem.h"
#include "RenderSystemManager.h"
#include "GameStartUp.h"
#include "GameRunning.h"


static LPCSTR MAIN_CLASS	= _T("_RPG4U_ENGINE_");

ClientApplication::ClientApplication(GameRunningBase&	gameRunning
												,GameStartUp&		gameStartUp)
:m_Running(gameRunning)
,m_StartUp(gameStartUp)
{
	m_nAppStatus	= APP_INIT;
}

ClientApplication::~ClientApplication()
{
}

LRESULT ClientApplication::MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam )
{
	if(::IsWindow(m_hMainWnd))
	{
		RenderSystem* pRenderSystem = GetActiveRender();
		if(pRenderSystem)
			pRenderSystem->MsgProc(  hWnd,  uMsg,  wParam,  lParam );
	}

	return _SUPER::MsgProc( hWnd, uMsg, wParam, lParam );
}


BOOL ClientApplication::OnRun()
{
	if(m_nAppStatus != APP_RUNNING)
		return TRUE;

	__BOOL_CALL( m_Running.Run());

	return TRUE; 
}


BOOL ClientApplication::PreCreateApp()
{ 
	if(!m_StartUp.InitStartUp(m_hInstance))
		return FALSE;

	return TRUE;
}

BOOL ClientApplication::OnCreateApp()
{ 
	m_nAppStatus	= APP_RUNNING;
	m_Running.SetApplication(this);
	return TRUE;
}

BOOL ClientApplication::PreCreateMainWnd()
{ 
	return TRUE;
}


LPCSTR ClientApplication::OnCreateWndClass(WNDPROC     lpfnWndProc)
{
	//WNDCLASS wndClass = 
	//{ 
	//	CS_DBLCLKS,
	//	lpfnWndProc, 
	//	0,
	//	0,
	//	m_hInstance,
	//	NULL,//LoadIcon( m_hInstance, MAKEINTRESOURCE(IDI_MAIN_ICON) ),
	//	LoadCursor( NULL, IDC_ARROW ),
	//	(HBRUSH)GetStockObject(WHITE_BRUSH),
	//	NULL,
	//	_T("_RPG4U_")
	//	
	//};

	//ATOM  ret= RegisterClass( &wndClass );
	//return ret != 0;
	WNDCLASSEX wcex;

	wcex.cbSize = sizeof(WNDCLASSEX);

	wcex.style				= CS_HREDRAW | CS_VREDRAW;
	wcex.lpfnWndProc		= lpfnWndProc;
	wcex.cbClsExtra		= 0;
	wcex.cbWndExtra		= 0;
	wcex.hInstance			= m_hInstance;
	wcex.hIcon				= NULL;//LoadIcon(hInstance, MAKEINTRESOURCE(IDI_HELLOW));
	wcex.hCursor			= LoadCursor(NULL, IDC_ARROW);
	wcex.hbrBackground	= (HBRUSH)(COLOR_WINDOW+1);
	wcex.lpszMenuName		= NULL;//MAKEINTRESOURCE(IDC_HELLOW);
	wcex.lpszClassName	= MAIN_CLASS;
	wcex.hIconSm			= NULL;//LoadIcon(wcex.hInstance, MAKEINTRESOURCE(IDI_SMALL));

	if( RegisterClassEx(&wcex) != 0)
		return MAIN_CLASS;
	return NULL;
}

BOOL ClientApplication::OnCreateMainWnd()
{
	if(!m_StartUp.StartUp())
		return FALSE;

	///////////////////////////////
	StringHandle	sTitle;
	sTitle.Format(_T("%s[%u]"),_GAMENAME_,_GAMEVERSION_);

	::SetWindowText(m_hMainWnd,sTitle);

	return TRUE;
}


BOOL ClientApplication::OnDestroyApp()
{ 
	//防止重入
	if(m_nAppStatus == APP_EXIT)
		return TRUE;

	m_nAppStatus	= APP_EXIT;

	RemoveListener(&m_Running);

	m_StartUp.Shutdown();
	


	return TRUE;
}

BOOL ClientApplication::OnResizeMainWnd()
{
	return TRUE;
}

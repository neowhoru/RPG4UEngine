/*////////////////////////////////////////////////////////////////////////
文 件 名：BattleScene.cpp
创建日期：2008年4月4日
最后更新：2008年4月4日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketInclude.h"
#include "SceneBase.h"
#include "BattleScene.h"
#include "GameInPlaying.h"
#include "Monster.h"
#include "MapNPC.h"
#include "VarPacket.h"
#include "MapObject.h"
#include "SkillEffectManager.h"
#include "SkillEffect.h"
#include "HeroEventManager.h"
#include "LoadScene.h"
#include "VillageScene.h"
#include "SkillEffectManager.h"
#include "NetworkLayer.h"
#include "SkillInfoParser.h"
#include "NPCInfoParser.h"
#include "BGMusicInfoParser.h"
#include "EquipmentContainer.h"
#include "ServerUtil.h"
#include "KeyQueueManager.h"
#include "HeroTriggerManager.h"
#include "StateInfoParser.h"
#include "SkillResultManager.h"
#include "AbilityResultManager.h"
#include "HeroData.h"


using namespace input;
///////////////////////////////////////////////////////////////
DWORD					BattleScene::ms_RoomKey = 0;
BattleRoomInfo	BattleScene::ms_PendingRoomInfo;
///////////////////////////////////////////////////////////////



//------------------------------------------------------------------------------
BattleScene::BattleScene()
{
	m_dwFreshTipGroup	= TIPGROUP_PLAYING;
}

//------------------------------------------------------------------------------
BattleScene::~BattleScene()
{
}



//------------------------------------------------------------------------------
BOOL BattleScene::Init()
{
	__BOOL_SUPER(Init());

	return TRUE;
}

void BattleScene::Release()
{
}

//----------------------------------------------------------------------------
void BattleScene::SendPlayerEnterSyn(BOOL bWaitDraw )
{
	MSG_CG_SYNC_PLAYER_ENTER_SYN		SendPacket;
	SendPacket.m_dwCheckSum			= 0;
	//SendPacket.m_dwCheckSum		= theTileMap.GetCheckSum();

	LOGINFO( "Check Sum : %d\n", SendPacket.m_dwCheckSum);

	__VERIFY2(theNetworkLayer.SendPacket(CK_GAMESERVER
                             ,&SendPacket
									  ,sizeof(MSG_CG_SYNC_PLAYER_ENTER_SYN))
				,"发送失败 - MSG_CG_SYNC_PLAYER_ENTER_SYN"
				,);

	m_bWaitEnterSync	= TRUE;
	m_bDrawWait			= bWaitDraw;
}


//------------------------------------------------------------------------------
void BattleScene::Activate()
{
}

BOOL BattleScene::OnChangeFrameworkIn()
{
	//theGameRunning.ResetCurTime();
	theGameRunning.AddListener(this);

	__BOOL_SUPER(OnChangeFrameworkIn());

	__INIT(theGameInPlaying.RequestWorkingData());


	m_bComebackFromLobby = FALSE;
	m_bPlayerAck			= FALSE;
	m_nRenderCounter		= 0;
	m_bDrawWait				= FALSE;
	m_bWaitEnterSync		= FALSE;
	m_bProcessEnterWorld	= FALSE;
	m_iShowTextInfo		= 0;

	m_dwBattleTimeSerial = 0;

	//-------------------------------------------------------------------------------
	if( theGeneralGameParam.IsEnableNetwork() )
	{
		SendPlayerEnterSyn();
	}

	InitializeMissionSwitch();
	theCloneManager.Clear();
	m_MoveInfoList.clear();

	//if( theGeneralGameParam.IsFreeCameraState() )
	//{
	//	theGeneralGameParam.SetFreeCameraState( FALSE );
	//}

	theKeyQueueManager.Reset();
	theKeyQueueManager.SetKey(KEYC_ESCAPE);
	theKeyQueueManager.SetDefaultFun(GlobalUtil::DoSystemDialog);

	return TRUE;
}



//------------------------------------------------------------------------------
BOOL BattleScene::OnChangeFrameworkOut()
{
	__BOOL_SUPER(OnChangeFrameworkOut());

	__END(theGameInPlaying.ReleaseWorkingData());

	theGameRunning.RemoveListener(this);

	theSound.StopEffectSoundAll();

	m_MoveInfoList.clear();

	m_bPlayerAck = FALSE;

	ReleaseObjects();


	//if( theGeneralGameParam.IsFreeCameraState() )
	//{
	//	theGeneralGameParam.SetFreeCameraState( FALSE );
	//}

	return TRUE;
}


BOOL BattleScene::FrameMove( DWORD dwTick )
{	
	//static int iCount = 0;

	//static BOOL bOld = FALSE;

	m_dwProcessStartTick = GetTickCount();

	__BOOL_SUPER(FrameMove(dwTick));
	__BOOL_CALL(theGameInPlaying.FrameMove(dwTick));


	theCloneManager.FrameMove(dwTick);



	PROFILE_PROC_BEGIN(_T("ProcessUnits"));
	ProcessUnits(dwTick);
	PROFILE_PROC_END(_T("ProcessUnits"));

	if ( m_bWaitEnterSync == FALSE)
		m_bDrawWait = FALSE;


	////////////////////////////////////////////
	if( m_nRenderCounter > 10)
	{
		if(!m_bProcessEnterWorld)
		{
			m_bProcessEnterWorld = TRUE;
			theGameInPlaying.OnEnterWorld();
		}
	}


	ProcessMoveInfo(dwTick);

	m_dwProcessEndTick = GetTickCount();


	//if (m_dwMissionClearTick + 10000 < GetCurrentTick() )
	//{
	//	m_dwMissionClearTick = 0;

	//	/*
	//	MSG_CG_CONNECTION_BACKTOCHARSELECT_SYN SendPacket;
	//	SendPacket.m_dwKey = theGeneralGameParam.GetUserID();
	//	SendPacket.m_byCategory = CG_CONNECTION;
	//	SendPacket.m_byProtocol	= CG_CONNECTION_BACKTOCHARSELECT_SYN;
	//	theNetworkLayer.SendPacket( CK_GAMESERVER, &SendPacket, sizeof(SendPacket) );
	//	ShowWindow( InterfaceManager::DIALOG_SYSTEM, FALSE );
	//	*/
	//}

	return TRUE;
}



BOOL BattleScene::RenderHUD(DWORD dwTick)
{
	//__BOOL_SUPER(RenderHUD(dwTick));
	__BOOL_CALL(theGameInPlaying.RenderHUD(dwTick));
	return TRUE;
}

//------------------------------------------------------------------------------
BOOL BattleScene::Render( DWORD dwTick )
{	
	m_dwRenderStartTick = GetTickCount();

	__BOOL_SUPER(Render(dwTick));
	__BOOL_CALL(theGameInPlaying.Render(dwTick));


	PROFILE_BEGIN( _T("RenderOthers") );
	theCloneManager.Render(dwTick);
	PROFILE_END( _T("RenderOthers") );



	if (g_BaseSetting.m_ShowDebugInfo)
	{
		RenderMoveInfo();
		RenderDebugText(dwTick);
	}


	m_nRenderCounter++;
	theHeroTriggerManager.SetCanProcess(TRUE);


	m_dwRenderEndTick = GetTickCount();

	return TRUE;
}



//------------------------------------------------------------------------------
void BattleScene::ProcessUnits(DWORD dwTick)
{
	Object *			pObject;
	ObjectMapIt		itDel;
	ObjectMapIt		it = theObjectManager.GetBegin();

	while(it != theObjectManager.GetEnd() )
	{		
		pObject = static_cast<Object *>(it->second);
		if (!pObject)
		{
			ASSERT(!"Object Is NULL!!");
			continue;
		}

		if (!pObject->Process(dwTick))
		{
			itDel = it;
			it++;
			theObjectManager.Delete(itDel->first);
			continue;
		}

		it++;
	}
}


//------------------------------------------------------------------------------
DWORD BattleScene::AddBattleTimeSerial()
{
	DWORD ret = m_dwBattleTimeSerial;
	m_dwBattleTimeSerial++;
	return ret;
}



//------------------------------------------------------------------------------
void BattleScene::InitializeMissionSwitch()
{

	for (int nIndex = 0; nIndex < MAX_MISSION_SWITCH; nIndex++)
	{
		m_arMissionSwitch[nIndex] = INVALID_BYTE_ID;
	}
}

//------------------------------------------------------------------------------
BYTE BattleScene::GetMissionSwitchValue(int index)
{
	ASSERT(index >= 0 && index < MAX_MISSION_SWITCH);
	return m_arMissionSwitch[index];
}

//------------------------------------------------------------------------------
void BattleScene::SetMissionSwitchValue(int index,BYTE value)
{
	ASSERT(index >= 0 && index < MAX_MISSION_SWITCH);
	m_arMissionSwitch[index] = value;
}



//------------------------------------------------------------------------------
int BattleScene::GetCharacterHP(DWORD dwID)
{
	Character *pChr = (Character *)theObjectManager.GetObject(dwID);
	if (pChr)
	{
		return pChr->GetHP();
	}

	return 0;
}


//------------------------------------------------------------------------------
void BattleScene::BackToTheVillage()
{
	ASSERT(singleton::ExistHero());

	BattleScene::SetRoomKey(0);
	theHeroData.SaveHero(theHero);

	theSceneLogicFlow.GotoVillageScene	((MAPCODE)theHero.GetCharInfo()->m_dwRegion
													,theHero.GetCharInfo()->m_sLocationX
													,theHero.GetCharInfo()->m_sLocationY
													);
}

//----------------------------------------------------------------------------
void	BattleScene::BackToTheVillageSyn()
{
	switch (theFramework.GetType() )
	{
	case SCENE_TYPE_HUNTING:
		{
			//pSystemDialog->AddPacketStatus(SystemDialog::BACK_TO_THE_VILLAGE);
			MSG_CG_ZONE_HUNTING_LEAVE_SYN SendPacket;
			theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket));
		}
		break;

	case SCENE_TYPE_MISSION:
		{
			//pSystemDialog->AddPacketStatus(SystemDialog::BACK_TO_THE_VILLAGE);

			MSG_CG_ZONE_MISSION_LEAVE_SYN SendPacket;
			theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket));
		}
		break;

	case SCENE_TYPE_LOBBY:
		{
			//pSystemDialog->AddPacketStatus(SystemDialog::BACK_TO_THE_VILLAGE);

			MSG_CG_ZONE_LOBBY_LEAVE_SYN SendPacket;
			theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket));
		}
		break;

	case SCENE_TYPE_VILLAGE:
		{
			if (theVillageScene.IsLobby())
			{
				//pSystemDialog->AddPacketStatus(SystemDialog::BACK_TO_THE_VILLAGE);
				MSG_CG_ZONE_LOBBY_LEAVE_SYN	SendPacket;
				theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket));
			}
		}
		break;

	case SCENE_TYPE_PVP:
		{
			//pSystemDialog->AddPacketStatus(SystemDialog::BACK_TO_THE_VILLAGE);
			MSG_CG_ZONE_PVP_LEAVE_SYN SendPacket;
			theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket));
		}
		break;
	}
}





//------------------------------------------------------------------------------
void BattleScene::ReleaseObjects()
{
	//theCloneManager.Clear();
}


void BattleScene::RenderPacketBufferSize()
{

}


void BattleScene::RenderKeyQueueNum()
{

}


void BattleScene::RenderSkillQueueNum()
{

}




void BattleScene::RenderResourceStatus()
{

	//theHero.RenderArrayEquipment();
}





//------------------------------------------------------------------------------
void BattleScene::AddAttackResultAction	(ATTACK_RESULT_TARGET_INFO* pResult
                                          ,DWORD                      dwTimeSerial
														,DWORD                      size)
{
	Character *pTarget;

	for (DWORD nIndex = 0; nIndex < size; nIndex++)
	{
		pTarget = (Character *)theObjectManager.GetObject(pResult[nIndex].dwTargetID);
		if (!pTarget) 
			continue;


		if (pResult[nIndex].bMiss) 
			continue;


		if (pResult[nIndex].dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECT_DOWN)
		{
			//ASSERT(!pTarget->IsKindOfObject(PLAYER_OBJECT));
			PLAYER_ACTION		action;
			action.ActionID				= ACTION_DOWN;
			action.dwTimeSerial			= dwTimeSerial;
			action.DOWN.fInitialSpeed	= theApplicationSetting.m_KnockBackInitSpeed;

			//if(pTarget->GetObjectKey()==theHero.GetObjectKey())
			//{
			//action.DOWN.vCurPos.vvPos			= theHero.GetPosition();
			//action.DOWN.vCurPos.m_TileIndex	=-1;
			//}
			//else

			action.DOWN.vCurPos	= pResult[nIndex].vCurPosition;
			action.DOWN.vDestPos = pResult[nIndex].vDestPosition;

			if (pResult[nIndex].dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECT_KNOCKBACK)
			{
				action.DOWN.dwEndTime   = g_CurTime + 2000;
				action.DOWN.dwGetupTime = g_CurTime + 3000;
			}
			else
			{
				action.DOWN.dwEndTime   = g_CurTime + 1000;
				action.DOWN.dwGetupTime = g_CurTime + 2000;
			}
			action.DOWN.bFly = TRUE;
			pTarget->PutAction(&action);

		}	

		//////////////////////////////////
		else if (pResult[nIndex].dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECT_KNOCKBACK)
		{
			PLAYER_ACTION action;
			action.ActionID						= ACTION_KNOCKBACK;
			action.dwTimeSerial					= dwTimeSerial;
			action.KNOCKBACK.fInitialSpeed	= theApplicationSetting.m_KnockBackInitSpeed;

			//if(pTarget->GetObjectKey()==theHero.GetObjectKey())
			//{
			//action.KNOCKBACK.vCurPos.vvPos = theHero.GetPosition();
			//action.KNOCKBACK.vCurPos.m_TileIndex =-1;
			//}
			//else
			action.KNOCKBACK.vCurPos		= pResult[nIndex].vCurPosition;
			action.KNOCKBACK.vDestPos		= pResult[nIndex].vDestPosition;
			action.KNOCKBACK.dwEndTime		= g_CurTime + 10000;
			pTarget->PutAction(&action);
		}

		///////////////////////////////////////
		else if (pResult[nIndex].dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECT_AIR)
		{				
			if(!pTarget->CannotAir())
			{

				PLAYER_ACTION action;
				action.ActionID					= ACTION_TOAIR;
				action.dwTimeSerial				= dwTimeSerial;
				action.TOAIR.fDownForceLimit	= -40.0f;
				action.TOAIR.fUpperForce		= 50.0;
				action.TOAIR.fGravity			= 70.0f;
				action.TOAIR.bForceDown			= TRUE;
				action.TOAIR.fHeightLimit		= 3.4f;
				action.TOAIR.dwAirTime			= pResult[nIndex].dwAirTime;
				action.TOAIR.dwGetupTime		=  pResult[nIndex].dwGetupTime;			
				pTarget->PutAction(&action);
			}
		}
	}
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void BattleScene::DistributeDamage	( DWORD                      dwSkillId
												 ,Character*                 pChr
												 ,eWEAPONSOUNDKIND           eSoundKind
												 ,eARMOUR_TEX_TYPE		  eArmourTex
												 ,ATTACK_RESULT_TARGET_INFO* pResult
												 ,int                        nHitCount
												 ,DWORD                      dwAttackSerial
												 ,DWORD                      dwTimeSerial
												 ,DWORD                      size
												 ,DWORD                      startGroupId)
{
	ATTACK_RESULT attackResult={0};

	attackResult.dwAttackSerial		= dwAttackSerial;
	attackResult.dwTimeSerial			= dwTimeSerial;
	attackResult.dwWeaponSoundKind	= eSoundKind;
	attackResult.dwArmourTexture		= eArmourTex;
	attackResult.dwSkillID				= dwSkillId;
	attackResult.dwTimeStart			= g_CurTime;



	for (DWORD nIndex = 0; nIndex < size; nIndex++)
	{
		int hp = GetCharacterHP(pResult[nIndex].dwTargetID);

		if (pResult[nIndex].wDamage == 0)
		{
			pResult[nIndex].bMiss = TRUE;
		}

		if ((int)pResult[nIndex].wDamage >= hp)
		{				
			pResult[nIndex].wDamage = (DAMAGETYPE)hp;
		}
	}

	///////////////////////////////////////////////////
	if (nHitCount > 1)
	{
		DWORD dwTotalDamage	[MAX_ATTACK_RESULT_TARGET];
		DWORD dwLastHP			[MAX_ATTACK_RESULT_TARGET];

		for (DWORD nIndex = 0; nIndex <  size; nIndex++) 
		{
			dwTotalDamage[nIndex]	= pResult[nIndex].wDamage;			
			dwLastHP[nIndex]			= pResult[nIndex].dwTargetHP;							
		}

		for (int b = 0; b < nHitCount - 1; b++ )
		{
			attackResult.dwGroupId	= startGroupId+b+1;

			for (DWORD nIndex = 0; nIndex < size; nIndex++) 
			{
				DWORD firstDamage				= (dwTotalDamage[nIndex] / nHitCount);
				pResult[nIndex].wDamage    = (DAMAGETYPE)firstDamage;
				pResult[nIndex].dwTargetHP = dwLastHP[nIndex] + firstDamage * (nHitCount - b - 1);	

				attackResult.TargetInfo=pResult[nIndex];

				pChr->AddAttackResult(&attackResult);		
			}			
		}

		attackResult.dwGroupId++;// 

		////////////////////////////////////////////////////
		for (DWORD nIndex = 0; nIndex < size; nIndex++) 
		{
			DWORD firstDamage = (dwTotalDamage[nIndex] / nHitCount);
			DWORD lastDamage	= dwTotalDamage[nIndex] - (firstDamage * (nHitCount - 1));	

			pResult[nIndex].wDamage       = (DAMAGETYPE)lastDamage;
			pResult[nIndex].dwTargetHP    = dwLastHP[nIndex];
			attackResult.TargetInfo			= pResult[nIndex];
			pChr->AddAttackResult(&attackResult);
		}
	}
	else
	{
		for (DWORD nIndex = 0; nIndex < size; nIndex++) 
		{
			attackResult.TargetInfo	= pResult[nIndex];
			attackResult.dwGroupId	= startGroupId+1;
			pChr->AddAttackResult(&attackResult);		
		}
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void BattleScene::QueueSkillAction(PLAYER_ACTION *pAction,Character *pChar)
{
	__CHECK2_PTR(pChar,);

	if (theGeneralGameParam.IsServerDebug()) 
	{
		pChar->DoAction(pAction);
		pChar->ForceStateChange(g_CurTime);

	}
	else
	{
		pChar->PutAction(pAction);
	}			
}


//------------------------------------------------------------------------------
char *BattleScene::AddSkillResult	(Character* pChr
                                    ,BYTE       byNumTarget
												,DWORD      dwClientSerial
												,DWORD      dwSkillCode
												,DWORD      dwPrimaryTargetKey
												,char*      pPacket				
												)
{
	if (byNumTarget == 0)
		return pPacket;

	sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillCode);
	__VERIFY2_PTR	(pInfo
						, "技能信息不可为空"
						, pPacket);

	ASSERT(byNumTarget < MAX_ATTACK_RESULT_TARGET);


	////////////////////////////////////////////////////
	sSKILLRESULT_OWNER	ownerInfo;

	ownerInfo.pOwner					= this;
	ownerInfo.pChar					= pChr;
	ownerInfo.byNumTarget			= byNumTarget;
	ownerInfo.dwClientSerial		= dwClientSerial;
	ownerInfo.dwSkillCode			= dwSkillCode;
	ownerInfo.dwPrimaryTargetKey	= dwPrimaryTargetKey;
	ownerInfo.pPacket					= pPacket;
	ownerInfo.mainTargetIndex		= INVALID_DWORD_ID;
	ownerInfo.eArmourTex				= ARMOUR_TEX_CLOTH;

	////////////////////////////////////////////////////
	int		iResultSize				= 0;

	Character*							pTargetObject;
	eWEAPONSOUNDKIND&					eSoundKind			= ownerInfo.eSoundKind;
	eARMOUR_TEX_TYPE&					eArmourTex			= ownerInfo.eArmourTex;
	ATTACK_RESULT&						attackresult		= ownerInfo.attackResult;
	DWORD&								mainTargetIndex	= ownerInfo.mainTargetIndex;
	ATTACK_RESULT_TARGET_INFO*		TargetInfo			= ownerInfo.TargetInfo;



	////////////////////////////////////////////////////
	pTargetObject		= (Character*)pChr->GetTargetObject();
	eSoundKind			= pChr->GetWeaponSoundKind();

	if(pTargetObject && pTargetObject->IsKindOfObject(CHARACTER_OBJECT))
		eArmourTex		= pTargetObject->GetArmourTexture();

	////////////////////////////////////////////////////
	for (int nIndex = 0; nIndex < byNumTarget; nIndex++)
	{
		SKILL_RESULT_BASE *pResult = (SKILL_RESULT_BASE *)pPacket;

		ZeroMemory(&attackresult,sizeof(attackresult));
		attackresult.dwAttackSerial = dwClientSerial;
		attackresult.dwTimeSerial   = GetBattleTimeSerial();      
		attackresult.dwTimeStart    = g_CurTime;
		//attackresult.dwTargetNum    = byNumTarget;

		iResultSize = AddTargetResultInfo	(pChr
                                          ,pResult
														,&attackresult.TargetInfo
														,dwSkillCode
														,dwClientSerial);

		TargetInfo[nIndex]=attackresult.TargetInfo;

		if(dwPrimaryTargetKey == TargetInfo[nIndex].dwTargetID)
			mainTargetIndex=nIndex; //

		//if (theGeneralGameParam.GetSpecialMode())
		//{
		//	if (theHero.GetObjectKey() == TargetInfo[nIndex].dwTargetID)						
		//	{
		//		if (TargetInfo[nIndex].dwTargetHP == 0)
		//		{
		//		}
		//	}
		//}

		pPacket += iResultSize;
	}


	if(!theSkillResultManager.ProcessSkillResult	(pInfo->m_SkillClassCode
                                                ,ownerInfo)		)
	{
	}





	/////////////////////////////////////////////////////////////////// 
	if (theGeneralGameParam.IsServerDebug())
	{
		pChr->ProcessAttackResult(dwClientSerial,TRUE);//true
	}


	/////////////////////////////////////////////////////////////////// 
	//  AttackSerial
	if ( dwClientSerial < pChr->GetNextAttackSerial() )
	{
		assert(!"待测 弄清点解调用GetNextAttackSerial... 唔调 GetCurrentAttackSerial ");
		pChr->ProcessAttackResult(dwClientSerial,TRUE);
	}

	return pPacket;
}


//------------------------------------------------------------------------------
int  BattleScene::AddTargetResultInfo	(Character*                 pChr
                                       ,SKILL_RESULT_BASE*         pResult
													,ATTACK_RESULT_TARGET_INFO* pInfo
													,DWORD                      dwSkillCode
													,DWORD                      /*dwClientSerial*/)
{
	int					size;
	int					iAbilityNum;
	SkillDetailInfo *	pSkillInfo;
	Character *			pTarget;
	
	pSkillInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillCode);

	ASSERT(pSkillInfo && "SkillDetailInfo信息不可为空");


	//////////////////////////////////////////////////////
	size								= pResult->GetSize();
	pInfo->dwTargetID				= pResult->m_dwTargetKey;
	pInfo->dwAdditionalEffect	= pResult->m_EffectFlag;	
	pInfo->bNoDamage				= TRUE; //  Ability  FALSE

	iAbilityNum						= pResult->m_byAbilityNum;

	//////////////////////////////////////////////////////
	pTarget = (Character *)theObjectManager.GetObject(pInfo->dwTargetID);

	if (pTarget)
	{
		pInfo->vCurPosition.wvPos			= pTarget->GetPosition();
		pInfo->vCurPosition.m_TileIndex	= -1;
		pInfo->vDestPosition.wvPos			= pTarget->GetPosition();
		pInfo->vDestPosition.m_TileIndex = -1;

		switch(pSkillInfo->m_SkillClassCode)
		{
		case SKILLCODE_SONY_VINE:
			{
				//srand(dwClientSerial);
				sABILITYINFO_BASE *pAbility=pSkillInfo->GetAbilityInfo(101);
				__UNUSED(pAbility);
				ASSERT(pAbility);
				//Project
			}
			break;

		case SKILLCODE_BEAST_MASTER_DEADLY_STRIKE:
			{
				//Project
			}
			break;
		}//switch(pSkillInfo->m_SkillClassCode)


		//////////////////////////////////////////////////
		if (iAbilityNum > 0)
		{
			char *pPacket	= (char *)pResult;
			pPacket			+= sizeof(SKILL_RESULT_BASE);

			for (int nIndex = 0; nIndex < iAbilityNum; nIndex++)
			{
				int abilitysize = AddAbilityResult	(pPacket
																,pInfo
																,pTarget
																,dwSkillCode);
				pPacket	+= abilitysize;
				size		+= abilitysize;

				/// Hero被攻击
				if( pTarget->GetObjectKey() == theHero.GetObjectKey() )
				{
					if( theHeroActionInput.GetCurrentTarget() == 0 )
					{
						theHeroActionInput.SetCurrentTarget( pChr->GetObjectKey() );
					}
				}
			}
		}//if (iAbilityNum > 0)

	}//if (pTarget)

	return size;
}


//------------------------------------------------------------------------------
int   BattleScene::AddAbilityResult	(char*                      pPacket
                                    ,ATTACK_RESULT_TARGET_INFO* pInfo
												,Character*                 pTarget
												,DWORD                      dwSkillCode)
{

	sABILITYINFO_BASE *	pAbilityInfo;
	SkillDetailInfo *		pSkillInfo;
	SKILL_RESULT_CODE *	pResultCode = (SKILL_RESULT_CODE *)pPacket;


	// ABILITY
	pSkillInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillCode);
	ASSERT(pSkillInfo);

	if (!pSkillInfo)
	{
		FILE *fp = fopen("CrashLog.txt","wt");
		if (fp)
		{
			fprintf(fp,"AddAbilityResult :  SkillCode  SkillCode : %d",dwSkillCode);
			fclose(fp);
		}
	}

	pAbilityInfo = pSkillInfo->GetAbilityInfoAt((BYTE)pResultCode->m_wAbilityIndex);
	ASSERT(pAbilityInfo);

	if (!pAbilityInfo)
	{
		FILE *fp = fopen("CrashLog.txt","wt");
		if (fp)
		{
			fprintf	(fp
						,"AddAbilityResult : AbilityIndex SkillCode : %d AbilityIndex : %d"
						,dwSkillCode
						,pResultCode->m_wAbilityIndex);
			fclose(fp);
		}
	}

	sABILITYRESULT_OWNER  ownerInfo;
	ownerInfo.pTarget			= pTarget;
	ownerInfo.pPacket			= pPacket;
	ownerInfo.pInfo			= pInfo;
	ownerInfo.dwSkillCode	= dwSkillCode;
	ownerInfo.dwRetSize		= sizeof(SKILL_RESULT_CODE);

	DWORD&		dwSize			= ownerInfo.dwRetSize;
	WORD			wAbilityCode	= pAbilityInfo->m_wAbilityID;


	if(!theAbilityResultManager.ProcessAbilityResult	(wAbilityCode
																		,ownerInfo)		)
	{
	}




	///////////////////////////////////////////////////////////////
	if (pTarget)
	{	
		if (pAbilityInfo&&pAbilityInfo->m_wStateID != 0)
		{
			SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	

			pEffect->dwSkillID		= dwSkillCode;
			pEffect->dwStatusID		= pAbilityInfo->m_wStateID;					

			pEffect->AbilityInfo		= *pAbilityInfo;

			pEffect->dwDuration		= pAbilityInfo->m_iParam[2];
			pEffect->iRemainTime		= pEffect->dwDuration;
			pEffect->dwAbilityID		= wAbilityCode;
			pEffect->fEffectHeight	= 1.0f;
			pEffect->bAutoDestroy	= FALSE; 


			pEffect->hEffect			= INVALID_HANDLE_VALUE;
			pEffect->byEffectPos		= 0;

			if (pEffect->dwStatusID)
			{
				sSTATEINFO_BASE *pStateInfo = theStateInfoParser.GetStateInfo(pEffect->dwStatusID);

				if (pStateInfo)
				{
					Vector3D vEffectPos;
					pEffect->byEffectPos = pStateInfo->m_byEffectPos;
					vEffectPos = pTarget->GetSkillEffectPos(0,pEffect->byEffectPos);

				}
			}

			pTarget->AddSkillEffect(&pEffect);

			if (pEffect->dwStatusID == CHARSTATE_DOWN)
			{
				pInfo->dwAdditionalEffect |= ATTACK_ADDITIONAL_EFFECT_DOWN;
				pInfo->bForceSetLatestAttack = TRUE;

			}
		}
	}


	return dwSize;
}


//------------------------------------------------------------------------------
void   BattleScene::AddAbilityResult(char *pPacket,Character *pChr)
{
	MSG_CG_SKILL_REFLECT_DAMAGE_BRD  *pResultCode = (MSG_CG_SKILL_REFLECT_DAMAGE_BRD *)pPacket;



	if(pResultCode->m_SkillCode==0)
		return;

	SkillDetailInfo *pSkillInfo = theSkillInfoParser.GetSkillInfo(pResultCode->m_SkillCode);
	ASSERT(pSkillInfo);

	if(!pSkillInfo)
		return;


	DWORD dwState = 0;
	DWORD dwAbility = 0;


	sABILITYINFO_BASE *pAbilityInfo;
	
	pAbilityInfo = pSkillInfo->GetAbilityInfo(pResultCode->m_wAbilityCode);

	switch(pResultCode->m_wAbilityCode)
	{
	case ABILITY_REFLECT_DAMAGE:
		break;

	case ABILITY_REFLECT_FEAR:
		{
			dwState		= CHARSTATE_FEAR;
			dwAbility	= ABILITY_FEAR;
		}
		break;

	case ABILITY_REFLECT_FROZEN:
		{
			dwState		= CHARSTATE_FROZEN;
			dwAbility	= ABILITY_ABNORMAL_STATUS;
		}
		break;

	case ABILITY_REFLECT_SLOW:
		{
			dwState		= CHARSTATE_FETTER;
			dwAbility	= ABILITY_MOVE_SPEED_INCREASE;
		}
		break;

	case ABILITY_REFLECT_SLOWDOWN:
		{
			dwState		= CHARSTATE_SLOWDOWN;
			dwAbility	= ABILITY_PHYSICAL_SPEED_INCREASE;
		}
		break;

	case ABILITY_REFLECT_STUN:
		{
			dwState		= CHARSTATE_STUN;
			dwAbility	= ABILITY_ABNORMAL_STATUS;
		}
		break;
	}


	////////////////////////////////////////////
	if (	pChr 
		&& pAbilityInfo
		&& pAbilityInfo->m_wStateID != 0
		&& dwState)
	{	
		SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	

		pEffect->dwSkillID	= pResultCode->m_SkillCode;
		pEffect->dwStatusID	= dwState;	
		pEffect->dwAbilityID = dwAbility;

		for (int nIndex = 0; nIndex < 4; nIndex++)
		{
			pEffect->AbilityInfo.m_iParam[0] = pAbilityInfo->m_iOption2;	
			pEffect->AbilityInfo.m_iParam[1] = pAbilityInfo->m_iParam[0];	
			pEffect->AbilityInfo.m_iParam[2] = pAbilityInfo->m_iParam[1];	
			pEffect->AbilityInfo.m_iParam[3] = 0;	

		}

		pEffect->iRemainTime = pAbilityInfo->m_iParam[1];
		pEffect->dwAbilityID = pResultCode->m_wAbilityCode;
		pChr->AddSkillEffect(&pEffect);
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void   BattleScene::SetMonsterHPMultiplier(DWORD partymemberNum)
{
	float fMultiplier = theFormularManager.CalcHPIncreaseRatioAsParty(partymemberNum);

	ObjectMapIt it = theObjectManager.GetBegin();
	for (; it != theObjectManager.GetEnd(); it++)
	{
		Object * pObject = static_cast<Object *>(it->second);
		if (0 == pObject)
		{
			ASSERT(!"Object Is NULL!!");
			continue;
		}

		if (pObject->IsKindOfObject(MONSTER_OBJECT))
		{
			Monster *pMon = (Monster *)pObject;
			pMon->ChangeHPRatio(fMultiplier);
		}
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void BattleScene::ProcessMoveInfo(DWORD dwTick)
{
	MoveInfoList::iterator iter,tempiter;

	iter = m_MoveInfoList.begin();

	while(iter != m_MoveInfoList.end())
	{
		(*iter).m_dwDelay += dwTick;

		if ((*iter).m_dwDelay >= 20000)
		{
			tempiter = iter;
			iter++;
			m_MoveInfoList.erase(tempiter);
		}
		else
		{
			iter++;
		}
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void BattleScene::RenderMoveInfo()
{

}


void BattleScene::RegisterMoveInfo	(sTILE_POS /*Cur*/
                                    ,sTILE_POS /*Dest*/
												,DWORD     /*dwKey*/
												,BOOL      /*bThrust*/)
{


}


char	*BattleScene::AddSkillFieldEffect	(Character* /*pChr*/
                                          ,BYTE       byNumFieldEffect
														,DWORD      /*dwClientSerial*/
														,DWORD      dwSkillCode
														,char*      pPacket)
{
	if (byNumFieldEffect == 0)
	{
		return pPacket;
	}

	for (int nIndex = 0; nIndex < byNumFieldEffect; nIndex++)
	{
		SKILL_RESULT_EFFECT *pResult = (SKILL_RESULT_EFFECT *)pPacket;

		//g_pEffectManager->CreateEffect(STR2TAG("FN53"),pResult->m_vCurPos);

		DWORD lifeTime=0;
		SkillDetailInfo *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)dwSkillCode);
		ASSERT(pInfo);
		sABILITYINFO_BASE *pAInfo=pInfo->GetAbilityInfoAt((BYTE)pResult->m_wAbilityIndex);
		ASSERT(pAInfo);

		if(pAInfo)
		{
			lifeTime=pAInfo->m_iParam[2];
		}
		else
		{
			lifeTime=1000;
		}

		if(pInfo)
		{
			switch(pInfo->m_SkillClassCode)  
			{
			case SKILLCODE_STAR_FALL:
			case SKILLCODE_FIRE_FILA:
				{
				//Project
				}
				break;

			case SKILLCODE_POISON_RAIN:
				{
				//Project
				}
				break;
			}
		}


		pPacket += pResult->GetSize();
	}

	return pPacket;
}


void BattleScene::RenderDebugText(DWORD /*dwTick*/)
{
	if (m_iShowTextInfo)
	{
		switch(m_iShowTextInfo) 
		{
		//case TEXTINFO_RENDERSTATE:
		//	RenderRenderStates();
		//	break;
		case TEXTINFO_RESOURCE:
			RenderResourceStatus();
			break;	

		case TEXTINFO_PACKET_BUFF_SIZE:
			RenderPacketBufferSize();
			break;

		case TEXTINFO_KEY_QUEUE:
			RenderKeyQueueNum();
			break;

		case TEXTINFO_SKILL_QUEUE:
			RenderSkillQueueNum();
			break;
		}
	}
}


BOOL  BattleScene::SceneWndProc(HWND hWnd, UINT iMessage, WPARAM wParam, LPARAM lParam ,SI_MESSAGE *stMessage)
{
	if (theHeroEventManager.GetEventCurScript())
		return TRUE;

	return SceneBase::SceneWndProc(hWnd,iMessage,wParam,lParam,stMessage);
}



void BattleScene::OnEnterFrame	(DWORD /*dwTick*/)
{
}
void BattleScene::OnLeaveFrame	(DWORD dwTick)
{
	theObjectManager.PurgeDeadList(dwTick);
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：tooltipmanager.cpp
创建日期：2007年8月9日
最后更新：2007年8月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ToolTipManagerInstance.h"
#include "ItemSlot.h"
#include "CursorHandler.h"
#include "ItemInfoParser.h"
#include "QuickPaneSlot.h"
#include "ItemManager.h"
#include "SkillInfoParser.h"
#include "BaseContainer.h"
#include "StyleSlot.h"
#include "Hero.h"
#include "MouseHandler.h"
#include "HeroTipLayer.h"
#include "StateInfoParser.h"
#include "ObjectManager.h"
#include "SkillEffect.h"
#include "TextResManager.h"
#include "ColorManager.h"
#include "ItemSlot.h"
#include "FormularManager.h"
#include "PlayerAttributes.h"
#include "SkillSlot.h"
#include "MacroTextParser.h"
#include "DataChunker.h"
#include "TableTextFile.h"
#include "ToolTipToken.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ToolTipManagerInstance, ()  , gamemain::eInstPrioGameFunc);




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ToolTipManagerInstance::ToolTipManagerInstance()
{
	//m_pInfoSlot			= NULL;
	m_iToolTipAmount	= 0;
	//memset(m_szSlotInfoMessage,0,sizeof(m_szSlotInfoMessage));
	memset(m_arToolTips,0,sizeof(m_arToolTips));

	memset(m_arMacroParsers,0,sizeof(m_arMacroParsers));
	m_pTextBuffer		= NULL;
	m_pDefaultToken	= NULL;

}

ToolTipManagerInstance::~ToolTipManagerInstance()
{
	assert(m_pTextBuffer == NULL);
}
//System函数
BOOL ToolTipManagerInstance::Init()							
{
	m_pTextBuffer	=	new DataChunker(NULL);
	for(INT n=0; n<SI_MAXNUM; n++)
	{
		m_arMacroParsers[n] = new MacroTextParser();
		m_arMacroParsers[n]->Init(m_pTextBuffer, TRUE);
	}
	//__BOOL_SUPER(Init());
	return TRUE;
}

void ToolTipManagerInstance::Release()	
{
	for(INT n=0; n<SI_MAXNUM; n++)
	{
		SAFE_DELETE(m_arMacroParsers[n]);
	}
	SAFE_DELETE(m_pTextBuffer);
	//_SUPER::Release();
}


BOOL ToolTipManagerInstance::RequestWorkingData()	
{
	//__BOOL_SUPER(RequestWorkingData());
	return TRUE;
}

void ToolTipManagerInstance::ReleaseWorkingData()	
{
	//_SUPER::ReleaseWorkingData();
}

BOOL ToolTipManagerInstance::FrameMove(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(FrameMove(dwTick));
	return TRUE;
}



void  ToolTipManagerInstance::GetUIText(int iStringIndex,TCHAR *szText,int bufsize)
{
	__UNUSED(bufsize);
	ASSERT(bufsize <= MAX_UITEXT_LENGTH && bufsize > 0);
	szText[0] = 0;

	LPCSTR szContent = theTextResManager.GetString(iStringIndex);
	if(szContent && szContent[0])
	{
		strcpy( szText, (LPCSTR)szContent );
	}
}




void ToolTipManagerInstance::InitToolTip()
{
    m_iToolTipAmount = 0;

    ZeroMemory(m_arToolTips, (sizeof(ToolTipInfo) * MAX_SLOTINFO_LINE));
}






void ToolTipManagerInstance::_AddToolTipText(TCHAR *szText,int /*iStrLength*/)
{
	AddToolTip(szText
                  ,-1
						,0
						,0
						,0xFFFFFFFF
						,TOOLTIP_BG_COLOR
						,0);


}





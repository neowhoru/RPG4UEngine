/*////////////////////////////////////////////////////////////////////////
文 件 名：HeroQuestManagerInstance.cpp
创建日期：2007年5月1日
最后更新：2007年5月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "HeroQuestManagerInstance.h"
#include "Hero.h"
#include "MapNPC.h"
#include "QuestManager.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "TipLogManagerInstance.h"
#include "ConstTextRes.h"

using namespace object;
using namespace vobject;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(HeroQuestManagerInstance, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
HeroQuestManagerInstance::HeroQuestManagerInstance()
{
}
HeroQuestManagerInstance::~HeroQuestManagerInstance()
{
}
//System函数
BOOL HeroQuestManagerInstance::Init()							
{
	//__BOOL_SUPER(Init());
	return TRUE;
}

void HeroQuestManagerInstance::Release()	
{
	//_SUPER::Release();
}

BOOL HeroQuestManagerInstance::RequestWorkingData()	
{
	//__BOOL_SUPER(RequestWorkingData());
	return TRUE;
}

void HeroQuestManagerInstance::ReleaseWorkingData()	
{
	//_SUPER::ReleaseWorkingData();
}

BOOL HeroQuestManagerInstance::FrameMove(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(FrameMove(dwTick));
	return TRUE;
}

BOOL HeroQuestManagerInstance::Render(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(Render(dwTick));
	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class CheckQuestOnInvisit : public QuestOnInvisit
{
public:
	BOOL				bHaveQuest		;
	BOOL				bHaveQuestNotNow;
	BOOL				bQuestDone		;
	BOOL				bQuestNotDone	;
	sQUEST_INFO*	m_pQuest;

public:
	CheckQuestOnInvisit()
	{
		bHaveQuest			= FALSE;
		bHaveQuestNotNow	= FALSE;
		bQuestDone			= FALSE;
		bQuestNotDone		= FALSE;
		m_pQuest				= NULL;
	}

	BOOL OnQuestCast	(sQUEST_INFO* pQuest,BOOL bLevelFit)
	{
		m_pQuest = pQuest;
		if ( bLevelFit )
			bHaveQuest = true;
		else
			bHaveQuestNotNow = true;
		return bHaveQuest == false;
	}
	///////////////////////////////////////////////////////
	BOOL OnQuestEnd	(sQUEST_INFO* pQuest,BOOL bFinished)
	{
		m_pQuest = pQuest;
		if ( bFinished )
			bQuestDone = true;
		else
			bQuestNotDone = true;
		return bQuestDone == false;
	}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int	HeroQuestManagerInstance::UpdateNpcTipsType( VOBJID npcID )
{
	MapNPC*		pNpc;

	pNpc	= (MapNPC*)theObjectManager.GetObject(npcID);
	__CHECK(pNpc && pNpc->IsMapNpc());

	if(!pNpc->GetRelateQuestInfoCount())
		return eNpcTips_None;

	CheckQuestOnInvisit	questCheck;
	ForEachQuest(npcID,questCheck);

	if ( questCheck.bQuestDone )					return eNpcTips_QuestDone;
	else if ( questCheck.bHaveQuest )			return eNpcTips_HaveQuest;
	else if ( questCheck.bQuestNotDone )		return eNpcTips_QuestNotDone;
	else if ( questCheck.bHaveQuestNotNow )	return eNpcTips_HaveQuestNotNow;
	
	return eNpcTips_None;
}


struct sNPCTIPS_INFO
{
	sQUEST_INFO*	m_pQuest;
	MapNPC*			m_pNPC;
	BOOL				m_bHaveQuest;
	BOOL				m_bQuestDone;
};

class HeroQuestManagerInstanceUpdateNpcQuestTipsOpr
{
public:
	HeroQuestManagerInstance*	m_pMan;
	vector<sNPCTIPS_INFO>		m_arQuests;


	BOOL operator()(Object* pObject)
	{
		MapNPC*	pNpc = (MapNPC*)pObject;

		if(pNpc->GetRelateQuestInfoCount())
		{
			CheckQuestOnInvisit	questCheck;
			m_pMan->ForEachQuest(pNpc->GetObjectKey(),questCheck);

			if(	questCheck.bHaveQuest 
				|| questCheck.bQuestDone 
				|| questCheck.bQuestNotDone
				|| questCheck.bHaveQuestNotNow)
			{
				sNPCTIPS_INFO	info=	{questCheck.m_pQuest
											,pNpc
											,questCheck.bHaveQuest
											,questCheck.bQuestDone
											};
				m_arQuests	.push_back(info);
				//m_nCounter++;
			}
			//if ( questCheck.bQuestDone )					return eNpcTips_QuestDone;
			//else if ( questCheck.bHaveQuest )			return eNpcTips_HaveQuest;
			//else if ( questCheck.bQuestNotDone )		return eNpcTips_QuestNotDone;
			//else
		}
		return TRUE;
	}
};


void	HeroQuestManagerInstance::UpdateNpcQuestTips( )
{
	StringHandle	sText;

	HeroQuestManagerInstanceUpdateNpcQuestTipsOpr opr;
	opr.m_pMan		= this;
	//opr.m_szFormat	= sText;
	theObjectManager.ForEach(opr, MAPNPC_OBJECT);

	///////////////////////////////////////////
	for(UINT n=0; n< opr.m_arQuests.size(); n++)
	{
		sNPCTIPS_INFO&	info	= opr.m_arQuests[n];

		sText.Format	(_STRING(info.m_bHaveQuest?TEXTRES_QUEST_NPC_DIRECT : TEXTRES_QUEST_NPC_DIRECT2)
							,info.m_pNPC->GetName()
							,info.m_pQuest->m_Name);
		TIPGROUP	(	sText
					,eTIP_HELP
					,TipLogHelper::OnHelp
					,n == opr.m_arQuests.size()-1
					,FALSE
					,INVALID_SOUNDINDEX
					,info.m_pNPC->GetName());
	}

	if(opr.m_arQuests.size() == 0)
	{
		OUTPUTTIP(TEXTRES_QUEST_NPC_NO_QUESTS);
	}

}

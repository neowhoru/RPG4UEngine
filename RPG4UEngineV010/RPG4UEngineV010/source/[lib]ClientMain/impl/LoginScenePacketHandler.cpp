/*////////////////////////////////////////////////////////////////////////
文 件 名：LoginScenePacketHandler.cpp
创建日期：2007年9月22日
最后更新：2007年9月22日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "LoginScenePacketHandler.h"
#include "Protocol_ClientGameS.h"
#include "PacketStruct_ClientGameS.h"
#include "Protocol_ClientLogin.h"
#include "PacketStruct_ClientLogin.h"
#include "Version.h"
#include "SceneLogicFlow.h"
#include "LoginScene.h"
#include "GameClientApplication.h"
#include "GameParameter.h"
#include "NetworkLayer.h"
#include "PacketHandlerManager.h"
#include "ConstTextRes.h"
#include "PacketCryptManager.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(LoginScenePacketHandler, ()  , gamemain::eInstPrioGameFunc);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
LoginScenePacketHandler::LoginScenePacketHandler()
{
	m_bAuthFinished	= FALSE;
	m_bTryReconnect	= FALSE;
	m_bNetworkPending	= FALSE;
	m_dwEncKey			= 0;
	//m_dwUserKey			= 0;
	m_dwServerCount	= 0;		
	//memset(m_arServerStatusList,	0,sizeof(m_arServerStatusList));
	memset(m_arSerialKey,			0,sizeof(m_arSerialKey));

	//m_Account[0]				= 0;
	//m_Password[0]			= 0;

	m_szAgentServerIP[0]		= 0;
	m_AgentServerPort			= 0;

	m_uServerAmount			= 0;
	m_uGroupAmount				= 0;
	m_iSelectedServerIndex	= -1;
	m_dwStatus					= 0;
	m_pAutoSelectChannel		= NULL;
}

LoginScenePacketHandler::~LoginScenePacketHandler()
{
}

void LoginScenePacketHandler::Init()
{
}

void LoginScenePacketHandler::Release()
{
}


BOOL LoginScenePacketHandler::FrameMove(DWORD /*dwTick*/)
{
	if(theClientSetting.IsLoginAuto())
		return TRUE;

	if(m_tmUpdateSvrStatus.IsExpired())
	{
		SendMsg_GATESTATUS_LIST_SYN();
	}
	if(m_pAutoSelectChannel && theClientSetting.IsLoginAuto())
	{
		Sleep(100);
		ExecSelectChannel(*m_pAutoSelectChannel);
		m_pAutoSelectChannel = NULL;
	}

	return TRUE;
}

void LoginScenePacketHandler::OnDisconnect	()
{
	m_tmUpdateSvrStatus.DisableCheckTime();
	m_bAuthFinished	= FALSE;
	m_bTryReconnect	= TRUE;
	m_dwEncKey			= 0;
	m_dwStatus			= 0;
	m_bNetworkPending	= 0;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_AUTH2(USER_READY_CMD)
{
	theLoginScenePacketHandler.RecvMsg_USER_READY_CMD(pMsg);
	theLoginScenePacketHandler.SendMsg_VERSION_CHECK_SYN();
}
PACKETHANDLER_CLIENT_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_AUTH2(VERSION_CHECK_ACK)
{
	theLoginScenePacketHandler.RecvMsg_VERSION_CHECK_ACK(pMsg);
}
PACKETHANDLER_CLIENT_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_AUTH2(ACCOUNT_LOGIN_ACK)
{
	theLoginScenePacketHandler.RecvMsg_ACCOUNT_LOGIN_ACK(pMsg);
}
PACKETHANDLER_CLIENT_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_AUTH2(GATESTATUS_LIST_ACK)
{
	theLoginScenePacketHandler.RecvMsg_GATESTATUS_LIST_ACK(pMsg);
}
PACKETHANDLER_CLIENT_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_AUTH2(GATENAME_LIST_ACK)
{
	theLoginScenePacketHandler.RecvMsg_GATENAME_LIST_ACK(pMsg);
}
PACKETHANDLER_CLIENT_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_AUTH2(GATE_SELECT_ACK)
{
	theLoginScenePacketHandler.RecvMsg_GATE_SELECT_ACK(pMsg);
}
PACKETHANDLER_CLIENT_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::NetworkProc( MSG_BASE * /*pMsg*/ )
{

}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::RecvMsg_USER_READY_CMD( MSG_BASE * pMsg )
{
    MSG_CL_LOGIN_USER_READY_CMD *pRecvPacket = (MSG_CL_LOGIN_USER_READY_CMD *)pMsg;

    m_dwEncKey = pRecvPacket->dwEncKey;
    
#ifdef _DEBUG
    LOGINFO(_T("服务器信息: %s\n"), pRecvPacket->szServerInfo);
#endif

    //m_dwUserKey = 0;
    theGeneralGameParam.SetUserID(0);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::SendMsg_VERSION_CHECK_SYN()
{
    if (IsNetworkPending())
    {
        LOGINFO("NetworkPending...\n");
        return;
    }

    MSG_CL_LOGIN_VERSION_CHECK_SYN  sync;

    sync.byHighVersion		= VERSION_C2S_HIGH_NO;
    sync.byMiddleVersion	= VERSION_C2S_MIDDLE_NO;
    sync.byLowVersion		= VERSION_C2S_LOW_NO;

    theNetwork.GetLocalAddress(CK_LOGINSERVER, sync.szLocalIP);

	 //leo
	 sync.dwEncKey				= m_dwEncKey;

    theNetworkLayer.SendPacket(CK_LOGINSERVER, &sync, sizeof (sync));

    LOGINFO(_T("已向服务端发送验证版本验证信息.\n"));
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::RecvMsg_VERSION_CHECK_ACK( MSG_BASE * pMsg )
{
    SetNetworkPending(FALSE);

    MSG_CL_LOGIN_VERSION_CHECK_ACK *pRecvPacket = (MSG_CL_LOGIN_VERSION_CHECK_ACK *)pMsg;

    BYTE  byResult = pRecvPacket->byResult;

	 //出错处理
	 if(byResult != MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_OK)
	 {
		 DWORD dwTextID = TEXTRES_LOGIN_UnKnown;
		 switch(byResult)
		 {
		 case MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_VERSION_NOTMATCH:
		 {
			 dwTextID = TEXTRES_LOGIN_VersionError;
			 break;
		 }
		 case MSG_CL_LOGIN_VERSION_CHECK_ACK::ERR_UNKNOWN:
		 default:
			 break;
		 }
		 theGameUIManager.UIMessageBox(dwTextID);
		 GetGameClientApplication().Exit();
		 return;
	 }


   LOGINFO(_T("版本验证通过\n!!"));
	//自动登录处理
	if(theClientSetting.IsLoginAuto())
	{
		ExecLoginQuery(theClientSetting.GetLoginID()
                    ,theClientSetting.GetLoginPassword())			;
	}
	else
	{
		theLoginScene.ChangeStep(LSTEP_ACCOUNT_INPUT);

	}
}


BOOL ProcInvalidAccount(const  BOOL /*bPressYesButton*/,  void * /*pData*/ )
{
	return TRUE;
}


BOOL LoginScenePacketHandler::ExecLoginQuery(LPCSTR strID, LPCSTR strPW)
{
	if(IsAuthFinished())
	{
		theLoginScene.SetPendingConfrim(TRUE);
		theLoginScene.ChangeStep(LSTEP_SERVERLIST);
		return TRUE;
	}

	m_Password	= strPW;
	m_Account	= strID;



    if (m_Password.Length() >= MAX_ID_LENGTH)
    {
	   OUTPUTTIP(TEXTRES_LOGIN_AccountNameTooLong);
		theGameUIManager.UIShowMessageBox	(TEXTRES_LOGIN_AccountNameTooLong
                                          ,TEXTRES_Prompt
														,MB_YESNO
														,TRUE
														,ProcInvalidAccount
														,this);
        return FALSE;
    }

	 m_Account.ToUpper();
    // toupper account id


    if( theGeneralGameParam.IsEnableNetwork() )
    {
        return SendMsg_ACCOUNT_LOGIN_SYN();
    }
    else
    {
        // single mode
		 theLoginScene.DoSceneEnd();

    }
    return TRUE;
}

BOOL LoginScenePacketHandler::AutoExecLoginQuery()
{

    if( theGeneralGameParam.IsEnableNetwork() )
    {
        // net
        return SendMsg_ACCOUNT_LOGIN_SYN();
    }
    else
    {
        // single mode
		 theLoginScene.DoSceneEnd();
    }

    return TRUE;
}


BOOL LoginScenePacketHandler::SendMsg_ACCOUNT_LOGIN_SYN()
{
	if (IsNetworkPending())
		return FALSE;


	MSG_CL_LOGIN_ACCOUNT_LOGIN_SYN    sync;

	sync.dwAuthUserID = 0;

	lstrcpyn(sync.szID,		m_Account,	MAX_ID_LENGTH);
	lstrcpyn(sync.szPasswd,	m_Password,	MAX_PASSWORD_LENGTH);

	thePacketCryptManager.PacketEncode( (unsigned char *)sync.szPasswd,
							  MAX_PASSWORD_LENGTH,
							  (unsigned char *)&sync.szPasswd,
							  m_dwEncKey );

	BOOL bRet;
	bRet = theNetworkLayer.SendPacket(CK_LOGINSERVER, &sync, sizeof (sync));
	if(bRet)
		SetNetworkPending(TRUE);
   return bRet;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::RecvMsg_ACCOUNT_LOGIN_ACK( MSG_BASE * pMsg )
{
    SetNetworkPending(FALSE);

    MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK *pRecvPacket = (MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK *)pMsg;

    BYTE bOk = !(pRecvPacket->byResult);

    if (bOk == FALSE)
    {
		 DWORD dwText = TEXTRES_LOGIN_UnKnown;

        switch (pRecvPacket->byResult)
        {
		  case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_DUPLICATED_ID:
			  dwText = TEXTRES_LOGIN_Duplicated_Account;
            break;
        case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_ID:
			  dwText = TEXTRES_LOGIN_UserError;
            break;

        case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_PW:
			  dwText = TEXTRES_LOGIN_PasswordError;
            break;
        case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_PWDNOTMATCH:
			  dwText = TEXTRES_LOGIN_PasswordNotMatch;
            break;

        case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_LOGINING:
			  dwText = TEXTRES_LOGIN_UseOnline;
            break;

        case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_NOTEXIST:
			  dwText = TEXTRES_LOGIN_Account_NotFound;
            break;

        case MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_DB:
			  dwText = TEXTRES_DataBaseFailed;
            break;
        }

		  OUTPUTTIP(dwText);

		theLoginScene.ChangeStep(LSTEP_ACCOUNT_INPUT);

        return;
    }

    LOGINFO("INFO：%s\n",pRecvPacket->szInfo);

    m_bAuthFinished = TRUE;
    m_bTryReconnect = FALSE;

    //SendMsg_GATESTATUS_LIST_SYN();
    SendMsg_GATENAME_LIST_SYN();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::SendMsg_GATESTATUS_LIST_SYN()
{
    if (IsNetworkPending())
    {
        LOGINFO("SendMsg_GATESTATUS_LIST_SYN NetworkPending.... \n");
        return;
    }
	 //m_dwStatus	= 0;
    MSG_CL_LOGIN_GATESTATUS_LIST_SYN sync;
    theNetworkLayer.SendPacket(CK_LOGINSERVER, &sync, sizeof (sync));
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::RecvMsg_GATESTATUS_LIST_ACK( MSG_BASE * pMsg )
{
	if(theClientSetting.IsLoginAuto())
		return;

    MSG_CL_LOGIN_GATESTATUS_LIST_ACK* pRecvPacket = (MSG_CL_LOGIN_GATESTATUS_LIST_ACK *)pMsg;

    //m_uServerAmount = pRecvPacket->bySvrCnt;
    //memcpy(m_arServerStatusList,
				//  pRecvPacket->szServerStatusList,
				//  sizeof(pRecvPacket->szServerStatusList));

	 //m_dwStatus |=  STATUS_SVRLIST;
	 IntegrateChannelStatus(pRecvPacket->szServerStatusList, pRecvPacket->bySvrCnt);

	 //自动登录处理
    //if (theGeneralGameParam.GetLoginAuto())
    //{
    //    MSG_CL_LOGIN_GATE_SELECT_SYN           SendPacket;
    //    SendPacket.m_byCategory         = CL_LOGIN;
    //    SendPacket.m_byProtocol         = CL_LOGIN_GATE_SELECT_SYN;
    //    SendPacket.bySelectIndex        = theGeneralGameParam.GetSelectServer();		//急琶茄 辑滚岿靛狼 牢郸胶
    //    checkSendPacket(sizeof(MSG_CL_LOGIN_GATE_SELECT_SYN), (char *)&SendPacket);
    //}

    //GetUIMan()->GetUIText( TEXTRES_SELECT_SERVER_LIST, szMessage, MAX_MESSAGE_LENGTH );
    //SystemMessage(szMessage);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::SendMsg_GATENAME_LIST_SYN()
{
    if (IsNetworkPending())
    {
        LOGINFO("SendMsg_GATESTATUS_LIST_SYN NetworkPending.... \n");
        return;
    }
    
	 m_dwStatus	= 0;

   MSG_CL_LOGIN_GATENAME_LIST_SYN sync;
   theNetworkLayer.SendPacket(CK_LOGINSERVER, &sync, sizeof (sync));

	theLoginScene.ChangeStep(LSTEP_SERVERLIST);


    SetNetworkPending(TRUE);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::RecvMsg_GATENAME_LIST_ACK( MSG_BASE * pMsg )
{
    SetNetworkPending(FALSE);

	MSG_CL_LOGIN_GATENAME_LIST_ACK*	pMsgNameList = (MSG_CL_LOGIN_GATENAME_LIST_ACK*)pMsg;
	
    BYTE          byGroupCnt;
    BYTE				byServerCnt;
	 TCHAR*			pText(NULL);

	m_arGroupInfo.clear();

	byGroupCnt			= pMsgNameList->byGroupCnt;
	byServerCnt			= pMsgNameList->byServerCnt;

	m_uServerAmount	= byGroupCnt;
	m_uGroupAmount		= byServerCnt;

    if (!byGroupCnt)
        return;

	 //////////////////////////////////////
	 int i;
	 pText	= (TCHAR*)pMsgNameList->szBuffer;

	 for (i=0; i<byGroupCnt; ++i)
	 {
		 SServerGroupInfo	info;
		 info.m_sGroupName = pText;
		 m_arGroupInfo.push_back(info);
		 pText += info.m_sGroupName.Length()+1;
	 }


	 //////////////////////////////////////////////////////////
	 int						nIndex;
	 SServerChannelInfo	infoDat;
	 

    for (i=0; i<byServerCnt; ++i)
    {
		 nIndex = (int)*pText;
		 pText++;
		 infoDat.m_sChannelName	= pText;

		 infoDat.m_fProgress					= 0.f;
		 infoDat.m_nIndexAtAllChannels	= i;
		 m_arGroupInfo[nIndex].m_arChannelInfo.push_back(infoDat);
		 pText += infoDat.m_sChannelName.Length()+1;
    }


	//////////////////////////////////////////////
	if(theClientSetting.IsLoginAuto())
	{
		int nGroupAt = theClientSetting.GetLoginServerGroup();
		if(nGroupAt < 0)
			nGroupAt = 0;
		theLoginScene.SetPendingConfrim(TRUE);
		m_iSelectedServerIndex = nGroupAt;
		SendMsg_GATE_SELECT_SYN();
		return;
	}

	//////////////////////////////////////////////
	//把数据传送到UI中
	 theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN,
										gameui::eClearServerGroupInfo);
    for (i=0; i<byGroupCnt; ++i)
	 {
		 theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN,
											gameui::eAddServerGroupInfo,
											(LPARAM)&m_arGroupInfo[i]);
	 }


	theLoginScene.SetPendingConfrim(TRUE);
	m_dwStatus |=  STATUS_NAMELIST;
	m_tmUpdateSvrStatus.SetTimer(theGeneralGameParam.GetUpdateSvrStatusInterval());
	//m_tmUpdateSvrStatus.DisableCheckTime();


	if(theClientSetting.IsLoginAuto() && m_arGroupInfo.size())
	{
		int nGroupAt = theClientSetting.GetLoginServerGroup();
		//-1 表示不自动选择
		if(nGroupAt >= 0)
		{
			if(nGroupAt >= (INT)m_arGroupInfo.size())
				nGroupAt = 0;

			if(m_arGroupInfo[nGroupAt].m_arChannelInfo.size())
			{
				int nChannelAt = theClientSetting.GetLoginServerChannel();
				if(nChannelAt >= (INT)m_arGroupInfo[nGroupAt].m_arChannelInfo.size())
					nChannelAt = 0;

				//显示服务器列表、服务线列表
				theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN, 
											gameui::eSetServerGroupSelectAt,
											nGroupAt);
				m_pAutoSelectChannel = &m_arGroupInfo[nGroupAt].m_arChannelInfo[nChannelAt];
				//ExecSelectChannel(m_arGroupInfo[nGroupAt].m_arChannelInfo[nChannelAt]);
			}
		}
		
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::IntegrateChannelStatus(BYTE* szStatus,UINT uAmount)
{
	if((m_dwStatus& STATUS_SVRLIST_ALL) != STATUS_NAMELIST)
		return;
	SetNetworkPending(FALSE);

	UINT nGroupSize	= m_arGroupInfo.size();
	UINT nChannelSize;
	UINT nStatusIndex;
	UINT nGroupIndex;
	UINT nChannelIndex;

	nStatusIndex = 0;
	for(nGroupIndex=0; nGroupIndex<nGroupSize; nGroupIndex++)
	{
		std::vector<SServerChannelInfo>& channelInfo = m_arGroupInfo[nGroupIndex].m_arChannelInfo;
		nChannelSize	= channelInfo.size();
		for(nChannelIndex=0; nChannelIndex<nChannelSize; nChannelIndex++)
		{
			channelInfo[nChannelIndex].m_fProgress = (float)szStatus[nStatusIndex]/(float)MSG_CL_LOGIN_GATESTATUS_LIST_ACK::STATUS_VALUE_AMOUNT;
			nStatusIndex++;
			if(nStatusIndex >= uAmount)
				return;
		}
	}
}


BOOL  LoginScenePacketHandler::ExecSelectChannel(const SServerChannelInfo& channelInfo)
{
	/////////////////////////////////
	//a.第1次进入服务线
	if(m_iSelectedServerIndex == -1)
		goto laSend;

	/////////////////////////////////
	//b.重入同一服务线
	if(m_iSelectedServerIndex == channelInfo.m_nIndexAtAllChannels)
	{
		theLoginScene.DoSceneEnd();
		return TRUE;
	}

	/////////////////////////////////
	//b.重入另一服务线
	theLoginScene.SetPendingConfrim(TRUE);
	theNetworkLayer.SetChangeServer(TRUE);

laSend:
	m_iSelectedServerIndex = channelInfo.m_nIndexAtAllChannels;
	SendMsg_GATE_SELECT_SYN();
	return TRUE;
}

void LoginScenePacketHandler::SendMsg_GATE_SELECT_SYN()
{
    if (m_iSelectedServerIndex == -1)
        return;

    MSG_CL_LOGIN_GATE_SELECT_SYN           sendMsg;
    sendMsg.m_byCategory            = CL_LOGIN;
    sendMsg.m_byProtocol            = CL_LOGIN_GATE_SELECT_SYN;
    sendMsg.bySelectIndex           = (BYTE)m_iSelectedServerIndex;

    if (!theLoginScene.CheckSendPacket(sizeof(MSG_CL_LOGIN_GATE_SELECT_SYN), (char *)&sendMsg))
        return;
    
	 theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN, gameui::eLockUI,TRUE);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void LoginScenePacketHandler::RecvMsg_GATE_SELECT_ACK( MSG_BASE * pMsg )
{
	theGameUIManager.UITriggerFunc(gameui::eUISceneLOGIN, gameui::eLockUI,FALSE);

	MSG_CL_LOGIN_GATE_SELECT_ACK* pRecvPacket = (MSG_CL_LOGIN_GATE_SELECT_ACK *)pMsg;

	//获得AgentServer IP、Port能登录验证信息
	memcpy (m_arSerialKey,			pRecvPacket->szSerialKey, MAX_AUTH_SERIAL_LENGTH);
	memcpy (m_szAgentServerIP,	pRecvPacket->szSvrIP, MAX_IP_LENGTH);
	m_AgentServerPort	= (PORTTYPE)pRecvPacket->dwSvrPort;
	m_dwAuthUserID		= pRecvPacket->dwAuthUserID;


	theGeneralGameParam.SetGameServerIP	(pRecvPacket->szSvrIP);
	theGeneralGameParam.SetGameServerPort	((PORTTYPE)pRecvPacket->dwSvrPort);
	theGeneralGameParam.SetUserID			(pRecvPacket->dwAuthUserID);


	DWORD dwTextID = TEXTRES_LOGIN_UnKnown;
	if(pRecvPacket->byResult != MSG_CL_LOGIN_GATE_SELECT_ACK::ERR_OK)
	{
		switch(pRecvPacket->byResult)
		{
		case MSG_CL_LOGIN_GATE_SELECT_ACK::ERR_CANNT_CONNECT_SELECTED_SERVER:
			dwTextID = TEXTRES_SelectServerFailed;
			break;

		case MSG_CL_LOGIN_GATE_SELECT_ACK::ERR_EXIST_CONNECT_INFO_RETRY:
			dwTextID = TEXTRES_NowTryConnecting;
			break;
		}
		goto laFailed;
	}

	//.........................
	//选择服务器通过
	int iConnectRet = theNetwork.Connect	(CK_GAMESERVER
                                          ,m_szAgentServerIP
														,m_AgentServerPort);
	if( FALSE == iConnectRet )
	{
		dwTextID = TEXTRES_ConnectFail;
		//在人物列表获取成功后
		//theLoginScene.Disconnect();
		goto laFailed;
	}


	//在人物列表获取成功后
	//theLoginScene.Disconnect();
	theLoginScene.SetConnectedToAuthServer();

	theLoginScene.DoSceneEnd();
	return;


	/////////////////////////////////////////////////////////
	//失败处理
laFailed:
	OUTPUTTIP(dwTextID);
	theLoginScene.ChangeStep(LSTEP_ACCOUNT_INPUT);

}


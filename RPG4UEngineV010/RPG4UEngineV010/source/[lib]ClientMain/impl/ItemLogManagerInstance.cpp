/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemLogManagerInstance.cpp
创建日期：2006年8月23日
最后更新：2006年8月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemLogManagerInstance.h"
#include "ItemSlot.h"
#include "SlotUIListener.h"
#include "DummyItemSlot.h"
#include "ItemManager.h"
#include "SlotHandle.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ItemLogManagerInstance, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemLogManagerInstance::ItemLogManagerInstance()
{
	m_pLogContainer	= NULL;
	m_LogCursor			= 0;
	m_bEnableLog		= FALSE;
}
ItemLogManagerInstance::~ItemLogManagerInstance()
{
}

//System函数
BOOL ItemLogManagerInstance::Init()							
{
	//__BOOL_SUPER(Init());
	ContainerHandle	logContainer(SLOTT_ITEMHANDLE);
	__CHECK_PTR(&logContainer);

	m_pLogContainer	= /*(ItemHandleSlotContainer*)*/logContainer.Detach();

	m_pLogContainer		->Init(eITEMLOG_SOCKET_MAX_SLOT,	SI_ITEMLOG);
	return TRUE;
}

void ItemLogManagerInstance::Release()	
{

	if(m_pLogContainer)
	{
		m_pLogContainer->ClearAll();
		m_pLogContainer->Release();

		ContainerHandle	logContainer(m_pLogContainer);
		m_pLogContainer = NULL;
	}
	//_SUPER::Release();
}

BOOL ItemLogManagerInstance::RequestWorkingData()	
{
	//__BOOL_SUPER(RequestWorkingData());
	return TRUE;
}

void ItemLogManagerInstance::ReleaseWorkingData()	
{
	//_SUPER::ReleaseWorkingData();
}

BOOL ItemLogManagerInstance::FrameMove(DWORD /*dwTick*/)
{
	//__BOOL_SUPER(FrameMove(dwTick));
	return TRUE;
}

VOID ItemLogManagerInstance::PushItem(ItemSlot& item, eITEMLOG_OPR opr,INT64 amount)
{
	if(!m_bEnableLog)
		return;

	CheckCursor();

	m_pLogContainer->InsertSlot((SLOTPOS)m_LogCursor,item);

	//DWORD dwState(SLOT_UISTATE_NONE);
	
	//switch(opr)
	//{
	//case eITEMLOG_DELETE:	dwState = SLOT_UISTATE_DAMAGED;	break;
	//case eITEMLOG_UPDATE:	dwState = SLOT_UISTATE_DISABLE;	break;
	//}

	DummyItemSlot& itemDummy = (DummyItemSlot&)m_pLogContainer->GetSlot(m_LogCursor);
	if(amount == 0)
		amount = item.GetNum();
	itemDummy.SetExtraData(amount);

	m_pLogContainer->SetSlotState(m_LogCursor,opr);

	StepCursor();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemLogManagerInstance::PushItem(LPCSTR szItemName,INT64 amount, eITEMLOG_OPR opr)
{
	if(!m_bEnableLog)
		return;
	if(amount == 0)
		return;

	sITEMINFO_BASE* pItemInfo;
	pItemInfo = theItemInfoParser.GetItemInfo(szItemName);
	if(pItemInfo == 0)
		return;

	ItemSlot	item;
	item.SetCode(pItemInfo->m_Code);
	item.SetNum((DURATYPE)amount);

	if(opr == eITEMLOG_NULL)
	{
		if(amount <0)
		{
			opr		= eITEMLOG_DELETE;
			amount	= -amount;
		}
		else
			opr = (eITEMLOG_ADD);
	}

	PushItem(item, opr  ,amount);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemLogManagerInstance::PushMoney(INT64 amount, eITEMLOG_OPR opr)
{
	if(!m_bEnableLog)
		return;
	if(amount == 0)
		return;

	ItemSlot	item;
	item.SetCode(5000);
	item.SetNum(1);

	if(opr == eITEMLOG_NULL)
	{
		if(amount <0)
		{
			opr		= eITEMLOG_DELETE;
			amount	= -amount;
		}
		else
			opr = (eITEMLOG_ADD);
	}

	PushItem(item, opr  ,amount);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemLogManagerInstance::PushExp(INT64 amount, eITEMLOG_OPR opr)
{
	if(!m_bEnableLog)
		return;
	if(amount == 0)
		return;

	ItemSlot	item;
	item.SetCode(5001);
	item.SetNum(1);

	if(opr == eITEMLOG_NULL)
	{
		if(amount <0)
		{
			opr		= eITEMLOG_DELETE;
			amount	= -amount;
		}
		else
			opr = (eITEMLOG_ADD);
	}

	PushItem(item, opr  ,amount);

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID ItemLogManagerInstance::PushSkillExp	(INT64 amount, eITEMLOG_OPR opr)
{
	if(!m_bEnableLog)
		return;
	if(amount == 0)
		return;

	ItemSlot	item;
	item.SetCode(5002);
	item.SetNum(1);

	if(opr == eITEMLOG_NULL)
	{
		if(amount <0)
		{
			opr		= eITEMLOG_DELETE;
			amount	= -amount;
		}
		else
			opr = (eITEMLOG_ADD);
	}

	PushItem(item, opr  ,amount);
}


BOOL	ItemLogManagerInstance::CheckCursor()
{
	if(m_LogCursor >= eITEMLOG_SOCKET_MAX_SLOT)
	{
		ClearLog();
		m_LogCursor = 0;
	}
	return TRUE;
}

BOOL	ItemLogManagerInstance::StepCursor()
{
	m_LogCursor++;
	return TRUE;
}


void ItemLogManagerInstance::ClearLog()
{
	m_pLogContainer->ClearAll();
	m_LogCursor = 0;

	theGameUIManager.UISetData	(gameui::eUIItemLog
										,gameui::eSetVisible
										,FALSE);
}



/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInPlayingInstance.cpp
创建日期：2007年4月26日
最后更新：2007年4月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameInPlayingInstance.h"
#include "V3DGameWorld.h"
#include "MapViewManager.h"
#include "PathGameMap.h"
#include "V3DAshesFloatManager.h"
#include "ScreenTipManager.h"
#include "VHeroActionInput.h"
#include "ApplicationSetting.h"
#include "CharActionInfo.h"
#include "V3DHugeModelManager.h"
#include "SkillStorageManager.h"
#include "DemoSystemManager.h"
#include "ShopDialog.h"
#include "ItemCompoundDialog.h"
#include "GameInScriptManager.h"
#include "VHeroInputLayer.h"
#include "HeroEffectManager.h"
#include "HeroEventManager.h"
#include "HeroTriggerManager.h"
#include "NpcCoordinateManager.h"
#include "V3DTerrainEditor.h"
#include "HeroData.h"
#include "StudioPathManager.h"
#include "TradeDialog.h"
#include "VendorDialog.h"
#include "GameParameter.h"
#include "VUIFontDDraw.h"

using namespace gamemain;
using namespace vobject;
using namespace object;

const		DWORD	CHECK_TIMELENGTH	= 5*1000;
const		DWORD	CHECK_TIMELENGTH2	= 10*1000;
const		DWORD	CHECK_FPS_BASE		= 30;
const		DWORD	CHECK_FPS_GOOD		= 40;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameInPlayingInstance, ()  , gamemain::eInstPrioGameFunc);


#define INPLAYING_UILISTENER	gameui::eUIGameInPlaying



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameInPlayingInstance::GameInPlayingInstance()
:m_CheckStep(CHECK_READY)
//,m_dwFPSSum(0)
,m_dwCheckTick(0)
,m_dwReadyCheckTick(0)
,m_dwFpsAvert(100)
{
}

GameInPlayingInstance::~GameInPlayingInstance()
{
}

BOOL GameInPlayingInstance::Init()		
{
	SetInit();
	//theGameWorld.Init();

	// Map
	__CHECK(theGameWorld.RequestWorkingData());

	__INIT(theNpcCoordinateManager.LoadDatas());

	__INIT(theVHeroInputLayer.Init());
	//__INIT(theHeroEffectManager.Init());

	return TRUE;
}

void GameInPlayingInstance::Release()
{
	assert(m_nRefCounter == 0);
	SetEnd();
	
	//m_nRefCounter = 0;
	//ReleaseWorkingData();

	//__END(theHeroEffectManager.Release());

	__END(theVHeroInputLayer.Release());

	//__END(theNpcCoordinateManager.Release());

	theGameWorld.ReleaseWorkingData();
	//theGameWorld.Shutdown();
}

BOOL GameInPlayingInstance::RequestWorkingData()	
{
	SetWorking();
	SetPlaying();
	//限于第一次调用
	if(m_nRefCounter == 0)
	{
		__INIT(theMapViewManager.LoadSettings(_PATH_SETTING("MiniMap.ini")));
		//theApplication.AddListener(&theMapViewManager);

		__INIT(theItemSystem.Init());

		__INIT( theCloneManager.Init() );

		__INIT( theHeroEffectManager.Init() );
		__INIT( theHeroEventManager.Init() );
		__INIT( theHeroTriggerManager.Init() );

		//1.1技能Hero处理
		__INIT( theSkillStorageManager.Init((ePLAYER_TYPE)theHeroData.GetHeroInfo().m_CharInfo.m_byClassCode) );

		////////////////////////////////////////////////////////
		//1.创建Hero
		__INIT( CreateHero(!theGeneralGameParam.IsEnableNetwork()) );


		///辅助处理
		theCursorHandler.ForceChange(cursor::CURSORTYPE_DEFAULT);
		theApplication.AddListener(this);


		/// .创建Hero输入层
		__INIT(theHeroActionInput.Init());

		__INIT(theVHeroInputLayer.RequestWorkingData());


		/// 初始化ShopDialog
		__INIT(theShopDialog.Init());
		__INIT(theItemCompoundDialog.Init());
		__INIT(theTradeDialog.Init());
		__INIT(theVendorDialog.Init());

		/// .通知UI监听器
		theGameUIManager.UIOnStartUp(INPLAYING_UILISTENER);

		/// .创建全局辅助寻路句柄
		g_pPathExplorer = theTileWorld.CreatePathHandler();

		//theFreshmanTipManager.RequestWorkingData();

		if(theGeneralGameParam.IsLocalHeroData())
			theDemoSystemManager.RequestWorkingData();
	}
	m_nRefCounter++;


	InitLandData();

	///////////////////////////////////////////////////
	//每次申请数据时。。。 
	Vector3D vPos(	m_fJumpToX ,m_fJumpToY, m_fJumpToZ);

	theVHero.JumpToTilePos(vPos, &thePathGameMap );
	theVHero.ResetRenderAlpha();

	LOGINFO("\n******** GameInPlayingInstance starting.... ********\n");
  return TRUE;
}


void GameInPlayingInstance::ReleaseWorkingData()	
{
	LOGINFO("\n******** GameInPlayingInstance End now .... ********\n");
	SetInit();
	SetPause();


	m_nRefCounter--;
	assert(m_nRefCounter >= 0);
	if(m_nRefCounter == 0)
	{
		if(theGeneralGameParam.IsLocalHeroData())
		{
			theHeroData.SaveGameData( GAMEDATA_PATH );
			theHeroData.SaveQuestData	(GAMEQUEST_PATH );

			theDemoSystemManager.ReleaseWorkingData();
		//theFreshmanTipManager.ReleaseWorkingData();
		}


		SetEnd();
		SetStop();

		assert(g_pPathExplorer);
		theTileWorld.DestroyPathHandler(g_pPathExplorer);

		/// 初始化ShopDialog
		__END(theVendorDialog.Release());
		__END(theTradeDialog.Release());
		__END(theItemCompoundDialog.Release());
		__END(theShopDialog.Release());

		/////////////////////////////////////////
		//theApplication.RemoveListener(&theMapViewManager);
		theMapViewManager.Destroy();

		__END(theHeroTriggerManager.Release());
		__END(theHeroEventManager.Release());
		__END(theHeroEffectManager.Release());

		__END(theHeroActionInput.Release());


		theVHeroInputLayer.ReleaseWorkingData();
		theApplication.RemoveListener(this);

		__END( theSkillStorageManager.Release());

		DestroyHero(!theGeneralGameParam.IsEnableNetwork());

		__END( theCloneManager.Release() );

		__END(theItemSystem.Release());

		theGameUIManager.UIOnClose(INPLAYING_UILISTENER);
	}
}


BOOL GameInPlayingInstance::FrameMove(DWORD dwTick)	
{

	__RUN(theScriptManager.Run());

	if(theGeneralGameParam.IsLocalHeroData())
	{
		__RUN(theDemoSystemManager.FrameMove(dwTick));
	}

	//__RUN(theFreshmanTipManager.FrameMove(dwTick));

	__RUN(theItemSystem.FrameMove(dwTick));

	__RUN(theHeroActionInput.FrameMove(dwTick));

	__RUN(theVHeroInputLayer.FrameMove(dwTick));

	__RUN(theHeroEffectManager.FrameMove(dwTick));
	__RUN(theHeroTriggerManager.FrameMove(dwTick));
	__RUN(theHeroEventManager.FrameMove(dwTick));

	//////////////////////////
	//更新水波位置
	theV3DAshesFloatManager.SetEmitterCenter( theVHero.GetPosition() + Vector3D(0,0,200*SCALE_MESH) );

	theMapViewManager.FrameMove(dwTick);
	theScreenTipManager.FrameMove(dwTick);
	theGameUIManager.UIFrameMove(INPLAYING_UILISTENER,gameui::eUINull, dwTick);


  return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class RenderPathRoutesOpr
{
public:
	DWORD m_dwTick;
	BOOL operator()(Object* pObject)
	{
		if(	!pObject->GetPathExplorer() 
			|| !pObject->GetPathExplorer()->IsMoving())
			return TRUE;
		theTileMap.RenderPathRoute	(m_dwTick
											,pObject->GetPathExplorer()
											,pObject->IsHero()?0x5f0077ff : 0x5fff7700
											,pObject->IsHero());
		return TRUE;
	}
};

BOOL GameInPlayingInstance::Render(DWORD dwTick)	
{	
	TRUETIMEFUNC();

	__CHECK(theVHeroInputLayer.Render(dwTick));

	__CHECK(theItemSystem.Render(dwTick));

	__CHECK(theHeroEventManager.Render(dwTick));
	__CHECK(theHeroEffectManager.Render(dwTick));
	__CHECK(theHeroTriggerManager.Render(dwTick));

	if(g_BaseSetting.m_ShowDebugInfo )
	{
		RenderPathRoutesOpr opr={dwTick};
		theObjectManager.ForEach(opr);
	}


	__CHECK(_CheckBestFPS(dwTick));


	 return TRUE;
}

BOOL GameInPlayingInstance::RenderHUD(DWORD dwTick)	
{	
	TRUETIMEFUNC();

	IV3DRenderer* RI = the3DEngine.GetRenderer();
	RenderUtil::FlushBloodBarToScreen();

	//画鼠标上的道具
	//theVObjectManager.RenderMouseBag();

	if(theGeneralGameParam.IsLocalHeroData())
	{
		theDemoSystemManager.Render(dwTick);
	}



	{
		TRUETIMEBLOCK( "RenderUtil::FlushRenderPic" );
		RenderUtil::FlushRenderPic();
		the3DRenderer.Flush( RENDER_SORT_FAR_Z );
	}


	if(!theApplicationSetting.m_ShowAdvancedMiniMap )
	{
		if( g_pFlyTextEffect )
			g_pFlyTextEffect->Render();


		g_pLockPassivityTargetEffect->Render();
		g_pLockInitiativeTargetEffect->Render();
	}


	theStudioPathManager.Render(dwTick);

	// UI渲染
	theVObjectManager.RenderInfo( dwTick );		
	theGameUIManager.Render(dwTick);

//#ifndef NONE_TEMP
	//RenderUtil::TestingRender();
//#endif

	///ScreenText最终渲染
	theScreenTipManager.Render(dwTick);


	//输出物品提示
	//VObject* pSelObject = theVObjectManager.GetMouseTargetObject();
	//if(pSelObject && pSelObject->IsItem())
	//{
	//	VItem* pItem = (VItem*)pSelObject;
	//	if(pItem->IsItemDropDelayOver() )
	//	{
	//		int x, y;
	//		ObjectUtil::World3DToScreen2D( pSelObject->GetPosition(), x, y );
	//		DrawGameText( x, y+20, pSelObject->GetName() );
	//	}
	//}



#ifdef _DEBUG
#pragma message(__FILE__  "(836) 处理DrawNetStreamFlux 绘画网络性能线...  " )

	//theVObjectManager.DrawNetStreamFlux(pDevice, 470, 60);
#endif;


	///////////////////////////////////////////////////////
	//渲染 MiniMap
	//ON_FUNC_PROFILER(test2.7);

	V3DTerrain* pTerrain = NULL;
	if( theGameWorld.GetHeroPlotMap() )
	{
		pTerrain = theGameWorld.GetHeroPlotMap()->GetTerrain();
	}

	if( pTerrain )
	{
		//mInImap
		TRUETIMEBLOCK( "GameInPlayingInstance::RenderHUD:RenderPlayersInfo" );
		int nMapWidth = 0;
		int nMapHeight = 0;
		//ON_FUNC_PROFILER(pTerrain Error);
		nMapWidth = pTerrain->GetMapWidth();
		nMapHeight = pTerrain->GetMapWidth();
		;

		int nFullMapWindowWidth = nMapWidth*4;
		int nFullMapWindowHeight = nMapHeight*4;
		while( nFullMapWindowWidth > 512 || nFullMapWindowHeight > 512 )
		{
			nFullMapWindowWidth /= 2;
			nFullMapWindowHeight /= 2;
		}
		
		RI->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
		RI->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );


		//////////////////////////////////////////////////////////
		if( theApplicationSetting.m_ShowAdvancedMiniMap )
		{
			RECT rcWindow = 
			{ 
				0,
				0,
				SCREEN_WIDTH,
				SCREEN_HEIGHT 
			};

			int nSight = nMapWidth>nMapHeight?nMapWidth:nMapHeight;

			if(theMapViewManager.ReadyRender(mapinfo::MAPMODE_FULL
														,rcWindow
														,theVHero.GetTilePos()
														,nSight))
			{
				theMapViewManager.Render	(dwTick);
			}
		}

#ifdef _DEBUG
		if( g_V3DConfig.IsShowGrid() || g_V3DConfig.IsRenderChunkGrid() )
		{
			V3DTerrainEditor* pEditorTerr = dynamic_cast<V3DTerrainEditor*>( pTerrain );
			if(pEditorTerr)
				pEditorTerr->RenderGrid(dwTick,COLOR_RGBA(0xff,0xff,0,0x7f),COLOR_RGBA(0xff,0xff,0,0x7f));
		}
#endif



		RI->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
		RI->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
	}


	/////////////////////////////////////////////////////////
	//渲染坐标信息
	if(	g_BaseSetting.m_ShowDebugInfo 
		&& theGameWorld.GetHeroPlotMap()
		&& singleton::ExistHero() )
	{

		/////////////////////////////////////////////////////
		//绘鼠标信息
		if(theVHeroInputLayer.m_MouseRay.m_bResult)
		{			
			LPCSTR szText;
			Vector3C&	vCrossTile	= theVHeroInputLayer.m_MouseRay.m_vCross;
			V3DTerrain* pTerrain = NULL;

		//////////////////////////////////////////////////////
			int	xTile = 0,
					yTile = 0;
			int	xTileLocal = 0,
					yTileLocal = 0;
			INT	nWorldMapX = 0, 
					nWorldMapY = 0;
			INT	nMouseTile;


			pTerrain = theGameWorld.GetHeroPlotMap()->GetTerrain();

			//////////////////////////////////////////
			//光标射线处理
			{
				nMouseTile	= theTileMap.GetTileToStand(vCrossTile);
				xTile = (INT) (vCrossTile.x / MAPTILESIZE);
				yTile = (INT) (vCrossTile.y / MAPTILESIZE);
				xTileLocal = xTile;
				yTileLocal = yTile;

				pTerrain->TilePosToLocal( xTileLocal, yTileLocal );
				pTerrain->GetWorldMapXY( nWorldMapX, nWorldMapY );

				szText = FMSTR	("TileIndex(H:%d,G:%d,M:%d) WPos(%.2f,%.2f,%.2f) Tile(%d,%d L:%d,%d) Mask(%d,%d) Chunk(%d,%d L:%d,%d) Map(%d,%d)"
									,theHero.GetPathExplorer()->GetTile() 
									,g_pPathExplorer->GetTile()
									,nMouseTile
									,vCrossTile.x,vCrossTile.y,vCrossTile.z
									,xTile,yTile
									,xTileLocal, yTileLocal
									,(INT)(vCrossTile.x / WALK_MASK_SIZE)
									,(INT)(vCrossTile.y / WALK_MASK_SIZE)
									,xTile/TILE_RES ,yTile / TILE_RES,xTileLocal/TILE_RES, yTileLocal/TILE_RES
									,nWorldMapX, nWorldMapY
									);
				DrawGameText(0, SCREEN_HEIGHT - 100, szText,0xffff0000);
			}
		}


		/////////////////////////////////////////////////////////
		//if()
		{
			static Vector3D arVectors[4][4];

			Vector3D vHeroPos = theVHero.GetPosition();
			int	nSectorX		= (INT)(vHeroPos.x / VILLAGE_SECTOR_SIZE);
			int	nSectorY		= (INT)(vHeroPos.y / VILLAGE_SECTOR_SIZE);
			int	nSectorIndex = nSectorY * m_dwSectorXNum + nSectorX;



			///////////////////////////
			//HugeModelManager 信息
			TCHAR strFrameStats [90] = {0};
			INT nHugeModelCnt = theV3DHugeModelManager.GetModelCount();
			for( int i = 0; i< nHugeModelCnt; i++ )
			{
				V3DHugeModel* pModel = theV3DHugeModelManager.GetMeshModel( i );
				if( pModel )
				{
					sprintf( strFrameStats, "%d %s", pModel->GetRenderFaceCount(), pModel->m_szName.c_str() );
					INT nOffSet = 0;
					if( pModel->IsHeroInRoom() )
					{
						nOffSet = 50;
					}
					DrawGameText( nOffSet, 140+i*16, strFrameStats );		
					pModel->SetRenderFaceCount(0);
				}
			}


			/////////////////////////////////////
			//测试数据
			//DWORD MAX_MON = 3;
			//LPCSTR	arNames	[MAX_NAME];
			//INT		arNums	[MAX_NAME];
			//INT		nBaseID	= 50001;
			//std::string		sText = "ActionQueue";

			//for(INT mon =0; mon<MAX_MON; mon++)
			//{
			//	Object* pObject = theObjectManager.GetObject(nBaseID + mon);
			//	if(pObject && pObject->IsKindOfObject(MONSTER_OBJECT))
			//	{
			//		sText += "[";
			//		sText += pObject->GetName();
			//		sText += ",";
			//		sText += FMSTR("%d", ((Monster*)pObject)->GetActionQueueCount() );
			//		sText += "]";
			//		//arNames[0] = pObject->GetName();
			//		//arNames[0] = ((Monster*)pObject)->GetActionQueueCount();
			//	}
			//}
			
			{
			//	Vector3D vCurPos = pObject->GetPosition();
			//	szText = FMSTR(" %s Wander at %d,%d,%d   Area at %d,%d\n"
			//			, pObject->GetName()
			//			, (int)(vCurPos.x+0.5f)
			//			, (int)(vCurPos.y+0.5f)
			//			, (int)(vCurPos.z+0.5f)
			//			, (int)(vCurPos.x/VILLAGE_SECTOR_SIZE)
			//			, (int)(vCurPos.y/VILLAGE_SECTOR_SIZE)
			//	);
				//DrawGameText( 0, 160, sText.c_str() );
			}

			/////////////////////////////////////
			//sector信息
			LPCSTR szText = FMSTR(  "Sector:(Idx:%d ,%d,%d) 3DPos(%d,%d,%d) " 
											,nSectorIndex
											,nSectorX
											,nSectorY
											,(int)(vHeroPos.x + 0.5f)
											,(int)(vHeroPos.y + 0.5f)
											,(int)(vHeroPos.z + 0.5f));
			DrawGameText( 20, 310, szText );

			//////////////////////////////////////////////
			//绘画Scector辅助线
			nSectorX--;
			nSectorY--;

			for(int r=0;r<4;r++)
			{
				for(int c=0;c<4;c++)
				{
					arVectors[r][c].x = (float)(nSectorX+c)* VILLAGE_SECTOR_SIZE;
					arVectors[r][c].y = (float)(nSectorY+r)* VILLAGE_SECTOR_SIZE;
					arVectors[r][c].z = vHeroPos.z;
					arVectors[r][c].z = theGameWorld.GetHeightAt(vHeroPos.z
																	,100
																	,arVectors[r][c].x
																	,arVectors[r][c].y
																	,0) + 0.3f;
				}
			}
			static DWORD sColors[]=
			{
				 0xff0f00ff
				,0xffff0000
			};
			for(int r=0;r<3;r++)
			{
				for(int c= (r&1);c<3;c+=2)
				{
					theV3DGraphicDDraw.DrawBox3D	(arVectors[r][c]
																,arVectors[r+1][c+1]
																,sColors[r&1]);
					//theV3DGraphicDDraw.FillTriangle		(arVectors[r][c]
					//											,arVectors[r][c+1]
					//											,arVectors[r+1][c]
					//											,0x8000ff00);
				}
			}

			_RenderHeroStatus(dwTick);
			}
	}


	_RenderCheckFPS(dwTick);


	//V3DGraphicDDraw::DrawLine3D();

	 return TRUE;
}


BOOL	GameInPlayingInstance::_RenderCheckFPS	(DWORD /*dwTick*/)
{
	//if(!singleton::ExistHero())
	//	return TRUE;

	if(m_CheckStep != CHECK_RUN)
		return TRUE;

	DrawGameText(0
					,SCREEN_HEIGHT/2
					,FMSTR(_T("总时:%dms,用时:%dms,平均FPS:%d >=30:%d <30:%d")
							,CHECK_TIMELENGTH2
							,m_dwCheckTick
							,m_dwFpsAvert
							,theGameRunning.GetTargetBig() 
							,theGameRunning.GetTargetSmall())
					);
	 return TRUE;
}

BOOL	GameInPlayingInstance::_CheckBestFPS		(DWORD dwTick)
{
	if(!singleton::ExistHero())
		return TRUE;

	if(!theHero.IsEnterWorld())
		return TRUE;


	////////////////////////////////////
	if(SCREEN_WIDTH <= 800)
	{
		m_CheckStep		= CHECK_END;
		return TRUE;
	}

	////////////////////////////////////
	switch(m_CheckStep)
	{
	////////////////////////////////////////////
	case CHECK_NOTRY:
		return TRUE;

	////////////////////////////////////////////
	case CHECK_END:
		{
			if(theGameRunning.GetFPS() >= CHECK_FPS_BASE)
				return TRUE;

			m_CheckStep = CHECK_READY;
		}
		//break;

	////////////////////////////////////////////
	case CHECK_READY:
		{
			m_dwReadyCheckTick += dwTick;
			if(m_dwReadyCheckTick >= CHECK_TIMELENGTH)
			{
				m_CheckStep		= CHECK_RUN;
				m_dwCheckTick	= 0;
				m_dwFpsAvert	= 100;
				theGameRunning.SetTargetFPS(CHECK_FPS_BASE);
			}
			return TRUE;
		}
		break;
	}


	m_dwCheckTick	+= dwTick;
	m_dwFpsAvert	= math::Min(m_dwFpsAvert, theGameRunning.GetFPS());




	if(m_dwCheckTick >= CHECK_TIMELENGTH2)
	{
		theGameRunning.SetTargetFPS(0);

		INT				nWidth	= SCREEN_WIDTH;
		INT				nHeight	= SCREEN_HEIGHT;
		INT				nMode		= g_nDisplayMode;

		///////////////////////////////////
		m_CheckStep		= CHECK_END;

		//if(m_dwFpsAvert >= CHECK_FPS_GOOD)
		//	return TRUE;

		///////////////////////////////////
		//if(m_dwFpsAvert < CHECK_FPS_BASE)
		if(theGameRunning.GetTargetBig() <  theGameRunning.GetTargetSmall())
		{
			StringHandle	szTip	;

#ifdef _DEBUG
#define	FULLSCREEN_VALUE		0
#else
#define	FULLSCREEN_VALUE		1
#endif
			//////////////////////////////////
			//1. > 1024时, 则尝试跳到  1024 桌面模式
			if(SCREEN_WIDTH > 1024)
			{
				SCREEN_WIDTH								= 1024;
				SCREEN_HEIGHT								= 768;
				g_nDisplayMode								= FULLSCREEN_VALUE;
				theApplicationSetting.m_bFullScreen	= FULLSCREEN_VALUE;

				szTip.Format(_T("因分辨率过大，导致现有FPS(%d)低于推荐(%d)；将自动更换为1024x768模式；\n是否需要马上重启游戏？")
								,m_dwFpsAvert
								,CHECK_FPS_BASE);
				goto laRestart;
			}

			//////////////////////////////////
			//2. > 1024时, 则尝试跳到  1024 桌面模式
			else// if(SCREEN_WIDTH > 800)
			{
				SCREEN_WIDTH								= 800;
				SCREEN_HEIGHT								= 600;
				g_nDisplayMode								= FULLSCREEN_VALUE * 2;
				theApplicationSetting.m_bFullScreen	= FULLSCREEN_VALUE;

				szTip.Format(_T("因分辨率过大，导致现有FPS(%d)低于推荐(%d)；现自动更换为800x600模式；\n是否需要马上重启游戏？")
								,m_dwFpsAvert
								,CHECK_FPS_BASE);
				goto laRestart;
			}

	laRestart:
			{
				////////////////////////////////////
				theCursorManager.OnEndDragCamera();
				

				if(MessageBox(g_hWndMain,szTip,_T("RPG4U提示"),MB_YESNO) == IDYES)
				{
					theApplicationSetting.SaveGraphicSetting(_T("Dependence\\screen.ini"));
					theApplication.Exit();

					TCHAR	szPath[MAX_PATH];
					GetModuleFileName(NULL,szPath,MAX_PATH);
					ShellExecute(g_hWndMain
									,_T("open")
									,szPath
									,NULL
									,_PATH_ROOT("")
									,SW_SHOWNORMAL);
					return FALSE;
				}
				else
				{
					SCREEN_WIDTH	= nWidth	;
					SCREEN_HEIGHT	= nHeight;
					g_nDisplayMode	= nMode	;
					m_CheckStep		= CHECK_NOTRY;
				}
			}
		}

	}
	return TRUE;
}


BOOL GameInPlayingInstance::_RenderHeroStatus(DWORD /*dwTick*/)
{
//#ifdef _NO_DISABLE
	LPCSTR szText;

	Vector3D vPos  = theHero.GetPosition();
	//Vector3D vPos2 = theVHero.GetPosition();

	theTileMap.PathHandleReset(g_pPathExplorer,vPos);

	szText = FMSTR	( "Current Tile : %d[%d]"
						, theHero.GetPathExplorer()->GetTile() 
						,g_pPathExplorer->GetTile());

	DrawGameText( 20, 90, szText);

	szText = FMSTR	( "Current Pos : x : %f y: %f z : %f", vPos.x,vPos.y,vPos.z );
	DrawGameText( 20, 105, szText);

	//if(vPos.x - vPos2.x != 0)
	//{
	//	LOGINFO	( "Current Pos : x : %f y: %f z : %f   V:x : %f y: %f z : %f\n"
	//				, vPos.x,vPos.y,vPos.z,vPos2.x,vPos2.y,vPos2.z );
	//}


	PLAYER_ACTION& curact	= theHero.GetCurrentAction();
	PLAYER_ACTION queueact	= theHeroActionInput.GetQueueAction();

	szText = FMSTR	( "Current Action : %s" , GetActionTypeLabel(curact.ActionID) );
	DrawGameText( 20, 130, szText);
	szText = FMSTR	( "Queue Action : %s" , GetActionTypeLabel(queueact.ActionID) );
	DrawGameText( 20, 145, szText);
	szText = FMSTR	( "Remain Attack Result : %d" , theHero.GetAttackResultListSize());
	DrawGameText( 20, 160, szText);

	szText = FMSTR	( "Current Move Speed : %f" , theHero.GetMoveSpeedModifier() );
	DrawGameText( 20, 175, szText);

	szText = FMSTR	( "Current Attack Speed : %ld" , theHero.GetPlayerAttributes()->GetAttSpeedRatio()  );
	DrawGameText( 20, 190, szText);

	szText = FMSTR	( "Fighting Energy : %d" , theHero.GetFightingEnergy());
	DrawGameText( 20, 205, szText);
	

	int iMaxTile[64];

	ZeroMemory(iMaxTile,sizeof(iMaxTile));


	static char szRoutes[1024]={0};
	if (theHero.GetPathExplorer()->IsMoving()) 
	{
		int iRouteNum = theHero.GetPathExplorer()->GetTileRoute(iMaxTile);

		if (iRouteNum > 0)
		{

			int iVisibleRouteNum = iRouteNum;

			if (iVisibleRouteNum > 10)
				iVisibleRouteNum = 10;

			//ZeroMemory(szText,sizeof(szText));

			sprintf(szRoutes,"Route Count(%d) ",iRouteNum);
			//strcat(szText,szTile);

			for (int a = 0; a < iVisibleRouteNum; a++)
			{
				//ZeroMemory(szTile,sizeof(szTile));
				sprintf(szRoutes,"%s[%d] ",szRoutes,iMaxTile[a]);
				//strcat(szText,szTile);				
			}

		}
	}
	DrawGameText( 20, 230, szRoutes);//, COLOR_RGB( 0x0F, 255, 0x0f ) );


	szText = FMSTR	( "TempVariable : %d %d %d" , g_TempVariable[0],g_TempVariable[1],g_TempVariable[2]);
	DrawGameText( 20, 245, szText);//, COLOR_RGB( 0x0F, 255, 0x0f ) );

	int iCurTile = theHero.GetPathExplorer()->GetTile();

	int iNearNode[3];

	theTileMap.GetNearNodes(iCurTile , iNearNode , 3);

	szText = FMSTR	( "NearNodes : %d %d %d" , iNearNode[0],iNearNode[1],iNearNode[2]);
	DrawGameText( 20, 260, szText);//, COLOR_RGB( 0x0F, 255, 0x0f ) );

	szText = FMSTR	( "Picked Tile : %d (%f %f %f)" , theHeroActionInput.GetPickedTile(),theHeroActionInput.GetPickedPos().x,theHeroActionInput.GetPickedPos().y,theHeroActionInput.GetPickedPos().z);
	DrawGameText( 20, 275, szText);//, COLOR_RGB( 0x0F, 255, 0x0f ) );


    szText = FMSTR	( "Exp : %ld / %ld" , theHero.GetExp() , theHero.GetNextExp());
    DrawGameText( 20, 290, szText);//, COLOR_RGB( 0x0F, 255, 0x0f ) );

//#endif
	return TRUE;
}


void GameInPlayingInstance::OnWinMsg( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
	if(!IsPlaying())
		return;
	theVHeroInputLayer.OnWinMsg(  hWnd,  uMsg,  wParam,  lParam);
}
void GameInPlayingInstance::OnAppFocus(BOOL bFocus)
{
	if(bFocus)
		SetPlaying();
	else
		SetPause();
}
void GameInPlayingInstance::OnAppActive(BOOL bActive)
{
	if(bActive)
		SetPlaying();
	else
		SetPause();
}


void GameInPlayingInstance::OnEnterWorld()
{	
	theGameUIManager.UIOnEnterWorld(INPLAYING_UILISTENER);


	if(theHeroData.GetHeroInfo().m_byFirstEnterWorld)
	{
		theHeroData.GetHeroInfo().m_byFirstEnterWorld = FALSE;
		theHero.OnFirstEnterWorld();
	}

	theHero.OnGameInPlaying();
	//theFreshmanTipManager.BeginTip(theGeneralGameParam.GetFreshManTipReadyTime());
}


void GameInPlayingInstance::InitLandData()
{	
	BBox3D bdv	= theTileWorld.GetTileMapCombination()->GetLandSize();
		DWORD		XSize = 0,
					YSize = 0;

		XSize = (DWORD)abs(bdv.m_Max.x - bdv.m_Min.x);
		YSize = (DWORD)abs(bdv.m_Max.y - bdv.m_Min.y);

	BYTE	byMarginX = 0,
			byMarginY = 0;

	XSize % VILLAGE_SECTOR_SIZE == 0 ? byMarginX = 0 : byMarginX = 1;
	YSize % VILLAGE_SECTOR_SIZE == 0 ? byMarginY = 0 : byMarginY = 1;

	m_dwSectorXNum				= XSize / VILLAGE_SECTOR_SIZE + byMarginX;
	m_dwSectorYNum				= YSize / VILLAGE_SECTOR_SIZE + byMarginY;
}


BOOL GameInPlayingInstance::CreateHero(BOOL bLocal)
{
	if(singleton::ExistHero())
		return TRUE;
		//DestroyHero(bLocal);
	
	DWORD dwHeroKey = INVALID_DWORD_ID;
	if(!bLocal)
		dwHeroKey = theHeroData.GetHeroInfo().m_dwKey;
	if( NULL == theObjectManager.CreateHero(dwHeroKey,theHeroData.GetHeroInfo().m_CharInfo.m_byClassCode))
		return FALSE;


	sHERO_INFO&			heroData		=  theHeroData.GetHeroInfo();
	sPLAYERINFO_BASE&	playerInfo	= heroData.m_CharInfo;

	if(bLocal)
	{
		CreateHeroData();
	}

	theHero.SetHeroData(theHeroData);
	//theHero.SetHeroInfo(theHeroData.GetHeroInfo());
	if(bLocal)
	{

		theHero.SetMoveSpeedRatio(100);
		theHero.SetAttackSpeedRatio(100);

		//theHero.SetPosition(wzVec );
		//theHero.Rotate(0.0f);		
		//theHero.SetHP(100);
		//theHero.SetMP(100);

		sCHARACTER_CLIENTPART part;
		ZeroMemory(&part,sizeof(part));

		strcpy(part.m_szCharName, playerInfo.m_szCharName);

		part.m_bySlot			= playerInfo.m_bySlot;
		part.m_byClass			= playerInfo.m_byClassCode;
		part.m_LV				= playerInfo.m_LV;
		part.m_bySex			= playerInfo.m_bySex;
		part.m_byHairColor	= playerInfo.m_byHairColor;
		part.m_byHair			= playerInfo.m_byHair;
		part.m_byHeight		= playerInfo.m_byHeight	;
		part.m_byFace			= playerInfo.m_byFace	;

		part.m_dwRegion		= playerInfo.m_dwRegion	;
		part.m_wX				= playerInfo.m_sLocationX;
		part.m_wY				= playerInfo.m_sLocationY;
		part.m_wZ				= playerInfo.m_sLocationZ;

		theHero.SetClientPartInfo(&part);

		///////////////////////////////////////////////////
		//暂时使用
	}
	else
	{
	}

	if(m_fJumpToZ == 0.f)
	{
		FLOAT fRoleHeight = 80.0f*SCALE_MESH;
		m_fJumpToZ = theGameWorld.GetHeightAt(m_fJumpToZ+fRoleHeight
                                   ,20.0f*SCALE_MESH
											  ,m_fJumpToX
											  ,m_fJumpToY
											  ,0);
	}

	Vector3D vPos;
	vPos.Set( 	m_fJumpToX ,m_fJumpToY, m_fJumpToZ);

	theHero.SetPosition(vPos);





	return TRUE;
}

void GameInPlayingInstance::DestroyHero(BOOL /*bLocal*/)
{
	theObjectManager.DestroyHero();
}

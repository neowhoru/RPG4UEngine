/*////////////////////////////////////////////////////////////////////////
文 件 名：SceneLogicFlow.cpp
创建日期：2007年9月29日
最后更新：2007年9月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SceneLogicFlow.h"
#include "MoviePlayScene.h"
#include "StartScene.h"
#include "LoadScene.h"
#include "LoginScene.h"
#include "UITestingScene.h"
#include "CharacterScene.h"
#include "VillageScene.h"
#include "V3DGameWorldDefine.h"
#include "VObjectManager.h"
#include "V3DTerrain.h"
#include "HeroData.h"
#include "GameParameter.h"
#include "FreshmanTipManager.h"

using namespace scene;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SceneLogicFlow, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SceneLogicFlow::SceneLogicFlow()
{
}
SceneLogicFlow::~SceneLogicFlow()
{
}

BOOL SceneLogicFlow::StartUp()
{
	singleton::GetMoviePlayScene().Init();
	singleton::GetStartScene()		.Init();
	singleton::GetLoadScene()		.Init();
	singleton::GetLoginScene()		.Init();
	theCharacterScene					.Init();
	theVillageScene					.Init();

	theFrameworkSystem.RegFramework( &singleton::GetMoviePlayScene() );
	theFrameworkSystem.RegFramework( &singleton::GetStartScene() );
	theFrameworkSystem.RegFramework( &singleton::GetLoadScene() );
	theFrameworkSystem.RegFramework( &singleton::GetLoginScene() );
	theFrameworkSystem.RegFramework( &theCharacterScene);
	theFrameworkSystem.RegFramework( &theVillageScene);

	singleton::GetMoviePlayScene().AddListener(this);
	singleton::GetStartScene()		.AddListener(this);
	singleton::GetLoadScene()		.AddListener(this);
	singleton::GetLoginScene()		.AddListener(this);
	theCharacterScene					.AddListener(this);
	theVillageScene					.AddListener(this);
	//singleton::GetMoviePlayScene().Init(); 由GetFrameworkSystem.ChangeScene来调用

	///////////////////////////////////////////
	//theFreshmanTipManager.BeginTip(theGeneralGameParam.GetFreshManTipReadyTime());

	///////////////////////////////////////////
	if(theGeneralGameParam.IsUITesting())
	{
		theFrameworkSystem.RegFramework( &singleton::GetUITestingScene());
		theFrameworkSystem.ChangeScene( SCENE_TYPE_TESTUI );
	}
	else
	{
		ESCENE_TYPE scene = GetSceneFromName(theGeneralGameParam.GetFirstSceneType());
		if( !CanSceneBeFirstStart(scene) )
			scene = GetDefaultFirstStartScene();

		if(theGeneralGameParam.IsMustLoadMapData())
		{
			char*		pSceneName	= NULL;
			MAPCODE	dwID			= INVALID_MAPCODE;
			FIELDID	fieldID		= INVALID_FIELDID;
			BOOL		bAutoHide	= TRUE;
			switch(scene)
			{
			case SCENE_TYPE_LOGIN:
				{
					if(theClientSetting.IsLoginAuto())
					{
						scene = SCENE_TYPE_LOGIN ;
					}
					else
					{
						pSceneName = theGeneralGameParam.GetLoginSceneName();
					}
				}
				break;
			case SCENE_TYPE_CHARSELECT:
				pSceneName = theGeneralGameParam.GetCharSelectSceneName();
				break;
			case SCENE_TYPE_VILLAGE:
				{
					bAutoHide		= FALSE;
					if(theGeneralGameParam.IsLocalHeroData())
					{
						dwID			= (MAPCODE)theHeroData.GetHeroInfo().m_CharInfo.m_dwRegion;
						//fieldID	= theHeroData.GetHeroInfo().m_CharInfo.m_dwRegion;
					}
					else
					{
						dwID		= theGeneralGameParam.GetStartMapID();
						fieldID	= theGeneralGameParam.GetStartFieldID();
					}
				}
				break;
			}
			if(pSceneName || dwID != -1)
			{
				if(pSceneName)
					theLoadScene.SetGoingToMap(pSceneName  , TRUE, bAutoHide);
				else
					CreateVillageGoingToMap(dwID,fieldID);

				theLoadScene.SetNextSceneAfterLoaded(scene);
				theFrameworkSystem.ChangeScene( SCENE_TYPE_LOAD );
				scene = SCENE_TYPE_NONE;
			}
		}

		if(scene != SCENE_TYPE_NONE)
			theFrameworkSystem.ChangeScene(scene);
		//GotoNextScene(scene, );
	}
	return TRUE;
}

void SceneLogicFlow::Shutdown()
{
	theFrameworkSystem.CloseScene(  );

	theVillageScene				.RemoveListener(this);
	theCharacterScene				.RemoveListener(this);
	singleton::GetLoginScene()	.RemoveListener(this);
	singleton::GetLoadScene()	.RemoveListener(this);
	singleton::GetStartScene()	.RemoveListener(this);
	singleton::GetMoviePlayScene().RemoveListener(this);

	if(theGeneralGameParam.IsUITesting())
		theFrameworkSystem.UnregFramework( singleton::GetUITestingScene().	GetType() );

	theFrameworkSystem.UnregFramework( theVillageScene.					GetType() );
	theFrameworkSystem.UnregFramework( theCharacterScene.					GetType() );
	theFrameworkSystem.UnregFramework( singleton::GetLoginScene().		GetType() );
	theFrameworkSystem.UnregFramework( singleton::GetLoadScene().		GetType() );
	theFrameworkSystem.UnregFramework( singleton::GetStartScene().		GetType() );
	theFrameworkSystem.UnregFramework( singleton::GetMoviePlayScene().GetType() );

	theVillageScene					.Release();
	theCharacterScene					.Release();
	singleton::GetLoginScene()		.Release();
	singleton::GetLoadScene()		.Release();
	singleton::GetStartScene()		.Release();
	singleton::GetMoviePlayScene().Release();

}


void SceneLogicFlow::OnSceneStart(LPARAM  lpParam)
{
	SceneBase* pScene = (SceneBase*)lpParam;
	switch(pScene->GetType())
	{
	case SCENE_TYPE_MOVIEPLAY:
		OnMoviePlaySceneStart(pScene);
		break;
	case SCENE_TYPE_START:
		OnStartSceneStart(pScene);
		break;
	case SCENE_TYPE_LOAD:
		OnLoadSceneStart(pScene);
		break;
	case SCENE_TYPE_LOGIN:
		OnLoginSceneStart(pScene);
		break;
	case SCENE_TYPE_CHARSELECT:
		OnCharacterSceneStart(pScene);
		break;
	case SCENE_TYPE_VILLAGE:
		OnVillageSceneStart(pScene);
		break;
	}
}

void SceneLogicFlow::OnSceneEnd(LPARAM  lpParam)
{
	SceneBase* pScene = (SceneBase*)lpParam;
	switch(pScene->GetType())
	{
	case SCENE_TYPE_MOVIEPLAY:
		OnMoviePlaySceneFinished(pScene);
		break;
	case SCENE_TYPE_START:
		OnStartSceneFinished(pScene);
		break;
	case SCENE_TYPE_LOAD:
		OnLoadSceneFinished(pScene);
		break;
	case SCENE_TYPE_LOGIN:
		OnLoginSceneFinished(pScene);
		break;
	case SCENE_TYPE_CHARSELECT:
		OnCharacterSceneFinished(pScene);
		break;
	case SCENE_TYPE_VILLAGE:
		OnVillageSceneFinished(pScene);
		break;
	}
}


void SceneLogicFlow::GotoLoginScene(SceneBase* pFromScene,DWORD dwWaitingSec)
{
    theLoadScene.SetGoingToMap( theGeneralGameParam.GetLoginSceneName() , TRUE);
    theLoadScene.SetNextSceneAfterLoaded(SCENE_TYPE_LOGIN);

	 if(dwWaitingSec != -1 && pFromScene)
		pFromScene->ChangeSceneReady(dwWaitingSec, SCENE_TYPE_LOAD);
	 else
		theFrameworkSystem.ChangeScene( SCENE_TYPE_LOAD );
}


void SceneLogicFlow::GotoCharSelectScene(SceneBase* pFromScene,DWORD dwWaitingSec)
{
    theLoadScene.SetGoingToMap( theGeneralGameParam.GetCharSelectSceneName() , TRUE);
    theLoadScene.SetNextSceneAfterLoaded(SCENE_TYPE_CHARSELECT);

	 if(dwWaitingSec != -1 && pFromScene)
		pFromScene->ChangeSceneReady(dwWaitingSec, SCENE_TYPE_LOAD);
	 else
		theFrameworkSystem.ChangeScene( SCENE_TYPE_LOAD );
}

void SceneLogicFlow::GotoNextScene(SCENE_TYPE scene, BOOL bLoad)
{
	if(bLoad)
	{
		singleton::GetLoadScene().SetNextSceneAfterLoaded(scene);
		theFrameworkSystem.ChangeScene(SCENE_TYPE_LOAD);
	}
	else
		theFrameworkSystem.ChangeScene(scene);
}


void SceneLogicFlow::GotoVillageScene(MAPCODE mapID,int nTilePosX,int nTilePosY)
{
	int nMapX;
	int nMapY;
	int		nLoadX,
				nLoadY;
	float fMoveToX;
	float fMoveToY;

	fMoveToX		= nTilePosX * MAPTILESIZE;
	fMoveToY		= nTilePosY * MAPTILESIZE;
	nMapX			= nTilePosX / MAP_SIZE_BYTILE;
	nMapY			= nTilePosY / MAP_SIZE_BYTILE;

	nLoadX	= nMapX*MAP_SIZE_BYTILE + 96;
	nLoadY	= nMapY*MAP_SIZE_BYTILE + 69;

	theLoadScene.SetGointToMovePos	(fMoveToX,fMoveToY);
	theLoadScene.SetGoingToMap(mapID,-1,nLoadX, nLoadY, FALSE);
	theLoadScene.SetNextSceneAfterLoaded(SCENE_TYPE_VILLAGE);
	theFrameworkSystem.ChangeScene(SCENE_TYPE_LOAD);
}


void SceneLogicFlow::CreateVillageGoingToMap(MAPCODE mapID,FIELDID fieldID)
{

	int nBornX;
	int nBornY;
	float		fMoveToX,
				fMoveToY;
	int		nLoadX,
				nLoadY;
	INT		nStartX,
				nStartY;

	if(theGeneralGameParam.IsLocalHeroData())
	{
		nBornX	= -1;
		nBornY	= -1;
		nStartX	= theHeroData.GetHeroInfo().m_CharInfo.m_sLocationX;
		nStartY	= theHeroData.GetHeroInfo().m_CharInfo.m_sLocationY;
	}
	else
	{
		nBornX	= theGeneralGameParam.GetStartMapX();
		nBornY	= theGeneralGameParam.GetStartMapY();
		nStartX	= theGeneralGameParam.GetStartTileX();
		nStartY	= theGeneralGameParam.GetStartTileY() ;
	}

	//////////////////////////////////////////////////////
	if(nBornX == -1 || nBornY == -1)
	{
		nBornX = nStartX / MAP_SIZE_BYTILE;
		nBornY = nStartY / MAP_SIZE_BYTILE;

		fMoveToX  = nStartX * MAPTILESIZE;
		fMoveToY	 = nStartY * MAPTILESIZE;
	}
	else
	{
		fMoveToX  = (nBornX*MAP_SIZE_BYTILE + nStartX) * MAPTILESIZE;
		fMoveToY	 = (nBornY*MAP_SIZE_BYTILE + nStartY) * MAPTILESIZE;
	}

	nLoadX	= nBornX*MAP_SIZE_BYTILE + 96;
	nLoadY	= nBornY*MAP_SIZE_BYTILE + 69;

	//assert(0 && "有待测试 ");//
	theLoadScene.SetGointToMovePos	(fMoveToX,fMoveToY);
	theLoadScene.SetGoingToMap		(mapID,fieldID,nLoadX, nLoadY, FALSE);

}


void SceneLogicFlow::OnMoviePlaySceneStart(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnMoviePlaySceneFinished(SceneBase* /*pScene*/)
{
	theFrameworkSystem.ChangeScene( SCENE_TYPE_START );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void SceneLogicFlow::OnLoadSceneStart(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnLoadSceneFinished(SceneBase* pScene)
{
	LoadScene* pLoadScene = (LoadScene*)pScene;
	theFrameworkSystem.ChangeScene( pLoadScene->GetNextSceneAfterLoaded() );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void SceneLogicFlow::OnStartSceneStart(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnStartSceneFinished(SceneBase* /*pScene*/)
{
	//SCENE_TYPE iSceneType = SCENE_TYPE_LOGIN;

    //if (theGeneralGameParam.IsTestUIScene())
    //{
    //    iSceneType = SCENE_TYPE_TESTUI;
    //}
    //else
    {
        if (theGeneralGameParam.IsEnableNetwork())
        {
            // 载入登录窗口
				GotoLoginScene();
        }
        else
        {
			  GotoCharSelectScene();
        }
    }

	//theFrameworkSystem.ChangeScene( iSceneType );
}


void SceneLogicFlow::OnLoginSceneStart(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnLoginSceneFinished(SceneBase* pScene)
{
	if(theClientSetting.IsLoginAuto())
		theFrameworkSystem.ChangeScene( SCENE_TYPE_CHARSELECT );
	else
		GotoCharSelectScene((SceneBase*)pScene, 0);
}

void SceneLogicFlow::OnCharacterSceneStart(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnCharacterSceneFinished(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnVillageSceneStart(SceneBase* /*pScene*/)
{
}

void SceneLogicFlow::OnVillageSceneFinished(SceneBase* /*pScene*/)
{
}

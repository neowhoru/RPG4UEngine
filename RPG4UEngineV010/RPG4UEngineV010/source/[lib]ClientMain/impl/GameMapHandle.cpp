/*////////////////////////////////////////////////////////////////////////
文 件 名：GameMapHandle.cpp
创建日期：2007年9月18日
最后更新：2007年9月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameMapHandle.h"
#include "V3DCamera.h"
#include "V3DGameWorld.h"
#include "V3DScene.h"
#include "V3DSceneAnimation.h"
#include "V3DSceneObject.h"
#include "VObjectSystem.h"
#include "ObjectSystem.h"
#include "SceneDefine.h"
#include "V3DCamera.h"
#include "V3DConfig.h"
#include "RenderSystemManager.h"
#include "V3DHugeModelManager.h"
#include "PathGameMap.h"
#include "Camera.h"
#include "TileWorldClient.h"
#include "ApplicationSetting.h"
#include "GameParameter.h"
#include "V3DTerrainChunk.h"
#include "Map.h"

#define WH_DUMMY_SKY			"sky"
#define WH_DUMMY_EYE			"eye"
#define WH_DUMMY_LOOKAT		"lookat"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameMapHandle::GameMapHandle()
{
	m_pSkyDummy			= NULL;
	m_pEyeDummy			= NULL;
	m_pLookAtDummy		= NULL;
	m_bNeedWorld		= TRUE;//默认情况，需要世界地图
	m_bNeedVObject		= TRUE;
	m_pRenderer			= NULL;
	m_pSceneFrustum	= new V3DFrustum;
	m_bNeedToBuildCamera	= TRUE;
	m_pCameraHandle	= &theCamera;
	m_bLockCamera		= TRUE;
}

GameMapHandle::~GameMapHandle()
{
	SAFE_DELETE(m_pSceneFrustum);
}

BOOL GameMapHandle::Init()			
{
	__BOOL_SUPER(Init());

	if(m_pCameraHandle == NULL)
		m_pCameraHandle	= &theCamera;

	return TRUE;
}

void GameMapHandle::Release()		
{
	_SUPER::Release();
}

BOOL GameMapHandle::RequestWorkingData()	
{
	if(m_pCameraHandle == NULL)
		m_pCameraHandle	= &theCamera;

	if(!m_bNeedWorld)
	{
		__BOOL_SUPER(RequestWorkingData());
	}
	else
	{
		//assert(m_pCamera == NULL);
		m_pCamera = &g_V3DConfig.GetCamera();

		Init3DCamera();

		if(m_bNeedVObject)
		{
			theVObjectSystem.RequestWorkingData();
			theObjectSystem.RequestWorkingData();
		}

		AttachMapData();
		m_bNeedToBuildCamera	= TRUE;
	}
	//ResetCamera();

	return TRUE;
}

void GameMapHandle::ReleaseWorkingData()
{
	if(m_bNeedWorld)
	{
		//theGameWorld.ReleaseWorkingData();
		//theGameWorld.Shutdown();	

		__END(m_pCameraHandle->Release());

		//SAFE_DELETE(m_pCamera);
		m_pCamera	= NULL;
		m_pScene		= NULL;//不可删除


		if(m_bNeedVObject)
		{
			theObjectSystem.ReleaseWorkingData();
			theVObjectSystem.ReleaseWorkingData();
		}
	}
}


BOOL GameMapHandle::FrameMove(DWORD dwTick)
{
	if(m_bNeedVObject)
	{
     m_pCameraHandle->FrameMove(dwTick);

		if(m_bNeedToBuildCamera)
		{
			BuildSceneFrustum();
			m_bNeedToBuildCamera = FALSE;
		}

		theObjectSystem.FrameMove(dwTick);
		theVObjectSystem.FrameMove(dwTick);
	}

	return TRUE;
}

BOOL GameMapHandle::ReadyRender(DWORD /*dwTick*/)	
{
	m_pRenderer = the3DEngine.GetRenderer();
	if( !m_pRenderer )
		return FALSE;

	m_pRenderer->SetViewport(&GetViewPortMain());

	m_pRenderer->Clear( 0L, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER,	0, 1.0f, 0L );

	return TRUE;
}

BOOL GameMapHandle::Render(DWORD dwTick)	
{
	if(m_bNeedWorld)
	{
		m_pRenderer->SetRenderState( D3DRS_FOGENABLE, FALSE );

		if(theGameWorld.GetDayNightAnimation())
			theGameWorld.UpdateDayNightAnimation();

		if(m_pSkyDummy)
		{
			theGameWorld.RenderSky(dwTick,m_pSkyDummy->GetPosition() );
		}

		m_pRenderer->SetTransform( D3DTS_VIEW, (FLOAT*)m_pCamera->GetViewMatrix() );
		m_pRenderer->SetTransform( D3DTS_PROJECTION, (FLOAT*)m_pCamera->GetProjMatrix() );

		if( m_pScene )
		{
			m_pScene->RenderSceneObjects();
		}

		RenderCollision(dwTick);
	}

	if(m_bNeedVObject)
	{
		theObjectSystem.Render(dwTick);
		theVObjectSystem.Render(dwTick);

		IV3DRenderer* pRenderer = the3DEngine.GetRenderer();
		m_pRenderer->SetRenderState( D3DRS_FOGENABLE, FALSE );	
		theVObjectSystem.RenderInfo(dwTick);
		pRenderer->SetRenderState( D3DRS_FOGENABLE, TRUE );	
	}
	return TRUE;
}


BOOL GameMapHandle::RenderCollision(DWORD dwTick)
{
	if(!theApplicationSetting.m_ShowCollision )
		return TRUE;

	if( theGameWorld.GetHeroPlotMap() )
	{
		theGameWorld.GetHeroPlotMap()->GetScene()->GetCollision()->Render(dwTick);

		theV3DHugeModelManager.RenderCollision(dwTick);
	}
	return TRUE;
}


INT GameMapHandle::AttachMapData()
{
	//if(m_pBigObject)
	//	return;
	Vector3 vFromPt	= Vector3( 0.0f, 300.0f, 200.0f );
	Vector3 vLookatPt	= Vector3( 0.0f, 0.0f, 0.0f );
	Vector3 vUpVec		= Vector3( 0.0f, 0.0f, 1.0f );
	m_pCamera->SetViewParams( vFromPt, vLookatPt, vUpVec );
	
	float fAspect = (float)SCREEN_WIDTH/SCREEN_HEIGHT;
	m_pCamera->SetProjParams( math::cPI*(50/45.0f)/4, fAspect,  1.0f, 18000.0f );

	//SetThreadPriority( GetGameClientApplication().m_hLoadWnd,THREAD_PRIORITY_NORMAL);//THREAD_PRIORITY_BELOW_NORMAL);

	if(! theGameWorld.GetHeroPlotMap() )
		return ATTACH_NONE_SCENE;

	m_pScene = theGameWorld.GetHeroPlotMap()->GetScene();
	if(!m_pScene )
		return ATTACH_NONE_SCENE;

	m_pSkyDummy = m_pScene->FindSceneObject(WH_DUMMY_SKY);
	if(!m_pSkyDummy)
		return ATTACH_NONE_SKY;

	m_pEyeDummy = m_pScene->FindSceneObject(WH_DUMMY_EYE);
	if(!m_pEyeDummy)
		return ATTACH_NONE_EYE;

	m_pLookAtDummy = m_pScene->FindSceneObject(WH_DUMMY_LOOKAT);
	if(!m_pLookAtDummy)
		return ATTACH_NONE_LOOKAT;

	ResetCamera();
	//m_pCamera->SetViewParams(m_pEyeDummy->GetPosition()
 //                          ,m_pLookAtDummy->GetPosition()
	//								,Vector3( 0,0,1));

	thePathGameMap.SetSize(128, 128);
	thePathGameMap.SetCellSize(MAPTILESIZE);

	return ATTACH_OK;
}

void GameMapHandle::UseGameWorld(BOOL bUseWorld,BOOL bUseVObject)
{
	m_bNeedWorld	= bUseWorld;
	m_bNeedVObject	= bUseVObject;
}

BOOL GameMapHandle::OnUpdateWorldPos	()
{
	return TRUE;
}

BOOL GameMapHandle::OnUpdateCameraPos()
{


	theCamera.SetLockDistance	(theClientSetting.IsLockCameraDistance());
	theCamera.SetCameraFromTo	(theClientSetting.GetCameraFrom()
										,theClientSetting.GetCameraTo());
	theCamera.UpdateListeners();

	return TRUE;
}

void GameMapHandle::BuildSceneFrustum()
{
	OnUpdateCameraPos();
	m_pSceneFrustum->Build(m_pCamera->GetViewMatrix()
                         ,m_pCamera->GetProjMatrix())		;
}

void GameMapHandle::ResetViewport()
{
	IV3DRenderer* m_pRenderer = the3DEngine.GetRenderer();
	if(m_pRenderer )
		m_pRenderer->SetViewport( &m_ViewPortMain );
}

void GameMapHandle::ResetCamera()
{
	if(m_bLockCamera )
	{
		SceneBase*	pScene;
		pScene = dynamic_cast<SceneBase*>(theFrameworkSystem.GetCurrentScene());
		if(pScene)
			theClientSetting.SetLockCameraDistance(pScene->IsLockCameraDistance());

		if(m_pLookAtDummy)
			theClientSetting.SetCameraTo	(m_pLookAtDummy->GetPosition());
		if(m_pEyeDummy)
			theClientSetting.SetCameraFrom(m_pEyeDummy->GetPosition());
	}

	BuildSceneFrustum();
}


BOOL GameMapHandle::PickRayAtScreen(Ray& ray,const POINT& ptScreen)
{
	UINT	uWidth	(SCREEN_WIDTH),
			uHeight	(SCREEN_HEIGHT);

	Vector3X		v;
	Matrix4&		matProj = m_pCamera->GetProjMatrix();

	v.x =  ( ( ( 2.0f * ptScreen.x ) / uWidth  ) - 1 ) / matProj._11;
	v.y = -( ( ( 2.0f * ptScreen.y ) / uHeight ) - 1 ) / matProj._22;
	v.z =  1.0f;

	// Get the inverse view matrix
	Matrix4  m(m_pCamera->GetViewMatrix());
	m.Inverse();

	// Transform the screen space pick ray into 3D space
	ray.m_Direction.x		= v.x*m._11 + v.y*m._21 + v.z*m._31;
	ray.m_Direction.y		= v.x*m._12 + v.y*m._22 + v.z*m._32;
	ray.m_Direction.z		= v.x*m._13 + v.y*m._23 + v.z*m._33;
	ray.m_Origin.x			= m._41;
	ray.m_Origin.y			= m._42;
	ray.m_Origin.z			= m._43;

	ray.m_Direction.Normalize();
//#ifndef USE_RAYNEG 
//	ray.m_Direction *= -1.f;
//#endif

	return TRUE;
}

BOOL GameMapHandle::PickCamera(const RAY& ray,float& t)
{
	__CHECK( theGameWorld.GetHeroPlotMap() );

	V3DTerrain *				pTerrain;
	V3DScene*					pScene;
	//sBSP_INTERSECT* pIntersect;

	pTerrain = (V3DTerrain*)theGameWorld.GetHeroPlotMap()->GetTerrain();
	pScene	= theGameWorld.GetHeroPlotMap()->GetScene();

	__CHECK( pTerrain && pScene );

	V3DTerrainIntersectionArray	arIntersection;

	INT nChunkX = (INT)(ray.m_Origin.y/( TILE_3DSIZE*TILE_RES ));
	INT nChunkY = (INT)(ray.m_Origin.x/( TILE_3DSIZE*TILE_RES ));

	V3DTerrainChunk* pChunk = theGameWorld.GetChunk( nChunkY, nChunkX );

	if( pChunk )
		pChunk->GetTerrainIntersection(ray
												,&arIntersection
												,pChunk->GetDetailLevel());



	INT nCollisionChunkX[3];
	INT nCollisionChunkY[3];

	if( ray.m_Direction.y > 0 )
	{
		if( ray.m_Direction.x > 0 )
		{
			nCollisionChunkX[0] = nChunkX+1;
			nCollisionChunkY[0] = nChunkY+1;
			nCollisionChunkX[1] = nChunkX+1;
			nCollisionChunkY[1] = nChunkY;
			nCollisionChunkX[2] = nChunkX;
			nCollisionChunkY[2] = nChunkY+1;
		}
		else
		{
			nCollisionChunkX[0] = nChunkX+1;
			nCollisionChunkY[0] = nChunkY-1;
			nCollisionChunkX[1] = nChunkX+1;
			nCollisionChunkY[1] = nChunkY;
			nCollisionChunkX[2] = nChunkX;
			nCollisionChunkY[2] = nChunkY-1;
		}
	}
	else
	{
		if( ray.m_Direction.x > 0 )
		{
			nCollisionChunkX[0] = nChunkX-1;
			nCollisionChunkY[0] = nChunkY+1;
			nCollisionChunkX[1] = nChunkX-1;
			nCollisionChunkY[1] = nChunkY;
			nCollisionChunkX[2] = nChunkX;
			nCollisionChunkY[2] = nChunkY+1;
		}
		else
		{
			nCollisionChunkX[0] = nChunkX-1;
			nCollisionChunkY[0] = nChunkY-1;
			nCollisionChunkX[1] = nChunkX-1;
			nCollisionChunkY[1] = nChunkY;
			nCollisionChunkX[2] = nChunkX;
			nCollisionChunkY[2] = nChunkY-1;
		}
	}

	for( int i = 0; i < 3; i++ )
	{
		pChunk = theGameWorld.GetChunk( nCollisionChunkY[i], nCollisionChunkX[i] );
		if( pChunk )
			pChunk->GetTerrainIntersection(ray
													,&arIntersection
													,pChunk->GetDetailLevel());
	}

	if( arIntersection.size() > 0 )
	{
		V3DTerrainIntersection* pInters  = &arIntersection[0];
		V3DTerrainIntersection* pCmp ;
		for(UINT i = 1; i < arIntersection.size(); i++ )
		{
			pCmp = &arIntersection[i];
			if( pInters->t > pCmp->t )
				pInters = pCmp;
		}

		t = pInters->t;//(pInters->p - ray.m_Origin).Length();
		return TRUE;
	}

	return FALSE;
}


BOOL GameMapHandle::Init3DCamera()
{

	FLOAT				fAspect			= ((FLOAT)SCREEN_WIDTH) / SCREEN_HEIGHT;

	m_pRenderer		= the3DEngine.GetRenderer();

	///读取配置信息

	__INIT(m_pCameraHandle->Init (theGeneralGameParam.GetCameraNearClip()
                         ,theGeneralGameParam.GetCameraFarClip()
								 ,theGeneralGameParam.GetCameraFOV()
								 ,fAspect
								 ,this));

#define CAMERA_SETTING(Name)\
	m_pCameraHandle->Set##Name(theGeneralGameParam.GetCamera##Name());
#define CAMERA_SETTING2(Name)\
	m_pCameraHandle->Set##Name(theGeneralGameParam.GetCamera##Name() );

	CAMERA_SETTING2(DistanceMin);
	CAMERA_SETTING2(DistanceMax);
	CAMERA_SETTING2(Distance);

	CAMERA_SETTING(PitchMin);
	CAMERA_SETTING(PitchMax);
	CAMERA_SETTING(AnglePitch);
	CAMERA_SETTING(YawMin);
	CAMERA_SETTING(YawMax);
	CAMERA_SETTING(AngleYaw);
	CAMERA_SETTING(YawCenter);
	CAMERA_SETTING(RotationStep);
	CAMERA_SETTING(RotationSlowDownLevel);
	CAMERA_SETTING(RotationSlowDown);
	CAMERA_SETTING(RotationMin);

	CAMERA_SETTING(UpCorrect);
	CAMERA_SETTING(TargetTrackSlotDownStep);


	m_ViewPortMain.X		= 0;
	m_ViewPortMain.Y		= 0;
	m_ViewPortMain.Width = SCREEN_WIDTH;
	m_ViewPortMain.Height= SCREEN_HEIGHT;
	m_ViewPortMain.MinZ	= 0;
	m_ViewPortMain.MaxZ	= 1;
	m_pRenderer->SetViewport( &m_ViewPortMain );

	//theApplicationSetting.m_FarPlane				= theGameWorld.ChangeFarPlane( 7 );
	theApplicationSetting.m_FarPlane				= theGameWorld.GetFarPlane();
	theApplicationSetting.m_FarFog				= theApplicationSetting.m_FarPlane *0.8f;	

	return TRUE;
}

VOID GameMapHandle::SetCameraParams	( const VECTOR3D& vEyePt
												, const VECTOR3D& vLookatPt
												, const VECTOR3D& vUpVec
												, BOOL				bLeftHand )
{
	if(!m_pCamera)
		return;
	m_pCamera->SetViewParams((Vector3&) vEyePt
									,(Vector3&) vLookatPt
									,(Vector3&) vUpVec
									,bLeftHand);
}

VOID GameMapHandle::SetProjectionParams	(FLOAT fFOV
															,FLOAT fAspect
															,FLOAT fNearPlane
															,FLOAT fFarPlane)
{
	if(!m_pCamera)
		return;
	m_pCamera->SetProjParams( fFOV, 
						fAspect, 
						fNearPlane, 
						fFarPlane );

	m_pRenderer->SetTransform( D3DTS_PROJECTION, m_pCamera->GetProjMatrix() );
}


BOOL GameMapHandle::OnSetCamera(const RAY&	ray ,FLOAT& t, BOOL /*bHeightCheck*/)
{
	__CHECK(theGameWorld.GetWorldCastRay(ray,t) );
	//__CHECK( /*theMap.*/PickCamera(ray, t ) );

	return TRUE;
}


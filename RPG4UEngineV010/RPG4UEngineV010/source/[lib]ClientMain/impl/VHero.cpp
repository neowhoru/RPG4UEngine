/*////////////////////////////////////////////////////////////////////////
文 件 名：VHero.cpp
创建日期：2007年2月29日
最后更新：2007年2月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VHero.h"
#include "V3DTerrain.h"
#include "V3DConfig.h"
#include "V3DSceneDefine.h"
#include "VCharacterAnimationCtrl.h"
#include "PathGameMap.h"
#include "VUIFontDDraw.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace vobject;
using namespace util;
using namespace math;
using namespace tile;
GLOBALINST_SINGLETON_PTR_IMPL(VHero);//, ()  , gamemain::eInstPrioGameFunc);


namespace vobject
{

	const float MOVE_HEIGHT_GAP		=	16.18f;	//前进时高度差视为障碍
	const float MOVE_HEIGHT_GAP2		=	MOVE_HEIGHT_GAP * 2;
	const float MOVE_HEIGHT_GAP3		=	MOVE_HEIGHT_GAP * 3;
	const float ANGLE_AUTO_JUMP_MAX	=	math::cPI_X2;
	const float MOUNT_COLLISION_DIST	= 80.0f;
	const float SELF_COLLISION_DIST	= 15.0f;
	const float SELF_COLLISION_DIST2	= 40.0f;
	const float JUMP_COLLISION_DIST	= 6.0f;

	static VHeroFuncOperation gs_vheroOperation[sOPERATION::eOpMax]=
	{
		{&VHero::_HandleNothing				,sOPERATION::eOpNothing				},
		{&VHero::_HandleIdle					,sOPERATION::eOpIdle					},
		{&VHero::_HandleUseSkill			,sOPERATION::eOpUseSkill			},
		{&VHero::_HandleTalk					,sOPERATION::eOpTalk					},
		{&VHero::_HandleGetItem				,sOPERATION::eOpGetItem				},
		{&VHero::_HandleDropItem			,sOPERATION::eOpDropItem			},
		{&VHero::_HandleMoveTo				,sOPERATION::eOpMoveTo				},
		{&VHero::_HandleTrade				,sOPERATION::eOpTrade				},
		  
		{&VHero::_HandleChangeDir			,sOPERATION::eOpChangeDir			},
		{&VHero::_HandleMoveSkill			,sOPERATION::eOpMoveSkill			},
		{&VHero::_HandleMoveTalk			,sOPERATION::eOpMoveTalk			},
		{&VHero::_HandleMovePickItem			,sOPERATION::eOpMoveGetItem		},
		{&VHero::_HandlePickItem	,sOPERATION::eOpMoveGetRangeItem	},
		{&VHero::_HandleMoveDroptem		,sOPERATION::eOpMoveDropItem		},
		{&VHero::_HandleMoveTrade			,sOPERATION::eOpMoveTrade			},
		{&VHero::_HandleMoveFollow			,sOPERATION::eOpMoveFollow			},
		{&VHero::_HandleDoAction			,sOPERATION::eOpDoAction			},
		{&VHero::_HandleVerifyPos			,sOPERATION::eOpVerifyPos			},
	};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VHero::VHero()
{
	GLOBALINST_SINGLETON_PTR_INIT(VHero);
	m_dwObjectType		= HERO_OBJECT;
	m_LockTargetID		= VOBJID_NULL;
	m_dwStartSlipTime = 0;
	m_TurnDirection	(0.f,0.f,0.f);

	m_dwShowNameFlag	= OBJNAME_SHOWHERO;
}

VHero::~VHero()
{
	GLOBALINST_SINGLETON_PTR_FREE(VHero);
}

void VHero::Update( DWORD dwTick )
{
	_SUPER::Update(dwTick);

	assert(gs_vheroOperation[m_Operation.dwType].oprType == m_Operation.dwType && "Opr类型不一样，请检查gs_vheroOperation的定义与oprType的定义");
	
	FuncOperation fOpr = gs_vheroOperation[m_Operation.dwType].pFunc;
	//if(fOpr)
	(this->*fOpr)();
}

void VHero::UpdateManual(DWORD dwTime)
{
	_SUPER::UpdateManual(dwTime) ;



	SetFinalRenderAlpha( IsInvisible()?0.3f : m_fDefaultFinalRenderAlpha);
}


BOOL VHero::RenderInfo( DWORD dwTick, DWORD dwColor , float fTransparent , float fPlayerTransparent )
{
	__BOOL_SUPER(RenderInfo(dwTick,  dwColor ,  fTransparent , fPlayerTransparent));

	if( theApplicationSetting.m_ShowAttackRange )
	{
		theV3DGraphicDDraw.DrawCircle3D( m_vPosition.x, m_vPosition.y, m_vPosition.z+1, m_fBodySize + theApplicationSetting.m_AttackRangeInc, 32, 0xffffffff );
		//theV3DGraphicDDraw.DrawCircle3D( m_vPosition.x, m_vPosition.y, m_vPosition.z+1, m_fBodySize+m_fWeaponAttackRange+theApplicationSetting.m_AttackRangeInc, 32, 0xffffffff );
	}

#ifdef _DEBUG
		char szBuf[256];
		sprintf( szBuf,"%s:(%ld,%ld)",GetName(),m_TilePos.x,m_TilePos.y );
//		DrawTextEx( 0, 30, 0xff7f7f7f, szBuf );
//		theFontMgr.DrawTextARGB( 0, 30, szBuf, 0, 0, 0xff7f7f7f );
		DrawGameText( 0, 30, szBuf, 0xff7f7f7f );
		//
		//画人物移动的轨迹
		RenderPath();
#endif
	return TRUE;
}


void VHero::JumpToTilePos( const VECTOR3D& vDest, PathGameMap* pMap )
{
	_SUPER::JumpToTilePos( vDest , pMap );
	theGameUIManager.UIRefresh(gameui::eUIMiniMap);
}


void VHero::JumpToTilePos( int x,int y,PathGameMap* pMap )
{
	_SUPER::JumpToTilePos(  x, y, pMap );
	theGameUIManager.UIRefresh(gameui::eUIMiniMap);
}

void VHero::SetMoving( BOOL bMoving, BOOL bBackOff )
{
	if(bBackOff)
	{
		g_InputData.m_HeroCounterMarch = bMoving;
	}
	_SUPER::SetMoving(bMoving, bBackOff);
}


void VHero::DisDrome()
{
	_SUPER::DisDrome();

	theVHeroActionInput.DisDrome();
}

Vector3 g_arCollisionVertices[3];
Vector3 g_CollisionRay[2];

BOOL VHero::GetCollisionDistance	(Vector3C& vStart
                                 ,Vector3C& vDir
											,float*    pfDistToCollision)
{
	float	t;
	if(pfDistToCollision == NULL)
		pfDistToCollision = &t;

	/////////////////////////////////
	memset( &g_arCollisionVertices, 0, sizeof(Vector3)*3 );
	memset( &g_CollisionRay, 0, sizeof(Vector3)*2 );


	sBSP_INTERSECT* pIntersect;
	if(theGameWorld.GetCollisionDistance( vStart,  vDir,  pfDistToCollision,&pIntersect ))
	{
		if( pIntersect )
		{
			Vector3 p0 = (pIntersect->arFacePos[0]);
			Vector3 p1 = (pIntersect->arFacePos[1]);
			Vector3 p2 = (pIntersect->arFacePos[2]);

			g_arCollisionVertices[0] = p0;
			g_arCollisionVertices[1] = p1;
			g_arCollisionVertices[2] = p2;



			//*pfDistToCollision = fDist;
			g_CollisionRay[0] = vStart;
			g_CollisionRay[1] = vStart +vDir * *pfDistToCollision;

			return TRUE;
		}
	}
	return FALSE;
}



//使用技能
BOOL	VHero::TryUseSkill( VCharacter* /*pTargetChar*/, sVSKILLINFO_BASE* /*pSkill*/ )
{
	return FALSE;
}



void VHero::TryPickItem( VObject* /*pItem*/ )
{
}

BOOL	VHero::TryDropItem		( UINT /*nPackIndex*/ )
{
    return TRUE;
}

BOOL VHero::TryPickItem()
{
    return TRUE;
}


void VHero::TryTalkWithNpc( VCharacter* /*pNpc*/ )
{
}

BOOL VHero::TryTalkWithNpc()
{

    return TRUE;
}

BOOL VHero::TryTradeWithNpc()
{
    return TRUE;
}

BOOL    VHero::IsCanTalk(int /*iDestID*/)
{
	BOOL ret = FALSE;
	//
	if (IsDead())
		return ret;

	VObject* pObject = theVObjectManager.FindByID( m_Operation.target.stDst );
	if ( pObject )
	{
		if (pObject->IsMapNpc())
		{
			ret = TRUE;
		}
	}

	return ret;
}


//一些功能的判定函数
//---
//	判定攻击的目标是否正确
//---
BOOL	VHero::IsTargetIsRight(int /*skill*/, int /*skill_level*/, int /*id*/)
{
	return FALSE;
	
}

//---
//	判断可否使用道具
//---
BOOL	VHero::IsCanUseItem()
{
	;
	//判断当前的动作可否被打断
	if (!IsPlayerCanChangeStatus())
		return FALSE;
	//
	switch (m_status.CurStatus())
	{
	case esk_PickItem:       // 捡取道具
	case esk_Talk:           // 对话
	case esk_Trade:          //交易
	case esk_Attack:         // 攻击状态
	//case esk_Intonate:       // 吟唱状态
	case esk_BeAttack:       // 受击状态
	case esk_BeatBack:       // 击退处理
	case esk_Miss:           // 闪避状态
	case esk_Sleep:          // 昏倒状态
	case esk_Die:            // 死亡状态
	case esk_PreAttack:      // 预备攻击状态
		return FALSE;
		break;
	}
	return TRUE;
	;
}
//---
//	判断可否作表情动作
//---
BOOL	VHero::IsCanDoExpression()
{
	;
	//战斗状态不可以做表情动作
	if (GetFightStatus())
		return FALSE;
	//
	//判断当前的动作可否被打断
	if (!IsPlayerCanChangeStatus())
		return FALSE;
	//
	switch (m_status.CurStatus())
	{
	case esk_PickItem:       // 捡取道具
	case esk_Talk:           // 对话
	case esk_Trade:          //交易
	case esk_Attack:         // 攻击状态
	//case esk_Intonate:       // 吟唱状态
	case esk_BeAttack:       // 受击状态
	case esk_BeatBack:       // 击退处理
	case esk_Miss:           // 闪避状态
	case esk_Sleep:          // 昏倒状态
	case esk_Die:            // 死亡状态
	case esk_PreAttack:      // 预备攻击状态
		return FALSE;
		break;
	}
	return TRUE;
	;
}

BOOL IsInAttackRange( int x, int y, VCharacter *pAttacker, POINT ptag, float fTargetBodySize, int iAttackRange )
{
	;
	static float fAttackRange = 0;
	fAttackRange = 0;

	fAttackRange += pAttacker->GetBodySize();
	fAttackRange += pAttacker->GetWeaponAttackRange();
	fAttackRange += fTargetBodySize;

	//if ( 1 > iAttackRange ) iAttackRange = 1;

	if ((abs(ptag.x - x) > iAttackRange) ||
		(abs(ptag.y - y) > iAttackRange)  )
		return FALSE;
	return TRUE;
	;
}


//是否在攻击距离内
BOOL VHero::IsInAttackRange( float /*fTargetX*/, float /*fTargetY*/, float /*fTargetBodySize*/, sVSKILLINFO_BASE * /*pSkill*/ )
{
	return FALSE;
}


BOOL VHero::IsTargetInFront( float fTargetX, float fTargetY, float /*fTargetZ*/, float fAngleMax)
{
	;
	float fAngle = 0;//fDir1和fDir2的夹角
	float x = 0;
	float y1 = 0;
	float y2 = 0;
	float c = 0;
	Vector3 vSelfDir;
	Vector3 vTargetDir;

	vSelfDir.x = m_TurnDirection.x;
	vSelfDir.y = m_TurnDirection.y;
	vSelfDir.z = 0;
	vTargetDir.x = fTargetX - m_vPosition.x;
	vTargetDir.y = fTargetY - m_vPosition.y;
	vTargetDir.z = 0;
	x = vSelfDir.x*vTargetDir.x + vSelfDir.y*vTargetDir.y + vSelfDir.z*vTargetDir.z;
	y1 = sqrtf(vSelfDir.x*vSelfDir.x + vSelfDir.y*vSelfDir.y + vSelfDir.z*vSelfDir.z);
	y2 = sqrtf(vTargetDir.x*vTargetDir.x + vTargetDir.y*vTargetDir.y + vTargetDir.z*vTargetDir.z);

	if( y1*y2 <= 0 )
	{
		if( x > 0 )
		{
			c = 1.0f;
		}

		if( x < 0 )
		{
			c = -1.0f;
		}

		if( x == 0 )
		{
			c = 0;
		}
	}
	else
	{
		c = x/(y1*y2);
	}

	if( c > 1.0f )
	{
		c = 1.0f;
	}

	if( c < -1.0f )
	{
		c = -1.0f;
	}

	fAngle = acos( c );

	if( fAngle < fAngleMax )
	{
		return TRUE;
	}
	return FALSE;

	;
}



BOOL  VHero::CanRemoveAttackInfo	(sATTACK_INFO* pAttackInfo)
{
	if(pAttackInfo->nRecordCount)
		return TRUE;
	return FALSE;
}

void	VHero::OnStepHeroRange	(BOOL /*bIn*/)
{
}

BOOL VHero::CanHideOutofHeroRange(float /*fAlphaGap*/)
{
	return FALSE;
		
}


//  判断一个玩家可否移动
BOOL    VHero::IsCanMove()
{
    return TRUE;
}
//---
//  判断是否处在一步走完的状态
//---
BOOL    VHero::IsMoveStepOver()
{
    if (0 == m_MoveInfo.dwStartTime)
        return TRUE;
    //
    return FALSE;
}
//---
//  判断一个玩家是否可以改变状态(动作)
//---
BOOL    VHero::IsPlayerCanChangeStatus()
{
    //判断当前是否处在可以中断的动作状态
    if (!m_status.IsStateCanBreak())
    {
        //
        return FALSE;
    }
	if( Player_Status_HoldSkill ==  GetPlayerStatus() )
		return FALSE;
	//坐下的时候不能改变动作
	if (0 != GetSitStatus())
		return FALSE;

    return TRUE;
}
//---
//	判断player能否改变到目标状态
//---
BOOL    VHero::IsPlayerCanChangeToStatus(int next_status)
{
	;
	switch (next_status)
	{
	case esk_PreAttack:// 预备攻击状态
	case esk_Attack:// 攻击状态
	case esk_Intonate:// 吟唱状态
	case esk_Die:// 死亡状态
	case esk_ActionTime://在一段时间内作莫个动作
	//case esk_BeAttack,       // 受击状态
	//case esk_BeatBack,       // 击退处理
	//case esk_Miss,           // 闪避状态
	//case esk_Sleep,          // 昏倒状态
		return TRUE;
		break;
	}
	return IsPlayerCanChangeStatus();
	;
}
//---
//判定目标可否攻击
//---
BOOL    VHero::IsTagCanBeAttack()
{
	;
//assert(0 && " VCharacter..IsTagCanBeAttack ");
//#pragma message(__FILE__  "(4013) VCharacter..IsTagCanBeAttack  " )

//#ifdef USE_OLD  
	VObject* pPlayerTag = theVObjectManager.FindByID( m_Operation.target.stDst );
    //
    if( pPlayerTag->IsMonster() || 
        pPlayerTag->IsPlayer() 
		|| pPlayerTag->IsBuilding())//BUILDING_OBJECT == pPlayerTag->GetCharType() )
        return TRUE;
    //
//#endif
    return FALSE;
	;
}


BOOL VHero::AddOperationMoveTo( TILEPOS x, TILEPOS y )
{
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveTo;
	m_Operation.target.x = x;
	m_Operation.target.y = y;
	return TRUE;
}
BOOL VHero::AddOperationVerifyPos( TILEPOS x, TILEPOS y, DWORD /*dwPeriod*/ )
{
	m_TilePos.x = x;
	m_TilePos.y = y;

    ChangeTilePos(x, y);
	ResetMove( 0xffffffff );
	//m_ActionQueue.SkipAllWalkActionBefore( 0xffffffff );

	if( m_bInWater )
	{
		SwitchSwimIdleAnim(0);
	}
	else
	{
		SwitchIdleAnim(0);
	}
	//
	return TRUE;
}

BOOL VHero::AddOperationPickItem( VCharacter* pItem )
{
	assert( pItem != NULL && "pick item is NULL" );
	//POINT pt = pItem->GetTilePos();
	m_Operation.Clear();
	
	m_Operation.dwType = sOPERATION::eOpMoveGetItem;
	m_Operation.target.stDst = pItem->GetID();
	//m_Operation.dwStartTime = base::GetRunTime();
	//m_Operation.SetStartTime( base::GetRunTime() );
	//m_Operation.dwTimeOut = 0;
	//m_Operation.dwTryTimesLeft = 2;
	//m_Operation.dwTryInterval = 500;
	
	return TRUE;
}

BOOL VHero::AddOperationUseSkill( int tag_npc, int x, int y, int skillid, int skilllevel )
{
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpUseSkill;
	m_Operation.target.x = x;
	m_Operation.target.y = y;
	m_Operation.target.stDst = tag_npc;
	m_Operation.skill.iSkillID = skillid;
	m_Operation.skill.iSkillLevel = skilllevel;

	return TRUE;
}


BOOL VHero::AddOperationGotoAttackTarget( int tag_npc, int x, int y, int iSkillID, int iSkillLevel)
{
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveSkill;
	m_Operation.target.x = x;
	m_Operation.target.y = y;
	m_Operation.target.stDst = tag_npc;
	//
	m_Operation.skill.iSkillID = iSkillID;
	m_Operation.skill.iSkillLevel = iSkillLevel;
    //
	return TRUE;
}


BOOL VHero::AddOperationGotoTalkWithNpc( VCharacter* pNpc )
{
	assert( pNpc != NULL && "target npc is NULL" );
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveTalk;
	m_Operation.target.stDst = pNpc->GetID();
    //
	return TRUE;
}
BOOL VHero::AddOperationIdle( void)
{
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpIdle;
	m_Operation.target.stDst = NULL;
    //
	m_Operation.dwOperBeginTime = base::GetRunTime();
	//
    return FALSE;
}

BOOL VHero::AddOperationGotoPickItem( VCharacter* pNpc )
{
	assert( pNpc != NULL && "target item npc is NULL" );
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveGetItem;
	m_Operation.target.stDst = pNpc->GetID();
    //
    return FALSE;
}
BOOL VHero::AddOperationGotoPickRangeItem()
{
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveGetRangeItem;
	m_Operation.target.stDst = -1;
	//
	return FALSE;
}
BOOL VHero::AddOperationChangeDir( TILEPOS x, TILEPOS y  )
{
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpChangeDir;
	m_Operation.target.stDst = 0;
	m_Operation.target.x = x;
	m_Operation.target.y = y;
	//
	return FALSE;
}
BOOL    VHero::AddOperationGotoDropItem( TILEPOS x, TILEPOS y  )
{ 
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveDropItem;
	m_Operation.target.stDst = 0;
	m_Operation.target.x = x;
	m_Operation.target.y = y;
	//
	return FALSE;
}

BOOL	VHero::AddOperationGotoTrade( VCharacter* pNpc )
{
	assert( pNpc != NULL && "target player npc is NULL" );
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveTrade;
	m_Operation.target.stDst = pNpc->GetID();
    //
    return FALSE;
}
BOOL	VHero::AddOperationFollowNpc( VCharacter* pNpc )
{
	assert( pNpc != NULL && "target player npc is NULL" );
	m_Operation.Clear();
	m_Operation.dwType = sOPERATION::eOpMoveFollow;
	m_Operation.target.stDst = pNpc->GetID();
    //
    return FALSE;
}
BOOL	VHero::AddOperationDoAction(int ActionID)
{
	m_Operation.dwType = sOPERATION::eOpDoAction;
	m_Operation.action.iActionID = ActionID;
	//
	return FALSE;
}




//---
//对npc几个最小操作的定义
//---
BOOL    VHero::_HandleNothing()
{
	AddOperationIdle();
	return FALSE;
}
//待机
BOOL    VHero::_HandleIdle()     
{
    return TRUE;
}



//使用技能
BOOL    VHero::_HandleUseSkill()
{
	//POINT               ptag;
	//POINT               pnow;
    int                 iFirstAttackSkill = -1;
    int                 iFirstAttackSkillLevel = 0;
    //int                 iAttackRange = 0;	
    sVSKILLINFO_BASE *	pSkill;
    //
	iFirstAttackSkill			= m_Operation.skill.iSkillID;
	iFirstAttackSkillLevel	= m_Operation.skill.iSkillLevel;

	//取得技能
	pSkill = theSkillInfoParser.GetVSkillInfo((SLOTCODE)iFirstAttackSkill);


	////如果目标技能不存在，就
	//if (NULL == pSkill)
	//{
	//	AddOperationIdle();
	//	return FALSE;
	//}

    //
    //判定现在可否使用技能 

				//判定是否有攻击目标
				VCharacter* pPlayerTag = (VCharacter*)theVObjectManager.FindByID( m_Operation.target.stDst );
				if (NULL == pPlayerTag)
					return FALSE;

				if ( pPlayerTag->IsDead() )
					return FALSE;

				int nIntonateTime = 0;
				nIntonateTime = pSkill->m_dwPrepareTime;

				if( nIntonateTime > 0 )
				{
					theGameUIManager.UIShowByTime(gameui::eUIProgress
                                               ,base::GetRunTime()
															  ,nIntonateTime
															  ,TRUE
															  ,1
															  ,TRUE
															  ,TRUE
															  ,(LPCSTR)pSkill->m_sName);
				}


				assert(0 && " SwitchIntoateAnim 这里播放技能 iFirstAttackSkill");
#pragma message(__FILE__  "(1512) SwitchIntoateAnim 这里播放技能 iFirstAttackSkill" )

				SwitchIntoateAnim(0);
				AddOperationIdle();

				//发消息通知服务器，开始攻击了

	 return TRUE;
}

//对话
BOOL    VHero::_HandleTalk()
{
	;
	if (IsCanTalk(m_Operation.target.stDst))
	{
		//客户端进入捡道具的状态(动作)
		SetActionStatus( esk_Talk, (DWORD)VCharacterStatusTimer::NoLimitDelay );
	}
    //
    return TRUE;
	;
}
//取得道具
BOOL    VHero::_HandleGetItem()
{
	;
	if (!IsDead())
	{
		//客户端进入捡道具的状态(动作)
		SetActionStatus( esk_PickItem, (DWORD)VCharacterStatusTimer::NoLimitDelay );
	}
    //
    return TRUE;
	;
}
//丢弃道具
BOOL    VHero::_HandleDropItem()
{
	;
	if (!IsDead())
	{
		//客户端进入捡道具的状态(动作)
		SetActionStatus( esk_DropItem, (DWORD)VCharacterStatusTimer::NoLimitDelay );
	}
    //
    return TRUE;
	;
}



//移动到目标地点
BOOL    VHero::_HandleMoveTo()
{
	;
	static DWORD dwUpdateStep = 0;

	if( base::GetRunTime() - dwUpdateStep > 150 )
	{
		dwUpdateStep = base::GetRunTime();
	}
	else
	{
		return FALSE;
	}

    VObject* pPlayerTag;
	//int					iAttackRange = 0;
	 POINT               ptag = {0};
    POINT               pnow = {0};
    BOOL			   ret = TRUE;

	m_bMoving = TRUE;

	pnow = GetTilePos();

    if (IsCanMove())
    {
        if (0 <=  m_Operation.target.stDst)
        {
            //判定是否有攻击目标
            pPlayerTag = theVObjectManager.FindByID(m_Operation.target.stDst);

            if ( NULL == pPlayerTag )
            {
                //如果目标npc不存在，取目标坐标
                ptag.x = m_Operation.target.x;
                ptag.y = m_Operation.target.y;
            }
            else
            {
				//iAttackRange += GetBodySize() / 10;
				//iAttackRange += GetWeaponAttackRange();
				//iAttackRange += pPlayerTag->GetBodySize() / 10;

				//if ( 1 > iAttackRange )
				//{
				//	iAttackRange = 1;
				//}

				//ptag = GetTargetMovePos( GetTilePos(), pPlayerTag->GetTilePos(), iAttackRange );
    //            m_Operation.target.x = ptag.x;
    //            m_Operation.target.y = ptag.y;
            }
        }
        else
        {
            //移动的目标npc
            ptag.x = m_Operation.target.x;
            ptag.y = m_Operation.target.y;
        }
        // 判断目标和但前的位置的距离
        if ((ptag.x == pnow.x) && (ptag.y == pnow.y))
        {
            ret = FALSE;
        }
        else
        {
            ret = MoveToTilePos(ptag.x, ptag.y, &thePathGameMap);
        }
		//
		if (TRUE != ret)
		{
			if (sOPERATION::eOpMoveTo == m_Operation.dwType)
			{
				m_Operation.Clear();
			}
		}
    }
	else
	{
		ret = FALSE;
	}
    //
    return ret;
	;
}

//交易
BOOL    VHero::_HandleTrade()
{
	;
    //
    SetActionStatus( esk_Trade, (DWORD)VCharacterStatusTimer::NoLimitDelay );
    //
    return TRUE;
	;
}

//转身
BOOL    VHero::_HandleChangeDir()
{
	;
    SetActionStatus( esk_ChangeDir, (DWORD)VCharacterStatusTimer::NoLimitDelay );
	//
	return TRUE;
	;
}







//移动过去使用技能
BOOL    VHero::_HandleMoveSkill()
{
	return TRUE;
}

//移动过去对话
BOOL    VHero::_HandleMoveTalk()
{
	;
    const   int         c_talk_dis = 4;
    POINT               pnow;
    POINT               ptag;
    //
    pnow = GetTilePos();
    //
    if (0 <=  m_Operation.target.stDst)
    {
        VObject* pPlayerTag = theVObjectManager.FindByID( m_Operation.target.stDst );
        if (NULL != pPlayerTag)//判定目标道具是否存在
        {
            //取得目标地点
            ptag = pPlayerTag->GetTilePos();
            //判定是否到了可以拿道具的范围了
            if ((abs(ptag.x - pnow.x) >= c_talk_dis) ||
                (abs(ptag.y - pnow.y) >= c_talk_dis)  )
            {
                //在范围外，移动过去
                return (_HandleMoveTo());
            }
            else
            {
                //停止移动
                StopMovePath();
                //到了可以捡道具的距离了，去捡道具
				if( IsMoveStepOver() )
					return _HandleTalk();
            }
            //
            return TRUE;
        }
    }
    //
    return FALSE;
	;
}
//移动过去取得道具
BOOL    VHero::_HandleMovePickItem()
{
	;
    const   int     c_get_item_dis = 2;
    POINT               pnow;
    POINT               ptag;
    //
    pnow = GetTilePos();
    //
    if (0 <=  m_Operation.target.stDst)
    {
        VObject* pPlayerTag = theVObjectManager.FindByID( m_Operation.target.stDst );
        if (NULL != pPlayerTag)//判定目标道具是否存在
        {
            //取得目标地点
            ptag = pPlayerTag->GetTilePos();
            //判定是否到了可以拿道具的范围了
            if ((abs(ptag.x - pnow.x) >= c_get_item_dis) ||
                (abs(ptag.y - pnow.y) >= c_get_item_dis)  )
            {
                //在范围外，移动过去
                return (_HandleMoveTo());
            }
            else
            {
                //停止移动
                StopMovePath();
                //到了可以捡道具的距离了，去捡道具
				//移动停止了才能真的捡
				if( IsMoveStepOver() )
					return _HandleGetItem();
            }
            //
            return TRUE;
        }
    }
    //
    return FALSE;
	;
}
//移动取得周围的道具
BOOL    VHero::_HandlePickItem()
{
	BOOL	bFindItemNpcFlag = FALSE;
	VObject* pObject = theVObjectManager.FindByID(m_Operation.target.stDst);
	if (NULL == pObject)
	{
		bFindItemNpcFlag = TRUE;
	}
	else if (!pObject->IsItem())
	{
		bFindItemNpcFlag = TRUE;
	}
	//
	if (bFindItemNpcFlag)
	{
		POINT ptTilePos = GetTilePos();
		VObject* pItemPlayer = theVObjectManager.FindNearItemNpc(ptTilePos.x, ptTilePos.y, 10);
		if (NULL != pItemPlayer)
		{
			m_Operation.target.stDst = pItemPlayer->GetID();
		}
		else
			m_Operation.target.stDst = -1;
	}
	//寻找周围的一个道具npc
	return _HandleMovePickItem();
}


//移动过去丢弃
BOOL    VHero::_HandleMoveDroptem()
{
	//停止移动
	StopMovePath();
	//到了可以捡道具的距离了，去捡道具
	return _HandleDropItem();
}


//移动过去交易
BOOL    VHero::_HandleMoveTrade()
{
    const   int     c_get_item_dis = 4;
    POINT               pnow;
    POINT               ptag;
    //
    pnow = GetTilePos();
    //
    if (0 <=  m_Operation.target.stDst)
    {
        VCharacter* pPlayerTag = (VCharacter*)theVObjectManager.FindByID( m_Operation.target.stDst );
        if (NULL != pPlayerTag)//判定目标道具是否存在
        {
            //取得目标地点
            ptag = pPlayerTag->GetTilePos();
            //判定是否到了可以拿道具的范围了
            if ((abs(ptag.x - pnow.x) >= c_get_item_dis) ||
                (abs(ptag.y - pnow.y) >= c_get_item_dis)  )
            {
                //在范围外，移动过去
                return (_HandleMoveTo());
            }
            else
            {
                //停止移动
                StopMovePath();
                //到了可以交易的距离了
				if( IsMoveStepOver() )
					return _HandleTrade();
            }
            //
            return TRUE;
        }
    }
    //
    return FALSE;
}

//跟随
BOOL    VHero::_HandleMoveFollow()
{
	;
    //要记得在 m_Operation.target.stDst 纪录需要跟随的npc
    //
    VCharacter*        pPlayerTag;
    POINT           ptag;
    POINT           pnow;
    int             follow_range = 4 + rand() % 5;
    //
    if (0 <=  m_Operation.target.stDst)
    {
        //判定是否有攻击目标
        pPlayerTag = (VCharacter*)theVObjectManager.FindByID(m_Operation.target.stDst);
        if (NULL != pPlayerTag)
        {
            ptag = pPlayerTag->GetTilePos();
            pnow = GetTilePos();
            //
            if ((abs(pnow.x - ptag.x) > follow_range) ||
                (abs(pnow.y - ptag.y) > follow_range)   )
                _HandleMoveTo();
        }
        else
        {
            //如果目标npc不存在，移动到目标点
            _HandleMoveTo();
        }
        //
    }
    else
    {
        //如果目标npc不存在，移动到目标点
        _HandleMoveTo();
    }
    return TRUE;
	;
}

BOOL    VHero::_HandleDoAction()
{
	// 
	if (IsCanDoExpression())//判断可否作表情动作
	{


		//客户端自己进入了一段的硬直等待时间,等待服务器发回来的确认攻击命令
		DWORD dwPreDealy = theVObjectManager.GetGamePing() + 3000;
		SetActionStatus(esk_PreAttack, dwPreDealy);
		//
		AddOperationIdle();
	}
	//
	return TRUE;
}

//作莫个动作
BOOL    VHero::_HandleVerifyPos()
{
	return TRUE;
}





void   VHero::_DoStatusPickItem()
{
    TryPickItem();
    //
    //捡完道具改变状态到Idle
    SetActionStatus( esk_Idle, (DWORD)VCharacterStatusTimer::NoLimitDelay );
    //清空行动路径
    StopMovePath();
    //改变AI到idle...(我不想加在这里，，可以没有办法啊。。:S)..麻烦啊。。
	if (sOPERATION::eOpMoveGetRangeItem != m_Operation.dwType)//在pick很多道具的时候不能。pick一个就停下来..
		AddOperationIdle();
}
//	丢弃道具
void VHero::_DoStatusDropItem()
{
}
//  对话
void VHero::_DoStatusTalk()
{
    //
    TryTalkWithNpc();
    //
    SetActionStatus( esk_Idle, (DWORD)VCharacterStatusTimer::NoLimitDelay );
    //清空行动路径
    StopMovePath();
    //改变AI到idle...(我不想加在这里，，可以没有办法啊。。:S)
    AddOperationIdle();
}

//  交易
void  VHero::_DoStatusTrade()
{
    //
    TryTradeWithNpc();
    //捡完道具改变状态到Idle
    SetActionStatus(esk_Idle, (DWORD)VCharacterStatusTimer::NoLimitDelay);
    //清空行动路径
    StopMovePath();

    //改变AI到idle...(我不想加在这里，，可以没有办法啊。。:S)
    AddOperationIdle();
}

//转身
void    VHero::_DoStatusChangeDir()
{
	float	dir;
	dir = atan2f((float)(m_Operation.target.y- m_TilePos.y), (float)(m_Operation.target.x-m_TilePos.x));

	SetDirection(dir);
	//发消息通知。其他人转身了



	//捡完道具改变状态到Idle
	SetActionStatus( esk_Idle, (DWORD)VCharacterStatusTimer::NoLimitDelay );
	//
	AddOperationIdle();
}


BOOL VHero::CanSwitchAnim()
{
	if( g_InputData.m_IsJumping || m_bDead )
		return FALSE;
	return TRUE;
}

BOOL  VHero::CanCountermarchAnim()
{
	if( g_InputData.m_HeroCounterMarch || m_bCountermarch )
		return TRUE;
	return FALSE;
}

void VHero::StopMoving()
{
	g_InputData.m_TargetPos.x = m_vPosition.x;
	g_InputData.m_TargetPos.y = m_vPosition.y;
	g_InputData.m_TargetPos.z = 0;
	g_InputData.m_MovingByDir = FALSE;
	m_bMoving = FALSE;
	m_bLastFrameMoving = m_bMoving;


	theGameUIManager.SetMovePointer(FALSE);
}


void VHero::SwitchWalkAnim(DWORD /*dwTimeOut*/)
{
	//_SUPER::SwitchWalkAnim(dwTimeOut) 不调用超类
	if( g_InputData.m_IsJumping || IsDead() || m_bMounting )
		return;

	DWORD dwTimePerLoop = 0;
	m_fCurMoveRate = GetMoveRate()/2;
	dwTimePerLoop = (DWORD)((m_fMovingStep/m_fCurMoveRate)*1000);

	if( m_bMounting )
	{
		m_pAnim->PlayLowerAnim(	"mountwalk",
			PLAYMODE_NORMAL,
			0,
			MAX_ANIMATION_LOOP,
			ANIMATION_TIMEOUT,
			ANIMATION_BLENDING_TIME,
			"mountwalk",
			PLAYMODE_NORMAL );

		m_pMountAnim->PlayAnimByActionName( "walk",
			PLAYMODE_NORMAL,
			dwTimePerLoop,
			MAX_ANIMATION_LOOP,
			ANIMATION_TIMEOUT,
			"idle",
			PLAYMODE_NORMAL );

		//m_pMountAnim->PlayLowerAnim( "run",
		//	PLAYMODE_NORMAL,
		//	dwTimePerLoop,
		//	MAX_ANIMATION_LOOP,
		//	ANIMATION_TIMEOUT,
		//	ANIMATION_BLENDING_TIME,
		//	"idle",
		//	PLAYMODE_NORMAL );
	}
	else
	{
		m_pAnim->PlayLowerAnim(	"walk",
			PLAYMODE_NORMAL,
			dwTimePerLoop,
			MAX_ANIMATION_LOOP,
			ANIMATION_TIMEOUT,
			ANIMATION_BLENDING_TIME,
			"idle",
			PLAYMODE_NORMAL );
	}
}

//切换到跑步的动作
void VHero::SwitchRunAnim(DWORD dwTimeOut)
{
	if( g_InputData.m_IsJumping || m_bDead )
		return;
	_SUPER::SwitchRunAnim(dwTimeOut);
}

// 冲锋动作
void VHero::SwitchAssaultAnim(DWORD dwTimeOut)
{
	//LOGINFO("VHero::SwitchAssaultAnim\n");
	if( g_InputData.m_IsJumping || m_bDead )
		return;
	_SUPER::SwitchAssaultAnim(dwTimeOut);
}

void VHero::SwitchIdleAnim(DWORD dwTimeOut)
{
	if( g_InputData.m_IsJumping || m_bDead )
		return;
	_SUPER::SwitchIdleAnim(dwTimeOut);
}


void VHero::SwitchJumpAnim(DWORD dwTimeOut)
{
	if( IsDead() )
		return;

	m_fStartJumpingTerrainZ = m_vPosition.z;
	m_dwStartStopMovingTime = g_CurTime;

	if( g_InputData.m_IsSlipJumping )
	{
		m_pAnim->PlayLowerAnim(	"glide",
			PLAYMODE_NORMAL,
			0,
			MAX_ANIMATION_LOOP,
			ANIMATION_TIMEOUT,
			ANIMATION_BLENDING_TIME,
			"glide",
			PLAYMODE_NORMAL );

		return;
	}

	_SUPER::SwitchJumpAnim(dwTimeOut);

}




void VHero::_DoRotation(float fCurRotateDistance)
{
	/// 转身处理，分两类
	///1.移动时转向
	if( m_bMoving )
	{
		//LOGINFO	("VHero::_DoRotation Move:%d  Assault:%d\n"
		//			,m_bMoving
		//			,m_bAssaultMoving);
		m_bRotateLowerBody = FALSE;
		m_bRotateLowerBodyStart = FALSE;

		//////////////1
		ObjectUtil::RotateBody( &m_fLowerBodyDir, &m_fTargetLowerBodyDir, fCurRotateDistance*4.0f,	cPI );
		ObjectUtil::RotateBody( &m_fUpperBodyDir, &m_fCurrentDir,						fCurRotateDistance*1.0f,	cPI*0.3f );
		ObjectUtil::RotateBody( &m_fUpperBodyDir, &m_fLowerBodyDir,			0,									cPI*0.2f );

		//////////////////////2
		//m_fLowerBodyDir = m_fTargetLowerBodyDir;
		//m_fUpperBodyDir = m_fTargetLowerBodyDir;
	}
	///2.原地转身，这两种情况，如果有角度差，都播放Shuffle动画
	//		a.鼠标拖动导致转身
	//		b.设置角度后，自动转身 
	else
	{

		//		b.设置角度后，因角度差，自动转身 
		{
			ObjectUtil::RotateBody( &m_fUpperBodyDir, &m_fCurrentDir, fCurRotateDistance*0.6f, cPI*0.3f );
			ObjectUtil::RotateBody( &m_fUpperBodyDir, &m_fLowerBodyDir, 0, cPI*0.2f );

			float fUpperLowerBodyAngle = 0;

			if( !m_bRotateLowerBody )
			{
				fUpperLowerBodyAngle = ObjectUtil::CalcRotateAngle( m_fLowerBodyDir, m_fCurrentDir);

				if( fUpperLowerBodyAngle > ROTATECORRECT )
				{
					if( !m_bRotateLowerBodyStart )
					{
						m_bRotateLowerBodyStart = TRUE;
						m_dwRotateLowerBodyStartTime = g_CurTime;
					}
					fUpperLowerBodyAngle += (float)( g_CurTime - m_dwRotateLowerBodyStartTime )*0.0005f;
				}

				if( fUpperLowerBodyAngle > cPI*0.2f )
				{
					m_bRotateLowerBody = TRUE;
					if( !m_bInWater && !m_bIntonating && !HasFightFlag( FIGHTFLAG_FIGHTING ) )
					{
						if( ObjectUtil::CalcRotateAngleWithDir( m_fLowerBodyDir, m_fUpperBodyDir ) > 0 )
							SwitchShuffleRightAnim();
						else
							SwitchShuffleLeftAnim();
					}
				}
			}//if( !m_bRotateLowerBody )

			if( m_bRotateLowerBody )
			{
				ObjectUtil::RotateBody( &m_fLowerBodyDir, &m_fUpperBodyDir, fCurRotateDistance*0.6f, cPI*0.2f );
				fUpperLowerBodyAngle = ObjectUtil::CalcRotateAngle( m_fLowerBodyDir, m_fCurrentDir);

				if( fUpperLowerBodyAngle < ROTATECORRECT )
				{
					m_bRotateLowerBody = FALSE;
					m_bRotateLowerBodyStart = FALSE;

					if( !m_bIntonating )
					{
						if( m_bInWater )
						{
							SwitchSwimIdleAnim(0);
						}
						else
						{
							SwitchIdleAnim(0);
						}
					}
				}
			}//if( m_bRotateLowerBody )
		}//else if( g_bMouseRButtonIsDown )
	}//if m_bMoving
}

float  VHero::_GetUpdatePosZ(FLOAT fRoleHeight)
{
	DWORD dwLoadFlags = 0;
	return theGameWorld.GetHeightAt( m_vPosition.z+fRoleHeight,
								20.0f*SCALE_MESH, 
								m_vPosition.x,
								m_vPosition.y,
								0,
								&m_bInFloor,
								&dwLoadFlags,
								TRUE,
								(Vector3*)&m_vTargetNormal );
}


void VHero::_CalcStepCost(float& /*fCost*/)
{
	//Hero不处理
}

void VHero::_OnNextStep()
{
	//if (IsHero())//只有是主角才需要发送消息
	// 如果正处于位置校正状态，则不发送消息
	if( m_Operation.dwType != sOPERATION::eOpVerifyPos )
	{

	}
}

BOOL VHero::IsInHeroRange(float /*fRadius*/)
{
	//不处理
	return TRUE;
}

void	VHero::_UpdateMountAnim(DWORD dwTime)
{
	if(! m_bMounting )
		return;


	if( m_bJumping )
		{
			m_pMountAnim->SetRotationX( 0 );
		}
		else
		{
			float fRoleHeight = 80.0f*SCALE_MESH;
			float fFrontWorldZ = 0.0f;
			float fBehindWorldZ = 0.0f;
			DWORD dwLoadFlags = 0;
			float fLowerDirX;
			float fLowerDirY;

			RenderUtil::GetAngleTo2D( m_fLowerBodyDir, &fLowerDirX, &fLowerDirY );
			fFrontWorldZ  = theGameWorld.GetHeightAt( m_vPosition.z+fRoleHeight, 38.2f*SCALE_MESH, m_vPosition.x + fLowerDirX, m_vPosition.y + fLowerDirY, 0, NULL, &dwLoadFlags, FALSE );
			fBehindWorldZ = theGameWorld.GetHeightAt( m_vPosition.z+fRoleHeight, 38.2f*SCALE_MESH, m_vPosition.x - fLowerDirX, m_vPosition.y - fLowerDirY, 0, NULL, &dwLoadFlags, FALSE );
			m_pMountAnim->SetRotationX( atan2f(fBehindWorldZ-fFrontWorldZ,2) );
		}

	_SUPER::_UpdateMountAnim(dwTime);
}



BOOL  VHero::_PrevIntoWater()
{
	g_InputData.m_IsJumping			= FALSE;
	g_InputData.m_IsSlipJumping	= FALSE;
	return TRUE;
}


void   VHero::_OnIntoWater	()
{
	if( m_bMoving )
	{
		if(g_InputData.m_HeroCounterMarch )
		{
			SwitchSwimIdleAnim();
			return;
		}
	}
	_SUPER::_OnIntoWater(); 
}

void   VHero::_OnOutofWater()
{
	_SUPER::_OnOutofWater();
}

void VHero::_UpdateJumping( DWORD /*dwTick*/ )
{
	//_SUPER::_UpdateJumping( dwTick )  不调用父类
	if(m_bDead)
		return;

		DWORD dwToHighestTime = (DWORD)(m_fStartJumpingSpeed/theApplicationSetting.m_Gravitation);//跳到最高点用的时间
		DWORD dwJumpingInTheSkyTime = 0;//跳跃时，在空中飞行的时间

		if( g_CurTime > g_InputData.m_JumpStartTime )
		{
			dwJumpingInTheSkyTime = g_CurTime - g_InputData.m_JumpStartTime;
			m_fJumpingHeight = m_fStartJumpingSpeed*dwJumpingInTheSkyTime - theApplicationSetting.m_Gravitation*dwJumpingInTheSkyTime*dwJumpingInTheSkyTime/2;
		}
		else
		{
			m_fJumpingHeight = 0;
			m_fStartJumpingTerrainZ = m_vPosition.z;
		}

		if(	g_InputData.m_IsJumping 
			&& g_CurTime > g_InputData.m_JumpStartTime )
		{
			m_vPosition.z = m_fStartJumpingTerrainZ + m_fJumpingHeight;

			if( dwJumpingInTheSkyTime > dwToHighestTime || m_vPosition.z - m_fTerrainZ < THEROLEANDTERRAINOFFZLIMIT  )
			{
				float a = theApplicationSetting.m_Gravitation/2;
				float b = -m_fStartJumpingSpeed;
				float c = m_fTerrainZ - m_fStartJumpingTerrainZ;

				DWORD dwDropToTerrainTime = (DWORD)(( -b + sqrt( b*b - 4*a*c ))/( 2*a ) - dwJumpingInTheSkyTime);

				if( !m_bNeedDropBuf && !m_bPlayedDropAnim && dwDropToTerrainTime > 200 )
				{
					m_bNeedDropBuf = TRUE;
				}

				if( m_bNeedDropBuf && dwDropToTerrainTime < 200 )
				{
					//if( !g_InputData.m_IsSlipJumping )
					{
						SwitchDropAnim();
						m_bPlayedDropAnim = TRUE;
					}
					m_bNeedDropBuf = FALSE;
				}

				if( m_vPosition.z < m_fTerrainZ )
				{
					m_fJumpingHeight = 0;
					m_vPosition.z = m_fTerrainZ;
					g_InputData.m_IsJumping = FALSE;
					g_InputData.m_IsSlipJumping = FALSE;
					m_bNeedDropBuf = FALSE;
					m_bJumping = FALSE;

					if( !m_bPlayedDropAnim )
					{
						if( m_bMoving )
						{
							if( g_InputData.m_HeroCounterMarch )
							{
								//if( m_bInWater )
								//{
								//	SwitchSwimIdleAnim();
								//}
								//else
								{
									SwitchWalkBackWardsAnim();
								}
							}
							else
							{
								//if( m_bInWater )
								//{
								//	SwitchSwimAnim();
								//}
								//else
								{
									SwitchRunAnim();
								}
							}
						}
						else
						{
							//if( m_bInWater )
							//{
							//	SwitchSwimIdleAnim();
							//}
							//else
							{
								SwitchIdleAnim();
							}
						}
					}
					m_bPlayedDropAnim = FALSE;
				}
			}
		}
		else
		{
			m_vPosition.z = m_fTerrainZ;
		}
}

void VHero::_UpdateFootStep( DWORD /*dwTick*/ )
{
	//不处理父类
	if(m_bMounting)
		return;

	static BOOL bPlayedLeftSound = FALSE;
	static BOOL bPlayedRightSound = FALSE;
	const char* szLowerString = m_pAnim->GetCurLowerAnim();

	if( szLowerString == NULL || szLowerString[0] == 0)
		return;


	int nLen = strlen(szLowerString);

	if( ( nLen == 18 && stricmp( &szLowerString[0], "falltothegroundrun" ) == 0 ) )
	{
		float fLowerFactor = m_pAnim->GetLowerBodyFactor();
		if( m_pAnim->IsLowerBodyBlending() )
		{
			bPlayedLeftSound = TRUE;
			bPlayedRightSound = TRUE;
			fLowerFactor = 1;
		}

		if( fLowerFactor > 0.75 && !bPlayedLeftSound )
		{

			theSoundEffect.PlayUI(SOUND_STEP_LEFT);
			bPlayedLeftSound = TRUE;
		}

		if( fLowerFactor < 0.75 )
		{
			bPlayedLeftSound = FALSE;
		}

		if( fLowerFactor > 0.8667 && !bPlayedRightSound )
		{

			theSoundEffect.PlayUI(SOUND_STEP_RIGHT);
			bPlayedRightSound = TRUE;
		}


		if( fLowerFactor < 0.8667 )
		{
			bPlayedRightSound = FALSE;
		}
	}

	if( ( nLen == 3 && stricmp( &szLowerString[0], "run" ) == 0 )
		|| ( nLen == 13 && stricmp( &szLowerString[0], "walkbackwards" ) == 0 ) )
	{
		float fLowerFactor = m_pAnim->GetLowerBodyFactor();
		if( m_pAnim->IsLowerBodyBlending() )
		{
			bPlayedLeftSound = TRUE;
			bPlayedRightSound = TRUE;
			fLowerFactor = 1;
		}

		if( fLowerFactor > 0.25 && !bPlayedLeftSound )
		{

			theSoundEffect.PlayUI(SOUND_STEP_LEFT);
			bPlayedLeftSound = TRUE;
		}

		if( fLowerFactor < 0.25 )
		{
			bPlayedLeftSound = FALSE;
		}

		if( fLowerFactor > 0.75 && !bPlayedRightSound )
		{

			theSoundEffect.PlayUI(SOUND_STEP_RIGHT);
			bPlayedRightSound = TRUE;
		}

		if( fLowerFactor < 0.75 )
		{
			bPlayedRightSound = FALSE;
		}
	}
}


void VHero::_UpdateWalkingOnLand( DWORD /*dwTick*/ )
{
	//不处理父类
	if( g_InputData.m_IsJumping || m_bInWater )
		return;

	Vector3D vAxis(0,0,1);
	Vector3D vUp( 0, 0, 1 );

	//char szBuf[256];
	//_snprintf( szBuf, 256, "Normal x %f y %f z %f", m_vTargetNormal.x, m_vTargetNormal.y, m_vTargetNormal.z );
	//OutputDebugString( szBuf );

	if( !m_bInWater && !m_bJumping && theGameWorld.GetHeroPlotMap() )
	{
		if( m_bInFloor )
			vAxis = -m_vTargetNormal;
		else
			vAxis = m_vTargetNormal;
		//vAxis = theGameWorld.GetNormal( m_vPosition.x, m_vPosition.y );
	}

	vAxis.Normalize();


	float fDot = vUp.DotProduct(vAxis );
	float fTheta = acosf( fDot );

	//if( fTheta > theApplicationSetting.m_HeroSlipJumpingAngle /*&& ( vAxis.x != 0 || vAxis.y != 0 )*/ && g_CurTime > g_InputData.m_SlipStartTime + 600 )
	if( fTheta > theApplicationSetting.m_HeroSlipJumpingAngle && ( vAxis.x != 0 || vAxis.y != 0 ) )
	{
		if( m_bInFloor )
		{
			if( g_CurTime > g_InputData.m_SlipStartTime + 600 )
			{
				 //OutputDebugString( "slip" );
				 g_InputData.m_SlipStartTime	= g_CurTime;
				 g_InputData.m_IsSlipJumping	= TRUE;
				 g_InputData.m_SlipJumpingDir = vAxis;
				 g_InputData.m_IsJumping = TRUE;
				 g_InputData.m_JumpingDir = vAxis;
				 g_InputData.m_JumpingRotate = g_InputData.m_RotateZCorrect;
				 g_InputData.m_JumpStartTime = g_CurTime + 61;
				 m_fJumpingHeight = 0;
				 m_fStartJumpingTerrainZ = m_vPosition.z;
				 m_bJumping = TRUE;
				 m_dwStartJumpingTime = g_CurTime + 61;
			}
		}
		else
		{
			if( g_CurTime > g_InputData.m_SlipStartTime + 600 )
			{
				//OutputDebugString( "slip" );
				g_InputData.m_SlipStartTime = g_CurTime;
				g_InputData.m_IsSlipJumping = TRUE;
				g_InputData.m_SlipJumpingDir = vAxis;
				g_InputData.m_IsJumping = TRUE;
				g_InputData.m_JumpingDir = vAxis;
				g_InputData.m_JumpingRotate = g_InputData.m_RotateZCorrect;
				g_InputData.m_JumpStartTime = g_CurTime + 61;
				m_fJumpingHeight = 0;
				m_fStartJumpingTerrainZ = m_vPosition.z;
				m_bJumping = TRUE;
				m_dwStartJumpingTime = g_CurTime + 61;
			}
		}
	}
	else
	{
		if( m_bInFloor )
			g_InputData.m_SlipStartTime = g_CurTime;
	}
	//char szBuf[256];
	//_snprintf( szBuf, 256, "fTheta %f %d", fTheta*180/cPI, g_CurTime - g_InputData.m_SlipStartTime );
	//OutputDebugString( szBuf );
}

void VHero::_OnMountMonster(BOOL bMountUp)
{
	_SUPER::_OnMountMonster(bMountUp);
	if(bMountUp)
	{
		if( !HasFightFlag(FIGHTFLAG_FIGHTING) )
			theVHeroActionInput.SendDrawInWeapon();
	}
	else
	{
	}
}



BOOL VHero::_DoStatusMove()
{
	//__BOOL_SUPER(_DoStatusMove());  不调用父类
	if( IsDead() )
		return FALSE;

	if( m_bAssaultMoving )
	{
		AssaultMoving( m_shAssaultTargetID );
	   return TRUE; 
	}

	static float fRotateX = g_InputData.m_RotateZCorrect;
	float			fDir = 0;
	Vector3 vNextFramePos;


	//////////////////////////////////////////
	//获取角度
	fRotateX = g_InputData.m_RotateZCorrect + g_InputData.m_DirectionCorrect;
	fRotateX = DegreeInPI_TWO(fRotateX);


	///////////////////////////////////////////
	//静止处理
	if( !m_bMoving && !g_InputData.m_MouseCtrlMode )
	{
		static float fLastDir = 0.0f;
		static BOOL	bChangeDir = FALSE;
		static DWORD dwStartChangeDirTime = 0;

		bChangeDir	= FALSE;
		fDir			= -fRotateX - cPI_V2;
		fDir			= DegreeInPI_TWO(fDir);
		if( fDir > cPI )
			fDir = -cPI_TWO + fDir;

		SetDirection		( fDir );
		SetDirectionLower	( fDir );
	}//if( !m_bMoving && !g_InputData.m_MouseCtrlMode )


	//////////////////////////////////
	//更新跳速度
	if( g_InputData.m_IsJumping )
		g_InputData.m_TargetDirection = g_InputData.m_JumpingDir;

	if( m_bMoving )
	{
		if(  !g_InputData.m_IsJumping )
		{
			if( m_bMounting )
				m_fJumpSpeed = GetMoveRate();
			else
				m_fJumpSpeed = GetMoveRate();
		}
	}
	else
	{
		if( !g_InputData.m_IsJumping )
			m_fJumpSpeed = 0.0f;

		if( g_InputData.m_IsSlipJumping )
			m_fJumpSpeed = GetMoveRate();
	}

	RenderUtil::GetAngleTo2D( m_fCurrentDir, &m_TurnDirection.x, &m_TurnDirection.y );


	////////////////////////////
	//确定移动类型
	{
		int x = (INT)g_InputData.m_TargetDirection.x;
		int y = (INT)g_InputData.m_TargetDirection.y;

		// 向前移动
		if( x == 0 && y < 0 )	m_shCurMovingType = eMovingForward;
		// 向右前移动
		if( x > 0 && y < 0 )		m_shCurMovingType = eMovingForwardRight;
		// 向右移动
		if( x > 0 && y == 0 )	m_shCurMovingType = eMovingRight;
		// 向右后退移动
		if( x > 0 && y > 0 )		m_shCurMovingType = eMovingCountermarchRight;
		// 后退移动
		if( x == 0 && y > 0 )	m_shCurMovingType = eMovingCountermarch;
		// 向左后退移动
		if( x < 0 && y > 0 )		m_shCurMovingType = eMovingCountermarchLeft;
		// 向左移动
		if( x < 0 && y == 0 )	m_shCurMovingType = eMovingLeft;
		// 向左前移动
		if( x < 0 && y < 0 )		m_shCurMovingType = eMovingForwardLeft;
	}


	/////////////////////////////////////
	//移动处理核心...
	if( m_bMoving || g_InputData.m_IsJumping )
	{
		if(m_pPathHandler)
		{
			vNextFramePos = m_pPathHandler->GetPos();
		}
		else
		{
			vNextFramePos.x = m_vPosition.x;
			vNextFramePos.y = m_vPosition.y;
			vNextFramePos.z = 0;
		}

		if( g_InputData.m_IsJumping )
		{
			if( g_InputData.m_IsSlipJumping )
				m_fStartJumpingSpeed = 0.0001f;
			else
				m_fStartJumpingSpeed = m_fMaxStartJumpingSpeed;
		}



		////////////////////////////////////
		//自动移动处理
		if(	g_InputData.m_AutoRunForward 
			|| g_InputData.m_IsSlipJumping 
			|| ( ( g_InputData.m_MovingByDir || g_InputData.m_IsJumping ) && !g_InputData.m_MouseCtrlMode)   )
		{
			_UpdateAutoMoving( vNextFramePos,fRotateX);
		}
				//////////////////////////////////////////////////////
		else	//向目标移动处理..............
		{
			_UpdateMovingGoal( vNextFramePos);
		}


		BOOL bCollision(FALSE);
		bCollision = _CheckMovingCollision(vNextFramePos);

		///////////////////////////////////////////
		//所有碰撞检测过后，可以变换位置
		_ApplyMovingData(bCollision, vNextFramePos);

		theGameUIManager.UIRefresh(gameui::eUIMiniMap);

	}//if( m_bMoving || g_InputData.m_IsJumping )

	//////////////////////////////////
	//切换人物状态....
	_UpdateMovingState();

   return _On_DoStatusMove(); 
}


void VHero::_UpdateAutoMoving(Vector3& vNextFramePos, float fRotateX)
{
	float		fCurMoveDist;
	float		fDir;
	//float		fTargetDistance;
	float		fPosXOff;
	float		fPosYOff;
	float		fxtoy;

	//////////////////////////////////////
	if(m_pPathHandler == NULL)
	{
		if( g_InputData.m_IsJumping )
			fDir = atan2f(g_InputData.m_JumpingDir.y,g_InputData.m_JumpingDir.x) - g_InputData.m_JumpingRotate;
		else
			fDir = atan2f(g_InputData.m_TargetDirection.y,g_InputData.m_TargetDirection.x)   - fRotateX;

		//////////////////////////////////////
		//获取速率
		if( g_InputData.m_IsJumping )
		{
			if( g_InputData.m_IsSlipJumping )
				fCurMoveDist = m_fJumpSpeed*g_FrameCostSmooth/1250;
			else
				fCurMoveDist = m_fJumpSpeed*g_FrameCostSmooth/1000;
		}
		else
		{
			if( g_InputData.m_HeroCounterMarch )
				fCurMoveDist = GetMoveRate()*g_FrameCostSmooth/2000;
			else
				fCurMoveDist = GetMoveRate()*g_FrameCostSmooth/1000;
		}


		//计算位移量
		fxtoy = tan(fDir);

		if( fxtoy == 0 )
		{
			fPosXOff = fCurMoveDist;
		}
		else
		{
			if( g_InputData.m_HeroCounterMarch || base::GetRunTime() - GetStartSlipTime() > 382 )
			{
				fPosXOff = sqrtf(fCurMoveDist*fCurMoveDist/(1+fxtoy*fxtoy));
				fPosYOff = fabs(fxtoy*fPosXOff);
			}
			else
			{
				float a = atan2f(100,0)-fRotateX;
				float b = tan(a);
				fPosXOff = -sqrtf(fCurMoveDist*fCurMoveDist/(1+b*b));
				fPosYOff = -fabs(b*fPosXOff);
			}

			fDir = DegreeInPI_TWO(fDir);

			/////////////////////////////////////////////////////////
			//计算vNextFramePos
			if( g_InputData.m_IsSlipJumping )
			{
				vNextFramePos.x += g_InputData.m_SlipJumpingDir.x * fCurMoveDist;
				vNextFramePos.y += g_InputData.m_SlipJumpingDir.y * fCurMoveDist;
			}
			else
			{
				//0-90
				if( ( fDir >= 0 && fDir <= cPI/2 ))// || ( fDir >= -cPI*2 && fDir <= -cPI*3/2 ) )
				{
					vNextFramePos.x += fPosXOff;
					vNextFramePos.y += fPosYOff;
				}
				if( ( fDir >= cPI/2 && fDir <= cPI) )// || ( fDir >= -cPI*3/2 && fDir <= -cPI ) )
				{
					vNextFramePos.x -= fPosXOff;
					vNextFramePos.y += fPosYOff;
				}
				if( ( fDir >= cPI && fDir <= cPI*3/2 ))// || ( fDir >= -cPI && fDir <= -cPI/2 ) )
				{
					vNextFramePos.x -= fPosXOff;
					vNextFramePos.y -= fPosYOff;
				}
				if( ( fDir >= cPI*3/2 && fDir <= cPI*2 ))// || ( fDir >= -cPI/2 && fDir <= 0 ) )
				{
					vNextFramePos.x += fPosXOff;
					vNextFramePos.y -= fPosYOff;
				}
			}//else
		}
	}

	//g_InputData.m_TargetPos.x = vNextFramePos.x;
	//g_InputData.m_TargetPos.y = vNextFramePos.y;
	//if( g_InputData.m_IsJumping )
	//	fDir = atan2f(g_InputData.m_JumpingDir.y-3,g_InputData.m_JumpingDir.x)-g_InputData.m_JumpingRotate;
	//else
	fDir = atan2f(g_InputData.m_TargetDirection.y-3,g_InputData.m_TargetDirection.x)-fRotateX;

	if( fDir > cPI )		fDir = -cPI_TWO + fDir;
	if( fDir < 0 )			fDir += cPI*2;

	if( !g_InputData.m_HeroCounterMarch )
	{
		SetDirection( -fRotateX - cPI*0.5f );
		SetDirectionLower( fDir );
	}
	else
	{
		if( fDir < 0 )
			SetDirectionLower( fDir + cPI );
		else
			SetDirectionLower( fDir - cPI );

		SetDirection( -fRotateX - cPI*0.5f );

		if( g_InputData.m_IsJumping && g_InputData.m_TargetDirection.y < 0 )
			SetDirectionLower( fDir );
	}


	if( m_bMounting )
		m_fCurrentDir = m_fTargetDir;

	//fPosXOff = vNextFramePos.x - m_vPosition.x;
	//fPosYOff = vNextFramePos.y - m_vPosition.y;
	//float fMovingDir = atan2f( fPosYOff, fPosXOff );
	//m_shCurMovingType = GetMovingType( m_fTargetDir, fMovingDir );

}

void VHero::_UpdateMovingGoal(Vector3& vNextFramePos)
{
	float		fCurMoveDist;
	float		fDir;
	float		fTargetDistance;
	float		fPosXOff;
	float		fPosYOff;

	fCurMoveDist		= m_fCurMoveRate * g_FrameCostSmooth/1000;
	fPosXOff				= g_InputData.m_TargetPos.x - vNextFramePos.x;
	fPosYOff				= g_InputData.m_TargetPos.y - vNextFramePos.y;
	fTargetDistance	= sqrt(fPosXOff*fPosXOff + fPosYOff*fPosYOff) ;

	//判断是否到达目标点
	if( fCurMoveDist < fTargetDistance )
	{
		if(m_pPathHandler == NULL)
		{
			vNextFramePos.x	= vNextFramePos.x + fCurMoveDist/fTargetDistance* fPosXOff ;
			vNextFramePos.y	= vNextFramePos.y + fCurMoveDist/fTargetDistance* fPosYOff ;
		}

		if(!m_bLookAtTarget)
		{
			fPosXOff		= vNextFramePos.x - m_vPosition.x;
			fPosYOff		= vNextFramePos.y - m_vPosition.y;
			fDir			= atan2f(fPosYOff, fPosXOff);
		}
		else
		{
			fDir		= atan2f(fPosYOff, fPosXOff);
		}

		//后退时，转身180
		if(m_pPathHandler && m_pPathHandler->IsBackOff())
		{
			fDir += MATH_PI;
		}

		SetDirection		(fDir);
		SetDirectionLower	(fDir);
	}
	else
	{

		theGameUIManager.SetMovePointer(FALSE);

		vNextFramePos.x = g_InputData.m_TargetPos.x;
		vNextFramePos.y = g_InputData.m_TargetPos.y;
		m_bMoving = FALSE;
	}
}

BOOL VHero::_CheckMovingCollision(const Vector3& vNextFramePos)
{
	//////////////////////////////////////////////
	//1.检测前方的高度差，判断能否前进

	FLOAT fRoleHeight = 80.0f*SCALE_MESH;

	BOOL  bRole			= IsHero();
	BOOL	bCollision	= FALSE;
	DWORD dwLoadFlags = 0;

	//获取当前位置高度
	float fWorldZ = theGameWorld.GetHeightAt( m_vPosition.z + fRoleHeight,
										38.2f*SCALE_MESH, 
										vNextFramePos.x,
										vNextFramePos.y,
										0, 
										NULL,
										&dwLoadFlags,
										bRole );

	if( dwLoadFlags == v3d::SCENE_NO_INTERSECT_TERRAIN )
	{
		OutputDebugString("前方没有地图,等待\n");
		bCollision = TRUE;
	}

	float fHeadHeight			= GetMeshHeight(0);//m_pAnim->GetWorldBBox().v[4].z - m_pAnim->GetWorldBBox().v[0].z;
	float	fDistToCollision	= 0.0f;


	//获取前进方向高度
	Vector3D		vStart = GetPosition();
	Vector3D		vDir(0,0,0);

	vDir.x = vNextFramePos.x - m_vPosition.x;
	vDir.y = vNextFramePos.y - m_vPosition.y;

	vDir.Normalize();

	vStart.z = vStart.z + theApplicationSetting.m_CharCollisionHeight*SCALE_MESH;

	//if( !g_InputData.m_IsJumping )
	{
		float fFarWorldZ = 0.0f;
		DWORD dwLoadFlags = 0;
		BOOL  bInFloor = FALSE;
		fFarWorldZ = theGameWorld.GetHeightAt	(m_vPosition.z+fRoleHeight
                                             ,38.2f*SCALE_MESH
															,m_vPosition.x + vDir.x
															,m_vPosition.y + vDir.y
															,0
															,&bInFloor
															,&dwLoadFlags
															,bRole);


		//比较高差，以下情况视为 Z过高的碰撞
		//	a.非水下时，
		float fOffZ  = fFarWorldZ - m_fTerrainZ;
		if( !m_bInWater && fOffZ >  MOVE_HEIGHT_GAP3 * SCALE_MESH )
			bCollision = TRUE;

		//下滑碰撞处理...
		//HeroSlipJumpingAngle
		if(	!g_InputData.m_IsJumping 
			&& fOffZ < -MOVE_HEIGHT_GAP2 * SCALE_MESH 
			&& !m_bInWater 
			&& !m_bInFloor 
			&& theApplicationSetting.m_HeroSlipJumpingAngle < ANGLE_AUTO_JUMP_MAX)
		{
			g_InputData.m_SlipStartTime		= g_CurTime;
			g_InputData.m_IsSlipJumping		= TRUE;
			g_InputData.m_SlipJumpingDir		= vDir;
			g_InputData.m_IsJumping				= TRUE;
			g_InputData.m_JumpingDir			= vDir;
			g_InputData.m_JumpingRotate		= g_InputData.m_RotateZCorrect;
			g_InputData.m_JumpStartTime			= g_CurTime + 61;
			m_fJumpingHeight				= 0;
			m_fStartJumpingTerrainZ		= m_vPosition.z;
			bCollision						= TRUE;
			//SwitchJumpAnim();
		}
		m_fZBackup = fWorldZ;
	}

	if(m_pPathHandler == NULL)
	{
		//2.处理玩家碰撞...
		Vector3D vNextStart ( vNextFramePos.x, vNextFramePos.y, m_fZBackup + fHeadHeight);
		BOOL bCheck = theVObjectManager.CheckViewPlayerBBox(this, vDir);
		if( bCheck )
			bCollision = TRUE;
	

		//3.检测前方是否存在障碍物
		Vector3D vHead( 0, 0, 1 );
		if( !bCollision )
		{
			if(	GetCollisionDistance( vNextStart, vHead, &fDistToCollision )
				&& fDistToCollision > 0)
			{
				float fDistCmp = (m_bMounting?MOUNT_COLLISION_DIST : SELF_COLLISION_DIST);

				if( fDistToCollision <  fDistCmp * SCALE_MESH)
				{
					bCollision = TRUE;
					if( g_InputData.m_MouseCtrlMode )
						StopMoving();
					OutputDebugString( "碰到头了...\n" );
				}
			}
		}
	}//		if(m_pPathHandler == NULL)


	//4.检测当前位置是否有障碍物
	if(	!bCollision 
		&& vDir.Length2() >= math::cEPSILON
		&& GetCollisionDistance( vStart, vDir, &fDistToCollision )
		&&	fDistToCollision >= 0)
	{
		{
			float fDistCmp = (m_bMounting?MOUNT_COLLISION_DIST : SELF_COLLISION_DIST2);

			if(m_pPathHandler == NULL)
			{
				if( fDistToCollision <  fDistCmp * SCALE_MESH)
				{
					bCollision = TRUE;
					if( g_InputData.m_MouseCtrlMode )
						StopMoving();
					OutputDebugString( "碰到头了...\n" );
				}
			}

			if(	fDistToCollision < JUMP_COLLISION_DIST * SCALE_MESH
				&& theApplicationSetting.m_HeroSlipJumpingAngle < ANGLE_AUTO_JUMP_MAX)
			{
				g_InputData.m_SlipStartTime	= g_CurTime;
				g_InputData.m_IsSlipJumping		= TRUE;
				g_InputData.m_SlipJumpingDir	= -vDir;
				g_InputData.m_IsJumping				= TRUE;
				g_InputData.m_JumpingDir			= -vDir;
				g_InputData.m_JumpingRotate		= g_InputData.m_RotateZCorrect;
				g_InputData.m_JumpStartTime			= g_CurTime + 61;
				m_fJumpingHeight				= 0;
				m_fStartJumpingTerrainZ		= m_vPosition.z;
				m_bJumping						= TRUE;
				m_dwStartJumpingTime			= g_CurTime + 61;
			}
		}
	}

	return bCollision;
}


void VHero::_ApplyMovingData(BOOL bCollision, const Vector3& vNextFramePos)
{
	_SUPER::_ApplyMovingData(bCollision,  vNextFramePos);

}


void VHero::_SendMovingState()
{
}

void VHero::_UpdateMovingState()
{
	_SUPER::_UpdateMovingState();

}


void VHero::OnStop()
{
	if(m_pPathHandler && m_pPathHandler->IsThrushMode())
		return;

	_SUPER::OnStop();
	StopMoving();

	//LOGINFO("VHero::OnStop\n");
	g_InputData.m_IsJumping			= FALSE;
	g_InputData.m_IsSlipJumping	= FALSE;
}

void VHero::OnReset()
{
	//LOGINFO("VHero::OnReset\n");
	_SUPER::OnReset();
}

void VHero::OnReadyToMove(BOOL bThrust)
{
	//LOGINFO	("VHero::OnReadyToMove Move:%d  Assault:%d\n"
	//			,m_bMoving
	//			,m_bAssaultMoving);
	_SUPER::OnReadyToMove(bThrust);
	if(!bThrust)
	{
		Vector3D vDestPos = m_pPathHandler->GetTargetPos();

		g_InputData.m_TargetPos				= vDestPos;
		//g_InputData.m_HeroCounterMarch	= FALSE;
		g_InputData.m_AutoAttackTarget	= FALSE;
	}
}


void VHero::UpdateDistanceToHero(  const VECTOR3D& /*vPos*/ )
{
	//空处理...
}



};//namespace vobject


/*////////////////////////////////////////////////////////////////////////
文 件 名：SceneBase.cpp
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "SceneBase.h"
#include "PacketStruct_ClientAgent.h"
#include "PacketStruct_ClientWorldS.h"
#include "Camera.h"
#include "GameClientApplication.h"
#include "GameUIManager.h"
#include "NetworkLayer.h"
#include "FormatString.h"
#include "Map.h"
#include "GameParameter.h"
#include "FreshmanTipManager.h"
#include "PacketInclude.h"

#ifdef NONE_WOLRDHANDLER
#include "V3DCamera.h"
#include "V3DGameWorld.h"
#include "V3DScene.h"
#include "V3DSceneAnimation.h"
#include "V3DSceneObject.h"
#include "VObjectSystem.h"
#include "ObjectSystem.h"
#include "PathGameMap.h"
#include "V3DCamera.h"
#endif


#define	MAX_LOAD_IMAGE			11

//int			g_iLoadingImage = 0;

#ifdef NONE_WOLRDHANDLER
V3DFrustum	SceneBase::ms_SceneFrustum;
//PathGameMap			SceneBase::ms_gameMap;
#endif

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SceneBase::SceneBase( void ) 
: m_SceneType(scene::SCENE_TYPE_NONE)
, m_dwUIListener(gameui::eUIBlank)
, m_dwMapHandle(WH_TYPE_BLANK)
, m_bLockCameraDistance(FALSE)
{ 
	m_dwFreshTipGroup				= 0;
}

SceneBase::~SceneBase( void ) 
{
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL SceneBase::Init() 
{
	m_dwPacketStatus				=0;
	m_bChangeSceneReady			= FALSE;
	m_dwBaseTick					= 0;
	m_dwChangeSceneTick			= 0;
	m_dwChangeSceneOverTick		= 0;
	m_dwChangeSceneSecond		= 0;

	theCamera.SetLockDistance(m_bLockCameraDistance);

	return TRUE; 
}


void SceneBase::Release( void )
{
}




FWORK_TYPE SceneBase::GetType()
{
	return (FWORK_TYPE)m_SceneType;
}


BOOL SceneBase::OnChangeFrameworkIn()
{
	//激活当前MapHandle
	theMap.ActivateMapHandle(m_dwMapHandle);


	//Init();
	DoSceneStart();
	theGameUIManager.UIOnStartUp((gameui::EGameUIType)m_dwUIListener);

	theFreshmanTipManager.BeginTip(theGeneralGameParam.GetFreshManTipReadyTime(),m_dwFreshTipGroup);

	theClientSetting.SetLockCameraDistance(m_bLockCameraDistance);

	return TRUE;
}

BOOL SceneBase::OnChangeFrameworkOut()
{

	//DoSceneEnd();//让子类手工调用，这里不能调用，否则会引起循环
	theGameUIManager.UIOnClose((gameui::EGameUIType)m_dwUIListener);
	return TRUE;
}


BOOL SceneBase::FrameMove( DWORD dwTick )
{
	m_dwBaseTick += dwTick;

	theMap.FrameMove(dwTick);
#ifdef NONE_WOLRDHANDLER
	if(m_bNeedVObject)
	{
		theObjectSystem.FrameMove(dwTick);
		theVObjectSystem.FrameMove(dwTick);
	}
#endif

	__RUN(theFreshmanTipManager.FrameMove(dwTick));

	theGameUIManager.UIFrameMove((gameui::EGameUIType)m_dwUIListener, gameui::eUINull, dwTick);


	theScreenTipManager.FrameMove(dwTick);

	if (IsChangeSceneReady())
	{
		if (ProcessChangeSceneOverTick(dwTick) > GetChangeSceneOverTick())
		{
			ClearChangeSceneReady();

			singleton::GetFrameworkSystem().ChangeScene( m_ChangeSceneType/*SCENE_TYPE_LOAD*/ );
		}
	}

	return TRUE;
}


BOOL SceneBase::Render( DWORD dwTick )
{
	theMap.Render(dwTick);

	return TRUE;
}

BOOL SceneBase::RenderHUD(DWORD dwTick)
{
//#ifndef NONE_TEMP
//	RenderUtil::TestingRender();
//	return TRUE;
//#endif

	the3DRenderer.Flush( RENDER_SORT_FAR_Z );


	if( g_pFlyTextEffect )
		g_pFlyTextEffect->Render();


//#ifdef NONE_TEMP
	theVObjectManager.RenderInfo( dwTick );		
	theGameUIManager.Render(dwTick);
//#endif

	theScreenTipManager.Render(dwTick);


	//IV3DRenderer* RI = the3DEngine.GetRenderer();

	//RI->SetRenderState( D3DRS_ALPHABLENDENABLE, FALSE );
	//RI->SetRenderState( D3DRS_ALPHATESTENABLE, FALSE );
	//RI->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
	//RI->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );


	//if( !g_bRotateEnable )
	//	theCursorManager.Render();
	return TRUE;
}


void SceneBase::OnRecvCG_CHARINFO( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_MAP( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_SYNC( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_BATTLE( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_SKILL( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_CONVERSATION( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_ETC( MSG_BASE * /*pMsg*/ )
{

}

void SceneBase::OnRecvCG_GM( MSG_BASE * /*pMsg*/ )
{

}


void SceneBase::OnRecvCG_CONNECTION( MSG_BASE * /*pMsg*/ )
{
#ifdef NONE_PACKET
	TCHAR	szTitle[MAX_MESSAGE_LENGTH], szMessage[MAX_MESSAGE_LENGTH];

	switch( pMsg->m_byProtocol )
	{
		/*
		case CG_CONNECTION_ALIVE_CMD:
		{
		// idle check
		MSG_CG_CONNECTION_ALIVE_SYN	alivePacket;
		alivePacket.m_byCategory = CA_CONNECTION;
		alivePacket.m_byProtocol = CA_CONNECTION_ALIVE_SYN;			
		theNetworkLayer.SendPacket(CK_GAMESERVER, &alivePacket, sizeof(alivePacket) );
		}
		break;
		*/

	case CG_CONNECTION_DISCONNECT_CMD:
		{

			MSG_CG_CONNECTION_DISCONNECT_CMD *pPacket = (MSG_CG_CONNECTION_DISCONNECT_CMD *)pMsg;
			//theGameUIManager.UIMessageBoxF(TEXTRES_DisconnecttedReason,pPacket->m_dwErrorCode);
			theGameUIManager.Sys_VerifyLockF(TEXTRES_DisconnecttedReason, TRUE, pPacket->m_dwErrorCode);
		}
		break;

	case CG_CONNECTION_RESTART_CMD:
		{
			MSG_CG_CONNECTION_RESTART_CMD * pPacket = (MSG_CG_CONNECTION_RESTART_CMD *)pMsg;

			theGameUIManager.Sys_VerifyLockF(TEXTRES_DisconnecttedReason, TRUE);
		}
		break;

	case CG_PREPARE_WORLD_CONNECT_ACK:
		{
			theCursorHandler.ForceChange( cursor::CURSORTYPE_DEFAULT );

			MSG_CG_PREPARE_WORLD_CONNECT_ACK *pPacket = (MSG_CG_PREPARE_WORLD_CONNECT_ACK*)pMsg;

			if (!theNetwork.IsConnected(CK_CHATSERVER))
			{
				int iConnectRet = theNetwork.Connect( CK_CHATSERVER,
					pPacket->szChatServerIP,
					pPacket->wChatServerPort );

				if( FALSE == iConnectRet )
				{
					theNetwork.SetEnableSendChatHeartBeat(FALSE);
				}
				else
				{
					MSG_CW_ENTER_SERVER_SYN SendPacket;
					SendPacket.m_byCategory = CW_CONNECTION;
					SendPacket.m_byProtocol = CW_ENTER_SERVER_SYN;
					SendPacket.dwAuthUserID = theGeneralGameParam.GetUserID();
					theNetworkLayer.SendPacket(CK_CHATSERVER, &SendPacket, sizeof(SendPacket) );

					theNetwork.SetEnableSendChatHeartBeat(TRUE);

					OnConnectChatServer();
				}
			}

			theNetworkLayer.AfterReconnectToChatServer();

#ifdef _DEBUG
			//COLOR	Color = COLOR_RGBA( 255, 80, 80, 255 );
			//Sprintf (szMessage, _T("HeartBeat Status (%d)"), theNetwork.IsEnableSendChatHeartBeat());
			theGameUIManager.UIMessageBox(FMSTR( "HeartBeat Status (%d)",  theNetwork.IsEnableSendChatHeartBeat() ) );
			//g_InterfaceManager.ShowMessageBox(szMessage, 10000, &Color);
#endif
		}
		break;

	case CG_PREPARE_WORLD_CONNECT_NAK:
		{
#ifdef _DEBUG
			//COLOR	Color = COLOR_RGBA( 255, 80, 80, 255 );
			//g_InterfaceManager.ShowMessageBox(szMessage, 10000, &Color);
			theGameUIManager.UIMessageBox("CG_PREPARE_WORLD_CONNECT NAK");
#endif
		}
		break;
	}
#endif
}


void SceneBase::OnRecvCW_CHAT( MSG_BASE * pMsg )
{
	//转移到 
	theGameUIManager.UIOnMessage(gameui::eUIChatSystem, pMsg);
}

void SceneBase::OnRecvCW_FRIEND( MSG_BASE * pMsg )
{
	//转移到 
	theGameUIManager.UIOnMessage(gameui::eUIFriendDlg, pMsg);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void SceneBase::ParsePacket(MSG_BASE * pMsg)
{
	switch(pMsg->m_byCategory)
	{
	case CG_CONNECTION:		OnRecvCG_CONNECTION(pMsg);	break;
	case CW_CHAT:				OnRecvCW_CHAT( pMsg );		break;
	case CW_FRIEND:			OnRecvCW_FRIEND( pMsg);		break;
	}
}


BOOL SceneBase::NeedToParsePacket(MSG_BASE * /*pMsg*/)
{
	//switch(pMsg->m_byCategory)
	//{
	//case CG_CONNECTION:
	//	return TRUE;

	//default:
	//	break;
	//}
	return TRUE;
}

BOOL SceneBase::PrevPacketHandler(MSG_BASE * /*pMsg*/)
{
	return TRUE;
}














void SceneBase::OnConnectChatServer()
{
}

void SceneBase::OnReconnect()
{
}



BOOL SceneBase::SceneWndProc(HWND        /*hWnd*/
                            ,UINT        /*iMessage*/
									 ,WPARAM      /*wParam*/
									 ,LPARAM      /*lParam*/
									 ,SI_MESSAGE* /*stMessage*/)
{
	return FALSE;
}


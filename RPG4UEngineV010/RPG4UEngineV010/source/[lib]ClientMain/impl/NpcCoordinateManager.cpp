/*////////////////////////////////////////////////////////////////////////
文 件 名：NpcCoordinateManager.cpp
创建日期：2007年11月29日
最后更新：2007年11月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	npc指引系统用的客户端信息及相关操作 画npc、怪物、物品 指引


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "NpcCoordinateManager.h"
#include "MapViewManager.h"
#include "NPCInfoParser.h"
#include "HeroActionInput.h"
#include "RegenParser.h"
#include "NpcInfoParser.h"
#include "Map.h"

#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(NpcCoordinateManager, ()  , gamemain::eInstPrioNpcCoordinateManager/*ePriorityNormal*/);





#define HYPERLINK_TEXT_LENGTH	2
#define HYPER_HEADER				"{h"
#define HYPER_TAIL				"h}"



NpcCoordinateManager::NpcCoordinateManager()
{
}

NpcCoordinateManager::~NpcCoordinateManager()
{
	m_CoordinateInfos.clear();
}



sNPC_COORDINFO*	NpcCoordinateManager::GetCoordinateInfo(LPCSTR szNpcName)
{

	NpcCoordInfoMMapIt	it;
	VRSTR						sName;

	sName	= _V(szNpcName);
	__CHECK2(sName != NULL, NULL);

	it = m_CoordinateInfos.find(sName);
	if(it == m_CoordinateInfos.end())
		return NULL;
	
	return &it->second;
}


class NpcCoordinateManagerLoadDatasOpr
{
public:
	NpcCoordinateManager*	m_pMan;
	void operator()(sNPCINFO_FUNC* pExtraInfo)
	{
		sNPC_COORDINFO	info;
		sNPCINFO_BASE*	pNpcInfo;
		Object*			pObject;

		//theNPCInfoParser.SetExtraFirst();
		//for(;pExtraInfo = theNPCInfoParser.GetExtraNext();)
		{
			pNpcInfo		= theNPCInfoParser.GetMonsterInfo(pExtraInfo->m_NPCCODE);
			if(!pNpcInfo)
				return;

			info.m_NpcFuncID	= pExtraInfo->m_ExtraCode;	
			info.m_Name			= pNpcInfo->m_NpcName;
			info.m_MapX			= pExtraInfo->m_vPos.x;
			info.m_MapY			= pExtraInfo->m_vPos.y;
			info.m_NpcID		= 0;	
			//info._IsMask = bMask;
			pObject = theObjectManager.GetFuncNpc(info.m_NpcFuncID);
			if(pObject)
				info.m_NpcID	= pObject->GetObjectKey();	

			//m_NpcCoords.push_back(info);
			m_pMan->PushNpcInfo(info);
		}
	}
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL NpcCoordinateManager::LoadDatas()
{
	NpcCoordinateManagerLoadDatasOpr opr={this};
	theNPCInfoParser.ForEachExtra(opr);
	return TRUE;
}
void NpcCoordinateManager::PushNpcInfo		(sNPC_COORDINFO&	info)
{
	m_CoordinateInfos.insert(NpcCoordInfoPair(info.m_Name,info));
}

void NpcCoordinateManager::_ConvertName	(LPCSTR      szName
														,StringHandle& sResult)
{
	int	nLength			= 0,
			nIdx			= 0, 
			nHyperTail	= -1,
			nHyperHead	= -1;

	//StringText	sResult  = (LPCTSTR)sRet;
	nLength = lstrlen((LPCTSTR)szName);
	nIdx = 0;

	////////////////////////////////////////////
	for(;; )
	{
		nIdx = sResult.Find((LPCTSTR)szName, nIdx);
		if(nIdx == -1)
			break;

		if(nIdx > 2)
		{
			if(sResult[nIdx-2] == '{' && sResult[nIdx-1] == 'h')
			{
				nIdx += nLength + HYPERLINK_TEXT_LENGTH * 2;
				continue;
			}

			nHyperTail	= sResult.Find( HYPER_TAIL, nIdx );
			nHyperHead	= sResult.Find( HYPER_HEADER, nIdx );
			if( nHyperTail != -1)
			{
				if( nHyperHead == -1)
				{
					nIdx += nLength;
					continue;
				}
				if( nHyperTail < nHyperHead )
				{
					nIdx += nLength;
					continue;
				}
			}//--
		}

		///////////////////////////////////////////////
		sResult.Insert(nIdx, HYPER_HEADER);

		///////////////////////////////////////////////
		sResult.Insert(nIdx + nLength + HYPERLINK_TEXT_LENGTH, HYPER_TAIL);

		nIdx += nLength + HYPERLINK_TEXT_LENGTH * 2;
	}
}

class NpcCoordinateManagerConvertHyperLinkOpr
{
public:
	NpcCoordinateManager*	m_pMan;
	StringHandle*				m_pResult;
	void operator()(sNPCINFO_BASE*	pNpc)
	{
		m_pMan->_ConvertName((LPCTSTR)pNpc->m_NpcName,*m_pResult);
	}
};

/*////////////////////////////////////////////////////////////////////////
//改变szNpcName 如果有npc名字 加入超链接 {h.... h}
/*////////////////////////////////////////////////////////////////////////
void NpcCoordinateManager::ConvertHyperLink	(LPCSTR      szText
															,StringHandle& sResult)
{
	sResult = szText;


	NpcCoordInfoMMapIt	it;

	it = m_CoordinateInfos.begin();


	////////////////////////////////////////////
	for(;it != m_CoordinateInfos.end();it++)
	{
		sNPC_COORDINFO&	info = it->second;
		_ConvertName((LPCTSTR)info.m_Name,sResult);
	}

	////////////////////////////////////////////
	NpcCoordinateManagerConvertHyperLinkOpr opr={this,&sResult};
	theNPCInfoParser.ForEach(opr);

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void NpcCoordinateManager::ProcessHyperLink(LPCSTR szData)
{
	theMapViewManager.ClearNpcInfo();

	StringHandle	strData = szData;
	strData.Replace(_T("\n"), _T(""));


	NpcCoordInfoMMapIt	it;
	VRSTR						sName;
	Object*					pObject(NULL);

	sName	= _V(strData);
	__CHECK2(sName != NULL, );


	////////////////////////////1
	Vector3D	vPos;

	////////////////////////////
	it = m_CoordinateInfos.find(sName);
	if(it == m_CoordinateInfos.end())
	{
		pObject = theObjectManager.GetObject(strData,NPC_OBJECT,TRUE,TRUE);
		if(!pObject)
			pObject = theObjectManager.GetObject(strData,PLAYER_OBJECT,FALSE,TRUE);

		////////////////////////////////////////
		if(pObject)
		{
			vPos = pObject->GetPosition();
		}
		else
		{
			vPos = theHero.GetPosition();
			if(!theRegenParser.GetNearRegenPos	(strData
															,theMap.GetMapID()
															,INVALID_FIELDID
															,vPos))
				return;
		}

		////////////////////////////////////
		theHeroActionInput.ProcessClickPosition(vPos);
		theMapViewManager.SetNpcInfo	(strData
												,vPos.x/TILE_3DSIZE
												,vPos.y/TILE_3DSIZE);
		return;
	}

	sNPC_COORDINFO&	info		= it->second;

	////////////////////////////
	//if(!pObject)
	{
		pObject = theObjectManager.GetFuncNpc(info.m_NpcFuncID);
		if(pObject)
			vPos = pObject->GetPosition();
		else
		{
			vPos.x  = info.m_MapX* TILE_3DSIZE;
			vPos.y  = info.m_MapY* TILE_3DSIZE;
			vPos.z  = theGameWorld.GetHeightAt(100,100,vPos.x,vPos.y,0);	
		}
		theHeroActionInput.ProcessClickPosition(vPos);
	}



	////////////////////////////
	for(;it != m_CoordinateInfos.end() && it->first == sName;it++)
	{
		sNPC_COORDINFO&	info = it->second;
		theMapViewManager.SetNpcInfo((LPCTSTR)info.m_Name, info.m_MapX, info.m_MapY);
	}

	
}

LPCSTR	NpcCoordinateManager::GetNpcName(DWORD  npcID)
{
	sVNPCINFO_BASE*	pVNpcInfo;
	pVNpcInfo = theNPCInfoParser.GetVMonsterInfoBy(npcID);
	if(pVNpcInfo)
		return (LPCSTR)pVNpcInfo->m_sMonsterName;

	return NULL;
	
}
/*////////////////////////////////////////////////////////////////////////
文 件 名：Player.cpp
创建日期：2008年3月31日
最后更新：2008年3月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Player.h"
#include "CharSoundInfoParser.h"
#include "WeaponSoundInfoParser.h"
#include "PlayerHeightInfoParser.h"
#include "GameAvatarSetting.h"
#include "VPlayer.h"
#include "CharActionInfo.h"
#include "SkillEffect.h"
#include "QuestResourceList.h"
#include "MapNPC.h"
#include "EquipmentContainer.h"
#include "ConstTextRes.h"
#include "Player.inc.h"
#include "IScriptCharacter.h"
#include "ScriptManager.h"
#include "HeroParty.h"


#ifndef  NONE_SCRIPTMAN
#include "ScriptVarManager.h"
#endif


using namespace util;
using namespace vobject;
using namespace info;




namespace object
{ 
const float		CHECK_READY_PERIOD	= 300;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
Player::Player( void )
{
	m_ObjectCookie.SetObjectType(PLAYER_OBJECT);	


	SetAttackSequence(ATTACK_SEQUENCE_THIRD);

	m_dwCheckAttackReady	= 0;
	m_pVPlayer				= NULL;
	m_pEquipContainer		= NULL;


	m_dwStabberShotSerial = 0;

	m_bStabberReload		= FALSE;

	m_byGMGrade				= 0;		
	m_bInvisible			= FALSE;
	m_bInvincible			= FALSE; 
	m_bHideHelmet			= FALSE;


	for (int a = 0; a < PLAYER_VARIATION_MAX; a++)
	{
		m_arPartVariation[a] = 1;
	}

	m_wAttSpeedRatio	= 100;
	m_wMoveSpeedRatio = 100;

	m_iHeightVariation = 2;

	m_bExistHPInfo = FALSE;

    m_GuildInfo.m_byExistGuild = 0;

#ifndef NONE_MONSTERTESTING
	m_pItemManager		= NULL;
#endif



}

//------------------------------------------------------------------------------
Player::~Player( void )
{
}

//------------------------------------------------------------------------------
BOOL Player::Create( DWORD dwObjectKey, DWORD dwParam )
{
	m_ObjectCookie.SetObjectKey( dwObjectKey );
	GetCharInfo()->m_byClassCode = (BYTE)dwParam;

	///
	__CHECK( InitContainer() );

	m_pPlayerAttributes = new PlayerAttributes;
	m_pPlayerAttributes->SetOwner( (PlayerCookie *)&m_PlayerCookie); 
	

	BOOL bRet;
	bRet = Character::Create(dwObjectKey, dwParam);
	bRet;
	ASSERT(bRet);


	InitState(STATE_IDLE,g_CurTime);
	
	m_iStabberShotDelay		= 0;
	m_iStabberShotCount		= 0;
	m_iStabberShotRemain		= 0;
	m_dwStabberShotSerial	= 0;	

	m_bSpecialMoveFlag		= 0;
	m_dwOldAttackStyle		= INVALID_DWORD_ID;
	m_iBulletCount				= 20;

	m_iStabberShotAnimSet	= 0;
	m_iStabberShotAnimCount = 0;

	m_byGMGrade					= 0;		
	m_bInvisible				= FALSE;	

	// PVP 
	m_iTeam					= INVALID_TEAM;

	for (int a = 0; a < PLAYER_VARIATION_MAX; a++)
	{
		m_arPartVariation[a] = 1;
	}

	m_bExistHPInfo = FALSE;
	SetBehaveState( PLAYER_BEHAVE_IDLE );	

	return TRUE;
}


//------------------------------------------------------------------------------
void Player::Destroy( void )
{
	//////////////////////////////////////////

	//////////////////////////////////////////
	ReleaseContainer();

	if (m_pPlayerAttributes)
	{
		delete m_pPlayerAttributes;
		m_pPlayerAttributes = NULL;
	}
	Character::Destroy();
}







void Player::OnSetItemLog(BOOL bSet)
{
	theItemLogManager.SetEnableLog(bSet);
}

BOOL Player::RunScript(DWORD	dwData,DWORD npcID,BOOL bRunNow)
{

	///////////////////////////////////////////
	//IScriptCharacter*	pChar;
	KeepQuestScript(SCRIPTCHARACTER_CHAR,QUESTCHARACTER_PLAYER);

	if(m_pScriptChar)
	{
		if(!bRunNow)
		{
			m_pScriptChar->SetScriptTimer(theGeneralGameParam.GetScriptRunPeriod() );
		}
		return m_pScriptChar->RunScript(dwData,npcID,bRunNow);
	}
	return FALSE;
}


BOOL Player::RunQuestScript(CODETYPE questID,DWORD	dwData,BOOL bRunNow)
{
	IScriptCharacter*	pChar;
	Character*			pDestChar;

	KeepQuestScript(SCRIPTCHARACTER_CHAR,QUESTCHARACTER_PLAYER);

	pChar = GetScriptChar();
	__CHECK_PTR(pChar);

	///////////////////////////////////////////
	pDestChar	= (Character*)theObjectManager.GetObject( pChar->GetTargetNpcID() );
	__CHECK(pDestChar && pDestChar->IsMapNpc());

	///////////////////////////////////////////
	if(m_pScriptChar)
	{
		if(!bRunNow)
		{
			m_pScriptChar->SetScriptTimer(theGeneralGameParam.GetScriptRunPeriod() );
		}

		return pChar->RunQuestScript(questID
											 ,dwData
											 ,pDestChar->GetQuestChar()
											 ,bRunNow);
	}
	return FALSE;
}


DWORD	Player::GetQuestState		( CODETYPE	questID )
{
	if(m_pQuestChar)
		return m_pQuestChar->GetQuestState (questID);
	return 0;
}

void	Player::SetQuestState		( CODETYPE	questID, DWORD state )
{
	if(m_pQuestChar)
		m_pQuestChar->SetQuestState( questID, state);
}
void	Player::DiscardQuest( CODETYPE	questID )
{
	if(m_pQuestChar)
		m_pQuestChar->DiscardQuest (questID);
}

BOOL	Player::IsQuestActive( CODETYPE	questID )
{
	if(m_pQuestChar)
		return m_pQuestChar->IsQuestActive (questID);
	return FALSE;
}

BOOL	Player::IsQuestDone( CODETYPE	questID )
{
	if(m_pQuestChar)
		return m_pQuestChar->IsQuestDone (questID);
	return FALSE;
}

BOOL	Player::CanGetNewQuest		()
{
	if(m_pQuestChar)
		return m_pQuestChar->CanGetNewQuest ();
	return FALSE;
}

BOOL	Player::IsNeedDropTaskItem	(CODETYPE		questID
											,DWORD			taskState
											,SLOTCODE		itemCode)
{
	if(m_pQuestChar)
		return m_pQuestChar->IsNeedDropTaskItem (questID
															 ,taskState
															 ,itemCode);
	return FALSE;
}

void Player::ChangeQuestData( CODETYPE /*questID*/, DWORD /*state*/, DWORD )
{
}


//------------------------------------------------------------------------------
BaseContainer * Player::GetSlotContainer( SLOTINDEX slotIdx )
{
	switch( slotIdx )
	{
	case SI_EQUIPMENT:
		{
			return m_pEquipContainer;
		}
		break;
	}
	return NULL;
}

//------------------------------------------------------------------------------
BOOL Player::InitContainer()
{
	ContainerHandle	equipHandle(SLOTT_EQUIP);

	__CHECK_PTR(&equipHandle);

	m_pEquipContainer = equipHandle.Detach();//new EquipmentContainer();

	m_pEquipContainer->Init(MAX_EQUIPMENT_SLOT_NUM, SI_EQUIPMENT);
	m_pEquipContainer->SetOwnerKey(GetObjectKey());

	return TRUE;
}

//------------------------------------------------------------------------------
void Player::ReleaseContainer()
{
	if( m_pEquipContainer )
	{
		m_pEquipContainer->ClearAll();
		m_pEquipContainer->Release();

		ContainerHandle	equipHandle(m_pEquipContainer);
		//delete m_pEquipContainer;
		m_pEquipContainer = NULL;
	}
}

//------------------------------------------------------------------------------
void Player::ResetMoveFlag()
{
	m_iMoveFlag = 0;
}

//------------------------------------------------------------------------------
void Player::AddMoveFlag(int MoveFlag)
{	
	BIT_ADD(m_iMoveFlag,MoveFlag);
}

//------------------------------------------------------------------------------
void Player::RemoveMoveFlag(int MoveFlag)
{
	BIT_REMOVE(m_iMoveFlag,MoveFlag);
}

//------------------------------------------------------------------------------
BOOL Player::CheckMoveFlag(int MoveFlag)
{
	return BIT_CHECK(m_iMoveFlag,MoveFlag);
}


//------------------------------------------------------------------------------
void Player::SetClientPartInfo(sCHARACTER_CLIENTPART *	pClientPart
										,BOOL								bUpdateEquips)
{

	GetCharInfo()->m_byClassCode		= pClientPart->m_byClass;
	GetCharInfo()->m_LV					= pClientPart->m_LV;
	GetCharInfo()->m_bySex				= pClientPart->m_bySex;
	GetCharInfo()->m_byHairColor		= pClientPart->m_byHairColor;
	GetCharInfo()->m_byHair				= pClientPart->m_byHair;

	if(pClientPart->m_dwRegion != 0)
	{
		GetCharInfo()->m_bySlot			= pClientPart->m_bySlot;

		GetCharInfo()->m_dwRegion		= pClientPart->m_dwRegion;
		GetCharInfo()->m_sLocationX	= pClientPart->m_wX;
		GetCharInfo()->m_sLocationY	= pClientPart->m_wY;
		GetCharInfo()->m_sLocationZ	= pClientPart->m_wZ;
	}

	SetName				( pClientPart->m_szCharName);
	SetPartVariation	(PLAYER_VARIATION_HAIR,pClientPart->m_byHair,FALSE);
	SetPartVariation	(PLAYER_VARIATION_FACE,pClientPart->m_byFace,FALSE);

	SetHeightVariation(pClientPart->m_byHeight);


	if(bUpdateEquips)
		SetEquipItemTotalInfo( pClientPart->m_EquipItemInfo );
	UpdateAppearance();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID Player::GetEquipItemTotalInfo( sEQUIP_ITEM_INFO & OUT rItemInfo )
{
	sITEM_SLOTEX * pItemSlot = rItemInfo.m_Slot; 

	rItemInfo.m_Count = 0;

	SLOTPOS start = 0;
	SLOTPOS total = MAX_EQUIPMENT_SLOT_NUM;

	for (SLOTPOS i = start; i < total; ++i)
	{
		if ( !m_pEquipContainer->IsEmpty(i) )
		{
			pItemSlot[rItemInfo.m_Count].m_ItemPos = i;
			ItemSlot&	slotItem = (ItemSlot &)m_pEquipContainer->GetSlot(i);
			slotItem.CopyOut( pItemSlot[rItemInfo.m_Count].m_Stream );

			++rItemInfo.m_Count;			
		}
	}
}


VOID Player::SetEquipItemTotalInfo(const sEQUIP_ITEM_INFO & IN rItemInfo )
{
	const sITEM_SLOTEX * pSlot = rItemInfo.m_Slot;
	SLOTPOS start	= 0;
	SLOTPOS total	= rItemInfo.m_Count;
	SLOTPOS equipAt;

	for(SLOTPOS i=start;i<total;++i)
	{
		if(pSlot[i].m_Stream.ItemPart.wCode == 0)
			continue;

		ItemSlot equipSlot(pSlot[i].m_Stream );

		equipAt	= pSlot[i].m_ItemPos;

		if(m_pEquipContainer->IsEmpty(equipAt))
			m_pEquipContainer->InsertSlot(equipAt, equipSlot);
		else
			m_pEquipContainer->UpdateSlot(equipAt, equipSlot);
	}

	AttributeAffectorItemHandle itemCalc( *GetPlayerAttributes() 
													, *m_pEquipContainer );
	itemCalc.UpdateAll();

	UpdateSkillAttributes();

	GetPlayerAttributes()->Update();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID Player::SetEquipItemBaseInfo( SLOTPOS total, sITEM_SLOT * IN pSlot )
{
	for( SLOTPOS i = 0; i < total; ++i )
	{
		ItemSlot equipSlot( pSlot[i].m_Stream );

		m_pEquipContainer->InsertSlot( pSlot[i].m_ItemPos, equipSlot );
	}
	AttributeAffectorItemHandle itemCalc( *GetPlayerAttributes() 
													, *m_pEquipContainer );
	itemCalc.UpdateAll();

	UpdateSkillAttributes();

	GetPlayerAttributes()->Update();

	UpdateAppearance();
}



DWORD	Player::GetCharSoundCode()
{
	return (GetClass() - PLAYERTYPE_BASE) * SEX_MAX + 1 + GetSex();
}



BOOL Player::ProcessUpdateFlag(DWORD dwTick,DWORD dwFlags)
{
	__BOOL_SUPER(ProcessUpdateFlag(dwTick,dwFlags));

	//const DWORD UPDATE_GROUP	= CHAR_UPDATE_SKILLEFFECT
	//									| CHAR_UPDATE_HPMPINFO
	//									| CHAR_UPDATE_DEATH
	//									| CHAR_UPDATE_LEVELUP
	//									| CHAR_UPDATE_ATTRIB;

	const DWORD UPDATE_PARTY	= CHAR_UPDATE_HPMPINFO | CHAR_UPDATE_PARTY;

	//if(_BIT_EXIST(dwFlags,  UPDATE_GROUP ))
	//{
	//	theGameUIManager.UIRefresh(gameui::eUIGroup);
	//}

	if(_BIT_TEST(dwFlags,  CHAR_UPDATE_LEVELUP ))
	{
		m_pVPlayer->OnLevelUp();
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_EQUIPMENTS))
	{
		#pragma message(__FILE__  "(425) Hero..UpdateAppearance 更新头像信息... " )
		UpdateAppearance(FALSE);
	}

	if(_BIT_EXIST(dwFlags,  UPDATE_PARTY ))
	{
		if(theHeroParty.GetMemberBy(GetObjectKey()))
			theGameUIManager.UIRefresh(gameui::eUITeamHero);
	}

	if(_BIT_TEST(dwFlags,  CHAR_UPDATE_VENDOR ))
	{
		m_pVPlayer->InitSericeIcon(PREFIXKIND_VENDOR);
	}

	//if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_EXPINFO))
	//{
	//	theGameUIManager.UIRefresh(gameui::eUIMain);
	//}

	//if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_SPAWN))
	//{
	//}

	//if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_SKILLINFO))
	//{
	//}

	return TRUE;
}



//------------------------------------------------------------------------------
float Player::ProcessAnimation(DWORD dwTick,GAMEEVENT *pEvent)
{	
	ProcessStandAnimationDelay(dwTick);

	return Character::ProcessAnimation(dwTick,pEvent);
}

void	Player::SetVendorTitle(LPCTSTR szTitle)
{
	AddUpdateFlag(CHAR_UPDATE_VENDOR);
	m_VendorTitle = szTitle;
}

//------------------------------------------------------------------------------
IDTYPE Player::GetCurrentAttackAnimation()
{
	IDTYPE animationID = 0;

	DWORD dwStyle = GetCurrentAttackStyle();
	DWORD sequence= GetAttackSequence();
	DWORD weapon  = GetWeaponKind();

	sSTYLEINFO_BASE *pInfo = theSkillInfoParser.GetStyleInfo( (SLOTCODE)dwStyle );
	if (!pInfo)
	{
		switch(sequence)
		{
		case ATTACK_SEQUENCE_THIRD:
			animationID = info::Attack3;
			break;
		case ATTACK_SEQUENCE_SECOND:
			animationID = info::Attack2;
			break;
		default :
			animationID = info::Attack1;
			break;
		}
	}
	else 
	{
		VRSTR sAction = NULL;
		if (weapon != WEAPONTYPE_CROSSBOW && weapon != WEAPONTYPE_BOW)
		{
			sAction = pInfo->m_arAttackActions[sequence];
		}
		else
		{
			if (m_iStabberShotAnimSet)
			{
				switch(m_iStabberShotAnimCount)
				{
				case 0:
				case 1:
				case 2:
					sAction = pInfo->m_arAttackActions[ATTACK_SEQUENCE_FIRST];
					break;
				default:
					sAction = pInfo->m_arAttackActions[ATTACK_SEQUENCE_SECOND];
					break;				
				}
			}
			else
			{
				switch(m_iStabberShotAnimCount)
				{
				case 0:
				case 1:
				case 2:
				case 3:
				case 4:
					sAction = pInfo->m_arAttackActions[ATTACK_SEQUENCE_SECOND];
					break;
				default:
					sAction = pInfo->m_arAttackActions[ATTACK_SEQUENCE_FIRST];
					break;				
				}

			}

			m_iStabberShotAnimCount++;

			if (m_iStabberShotAnimCount >= 8)
			{
				m_iStabberShotAnimSet = rand() % 2;

				m_iStabberShotAnimCount = 0;
			}
		}

		INT nActionID = theCharActionInfo.FindIdByName((LPCSTR)sAction);
		if(nActionID == -1)
			nActionID = info::Attack1;
		animationID = (IDTYPE) nActionID;
	}
	return animationID;
}



void Player::UpdateShotStyle()
{
	DWORD dwStyle(0);
	switch(GetClass())
	{
	case PLAYERTYPE_WARRIOR:
		dwStyle	= STYLECODE_WARRIOR_BOW;
		break;
	case PLAYERTYPE_PALADIN:
		dwStyle	= STYLECODE_PALADIN_BOW;
		break;
	case PLAYERTYPE_POWWOW:
		dwStyle	= STYLECODE_POWWOW_BOW;
		break;
	case PLAYERTYPE_STABBER:
		dwStyle	= STYLECODE_STABBER_BOW;
		break;
	case PLAYERTYPE_NECROMANCER:
		dwStyle	= STYLECODE_NECROMANCER_BOW;
		break;
	}

	if(dwStyle)
		SetCurrentAttackStyle(dwStyle);
}
	
//------------------------------------------------------------------------------
void Player::SetCurrentNomalAttackStyle(DWORD dwWeaponType)
{
	DWORD dwStyle(0);

	switch(dwWeaponType)
	{
	case WEAPONTYPE_TWOHANDAXE:	dwStyle	= STYLECODE_TWOHANDAXE_NORMAL;	break;
	case WEAPONTYPE_TWOHANDSWORD:	dwStyle	= STYLECODE_TWOHANDSWORD_NORMAL;	break;
	//case WEAPONTYPE_ONEHANDSWORD:	dwStyle	= STYLECODE_ONEHANDSWORD_NORMAL;	break;
	//case WEAPONTYPE_SPEAR:			dwStyle	= STYLECODE_SPEAR_NORMAL;			break;
	//case WEAPONTYPE_STAFF:			dwStyle	= STYLECODE_STAFF_NORMAL;			break;
	//case WEAPONTYPE_ORB:				dwStyle	= STYLECODE_ORB_NORMAL;				break;
	//case WEAPONTYPE_BOW:
	//		UpdateShotStyle();
	//	break;

	default:
	dwStyle	= STYLECODE_TWOHANDSWORD_NORMAL;	break;
	}
	if(dwStyle)
		SetCurrentAttackStyle(dwStyle);
}


//------------------------------------------------------------------------------
void Player::SetCondition(BYTE byCondition)
{
	Character::SetCondition(byCondition);

	switch(byCondition)
	{
		case CHAR_CONDITION_STANDUP:
			{
				if( GetCurrentState()==STATE_SIT )
				{
					SetNextState(STATE_IDLE,g_CurTime);
				}
			}
			break;

		case CHAR_CONDITION_SITDOWN:
			{
				SetNextState(STATE_SIT,g_CurTime);		
			}
			break;
	}
}


//------------------------------------------------------------------------------





void Player::SetPlayerInfo	(sPLAYERINFO_RENDER *	pPlayerInfo
									,BOOL							bUpdateAppearance
									,BOOL							bVillageInfo)
{
	ZeroMemory(GetCharInfo(),sizeof(sPLAYERINFO_BASE));

	sCHARACTER_CLIENTPART	info={0};

	RenderInfo2ClientPart(*pPlayerInfo,info);
	
	SetClientPartInfo(&info);


	////////////////////////////////////
	GetCharInfo()->m_dwHP 			= pPlayerInfo->m_HP		;
	GetCharInfo()->m_dwMaxHP		= pPlayerInfo->m_MaxHP	;	

	//m_dwAttackStyle					= pPlayerInfo->m_SelectStyleCode;
	m_wMoveSpeedRatio             = pPlayerInfo->m_wMoveSpeedRatio;
	m_wAttSpeedRatio	            = pPlayerInfo->m_wAttSpeedRatio;


	SetCurrentAttackStyle(pPlayerInfo->m_SelectStyleCode);


	SetHP				( pPlayerInfo->m_HP );
	SetCondition	(pPlayerInfo->m_byCondition);
	SetBehaveState	((ePLAYER_BEHAVE_STATE)pPlayerInfo->m_BehaveState);

	m_pPlayerAttributes->SetCharInfo( m_PlayerCookie.GetCharInfo(),0,0);


	/////////////////////////////////////////////
	//sSTATE_INFO *pStateInfo;
	//
	//pStateInfo = (sSTATE_INFO*)( (BYTE*)pPlayerInfo + sizeof(sPLAYERINFO_RENDER) );
	//for( int i = 0; i < pPlayerInfo->m_byCount; ++i )
	//{
	//   pStateInfo[i];
	//}


	if (bUpdateAppearance) 
		UpdateAppearance();

	////////////////////////////////////////////////
	if(!bVillageInfo)
	{
		m_bExistHPInfo = TRUE;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Player::SetPlayerVillageInfo(sPLAYERINFO_VILLAGE * pPlayerInfo,BOOL bUpdateAppearance)
{
	ZeroMemory(GetCharInfo(),sizeof(sPLAYERINFO_BASE));
	
	sPLAYERINFO_RENDER	render;
	__ZERO(render);
	Village2RenderInfo(*pPlayerInfo, render);

	SetPlayerInfo(&render,bUpdateAppearance,TRUE);

#ifdef USE_DISALE
	GetCharInfo()->m_byClassCode	= pPlayerInfo->m_byClass;
	GetCharInfo()->m_LV				= 1;
	SetName(pPlayerInfo->m_szName);

	GetCharInfo()->m_dwHP 			= 100;
	GetCharInfo()->m_dwMaxHP		= 100;
	m_dwAttackStyle					= 0;
	m_wMoveSpeedRatio               = pPlayerInfo->m_wMoveSpeedRatio;
	m_wAttSpeedRatio	            = 100;

	SetHP				( GetCharInfo()->m_dwHP );
	SetCondition	(pPlayerInfo->m_byCondition);
	SetBehaveState	((ePLAYER_BEHAVE_STATE)pPlayerInfo->m_BehaveState);

	m_pPlayerAttributes->SetCharInfo( m_PlayerCookie.GetCharInfo(),0,0);

	SetPartVariation	(PLAYER_VARIATION_HAIR,pPlayerInfo->m_byHair,FALSE);
	SetPartVariation	(PLAYER_VARIATION_FACE,pPlayerInfo->m_byFace,FALSE);
	SetHeightVariation(pPlayerInfo->m_byHeight);
	

	if (bUpdateAppearance) 
	{
		UpdateAppearance();
	}
#endif
	
}



//------------------------------------------------------------------------------
void Player::StabberShot(DWORD /*dwAttackSerial*/)
{
	//reserved


}


//------------------------------------------------------------------------------
BOOL Player::CanStabberAction()
{
	if( theHeroActionInput.IsIdleState() 
		|| GetCurrentState() == STATE_KEYBOARDMOVE 
		|| GetCurrentState() == STATE_MOVE )
	{
		return TRUE;
	}
	return FALSE;
}



void Player::SetEquipAppearInfo(EQUIPAPPEARINFO *pInfo,ItemSlot & slotDat)
{
	pInfo->dwItemID = slotDat.GetCode();
	pInfo->bDivine = slotDat.IsDivine();	
	pInfo->iEnchant = slotDat.GetEnchant();

	switch(slotDat.GetPos())
	{
	case EQUIPPOS_WEAPON:
	//case EQUIPPOS_WEAPON2:
		{
			sITEMINFO_BASE*	pItemInfo;
			eATTACK_KIND		kind		(ATTACKKIND_ALL_OPTION);

			pItemInfo = slotDat.GetItemInfo();
			if(slotDat.GetCode() && pItemInfo)
				kind = (eATTACK_KIND)pItemInfo->m_bySeries;
			m_pVPlayer->InitSericeIcon(kind);
		}
		break;
	}
}

//------------------------------------------------------------------------------
void Player::UpdateAppearance(BOOL bAddUpdateFlag)
{
	if(bAddUpdateFlag)
	{
		AddUpdateFlag(CHAR_UPDATE_EQUIPMENTS);
		return;
	}


	APPEARANCEINFO appinfo;	
	ZeroMemory(&appinfo, sizeof(appinfo));
	appinfo.ObjectType	= APPEARANCEINFO::PLAYER;
	appinfo.Job				= GetClass();

	//int iHairSet = 0;

	for (int i = 0; i < EQUIPPOS_MAX; i++ )
	{
		ItemSlot & slotDat =(ItemSlot &) m_pEquipContainer->GetSlot((SLOTPOS)i);

		switch(i)
		{
		//case EQUIPPOS_RING1:				
		//case EQUIPPOS_RING2:				
		//case EQUIPPOS_SHIRTS:		
		//case EQUIPPOS_BELT:			
		//case EQUIPPOS_NECKLACE:
		//case EQUIPPOS_CAPE:				
		default:
			break;

		case EQUIPPOS_HAIR:		
		case EQUIPPOS_FACE:		
		case EQUIPPOS_HEADWEAR:	
#ifdef MORE_EQUIPPOS
		case EQUIPPOS_CAPE:
#endif

		case EQUIPPOS_PROTECTOR:		
		case EQUIPPOS_WEAPON:
		case EQUIPPOS_WEAPON2:
		case EQUIPPOS_BOOTS:
		case EQUIPPOS_PANTS:
		case EQUIPPOS_ARMOR:
		case EQUIPPOS_GLOVE:
		case EQUIPPOS_SHIELD:
			{
				SetEquipAppearInfo(&appinfo.Equipment[i],slotDat);
			}
			break;

		case EQUIPPOS_HELMET:
			{
				if (m_bHideHelmet) 
				{
					appinfo.Equipment[i].dwItemID = 0;	
					//iHairSet = 0;
				}
				else
				{
					SetEquipAppearInfo(&appinfo.Equipment[i],slotDat);	

					//sITEMINFO_BASE * pItemInfo = theItemInfoParser.GetItemInfo( slotDat.GetCode() );

					//if (pItemInfo)
					//	iHairSet = pItemInfo->m_bHeadType;
					//else
					//	iHairSet = 0;
				}
			}
			break;
		}

	}


	RefreshRender(&appinfo, FALSE);	

	float fScale = 1.0f;

	sPLAYER_HEIGHTINFO *pInfo;
	
	pInfo = thePlayerHeightInfoParser.GetHeroHeightInfo(GetClass());

	ASSERT(m_iHeightVariation >= 0 && m_iHeightVariation < MAX_HEIGHT_VARIATION);

	if (pInfo)
	{
		fScale = pInfo->m_fHeight[m_iHeightVariation];
	}

	SetScale(fScale);
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Player::UpdateAttackReady(DWORD dwTick)
{
	m_dwCheckAttackReady += dwTick;

	if(m_dwCheckAttackReady < theGeneralGameParam.GetAttackReadyPeriod())
		return;

	m_dwCheckAttackReady	= 0;

	if(ObjectUtil::GetAttackTargetWithinRange(GetPosition()
                                          ,theGeneralGameParam.GetAttackReadyRange()
														,NULL
														,0))
	{
		if( !m_pVPlayer->HasFightFlag(FIGHTFLAG_FIGHTING) )
			m_pVPlayer->AddFightFlag( FIGHTFLAG_FIGHTING );
	}
	else
	{
		if(m_pVPlayer->HasFightFlag(FIGHTFLAG_FIGHTING) )
			m_pVPlayer->ClearFightFlag( FIGHTFLAG_FIGHTING );
	}
}


//------------------------------------------------------------------------------
void Player::LookLockedTarget()
{
	if (m_bLocked)
	{
		if (m_dwLockedTargetID) 
		{
			Object *pObject = theObjectManager.GetObject(m_dwLockedTargetID);
			if (pObject) 
			{
				Vector3D vPos;			
				Character *pChr  = (Character *)pObject;
				vPos = pChr->GetVisiblePos();
				vPos.z += pChr->GetArrowTargetHeight();
				
				switch (GetCurrentState())
				{				
				case STATE_KEYBOARDMOVE:
				case STATE_MOVE:
				case STATE_IDLE:
					if (GetStabberShotDelay())
					{
						SetLookAtAnimation(TRUE,&vPos);
					}
					else
					{
						SetLookAtAnimation(FALSE,NULL);
					}
					break;
				default:
					SetLookAtAnimation(FALSE,NULL);
					break;
				}
			}
		}
		else
		{
			SetLookAtAnimation(FALSE,NULL);
		}
	}
	else
	{
		if (m_bLockedPosition)
		{
			SetLookAtAnimation(TRUE,&m_vLockedPosition);

		}
		else 
		{
			SetLookAtAnimation(FALSE,NULL);
		}
	}
}


//------------------------------------------------------------------------------
BOOL Player::Process(DWORD dwTick)
{
	if(m_pScriptChar)
		m_pScriptChar->FrameMove(dwTick);


	UpdateAttackReady(dwTick);

	if (GetClass() == PLAYERTYPE_STABBER)
	{
		ProcessStabberShot(dwTick);
	}
	

	return Character::Process(dwTick);
}



//------------------------------------------------------------------------------
DWORD Player::GetCurrentAttackStyle()const
{
	return m_dwAttackStyle;
}


//------------------------------------------------------------------------------
BOOL Player::SetCurrentAttackStyle(DWORD dwStyle,BOOL /*bCheck*/)
{

	/////////////////////////////////////////
	if(	m_dwAttackStyle != dwStyle
		&&	theHero.GetObjectKey() == GetObjectKey())
	{
		theHeroActionInput.SetComboReset();
	}

	m_dwAttackStyle							= dwStyle;
	GetCharInfo()->m_wSelectStyleCode	= (WORD)dwStyle;
//#endif

	AddUpdateFlag(CHAR_UPDATE_STYLE);

	return TRUE;
}


//------------------------------------------------------------------------------
BOOL Player::CheckAttackStyle(DWORD dwStyle, BOOL bOutput)
{

	sSTYLEINFO_BASE *pInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)dwStyle);

	__CHECK_PTR(pInfo);

	DWORD	dwTextID(0);

	if(pInfo==NULL)
	{
		dwTextID = TEXTRES_NOT_EXIST_STYLE;
		goto laFailed;
	}

	sPLAYERINFO_BASE* bp = GetCharInfo();
	if(bp==NULL)
	{
		dwTextID = (TEXTRES_NOT_CHARACTER_INFO_FOR_STYLE);
		goto laFailed;
	}

	if(bp->m_byClassCode != pInfo->m_dwClassDefine && pInfo->m_dwClassDefine != -1)
	{
		dwTextID = (TEXTRES_NOT_APPLY_STYLE);
		goto laFailed;
	}


	if(GetWeaponKind() != (DWORD)pInfo->m_WeaponDefines && pInfo->m_WeaponDefines != INVALID_DWORD_ID)
	{
		dwTextID = (TEXTRES_NOT_STYLE_FOR_WEAPON);
		goto laFailed;
	}
	return TRUE;

laFailed:
	if(bOutput && dwTextID)
	{
		OUTPUTTIP(dwTextID);
	}
	return FALSE;
}


//------------------------------------------------------------------------------
BOOL Player::CheckStyleForWeapon(DWORD dwWeaponType)
{
	DWORD dwStyle =GetCurrentAttackStyle();

	sSTYLEINFO_BASE *pInfo;
	
	pInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)dwStyle);

	if(pInfo==NULL)
	{
		SetCurrentNomalAttackStyle(dwWeaponType);
		return FALSE;
	}
	else if((DWORD)pInfo->m_WeaponDefines != dwWeaponType)
	{
		SetCurrentNomalAttackStyle(dwWeaponType);
		return FALSE;
	}

	return TRUE;
}



//------------------------------------------------------------------------------
void Player::ProcessStabberShot(DWORD /*dwTick*/)
{
	//reserved

}




//------------------------------------------------------------------------------
void Player::SetName(const char * pszName)
{
	//m_pVObject->SetName(pszName);
	strcpy( m_PlayerCookie.GetCharInfo()->m_szCharName, pszName );
	_SUPER::SetName(pszName);
}


TCHAR * Player::GetName() const
{
	return m_pVObject->GetName();
}




DWORD Player::GetWeaponKind()
{
	//IDTYPE animationID = 0;	
	BaseSlot & slotDat = m_pEquipContainer->GetSlot(EQUIPPOS_WEAPON);

	if (slotDat.GetCode() == 0)
	{
		return WEAPONTYPE_BOW;
	}
	else
	{
		sITEMINFO_BASE *pItemInfo = theItemInfoParser.GetItemInfo(slotDat.GetCode());
		ASSERT(pItemInfo && "item invalid.");
		return pItemInfo->m_wType;
	}
}

eARMOUR_TEX_TYPE Player::GetArmourTexture()
{
	BaseSlot & slotDat = m_pEquipContainer->GetSlot(EQUIPPOS_ARMOR);

	if (!slotDat.GetCode())
		return ARMOUR_TEX_CLOTH;

	{
		sVITEMINFO_BASE *pVItemInfo = theItemInfoParser.GetVItemInfo(slotDat.GetCode());
		ASSERT(pVItemInfo && "无效的装备信息.");

		return (eARMOUR_TEX_TYPE)pVItemInfo->ARMOUR.m_byArmourTexture;
	}
}



sITEMINFO_BASE *Player::GetWeaponInfo()
{	
	BaseSlot & slotDat = m_pEquipContainer->GetSlot(EQUIPPOS_WEAPON);
	if (slotDat.GetCode() == 0)
	{
		return NULL;
	}
	else
	{
		sITEMINFO_BASE *pItemInfo = theItemInfoParser.GetItemInfo(slotDat.GetCode());
		ASSERT(pItemInfo && "item invalid.");
		return pItemInfo;
	}
}

float Player::GetMoveSpeedModifier()const
{
   float fMoveSpeedModifier = m_wMoveSpeedRatio / 100.0f;

   
   return fMoveSpeedModifier;
}

CODETYPE Player::GetCurrentVSkill()
{
	if(m_CurrentAction.ActionID == ACTION_ATTACK)
	{
		sSKILLINFO_COMMON*	pSkill;
		pSkill = theSkillInfoParser.GetInfo((SLOTCODE)m_dwAttackStyle);
		if(pSkill)
			return pSkill->m_VSkillCode;
	}
	return _SUPER::GetCurrentVSkill();
}


float Player::GetAttackSpeedModifier()const
{
	const float fWeaponMultiplier = 0.67f;

	float fFinalMultiplier = fWeaponMultiplier * m_wAttSpeedRatio / 100.0f;

	return fFinalMultiplier;

}


ePLAYER_TYPE	Player::GetClass()const
{
	if (!GetCharInfo())
	{
		return PLAYERTYPE_MAX;
	}

	return static_cast<ePLAYER_TYPE>(GetCharInfo()->m_byClassCode);
}

MONEY Player::GetMoney( void )const
{
	ASSERT(GetCharInfo());

	return GetCharInfo()->m_Money;
}


//------------------------------------------------------------------------------
void Player::SetMoney(MONEY Money)
{
	ASSERT(GetCharInfo());
	GetCharInfo()->m_Money = Money;
	AddUpdateFlag(CHAR_UPDATE_PACKINFO);
}

BOOL Player::MinusMoney( MONEY minus_money )
{
	SetMoney(GetMoney() - minus_money);
	return TRUE;
}


void Player::SetExp(DWORD dwExp)
{
	GetCharInfo()->m_dwExp = dwExp;
	AddUpdateFlag(CHAR_UPDATE_EXPINFO);
}


void Player::SetLevel(LEVELTYPE LV)
{
	GetCharInfo()->m_LV = LV;
	AddUpdateFlag(CHAR_UPDATE_ATTRIB);
}

void Player::SetHP(DWORD dwHP )
{
	GetCharInfo()->m_dwHP = dwHP;

	Character::SetHP(dwHP);
	AddUpdateFlag(CHAR_UPDATE_HPMPINFO);
}

void Player::SetMP(DWORD dwMP )
{
	GetCharInfo()->m_dwMP = dwMP;

	Character::SetMP(dwMP);
	AddUpdateFlag(CHAR_UPDATE_HPMPINFO);
}

DWORD Player::GetExp()const
{
	return GetCharInfo()->m_dwExp;
}


LEVELTYPE Player::GetLevel()const
{
	return GetCharInfo()->m_LV;
}


DWORD Player::GetHP()const
{
	return GetCharInfo()->m_dwHP;
}

DWORD Player::GetMP()const
{
	return GetCharInfo()->m_dwMP;
}


DWORD Player::GetMaxHP()const
{
	return GetCharInfo()->m_dwMaxHP;
}


DWORD Player::GetMaxMP()const
{
	return GetCharInfo()->m_dwMaxMP;
}


DWORD Player::GetNextExp()const
{
	return theFormularManager.GetExpAccumlate(GetLevel()+1);	
}

Attributes& Player::GetAttr()
{
	return *m_pPlayerAttributes;
}
const Attributes&	Player::GetAttr() const
{
	return *m_pPlayerAttributes;
}

void Player::SetVObject(VObject* pVObject)
{
	_SUPER::SetVObject(pVObject);
	m_pVPlayer = (VPlayer*)pVObject;
}


//------------------------------------------------------------------------------
int   Player::AddSkillEffect(SKILL_EFFECT *pEffect)
{
	int iID = Character::AddSkillEffect(pEffect);

	UpdateSkillAttributes();

	if(theHeroParty.GetMemberBy(GetObjectKey()) )
		AddUpdateFlag(CHAR_UPDATE_PARTY);

	return iID;

}

void  Player::DeleteSkillEffect(int iEffectID)
{
	Character::DeleteSkillEffect(iEffectID);

	UpdateSkillAttributes();
	

	return;
}

void  Player::UpdateSkillAttributes()
{
	if (!GetPlayerAttributes())
	{
		return;
	}

	AttributeAffectorSkillHandle SkillCalc( *GetPlayerAttributes() );

	SkillCalc.Clear();

	UpdateActiveSkillEffectAttr(SkillCalc);	
	ApplyStyleAttributes(GetCurrentAttackStyle(),SkillCalc);

	GetPlayerAttributes()->Update();

	AddUpdateFlag(CHAR_UPDATE_ATTRIB | CHAR_UPDATE_HPMPINFO);

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Player::UpdateActiveSkillEffectAttr(AttributeAffectorSkillHandle &SkillCalc)
{

	SKILL_EFFECT_LIST::iterator iter;
	iter = m_SkillEffectList.begin();

	

	////////////////////////////////////////////////////
	while (iter != m_SkillEffectList.end())
	{
		SKILL_EFFECT&		pEffect = *(*iter);
		SkillDetailInfo *	pInfo;
		DWORD					dwAbilityID = pEffect->dwAbilityID;
		
		pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)pEffect->dwSkillID);

		if (pInfo)
		{
			if (pEffect->AbilityInfo.m_wAbilityID == dwAbilityID)
			{
				eATTRIBUTE_TYPE eType = theSkillInfoParser.GetAttrType( pEffect->AbilityInfo);

				if( eType == ATTRIBUTE_CUR_HP )
				{
				}
				else if( eType == ATTRIBUTE_CUR_MP )
				{
				}
				else if( eType != ATTRIBUTE_TYPE_INVALID )
				{
					//int iAddValue =
						SkillCalc.AddAttr	( eType
												, pEffect->AbilityInfo.m_iParam[0]	
												, pEffect->AbilityInfo.m_iParam[1] );	
				}
			}
		}
		iter++;
	}


}







void Player::OnDamage	(DWORD                dwAdditionalEffect
                        ,BOOL                 bRight
								,eWEAPONSOUNDKIND     eSoundKind
								,eARMOUR_TEX_TYPE eArmourTex
								,DWORD                dwSkillCode)
{
	Character::OnDamage(dwAdditionalEffect,bRight,eSoundKind,eArmourTex,dwSkillCode);
	SetAttackStandTime(ATTACK_STAND_TIME);

	if (dwAdditionalEffect & ATTACK_ADDITIONAL_EFFECT_TOGROUND)
	{
		DownToGround();
	}

	CreateDamageEffect	(dwAdditionalEffect & (ATTACK_ADDITIONAL_EFFECT_CRITICAL)
                        ,bRight)		;	


	//if ( !(rand() % 3) )
	if(util::DrawLots(theGeneralGameParam.GetPlayDamageSoundRate() ) )
	{
		PlayDamageSound(eSoundKind, eArmourTex);														
	}

	//if ( (rand() % 4) )
	if(util::DrawLots(theGeneralGameParam.GetPlayDamageAnimRate()) )
	{
		switch (GetCurrentState())
		{
		case STATE_SKILL:
		case STATE_DEATH:
			break;
		default:
			PlayDamageAnimation(bRight);							
			break;
		}
	}

	if(GetCurrentState()==STATE_SIT || GetCurrentState()==STATE_EMOTICON)
	{
		SetNextState(STATE_IDLE,g_CurTime);	
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID	Player::LearnSkill	(LPCSTR /*szSkillName*/, INT /*nLevel*/ )
{
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void   Player::OnUseItem(DWORD dwItemCode)
{
	sITEMINFO_BASE *pItemInfo = theItemInfoParser.GetItemInfo(dwItemCode);
	if (!pItemInfo)
	{
		return;
	}

	DWORD dwFlag = CHAR_UPDATE_ATTRIB | CHAR_UPDATE_HPMPINFO;

	if (pItemInfo->IsCanUseWaste())
	{
		if (pItemInfo->m_wHealHP)
		{
			//#pragma message(__FILE__  "(2784) pItemInfo->m_wHealHP播放吃药特效...  " )
			m_pVCharacter->OnUseItem(pItemInfo->m_byWasteType);
		}
	}
	else
	{
		dwFlag |= CHAR_UPDATE_EQUIPMENTS;
	}

	AddUpdateFlag(dwFlag);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Player::OnLevelUp	(DWORD dwLevel
                        ,DWORD dwHP
								,DWORD dwMP
								,DWORD /*dwRemainStat*/
								,DWORD /*dwRemainSkill*/)
{
	SetLevel((LEVELTYPE)dwLevel);
	OUTPUTCHATF(TEXTRES_PLAYER_LEVEL_UP, GetName());

	SetHP( dwHP );
	SetMP( dwMP );


	if (!IsHero())
	{
		Vector3D vPos = GetPosition();
		theSoundEffect.Play(666,&vPos);
	}

	UpdateSkillAttributes();

	AddUpdateFlag(CHAR_UPDATE_LEVELUP | CHAR_UPDATE_HPMPINFO | CHAR_UPDATE_ATTRIB);
}


void Player::OnRevive(const Vector3D& vCurPos, DWORD dwHP, DWORD dwMP,DWORD dwLevel)
{
	_SUPER::OnRevive( vCurPos,  dwHP,  dwMP, dwLevel);
	//SetLevel((LEVELTYPE)dwLevel);
	OUTPUTCHATF(TEXTRES_PLAYER_RESURRECTION_SUCCEED, GetName());

	//SetHP( dwHP );
	//SetMP( dwMP );


	UpdateSkillAttributes();

	AddUpdateFlag(CHAR_UPDATE_LEVELUP | CHAR_UPDATE_HPMPINFO | CHAR_UPDATE_ATTRIB);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Player::ApplyStyleAttributes	(DWORD                        dwStyleCode
                                    ,AttributeAffectorSkillHandle &SkillCalc)
{
	sSTYLEINFO_BASE *pBaseStylenfo;
	
	pBaseStylenfo = theSkillInfoParser.GetStyleInfo( (SLOTCODE)dwStyleCode );
	if( !pBaseStylenfo )	
		return;

	if( pBaseStylenfo->m_iAttackRate )
		SkillCalc.AddAttr(ATTRIBUTE_PHYSICAL_ATTACK_SUCCESS_RATIO
                       ,VALUE_TYPE_VALUE
							  ,pBaseStylenfo->m_iAttackRate);

	if( pBaseStylenfo->m_iAvoidRate )
		SkillCalc.AddAttr(ATTRIBUTE_PHYSICAL_ATTACK_BLOCK_RATIO
                       ,VALUE_TYPE_VALUE
							  ,pBaseStylenfo->m_iAvoidRate);

	if( pBaseStylenfo->m_iAttackSpeed )
		SkillCalc.AddAttr(ATTRIBUTE_ATTACK_SPEED
                       ,VALUE_TYPE_VALUE
							  ,pBaseStylenfo->m_iAttackSpeed);

	if( pBaseStylenfo->m_iBonusDefence )
		SkillCalc.AddAttr(ATTRIBUTE_OPTION_ALL_DEFENSE_POWER
                       ,VALUE_TYPE_VALUE
							  ,pBaseStylenfo->m_iBonusDefence);
}


void Player::AddSummonId(const DWORD &dwSummonId)
{
	m_SummonIdArray.Append(dwSummonId);
}


BOOL Player::RemoveSummonId(const DWORD &dwSummonId)
{
	for(int i=0;i<m_SummonIdArray.Size();i++)
	{
		if(m_SummonIdArray[i]==dwSummonId)
		{
			m_SummonIdArray.EraseQuick(i);
			return TRUE;
		}
	}

	return FALSE;
}

DWORD Player::GetSummonId()const
{
	if(m_SummonIdArray.Size())
		return m_SummonIdArray.Front();

	return INVALID_DWORD_ID;
}


void Player::SetGuildInfo(sGUILDINFO_RENDER* pGuildInfo)
{
    if (pGuildInfo)
    {
        if (pGuildInfo->m_byExistGuild)
        {
            m_GuildInfo.m_byExistGuild = 1;
            m_GuildInfo.m_GuildMarkIdx = pGuildInfo->m_GuildMarkIdx;
            Sprintf (m_GuildInfo.m_szGuildName, "%s", pGuildInfo->m_szGuildName);
        }
        else
        {
            m_GuildInfo.m_byExistGuild = 0;
        }
    }
    else
    {
        m_GuildInfo.m_byExistGuild = 0;
    }
}

void Player::SetVendorInfo (sVENDORINFO_RENDER* pInfo)
{
    if (pInfo && pInfo->m_byTitleLen)
    {
		 SetVendorTitle(pInfo->m_pszTitle);
    }
    else
    {
    }
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
float Player::GetAttackRange( void )
{

	const float BOW_ATTACK_RANGE = 2.0f;
	
	BaseSlot & slotDat = m_pEquipContainer->GetSlot(EQUIPPOS_WEAPON);

	if (slotDat.GetCode() == 0)
	{
		return BOW_ATTACK_RANGE;
	}
	else
	{
		sITEMINFO_BASE *pItemInfo;
		
		pItemInfo = theItemInfoParser.GetItemInfo(slotDat.GetCode());
		ASSERT(pItemInfo && "item invalid");
		return (float)pItemInfo->m_wAttRange;
	}

}

BOOL Player::IsNormalRangedAttack()
{
	switch (GetWeaponKind())
	{
	case WEAPONTYPE_CROSSBOW:
	case WEAPONTYPE_ETHERWEAPON:
	case WEAPONTYPE_ORB:
	case WEAPONTYPE_BOW:
		{
			return TRUE;		
		}
		break;
	default:
		{
			return FALSE;
		}
		break;	
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
float Player::GetMeshHeight(float /*fExtra*/)
{
	sPLAYER_HEIGHTINFO *pInfo = thePlayerHeightInfoParser.GetHeroHeightInfo(GetClass());

	ASSERT(m_iHeightVariation >= 0 && m_iHeightVariation < MAX_HEIGHT_VARIATION);

	float fScale = 1.0f;

	if (pInfo)
	{
		fScale = pInfo->m_fHeight[m_iHeightVariation];

		switch(GetClass())
		{
		case PLAYERTYPE_WARRIOR:
			return 2.2f * fScale;
			break;
		case PLAYERTYPE_PALADIN:
			return 2.0f * fScale;
			break;
		case PLAYERTYPE_POWWOW:
			return 2.0f * fScale;
			break;
		case PLAYERTYPE_STABBER:
			return 1.75f * fScale;
			break;
		case PLAYERTYPE_NECROMANCER:
			return 1.75f * fScale;
			break;
		}
	}

	return _SUPER::GetMeshHeight();
}

void Player::OnFootStep()
{
}


void Player::ProcessStandAnimationDelay(DWORD dwTick)
{
	if (m_dwStandAnimDelay < dwTick)
	{
		m_dwStandAnimDelay = 0;

	}
	else {
		m_dwStandAnimDelay -= dwTick;
	}


}

void Player::SetStandAnimationDelay()
{
	
	const int STAND_ANIMATION_MINIMUM_DELAY	= 8000;
	const int STAND_ANIMATION_DELAY_VARIATION = 7000;

	m_dwStandAnimDelay = STAND_ANIMATION_MINIMUM_DELAY + rand() % STAND_ANIMATION_DELAY_VARIATION;


}


int   Player::DecreaseBulletCount()
{
	m_iBulletCount--;

	return m_iBulletCount;
}


void  Player::StabberReload( BOOL /*bAnimation*/ )
{

	//IDTYPE ReloadAnimID = GetStabberReloadAnimation();
	//sANIMATION_INFO *pAniInfo = GetRenderPart()->GetUnitDrawBase()->GetAnimationInfo(ReloadAnimID);

	//m_iStabberShotDelay = 300;

	//if (m_dwStabberShotSerial)	
	//{
	//			ATTACK_RESULT* pResult = GetAttackResultBy(m_dwStabberShotSerial);
	//			if(pResult)
	//			{
	//				ProcessAttackResult(m_dwStabberShotSerial,TRUE);
	//				pResult->SetActioned();
	//			}
	//	m_dwStabberShotSerial = 0;
	//}

	//if (pAniInfo)
	//{
	//	m_iStabberShotDelay = (int)((float)pAniInfo->m_nMaxFrame * 33.0f); 
	//}

	//if( bAnimation )
	//{
	//	SetCurrentBlendAnimation(ReloadAnimID,GetShotBlendBone());
	//}
	//m_bStabberReload = TRUE;
}

void Player::SetMaxHP( DWORD iHP )
{
	GetCharInfo()->m_dwMaxHP = iHP;
	AddUpdateFlag(CHAR_UPDATE_HPMPINFO);
	
	Character::SetMaxHP(iHP);
}
void Player::SetMaxMP( DWORD iMP )
{
	GetCharInfo()->m_dwMaxMP = iMP;
	AddUpdateFlag(CHAR_UPDATE_HPMPINFO);

    Character::SetMaxHP(iMP);;
}
void Player::SetAttackSpeedRatio(int iSpeed)
{
    m_wAttSpeedRatio = (WORD)iSpeed;
    Character::SetAttackSpeedRatio(iSpeed);
}
void Player::SetMoveSpeedRatio(int iSpeed)
{
	m_wMoveSpeedRatio = (WORD)iSpeed;
    Character::SetMoveSpeedRatio(iSpeed);
}

//void Player::LaunchStabberWeapon()
//{
//}


void Player::OnShotStart()
{
	OpenCrossbowAnim();
	LaunchStabberWeapon();
}

void Player::OnShotEnd()
{
	CloseCrossbowAnim();
}


void Player::OnParalyzed()
{
	// 虐焊靛捞悼档肛勉
	ResetMoveFlag();
	Character::OnParalyzed();
}

void Player::OnShotFire(int /*iShotCount*/)
{
	//switch(GetWeaponKind())
	//{
	//case WEAPONTYPE_CROSSBOW:
	//	{
	//		CreateEffect(,GetArrowBonePos(iShotCount));
	//		SetAttachmentAnimation(iShotCount,STR2TAG("N001"),FALSE);
	//	}
	//	break;
	//case WEAPONTYPE_ETHERWEAPON:
	//	break;
	//}

}

void Player::HideHelmet(BOOL bFlag,BOOL bRefresh)
{
	if (m_bHideHelmet != bFlag)
	{
		m_bHideHelmet = bFlag;
		if (bRefresh) 
		{
			UpdateAppearance();
		}
	}
}


void Player::SetObserverMode( BOOL bObserverMode, BOOL /*IsHero*/ )
{
	m_bObserverMode = bObserverMode;
	SetShow( !m_bObserverMode );
}



BOOL Player::CanBeAttackTarget()
{
	BOOL bFlag = Character::CanBeAttackTarget();

	if (bFlag)
	{
		if (IsInvincible())
		{
			bFlag = FALSE;
		}
	}
	return bFlag;
}



#ifndef NONE_MONSTERTESTING
BOOL Player::SendPacket( MSG_OBJECT_BASE* pMsg, DWORD /*dwSize*/)
{
	assert(!theGeneralGameParam.IsEnableNetwork());
	theNetworkLayer.ParsePacket(pMsg);
	return TRUE;
}
#endif


int     Player::GetPartVariation(int iPart)
{

	if (iPart < 0 || iPart >= PLAYER_VARIATION_MAX)
	{
		return 0;
	}

	return m_arPartVariation[iPart];

}

void    Player::SetPartVariation(int iPart,int iVariation,BOOL bRefresh)
{
	if (iPart < 0 || iPart >= PLAYER_VARIATION_MAX)
	{
		return;
	}

	m_arPartVariation[iPart] = iVariation;

	switch (iPart)
	{
	case PLAYER_VARIATION_HAIR:
		GetCharInfo()->m_byHair = (BYTE)iVariation;
		break;
	case PLAYER_VARIATION_FACE:
		GetCharInfo()->m_byFace = (BYTE)iVariation;
		break;
	}

	if (bRefresh)
	{
		UpdateAppearance();
	}

}

int   Player::GetHeightVariation()
{
	return m_iHeightVariation;

}

void  Player::SetHeightVariation(int iIndex)
{
	m_iHeightVariation = iIndex;

	if (m_iHeightVariation >= MAX_HEIGHT_VARIATION)
	{
		m_iHeightVariation = MAX_HEIGHT_VARIATION - 1;

	}

	if (m_iHeightVariation < 0)
	{
		m_iHeightVariation = 0;
	}

	GetCharInfo()->m_byHeight = (BYTE)m_iHeightVariation;

}


void  Player::SetHPInfo(HPINFO *pInfo)
{

	Character::SetHPInfo(pInfo);

	GetCharInfo()->m_LV				= (LEVELTYPE)pInfo->m_LV;	
	GetCharInfo()->m_dwHP 			= pInfo->m_HP;
	GetCharInfo()->m_dwMaxHP		= pInfo->m_MaxHP;
	
	SetHP( pInfo->m_HP );
	

	m_bExistHPInfo = TRUE;
}

DWORD	Player::GetSex()const
{
	return GetCharInfo()->m_bySex;
}



int Player::GetShotCount()
{
	if (GetWeaponKind() == ITEMTYPE_CROSSBOW)
	{
		return 2;
	}
	else
	{
		return 1;
	}
}


};//namespace object

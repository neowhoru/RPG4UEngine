/*////////////////////////////////////////////////////////////////////////
文 件 名：HeroData.cpp
创建日期：2007年12月24日
最后更新：2007年12月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "HeroData.h"
#include "Hero.h"
#include "TableTextFile.h"
#include "WorldConst.h"
#include "FormularManager.h"
#include "SkillEffectManager.h"
#include "ConstItem.h"
#include "ConstSlot.h"
#include "Archive.h"
#include "GameParameter.h"
#include "QuestManager.h"
#include "IScriptCharacter.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace object;
#ifndef _SERVER
GLOBALINST_SINGLETON_IMPL(HeroData, ()  , gamemain::eInstPrioGameFunc);
#endif

namespace object
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
HeroData::HeroData()
{
	m_dwGameTime				= 0;
	m_bLevelChanged			= FALSE;
	m_bLoadStreamWhileNull	= FALSE;
	CreateDefaultData();
}
HeroData::~HeroData()
{
}

void HeroData::CreateDefaultData()
{
	ZeroMemory(&m_PlayerInfo,sizeof(m_PlayerInfo));

	m_PlayerInfo.m_bHideHelmet			= FALSE;
	m_PlayerInfo.m_byGMGrade				= 0;
	m_PlayerInfo.m_byFirstEnterWorld	= TRUE;

	sPLAYERINFO_BASE&	playerInfo	= m_PlayerInfo.m_CharInfo;


	playerInfo.m_byClassCode = PLAYERTYPE_WARRIOR;
	playerInfo.m_dwHP		= 777;
	playerInfo.m_dwMP		= 777;
	playerInfo.m_dwMaxHP	= 777;
	playerInfo.m_dwMaxMP	= 777;
	playerInfo.m_LV				= 12;
	playerInfo.m_dwExp			= 12000;
	playerInfo.m_sSpirit			= 120;
	playerInfo.m_sStrength		= 120;
	playerInfo.m_sVitality		= 120;
	playerInfo.m_sInteligence	= 120;
	playerInfo.m_sDexterity		= 120;
	playerInfo.m_sLearn			= 120;
	playerInfo.m_sCredit			= 120;

	playerInfo.m_bySlot			= 0;
	playerInfo.m_bySex			= SEX_MALE;
	playerInfo.m_byHair			= 2;
	playerInfo.m_byHairColor	= 2;
	playerInfo.m_byHeight		= 10;
	playerInfo.m_byFace			= 2;

	playerInfo.m_dwRegion		= theGeneralGameParam.GetStartMapID();
	playerInfo.m_sLocationX		= 590;
	playerInfo.m_sLocationY		= 610;
	playerInfo.m_sLocationZ		= 11;

	strcpy(playerInfo.m_szCharName, "本地测试");
}


//BOOL HeroData::LoadLocalData(LPCSTR szFileInfo)
//{
//	util::TableTextFile resFile;
//	__CHECK(resFile.OpenFile(szFileInfo,TRUE));
//
//	sPLAYERINFO_BASE&	playerInfo	= m_PlayerInfo.m_CharInfo;
//	ZeroMemory(&m_PlayerInfo,sizeof(m_PlayerInfo));
//	ZeroMemory(&m_WareHouseData,sizeof(m_WareHouseData));
//	m_WareHouseMoney	= 0;
//
//	//INT		nCount;
//	//INT		nCounter;
//	//INT		nDupCount;
//	//CODETYPE	itemID;
//	//SLOTPOS	itemPos;
//	//DWORD		dwSerial(0);
//
//	if(resFile.GotoNextLine())
//	{
//		resFile.GetNextNumber();	///跳过第1列空格
//
//		resFile >> playerInfo.m_UserGuid;;
//		resFile >> playerInfo.m_bySlot;;
//		resFile >> playerInfo.m_byClassCode;;
//		resFile >> playerInfo.m_CharGuid;;
//		resFile >> playerInfo.m_szCharName;;
//		resFile >> playerInfo.m_LV;;
//		resFile >> playerInfo.m_dwExp;;
//		resFile >> playerInfo.m_dwMaxHP;;
//		resFile >> playerInfo.m_dwHP;;
//		resFile >> playerInfo.m_dwMaxMP;;
//		resFile >> playerInfo.m_dwMP;;
//		resFile >> playerInfo.m_Money;;
//		resFile >> playerInfo.m_dwRemainStat;
//		resFile >> playerInfo.m_dwRemainSkill;;
//		resFile >> playerInfo.m_wSelectStyleCode;;
//		resFile >> playerInfo.m_byPKState;;
//		resFile >> playerInfo.m_byCharState;;
//		resFile >> playerInfo.m_StateTime;;
//		resFile >> playerInfo.m_dwRegion;;
//		resFile >> playerInfo.m_sLocationX;;
//		resFile >> playerInfo.m_sLocationY;;
//		resFile >> playerInfo.m_sLocationZ;;
//		resFile >> playerInfo.m_szTitleID;;
//		resFile >> playerInfo.m_TitleTime;;
//		resFile >> playerInfo.m_bInventoryLock;;
//		resFile >> playerInfo.m_sStrength;;
//		resFile >> playerInfo.m_sDexterity;;
//		resFile >> playerInfo.m_sVitality;;
//		resFile >> playerInfo.m_sInteligence;;
//		resFile >> playerInfo.m_sSpirit;;
//		resFile >> playerInfo.m_sLearn;;
//		resFile >> playerInfo.m_sCredit;;
//		resFile >> playerInfo.m_sSkillStat1;;
//		resFile >> playerInfo.m_sSkillStat2;;
//		resFile >> playerInfo.m_byHeight;;
//		resFile >> playerInfo.m_byFace;;
//		resFile >> playerInfo.m_byHair;;
//		resFile >> playerInfo.m_byHairColor;	//leo sex;
//		resFile >> playerInfo.m_bySex;			//leo;
//		resFile >> playerInfo.m_PlayLimitedTime;;
//
//
//		resFile >> playerInfo.m_dwPVPPoint;;
//		resFile >> playerInfo.m_dwPVPScore;;
//		resFile >> playerInfo.m_byPVPGrade;;
//		//resFile >> playerInfo.	m_dwPVPRanking;;
//		resFile >> playerInfo.m_dwPVPMaxSeries;;
//		resFile >> playerInfo.m_dwPVPMaxKill;;
//		resFile >> playerInfo.m_dwPVPMaxDie;;
//		resFile >> playerInfo.m_dwPVPTotalKill;;
//		resFile >> playerInfo.m_dwPVPTotalDie;;
//		resFile >> playerInfo.m_dwPVPTotalDraw;;
//
//		resFile >> playerInfo.	m_UserPoint;;
//		resFile >> playerInfo.m_byInvisibleOptFlag;	
//
//
//		resFile >> playerInfo.	m_GuildGuid;;
//		resFile >> playerInfo.m_szGuildName;	
//		resFile >> playerInfo.m_GuildPosition;;
//		resFile >> playerInfo.m_szGuildNickName;
//
//		resFile >> m_WareHouseMoney;
//
//		resFile >> m_PlayerInfo.m_byFirstEnterWorld;
//
//	}//if(resFile.GotoNextLine())
//
//	DWORD curAccumExp		= theFormularManager.GetExpAccumlate(playerInfo.m_LV);
//	DWORD nextAccumExp	= theFormularManager.GetExpAccumlate(playerInfo.m_LV+1);
//
//	playerInfo.m_dwExp	= min(playerInfo.m_dwExp, 100);
//	playerInfo.m_dwExp	= playerInfo.m_dwExp*(nextAccumExp-curAccumExp)/100;
//	playerInfo.m_dwExp	+= curAccumExp;
//
//
//	return TRUE;
//}
//
//
//BOOL HeroData::LoadItemData(LPCSTR szFileInfo)
//{
//	util::TableTextFile resFile;
//	__CHECK(resFile.OpenFile(szFileInfo,TRUE));
//
//	DWORD		dwSerial(0);
//
//	m_arStateInfos.clear();
//
//	for(;resFile.GotoNextLine();)
//	{
//		SLOTINDEX		slotIdx;
//		//SLOTPOS		slotPos;
//		DWORD			slotPos;
//		INT			nSocketNum;
//		sSOCKET_INFO	arSockets[SOCKET_MAX]={0};		
//		DWORD			enchant;
//		//BYTE			enchant;
//		BYTE			potential;
//		BYTE			potentialUsed;
//		BYTE			potentialDat;
//		BYTE			rank;
//		BYTE			rank0;
//		INT			itemID;
//		INT			nData;
//		INT			nDupCount;
//
///*////////////////////////////////////////////////////////////////////////
//ItemID	注释	源容器2	槽位置2	Enchant	Rank	RankDat	SocketNum	type	optionNo	Lvl	Att	Att2	Def	type	optionNo	Lvl	Main	Rand1No	Rand1	Rand2No	Rand2	Enchant	Rank	RankDat
///*////////////////////////////////////////////////////////////////////////
//
//		resFile.GetInt();
//		resFile >> itemID;
//						if(itemID == 0)	continue;
//
//						resFile.GetString(); //注释
//		resFile >> slotIdx;
//		resFile >> slotPos;
//		resFile >> nDupCount;
//		resFile >> enchant;
//		resFile >> rank;
//		resFile >> rank0;
//		resFile >> potential;
//		resFile >> potentialUsed;
//		resFile >> potentialDat;
//
//		resFile >> nSocketNum;
//
//
//		/////////////////////////////////////////////////////////
//		//镶嵌石信息
//		if(nSocketNum > 0)
//		{
//			INT n=0;
//			resFile >> nData;			arSockets[n].SocketType		= eSOCKET_TYPE_DOUBLE;
//			resFile >> nData;			arSockets[n].OptionIndex	= nData;
//			resFile >> nData;			arSockets[n].LimitLevel		= nData;
//
//			//if(arSockets[n].SocketType == eSOCKET_TYPE_DOUBLE)
//			{
//				resFile >> nData;		 arSockets[n].DOUBLE.AttackAttr	= nData;
//				resFile >> nData;		 arSockets[n].DOUBLE.AttackAttr2 = nData;
//				resFile >> nData;		 arSockets[n].DOUBLE.DefenceAttr = nData;
//			}
//		}
//
//		if(nSocketNum > 1)
//		{
//			INT n=1;
//			resFile >> nData;			arSockets[n].SocketType		= eSOCKET_TYPE_RANDOM;
//			resFile >> nData;			arSockets[n].OptionIndex	= nData;
//			resFile >> nData;			arSockets[n].LimitLevel		= nData;
//			//else if(arSockets[n].SocketType == eSOCKET_TYPE_RANDOM)
//			{
//				resFile >> nData;		 arSockets[n].RANDOM.MainAttr			= nData;
//				resFile >> nData;		 arSockets[n].RANDOM.RandomAttrNo	= nData;
//				resFile >> nData;		 arSockets[n].RANDOM.RandomAttr		= nData;
//				resFile >> nData;		 arSockets[n].RANDOM.RandomAttr2No	= nData;
//				resFile >> nData;		 arSockets[n].RANDOM.RandomAttr2		= nData;
//			}
//		}
//			//resFile(0);
//
//		///////////////////////////////////////////////////////
//		//存在到Container中
//		sITEM_SLOTEX*	pContainer(NULL);
//		BYTE*			pNum(NULL);
//		DWORD			dwNum(0);
//		switch(slotIdx)
//		{
//		case 100:
//			{
//		//处理附加状态
//				SStateInfo state;
//				state.dwStatusID	= itemID;
//				state.dwDuration	= slotPos;
//				state.dwSkillID	= nDupCount;
//				state.dwAbilityID	= enchant;
//				m_arStateInfos.push_back(state);
//
//			}break;
//		case SI_INVENTORY:		
//			{
//				pContainer	= &m_PlayerInfo.m_InventoryTotalInfo.m_Slot[0];
//				pNum			= &m_PlayerInfo.m_InventoryTotalInfo.m_InvenCount;
//				dwNum			= MAX_INVENTORY_SLOT_NUM;
//			}break;
//		case SI_INVENTORY2:
//			{
//				pContainer	= &m_PlayerInfo.m_InventoryTotalInfo.m_Slot[m_PlayerInfo.m_InventoryTotalInfo.m_InvenCount];
//				pNum			= &m_PlayerInfo.m_InventoryTotalInfo.m_TmpInvenCount;
//				dwNum			= MAX_TEMPINVENTORY_SLOT_NUM;
//			}	break;
//		case SI_EQUIPMENT:	
//			{
//				pContainer	= &m_PlayerInfo.m_EquiptItemInfo.m_Slot[0];	
//				pNum			= &m_PlayerInfo.m_EquiptItemInfo.m_Count;
//				dwNum			= MAX_EQUIPMENT_SLOT_NUM;
//			}break;
//		case SI_WAREHOUSE:
//			{
//				pContainer	= &m_WareHouseData.m_Slot[0];		
//				pNum			= &m_WareHouseData.m_Count;
//				dwNum			= MAX_WAREHOUSE_SLOT_NUM;
//			}break;
//
//		case SI_SKILL:
//			{
//				assert(slotPos < MAX_SKILL_SLOT_NUM);
//				pContainer	= NULL;
//
//				INT nIndex = m_PlayerInfo.m_SkillTotalInfo.m_Count;
//
//				sSKILL_SLOT& slot					= m_PlayerInfo.m_SkillTotalInfo.m_Slot[nIndex];
//				slot.m_SkillPos					= (SLOTPOS)slotPos;
//				slot.m_Stream.SkillPart.wCode	= (WORD)itemID;
//				m_PlayerInfo.m_SkillTotalInfo.m_Count++;
//				continue;
//			}break;
//
//		case SI_QUICK:
//			{
//				pContainer	= NULL;
//				//处理Quick
//				SLOTINDEX	slotFrom = (SLOTINDEX)nDupCount;
//				SLOTPOS	posFrom	= (SLOTPOS)enchant;
//
//				INT nIndex = m_PlayerInfo.m_QuickTotalInfo.m_Count;
//
//				assert(slotPos < MAX_QUICK_SLOT_NUM);
//				sQUICK_SLOT& slot = m_PlayerInfo.m_QuickTotalInfo.m_Slot[nIndex];
//				slot.m_QuickPos						= (SLOTPOS)slotPos;
//				slot.m_Stream.QuickPart.SlotIdx	= slotFrom;
//				slot.m_Stream.QuickPart.Code		= (SLOTCODE)itemID;
//				slot.m_Stream.QuickPart.Pos		= posFrom;
//
//				m_PlayerInfo.m_QuickTotalInfo.m_Count++;
//				continue;
//			}break;
//
//		case SI_STYLE:
//			{
//			}break;
//
//		default:				
//			{
//				pContainer = NULL;	
//			}break;
//		}
//
//
//		///////////////////////////////////////////
//		if(pContainer)
//		{
//			assert(slotPos < dwNum);
//			sITEM_SLOTEX& slot = pContainer[*pNum];
//			slot.m_ItemPos	= (SLOTPOS)slotPos;
//			slot.m_Stream.ItemPart.byDura		= (BYTE)nDupCount;
//			slot.m_Stream.ItemPart.wCode		= (WORD)itemID;
//			slot.m_Stream.ItemPart.dwSerial	= dwSerial++;
//
//			//if(pContainer[n].m_ItemPos == slotPos)
//			{
//				sOPTION_INFO& opt = slot.m_Stream.OptionPart;
//				opt.SocketNum		= nSocketNum;
//
//				for(INT n=0; n<SOCKET_MAX; n++)
//					opt.SocketOption[n]	= arSockets[n];
//
//				opt.Enchant			= enchant;
//				opt.Rank				= rank;
//				opt.RankOption1	= rank0;
//				opt.RankOption2	= 54;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption3	= 55;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption4	= 56;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption5	= 57;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption6	= 58;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption7	= 59;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption8	= 60;//rand()%MAX_ITEM_OPTIONS;
//				opt.RankOption9	= 61;//rand()%MAX_ITEM_OPTIONS;
//
//				opt.Potential		= potential;
//				opt.PotentialUsed	= potentialUsed;
//				opt.PotentialOpt1	= potentialDat;
//				opt.PotentialOpt2	= potentialDat;
//				opt.PotentialOpt3	= potentialDat;
//			}
//			(*pNum) ++;
//		}//if(pContainer)
//	}//for(;resFile.GotoNextLine();)
//	resFile.CloseFile();
//
//	return TRUE;
//}


BOOL HeroData::LoadQuestData(LPCSTR szFileInfo)
{
	util::TableTextFile resFile;
	__CHECK(resFile.OpenFile(szFileInfo,TRUE));

	//DWORD		dwSerial(0);
	//__ZERO(m_VarBufs);

	INT nQuestID;
	INT nQuestDat;
	for(;resFile.GotoNextLine();)
	{
					resFile.GetInt();
		resFile >> nQuestID;
		resFile >> nQuestDat;

		if(m_QuestStates.find(nQuestID) == m_QuestStates.end())
			m_QuestStates[nQuestID] = nQuestDat;
		//if(nQuestID >= 0 && nQuestID < MAX_PLAYERVAR_SIZE)
		//{
		//	m_VarBufs[nQuestID] = (BYTE)(char)nQuestDat;
		//}
	}
	resFile.CloseFile();
	return TRUE;
}

BOOL HeroData::LoadGameData(LPCSTR szFileInfo)
{
	Archive	file;

	__CHECK(file.LoadFile(szFileInfo));

	__CHECK(file.GetBufferSize() > sizeof(m_PlayerInfo) + sizeof(m_arVarBufs));

	file.Read(&m_PlayerInfo, sizeof(m_PlayerInfo));
	file.Read(m_arVarBufs, sizeof(m_arVarBufs));

	file >> m_dwGameTime;


	return TRUE;
}

BOOL HeroData::SaveGameData(LPCSTR szFileInfo)
{
	Archive	file;

	__CHECK(file.SaveFile(szFileInfo,TRUE));

	SaveHero(theHero);

	file.Write(&m_PlayerInfo, sizeof(m_PlayerInfo));
	file.Write(m_arVarBufs, sizeof(m_arVarBufs));

	file << theGameWorld.GetGameTime();

	return TRUE;
}


BOOL HeroData::SaveQuestData(LPCSTR szFileInfo)
{
	Archive	file;

	__CHECK(file.SaveFile(szFileInfo, TRUE));

	file.WriteText(
				"//	本地Quest数据	\n"
				"//						\n"
				"//	支持：	李亦	\n"
				"//						\n"
				"//						\n"
				"//						\n"
				"//						\n"
				"//						\n"
				"//	QuestID	State	\n"
				);

	sHERO_QUESTINFO*	pQuest;
	//INT nValue;
	//for(INT n=0; n<MAX_PLAYERVAR_SIZE; n++)
	theHero.SetFirstQuestState();
	for(;pQuest = theHero.GetNextQuestState();)
	{
		if(theQuestManager.LoadQuest(pQuest->m_QuestID) == NULL)
			continue;
		file.WriteText("\t%d\t%d\n",pQuest->m_QuestID,(INT)pQuest->m_QuestData);
	}
	file.Close();

	return TRUE;
}


void HeroData::AddEffectInfosTo	(Character* pChar)
{
	assert(pChar);
	for(UINT n=0; n<m_arStateInfos.size();n++)
	{

		SKILL_EFFECT& pEffect	= *theSkillEffectManager.CreateSkillEffect();	
		pEffect->dwSkillID	= m_arStateInfos[n].dwSkillID;
		pEffect->dwStatusID	= m_arStateInfos[n].dwStatusID;	
		pEffect->iRemainTime	= m_arStateInfos[n].dwDuration;
		pEffect->dwDuration	= m_arStateInfos[n].dwDuration;
		pEffect->dwAbilityID	= m_arStateInfos[n].dwAbilityID;

		pChar->AddSkillEffect(&pEffect);

	}
}


void HeroData::SetHeroInfo( sPLAYER_LOCALDATA * pHeroInfo )
{
	ASSERT(pHeroInfo);
	m_PlayerInfo = *pHeroInfo;


}

void HeroData::SaveHero(Hero&  hero)
{
	m_PlayerInfo.m_dwKey		= hero.GetObjectKey();
	m_PlayerInfo.m_MakeType	= hero.GetPlayerType();
	memcpy(&m_PlayerInfo.m_CharInfo, hero.GetCharInfo(), sizeof(m_PlayerInfo.m_CharInfo));

	hero.GetEquipItemTotalInfo(m_PlayerInfo.m_EquiptItemInfo);

	hero.GetInventoryTotalInfo(m_PlayerInfo.m_InventoryTotalInfo);
	hero.GetSkillTotalInfo(m_PlayerInfo.m_SkillTotalInfo);	
	hero.GetQuickTotalInfo(m_PlayerInfo.m_QuickTotalInfo);	

#if !defined( _USE_AUTO_STYLE_QUICK )
	hero.GetStyleTotalInfo(m_PlayerInfo.m_StyleTotalInfo);
#endif

	m_PlayerInfo.m_byGMGrade		= hero.GetGMGrade();
	m_PlayerInfo.m_bHideHelmet	= (BYTE)hero.IsHideHelmet();
	//m_byFirstEnterWorld

	Vector3D	vPos = theHero.GetPosition();

	m_PlayerInfo.m_CharInfo.m_sLocationX	= (SHORT)(vPos.x/TILE_3DSIZE);
	m_PlayerInfo.m_CharInfo.m_sLocationY	= (SHORT)(vPos.y/TILE_3DSIZE);

	IScriptCharacter*	pChar;
	pChar = theHero.GetScriptChar();

	if(pChar)
		memcpy(m_arVarBufs,pChar->GetVarBuffer(),sizeof(m_arVarBufs));
}


void HeroData::UpdateHeroQuest	( Hero&  hero, BOOL bAppendQuest)
{
	/////////////////////////////////////////////////
	//从服务端下载的任务状态
	if(theGeneralGameParam.IsEnableNetwork())
	{
		if(!hero.GetScriptChar())
			return;

		sTOTALINFO_QUEST& QuestInfo = m_PlayerInfo.m_QuestTotalInfo;

		BYTE*				pQuests	= hero.GetScriptChar()->GetQuestBuffer();
		WORD				start		= 0;
		WORD				total		= QuestInfo.m_Count;

		if(!bAppendQuest)
			memset(pQuests, INVALID_BYTE_ID, MAX_QUEST_SIZE * sizeof(BYTE) );

		for(WORD i=start;i<total;++i)
		{
			sQUEST_SLOT&	slot = QuestInfo.m_Slot[i];
			//pQuests[slot.m_QuestPos] =  slot.m_Stream.QuestPart.byState;
			hero.SetQuestState(slot.m_QuestPos + MIN_QUESTID
									,slot.m_Stream.QuestPart.byState );
		}

		return;
	}

	/////////////////////////////////////////////////
	QuestStateMapIt	it;
	it = m_QuestStates.begin();
	for(;it != m_QuestStates.end(); it++)
	{
		hero.SetVar(it->first, it->second, KEYW_SET);
		//hero.ChangeQuestData(it->first, it->second, 0);
	}

	theGameWorld.SetGameTime(m_dwGameTime);
}

void HeroData::OnLevelUp		(DWORD dwLevel
                              ,DWORD dwHP
										,DWORD dwMP
										,DWORD dwRemainStat
										,DWORD dwRemainSkill)
{
	m_bLevelChanged = TRUE;
	m_PlayerInfo.m_CharInfo.m_LV					= (LEVELTYPE)dwLevel;
	m_PlayerInfo.m_CharInfo.m_dwHP				= dwHP;
	m_PlayerInfo.m_CharInfo.m_dwMP				= dwMP;
	m_PlayerInfo.m_CharInfo.m_dwRemainStat		= dwRemainStat;
	m_PlayerInfo.m_CharInfo.m_dwRemainSkill	= dwRemainSkill;
}

};//namespace object

/*////////////////////////////////////////////////////////////////////////
文 件 名：VObjectRenderInstance.cpp
创建日期：2007年11月28日
最后更新：2007年11月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "VObjectRenderInstance.h"
#include "VObjectFactoryManager.h"
#include "VObjectManager.h"
#include "AttackInfoManager.h"
#include "ObjectUtil.h"
#include "VHero.h"
#include "VEffect.h"
#include "VBuilding.h"
#include "VMoney.h"
#include "VMapObject.h"
#include "VHeroAutoInput.h"
#include "VHeroActionInput.h"
#include "VClone.h"
#include "VObjectManagerImpl.h"
#include "VCharacterAnimationCtrl.h"
#include "ClientSetting.h"
#include "ExpressionIconManager.h"
#include "V3DDecal.h"
#include "V3DTerrainLightManager.h"
#include "SkillSpecialEffectManager.h"
#include "VUCtrlPicture.h"
#include "VUIFontDDraw.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace vobject
{
const COLOR4F	g_objOverColorFactor	= {.5f,0.2f,0.2f,.7f}; //对象在鼠标经过时。。。着色
const INT		SERIES_ICON_SPACE		= 1;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VObjectRenderInstance::VObjectRenderInstance()
{
}
VObjectRenderInstance::~VObjectRenderInstance()
{
}


BOOL VObjectRenderInstance::ShowPopupText( int nBalanceType, int nNum )
{
	__CHECK_PTR(m_pVObject);
	__CHECK(m_pVObject->CanShowPopupText());

	int			xEffect = 0,
					yEffect = 0;
	DWORD			dwFlyType(0);
	INT			nValue(0);

	ObjectUtil::World3DToScreen2D(m_pVObject->GetPosition() ,xEffect,yEffect );

	switch(nBalanceType)
	{
	case POPUPTIP_PHYDAMAGE://物理伤害结算
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_RED;	
		nValue		=  nNum;
		break;
	case POPUPTIP_MAGDAMAGE://魔法伤害结算
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_BLUE;	
		nValue		=  nNum;
		break;

	case POPUPTIP_ENEMYHPDAMAGE:	
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_RED;	
		nValue		= nNum ;
		break;

	case POPUPTIP_ENEMYMPDAMAGE:	
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_BLUE;	
		nValue		= nNum ;
		break;

	case POPUPTIP_HP_RESTORE:	
	case POPUPTIP_HEAL:			
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_GREEN;
		nValue		= -nNum;
		break;

	case POPUPTIP_MISS:			
		dwFlyType	= sseffect::POPUPTEXT_MISS;	
		nValue		= 1;
		break;

	case POPUPTIP_CRITICAL:			
		dwFlyType	= sseffect::POPUPTEXT_CRITICAL;	
		nValue		= nNum;
		break;

	case POPUPTIP_BACKATTCK:	
		dwFlyType	= sseffect::POPUPTEXT_ATTACK_BACK;
		nValue		= 1;
		break;

	case POPUPTIP_DEFENCE:	
		dwFlyType	= sseffect::POPUPTEXT_DEFENCE;	
		nValue		= 1;
		break;

	case POPUPTIP_BLOCK:
		dwFlyType	= sseffect::POPUPTEXT_BLOCK;	
		nValue		= 1;
		break;

	case POPUPTIP_DEATH:
		dwFlyType	= sseffect::POPUPTEXT_DEATH_HIT;	
		nValue		= 1;
		break;

	case POPUPTIP_PLAYERHPDAMAGE:
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_RED;
		nValue		= nNum ;
		break;

	case POPUPTIP_PLAYERMPDAMAGE:
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_BLUE;	
		nValue		= nNum ;
		break;

	case POPUPTIP_EXP:
		dwFlyType	= sseffect::POPUPTEXT_NUMBER_YELLOW;
		nValue		= nNum ;
		break;
	}

	if(dwFlyType == 0)
		return FALSE;
	
	g_pFlyTextEffect->Create(dwFlyType
                        ,xEffect
								,yEffect
								,nValue);

	return TRUE;
}

BOOL VObjectRenderInstance::ShowPopupText(VObject* pTagObject
                           ,VObject* pSrcObject
									,int      iBalanceType
									,int      iTargetNumberHUD
									,int      iScNum)
{
	__CHECK_PTR(m_pVObject);

	if (pTagObject == NULL || pSrcObject == NULL)
		return FALSE;

	if(pSrcObject->IsHero() && pTagObject->IsHero())
	{
		if(pSrcObject->ShowPopupText(iBalanceType,iScNum))
			return TRUE;
	}


	switch (iBalanceType)
	{
	case POPUPTIP_PHYDAMAGE:	//物理伤害结算
	case POPUPTIP_MAGDAMAGE:	//魔法伤害结算
	case POPUPTIP_RELIVE:		//复活结算
	case POPUPTIP_HP_SUCK:		//瞬间吸血
	case POPUPTIP_NONE:
	default:
		if (iTargetNumberHUD != 0)
		{
			if (NULL != pTagObject)
			{
				int			xEffect	= 0,
								yEffect	= 0;

				ObjectUtil::World3DToScreen2D( pTagObject->GetPosition(),xEffect,yEffect );

				//if( pTagObject->IsHero() || pTagObject->IsMaster() )	//怪打我
				{
					g_pFlyTextEffect->Create(sseffect::POPUPTEXT_NUMBER_RED
													,xEffect
													,yEffect
													,(iTargetNumberHUD > 0)?iTargetNumberHUD:(-1*iTargetNumberHUD));
				}
				//else if( pSrcObject->IsHero() || pSrcObject->IsMaster() ) // 我打怪
				//{
				//	g_pFlyTextEffect->Create(sseffect::POPUPTEXT_NUMBER_YELLOW
				//									,xEffect
				//									,yEffect
				//									,(iTargetNumberHUD > 0)?iTargetNumberHUD:(-1*iTargetNumberHUD));
				//}
			}
		}


		//
		if (iScNum != 0)
		{
			if (NULL != pSrcObject)
			{
				int xEffect,yEffect;
				ObjectUtil::World3DToScreen2D( pSrcObject->GetPosition(),xEffect,yEffect );

				if (iScNum > 0)
					g_pFlyTextEffect->Create(sseffect::POPUPTEXT_NUMBER_BLUE
                                       ,xEffect
													,yEffect
													,iScNum);
				else
					g_pFlyTextEffect->Create(sseffect::POPUPTEXT_NUMBER_GREEN
                                       ,xEffect
													,yEffect
													,-1 * iScNum);
			}
		}

		break;
	}

	return TRUE;


}

BOOL VObjectRenderInstance::RenderMesh( DWORD dwTick )
{
	//////////////////////////////////////////////////
	__CHECK_PTR(m_pVObject);

	LIGHT_INFO light = *GetGameSun();


	//////////////////////////////////////////////////
	if(theGameWorld.GetHeroPlotMap())
	{
		light = theGameWorld.m_HeroLightInfo;
		light.Direction = Vector3D(light.Direction).NormalizeNew();
	}

	if( theVObjectManager.GetMouseTargetPlayerSeverID() == m_pVObject->GetID() )
	{
		light.Ambient.a = g_objOverColorFactor.a;
		light.Ambient.r = g_objOverColorFactor.r;
		light.Ambient.g = g_objOverColorFactor.g;
		light.Ambient.b = g_objOverColorFactor.b;
	}

	m_pVObject->_PrevRenderMesh( dwTick, light );
	the3DRenderer.SetLight( 0, &light );

	//////////////////////////////////////////////////
	{
		//TRUETIMEBLOCK( "VObjectRenderInstance::RenderMesh" );
	
		m_pVObject->_OnRenderMesh( dwTick, light );
	}

	//////////////////////////////////////////////////
	the3DRenderer.SetLight( 0, &theGameWorld.m_HeroLightInfo );

	return TRUE;
}


BOOL VObjectRenderInstance::RenderInfo	(DWORD dwTick
													,DWORD /*dwColor*/
													,float fTransparent
													,float /*fPlayerTransparent*/)
{
	__CHECK_PTR(m_pVObject);

	Vector3C&	vPos = m_pVObject->GetPosition();

	////////////////////////////////////////////////
	// 当死亡完而且不是活动的
	BOOL bRender = TRUE;

	// 如果是废弃队列中对象,处理下面的一种情况,其他都不渲染,
	if( !m_pVObject->IsActive() )
	{
		// 如果将要死,但是还没有播放完成死亡动画
		if( m_pVObject->IsWillDie() && !m_pVObject->IsDeadOver() )
			bRender = TRUE;
		else
			bRender = FALSE;
	}
	if( !bRender )
		return FALSE;

	V3DSpaceBox* pBBox = &m_pVObject->GetAnim()->GetWorldBBox();
	
	if(!m_pVObject->IsInHeroRange(theClientSetting.m_CharRenderRadius))
		return FALSE;

	////////////////////////////////////////////////////////////////////
#	pragma message(__FILE__  "(102) Decal处理。。。需要移开...  " )
	if( theClientSetting.m_bUseObjectShadow &&
		!(m_pVObject->GetShowFlag()&VOBJ_SHOW_NOSHADOW) )
	{
		V3DDecal		decal;

		float size = MAPTILESIZE/6;
		static float fx = 0.00f;
		static float fy = 0.00f;

		Vector3X rect[5] = 
		{
			Vector3X( vPos.x-size+fx, vPos.y-size+fy, 0 ),
			Vector3X( vPos.x-size+fx, vPos.y+size+fy, 0 ),
			Vector3X( vPos.x+size+fx, vPos.y+size+fy, 0 ),
			Vector3X( vPos.x+size+fx, vPos.y-size+fy, 0 ),
			Vector3X( vPos.x-size+fx, vPos.y-size+fy, 0 ),
		};


		int nFaceCount = 0;
		V3DGameMap *pLevel = theGameWorld.GetHeroPlotMap();
		if( pLevel && !m_pVObject->IsInFloor() )
		{
			V3DVector* tri = (V3DVector*)theGameWorld.GetDecal( rect[0], rect[1], rect[2], rect[3], nFaceCount );

			if( tri )
			{
				for( int i = 0; i < nFaceCount*3; i++ )
					tri[i].SwapYZ(  );

				decal.SetTextureID( theGlobalTexture.GetTexture( GLOBALTEX_SHADOW ) );

				rect[0].SwapYZ(  );
				rect[1].SwapYZ(  );
				rect[2].SwapYZ(  );
				rect[3].SwapYZ(  );

				decal.ProcessAABB( nFaceCount, tri, (V3DVector*)rect );
				decal.Render(fTransparent);		
			}
		}
	}


	//////////////////////////////////////////////////////
	// 输出名字名称,
	if(	m_pVObject->IsShowName() 
		&& (m_pVObject->IsMapNpc() || !m_pVObject->IsDead()))
	{
		//char	szCountry[32]	= "";
		StringHandle	szBuf;
		DWORD color				= m_pVObject->GetNameColor();


		m_pVObject->GetShowTextInfo(szBuf);

		int   nLength =  szBuf.Length()*7;
		if(nLength)
		{
			float			fFactor;
			float			fHeight;

			fFactor	= (theCamera.GetLookFrom() - m_pVObject->GetPosition() ).Length();
			fFactor	= (float)math::Round( fFactor * 0.1f);
			fHeight	= pBBox->GetMaxZ() + 10*SCALE_MESH + fFactor *0.1f;

			if(m_pVObject->IsPlayer())
			{
				if(singleton::ExistHero() && theHero.IsEnterWorld())
					fHeight += 30*SCALE_MESH;
				else
					fHeight += 80*SCALE_MESH;
			}


			///////////////////////////////
			V3DVector	vCenter( vPos.x, vPos.y, fHeight );
			int	ix = 0,
					iy = 0;


			if( theMapHandleManager.GetCamera()->World3DToScreen2D( vCenter, &ix, &iy, SCREEN_WIDTH, SCREEN_HEIGHT ) )
			{
				//ix	=	(ix - ix%5);
				//iy	=	(iy - iy%5);
				static int nFontIndex = -1;
				if ( nFontIndex == -1 )
				{
					nFontIndex = uidraw::CreateFont((LPCSTR) theClientSetting.m_szFontName,
													theClientSetting.m_nNameFontSize );
				}

				int	nFIndex(0) ;
				COLOR	clrStroke = _COLOR(COLOR_TITLE_STROKE);

				theGameUIManager.UIGetData	(gameui::eUIChatInfoBox
													,gameui::eGetFontIndex
													,&nFIndex);
				ix = ix-nLength/2;
				iy = iy - 0*theClientSetting.m_nNameFontSize;

				m_pVObject->_OnDrawName(dwTick,ix,iy,szBuf,color);
				if(clrStroke)
					uidraw::DrawStrokeText(ix
									  ,iy
									  ,szBuf
									  ,color
									  ,0xff222255
									  ,nFIndex);
				else
					DrawGameText( ix-nLength/2, iy-8, szBuf, color, nFontIndex );
			}
		}
	}

	//////////////////////////////////////////////////////
	VUCtrlText*	pChatCtrl;
	pChatCtrl = m_pVObject->GetChatDialog();

	if(pChatCtrl->IsVisible())
	{
		pChatCtrl->Render(dwTick);
		RECT rc;
		pChatCtrl->GetGlobalRect( &rc );
		theExpressionIconManager.DisplayExpression(m_pVObject->GetExpressionID()
                                                ,rc.left
																,rc.top
																,m_pVObject->GetChatDialogStartTime());
	}

	return TRUE;
}


void	VObjectRenderInstance::_OnDrawName(DWORD   dwTick
                                        ,INT&    x
													 ,INT&    y
													 ,LPCTSTR /*szText*/
													 ,COLOR   /*color*/)
{
	if(!m_pVObject)
		return;

	VUCtrlPicture*	pIcon;
	pIcon = m_pVObject->GetSeriesIcon();

	if(!pIcon || !pIcon->IsVisible())
		return;

	pIcon->SetPos( x,y );
	x += pIcon->GetWidth() + SERIES_ICON_SPACE;
	pIcon->Render(dwTick);
}



};//namespace vobject


/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillStorageOpr.cpp
创建日期：2007年10月25日
最后更新：2007年10月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillStorageOpr.h"
#include "SkillStorageManager.h"
#include "SkillInfoParser.h"
#include "SkillStorageParser.h"
#include "SkillPaneSlot.h"
#include "SlotKeyGenerator.h"
#include "SlotUIListener.h"
#include "Hero.h"
#include "PlayerAttributes.h"
#include "QuickContainer.h"
#include "ItemManager.h"
#include "SkillDependenceParser.h"
#include "UISkillContainer.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SkillStorageOpr::	SkillStorageOpr(DWORD							nSkillCategory
											,SkillStorageManager*		pManager)
:m_nSkillCategory	(nSkillCategory)
,m_pManager			(pManager)
{
	assert(m_pManager);
	assert(nSkillCategory<SKILL_KIND_MAX);

	m_pContainer	= m_pManager->m_arContainers[nSkillCategory];

	switch(nSkillCategory)
	{
	case SKILL_KIND_ACTIVE1			:	m_parInfos	= &m_pManager->m_arSkillInfoActive1;break;
	case SKILL_KIND_ACTIVE2			:	m_parInfos	= &m_pManager->m_arSkillInfoActive2;break;
	case SKILL_KIND_ACTIVE2VALKIRY:	m_parInfos	= &m_pManager->m_arSkillInfoActive2;break;
	case SKILL_KIND_PASSIVE			:	m_parInfos	= &m_pManager->m_arSkillInfoPassive;break;
	case SKILL_KIND_STYLE			:	m_parInfos	= &m_pManager->m_arSkillInfoStyle  ;break;
	case SKILL_KIND_ACTION			:	m_parInfos	= &m_pManager->m_arSkillInfoAction ;break;
	case SKILL_KIND_EMOTION   		:	m_parInfos	= &m_pManager->m_arSkillInfoEmotion;break;
	}
}

SkillStorageOpr::~SkillStorageOpr()
{
}


//------------------------------------------------------------------------------
void SkillStorageOpr::LoadInfo(sSKILL_STORAGEINFO * pSkillInven,BOOL bForceActivated)
{
	if (m_pManager->m_bOnceInitSkillInfos)
		return;

	if(m_pContainer->GetSlotMaxSize() == 0)
	{
		m_pContainer->Init((SLOTPOS)pSkillInven->m_SkillCount);
		m_pManager->_SetPageSize	(pSkillInven);
	}

	for (SLOTPOS i=0; i<pSkillInven->m_SkillCount; ++i)
	{
		CODETYPE skillCode = pSkillInven->m_SkillIDs[i];

		SkillStorageInfo	skillInfo;
		SkillStorageInfo* pSkillNew;
		skillInfo.pCurrSkillInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillCode);
		skillInfo.pNextSkillInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)(skillCode+1));

		if (skillInfo.pCurrSkillInfo == NULL)
			continue;

		SLOTPOS nAt(i);
		//nAt = (POSTYPE)m_pManager->_GetPageSkillNum(pSkillInven->m_SkillType
		//																,pSkillInven->m_SkillTab2);
		//if (nAt == INVALID_SLOTPOS)
		//	continue;

		//////////////////////////////////////////////////
		SkillPaneSlot skillSlot;
		skillSlot.SetCode((SLOTCODE)skillCode);
		skillSlot.SetSerial(theSlotKeyGenerator.GetKey());
		m_pContainer->InsertSlot(nAt, skillSlot);

		skillInfo.nSlotPos			= (SLOTPOS)nAt;
		skillInfo.bEnableLevelUp	= FALSE;
		skillInfo.state				= bForceActivated?SLOT_UISTATE_FORCE_ACTIVATED:SLOT_UISTATE_DEACTIVATED;




		/////////////////////////////////////////////////////
		if(!bForceActivated && skillInfo.pCurrSkillInfo->IsSkill())
		{
			sABILITYINFO_BASE*	pAbilityInfo = NULL ;
			SkillDetailInfo*		pSKillDetail = (SkillDetailInfo*)skillInfo.pCurrSkillInfo;
			pSKillDetail->SetFirst();
			while( pAbilityInfo = pSKillDetail->GetNext() )
			{
				skillInfo.arCurrStateCodes.push_back(pAbilityInfo->m_wAbilityID);
			}
		}

		pSkillNew = m_pManager->_SetPageSkill(pSkillInven,nAt,skillInfo);

		/////////////////////////////////////////////////////
		BaseSlot&	slot	= m_pContainer->GetSlot(skillInfo.nSlotPos);
		slot.SetState		(skillInfo.state);
		slot.SetUserData	((LPARAM)pSkillNew);
	}
}

//------------------------------------------------------------------------------
void SkillStorageOpr::ResetLevelUpInfo()
{
	__CHECK2(singleton::ExistHero(),);

	PlayerAttributes *pCharInfo = theHero.GetPlayerAttributes();

	m_pContainer->SetRequiredSkillPoint		(pCharInfo->GetExperty1());
	m_pContainer->SetRequiredSkillPoint2	(pCharInfo->GetExperty2());
	m_pContainer->SetRemainPoint				(theHero.GetCharInfo()->m_dwRemainSkill);
   m_pContainer->SetSkillLevel				(theHero.GetLevel());

	BOOL bCondition1 = FALSE; 

	for (UINT i=0; i<(*m_parInfos).size(); ++i)
	{
		SkillStorageInfo&	info = (*m_parInfos)[i];
		bCondition1 = FALSE;

		bCondition1 = m_pManager->CheckSkillSlotState(info, m_nSkillCategory);
		///////////////////////////////////////////

		///////////////////////////////////////////
		info.bEnableLevelUp = bCondition1;
	}
}


//------------------------------------------------------------------------------
BOOL SkillStorageOpr::SlotUpdate(const SkillPaneSlot& postSlot
                                ,sSKILLINFO_COMMON*   postSkillInfo)
{
	__CHECK(singleton::ExistHero());

	PlayerAttributes *pCharInfo = theHero.GetPlayerAttributes();			

	assert (pCharInfo);
	assert (m_pContainer);

	m_pContainer->SetRequiredSkillPoint		(pCharInfo->GetExperty1());
	m_pContainer->SetRequiredSkillPoint2	(pCharInfo->GetExperty2());
	m_pContainer->SetRemainPoint				(theHero.GetCharInfo()->m_dwRemainSkill);
   m_pContainer->SetSkillLevel				(theHero.GetLevel());


	BOOL bRet(FALSE);

	/////////////////////////////////////
	for (UINT i=0; i<(*m_parInfos).size(); ++i)
	{
		SkillStorageInfo&	info = (*m_parInfos)[i];

		sSKILLINFO_COMMON* pCurrSkillInfo = info.pCurrSkillInfo;
		sSKILLINFO_COMMON* pNextSkillInfo = info.pNextSkillInfo;

		if (pCurrSkillInfo->m_SkillClassCode != postSkillInfo->m_SkillClassCode)
			continue;

		SkillSlot&	slot	= (SkillSlot&)m_pContainer->GetSlot(info.nSlotPos);

		/////////////////////////////////////
		info.state		= SLOT_UISTATE_ACTIVATED;
		slot.SetState	(SLOT_UISTATE_ACTIVATED);

		/////////////////////////////////////
		if(m_pManager->CheckNextSkillValid(postSkillInfo,m_nSkillCategory))
		{
			pNextSkillInfo		= theSkillInfoParser.GetSkillInfo(postSkillInfo->m_SkillCode + 1);
		}
		else
			pNextSkillInfo		= NULL;

		/////////////////////////////////////
		if(	pNextSkillInfo 
			&& pNextSkillInfo->m_SkillClassCode != pCurrSkillInfo->m_SkillClassCode)
				pNextSkillInfo	= NULL;

		//////////////////////////////////////////////////
		info.pCurrSkillInfo = postSkillInfo;
		info.pNextSkillInfo = pNextSkillInfo;

		slot.SetSlotType	(postSlot.GetSlotType());
		slot.SetCode		(postSlot.GetCode());

		//////////////////////////////////////////////////
		info.bEnableLevelUp	= m_pManager->CheckSkillSlotState(info, m_nSkillCategory);
		

		//////////////////////////////////////////////////
		//更新链接信息
		QuickContainer * pQuickDlg;
		
		pQuickDlg = (QuickContainer*)theHero.GetSlotContainer(SI_QUICK);
		if (pQuickDlg)
		{
			SLOTPOS pos = pQuickDlg->GetPosByCode(postSlot.GetCode() - 1);

			if (pos != INVALID_QUICKSLOT)
			{
				BaseSlot FromSlot;
				pQuickDlg->DeleteSlot(pos, &FromSlot);
				theItemManager.LinkSkillToQuick(SI_SKILL, postSlot.GetCode(), pos);
			}
			else
			{
				//assert ("Quick Slot not valid");
			}
		}
		//////////////////////////////////////////////////
		if(singleton::ExistHero())
		{	
			theHero.RefreshStyleQuick();
			theHero.UpdateSkillAttributes();
		}

		bRet |= TRUE;
	}

	return bRet;
}


//------------------------------------------------------------------------------
BOOL SkillStorageOpr::SetSkillInfos	(const SkillPaneSlot& postSlot
												,sSKILLINFO_COMMON* postSkillInfo)
{
	BOOL bRet(FALSE);

	for (UINT i=0; i<(*m_parInfos).size(); ++i)
	{
		SkillStorageInfo&	info = (*m_parInfos)[i];

		sSKILLINFO_COMMON* pCurrSkillInfo = info.pCurrSkillInfo;
		sSKILLINFO_COMMON* pNextSkillInfo = info.pNextSkillInfo;

		if (pCurrSkillInfo->m_SkillClassCode != postSkillInfo->m_SkillClassCode)
			continue;

		/////////////////////////////////////
		SkillSlot&	slot	= (SkillSlot&)m_pContainer->GetSlot(info.nSlotPos);

		info.state = SLOT_UISTATE_ACTIVATED;
		slot.SetState	(SLOT_UISTATE_ACTIVATED);


		/////////////////////////////////////
		pNextSkillInfo = NULL;


		pNextSkillInfo =	theSkillInfoParser.GetSkillInfo(postSlot.GetCode()+1);

		if(	pNextSkillInfo 
			&& pNextSkillInfo->m_SkillClassCode != pCurrSkillInfo->m_SkillClassCode)
			pNextSkillInfo	= NULL;


		/////////////////////////////////////////////////////////
		slot.SetSlotType	(postSlot.GetSlotType());
		slot.SetCode		(postSlot.GetCode());


		info.pCurrSkillInfo = postSkillInfo;
		info.pNextSkillInfo = pNextSkillInfo;

		bRet |= TRUE;
	}

	return bRet;
}


SkillStorageInfo* SkillStorageOpr::GetSkillStorage(SLOTCODE	skillClassCode,BOOL bCheckClassCode)
{
	for (UINT i=0; i<(*m_parInfos).size(); ++i)
	{
		sSKILLINFO_COMMON* pCurrSkillInfo = (*m_parInfos)[i].pCurrSkillInfo;

		if(	bCheckClassCode && pCurrSkillInfo->m_SkillClassCode	== skillClassCode 
			||	!bCheckClassCode && pCurrSkillInfo->m_SkillCode			== skillClassCode )
			return &(*m_parInfos)[i];
	}
	return NULL;
}


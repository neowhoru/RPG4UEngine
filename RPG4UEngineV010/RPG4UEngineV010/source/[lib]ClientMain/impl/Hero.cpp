/*////////////////////////////////////////////////////////////////////////
文 件 名：Hero.cpp
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Hero.h"
#include "WeaponSoundInfoParser.h"
#include <SkillInfoParser.h>
#include "VPlayer.h"
#include "QuickContainer.h"
#include "ItemCompoundDialog.h"
#include "HeroEquipmentContainer.h"
#include "WareHouseDialog.h"
#include "TempInventoryContainer.h"
#include "SummonSkillContainer.h"
#include "InventoryDialog.h"
#include "ShopDialog.h"
#include "EventInventoryDialog.h"
#include <SkillPaneSlot.h>
#include "QuickPaneSlot.h"
#include "EquipmentContainer.h"
#include "SkillStorageManager.h"
#include "ItemInfoParser.h"
#include "ConstTextRes.h"
#include "HeroData.h"
#include "PacketInclude.h"
#include "ResultCode.h"
#include "PacketStruct_ClientGameS_Event.h"
#include "ObjectManager.h"
#include "Monster.h"
#include "QuestItemDropInfoParser.h"
#include "QuestManager.h"
#include "IScriptInterface.h"
#include "TradeDialog.h"
#include "VendorDialog.h"
#include "HeroQuestManager.h"

using namespace object;
using namespace RC;
GLOBALINST_SINGLETON_PTR_IMPL(Hero);

namespace object
{
const float WEAPON_MULTIPLIER = 0.67f;

//------------------------------------------------------------------------------
Hero::Hero(void)
:	m_pInventoryContainer ( NULL )
,	m_pQuickContainer ( NULL )
,	m_pStyleContainer(NULL)
,	m_pSummonContainer(NULL)
,	m_dwPacketStatus( 0 )
,  m_bDeaded(FALSE)

{
	GLOBALINST_SINGLETON_PTR_INIT(Hero);
	m_ObjectCookie.SetObjectType(HERO_OBJECT);

	m_bWaitAttackingFromServer		= TRUE;
	m_pPlayerAttributes	= NULL;
	m_eTriggerState		= TRIGGER_STATE_LEAVE_AREA;

	InitTriggerState();

	m_iHeroTurnState = eHERO_NOT_TURN;
}


//------------------------------------------------------------------------------
Hero::~Hero(void)
{
	GLOBALINST_SINGLETON_PTR_FREE(Hero);
}

//------------------------------------------------------------------------------
BOOL Hero::Create( DWORD dwObjectKey, DWORD dwParam )
{
	Player::Create(dwObjectKey, dwParam);


	
	m_bEnterWorld			= FALSE;

	m_iComboCount			= 0;
	m_bCustomAttackSpeed = FALSE;
	m_fCustomAttackSpeed = 1.0f;

	m_bCustomMoveSpeed	= FALSE;
	m_fCustomMoveSpeed	= 1.0f;


	m_vectorGetItem.clear();

	m_vectorTargetMonster.clear();
	m_TabTargetInfo.dwObjectKey	= 0;
	m_TabTargetInfo.fDistance		= 0.0f;
	m_TabTargetInfo.wTargetCheck	= 0;
	m_TabTargetInfo.wTargetCount	= 0;



	if(CharacterCookie::Create())
	{
		SetCoolTimer( FALSE, 0 );
		InitCoolTimes();
	}

	if(GetPlayerCookie().Create( ))
	{
		GetPlayerCookie().SetItemCoolTime( FALSE, 0 );
		GetPlayerCookie().InitItemCoolTimes();
	}


	m_pHeroEquipContainer->RefreshEquipForExtra(EQUIPPOS_WEAPON);

	return TRUE;
}


//------------------------------------------------------------------------------
void Hero::OnInitializeState()
{
	if(m_StateArray[STATE_MOVE])		theObjectSystem.FreeState(m_StateArray[STATE_MOVE]);
	if(m_StateArray[STATE_ATTACK])	theObjectSystem.FreeState(m_StateArray[STATE_ATTACK]);


	m_StateArray[STATE_MOVE]		= theObjectSystem.AllocState(STATE_HEROMOVE);
	m_StateArray[STATE_ATTACK]		= theObjectSystem.AllocState(STATE_HEROATTACK);	
}

//------------------------------------------------------------------------------
void Hero::Destroy()
{
	GetPlayerCookie().FreeItemCoolTime( FALSE );
	FreeCoolTimer( FALSE );


	m_vectorGetItem.clear();
	m_vectorTargetMonster.clear();

	Player::Destroy();


	GetPlayerCookie().Destroy(  );
}


//----------------------------------------------------------------------------
void Hero::RefreshStyleQuick()
{
	ASSERT(m_pHeroEquipContainer);
	if (m_pHeroEquipContainer)
	{
		m_pHeroEquipContainer->RefreshEquipForExtra(EQUIPPOS_WEAPON);
	}
}


//------------------------------------------------------------------------------
BaseContainer * Hero::GetSlotContainer( SLOTINDEX slotIdx )
{
	switch( slotIdx )
	{
	case SI_INVENTORY:			return m_pInventoryContainer;
	case SI_INVENTORY2:			return m_pTempInvenContainer;
	case SI_EQUIPMENT:			return m_pEquipContainer;
	case SI_STYLE:					return m_pStyleContainer;
	case SI_QUICK:					return m_pQuickContainer;
	//case SI_VENDOR_SELL:	 return &theVendorSellDlg;
	case SI_SKILL:					return theSkillStorageManager.GetUISkillSlotContainer();
	case SI_WAREHOUSE:			return m_pWareHouseContainer;
	case SI_ENCHANT:				return theItemCompoundDialog.GetMainContainer();
	case SI_ENCHANT_TARGET:		return theItemCompoundDialog.GetTargetContainer();
	case SI_ENCHANT_SUBRESULT:	return theItemCompoundDialog.GetSubResultContainer();
	case SI_ENCHANT_SOCKET:		return theItemCompoundDialog.GetSocketContainer();
	case SI_ITEMLOG:				return theItemLogManager.GetLogContainer();
	case SI_NPCSHOP:				return &theShopDialog;
	case SI_TRADE:					return theTradeDialog.GetTradeContainer();
	case SI_TRADE2:				return theTradeDialog.GetViewContainer();
	case SI_VENDOR_BUY:
	case SI_VENDOR_SELL:			return theVendorDialog.GetTradeContainer();
	}
	return NULL;
}


//------------------------------------------------------------------------------
BOOL Hero::Process( DWORD dwTick )
{
	BOOL bRet = Player::Process( dwTick );
		
	//const Vector3D * pVec = &m_pPathExplorer->GetPos();

	return bRet;
}


void Hero::StopAtServer()
{
	MSG_CG_SYNC_STOP_SYN packet;
	packet.vCurPos			= GetPosition();
	//packet.vCurPos.wvPos = theHero.GetPosition();
	//packet.vCurPos.m_TileIndex = theHero.GetPathExplorer()->GetTile();
	SendPacket(&packet, sizeof(MSG_CG_SYNC_STOP_SYN));
}


//------------------------------------------------------------------------------
BOOL Hero::SendPacket( void *pBuf, int iLength, BOOL bLocalPacket)
{


	MSG_OBJECT_BASE *	pMsg = (MSG_OBJECT_BASE *)pBuf;
	pMsg->m_dwKey = theGeneralGameParam.GetUserID();	///这里传入 User编号，非ObjectKey

	LOGINFO	("Send:Cat:%d,Pro:%d Size:%d\n"
				,pMsg->m_byCategory
				,pMsg->m_byProtocol
				,iLength);

	return theNetworkLayer.SendPacket(CK_GAMESERVER
                                ,pBuf
										  ,iLength
										  ,bLocalPacket?this:NULL) ? TRUE : FALSE;
}


//------------------------------------------------------------------------------
MONEY Hero::GetMoney( void )const
{
	ASSERT(GetCharInfo());

	return GetCharInfo()->m_Money;
}


//------------------------------------------------------------------------------
void Hero::SetMoney(MONEY Money)
{
	ASSERT(GetCharInfo());

	INT64	moneyOff;
	moneyOff = Money - GetCharInfo()->m_Money;
	if(moneyOff == 0)
		return;

	//	money:%I64u
	if(moneyOff > 0)
		OUTPUTCHATF(TEXTRES_ITEMLOG_GETMONEY, moneyOff);
	else
		OUTPUTCHATF(TEXTRES_ITEMLOG_LOSEMONEY, -moneyOff);

	theItemLogManager.PushMoney(moneyOff);

	GetCharInfo()->m_Money = Money;
	AddUpdateFlag(CHAR_UPDATE_PACKINFO);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Hero::MinusMoney( MONEY minus_money )
{
	SetMoney(GetMoney() - minus_money);
	return TRUE;
}

//------------------------------------------------------------------------------
BOOL Hero::InitContainer()
{
	m_pHeroEquipContainer	= new HeroEquipmentContainer;
	m_pEquipContainer			= m_pHeroEquipContainer;

	m_pInventoryContainer	= new InventoryDialog;	
	m_pQuickContainer			= new QuickContainer;	
	m_pStyleContainer			= new StyleContainer;
	m_pSummonContainer		= new SummonSkillContainer;

	m_pWareHouseContainer	= new WareHouseDialog;	


	m_pHeroEquipContainer->Init(MAX_EQUIPMENT_SLOT_NUM, SI_EQUIPMENT);
	m_pInventoryContainer->Init(MAX_INVENTORY_SLOT_NUM, SI_INVENTORY);
	m_pQuickContainer		->Init(MAX_QUICK_SLOT_NUM,		 SI_QUICK);
	m_pStyleContainer		->Init(MAX_STYLE_SLOT_NUM,		 SI_STYLE);
	m_pSummonContainer	->Init(MAX_SUMMON_SLOT_NUM,	 SI_SKILL);
	m_pWareHouseContainer->Init(MAX_WAREHOUSE_SLOT_NUM, SI_WAREHOUSE);


	m_pEquipContainer->SetOwnerKey(GetObjectKey());
	m_pHeroEquipContainer->SetInventoryDialog((InventoryDialog*)m_pInventoryContainer);

	m_pHeroEquipContainer->InitSlotListener();
	m_pQuickContainer->InitSlotListener();
	m_pStyleContainer->InitSlotListener();
	m_pSummonContainer->InitSlotListener();

	m_pTempInvenContainer = ((WareHouseDialog*)m_pWareHouseContainer)->GetTempInventory();

	return TRUE;
}



//------------------------------------------------------------------------------
void Hero::ReleaseContainer()
{
	if(m_pHeroEquipContainer == NULL)
		return;

	m_pInventoryContainer	->FlushSlotListener();
	m_pWareHouseContainer	->FlushSlotListener();
	m_pTempInvenContainer->FlushSlots();
	m_pHeroEquipContainer->FlushSlotListener();
	m_pQuickContainer		->FlushSlotListener();
	m_pStyleContainer		->FlushSlotListener();
	m_pSummonContainer	->FlushSlotListener();
	

	m_pTempInvenContainer->Release();
	m_pInventoryContainer->Release();
	m_pHeroEquipContainer->Release();
	m_pQuickContainer		->Release();
	m_pStyleContainer		->Release();
	m_pSummonContainer	->Release();
	m_pWareHouseContainer->Release();

	SAFE_DELETE(m_pHeroEquipContainer);
	SAFE_DELETE(m_pInventoryContainer);
	SAFE_DELETE(m_pQuickContainer		);
	SAFE_DELETE(m_pStyleContainer		);
	SAFE_DELETE(m_pSummonContainer	);
	SAFE_DELETE(m_pWareHouseContainer);

	m_pEquipContainer	= NULL;

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Hero::RunScript(DWORD	dwData,DWORD npcID,BOOL bRunNow)
{
	///////////////////////////////////////////
	//发送脚本执行协议
	if(theGeneralGameParam.IsEnableNetwork())
	{
		MSG_CG_EVENT_RUNSCRIPT_SYN		msgRunScript;
		msgRunScript.m_dwNpcID = npcID;
		msgRunScript.m_dwParam = dwData;

		return SendPacket(&msgRunScript, sizeof(msgRunScript));
	}
	return _SUPER::RunScript(dwData,npcID,bRunNow);
}

BOOL Hero::RunQuestScript(CODETYPE questID,DWORD	dwData,BOOL bRunNow)
{
	///////////////////////////////////////////
	//发送脚本执行协议
	if(theGeneralGameParam.IsEnableNetwork())
	{
		MSG_CG_EVENT_RUNQUESTSCRIPT_SYN		msgRunScript;
		msgRunScript.m_dwQuestID	= questID;
		msgRunScript.m_dwParam		= dwData;

		return SendPacket(&msgRunScript, sizeof(msgRunScript));
	}

	return _SUPER::RunQuestScript(questID,dwData,bRunNow);
}

sHERO_QUESTINFO* Hero::FindQuestInfoById( CODETYPE questID )
{
	HeroQuestInfoMapIt it;
	it = m_QuestInfoMap.find(questID);
	if(it != m_QuestInfoMap.end())
		return &it->second;

	return NULL;
}

BOOL Hero::KeepQuestScript(DWORD dwScriptType,DWORD dwQuestType)
{
	if(m_pScriptChar)
		return TRUE;

	__BOOL_SUPER(KeepQuestScript( dwScriptType, dwQuestType));

	if(m_pScriptChar)
		m_pScriptChar->SetListener(this);

	return TRUE;
}

void Hero::ChangeQuestData( CODETYPE questID, DWORD state, DWORD /*questAction*/)
{

	sHERO_QUESTINFO* pInfo;
	
	pInfo = FindQuestInfoById( questID );
	// 能找到，是状态改变
	if( pInfo )
	{	
		pInfo->m_QuestData = state;
	}
	else
	{
		// 新任务
		sHERO_QUESTINFO info;
		info.m_QuestID		= questID;
		info.m_QuestData	= state;
		info.m_bFinished	= FALSE;
		info.m_bReqInfoed	= FALSE;

		m_QuestInfoMap[questID] =  info;
	}
	
	UpdateActiveQuestState();
}

BOOL Hero::OnQuestDrop	(MONSTERCODE monsterCode
								,DWORD		 dwValue 
								,INT			 nUnitIndex)
{
	sQUESTITEM_DROPINFO*		pDropInfo;
	sQUESTITEM_DROPUNIT*		pDropUnit;
	//Monster*					pMonster;
	quest::sQUEST_INFO*		pQuest;


	//pMonster	= (Monster*)theObjectManager.GetObject(monsterCode);

	//__CHECK(pMonster && pMonster->IsMonster());

	pDropInfo = theQuestItemDropInfoParser.GetQuestItemDropInfo(monsterCode);
	__CHECK_PTR(pDropInfo);


	////////////////////////////////////////
	__CHECK_RANGE(nUnitIndex,(INT)pDropInfo->m_DropUnits.size());

	pDropUnit = &pDropInfo->m_DropUnits[nUnitIndex];


	////////////////////////////////////////
	pQuest = theQuestManager.LoadQuest( pDropUnit->m_QuestID );
	if( !pQuest )
		return FALSE;

	SetVar(pDropUnit->m_VarID, (INT)dwValue);

	////////////////////////////////////////
	LOGTIPF	("[%s]%s(%d//%d)"
				,dwValue < pDropUnit->m_DestValue? eTIP_NORMAL : eTIP_QUEST
				,NULL
				,INVALID_SOUNDINDEX
				,pQuest->m_Name
				,(LPCSTR)pDropUnit->m_ShowName
				,dwValue
				,pDropUnit->m_DestValue
				);


	theGameUIManager.UIRefresh(gameui::eUIQuest);
	return TRUE;
}


void Hero::OnQuestDone(CODETYPE	 /*questID*/)
{
	theHeroQuestManager.UpdateNpcQuestTips();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::UpdateActiveQuestState()
{
	HeroQuestInfoMapIt	it;

	m_vectorActiveQuestInfo.clear();

	for(it = m_QuestInfoMap.begin(); it!= m_QuestInfoMap.end();it++)
	{
		sHERO_QUESTINFO& info = it->second;

		if (	info.m_QuestID < MIN_QUESTID 
			||	info.m_QuestID >= MAX_QUESTID )
			continue;

		if( info.m_QuestData != 0 &&
			info.m_QuestData != INVALID_DWORD_ID )
		{
			m_vectorActiveQuestInfo.push_back( info );
		}
	}
	AddUpdateFlag(CHAR_UPDATE_QUEST);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::ReqQuestInfo( CODETYPE questID )
{
#pragma message(__FILE__  "(703) Hero..ReqQuestInfo 从服务端更新sQUEST_INFO状态  " )

	quest::sQUEST_INFO *pQuest;
	pQuest = theQuestManager.LoadQuest( questID );
	if( pQuest )
		return;

	sHERO_QUESTINFO *pInfo;
	pInfo = FindQuestInfoById( questID );
	if( !pInfo )
		return;

	if ( pInfo->m_bReqInfoed == FALSE )
	{
		pInfo->m_bReqInfoed = TRUE;
		assert(!"Get Data from server");
	}
}


//------------------------------------------------------------------------------
int  Hero::DoAction(PLAYER_ACTION *pAction)
{
	SetCurrentAction(*pAction);

	switch(pAction->ActionID)
	{
		///////////////////////////////////////////////////
	case ACTION_ATTACK:
		{
			Character *pTarget;
			
			pTarget = (Character *)theObjectManager.GetObject(pAction->ATTACK.dwTargetID);
			if (pTarget)
			{
				SetTargetID(pTarget->GetObjectKey());
				SetNextState(STATE_ATTACK,g_CurTime);
			}
			else
			{
				SetTargetID(0);
				SetNextState(STATE_IDLE,g_CurTime);
			}
		}
		break;

		/////////////////////////////////////////////
	case ACTION_SKILL:
		{
			Character *pTarget = (Character *)theObjectManager.GetObject(pAction->SKILL.dwTargetID);
			StopMove();
			sSKILLINFO_BASE *pInfo;
			
			pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)pAction->SKILL.dwSkillID);
			
			/////////////////////////////////////////////
			if (	pInfo->m_byTarget != SKILL_TARGET_ME
				&&	pInfo->m_byTarget != SKILL_TARGET_AREA
				&&	pInfo->m_byTarget != SKILL_TARGET_SUMMON) 
			{
				if (pTarget)
				{
					pAction->SKILL.vCurPos.wvPos			= GetPosition();
					pAction->SKILL.vCurPos.m_TileIndex	= -1;

					SetCurrentAction(*pAction);
					SetTargetID(pAction->SKILL.dwTargetID);

					//////////////////////////////////////////////
					SKILL_USE_PARAMETER skillparam;
					skillparam._iLevel		= 1;
					skillparam._iSkillID		= pAction->SKILL.dwSkillID;
					SetTargetID(pAction->SKILL.dwTargetID);

					UseSkill(&skillparam);

					//////////////////////////////////////
					/// 将自动攻击
					if(	pInfo->m_byTarget == SKILL_TARGET_ENEMY 
						|| pInfo->m_byTarget == SKILL_TARGET_REACHABLE_ENEMY  )
					{
						if(theGeneralGameParam.IsAutoAttackAfterUseSkill())
						{
							if( theHero.GetPlayerCookie().GetCharType() != PLAYERTYPE_NECROMANCER )
							{
								theHeroActionInput.SwitchAutoAttack( TRUE );
							}
						}
					}
				}
				else
				{
					SetTargetID(0);
					SetNextState(STATE_IDLE,g_CurTime);
				}
			}
			////////////////////////////////////////////////////////
			else
			{
				pAction->SKILL.vCurPos.wvPos			= GetPosition();
				pAction->SKILL.vCurPos.m_TileIndex	= -1;
				SetCurrentAction(*pAction);
				SetTargetID(0);

				///////////////////////////////////////////////
				SKILL_USE_PARAMETER skillparam;
				skillparam._iLevel	= 1;
				skillparam._iSkillID = pAction->SKILL.dwSkillID;
				SetTargetID(pAction->SKILL.dwTargetID);
				UseSkill(&skillparam);
			}
		}
		break;


		///////////////////////////////////////////////////
	case ACTION_KNOCKBACK:
		{
			StopMove();		
			SetNextState(STATE_DAMAGE,g_CurTime);	
		}
		break;

		////////////////////////////////////////////////////
	case ACTION_TOAIR:
		{
			StopMove();		

			TOAIR_INFO info;
			info.bGroundHit		= FALSE;
			info.fDownForceLimit = pAction->TOAIR.fDownForceLimit;
			info.fGravity			= pAction->TOAIR.fGravity;
			info.fHeightLimit		= pAction->TOAIR.fHeightLimit;
			info.fUpperForce		= pAction->TOAIR.fUpperForce;
			info.iBoundCount		= 0;
			info.bForceDown		= pAction->TOAIR.bForceDown;
			FlyToAir(&info);
			SetNextState(STATE_AIR,g_CurTime);	
			
		}
		break;

		//////////////////////////////////////////////////
	case ACTION_DOWN:
		{
			StopMove();	
			SetNextState(STATE_DOWN,g_CurTime);
		
		}
		break;

		//////////////////////////////////////////////////
	case ACTION_GETUP:
		{
			StopMove();	
			SetNextState(STATE_GETUP,g_CurTime);
			
		}
		break;

		//////////////////////////////////////////////////
	case ACTION_SPECIAL_MOVE:
		{
			StopMove();	
			SetSpecialMoveFlag(pAction->SPECIALMOVE.bType);
			SetNextState(STATE_SPECIALMOVE,g_CurTime);
		}
		break;


		//////////////////////////////////////////////////
	default:
		ASSERT(!"Hero::DoNextAction() invalid");
		break;
	}

	return 1;
}

//------------------------------------------------------------------------------
BOOL Hero::CheckQueueActionCondition()
{
	__CHECK(m_ActionQueue.GetLength() > 0);

	PLAYER_ACTION& action =  *m_ActionQueue.Front();
	switch (action.ActionID)
	{
		/////////////////////////////////////////
	case ACTION_TOAIR:
	case ACTION_KNOCKBACK:
	case ACTION_DOWN:
		return FALSE;

		/////////////////////////////////////////
	case ACTION_ATTACK:
		{
			Character *pTarget;
			pTarget = (Character *)theObjectManager.GetObject(action.ATTACK.dwTargetID);

			__CHECK_PTR(pTarget);



			Vector3D vDiff = pTarget->GetPosition() - GetPosition();
			float fActionLength	= theHeroActionInput.GetAttackActionDistance();

			__CHECK(vDiff.Length2() <= fActionLength * fActionLength);
			return TRUE;
		}
		break;

	case ACTION_SKILL:
		{
			sSKILLINFO_BASE *	pInfo;
			Character *			pTarget;
			
			pInfo= theSkillInfoParser.GetSkillInfo((SLOTCODE)action.SKILL.dwSkillID);
			__VERIFY_PTR(pInfo,"skill is null");

			pTarget = (Character *)theObjectManager.GetObject(action.SKILL.dwTargetID);
			__CHECK_PTR(pTarget);



				Vector3D vDiff = pTarget->GetPosition() - GetPosition();

				__CHECK (vDiff.Length() <= theHeroActionInput.GetActionDistance(action) );
				return TRUE;
		}
		break;
	}

	return FALSE;
}


//------------------------------------------------------------------------------
float Hero::GetProcessFactor()
{
	return 1.0f;
}



//------------------------------------------------------------------------------
void Hero::AddKeyboardMovePacketSendTime(DWORD dwTime)
{
	m_dwKeyboardMovePacketSendTime += dwTime;
}


//------------------------------------------------------------------------------
BOOL Hero::ProcessNextAction()
{	
	if (GetActionQueueCount()) 
	{
		if (CheckQueueActionCondition())
		{
			DoNextAction();							
			return TRUE;
		}
		else
		{
			SetNextState(STATE_IDLE,g_CurTime);
			return TRUE;
		}
	}

	__CHECK(theHeroActionInput.ProcessQueueAction());

	return TRUE;
}


//------------------------------------------------------------------------------
BOOL Hero::IsLockInput()
{
	if (IsDead() || 
		CannotAct() ||		
		GetCurrentState()==STATE_DAMAGE||
		GetCurrentState()==STATE_GETUP||
		GetCurrentState()==STATE_DOWN)
	{
		return TRUE;
	}

    if (GetBehaveState() == PLAYER_BEHAVE_VENDOR_ESTABLISHER ||
        GetBehaveState() == PLAYER_BEHAVE_VENDOR_OBSERVER)
    {
        return TRUE;
    }

	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Hero::InstallSlotListener(SLOTINDEX slotIdx, SlotUIListener* pRenderer)
{
	__CHECK_PTR(pRenderer);

	__CHECK( pRenderer->PrevInstall() );

	BaseContainer* pContainer = GetSlotContainer(slotIdx);
	__CHECK_PTR(pContainer);


	pContainer->SetSlotListener(pRenderer);
	pRenderer->OnInstallTo(pContainer);

	return TRUE;
}

//------------------------------------------------------------------------------
BOOL Hero::CanDoQueueAction(PLAYER_ACTION *pAction)
{
	switch (GetCurrentState())
	{
	case STATE_IDLE:
	case STATE_SIT:
	case STATE_EMOTICON:
	case STATE_MOVE:
		return TRUE;

	case STATE_ATTACK:
		if (	pAction->ActionID == ACTION_ATTACK 
			|| pAction->ActionID == ACTION_SKILL)
		{
			if (theHeroActionInput.IsCanDoNextAttack()) 
			{
				return TRUE;
			}
		}
	}
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::SetHeroData(  HeroData& heroData)
{
	m_QuestInfoMap.clear();
	m_vectorActiveQuestInfo.clear();

	KeepQuestScript(SCRIPTCHARACTER_CHAR,QUESTCHARACTER_PLAYER);
	if(m_pScriptChar)
		m_pScriptChar->SetVarBuffer( heroData.GetVarBufs(), MAX_PLAYERVAR_SIZE*sizeof(BYTE));

	heroData.UpdateHeroQuest(*this);
	
	SetHeroInfo(heroData.m_PlayerInfo);
}

//------------------------------------------------------------------------------
void Hero::SetHeroInfo( const sHERO_INFO&  heroInfo )
{

    sPLAYERINFO_BASE* pPlayerInfo=GetCharInfo();

    memcpy(pPlayerInfo, &heroInfo.m_CharInfo, sizeof(heroInfo.m_CharInfo));

	SetName(pPlayerInfo->m_szCharName);

	m_PlayerType		=  (ePLAYER_MAKE_TYPE)heroInfo.m_MakeType;
	m_dwAttackStyle	=  heroInfo.m_CharInfo.m_wSelectStyleCode;

    //hp,mp 
	m_pPlayerAttributes->SetCharInfo( m_PlayerCookie.GetCharInfo(),0,0);

	SetPartVariation(PLAYER_VARIATION_HAIR,heroInfo.m_CharInfo.m_byHair,FALSE);
	SetPartVariation(PLAYER_VARIATION_FACE,heroInfo.m_CharInfo.m_byFace,FALSE);

	HideHelmet( heroInfo.m_bHideHelmet ,FALSE);

	theGameOption.SetShowHelm(heroInfo.m_bHideHelmet);

	////////////////////////////////////////////////////
   // 1.EQUIP
	SetEquipItemTotalInfo( heroInfo.m_EquiptItemInfo );

	// SKILL
	SetSkillTotalInfo( heroInfo.m_SkillTotalInfo );

	// INVENTORY
	SetInventoryTotalInfo( heroInfo.m_InventoryTotalInfo );

	// z.QUICK 
	SetQuickTotalInfo( heroInfo.m_QuickTotalInfo );

	SetHeightVariation(heroInfo.m_CharInfo.m_byHeight);

#ifndef _USE_AUTO_STYLE_QUICK
	// STYLE
	SetStyleTotalInfo( heroInfo.m_StyleTotalInfo );
#endif


	SetHP(heroInfo.m_CharInfo.m_dwHP);
	SetMP(heroInfo.m_CharInfo.m_dwMP);

	if (GetHP() == 0)
		SetHP(1);

	///////////////////////////////////////////////
	UpdateAppearance(FALSE);


	m_pInventoryContainer->UpdateSlotListener();
	m_pInventoryContainer->UpdateSlotListener();

	m_pQuickContainer->UpdateSlotListener();
	m_pStyleContainer->UpdateSlotListener();
    m_pSummonContainer->UpdateSlotListener();

	UpdateSkillAttributes();

	SetGMGrade( heroInfo.m_byGMGrade );

	m_bExistHPInfo = TRUE;

	AddUpdateFlag(CHAR_UPDATE_EXPINFO);
}



VOID Hero::GetSkillTotalInfo( sTOTALINFO_SKILL & OUT /*rSkillInfo*/ )
{
	//sSKILL_SLOT * pSkillSlot = rSkillInfo.m_Slot; 
	//
	//rSkillInfo.m_Count = 0;

	//SLOTPOS posFirst = 0;
	//SLOTPOS posNum = MAX_SKILL_SLOT_NUM;

	//for (SLOTPOS i = posFirst; i < posNum; ++i)
	//{
	//	if ( !m_pSkillContainer->IsEmpty(i) )
	//	{
	//		pSkillSlot[rSkillInfo.m_Count].m_SkillPos = i;
	//		((SkillPaneSlot &)m_pSkillContainer->GetSlot(i)).CopyOut( pSkillSlot[rSkillInfo.m_Count].m_Stream );

	//		++rSkillInfo.m_Count;			
	//	}
	//}
}


VOID Hero::GetInventoryTotalInfo( sTOTALINFO_INVENTORY & OUT rItemInfo )
{
	sITEM_SLOTEX * pItemSlot = rItemInfo.m_Slot; 

	rItemInfo.m_TmpInvenCount = rItemInfo.m_InvenCount = 0;
	
	SLOTPOS posFirst = 0;
	SLOTPOS posNum = MAX_INVENTORY_SLOT_NUM;

	for (SLOTPOS i = posFirst; i < posNum; ++i)
	{
		if ( !m_pInventoryContainer->IsEmpty(i) )
		{
			pItemSlot[rItemInfo.m_InvenCount].m_ItemPos = i;
			ItemSlot & slotLoop = (ItemSlot &)m_pInventoryContainer->GetSlot(i);
			slotLoop.CopyOut( pItemSlot[rItemInfo.m_InvenCount].m_Stream );

			++rItemInfo.m_InvenCount;			
		}
	}

	/////////////////////////////////
	BaseContainer * pTempContainer;
	
	pTempContainer = theItemManager.GetContainer(SI_INVENTORY2);

	posFirst = 0;
	posNum = pTempContainer->GetSlotMaxSize();

	///////////////////////////////////
	for (SLOTPOS i = posFirst; i < posNum; ++i)
	{
		if ( !pTempContainer->IsEmpty(i) )
		{
			pItemSlot[rItemInfo.m_InvenCount + rItemInfo.m_TmpInvenCount].m_ItemPos = i;
			ItemSlot & slotLoop = (ItemSlot &)m_pInventoryContainer->GetSlot(i);
			slotLoop.CopyOut( pItemSlot[rItemInfo.m_InvenCount].m_Stream );

			++rItemInfo.m_TmpInvenCount;			
		}
	}
	
}


//------------------------------------------------------------------------------
VOID Hero::GetQuickTotalInfo( sTOTALINFO_QUICK & OUT slotQuitInfo )
{
	sQUICK_SLOT * pQuickSlot = slotQuitInfo.m_Slot; 

	slotQuitInfo.m_Count = 0;
	
	SLOTPOS posFirst = 0;
	SLOTPOS posNum = MAX_QUICK_SLOT_NUM;

	for (SLOTPOS i = posFirst; i < posNum; ++i)
	{
		if ( !m_pQuickContainer->IsEmpty(i) )
		{
			pQuickSlot[slotQuitInfo.m_Count].m_QuickPos = i;
			((QuickPaneSlot &)m_pQuickContainer->GetSlot(i)).CopyOut( pQuickSlot[slotQuitInfo.m_Count].m_Stream );

			++slotQuitInfo.m_Count;			
		}
	}
}


//------------------------------------------------------------------------------
VOID Hero::GetStyleTotalInfo( sTOTALINFO_STYLE & OUT slotStyleInfo )
{
	sSTYLE_SLOT * pStyleSlot = slotStyleInfo.m_Slot; 

	slotStyleInfo.m_Count = 0;
	
	SLOTPOS posFirst = 0;
	SLOTPOS posNum = MAX_STYLE_SLOT_NUM;

	for (SLOTPOS i = posFirst; i < posNum; ++i)
	{
		if ( !m_pStyleContainer->IsEmpty(i) )
		{
			pStyleSlot[slotStyleInfo.m_Count].m_StylePos = i;
			((StyleSlot &)m_pStyleContainer->GetSlot(i)).CopyOut( pStyleSlot[slotStyleInfo.m_Count].m_Stream );

			++slotStyleInfo.m_Count;			
		}
	}
}


//------------------------------------------------------------------------------
VOID Hero::SetInventoryTotalInfo(const sTOTALINFO_INVENTORY & IN ItemInfo )
{
	const sITEM_SLOTEX * pSlot = ItemInfo.m_Slot;
	SLOTPOS posFirst = 0;
	SLOTPOS posNum = ItemInfo.m_InvenCount;

	for(SLOTPOS i=posFirst;i<posNum;++i)
	{
		ItemSlot InvenSlot(pSlot[i].m_Stream);
		m_pInventoryContainer->InsertSlot(pSlot[i].m_ItemPos, InvenSlot);
	}

	//////////////////////////
	posFirst = ItemInfo.m_InvenCount;
	posNum = posNum + ItemInfo.m_TmpInvenCount;

	for(SLOTPOS i=posFirst;i<posNum;++i)
	{
		ItemSlot TempInvenSlot(pSlot[i].m_Stream);
		m_pTempInvenContainer->InsertSlot(pSlot[i].m_ItemPos, TempInvenSlot);
	}
}


//------------------------------------------------------------------------------
VOID Hero::SetQuickTotalInfo(const sTOTALINFO_QUICK & IN slotQuitInfo )
{
	const sQUICK_SLOT * pSlot = slotQuitInfo.m_Slot;
	SLOTPOS posFirst = 0;
	SLOTPOS posNum = slotQuitInfo.m_Count;

	for(SLOTPOS i=posFirst;i<posNum;++i)
	{
		QuickPaneSlot TempQuickSlot(pSlot[i].m_Stream);
		
		switch (TempQuickSlot.GetOrgSlotIndex())
		{
		case SI_EQUIPMENT:
			_asm nop;

		case SI_INVENTORY:
			{

				theItemManager.LinkItemToQuick	(TempQuickSlot.GetOrgSlotIndex()
                                             ,TempQuickSlot.GetOrgPos()
															,pSlot[i].m_QuickPos
															,TempQuickSlot.GetOrgCode());
			}
			break;

		case SI_SKILL:
			{
				theItemManager.LinkSkillToQuick	(TempQuickSlot.GetOrgSlotIndex()
                                             ,TempQuickSlot.GetOrgCode()
															,pSlot[i].m_QuickPos);
			}
			break;

		default:
			ASSERT(0);
		break;
		}	
	}

	theItemManager.UpdateQuickInfo();
}


//------------------------------------------------------------------------------
VOID Hero::SetStyleTotalInfo(const sTOTALINFO_STYLE & IN slotStyleInfo )
{
	const sSTYLE_SLOT * pSlot = slotStyleInfo.m_Slot;
	SLOTPOS posFirst = 0;
	SLOTPOS posNum = slotStyleInfo.m_Count;

	for(SLOTPOS i=posFirst;i<posNum;++i)
	{		
		theItemManager.LinkStyle	(pSlot[i].m_Stream.StylePart.Code
                                 ,pSlot[i].m_StylePos)			;
	}
}



//------------------------------------------------------------------------------
VOID Hero::SetSkillTotalInfo(const sTOTALINFO_SKILL & IN SkillInfo )
{
    theSkillStorageManager.SetSkillInfos(SkillInfo);
    UpdateSkillAttributes();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID Hero::LearnSkill(LPCSTR szSkillName, INT nLevel )
{
	sTOTALINFO_SKILL SkillInfo;

	sSKILLINFO_COMMON*	pSkill;
	pSkill = theSkillInfoParser.GetInfo(szSkillName);
	__CHECK2_PTR(pSkill,;);

	pSkill = theSkillInfoParser.GetInfo((SLOTCODE)(pSkill->m_SkillCode + nLevel-1));
	__CHECK2_PTR(pSkill,;);

	SkillInfo.m_Count	= 1;
	SkillInfo.m_Slot[0].m_SkillPos					= INVALID_POSTYPE;
	SkillInfo.m_Slot[0].m_Stream.SkillPart.wCode	= pSkill->m_SkillCode;

    theSkillStorageManager.SetSkillInfos(SkillInfo, TRUE);
    UpdateSkillAttributes();
}


//------------------------------------------------------------------------------
DWORD Hero::GetCurrentAttackStyle()const
{
	//由服务端来选择default
	//if(m_dwAttackStyle == 0)
	//{
	//	m_dwAttackStyle = theSkillDefaultParser.GetSkill(GetCharInfo()->m_byClassCode);
	//}
	return m_dwAttackStyle;
}


void Hero::UpdateStyleAttributeIn()
{

	HeroEquipmentContainer *	pEquipContainer = (HeroEquipmentContainer *)theItemManager.GetContainer(SI_EQUIPMENT);
	BaseSlot &						slotDat = pEquipContainer->GetSlot(EQUIPPOS_WEAPON);

	////////////////////////////////////////
	if (!slotDat.GetCode())
	{
		UpdateShotStyle();
	}

	////////////////////////////////
	else 
	{
		DWORD dwType = GetWeaponKind();

		/////////////////////////////
		sSTYLEINFO_BASE *pInfo;
		
		pInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)m_dwOldAttackStyle);

		if(	pInfo 
			&& (	(DWORD)pInfo->m_WeaponDefines == GetWeaponKind()
				||	pInfo->m_WeaponDefines == -1))
		{
			m_dwAttackStyle=m_dwOldAttackStyle;
		}
		else
		{
			DWORD dwStyleNew(0);

			switch(dwType)
			{
			case ITEMTYPE_TWOHANDSWORD:		
				dwStyleNew	= STYLECODE_TWOHANDSWORD_NORMAL;
				break;
			case ITEMTYPE_TWOHANDAXE:
				dwStyleNew	= STYLECODE_TWOHANDAXE_NORMAL;
				break;
			case ITEMTYPE_ONEHANDSWORD:
				dwStyleNew	= STYLECODE_ONEHANDSWORD_NORMAL;
				break;
			case ITEMTYPE_SPEAR:
				dwStyleNew	=STYLECODE_SPEAR_NORMAL;
				break;		
			case ITEMTYPE_STAFF:
				dwStyleNew	=STYLECODE_STAFF_NORMAL;
				break;

			case ITEMTYPE_ORB:
				dwStyleNew	=STYLECODE_ORB_NORMAL;
				break;

			case ITEMTYPE_CROSSBOW:
				dwStyleNew	=STYLECODE_ETHER_NORMAL;
				break;
			case ITEMTYPE_ETHERWEAPON:
				dwStyleNew	=STYLECODE_CROSSBOW_NORMAL;
				break;

			default:
				ASSERT(!"not supported");
				UpdateShotStyle();
				break;
			}
			m_dwAttackStyle = dwStyleNew;
		}
	}
}


//------------------------------------------------------------------------------
DWORD Hero::GetWeaponKind()
{
	HeroEquipmentContainer * pEquipContainer = (HeroEquipmentContainer *)theItemManager.GetContainer(SI_EQUIPMENT);
	BaseSlot & slotDat = pEquipContainer->GetSlot(EQUIPPOS_WEAPON);

	if (!slotDat.GetCode())
		return WEAPONTYPE_BOW;

	{
		sITEMINFO_BASE *pItemInfo = theItemInfoParser.GetItemInfo(slotDat.GetCode());
		ASSERT(pItemInfo && "无效的装备信息.");

		return pItemInfo->m_wType;
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
eARMOUR_TEX_TYPE Hero::GetArmourTexture()
{
	HeroEquipmentContainer * pEquipContainer = (HeroEquipmentContainer *)theItemManager.GetContainer(SI_EQUIPMENT);
	BaseSlot & slotDat = pEquipContainer->GetSlot(EQUIPPOS_ARMOR);

	if (!slotDat.GetCode())
		return ARMOUR_TEX_CLOTH;

	sVITEMINFO_BASE *pVItemInfo;
	pVItemInfo = theItemInfoParser.GetVItemInfo(slotDat.GetCode());
	ASSERT(pVItemInfo && "无效的装备信息.");

	return (eARMOUR_TEX_TYPE)pVItemInfo->ARMOUR.m_byArmourTexture;
}


//----------------------------------------------------------------------------
BOOL  Hero::CanUseSkill(DWORD skillID,BOOL bShowFailMessage)
{
	//	if(GetCurrentState()==STATE_SIT)
	//		return FALSE;

	if (theGeneralGameParam.GetSpecialMode())
		return TRUE;

	//SkillValidation	valid(this,skillID,bShowFailMessage);
	//return valid.CanUseSkill();
	__CHECK (CanUseSkillLimitWeapon(skillID, bShowFailMessage));

	__CHECK (CanUseSkillLimitLevel( skillID, bShowFailMessage));

	__CHECK (CanUseSkillLimitMP(skillID, bShowFailMessage));

	__CHECK (CanUseSkillLimitHP(skillID, bShowFailMessage));
	
	__CHECK (CanUseSkillLimitCoolTime(skillID, bShowFailMessage));
	
	return TRUE;
}


//#ifdef USE_OLD//x

BOOL Hero::CanUseSkillWithoutCooltime(DWORD skillID, BOOL bShowFailMessage /*= FALSE*/)
{
	if (theGeneralGameParam.GetSpecialMode() )
		return TRUE;

	BOOL bCheck = FALSE;
	bCheck = CanUseSkillLimitWeapon(skillID, bShowFailMessage);
	if ( !bCheck )
	{
		return FALSE;
	}

	bCheck = CanUseSkillLimitLevel(skillID, bShowFailMessage);
	if ( !bCheck )
	{
		return FALSE;
	}

	bCheck = CanUseSkillLimitMP(skillID, bShowFailMessage);
	if ( !bCheck )
	{
		return FALSE;
	}
	
	bCheck = CanUseSkillLimitHP(skillID, bShowFailMessage);
	if ( !bCheck )
	{
		return FALSE;
	}

	return TRUE;
}

//----------------------------------------------------------------------------
//BOOL Hero::CanUseSkillLimitHP(DWORD skillID, BOOL bShowFailMessage /*= FALSE*/)
//{
//	sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillID);
//	__VERIFY_PTR(pInfo
//					, FMSTR("CanUseSkillLimitHP 发现技能为NULL %d\n",skillID));
//
//	if (GetHP() < pInfo->m_wHPSpend)
//	{
//		if (bShowFailMessage)
//		{
//			OUTPUTCHAT(TEXTRES_SKILL_FAIL_NOT_ENOUGH_HP);
//		}
//		return FALSE;
//	}
//
//	return TRUE;
//}

//----------------------------------------------------------------------------
BOOL Hero::CanUseSkillLimitMP(DWORD skillID, BOOL bShowFailMessage /*= FALSE*/)
{
	sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillID);

	__VERIFY_PTR(pInfo
					, FMSTR("CanUseSkillLimitWeapon 发现技能为NULL %d\n",skillID));


	if (GetMP() < pInfo->m_wMPSpend)
	{
		if (bShowFailMessage)
		{
			OUTPUTCHAT(TEXTRES_SKILL_FAIL_NOT_ENOUGH_MP);

			theHeroActionInput.PlayNotEnoughManaSound();
		}

		return FALSE;
	}

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL Hero::CanUseSkillLimitLevel(DWORD skillID, BOOL bShowFailMessage /*= FALSE*/)
{
	sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillID);
	__VERIFY_PTR(pInfo
					, FMSTR("CanUseSkillLimitWeapon 发现技能为NULL %d\n",skillID));


	if (pInfo->m_wRequireLV > GetCharInfo()->m_LV)
	{
		if (bShowFailMessage)
		{
			OUTPUTCHATF(TEXTRES_SKILL_FAIL_NOT_ENOUGH_LEVEL,pInfo->m_wRequireLV);
		}
		return FALSE;
	}

	return TRUE;
}


//----------------------------------------------------------------------------
BOOL Hero::CanUseSkillLimitWeapon(DWORD skillID,BOOL bShowFailMessage /*= FALSE*/)
{
	sSKILLINFO_BASE *pInfo = theSkillInfoParser.GetSkillInfo((SLOTCODE)skillID);
	__VERIFY_PTR(pInfo
					, FMSTR("CanUseSkillLimitWeapon 发现技能为NULL %d\n",skillID));

	// 
	if (pInfo->m_WeaponDefines[0] != -1)
	{
		// 
		//const int	iWeaponName = 30000;
		BOOL bPass = FALSE;
		DWORD dwCurWeaponKind = GetWeaponKind();
		//
		if (dwCurWeaponKind == 0)
		{
			if (bShowFailMessage)
			{
				LPCSTR szName = _STRING(pInfo->m_WeaponDefines[0] + TEXTRES_EQUIP_SingleSword_0 );
				OUTPUTCHATF(TEXTRES_SKILL_FAIL_NOT_PROPER_WEAPON, szName);//%s
				//(iWeaponName + pInfo->m_WeaponDefines[0])
			}
			return FALSE;
		}

		for (int a = 0; a < sSKILLINFO_BASE::WEAPONDEFINE_NUM; a++)
		{
			if (dwCurWeaponKind == (DWORD)pInfo->m_WeaponDefines[a])
			{
				bPass = TRUE;
				break;
			}
		}

		if (!bPass)
		{
			if (bShowFailMessage)
			{
				LPCSTR szName = _STRING(pInfo->m_WeaponDefines[0] + TEXTRES_EQUIP_SingleSword_0 );
				OUTPUTCHATF(TEXTRES_SKILL_FAIL_NOT_PROPER_WEAPON, szName);//%s
			}
			return FALSE;
		}
	}

	return TRUE;
}
//#endif

BOOL Hero::CanUseItem(DWORD dwWasteType,BOOL bShowFailMessage)
{
	util::Timer *pTimer = GetPlayerCookie().GetItemCoolTimer((BYTE)dwWasteType);
		
	if (!pTimer) 
		return TRUE;


	if (!pTimer->IsExpired(FALSE))
	{
		if (bShowFailMessage)
		{
			OUTPUTCHAT(TEXTRES_SKILL_FAIL_COOLTIME_NOT_EXPIRED);
		}
		return FALSE;
	}

	return TRUE;
}

//----------------------------------------------------------------------------
int   Hero::AddSkillEffect(SKILL_EFFECT *pEffect)
{
	int iID = Player::AddSkillEffect(pEffect);

	//theGameUIManager.UIRefresh(gameui::eUIGroup);
	AddUpdateFlag(CHAR_UPDATE_SKILLEFFECT);

	return iID;
}


//----------------------------------------------------------------------------
void Hero::DeleteSkillEffect(int iEffectID)
{
	Player::DeleteSkillEffect(iEffectID);

	//theGameUIManager.UIRefresh(gameui::eUIGroup);
	AddUpdateFlag(CHAR_UPDATE_SKILLEFFECT);

	return;
}



BOOL Hero::ProcessUpdateFlag(DWORD dwTick,DWORD dwFlags)
{
	__BOOL_SUPER(ProcessUpdateFlag(dwTick,dwFlags));

	const DWORD UPDATE_GROUP	= CHAR_UPDATE_SKILLEFFECT
										| CHAR_UPDATE_HPMPINFO
										| CHAR_UPDATE_DEATH
										| CHAR_UPDATE_EQUIPMENTS
										| CHAR_UPDATE_LEVELUP
										| CHAR_UPDATE_ATTRIB;

	if(_BIT_EXIST(dwFlags,  UPDATE_GROUP ))
	{
		theGameUIManager.UIRefresh(gameui::eUIGroup);
		theGameUIManager.UIRefresh(gameui::eUIBaseProperty);
	}

	//if(_BIT_TEST(dwFlags,  CHAR_UPDATE_LEVELUP ))
	//{
	//}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_EQUIPMENTS))
	{
		theGameUIManager.UIRefresh(gameui::eUIInventory);
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_PACKINFO))
	{
		theGameUIManager.UITriggerFunc(gameui::eUIInventory, gameui::eOnPackInfoChange);
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_EXPINFO))
	{
		theGameUIManager.UIRefresh(gameui::eUIMain);
	}

	//if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_SPAWN))
	//{
	//}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_SKILLINFO))
	{
		theGameUIManager.UIRefresh(gameui::eUISkill);
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_QUEST))
	{
		theGameUIManager.UIRefresh(gameui::eUIQuest);
		//s_CUI_ID_FRAME_Task.Refeash( m_vectorActiveQuestInfo );
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_STYLE))
	{
		theGameUIManager.UIRefresh(gameui::eUIActionUI);
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_PARTY))
	{
		theGameUIManager.UIRefresh(gameui::eUITeamHero);
	}

	if(_BIT_TEST(m_dwUpdateFlags, CHAR_UPDATE_TRADE))
	{
		theGameUIManager.UIRefresh(gameui::eUIBargaining);
	}


	return TRUE;
}


//----------------------------------------------------------------------------
eTRIGGER_STATE	 Hero::GetAreaTriggerState( int iAreaID  ) 
{
	AreaTriggerStateMap::iterator iter;

	iter = m_TriggerStates.find(iAreaID);

	
	if (iter == m_TriggerStates.end())
	{
		return TRIGGER_STATE_NONE;
	}

	return (iter->second); 
}


//----------------------------------------------------------------------------
void	Hero::SetAreaTriggerState( int iAreaID,eTRIGGER_STATE TriggerState ) 
{
	AreaTriggerStateMap::iterator iter;

	iter = m_TriggerStates.find(iAreaID);

	
	if (iter != m_TriggerStates.end())
	{
		iter->second = TriggerState;
	}
	else
	{
		m_TriggerStates.insert(AreaTriggerStateValue(iAreaID,TriggerState));
	}
}


//----------------------------------------------------------------------------
eSHORTCUT_AREA_STATE  Hero::GetAreaShortcutState( DWORD dwAreaID  ) 
{
	AreaShortcutStateMap::iterator iter;
	
	iter = m_AreaShorcutStates.find(dwAreaID);
	
	if (iter == m_AreaShorcutStates.end())
	{
		return SHORTCUT_AREA_STATE_NONE;
	}

	return (iter->second); 
}


//----------------------------------------------------------------------------
void	Hero::SetAreaShortcutState( DWORD dwAreaID, eSHORTCUT_AREA_STATE ShortcutState )
{
	AreaShortcutStateMap::iterator iter;

	iter = m_AreaShorcutStates.find(dwAreaID);

	
	if (iter != m_AreaShorcutStates.end())
	{
		iter->second = ShortcutState;
	}
	else
	{
		m_AreaShorcutStates.insert(AreaShortcutStateValue(dwAreaID,ShortcutState));
	}
}
	
void Hero::InitTriggerState()
{
	m_TriggerStates.clear();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD Hero::GetAttackDelay()const
{
	sSTYLEINFO_BASE *pBaseStyleInfo;
	

	pBaseStyleInfo = theSkillInfoParser.GetStyleInfo( (SLOTCODE)GetCurrentAttackStyle() );
	if(!pBaseStyleInfo)
		return GetAttackSpeed();

	//////////////////////////////////////////
	DWORD dwActionDelay;

	if(m_AttackSequence >= ATTACK_SEQUENCE_MAX)
	{
		LOGINFO	("[Hero::GetAttackDelay] Invalid attack sequence! Style[%d] m_AttackSequence[%d]"
					, GetCurrentAttackStyle()
					, m_AttackSequence );
		return GetAttackSpeed();
	}
	dwActionDelay = pBaseStyleInfo->m_dwAttackTime[m_AttackSequence];


	//dwActionDelay = (DWORD)(dwActionDelay * 0.9f);
	float fSpeed = GetPhysicalAttackSpeed();
	if( 0 !=  fSpeed)
		return( (DWORD)( dwActionDelay / fSpeed ) );
	return dwActionDelay;
};

float Hero::GetAttackSpeedModifier()const
{
	if (m_bCustomAttackSpeed)
		return m_fCustomAttackSpeed;
	return ((float)m_pPlayerAttributes->GetAttSpeedRatio() / 100.0f) * WEAPON_MULTIPLIER;		
}

float Hero::GetMoveSpeedModifier()const
{
	if (m_bCustomMoveSpeed)
		return m_fCustomMoveSpeed;
	return ((float)m_pPlayerAttributes->GetMoveSpeedRatio() / 100.0f);
}


DWORD Hero::GetMaxHP()const
{
	return	(DWORD)(m_pPlayerAttributes->GetMaxHP() 
						 *(1.0f + (float)m_pPlayerAttributes->GetMaxHPPer()/100.f));
}


DWORD Hero::GetMaxMP()const
{
	return	(DWORD)(m_pPlayerAttributes->GetMaxMP()
			*  (1.0f + (float)m_pPlayerAttributes->GetMaxMPPer()/100.f));
}

void Hero::SetCustomAttackSpeed(float fSpeed)
{
	m_bCustomAttackSpeed = TRUE;
	m_fCustomAttackSpeed = fSpeed;

}

void Hero::SetCustomMoveSpeed(BOOL bUse,float fSpeed)
{
	m_bCustomMoveSpeed = bUse;
	m_fCustomMoveSpeed = fSpeed;
}


VOID Hero::ProcessPlayerError(DWORD dwErrCode,DWORD dwCategory)
{
	DWORD dwTextID = 0;//TEXTRES_UNKNOWN_ERROR;

	#define _CASE_BATTLE(c)		case _RC_WAR(c):		dwTextID = (TEXTRES_BATTLE_##c); break
	#define _CASE_GM(c)			case _RC_GM(c):		dwTextID = (TEXTRES_GM_##c); break
	#define _CASE_SKILL(c)		case _RC_SKILL(c):	dwTextID = TEXTRES_SKILL_ACTION_##c; break
	#define _CASE_SKILL2(c,t)	case _RC_SKILL(c):	dwTextID = TEXTRES_SKILL_ACTION_##t; break
	#define _CASE_QUEST(c)		case _RC_QUE(c):		dwTextID = (TEXTRES_QUEST_##c); break
	#define _CASE_PARTY(c)		case _RC_PTY(c):		dwTextID = (TEXTRES_PARTY_##c); break
	#define _CASE_TRADE(c)		case _RC_TRADE(c):	dwTextID = (TEXTRES_TRADE_##c); break
	#define _CASE_VENDOR(c)		case _RC_VEN(c):		dwTextID = (TEXTRES_VENDOR_##c); break


	//switch(pMsgHandler->m_byErrorCode)
	//{
	//case RC_BATTLE_STYLECODE_WHERE_DONOT_SELECT:
	//}
	switch(dwCategory)
	{
	case CG_STATUS:
		{
			dwTextID = TEXTRES_STATUS_STAT_FAILED;
		}break;

	case CG_TRADE:
		{
			dwTextID = TEXTRES_TRADE_FAILED;
			switch (dwErrCode)
			{
			_CASE_TRADE(SUCCESS							);
			_CASE_TRADE(FAILED							);
			_CASE_TRADE(CANCEL							);
			_CASE_TRADE(PLAYER1_HAVENOTSPACE			);
			_CASE_TRADE(PLAYER1_HAVENOTMONEY			);
			_CASE_TRADE(PLAYER2_HAVENOTSPACE			);
			_CASE_TRADE(PLAYER2_HAVENOTMONEY			);
			_CASE_TRADE(COUNTERPARTNER_NOTACCEPT	);
			_CASE_TRADE(COUNTERPARTNER_NOTPROPOSAL	);
			_CASE_TRADE(INVALID_STATE					);
			_CASE_TRADE(PLAYER1_NOT_EXIST				);
			_CASE_TRADE(PLAYER1_NOT_EXIST_ATFIELD	);
			_CASE_TRADE(PLAYER2_NOT_EXIST				);
			_CASE_TRADE(PLAYER2_NOT_EXIST_ATFIELD	);
			_CASE_TRADE(PLAYER1_NOT_IDLE				);
			_CASE_TRADE(PLAYER2_NOT_IDLE				);
			_CASE_TRADE(CREATE_FAIELD					);
			_CASE_TRADE(PUT_FAIELD						);
			_CASE_TRADE(PUT_MONEY_FAIELD				);
			_CASE_TRADE(GET_FAIELD						);
			_CASE_TRADE(GET_MONEY_FAIELD				);
			_CASE_TRADE(MODIFY_FAIELD					);
			_CASE_TRADE(PROPOSAL_FAIELD				);
			_CASE_TRADE(TOOFAR							);
			}
		}break;


	case CG_PARTY:
		{
			dwTextID = TEXTRES_PARTY_UNKNOWN_REASON;
			switch (dwErrCode)
			{
			_CASE_PARTY(PLAYER_NOTEXIST			);			
			_CASE_PARTY(ISNOT_MASTEROFPARTY		);		
			_CASE_PARTY(ISNOT_MEMBEROFPARTY		);		
			_CASE_PARTY(PARTY_NOTEXIST				);			
			_CASE_PARTY(REJECT_INVITEMENT			);			
			_CASE_PARTY(ISNOT_QUALIFIED_LEVEL	);		
			_CASE_PARTY(INVALID_ROOMTYPE			);			
			_CASE_PARTY(CANNOT_CHANGE_MASTER		);		
			_CASE_PARTY(ALREADY_IS_MEMBEROFPARTY);	
			_CASE_PARTY(WASNOT_REQUEST_PARTY		);		
			_CASE_PARTY(INVALID_DATATYPE			);			
			_CASE_PARTY(PLAYER_NOTEXIST_TO_FIELD);	
			_CASE_PARTY(CANNOT_FIND_TARGET		);		
			_CASE_PARTY(TARGET_ISNOT_ENEMY		);		
			_CASE_PARTY(PLAYER_OBSERVER_MODE		);		
		}
		}break;

	case CG_VENDOR:
		{
			enum
			{
				 TEXTRES_VENDOR_INSUFFICIENT_MONEY	= TEXTRES_NOT_ENOUGH_MONEY
				,TEXTRES_VENDOR_NOSPACEININVENTORY	= TEXTRES_RC_ITEM_NOSPACEININVENTORY
			};
			dwTextID = TEXTRES_VENDOR_FAILED;
			switch (dwErrCode)
			{
			//,_CASE_VENDOR(SUCCESS				);
			_CASE_VENDOR(INVALID_STATE		);
			_CASE_VENDOR(INVALID_VENDORSLOT	);
			_CASE_VENDOR(NOSPACEININVENTORY	);
			_CASE_VENDOR(INSUFFICIENT_MONEY	);
			_CASE_VENDOR(JUST_SHOW		);
			}
		}break;

	case CG_EVENT:
		{
			enum 
			{
				 TEXTRES_GM_NOTEXIST_USER		= TEXTRES_NOT_EXIST_USER
				,TEXTRES_GM_ROOM_NOT_EXIST		= TEXTRES_RC_ROOM_NOTEXISTROOM
				,TEXTRES_GM_INVALID_ARGUMENT	= TEXTRES_GM_INVALID_NUM_PARAM
				,
			};

			dwTextID = TEXTRES_QUEST_INVALID_ACTION;
			switch (dwErrCode)
			{
			_CASE_QUEST(PLAYER_NOT_EXISTED);
			_CASE_QUEST(INFO_NOT_EXISTED	);
			_CASE_QUEST(CANNT_DISCARD		);
			_CASE_QUEST(NOT_ACTIVATED		);
			_CASE_QUEST(INVALID_ACTION		);
			}
		}break;

	case CG_SKILL:
		{
		/////////////////////////////////////////
			dwTextID = TEXTRES_CHANGE_STYLE_FAIL;
			switch (dwErrCode)
			{
				_CASE_SKILL(SUCCESS				);
				_CASE_SKILL2(STATE_WHERE_CANNOT_ATTACK_ENEMY,	CANNOT_ATTACK_ENEMY		);
				_CASE_SKILL(COOLTIME_ERROR						);
				_CASE_SKILL(HPMP_RUN_SHORT						);
				_CASE_SKILL(DOES_NOT_HAVE						);
				_CASE_SKILL(CHAR_CLASS_LIMIT					);
				_CASE_SKILL(WEAPON_LIMIT						);
				_CASE_SKILL(REQUIRE_LEVEL_LIMIT				);
				_CASE_SKILL(REQUIRE_SKILLSTAT_LIMIT			);

				_CASE_SKILL(FAILED);									

				_CASE_SKILL(BASEINFO_NOTEXIST);						

				_CASE_SKILL(SEALING_STATE);						
				_CASE_SKILL(OUT_OF_RANGE);						

				_CASE_SKILL(REMAIN_SKILLPOINT_LACK);			
				_CASE_SKILL(MAX_LEVEL_LIMIT);					
				_CASE_SKILL(ALREADY_EXIST_SKILL);				

				_CASE_SKILL(INVALID_STATE);						
				_CASE_SKILL(NOTEXIST);							
				_CASE_SKILL(INVLIDPOS);							

				_CASE_SKILL(FIGHTING_ENERGY_FULL);				

				_CASE_SKILL(POSITION_INVALID);					

				_CASE_SKILL(SUMMONED_NOTEXIST);					
				_CASE_SKILL(TARGET_NOTEXIST);					
				_CASE_SKILL(DEPENDENCE_NOTFOUND);					

			}

			switch (dwErrCode)
			{
			case RC_SKILL_HPMP_RUN_SHORT:
				theHeroActionInput.PlayNotEnoughManaSound();
				break;
			}
		}break;

		/////////////////////////////////////////
	case CG_BATTLE:
		{
			dwTextID = TEXTRES_PLAYER_ATTACK_MISS;
			switch (dwErrCode)
			{
				_CASE_BATTLE(PLAYER_NOTEXIST_TO_FIELD);
				_CASE_BATTLE(INVALID_MAINTARGET);
				_CASE_BATTLE(PLAYER_STATE_WHERE_CANNOT_ATTACK_ENEMY);	
				_CASE_BATTLE(VKR_RELOAD_COUNT_LACK);
				_CASE_BATTLE(TRAGET_STATE_WHERE_CANNOT_ATTACKED);
				_CASE_BATTLE(OUT_OF_RANGE);
				_CASE_BATTLE(INVLIDPOS);
				_CASE_BATTLE(THRUST_DIST_OVER);
				_CASE_BATTLE(ALREADY_DOING_ACTION);
				_CASE_BATTLE(BASEINFO_NOTEXIST);
				_CASE_BATTLE(STYLECODE_WHERE_DONOT_SELECT);
				_CASE_BATTLE(CHAR_CLASS_LIMIT);
				_CASE_BATTLE(WEAPON_LIMIT);
			}
		}break;

		///////////////////////////////////////////////////
	case CG_GM:
		{
			enum 
			{
				 TEXTRES_GM_NOTEXIST_USER		= TEXTRES_NOT_EXIST_USER
				,TEXTRES_GM_ROOM_NOT_EXIST		= TEXTRES_RC_ROOM_NOTEXISTROOM
				,TEXTRES_GM_INVALID_ARGUMENT	= TEXTRES_GM_INVALID_NUM_PARAM
				,
			};
			dwTextID = TEXTRES_PLAYER_ATTACK_MISS;
			switch (dwErrCode)
			{
				_CASE_GM(ROOM_NOT_EXIST			);				
				_CASE_GM(INVALID_ARGUMENT		);				
				_CASE_GM(ISNOT_GM					);						
				_CASE_GM(NOTEXIST_USER			)	;				
				_CASE_GM(CANNOT_RESURRECTION	)	;		
				_CASE_GM(CANNOT_RECOVERY		);				
				_CASE_GM(PLAYER_NOTEXIST		);				
			}
		}break;
	}

	///内容输出...
	OUTPUTTIP(dwTextID);
}


void Hero::UpdateSkillAttributes()
{
	if (!GetPlayerAttributes())
		return;


	AttributeAffectorSkillHandle SkillCalc( *GetPlayerAttributes() );

	SkillCalc.Clear();

	UpdatePassiveSkillEffectAttributes(SkillCalc);	
	UpdateActiveSkillEffectAttr(SkillCalc);	
   ApplyStyleAttributes(GetCurrentAttackStyle(),SkillCalc);

	GetPlayerAttributes()->Update();

	AddUpdateFlag(CHAR_UPDATE_ATTRIB | CHAR_UPDATE_HPMPINFO);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::OnDamage	(DWORD                dwAdditionalEffect
                     ,BOOL                 bRight
							,eWEAPONSOUNDKIND     eSoundKind
							,eARMOUR_TEX_TYPE eTexture
							,DWORD                dwSkillCode)
{
	///刷新Hero UI信息
	//UpdateToVObject();
	AddUpdateFlag(CHAR_UPDATE_ATTRIB | CHAR_UPDATE_HPMPINFO);

	Player::OnDamage(dwAdditionalEffect,bRight,eSoundKind,eTexture,dwSkillCode);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::UpdatePassiveSkillEffectAttributes(AttributeAffectorSkillHandle &SkillCalc)
{
	int nSkillNum;

	nSkillNum = theSkillStorageManager.GetSkillNumPassive();

	for (int a = 0; a < nSkillNum; a++)
	{
		const SkillStorageInfo *	pInfo;
		SkillDetailInfo *		pBaseSkillInfo;

		pInfo = theSkillStorageManager.GetSkillPassive(a);

		if (!pInfo)
			continue;

		if (pInfo->state != SLOT_UISTATE_ACTIVATED)
			continue;

		pBaseSkillInfo = (SkillDetailInfo*)pInfo->pCurrSkillInfo;
		if( !pBaseSkillInfo || !pBaseSkillInfo->IsSkill())
			continue;

		ASSERT( pBaseSkillInfo->m_bySkillType == SKILL_TYPE_PASSIVE );

		BOOL bCanApply = FALSE;

		//////////////////////////////////////////////
		if (pBaseSkillInfo->m_WeaponDefines[0] == -1)
		{
			bCanApply = TRUE;			
		}

		DWORD dwWeaponType = GetWeaponKind();
		for (int b = 0; b < 4; b++)
		{			
			if ((DWORD)pBaseSkillInfo->m_WeaponDefines[b] == dwWeaponType)
			{
				bCanApply = TRUE;
				break;
			}
		}

		if (!bCanApply)
			continue;
		

		//////////////////////////////////////////////
		sABILITYINFO_BASE * pAbilityInfo = NULL ;

		pBaseSkillInfo->SetFirst();
		while( pAbilityInfo = pBaseSkillInfo->GetNext() )
		{
			eATTRIBUTE_TYPE eAttrID = theSkillInfoParser.GetAttrType(*pAbilityInfo);

			if( pAbilityInfo->m_wAbilityID == ABILITY_WEAPON_MASTERY )
			{
				SkillCalc.AddAttr	( ATTRIBUTE_OPTION_ALL_ATTACK_POWER
										, VALUE_TYPE_VALUE
										, pAbilityInfo->m_iParam[0] );

				SkillCalc.AddAttr	( ATTRIBUTE_ATTACK_SPEED
										, VALUE_TYPE_VALUE
										, pAbilityInfo->m_iParam[1] );
			}


			if( eAttrID != ATTRIBUTE_TYPE_INVALID )
			{
				int iValueType = pAbilityInfo->m_iParam[0];
				int iValue		= pAbilityInfo->m_iParam[1];

				SkillCalc.AddAttr( eAttrID, iValueType, iValue );
			}
		}
	}
//#endif
}



static void OnDeadCity(BOOL bAgree)
{
	if(!bAgree)
		return;
	//DeadConfirmInHunting(TRUE);
}

void Hero::OnDead()
{
	if(m_bDeaded)
		return;

	m_bDeaded = TRUE;
	_SUPER::OnDead();
	LOGTIP(_STRING(TEXTRES_PLAYER_RESURRECTION_HERE),eTIP_DEATH,NULL);
	//LOGTIP(_STRING(TEXTRES_PLAYER_RESURRECTION_CITY),eTIP_DEATH, OnDeadCity);
}


//----------------------------------------------------------------------------
void  Hero::OnLevelUp(DWORD dwLevel, DWORD dwHP, DWORD dwMP, DWORD dwRemainStat,DWORD dwRemainSkill)
{

	SetLevel((LEVELTYPE)dwLevel);
	GetCharInfo()->m_dwRemainStat		= dwRemainStat;
	GetCharInfo()->m_dwRemainSkill	= dwRemainSkill;
	
	theSoundEffect.PlayUI(80);

	Player::OnLevelUp(dwLevel, dwHP, dwMP, dwRemainStat, dwRemainSkill);

	if(!theGeneralGameParam.IsEnableNetwork())
		theScriptManager.StartupVM(SCRIPTTYPE_LEVELUP, dwLevel,m_pScriptChar);
}

void  Hero::OnRevive(const Vector3D& vCurPos, DWORD dwHP, DWORD dwMP,DWORD dwLevel)
{
	m_bDeaded	= FALSE;
	_SUPER::OnRevive( vCurPos,  dwHP,  dwMP, dwLevel);

	//SetLevel(dwLevel);
	//GetCharInfo()->m_dwRemainStat		= dwRemainStat;
	//GetCharInfo()->m_dwRemainSkill	= dwRemainSkill;
	//if (pChr->IsHero())
	//{
	//	if (theGeneralGameParam.GetSpecialMode())
	//	{
	//	}
	//	SetBrightDelay(1500);
	//}
	
	theSoundEffect.PlayUI(80);

	if(!theGeneralGameParam.IsEnableNetwork())
		theScriptManager.StartupVM(SCRIPTTYPE_PLAYERDEAD, dwLevel,m_pScriptChar);
}



void  Hero::OnFirstEnterWorld()
{
	if(!theGeneralGameParam.IsEnableNetwork())
		theScriptManager.StartupVM(SCRIPTTYPE_PLAYERCREATE,0,m_pScriptChar);
}

void  Hero::OnGameInPlaying()
{
	m_bEnterWorld			= TRUE;
	if(!theGeneralGameParam.IsEnableNetwork())
		theScriptManager.StartupVM(SCRIPTTYPE_PLAYERONLINE,0,m_pScriptChar);
	OnQuestDone(0);
}




void Hero::UpdateToVObject()
{
	_SUPER::UpdateToVObject();
	//theGameUIManager.UIRefresh(gameui::eUIGroup);
	AddUpdateFlag(CHAR_UPDATE_ATTRIB);
}


void Hero::OnFootStep()
{
	Player::OnFootStep();
}


void Hero::SetMaxHP( DWORD /*iHP*/ )
{
	return;
}
void Hero::SetMaxMP( DWORD /*iMP*/ )
{
	return;
}
void Hero::SetAttackSpeedRatio(int /*iSpeed*/)
{
	return;
}
void Hero::SetMoveSpeedRatio(int /*iSpeed*/)
{
	return;
}

BOOL Hero::CanEquipItem()
{
	switch (GetCurrentState())
	{
	case STATE_IDLE:
	case STATE_SIT:
	case STATE_EMOTICON:
	case STATE_MOVE:
	case STATE_KEYBOARDMOVE:
		break;
	default:
		{
			return FALSE;
		}
		break;
	}

	__CHECK(m_iStabberShotDelay <= 0);
	__CHECK(m_iAttackStandTime <= 0);

	return TRUE;

}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::SetExp(DWORD dwExp)
{
	INT  nOffExp;

	nOffExp = dwExp - GetCharInfo()->m_dwExp;
	OUTPUTCHATF(TEXTRES_GET_EXP, nOffExp);

	theItemLogManager.PushExp(nOffExp);

	_SUPER::SetExp(dwExp);
}


//void Hero::SetHP(DWORD dwHP)
//{
//	Player::SetHP(dwHP);
//}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void  Hero::HideHelmet(BOOL bFlag,BOOL bRefresh)
{
	Player *pPlayer;

	pPlayer = (Player *)theObjectManager.GetObject(EQUIPMENT_CHARACTER_KEY);
	if (pPlayer) 
	{
		pPlayer->HideHelmet(bFlag,bRefresh);
	}

	Player::HideHelmet(bFlag,bRefresh);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD Hero::LockTargetToList()
{

	m_TabTargetInfo.wTargetCount = 0;

	sTAB_TARGET_INFO			itemMin = { 0, 99999.0f, 0, 0 };
	TabTargetInfoVectorIt it;
	int							iCount = 0;

	for( it = m_vectorTargetMonster.begin() ; it != m_vectorTargetMonster.end() ; ++it )
	{
		if( itemMin.fDistance > (*it).fDistance )
		{
			itemMin.dwObjectKey	= (*it).dwObjectKey;
			itemMin.fDistance		= (*it).fDistance;

			m_TabTargetInfo.wTargetCount = (WORD)iCount;
		}

		++iCount;
	}

	m_TabTargetInfo.dwObjectKey	= itemMin.dwObjectKey;
	m_TabTargetInfo.fDistance		= itemMin.fDistance;

	return itemMin.dwObjectKey;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Hero::TogglePrevTarget	()
{

	TabTargetInfoVectorIt	it;
	int							iCount	= 0;
	BOOL							bFind		= FALSE;

	for( it = m_vectorTargetMonster.begin() ; it != m_vectorTargetMonster.end() ; ++it )
	{
		if( m_TabTargetInfo.dwObjectKey == (*it).dwObjectKey )
		{
			if( it != m_vectorTargetMonster.begin() )
			{
				--it;
			}
			else
			{
				it = m_vectorTargetMonster.end();
				--it;
			}
			m_TabTargetInfo.dwObjectKey	= (*it).dwObjectKey;
			m_TabTargetInfo.fDistance		= (*it).fDistance;
			m_TabTargetInfo.wTargetCheck	= 0;
			m_TabTargetInfo.wTargetCount	= (WORD)(iCount - 1);

			bFind = TRUE;
			break;
		}
		++iCount;
	}
	return bFind;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Hero::ToggleNextTarget	()
{
	TabTargetInfoVectorIt	it;
	int									iCount	= 0;
	BOOL									bFind		= FALSE;

	for( it = m_vectorTargetMonster.begin() ; it != m_vectorTargetMonster.end() ; ++it )
	{
		if( m_TabTargetInfo.dwObjectKey == (*it).dwObjectKey )
		{
			++it;
			if( it == m_vectorTargetMonster.end() )
				it = m_vectorTargetMonster.begin();

			m_TabTargetInfo.dwObjectKey	= (*it).dwObjectKey;
			m_TabTargetInfo.fDistance		= (*it).fDistance;
			m_TabTargetInfo.wTargetCheck	= 0;
			m_TabTargetInfo.wTargetCount	= (WORD)iCount;

			bFind = TRUE;
			break;
		}
		++iCount;
	}//for( it = m_vectorTargetMonster.begin() ; it != m_vectorTargetMonster.end() ; ++it )

	return bFind;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Hero::SortTargetList()
{
	__CHECK( ExistTarget() );

	sort	( m_vectorTargetMonster.begin()
			, m_vectorTargetMonster.end() );
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::LockFirstTarget()
{
	sTAB_TARGET_INFO*			pTarget;
	pTarget = GetTargetAt(0);
	if(pTarget)
	{
		m_TabTargetInfo.dwObjectKey	= pTarget->dwObjectKey;
		m_TabTargetInfo.fDistance		= pTarget->fDistance;
		m_TabTargetInfo.wTargetCheck	= 0;
		m_TabTargetInfo.wTargetCount	= (WORD)-1;
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void Hero::LockLastTarget()
{
	sTAB_TARGET_INFO*			pTarget;
	pTarget = GetTargetAt(m_vectorTargetMonster.size()-1);
	if(pTarget)
	{
		m_TabTargetInfo.dwObjectKey	= pTarget->dwObjectKey;
		m_TabTargetInfo.fDistance		= pTarget->fDistance;
		m_TabTargetInfo.wTargetCheck	= 0;
		m_TabTargetInfo.wTargetCount	= (WORD)-1;
	}
}


void Hero::UpdateAppearance(BOOL bAddUpdateFlag)
{
	Player::UpdateAppearance(bAddUpdateFlag);
}


};

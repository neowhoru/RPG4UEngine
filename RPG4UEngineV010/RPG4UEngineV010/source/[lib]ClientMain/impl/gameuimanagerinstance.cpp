/*////////////////////////////////////////////////////////////////////////
文 件 名：GameUIManagerInstance.cpp
创建日期：2007年4月3日
最后更新：2007年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameUIManagerInstance.h"
#include "GameMainUtil.h"
#include "FontTextureManager.h"
#include "VUIPictureManager.h"
#include "ApplicationBase.h"
#include "VUIIconMouseTip.h"
#include "ISkillSpecialEffect.h"
#include "V3DConfig.h"
#include "SkillSpecialEffectManager.h"
#include "GameIconDragListener.h"
#include "VUCtrlIconDragManager.h"
#include "ToolTipToken.h"
#include "ToolTipItemToken.h"
#include "UIListener.h"
#include "VUIBaseData.h"
#include "VUIIconInfoFactoryManager.h"
#include "IV3DTextureManager.h"

using namespace gameui;
using namespace gamemain;
using namespace sound;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameUIManagerInstance, ()  , gamemain::eInstPrioGameUIManager);




LPCSTR MOVE_POINT_EFFECT_NAME		= "MoveToTarget Effect";
LPCSTR AREA_POINT_EFFECT_NAME		= "AreaSkill Effect";
LPCSTR ATTACK_POINT_EFFECT_NAME	= "AttackPoint Effect";

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameUIManagerInstance::GameUIManagerInstance()
{
	m_bMovePointerFlag	= FALSE;
	m_bAreaPointerFlag	= FALSE;
	m_bAttackPointerFlag	= FALSE;
}

GameUIManagerInstance::~GameUIManagerInstance()
{
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameUIManagerInstance::Init()
{
	UninstallToolTipManager(NULL);//安装默认


	theGameIconDragListener.Init();
	theIconDragManager.InstallListener(&theGameIconDragListener);
	theMouseHandler.InitSlotListener(&theGameIconDragListener);


	theVUIIconInfoFactoryManager.InitAllFactories();

	__INIT(theUIPictureManager.Init());

	g_UIBaseData.SetTipColors	(_COLOR(COLOR_TIPCOLOR0)
										,_COLOR(COLOR_TIPCOLOR1)
										,_COLOR(COLOR_WHITE)
										,_COLOR(COLOR_BLUE)
										,_COLOR(COLOR_GRAYED)
										,_COLOR(COLOR_GREEN)
										,_COLOR(COLOR_GOLD)
										,_COLOR(COLOR_YELLOW)
										,_COLOR(COLOR_RED	)
										,_COLOR(COLOR_PURPLE)
										);

	theUICtrlManager.InitProcInfo	(this);

	
	UIOnStartUp(gameui::eUIRoot);

	VUCtrlIconDrag::SetToolTipDelay(theGeneralGameParam.GetToolTipDelay());

	theUICtrlManager.SetPlaySoundFun( SoundEffect::PROC_PlayUI );
	theUICtrlManager.m_pLogInfoFun = ProcLogInfo;

	//UILoadUI(gameui::eUIRoot);移到root

	theApplication.AddListener(this);
	return TRUE;
}


void GameUIManagerInstance::Release()
{
	theApplication.RemoveListener(this);
	//UIUnloadUI(gameui::eUIRoot);移到root
	UIOnClose(gameui::eUIRoot);
	
	__END(theUIPictureManager.Release());

	__END(theVUIIconInfoFactoryManager.ReleaseAllFactories());
	theVUIIconInfoFactoryManager.RemoveAll();

	theIconDragManager.UninstallListener(&theGameIconDragListener);
	theMouseHandler.ReleaseSlotListener(&theGameIconDragListener);
	theGameIconDragListener.Release();

	UninstallToolTipManager(m_pToolTipManager);//安装默认
}


BOOL GameUIManagerInstance::FrameMove(DWORD dwTick)
{

	if(!g_BaseSetting.m_EnableRenderUI )
		return TRUE;

	TRUETIMEBLOCK( "GameUIManagerInstance::FrameMove" );
	theUICtrlManager.FrameMove(dwTick);

	//UIOnProcess(gameui::eUIRoot); 移到root
	UIFrameMove(gameui::eUIRoot,gameui::eUINull, dwTick);

	//if( !g_bRotateEnable )
	theCursorManager.FrameMove(dwTick);

	return TRUE;
}


BOOL GameUIManagerInstance::Render(DWORD dwTick)
{
//#ifndef NONE_TEMP
//	RenderUtil::TestingRender();
//#endif

	if(!g_BaseSetting.m_EnableRenderUI )
		return TRUE;

	TRUETIMEFUNC();

		
	IV3DRenderer* render = the3DEngine.GetRenderer();
	render->SetRenderState( D3DRS_FOGENABLE, FALSE );

//#ifdef NONE_TEMP
	if(!theApplicationSetting.m_ShowAdvancedMiniMap)
	{
		//TRUETIMEBLOCK("theUICtrlManager.Render");
		theUICtrlManager.Render(dwTick);
	}
//#endif


//#ifndef NONE_TEMP
//
////#ifdef NONE_TEMP
//	DrawText(208,3, FMSTR("fps:%d DeltaTick:%d", 666, 666));
//#endif



//#endif


	{
		TRUETIMEBLOCK("theUICtrlManager.RenderTips");
		theUICtrlManager.RenderTips(g_CurTime);
	}

	//if( !g_bRotateEnable )
	{
		TRUETIMEBLOCK("theCursorManager.Render");
		theCursorManager.Render(dwTick);
	}

	render->SetRenderState( D3DRS_FOGENABLE, TRUE );
	return TRUE;
}

void GameUIManagerInstance::OnWinMsg	(HWND   /*hWnd*/
                                       ,UINT   uMsg
													,WPARAM /*wParam*/
													,LPARAM /*lParam*/)
{
	//if(!theApplication.IsHasFocus() || theApplication.IsActive())
	//	return;
	switch(uMsg)
	{
	case WM_SETCURSOR:
		{
			theCursorManager.Update();
		}
		break;

   case WM_ACTIVATEAPP:
	case WM_SETFOCUS:
		{
			theVUICursorManager.OnEndDragCamera();
			theVUICursorManager.SetCursor(CURSORTYPE_DEFAULT);
			theCursorManager.Update();
		}
		break;
	}

//	if ( theMapViewManager.MsgProc( hWnd, uMsg, wParam, lParam ) == TRUE )

}

void GameUIManagerInstance::SetMovePointer(BOOL bFlag)
{
	if(m_bMovePointerFlag == bFlag && !bFlag)
		return;

	if(!bFlag)
	{
		m_bMovePointerFlag = FALSE;
		theSkillSpecialEffectManager.DelEffect( MOVE_POINT_EFFECT_NAME );
	}
	else
	{
		assert("must call SetMovePointer( bFlag,vPos)");
	}
}


void GameUIManagerInstance::SetMovePointer(BOOL bFlag,const VECTOR3D& vPos)
{
	if(m_bMovePointerFlag == bFlag && !bFlag)
		return;

	if (bFlag)
	{
		m_bMovePointerFlag	= TRUE;
		m_vMovePointerPos		= vPos;

		//客户端需要增加一个，目标点的效果
		ISkillSpecialEffect* pEffect = theSkillSpecialEffectManager.FindEffect( MOVE_POINT_EFFECT_NAME );
		if( pEffect )
		{
			if(pEffect->GetType() == SKILL_SPECIAL_EFFECT_MESH )
			{
				pEffect->SetPos(vPos.x,vPos.y,0.0f);
			}
		}
		else
		{
			pEffect = theSkillSpecialEffectManager.CreateEffect(PLAYEREFFECT_BYMESH);

			pEffect->SetName( MOVE_POINT_EFFECT_NAME );
			pEffect->Create( 15103, 0, 100000000 );
			pEffect->SetPos(vPos.x,vPos.y,0.0f);
			theSkillSpecialEffectManager.AddEffect( pEffect );
		}

	}
	else
	{
		m_bMovePointerFlag = FALSE;
		theSkillSpecialEffectManager.DelEffect( MOVE_POINT_EFFECT_NAME );
	}

}

void GameUIManagerInstance::SetAreaPointer	(BOOL            bFlag)
{
	if(m_bAreaPointerFlag == FALSE)
		return;

	if (!bFlag)
	{
		m_bAreaPointerFlag = FALSE;
		ISkillSpecialEffect* pEffect = theSkillSpecialEffectManager.FindEffect( AREA_POINT_EFFECT_NAME );
		if( pEffect )
		{
			pEffect->SetVisible(FALSE);
		}
		//theSkillSpecialEffectManager.DelEffect( MOVE_POINT_EFFECT_NAME );
	}
	else
	{
		assert("must call SetAreaPointer( bFlag,vPos)");
	}
}


void GameUIManagerInstance::SetAreaPointer	(BOOL            bFlag
                                             ,const VECTOR3D& vPos
															,WORD            /*wArea*/
															,COLOR           /*color*/)
{
	if(m_bAreaPointerFlag == FALSE)
		return;
	//if(m_bAreaPointerFlag == bFlag)
	//	return;

	if (bFlag)
	{
		m_bAreaPointerFlag	= TRUE;
		m_vAreaPointerPos		= vPos;

		//客户端需要增加一个，目标点的效果
		ISkillSpecialEffect* pEffect = theSkillSpecialEffectManager.FindEffect( AREA_POINT_EFFECT_NAME );
		if( pEffect )
		{
			if(pEffect->GetType() == SKILL_SPECIAL_EFFECT_MESH )
			{
				pEffect->SetPos(vPos.x,vPos.y,0.0f);
				pEffect->SetVisible(TRUE);
			}
		}
		else
		{
			pEffect = theSkillSpecialEffectManager.CreateEffect(PLAYEREFFECT_BYMESH);

			pEffect->SetName( AREA_POINT_EFFECT_NAME );
			pEffect->Create( 15103, 0, 100000000 );
			pEffect->SetPos(vPos.x,vPos.y,0.0f);
			//pEffect->SetVisible(TRUE);
			theSkillSpecialEffectManager.AddEffect( pEffect );
		}

	}
	else
	{
		m_bAreaPointerFlag = FALSE;
		ISkillSpecialEffect* pEffect = theSkillSpecialEffectManager.FindEffect( AREA_POINT_EFFECT_NAME );
		if( pEffect )
		{
			pEffect->SetVisible(FALSE);
		}
		//theSkillSpecialEffectManager.DelEffect( MOVE_POINT_EFFECT_NAME );
	}
}

void GameUIManagerInstance::SetAttackPointer(BOOL /*bFlag*/,const VECTOR3D& /*vPos*/)
{
}


void GameUIManagerInstance::ToggleShowName()
{
	if( g_V3DConfig.GetShowInfoLV()+1 > OBJNAME_SHOWALL )
	{
		g_V3DConfig.SetShowInfoLV( 0 );
	}
	else
	{
		g_V3DConfig.SetShowInfoLV( g_V3DConfig.GetShowInfoLV()+1 );
	}
}



///////////////////////////////////////
//VUCtrlGraphicListener实现接口
BOOL	GameUIManagerInstance::CreateFont		(LPCSTR szFontName, UINT nSize )
{
	return theFontTextureManager.Create( szFontName, nSize );
}
void	GameUIManagerInstance::ReleaseFonts	()
{
	theFontTextureManager.Destroy();
}

//默认带有阴影
void GameUIManagerInstance::DrawGameText	(const RECT&	rc
                           ,LPCSTR			pText
									,COLOR			colorARGB
									,int				nFontIndex
									,DWORD			dwFormat
									,int				nCount)
{
	theFontTextureManager.DrawGameText( rc,pText,colorARGB ,nFontIndex ,dwFormat ,nCount);
}

//默认带有阴影
void	GameUIManagerInstance::DrawGameText	(int    nPosX
                           ,int    nPosY
									,LPCSTR pText
									,COLOR  colorARGB
									,int    nFontIndex
									,DWORD  dwFormat
									,int    nCount)
{
	theFontTextureManager.DrawGameText (nPosX,nPosY
											,pText
											,colorARGB
											,nFontIndex
											,dwFormat
											,nCount);
}

//带有描边
void GameUIManagerInstance:: DrawStrokeText	(const RECT&	rc
                           ,LPCSTR			pText
									,COLOR			colorARGB
									,COLOR			colorStroke
									,int				nFontIndex
									,DWORD			dwFormat
									,int				nCount)
{
	theFontTextureManager.DrawStrokeText (rc
                           ,pText
									,colorARGB
									,colorStroke
									,nFontIndex
									,dwFormat
									,nCount);
}

//带有描边
void  GameUIManagerInstance::DrawStrokeText	(INT x
                           ,INT y
                           ,LPCSTR			pText
									,COLOR			colorARGB
									,COLOR			colorStroke
									,int				nFontIndex
									,DWORD			dwFormat
									,int				nCount)
{
	theFontTextureManager.DrawStrokeText (x,y
                           ,pText
									,colorARGB
									,colorStroke
									,nFontIndex
									,dwFormat
									,nCount);
}


TEXTUREID	GameUIManagerInstance::RegisterTexture	(LPCSTR szTextureName
                                 ,BOOL   /*bWarning*/
											,BOOL   /*bUseBlp*/)
{
	return  the3DTextureManager.RegisterTexture(szTextureName, false, false, false );
}

void		GameUIManagerInstance::UnregisterTexture	(TEXTUREID textureID )
{
	the3DTextureManager.UnRegisterTexture( textureID );
}


void		GameUIManagerInstance::Blt					(int    nTextureID
                                 ,LPRECT pDestRect
											,LPRECT pSrcRect
											,int    nSrcW
											,int    nSrcH
											,float  z
											,COLOR  dwColor
											,DWORD  dwFlag)
{
	theV3DGraphicDDraw.Blt	(nTextureID
                        ,pDestRect
								,pSrcRect
								,nSrcW
								,nSrcH
								,z
								,dwColor
								,dwFlag);
}

void		GameUIManagerInstance::DrawRect2D			(const RECT& rect, DWORD dwColor )
{
	theV3DGraphicDDraw.DrawRect2D(rect, dwColor);
}
void		GameUIManagerInstance::FillRect2D			(const RECT& rect, DWORD dwColor )
{
	theV3DGraphicDDraw.FillRect2D(rect, dwColor);
}

void		GameUIManagerInstance::DrawProgressRect	(int x, int y, int size, int nDegree )
{
	theV3DGraphicDDraw.DrawProgressRect(x,  y,  size,  nDegree);
}

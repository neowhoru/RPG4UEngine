/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemHandleSlotContainer.cpp
创建日期：2006年9月26日
最后更新：2006年9月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemHandleSlotContainer.h"
#include "SlotKeyGenerator.h"
#include "SlotUIListener.h"
#include "DummyItemSlot.h"
#include "ItemManager.h"
#include "SlotHandle.h"

BASECONTAINER_IMPL(ItemHandleSlotContainer);
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemHandleSlotContainer::ItemHandleSlotContainer()
{
}
ItemHandleSlotContainer::~ItemHandleSlotContainer()
{
}

VOID ItemHandleSlotContainer::ClearAll()
{
	__CHECK2_PTR(m_ppSlots,);

	SLOTINDEX slotIdxFrom;
	for(SLOTPOS pos=0; pos < GetSlotMaxSize(); pos++)
	{
		DummyItemSlot&	slotDat = (DummyItemSlot&)GetSlot(pos);

		slotIdxFrom	= slotDat.GetFromContainerIdx();

		if(slotIdxFrom != INVALID_SLOTINDEX)
		{
			BaseContainer* pContainer;
			pContainer = theItemManager.GetContainer(slotIdxFrom);
			if(pContainer)
				pContainer->SetSlotState(slotDat.GetFromPosition(), 0);
		}
		//DeleteSlot(pos,NULL);
	}

	_SUPER::ClearAll();
}

//----------------------------------------------------------------------------
BOOL ItemHandleSlotContainer::InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )
{


	if(slotDat.GetSlotType() == ST_ITEMHANDLE)
	{
		_SUPER::InsertSlot( posIndex, slotDat );

	}
	else
	{
		SLOTINDEX		slotIdxFrom;		///INVALID_SLOTINDEX时为临时物品 

		assert(slotDat.GetSlotType() == ST_ITEM);
		SlotHandle	slotDummyItem(SLOTT_ITEMDUMMY);

		if((BOOL)slotDummyItem)
		{
			slotIdxFrom	= slotDat.GetSlotIndex();
			slotDummyItem->Copy(slotDat);
			_SUPER::InsertSlot( posIndex, slotDummyItem );

			if(slotIdxFrom == INVALID_SLOTINDEX)
			{
				SERIALTYPE	sn;
				sn = theSlotKeyGenerator.GetKey();
				DummyItemSlot&	rDummyCur = (DummyItemSlot&)GetSlot(posIndex);
				rDummyCur.SetSerial		(sn);
			}
		}
	}

	DummyItemSlot&	rRealSlot = (DummyItemSlot&)GetSlot(posIndex);
	rRealSlot.RemoveState(SLOT_UISTATE_INVISIBLE);

	if(rRealSlot.GetFromContainerIdx() != INVALID_SLOTINDEX)
	{
		BaseContainer* pContainer;
		pContainer = theItemManager.GetContainer(rRealSlot.GetFromContainerIdx());
		if(pContainer)
			pContainer->SetSlotState(rRealSlot.GetFromPosition(), SLOT_UISTATE_DISABLE);
	}

	if(m_pSlotListener)
	{
		BaseSlot & rRealSlot = GetSlot(posIndex);
		m_pSlotListener->OnSlotAdd(rRealSlot);
	}

	return TRUE;
}


//----------------------------------------------------------------------------
VOID ItemHandleSlotContainer::DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )
{
	ASSERT( FALSE == IsEmpty(posIndex) );
	SLOTINDEX			slotIdxFrom;		///INVALID_SLOTINDEX时为临时物品 

	//BaseSlot & slotDat = GetSlot(posIndex);
	DummyItemSlot&	slotDat = (DummyItemSlot&)GetSlot(posIndex);;

	slotIdxFrom	= slotDat.GetFromContainerIdx();

	if(slotIdxFrom != INVALID_SLOTINDEX)
	{
		BaseContainer* pContainer;
		pContainer = theItemManager.GetContainer(slotIdxFrom);
		if(pContainer)
			pContainer->SetSlotState(slotDat.GetFromPosition(), 0);
	}
	else
	{
		theSlotKeyGenerator.Restore(slotDat.GetSerial());
	}


	//slotDat.SetSerial(0);

	//if(m_pSlotListener)
	//	m_pSlotListener->OnSlotRemove(slotDat);

	BaseContainer::DeleteSlot(posIndex, pSlotOut );

}


//----------------------------------------------------------------------------
VOID ItemHandleSlotContainer::UpdateSlot( SLOTPOS posIndex, BaseSlot & IN slotDat )
{
	BaseSlot& rItem = GetSlot(posIndex);
	if(&rItem != &slotDat)
	{
		if(!IsEmpty(posIndex))
			DeleteSlot(posIndex,NULL);
		InsertSlot(posIndex,slotDat);
	}
	else
	{
		if(m_pSlotListener)
		{
			m_pSlotListener->OnSlotAdd(slotDat);
		}

		//DummyItemSlot itemUpdate;
		//DeleteSlot(posIndex,&itemUpdate);
		//InsertSlot(posIndex,itemUpdate);
	}
	//SLOTINDEX			slotIdxFrom;		///INVALID_SLOTINDEX时为临时物品 
	//BaseContainer* pContainer(NULL);

	//slotIdxFrom	= slotDat.GetFromContainerIdx();
	//if(slotIdxFrom != INVALID_SLOTINDEX)
	//	pContainer = theItemManager.GetContainer(slotIdxFrom);

	//if(pContainer)
	//	pContainer->SetSlotState(slotDat.GetFromPosition(), 0);


	//_SUPER::UpdateSlot(posIndex, slotDat);

	////theItemManager.UpdateQuickInfo();

	//if(m_pSlotListener)
	//	m_pSlotListener->Update(posIndex);
}

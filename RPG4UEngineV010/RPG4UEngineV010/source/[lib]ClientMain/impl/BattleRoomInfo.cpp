/*////////////////////////////////////////////////////////////////////////
文 件 名：BattleRoomInfo.cpp
创建日期：2008年9月9日
最后更新：2008年9月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "BattleRoomInfo.h"
#include "MapInfoParser.h"

//----------------------------------------------------------------------------
BattleRoomInfo::BattleRoomInfo()
:m_dwSerial(0)
,m_MapCode(0)
,m_FieldCode(0)								
{
}


//----------------------------------------------------------------------------
BOOL BattleRoomInfo::Init()
{
	m_dwSerial			= 0;
	m_MapCode			= 0;
	m_FieldCode			= 0;
	m_Type				= ZONETYPE_HUNTING;
	m_Public				= ZONEOPEN_PUBLIC;		
	m_szTitle[0]		= NULL;	
	m_szPassword[0]	= NULL;	

	m_BaseInfo.m_ClassLimit		= ( PLAYERJOB_WARRIOR		 
											|PLAYERJOB_PALADIN			
											|PLAYERJOB_POWWOW			
											|PLAYERJOB_STABBER		 
											|PLAYERJOB_NECROMANCER );

	m_BaseInfo.m_MinLV			= 0;
	m_BaseInfo.m_MaxLV			= 0;


	m_AdditionalInfo.m_Difficulty	= (BYTE)HUNTING_DIFFICULTY_EASY;
	m_AdditionalInfo.m_Bonus		= (BYTE)HUNTINGBONUS_ITEM;

#ifdef _PVP
	m_AdditionalPVPInfo.m_Rule = PVPRULE_10;
	m_AdditionalPVPInfo.m_Mode = PVPMODE_PERSONAL;
#endif 
	return TRUE;
}


//----------------------------------------------------------------------------
BattleRoomInfo &  BattleRoomInfo::operator=(BattleRoomInfo & rEntry )
{
	memcpy(this ,&rEntry,sizeof(BattleRoomInfo));
	return *this;
}

//----------------------------------------------------------------------------
void	BattleRoomInfo::SetAdditionalInfo(sROOMINFO_ADDITIONAL * pAdditionalInfo)
{
	memcpy	(&m_AdditionalInfo
            ,pAdditionalInfo
				,sizeof(m_AdditionalInfo));

	SetFieldIndex(0);
}

#ifdef _PVP
//----------------------------------------------------------------------------
void	BattleRoomInfo::SetAdditionalPVPInfo(sPVPINFO_ADDITIONAL * pAdditionalInfo)
{
	memcpy (&m_AdditionalPVPInfo
          ,pAdditionalInfo
			 ,sizeof(m_AdditionalPVPInfo));

	SetFieldIndex(0);
}
#endif


//----------------------------------------------------------------------------
void	
BattleRoomInfo::SetTotalInfo	(DWORD                  dwSerial
										,eZONE_TYPE             eRoomType
										,eZONE_OPEN_STATE       eRoomPublic
										,LPCSTR                 pszRoomTitle
										,LPCSTR                 pszRoomPW
										,sROOMINFO_BASE *       pRoomInfo
										,sROOMINFO_ADDITIONAL * pAdditionalInfo)
{
	SetSerial	( dwSerial );
	SetType		( eRoomType );
	SetPublic	( eRoomPublic );
	SetTitle		( pszRoomTitle );
	SetPassword	( pszRoomPW );
	SetBaseInfo	( *pRoomInfo );
	SetAdditionalInfo( pAdditionalInfo );
}


//----------------------------------------------------------------------------
void	BattleRoomInfo::SetMinLevel(BYTE byLevel)
{
	m_BaseInfo.m_MinLV = byLevel;
}


//----------------------------------------------------------------------------
void	BattleRoomInfo::SetMaxLevel(BYTE byLevel)
{
	m_BaseInfo.m_MaxLV = byLevel;
}


//----------------------------------------------------------------------------
void	BattleRoomInfo::SetClassLimit(ePLAYER_JOB_TYPE eClassPermition )
{
    m_BaseInfo.m_ClassLimit = (BYTE)eClassPermition;
}


//----------------------------------------------------------------------------
void	BattleRoomInfo::SetFieldIndex(BYTE	byFieldIndex)
{
	ASSERT( byFieldIndex < sMAPINFO_BASE::MAX_FIELD_NUM );

	sMAPINFO_BASE* pMapInfo = (sMAPINFO_BASE*)GetCurMapInfo();
	__VERIFY2_PTR(pMapInfo,"MapInfo invalid!",);

	sFIELDINFO_BASE * pFieldInfo = (sFIELDINFO_BASE*)theMapInfoParser.GetFieldInfo(pMapInfo->arFieldIDs[byFieldIndex]);
	__VERIFY2_PTR(pFieldInfo,"FieldInfo invalid!",);

   SetFieldCode(pMapInfo->arFieldIDs[byFieldIndex]);	
}


//----------------------------------------------------------------------------
void	BattleRoomInfo::SetDifficult(eHUNTING_DIFFICULTY eMonsterDifficult)
{
	m_AdditionalInfo.m_Difficulty = (BYTE)eMonsterDifficult;
}


//----------------------------------------------------------------------------
void BattleRoomInfo::SetBonusType(eHUNTING_BONUS_TYPE eBonusType)
{
	m_AdditionalInfo.m_Bonus = (BYTE)eBonusType;
}

#if defined ( _PVP )

//----------------------------------------------------------------------------
void	BattleRoomInfo::SetPVPRule(ePVPRULE_TYPE eRule)
{
	m_AdditionalPVPInfo.m_Rule = eRule;
}

//----------------------------------------------------------------------------
void	BattleRoomInfo::SetPVPMode(ePVP_MODE_TYPE eMode)
{
	m_AdditionalPVPInfo.m_Mode = (BYTE)eMode;
}

#endif





//----------------------------------------------------------------------------
const BYTE BattleRoomInfo::GetMinLevel()
{
	return m_BaseInfo.m_MinLV;
}


//----------------------------------------------------------------------------
const BYTE BattleRoomInfo::GetMaxLevel()
{
	return m_BaseInfo.m_MaxLV;
}


//----------------------------------------------------------------------------
const ePLAYER_JOB_TYPE  BattleRoomInfo::GetClassLimit()
{
	return (ePLAYER_JOB_TYPE)m_BaseInfo.m_ClassLimit;
}

//----------------------------------------------------------------------------
const BYTE	 BattleRoomInfo::GetFieldIndex()
{
	sMAPINFO_BASE * pMapInfo = theMapInfoParser.GetMapInfo( GetMapCode() ); 
	if ( pMapInfo )
		return pMapInfo->byMapSubIndex;
	return 0;
}


//----------------------------------------------------------------------------
const eHUNTING_DIFFICULTY	BattleRoomInfo::GetDifficult()
{
	return (eHUNTING_DIFFICULTY)m_AdditionalInfo.m_Difficulty;
}


//----------------------------------------------------------------------------
const eHUNTING_BONUS_TYPE	BattleRoomInfo::GetBonusType()
{
	return (eHUNTING_BONUS_TYPE)m_AdditionalInfo.m_Bonus;
}

#ifdef _PVP

//----------------------------------------------------------------------------
const ePVPRULE_TYPE BattleRoomInfo::GetPVPRule()
{
	return (ePVPRULE_TYPE)m_AdditionalPVPInfo.m_Rule;
}


//----------------------------------------------------------------------------
const ePVP_MODE_TYPE BattleRoomInfo::GetPVPMode()
{
	return (ePVP_MODE_TYPE)m_AdditionalPVPInfo.m_Mode;
}

#endif



//----------------------------------------------------------------------------
const sMAPINFO_BASE * BattleRoomInfo::GetCurMapInfo()
{
	sMAPINFO_BASE * pMapInfo = theMapInfoParser.GetMapInfo(m_MapCode);
	return pMapInfo;
}


//----------------------------------------------------------------------------
const sFIELDINFO_BASE *	 BattleRoomInfo::GetCurFieldInfo()
{
	sFIELDINFO_BASE * pFieldInfo = theMapInfoParser.GetFieldInfo(m_FieldCode);
	return pFieldInfo;
}


//----------------------------------------------------------------------------
const MapGroup * BattleRoomInfo::GetCurGroup()
{
	const sMAPINFO_BASE * pMapInfo = GetCurMapInfo();
	if ( !pMapInfo )
	{
		ASSERT(pMapInfo);
		return NULL;
	}

	const MapGroup * pMapGroup;
	
	pMapGroup = theMapInfoParser.GetMapGroup( pMapInfo->MapGroupIndex );
	if ( !pMapGroup )
	{
		ASSERT(pMapInfo);
		return NULL;
	}

	return pMapGroup;
}

//----------------------------------------------------------------------------
const BYTE BattleRoomInfo::GetCurLimitMinUser()
{
	if ( GetCurMapInfo() )
	{
		return GetCurMapInfo()->byMinUserNum;
	}
	return 1;
}

//----------------------------------------------------------------------------
const BYTE BattleRoomInfo::GetCurLimitMaxUser()
{
	if ( GetCurMapInfo() )
		return GetCurMapInfo()->byMaxUserNum;
	return 10;
}

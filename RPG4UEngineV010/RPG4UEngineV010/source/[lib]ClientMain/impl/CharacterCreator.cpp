/*////////////////////////////////////////////////////////////////////////
文 件 名：CharacterCreator.cpp
创建日期：2006年4月24日
最后更新：2006年4月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "CharacterCreator.h"
#include "CharacterScene.h"
#include "MathInc.h"
#include "GameAvatarSetting.h"
#include "Player.h"
#include "CharacterScene.h"
#include "ObjectManager.h"
#include "GameUIManager.h"
#include "Map.h"
#include "V3DCamera.h"
#include "SkeletonDefine.h"
#include "TextFilterManager.h"
#include "ApplicationSetting.h"
#include "PacketInclude.h"

using namespace object;
using namespace math;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(CharacterCreator, ()  , gamemain::eInstPrioGameFunc);


const float ROTATE_STEP			= cPI/10;
const DWORD MAX_HAIRCOLOR		=	5;
const DWORD DEFAULT_HAIRCOLOR	= 1;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CharacterCreator::CharacterCreator()
{
	m_bNearCamera		= FALSE;
	m_fRot				= 0.94247826f;

	m_pPlayerCreating	= NULL;
	m_nCharSlot			= -1;
	m_fCameraDistBak	= 0;

	m_nHeight		= 10;
	m_nFace			= 0;
	m_nHair			= 0;
	m_nHairColor	= 1;
	m_nCloth			= 0;
	m_nSex			= 0;
	m_szName[0]		= 0;
	m_CharType		= PLAYERTYPE_WARRIOR;

}

CharacterCreator::~CharacterCreator()
{
}

BOOL CharacterCreator::Init()
{
	return TRUE;
}

void CharacterCreator::Release()
{

}

BOOL CharacterCreator::RequestWorkingData()
{
	return TRUE;
}

void CharacterCreator::ReleaseWorkingData()
{
	if(m_pPlayerCreating)
	{
		theObjectManager.Delete(m_pPlayerCreating->GetObjectKey());
		m_pPlayerCreating = NULL;
	}
}

BOOL CharacterCreator::FrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}

BOOL CharacterCreator::Open(INT nSlot)
{
	theGameUIManager.UITriggerFunc((gameui::EGameUIType)theCharacterScene.GetUIListener(),
								gameui::eOpenCharacterCreator);
	m_nCharSlot = nSlot;

	__CHECK(MakePlayerDummy() );

	__CHECK(Update(TRUE) );

	Vector3D	vLookAt;

	vLookAt = m_pPlayerCreating->GetPosition() + Vector3D(0,0,m_pPlayerCreating->GetMeshHeight() ) ;

	m_pPlayerCreating->SetVisible					(TRUE);
	m_pPlayerCreating->SetAlpha					(1.f);

	theCharacterScene	.SetLockPlayer				(TRUE);
	theClientSetting	.SetLockCameraDistance	(FALSE);
	theClientSetting	.SetCameraTo				(vLookAt);

	//theMapHandleManager.GetActiveMapHandle()->BuildSceneFrustum();

	m_fCameraDistBak = theCamera.GetDistance();

	return TRUE;
}

void CharacterCreator::Close(BOOL bSucc,BOOL bNotifyUI)
{
	theCharacterScene.SetLockCameraDistance(TRUE);

	if(bNotifyUI)
	{
		theGameUIManager.UITriggerFunc((gameui::EGameUIType)theCharacterScene.GetUIListener(),
								gameui::eCloseCharacterCreator);
	}

	if(m_pPlayerCreating == NULL)
		return;

	//if(m_fCameraDistBak > 0)
	//	theCamera.SetDistanceMin(m_fCameraDistBak);


	if(bSucc)
		m_pPlayerCreating->SetAlpha(0.f);
	else
		m_pPlayerCreating->SetVisible(FALSE);

	theCharacterScene.SetLockPlayer(FALSE);
	//theCharacterScene.ShowCharacter(TRUE,-1);

	/////////////////////////////
	theMapHandleManager.ResetCamera();
	if(m_fCameraDistBak > 0)
	{
		theCamera.SetDistance(m_fCameraDistBak);
		m_fCameraDistBak = 0;
	}

	m_nCharSlot = -1;

}

void CharacterCreator::Clear()
{
	m_nFace			= 0;
	m_nHair			= 0;
	m_nHairColor	= 1;
	m_nCloth			= 0;
}


BOOL CharacterCreator::CheckCharacterName(LPCSTR szName)
{
	if(theTextFilterManager.ExistFilterWord(szName))
	{
		return FALSE;
	}
assert(0 && " CheckCharacterName ");
#pragma message(__FILE__  "(51) CheckCharacterName  " )

	////接下来需要把这个szName传给服务器 数据库搜索名字是否重复

	return TRUE;
}

BOOL CharacterCreator::SendCreateCharacter()
{
	MSG_CG_CHARINFO_CREATE_SYN	charCreatePacket;

	charCreatePacket.m_byCategory = CG_CHARINFO;
	charCreatePacket.m_byProtocol = CG_CHARINFO_CREATE_SYN;
	charCreatePacket.m_byClass			= (BYTE)m_CharType;
	charCreatePacket.m_byFace			= (BYTE)m_nFace;
	charCreatePacket.m_byHair			= (BYTE)m_nHair;
	charCreatePacket.m_byHairColor	= (BYTE)m_nHairColor;
	charCreatePacket.m_byHeight		= (BYTE)m_nHeight;
	charCreatePacket.m_bySex			= (BYTE)m_nSex;	

	lstrcpyn(charCreatePacket.m_szCharName,m_szName, sizeof(charCreatePacket.m_szCharName));
	//msg.create.ucModelIndex			= (BYTE)m_nCloth;//身体模型的索引


	if(!theNetworkLayer.SendPacket(CK_GAMESERVER, &charCreatePacket, sizeof(charCreatePacket) ) )
	{
		assert(!"net failed");
		return FALSE;
	}

	return TRUE;
}

BOOL CharacterCreator::MakePlayerDummy()
{

	DWORD dwID;		
	Vector3D pos;
	Vector3D vLookat;
	//Player*	pPlayer;

	if(m_pPlayerCreating == NULL)
	{
		dwID					= theObjectManager.GenerateKeyAtSingleMode(PLAYER_OBJECT);
		m_pPlayerCreating	= (Player*)theObjectManager.Add(dwID, PLAYER_OBJECT, dwID);
	}

	__CHECK_PTR(m_pPlayerCreating);

	pos		= theCharacterScene.GetPosInfoAt(m_nCharSlot).vPosition;
	vLookat	= theCharacterScene.GetPosInfoAt(m_nCharSlot).vLookAt;

	m_pPlayerCreating->SetPosition(pos);
	m_pPlayerCreating->SetDirection(vLookat, &pos, TRUE);	 
	m_pPlayerCreating->SetShowName(TRUE);

	m_pPlayerCreating->GetVObject()->ModShowFlag(VOBJ_SHOW_NOCAMERACHECK,0);

	return  m_pPlayerCreating != NULL;
}



BOOL CharacterCreator::Update(BOOL bUseDefault)
{
	__CHECK_PTR(m_pPlayerCreating);

	//Select 
	//	女刺客，男战士，男方士，女猎人，女巫师
	int	body_id = 0,
		hair_id = 0, 
		face_id = 0;

	m_pSetting			= &GetGameAvatarSetting();
	m_pSex				= m_pSetting->GetSettingSex();
	m_pMaleDressId		= m_pSetting->GetSettingMaleDressId();
	m_pFemaleDressId	= m_pSetting->GetSettingFemaleDressId();
	m_pMaleFaceId		= m_pSetting->GetSettingMaleFaceId();
	m_pFemaleFaceId	= m_pSetting->GetSettingFemaleFaceId();
	m_pMaleHairId		= m_pSetting->GetSettingMaleHairId();
	m_pFemaleHairId	= m_pSetting->GetSettingFemaleHairId();

	if( !bUseDefault )
	{
		// 男
		if ( m_pSex->m_Datas[m_nSex].m_ResID == SEX_MALE )
		{
			body_id = m_pMaleDressId->	GetResID(m_nCloth);
			hair_id = m_pMaleHairId->	GetResID(m_nHair);
			face_id = m_pMaleFaceId->	GetResID(m_nFace);
		}
		else
		{
			body_id = m_pFemaleDressId->	GetResID(m_nCloth);
			hair_id = m_pFemaleHairId->	GetResID(m_nHair);
			face_id = m_pFemaleFaceId->	GetResID(m_nFace);
		}
	}
	else
	{
		// 男
		if ( m_pSex->m_Datas[m_nSex].m_ResID == SEX_MALE )
		{
			body_id = m_pMaleDressId->GetResID(0);
			hair_id = m_pMaleHairId->GetResID(0);
			face_id = m_pMaleFaceId->GetResID(0);
		}
		// 女
		else
		{
			body_id = m_pFemaleDressId->GetResID(0);
			hair_id = m_pFemaleHairId->GetResID(0);
			face_id = m_pFemaleFaceId->GetResID(0);
		}
	}

	#pragma message(__FILE__  "(223) CharacterCreator增加Sex、HairColor处理...  " )

	sCHARACTER_CLIENTPART	clientPart={0};
	BYTE							nItemCount(0);

	clientPart.m_byClass			= (BYTE)m_CharType;
	clientPart.m_byFace			= (BYTE)m_nFace;
	clientPart.m_byHair			= (BYTE)m_nHair;
	clientPart.m_byHairColor	= (BYTE)m_nHairColor;
	clientPart.m_bySex			= (BYTE)m_nSex;
	lstrcpyn(clientPart.m_szCharName,	m_szName,MAX_NAME_LENGTH);

	//m_pPlayerCreating->SetSex();
	sEQUIP_ITEM_INFO&	arEquips		= clientPart.m_EquipItemInfo;
	sITEM_SLOTEX*		parSlots		= arEquips.m_Slot;
	sITEM_SLOTEX&		armourSlot	= parSlots[nItemCount++];
	arEquips.m_Count					= nItemCount;

	//parSlots[skeleton::eHairPart].m_Stream.ItemPart.wCode		= hair_id;
	//parSlots[skeleton::eFacePart].m_Stream.ItemPart.wCode		= face_id;
	armourSlot.m_Stream.ItemPart.wCode	= (WORD)body_id;
	armourSlot.m_ItemPos						= EQUIPPOS_ARMOR;

	m_pPlayerCreating->SetClientPartInfo(&clientPart);
	m_pPlayerCreating->GetCharInfo()->m_bySlot = (BYTE)m_nCharSlot;
	m_pPlayerCreating->Process(0);

	m_pPlayerCreating->SetScale(theApplicationSetting.m_UICreatorScale + 0.4f);

	return TRUE;
}

BOOL CharacterCreator::UpdateName		(LPCTSTR szName)
{
	m_szName = szName;
	if(m_pPlayerCreating)
	{
		m_pPlayerCreating->SetName(szName);
	}
	return TRUE;
}

void CharacterCreator::ChangeFace(BOOL bAdd)
{
	int nCount = m_pMaleFaceId->m_Datas.size();
	if( m_nSex == 1 )
		nCount = m_pFemaleFaceId->m_Datas.size();

	if(bAdd == FALSE)
	{
		m_nFace--;
		if ( m_nFace >= nCount || m_nFace < 0 )
			m_nFace = nCount-1;
	}
	else
	{
		//if( m_nSex == 1 )
		//	nCount = m_pFemaleFaceId->nCount;
		m_nFace++;
		if ( m_nFace >= nCount || m_nFace < 0 )
			m_nFace = 0;
	}
}

void CharacterCreator::ChangeHair(BOOL bAdd)
{
	int nCount = m_pMaleHairId->m_Datas.size();
	if( m_nSex == 1 )
		nCount = m_pFemaleHairId->m_Datas.size();

	if(bAdd)
	{
		m_nHair++;
		if ( m_nHair >= nCount || m_nHair < 0 )
		{
			m_nHair = 0;
		}
	}
	else
	{
		m_nHair--;
		if ( m_nHair >= nCount || m_nHair < 0 )	
		{
			m_nHair = nCount-1;
		}
	}
}


void CharacterCreator::ChangeHairColor(BOOL bAdd)
{
	if(bAdd)
	{
		if( m_nHairColor < MAX_HAIRCOLOR && m_nHairColor >= DEFAULT_HAIRCOLOR)
		{
			++m_nHairColor;
		}
		else if( m_nHairColor == MAX_HAIRCOLOR)
		{
			m_nHairColor = DEFAULT_HAIRCOLOR;
		}
	}
	else
	{
		if( m_nHairColor <= MAX_HAIRCOLOR && m_nHairColor > DEFAULT_HAIRCOLOR)
		{
			--m_nHairColor;
		}
		else if( m_nHairColor == DEFAULT_HAIRCOLOR )
		{
			m_nHairColor = MAX_HAIRCOLOR;
		}
	}
}


void CharacterCreator::RotatePlayer(INT nStep)
{
	m_fRot = m_pPlayerCreating->GetAngle();
	m_fRot = DegreeInPI_TWO(m_fRot + nStep * ROTATE_STEP);
	m_pPlayerCreating->SetAngle(m_fRot);
	//if(nStep < 0)
	//{
	//	if( m_fRot > 0 )
	//	{
	//		m_fRot += nStep * ROTATE_STEP;
	//	}
	//	else
	//	{
	//		m_fRot = cPI_TWO;
	//	}
	//}//if(nStep < 0)
	//else if(nStep > 0)
	//{
	//	if( m_fRot < cPI_TWO )
	//	{
	//		m_fRot += nStep * ROTATE_STEP;
	//	}
	//	else
	//	{
	//		m_fRot = 0;
	//	}
	//}//else if(nStep > 0)
}

void CharacterCreator::ToggleCamera()
{

	theCamera.MoveFrontBack(100);

	//V3DCamera* pCamera = theMap.GetCamera();
	//if(!pCamera)
	//	return;

	//Vector3D pos;
	//
	//if(!m_bNearCamera)
	//{
	//	pos	=	m_pPlayerCreating->GetPosition();
	//	pos.z += m_pPlayerCreating->GetMeshHeight();
	//	theCamera.SetCamera(pos);
	//}
	//else
	//{
	//	theMap.RebuildCamera();
	//}


}

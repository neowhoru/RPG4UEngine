/*////////////////////////////////////////////////////////////////////////
文 件 名：ObjectSystemInstance.cpp
创建日期：2008年9月4日
最后更新：2008年9月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ObjectSystemInstance.h"
#include "ObjectManager.h"
#include "ObjectStateFactoryManager.h"
#include "SkillEffectManager.h"
#include "SlotKeyGenerator.h"
#include "VObjectManager.h"
#include "ObjectUtil.h"

//#include "VEffect.h"
//#include "VBuilding.h"
//#include "VMoney.h"
#include "Hero.h"
#include "Item.h"
#include "MapObject.h"
#include "Monster.h"
#include "MapNPC.h"
#include "Clone.h"
#include "ObjectManagerImpl.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace object;
GLOBALINST_SINGLETON_IMPL(ObjectSystemInstance, ()  , gamemain::eInstPrioGameFunc);


#ifdef _DEBUG 
#define STATE_FACTORY_NUM		25
#define NPCSTATE_FACTORY_NUM	6
#define NONPC_FACTORY_NUM		6
#define HERO_FACTORY_NUM		2
#define SKILL_FACTORY_NUM		2
#else 
#define STATE_FACTORY_NUM			64
#define NPCSTATE_FACTORY_NUM		32
#define NONPC_FACTORY_NUM			16
#define HERO_FACTORY_NUM			4
#define SKILL_FACTORY_NUM			10
#endif



////////////////////////////////////
#define INSTALL_IMPL(Name)\
						static Name##Impl	s_##Name##Impl(NULL);\
						the##Name.InstallImpl(&s_##Name##Impl);

namespace object
{

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ObjectSystemInstance::ObjectSystemInstance()
{
}
ObjectSystemInstance::~ObjectSystemInstance()
{
	//Release();
}


BOOL ObjectSystemInstance::Init()
{
	INSTALL_IMPL(ObjectManager);

	////////////////////////////////////////////
	_RegObjects();

	theObjectFactoryManager.InitAllFactories();


	_RegisterSkillStates();


	theObjectStateFactoryManager.InitAllFactories();

	__INIT(theSlotKeyGenerator.Init());

	__INIT(theSkillEffectManager.Init());

	//m_pKeyGenerator		= new util::KeyGenerator( PLAYER_OBJECT_KEY,			 MAX_OBJECT_KEY);

	//RequestWorkingData();
	return TRUE;
}


void ObjectSystemInstance::Release()
{
	__END(theSkillEffectManager.Release());

	ReleaseWorkingData();

	theObjectStateFactoryManager.	ReleaseAllFactories();
	theObjectStateFactoryManager.	RemoveAll();

	theObjectFactoryManager.	ReleaseAllFactories();
	theObjectFactoryManager.	RemoveAll();

	__END(theSlotKeyGenerator.Release());

}

BOOL ObjectSystemInstance::RequestWorkingData()
{
	theObjectManager.Create(&theVObjectManager);
	theSkillEffectManager.RequestWorkingData();
	return TRUE;
}

void ObjectSystemInstance::ReleaseWorkingData()
{
	theSkillEffectManager.ReleaseWorkingData();
	theObjectManager.Destroy();
}


BOOL ObjectSystemInstance::FrameMove(DWORD dwTick)
{
	//assert(0 && " ObjectSystemInstance..FrameMove ");
	//#pragma message(__FILE__  "(108) ObjectSystemInstance..FrameMove  " )

	theSkillEffectManager.FrameMove(dwTick);
	return TRUE;
}

BOOL ObjectSystemInstance::Render(DWORD /*dwTick*/)
{
	//assert(0 && " ObjectSystemInstance..Render ");
	//#pragma message(__FILE__  "(116) ObjectSystemInstance..Render  " )

	return TRUE;
}

BOOL ObjectSystemInstance::_RegisterSkillStates()
{
	return TRUE;
}



/*////////////////////////////////////////////////////////////////////////
	//对象工厂注册
/*////////////////////////////////////////////////////////////////////////
#define REG_FACTORY(NAME,TYPE,Size)\
	static NAME##ObjectFactory	s##NAME##ObjectFactory(TYPE,Size);\
		theObjectFactoryManager.RegisterFactory(&s##NAME##ObjectFactory);

//////////////////////////////////////////////
BOOL ObjectSystemInstance::_RegObjects()
{
	#pragma message(__FILE__  "(68) ObjectSystem..Init 目前还没开放 REG_FACTORY[ITEM_OBJECT MAP_OBJECT]   " )

	//////////////////////////////////////////////
	//对象工厂注册
#ifdef _DEBUG
	REG_FACTORY(Player,		PLAYER_OBJECT,	5);
	REG_FACTORY(Monster,		MONSTER_OBJECT,20);
	REG_FACTORY(MapNPC,		MAPNPC_OBJECT,	20);
	//REG_FACTORY(MapNPC,	NPC_OBJECT	,	20);
	REG_FACTORY(Clone,		CLONE_OBJECT,	10);
	REG_FACTORY(Item,			ITEM_OBJECT,	10);
#else
	REG_FACTORY(Player,		PLAYER_OBJECT,	30);
	REG_FACTORY(Monster,		MONSTER_OBJECT,50);
	//REG_FACTORY(MapNPC,	NPC_OBJECT	,	30);
	REG_FACTORY(MapNPC,		MAPNPC_OBJECT,	30);
	REG_FACTORY(Clone,		CLONE_OBJECT,	30);
	REG_FACTORY(Item,			ITEM_OBJECT,	50);
#endif

	//REG_FACTORY(GameMapObject,	MAP_OBJECT,		40);

	return TRUE;
}



};//namespace vobject


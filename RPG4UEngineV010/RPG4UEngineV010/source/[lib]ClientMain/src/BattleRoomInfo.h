/*////////////////////////////////////////////////////////////////////////
文 件 名：BattleRoomInfo.h
最后更新：2008年9月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BATTLEROOMINFO_H__
#define __BATTLEROOMINFO_H__
#pragma once



#include <CommonDefine.h>
#include <atltime.h>


class MapGroup;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API BattleRoomInfo
{
public:
	BattleRoomInfo();
	~BattleRoomInfo() {}

public:
	BattleRoomInfo & operator=(BattleRoomInfo & rEntry);

public:
	BOOL Init();

public:
	void	SetAdditionalInfo		(sROOMINFO_ADDITIONAL * pAdditionalInfo);

	void	SetTotalInfo	(DWORD                  dwSerial
                        ,eZONE_TYPE             eRoomType
								,eZONE_OPEN_STATE       eRoomPublic
								,LPCSTR                 pszRoomTitle
								,LPCSTR                 pszRoomPW
								,sROOMINFO_BASE *       pRoomInfo
								,sROOMINFO_ADDITIONAL * pAdditionalInfo);

	void	SetMinLevel	(BYTE byLevel);
	void	SetMaxLevel	(BYTE byLevel);

	void	SetClassLimit	( ePLAYER_JOB_TYPE eClassPermition );
	void	SetFieldIndex	(BYTE	byFieldIndex);
	void	SetDifficult	(eHUNTING_DIFFICULTY eMonsterDifficult);
	void	SetBonusType	(eHUNTING_BONUS_TYPE	eBonusType);

#ifdef  _PVP
	void	SetAdditionalPVPInfo	(sPVPINFO_ADDITIONAL * pAdditionalInfo);
	void	SetPVPRule				(ePVPRULE_TYPE eRule);
	void	SetPVPMode				(ePVP_MODE_TYPE eMode);
#endif
	

public:
	///////////////////////////////////////////////
	const BYTE							GetMinLevel();
	const BYTE							GetMaxLevel();
	const ePLAYER_JOB_TYPE			GetClassLimit();
	const BYTE							GetFieldIndex();
	const eHUNTING_DIFFICULTY		GetDifficult();
	const eHUNTING_BONUS_TYPE		GetBonusType();
#ifdef _PVP
	const ePVPRULE_TYPE				GetPVPRule();
	const ePVP_MODE_TYPE				GetPVPMode();
#endif


	const sMAPINFO_BASE *			GetCurMapInfo();
	const sFIELDINFO_BASE *			GetCurFieldInfo();
	const MapGroup *					GetCurGroup();
	const BYTE							GetCurLimitMinUser();
	const BYTE							GetCurLimitMaxUser();


public:
	VG_DWORD_PROPERTY			(Serial);
	VG_TYPE_PROPERTY			(MapCode,	CODETYPE);
	VG_TYPE_PROPERTY			(FieldCode, CODETYPE);
	VG_TYPE_PROPERTY			(Type,		eZONE_TYPE);
	VG_TYPE_PROPERTY			(Public,		eZONE_OPEN_STATE);

	VG_CSTR_PROPERTY			(Name,		MAX_MAPNAME_LENGTH);
	VG_CSTR_PROPERTY			(Title,		MAX_ROOMTITLE_LENGTH);
	VG_CSTR_PROPERTY			(Password,	MAX_ROOMPASSWORD_LENGTH);

	VG_TYPE_PROPERTY			(BaseInfo,			sROOMINFO_BASE);
	VG_TYPE_GET_PROPERTY		(AdditionalInfo,	sROOMINFO_ADDITIONAL);
	VG_INT_PROPERTY			(MinPlayer);
	VG_INT_PROPERTY			(MaxPlayer);

#ifdef _PVP
	VG_TYPE_GET_PROPERTY		(AdditionalPVPInfo,	sPVPINFO_ADDITIONAL);
#endif
};



#endif //__BATTLEROOMINFO_H__
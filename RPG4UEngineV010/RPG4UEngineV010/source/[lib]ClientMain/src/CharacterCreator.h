/*////////////////////////////////////////////////////////////////////////
文 件 名：CharacterCreator.h
创建日期：2006年4月24日
最后更新：2006年4月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTERSCENECREATOR_H__
#define __CHARACTERSCENECREATOR_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "ISystem.h"
#include "GameAvatarSetting.h"
#include "CharacterSceneDefine.h"

namespace object
{
	class Player;
};
using namespace object;
using namespace scene;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API CharacterCreator : public ISystem
{
public:
	CharacterCreator();
	~CharacterCreator();

public:
	virtual BOOL Init();
	virtual void Release();
	virtual BOOL RequestWorkingData();/*一般由外部类请求调用*/
	virtual void ReleaseWorkingData();/*一般由外部类请求调用*/

	virtual BOOL FrameMove(DWORD dwTick);

	BOOL Open(INT nSlot);
	void Close(BOOL bSucc,BOOL bNotifyUI=TRUE);

	VOID Clear();
	BOOL CheckCharacterName(LPCSTR szName);
	BOOL SendCreateCharacter();
	BOOL IsWorking()	{return m_nCharSlot != -1;}

	BOOL MakePlayerDummy();



	BOOL Update				(BOOL bDefault=FALSE);
	BOOL UpdateName		(LPCTSTR szName);
	void ChangeFace		(BOOL bAdd);
	void ChangeHair		(BOOL bAdd);
	void ChangeHairColor	(BOOL bAdd);
	void RotatePlayer		(INT nStep=1);


	void ToggleCamera();
protected:
	VG_INT_PROPERTY	(CharSlot);	//当前占用的人物位置
	VG_PTR_PROPERTY	(PlayerCreating, Player);	//创建中人物控制

	VG_FLOAT_PROPERTY		(Rot);
	VG_BOOL_GET_PROPERTY	(NearCamera);

	VG_INT_PROPERTY	(Height);
	VG_INT_PROPERTY	(Face);
	VG_INT_PROPERTY	(Hair);
	VG_INT_PROPERTY	(Cloth);
	VG_INT_PROPERTY	(Sex);
	VG_TYPE_PROPERTY	(CharType , ePLAYER_TYPE);
	VG_INT_PROPERTY	(HairColor);


	StringHandle		m_szName;
	GameAvatarSetting*	m_pSetting;
	sAvatarSetting*		m_pSex;
	sAvatarSetting*		m_pMaleDressId;
	sAvatarSetting*		m_pFemaleDressId;
	sAvatarSetting*		m_pMaleFaceId;
	sAvatarSetting*		m_pFemaleFaceId;
	sAvatarSetting*		m_pMaleHairId;
	sAvatarSetting*		m_pFemaleHairId;

	Vector3					m_camPosBak;
	float						m_fCameraDistBak;
	//CPlayer_SEquipment	m_equip[ ggdat::VISUAL_MAX ];

};
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(CharacterCreator , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API CharacterCreator& singleton::GetCharacterCreator();
#define theCharacterCreator  singleton::GetCharacterCreator()


#endif //__CHARACTERSCENECREATOR_H__

#include "SkillPaneContainer.h"

class SlotUIListener;
class DDrawRender;

	// server client shared container
class _CLIENTMAIN_API UISkillPassive :    public SkillPaneContainer 
{

public:
    UISkillPassive();
    virtual ~UISkillPassive();

    //--------------------------------------------------------------------------
    // init/release
    //
public:
    virtual BOOL            Init(UINT nSlotNum);
    virtual void            Release();

protected:
    void                    InitControls();

    //--------------------------------------------------------------------------
    // helper/comms.
    //
public:
    //SlotUIListener *          GetItemUnitRender() const;
    //virtual BaseContainer *   GetSlotContainer();

    SHORT                   GetRequiredSkillPoint();
    void                    SetRequiredSkillPoint(SHORT stat);
    SHORT                   GetRequiredSkillPoint2();
    void                    SetRequiredSkillPoint2(SHORT stat);
    LEVELTYPE               GetSkillLevel();
    void                    SetSkillLevel(LEVELTYPE level);
    DWORD                   GetRemainPoint();
    void                    SetRemainPoint(DWORD stat);

    //--------------------------------------------------------------------------
    // functions
    //
public:
    // item unit renderer
    void                    InitSlotListener();
    //void                    //ResetItemRenderer();
    void                    FlushSlotListener();

    void                    Clear();

    //--------------------------------------------------------------------------
    // dialog functions
    //
public:
    virtual BOOL			InsertSlot( SLOTPOS posIndex, BaseSlot & IN slotDat );				//< ���Կ� ��ü �߰�
    virtual VOID            DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut );



    //--------------------------------------------------------------------------
    // member vars
    //
private:
    //SlotUIListener *		m_pSlotUIListener;


    SHORT                   m_sRequiredSkillStat1;
    SHORT                   m_sRequiredSkillStat2;
    LEVELTYPE               m_lvStyleLevel;
    DWORD                   m_dwRemainPoint;

};



//------------------------------------------------------------------------------
/**
*/
inline
SHORT
UISkillPassive::GetRequiredSkillPoint()
{
    return m_sRequiredSkillStat1;
}

//------------------------------------------------------------------------------
/**
*/
inline
void
UISkillPassive::SetRequiredSkillPoint(SHORT stat)
{
    m_sRequiredSkillStat1 = stat;
}

//------------------------------------------------------------------------------
/**
*/
inline
SHORT
UISkillPassive::GetRequiredSkillPoint2()
{
    return m_sRequiredSkillStat2;
}

//------------------------------------------------------------------------------
/**
*/
inline
void
UISkillPassive::SetRequiredSkillPoint2(SHORT stat)
{
    m_sRequiredSkillStat2 = stat;
}

//------------------------------------------------------------------------------
/**
*/
inline
DWORD
UISkillPassive::GetRemainPoint()
{
    return m_dwRemainPoint;
}

//------------------------------------------------------------------------------
/**
*/
inline
void
UISkillPassive::SetRemainPoint(DWORD stat)
{
    m_dwRemainPoint = stat;
}

//------------------------------------------------------------------------------
/**
*/
inline
LEVELTYPE
UISkillPassive::GetSkillLevel()
{
    return m_lvStyleLevel;
}

//------------------------------------------------------------------------------
/**
*/
inline
void
UISkillPassive::SetSkillLevel(LEVELTYPE level)
{
    m_lvStyleLevel = level;
}



/*////////////////////////////////////////////////////////////////////////
文 件 名：	BaseLibExport.h
创建日期：	2007.12.27
最后更新：	2007.12.27
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#pragma once

// disable: "<type> needs to have dll-interface to be used by clients'
// Happens on STL member variables which are not public therefore is ok
#pragma warning (disable : 4251)

// disable: "non dll-interface class used as base for dll-interface class"
// Happens when deriving from Singleton because bug in compiler ignores
// template export
#pragma warning (disable : 4275)


	//#define _CLIENTMAINSTATIC_API 

#if defined(_CLIENTMAINSTATIC)

	///////////////////////////////////
	//动态库输出
	#ifdef _USRDLL
		#define _CLIENTMAINSTATIC_API __declspec( dllexport )
		#pragma message("   _CLIENTMAINSTATIC_API __declspec( dllexport )")

	///////////////////////////////////
	//非动态输出
	#else
		#define _CLIENTMAINSTATIC_API
		#define _CLIENTMAINSTATIC_STATIC
		#pragma message("   _CLIENTMAINSTATIC_API STATIC")
	#endif

//#elif defined(_CLIENTUI_STATIC)
//	#define _CLIENTMAINSTATIC_API 
//	#pragma message("   USING CLIENTMAIN... ")

#else

#define USE_CLIENTMAINSTATIC_STATIC
	///////////////////////////////////
	//静态库
	#ifdef USE_CLIENTMAINSTATIC_STATIC
		#pragma message("   USING _CLIENTMAINSTATIC_API STATIC")
		#define _CLIENTMAINSTATIC_API
		#ifdef _DEBUG
			#pragma comment(lib,"ClientMainStaticD.lib")
		#else
			#pragma comment(lib,"ClientMainStatic.lib")
		#endif

		///////////////////////////////////
		//动态库
	#else
		#pragma message("   _CLIENTMAINSTATIC_API __declspec( dllimport )")
		#define _CLIENTMAINSTATIC_API __declspec( dllimport )
		#ifdef _DEBUG
			#pragma comment(lib,"ClientMainD.lib")
		#else
			#pragma comment(lib,"ClientMain.lib")
		#endif
	#endif

#endif




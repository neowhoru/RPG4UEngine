/*////////////////////////////////////////////////////////////////////////
文 件 名：NPC.h
创建日期：2008年3月28日
最后更新：2008年3月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NPC_H__
#define __NPC_H__
#pragma once


#include "CommonDefine.h"
#include "ObjectFactoryManager.h"
#include "Character.h"

namespace object
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API NPC : public Character
{
public:
	NPC( void );
	virtual ~NPC( void );

public:
	virtual BOOL		Create	( DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void		Destroy	( void );
	virtual BOOL		Process	(DWORD dwTick);

public:
	virtual float		GetMeshHeight		(float fExtra=0.f);
	virtual DWORD		GetCharSoundCode	();



public:
	///////////////////////////////////////////////////
	//音效处理
	virtual void		ProcessVoice		(DWORD dwTick);

	virtual eWEAPONSOUNDKIND		GetWeaponSoundKind	();
	virtual eARMOUR_TEX_TYPE		GetArmourTexture		();


public:
	///////////////////////////////////////////////////
	//事件处理
	virtual void      OnDamage	(DWORD                dwAddtionalEffect
                              ,BOOL                 bRight
										,eWEAPONSOUNDKIND     eSoundKind
										,eARMOUR_TEX_TYPE		 eArmourTex
										,DWORD                dwSkillCode);


	virtual void     OnUseSkill();
	virtual void     OnDead();
	virtual void     OnStartRun();

public:
	///////////////////////////////////////////////////
	//基本属性处理
   virtual void				SetNpcFunc(eNPC_FUNC_TYPE funcType);
   virtual eNPC_FUNC_TYPE	GetNpcFunc();

   virtual void		SetName(const char * pszName);

	virtual void      ChangeHPRatio		(float fRatio);
	virtual void		SetExp				(DWORD dwExp);
	virtual void		SetLevel				(LEVELTYPE LV);
	virtual void		SetMaxHP				( DWORD iHP );
	virtual void		SetMaxMP				( DWORD iMP );

	virtual void      SetAttackSpeedRatio	(int iSpeed);
	virtual void      SetMoveSpeedRatio		(int iSpeed);
	
	virtual LEVELTYPE		GetLevel		()const;
	virtual DWORD			GetExp		()const;
	virtual DWORD			GetMaxHP		()const;
	virtual DWORD			GetMaxMP		()const;
	virtual DWORD			GetNextExp	()const;
	virtual DWORD			GetInfoID	()const;
	virtual eATTACK_KIND	GetAttackKind	(eATTACK_KIND defaultKind=ATTACKKIND_ALL_OPTION) const ;

	///////////////////////////////////////////////////
	//其它辅助信息
	virtual void      SetCondition(BYTE byCondition);

	virtual CODETYPE	GetCurrentVSkill			();
	virtual DWORD     GetAttackSpeed				()const;
	virtual float     GetAttackSpeedModifier	()const;
	virtual float     GetMoveSpeedModifier		()const;

	virtual VOID		SetAttackSpeedModifier	( float fAttSpeed );	
	virtual VOID		SetMoveSpeedModifier		( float fMoveSpeed );
	
	virtual BOOL      IsNormalRangedAttack		();

public:
	///////////////////////////////////////////////////
	//动作辅助信息
	virtual	void		ProcessStandAnimationDelay	(DWORD dwDelay);
	virtual	void		SetStandAnimationDelay		();
	virtual  void		DisplayAIInfo					(DWORD dwAIType, DWORD dwIndex, DWORD dwIndex2);



	///////////////////////////////////////////////////
	//技能状态信息
	virtual	int		AddSkillEffect		(SKILL_EFFECT *pEffect);
	virtual void		DeleteSkillEffect	(int iEffectID);


	///////////////////////////////////////////////////
	//技能相关操作
	virtual void      UpdateSkillAttributes	();
	virtual BOOL      CanUseSkill						(DWORD   skillID
                                                ,BOOL		bShowFailMessage = FALSE)		;
	virtual BOOL      CanUseSkillLimitClassDefine(DWORD   skillID
                                                ,BOOL		bShowFailMessage = FALSE)		;

	virtual void      AddDelaySkillSerial		(DWORD dwDelaySkill);
	virtual DWORD		GetDelaySkillSerialCount();
	virtual DWORD     GetDelaySkillSerial		();

	virtual void      SetSummonMasterID			(const DWORD & dwID);


public:
	///////////////////////////////////////////////////
	//音效处理
	virtual void      PlayIdleVoice	();
	virtual void      PlaySkillVoice	();
	virtual void      PlayBattleVoice();
	virtual void      PlayHurtVoice	();
	virtual void      PlayDieVoice	();
	virtual void      PlayStateVoice	();	


	virtual void      CreateEffect	(){};
	virtual void      DestoryEffect	(){};


public:
	virtual void		SetMonsterInfo(sNPCINFO_BASE *pInfo);
	sNPCINFO_BASE *	GetMonsterInfo();
	sVNPCINFO_BASE *	GetVMonsterInfo();
	const sNPCINFO_BASE *	GetMonsterInfo()const ;
	const sVNPCINFO_BASE *	GetVMonsterInfo()const ;


protected:
	sNPCINFO_BASE				m_NPCInfo;
	sVNPCINFO_BASE*			m_pVNPCInfo;

	list<DWORD>					m_DelaySkillSerial;
	VG_DWORD_GET_PROPERTY	(SummonMasterID);
	DWORD							m_dwStandAnimDelay;

	///////////////////////////////
	//速度信息
	float							m_fAttackSpeedModifier;
	float							m_fMoveSpeedModifier;

	///////////////////////////////
	//音效信息
	BOOL							m_bEnableIdleSound;
	int							m_iAlarmVoice;
	int							m_iWaitSoundDelay;
	
};//NPC

OBJECT_FACTORY_DECLARE(NPC);

};//namespace object



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "NPC.inl"








#endif //__NPC_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：WorldHandlerDefine.h
创建日期：2008年4月4日
最后更新：2008年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __WORLDHANDLERDEFINE_H__
#define __WORLDHANDLERDEFINE_H__
#pragma once



typedef DWORD	WH_TYPE;
enum
{
	WH_TYPE_NULL = -1,
	WH_TYPE_BLANK,
	WH_TYPE_LOGIN,
	WH_TYPE_CHARSELECT,
	WH_TYPE_BATTLE,
	WH_TYPE_VILLAGE,
};

enum
{
	 ATTACH_OK		= 0
	,ATTACH_ERROR
	,ATTACH_NONE_SCENE
	,ATTACH_NONE_EYE
	,ATTACH_NONE_LOOKAT
	,ATTACH_NONE_SKY   
};

static LPCSTR gs_szAttachErrorText[]=
{
	 "OK"
	,"Attach出错"
	,"SCENE"
	,"EYE"
	,"LOOKAT"
	,"SKY"
};


#endif //__WORLDHANDLERDEFINE_H__
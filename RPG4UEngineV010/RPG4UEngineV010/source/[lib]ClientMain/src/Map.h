/*////////////////////////////////////////////////////////////////////////
文 件 名：Map.h
创建日期：2008年6月6日
最后更新：2008年6月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

	地图主类

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAP_H__
#define __MAP_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "MapHandleManager.h"
#include "MapDefine.h"
#include "SceneStruct.h"
#include "V3DGameWorldListener.h"

using namespace scene;
class MapAttribute;
class MapTrigger;

class V3DGameWorld;
_NAMESPACEU	(MapObject,	object);
_NAMESPACEU	(Character,	object);
_NAMESPACEU	(Item,		object);
_NAMESPACE2U(SpecialArea,tile);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API Map	: public MapHandleManager
									, public V3DGameWorldMapLoadingListener
{
public:
	Map(void);
	virtual ~Map(void);

public:
	virtual BOOL Init		();
	virtual BOOL Init		(V3DGameWorld* pVMap);
	virtual void Release	();
	virtual BOOL RequestWorkingData();
	virtual void ReleaseWorkingData();

	BOOL FrameMove	(DWORD dwTick );
	BOOL Render		(DWORD dwTick);

public:
	/// Listener实现函数
	virtual void OnLoadMapPlot		(V3DGameMap* pMapPlot);
	virtual void OnUnloadMapPlot	(V3DGameMap* pMapPlot);


public:

	BOOL LoadLand	(LPCSTR szLandName, const Vector3D& vStartPos);

	BOOL Load		(MAPCODE           MapID
                  ,FIELDID         FieldID
						,const Vector3D& vStartPos);
	void UnLoad		(void);

public:

	void ProcessLoadInfo	(DWORD dwTick);
	void AddPlotLoadInfo	(V3DGameMap* pMapPlot,ePlotLoadAction eAction);
	void LoadNPCsIn		(VMapPlotLoadInfo& info);
	void UnloadNPCsIn		(VMapPlotLoadInfo& info);

public:
	void ToggleShowAreaInfo		(void);
	void RenderAreaInfo			(void);
	void RenderSector				();

	void ProcessMapObjects		( DWORD dwTick );

public:
	Ray*	GetMouseCursorRay		(Ray * pRay, POINT *pptCursor);
	int	GetCurrentFieldID		(void)const;



	SpecialArea *GetMapArea			(DWORD dwID );
	SpecialArea *GetMapAreaByIndex(DWORD dwIndex);

	BOOL GetStartAreaPosition		(Vector3D * pwzPos);
	BOOL GetPlayerDirection			(Vector3D * pwzDir);


	BOOL	GetTerrainHeight			(const Vector3D& vPos,Vector3D& vResult);
	DWORD GetSectorIndex				(const Vector3D& vPos ) ;
	

public:
	LPCSTR		GetFieldName		();

public:


	void        PlayAmbientSound();
	void        StopAmbientSound();
	void			SetAmbientVolume(float fVolume);



public:
	virtual	void OnLoadingFinished	();

protected:
	BOOL _RegMapHandles		();
	void _UnregMapHandles	();

	BOOL LoadBaseMapInfo		(MAPCODE MapID, FIELDID FieldID);
	BOOL LoadMapData			();
	BOOL LoadMapObject		();


	BOOL LoadCameraExtraInfo();

	//BOOL LoadAreaInfo		();
	BOOL LoadTrigger			();
	BOOL LoadTriggerGroup	();
	void CalculateSectorInfo();



protected:
	INT						m_nLoadTimer;
	PlotLoadInfoMap		m_mpPlotLoadInfo;

	VG_PTR_GET_PROPERTY	(Trigger, MapTrigger);

	//////////////////////////////////////////////////////////////////////////
	
	char							m_szFullFileName		[MAX_PATH];
	char							m_szMapObjectDir		[MAX_PATH];
	VRSTR							m_sFieldID;

	VG_TYPE_PROPERTY			(MapID,		MAPCODE);
	VG_TYPE_PROPERTY			(FieldID,	FIELDID);
	int                     m_iFieldIndex;


	sFIELDINFO_BASE *			m_pCurFieldInfo;
	sMAPINFO_BASE *			m_pMapInfo;
   VG_INT2_PROPERTY			(TriggerGroupIndex);

	Vector3D						m_vStartPos;
	V3DGameWorld*				m_pVMap;


	VG_TYPE_PROPERTY			(ExtraCameraInfo, CAMERA_EXTRA_INFO);
   VG_DWORD_PROPERTY			(CurrentTrigger);


	VG_BOOL_GET_PROPERTY		(Loaded);



	BOOL              m_bRenderAreaInfo;
	int					m_iAmbientSoundHandle;
	// SectorIndex
	DWORD             m_dwSectorSize;
	int               m_iShiftMinX;
	int               m_iShiftMinY;
	DWORD             m_dwSectorXNum;
	DWORD             m_dwSectorYNum;
	DWORD             m_dwTotalSectorNum;

	VRSTR					m_sFieldName;

	VG_FLOAT_PROPERTY	(MapProjectionFar);
	VG_FLOAT_PROPERTY	(MapProjectionNear);
	VG_FLOAT_PROPERTY	(MapProjectionFOV);

	sCAMERA_INFO		m_CameraInfo;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(Map , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API Map& singleton::GetMap();
#define theMap					singleton::GetMap()
//#define MapHandleManager	theMap


#endif //__MAP_H__
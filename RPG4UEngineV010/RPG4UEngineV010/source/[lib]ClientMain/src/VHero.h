/*////////////////////////////////////////////////////////////////////////
文 件 名：VHero.h
创建日期：2007年2月29日
最后更新：2007年2月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VHERO_H__
#define __VHERO_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "VPlayer.h"
#include "VHeroDefine.h"
//#include "VHeroInputData.h"

namespace vobject
{

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API VHero : public VPlayer
{
public:
	VHero();
	~VHero();

public:
	virtual void	Update		(DWORD dwTick );
	virtual void   UpdateManual(DWORD dwTime );
	virtual BOOL	RenderInfo	(DWORD dwTick
                              ,DWORD dwColor=0xffffffff
										,float fTransparent=1.0f
										,float fPlayerTransparent=1.0f);

	virtual void	JumpToTilePos	( int x,int y, PathGameMap* pMap );
	virtual void	JumpToTilePos	( const VECTOR3D& vDest, PathGameMap* pMap );
	virtual void	SetMoving		( BOOL bMoving , BOOL bBackOff=FALSE);

	virtual void	DisDrome			();


public:
	virtual BOOL	GetCollisionDistance	(Vector3C& vStart
													,Vector3C& vDir
													,float*    fDistToCollision=NULL);

	//主角基本操作
	virtual BOOL	TryUseSkill		( VCharacter* pTagPlayer, sVSKILLINFO_BASE* pSkill );

	virtual BOOL	TryDropItem		( UINT nPackIndex );
	virtual BOOL	TryPickItem		();
	virtual void	TryPickItem		( VObject* pItem );

	virtual BOOL	TryTalkWithNpc	();
	virtual void	TryTalkWithNpc	( VCharacter* pNpc );
   virtual BOOL	TryTradeWithNpc();
	virtual BOOL   IsCanTalk			(int iDestID);					//	判断能否对话

	virtual BOOL	IsCanUseItem		();			//	判断可否使用道具
	virtual BOOL	IsCanDoExpression	();//判断可否作表情动作
	virtual BOOL	IsTargetIsRight	(int skill, int skill_level, int id);
   virtual BOOL	IsTagCanBeAttack	(); //判定目标可否攻击
	virtual BOOL	IsInAttackRange	( float fTargetX, float fTargetY, float fTargetBodySize, sVSKILLINFO_BASE *pSkill );
	virtual BOOL	IsTargetInFront	( float fTargetX, float fTargetY, float fTargetZ, float fAngleMax = math::cPI_V2 );

   //状态判断函数
	virtual BOOL	IsPlayerCanChangeToStatus	(int next_status);//	判断player能否改变到目标状态
   virtual BOOL	IsPlayerCanChangeStatus		();					// 判断一个玩家是否可以改变状态(动作)

	//移动
	virtual BOOL	IsMoveStepOver		();//  判断是否处在一步走完的状态
   virtual BOOL	IsCanMove			();        //判断可否移动

	virtual  BOOL  CanRemoveAttackInfo	(sATTACK_INFO* pAttackInfo);

	virtual void	OnStepHeroRange	(BOOL bIn);
	virtual BOOL	CanHideOutofHeroRange(float fAlphaGap=0.01f);

public:
	////////////////////////////////////////////////
	//行为命令
	#define VCHARACTER_ADDOP(Name,p)		BOOL AddOperation##Name p

   VCHARACTER_ADDOP (Idle,					() );
	VCHARACTER_ADDOP (PickItem,			(VCharacter* pItem) );
	VCHARACTER_ADDOP (UseSkill,			( int tag_npc, int x, int y, int skillid, int skilllevel ) );
	VCHARACTER_ADDOP (GotoAttackTarget, ( int tag_npc, int x, int y, int iSkillID, int iSkillLevel = 0) );
	VCHARACTER_ADDOP (GotoTalkWithNpc,	( VCharacter* pNpc ) );
	VCHARACTER_ADDOP (MoveTo,				( TILEPOS x, TILEPOS y  ) );
   VCHARACTER_ADDOP (GotoPickItem,		( VCharacter* pNpc ) );
	VCHARACTER_ADDOP (GotoPickRangeItem,() );
	VCHARACTER_ADDOP (ChangeDir,			( TILEPOS x, TILEPOS y  ) );
	VCHARACTER_ADDOP (GotoDropItem,		( TILEPOS x, TILEPOS y  ) );
	VCHARACTER_ADDOP (GotoTrade,			( VCharacter* pNpc ) );
   VCHARACTER_ADDOP (FollowNpc,			( VCharacter* pNpc ) );
	VCHARACTER_ADDOP (VerifyPos,			( TILEPOS x, TILEPOS y, DWORD dwPeriod ) );
	VCHARACTER_ADDOP (DoAction,			( int ActionID) );


public:

	/////////////////////////////////////////////////
	//最小操作的定义
	BOOL  _HandleNothing	();
	BOOL  _HandleIdle		();			//待机
	BOOL  _HandleUseSkill();			//使用技能
	BOOL  _HandleTalk		();			//对话
	BOOL  _HandleGetItem	();			//取得道具
	BOOL  _HandleDropItem();			//丢弃道具
	BOOL  _HandleMoveTo	();			//移动到目标地点
	BOOL  _HandleTrade	();			//交易

	BOOL  _HandleChangeDir		();	//转身
	BOOL  _HandleMoveSkill		();   //移动过去使用技能
	BOOL  _HandleMoveTalk		();   //移动过去对话
	BOOL  _HandleMovePickItem	();	//移动过去取得道具
	BOOL  _HandlePickItem		();	//移动取得周围的道具
	BOOL  _HandleMoveDroptem	();	//移动过去丢弃道具
	BOOL  _HandleMoveTrade		();   //移动过去交易

	BOOL  _HandleMoveFollow		();   //跟随
	BOOL	_HandleDoAction		();	//作莫个动作
	BOOL	_HandleVerifyPos		();	//

protected:

	//状态控制函数
	//virtual void    _DoStatusIdle();         //待机
	virtual void    _DoStatusPickItem();	  //捡道具
	virtual void    _DoStatusDropItem();	  //丢弃道具
	virtual void    _DoStatusTalk();         //对话
	virtual void    _DoStatusTrade();        //交易
	//virtual BOOL    _DoStatusMove();         //移动
	virtual void    _DoStatusChangeDir();	  //转身
	//virtual void    _DoStatusPreAttack();    // 预备攻击状态
	////
	//virtual void	 _DoStatusPreIntonate();
	//virtual void	 _DoStatusHoldSkill();

	//virtual void    _DoStatusAttack();       // 攻击状态
	//virtual void    _DoStatusIntonate();	  // 吟唱
	//virtual void    _DoStatusBeAttack();     // 受击状态
	//virtual void    _DoStatusMiss();         // 闪避状态
	//virtual void    _DoStatusSleep();        // 昏倒状态
	//virtual void    _DoStatusDie();          // 死亡状态
	//virtual void    _DoStatusBeatBack();	  // 击退
	//virtual void    _DoStatusActionTime();	  //在一段时间内作莫个动作

public:
	virtual BOOL   CanSwitchAnim();
	virtual BOOL   CanCountermarchAnim();
	void					StopMoving();			//停止移动
	VCHARACTER_ANIM	(Walk);					//切换到行走的动作
	VCHARACTER_ANIM	(Run);					//切换到跑步的动作
   VCHARACTER_ANIM	(Idle);					//切换到待机动作
	VCHARACTER_ANIM	(Assault);				//冲锋动作      
	VCHARACTER_ANIM	(Jump);					//

public:
	virtual BOOL   IsInHeroRange			(float fRadius);

protected:
	virtual void	_CalcStepCost			(float& fCost);
	virtual void	_OnNextStep();

	virtual void	_UpdateMountAnim		(DWORD dwTime);

	virtual void   _DoRotation				(float fRot);
	virtual float  _GetUpdatePosZ			(FLOAT fRoleHeight);
	virtual void   _UpdateJumping			(DWORD dwTick);
	virtual BOOL   _PrevIntoWater			();
	virtual void   _OnIntoWater			();
	virtual void   _OnOutofWater			();
	virtual void   _UpdateFootStep		(DWORD dwTick);
	virtual void   _UpdateWalkingOnLand	(DWORD dwTick);

	virtual void	_OnMountMonster		(BOOL bMountUp);

protected:
	virtual void _UpdateAutoMoving		(Vector3& vNextFramePos,float fRotateX);
	virtual void _UpdateMovingGoal		(Vector3& vNextFramePos);
	virtual BOOL _CheckMovingCollision	(const Vector3& vNextFramePos);
	virtual void _ApplyMovingData			(BOOL bCollision, const Vector3& vNextFramePos);
	virtual void _SendMovingState			();
	virtual void _UpdateMovingState		();
public:
    //状态控制函数
	//virtual void    _DoStatusIdle();         //待机
	//virtual void    _DoStatusPickItem();	  //捡道具
	//virtual void    _DoStatusDropItem();	  //丢弃道具
	//virtual void    _DoStatusTalk();         //对话
	//virtual void    _DoStatusTrade();        //交易
	virtual BOOL    _DoStatusMove();         //移动
	//virtual void    _DoStatusChangeDir();	  //转身
	//virtual void    _DoStatusPreAttack();    // 预备攻击状态
	////
	//virtual void	 _DoStatusPreIntonate();
	//virtual void	 _DoStatusHoldSkill();

	//virtual void    _DoStatusAttack();       // 攻击状态
	//virtual void    _DoStatusIntonate();	  // 吟唱
	//virtual void    _DoStatusBeAttack();     // 受击状态
	//virtual void    _DoStatusMiss();         // 闪避状态
	//virtual void    _DoStatusSleep();        // 昏倒状态
	//virtual void    _DoStatusDie();          // 死亡状态
	//virtual void    _DoStatusBeatBack();	  // 击退
	//virtual void    _DoStatusActionTime();	  //在一段时间内作莫个动作

public:
	//PathHandlerListener 虚函数
	virtual void OnStop			();
	virtual void OnReset			();
	virtual void OnReadyToMove	(BOOL bThrust);

public:
	virtual void UpdateDistanceToHero(  const VECTOR3D& vPos );

public:
	VG_TYPE_PROPERTY		(LockTargetID, VOBJID);
	VG_DWORD_PROPERTY		(StartSlipTime);
	VG_TYPE_PROPERTY		(TurnDirection,		VECTOR3D);		//当前方向变换
};
};//namespace vobject

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(vobject::VHero ,VHero, _CLIENTMAIN_API);
//extern _CLIENTMAIN_API VHero& singleton::GetVHero();
#define theVHero  singleton::GetVHero()


#endif //__VHERO_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillStorageManager.h
创建日期：2008年5月20日
最后更新：2008年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SKILLSTORAGEMANAGER_H__
#define __SKILLSTORAGEMANAGER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "SkillStorageManagerDefine.h"
#include "SkillStorageInfo.h"
#include "UISkillContainer.h"
#include "SkillStorageParserDefine.h"


class UISkillContainer; 
class SkillPaneContainer;
class SkillSlot;
class SkillPaneSlot;
struct MSG_CG_SKILL_SELECT_SKILLPOINT_SYN;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API SkillStorageManager 
{
	friend class SkillStorageOpr;
public:
    SkillStorageManager();
    virtual ~SkillStorageManager();

    //--------------------------------------------------------------------------
public:
    virtual BOOL            Init		(ePLAYER_TYPE charType);
    virtual void            Release	();
    void                    Clear	();

    //--------------------------------------------------------------------------
public:

    UINT							GetSkillNumActive1	() ;
    UINT							GetSkillNumActive2	() ;
    UINT							GetSkillNumPassive	() ;
    UINT							GetSkillNumStyle		() ;
    UINT							GetSkillNumEmotion	() ;
    UINT							GetSkillNumAction		() ;

    const SkillStorageInfo *		GetSkillActive1		(UINT nIndex);
    const SkillStorageInfo *		GetSkillActive2		(UINT nIndex);
    const SkillStorageInfo *		GetSkillPassive		(UINT nIndex);
    const StyleStorageInfo *		GetSkillStyle			(UINT nIndex);
    const SkillStorageInfo *		GetSkillEmotion		(UINT nIndex);
    const SkillStorageInfo *		GetSkillAction			(UINT nIndex);


private:
    SkillStorageInfo*      _SetPageSkill		(sSKILL_STORAGEINFO * pSkillInven
															,UINT   nAt
															,const SkillStorageInfo& skillInfo);
    void					      _SetPageSize		(sSKILL_STORAGEINFO * pSkillInven);
    SkillStorageArray*     _GetPage				(sSKILL_STORAGEINFO * pSkillInven);



public:
    void                   ResetOnceInitSkillInfos();

    void                   LoadFromInventoryStore();
    void                   LoadInfo_Active1	(sSKILL_STORAGEINFO * pSkillInven);
    void                   LoadInfo_Active2	(sSKILL_STORAGEINFO * pSkillInven);
    void                   LoadInfo_Passive	(sSKILL_STORAGEINFO * pSkillInven);
    void                   LoadInfo_Style		(sSKILL_STORAGEINFO * pSkillInven);
    void                   LoadInfo_Emotion	(sSKILL_STORAGEINFO * pSkillInven);
    void                   LoadInfo_Action	(sSKILL_STORAGEINFO * pSkillInven);

public:
    void                   ResetLevelUpInfo				();
    void                   ResetLevelUpInfo_Active1	();
    void                   ResetLevelUpInfo_Active2	();
    void                   ResetLevelUpInfo_Passive	();
    void                   ResetLevelUpInfo_Style		();

public:
    void                   SlotUpdate				(const SKILL_STREAM& stream);
    BOOL                   SlotUpdate_Active1	(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);
    BOOL                   SlotUpdate_Active2	(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);
    BOOL                   SlotUpdate_Passive	(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);
    BOOL                   SlotUpdate_Style		(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postStyleInfo);

public:
    void                   SetSkillInfos					(const sTOTALINFO_SKILL& skillInfos, BOOL bAppend=FALSE);
    BOOL                   SetSkillInfos_Active1		(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);
    BOOL                   SetSkillInfos_Active2		(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);
    BOOL                   SetSkillInfos_Passive		(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);
    BOOL                   SetSkillInfos_Style			(const SkillPaneSlot& postSlot, sSKILLINFO_COMMON* postSkillInfo);

public:
    BOOL                   IsStyleActivated				(DWORD dwCode);

   SkillSlot *					GetSkillSlot			(CODETYPE code, BOOL bCheckClassCode=FALSE);
   SkillStorageInfo *		GetSkillStorage		(SLOTCODE code, BOOL bCheckClassCode);
	BOOL							CheckSkillDependence	(CODETYPE code);
	BOOL							CheckSkillCanUsing	(SLOTPOS	 pos);
	SLOTCODE						GetLevelUpSkillCode	(SLOTPOS	 pos);
	BOOL							CheckSkillValid		(sSKILLINFO_COMMON*	pSkill,DWORD dwSkillKind);
	BOOL							CheckNextSkillValid	(sSKILLINFO_COMMON*	pSkill,DWORD dwSkillKind);

	BOOL							CanLearnSkill			(sSKILLINFO_COMMON*	pSkill,DWORD dwSkillKind);
	BOOL							CanLevelUpSkill		(sSKILLINFO_COMMON*	pSkill,DWORD dwSkillKind);
	BOOL							CheckSkillSlotState	(SkillStorageInfo& info,DWORD dwSkillKind);

	SkillStorageInfo*			GetSkillStorageAt		(SLOTPOS	 pos);
public:
	 BOOL							SetUISkillContainer(SkillPaneContainer* pContainer);
	 BOOL							SetUISkillContainer(eSKILL_CATEGORY containerType);
	 SkillPaneContainer*		GetUISkillContainer(eSKILL_CATEGORY type);

    //--------------------------------------------------------------------------
public:
    virtual void				NetworkProc( MSG_BASE * pMsg );         // Pure
    void                   SendSelectSkillPointMsg(MSG_CG_SKILL_SELECT_SKILLPOINT_SYN& msg);


    //--------------------------------------------------------------------------
private:
    SkillStorageArray   m_arSkillInfoActive1;
    SkillStorageArray   m_arSkillInfoActive2;
    SkillStorageArray   m_arSkillInfoPassive;
    SkillStorageArray	m_arSkillInfoStyle;
    SkillStorageArray   m_arSkillInfoEmotion;
    SkillStorageArray   m_arSkillInfoAction;

	 union
	 {
		 struct
		 {

			 UISkillContainer*			m_pUISkillActive1;
			 UISkillContainer*			m_pUISkillActive2;
			 UISkillContainer*			m_pUISkillActive2Shot;
			 UISkillContainer*			m_pUISkillPassive;
			 UISkillContainer*			m_pUISkillStyle;
			 UISkillContainer*         m_pUISkillAction;
			 UISkillContainer*			m_pUISkillEmotion;   
		 };
			UISkillContainer*				m_arContainers[SKILL_KIND_MAX];
	 };

	 VG_PTR_GET_PROPERTY			  (UISkillSlotContainer,	SkillPaneContainer);
	 VG_PTR_GET_PROPERTY			  (SkillStorage,				SkillStorageArray);

    BOOL									m_bOnceInitSkillInfos;
	 VG_TYPE_PROPERTY					(PlayerType, ePLAYER_TYPE);
	 eSKILL_CATEGORY					m_nSkillCategory;

};//SkillStorageManager


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(SkillStorageManager , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API SkillStorageManager& singleton::GetSkillStorageManager();
#define theSkillStorageManager  singleton::GetSkillStorageManager()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "SkillStorageManager.inl"



#endif //__SKILLSTORAGEMANAGER_H__
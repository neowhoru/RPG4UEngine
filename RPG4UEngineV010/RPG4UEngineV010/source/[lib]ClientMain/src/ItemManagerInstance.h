
/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManagerInstance.h
创建日期：2008年5月31日
最后更新：2008年5月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

			 - 各种物品移动处理、使用处理,在服务器的消息处理
			 - DoItemUsing
			 - Itemcombine
			 - Item move
			 - Item Equip
			 - Item pick up
			 - Item drop
			 - Quick slot Link / Unlink 
			 - Style Quick slot Link / Unlink

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMMANAGERINSTANCE_H__
#define __ITEMMANAGERINSTANCE_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "ItemManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API ItemManagerInstance : public ItemManagerClient
{
public:
	ItemManagerInstance();
	~ItemManagerInstance();

	BOOL								Init();
	VOID								Release();
	//VOID								Render();
	//VOID								Update();

public:


public:
	BaseContainer *				GetContainer				(SLOTINDEX ContainerIndex );
   SkillSlot *						GetSkillSlot				(CODETYPE code);
	BOOL								GetItemDesc					(SERIALTYPE  serial
                                                      ,SLOTINDEX & slotIdxOut
																		,SLOTPOS &   posOut);

	BOOL								GetEmptySlotPos			(SLOTINDEX	AtSlotIdx, SLOTPOS & OUT rPosOut);
	BOOL								GetEquipPosition			(BaseSlot & slotDat, SLOTPOS & OUT rEqPosOut);

public:
	// sound	
	DWORD								GetSkillSound	(eSLOT_SOUND_TYPE eSoundType, SkillSlot * pSkillSlot);
	DWORD								GetItemSound	(eSLOT_SOUND_TYPE eSoundType, ItemSlot *	pItemSlot);
	DWORD								GetItemSound	(eSLOT_SOUND_TYPE eSoundType, DWORD			dwItemType);
	VOID								PlaySlotSound	(eSLOT_SOUND_TYPE eSoundType, BaseSlot *	pSlot );

	// 
	BOOL								InsertInventoryItems	(eTOTALINFO_INSERT_TYPE		eType
                                                   ,sTOTALINFO_INVENTORY &		totalInfo)		;
	BOOL								InsertEventItems		(sTOTALINFO_EVENT_ITEM &	totalInfo);
	BOOL								ExistEmptySlotAllInventory	();
	
public:
	// 网络消息处理
	VOID								ParsePacket				(MSG_BASE * pMsg );


public:
	/*////////////////////////////////////////////////////////////////////////
	@name Item移动处理
		上装、下装本身亦看作为物品移动处理的其中一类
	/*////////////////////////////////////////////////////////////////////////
	VOID								MoveItem			(SLOTINDEX fromIdx
                                             ,SLOTINDEX toIdx
															,SLOTPOS   fromPos
															,SLOTPOS   toPos);								

	// QuickPaneSlot处理
	VOID								LinkItemToQuick(SLOTINDEX  atIndex
                                             ,SLOTPOS		OrgPos
															,SLOTPOS		toQuickPos
															,SLOTCODE	OrgCode=0);

	VOID								LinkSkillToQuick(SLOTINDEX  atIndex
                                             ,SLOTCODE	OrgCode
															,SLOTPOS		toQuickPos); 

	// StyleSlot处理
	VOID								LinkStyle		(SLOTCODE OrgCode, SLOTPOS toQuickPos ); 

	// Quick/Style处理
	VOID								MoveLinkItem	(SLOTINDEX	AtIndex
                                             ,SLOTPOS		fromPos
															,SLOTPOS		toQuickPos);

	// Dummy Item 处理
	VOID								LinkDummyItem	(SLOTINDEX	fromIdx
                                             ,SLOTINDEX	toIdx
															,SLOTPOS		fromPos
															,SLOTPOS		toPos);								


public:
	//////////////////////////////////////////////////////////////
	///@name 商店物品处理
	VOID								TradeBuyItem	(SLOTINDEX fromIdx
                                             ,SLOTINDEX toIdx
															,SLOTPOS fromPos
															,SLOTPOS toPos);

	VOID								TradeSellItem	(SLOTINDEX fromIdx
                                             ,SLOTINDEX toIdx
															,SLOTPOS fromPos
															,SLOTPOS toPos);


public:
	//////////////////////////////////////////////////////////////
	///@name 物品使用处理
	VOID								UseInventoryItem( SLOTINDEX AtIndex, SLOTPOS posIndex );
	VOID								UseEquipmentItem( SLOTINDEX AtIndex, SLOTPOS posIndex );

	VOID								EquipItem	(SLOTINDEX AtIndex
                                          ,SLOTPOS posIndex)		;

public:
	//////////////////////////////////////////////////////////////
	VOID								CopyItem		(SLOTINDEX fromIdx
                                          ,SLOTINDEX toIdx
														,SLOTPOS fromPos
														,SLOTPOS toPos);




public:
	// Item属性存取
	BOOL								CanEquip			(SLOTINDEX AtIndex
                                             ,SLOTPOS posIndex
															,SLOTPOS ToPos);

	BOOL								CanEquipClass	( sITEMINFO_BASE * pInfo ) const;
	BOOL								CanEquipLevel	( sITEMINFO_BASE * pInfo ) const;





	BOOL								CanLinkQuick		(SLOTINDEX           AtIndex
                                                ,SLOTPOS           ToPos
																,const BaseSlot * IN pSlot);
	BOOL								CanLinkStyleQuick	(SLOTINDEX           AtIndex
                                                ,SLOTPOS           ToPos
																,const BaseSlot * IN pSlot);

	//BOOL								CanUse( SLOTINDEX AtIndex, SLOTPOS posIndex);
	


public:
	/////////////////////////////////////////////////////////
	BOOL								IsExistItem						(SLOTINDEX  FromSlotIdx
                                                         ,CODETYPE ItemCode
																			,int      iItemNum);
	BOOL								SpendItem						(SLOTINDEX  FromSlotIdx
                                                         ,CODETYPE ItemCode
																			,int      iItemNum);

	
	//物品重叠处理
	int								GetItemToltalAmount	(CODETYPE				code
                                                   ,SLOTINDEX				AtIndex = SI_INVENTORY)		;
	BOOL								LocateItemAtFirst			(CODETYPE				code
                                                   ,SLOTPOS & OUT			rOutPos
																	,SLOTINDEX				AtIndex = SI_INVENTORY);
	VOID								UpdateQuickInfo		();


	//
	VOID								UpdateQuickForEquips	();
	VOID								UpdateQuickItems			();
	//--

public:
	///物品掉落
	BOOL								CreateFieldItem		( const sITEMINFO_RENDER * pItemInfo );


	//////////////////////////////////////////////////////////////////////////
	// NAK 物品信息出错处理
	VOID								ProcessItemError(DWORD dwErrCode);
		
public:
	//////////////////////////////////////////////////////////////////////////
	//以下为Server模拟接口
	BOOL						IsPossibleFunction( eITEM_FUNCTIONAL_TYPE /*type*/ ) { return TRUE; }
	BOOL						ValidState(){return TRUE;}

	virtual ItemSlotContainer *	GetItemSlotContainer( SLOTINDEX Index );
	virtual RC::eITEM_RESULT		Lose(SLOTCODE			ItemCode
                                     ,BOOL				bAllOfItem)		;
	virtual VOID						Lose(BaseContainer * pContainer
                                     ,SLOTPOS         pos
												 ,BYTE            num);
	virtual VOID						Lose(SLOTINDEX			slotIdx
                                     ,SLOTPOS			pos
												 ,BYTE				num);

	RC::eITEM_RESULT		Obtain(SLOTCODE                   ItemCode
                              ,SLOTPOS                    num
										,sTOTALINFO_INVENTORY * OUT pTotalInfo);
	RC::eITEM_RESULT		Obtain(ItemSlot & IN              slot
                              ,SLOTPOS                    num
										,sTOTALINFO_INVENTORY * OUT pTotalInfo);

	VOID					CanInsertItem	(BaseContainer * pContainer
                                    ,SLOTPOS         MAX_DUP_NUM
												,SLOTPOS & INOUT need_num
												,SLOTPOS * OUT   PosArray
												,SLOTPOS & OUT   PosNum);

	BOOL					IsExistItem		(SLOTINDEX         idx
                                    ,SLOTCODE        code
												,DURATYPE        dur
												,SLOTPOS * OUT   pPosArray
												,SLOTPOS * INOUT pPosNum);

	BOOL					IsExistItemDup	(SLOTINDEX         idx
												,SLOTCODE        code
												,BYTE            num
												,SLOTPOS * OUT   pPosArray
												,SLOTPOS & INOUT posNum) ;
	BOOL					IsExistItem		( SLOTINDEX Index, SLOTCODE code );

	
	RC::eITEM_RESULT		MakeItem		(SLOTCODE					destItemCode
												,DWORD						dwMaterialNums
												,BYTE							byMaterialRow
												,SLOTPOS*					parMaterialPos
												,LPCSTR						szItemName
												,sTOTALINFO_INVENTORY&	resultItems);

protected:
	VOID								_MoveLinkItem	(SLOTINDEX AtIndex
                                             ,SLOTPOS fromQuickPos
															,SLOTPOS toQuickPos);
	VOID								_MoveLinkStyle	(SLOTINDEX AtIndex
                                             ,SLOTPOS fromQuickPos
															,SLOTPOS toQuickPos);

};//class ItemManagerInstance


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ItemManagerInstance , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API ItemManagerInstance& singleton::GetItemManager();
#define theItemManagerInstance  singleton::GetItemManagerInstance()



#endif //__ITEMMANAGERINSTANCE_H__
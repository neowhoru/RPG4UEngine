/*////////////////////////////////////////////////////////////////////////
文 件 名：Player.h
创建日期：2008年3月31日
最后更新：2008年3月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PLAYER_H__
#define __PLAYER_H__
#pragma once

#include "CommonDefine.h"
#include "Character.h"
#include "PlayerDefine.h"
#include "PlayerCookie.h"
#include "TArray.h"
#include "PacketStruct_Base.h"
#include "ObjectFactoryManager.h"
#include "IScriptCharacterListener.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PlayerAttributes;
class BaseContainer;
class AttributeAffectorSkillHandle;
class ItemSlot;

_NAMESPACEU(VPlayer, vobject);

#ifndef NONE_MONSTERTESTING
class ItemManager;
#endif

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

namespace object
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API Player : public Character, public IScriptCharacterListener
{
public:
	Player( void );
	virtual ~Player( void );


public:
	virtual BOOL				Create	( DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void				Destroy	( void );
	virtual BOOL				Process	( DWORD dwTick );
	virtual BaseContainer *	GetSlotContainer( SLOTINDEX slotIdx );

protected:
	virtual BOOL				InitContainer		();
	virtual void				ReleaseContainer	();

public:
	/*////////////////////////////////////////////////////////////////////////
	脚本与任务操作
	/*////////////////////////////////////////////////////////////////////////

	virtual BOOL	RunScript			(DWORD dwData, DWORD	npcID,BOOL bRunNow=FALSE );
	virtual BOOL	RunQuestScript		(CODETYPE questID, DWORD dwData,BOOL bRunNow=FALSE);//, DWORD	npcID );

	virtual DWORD	GetQuestState		( CODETYPE	questID );
	virtual void	SetQuestState		( CODETYPE	questID, DWORD state );
	virtual void	DiscardQuest		( CODETYPE	questID);
	virtual BOOL	IsQuestActive		( CODETYPE	questID );
	virtual BOOL	IsQuestDone			( CODETYPE	questID );
	virtual BOOL	CanGetNewQuest		();
	virtual BOOL	IsNeedDropTaskItem(CODETYPE		questID
												,DWORD			taskState
												,SLOTCODE		itemCode);

	virtual void	ChangeQuestData	(CODETYPE questID
                                    ,DWORD    state
												,DWORD    action);
	virtual void	OnSetItemLog		(BOOL bSet);



public:
	////////////////////////////////////////////////
	//其它信息
	virtual void	LookLockedTarget		();
	virtual void	UpdateAppearance		(BOOL		bAddUpdateFlag=TRUE);
	virtual void	SetEquipAppearInfo	(EQUIPAPPEARINFO *pInfo,ItemSlot & slotDat);
	virtual void	UpdateAttackReady		(DWORD	dwTick);
	virtual void   SetCondition			(BYTE		byCondition);
	virtual DWORD	GetCharSoundCode		();

	virtual void	SetObserverMode		(BOOL bObserverMode, BOOL IsHero = FALSE );
	virtual void   HideHelmet				(BOOL bFlag,			BOOL bRefresh = TRUE);
	virtual void   SetHPInfo				(HPINFO *pInfo);

	virtual BOOL	ProcessUpdateFlag		(DWORD dwTick,DWORD dwFlags); 
	virtual float	ProcessAnimation		(DWORD dwTick ,GAMEEVENT *pEvent = NULL);

	virtual void	SetVendorTitle			(LPCTSTR szTitle);

	////////////////////////////////////////////////
	//移动设置
	void				ResetMoveFlag	();
	void				AddMoveFlag		(int MoveFlag);
	void				RemoveMoveFlag	(int MoveFlag);
	BOOL				CheckMoveFlag	(int MoveFlag);


public:
	////////////////////////////////////////////////
	//信息设置
	sPLAYERINFO_BASE*		GetCharInfo();
	const sPLAYERINFO_BASE*		GetCharInfo() const;

	virtual void	SetPlayerInfo			(sPLAYERINFO_RENDER *	pPlayerInfo
                                       ,BOOL							bRefreshRender =TRUE
													,BOOL							bVillageInfo	=FALSE)		;
	virtual void	SetPlayerVillageInfo	(sPLAYERINFO_VILLAGE *	pPlayerInfo
                                       ,BOOL							bRefreshRender = TRUE)		;

	virtual void	GetEquipItemTotalInfo	(sEQUIP_ITEM_INFO & OUT			rItemInfo );
	virtual void	SetEquipItemTotalInfo	(const sEQUIP_ITEM_INFO & IN	rItemInfo );
	virtual void	SetEquipItemBaseInfo		(SLOTPOS total, sITEM_SLOT * IN pSlot );

	virtual void	SetClientPartInfo			(sCHARACTER_CLIENTPART *	pClientPart
														,BOOL								bUpdateEquips=TRUE);

public:
	////////////////////////////////////////////////
	//动作设置
	virtual IDTYPE				GetCurrentAttackAnimation();	
	virtual IDTYPE				GetCurrentIdleAnimation		(){return 0;};
	virtual IDTYPE				GetCurrentMoveAnimation		(){return 0;};
	virtual IDTYPE				GetCurrentDeathAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentSitAnimation		(){return 0;};
	virtual IDTYPE				GetCurrentSittingAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentDamageAnimation(BOOL /*bRight*/){return 0;};

	virtual IDTYPE				GetCurrentLeftTurnAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentRightTurnAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentLeftStepAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentRightStepAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentBackStepAnimation	(){return 0;};
	virtual IDTYPE          GetCurrentBlockAnimation		(){return 0;};

	//瞬移动作
	virtual IDTYPE				GetCurrentSpecialLeftStepAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentSpecialRightStepAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentSpecialFrontStepAnimation	(){return 0;};
	virtual IDTYPE				GetCurrentSpecialBackStepAnimation	(){return 0;};





public:
	virtual void         SetStandAnimationDelay	();
	virtual void			ProcessStandAnimationDelay(DWORD dwTick);

	/////////////////////////////////////////////
	//技能心法操作
	virtual DWORD			GetCurrentAttackStyle		()const;
	virtual BOOL			SetCurrentAttackStyle		(DWORD dwStyle,BOOL bCheck=TRUE);
	virtual void         SetCurrentNomalAttackStyle	(DWORD dwWeaponType);	

	virtual BOOL         CheckAttackStyle				(DWORD dwStyle, BOOL bOutput=FALSE);		
	virtual BOOL         CheckStyleForWeapon			(DWORD dwWeaponType);	
	virtual void         UpdateShotStyle				();



	//事件处理
	//virtual void              OnStartMove();
	virtual void                OnDamage	(DWORD                dwAddtionalEffect
                                          ,BOOL                 bRight
														,eWEAPONSOUNDKIND     eSoundKind
														,eARMOUR_TEX_TYPE eArmourTex
														,DWORD                dwSkillCode);

	///////////////////////////////////////////
	////人物射击操作
	virtual void		OpenCrossbowAnim		(){};
	virtual void		CloseCrossbowAnim		(){};
	virtual void		ShotCrossbowAnim		(){};
	virtual void		LaunchStabberWeapon	(){};

	virtual BOOL		CanStabberAction			();
	virtual void		StabberShot					(DWORD dwAttackSerial);
	virtual IDTYPE		GetStabberShotAnimation	(){return 0;}
	virtual void		ProcessStabberShot		(DWORD dwTick);

	virtual IDTYPE		GetStabberReloadAnimation(){return 0;} 
	virtual int       DecreaseBulletCount			();
	virtual void      StabberReload					( BOOL bAnimation = TRUE );

	virtual int			GetShotCount();

public:
	///////////////////////////////////////////////////////
	// 事件处理
	virtual void   OnUseItem	(DWORD dwItemCode);
	virtual void   OnLevelUp	(DWORD dwLevel
                              ,DWORD dwHP
										,DWORD dwMP
										,DWORD dwRemainStat
										,DWORD dwRemainSkill);
	virtual void   OnRevive		(const Vector3D& vCurPos
                              ,DWORD           dwHP
										,DWORD           dwMP
										,DWORD           dwLevel=-1);

	virtual void   OnFootStep	();
	virtual void   OnShotStart	();
	virtual void   OnShotFire	(int iShotCount);
	virtual void   OnShotEnd	();
	virtual void   OnParalyzed	();



public:
	///////////////////////////////////////////
	//模型相关操作
	virtual float					GetMeshHeight		(float fExtra=0.f);
	virtual DWORD					GetWeaponKind		();
	virtual eARMOUR_TEX_TYPE	GetArmourTexture	();
	virtual sITEMINFO_BASE*    GetWeaponInfo		();


public:
	/////////////////////////////////////////
	//外貌信息
	virtual int                 GetPartVariation(int iPart);
	virtual void                SetPartVariation(int iPart,int iVariation,BOOL bRefresh);

	virtual int                 GetHeightVariation();
	virtual void                SetHeightVariation(int iIndex);


	/////////////////////////////////////////
	//基本属性
	virtual void			SetVObject	(VObject* pVObject);
	virtual void			SetName		(LPCSTR pszName);
	virtual TCHAR *		GetName		() const;

	virtual ePLAYER_TYPE	GetClass		()const;
	virtual DWORD			GetSex		()const;

	virtual MONEY			GetMoney		(void )const;
	virtual void			SetMoney		(MONEY Money);
	virtual BOOL			MinusMoney	(MONEY minus_money );

	virtual void			SetExp		(DWORD dwExp);
	virtual void			SetLevel		(LEVELTYPE LV);
	virtual void			SetHP			(DWORD dwHP );
	virtual void			SetMP			(DWORD dwHP );
	virtual void			SetMaxHP		(DWORD iHP );
	virtual void			SetMaxMP		(DWORD iMP );

	virtual void			SetAttackSpeedRatio	(int iSpeed);
	virtual void			SetMoveSpeedRatio		(int iSpeed);


	virtual LEVELTYPE		GetLevel		()const;
	virtual DWORD			GetExp		()const;
	virtual DWORD			GetHP			()const;
	virtual DWORD			GetMP			()const;
	virtual DWORD			GetMaxHP		()const;
	virtual DWORD			GetMaxMP		()const;
	virtual DWORD			GetNextExp	()const;

	virtual Attributes&			GetAttr();
	virtual const Attributes&	GetAttr() const;

public:
	////////////////////////////////////////////////
	//攻击信息
	virtual CODETYPE		GetCurrentVSkill			();
	virtual float			GetMoveSpeedModifier		()const;
	virtual float			GetAttackSpeedModifier	()const;

	virtual float			GetAttackRange				( void );
	virtual BOOL			IsNormalRangedAttack		();


	////////////////////////////////////////////////
	//技能状态信息
	virtual VOID			LearnSkill					(LPCSTR szSkillName, INT nLevel );
	virtual void			UpdateSkillAttributes	();

	virtual	int			AddSkillEffect		(SKILL_EFFECT *pEffect);
	virtual void			DeleteSkillEffect	(int iEffectID);
	virtual void			UpdateActiveSkillEffectAttr(AttributeAffectorSkillHandle &calc);



	virtual void			ApplyStyleAttributes		(DWORD                        dwStyleCode
                                          ,AttributeAffectorSkillHandle &SkillCalc)		;

	virtual BOOL			CanBeAttackTarget();

public:
	//////////////////////////////////////////////////
	//宠物操作	
	void                AddSummonId		(const DWORD &dwSummonId);
	BOOL                RemoveSummonId	(const DWORD &dwSummonId);
	DWORD               GetSummonId		()const;

	//////////////////////////////////////////////////
	//公会信息操作	
	void                SetGuildInfo (sGUILDINFO_RENDER* pGuildInfo);
	const sGUILDINFO_RENDER* GetGuildInfo()const;

	void                SetVendorInfo (sVENDORINFO_RENDER* pInfo);



public:
#ifndef NONE_MONSTERTESTING
	//////////////////////////////////////////////////
	///不可设Virtual 以下给Hero SendPacket重写
	///本函数只用来配合 GameInTesting
	BOOL					SendPacket			( MSG_OBJECT_BASE* pMsg, DWORD dwSize );
	BOOL					SendPacketAround	( MSG_OBJECT_BASE* pMsg, DWORD dwSize ){return SendPacket			(  pMsg, dwSize );}

#endif



protected:
	////////////////////////////////////////
	//基本信息
	VG_TYPE_PROPERTY			(PlayerType,		ePLAYER_MAKE_TYPE);
	VG_PTR_GET_PROPERTY		(PlayerAttributes,PlayerAttributes);
	VG_PTR_GET_PROPERTY		(VPlayer,			VPlayer);		//VObject指针
	VG_TYPE_PROPERTY			(PlayerCookie,		PlayerCookie);
	//EquipmentContainer *		m_pEquipContainer;
	BaseContainer *			m_pEquipContainer;

	////////////////////////////////////////
	//移动信息
	VG_INT2_PROPERTY			(MoveFlag);	
	VG_BOOL_PROPERTY			(SpecialMoveFlag);	
	VG_INT2_PROPERTY			(OldMoveFlag);
	VG_TYPE_PROPERTY			(OldKeyboardDirection, VECTOR3D);

	VG_TYPE_PROPERTY			(BehaveState, ePLAYER_BEHAVE_STATE);
	VG_TYPE_GET_PROPERTY		(VendorTitle, StringHandle);

	/////////////////////////////////
	//射击信息
	VG_INT2_GET_PROPERTY		(StabberShotDelay);
	VG_INT2_PROPERTY			(BulletCount);
	VG_DWORD_GET_PROPERTY	(StabberShotSerial);
	int							m_iStabberShotDelayTime;
	int							m_iStabberShotCount;
	int							m_iStabberShotRemain;
	float                   m_fStabberShotSpeed;
	BOOL                    m_bStabberReload;
	int                     m_iStabberShotAnimCount;
	int                     m_iStabberShotAnimSet;


	/////////////////////////
	//速度信息
	WORD                    m_wAttSpeedRatio;
	WORD                    m_wMoveSpeedRatio;


	/////////////////////////
	//心法信息
	DWORD							m_dwAttackStyle;
	DWORD							m_dwOldAttackStyle;
	DWORD							m_dwStandAnimDelay;	// 
	VG_BOOL_GET_PROPERTY		(HideHelmet);			//

	VG_BOOL_PROPERTY			(Invincible);			// 
	VG_BOOL_PROPERTY			(Invisible);			// 
	VG_INT2_PROPERTY			(Team);					// 
	VG_BYTE_PROPERTY			(GMGrade);		//
	VG_BOOL_GET_PROPERTY		(ObserverMode);	//
	VG_BOOL_GET_PROPERTY		(ExistHPInfo);        //


	////////////////////////////////////////
	//其它信息
	TArray<DWORD>				m_SummonIdArray;		// 
	sGUILDINFO_RENDER       m_GuildInfo;
	DWORD							m_dwCheckAttackReady;
	int                     m_arPartVariation[PLAYER_VARIATION_MAX];
	int                     m_iHeightVariation;


	///////////////////////////////////////////////
	//测试信息
#ifndef NONE_MONSTERTESTING
	VG_PTR_PROPERTY	(ItemManager, ItemManager);
#endif


	///////////////////////////////////////////////
	//脚本信息



};//Player

OBJECT_FACTORY_DECLARE(Player);

};//namespace object

#include "Player.inl"


#endif //__PLAYER_H__
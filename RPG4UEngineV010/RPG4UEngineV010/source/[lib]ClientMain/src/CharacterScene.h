/*////////////////////////////////////////////////////////////////////////
文 件 名：CharacterScene.h
创建日期：2008年3月25日
最后更新：2008年3月25日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTERSCENE_H__
#define __CHARACTERSCENE_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "scenebase.h"
#include "CharacterSceneDefine.h"
#include "TArray.h"
#include "InputDefine.h"

struct MSG_CG_CONNECTION_ENTERSERVER_ACK;
struct MSG_CG_CHARINFO_DESTROY_ACK;
namespace object
{
class Object;
class Character;
class Player;
};
using namespace object;
using namespace scene;
using namespace input;


class V3DSceneAnimation;
class V3DScene;
class V3DCamera;
class V3DSceneObject;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API CharacterScene : public SceneBase
{
	friend class CharacterScenePacketHandler;
	friend class ConnectionPacketHandler;
public:
	CharacterScene( void );
	virtual ~CharacterScene( void );

public: 
	//父级虚函数实现
	//virtual FWORK_TYPE GetType();

	virtual BOOL Init( void );						    
	virtual void Release( void );					    
	virtual BOOL FrameMove( DWORD dwTick );			    
	virtual BOOL Render( DWORD dwTick );			    

	virtual BOOL OnChangeFrameworkIn();
	virtual BOOL OnChangeFrameworkOut();

	virtual void OnReconnect();
	virtual void Activate( void );					    

protected: 
	BOOL				InitNet		();
	BOOL				InitLocal	();
	void				InitSlotForPositionAndAni();        

	INT				AttachMapData	();

	void				MoveFront		(Player * pChar);
	void				MoveBack			(Player * pChar);

public: 
	void				 OnDisconnect		( );
	void            ParsePacket		( MSG_BASE * pMsg );	 
	void            ProcessKeyInput	( DWORD dwTick );
	void            ProcessMouse		( DWORD dwTick );
	void            ProcessUnits		( DWORD dwTick );
	void            ProcessState		( Player * pPlayer,DWORD dwTick );
	void            ProcessCamera		( const sMOUSE_INFO& info );


	BOOL				 ShowCharacter(float fAlpha,int nSlot=-1);
	void				 SetLockPlayer(BOOL bSet);

	BOOL				 DoCreateCharacter();
	BOOL				 DoEnterWorld();
	BOOL				 DoDeleteCharacter(LPCSTR szVerify);
public: 
	void            AddCharID			( CharListInfo * pCharListInfo );	

	BOOL            CreateCharacter	(const sCHARACTER_CLIENTPART& info);
	BOOL            CreateCharacter	(BYTE           classCode
                                    ,LPCTSTR        szName
												,DWORD          slot
												,CharListInfo * pCharListInfo);

	VOID            DeleteCharacter	(CharListInfo * pCharListInfo);

	void				 SelectCharacter	(CharListInfo* pPlayerInfo); 
	CharListInfo *  GetCurSelectCharacterInfo(); 
	Character*      GetPickedChar		(const RAY& ray);

public: 
	void				 _CheckAutoEnterWorld	();
	void				 _OnGetCharacterList		(MSG_CG_CONNECTION_ENTERSERVER_ACK* pMsg);
	void				 _OnRemoveCharacterBy	(MSG_CG_CHARINFO_DESTROY_ACK* pMsg);

	void				 _SelectCharacter			(Character * pChar,BOOL bNotifyUI=TRUE); 
	void				 _ProcessCharInfoRemove	(Player * pPlayer);
	void				 _OnEnterWorldFromServer(Player* pPlayer);
	void				 _OnEnterWorldAnimEnd	(Player* pPlayer);
	void				 _OnEnterWorldFailed		();
public: 
	BOOL            SendCreateCharacter();	
	BOOL            SendDeleteCharPacket(const char *pString);	
	BOOL            SendCharSelectPacket();

	BOOL            IsSendCharEnterPacket();
	BOOL            IsSendCreateCharPacket();
	BOOL				 IsSendSelectCharPacket();
	BOOL				 IsSendDelCharPacket();

	void				 RemovePacketStatus(DWORD dwStatus);

	BOOL            AddStatus		(DWORD objKey,eCHARACTER_STATUS status); 
	BOOL            RemoveStatus	(DWORD objKey,eCHARACTER_STATUS status);
	BOOL            CheckStatus	(DWORD objKey,eCHARACTER_STATUS status);
	sCHARLIST_OBJSTATUS *  GetStatus		(DWORD objKey);

public:
	//void    RenderTexts( void );					
	int     GetEmptySlot();
	DWORD   GetSelectCharOId();
	inline  BOOL IsHaveSelectCharacter(){return 	m_dwSelectObjID		!= NONE_SELECT;}


public:
	VG_DWORD_PROPERTY		(CurDelObjID);
	VG_TYPE_GET_PROPERTY	(CharList,		CharListInfoList);
	VG_DWORD_PROPERTY		(SelectCharacter);


protected:
	 //V3DScene*				m_pScene;
	 //V3DCamera*				m_pCamera;
	 //V3DSceneObject*		m_pSkyDummy;

private:
	UINT									m_nShowPlayerInfoBak;
	DWORD									m_dwSelectObjID;
	DWORD									m_dwPreSelectOid;

	TArray<sCHARLIST_OBJSTATUS>   m_StatusArray;  
	VG_ARRAY_PROPERTY					(PosInfo,	sCHARLIST_POSINFO,MAX_CHAR_NUM);

	IDTYPE								m_Animations[MAX_CHAR_NUM];
	float									m_fOriginalFarClip;
	BOOL									m_bCreateCharacter;

	float									m_fAngleCurrent;
	float									m_fAngleStep;

	VG_BOOL_GET_PROPERTY				(LockPlayer);	//锁定人物，不允许选择其它

	vector<Player*>					m_arPlayerRemove;	//待移除

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(CharacterScene , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API CharacterScene& singleton::GetCharacterScene();
#define g_CharSelectScene  singleton::GetCharacterScene()
#define theCharacterScene  singleton::GetCharacterScene()

#include "CharacterScene.inl"

#endif //__CHARACTERSCENE_H__
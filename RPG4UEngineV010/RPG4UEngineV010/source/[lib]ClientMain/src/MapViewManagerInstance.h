/*////////////////////////////////////////////////////////////////////////
文 件 名：MapViewManagerInstance.h
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

	MapView与MapSize比例为： 
		水平比 150：128
		竖起比 160：128
	MapView偏移量：	（100,80）
	MapView内容：
		区域	：（100,80）-(699,559)
		宽		：150*4
		高		：160*3
					即容纳：4x3张地图

	小地图有两类：
		1.雷达图
		2.MapView俯瞰图

	MapView按照以上比例转换到MapView之前，须指定相应Map在land偏移量（Tile坐标）


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPVIEWMANAGERINSTANCE_H__
#define __MAPVIEWMANAGERINSTANCE_H__
#pragma once

#include "CommonDefine.h"
#include "MapViewManager.h"
#include "ApplicationBaseListener.h"

using namespace mapinfo;
using namespace std;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API MapViewManagerInstance : public MapViewManager
															, public ApplicationBaseListener
{

public:
	MapViewManagerInstance();
	~MapViewManagerInstance();

public:
	//////////////////////////////////////////////
	//ApplicationBaseListener接口实现
	void OnWinMsg			(HWND   hWnd
								,UINT   uMsg
								,WPARAM wParam
								,LPARAM lParam);

public:
	//////////////////////////////////////////////
	BOOL		Create	(int              nMapOriginW
                     ,int              nMapOriginH
							,sFIELDINFO_BASE* pField);
	void		Destroy	();

	BOOL		FrameMove	(DWORD dwTick);
	BOOL		Render		(DWORD dwTick);

	BOOL		ReadyRender	(int          nMode
                        ,const RECT&  rcWindow
								,const POINT& ptHero
								,int          nSight);

public:
	BOOL		MsgProc	(HWND   hWnd
                     ,UINT   msg
							,WPARAM wParam
							,LPARAM lParam);

	BOOL		LoadSettings	(LPCSTR szFile);

	
	void		Transfrom	( int &nCovX, int &nCovY );
	BOOL		AddMark		( sMAP_MARK* pMark );
	//BOOL		LoadFullMap	( sFIELDINFO_BASE*	m_pFieldInfo);
	BOOL		LoadImage	(int    nImage
                        ,LPCSTR szFileName)		;


	void		Update				();
	BOOL		DrawTextUnderImage(RECT   rcImage
                              ,LPCSTR pszText
										,DWORD  dwColor);

	BOOL		DrawIconImg			(RECT  rcRenderWindow
										,RECT  rcSight
										,int   nImageType
										,float fImageX
										,float fImageY
										,RECT* prcDst=NULL
										,int   nHeroPosX=0
										,int   nHeroPosY=0
										,float fScale=1.0f);

	void	DrawObjectIcon		(Object*	pObject,RECT& rcSight);

	//void		GetImageWindowRect(RECT   rcSight
 //                             ,sMAP_ICONINFO* pImage
	//									,int    nImageX
	//									,int    nImageY
	//									,RECT&  rcDst);

	//BOOL		MarkNpcPosition		(int		nNpcID );
	//BOOL		MarkNpcPositionBy	(LPCSTR  pszName );


	//void		ClearNpcInfo		();
	void		SetNpcInfo			(LPCSTR szName
                              ,float mapX
										,float mapY);

	//POINT	GetPlayerPos		(int mapIdx,POINT MaxMapPos);
	void SetOffPoint			(int idx,POINT pnt);
	void CreateMapButton		(LPCSTR pszFilename1
                           ,LPCSTR pszFilename2
									,RECT   rtArea);
	//void _RenderMapButtons		(void);

	void RealeaseSecondLV	();	//用于释放当前显示的２级大地图

	int CheckWherePlayerIn	(POINT pPlayerPos);
	void SetMaxMapLevel		(BOOL bIsSec){ m_bIsSecondMap = bIsSec; };
	//POINT GetMouseGlobalPos	();
	void SecondMapBtnDown	();
	BOOL DrawTipInfo			(sNPC_COORDINFO* pInfo);
	BOOL GetTargetPosAt		(POINT&	pt,Vector3D& vPos);


protected:
	float  _GetHeroTargetDir		();
	//void	_RenderFullMap			();
	//void	_DrawMiniMapPicture	();
	//void	_DrawNormapNpc			();
	//BOOL	_CalcMiniMapInfo		();

public:

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(MapViewManagerInstance , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API MapViewManagerInstance& singleton::GetMapViewManager();
#define theMapViewManager  singleton::GetMapViewManager()


#endif //__MAPVIEWMANAGERINSTANCE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Item.h
创建日期：2008年3月25日
最后更新：2008年3月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEM_H__
#define __ITEM_H__
#pragma once

#include "CommonDefine.h"
#include "object.h"
#include "StructInPacket.h"
#include <ItemSlot.h>
#include "ObjectFactoryManager.h"

namespace object
{ 
enum eITEM_CATEGORY
{
	 ITEMCATEGORY_MONEY = 0
	,ITEMCATEGORY_ITEM = 1
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API Item : public Object
{
public:
	Item(void);
	virtual ~Item(void);

public:
	virtual BOOL	InitializeStates();
	virtual void	OnInitializeState();

	virtual BOOL	Create(DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void	Destroy( void );

	virtual BOOL	Process					( DWORD dwTick );
	virtual BOOL	ProcessInterpolation	( DWORD dwTick );	

	virtual float	ProcessAnimation		(DWORD       dwTick
                                       ,GAMEEVENT	*pEvent = NULL)		;


public:
	virtual void		SetName		(const char * pszName);
	virtual BOOL		SetPosition	( Vector3D *wvPos);
	
	virtual IDTYPE		GetCurrentDropAnimation();
	virtual IDTYPE		GetCurrentIdleAnimation();
	virtual IDTYPE		GetCurrentOpenAnimation();


	virtual void		Open					();

public:
	/////////////////////////////////////
	//基本信息操作
	void					SetItemInfo	(sITEMINFO_BASE* pItemInfo);
	sITEMINFO_BASE&		GetItemInfo	()	;
	sVITEMINFO_BASE&	GetVItemInfo()	;

	void					SetItem		(ITEMOPT_STREAM & ItemStream);
	BOOL					IsMoney		();
protected:

	VG_BOOL_PROPERTY		(SendPickPacket);

	VG_DWORD_PROPERTY		(OwnerKey);
	VG_DWORD_PROPERTY		(DropMonsterID); 
	VG_BYTE_PROPERTY		(DropType);				// sITEMINFO_RENDER::eFIELDITEMTYPE

	VG_BOOL_PROPERTY		(Hide);
	VG_BOOL_GET_PROPERTY	(Opened);
	DWORD						m_dwOpenDelay;
	DWORD						m_dwLifeTime;	//本地有效

	VG_TYPE_GET_PROPERTY	(Item,	ItemSlot);
	VG_TYPE_PROPERTY		(Money,	MONEY);

	sITEMINFO_BASE			m_ItemInfo;
	sVITEMINFO_BASE			m_VItemInfo;

};//Item

OBJECT_FACTORY_DECLARE(Item);

};//namespace object

#include "Item.inl"


#endif //__ITEM_H__
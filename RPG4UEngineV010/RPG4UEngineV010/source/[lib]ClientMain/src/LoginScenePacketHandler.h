/*////////////////////////////////////////////////////////////////////////
文 件 名：LoginScenePacketHandler.h
创建日期：2007年9月22日
最后更新：2007年9月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	LoginScene中协议消息处理

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __LOGINSCENEPACKETHANDLER_H__
#define __LOGINSCENEPACKETHANDLER_H__
#pragma once

struct MSG_BASE;

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "LoginSceneDefine.h"
#include "LoginScenePacketHandlerDefine.h"
#include "Timer.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class LoginScenePacketHandler
{
	friend class LoginScene;
public:
	LoginScenePacketHandler();
	~LoginScenePacketHandler();

protected:
	enum
	{
		 STATUS_SVRLIST	= 1
		,STATUS_NAMELIST	= 2
		,STATUS_SVRLIST_ALL = STATUS_SVRLIST|STATUS_NAMELIST
	};

protected:
    void Init				();
    void Release			();
	 BOOL FrameMove		(DWORD dwTick); 
	 void OnDisconnect	();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
public:
	PACKETHANDLER_METHOD_AUTH(USER_READY_CMD);
	PACKETHANDLER_METHOD_AUTH(VERSION_CHECK_ACK);
	PACKETHANDLER_METHOD_AUTH(ACCOUNT_LOGIN_ACK);
	PACKETHANDLER_METHOD_AUTH(GATESTATUS_LIST_ACK);
	PACKETHANDLER_METHOD_AUTH(GATENAME_LIST_ACK);
	PACKETHANDLER_METHOD_AUTH(GATE_SELECT_ACK);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
protected:
	void NetworkProc( MSG_BASE * pMsg );

	//版本验证处理
    void  RecvMsg_USER_READY_CMD		( MSG_BASE * pMsg );
    void  SendMsg_VERSION_CHECK_SYN	();
    void  RecvMsg_VERSION_CHECK_ACK	( MSG_BASE * pMsg );

	 //登录验证处理
    BOOL  ExecLoginQuery			(LPCSTR strID, LPCSTR strPW);
    BOOL  AutoExecLoginQuery		();
    BOOL  SendMsg_ACCOUNT_LOGIN_SYN	();
    void  RecvMsg_ACCOUNT_LOGIN_ACK	( MSG_BASE * pMsg );

	 //服务器列表信息处理
    void  SendMsg_GATESTATUS_LIST_SYN	();
    void  RecvMsg_GATESTATUS_LIST_ACK	( MSG_BASE * pMsg );
    void  SendMsg_GATENAME_LIST_SYN	();
    void  RecvMsg_GATENAME_LIST_ACK	( MSG_BASE * pMsg );


	 //选择服务线，准备进入人物列表Scene
	 BOOL  ExecSelectChannel			(const SServerChannelInfo& channelInfo);
    void  SendMsg_GATE_SELECT_SYN	();
    void  RecvMsg_GATE_SELECT_ACK	( MSG_BASE * pMsg );

public:
	const vector<SServerGroupInfo>& GetGroupInfos()	const;

protected:
	void IntegrateChannelStatus	(BYTE* szStatus,UINT uAmount);

protected:
	VG_DWORD_PROPERTY	(Status);
	VG_BOOL_PROPERTY	(AuthFinished);	//
	VG_BOOL_PROPERTY	(TryReconnect);	//
	VG_BOOL_PROPERTY	(NetworkPending);	//
	VG_DWORD_PROPERTY	(AuthUserID);		//验证后返回的UserID
	VG_DWORD_PROPERTY	(EncKey);			//服务器传过来的密钥
	VG_DWORD_PROPERTY	(ServerCount);		//服务器数量
	
	VG_TYPE_PROPERTY	(Account,	StringHandle);		//
	VG_TYPE_PROPERTY	(Password,	StringHandle);		//

	//服务器信息
	VG_BUF_PROPERTY	(SerialKey,			BYTE, MAX_AUTH_SERIAL_LENGTH);	//序列号信息
	
	VG_CSTR_PROPERTY	(AgentServerIP,	MAX_IP_LENGTH);	//待连接的代理服务器IP、端口
	VG_TYPE_PROPERTY	(AgentServerPort,	PORTTYPE);					//
	VG_UINT_PROPERTY	(ServerAmount);						//服务器数量
	VG_UINT_PROPERTY	(GroupAmount);						//服务器数量
	
	VG_INT2_PROPERTY	(SelectedServerIndex);				//选择进入的Agent索引

private:
	vector<SServerGroupInfo>	m_arGroupInfo;
	util::Timer						m_tmUpdateSvrStatus;
	SServerChannelInfo*			m_pAutoSelectChannel;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(LoginScenePacketHandler , _CLIENTMAIN_API);
//extern _CLIENTMAIN_API LoginScenePacketHandler& singleton::GetLoginScenePacketHandler();
#define theLoginScenePacketHandler  singleton::GetLoginScenePacketHandler()


#include "LoginScenePacketHandler.inl"

#endif //__LOGINSCENEPACKETHANDLER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Hero.h
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __HERO_H__
#define __HERO_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "RPGGameDefine.h"
#include "HeroDefine.h"
#include "QuestDefine.h"
#include "ConstSlot.h"
#include "Player.h"


using namespace quest;
typedef  vector<sHERO_QUESTINFO>			HeroQuestInfoArray;
typedef  map<DWORD,sHERO_QUESTINFO>		HeroQuestInfoMap;
typedef  HeroQuestInfoMap::iterator		HeroQuestInfoMapIt;

class AttributeAffectorSkillHandle;
class TempInventoryContainer;
class QuickContainer;

class PlayerAttributes;
class BaseContainer;
class QuickContainer;
class HeroEquipmentContainer;
class StyleContainer;
class SummonSKillContainer;
class SummonSkillContainer;
class InventoryDialog;
class WareHouseDialog;
class SlotUIListener;
//class EventInventoryDialog;

_NAMESPACEU(PathHandler,	tile);

using namespace quest;

namespace object
{ 

class HeroData;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API Hero : public Player 
{
public:
	Hero();
	virtual ~Hero();

private:
	virtual void				OnInitializeState	();
	virtual BOOL				InitContainer		();
	virtual void				ReleaseContainer	();

public:
	virtual void				UpdateToVObject();
	virtual void				UpdateAppearance(BOOL bAddUpdateFlag=TRUE);

	virtual BOOL				Process	( DWORD dwTick );
	virtual BOOL				Create	( DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void				Destroy	();

	virtual int					DoAction							(PLAYER_ACTION *pAction);
	virtual BOOL				CheckQueueActionCondition	();
	virtual BOOL				ProcessNextAction				();

	virtual void				OnDamage		(DWORD                dwAddtionalEffect
                                       ,BOOL                 bRight
													,eWEAPONSOUNDKIND     eSoundKind
													,eARMOUR_TEX_TYPE eArmourTex
													,DWORD                dwSkillCode);


public:
	////////////////////////////////////
	//容器存取操作
	virtual BaseContainer*		GetSlotContainer		( SLOTINDEX slotIdx );
	QuickContainer *				GetQuickContainer		();
	//HeroEquipmentContainer *	GetHeroEquipContainer();
	virtual BOOL					InstallSlotListener	(SLOTINDEX slotIdx, SlotUIListener* pRenderer);

public:
	////////////////////////////////////
	//基本信息操作
	virtual void		SetHeroData				( HeroData& heroData);
	virtual void		SetHeroInfo				( const sHERO_INFO&  pHeroInfo );

	virtual VOID		GetInventoryTotalInfo( sTOTALINFO_INVENTORY & OUT rItemInfo );
	virtual VOID		GetSkillTotalInfo		( sTOTALINFO_SKILL & OUT rSkillInfo );
	virtual VOID		GetQuickTotalInfo		( sTOTALINFO_QUICK & OUT rQuickInfo ); 
	virtual VOID		GetStyleTotalInfo		( sTOTALINFO_STYLE & OUT rStyleInfo ); 

	virtual VOID		SetInventoryTotalInfo(const sTOTALINFO_INVENTORY &	IN rItemInfo );
	virtual VOID		SetSkillTotalInfo		(const sTOTALINFO_SKILL &			IN rSkillInfo );
	virtual VOID		SetQuickTotalInfo		(const sTOTALINFO_QUICK &			IN rQuickInfo );
	virtual VOID		SetStyleTotalInfo		(const sTOTALINFO_STYLE &			IN rStyleInfo );



public:
	//////////////////////////////////////////////
	// 其它操作
	virtual BOOL			CanDoQueueAction	(PLAYER_ACTION *pAction);

	virtual BOOL         IsLockInput();
	virtual float			GetProcessFactor			();
	virtual BOOL			ProcessUpdateFlag			(DWORD dwTick,DWORD dwFlags); 
	virtual void         RestoreAttackStyleOut	();
	virtual void         UpdateStyleAttributeOut	();
	virtual void			RefreshStyleQuick			();
	virtual void			HideHelmet					(BOOL bFlag,BOOL bRefresh = TRUE);



public:
	//////////////////////////////////////////////
	// 任务状态操作监听
	virtual BOOL						RunScript					(DWORD dwData
                                                         ,DWORD npcID
																			,BOOL  bRunNow=FALSE);
	virtual BOOL						RunQuestScript				(CODETYPE questID
                                                         ,DWORD    dwData
																			,BOOL     bRunNow=FALSE);//, DWORD	npcID );
	virtual BOOL						KeepQuestScript			(DWORD dwScriptType,DWORD dwQuestType);
	virtual void						ChangeQuestData			(CODETYPE questID
                                                         ,DWORD    state
																			,DWORD    action);
	virtual BOOL						OnQuestDrop					(MONSTERCODE monsterCode
																			,DWORD		 dwValue
																			,INT			 nUnitIndex);
	virtual void						OnQuestDone					(CODETYPE	 questID);
	// 任务状态操作
	virtual void						SetFirstQuestState		();
	virtual sHERO_QUESTINFO*		GetNextQuestState			();

	virtual sHERO_QUESTINFO*		FindQuestInfoById			( CODETYPE questID );
	//virtual void						FilterQuest					( CODETYPE questID  );
	virtual void						UpdateActiveQuestState	();
	virtual void						ReqQuestInfo				( CODETYPE questID );

	virtual HeroQuestInfoArray&	GetActiveQuestInfo		();



public:
	////////////////////////////////////
	//协议包操作
	virtual BOOL				SendPacket		(void* pBuf
														,int   iLength
														,BOOL  bLocalPacket=FALSE);
	virtual void				StopAtServer	();
	virtual void				AddKeyboardMovePacketSendTime		(DWORD dwTime);

	////////////////////////////////////
	//攻击信息操作
	virtual DWORD					GetCurrentAttackStyle	()const;
	virtual void					UpdateStyleAttributeIn	();
	virtual DWORD					GetWeaponKind				();
	virtual eARMOUR_TEX_TYPE	GetArmourTexture			();



public:
	////////////////////////////////////
	//技能信息操作 及相关检测操作
	virtual VOID			LearnSkill						(LPCSTR szSkillName
                                                   ,INT    nLevel)		;
	virtual void         UpdateSkillAttributes				();
	virtual BOOL         CanUseSkill						(DWORD skillID
                                                   ,BOOL  bShowFailMessage=FALSE)		;

	virtual BOOL			CanUseSkillLimitMP			(DWORD skillID
                                                   ,BOOL  bShowFailMessage=FALSE)		;
	virtual BOOL			CanUseSkillWithoutCooltime	(DWORD skillID
                                                   ,BOOL  bShowFailMessage=FALSE)		;

	virtual BOOL			CanUseSkillLimitLevel		(DWORD skillID
                                                   ,BOOL  bShowFailMessage=FALSE)		;
	virtual BOOL			CanUseSkillLimitWeapon		(DWORD skillID
                                                   ,BOOL  bShowFailMessage=FALSE)		;
//#endif

	////////////////////////////////////
	virtual BOOL         CanUseItem						(DWORD dwWasteType
                                                   ,BOOL  bShowFailMessage=FALSE)		;

	virtual BOOL         CanEquipItem					();

public:
	////////////////////////////////////
	//基本属性操作
	virtual MONEY			GetMoney		( void )const;
	virtual void			SetMoney		(MONEY Money);
	virtual BOOL			MinusMoney	( MONEY minus_money );

	virtual DWORD			GetMaxHP	()const;
	virtual DWORD			GetMaxMP	()const;
	//virtual void         SetHP		(DWORD dwHP);
	virtual void			SetExp	(DWORD dwExp);



	virtual DWORD			GetAttackDelay				()const;
	virtual float        GetAttackSpeedModifier	()const;
	virtual float        GetMoveSpeedModifier		()const;

	virtual void			SetMaxHP( DWORD iHP );
	virtual void			SetMaxMP( DWORD iMP );
	virtual void			SetAttackSpeedRatio	(int iSpeed);
	virtual void			SetMoveSpeedRatio		(int iSpeed);

	virtual void         SetCustomAttackSpeed	(float fSpeed);
	virtual void         SetCustomMoveSpeed	(BOOL bUse,float fSpeed = 1.0f);

	VOID						ProcessPlayerError	(DWORD dwErrCode,DWORD dwCategory);

public:
	////////////////////////////////////
	//技能状态操作
	virtual	int			AddSkillEffect		(SKILL_EFFECT *pEffect);
	virtual void			DeleteSkillEffect	(int iEffectID);
	//被动技能状态属性处理
	virtual void         UpdatePassiveSkillEffectAttributes(AttributeAffectorSkillHandle &calc);


	////////////////////////////////////
	// 人物事件
	virtual void			OnDead				();
	virtual void			OnLevelUp			(DWORD dwLevel, DWORD dwHP, DWORD dwMP, DWORD dwRemainStat,DWORD dwRemainSkill);
	virtual void			OnRevive				(const Vector3D& vCurPos, DWORD dwHP, DWORD dwMP,DWORD dwLevel=-1);
	virtual void			OnFirstEnterWorld	();
	virtual void			OnGameInPlaying	();

	virtual void         OnFootStep();


public:
	/////////////////////////////////////////////////
	//触发器相关操作
	virtual eTRIGGER_STATE				GetAreaTriggerState( int iAreaID  ) ;
	virtual void							SetAreaTriggerState( int iAreaID,eTRIGGER_STATE TriggerState );

	virtual eSHORTCUT_AREA_STATE		GetAreaShortcutState( DWORD dwAreaID  ) ;
	virtual void							SetAreaShortcutState( DWORD dwAreaID, eSHORTCUT_AREA_STATE ShortcutState );

	virtual void                    InitTriggerState();

public:
	/////////////////////////////////////////////////
	//目标相关操作
	virtual DWORD						LockTargetToList	();
	virtual BOOL						TogglePrevTarget	();
	virtual BOOL						ToggleNextTarget	();
	virtual void						LockFirstTarget	();
	virtual void						LockLastTarget		();
	virtual BOOL						ExistTarget			();
	virtual void						ClearTargetList	();
	virtual void						PushTarget			(const sTAB_TARGET_INFO& info);
	virtual BOOL						SortTargetList		();
	virtual void						SetFirstTarget		();
	virtual sTAB_TARGET_INFO*	GetNextTarget		();
	virtual sTAB_TARGET_INFO*	GetTargetAt			(INT nAt);


public:
	DWORD									m_dwPacketStatus;

	/////////////////////////
	//物品及目标信息
	ItemObtainVector					m_vectorGetItem;
	sTAB_TARGET_INFO					m_TabTargetInfo;
	TabTargetInfoVector				m_vectorTargetMonster;
	TabTargetInfoVectorIt			m_TargetMonsterIt;
	BOOL									m_bDeaded;

	////////////////////////////////////////
	//触发器信息
	eTRIGGER_STATE					m_eTriggerState;

	AreaTriggerStateMap			m_TriggerStates;
	AreaShortcutStateMap			m_AreaShorcutStates;

	//////////////////////
	//速度信息
	BOOL                       m_bCustomAttackSpeed;
	float                      m_fCustomAttackSpeed;

	BOOL                       m_bCustomMoveSpeed;
	float                      m_fCustomMoveSpeed;

	VG_INT2_PROPERTY				(HeroTurnState);



	VG_DWORD_PROPERTY				(CurrentTarget);
	VG_BOOL_PROPERTY				(WaitAttackingFromServer);

	////////////////////////////////////////////////////
	//容器信息
	WareHouseDialog *					m_pWareHouseContainer;
	InventoryDialog *				m_pInventoryContainer;
	QuickContainer *				m_pQuickContainer;
	StyleContainer *				m_pStyleContainer;


	VG_PTR_GET_PROPERTY			(SummonContainer,		SummonSkillContainer);
	VG_PTR_GET_PROPERTY			(HeroEquipContainer, HeroEquipmentContainer);
	VG_PTR_GET_PROPERTY			(TempInvenContainer, TempInventoryContainer);


	///////////////////////////////////
	//攻击交互
	VG_INT2_PROPERTY				(ComboCount);


	VG_DWORD_PROPERTY				(KeyboardMovePacketSendDuration);
	VG_DWORD_PROPERTY				(KeyboardMovePacketSendTime);		


	// 任务相关
	HeroQuestInfoMapIt	m_QuestInfoCursor;
	HeroQuestInfoMap		m_QuestInfoMap;		// 所有接触过的任务
	HeroQuestInfoArray	m_vectorActiveQuestInfo;	// 当前进行的任务

};//Class Hero

};//namespace object



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(object::Hero, Hero ,_CLIENTMAIN_API);
#define theHero				singleton::GetHero()
#define ExistTheHero()		singleton::ExistHero()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "Hero.inl"

#endif //__HERO_H__


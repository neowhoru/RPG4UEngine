/*////////////////////////////////////////////////////////////////////////
文 件 名：ChatDefine.h
创建日期：2008年7月8日
最后更新：2008年7月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHATDEFINE_H__
#define __CHATDEFINE_H__
#pragma once


namespace gameui
{

	enum CHAT_TYPE
	{
		 CHAT_TYPE_NORMAL = 0		//普通聊天
		,CHAT_TYPE_GUILD				//公会聊天
		,CHAT_TYPE_TERM				//小队聊天
		,CHAT_TYPE_PRIVATE			//私聊
		,CHANNEL_SYSTEM				//系统公告
		,CHAT_TYPE_BULL				//全服公告--目前只用于gm
		,CHAT_TYPE_SHOUT				//喊话 区域服务器九宫格
		,CHAT_TYPE_HIGHSHOUT			//高喊 区域同全服-- 其实用于玩家
      ,CHAT_TYPE_GM              //GM 玩家向GM发送求助信息
		,AREA_BROADCAST
		,CHAT_TYPE_GM_WHISPER
		,CHAT_TYPE_HELP
		,CHAT_TYPE_SYSTEM
		,CHAT_TYPE_WHISPER	= CHAT_TYPE_PRIVATE
		,PRIVATE_CHAT			= CHAT_TYPE_PRIVATE
	};
};





#endif //__CHATDEFINE_H__
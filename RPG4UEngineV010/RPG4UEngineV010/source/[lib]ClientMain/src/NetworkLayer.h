/*////////////////////////////////////////////////////////////////////////
文 件 名：NetworkLayer.h
创建日期：2006年5月23日
最后更新：2006年5月23日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NETWORKLAYER_H__
#define __NETWORKLAYER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "NetworkDefine.h"
#include "ApplicationBaseListener.h"
#include "INetworkSystem.h"
#include "Timer.h"

struct MSG_BASE;
using namespace util;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API NetworkLayer : public ApplicationBaseListener
{
public:
	NetworkLayer();
	~NetworkLayer();

	//网络模块
public:
	virtual BOOL Init			();
	virtual void Release		();
	virtual BOOL FrameMove	(DWORD dwTick);

public:
	BOOL		InitStartUp		(HINSTANCE hInstance);
	LRESULT	ProcStartUp		(HWND   hDlg
                           ,UINT   message
									,WPARAM wParam
									,LPARAM lParam);


public:
	//从Listen中得来
	virtual void OnWinMsg	(HWND   hWnd
                           ,UINT   uMsg
									,WPARAM wParam
									,LPARAM lParam);

	VOID NetworkRecv			( DWORD dwIndex, BYTE * pData );
	VOID NetworkDisconnect	( DWORD dwIndex );
	VOID NetworkError			( char * pszMsg );

public:
	BOOL SendGamePacket		(void *            pBuf
									,int               iLength
									,Character*        pSender=NULL);
	BOOL SendLoginPacket		(void *            pBuf
									,int               iLength
									,Character*        pSender=NULL);
	BOOL SendChatPacket		(void *            pBuf
									,int               iLength
									,Character*        pSender=NULL);
	BOOL SendPacket			(eCONNECTION_KIND idx
									,void *            pBuf
									,int               iLength
									,Character*        pSender=NULL);

	BOOL ParsePacket			(MSG_BASE* pMsg, DWORD dwIndex = CK_GAMESERVER);

public:

	void CheckReconnectChatServer		(DWORD dwTick);
	BOOL OnReconnectToChatServer		();
	void AfterReconnectToChatServer	();
	void ReconnectToChatServer			();

	BOOL ConnectToLoginServer			();
	BOOL ReconnectToLoginServer		();
	void CheckReconnectLoginServer	(DWORD dwTick);

	void Reconnect();

public:
	///////////////////////////////////
	//net
	static bool ProcDisconnectted			(const bool		bPressYesButton
													,void*			pData)		;
	static bool ProcDisconnecttedLogin	(const bool		bPressYesButton
													,void*			pData)		;

protected:

	BOOL					m_bLoginServerIP;

	VG_BOOL_PROPERTY	(ChangeServer);
	VG_PTR_PROPERTY	(Network, INetworkSystem);
	Timer					m_LoginReconnectTimer;
	Timer					m_ChatReconnectTimer;
	int					m_nChatTryCount;
	VG_BOOL_PROPERTY	(Disconnectting);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(NetworkLayer , _CLIENTMAIN_API);
//extern API_NULL NetworkLayer& theNetworkLayer;
#define theNetworkLayer		singleton::GetNetworkLayer()
#define theNetwork			(*theNetworkLayer.GetNetwork())


#include "NetworkLayer.inl"



#endif //__NETWORKLAYER_H__
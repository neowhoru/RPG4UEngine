/*////////////////////////////////////////////////////////////////////////
文 件 名：MapNPC.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPNPC_H__
#define __MAPNPC_H__
#pragma once


#include "CommonDefine.h"
#include "ObjectFactoryManager.h"
#include "NPC.h"
#include "MapNPCMovementListener.h"




class ContainerDialogBase;
class MapNPCMovement;

namespace object
{ 
class Player;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTMAIN_API MapNPC : public NPC, public MapNPCMovementListener
{
public:
	MapNPC( void );
	virtual ~MapNPC( void );

public:
	virtual BOOL InitializeStates();
	virtual void OnInitializeState();

public:
	virtual BOOL	Create	( DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void	Destroy	( void );
	virtual BOOL	Process	( DWORD dwTick );

public:
	virtual void	OnNextRoutePos();

public:


public:


   virtual void				SetNpcFunc		(eNPC_FUNC_TYPE funcType);
   virtual eNPC_FUNC_TYPE	GetNpcFunc		();
	virtual void				SetFuncInfo		( sNPCINFO_FUNC	* pExtraInfo, BOOL bForceLocal=FALSE);
	sNPCINFO_FUNC *			GetFuncInfo		();


public:
	//////////////////////////////////////
	//摇摆
	virtual void		InitDelaySwingTick	() ;
	virtual void		SetDelaySwingTick		(DWORD dwTick);
	virtual int			ProcessDelaySwingTick(DWORD dwTick);

public:
	//////////////////////////////////////
	//基本属性



public:
	//////////////////////////////////////
	//音效处理
	virtual BOOL      ProcessNPCVoice( DWORD dwTick);
	virtual void      PlayMeetSound	();
	virtual void      PlayWaitSound	();
	virtual void      PlayLeaveSound	();

	//////////////////////////////////////
	//行走AI处理
	virtual void		ProcessNPCMove	(DWORD dwTick);
	virtual void		StopMove			( void );
	//virtual void      SetRoutePos		(int iIndex,Vector3D vPos);
	//virtual VECTOR3D  GetNextRoutePos();    

	//////////////////////////////////////
	//事件处理
	virtual void		ProcessPlayerMeeting	(Object* pPlayer);
	virtual void		OnMeetPlayer			(Object* pPlayer);


	//////////////////////////////////////
	//任务处理
	virtual BOOL					AddRelateQuestInfo			( sQUEST_RELATEINFO* pInfo );
	virtual UINT					GetRelateQuestInfoCount		()		;
	virtual sQUEST_RELATEINFO*	GetRelateQuestInfo			( int n );



	BOOL	  IsLocalNPC			();
	//////////////////////////////////////
protected:
	virtual DWORD					CalDelaySwingTick(DWORD dwTick);



protected:
	//////////////////////////////////////
	//数据信息
	VG_TYPE_PROPERTY	(DialogSlotIdx, SLOTINDEX);

	Object*				m_pPlayerMeet;

	int					m_iDelaySwingTick;

	//////////////////////////////////////
	//基本信息

	sNPCINFO_FUNC		m_FuncInfo;
	MapNPCMovement*	m_pMovement;

	//////////////////////////////////////
	//音效信息
	int               m_iWaitSoundDelay;

	BOOL					m_bEnableIdleSound;
	int               m_iMeetSoundDelay;
	BOOL					m_bReadyPlayMeet;



};//MapNPC

OBJECT_FACTORY_DECLARE(MapNPC);

};//namespace object


#include "MapNPC.inl"


#endif //__MAPNPC_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：User.h
创建日期：2009年4月5日
最后更新：2009年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __USER_H__
#define __USER_H__
#pragma once

#pragma once

#include <CommonDefine.h>
#include <UserDefine.h>
#include <IUserSession.h>


class Zone;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASECHAT_API User : public IUserSession
{
	friend class UserManager;

public:
	User();
	virtual ~User();

public:
	DWORD GetFactoryType(){return GetUserType();}

public:
	virtual BOOL	Init			();
	virtual VOID	Release		();
	virtual VOID	Update		();
	virtual VOID	OnRedirect	() {}

	virtual VOID	DoLogout			();
	virtual VOID	OnEnterZone		( Zone *pZone );
	virtual VOID	OnLeaveZone		( Zone *pZone );
	virtual VOID	OnEnterSector	( Zone *pZone );
	virtual VOID	OnLeaveSector	( Zone *pZone );
	virtual VOID	ProcessLogout	();

public:
	VOID				StartTimeCheck	(DWORD dwDelay=USER_WAITING_DELAY) ;
	BOOL				IsTimeout		()	;

	void				OnLogString		(LPCTSTR szLog);


public:
	int						CheckFriendList		();
	sFRIEND_INFO_BASE*	GetFirstFriendInfo	();
	sFRIEND_INFO_BASE*	GetNextFriendInfo		();
	BOOL						AddFriendInfo			( sFRIEND_INFO_BASE* pFriend );
	sFRIEND_INFO_BASE*	FindFriendInfo			( DWORD dwCharGuid );
	sFRIEND_INFO_BASE*	FindFriendInfo			( LPCTSTR szFriendName );
	VOID						RemoveFriendInfo		( DWORD dwGuid );

	VOID						AddOnlineFriend		( User *pUser );
	VOID						RemoveOnlineFriend	( User *pUser );
	User*						FindOnlineFriend		( DWORD CharGuid );
	VOID						AlarmFriendStatChange( int nFlag, User *pUser );


public:
	//////////////////////////////
	//黑名单
	sBLOCK_INFO_BASE*			GetFirstBlock	();
	sBLOCK_INFO_BASE*			GetNextBlock	();
	BOOL							AddBlockInfo	( sBLOCK_INFO_BASE* pBlock );
	sBLOCK_INFO_BASE*			FindBlock		( DWORD dwGuid );
	sBLOCK_INFO_BASE*			FindBlock		( TCHAR* szBlockName );
	VOID							RemoveBlock		( DWORD dwGuid );
	VOID							RemoveBlock		( TCHAR* szBlockName );

protected:
	virtual VOID			OnAccept		( DWORD dwNetworkIndex );
	virtual VOID			OnDisconnect();
	virtual VOID			OnRecv		( BYTE *pMsg, WORD wSize );


protected:
	VG_TYPE_PROPERTY	(CharName		, StringHandle);
	VG_DWORD_PROPERTY	(NetworkIndex	);
	VG_DWORD_PROPERTY	(GUID				);
	VG_TYPE_PROPERTY	(CharGuid		,IDTYPE);
	VG_TYPE_PROPERTY	(UserType		,eUSER_TYPE);
	VG_TYPE_PROPERTY	(LoginIP			,StringHandle);
	VG_DWORD_PROPERTY	(LoginUserID	);
	VG_DWORD_PROPERTY	(ChannelID		);
	VG_DWORD_PROPERTY	(ZoneKey			);
	VG_TYPE_PROPERTY	(Status			,eZONE_TYPE);
	VG_BOOL_PROPERTY	(EnableWhisper	);
	VG_DWORD_PROPERTY	(FieldCode		);
	VG_DWORD_PROPERTY	(SectorIndex	);

	VG_BOOL_PROPERTY	(BlockList		);
	VG_BOOL_PROPERTY	(GuildList		);
	VG_TYPE_PROPERTY	(GuildGuid		,GUILDGUID);

private:
	BOOL					m_bLogoutDealed;

	DWORD					m_dwTimeoutTick;

	FriendInfoMap			m_FriendInfos;	
	FriendInfoMapIt		m_FriendInfoIt;
	BOOL						m_bCheckFriendList;
	FriendOnlineMap		m_OnlineFriends;

	//////////////////////////////////////
	BlockInfoMap			m_BlockInfos;	
	BlockInfoMapIt			m_BlockInfoIt;
	VG_BOOL_PROPERTY		(FriendChatBlock);
};


#include "User.inl"

#endif //__USER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：UserManager.h
创建日期：2009年4月5日
最后更新：2009年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __USERMANAGER_H__
#define __USERMANAGER_H__
#pragma once

#include "CommonDefine.h"
#include "UserDefine.h"
#include "UserManagerDefine.h"


class User;
class KeyUserList;
class NameUserList;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASECHAT_API UserManager
{
public:
	UserManager(void);
	virtual ~UserManager(void);

public:
	virtual BOOL					Init();
	virtual VOID					Release();

public:
	//实例创建管理
	virtual User*					AllocUser	(USER_TYPE type /* =USER_NULL*/)	;
	virtual void              FreeUser		(User* pUser)	;

public:
	virtual BOOL					SetUserCharGuid	( DWORD dwGuid, DWORD dwCharGuid )=0;
	virtual BOOL					SetUserCharName	( DWORD dwGuid, LPCSTR strCharName )=0;
	virtual BOOL					UnSetCharGuid		( DWORD dwGuid )=0;
	virtual BOOL					UnsetUserCharName	( DWORD dwGuid )=0;

	virtual User*					FindUser				( DWORD dwGuid )=0;
	virtual User*					FindUser				( LPCSTR strCharName )=0;
	virtual User*					FindUserBy			( DWORD CharGuid )=0;
	virtual VOID					SendToAll			( BYTE *pMsg, WORD wSize, DWORD dwExceptUserID=0 )=0;

	virtual DWORD					GetUserAmount		()=0;
	virtual DWORD					GetNameUserAmount()=0;

	////////////////////////////////////////
	virtual BOOL					AddNewFriend	( DWORD FriendCharGuid, User *pUser );	
	virtual VOID					RemoveNewFriend( DWORD FriendCharGuid, User *pUser );
	virtual BOOL					AddFriend		( User* pUser );
	virtual VOID					RemoveFriend	( User* pUser );
	virtual VOID					ClearFriends	();

	virtual VOID					AddUser		( DWORD dwGuid, User *pUser )=0;
	virtual VOID					RemoveUser	( DWORD dwGuid )=0;


protected:
	KeyUserList			*m_pUserList;
	KeyUserList			*m_pKeyUserList;
	NameUserList		*m_pNameUserList;

private:
	FriendAtomMapMap			m_UserFriends;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(UserManager , _SERVERBASECHAT_API );
//extern API_NULL UserManager& singleton::GetUserManager();
#define theUserManager  singleton::GetUserManager()


#endif //__USERMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Channel.h
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHANNEL_H__
#define __CHANNEL_H__
#pragma once


#include <CommonDefine.h>
#include <KeyGenerator.h>
#include <ChannelDefine.h>
#include <ZoneDefine.h>
#include <ServerInfoDefine.h>

using namespace util;

class ViewPortWork;

#define	MAX_ZONE_TYPE		(ZONETYPE_MAX+1)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASECHAT_API Channel
{
public:
	Channel(void);
	virtual ~Channel(void);

public:
	virtual DWORD	GetFactoryType(){return CHANNEL_TYPE_DEFAULT;}

public:
	virtual BOOL		Init		()=0;
	virtual VOID		Release	()=0;
	virtual VOID		Update	()=0;

public:
	virtual LPCTSTR	GetChannelName	()=0;

public:
	virtual void		RemoveZones		();

	virtual Zone*		_AddZone			( DWORD dwZoneKey, eZONE_TYPE	type );
	virtual BOOL		_RemoveZone		( DWORD dwZoneKey, eZONE_TYPE	type );
	virtual Zone*		_FindZone		( DWORD dwZoneKey, eZONE_TYPE	type );
	virtual BOOL		_UserEnterZone	( User *pUser, DWORD dwNewZoneKey, eZONE_TYPE	type );
	virtual BOOL		_UserLeaveZone	( User *pUser );
	virtual BOOL		_SendToZones	( BYTE *pMsg, WORD wSize, eZONE_TYPE	type, DWORD dwExceptUserID=0 )=0;
	virtual BOOL		_IncZoneUserNum( eZONE_TYPE	type)=0;
	virtual BOOL		_DecZoneUserNum( eZONE_TYPE	type)=0;

public:
	virtual Zone*		FindZone			(eZONE_TYPE eZoneType
                                    ,DWORD      dwZoneKey)		=0;

	virtual VOID		AddUser				(User*      pUser
                                       ,eZONE_TYPE userStatus
													,DWORD      dwZoneKey)=0;
	virtual VOID		RemoveUser			(User *pUser )=0;
	virtual VOID		RemoveUsers			();
	virtual BOOL		AddViewPortUser	(User *pUser, DWORD dwField, DWORD dwSector )=0;
	virtual BOOL		MoveViewPortUser	(User *pUser, DWORD dwField, DWORD dwSector )=0;
	virtual VOID		RemoveViewPortUser(User *pUser )=0;

	virtual VOID		UserLeavePrevZone	(User *pUser )=0;
	virtual VOID		UserEnterCharScene(User *pUser )=0;

	virtual VOID		SendToAll			(BYTE *pMsg, WORD wSize, DWORD dwExceptUserID=0 )=0;
	virtual BOOL		SendToOneVillage	(DWORD dwFieldCode
                                       ,BYTE* pMsg
													,WORD  wSize, DWORD dwExceptUserID=0)=0;
	virtual BOOL		SendToViewPort		(User* pUser
                                       ,BYTE* pMsg
													,WORD  wSize, DWORD dwExceptUserID=0)=0;

	virtual VOID		DisplayInfo			()=0;


public:
	/////////////////////////////////////////////////
	CHANNGE_DECL2		(Village		,ZONETYPE_VILLAGE		);
	CHANNGE_DECL2		(Lobby		,ZONETYPE_LOBBY		);
	//CHANNGE_DECL		(CharSelect	,ZONETYPE_CHARSELECT	);
	CHANNGE_DECL		(Mission		,ZONETYPE_MISSION		);
	CHANNGE_DECL		(Hunting		,ZONETYPE_HUNTING		);
	CHANNGE_DECL		(Quest		,ZONETYPE_QUEST		);
	CHANNGE_DECL		(PVP			,ZONETYPE_PVP			);
	CHANNGE_DECL		(Event		,ZONETYPE_GUILD		);
	CHANNGE_DECL		(Guild		,ZONETYPE_SIEGE		);
	CHANNGE_DECL		(Siege		,ZONETYPE_EVENT		);
	CHANNGE_DECL2		(FriendChat	,ZONETYPE_MAX			);

	///////////////////////////////////////////////

	virtual DWORD					GetTotalUserNum() ;
	virtual KEYTYPE				AllocKey			()						;
	virtual VOID					FreeKey			( KEYTYPE Key )			;

public:
	template<class OPR>	void	ForEachZone	(eZONE_TYPE	type,OPR& opr);
	template<class OPR>	void	ForEachUser	(OPR& opr);

	void	_AddUser		(DWORD dwKey, User* pUser);
	void	_EraseUser	(DWORD dwKey);

protected:
	VG_DWORD_PROPERTY	(ChannelID);
	VG_PTR_PROPERTY	(GroupInfo, sSERVER_GROUPC);

	Zone*					m_pCharSceneZone;
	KeyGenerator		m_RoomKeyGenerator;
	ViewPortWork*	m_pViewPortWork;
	DWORD					m_arUserNums[MAX_ZONE_TYPE];

private:
	UserHashMap			m_mapUsers;
	ZoneHashMap			m_arZoneMaps[MAX_ZONE_TYPE];




};


#include "Channel.inl"

#endif //__CHANNEL_H__
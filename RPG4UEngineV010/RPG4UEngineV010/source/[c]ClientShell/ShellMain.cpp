// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ClientMain.h"
#include "MathLib.h"
#include "EncodeMD5.h"


#define NONE_TEST
#include "ShellMainTest.inc.h"


INT WINAPI WinMain( HINSTANCE hInst, HINSTANCE hPrevInst, LPSTR lpCmd, INT iShow )
{
	//StringHandle	sText=EncodeMD5::GetMD5("rpg4u");

#ifndef NONE_TEST
	TestMain();
	return 0;
#endif

	return gamemain::GameStartUp(hInst,  hPrevInst,  lpCmd,  iShow);
}



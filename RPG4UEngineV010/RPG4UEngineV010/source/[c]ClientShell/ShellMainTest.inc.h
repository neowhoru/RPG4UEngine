#include "MathLib.h"

/*////////////////////////////////////////////////////////////////////////
MathLib
V3DGraphic
V3DMeshLib
GameMapLib
VR3D
ClientGameLib
ClientMain

SkillSpecialEffect
ClientUI
D3DRenderSystem
WorldBuilder
MdlStudio


V3DFrustum

D3DXVECTOR2
D3DXVECTOR3
D3DXVECTOR4
D3DXPLANE
D3DXMATRIX
D3DMATERIAL8
D3DLOCKED_RECT
D3DLIGHT8
/*////////////////////////////////////////////////////////////////////////


/*////////////////////////////////////////////////////////////////////////
D3DXVec2TransformCoord
.D3DXVec3Length
.D3DXVec3Cross
.D3DXVec3Length
.D3DXVec3Dot
.D3DXVec3Normalize
.D3DXVec3TransformCoord
D3DXVec3Transform
D3DXVec3TransformNormal

.D3DXSphereBoundProbe
.D3DXBoxBoundProbe
.D3DXPlaneFromPoints
.D3DXIntersectTri

.D3DXQuaternionNormalize
.D3DXQuaternionRotationMatrix
.D3DXQuaternionMultiply
.D3DXQuaternionRotationAxis
.D3DXQuaternionNormalize


.D3DXMatrixMultiply
.D3DXMatrixInverse
.D3DXMatrixIdentity
.D3DXMatrixLookAtLH
.D3DXMatrixRotationQuaternion
.D3DXMatrixTranslation
.D3DXMatrixLookAtRH
.D3DXMatrixPerspectiveFovLH
.D3DXMatrixPerspectiveFovRH
.D3DXMatrixOrthoOffCenterLH
.D3DXMatrixOrthoOffCenterRH
.D3DXMatrixRotationAxis
.D3DXMatrixRotationX
.D3DXMatrixTranspose
.D3DXMatrixScaling
.D3DXMatrixOrthoLH
.D3DXMatrixOrthoRH


D3DXCreateTexture
D3DXCreateTextureFromFileInMemory
D3DXCreateTextureFromFileInMemoryEx
D3DXLoadMeshFromX

/*////////////////////////////////////////////////////////////////////////

#ifndef NONE_TEST

//////////////////////////////
//namespace testing
//{
Vector3D		vec	(123,123,123);
Vector3D		vec2	(12,-12,12);
D3DXVECTOR3	dv		(123,123,123);
D3DXVECTOR3	dv2	(12,-12,12);

//////////////////////////////
D3DXMATRIX		dm	(10,100,10,100
						,200,-100,-100,100
						,20,-10,-100,10
						,100,10,100,100);
D3DXMATRIX		dm2(12,122,-12,122
						,222,122,-12,122
						,22,-12,-122,12
						,-122,12,122,122);
Matrix4			vm	(dm);
Matrix4			vm2(dm2);

Quaternion		q	(12,3,-5,1);
D3DXQUATERNION	dq	(12,3,-5,1);

//////////////////////////////
float				r4[10];
float				dr[10];
Vector3D			vcross, vmv, vnorm;
D3DXVECTOR3		dcross, dmv, dnorm;
D3DXMATRIX		dmout;
Matrix4			vmout;
Matrix4			vmout2;
Quaternion		qout;
D3DXQUATERNION	dqount;


VOID rotate(float cx, float cy, FLOAT alpha, float &x, float &y ) 
{ 
	D3DXVECTOR2 v( x-cx, y-cy );
	D3DXMATRIX m;
	D3DXMatrixRotationZ( &m, -alpha );
	D3DXVec2TransformCoord( &v, &v, &m );

	x = v.x+cx;
	y = v.y+cy;
} 


//////////////////////////////////////////////////
void TestUtil()
{
	assert(!"test rotate D3DXVec2TransformCoord");
	//Matrix4::TransformCoord

	//////////////////////////////////////////////////
	//Vector3D	v0(3,5,1),v1(7,25,2),v2(3,-6,1);
	Vector3D	v0(-2,1,5),v1(1,-2,1),v2(2,2,1);
	float		ret(0);
	float		dret(0),u,v;
	BOOL db;
	BOOL b;

	Ray ray;
	ray.m_Origin(0,0,-1);
	ray.m_Direction(0,0,1);

	b = ray.IntersectTriangle(v0,v1,v2,&ret,FALSE);

	db = D3DXIntersectTri((D3DXVECTOR3*)&v0
                   ,(D3DXVECTOR3*)&v1
						 ,(D3DXVECTOR3*)&v2
						 ,(D3DXVECTOR3*)&ray.m_Origin
						 ,(D3DXVECTOR3*)&ray.m_Direction
						 ,&u
						 ,&v
						 ,&dret);

	//////////////////////////////////////////////////
	D3DXPLANE dp;
	Plane		 p;

	p.Set(v0,v1,v2);
	D3DXPlaneFromPoints(&dp
                      ,(D3DXVECTOR3*)&v0
							 ,(D3DXVECTOR3*)&v1
							 ,(D3DXVECTOR3*)&v2);


	//////////////////////////////////////////////////
	db = D3DXSphereBoundProbe((D3DXVECTOR3*)&v0
									,10
									,(D3DXVECTOR3*)&ray.m_Origin
									,(D3DXVECTOR3*)&ray.m_Direction);

	Sphere sphere(v0,10);
	b = ray.Intersect(sphere,&ret);
	//sphere.Intersect();
//D3DXBoxBoundProbe

}

//////////////////////////////////////////////////
void TestQuaternaion()
{
	/////////////////////
	vmout = vm;
	vmout.Rotation(Vector3D(-22,67,12),math::cPI/5.3f);
	dmout = dm;
	D3DXMatrixRotationAxis(&dmout,&D3DXVECTOR3(-22,67,12),math::cPI/5.3f);

	q.FromMatrix(vmout);
	D3DXQuaternionRotationMatrix(&dq,&dmout);


	///////////////////////////////////////
	qout.SetRotation(Vector3D(-22,67,12),math::cPI/5.3f);
	D3DXQuaternionRotationAxis(&dqount,&D3DXVECTOR3(-22,67,12),math::cPI/5.3f);


	///////////////////////////////////////
	qout.SetRotation(Vector3D::UNIT_X,math::cPI/5.3f);
	qout.SetRotationViaMatrix(Vector3D::UNIT_X,math::cPI/5.3f);
	qout.SetRotationX(math::cPI/5.3f);


	///////////////////////////////////////
	qout.SetRotationViaMatrix(Vector3D(-22,67,12),math::cPI/5.3f);
	D3DXQuaternionRotationAxis(&dqount,&D3DXVECTOR3(-22,67,12),math::cPI/5.3f);


	///////////////////////////////////////
	qout = q;
	qout.Normalize();
	D3DXQuaternionNormalize(&dqount,&dq);


	///////////////////////////////////////
	qout.Multiply(Quaternion(1,2,3,4),q);
	D3DXQuaternionMultiply(&dqount,&D3DXQUATERNION(1,2,3,4),&dq);

	///////////////////////////////////////

}

//////////////////////////////////////////////////
void  TestMatrix()
{
	/////////////////////
	vmout.Multiply(vm,vm2);
	D3DXMatrixMultiply(&dmout,&dm,&dm2);

	/////////////////////
	vmout = vm;
	vmout.Inverse();
	vmout2 = vm;
	vmout2.Inverse2();
	D3DXMatrixInverse(&dmout,NULL,&dm);

	/////////////////////
	vmout = vm;
	vmout.Identity();
	dmout = dm;
	D3DXMatrixIdentity(&dmout);
	

	/////////////////////
	//vmout.TranslationTo(Vector3D(22,0,12));
	vmout.LookAtLH(Vector3D(22,0,12),Vector3D(2,-3,-1), Vector3D::UNIT_Z);
	D3DXMatrixLookAtLH(&dmout,&D3DXVECTOR3(22,0,12),&D3DXVECTOR3(2,-3,-1),&D3DXVECTOR3(0,0,1));

	/////////////////////
	//vmout.TranslationTo(Vector3D(22,0,12));
	vmout.LookAtRH(Vector3D(22,0,12),Vector3D(2,-3,-1), Vector3D::UNIT_Z);
	D3DXMatrixLookAtRH(&dmout,&D3DXVECTOR3(22,0,12),&D3DXVECTOR3(2,-3,-1),&D3DXVECTOR3(0,0,1));


	/////////////////////
	q.ToMatrix(vmout);
	D3DXMatrixRotationQuaternion(&dmout,&dq);

	/////////////////////
	vmout = vm;
	vmout.Translation(Vector3D(123,123,123),TRUE);
	dmout = dm;
	D3DXMatrixTranslation(&dmout,123,123,123);

	/////////////////////
	vmout.Scaling(Vector3D(2,5,6),TRUE);
	D3DXMatrixScaling(&dmout,2,5,6);


	/////////////////////
	vmout.PerspFovLH(30,1.333f,3,4);
	D3DXMatrixPerspectiveFovLH(&dmout,30,1.333f,3,4);

	vmout.PerspFovRH(30,1.333f,3,4);
	D3DXMatrixPerspectiveFovRH(&dmout,30,1.333f,3,4);


	/////////////////////
	vmout = vm;
	//vmout.Identity();
	vmout.Rotation(Vector3D(-22,67,12),math::cPI/5.3f);
	vmout.RotationViaQuaternion(Vector3D(-22,67,12),math::cPI/5.3f);
	//vmout2 = vm;
	//vmout2.Rotation2(Vector3D(22,0,12),math::cPI/3.3f);
	dmout = dm;
	D3DXMatrixRotationAxis(&dmout,&D3DXVECTOR3(-22,67,12),math::cPI/5.3f);

	/////////////////////
	vmout.RotationX(math::cPI/5.3f, TRUE);
	D3DXMatrixRotationX(&dmout,math::cPI/5.3f);
	vmout.RotationY(math::cPI/5.3f, TRUE);
	D3DXMatrixRotationY(&dmout,math::cPI/5.3f);
	vmout.RotationZ(math::cPI/5.3f, TRUE);
	D3DXMatrixRotationZ(&dmout,math::cPI/5.3f);

	/////////////////////
	vmout = vm;
	vmout.Transpose();
	D3DXMatrixTranspose(&dmout,&dm);

	/////////////////////
	vmout.OrthoLH(4,6,5,6);
	D3DXMatrixOrthoLH(&dmout,4,6,5,6);
	vmout.OrthoRH(4,6,5,6);
	D3DXMatrixOrthoRH(&dmout,4,6,5,6);

	/////////////////////
	vmout.OrthoOffCenterLH(1,2,3,4,5,6);
	D3DXMatrixOrthoOffCenterLH(&dmout,1,2,4,3,5,6);
	vmout.OrthoOffCenterRH(1,2,3,4,5,6);
	D3DXMatrixOrthoOffCenterRH(&dmout,1,2,4,3,5,6);

}


//////////////////////////////////////////////////
void  TestVec3()
{

	/////////////////////
	r4[0] = vec.Length();
	dr[0] = D3DXVec3Length(&dv);

	/////////////////////
	vcross = vec.CrossProduct(vec2);
	D3DXVec3Cross(&dcross,&dv,&dv2);

	/////////////////////
	r4[1] = vec.DotProduct(vec2);
	dr[1] = D3DXVec3Dot(&dv,&dv2);

	/////////////////////
	vmv = vm.TransformCoord(vec);
	D3DXVec3TransformCoord(&dmv,&dv,&dm);

	/////////////////////
	vnorm(vec);
	vnorm.Normalize();
	D3DXVec3Normalize(&dnorm,&dv);

	//vec.Multiply();

}
//};

//using namespace testing;

void TestMain()
{
	TestUtil();
	TestQuaternaion();
	TestMatrix();
	TestVec3();
}

#endif



/*////////////////////////////////////////////////////////////////////////
文 件 名：TRingQueue.h
创建日期：2008年5月9日
最后更新：2008年5月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TRINGQUEUE_H__
#define __TRINGQUEUE_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
	循环队列
/*////////////////////////////////////////////////////////////////////////
template<typename T>
class TRingQueue
{
public:
	TRingQueue() 
		: m_pData( NULL )
		, m_nLength( 0 )
		, m_nSize( 0 )
		, m_nHead( 0 )
		, m_nTail( 0 )
	{
		//InitializeCriticalSection( &m_cs );		
	}
	
	virtual ~TRingQueue()
	{
		assert(m_pData == NULL && "确保已经手工调用Destroy\n");
		Destroy();
		//DeleteCriticalSection( &m_cs );
	}


	virtual BOOL Lock()	{return TRUE;}
	virtual void Unlock(){;}

	
	/*////////////////////////////////////////////////////////////////////////
		@remarks
				创建队列缓冲空间
		@param	nSize
				缓冲大小
		@param	nExtraSize
				附加缓冲大小
	/*////////////////////////////////////////////////////////////////////////
	BOOL Create( int nSize, int nExtraSize = 0 )
	{
		//EnterCriticalSection( &m_cs );
		if(!Lock())
			return FALSE;
		
		SAFE_DELETE_ARRAY(m_pData);

		m_pData			= new T[nSize + nExtraSize];
		m_nSize			= nSize;
		m_nExtraSize	= nExtraSize;
		//LeaveCriticalSection( &m_cs );

		Unlock();

		return TRUE;
	}

	BOOL Destroy()
	{
		if(!Lock())
			return FALSE;

		SAFE_DELETE_ARRAY(m_pData);

		Unlock();
		return TRUE;
	}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline void Clear()
	{
		if(!Lock())
			return;
		//EnterCriticalSection( &m_cs );

		m_nLength       = 0;
		m_nHead         = 0;
		m_nTail         = 0;

		//LeaveCriticalSection( &m_cs );
		Unlock();
	}
	
	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline int      GetSpace()
	{
		int iRet;

		if(!Lock())
			return 0;
		//EnterCriticalSection( &m_cs );
		iRet = m_nSize - m_nLength;		
		//LeaveCriticalSection( &m_cs );

		Unlock();
		return iRet;
	}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline int      GetLength()
	{
		int iRet;

		if(!Lock())
			return FALSE;
		//EnterCriticalSection( &m_cs );
		iRet = m_nLength;
		//LeaveCriticalSection( &m_cs );
		Unlock();

		return iRet;
	}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline int      GetBackDataCount()
	{
		int iRet;

		if(!Lock())
			return 0;
		//EnterCriticalSection( &m_cs );
		iRet = m_nSize - m_nHead;
		//LeaveCriticalSection( &m_cs );

		Unlock();
		return iRet;
	}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline T*       GetReadPtr()
	{
		T *pRet;

		if(!Lock())
			return NULL;
		//EnterCriticalSection( &m_cs );
		pRet = m_pData + m_nHead;

		if( m_nHead > m_nTail && m_nExtraSize > 0 )
		{
			int nSplitFirstDataCount;
			nSplitFirstDataCount = m_nSize - m_nHead ;
			if(nSplitFirstDataCount  < m_nExtraSize )
			{
				memcpy( m_pData + m_nSize
						, m_pData
						, sizeof(T) * ( m_nExtraSize - nSplitFirstDataCount ) );
			}
		}

		//LeaveCriticalSection( &m_cs );

		Unlock();
		return pRet;
	}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline T*       GetWritePtr()
	{
		T *pRet;

		if(!Lock())
			return NULL;
		//EnterCriticalSection( &m_cs );
		pRet = m_pData + m_nTail;
		//LeaveCriticalSection( &m_cs );

		Unlock();
		return pRet;
	}
	
	inline T*		 Front(){return GetReadPtr();}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline int GetReadableLen()
	{
		int iRet;

		if(!Lock())
			return 0;
		//EnterCriticalSection( &m_cs );
		if( m_nHead == m_nTail )		iRet = GetLength() > 0 ? m_nSize - m_nHead: 0;
		else if( m_nHead < m_nTail )	iRet = m_nTail - m_nHead;
		else							 iRet = m_nSize - m_nHead;
		//LeaveCriticalSection( &m_cs );

		Unlock();
		return iRet;
	}
	
	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	inline int GetWritableLen()
	{
		int iRet;

		if(!Lock())
			return 0;
		//EnterCriticalSection( &m_cs );
		if( m_nHead == m_nTail )		iRet = GetLength() > 0 ? 0 : m_nSize - m_nTail;
		else if( m_nHead < m_nTail )	iRet = m_nSize - m_nTail;
		else							iRet = m_nHead - m_nTail;
		//LeaveCriticalSection( &m_cs );

		Unlock();
		return iRet;
	}

	inline BOOL PushBack( T *pSrc, int nSize=1 )
	{
		return Enqueue( pSrc,  nSize );
	}

	/*////////////////////////////////////////////////////////////////////////
		@remarks
				在队列尾处增加内容
		@param	pSrc
				数据源
		@param	nSize
				数据大小
		@retval	BOOL
				增加失败返回 FALSE ,否则成功
	/*////////////////////////////////////////////////////////////////////////
	inline BOOL Enqueue( T *pSrc, int nSize=1 )
	{
		//EnterCriticalSection( &m_cs );

		if( GetSpace() < nSize )
		{
			//Unlock();
			//LeaveCriticalSection( &m_cs );
			return FALSE;
		}

		if(!Lock())
			return FALSE;

		/*
		BOOL bCopyToExtraBuffer = ( ( m_nHead <= m_nTail ) && ( m_nSize - m_nTail < nSize ) );
		*/

		if( pSrc )
		{
			if( m_nHead <= m_nTail )
			{
				// 1. head  tail
				int nBackSpaceCount = m_nSize - m_nTail;

				if( nBackSpaceCount >= nSize )  
				{
					memcpy( m_pData + m_nTail, pSrc, sizeof(T) * nSize );
				}
				else
				{
					// 分两部分复制数据
					memcpy( m_pData + m_nTail, pSrc, sizeof(T) * nBackSpaceCount );
					memcpy( m_pData, pSrc + nBackSpaceCount, sizeof(T) * ( nSize - nBackSpaceCount ) );
				}
			}
			else
			{
				// 2. head tail
				memcpy( m_pData + m_nTail, pSrc, sizeof(T) * nSize );
			}
		}

		/*
		if( bCopyToExtraBuffer )
		{
			memcpy( m_pData + m_nSize, pSrc + m_nSize - m_nTail, sizeof(T) * ( nSize - ( m_nSize - m_nTail ) ) );
		}
		*/

		m_nTail		+= nSize;
		m_nTail		%= m_nSize;
		m_nLength	+= nSize;

		Unlock();
		//LeaveCriticalSection( &m_cs );

		return TRUE;
	}

	inline BOOL PopFront( T *pTar, int nSize=1 )
	{
		return Dequeue( pTar,  nSize );
	}

	/*////////////////////////////////////////////////////////////////////////
	@remarks
			从队列头部取数据
	@param	pTar
			目标缓冲区
	@param	nSize
			取数据大小
	@retval	BOOL
			成功返回TRUE
	/*////////////////////////////////////////////////////////////////////////
	inline BOOL Dequeue( T *pTar, int nSize=1 )
	{    
		//EnterCriticalSection( &m_cs );

		if( !Peek( pTar, nSize ) )
		{
			//Unlock();
			//LeaveCriticalSection( &m_cs );
			return FALSE;
		}

		if(!Lock())
			return FALSE;

		m_nHead		+= nSize;
		m_nHead		%= m_nSize;
		m_nLength	-= nSize;

		//LeaveCriticalSection( &m_cs );
		Unlock();

		return TRUE;
	}

	/*////////////////////////////////////////////////////////////////////////
	@remarks
			从队列头部取数据
	@param	pTar
			目标缓冲区
	@param	nSize
			取数据大小
	@retval	BOOL
			成功返回TRUE
	/*////////////////////////////////////////////////////////////////////////
	inline BOOL Peek( T *pTar, int nSize=1 )
	{
		if(!Lock())
			return FALSE;
		//EnterCriticalSection( &m_cs );

		if( m_nLength < nSize )
		{
			Unlock();
			//LeaveCriticalSection( &m_cs );
			return FALSE;
		}

		// 郴侩阑 罐阑 器牢磐啊 NULL捞 酒囱 版快俊父 皋葛府甫 汗荤茄促.
		if( pTar != NULL )
		{
			if( m_nHead < m_nTail )
			{
				memcpy( pTar, m_pData + m_nHead, sizeof(T) * nSize );
			}
			else
			{
				if( GetBackDataCount() >= nSize )
				{
					memcpy( pTar, m_pData + m_nHead, sizeof(T) * nSize );                           
				}
				else
				{
					memcpy( pTar
							, m_pData + m_nHead
							, sizeof(T) * GetBackDataCount() );

					memcpy( pTar + GetBackDataCount()
							, m_pData
							, sizeof(T) * ( nSize - GetBackDataCount() ) );
				}
			}
		}

		Unlock();
		//LeaveCriticalSection( &m_cs );

		return TRUE;
	}

//=============================================================================================================================
/**
	@remarks
			把数据复制到附加缓冲
	@param	nSize
			数据大小
*/
//=============================================================================================================================
	inline BOOL CopyHeadDataToExtraBuffer( int nSize )
	{
		assert( nSize <= m_nExtraSize );

		if(!Lock())
			return FALSE;
		//EnterCriticalSection( &m_cs );

		memcpy( m_pData + m_nSize, m_pData, nSize );

		//LeaveCriticalSection( &m_cs );
		Unlock();
		return TRUE;
	}

protected:
	T					*	m_pData;			
	int					m_nLength;		
	int					m_nSize;			
	int					m_nHead;			
	int					m_nTail;			
	int					m_nExtraSize;	
};




/*////////////////////////////////////////////////////////////////////////
	实现安全访问...多线程操作
/*////////////////////////////////////////////////////////////////////////
template<class T>
class CircuitQueueSafe : public TRingQueue<T>
{
public:
	CircuitQueueSafe()
	{
		InitializeCriticalSection( &m_cs );		
	}
	virtual ~CircuitQueueSafe()
	{
		DeleteCriticalSection( &m_cs );
	}

	virtual BOOL Lock()	{EnterCriticalSection( &m_cs );return TRUE;}
	virtual void Unlock(){LeaveCriticalSection( &m_cs );}

protected:
	CRITICAL_SECTION	m_cs;
};




#endif //__TRINGQUEUE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TRoundBuffer.h
创建日期：2007年10月14日
最后更新：2007年10月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TROUNDBUFFER_H__
#define __TROUNDBUFFER_H__
#pragma once

//循环buf

template< const DWORD t_dwSize>
class TRoundBuffer
{
	enum enumConst
	{
		const_iBlockMaxSize = 1024
	};

	struct SBlockHeader
	{
		//块大小
		unsigned short stBlockSize;
	};
	char m_szBuf[ t_dwSize ];
	DWORD	m_dwStartPos,m_dwEndPos;
	
	int		m_iBlockCount;
public:
	TRoundBuffer(void)
	{
		Clear();
	}

	~TRoundBuffer(void)
	{
	}

	int GetSize(void)
	{
		return m_iBlockCount;
	}

	bool CanAddBlock( unsigned short stSize )
	{
		if( m_dwStartPos < m_dwEndPos )
		{
			if( t_dwSize - m_dwEndPos > stSize )
				return true;
			else
			{
				stSize -= (t_dwSize - m_dwEndPos);
				if( m_dwStartPos > stSize )
					return true;
				else
					return false;
			}
		}
		else
		{
			if( m_dwStartPos - m_dwEndPos > stSize )
				return true;
			else
				return false;
		}
	}

	/*
	*/
	bool AddBlock( char*szBuf,unsigned short stSize )
	{
		if( 0 == stSize )
			return false;
		if( !( stSize + sizeof( SBlockHeader ) < const_iBlockMaxSize ) )
		{
			assert( false );
			return false;
		}
		char szStaticBuf[ const_iBlockMaxSize ];
		SBlockHeader header;
		header.stBlockSize = stSize;


		memcpy( szStaticBuf,&header,sizeof( SBlockHeader ) );
		memcpy( szStaticBuf + sizeof( SBlockHeader ),szBuf,stSize );
		stSize += sizeof( SBlockHeader );
		/*if( !CanAddBlock( stSize ) )
			return false;*/		

		if( (m_dwStartPos < m_dwEndPos)
			||( m_dwStartPos == m_dwEndPos && m_iBlockCount == 0 ))
		{
			if( t_dwSize - m_dwEndPos > stSize )
			{
				memcpy( m_szBuf+m_dwEndPos,szStaticBuf,stSize );

				m_dwEndPos += stSize;

				goto add_succ;
			}
			else
			{
				unsigned short stHalfSize = (unsigned short)(t_dwSize - m_dwEndPos);
				memcpy( m_szBuf+m_dwEndPos,szStaticBuf,stHalfSize );
				stSize -= stHalfSize;
				if( m_dwStartPos > stSize )
				{
					memcpy( m_szBuf,szStaticBuf+stHalfSize,stSize );
					m_dwEndPos = stSize;
					goto add_succ;
				}
				else
					return false;
			}
		}
		else
		{
			if( m_dwStartPos - m_dwEndPos > stSize )
			{
				memcpy( m_dwEndPos+m_szBuf,szStaticBuf,stSize );
				m_dwEndPos += stSize;
				goto add_succ;
			}
			else
				return false;
		}
add_succ:
		m_iBlockCount ++;
		return true;
	}


	/*
	dwOff
	    in: 数据起始位置，相对于m_szBuf
		out:拷贝完后，数据指针的位置
	*/

	void CopyBuf( char *szBuf,DWORD &dwOff,unsigned short stSize )
	{
		DWORD dwPointer = dwOff;		

		if( dwPointer + stSize < t_dwSize )
		{
			memcpy( szBuf,m_szBuf + dwPointer,stSize );
			dwOff += stSize;
		}
		else
		{
			DWORD dwHalfSize = t_dwSize - dwPointer;
			memcpy( szBuf,m_szBuf + dwPointer,dwHalfSize );
			dwPointer = stSize - dwHalfSize;
			memcpy( szBuf + dwHalfSize ,m_szBuf,dwPointer );			
			dwOff = dwPointer;
		}
	}

	bool PeekBlock( char *szBuf,unsigned short &stBufSize )
	{
		if( m_iBlockCount < 0 )
		{
			assert( false );
			return false;
		}
		if( 0 == m_iBlockCount )
			return false;


		SBlockHeader header;
		DWORD dwPointer = m_dwStartPos;

		CopyBuf( (char*)&header,dwPointer,sizeof( SBlockHeader ) );
		if( stBufSize < header.stBlockSize )
		{
			stBufSize = header.stBlockSize;
			return false;
		}

		CopyBuf( szBuf,dwPointer,header.stBlockSize );				
		stBufSize = header.stBlockSize;
		
		return true;
	}
	/*
	stBufSize 
		in:the size of szBuf 
		out:the size of block
	*/
	bool GetBlock( char*szBuf,unsigned short &stBufSize )
	{
		if( m_iBlockCount < 0 )
		{
			assert( false );
			return false;
		}
		if( 0 == m_iBlockCount )
			return false;

		
		SBlockHeader header;
		DWORD dwPointer = m_dwStartPos;
		CopyBuf( (char*)&header,dwPointer,sizeof( SBlockHeader ) );
		if( stBufSize < header.stBlockSize )
		{
			stBufSize = header.stBlockSize;
			return false;
		}

		CopyBuf( szBuf,dwPointer,header.stBlockSize );		
		m_dwStartPos = dwPointer;
		stBufSize = header.stBlockSize;
		m_iBlockCount --;		
		return true;
	}

	void OutputInfo(void)
	{
		printf( "m_dwStartPos:%d m_dwEndPos:%d m_iBlockCount:%d\r\n",
			m_dwStartPos,m_dwEndPos,m_iBlockCount );
	}

	void Clear(void)
	{		
		m_iBlockCount = 0;
		m_dwStartPos = m_dwEndPos = 0;
	}
};


#endif //__TROUNDBUFFER_H__
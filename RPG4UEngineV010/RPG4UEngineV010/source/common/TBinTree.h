/*////////////////////////////////////////////////////////////////////////
文 件 名：TBinTree.h
创建日期：2008年4月19日
最后更新：2008年4月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TBINTREE_H__
#define __TBINTREE_H__
#pragma once


#include "TBinNode.h"
#include "TMemoryPoolFactory.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class TYPE, class VALUE>
class TBinTree
{
public:
	TBinTree(int nPoolSize=64);
	TBinTree( const TBinTree<TYPE, VALUE>& RValue,int nPoolSize=64);	///< 
	~TBinTree();	///< Destructor


public:
	typedef typename TBinNode<TYPE, VALUE>	BINNODE,*LPBINNODE;

	struct ABTStack
	{
		BINNODE*		ptree;
	};

protected:
	void RemoveFrom( BINNODE* pNode);	///< 
	void CycleFrom( BINNODE* pNode, DWORD dwParam);
	void ( *m_Process)( TYPE Data, VALUE CompValue, DWORD dwParam);


public:
	void Release( void);	///< 

public:

	TBinTree<TYPE, VALUE>& operator = ( const TBinTree& RValue);
	friend TBinTree& operator + ( const TBinTree& Value1, const TBinTree& Value2);



	//////////////////////////////////////////////////////////
	BINNODE* Add( TYPE NewElement, VALUE CompValue);
	TYPE RemoveNode( BINNODE* &pNode);	
	void RemoveAll( void);	///< 

	BOOL IsEmpty( void)	{ return ( m_lCount == 0); }
	virtual LONG GetCount( void)	{ return ( m_lCount); }

	BINNODE* FindNode	( VALUE CompValue);	

	BINNODE* FindHead	( void);
	BINNODE* GetLeft	( BINNODE* pNode);
	BINNODE* GetRight	( BINNODE* pNode);
	BINNODE* GetParent( BINNODE* pNode);

	void SetData( BINNODE* pNode, TYPE Data);
	TYPE& GetData( BINNODE* pNode);
	VALUE GetValue( BINNODE* pNode);

	void Cycle		( void ( *Process)( TYPE Data, VALUE CompValue, DWORD dwParam), DWORD dwParam);
	void CycleFast	( void ( *Process)( TYPE Data, VALUE CompValue, DWORD dwParam), DWORD dwParam);
	void GetSortedList( TYPE *pBuffer);


public:
	BOOL Optimize( void);

protected:
	void CopyFrom				( BINNODE* pNode);
	void UnlinkOneBottomNode( BINNODE* &pNode);

protected:
	BOOL GetOptimizeList			( TYPE *pData, VALUE *pCompValue);
	void AddDataByOptimization	( TYPE *pData, VALUE *pCompValue, int nLeft, int nRight);


protected:
	util::TMemoryPoolFactory<BINNODE>	m_nodePool;
	long											m_lCount;///< List 
	BINNODE*										m_pHead;	///< Head 
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class TYPE, class VALUE>
TBinTree<TYPE, VALUE>::TBinTree(int nPoolSize)
:m_nodePool(TRUE)
{
	m_lCount = 0;
	m_pHead = NULL;
	m_nodePool.Initialize(nPoolSize, nPoolSize/3+1);
}

template <class TYPE, class VALUE>
TBinTree<TYPE, VALUE>::TBinTree( const TBinTree<TYPE, VALUE>& RValue, int nPoolSize)
:m_nodePool(TRUE)
{
	m_lCount = 0;
	m_pHead = NULL;
	m_nodePool.Initialize(nPoolSize, nPoolSize/3+1);

	if ( RValue.m_pHead)
	{
		CopyFrom( RValue.m_pHead);
	}
}

template <class TYPE, class VALUE>
TBinTree<TYPE, VALUE>::~TBinTree()
{
	Release();
	//RemoveAll();
}

template <class TYPE, class VALUE>
TBinTree<TYPE, VALUE>& TBinTree<TYPE, VALUE>::operator = ( const TBinTree<TYPE, VALUE>& RValue)
{
	if ( &RValue == this)
	{
		return ( *this);
	}

	RemoveAll();
	if ( RValue.m_pHead)
	{
		CopyFrom( RValue.m_pHead);
	}

	return ( *this);
}

template <class TYPE, class VALUE>
TBinTree<TYPE, VALUE>& operator + ( const TBinTree<TYPE, VALUE>& Value1, const TBinTree<TYPE, VALUE>& Value2)
{

	TBinTree<TYPE, VALUE> Result;
	TBinTree<TYPE, VALUE> Left;
	TBinTree<TYPE, VALUE> Right;
	if ( Value1.m_pHead->GetValue() <= Value2.m_pHead->GetValue())
	{
		Left = Value1;
		Right = Value2;
	}
	else
	{
		Left = Value2;
		Right = Value1;
	}


	TYPE NullData;
	memset( &NullData, 0, sizeof( TYPE));
	VALUE CompValue = 0;
	if ( Left.m_pHead != NULL && Right.m_pHead != NULL)
	{
		CompValue = ( Left.m_pHead->GetValue() + Left.m_pHead->GetValue() + 1) / 2;
	}
	Result.Add( NullData, CompValue);


	Result.m_pHead->SetLeft( Left.m_pHead);
	Result.m_pHead->SetRight( Right.m_pHead);
	Result.m_lCount = Left.m_lCount + Right.m_lCount + 1;


	Left.m_pHead = NULL;
	Right.m_pHead = NULL;

	return ( TBinTree<TYPE, VALUE>( Result));
}


template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::CopyFrom( TBinNode<TYPE, VALUE>* pNode)
{
	Add( pNode->GetData(), pNode->GetValue());
	if ( pNode->GetLeft())
	{
		CopyFrom( pNode->GetLeft());
	}
	if ( pNode->GetLeft())
	{
		CopyFrom( pNode->GetRight());
	}
}


template <class TYPE, class VALUE>
TBinNode<TYPE, VALUE>* TBinTree<TYPE, VALUE>::Add( TYPE NewElement, VALUE CompValue)
{
	if ( m_pHead == NULL)
	{
		m_lCount++;

		m_pHead = m_nodePool.Alloc();
		m_pHead->SetData	(NewElement);
		m_pHead->SetValue	(CompValue);

		return ( m_pHead);
	}

	TBinNode<TYPE, VALUE>* pNode = m_pHead;
	TBinNode<TYPE, VALUE>* pPrev = m_pHead;
	while( pNode != NULL)
	{
		if ( CompValue < pNode->GetValue())
		{
			pPrev = pNode;
			pNode = pNode->GetLeft();
		}
		else
		{
			pPrev = pNode;
			pNode = pNode->GetRight();
		}
	}

	if ( pNode == NULL)
	{
		m_lCount++;


		BINNODE* pOne = m_nodePool.Alloc();
		pOne->SetData	(NewElement);
		pOne->SetValue	(CompValue);


		if ( CompValue < pPrev->GetValue())
		{
			pPrev->SetLeft( pOne);
		}
		else
		{
			pPrev->SetRight( pOne);
		}

		return ( pOne);
	}
	else
	{	// 蔼捞 乐栏骨肺
		return ( pNode);
	}
}

template <class TYPE, class VALUE>
TYPE TBinTree<TYPE, VALUE>::RemoveNode( TBinNode<TYPE, VALUE>* &pNode)
{
	TYPE ResultData = pNode->GetData();

	// 1. 
	TBinNode<TYPE, VALUE> *pOld = NULL;

	TBinNode<TYPE, VALUE> *pSeek = pNode->GetLeft();
	if ( pSeek)
	{
		if ( !pNode->GetRight())
		{
			if ( pNode == m_pHead)
			{
				m_pHead = pSeek;
				m_pHead->m_pParent = NULL;
			}
			else
			{
				if ( pNode == pNode->GetParent()->m_pRight)
				{
					pNode->GetParent()->SetRight( pSeek);
				}
				else
				{
					pNode->GetParent()->SetLeft( pSeek);
				}
			}

			m_nodePool.Free(pNode);
			m_lCount--;
			return ( ResultData);
		}

		pOld = pSeek;
		while ( pSeek)
		{
			pOld = pSeek;
			pSeek = pSeek->GetRight();
		}

		// 2. 
		TYPE Data = pOld->m_Data;
		VALUE CompValue = pOld->m_CompValue;
		//UnlinkOneBottomNode( pOld);
		RemoveNode( pOld);

		// 3. 
		pNode->m_Data = Data;
		pNode->m_CompValue = CompValue;

		return ( ResultData);
	}
	else
	{
		pSeek = pNode->GetRight();
		if ( pSeek)
		{
			if ( pNode == m_pHead)
			{
				m_pHead = pSeek;
				m_pHead->m_pParent = NULL;
			}
			else
			{
				if ( pNode == pNode->GetParent()->m_pRight)
				{
					pNode->GetParent()->SetRight( pSeek);
				}
				else
				{
					pNode->GetParent()->SetLeft( pSeek);
				}
			}

			m_nodePool.Free(pNode);
			m_lCount--;
			return ( ResultData);
		}
	}

	UnlinkOneBottomNode( pNode);
	return ( ResultData);
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::UnlinkOneBottomNode( TBinNode<TYPE, VALUE>* &pNode)
{
	if ( pNode->GetParent())
	{
		if ( pNode->GetParent()->GetLeft() == pNode)
		{
			pNode->GetParent()->m_pLeft = NULL;
		}
		else
		{
			pNode->GetParent()->m_pRight = NULL;
		}
	}
	else
	{
		m_pHead = NULL;
	}


	m_lCount--;

	m_nodePool.Free(pNode);
	pNode = NULL;
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::Release( void)
{
	m_nodePool.Release();
	m_pHead = NULL;
	m_lCount = 0;
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::RemoveAll( void)
{
	if ( m_pHead != NULL)
	{
		RemoveFrom( m_pHead);
		m_pHead = NULL;
	}
	//m_nodePool.Release();
	m_lCount = 0;
}


template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::RemoveFrom( TBinNode<TYPE, VALUE>* pNode)
{
	if ( pNode->GetLeft() != NULL)
	{
		RemoveFrom( pNode->GetLeft());
	}

	if ( pNode->GetRight() != NULL)
	{
		RemoveFrom( pNode->GetRight());
	}

	RemoveNode( pNode);
}


template <class TYPE, class VALUE>
TBinNode<TYPE, VALUE>* TBinTree<TYPE, VALUE>::FindNode( VALUE CompValue)
{
	TBinNode<TYPE, VALUE>* pNode = m_pHead;
	while( pNode != NULL && pNode->GetValue() != CompValue)
	{
		if ( CompValue < pNode->GetValue())
		{
			pNode = pNode->GetLeft();
		}
		else
		{
			pNode = pNode->GetRight();
		}
	}

	return ( pNode);
}

template <class TYPE, class VALUE>
TBinNode<TYPE, VALUE>* TBinTree<TYPE, VALUE>::FindHead( void)
{
	return ( m_pHead);
}

template <class TYPE, class VALUE>
TBinNode<TYPE, VALUE>* TBinTree<TYPE, VALUE>::GetLeft( TBinNode<TYPE, VALUE>* pNode)
{
	return ( pNode->GetLeft());
}

template <class TYPE, class VALUE>
TBinNode<TYPE, VALUE>* TBinTree<TYPE, VALUE>::GetRight( TBinNode<TYPE, VALUE>* pNode)
{
	return ( pNode->GetRight());
}

template <class TYPE, class VALUE>
TBinNode<TYPE, VALUE>* GetParent( TBinNode<TYPE, VALUE>* pNode)
{
	return ( pNode->GetParent());
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::SetData( TBinNode<TYPE, VALUE>* pNode, TYPE Data)
{
	pNode->SetData( Data);
}

template <class TYPE, class VALUE>
TYPE& TBinTree<TYPE, VALUE>::GetData( TBinNode<TYPE, VALUE>* pNode)
{
	return ( pNode->GetData());
}

template <class TYPE, class VALUE>
VALUE TBinTree<TYPE, VALUE>::GetValue( TBinNode<TYPE, VALUE>* pNode)
{
	return ( pNode->GetValue());
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::Cycle( void ( *Process)( TYPE Data, VALUE CompValue, DWORD dwParam), DWORD dwParam)
{
	m_Process = Process;

	if ( m_pHead == NULL)
	{
		return;
	}

	if ( m_pHead->GetLeft() != NULL)
	{
		CycleFrom( m_pHead->GetLeft(), dwParam);
	}

	( *m_Process)( m_pHead->GetData(), m_pHead->GetValue(), dwParam);

	if ( m_pHead->GetRight() != NULL)
	{
		CycleFrom( m_pHead->GetRight(), dwParam);
	}
}


template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::CycleFast( void ( *Process)( TYPE Data, VALUE CompValue, DWORD dwParam), DWORD dwParam)
{
	m_Process = Process;

	if ( m_pHead == NULL)
		return;

	BINNODE*		pNode;
	ABTStack*	pStackTop;

	pNode			= m_pHead;
	pStackTop	= NULL;

	ABTStack*	pStack = new ABTStack[m_lCount];
	int			nTop	 = 0;
	pStack[0].ptree	 = 0;

	//printf("\n二叉树的中序遍历结果<非递归>：\t");
	while(pNode != NULL || nTop)  /*二叉树未遍历完,或堆栈非空*/
	{
		while(pNode != NULL)
		{  
			nTop++;
			pStackTop			= &pStack[nTop];
			pStackTop->ptree	= pNode;
			pNode					= pNode->GetLeft(); /*遍历节点左子树,经过的节点依次进栈*/
		}

		if(nTop)
		{
			pStackTop	= &pStack[nTop];
			nTop--;
			pNode			= pStackTop->ptree; /*栈顶节点出栈*/
			pStackTop->ptree	= NULL;

			//char szBuf[128];
			//sprintf(szBuf," %d\n",pNode->data);
			//OutputDebugString(szBuf);
			( *m_Process)( pNode->GetData(), pNode->GetValue(), dwParam);
			pNode			= pNode->GetRight(); /*遍历节点右子树*/
		}
	}
	SAFE_DELETE_ARRAY(pStack);
}


template <class TYPE, class VALUE>
inline void Process_GetSortedList( TYPE Data, VALUE CompValue, DWORD dwParam)
{
	DWORD *pdwBuffer = ( DWORD*)dwParam;
	TYPE *pBuffer = ( TYPE*)pdwBuffer[0];
	int *piCount = ( int*)&pdwBuffer[1];
	pBuffer[( *piCount)++] = Data;
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::GetSortedList( TYPE *pBuffer)
{
	DWORD dwBuffer[2];
	dwBuffer[0] = ( DWORD)pBuffer;
	dwBuffer[1] = 0;

	CycleFast( Process_GetSortedList, ( DWORD)dwBuffer);

}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::CycleFrom( TBinNode<TYPE, VALUE>* pNode, DWORD dwParam)
{
	if ( pNode->GetLeft() != NULL)
	{
		CycleFrom( pNode->GetLeft(), dwParam);
	}

	( *m_Process)( pNode->GetData(), pNode->GetValue(), dwParam);

	if ( pNode->GetRight() != NULL)
	{
		CycleFrom( pNode->GetRight(), dwParam);
	}
}

template <class TYPE, class VALUE>
BOOL TBinTree<TYPE, VALUE>::Optimize( void)
{
	TYPE *pData			= new TYPE[GetCount()];
	VALUE *pCompValue	= new VALUE[GetCount()];
	if ( !GetOptimizeList( pData, pCompValue))
	{
		delete [] pCompValue;
		delete [] pData;
		return ( FALSE);
	}

	long lCount = GetCount();
	RemoveAll();
	AddDataByOptimization( pData, pCompValue, 0, lCount);
	delete [] pCompValue;
	delete [] pData;

	return ( TRUE);
}

template <class TYPE, class VALUE>
void ProcessGetOptimizeList( TYPE Data, VALUE CompValue, DWORD dwParam)
{
	DWORD *pdwParam = ( DWORD *)dwParam;
	long *pCount = ( long*)pdwParam[0];
	TYPE *pData = ( TYPE *)pdwParam[1];
	VALUE *pCompValue = ( VALUE *)pdwParam[2];
	pData[*pCount] = Data;
	pCompValue[( *pCount)++] = CompValue;
}

template <class TYPE, class VALUE>
BOOL TBinTree<TYPE, VALUE>::GetOptimizeList( TYPE *pData, VALUE *pCompValue)
{
	long lOptimizeCount = 0;
	DWORD dwOptimizeData[3] = { ( DWORD)&lOptimizeCount, ( DWORD)pData, ( DWORD)pCompValue};
	Cycle( ProcessGetOptimizeList, ( DWORD)dwOptimizeData);
	if ( GetCount() != lOptimizeCount)
	{
		return ( FALSE);
	}

	return ( TRUE);
}

template <class TYPE, class VALUE>
void TBinTree<TYPE, VALUE>::AddDataByOptimization( TYPE *pData, VALUE *pCompValue, int nLeft, int nRight)
{
	if ( nLeft == nRight)
	{
		return;
	}

	Add( pData[( nLeft + nRight) / 2], pCompValue[( nLeft + nRight) / 2]);

	AddDataByOptimization( pData, pCompValue, nLeft, ( nLeft + nRight) / 2);
	AddDataByOptimization( pData, pCompValue, ( nLeft + nRight) / 2 + 1, nRight);
}




#endif //__TBINTREE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：THashTable.inl
创建日期：2008年1月11日
最后更新：2008年1月11日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __THASHTABLE_INL__
#define __THASHTABLE_INL__
#pragma once

namespace util
{ 

template <class T, class Type >
inline TIterator<T,Type>::TIterator():m_pCursor(NULL){}

template <class T, class Type >
inline TIterator<T,Type>::TIterator( const TIterator & itr ):m_pCursor(itr.m_pCursor){}

template <class T, class Type >
inline TIterator<T,Type>::~TIterator(){}

template <class T, class Type >
inline TIterator<T,Type>& TIterator<T,Type>::operator++()
{
	if( m_pCursor ) m_pCursor = m_pCursor->m_pNext;
	return *this;
}

template <class T, class Type >
inline TIterator<T,Type>& TIterator<T,Type>::operator++( int i )
{
	if( m_pCursor ) m_pCursor = m_pCursor->m_pNext;
	return *this;
}

template <class T, class Type >
inline TIterator<T,Type>& TIterator<T,Type>::operator--()
{
	if( m_pCursor ) m_pCursor = m_pCursor->m_pPrev;
	return *this;
}

template <class T, class Type >
inline TIterator<T,Type>& TIterator<T,Type>::operator--( int i )
{
	if( m_pCursor ) m_pCursor = m_pCursor->m_pPrev;
	return *this;
}

template <class T, class Type >
inline BOOL TIterator<T,Type>::operator!=( TIterator & it )
{
	return ( it.m_pCursor != m_pCursor );
}

template <class T, class Type >
inline BOOL TIterator<T,Type>::operator==( TIterator & it )
{
	return ( it.m_pCursor == m_pCursor );
}

template <class T, class Type >
inline T TIterator<T,Type>::operator*()
{
	if( m_pCursor ) 
		return ((THashBucketDat<T,Type>*)m_pCursor->m_pBucket)->m_Data;
	return NULL;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

template <class T, class Type >
THashTable<T,Type>::THashTable()
{
	m_dwDataNum			= 0;
	m_dwMaxBucketNum	= 0;
	m_ppBucketTable		= NULL;
	m_pListHead			= NULL;
	m_pListTail			= NULL;
	m_pListCur			= NULL;
	m_pCurBucket		= NULL;
	m_pHashListPool		= NULL;
	m_pHashBucketPool	= NULL;
	m_bInited			= FALSE;
}

template <class T, class Type >
THashTable<T,Type>::THashTable( DWORD dwMaxBucketNum )
{
	m_pHashBucketPool	= NULL;
	Initialize( dwMaxBucketNum );
}

template <class T, class Type >
THashTable<T,Type>::~THashTable()
{
	RemoveAll();

	if( m_pHashBucketPool )
	{
		m_pHashBucketPool->Release();
		delete m_pHashBucketPool;
		m_pHashBucketPool = NULL;
	}
}


template <class T, class Type >
BOOL THashTable<T,Type>::Initialize(DWORD dwMaxBucketNum)
{
	if(dwMaxBucketNum == 0)
		return FALSE;
	__BOOL_SUPER(Initialize(dwMaxBucketNum));
	m_pHashBucketPool	= new TMemoryPoolFactory<HashBucket>;
	m_pHashBucketPool->Initialize( dwMaxBucketNum, dwMaxBucketNum/2+1 );
	return TRUE;
}

template <class T, class Type >
inline THashBucket*	THashTable<T,Type>::_AllocBucket()
{
	return m_pHashBucketPool->Alloc();
}

template <class T, class Type >
void THashTable<T,Type>::_FreeBucket(THashBucket* pBucket)
{
	m_pHashBucketPool->Free((HashBucket*)pBucket);
}

/////////////////////////////////////////////////////////
/// don't check for the same key
template <class T, class Type >
BOOL	THashTable<T,Type>::Add(T TData, Type dwKey)
{
	HashBucket*	pBucket;
	Type			index = dwKey % m_dwMaxBucketNum;
	pBucket = (HashBucket*)_OnAddData(index);
	if(pBucket)
		pBucket->SetData( TData, dwKey );

	return pBucket != NULL;

#ifdef USE_PAUSE
	Type index = dwKey % m_dwMaxBucketNum;

	HashBucket *	cur = NULL;
	HashBucket *	prv = NULL;
	//HashBucket *	next = NULL;

	if (!m_ppBucketTable[index])
	{

		m_ppBucketTable[index] = m_pHashBucketPool->Alloc();
		m_ppBucketTable[index]->SetData( TData, dwKey );
		m_ppBucketTable[index]->m_pList = addList(m_ppBucketTable[index]);

	}
	else 
	{
		cur = m_ppBucketTable[index];
		while (cur)
		{
			prv = cur;
			cur = cur->m_pNext;
		}
		cur = prv->m_pNext = m_pHashBucketPool->Alloc();
		cur->SetData( TData, dwKey );
		cur->m_pPrev = prv;
		cur->m_pList = addList(cur);

	}
	m_dwDataNum++;

	return TRUE;
#endif
}


template <class T, class Type >
T	THashTable<T,Type>::GetData(Type dwKey)
{
	Type index = dwKey % m_dwMaxBucketNum;

	HashBucket* pBucket = (HashBucket*)m_ppBucketTable[index];

	while(pBucket)
	{
		if (pBucket->m_dwKey == dwKey)
		{
			return pBucket->m_Data;
		}
		pBucket = (HashBucket*)pBucket->m_pNext;
	}
	return NULL;

}

template <class T, class Type >
T	THashTable<T,Type>::GetHeadData() 
{
	if( !m_pListHead ) 
		return NULL;
	return ((HashBucket*)m_pListHead->m_pBucket)->m_Data;	
}

template <class T, class Type >
T THashTable<T,Type>::GetTailData() 
{
	if( !m_pListTail )
		return NULL;
	return ((HashBucket*)m_pListTail->m_pBucket)->m_Data;	
}


/*////////////////////////////////////////////////////////////////////////
/// Iterate List
/*////////////////////////////////////////////////////////////////////////
template <class T, class Type >
inline void	THashTable<T,Type>::SetFirst()
{
	m_pListCur = m_pListHead; 
}

template <class T, class Type >
T	THashTable<T,Type>::GetNext()
{
	if (m_pListCur)
	{
		const T& TData = ((HashBucket*)m_pListCur->m_pBucket)->m_Data;
		m_pListCur = m_pListCur->m_pNext;
		return TData;
	}

	return NULL;		
}

template <class T, class Type >
inline void	THashTable<T,Type>::SetLast()
{
	m_pListCur = m_pListTail;
}

template <class T, class Type >
T	THashTable<T,Type>::GetPrev()
{
	if (m_pListCur)
	{
		const T& TData = ((HashBucket*)m_pListCur->m_pBucket)->m_Data;
		m_pListCur = m_pListCur->m_pPrev;
		return TData;
	}

	return NULL;		
}



/// Iterate Bucket Chain List
template <class T, class Type >
void	THashTable<T,Type>::SetBucketFirst(Type dwKey)
{
	Type index = dwKey % m_dwMaxBucketNum;			
	m_dwBucketKey = dwKey;
	m_pCurBucket = m_ppBucketTable[index];
	m_pLastBucket = NULL;
}	


template <class T, class Type >
T	THashTable<T,Type>::GetBucketNext()
{
	while(m_pCurBucket)
	{
		if (((HashBucket*)m_pCurBucket)->m_dwKey == m_dwBucketKey)
		{
			T	TData = ((HashBucket*)m_pCurBucket)->m_Data;
			m_pLastBucket = m_pCurBucket;
			m_pCurBucket = m_pCurBucket->m_pNext;
			return TData;
		}
		m_pCurBucket = m_pCurBucket->m_pNext;
	}
	return NULL;
}


template <class T, class Type >
void	THashTable<T,Type>::RemoveCurBucketData()
{
	Type dwIndex = m_dwBucketKey%m_dwMaxBucketNum;
	_RemoveCurBucketData(dwIndex);

#ifdef USE_PAUSE
	HashBucket*	cur = m_pLastBucket;
	HashBucket*	prv = NULL;
	HashBucket*	next = NULL;

	prv = cur->m_pPrev;
	next = cur->m_pNext;
	if (!prv)
		m_ppBucketTable[dwIndex] = next;
	else 
		prv->m_pNext = next;

	if (next)
		next->m_pPrev = prv;

	removeList(cur->m_pList);
	m_pHashBucketPool->Free(cur);
	cur = NULL;
	--m_dwDataNum;
#endif
}


/// delete bucket, list for (dwKey)
template <class T, class Type >
void	THashTable<T,Type>::Remove(Type dwKey)
{
	Type dwIndex = dwKey % m_dwMaxBucketNum;

	HashBucket*	cur = (HashBucket*)m_ppBucketTable[dwIndex];
	//HashBucket*	prv = NULL;
	//HashBucket*	next = NULL;

	while (cur)
	{
		if (cur->m_dwKey == dwKey)
		{
			_Remove(dwIndex,cur);
#ifdef USE_PAUSE
			prv = cur->m_pPrev;
			next = cur->m_pNext;

			if(cur == m_pCurBucket)			/// for Iterator sync
				m_pCurBucket = next;

			if (!prv)
				m_ppBucketTable[dwIndex] = next;
			else 
				prv->m_pNext = next;

			if (next)
				next->m_pPrev = prv;

			removeList(cur->m_pList);
			m_pHashBucketPool->Free(cur);//delete cur;
			--m_dwDataNum;
#endif
			cur = NULL;
			return;
		}
		cur = (HashBucket*)cur->m_pNext;
	}
}

template <class T, class Type >
T	THashTable<T,Type>::RemoveHead()
{
	//HashNode*	cur = m_pListHead;
	if( !m_pListHead )
		return NULL;
	T data = ((HashBucket*)m_pListHead->m_pBucket)->m_Data;
	_RemoveHead();

#ifdef USE_PAUSE
	m_pListHead = m_pListHead->m_pNext;
	T data = cur->m_pBucket->m_Data;

	m_pHashBucketPool->Free(cur->m_pBucket);	//delete cur->m_pBucket;
	m_pHashListPool->Free(cur);				//delete cur;

	--m_dwDataNum;
#endif

	return data;
}

/// delete all of Bucket, List Node
template <class T, class Type >
void	THashTable<T,Type>::RemoveAll()
{
	_SUPER::RemoveAll();
#ifdef USE_PAUSE
	HashNode*	cur = m_pListHead;
	HashNode*	next = NULL;

	while (cur)
	{
		next = cur->m_pNext;
		m_pHashBucketPool->Free(cur->m_pBucket);	//delete cur->m_pBucket;
		m_pHashListPool->Free(cur);				//delete cur;
		cur = next;
	}
	m_dwDataNum = 0;
	m_pListHead = NULL;

	if(m_ppBucketTable)
		memset(m_ppBucketTable, 0, sizeof(HashBucket *) * m_dwMaxBucketNum);
#endif
}


template <class T, class Type >
TIterator<T,Type> THashTable<T,Type>::find( Type dwKey )
{
	Type index = dwKey % m_dwMaxBucketNum;

	HashBucket* pBucket = (HashBucket*)m_ppBucketTable[index];

	while(pBucket)
	{
		if (pBucket->m_dwKey == dwKey)
		{
			break;
		}
		pBucket = (HashBucket*)pBucket->m_pNext;
	}
	Iterator it;
	if( pBucket )
		it.m_pCursor = pBucket->m_pList;
	else
		it.m_pCursor = NULL;
	return it;
}

template <class T, class Type >
TIterator<T,Type> THashTable<T,Type>::begin()
{
	Iterator it;
	it.m_pCursor = m_pListHead;
	return it;
}

template <class T, class Type >
TIterator<T,Type> THashTable<T,Type>::end()
{
	Iterator it;
	it.m_pCursor = NULL;
	return it;
}

template <class T, class Type >
void THashTable<T,Type>::erase( Iterator & it )
{
	Remove( ((HashBucket*)it.m_pCursor->m_pBucket)->m_dwKey );
}

template <class T, class Type >
void THashTable<T,Type>::clear()
{
	RemoveAll();
	ASSERT( 0 == GetDataNum() );
}





};//namespace util

#endif //__THASHTABLE_INL__


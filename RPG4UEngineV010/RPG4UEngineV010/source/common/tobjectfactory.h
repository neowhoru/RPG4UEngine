/*////////////////////////////////////////////////////////////////////////
文 件 名：TObjectFactory.h
创建日期：2007年10月4日
最后更新：2007年10月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TOBJECTFACTORY_H__
#define __TOBJECTFACTORY_H__
#pragma once


#include "TMemoryPoolFactory.h"
#include "IUnitFactory.h"
VRU_USING;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<class IObject,typename Type=DWORD>
class  TIObjectFactory : public IUnitFactory
{
public:
	//virtual const Type& GetName() = 0;
	//virtual LPCSTR GetFileExt() = 0;
	//virtual BOOL Initialize() = 0;
	//virtual void Release() = 0;
	//virtual IObject*	CreateInstance() = 0;
	//virtual void		DestroyInstance(IObject*)=0;
	//virtual void LogFactoryInfo(LPCTSTR szExtra=_T(""))=0;
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<class Object, class IObject, typename Type, DWORD _OBJNULL=INVALID_DWORD_ID>
class  TObjectFactory : public TIObjectFactory<IObject,Type>
{
public:
	TObjectFactory(const Type& typeName, UINT nPoolSize=32,BOOL bConstructInPlace=TRUE)
	:m_objectPool(bConstructInPlace)
	{
		assert(ms_typeName == (Type)_OBJNULL);
		if(ms_typeName == (Type)_OBJNULL)
			ms_typeName = typeName;
		m_uPoolSize	= nPoolSize;
		//Initialize();	此处不许由构造函数调用
	}
	~TObjectFactory()
	{
		assert(m_uPoolSize == 0 && "调用者须调用TObjectFactoryManager.ReleaseAllFactories释放资源"); //调用者须释放资源 Release
		//Release();	此处不许由析构函数调用
	}

	//由调用者手工初始化
	BOOL Initialize()
	{
		if(m_uPoolSize)
			m_objectPool.Initialize(m_uPoolSize, m_uPoolSize/4+1);
		return TRUE;
	}

	//由调用者手工释放资源
	void Release()
	{
		if(m_uPoolSize)
		{
			m_objectPool.Release();
			m_uPoolSize = 0;
		}
	}


	virtual const Type& GetName()
	{
		assert(ms_typeName != (Type)_OBJNULL);
		return ms_typeName;
	}
	virtual LPCSTR GetFileExt() 
	{
		return "";
	}


	virtual void* CreateInstance()
	{
		return m_objectPool.Alloc();//new Object;
	}
	virtual void DestroyInstance(void* pObject)
	{
		if(pObject)m_objectPool.Free((Object*)pObject);
	}

	virtual void LogFactoryInfo(LPCTSTR /*szExtra*/)
	{
		LOGINFO	( "%s [Base:%4u,Extend:%4u][BandNumber:%u Nodes:%u]\n"
					, ms_FactoryName
					, m_objectPool.GetPoolBasicSize()
					, m_objectPool.GetPoolExtendSize()
					, m_objectPool.GetNumberOfBands()
					, m_objectPool.GetAvailableNumberOfTypes()
					);
	}


	static Type		ms_typeName;
	static LPCTSTR	ms_FactoryName;
public:
	util::TMemoryPoolFactory< Object >	m_objectPool;
	UINT											m_uPoolSize;
};


template<class Object,class IObject, typename Type, DWORD _OBJNULL>
Type TObjectFactory<Object,IObject,Type,_OBJNULL>::ms_typeName = (Type)_OBJNULL;//#Object;

template<class Object,class IObject, typename Type, DWORD _OBJNULL>
LPCTSTR TObjectFactory<Object,IObject,Type,_OBJNULL>::ms_FactoryName = _T("");//#Object;



/*////////////////////////////////////////////////////////////////////////
New Delete形式Factory
/*////////////////////////////////////////////////////////////////////////
template<class Object, class IObject, typename Type, DWORD _OBJNULL=INVALID_DWORD_ID>
class  TObjectFactoryNew : public IUnitFactory
{
public:
	TObjectFactoryNew(const Type& typeName)
	{
		assert(ms_typeName == (Type)_OBJNULL);
		if(ms_typeName == (Type)_OBJNULL)
			ms_typeName = typeName;
	}
	~TObjectFactoryNew()
	{
	}

	//由调用者手工初始化
	BOOL Initialize()	{	return TRUE;	}
	void Release()	{	}

	virtual const Type& GetName()
	{
		assert(ms_typeName != (Type)_OBJNULL);
		return ms_typeName;
	}
	virtual LPCSTR GetFileExt() 	{		return "";	}

	virtual void LogFactoryInfo(LPCTSTR /*szExtra*/)
	{
	}

	virtual void* CreateInstance()
	{
		return new Object;
	}
	virtual void DestroyInstance(void* pObjectPtr)
	{
		IObject* pObject = (IObject*)pObjectPtr;
		SAFE_DELETE(pObject);
	}

	static Type ms_typeName;
};


template<class Object,class IObject, typename Type, DWORD _OBJNULL>
Type TObjectFactoryNew<Object,IObject,Type,_OBJNULL>::ms_typeName = (Type)_OBJNULL;//#Object;

#endif //__TOBJECTFACTORY_H__
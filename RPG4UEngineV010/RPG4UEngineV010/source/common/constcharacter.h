/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstCharacter.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTCHARACTER_H__
#define __CONSTCHARACTER_H__
#pragma once




/*////////////////////////////////////////////////////////////////////////
MoveAI
/*////////////////////////////////////////////////////////////////////////
enum eNPC_MOVE_ATTITUDE
{
	 eMOVE_ATTITUDE_WANDER			= 0	// 0游荡型
	,eMOVE_ATTITUDE_ETERNAL_STOP	= 1	// 1静止型
	,eMOVE_ATTITUDE_BACK_SPAWN		= 2	// 2归家型 返回出生点
	,eMOVE_ATTITUDE_PATROL			= 3	// 3巡逻型
	,eMOVE_ATTITUDE_SEARCH_AREA	= 4	// 4搜索型            
	,eMOVE_ATTITUDE_MAX
};



enum eATTACK_ATTITUDE
{
	ATTACK_ATTITUDE_PASSIVE				= 1,	//1 被动攻击
	ATTACK_ATTITUDE_NEAREST_FIRST		= 2,	//2 就近攻击
	ATTACK_ATTITUDE_LOW_HP_FIRST		= 3,	//3 低血攻击
	ATTACK_ATTITUDE_LOW_LEVEL_FIRST	= 4,	//4 低级攻击
	ATTACK_ATTITUDE_HIGH_MP_FIRST		= 5,	//5 高魔攻击
	ATTACK_ATTITUDE_ONE_TARGET			= 6,	//6 单目标攻击
	ATTACK_ATTITUDE_MAX	
};

enum eTARGET_SEARCH_TYPE
{
	eTARGET_SEARCH_NEAREST	= 0,	// 寻找最近怪物
	eTARGET_SEARCH_LOW_HP,			// 寻找HP最低
	eTARGET_SEARCH_LOW_LEVEL,		// 寻找级别最低
	eTARGET_SEARCH_HIGH_MP,			// 寻找MP最高
	eTARGET_SEARCH_LOW_HPRATIO,	// 寻找HP率最低
	eTARGET_SEARCH_CORPSE,			// 寻找尸体
	eTARGET_SEARCH_MAX
};


/*////////////////////////////////////////////////////////////////////////
移动类型
/*////////////////////////////////////////////////////////////////////////
enum eMOVE_TYPE
{
	 MOVETYPE_WALK						= 0
	,MOVETYPE_RUN						= 1
	,MOVETYPE_SWIPE					= 2
	,MOVETYPE_KNOCKBACK				= 3
	,MOVETYPE_KNOCKBACK_DOWN		= 4
	,MOVETYPE_SIDESTEP				= 5
	,MOVETYPE_BACKSTEP				= 6
	,MOVETYPE_TUMBLING_FRONT		= 7
	,MOVETYPE_TUMBLING_LEFT			= 8
	,MOVETYPE_TUMBLING_RIGHT		= 9
	,MOVETYPE_TUMBLING_BACK			= 10
	,MOVETYPE_SHOULDER_CHARGE		= 11
	,MOVETYPE_SLIDING					= 12
	,MOVETYPE_TELEPORT				= 13

	,MOVETYPE_MAX
};




#endif //__CONSTCHARACTER_H__
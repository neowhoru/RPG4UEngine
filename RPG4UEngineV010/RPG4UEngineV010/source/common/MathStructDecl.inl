/*////////////////////////////////////////////////////////////////////////
文 件 名：MathStructDecl.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHSTRUCTDECL_INL__
#define __MATHSTRUCTDECL_INL__
#pragma once

namespace math
{ 

template<typename Type>
inline TPoint3D<Type>::operator Type* ()
{
	return (Type *) &x;
}


template<typename Type>
inline TPoint3D<Type>::operator const Type* () const
{
	return (const Type *) &x;
}

// assignment operators
template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator += ( CONST TPoint3D<Type>& v )
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}

template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator -= ( CONST TPoint3D<Type>& v )
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}


//template<typename Type>
//inline TPoint3D& TPoint3D<Type>::operator *= ( CONST TPoint3D& v )
//{
//	TPoint3D v0(*this);
//	*this = v0.CrossProduct(v);
//    //x *= v.x;
//    //y *= v.y;
//    //z *= v.z;
//    return *this;
//}


//template<typename Type>
//inline TPoint3D& TPoint3D<Type>::operator /= ( CONST TPoint3D& v )
//{
//    x /= v.x;
//    y /= v.y;
//    z /= v.z;
//    return *this;
//}

template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator() (Type x0, Type y0, Type z0)
{
	x = x0;
	y = y0;
	z = z0;
	return *this;
}

template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator () ( const TPoint3D<Type>& v )
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}


template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator () (const Type scale )
{
	x = scale;
	y = scale;
	z = scale;
	return *this;
}


template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator () (const Type* ar )
{
	x = ar[0];
	y = ar[1];
	z = ar[2];
	return *this;
}


//template<typename Type>
//inline TPoint3D<Type>& TPoint3D<Type>::operator = ( const TPoint3D<Type>& v )
//{
//	x = v.x;
//	y = v.y;
//	z = v.z;
//	return *this;
//}
//
//
//template<typename Type>
//inline TPoint3D<Type>& TPoint3D<Type>::operator = (const Type scale )
//{
//	x = scale;
//	y = scale;
//	z = scale;
//	return *this;
//}
//
//
//template<typename Type>
//inline TPoint3D<Type>& TPoint3D<Type>::operator = (const Type* ar )
//{
//	x = ar[0];
//	y = ar[1];
//	z = ar[2];
//	return *this;
//}



template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator += ( Type f )
{
	x += f;
	y += f;
	z += f;
	return *this;
}


template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator -= ( Type f )
{
	x -= f;
	y -= f;
	z -= f;
	return *this;
}


template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator *= ( Type f )
{
	x *= f;
	y *= f;
	z *= f;
	return *this;
}


template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::operator /= ( Type f )
{
	Type fInv;
	if(f != 0)	fInv = 1.0f / (Type)f;
	else			fInv = 0;

	x = (Type)(x *fInv);
	y = (Type)(y *fInv);
	z = (Type)(z *fInv);
	return *this;
}


// unary operators

template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator + () const
{
	return *this;
}


template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator - () const
{
	TPoint3D ret;
	return ret(-x, -y, -z);
}


// binary operators

template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator + ( CONST TPoint3D<Type>& v ) const
{
	TPoint3D ret;
	return ret(x + v.x, y + v.y, z + v.z);
}


template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator - ( CONST TPoint3D<Type>& v ) const
{
	TPoint3D ret;
	return ret(x - v.x, y - v.y, z - v.z);
}


//template<typename Type>
//inline TPoint3D TPoint3D<Type>::operator * ( CONST TPoint3D& v ) const
//{
//    return CrossProduct(v);
//    //return TPoint3D(x * v.x, y * v.y, z * v.z);
//}


//inline TPoint3D TPoint3D<Type>::operator / ( CONST TPoint3D& v ) const
//{
//    return TPoint3D(x / v.x, y / v.y, z / v.z);
//}




template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator + ( Type f ) const
{
	TPoint3D ret;
	return ret(x + f, y + f, z + f);
}

template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator - ( Type f ) const
{
	TPoint3D ret;
	return ret(x - f, y - f, z - f);
}

template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator * ( Type f ) const
{
	TPoint3D ret;
	return ret(x * f, y * f, z * f);
}

template<typename Type>
inline TPoint3D<Type> TPoint3D<Type>::operator / ( Type f ) const
{
	TPoint3D ret;
	Type fInv;
	if(f != 0)
		fInv = 1.0f / (Type)f;
	else
		fInv = 0;
	return ret(x * fInv, y * fInv, z * fInv);
}

/////////////////////////////////////////////////

template<typename Type>
inline BOOL TPoint3D<Type>::operator == ( CONST TPoint3D<Type>& v ) const
{
	return x == v.x && y == v.y && z == v.z;
}


template<typename Type>
inline BOOL TPoint3D<Type>::operator != ( CONST TPoint3D<Type>& v ) const
{
	return x != v.x || y != v.y || z != v.z;
}


template<typename Type>
inline BOOL TPoint3D<Type>::operator < ( CONST TPoint3D<Type>& v ) const
{
	return x < v.x && y < v.y && z < v.z;
}


template<typename Type>
inline BOOL TPoint3D<Type>::operator > ( CONST TPoint3D<Type>& v ) const
{
	return x > v.x && y > v.y && z > v.z;
}

template<typename Type>
inline Type TPoint3D<Type>::LengthSquared () const
{
	return x * x + y * y + z * z;
}

template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::SwapXY()
{
	Type t =x;
	x = y;
	y = t;
	return *this;
}

template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::SwapYZ()
{
	Type t = y;
	y = z;
	z = t;
	return *this;
}

template<typename Type>
inline TPoint3D<Type>& TPoint3D<Type>::SwapXZ()
{
	Type t = x;
	x = z;
	z = t;
	return *this;
}


};//namespace math


#endif //__MATHSTRUCTDECL_INL__


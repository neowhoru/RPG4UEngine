/*////////////////////////////////////////////////////////////////////////
文 件 名：WorldConst.h
创建日期：2008年4月28日
最后更新：2008年4月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __WORLDCONST_H__
#define __WORLDCONST_H__
#pragma once

#include "BaseSetting.h"

/*////////////////////////////////////////////////////////////////////////
//地图相关常量
/*////////////////////////////////////////////////////////////////////////
const float TILE_METER				= 4.0f;
const float TILE_ORIGINSIZE		= TILE_METER * 32.0f;
#define TILE_3DSIZE					(SCALE_TERRAIN * TILE_ORIGINSIZE) ////地形块的基本大小

#define MAPTILESIZE					TILE_3DSIZE							//地形块的基本大小
#define MAPHEIGHTSIZE				32										//128/4		//悬崖高度
#define MAPTICKHEIGHT				(MAPTILESIZE/MAPHEIGHTSIZE)	//地形高度的单位
#define HEIGHTINTERVAL2P			27										//相邻两点间的最大高度差


const INT TILE_RES					=	8;			//地图Chunk中Tile宽/高数
const float TILE_RES_1				=	0.125f;	//1/8
											
const INT MAP_CHUNK_CNT				=	16;			//地图中Chunk宽/高数
const INT ALPHA_RES					=	64	;		
const INT SHADOW_RES					=	64;
const INT MAP_SIZE_BYTILE			=	MAP_CHUNK_CNT*TILE_RES;	// 128
const INT MAP_CACHE_SIZE			=	5*5;	// 25
const INT TERRAIN_CACHE_SIZE		=	3;

											
const INT ALPHA_PIXEL_PER_TILE	=	ALPHA_RES/TILE_RES;
const INT CHUNK_VERTEX_COUNT		=	(TILE_RES+1)*(TILE_RES+1);
const INT CHUNK_VERTEX_RES			=	8;
const float eUnTerrainHeight		=	0.0f;


const INT	MAPSTRIDE_BY_TILE		= TILE_RES * MAP_CHUNK_CNT + 1;
const INT	MAPSIZE_BY_TILE		= TILE_RES * MAP_CHUNK_CNT;

const char	MAP_AXIS_INDEXS[]		= {"0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ[];',./!@#$%^&*()_+|{}:\"<>?"};
const INT MAP_AXIS_MAX				= sizeof(MAP_AXIS_INDEXS)-1;




#endif //__WORLDCONST_H__
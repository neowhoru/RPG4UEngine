/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstMap.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTMAP_H__
#define __CONSTMAP_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eFIELDEFFECT_TYPE
{
	 EFFECT_TYPE_NONE						= 0
	,EFFECT_TYPE_PERIODIC_DAMAGE		= 1
};


/*////////////////////////////////////////////////////////////////////////
用户在线状态
/*////////////////////////////////////////////////////////////////////////
enum eUSER_STATUS
{
	 US_INVALID					= 0
	,US_IN_CHARACTERSCENE
	,US_IN_VILLAGE
	,US_IN_BATTLEZONE
};


/*////////////////////////////////////////////////////////////////////////
"玩家所在地图"状态
/*////////////////////////////////////////////////////////////////////////
enum eZONE_STATE
{
	 ZONESTATE_NOT_ATZONE				= _BIT(0)
	,ZONESTATE_ATZONE						= _BIT(1)
	,ZONESTATE_ATVILLAGE					= _BIT(2)  | ZONESTATE_ATZONE
	,ZONESTATE_ATROOM						= _BIT(3)  | ZONESTATE_ATZONE
	,ZONESTATE_ATLOBBY					= _BIT(4)  | ZONESTATE_ATROOM
	,ZONESTATE_ATMISSION					= _BIT(5)  | ZONESTATE_ATROOM
	,ZONESTATE_ATHUNTING					= _BIT(6)  | ZONESTATE_ATROOM
	,ZONESTATE_ATPVP						= _BIT(7)  | ZONESTATE_ATROOM
	,ZONESTATE_ATCHARSELECT				= _BIT(8)  | ZONESTATE_ATROOM

	,ZONESTATE_PRE_ENTER_ZONE			= _BIT(9)
	,ZONESTATE_PRE_ENTER_VILLAGE		= _BIT(10) |  ZONESTATE_PRE_ENTER_ZONE
	,ZONESTATE_PRE_ENTER_ROOM			= _BIT(11) |  ZONESTATE_PRE_ENTER_ZONE
	,ZONESTATE_PRE_ENTER_LOBBY			= _BIT(12) |  ZONESTATE_PRE_ENTER_ROOM
	,ZONESTATE_PRE_ENTER_MISSION		= _BIT(13) |  ZONESTATE_PRE_ENTER_ROOM
	,ZONESTATE_PRE_ENTER_HUNTING		= _BIT(14) |  ZONESTATE_PRE_ENTER_ROOM
	,ZONESTATE_PRE_ENTER_PVP			= _BIT(15) |  ZONESTATE_PRE_ENTER_ROOM
	,ZONESTATE_PRE_ENTER_CHARSELECT	= _BIT(16) |  ZONESTATE_PRE_ENTER_ROOM
};




/////////////////////////////////////////////////////////////////////////////////////////
// Room 开放状态
/////////////////////////////////////////////////////////////////////////////////////////
enum eZONE_OPEN_STATE
{
	 ZONEOPEN_PUBLIC = 0
	,ZONEOPEN_PRIVATE
};


/*////////////////////////////////////////////////////////////////////////
战区类型
/*////////////////////////////////////////////////////////////////////////
enum eZONE_TYPE
{
	 ZONETYPE_LOBBY = 0
	,ZONETYPE_VILLAGE
	,ZONETYPE_CHARSELECT
	,ZONETYPE_MISSION
	,ZONETYPE_HUNTING
	,ZONETYPE_QUEST
	,ZONETYPE_PVP
	,ZONETYPE_GUILD
	,ZONETYPE_SIEGE
	,ZONETYPE_EVENT
	,ZONETYPE_MAX	
};


/*////////////////////////////////////////////////////////////////////////
练级副本难度
/*////////////////////////////////////////////////////////////////////////
enum eHUNTING_DIFFICULTY
{
	 HUNTING_DIFFICULTY_EASY = 0
	,HUNTING_DIFFICULTY_NORMAL
	,HUNTING_DIFFICULTY_HARD
	,HUNTING_DIFFICULTY_MAX
};

/*////////////////////////////////////////////////////////////////////////
练级副本奖励
/*////////////////////////////////////////////////////////////////////////
enum eHUNTING_BONUS_TYPE
{
	 HUNTINGBONUS_ITEM = 0
	,HUNTINGBONUS_EXPERIENCE
	,HUNTINGBONUS_MAX
};

/*////////////////////////////////////////////////////////////////////////
竞技副本类型
/*////////////////////////////////////////////////////////////////////////
enum ePVPRULE_TYPE
{
	 PVPRULE_10 = 0
	,PVPRULE_30
	,PVPRULE_INFINITY
	,PVPRULE_MAX

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eZONE_CATEGORY
{
	 ZONECATEGORY_VILLAGE			= 1
	,ZONECATEGORY_BATTLEZONE
};


enum ePVP_MODE_TYPE
{
	 PVPMODE_PERSONAL = 0
	,PVPMODE_TEAM
	,PVPMODE_MAX
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eTRIGGER_ATTRIBUTE
{
	 TRIGGER_ATTRIBUTE_CONDITION_OR_FLAG          = 0x01
	,TRIGGER_ATTRIBUTE_ACTIVE_FLAG                = 0x02
	,TRIGGER_ATTRIBUTE_LOOP_FLAG                  = 0x04
	,TRIGGER_ATTRIBUTE_ACTIVATE_NEXT_TRIGGER      = 0x08
};




#endif //__CONSTMAP_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：StructMap.h
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTMAP_H__
#define __STRUCTMAP_H__
#pragma once

#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sMAPINFO_BASE
{
	enum { MAX_FIELD_NUM = 6};

	CODETYPE		MapCode;
	CODETYPE		MapGroupIndex;
	VRSTR			sMapName;

	BYTE			byMKind;					//< 
	BYTE			byMapSubIndex;					//< 
	BYTE			byMinUserNum;
	BYTE			byMaxUserNum;
	LEVELTYPE	minLV;
	LEVELTYPE	maxLV;

	TAG_CODE		tagStartAreaID;
	FIELDID		arFieldIDs[MAX_FIELD_NUM];
	VRSTR			arFieldNames[MAX_FIELD_NUM];

};
typedef const sMAPINFO_BASE sMAPINFO_BASEC;
//#pragma warning(pop)


/*////////////////////////////////////////////////////////////////////////
	大陆中战区信息
/*////////////////////////////////////////////////////////////////////////
struct sFIELDINFO_BASE
{
	CODETYPE		FieldCode;
	VRSTR			szFieldName	;	//战区名称
	VRSTR			szFieldPath	;					//战区所在目录名称
	UINT			uWidth;										//战区地图数量
	UINT			uHeight;		
	BOOL			bShowMiniMap;								//是否显示小地图
	BOOL			bCanRide;									//是否可骑马
	BOOL			bCanPK;										//是否可PK
};
typedef const sFIELDINFO_BASE	sFIELDINFO_BASEC;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sROOMINFO_BASE
{
	BYTE	m_MinLV;
	BYTE	m_MaxLV;
	BYTE	m_ClassLimit;	
};
typedef const sROOMINFO_BASE	sROOMINFO_BASEC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sROOMINFO_ADDITIONAL
{
	BYTE		m_Difficulty	: 4;		//< eHUNTING_DIFFICULTY 场景难度
	BYTE		m_Bonus			: 4;		//< eHUNTING_BONUS_TYPE 奖励
};
typedef const sROOMINFO_ADDITIONAL	sROOMINFO_ADDITIONALC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPVPINFO_ADDITIONAL
{
	BYTE		m_Rule			: 4;		//< 
	BYTE		m_Mode			: 4;		//< 
};
typedef const sPVPINFO_ADDITIONAL	sPVPINFO_ADDITIONALC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPVPINFO_LOBBY
{
	BYTE						m_Team	:7;	
	BYTE						m_Ready	:1;
	DWORD						m_dwPlayerKey;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sROOMINFO_COMMON
{
	KEYTYPE			m_Key;
	CODETYPE			m_MapCode;
	TCHAR				m_szRoomTitle[MAX_ROOMTITLE_LENGTH];
	BYTE				m_byRoomPublic;
	struct 
	{
		BYTE			m_CurUserNum		: 4;
		BYTE			m_MaxLimitUserNum	: 4;
	};
	sROOMINFO_BASE		m_RoomInfo;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sROOMINFO_MISSION : public sROOMINFO_COMMON
{
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sROOMINFO_HUNTING : public sROOMINFO_COMMON
{
	sROOMINFO_ADDITIONAL m_AdditionalInfo;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sROOMINFO_PVP : public sROOMINFO_COMMON
{
	sPVPINFO_ADDITIONAL	m_AdditionalPVPInfo;
};


#pragma pack(pop)

#endif //__STRUCTMAP_H__
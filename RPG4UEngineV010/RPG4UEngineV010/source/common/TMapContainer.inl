/*////////////////////////////////////////////////////////////////////////
文 件 名：TMapContainer.inl
创建日期：2006年10月15日
最后更新：2006年10月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TMAPCONTAINER_INL__
#define __TMAPCONTAINER_INL__
#pragma once

template<class KEY, class OBJTYPE >
TMapContainer<KEY, OBJTYPE>::TMapContainer(BOOL							bNeedDelete
														 ,ObjectDeleteCallBack	pObjectDeleteCallBack)
:m_bNeedDelete(TRUE)
,m_pfnObjectDeleteCallBack(NULL)
{
	m_bNeedDelete					= bNeedDelete;
	m_pfnObjectDeleteCallBack	= pObjectDeleteCallBack;
}

template<class KEY, class OBJTYPE >
TMapContainer<KEY, OBJTYPE>::~TMapContainer(void)
{
	Destroy();
}

template<class KEY, class OBJTYPE >
inline void TMapContainer<KEY, OBJTYPE>::NeedDelete( BOOL b )
{
	m_bNeedDelete = b; 
}

template<class KEY, class OBJTYPE >
void	TMapContainer<KEY, OBJTYPE>::Destroy()
{
	if( m_bNeedDelete )
	{
		std::map<KEY, OBJTYPE>::iterator it;
		for( it = m_mapObject.begin(); it != m_mapObject.end(); it++ )
		{
			OBJTYPE obj = it->second;
			if( obj )
			{
				DestroyObject( &obj );
			}
		}			
	}
	m_mapObject.clear();
}

template<class KEY, class OBJTYPE >
BOOL	TMapContainer<KEY, OBJTYPE>::Add( KEY key, OBJTYPE obj )
{
	OBJTYPE found = Find( key );
	if( found )
	{
		return FALSE;
	}
	m_mapObject[key] = obj;
	return TRUE;
}


template<class KEY, class OBJTYPE >
BOOL	TMapContainer<KEY, OBJTYPE>::Remove( KEY key )
{
	OBJTYPE found = Find( key );
	if( !found )
		return FALSE;
	if( m_bNeedDelete )
	{
		if( found )
		{
			DestroyObject( &found );
		}
	}
	m_mapObject.erase( key );
	return TRUE;
}

template<class KEY, class OBJTYPE >
OBJTYPE TMapContainer<KEY, OBJTYPE>::Find( KEY key )
{
	std::map<KEY,OBJTYPE>::const_iterator it = m_mapObject.find( key );
	if( it == m_mapObject.end() )
		return NULL;
	return it->second;
}


template<class KEY, class OBJTYPE >
int	TMapContainer<KEY, OBJTYPE>::GetObjects( std::vector<OBJTYPE>* pvectorObject )
{
	if( pvectorObject )
	{
		pvectorObject->clear();
		std::map<KEY, OBJTYPE>::iterator it;
		for( it = m_mapObject.begin(); it != m_mapObject.end(); it++ )
		{
			OBJTYPE obj = it->second;
			pvectorObject->push_back( obj );
		}
	}
	return (int)m_mapObject.size();
} 

template<class KEY, class OBJTYPE >
int TMapContainer<KEY, OBJTYPE>::GetObjects( OBJTYPE buffer[], int nBufferSize )
{
	if( (int)m_mapObject.size() > nBufferSize )
	{
		assert( false );
		for( int i = 0; i < nBufferSize; i++ )
			buffer[i] = NULL;
		// 返回0有助于解决问题,返回一个合理的值往往会导致程序员忽略错误存在
		return 0;
	}
	std::map<KEY, OBJTYPE>::iterator it;
	int i = 0;
	for( it = m_mapObject.begin(); it != m_mapObject.end(); it++ )
	{
		OBJTYPE obj = it->second;
		//pvectorObject->push_back( obj );
		buffer[i++] = obj;
	}
	return i;
}

template<class KEY, class OBJTYPE >
void TMapContainer<KEY, OBJTYPE>::DestroyObject( OBJTYPE* pObj )
{
	if( m_pfnObjectDeleteCallBack )
		m_pfnObjectDeleteCallBack( pObj );
	else
	{
		delete *pObj;
		*pObj = NULL;
	}
}



#endif //__TMAPCONTAINER_INL__
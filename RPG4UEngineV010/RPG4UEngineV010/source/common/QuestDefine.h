/*////////////////////////////////////////////////////////////////////////
文 件 名：QuestDefine.h
创建日期：2007年11月2日
最后更新：2007年11月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __QUESTDEFINE_H__
#define __QUESTDEFINE_H__

#pragma once


#include "GameDefine.h"
#include "ConstArray.h"

#define QUEST_MAX	4

using namespace std;
/////////////////////////////////////////////////////////
namespace quest
{
	enum
	{
		 eQUESTSTATE_CANTCAST	= -2
		,eQUESTSTATE_DONE			= -1
		,eQUESTSTATE_DEACTIVE	= 0
		,eQUESTSTATE_CASTER		= 1
	};

	enum eQUEST_ACTION
	{
		 QUESTACTION_STEP
		,QUESTACTION_DISCARD
	};

	enum
	{
		 eQUESTREWARD_MONEY		= 0// 金钱
		,eQUESTREWARD_EXP				// 经验值
		,eQUESTREWARD_SKILLEXP		// 技能经验
		,eQUESTREWARD_ITEM			// 道具
		,eQUESTREWARD_VAR
		,eQUESTREWARD_SKILL

		,eQUESTREWARD_MAX
	};


	struct sQUEST_REWARD
	{
		sQUEST_REWARD()
			:m_RewardType(0)
			,m_VarID(0)
			,m_Value(0)
			,m_ItemName(0)
		{}
		BYTE		m_RewardType;							// 类型
		KEYWORD	m_VarID;
		DWORD		m_Value;									// 数值
		VRSTR		m_ItemName;	// 名字值
	};


	struct sQUEST_STATEINFO
	{
		CODETYPE	m_QuestID;	// 任务编号
		DWORD		m_State;		// 相关任务状态
	};

	struct sQUEST_RELATEINFO
	{
		sQUEST_RELATEINFO()
			:m_nVMId(-1)
			,m_State(INVALID_DWORD_ID)
			,m_Entrance(-1)
			,m_Desc(NULL)
			,m_QuestID(0)
		{}
		CODETYPE		m_QuestID;
		DWORD			m_State;
		int			m_nVMId;
		INT			m_Entrance;
		VRSTR			m_Desc;
	};

	// 任务状态
	struct sHERO_QUESTINFO
	{
		sHERO_QUESTINFO()
		{
			m_bReqInfoed = FALSE;
		}
		CODETYPE		m_QuestID;
		DWORD			m_QuestData;
		BOOL			m_bFinished;
		BOOL			m_bReqInfoed;
	};


 
	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	struct sQUEST_STEPINFO
	{
		sQUEST_STEPINFO()
		{
			m_StepID				= 0;
			m_StepDesc[0]		= 0;
			m_StepRequireNum	= 0;
		}

		UINT				m_StepID;
		UINT				m_StepRequireNum;
		sQUEST_REWARD	m_StepRequires		[MAX_QUESTNEXTSTATE];
		TCHAR				m_StepDesc			[MAX_QUESTDESC];

		BOOL AddState( const sQUEST_REWARD* pReward )
		{
			if( m_StepRequireNum >= MAX_QUESTNEXTSTATE )
				return FALSE;
			m_StepRequires[m_StepRequireNum++] = *pReward;
			return TRUE;
		}
	};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
	struct sQUEST_INFO
	{
		CODETYPE				m_QuestID;				// 任务Id

		TCHAR					m_Name		[MAX_NAME_LENGTH];
		TCHAR					m_QuestDesc	[MAX_QUESTDESC];
		sQUEST_STEPINFO	m_StepInfos	[MAX_QUESTSTATE];
		UINT					m_StepNum;
		sQUEST_REWARD		m_Rewards	[MAX_QUESTREWARD];
		UINT					m_RewardNum;


		BOOL					m_CanRepeate;					// 是否重复试练
		BYTE					m_QuestLV;						// 任务等级
		BYTE					m_LimitLV;						// 接受此任务玩家最低等级
		BYTE					m_LimitLVMin;					// 任务下限
		BYTE					m_LimitLVMax;					// 任务上限
		INT					m_CharType;						// 职业
		WORD					m_Reputation;					// 声望
		INT					m_QuestLimits[QUEST_MAX];	// 子任务需求

		sQUEST_INFO()
		{
			m_QuestID		= (CODETYPE)-1;
			m_CanRepeate	= FALSE;
			m_QuestLV		= 0;
			m_LimitLV		= 0;
			m_LimitLVMin	= 0;
			m_LimitLVMax	= 100;
			m_Reputation	= 0;
			m_StepNum		= 0;
			m_RewardNum		= 0;
			m_CharType		= 0;
			m_Name[0]		= 0;
			m_QuestDesc[0]	= 0;

			for( int n=0; n<QUEST_MAX; n++)
				m_QuestLimits[n] = -1;

			for( int n=0; n<MAX_QUESTSTATE; n++)
				m_StepInfos[n].m_StepID = n;

		}

		BOOL AddQuestState( const sQUEST_STEPINFO* pState )
		{
			if( m_StepNum >= MAX_QUESTSTATE)
				return FALSE;
			m_StepInfos[m_StepNum++] = *pState;
			return TRUE;
		}

		sQUEST_STEPINFO* GetLastQuestState()
		{
			if( m_StepNum-1 < 0 )
				return NULL;
			return &m_StepInfos[m_StepNum-1];
		}

		bool AddReward( const sQUEST_REWARD* pReward, bool bReward )
		{
			bReward;

			if ( pReward )
			{
				if( m_RewardNum >= MAX_QUESTREWARD )
					return false;
				m_Rewards[m_RewardNum++] = *pReward;
			}

			return true;
		}
	};

	typedef vector<sQUEST_INFO*>			QuestVector;
	typedef vector<sQUEST_STATEINFO>		QuestStateVector;
};

#endif //__QUESTMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：BaseDefine.h
创建日期：2007年12月12日
最后更新：2007年12月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BASEDEFINE_H__
#define __BASEDEFINE_H__
#pragma once

#define OUT	/*输出参数*/
#define IN	/*输入参数*/


/*////////////////////////////////////////////////////////////////////////
	颜色操作
/*////////////////////////////////////////////////////////////////////////
#define COLOR_RGBA( r, g, b, a)	( ( COLOR) (((BYTE)(a) & 0xff) << 24) | (((BYTE)(r) & 0xff) << 16) | (((BYTE)(g) & 0xff) << 8) | (((BYTE)(b) & 0xff)))
#define COLOR_ARGB( a, r, g, b)	COLOR_RGBA( r, g, b, a)
#define COLOR_RGB( r, g, b)		COLOR_RGBA( r, g, b, 0xff)
#define COLOR_A(clr)					((BYTE)( ( (clr) >> 24) & 0xff))
#define COLOR_R(clr)					((BYTE)( ( (clr) >> 16) & 0xff))
#define COLOR_G(clr)					((BYTE)( ( (clr) >>  8) & 0xff))
#define COLOR_B(clr)					((BYTE)( (clr) & 0xff))

#define COLORToD3DCOLOR( clr)		( clr)
#define COLORToOpenGL(clr)			( COLOR_R(clr) | ( COLOR_G(clr) << 8) | ( COLOR_B(clr) << 16) | ( COLOR_A(clr) << 24))

//获取颜色值的百分点
#define COLOR_PERCENT(clr,fMul)	COLOR_RGBA( (BYTE)((float)COLOR_R( clr) * fMul),\
															(BYTE)((float)COLOR_G( clr) * fMul),\
															(BYTE)((float)COLOR_B( clr) * fMul),\
															COLOR_A(clr))
#define COLORA_PERCENT(clr,fMul)	COLOR_RGBA( (BYTE)((float)COLOR_R( clr) * fMul),\
															(BYTE)((float)COLOR_G( clr) * fMul),\
															(BYTE)((float)COLOR_B( clr) * fMul),\
															(BYTE)((float)COLOR_A( clr) * fMul))
//从c1中依据c2相应通道与255比值的颜色值
#define COLOR_MULTIPLY( c1, c2)	COLOR_RGBA( (BYTE)((float)COLOR_R(c1) * (float)COLOR_R(c2)/255.0f),\
															(BYTE)((float)COLOR_G(c1) * (float)COLOR_G(c2)/255.0f),\
															(BYTE)((float)COLOR_B(c1) * (float)COLOR_B(c2)/255.0f),\
															(BYTE)((float)COLOR_A(c1) * (float)COLOR_A(c2)/255.0f))

//直接改变相应通道的颜色值
#define COLOR_WITH_R( clr, r)			( ( (clr) & 0xff00ffff) | ( ( (BYTE)(r) & 0xff) << 16))
#define COLOR_WITH_G( clr, g)			( ( (clr) & 0xffff00ff) | ( ( (BYTE)(g) & 0xff) << 8))
#define COLOR_WITH_B( clr, b)			( ( (clr) & 0xffffff00) | ( ( (BYTE)(b) & 0xff)))
#define COLOR_WITH_A( clr, a)			( ( (clr) & 0x00ffffff) | ( ( (BYTE)(a) & 0xff) << 24))



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define	__NOP()			__asm nop



#define VCHARID_NULL			((VCHARID)-1)
#define VOBJID_NULL			(INVALID_VOBJ_ID)
#define TILE_NULL				((TILEINDEX)-1)


/*////////////////////////////////////////////////////////////////////////
位检测...
/*////////////////////////////////////////////////////////////////////////
#define	_BIT(n)					(1<<(n))
#define	_MASK(n)					((1<<(n))-1)

#define _BIT_INDEX(x,b)			(((x) & _BIT(b)) != 0)
#define _BIT_TEST(x,b)			(((x) & (b)) == (b))
#define _BIT_EXIST(x,b)			(((x) & (b)) != 0)
#define _BIT_REMOVE(x,b)		((x) = ((x) & ~(b)))
#define _BIT_ADD(x,b)			((x) = ((x) | (b)))	

#define BIT_CHECK(x,b)			(((x) & (b)) ? TRUE:FALSE)
#define BIT_REMOVE(x,b)			((x) = ((x) & ~(b)))
#define BIT_ADD(x,b)				((x) = ((x) | (b)))	



/*////////////////////////////////////////////////////////////////////////
基本串转换宏
/*////////////////////////////////////////////////////////////////////////
//用法如：TAG('abcd')
#define TAG(x)				(((x>>24)&0xff) |((x>>8)&0xff00)|((x&0xff00)<<8) | ((x&0xff)<<24) )
#define MAP_TAG(x)		TAG(x)
#define TO_STRING(str)	#str
#define FtoDW(x)			*((DWORD*)(&x))



#endif //__BASEDEFINE_H__
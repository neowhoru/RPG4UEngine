/*////////////////////////////////////////////////////////////////////////
文 件 名：typedefine.h
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __DATATYPEDEFINE_H__
#define __DATATYPEDEFINE_H__
#pragma once

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef float					REAL;
typedef INT						KEYWORD;	
typedef DWORD					COLOR;
typedef WORD					MSG_TYPE;
typedef WORD					PORTTYPE;	///端口类型

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD					IDTYPE;
typedef DWORD					ID_TYPE;
typedef DWORD					KEYTYPE;				///< 
typedef DWORD					KEY_TYPE;			///< 
typedef DWORD					CODETYPE;			///< 
typedef DWORD					CODE_TYPE;			///< 

typedef INT						TILEINDEX;        ///< 注意... 目前TileWorld对Tile索引已经锁定 int类型了
typedef WORD					TILEPOS;
typedef WORD					TILETYPE;

typedef DWORD					RESOURCEID;
typedef DWORD					RESOURCE_ID;
typedef DWORD					MATERIALTYPE;
typedef DWORD					MATERIAL_TYPE;

typedef SHORT					TEXTUREID;

typedef INT						FIELDID;
typedef INT						FIELD_ID;
typedef ULONGLONG				MONEY;				///< 
typedef DWORD					MAPCODE;				///< 
typedef DWORD					MAP_CODE;			///< 
typedef WORD					MONSTERCODE;		///< 
typedef WORD					MONSTER_CODE;		///< 
typedef DWORD64				GROUPTYPE;
								
typedef int						SOUNDINDEX;
typedef int						SOUND_INDEX;		///< sound index 
typedef DWORD					EFFECTINDEX;
typedef DWORD					EFFECT_INDEX;
typedef DWORD              RESOURCECODE;       
typedef DWORD              RESOURCE_CODE;       

//游戏角色ID
typedef DWORD					VOBJID;
typedef VOBJID					VCHARID;
typedef WORD					GameCharID;

typedef unsigned __int64	OCT_INDEX;		//因为无限地图较大...需要增加深度，扩大ID宽度
typedef unsigned __int64	OCTINDEX;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef BYTE				DURATYPE;			///< 
typedef BYTE				DURA_TYPE;			///< 
typedef BYTE				SLOTPOS;				///< Slot位置
typedef BYTE				SLOT_POS;			///< 
typedef BYTE				POSTYPE;				///< 同上
typedef BYTE				POS_TYPE;			///< 
typedef BYTE				SLOTINDEX;			///< SlotContainer类型
typedef BYTE				SLOTIDX;				///< SlotContainer类型
typedef BYTE				SLOT_INDEX;			///< 
typedef WORD				QUESTPOS;			///< Quest位置
typedef WORD				SLOTCODE;			///< Slot中对象编号
typedef WORD				SLOT_CODE;			///< 
typedef WORD				LEVELTYPE;			///< item, character level type
typedef WORD				LEVEL_TYPE;			///< item, character level type
typedef DWORD				DBSERIAL;			///< 
typedef DWORD				SERIALTYPE;			///< 
typedef DWORD				SERIAL_TYPE;		///< 

typedef DWORD				PLAYERKEY;
typedef DWORD				PLAYER_KEY;

typedef WORD				DAMAGETYPE;			///< 
typedef WORD				DAMAGE_TYPE;		///< 

typedef WORD				HPTYPE;				///< 
typedef WORD				MPTYPE;				///< 
typedef INT					GPTYPE;
typedef INT					UPTYPE;
typedef short				TEXTURE_ID;

/*////////////////////////////////////////////////////////////////////////
与数据库相关的类型
/*////////////////////////////////////////////////////////////////////////
typedef INT					CHARGUID;
typedef INT					CHAR_GUID;
typedef INT					USERGUID;
typedef INT					USER_GUID;

typedef INT					GUILDGUID;
typedef INT					GUILD_GUID;
typedef INT					GUILDMARKIDX;
typedef INT					GUILDMARK_IDX;





/*////////////////////////////////////////////////////////////////////////
数据类型声明
/*////////////////////////////////////////////////////////////////////////
#define VRU_DECLARE_HANDLE(name)			struct name##__ { int unused; }; typedef struct name##__ *name
#define VRU_DECLARE_HANDLE_TEXT(name,cname)	\
			struct name##__ {\
				char szData[sizeof(int)/sizeof(char)];\
				inline operator char*(){return szData;}\
				inline operator const char*(){return (const char*)szData;}\
				inline const int IsEmpty() const{return this==0 || *szData==0;}\
			}; \
			typedef struct name##__ *name;\
			typedef	const	name		cname;

////////////////////////////////////////////
//VRSTR类型
VRU_DECLARE_HANDLE_TEXT(VRSTR, VRCSTR);
typedef VRSTR	VRPATH;




#pragma pack(push,1)
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
union TAG_CODE
{
	IDTYPE	m_ID;
	char		m_Text[4];
};

#pragma pack(pop)


#endif //__TYPEDEFINE_H__
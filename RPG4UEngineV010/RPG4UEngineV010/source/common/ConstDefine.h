/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstDefine.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTDEFINE_H__
#define __CONSTDEFINE_H__
#pragma once


#include "BaseDefine.h"
#include "ConstAttribute.h"
#include "ConstSkill.h"
#include "ConstServer.h"
#include "ConstCharState.h"
#include "ConstNPC.h"
#include "ConstCharacter.h"
#include "ConstMap.h"
#include "ConstGuild.h"
#include "ConstPlayer.h"
#include "ConstItem.h"
#include "ConstSound.h"




////////////////////////////////////////////////////
//	ObjectID编号使用范围，双端共用
enum eOBJECT_KEY
{
	 HERO_OBJECT_KEY				= 1		//	主角ID	适用于本地
	,PLAYER_OBJECT_KEY			= 10		//	玩家		
	,MONSTER_OBJECT_KEY			= 50000	//	怪物使用
	,NPC_OBJECT_KEY				= 100000	//	NPC		
	,NONCHARACTER_OBJECT_KEY	= 150000//	非人物	
	,MAP_OBJECT_KEY				= 200000	// 地图对象	
	,ITEM_OBJECT_KEY				= 250000//	物件		
	,MAX_OBJECT_KEY				= 300000	
};


/*////////////////////////////////////////////////////////////////////////
名称显示类型 
/*////////////////////////////////////////////////////////////////////////
enum eOBJNAME_STATE
{
	 OBJNAME_CLOSED				= 0 //关闭
	,OBJNAME_SHOWPLAYER			= _BIT(0) //玩家名称
	,OBJNAME_SHOWHERO				= _BIT(1) //主角名称
	,OBJNAME_SHOWMONSTER			= _BIT(2) //怪物名称
	,OBJNAME_SHOWNPC				= _BIT(3) //Npc名称
	,OBJNAME_SHOWITEM				= _BIT(4) //物品名称
	,OBJNAME_SHOWALL				= _MASK(5) //全部		
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eOBJECT_TYPE
{
	OBJECT_OBJECT			=  _BIT( 1 ),
	CHARACTER_OBJECT		= (_BIT( 2 ) | OBJECT_OBJECT ),		//人物
	NONCHARACTER_OBJECT	= (_BIT( 3 ) | OBJECT_OBJECT ),		//非人物
	CLONE_OBJECT			= (_BIT( 4 ) | CHARACTER_OBJECT ),	//复制体
	PLAYER_OBJECT			= (_BIT( 5 ) | CHARACTER_OBJECT ),	//玩家
	NPC_OBJECT				= (_BIT( 6 ) | CHARACTER_OBJECT ),	//NPC
	HERO_OBJECT				= (_BIT( 7 ) | PLAYER_OBJECT ),		//英雄主角
	MONSTER_OBJECT			= (_BIT( 8 ) | NPC_OBJECT ),			//普通怪物
	SUMMON_OBJECT			= (_BIT( 9 ) | NPC_OBJECT ),
	MERCHANT_OBJECT		= (_BIT(10 ) | NPC_OBJECT ),
	MAPNPC_OBJECT			= (_BIT(11 ) | NPC_OBJECT ),
	EFFECT_OBJECT			= (_BIT(12 ) | MONSTER_OBJECT ),
	BUILDING_OBJECT		= (_BIT(13 ) | MONSTER_OBJECT ),		//建筑
	MONSTER_BOSS			= (_BIT(14 ) | MONSTER_OBJECT ),		//Boss怪物
	MONSTER_LEADER			= (_BIT(15 ) | MONSTER_OBJECT ),		//精英怪物
	MONSTER_GOD				= (_BIT(16 ) | MONSTER_OBJECT ),		//神话怪物
	COLLECTION_OBJECT		= (_BIT(17 ) | MONSTER_OBJECT ),		//收集类物件
	COLLECTION_HERB		= (_BIT(18 ) | COLLECTION_OBJECT ),	//草药
	COLLECTION_MINE		= (_BIT(19 ) | COLLECTION_OBJECT ),	//矿物
	COLLECTION_WOOD		= (_BIT(20 ) | COLLECTION_OBJECT ),	//木头
	COLLECTION_TREASURE	= (_BIT(21 ) | COLLECTION_OBJECT ),	//宝箱

	ITEM_OBJECT				= (_BIT(25 ) | NONCHARACTER_OBJECT ),
	MAP_OBJECT				= (_BIT(26 ) | NONCHARACTER_OBJECT ),
	MONEY_OBJECT			= (_BIT(27 ) | ITEM_OBJECT  ),

	CAMERA_OBJECT			=  _BIT(30 ),  
	MAX_OBJECT				=  _BIT(31 ),

};





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSERIALIZE
{
	 SERIALIZE_STORE
	,SERIALIZE_LOAD
};





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSCRIPT_TYPE
{
	 SCRIPTTYPE_NULL				= -1	// 末知
	,SCRIPTTYPE_NPC				=0		// NPC触发
	,SCRIPTTYPE_QUEST				=1		// 任务触发
	,SCRIPTTYPE_LEVELUP			=2		// 等级触发
	,SCRIPTTYPE_NPCDEAD			=3		// 怪物死亡触发
	,SCRIPTTYPE_ITEMUSING		=4		// 使用道具触发
	,SCRIPTTYPE_AREA_ENTER		=5		// 进入区块触发
	,SCRIPTTYPE_AREA_LEAVE		=6		// 离开区块触发
	,SCRIPTTYPE_DATETIME			=7		// 真实时间触发
	,SCRIPTTYPE_GAMETIME			=8		// 游戏时间触发
	,SCRIPTTYPE_PLAYERONLINE	=10	// 上线触发
	,SCRIPTTYPE_PLAYERCREATE	=11	// 创建玩家
	,SCRIPTTYPE_PLAYERDEAD		=12	// 玩家死亡

	,SCRIPTTYPE_MAX
};









/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eINVISIABLE_TYPE
{
	 INVISIABLE_NONE		= 0	///<
	,INVISIABLE_SET		= 1	///< 设置隐形
	,INVISIABLE_CANCEL	= 2	///< 取消隐形
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eVALUE_TYPE
{
	 VALUE_TYPE_VALUE					= 1	// 普通值
	,VALUE_TYPE_PERCENT_PER_CUR	= 2	// 当前值百分比
	,VALUE_TYPE_PERCENT_PER_MAX	= 3	// 最大值百分比
	,VALUE_TYPE_RATIO_VALUE			= 5	// 机率值
};



enum eTIP_TYPE
{
	 eTIP_NORMAL			 =	0 
	,eTIP_TRADE				 =	1
	,eTIP_FRIEND			 =	2
	,eTIP_LEVELUP			 =	3
	,eTIP_REWARD			 =	4
	,eTIP_REMEMBER			 =	5
	,eTIP_ADDGOODS			 =	6
	,eTIP_QUEST				 =	7
	,eTIP_DEATH				 =	8
	,eTIP_EQUIP				 =	9
	,eTIP_SKILL				 =	10
	,eTIP_CHAT				 =	11
	,eTIP_TEAM				 =	12
	,eTIP_PK					 =	13
	,eTIP_PROPERTY			 =	14
	,eTIP_ADD				 =	15
	,eTIP_TAKE				 = 16
	,eTIP_HELP				 = 17
	,eTIP_MAX
};



#endif //__CONSTDEFINE_H__
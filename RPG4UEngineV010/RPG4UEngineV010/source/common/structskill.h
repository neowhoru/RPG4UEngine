/*////////////////////////////////////////////////////////////////////////
文 件 名：StructSkill.h
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTSKILL_H__
#define __STRUCTSKILL_H__
#pragma once


#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sVSKILLINFO_BASE
{
	enum
	{
		 ACTION_NUM	= 3
		,ATTACK_NUM	= 2
	};
	CODETYPE		m_Code;									///技能编号
	VRSTR			m_sName;									///技能名字
	LEVELTYPE	m_SkillLV;								///给自方附加状态级别
	VRSTR			m_arActions[ACTION_NUM];			///技能动作名字
	DWORD			m_dwActionTimeMin;					///技能动作最短时间
	DWORD			m_dwActionTimeMax;					///技能动作最长时间
	DWORD			m_dwPrepareTime;						///吟唱时间
	VRSTR			m_sIntonateAction;					///吟唱准备动作
	EFFECTINDEX	m_arIntonateEffect[ATTACK_NUM];	///吟唱特效，0为男性特效，1为女性特效，以下同
	EFFECTINDEX	m_arAttackerEffect[ATTACK_NUM];	///攻击者特效
	EFFECTINDEX	m_arSkillEffect	[ATTACK_NUM];	///技能特效
	EFFECTINDEX	m_arTargetEffect	[ATTACK_NUM];	///目标特效
	BYTE			m_byHoldSkill;							///是否为维持技能
	VRSTR			m_sHoldAction;							///维持动作
	CODETYPE		m_HoldEffectID;						///维持技能特效ID
	BYTE			m_byEmission;							///是否发射技能
	BYTE			m_byAssault;							///是否冲锋技能
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSKILLINFO_COMMON
{
	enum
	{
		 OVERSTAT_NUM		= 2
		,SKILL_TYPE			= 0
		,STYLE_TYPE			= 1
	};
	SLOTCODE		m_SkillCode;			// 
	SLOTCODE		m_SkillClassCode;		// 
	BYTE			m_byType;				// enum { ABILITY_TYPE, SKILL_TYPE };

	VRSTR			m_SkillName;
	CODETYPE	  	m_VItemCode;								// VItem
	CODETYPE	  	m_VSkillCode;								// VSkill

	LEVELTYPE	m_wRequireLV;								// 要求等级
	LEVELTYPE	m_wSkillLV;									// 技能等级
	LEVELTYPE	m_wMaxLV;									// 可用等级
	LEVELTYPE	m_wOverLV;									// 封顶等级
	BYTE			m_byRequireSkillPoint;					// 升级要求技能点
	DWORD		  	m_dwClassDefine;							// 职业要求（可组合）


	BOOL			IsStyle() ;
	BOOL			IsSkill() ;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSKILLINFO_BASE : public sSKILLINFO_COMMON
{
	enum
	{
		 WEAPONDEFINE_NUM	= 4
	};

	int  			m_WeaponDefines[WEAPONDEFINE_NUM];		// 要求武器 eWEAPON_TYPE
	BYTE			m_byNeedStop;									// 发技能时，是否站立
	WORD			m_wFlyingLifeTime;							// 飞行时间

	BYTE			m_bySkillType;									// 技能类型 eSKILL_TYPE
	BYTE			m_bySkillUserType;							// 技能使用类型 SKILLUSER_TYPE
	BYTE			m_bySkillStatType;							// 属性方向类型
	WORD  		m_wRequireSkillStat[OVERSTAT_NUM];		// 属性方向要求
	BYTE  		m_byTarget;										// 目标范围类型 eSKILL_TARGET_TYPE
	WORD			m_wHPSpend;										// HP消耗
	WORD  		m_wMPSpend;										// MP消耗
	WORD			m_wSkillCasting;								// 吟唱时间
	WORD			m_wCoolTime;									// 冷却时间

	WORD		  	m_wSkillRange;                         // 有效范围
	BYTE  		m_byAttRangeform;								// 范围形状 eSKILL_AREA_TYPE
	WORD		  	m_wSkillArea;                          // 影响范围
	BYTE			m_byMaxTargetNo;								// 目标最大数量

	/////////////////////////////////////////////////
	BOOL        IsNonStopSkill();                
	BOOL			IsSummonSkill();						
	BOOL			IsActionSkill();
	BOOL			IsEmotionSkill();
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sABILITYINFO_BASE
{
	enum {PARAM_NUM=4};
	WORD			m_wAbilityID;			// 效力编号 eABILITY_TYPE
	BYTE			m_byRangeType;			// 范围类型 eABILITY_RANGE_TYPE
	WORD			m_wSuccessRate;		// 命中率
	INT			m_iOption1;				// 属性1
	INT			m_iOption2;				// 属性2
	INT			m_iParam[PARAM_NUM];	// 参数值 [0]为Option1 [1]为Option2
	WORD			m_wStateID;				// 状态编号

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSTYLEINFO_BASE : public sSKILLINFO_COMMON
{
	int  			m_WeaponDefines;							// 要求武器 eWEAPON_TYPE
	float  		m_fAttRange;								// 攻击范围


	VRSTR			m_arAttackActions[ATTACK_SEQUENCE_MAX];	// 攻击动作

	WORD			m_dwAttackTime[ATTACK_SEQUENCE_MAX];	// 攻击1时间	

	int		 	m_iAddDamage[ATTACK_SEQUENCE_MAX];		// 伤害增加 
	float  		m_fDamagePercent[ATTACK_SEQUENCE_MAX];// 伤害加成

	BYTE		  	m_byAttRangeform;							// 范围形状 eSKILL_AREA_TYPE
	float  		m_fStyleArea;								// 影响范围
	float  		m_fThirdDelay;								// 技能延迟

	int  			m_iAttackRate;								// 命中率
	int		  	m_iAvoidRate;								// 闪避率
	int  			m_iAttackSpeed;							// 攻速
	int			m_iBonusDefence;							// 防御
	WORD		  	m_wCriticalBonus;							// 爆击率
	float		  	m_fDefenseIgnore;							// 破防率

	float  			m_fPierceRate;							// 穿刺攻击率  
	float  			m_fPierceRange;						// 穿刺攻击范围
	float  			m_fStunRate;							// 晕眩率
	WORD  			m_wStunTime;							// 晕眩时间
	float  			m_fKnockBackRate;						// 击倒率
	float  			m_fKnockBackRange;					// 击倒范围
	float  			m_fDownRate;							// 趴下率

	WORD		 		m_wHPAbsorb;							// 吸血量
	float  			m_fHPAbsorbPer;						// 吸血比
	WORD				m_wMPAbsorb;							// 吸魔量
	float  			m_fMPAbsorbPer;						// 吸魔比
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sVSTATEINFO_BASE
{
	WORD			m_VStateCode;								
	VRSTR  		m_sStateName;
	LEVELTYPE	m_StateLV;			/// 状态等级
	EFFECTINDEX	m_EffectCode;		/// 状态特效
	DWORD			m_dwDuration;		/// 状态持续时间
	DWORD			m_dwFuncPeriod;	/// 状态影响间隔
	BYTE			m_byUseColor;		/// 是否使用状态颜色
	DWORD			m_dwColor;			/// 状态颜色 ARGB，包括透明色
	FLOAT			m_fGrowSize;		/// 体形变化
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSTATEINFO_BASE
{
	WORD			m_wStateID;
	VRSTR  		m_sStateName;
	WORD			m_VStateCode;
	CODETYPE		m_VItemCode;
	CODETYPE  	m_dwNamecode;							
	CODETYPE	  	m_dwDescCode;							
	DWORD			m_dwIconCode;							
	BYTE			m_byType;								
	TAG_CODE    m_EffectTag;                  
	BYTE        m_byEffectPos;                   
	VRSTR  		m_sTipFormat;
};



#pragma pack(pop)



#endif //__STRUCTSKILL_H__
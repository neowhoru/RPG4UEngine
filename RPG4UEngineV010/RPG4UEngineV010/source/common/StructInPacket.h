#ifndef __STRUCTINPACKET_H__
#define __STRUCTINPACKET_H__

#pragma once


#include <DataTypeDefine.h>
#include "ConstData.h"
#include "StructBase.h"

#pragma pack(push,1)

////////////////////////////////////

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sRESULT_MSG
{
	enum { MAX_BUFFER_SIZE = 1024*2 };
	BYTE		m_byMsg[MAX_BUFFER_SIZE];
	WORD		m_wSize;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sITEM_SLOT
{
	SLOTPOS			m_ItemPos;
	ITEM_STREAM		m_Stream;
};

struct sITEM_SLOTEX
{
	SLOTPOS				m_ItemPos;
	ITEMOPT_STREAM		m_Stream;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSKILL_SLOT
{
	SLOTPOS			m_SkillPos;
	SKILL_STREAM	m_Stream;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sQUICK_SLOT
{
	SLOTPOS			m_QuickPos;
	QUICK_STREAM	m_Stream;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSTYLE_SLOT
{
	SLOTPOS			m_StylePos;
	STYLE_STREAM	m_Stream;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sQUEST_SLOT
{
	QUESTPOS			m_QuestPos;
	QUEST_STREAM	m_Stream;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sVENDOR_ITEM_SLOT
{
	MONEY			m_Money;		
	DURATYPE		m_Dura;		
	SLOTPOS		m_VendorPos;
	SLOTPOS		m_OrgPos;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sVENDOR_ITEM_SLOTEX
{
	MONEY				m_Money;
	BYTE				m_bLocked;
	sITEM_SLOTEX	m_Stream;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sEVENT_SLOT
{
	SLOTCODE	m_ItemCode;
	BYTE		m_ItemCount;
};

struct sEVENT_SLOTEX : public sEVENT_SLOT
{
	INT			m_ItemSeq;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sEQUIP_ITEM_INFO
{
	BYTE				m_Count;
	sITEM_SLOTEX	m_Slot[MAX_EQUIPMENT_SLOT_NUM];

	int GetSize() 
	{
		return sizeof(sEQUIP_ITEM_INFO)
				-(MAX_EQUIPMENT_SLOT_NUM-m_Count)*sizeof(sITEM_SLOTEX);
	}
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sEQUIP_ITEM_INFO_BASE
{
	DWORD		m_dwObjectKey;
	BYTE		m_Count;

	int GetSize()	
	{ 
		return	sizeof(sEQUIP_ITEM_INFO_BASE) 
				+	sizeof(sITEM_SLOT)*m_Count; 
	}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sEQUIP_ITEM_INFO_BASEEX
{
	BYTE		m_Count;

	/////////////////////////////////////////////////
	int GetSize()	
	{
		return	sizeof(sEQUIP_ITEM_INFO_BASEEX) 
				+	sizeof(sITEM_SLOTEX)*m_Count;
	}
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define TOTALINFO_DECL2(NAME,UNIT,MAX_INVEN)\
			struct sTOTALINFO_##NAME\
			{\
				enum CONST_VAL { MAX_SLOT_NUM = MAX_INVEN  };\
				BYTE				m_InvenCount;\
				BYTE				m_TmpInvenCount;\
				UNIT				m_Slot[MAX_SLOT_NUM];\
				int GetSize() \
				{\
					return sizeof(sTOTALINFO_##NAME)\
							-(MAX_SLOT_NUM - m_InvenCount-m_TmpInvenCount)*sizeof(UNIT);\
				}\
			};

#define TOTALINFO_DECL3(NAME,UNIT,MAX_INVEN)\
			struct sTOTALINFO_##NAME\
			{\
				enum CONST_VAL{ MAX_SLOT_NUM = MAX_INVEN  };\
				BYTE				m_EquipCount;\
				BYTE				m_InvenCount;\
				BYTE				m_TmpInvenCount;\
				UNIT				m_Slot[MAX_SLOT_NUM];\
				int GetSize() \
				{\
					return sizeof(sTOTALINFO_##NAME)\
							-(MAX_SLOT_NUM-m_EquipCount-m_InvenCount-m_TmpInvenCount)*sizeof(UNIT);\
				}\
			};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
TOTALINFO_DECL2	(INVENTORY,		sITEM_SLOTEX,	MAX_INVENTORY_SLOT_NUM + MAX_TEMPINVENTORY_SLOT_NUM);
TOTALINFO_DECL2	(TRADE,			sITEM_SLOTEX,	MAX_TRADE_SLOT_NUM);
TOTALINFO_DECL3	(ITEM,			sITEM_SLOTEX,	MAX_INVENTORY_SLOT_NUM
																+ MAX_EQUIPMENT_SLOT_NUM
																+ MAX_TEMPINVENTORY_SLOT_NUM);






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _TOTALINFO_DECL(NAME,UNIT,MAX_NUM,COUNT)\
			struct sTOTALINFO_##NAME\
			{\
				enum CONST_VAL{ MAX_SLOT_NUM = MAX_NUM };\
				COUNT		m_Count;\
				UNIT		m_Slot[MAX_SLOT_NUM];\
				int GetSize() \
				{\
					return sizeof(sTOTALINFO_##NAME)\
							-(MAX_SLOT_NUM-m_Count)*sizeof(UNIT);\
				}\
			};

#define TOTALINFO_DECL(NAME,UNIT,MAX_NUM)		_TOTALINFO_DECL(NAME,UNIT,MAX_NUM,BYTE)
#define TOTALINFO_DECL_W(NAME,UNIT,MAX_NUM)	_TOTALINFO_DECL(NAME,UNIT,MAX_NUM,WORD)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
TOTALINFO_DECL		(SKILL,					sSKILL_SLOT,			MAX_SKILL_SLOT_NUM );
TOTALINFO_DECL		(WAREHOUSE,				sITEM_SLOTEX,			MAX_WAREHOUSE_SLOT_NUM  );
TOTALINFO_DECL		(VENDOR,					sVENDOR_ITEM_SLOTEX,	MAX_PERSONAL_VENDOR_SLOT_NUM  );
TOTALINFO_DECL		(ESTABLISHER_VENDOR,	sVENDOR_ITEM_SLOT,	MAX_PERSONAL_VENDOR_SLOT_NUM	);
TOTALINFO_DECL		(QUICK,					sQUICK_SLOT,			MAX_QUICK_SLOT_NUM);
TOTALINFO_DECL		(STYLE,					sSTYLE_SLOT,			MAX_STYLE_SLOT_NUM);
TOTALINFO_DECL		(EVENT_ITEM,			sEVENT_SLOT,			MAX_EVENT_INVENTORY_SLOT_NUM);
TOTALINFO_DECL		(EVENT_ITEM_EX,		sEVENT_SLOTEX,			MAX_EVENT_INVENTORY_SLOT_NUM);
TOTALINFO_DECL_W	(QUEST,					sQUEST_SLOT,			MAX_QUEST_SIZE);
//TOTALINFO_DECL(,		,		);
//TOTALINFO_DECL(,		,		);





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSTATE_INFO
{
	SLOTCODE		m_SkillCode;	
	WORD			m_wAbilityCode;
	bool			m_bReflect;	
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPLAYERINFO_VILLAGE
{
	WORD						m_wPlayerKey;
	char						m_szName[MAX_CHARNAME_LENGTH];
	float						m_fPos[3];
	WORD						m_wMoveSpeedRatio;	
	BYTE						m_byHair;				//
	BYTE						m_byHairColor;			//
	BYTE						m_bySex				: 2;	//
	BYTE						m_BehaveState		: 6;	//  ePLAYER_BEHAVE_STATE
	BYTE						m_byHeight			: 3;	//
	BYTE						m_byHelmetOption	: 1;	// 
	BYTE						m_byFace				: 4;	// 
	BYTE						m_byPCBangUser		: 1;	// 
	BYTE						m_byGMGrade			: 3;	// 
	BYTE						m_byCondition		: 1;	//  eCHAR_CONDITION
	BYTE						m_byClass			: 3;

	int	GetSize() 
	{
		return sizeof(sPLAYERINFO_VILLAGE); 
	}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPLAYERINFO_RENDER : public sPLAYERINFO_VILLAGE
{
	enum { MAX_STATE_INFO_SIZE = 16 };
	HPTYPE					m_HP;
	HPTYPE					m_MaxHP;
	LEVELTYPE				m_LV;
	SLOTCODE					m_SelectStyleCode;
	WORD						m_wAttSpeedRatio;			//
	BYTE						m_byCount;	// 

	int GetSize()	{ return sizeof(sPLAYERINFO_RENDER) + sizeof(sSTATE_INFO)*m_byCount; }
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sMONSTERINFO_RENDER
{
	DWORD						m_dwObjectKey;
	MONSTERCODE				m_dwMonsterCode;
	MONSTERCODE				m_dwNpcFuncCode;
	VECTOR3D					m_vPos;
	DWORD						m_dwHP;
	WORD						m_wMoveSpeedRatio;	// 
	WORD						m_wAttSpeedRatio;		// 

	//BYTE					m_byType;				//eNPC_FUNC_TYPE 
	BYTE						m_byCount;				//

	int GetSize()	{ return sizeof(sMONSTERINFO_RENDER) + sizeof(sSTATE_INFO)*m_byCount; }
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sITEMINFO_RENDER
{
	enum eFIELDITEMTYPE{ eFIELDITEM_ITEM, eFIELDITEM_MONEY };
	DWORD						m_dwObjectKey;
	PLAYERKEY				m_dwOwnerPlayerKey;
	BYTE						m_byFieldItemType;
	union
	{
		ITEMOPT_STREAM		m_ItemStream;
		MONEY					m_Money;
	};

	float						m_fPos[3];
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sGUILDINFO_RENDER
{
	BYTE						m_byExistGuild;
	GUILDMARKIDX			m_GuildMarkIdx;
	TCHAR						m_szGuildName[MAX_GUILDNAME_LENGTH];
	int GetSize()
	{
		return ((m_byExistGuild != 0)?sizeof(sGUILDINFO_RENDER):sizeof(m_byExistGuild));
	}
};

//////////////////////////////////////////
struct sVENDORINFO_RENDER
{
	BYTE	m_byTitleLen;
	TCHAR m_pszTitle[MAX_VENDOR_TITLE_LENGTH+1];
	int GetSize()
	{
		return (int)(sizeof(*this)- ((MAX_VENDOR_TITLE_LENGTH+1) - m_byTitleLen )*sizeof(TCHAR));
	}
};






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sCHARACTER_CLIENTPART
{
	BYTE					m_bySlot;
	TCHAR					m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE					m_byHeight;
	BYTE					m_byFace;
	BYTE					m_byHair;
	BYTE					m_byHairColor;	
	BYTE					m_bySex;			
	BYTE					m_byClass;
	LEVELTYPE			m_LV;
	DWORD					m_dwRegion;
	WORD					m_wX;
	WORD					m_wY;
	WORD					m_wZ;
	sEQUIP_ITEM_INFO	m_EquipItemInfo;
	int				GetSize()
	{
		return sizeof(sCHARACTER_CLIENTPART) - (MAX_EQUIPMENT_SLOT_NUM - m_EquipItemInfo.m_Count)*sizeof(sITEM_SLOTEX);
	}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sTILE_POS
{
	VECTOR3D		wvPos;
	TILEINDEX	m_TileIndex; ///

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sCHARINFO_GM
{
	char			m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE			m_byClass;
	LEVELTYPE	m_LV;
	VECTOR3D		m_vCurPos;
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sPLAYERINFO_PACKET
{
	DWORD		m_dwExp;
	DWORD		m_dwRemainStat;
	DWORD		m_dwRemainSkill;
	MONEY		m_Money;
	SLOTCODE	m_SelectStyleCode;
	HPTYPE	m_MaxHP;
	HPTYPE	m_HP;
	MPTYPE	m_MaxMP;
	MPTYPE	m_MP;
	WORD		m_wMoveSpeedRatio;
	WORD		m_wAttSpeedRatio;
	INT64		m_StateTime;
	TCHAR		m_szTitleID[MAX_TITLE_LENGTH];
	INT64		m_TitleTime;
	BOOL		m_bInventoryLock;
	SHORT		m_sStrength;
	SHORT		m_sDexterity;
	SHORT		m_sVitality;
	SHORT		m_sInteligence;
	SHORT		m_sSpirit;
	SHORT		m_sLearn;
	SHORT		m_sCredit;
	SHORT		m_sSkillStat1;
	SHORT		m_sSkillStat2;

	struct { 	
	WORD		m_byGMGrade		: 3;			//<  eCHAR_CONDITION
	WORD		m_byPCBangUser	: 1;
	WORD		m_byCondition	: 1;			//<
	WORD		m_byPKState		: 3;			//<
	WORD		m_byCharState	: 8;			//<
	};

	//BYTE		m_byHeight;
	//BYTE		m_byFace;
	//BYTE		m_byHair;
	INT		m_PlayLimitedTime;
	BYTE		m_byInvisibleOptFlag;	//< eINVISIBLE_OPTION_FLAG
};






#pragma pack(pop)

#endif // __STRUCTINPACKET_H__
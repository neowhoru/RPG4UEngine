/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstSkeleton.h
创建日期：2007年12月18日
最后更新：2007年12月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTSKELETON_H__
#define __CONSTSKELETON_H__
#pragma once

#include "ConstArray.h"

namespace skeleton
{
	enum enumBoneId
	{
	//	// --------------------------------------
	//	// 头部
		Head,				// 头部

	//	Ponytail1,
	//	Ponytail11,
	//	Ponytail2,
	//	Ponytail21,		// 马尾辫

	//	--------------------------------------
	//	盔甲部
	//	Bip01,			// root
	//	Neck,				// 颈
	//	Spine,			// 胸部
	//	Pelvis,			// 骨盆
	//	L_Clavicle,		
	//	R_Clavicle,		// 锁骨
	//	L_UpperArm,
	//	R_UpperArm,		// 大臂
	//	L_Thigh,
	//	R_Thigh,			// 大腿

	//	Tail,
	//	Tail1,			// 衣摆
	//	
	//	--------------------------------------
	//	手套部
	//	L_ForeArm,
	//	R_ForeArm,		// 前臂
	//	L_Hand,
	//	R_Hand,			// 手

	//	--------------------------------------
	//	鞋部
	//	L_Calf,
	//	R_Calf,			// 小腿
	//	L_Foot,
	//	R_Foot,			// 脚
	//	L_Toe0,
	//	R_Toe0,			// 脚尖

	//	道具
		L_HandItem,
		R_HandItem,		// 道具
	//	
		eMaxBone,
	};


	enum enumPartId
	{
		 eHairPart				=	0//头发
		,eHelmetPart			=	1//头盔
		,eFacePart				=	2//脸
		,eArmourPart			=	3//甲
		,eGlovePart				=	4//手套
									
		,eShoePart				=	5//鞋子
		,eRightHandItemPart	=	6//右手武器
		,eLeftHandItemPart	=	7//左手武器
		,eHeadWearPart			=	8//头饰
		,eLeftHandShieldPart	=	9//盾
									
		,eShoulderPart			=	10//护肩
		,eTrousersPart			=	11//裤子
		,eAccouterment			=	12//项链
		,eRightRing				=	13//右手戒指
		,erBangle				=	14//手镯
									
		,eNecklace				=	15//项链
		,eSash				   =	16//腰带
		,eLeftRing				=	17//左手戒指	

		,eMaxPart	
		,eMaxVisualPart		= eTrousersPart+1
	};


};

#endif //__CONSTSKELETON_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：StructBase.inl
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTBASE_INL__
#define __STRUCTBASE_INL__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline BOOL sNPCINFO_BASE::CanAttack()
{
	switch(m_byGrade)
	{
	case NPC_GRADE_TREASURE:
	case NPC_GRADE_METERIAL:		return FALSE;
	};
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline eVITEMTYPE sVITEMINFO_BASE::GetIconType()
{
	switch(m_dwType)
	{
	case VITEMTYPE_RESTORE:
	case VITEMTYPE_WEAPON:
	case VITEMTYPE_ARMOUR:
	case VITEMTYPE_SKILL:
	case VITEMTYPE_OTHER:
	case VITEMTYPE_ICON:
		return (eVITEMTYPE)m_dwType;

	case VITEMTYPE_ACTION:
		return VITEMTYPE_SKILL;
	case VITEMTYPE_STATUS:
		assert(!"不支持此类型的转换，需要直接参数调用");
		break;

	case VITEMTYPE_STONE:
	case VITEMTYPE_REEL:
	case VITEMTYPE_MATERIAL:
	case VITEMTYPE_MEDAL:
	case VITEMTYPE_TASK:
	case VITEMTYPE_TOOL:
	case VITEMTYPE_GEM:
	case VITEMTYPE_CREATEVITEMRULE:
	case VITEMTYPE_CARD:
	case VITEMTYPE_CRYSTAL:
		return VITEMTYPE_OTHER;
	}
	return VITEMTYPE_ICON;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline eENCHANT_MATERIAL_TYPE sITEMINFO_BASE::GetEnchantMaterialType()
{
	if(m_wType == ITEMTYPE_MATERIAL)
		return ENCHANTMATERIAL_COMPOUND;
	if(m_byMaterialType&ITEMUSETYPE_WEAPON)			return ENCHANTMATERIAL_WEAPON;
	else if(m_byMaterialType&ITEMUSETYPE_ARMOR)		return ENCHANTMATERIAL_ARMOR;
	else if(m_byMaterialType&ITEMUSETYPE_ACCESSORY)	return ENCHANTMATERIAL_ACCESSORY;
	else if(m_byMaterialType&ITEMUSETYPE_USE)			return ENCHANTMATERIAL_WASTE;
	else if(m_byMaterialType&ITEMUSETYPE_COMPOUND)	return ENCHANTMATERIAL_COMPOUND;
	return ENCHANTMATERIAL_NONE;
}

inline BOOL		sITEMINFO_BASE::IsSocket()			
{
	return ( m_wType == ITEMTYPE_SOCKET );
}


inline BOOL		sITEMINFO_BASE::IsMaterialWeapon()
{	return 	m_wType == ITEMTYPE_MATERIAL
			&&	(	m_byMaterialType == MATERIALTYPE_STONE 
				||	m_byMaterialType == MATERIALTYPE_WOOD );	
}

inline BOOL		sITEMINFO_BASE::IsMaterialArmour()
{
	return m_wType == ITEMTYPE_MATERIAL 
		&& (m_byMaterialType == MATERIALTYPE_SKIN); 
}

inline BOOL		sITEMINFO_BASE::IsWeapon()	
{
	return ( m_wEqPos == EQUIPPOS_WEAPON );	
}


inline BOOL		sITEMINFO_BASE::IsArmor()	
{
	return (	m_wEqPos == EQUIPPOS_ARMOR 
			|| m_wEqPos == EQUIPPOS_PROTECTOR 
			|| m_wEqPos == EQUIPPOS_HELMET 
			|| m_wEqPos == EQUIPPOS_PANTS
			|| m_wEqPos == EQUIPPOS_BOOTS 
			|| m_wEqPos == EQUIPPOS_GLOVE
			|| m_wEqPos == EQUIPPOS_BELT	
			|| m_wEqPos == EQUIPPOS_SHIRTS );
	}

inline BOOL		sITEMINFO_BASE::IsAccessary()		
{
	return (	m_wEqPos == EQUIPPOS_RING1 
			|| m_wEqPos == EQUIPPOS_RING2 
			|| m_wEqPos == EQUIPPOS_NECKLACE);
}

inline BOOL		sITEMINFO_BASE::IsRing()			
{	return(	m_wEqPos == EQUIPPOS_RING1 
			|| m_wEqPos == EQUIPPOS_RING2);
}

inline BOOL		sITEMINFO_BASE::IsNecklace()		
{
	return (	m_wEqPos == EQUIPPOS_NECKLACE);
}

inline BOOL		sITEMINFO_BASE::IsCanEquip()		
{
	return (	ITEMUSETYPE_EQUIP		& m_byMaterialType );
}
inline BOOL		sITEMINFO_BASE::IsCanUseWaste()	
{
	return (	ITEMUSETYPE_USE		& m_byMaterialType );
}
inline BOOL		sITEMINFO_BASE::IsCanCompound()
{
	return (	ITEMUSETYPE_COMPOUND & m_byMaterialType );
}

inline BOOL		sITEMINFO_BASE::IsPotion()			
{
	return(	ITEMWASTE_HPPOTION		== m_byWasteType 
			|| ITEMWASTE_MPPOTION		== m_byWasteType
			|| ITEMWASTE_HPMPPOTION		== m_byWasteType
			|| ITEMWASTE_HPRATE_POTION	== m_byWasteType
			|| ITEMWASTE_MPRATE_POTION	== m_byWasteType
			|| ITEMWASTE_HPMPRATE_POTION == m_byWasteType
			);
}


inline ePLAYER_TYPE sITEMINFO_BASE::GetFirstAvailableClass()
{
	for(INT n=PLAYERTYPE_BASE; n < PLAYERTYPE_MAX; n++)
	{
		if(_BIT_INDEX(m_wEquipClass, n))
			return (ePLAYER_TYPE)n;
	}

	return PLAYERTYPE_WARRIOR;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline BOOL			sSKILLINFO_COMMON::IsStyle() 
{
	return ( m_byType == STYLE_TYPE );
}
inline BOOL			sSKILLINFO_COMMON::IsSkill()
{
	return ( m_byType == SKILL_TYPE );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline BOOL sSKILLINFO_BASE::IsNonStopSkill()
{
	return  m_byNeedStop != 0;
}


inline BOOL sSKILLINFO_BASE::IsSummonSkill()
{
	return( m_bySkillUserType == SKILLUSER_SUMMONED );
}

inline BOOL sSKILLINFO_BASE::IsActionSkill()
{
	return BOOL( m_bySkillUserType == SKILLUSER_ACTION );
}

inline BOOL sSKILLINFO_BASE::IsEmotionSkill()
{
	return BOOL( m_bySkillUserType == SKILLUSER_EMOTICON );
}



#endif //__STRUCTBASE_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：BAList.inl
创建日期：2008年1月26日
最后更新：2008年1月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BALIST_INL__
#define __BALIST_INL__
#pragma once



template <class T>
BAList<T>::BAList()
{
	m_pHead = new LISTNODE;
	m_pTail = new LISTNODE;

	m_pHead->SetNext( m_pTail);
	m_pTail->SetPrev( m_pHead);

	m_nCount = 0;
}

template <class T>
BAList<T>::~BAList()
{
	RemoveAll();
	delete m_pTail;
	delete m_pHead;
}


template <class T>
inline BOOL BAList<T>::IsEmpty( void)	
{
	return ( m_pHead->GetNext() == m_pTail);
}


template <class T>
BANode<T>* BAList<T>::AddHead( T NewElement)
{
	LISTNODE* pNewNode = new LISTNODE;

	if ( pNewNode == NULL)
		return ( NULL);


	pNewNode->SetData( NewElement);

	pNewNode->SetNext( m_pHead->GetNext());
	( m_pHead->GetNext())->SetPrev( pNewNode);

	pNewNode->SetPrev( m_pHead);
	m_pHead->SetNext( pNewNode);

	m_nCount++;

	return ( pNewNode);
}

template <class T>
BANode<T>* BAList<T>::AddTail( T NewElement)
{
	LISTNODE* pNewNode = new LISTNODE;

	if ( pNewNode == NULL)
	{
		return ( NULL);
	}

	pNewNode->SetData( NewElement);

	pNewNode->SetPrev( m_pTail->GetPrev());
	( m_pTail->GetPrev())->SetNext( pNewNode);

	pNewNode->SetNext( m_pTail);
	m_pTail->SetPrev( pNewNode);

	m_nCount++;

	return ( pNewNode);
}


template <class T>
BANode<T>* BAList<T>::InsertBefore( LISTNODE* pNode, T NewElement)
{
	LISTNODE* pNewNode = new LISTNODE;

	if ( pNewNode == NULL)
	{
		return ( NULL);
	}

	pNewNode->SetData( NewElement);

	pNewNode->SetPrev( pNode->GetPrev());
	( pNode->GetPrev())->SetNext( pNewNode);

	pNewNode->SetNext( pNode);
	pNode->SetPrev( pNewNode);

	m_nCount++;

	return ( pNewNode);
}

template <class T>
BANode<T>* BAList<T>::InsertAfter( LISTNODE* pNode, T NewElement)
{
	LISTNODE* pNewNode = new LISTNODE;

	if ( pNewNode == NULL)
		return ( NULL);


	pNewNode->SetData( NewElement);

	pNewNode->SetNext( pNode->GetNext());
	( pNode->GetNext())->SetPrev( pNewNode);

	pNewNode->SetPrev( pNode);
	pNode->SetNext( pNewNode);

	m_nCount++;

	return ( pNewNode);
}


template <class T>
T BAList<T>::RemoveHead( void)
{
	LISTNODE* pNewNode = m_pHead->GetNext();
	
	if ( pNewNode == m_pTail)
	{
		T NullType;
		memset( &NullType, 0, sizeof( T));

		return ( NullType);
	}


	T Data = pNewNode->GetData();


	( pNewNode->GetNext())->SetPrev( m_pHead);
	m_pHead->SetNext( pNewNode->GetNext());
	

	delete pNewNode;

	m_nCount--;

	return ( Data);
}


template <class T>
T BAList<T>::RemoveTail( void)
{
	LISTNODE* pNewNode = m_pTail->GetPrev();
	
	if ( pNewNode == m_pHead)
	{
		T NullType;
		memset( &NullType, 0, sizeof( T));

		return ( NullType);
	}


	T Data = pNewNode->GetData();


	( pNewNode->GetPrev())->SetNext( m_pTail);
	m_pTail->SetPrev( pNewNode->GetPrev());
	

	delete pNewNode;

	m_nCount--;

	return ( Data);
}


template <class T>
T BAList<T>::RemoveNode( LISTNODE* &pNode)
{
	if ( pNode == NULL)
	{
		T NullType;
		memset( &NullType, 0, sizeof( T));

		return ( NullType);
	}

	T Data = pNode->GetData();
	LISTNODE* pNextNode = pNode->GetNext();


	( pNode->GetPrev())->SetNext( pNextNode);
	pNextNode->SetPrev( pNode->GetPrev());


	delete pNode;
	pNode = ( pNextNode != m_pTail) ? pNextNode : NULL;

	m_nCount--;

	return ( Data);
}


template <class T>
void BAList<T>::RemoveAll( void)
{

	( m_pTail->GetPrev())->SetNext( NULL);

	LISTNODE *pToSeek, *pToDelete;

	pToDelete = pToSeek = m_pHead->GetNext();
	while ( pToSeek)
	{
		pToSeek = pToSeek->GetNext();
		delete pToDelete;
		pToDelete = pToSeek;
	}

	m_pHead->SetNext( m_pTail);
	m_pTail->SetPrev( m_pHead);

	m_nCount = 0;
}



template <class T>
BANode<T>* BAList<T>::FindHead( void)
{
	if ( IsEmpty())
	{
		return ( NULL);
	}
	else
	{
		return ( m_pHead->GetNext());
	}
}

template <class T>
BANode<T>* BAList<T>::FindTail( void)
{
	if( IsEmpty())
	{
		return ( NULL);
	}
	else
	{
		return ( m_pTail->GetPrev());
	}
}

template <class T>
BANode<T>* BAList<T>::FindNode( T SearchValue)
{
	( m_pTail->GetPrev())->SetNext( NULL);

	LISTNODE *pToSeek = m_pHead->GetNext();

	while( pToSeek)
	{
		if ( pToSeek->GetData() == SearchValue)
		{
			( m_pTail->GetPrev())->SetNext( m_pTail);
			return ( pToSeek);
		}

		pToSeek = pToSeek->GetNext();
	}

	( m_pTail->GetPrev())->SetNext( m_pTail);

	return ( NULL);
}


template <class T>
BANode<T>* BAList<T>::GetPrev( LISTNODE* pNode)
{
	if ( m_pHead == pNode->GetPrev())
	{
		return ( NULL);
	}
	else
	{
		return ( pNode->GetPrev());
	}
}


template <class T>
BANode<T>* BAList<T>::GetNext( LISTNODE* pNode)
{
	if ( m_pTail == ( pNode->GetNext()))
	{
		return ( NULL);
	}
	else
	{
		return ( pNode->GetNext());
	}
}



template <class T>
T& BAList<T>::GetData( LISTNODE* pNode)
{
	if ( m_pHead == pNode || m_pTail == pNode)
	{
		T NullType;	
		memset( &NullType, 0, sizeof( T));

		return ( NullType);
	}
	else
	{
		return ( pNode->GetData());
	}
}

template <class T>
void BAList<T>::SetData( LISTNODE* pNode, T Data)
{
	pNode->SetData( Data);
}

#endif //__BALIST_INL__
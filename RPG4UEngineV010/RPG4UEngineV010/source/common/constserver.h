/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstServer.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTSERVER_H__
#define __CONSTSERVER_H__
#pragma once



/*////////////////////////////////////////////////////////////////////////
// 服务器种类
// 这个信息须由DBTable的信息同步完成
// 修正定义时(注意)!!!!!!!
// 相关 : ServerUtil.h中 util::GetServerTypeLabel()
/*////////////////////////////////////////////////////////////////////////
enum eSERVER_TYPE
{
	 UNKNOWN_SHELL 		= 0
	,GATE_SHELL				= 1
	,FIELD_SHELL			= 2
	,BATTLE_SHELL			= 3
	,PLAYER_DBPROXY		= 4

	,CHAT_SHELL				= 5
	,MASTER_SHELL			= 6
	,GUILD_SHELL			= 7
	,USER_DBPROXY			= 8
	,AI_SHELL				= 9

	,LOGIN_SHELL			= 10	// 放入到DB
	,ADMIN_SHELL			= 11   // Agent Server
	,TEMP_SHELL				= 12
	,GAME_SHELL				= 13	//< 即FIELD_SHELL和BATTLE_SHELL

	,SHELL_MAX
};

////////////////////////////////////////
enum eSERVER_STATE
{
	 SERVER_ABNORMAL_DISCONNECTED
	,SERVER_BOOTING
	,SERVER_NORMAL
};


/////////////////////////////////////////
//日志级别
enum eLOG_LEVEL
{
	 LOG_CRITICAL			= 1
	,LOG_MIDDLE				= 2
	,LOG_FULL				= 3
};	


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eLOGOUT_REASON
{
	 LOGOUT_NORMAL					= 0			// 
	,LOGOUT_AUTH_REQUEST			= 1			// 
	,LOGOUT_USER_NOT_FOUND      = 2			// 
	,LOGOUT_HACKING_USER        = 3			// 
	,LOGOUT_WRONG_AUTHKEY       = 4			// 
	,LOGOUT_WRONG_VERSION       = 5			// 
	,LOGOUT_ETC                 = 6
};	


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eDISCONNECT_REASON
{
	 DISCONNECT_UNKNOWN_REASON = 0
	,DISCONNECT_MOVE_SYNC_BROKEN
	,DISCONNECT_KBMOVE_SYNC_BROKEN
	,DISCONNECT_ATTACK_SYNC_BROKEN

	,DISCONNECT_TOO_LONG_THRUST_DISTANCE
	,DISCONNECT_TOO_MANY_ATTACK_TARGETS
	,DISCONNECT_TOO_FAST_ATTACK

	,DISCONNECT_INCORRECT_ATTACK_SEQUENCE
	,DISCONNECT_INVALID_ATTACK_SEQUENCE
	,DISCONNECT_INCORRECT_WEAPON_ON_STYLE_ATTACK
	,DISCONNECT_INCORRECT_CLASS_ON_STYLE_ATTACK

	,DISCONNECT_NOT_ACQUIRED_STYLE
	,DISCONNECT_SPEED_HACK
	,DISCONNECT_GM_FORCE_DISCONNECT		

};


enum eDBCHAR_STATE
{
	 DBCHAR_STATE_CHAT_BLOCK
	,DBCHAR_STATE_BEHAVE_BLOCK
};


#endif //__CONSTSERVER_H__
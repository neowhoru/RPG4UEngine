/*////////////////////////////////////////////////////////////////////////
文 件 名：constdata.h
创建日期：2006年8月26日
最后更新：2006年8月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTDATA_H__
#define __CONSTDATA_H__
#pragma once

#include "DataTypeDefine.h"

#define	INVALID_INT_ID			((INT)				-1)
#define	INVALID_BYTE_ID		((BYTE)				-1)
#define	INVALID_WORD_ID		((WORD)				-1)
#define	INVALID_DWORD_ID		((DWORD)				-1)

#define	INVALID_KEYWORD		((KEYWORD)			-1)
#define	INVALID_VOBJ_ID		((VOBJID)			-1)
#define	INVALID_MATERIALTYPE	((MATERIALTYPE)	-1)
#define	INVALID_GUILD_ID		((DWORD)				-1)
#define	INVALID_GAMECHARID	((GameCharID)		-1)
#define  INVALID_OCTINDEX		((OCT_INDEX)		-1)
#define  INVALID_KEYTYPE		((KEYTYPE)			-1)
#define  INVALID_DURATYPE		((DURATYPE)			-1)
#define  INVALID_SERIALTYPE	((SERIALTYPE)		-1)
#define  INVALID_LEVELTYPE		((LEVELTYPE)		-1)
#define  INVALID_FIELDID		((FIELDID)			-1)
#define  INVALID_MAPCODE		((MAPCODE)			-1)
#define  INVALID_TILEINDEX		((TILEINDEX)		-1)
#define  INVALID_PLAYERKEY		((PLAYERKEY)		-1)

#define  INVALID_SLOTPOS		((SLOTPOS)			-1)
#define  INVALID_POSTYPE		((SLOTPOS)			-1)
#define  INVALID_QUICKSLOT		((SLOTPOS)			-1)
#define  INVALID_SLOTINDEX		((SLOTINDEX)		-1)


#define	INVALID_IDTYPE			((IDTYPE)			0)
#define	INVALID_CODETYPE		((CODETYPE)			0)
#define	INVALID_SKILLCODE		((CODETYPE)			0)
#define  INVALID_SLOTCODE		((SLOTCODE)			0)
#define  INVALID_SOUNDINDEX	((SOUND_INDEX)		0)
#define	INVALID_EFFECTINDEX	((EFFECTINDEX)		0)
#define	INVALID_RESOURCE_ID	((RESOURCE_ID)		0)

#define	INVALID_TEXTURE_ID	((TEXTURE_ID)		-1)






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define FRAME_MOVE					(0x0001)
#define FRAME_RENDER					(0x0002)


#define	ICON_BLANK_ID				99		/// VItem空白图标ID
#define  BIG_ICON_SIZE				64		/// 大图标宽高



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
const DWORD 	 TOTALSECONDS_PERDAY		= 24*60*60;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
const DWORD MAX_ID_LENGTH				= 24;
const DWORD MAX_ACCOUNT_LENGTH		= 24;
const DWORD MAX_PASSWORD_LENGTH		= 24;
const DWORD MAX_ROOMTITLE_LENGTH		= 21 ;
const DWORD MAX_ROOMPASSWORD_LENGTH	= 7 ;		//
const DWORD MAX_MD5_BUF_LENGTH		= 64;

const DWORD MAX_NAME_LENGTH			= 32;
const DWORD MAX_OFFICIAL_NAME_LENGTH= 32 ;	//
const DWORD MAX_WORLD_NAME_LEN		= 32;	
const DWORD MAX_CHANNEL_NAME_LEN		= 32; 
const DWORD MAX_ITEMNAME_LENGTH		= 30 ;	// 
const DWORD MAX_ITEMNAME2_LENGTH		= 10 ;	// 物品第二名称长度（自定义名称）
const DWORD MAX_SKILLNAME_LENGTH		= 32 ;	// 
const DWORD MAX_MAPNAME_LENGTH		= 20 ;	// 

const DWORD MAX_CHARNAME_LENGTH		= 16 ;	//
const DWORD MAX_TITLE_LENGTH			= 16 ;	//
const DWORD MAX_SHOPNAME_LENGTH		= 32 ;	//
const DWORD MAX_VENDOR_TITLE_LENGTH	= 80;		//


const DWORD MAX_IP_LENGTH				= 32;		
const DWORD MAX_SERVER_AMOUNT			= 1024;	//在登录服务列表中，最多服务线总数量
const DWORD MAX_INFO_LENGTH			= 64;
const DWORD MAX_CHAT_LENGTH			= 100;
const DWORD MAX_NOTICE_LENGTH			= 1024;
const DWORD MAX_AUTH_SERIAL_LENGTH	= 64;		///< 验证码缓冲
const DWORD MAX_SSN_KEY_LENGTH		= 64;		///<
const DWORD MAX_TIMEDATA_SIZE			= 20 ;	//
const DWORD MAX_STRING_LENGTH			= 256;
const DWORD MAX_CHATBUFFER_LENGTH	= 256;
const DWORD MAX_STR_CMD_LENGTH		= 100;
const DWORD MAX_DBNAME_LENGTH			= 64;

const DWORD	MAX_LOGPATH						= MAX_PATH;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
const DWORD		MAX_PATH_ROUTE					= 128; ///玩家最大节点数
const DWORD		MAX_ROUTE_SIZE					= 10;	///NPC行走最大节点数

const DWORD		EQUIPMENT_CHARACTER_KEY		= 999999;	///装备UI上预览人物ID
const CODETYPE	SKILLCODE_NORMAL_ATTACK		= 8100;
const DWORD 	TEMP_SERIALTYPE_VALUE		= 0;

const DWORD 	MAX_ABILITY_NUM				= 5  ;		/// 技能拥有效力数量
const BYTE		MAX_SHOT_RELOAD_COUNT		= 20;	

const DWORD		MAX_FRIEND_STAT_NUM			= 90;
const DWORD		MAX_FRIENDBLOCK_STAT_NUM	= 180;
const DWORD 	MAX_SUMMONED_NUM				= 10 ;	// 宠物数量
const DWORD 	MAX_AREA_ID_LENGTH			= 4  ;	// 


/*////////////////////////////////////////////////////////////////////////
Quest常量
/*////////////////////////////////////////////////////////////////////////
const DWORD 	MAX_QUESTDESC					= 256;		// 任务说明
const DWORD 	MAX_QUESTSTATEDESC			= 128;		// 任务状态说明
const DWORD 	MAX_QUESTNEXTSTATE			= 6	;		// 下一任务所需状态数量
const DWORD 	MAX_QUESTSTATE					= 10	;		// 任务状态数量
const DWORD 	MAX_QUESTREWARD				= 8	;		// 任务报酬
const DWORD 	MAX_QUEST_POOLSIZE			= 100;		// 最大任务单元池数
const DWORD 	MAX_QUEST_SIZE					= 3000;		// 最大任务数
const DWORD 	MAX_ACTIVEQUEST				= 10	;		// 最大活动任务数
const DWORD 	MAX_QUEST_LEVEL				= 100;		// 
const DWORD 	MIN_QUESTID						= 3000;		// 任务起始Id
const DWORD 	MAX_QUESTID						= MIN_QUESTID+MAX_QUEST_SIZE-1;	// 任务结束Id

const DWORD 	MAX_PLAYERVAR_SIZE			=  6000 ;		// 玩家变量数量
const DWORD 	MAX_TEMPVAR_SIZE				=  2000 ;		// 临时变量数量
const DWORD 	MAX_SYSVAR_SIZE				=  5000 ;		// 系统变量数量

//////////////////////////////////////////////////////////////////////////////
// Slot Pool
const DWORD 	MAX_EQUIPMENT_SLOT_NUM			= EQUIPPOS_MAX;
const DWORD 	MAX_INVENTORY_SLOT_NUM_PERPAGE= 6*4;
const DWORD 	MAX_INVENTORY_PAGE_NUM			= 5;
const DWORD 	MAX_WAREHOUSE_SLOT_NUM_PERPAGE= 6*4;
const DWORD 	MAX_WAREHOUSE_PAGE_NUM			= 2*2;
const DWORD 	MAX_INVENTORY_SLOT_NUM			= MAX_INVENTORY_SLOT_NUM_PERPAGE*MAX_INVENTORY_PAGE_NUM;
const DWORD 	MAX_TEMPINVENTORY_SLOT_NUM		= MAX_INVENTORY_SLOT_NUM_PERPAGE;
const DWORD 	MAX_WAREHOUSE_SLOT_NUM			= MAX_WAREHOUSE_SLOT_NUM_PERPAGE*MAX_WAREHOUSE_PAGE_NUM;
const DWORD 	MAX_SKILL_SLOT_NUM				= 100;
const DWORD 	MAX_TOTAL_SLOT_NUM				= MAX_INVENTORY_SLOT_NUM 
															+ MAX_EQUIPMENT_SLOT_NUM 
															+ MAX_SKILL_SLOT_NUM 
															+ MAX_WAREHOUSE_SLOT_NUM 
															+ MAX_TEMPINVENTORY_SLOT_NUM ;
const DWORD 	MAX_VENDOR_SLOT_NUM				= 5*25;
const DWORD 	MAX_EVENT_INVENTORY_SLOT_NUM	= 5*5;	
const DWORD 	MAX_PERSONAL_VENDOR_SLOT_NUM	= 24;
const DWORD 	MAX_QUICK_SLOT_NUM				= 12*4;
const DWORD 	MAX_STYLE_SLOT_NUM				= 4;
const DWORD 	MAX_SUMMON_SLOT_NUM				= 6;
const DWORD 	MAX_TRADE_SLOT_NUM				= 20;


////////////////////////////////////////
//等级相关配置
const DWORD 	MAX_LEVEL							= 100 ;
const DWORD 	MAX_ENCHANT_GRADE					= 12 ;		///< 物品强化级别数量
const DWORD 	MAX_SKILL_LEVEL					= 10 ;		// 
const DWORD 	MAX_ITEM_LEVEL						= 18	 ;		///< 物品层级数量

// 
const DWORD 	MAX_MONSTER_NUM					= 10000 ;	// 
const DWORD 	MAX_PLAYER_NUM						= 1000 ;	// 
const DWORD 	MAX_FIELDITEM_NUM					= 2000 ;	// 
const DWORD 	MAX_TARGET_NUM						= 32 ;		// 
const DWORD 	MAX_FIGHTING_ENERGY_NUM			= 3 ;		// 

const DWORD 	MAX_PARTYMEMBER_NUM				= 10 ;		// 
const DWORD 	MAX_CHARACTER_SLOT_NUM			= 5 ;		// 
const DWORD 	MAX_CHARACTER_LIST_NUM			= 5 ;		// 

///////////////////////////////////////
//视野范围内section数量
const DWORD 	VILLAGE_SECTOR_SIZE				= 40;		
const DWORD 	ROOM_SECTOR_SIZE					= 50	;	

///////////////////////////////////////
//更新tick
const DWORD 	 TICK_SERVER_UPDATE				= 20;			// 
const DWORD		 TICK_HEARTBEAT					= 20000;
const DWORD 	 TICK_PLAYER_MOVE_UPDATE		= 500;		// 

///////////////////////////////////////
const float 	  SCALE_SPEED_FACTOR				= 0.001f;	// 
const float 	  SCALE_SKILLRANGE_FACTOR		= 0.1f;	// 

///////////////////////////////////////
// GM 
const DWORD 	  GM_MAX_SKILLPOINT_UP_VALUE	=	100;
const DWORD 	  GM_MAX_STAT_UP_VALUE			=	100;
const DWORD 	  GM_MAX_CREATE_ITEM_NUM		=	1000;
const DWORD 	  GM_MAX_CREATE_MONEY			=	10000000;


/*////////////////////////////////////////////////////////////////////////
Guild相关
/*////////////////////////////////////////////////////////////////////////
const DWORD 	 	MAX_GUILDNAME_LENGTH				= 16;
const DWORD 	 	MAX_GUILDNOTICE_LENGTH			= 150;
const DWORD 	 	MAX_GUILDMARKSTREAM_SIZE		= 160;
const DWORD 	 	MAX_GUILDWAREHOUSESTREAM_SIZE	= 1700;
const DWORD 	 	MAX_GUILDSTYLESTREAM_SIZE		= 8;
const DWORD 	 	CREATE_GUILD_LIMIT_LEVEL		= 20;
const DWORD 	 	CREATE_GUILD_LIMIT_MONEY		= 10*10000;



/*////////////////////////////////////////////////////////////////////////
// PVP
/*////////////////////////////////////////////////////////////////////////
const DWORD 	  PVP_PRESS_KEY_TIMEOUT_TIME		= 30*1000;
const DWORD 	  PVP_MATCHLESSS_DELAY_TIME		= 10*1000;
const DWORD 	  PVP_REVIVAL_DELAY_TIME			= 20*1000;
const DWORD 	  PVP_PRESS_KEY_LEAVE_TIME			= PVP_PRESS_KEY_TIMEOUT_TIME - (10*1000);



#endif //__CONSTDATA_H__
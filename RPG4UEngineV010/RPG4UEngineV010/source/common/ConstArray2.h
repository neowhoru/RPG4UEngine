/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstArray2.h
创建日期：2008年5月20日
最后更新：2008年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTARRAY2_H__
#define __CONSTARRAY2_H__
#pragma once

#include "ConstDefine.h"
#include "StringHandle.h"

//#define MAX_CONST_CATEGORY		10	///最大种类数量
//enum eTYPE_CATEGORY
//{
//	 eTYPELABEL_CharType			= 1
//	,eTYPELABEL_SexType			= 2
//	,eTYPELABEL_VItemType		= 3
//	,eTYPELABEL_EquipName		= 4
//	,eTYPELABEL_EquipPos			= 5
//	,eTYPELABEL_SceneObjType	= 6
//	,eTYPELABEL_V3DExt			= 7
//	,eTYPELABEL_V3DFileTitle	= 8
//	,eTYPELABEL_PathInfo			= 9
//};


/*////////////////////////////////////////////////////////////////////////
	获取名字	LPCSTR	Get##Name
	取值		Type		Get##Name##Value
	Category	主要配合 PathInfoParser做类别Label解释
/*////////////////////////////////////////////////////////////////////////

#define CONSTARRAY2_DECLARE2(Name,Type,TypeMax,Category, API)\
	class API ConstArray2##Name\
	{\
		public:\
		enum{CATEGORY_INDEX =  Category,TypeBase=0};\
		struct sINFO{\
			Type		keyDat;\
			LPCSTR	szLabel;\
		};\
		static sINFO ms_sz##Name##s[TypeMax];\
		static inline LPCSTR GetLabel(Type keyDat)\
		{\
			for(INT n=TypeBase; n<TypeMax;n++)\
			{\
				if(ms_sz##Name##s[n].keyDat == keyDat)\
					return ms_sz##Name##s[n].szLabel;\
			}\
			return NULL;\
		}\
		static inline LPCSTR GetLabelList(Type data,LPCSTR szSep="|")\
		{\
			static StringHandle	sLabelList;\
			sLabelList = "";\
			for(INT n=TypeBase; n<TypeMax;n++)\
			{\
				if(_BIT_TEST(data,ms_sz##Name##s[n].keyDat))\
				{\
					if(sLabelList.Length())\
						sLabelList += szSep;\
					sLabelList += ms_sz##Name##s[n].szLabel;\
				}\
			}\
			return sLabelList;\
		}\
		static inline Type GetValue(LPCSTR szType)\
		{\
			for(INT n=TypeBase; n<TypeMax; n++)\
			{\
				if(stricmp(ms_sz##Name##s[n].szLabel,szType) == 0)\
					return ms_sz##Name##s[n].keyDat;\
			}\
			return (Type)0;\
		}\
	};\
	inline LPCSTR Get##Name##Label(Type nIndex){return ConstArray2##Name::GetLabel(nIndex);}\
	inline LPCSTR Get##Name##LabelList(Type data,LPCSTR szSep="|"){return ConstArray2##Name::GetLabelList(data,szSep);}\
	inline Type Get##Name##Value(LPCSTR szType){return ConstArray2##Name::GetValue(szType);}



#define CONSTARRAY2_DECLARE(Name,Type,TypeMax, API)\
								CONSTARRAY2_DECLARE2(Name,Type,TypeMax,0, API)

#define CONSTARRAY2_IMPL(Name,TypeMax)\
								ConstArray2##Name::sINFO ConstArray2##Name::ms_sz##Name##s[TypeMax]=






///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
//CONSTARRAY2_DECLARE2	(CharType
//                     ,ePLAYER_TYPE
//							,PLAYERTYPE_BASE
//							,PLAYERTYPE_MAX
//							,eTYPELABEL_CharType
//							,_BASE_API);
//
//
//CONSTARRAY2_DECLARE2	(SexType
//                     ,eSEX_TYPE
//							,SEX_MALE
//							,SEX_MAX
//							,eTYPELABEL_SexType
//							,_BASE_API);
//
//
//
///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
//
//CONSTARRAY2_DECLARE2	(VItemType
//                     ,eVITEMTYPE
//							,VITEMTYPE_NONE
//							,VITEMTYPE_MAX
//							,eTYPELABEL_VItemType
//							,_BASE_API);
//





#endif //__CONSTARRAY2_H__
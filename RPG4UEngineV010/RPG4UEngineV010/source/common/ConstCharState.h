/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstCharState.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTCHARSTATE_H__
#define __CONSTCHARSTATE_H__
#pragma once



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSTATE_TYPE
{
	 STATETYPE_ABNORMAL			= 1	
	,STATETYPE_WEAKENING			= 2	
	,STATETYPE_STRENGTHENING	= 3	
	,STATETYPE_SPECIALITY		= 4	
};


/*////////////////////////////////////////////////////////////////////////
// 人物状态类型
/*////////////////////////////////////////////////////////////////////////

enum eCHAR_STATE_TYPE
{
	 CHARSTATE_INVALID							= -1
	,CHARSTATE_ABILITY							= 0x7fffffff
	,CHARSTATE_DEFAULT							= 0

	//-------------------------------------------------------------------------------------------------
	// 基本状态
	//-------------------------------------------------------------------------------------------------
	,CHARSTATE_CHAOS								= 1		// 混乱(谁好象都攻击)
	,CHARSTATE_BLIND								= 2		// 失明
	,CHARSTATE_DOWN								= 3		// 跌倒(不能做任何动作)
	,CHARSTATE_DELAY								= 4		// 迟钝
	,CHARSTATE_SEALING							= 5		// 魔法封印
	,CHARSTATE_STUN								= 6		// 晕眩(不能做任何动作)
	,CHARSTATE_STONE								= 7		// 石化(不能做任何动作、不受攻击)
	,CHARSTATE_SLEEP								= 8		// 打滑(不能做任何动作、取消攻击)
	,CHARSTATE_FROZEN								= 9		// 冰冻 能力降低(不能做任何动作)
	,CHARSTATE_SEQUELA							= 10		// 后遗症(复活后)
															
	//-------------------------------------------------------------------------------------------------
	// 攻击状态
	//-------------------------------------------------------------------------------------------------
	,CHARSTATE_POISON								= 100		// 中毒(持续的伤害)
	,CHARSTATE_WOUND								= 101		// 裂伤(持续的伤害)
	,CHARSTATE_FIRE_WOUND						= 102		// 灼伤(持续的损坏)
	,CHARSTATE_PANIC								= 103		// 恐慌(MP减少)
	,CHARSTATE_LOW_SPIRIT						= 104		// 没精力(攻击减弱)
	,CHARSTATE_WEAKENING							= 105		// 虚弱(防御减弱)
	,CHARSTATE_ATTACK_FAIL						= 106		// 攻击失灵(攻击成功率降低)
	,CHARSTATE_FRUSTRATION						= 107		// 挫折(闪避成功率降低)
	,CHARSTATE_FETTER								= 108		// 束缚(移动速度降低)
	,CHARSTATE_SLOWDOWN							= 109		// 钝化(攻速降低)
	,CHARSTATE_HP_REDUCE							= 110		// 倒行逆流(HP减少)
	,CHARSTATE_MP_REDUCE							= 111		// 倒行逆流(MP减少)
	,CHARSTATE_LOW_STRENGTH						= 112		// 体力降低(HP恢复量减少)
	,CHARSTATE_DICLINE							= 113		// 变怪(MP恢复量减少)
	,CHARSTATE_MAGIC_EXPOSE						= 114		// 魔法暴烈(魔术防御力减弱)
	,CHARSTATE_HPRATE_REDUCE					= 116		// 弱点(HP恢复速率减少)
	,CHARSTATE_MPRATE_REDUCE					= 117		// 障碍(MP恢复速率减少)
															
	,CHARSTATE_MAGIC_ATTACK_DECRESE			= 123		// 魔攻减弱
	,CHARSTATE_STAT_STR_DECRESE				= 124		// 力量减弱
	,CHARSTATE_STAT_DEX_DECRESE				= 125		// 敏捷减弱
	,CHARSTATE_STAT_VIT_DECRESE				= 126		// 体力减弱
	,CHARSTATE_STAT_INT_DECRESE				= 127		// 精神减弱
	,CHARSTATE_STAT_SPI_DECRESE				= 128		// 智力减弱
	
	,CHARSTATE_STAT_LOWHP_ATTACK_DECREASE	= 129		// 物理攻击降低
	,CHARSTATE_STAT_LOWHP_DEFENSE_DECREASE	= 130		// 物理防御降低
	
	,CHARSTATE_FIRE_ATTACK_DECREASE			= 131		// 火系攻击减弱
	,CHARSTATE_WATER_ATTACK_DECREASE			= 132		// 水系攻击减弱
	,CHARSTATE_WIND_ATTACK_DECREASE			= 133		// 风系攻击减弱
	,CHARSTATE_EARTH_ATTACK_DECREASE			= 134		// 土系攻击减弱
	,CHARSTATE_DARK_ATTACK_DECREASE			= 135		// 暗系攻击减弱
																
	,CHARSTATE_FIRE_DEFENSE_DECREASE			= 136		// 火系防御减弱
	,CHARSTATE_WATER_DEFENSE_DECREASE		= 137		// 水系防御减弱
	,CHARSTATE_WIND_DEFENSE_DECREASE			= 138		// 风系防御减弱
	,CHARSTATE_EARTH_DEFENSE_DECREASE		= 139		// 土系防御减弱
	,CHARSTATE_DARK_DEFENSE_DECREASE			= 140		// 暗系防御减弱
																
	//-------------------------------------------------------------------------------------------------
	// 能力提升
	//-------------------------------------------------------------------------------------------------
	,CHARSTATE_ABSORB								= 115		// 吸血(HP,MP吸收)
	,CHARSTATE_ANGER								= 200		// 愤怒(攻击力上升)
	,CHARSTATE_DEFENSE							= 201		// 防御(防御力上升)
	,CHARSTATE_PROTECTION						= 202		// 保护(异常条件下)
	,CHARSTATE_FIGHTING							= 203		// 眼界(命中率上升)
	,CHARSTATE_BALANCE							= 204		// 灵活(闪避率上升)
	,CHARSTATE_VITAL_POWER						= 205		// 活力(HP恢复速率上升)
	,CHARSTATE_MEDITATION						= 206		// 冥想(MP,SP恢复速率上升)
	,CHARSTATE_HIGH_SPIRIT						= 207		// 兴奋(攻击速度增加)
	,CHARSTATE_SPEEDING							= 208		// 疾速(移动速度增加)
	,CHARSTATE_CONCENTRATION					= 209		// 集中(致命一击率增加)
	,CHARSTATE_INCREASE_SKILLRANGE			= 210		// 视野(视野范围、攻程增加)
	,CHARSTATE_PRECISION							= 211		// 精锐(致命一击伤害增加)
	,CHARSTATE_HP_INCREASE						= 212		// 激励(HP增加)
	,CHARSTATE_MP_INCREASE						= 213		// 弹力(MP增加)
	,CHARSTATE_HPRATE_INCREASE					= 214		// 活力(HP恢复量增加)
	,CHARSTATE_MPRATE_INCREASE					= 215		// 刺激(MP恢复量增加)      
	,CHARSTATE_CURE								= 216		// 净化(全部状态解除)
	,CHARSTATE_MAGIC_DEFENSE					= 217		// 魔法防御(魔术防御力增加)
	,CHARSTATE_MAGIC_SHIELD						= 218		// 魔法保护
	,CHARSTATE_HOLDING							= 219		// 硬化(不可移动、可攻击)
	,CHARSTATE_SP_BONUS							= 221		// MP追加攻击力(增加)
	,CHARSTATE_BUF_RANGE_DAMAGE				= 222		// 远程伤害(周边敌人持续伤害)
	,CHARSTATE_STAT_STR							= 223		// 力量强化
	,CHARSTATE_STAT_DEX							= 224		// 敏捷强化
	,CHARSTATE_STAT_VIT							= 225		// 体力强化
	,CHARSTATE_STAT_SPI							= 226		// 精神强化
	,CHARSTATE_STAT_INT							= 227		// 智力强化
	,CHARSTATE_MAGIC_ATTACK_INCREASE			= 228		// 魔攻上升
															
	,CHARSTATE_STAT_LOWHP_ATTACK_INCREASE	= 229	// 物理攻击上升
	,CHARSTATE_STAT_LOWHP_DEFENSE_INCREASE	= 230	// 物理防御上升
															
	,CHARSTATE_STAT_DAMAGE_ADD					= 231	// 追加伤害
																	
	,CHARSTATE_FIRE_ATTACK_INCREASE			= 232		// 火系攻击增强
	,CHARSTATE_WATER_ATTACK_INCREASE			= 233		// 水系攻击增强
	,CHARSTATE_WIND_ATTACK_INCREASE			= 234		// 风系攻击增强
	,CHARSTATE_EARTH_ATTACK_INCREASE			= 235		// 土攻击增强
	,CHARSTATE_DARK_ATTACK_INCREASE			= 236		// 暗系攻击增强
															
	,CHARSTATE_FIRE_DEFENSE_INCREASE			= 237		// 火系防御增强
	,CHARSTATE_WATER_DEFENSE_INCREASE		= 238		// 水系防御增强
	,CHARSTATE_WIND_DEFENSE_INCREASE			= 239		// 风系防御增强
	,CHARSTATE_EARTH_DEFENSE_INCREASE		= 240		// 土防御增强
	,CHARSTATE_DARK_DEFENSE_INCREASE			= 241		// 暗系防御增强
															
	,CHARSTATE_REFLECT_DAMAGE					= 220		// 反伤
	,CHARSTATE_REFLECT_SLOW						= 118		// 反伤束缚(伤害+攻速降低)     
	,CHARSTATE_REFLECT_FROZEN					= 119		// 反伤冰冻(伤害+不能任何动作)
	,CHARSTATE_REFLECT_SLOWDOWN				= 120		// 反伤钝化(伤害+移速降低)    
	,CHARSTATE_REFLECT_STUN						= 121		// 反伤晕眩(伤害+不能任何动作)
	,CHARSTATE_REFLECT_FEAR						= 122		// 反伤惊慌(伤害+逃跑)
															
	//------------------------------------------------------------------------------------------------
	// 
	//------------------------------------------------------------------------------------------------
	,CHARSTATE_TRANSFORMATION					= 300		// 伪装
	,CHARSTATE_STEEL								= 301		// 钢化(移动除外，不能做其它动作,移速减少)
	,CHARSTATE_TRANSPARENT						= 302		// 透明(除移动外任何动作将解除)
	,CHARSTATE_FEAR								= 303		// 惊慌
	,CHARSTATE_BLUR								= 304		// 不能动弹
	,CHARSTATE_THRUST								= 305		// 被推状态

	,CHARSTATE_PERIODIC_DAMAGE					= 1000	// 周期伤害

	//-------------------------------------------------------------------------------------------------
	// 
	//------------------------------------------------------------------------------------------------- 
	,CHARSTATE_STYLE_THRUST						= 2000	// 心法状态：被推
	,CHARSTATE_STYLE_STUN						= 2001	// 心法状态：眩晕
	,CHARSTATE_STYLE_DOWN						= 2002	// 心法状态：倒下
														
	,CHARSTATE_ETC_FLYING						= 3000	// 飘浮状态
	,CHARSTATE_ETC_DISABLE_VISION				= 3001	// 隐形状态，怪物无法发现
	,CHARSTATE_ETC_OBSERVER						= 3002	// 观看者(不可攻击,不可被击)
	,CHARSTATE_ETC_UNDEAD						= 3003	// 无敌状态(即使受伤，也处于不死状态)
	,CHARSTATE_ETC_RETURNING					= 3004	// 复活点返回状态
														
	,CHARSTATE_ETC_AUTO_RECOVER_HP			= 3005	// 自动恢复HP
	,CHARSTATE_ETC_AUTO_RECOVER_MP			= 3006	// 自动恢复MP
	,CHARSTATE_ETC_AUTO_RECOVER_HPMP			= 3007	// 自动恢复HPMP
	,CHARSTATE_ETC_ITEM_RECOVER_HP			= 3008	// 物品恢复HP
	,CHARSTATE_ETC_ITEM_RECOVER_MP			= 3009	// 物品恢复MP												
};


#endif //__CONSTCHARSTATE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：THashMap.inl
创建日期：2008年8月27日
最后更新：2008年8月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __THASHMAP_INL__
#define __THASHMAP_INL__
#pragma once

namespace util
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class T, class S> inline
THashMap<T,S>::THashMap()
{
	Clear();
}

template <class T, class S> inline
THashMap<T,S>::~THashMap()
{
	Destroy();
}

template <class T, class S> inline
DWORD THashMap<T,S>::GetTableSize( void)	
{
	return ( m_dwTableSize); 
}

template <class T, class S> inline
void THashMap<T,S>::Clear( void)
{
	m_dwTableSize = 0;
	m_ppHashTable = NULL;
}


template <class T, class S> inline
void THashMap<T,S>::Create( DWORD dwTableSize)
{
	m_dwTableSize = dwTableSize;
	m_ppHashTable = new THashMapItem<T,S>*[dwTableSize];
	ZeroMemory( m_ppHashTable, dwTableSize * sizeof(THashMapItem<T,S>*) );
}


template <class T, class S> inline
void THashMap<T,S>::Destroy( void)
{
	if( m_ppHashTable )
	{
		RemoveAll();
		delete[] m_ppHashTable;
		Clear();
	}
}


template <class T, class S> inline
void THashMap<T,S>::RemoveAll( void)
{
	THashMapItem<T, S>* pSave;

	for( UINT i = 0; i < m_dwTableSize; ++i ) 
	{
		while( m_ppHashTable[i] )
		{
			pSave = m_ppHashTable[i];
			m_ppHashTable[i] = m_ppHashTable[i]->m_pNext;
			ClearKey( pSave );
			delete pSave;
		}
	}
}


template <class T, class S> inline
BOOL THashMap<T,S>::IsSameKey( S Key1, S Key2)
{
	//return ( 0 == memcmp( &pKey1, &pKey2, sizeof(S) ) );
	return (Key1 == Key2 ) ? TRUE : FALSE;
}


template <class T, class S> inline
DWORD THashMap<T,S>::Hash( S Key)
{
	DWORD dwCalc = 0;
	BYTE *pbyCheck = ( BYTE*)&Key;
	for ( int i = 0; i < sizeof ( S); ++i, ++pbyCheck)
	{
		dwCalc = (dwCalc * 131) + *pbyCheck;
	}
	return ( dwCalc % m_dwTableSize);
}


template <class T, class S> inline
void THashMap<T,S>::ClearKey(THashMapItem<T,S>* pItem)
{
}

template <class T, class S> inline
BOOL THashMap<T,S>::Insert( T Data, S Key)
{
	DWORD dwIndex = Hash( Key );
	THashMapItem<T, S>* pItem = m_ppHashTable[dwIndex];

	while( pItem )
	{
		// 鞍篮 虐啊 粮犁窍绰 版快 丹绢敬促
		if( IsSameKey(Key, pItem->m_key) )
		{
			pItem->m_key = Key;
			pItem->m_val = Data;
			return TRUE;
		}
		pItem = pItem->m_pNext;
	}

	pItem = new THashMapItem<T,S>;
	assert(pItem);
	pItem->m_key = Key;
	pItem->m_val = Data;

	// Node狼 菊何盒俊 眠啊茄促! (A->B->C new D) => (D->A->B->C)
	pItem->m_pNext = m_ppHashTable[dwIndex];
	m_ppHashTable[dwIndex] = pItem;
	return TRUE;
}


template <class T, class S> inline
T THashMap<T,S>::Remove( S Key)
{
	T Result;

	DWORD dwIndex = Hash( Key );
	THashMapItem<T, S>* pItem = m_ppHashTable[dwIndex];

	if( pItem )
	{
		if( IsSameKey(Key, pItem->m_key) )
		{
			m_ppHashTable[dwIndex] = pItem->m_pNext;
			Result = pItem->m_val;
			ClearKey( pItem );
			delete pItem;
			return Result;
		}
		else
		{
			THashMapItem<T, S>* pPrev = pItem;
			THashMapItem<T, S>* pCurr = pPrev->m_pNext;

			while( pCurr && !IsSameKey(Key, pCurr->m_key) )
			{
				pPrev = pCurr;
				pCurr = pCurr->m_pNext;
			}

			if( pCurr )
			{
				pPrev->m_pNext = pCurr->m_pNext;
				Result = pCurr->m_val;
				ClearKey( pCurr );
				delete pCurr;
				return Result;
			}
		}
	}

	ZeroMemory( &Result, sizeof ( T));
	return ( Result);
}


template <class T, class S> inline
T THashMap<T,S>::Get( S Key)
{
	DWORD dwIndex = Hash( Key );
	THashMapItem<T, S>* pItem = m_ppHashTable[dwIndex];

	while( pItem )
	{
		if( IsSameKey(Key, pItem->m_key) )
		{
			return pItem->m_val;
		}
		pItem = pItem->m_pNext;
	}

	T NullData;
	ZeroMemory( &NullData, sizeof ( T));
	return ( NullData);
}

template <class T, class S> inline
THashMapIterator THashMap<T,S>::GetFirstPos()
{
	for( UINT i = 0; i < m_dwTableSize; ++i ) 
	{
		if( m_ppHashTable[i] )
			return m_ppHashTable[i];
	}
	return 0;
}

template <class T, class S> inline
void THashMap<T,S>::GetNext( THashMapIterator& pos, T& val, S& key )
{
	THashMapItem<T,S>* pItem = (THashMapItem<T,S> *)pos;

	key = pItem->m_key;
	val = pItem->m_val;

	if( pItem->m_pNext )
	{
		pos = pItem->m_pNext;
		return;
	}

	DWORD i = Hash(pItem->m_key);
	for( ++i; i < m_dwTableSize; ++i ) 
	{
		pItem = m_ppHashTable[i];
		if( pItem ) 
		{
			pos = pItem;
			return;
		}
	}

	pos = NULL;
}



};//namespace util

#endif //__THASHMAP_INL__


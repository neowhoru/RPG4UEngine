/*////////////////////////////////////////////////////////////////////////
文 件 名：CommonDefine.h
创建日期：2007年12月12日
最后更新：2007年12月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	使用宏定义，能减少代码碎片，让代码更整洁、清晰。
	当然作为其它程序员，掌握此规则，也能更容易阅读代码。

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __COMMONDEFINE_H__
#define __COMMONDEFINE_H__
#pragma once

#include "DataTypeDefine.h"
#include "ConstData.h"
#include "BaseDefine.h"


#ifndef USE_VRU //vru
#define VRURealloc	realloc
#define VRUMalloc		malloc
#define VRUFree		free
#endif


/*////////////////////////////////////////////////////////////////////////
VRSTR串操作
/*////////////////////////////////////////////////////////////////////////
#define  _VNULL()		theGlobalString.GetBlank()
#define  _VI(x)		theGlobalString.Insert(x,FALSE)
#define  _VIT(x)		theGlobalString.Insert(x,TRUE)
#define  _VIN(x,l)	theGlobalString.InsertN(x,l,FALSE)
#define  _VINT(x,l)	theGlobalString.InsertN(x,l,TRUE)
#define  _V(x)			theGlobalString.Find(x,FALSE)
#define  _VT(x)		theGlobalString.Find(x,TRUE)

#define  _VPATHI(x)	theGlobalString.Insert(x,FALSE)
#define  _VPATHIT(x)	theGlobalString.Insert(x,TRUE)
#define  _VPATH(x)	theGlobalString.Find(x,FALSE)
#define  _VPATHT(x)	theGlobalString.Find(x,TRUE)



////////////////////////////////////////////////////////////////////////
#define		MAKEDWORD(a, b)		((DWORD)(((WORD)((DWORD_PTR)(a) & 0xffff)) | ((DWORD)((WORD)((DWORD_PTR)(b) & 0xffff))) << 16))
#define		MAKEDWORD64(a, b)		(  ((DWORD64)((DWORD)(a) & 0xffffffff)) | (((DWORD64)(((DWORD)(b) & 0xffffffff))) << 32) )

/*////////////////////////////////////////////////////////////////////////
双字串与双字转换
/*////////////////////////////////////////////////////////////////////////
#define STR2TAG( str)								(   ( IDTYPE) (	(0x000000FF&  (str)[0]		  )	\
																			|		(0x0000FF00&( (str)[1] << 8 ))	\
																			|		(0x00FF0000&( (str)[2] << 16))	\
																			|		(0xFF000000&( (str)[3] << 24))	\
																			)\
															)

#define CHAR2TAG( ch1, ch2, ch3, ch4)		(   ( IDTYPE) (	(0x000000FF&  (ch1)		  )	\
																			|		(0x0000FF00&( (ch2) << 8 ))	\
																			|		(0x00FF0000&( (ch3) << 16))	\
																			|		(0xFF000000&( (ch4) << 24))	\
																			)\
															)

#define CombinateToTAG_C4( pre, ch4)				((pre)|CHAR2TAG( '\0','\0','\0', ch4))
#define CombinateToTAG_C34( pre, ch3,ch4)			((pre)|CHAR2TAG( '\0','\0',ch3,  ch4))

#define Prefix3CharToTAG( ch1, ch2, ch3)			 CHAR2TAG( ch1, ch2, ch3, '\0')
#define Prefix2CharToTAG( ch1, ch2)					 CHAR2TAG( ch1, ch2, '\0','\0')
#define Prefix1CharToTAG( ch1)						 CHAR2TAG( ch1, '\0','\0','\0')

#define IsTAGPrefix3(id, ch1, ch2, ch3)			(CHAR2TAG( ch1, ch2, ch3, '\0')	== ((id)&0x00FFFFFF))
#define IsTAGPrefix2(id, ch1, ch2)					(CHAR2TAG( ch1, ch2, '\0', '\0')	== ((id)&0x0000FFFF))
#define IsTAGPrefix1(id, ch1 )						((ch1)										== ((id)&0x000000FF))

#define IsTAGPrefixBy3(id, ch)						((ch)	== ((id)&0x00FFFFFF))
#define IsTAGPrefixBy2(id, ch)						((ch)	== ((id)&0x0000FFFF))
#define IsTAGPrefixBy1(id, ch)						((ch)	== ((id)&0x000000FF))


inline char* TAG2STR( char *lpStr, IDTYPE wiSrc)\
															{\
																*lpStr			= ( char)( wiSrc & 0xFF);\
																*( lpStr + 1)	= ( char)( ( wiSrc >> 8) & 0xFF);\
																*( lpStr + 2)	= ( char)( ( wiSrc >> 16) & 0xFF);\
																*( lpStr + 3)	= ( char)( ( wiSrc >> 24) & 0xFF);\
																return ( lpStr);\
															}

inline LPCSTR TAG2STR(IDTYPE wiSrc)\
															{\
																static char szID[MAX_AREA_ID_LENGTH+1];\
																TAG2STR(szID,wiSrc);\
																szID[MAX_AREA_ID_LENGTH] = 0;\
																return szID;\
															}


const	IDTYPE	IDTYPE_NULL	= STR2TAG("NULL");


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//防止类的自动拷贝
#define __DISABLE_COPY(className)\
											private :\
												className(const className&);\
												void	operator = (const className&)

//当形参不被函数内部调用时，建议此宏用在未使用到的形参
#define __UNUSED(variable)			variable
#define __INFINITE_LOOP(name)		BOOL b##name##Loop = TRUE;	while (b##name##Loop)
#define INOUT
/*////////////////////////////////////////////////////////////////////////
	基本TRUE FALSE 判断相关宏
/*////////////////////////////////////////////////////////////////////////
#define  __RETIF_FALSE(x)			if((x)==0)	return FALSE
#define  __RETIF_TRUE(x)			if(x)			return TRUE
//BOOL调用检测
#define  __BOOL_CALL(x)				__RETIF_FALSE(x)
#define  __BOOL_SUPER(x)			__RETIF_FALSE(_SUPER::x)
#define  __VOID_SUPER(x)			_SUPER::x
//检测判断
#define  __CHECK(x)					__RETIF_FALSE(x)
#define  __CHECK_PTR(x)				if((x)==NULL)	return FALSE
#define  __CHECK_STR(x)				if((x)==NULL || (x)[0] == 0)	return FALSE
#define  __CHECK2(x,r)				if((x)==FALSE)	return r
#define  __CHECK2_PTR(x,r)			if((x)==NULL)	return r
#define  __CHECK2_STR(x,r)			if((x)==NULL || (x)[0] == 0)	return r

///用于For等循环范围检测
#define  __CHECK_RANGE2(x,h,e)		__CHECK((x) >=(h) && (x) < (e) )
#define  __CHECK_RANGE(x,e)			__CHECK_RANGE2(x,0,e)
#define  __CHECK2_RANGE2(x,h,e,r)	__CHECK2((x) >=(h) && (x) < (e), r )
#define  __CHECK2_RANGE(x,e,r)		__CHECK2_RANGE2(x,0,e,r)


//检测并确认
#define  __VERIFY2(x,t,r)			{BOOL b=(x);assert((b) && (t)); if((b)==FALSE)		{	LOGINFO(t);return r;}}
#define  __VERIFY2_PTR(x,t,r)		{const void* ptr=(x); assert(ptr && (t)); if(ptr==NULL)	{	LOGINFO(t);return r;}}
#define  __VERIFY(x,t)				__VERIFY2(x,t,FALSE)
#define  __VERIFY_PTR(x,t)			__VERIFY2_PTR(x,t,NULL)

#define  __VERIFY0(x)				__VERIFY(x,#x " return FALSE! ")


//尝试返回非空x值
#define  __RETURN_TRY(x)			if((x)!=NULL)	return (x)



/*////////////////////////////////////////////////////////////////////////
	初始化、退出等相关宏
/*////////////////////////////////////////////////////////////////////////
#define  __INIT2(x,TipOK,TipErr)		if(!(x))\
												{\
													assert(! #x " return FALSE! Application will Quit!" );\
													LOGINFO(TipErr);\
													return FALSE;\
												}\
													LOGINFO(TipOK)

#define  __INIT(x)					__INIT2(x,#x" OK\n", #x " Failed\n")
#define  __INITH(x)					__INIT2(SUCCEEDED(x),#x" OK\n", #x " Failed\n")
#define  __END(x)						x;LOGINFO(#x" OK\n")

#define  __RUN(x)						__VERIFY(x,#x " return FALSE! Application will Quit!")

#define  __INITINST(INST,a)			__INIT(INST.Init(a) )
#define  __INITINST2(INST,a,b)		__INIT(INST.Init(a,b) )
#define  __INITINST3(INST,a,b,c)		__INIT(INST.Init(a,b,c) )
#define  __INITINST4(INST,a,b,c,d)	__INIT(INST.Init(a,b,c,d) )

#define  __LOADINFO(INST,F1)		__INIT(INST.Load(_PATH_DATAINFO(_T(F1))) )
#define  __LOADBASE(INST,F1)		__INIT(INST.Load(_PATH_DATAINFO(_T("BaseInfo\\") _T(F1))) )
#define  __LOADSVR(INST,F1)		__INIT(INST.Load(_PATH_DATAINFO(_T("server\\") _T(F1))) )
#define  __LOADFUNC(FUNC,F1)		__INIT(FUNC(_PATH_DATAINFO(_T(F1))) )
#define  __LOAD(INST,F1)			__INIT(INST.Load(_T(F1)) )
#define  __LOAD2(INST,F1,F2)		__INIT(INST.Load(_T(F1),_T(F2)) )
#define  __LOAD3(INST,F1,F2,F3)	__INIT(INST.Load(_T(F1),_T(F2),_T(F3)) )

////////////////////////////////////////////////////////////////////////
#define __FRMCALL(CALL)			__VERIFY(CALL,#CALL " return FALSE; will quit Application!")

#define __FRMMOVE(INST)			__RUN(INST.FrameMove(dwTick))

#define __FRMMOVE2(INST)		PROFILE_PROC_BEGIN("GameRunning::" #INST);\
										__RUN(INST.FrameMove(dwTick));\
										PROFILE_PROC_END( "GameRunning::" #INST )




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _USING(np)					using namespace np
#define _NAMESPACE(cls, np)		namespace np{class cls;}
#define _NAMESPACEU(cls, np)		namespace np{class cls;}	_USING(np)
#define _NAMESPACE2(cls, np)		namespace np{struct cls;}
#define _NAMESPACE2U(cls, np)		namespace np{struct cls;}	_USING(np)
											

/*////////////////////////////////////////////////////////////////////////
内存操作
/*////////////////////////////////////////////////////////////////////////
#define __ZERO(x)						memset(&(x),0,sizeof(x))
#define __ZERO_FOR(x,c)				for(INT mm=0;mm<(c);mm++){(x)[mm]=0;}
#define __ZERO_PTR(x)				memset((x),0,sizeof(*(x)))
#define __ZERO_ARRAY(x)				memset((x),0,sizeof(x))
#define __SET(x,s)					memset(&(x),s,sizeof(x))


//=============================================================================================================================
// SafeDeleter
//=============================================================================================================================

#ifndef SAFE_DELETE
#define SAFE_DELETE( p)				{ if ( p) { delete ( p); ( p) = NULL; } }
#define SAFE_DELETE_ARRAY( p)		{ if ( p) { delete[] ( p); ( p) = NULL; } }
#define SAFE_FREE( p)				{ if ( p) { free (p); ( p) = NULL; } }
#define SAFE_RELEASE( p)			{ if ( p) { ( p)->Release(); ( p) = NULL; } }
#define SAFE_RELEASENDELETE(p)	{ if ( p) { (p)->Release(); delete (p); (p) = NULL; }}
#endif

class SafeDeleter
{
public :
	template <class T>
	void	operator () (T*& ptr) const
	{
		SAFE_DELETE( ptr );
	}
};


#define SAFE_VRUFREE( p)			{ if ( p) { VRUFree ( p); ( p) = NULL; } }
#define SAFE_MEMFREE(o,p)			{ if ((o) &&(p)) { (o)->MemoryFree ( p); ( p) = NULL; } }

#define	_SUPER		__super


//////////////////////////////////////////////////////////
template <class T> 
struct Param2Class
{
	typedef T ClassType;
};


////////////////////////////////////////////////////////////////////////
#define __PROPERTY( DataType, name )											\
		public :																\
			void					Set##name( const DataType& value )	{ m_##name = value; }	\
			const DataType&	Get##name() const							{ return m_##name; }	\
			DataType&			Get##name() 								{ return m_##name; }	\
		protected :																\
			DataType	m_##name;


#define __ARRAY_PROPERTY( DataType, num, name )										\
		public :																	\
			void		Set##name( WORD idx, const DataType& value )	{ m_p##name[idx] = value; }	\
			const DataType&	Get##name( WORD idx )	{ return m_p##name[idx]; }	\
			DataType*			Get##name##Ptr()			{ return m_p##name; }			\
			const DataType*	Get##name##Ptr()	const	{ return m_p##name; }			\
		protected :																	\
			DataType	m_p##name[num];

#define __PTR_PROPERTY( DataType, name )										\
		public :																\
			void		Set##name( DataType* value )	{ m_p##name = value; }	\
			const DataType*	Get##name() const		{ return m_p##name; }	\
			DataType*			Get##name()				{ return m_p##name; }	\
		protected :																\
			DataType*	m_p##name;


//#define	PRE_BLANK
/*////////////////////////////////////////////////////////////////////////
// struct PROPERTY
/*////////////////////////////////////////////////////////////////////////
#define __STRUCT_PROPERTY( DataType, name )\
		public :\
			void					Set##name(const DataType& value ){ m_dat##name = value; }	\
			const DataType&	Get##name##Const() const			{ return m_dat##name; }	\
			DataType&			Get##name() 							{ return m_dat##name; }	\
		protected :\
			DataType	m_dat##name;


#define VG_ARRAY_PROPERTY( name, DataType, num  )\
		public :																	\
			void					Set##name##At( INT idx, const DataType& value )	{ m_ar##name##s[idx] = value; }	\
			DataType&			Get##name##At( INT idx )			{ return m_ar##name##s[idx]; }	\
			const DataType&	Get##name##At( INT idx ) const	{ return m_ar##name##s[idx]; }	\
			DataType*			Get##name##s()							{ return m_ar##name##s; }\
			const DataType*	Get##name##s()	const					{ return m_ar##name##s; }\
		protected :\
			DataType	m_ar##name##s[num];

#define VG_BUF_PROPERTY( name, DataType, num  )\
		public :																	\
			void					Set##name(DataType* dat )	{ memset(m_ar##name,0,sizeof(m_ar##name)); memcpy(m_ar##name, dat, (num) * sizeof(DataType)); }	\
			void					Set##name(DataType* dat,UINT l )	{ memset(m_ar##name,0,sizeof(m_ar##name)); memcpy(m_ar##name, dat, min(num,l) * sizeof(DataType)); }	\
			DataType*			Get##name()			{ return m_ar##name; }\
			const DataType*	Get##name()	const	{ return m_ar##name; }\
		protected :\
			DataType	m_ar##name[num];


#define VG_DWORD_ARRAY_PROPERTY( name, num  )		VG_ARRAY_PROPERTY( name, DWORD, num  )
	

/*////////////////////////////////////////////////////////////////////////
// PROPERTY
/*////////////////////////////////////////////////////////////////////////
#define _VG_GET_PROPERTY_NOVAR( DataType, name,  pre)\
						public :\
							DataType			Get##name() 		{ return m_##pre##name; }\
							const DataType	Get##name() const	{ return (const DataType)m_##pre##name; }\

#define _VG_GET_PROPERTY_SIMPLE( DataType, name,  pre)\
						_VG_GET_PROPERTY_NOVAR(DataType, name,  pre)\
						protected :\
							DataType		m_##pre##name;

///////////////////////////////////////////////////////
#define _VG_PROPERTY_NOVAR( DataType, name,  pre ,d )\
						_VG_GET_PROPERTY_NOVAR(DataType, name,  pre);\
						public :\
							inline void			Set##name( DataType  pre##name d){ m_##pre##name = pre##name; }

#define _VG_PROPERTY_SIMPLE( DataType, name,  pre ,d )\
						_VG_PROPERTY_NOVAR(DataType, name,  pre ,d);\
						protected :\
							DataType		m_##pre##name;



///////////////////////////////////////////////////////
#define VG_GET_PROPERTY2( DataType, name, prev)\
						public :\
						inline DataType&	Get##name() 				{ return m##prev##name; }\
						inline const DataType&	Get##name() const	{ return m##prev##name; }\
						protected :\
							DataType		m##prev##name;


#define VG_PROPERTY2( DataType, name ,prev )\
						VG_GET_PROPERTY2(DataType, name,prev);\
						public :\
							inline void			Set##name(const DataType&  name ){ m##prev##name = name; }


#define VG_PROPERTY_GET_STR( name,  pre ,l )\
						public :\
							LPCTSTR	Get##name() const						{ return m_##pre##name; }\
							TCHAR*	Get##name() 							{ return m_##pre##name; }\
						protected :\
							TCHAR		m_##pre##name[l+1];

#define VG_PROPERTY_STR( name,  pre ,l )\
						VG_PROPERTY_GET_STR( name,  pre ,l );\
						public :\
							void		Set##name(LPCSTR   pre##name )	{ lstrcpyn(m_##pre##name, pre##name,l+1); }



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define VG_BOOL_GET_NOVAR(name)\
															public :\
																BOOL	Is##name() const		{ return m_b##name; }

#define VG_BOOL_GET_PROPERTY(name)				VG_BOOL_GET_NOVAR(name)\
															protected :\
																BOOL	m_b##name;

#define VN_BOOL_GET_PROPERTY(name)				VG_BOOL_GET_NOVAR(name)


#define _VG_BOOL_NOVAR( name )					_VG_PROPERTY_NOVAR( BOOL, name, b, =TRUE)\
															public:\
															BOOL	Is##name() const	{ return m_b##name; }\
															void	Toggle##name() 	{ m_b##name = !m_b##name; }\
															BOOL	IsNot##name() const	{ return m_b##name == FALSE; }\
															void	Clear##name() 	{ m_b##name = FALSE; }\

#define VG_BOOL_PROPERTY( name )					_VG_BOOL_NOVAR( name)\
															protected :\
																BOOL	m_b##name;

#define VN_BOOL_PROPERTY( name )					_VG_BOOL_NOVAR( name)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define VG_PTR_PROPERTY(name, Type )		_VG_PROPERTY_SIMPLE(Type*,		name, p,		=NULL)
#define VG_PTR_PROPERTY2(Type )				_VG_PROPERTY_SIMPLE(Type*,		Type, p,		=NULL)
#define VG_PPTR_PROPERTY(name, Type )		_VG_PROPERTY_SIMPLE(Type**,	name, pp,	=NULL)
#define VG_DWORD_PROPERTY(name )				_VG_PROPERTY_SIMPLE(DWORD ,	name, dw,	=0)
#define VG_HWND_PROPERTY(name )				_VG_PROPERTY_SIMPLE(HWND ,		name, h,		=0)
#define VG_HTYPE_PROPERTY(name,T )			_VG_PROPERTY_SIMPLE(T ,			name, h,		=0)
#define VG_WORD_PROPERTY(name )				_VG_PROPERTY_SIMPLE(WORD ,		name, w,		=0)
#define VG_SHORT_PROPERTY(name )				_VG_PROPERTY_SIMPLE(short ,	name, st,	=0)
#define VG_SHORT2_PROPERTY(name )			_VG_PROPERTY_SIMPLE(short ,	name, sh,	=0)
#define VG_INT_PROPERTY(name )				_VG_PROPERTY_SIMPLE(INT ,		name, n,		=0)
#define VG_INT2_PROPERTY(name )				_VG_PROPERTY_SIMPLE(INT ,		name, i,		=0)
#define VG_UINT_PROPERTY(name )				_VG_PROPERTY_SIMPLE(UINT ,		name, u,		=0)
#define VG_FLOAT_PROPERTY(name )				_VG_PROPERTY_SIMPLE(float ,	name, f,		=0)
#define VG_DOUBLE_PROPERTY(name )			_VG_PROPERTY_SIMPLE(double ,	name, d,		=0)
#define VG_FLOAT_PROPERTY(name )				_VG_PROPERTY_SIMPLE(float ,	name, f,		=0)
#define VG_BYTE_PROPERTY(name )				_VG_PROPERTY_SIMPLE(BYTE ,		name, by,	=0)
#define VG_CHAR_PROPERTY(name )				_VG_PROPERTY_SIMPLE(char ,		name, c,		=0)
#define VG_LPARAM_PROPERTY(name )			_VG_PROPERTY_SIMPLE(LPARAM ,	name, lp,	=0)
#define VG_LONG_PROPERTY(name )				_VG_PROPERTY_SIMPLE(LONG ,		name, l,		=0)
#define VG_CSTR_PROPERTY(name,l )			VG_PROPERTY_STR(		name, sz,l	)
#define VG_TYPE_PROPERTY(name,T )			VG_PROPERTY2(T,		name, _)
#define VG_VECTOR_PROPERTY(name,T )			VG_PROPERTY2(T ,		name, _v)


#define VG_PTR_GET_PROPERTY(name,Type )	_VG_GET_PROPERTY_SIMPLE(Type*,		name, p)
#define VG_PTR_GET_PROPERTY2(Type )			_VG_GET_PROPERTY_SIMPLE(Type*,		Type, p)
#define VG_PPTR_GET_PROPERTY(name,Type )	_VG_GET_PROPERTY_SIMPLE(Type**,		name, pp)
#define VG_BYTE_GET_PROPERTY(name )			_VG_GET_PROPERTY_SIMPLE(BYTE ,		name, by)
#define VG_INT_GET_PROPERTY(name )			_VG_GET_PROPERTY_SIMPLE(INT ,			name, n)
#define VG_INT2_GET_PROPERTY(name )			_VG_GET_PROPERTY_SIMPLE(INT ,			name, i)
#define VG_UINT_GET_PROPERTY(name )			_VG_GET_PROPERTY_SIMPLE(UINT ,		name, u)
#define VG_SHORT_GET_PROPERTY(name )		_VG_GET_PROPERTY_SIMPLE(short ,		name, st)
#define VG_DWORD_GET_PROPERTY(name )		_VG_GET_PROPERTY_SIMPLE(DWORD ,		name, dw)
#define VG_LONG_GET_PROPERTY(name )			_VG_GET_PROPERTY_SIMPLE(LONG ,		name, l)
#define VG_FLOAT_GET_PROPERTY(name )		_VG_GET_PROPERTY_SIMPLE(float ,		name, f)
#define VG_LONGLONG_GET_PROPERTY(name )	_VG_GET_PROPERTY_SIMPLE(LONGLONG ,	name, ll)
#define VG_CSTR_GET_PROPERTY(name,l )		VG_PROPERTY_GET_STR(		name, sz,l	)
#define VG_TYPE_GET_PROPERTY(name,T )		VG_GET_PROPERTY2(T,		name, _)
#define VG_VECTOR_GET_PROPERTY(name,T )	VG_GET_PROPERTY2(T,		name, _v)



//////////////////////////////////////////////////////////////////////////
#define VN_PTR_PROPERTY(name, Type )		_VG_PROPERTY_NOVAR(Type*,		name, p,		=NULL)
#define VN_PPTR_PROPERTY(name, Type )		_VG_PROPERTY_NOVAR(Type**,		name, pp,	=NULL)
#define VN_DWORD_PROPERTY(name )				_VG_PROPERTY_NOVAR(DWORD ,		name, dw,	=0)
#define VN_HWND_PROPERTY(name )				_VG_PROPERTY_NOVAR(HWND ,		name, h,		=0)
#define VN_HTYPE_PROPERTY(name,T )			_VG_PROPERTY_NOVAR(T ,			name, h,		=0)
#define VN_WORD_PROPERTY(name )				_VG_PROPERTY_NOVAR(WORD ,		name, w,		=0)
#define VN_SHORT_PROPERTY(name )				_VG_PROPERTY_NOVAR(short ,		name, st,	=0)
#define VN_SHORT2_PROPERTY(name )			_VG_PROPERTY_NOVAR(short ,		name, sh,	=0)
#define VN_INT_PROPERTY(name )				_VG_PROPERTY_NOVAR(INT ,		name, n,		=0)
#define VN_INT2_PROPERTY(name )				_VG_PROPERTY_NOVAR(INT ,		name, i,		=0)
#define VN_UINT_PROPERTY(name )				_VG_PROPERTY_NOVAR(UINT ,		name, u,		=0)
#define VN_FLOAT_PROPERTY(name )				_VG_PROPERTY_NOVAR(float ,		name, f,		=0)
#define VN_DOUBLE_PROPERTY(name )			_VG_PROPERTY_NOVAR(double ,	name, d,		=0)
#define VN_FLOAT_PROPERTY(name )				_VG_PROPERTY_NOVAR(float ,		name, f,		=0)
#define VN_BYTE_PROPERTY(name )				_VG_PROPERTY_NOVAR(BYTE ,		name, by,	=0)
#define VN_CHAR_PROPERTY(name )				_VG_PROPERTY_NOVAR(char ,		name, c,		=0)
#define VN_LPARAM_PROPERTY(name )			_VG_PROPERTY_NOVAR(LPARAM ,	name, lp,	=0)
//#define VN_CSTR_PROPERTY(name,l )			VG_PROPERTY_STR(		name, sz,l	)
//#define VN_TYPE_PROPERTY(name,T )			VG_PROPERTY2(T,		name, _)
//#define VN_VECTOR_PROPERTY(name,T )			VG_PROPERTY2(T ,		name, _v)


#define VN_PTR_GET_PROPERTY(name,Type )	_VG_GET_PROPERTY_NOVAR(Type*,			name, p)
#define VN_PPTR_GET_PROPERTY(name,Type )	_VG_GET_PROPERTY_NOVAR(Type**,		name, pp)
#define VN_INT_GET_PROPERTY(name )			_VG_GET_PROPERTY_NOVAR(INT ,			name, n)
#define VN_INT2_GET_PROPERTY(name )			_VG_GET_PROPERTY_NOVAR(INT ,			name, i)
#define VN_UINT_GET_PROPERTY(name )			_VG_GET_PROPERTY_NOVAR(UINT ,			name, u)
#define VN_BYTE_GET_PROPERTY(name )			_VG_GET_PROPERTY_NOVAR(BYTE ,			name, by)
#define VN_SHORT_GET_PROPERTY(name )		_VG_GET_PROPERTY_NOVAR(short ,		name, st)
#define VN_DWORD_GET_PROPERTY(name )		_VG_GET_PROPERTY_NOVAR(DWORD ,		name, dw)
#define VN_LONG_GET_PROPERTY(name )			_VG_GET_PROPERTY_NOVAR(LONG ,			name, l)
#define VN_FLOAT_GET_PROPERTY(name )		_VG_GET_PROPERTY_NOVAR(float ,		name, f)
#define VN_LONGLONG_GET_PROPERTY(name )	_VG_GET_PROPERTY_NOVAR(LONGLONG ,	name, ll)
//#define VN_CSTR_GET_PROPERTY(name,l )		VG_PROPERTY_GET_STR(		name, sz,l	)
//#define VN_TYPE_GET_PROPERTY(name,T )		VG_GET_PROPERTY2(T,		name, _)
//#define VN_VECTOR_GET_PROPERTY(name,T )	VG_GET_PROPERTY2(T,		name, _v)


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////
template <class T>
inline T* ConstructInPlace(T* p)
{
	(p);
   return new(p) T;
}

template <class T,class P1>
inline T* ConstructInPlace1(T* p, P1 p1)
{
	(p);
   return new(p) T(p1);
}

template <class T,class P1, class P2>
inline T* ConstructInPlace2(T* p, P1 p1, P2 p2)
{
	(p);
   return new(p) T(p1,p2);
}


template <class T>
inline void DestructInPlace(T* p)
{
	(p);
   p->~T();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef _countof
#define _countof(array) (sizeof(array)/sizeof(array[0]))
#endif


#endif //__COMMONDEFINE_H__
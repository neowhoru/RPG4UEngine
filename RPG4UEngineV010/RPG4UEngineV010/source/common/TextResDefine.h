/*////////////////////////////////////////////////////////////////////////
文 件 名：TextResDefine.h
创建日期：2008年3月22日
最后更新：2008年3月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	文本信息资源定义表



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TEXTRESDEFINE_H__
#define __TEXTRESDEFINE_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eTEXTRES_ID
{
	TEXTRES_BaseID										=	0	,//	最小文本ID
	TEXTRES_UNKNOWN_ERROR							=	1	,//	失败
	TEXTRES_SystemMsg									=	2	,//	系统消息
															//	3	,//	预留
	TEXTRES_Disconnectted							=	4	,//	 连接断开[Error:%s]
	TEXTRES_DisconnecttedReason					=	5	,//	 连接断开[Error:%s]
	TEXTRES_CreateNameError							=	6	,//	名字不合法
	TEXTRES_USERLOGINING								=	7	,//	正在登入…
	TEXTRES_ConnectFail								=	8	,//	连接失败
	TEXTRES_SelectServer								=	9	,//	请选择服务器
	TEXTRES_DisconnecttedAndRetry					=	10	,//	你与服务器的连接已断开！
	TEXTRES_NowTryConnecting						=	11	,//	正在与服务器建立连接…
	TEXTRES_Connected									=	12	,//	已与服务器建立连接 正准备验证
	TEXTRES_GetGate									=	13	,//	正在获取负载较低Gate
	TEXTRES_ConnectToGate							=	14	,//	获取Gate信息成功 正准备连接…
	TEXTRES_SendRightCheck							=	15	,//	发送验证消息
	TEXTRES_WaitingRequest							=	16	,//	请稍候，正在等待服务器返回信息…
	TEXTRES_SelectServerFailed						=	17	,//	连接目标服务器失败
	TEXTRES_DataBaseFailed							=	18	,//	远程数据库连接出错
	TEXTRES_LoginServerFailedAndTry				= 	19	,//	预留
															//	20	,//	预留
															//	21	,//	预留
															//	22	,//	预留
	TEXTRES_LOGIN_VersionError						=	23	,//	登录失败:客户端和服务器版本不匹配！
	TEXTRES_LOGIN_UnKnown							=	24	,//	登录失败:未知错误！
	TEXTRES_LOGIN_UserUnknowError					=	25	,//	登录失败:未知错误!可能是用户名或密码错误！
	TEXTRES_LOGIN_AccountNameTooLong				=	26	,//	特别提示:帐号过长！
	TEXTRES_LOGIN_UserError							=	27	,//	登录失败:错误的用户名！
	TEXTRES_LOGIN_PasswordError					=	28	,//	登录失败:错误的密码！
	TEXTRES_LOGIN_PasswordNotMatch				=	29	,//	登录失败:用户名和密码不匹配！
	TEXTRES_LOGIN_UseOnline							=	30	,//	特别提示：该账号已在服务器，请再次登陆
	TEXTRES_LOGIN_Duplicated_Account				=	31	,//	重复帐号
	TEXTRES_LOGIN_Account_NotFound				=	32	,//	登录失败:用户不存在…
															//	33	,//	预留
															//	34	,//	预留
															//	35	,//	预留
															//	36	,//	预留
															//	37	,//	预留
															//	38	,//	预留
	TEXTRES_PlayerNameRepeat						=	39	,//	创建失败:角色重名！
	TEXTRES_PlayerDeleteFailed						=	40	,//	删除失败
	TEXTRES_LOGIN_DeletePlayerConfirm			=	41	,//	在删除角色之前，请输入帐号注册时身份证号码：
	TEXTRES_CreateCheckNameOK						=	42	,//	恭喜！该人物名可以正常使用
	TEXTRES_CreateCheckNameNO						=	43	,//	对不起，该人物名已被他人使用
	TEXTRES_CharacterFull							=	44	,//	角色数量已满
	TEXTRES_InvalidSecurity							=	45	,//	无效的验证信息
	TEXTRES_CreateCharFailed						=	46	,//	创建人物失败
	TEXTRES_SelectCharacter							=	47	,//	请选择人物
	TEXTRES_NameIsBlank								=	48	,//	名字不能为空
	TEXTRES_NameTooLong								=	49	,//	名字过长
															//	50	,//	预留
	TEXTRES_SERVERGROUP_IDLE						=	51	,//	空闲:%3d%%
	TEXTRES_SERVERGROUP_GOOD						=	52	,//	推荐:%3d%%
	TEXTRES_SERVERGROUP_BUSINESS					=	53	,//	繁忙:%3d%%
	TEXTRES_SERVERGROUP_FULL						=	54	,//	满载:%3d%%
															//	55	,//	预留
															//	56	,//	预留
															//	57	,//	预留
															//	58	,//	预留
															//	59	,//	预留
															//	60	,//	预留
	TEXTRES_EnterWorldFailed						=	61	,//	进入游戏世界失败
	TEXTRES_CharSelect_OutOfRangeIndex			=	62	,//	人物列表越界
	TEXTRES_CharInfo_DuplicateReq					=	63	,//	重复请求
	TEXTRES_Connection_NetYesService				=	64	,//	服务未启动
	TEXTRES_Connection_RequestFromGameServer	=	65	,//	请求来自GameServer
	TEXTRES_Connection_NotExistLinkServer		=	66	,//	相连的服务器不存在
	TEXTRES_Connection_RoomDoingTransaction	=	67	,//	Room正在处理中
	TEXTRES_Connection_MapNotFound				=	68	,//	指定的地图不存在
	TEXTRES_Connection_ZoneStateWrong			=	69	,//	登入状态有误
	TEXTRES_CharInfo_DBInfo_Failed				=	70	,//	人物数据库信息查询失败
	TEXTRES_AlreadyInZone							=	71	,//	你已经在世界场景了
															//	72	,//	预留
															//	73	,//	预留
															//	74	,//	预留
															//	75	,//	预留
															//	76	,//	预留
															//	77	,//	预留
															//	78	,//	预留
															//	79	,//	预留
															//	80	,//	预留
															//	81	,//	预留
															//	82	,//	预留
															//	83	,//	预留
															//	84	,//	预留
															//	85	,//	预留
															//	86	,//	预留
															//	87	,//	预留
															//	88	,//	预留





															//	94		,//	预留
															//	95		,//	预留
															//	96		,//	预留
															//	97		,//	预留
	TEXTRES_BARGAIN_Ask								=	98		,//	请求交易！是否接受？
	TEXTRES_BARGAIN_OK								=	99		,//	确定交易？
	TEXTRES_BARGAIN_OtherExit						=	100	,//	对方退出交易？
	TEXTRES_BARGAIN_Error_1							=	101	,//	对方正在和别人交易中!
	TEXTRES_BARGAIN_Error_2							=	102	,//	对方不同意交易
	TEXTRES_BARGAIN_Error_3							=	103	,//	对方取消交易!
	TEXTRES_BARGAIN_fail								=	104	,//	交易失败!
	TEXTRES_BARGAIN_timeout							=	105	,//	交易超时!
	TEXTRES_BARGAIN_ItemCnt							=	106	,//	要放上物品数量
	TEXTRES_BARGAIN_NotMoney						=	107	,//	没有足够的钱！
	TEXTRES_BARGAIN_InputCnt						=	108	,//	请输入正确的数量!
	TEXTRES_BARGAIN_NotGrid							=	109	,//	没有空间放物品!
	TEXTRES_BARGAIN_InputBuyCnt					=	110	,//	输入买物品数量
	TEXTRES_BARGAIN_InputSellCnt					=	111	,//	输入卖出物品数量
															//	112	,//	预留
															//	113	,//	预留
															//	114	,//	预留
															//	115	,//	预留
															//	116	,//	预留
															//	117	,//	包裹已满！
															//	118	,//	输入取物品数量
															//	119	,//	预留
															//	120	,//	预留
															//	121	,//	预留
															//	122	,//	预留
															//	123	,//	预留
															//	124	,//	预留
															//	125	,//	预留
															//	126	,//	预留
	TEXTRES_UI_Yes										=	127	,//	是
	TEXTRES_UI_NO										=	128	,//	否
															//	129,//	预留
															//	130,//	预留
															//	131,//	预留
															//	132,//	预留
															//	133,//	预留
															//	134,//	预留
	TEXTRES_UI_InputBox								=	135,//	输入框
	TEXTRES_UI_InputThrowCnt						=	136,//	输入丢弃物品数量
	TEXTRES_UI_Exchange								=	137,//	交易
	TEXTRES_UI_AddFriend								=	138,//	加为好友
	TEXTRES_UI_Team									=	139,//	组队
	TEXTRES_UI_FellowMe								=	140	,//	跟随
	TEXTRES_UI_Price									=	141	,//	价格
	TEXTRES_UI_ExitGame								=	142	,//	退出游戏
	TEXTRES_UI_InputName								=	143	,//	请输入姓名！
	TEXTRES_UI_PlayerFull							=	144	,//	角色已满！
	TEXTRES_UI_BuyConfirm							=	145	,//	你确定要购买吗？
	TEXTRES_Ask_Task									=	146	,//	是否放弃此任务
	TEXTRES_UI_ItemLvl_0								=	147	,//	普通
	TEXTRES_UI_ItemLvl_1								=	148	,//	基本
	TEXTRES_UI_ItemLvl_2								=	149	,//	精致
	TEXTRES_UI_ItemLvl_3								=	150	,//	稀有
	TEXTRES_UI_ItemLvl_4								=	151	,//	神器
	TEXTRES_EQUIP_Durable										=	152	,//	耐久点数
															//	153	,//	预留
															//	154	,//	预留
															//	155	,//	预留
															//	156	,//	预留
															//	157	,//	预留
															//	158	,//	预留
	TEXTRES_UI_LevelUp								=	159	,//	你的等级提升了
	TEXTRES_UI_SkillLevelup							=	160	,//	你的技能等级提升了！
	TEXTRES_UI_ConfirmThrowItem					=	161	,//	你确定要丢弃 此物品
	TEXTRES_UI_NotMemoy								=	162	,//	你没有足够的钱！
	TEXTRES_UI_ShopNotItem							=	163	,//	商店现在没有这么多物品
	TEXTRES_UI_EquipFail								=	164	,//	装备或卸载失败!
	TEXTRES_UI_MpNoEnough							=	165	,//	MP不足!
	TEXTRES_Item_UseFail								=	166	,//	物品使用失败!
	TEXTRES_TwoHandEqiupError_0					=	167	,//	你左手已有物品，不能装备双手武器了:)
	TEXTRES_TwoHandEqiupError_1					=	168	,//	你装备了2个单手剑，不能装备双手武器了:)
	TEXTRES_TwoHandEqiupError_2					=	169	,//	你装备了双手武器，不能装备盾牌了:)
	TEXTRES_UI_EquipItem								=	170	,//	你换上了装备
	TEXTRES_UI_UnequipItem							=	171	,//	你卸下了装备
															//	172	,//	预留
															//	173	,//	预留
															//	174	,//	预留
															//	175	,//	预留
															//	176	,//	预留
	TEXTRES_NeedItem									=	177	,//	需要任务物品
	TEXTRES_UI_NeedLevel								=	178	,//	需要级别
	TEXTRES_UI_NeedSex								=	179	,//	性别需求
	TEXTRES_UI_NeedMight								=	180	,//	力量需求
	TEXTRES_UI_NeedAgile								=	181	,//	敏捷需求
	TEXTRES_UI_NeedConstitution					=	182	,//	体质需求
	TEXTRES_UI_NeedSupernatural					=	183	,//	法力需求
	TEXTRES_UI_NeedPerception						=	184	,//	感知需求
	TEXTRES_UI_NeedLuck								=	185	,//	运气需求
	TEXTRES_UI_NeedProfession						=	186	,//	职业需求
															//	187	,//	预留
															//	188	,//	预留
															//	189	,//	预留
	TEXTRES_SEX_UNKNOWN								=	190	,//	未知性别
	TEXTRES_SEX_MAN									=	191	,//	男
	TEXTRES_SEX_FEMAN									=	192	,//	女
															//	193	,//	预留
															//	194	,//	预留
															//	195	,//	预留
															//=	196	,//
															//=	197	,//
															//=	198	,//
															//=	199	,//
															//=	200	,//
															//=	201	,//
															//	202	,//	预留
															//	203	,//	预留
															//	204	,//	预留
															//	205	,//	预留
															//	206	,//	预留
															//	207	,//	预留
															//	208	,//	预留
	TEXTRES_EQUIP_SingleSword						=	209	,//	未知武器
	TEXTRES_EQUIP_SingleSword_0					=	210	,//	未知武器
	TEXTRES_EQUIP_SingleSword_1					=	211	,//	双手斧
	TEXTRES_EQUIP_SingleSword_2					=	212	,//	双手剑
	TEXTRES_EQUIP_SingleSword_3					=	213	,//	单手剑
	TEXTRES_EQUIP_SingleSword_4					=	214	,//	矛
	TEXTRES_EQUIP_SingleSword_5					=	215	,//	鞭
	TEXTRES_EQUIP_SingleSword_6					=	216	,//	轻爪
	TEXTRES_EQUIP_SingleSword_7					=	217	,//	单手弓
	TEXTRES_EQUIP_SingleSword_8					=	218	,//	原力枪
	TEXTRES_EQUIP_SingleSword_9					=	219	,//	弯刀
	TEXTRES_EQUIP_SingleSword_10					=	220	,//	杖
	TEXTRES_EQUIP_SingleSword_11					=	221	,//	球
	TEXTRES_EQUIP_SingleSword_12					=	222	,//	预留
	TEXTRES_EQUIP_SingleSword_13					=	223	,//	徒手格斗
	TEXTRES_EQUIP_SingleSword_14					=	224	,//	匕首
	TEXTRES_EQUIP_SingleSword_15					=	225	,//	盾牌
	TEXTRES_EQUIP_SingleSword_16					=	226	,//	单手斧
	TEXTRES_EQUIP_SingleSword_17					=	227	,//	拳击   
															//	229	,//	预留
															//	230	,//	预留
															//	231	,//	预留
															//	232	,//	预留
															//	233	,//	预留
															//	234	,//	预留
															//	235	,//	预留
															//	236	,//	预留
															//	237	,//	预留
															//	238	,//	预留
															//	239	,//	预留
															//	240	,//	预留
															//	241	,//	预留
															//	242	,//	预留
															//	243	,//	预留
															//	244	,//	预留
	TEXTRES_EQUIPTYPE_UnKnow						=	245	,//	未知类型
	TEXTRES_EQUIPTYPE_hair							=	246	,//	头发
	TEXTRES_EQUIPTYPE_helmet						=	247	,//	头盔
	TEXTRES_EQUIPTYPE_face							=	248	,//	脸
	TEXTRES_EQUIPTYPE_armour						=	249	,//	衣服
	TEXTRES_EQUIPTYPE_glove							=	250	,//	手套
	TEXTRES_EQUIPTYPE_shoe							=	251	,//	鞋子
	TEXTRES_EQUIPTYPE_rhanditem					=	252	,//	右手物品
	TEXTRES_EQUIPTYPE_lhanditem					=	253	,//	左手物品
	TEXTRES_EQUIPTYPE_headwear						=	254	,//	头饰
	TEXTRES_EQUIPTYPE_HandShiedld					=	255	,//	盾牌
	TEXTRES_EQUIPTYPE_Shoulder						=	256	,//	肩胛
	TEXTRES_EQUIPTYPE_Trousers						=	257	,//	裤子
	TEXTRES_EQUIPTYPE_accouterment				=	258	,//	饰物
	TEXTRES_EQUIPTYPE_ring							=	259	,//	戒指
	TEXTRES_EQUIPTYPE_bangle						=	260	,//	手镯
	TEXTRES_EQUIPTYPE_necklace						=	261	,//	项链
	TEXTRES_EQUIPTYPE_sash							=	262	,//	腰带
	TEXTRES_EQUIPTYPE_ring2							=	263	,//	戒指
															//	264	,//	预留
															//	265	,//	预留
															//	266	,//	预留
															//	267	,//	预留
															//	268	,//	预留
															//	341	,//	预留
														
															//	348	,//	预留
															//	349	,//	预留
															//	350	,//	预留
														
															//	421	,//	预留
															//	422	,//	预留
															//	423	,//	预留
															//	424	,//	预留
															//	425	,//	预留
															//	444	,//	预留
															//	445	,//	预留
															//	446	,//	预留
															//	447	,//	预留
															//	448	,//	预留
	TEXTRES_UI_NeedText								=	449	,//	需要
	TEXTRES_UI_NextLevel								=	450	,//	下一级
	TEXTRES_YouGet										=	451	,//	你得到了
	TEXTRES_YouLost									=	452	,//	你失去了
	TEXTRES_UI_YouTo									=	453	,//	你向
															//=	454,//
	TEXTRES_Fail										=	455	,//	失败
	TEXTRES_Prompt										=	456	,//	提示
	TEXTRES_UI_ask										=	457	,//	询问
	TEXTRES_Second										=	458	,//	秒
	TEXTRES_And											=	459	,//	与
	TEXTRES_Confirm									=	460	,//	确认
	TEXTRES_complete									=	461	,//	完成
	TEXTRES_OverCount									=	462	,//	数量超过！
															//	463	,//	预留
															//	464	,//	预留
															//	465	,//	预留
	TEXTRES_Enemy										=	466	,//	敌人
	TEXTRES_Monster									=	467	,//	怪物
	TEXTRES_Player										=	468	,//	玩家
	TEXTRES_TeamMember								=	469	,//	队友
	TEXTRES_Boss										=	470	,//	Boss怪物
															//	471	,//	预留
															//	472	,//	预留
															//	473	,//	预留
	TEXTRES_InTeam										=	474	,//	你已经在一个队伍里
	TEXTRES_TargetInTeam								=	475	,//	目标已经在队伍中了
	TEXTRES_FindNotPlayer							=	476	,//	找不到你要加的人
	TEXTRES_TeamFull									=	477	,//	队伍已经满了
	TEXTRES_TeamFail									=	478	,//	组队失败
	TEXTRES_TeamScuscc								=	479	,//	组队成功
	TEXTRES_AddTeam_Ask								=	480	,//	要加入你的队伍，你同意吗?
	TEXTRES_Ask_AddTeam								=	481	,//	邀请你加入队伍，你同意吗?
	TEXTRES_UI_SendTeam_fail						=	482	,//	发送组队申请失败
	TEXTRES_UI_SendTeam								=	483	,//	发送组队申请
															//	484	,//	预留
															//	485	,//	预留
															//	486	,//	预留
															//	487	,//	预留
															//	488	,//	预留
															//	489	,//	预留
	TEXTRES_TargetAttRange							=	490	,//	目标不在攻击距离内
	TEXTRES_AttTarget									=	491	,//	没有正对攻击目标
	TEXTRES_Ask_Select								=	492	,//	请选择
	TEXTRES_BeginFight								=	493	,//	开始战斗
	TEXTRES_ExitFight									=	494	,//	结束战斗
															//	495	,//	预留
															//	496	,//	预留
															//	497	,//	预留
															//	498	,//	预留
	TEXTRES_SystemBull								=	499	,//	系统公告:
	TEXTRES_Chat_0										=	500	,//	[私]:
	TEXTRES_Chat_1										=	501	,//	[交易]:
	TEXTRES_Chat_2										=	502	,//	[队]:
	TEXTRES_Chat_3										=	503	,//	[会]:
	TEXTRES_Chat_4										=	504	,//	[呐喊]:
	TEXTRES_Chat_5										=	505	,//	[天下]:
	TEXTRES_Chat_6										=	506	,//	[全]:
															//	507	,//	预留
															//	508	,//	预留
															//	509	,//	预留
															//	510	,//	预留
	TEXTRES_BARGAIN_Error_3_fail					=	511	,//	对方取消交易
	TEXTRES_BARGAIN_UnKnown							=	512	,//	交易失败，未知错误
	TEXTRES_BARGAIN_sucess							=	513	,//	交易成功
	TEXTRES_BARGAIN_fail_0							=	514	,//	交易失败
	TEXTRES_UI_SendExchange							=	515	,//	发送交易申请
	TEXTRES_UI_SendExchange_fail					=	516	,//	发送交易申请失败
	TEXTRES_BARGAIN_DOING							=	517	,//	交易中…
															//	518	,//	预留
															//	519	,//	预留
															//	520	,//	预留
															//	521	,//	预留
	TEXTRES_UI_Req_AddGuild							=	522	,//	请求加入行会
	TEXTRES_UI_Req_AddGuild_1						=	523	,//	请求加入行会
	TEXTRES_UI_Call_AddGuild						=	524	,//	邀请您加入行会
	TEXTRES_UI_GuildCallOn							=	525	,//	行会邀请
															//	526	,//	预留
															//	527	,//	预留
															//	528	,//	预留
															//	529	,//	预留
															//	530	,//	预留
	TEXTRES_UI_SendShopSeek_fail					=	531	,//	请求查看个人商店失败
	TEXTRES_UI_SendShopSeek							=	532	,//	发送查看个人商店
	TEXTRES_UI_SendAddRoom_fail					=	533	,//	请求加入房间失败
	TEXTRES_UI_SendAddRoom							=	534	,//	发送加入房间请求
															//	535	,//	预留
															//	536	,//	预留
															//	537	,//	预留
															//	538	,//	预留
	TEXTRES_UI_Money_Error_0						=	539	,//	你输入钱的数量超过你身上钱的数量！
	TEXTRES_UI_InputSaveMoney						=	540	,//	请输入您存钱的数量
	TEXTRES_UI_BackSave_error						=	541	,//	你银行储备不足
	TEXTRES_UI_Take_Money							=	542	,//	请输入您取钱的数量
	TEXTRES_UI_Bank_LevelUp_error					=	543	,//	你身上钱不足,无法升级银行！
	TEXTRES_UI_Bank_LevelUp_Text_0				=	544	,//	你升级银行需要
	TEXTRES_UI_Bank_LevelUp_Text_1				=	545	,//	钱，确定升级银行?
	TEXTRES_EnterUsePass								=	546	,//	请输入用户名或密码
	TEXTRES_ShopRefresh								=	547	,//	商店中的物品已刷新!
	TEXTRES_Sell_Price								=	548	,//	输入卖出的价格
	TEXTRES_ChangPrice								=	549	,//	改变该物品价格
	TEXTRES_PrivateShop_Error						=	550	,//	你的个人商品中没有任何物品不能开店
	TEXTRES_EnterShopName							=	551	,//	请填写你个人商店的名字!
	TEXTRES_Sell_Price_0								=	552	,//	出售价格
															//	553	,//	预留
															//	554	,//	预留
															//	555	,//	预留
															//	556	,//	预留
	TEXTRES_Set_Materials							=	557	,//	请放入合成原料
	TEXTRES_Take_CompItem							=	558	,//	请取出合成目标栏物品
	TEXTRES_Input_Cnt									=	559	,//	请输入数量
	TEXTRES_CompSucess								=	560	,//	合成成功
	TEXTRES_CompFail									=	561	,//	合成失败
	TEXTRES_Material_Error_0						=	562	,//	原料不符合
	TEXTRES_Material_error_1						=	563	,//	宝石等级已是最高级
	TEXTRES_Input_MoveCnt							=	564	,//	请输入移动的数量
	TEXTRES_Ask_insert_Gem							=	565	,//	是否镶入宝石?
	TEXTRES_Ask_BuyCnt								=	566	,//	请输入要购买的物品数量
	TEXTRES_UseItem									=	567	,//	使用物品
															//	568	,//	预留
															//	569	,//	预留
															//	570	,//	预留
															//	571	,//	预留
															//	572	,//	预留
	TEXTRES_HPNoEnough								=	573	,//	HP不足!
	TEXTRES_MPNoEnough								=	574	,//	MP不足!
	TEXTRES_Motion_CantSing							=	575	,//	运动中不能吟唱
	TEXTRES_Jump_CantCharge							=	576	,//	跳跃中不能进行冲锋
	TEXTRES_UseSkill_NeedWeap						=	577	,//	没装备使用技能需要的武器
	TEXTRES_PassiveSkill_NotUse					=	578	,//	被动技能不可以被使用
	TEXTRES_NeedSkill_0								=	579	,//	你必需先学得相应的采矿技能
	TEXTRES_NeedSkill_1								=	580	,//	你必需先学得相应的采药技能
	TEXTRES_NeedSkill_2								=	581	,//	你必需先学得相应的伐木技能
	TEXTRES_NeedSkill_3								=	582	,//	你必需先学得相应的开箱技能
	TEXTRES_Input_SaveCnt							=	583	,//	请输入存入物品的数量
	TEXTRES_StudySkill								=	584	,//	你学会了技能
	TEXTRES_NoEnoughItemToUseSkill				=	585	,//	没有携带相应的道具来使用技能
	TEXTRES_CannotUsePassivitySkill				=	586	,//	不能使用被动技能
	TEXTRES_CannotUseSkillInCurStatus			=	587	,//	不能在当前状态下使用技能
	TEXTRES_SkillIsInColdDown						=	588	,//	技能在冷却中…
	TEXTRES_ItemNoPlace								=	589	,//	您的背包栏没有空位置了
	TEXTRES_AttAdd										=	590	,//	攻击速度加成
	TEXTRES_Null1										=	591	,//	目标超出范围
	TEXTRES_TargetNotInTheView						=	592	,//	目标不在视野中
	TEXTRES_BannedPK									=	593	,//	本服务器禁止PK
															//	594	,//	预留
															//	595	,//	预留
															//	596	,//	预留
															//	597	,//	预留
	TEXTRES_SelectConleMode							=	598	,//	请选择操作方式
	TEXTRES_GetItemFromOtherPlaceCostMoney		=	599	,//	异地取东西需要收取邮政费用
	TEXTRES_MakeSureToGetItemFromOtherPlace	=	600	,//	钱，确认异地取物品？
	TEXTRES_InputItemPwd								=	601	,//	请输入物品加锁密码
	TEXTRES_IsIntensify								=	602	,//	是否强化？
	TEXTRES_ItemPwdError								=	603	,//	您输入的物品加密密码错误
	TEXTRES_IsSellItem								=	604	,//	是否出售物品？
	TEXTRES_InputSellItemCnt						=	605	,//	请输入出售物品的数量
	TEXTRES_ItemNoBuy									=	606	,//	注意:本店将低价收购此物品!
	TEXTRES_MoneyNotEnoungh							=	607	,//	你没有足够的钱！
	TEXTRES_PwdNull									=	608	,//	物品加锁密码不能为空
	TEXTRES_ItemHaveBeenLocked						=	609	,//	物品已经加锁
	TEXTRES_YouHaveLeft								=	610	,//	提示：您还有
	TEXTRES_DaysToClearItemProtectPwd			=	611	,//	天，即可清除物品保护密码！
	TEXTRES_CanNotPushItemIntoStorage			=	612	,//	不能把物品放入仓库
	TEXTRES_BankTradeSucceed						=	613	,//	银行交易成功
	TEXTRES_BankTradeBankFull						=	614	,//	存的钱超过钱柜上限
	TEXTRES_BankTradePlayerNotMoney				=	615	,//	玩家没有这么多钱
	TEXTRES_BankTradeBankNotMoney					=	616	,//	钱柜里没这么多钱
	TEXTRES_BankTradeUpgradeMax					=	617	,//	钱柜已是最高级
	TEXTRES_BankTradeFalied							=	618	,//	打开银行失败
	TEXTRES_ItemCannotBeenLocked					=	619	,//	该物品不能被加锁
	TEXTRES_ItemMaxStoreNum							=	620	,//	物品最大存储数目为:
	TEXTRES_ThisItemCanNotDrop						=	621	,//	本物品不能被丢弃
	TEXTRES_ItemCanNotBeenMoved					=	622	,//	该物品不能移动
															//	623	,//	预留
															//	624	,//	预留
															//	625	,//	预留
															//	626	,//	预留
	TEXTRES_NoneAddMe									=	627	,//	不能加自己为好友
	TEXTRES_AddFriRepeat								=	628	,//	已经添加了此好友
	TEXTRES_FriNotOnline								=	629	,//	你加的好友不在线
	TEXTRES_FriOutLine								=	630	,//	好友已经离线
	TEXTRES_FriComeOnLine							=	631	,//	有好友上线了
	TEXTRES_FriCutDownLine							=	632	,//	有好友下线了
															//	633	,//	预留
															//	634	,//	预留
															//	635	,//	预留
															//	636	,//	预留
															//	637	,//	预留
	TEXTRES_ITEMTYPE_RESTORE						=	638	,//	恢复药品
	TEXTRES_ITEMTYPE_WEAPON							=	639	,//	武器
	TEXTRES_ITEMTYPE_ARMOUR							=	640	,//	防具
	TEXTRES_ITEMTYPE_SKILL							=	641	,//	技能
	TEXTRES_ITEMTYPE_OTHER							=	642	,//	其他
	TEXTRES_ITEMTYPE_STONE							=	643	,//	符石
	TEXTRES_ITEMTYPE_STATUS							=	644	,//	状态
	TEXTRES_ITEMTYPE_ACTION							=	645	,//	基本动作
	TEXTRES_ITEMTYPE_REEL							=	646	,//	卷轴
	TEXTRES_ITEMTYPE_MATERIAL						=	647	,//	原料
	TEXTRES_ITEMTYPE_MEDAL							=	648	,//	勋章
	TEXTRES_ITEMTYPE_TASK							=	649	,//	任务
	TEXTRES_ITEMTYPE_TOOL							=	650	,//	工具
	TEXTRES_ITEMTYPE_GEM								=	651	,//	宝石
	TEXTRES_ITEMTYPE_CREATEITEMRULE				=	652	,//	合成配方
	TEXTRES_ITEMTYPE_CARD							=	653	,//	卡片
	TEXTRES_ITEMTYPE_CRYSTAL      				=	654	,//	水晶
															//	655	,//	预留
															//	656	,//	预留
															//	657	,//	预留
															//	658	,//	预留
															//	659	,//	预留
															//	660	,//	预留
															//	661	,//	预留
															//	662	,//	预留
															//	663	,//	预留
															//	664	,//	预留

};			

/*
*/


#endif //__TEXTRESDEFINE_H__
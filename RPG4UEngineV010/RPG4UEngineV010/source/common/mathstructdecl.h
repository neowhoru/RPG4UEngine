/*////////////////////////////////////////////////////////////////////////
文 件 名：MathStructDecl.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	数据基本数据结构...

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHSTRUCTDECL_H__
#define __MATHSTRUCTDECL_H__
#pragma once


namespace math
{ 


////////////////////////////////////////////////////
template<typename Type>
struct TPoint2D
{
	union
	{
		struct
		{
			 Type x;
			 Type y;
		};
		Type m_Element[2];
		Type m[2];
	};
};

template<typename Type>
struct TPoint3D
{
	union
	{
		struct
		{
			Type x;
			Type y;
			Type z;
		};
		Type	m[3];
		Type	m_Element[3];
	};

	////////////////////////////////////////////////////
	TPoint3D& operator ()	(Type x0, Type y0, Type z0);
	TPoint3D& operator ()	(const TPoint3D&		);
	TPoint3D& operator ()	(const Type  fScalar);
	TPoint3D& operator ()	(const Type* fDat	);

    // casting
    operator Type* ();
    operator const Type* () const;

	////////////////////////////////////////////////////
	 //不允许出现赋值，有机会用在union结构中
    //TPoint3D& operator = ( const TPoint3D&			);
    //TPoint3D& operator = ( const Type  fScalar	);
    //TPoint3D& operator = ( const Type* fDat		);

	////////////////////////////////////////////////////
	// assignment operators
	TPoint3D& operator += ( CONST TPoint3D& );
	TPoint3D& operator -= ( CONST TPoint3D& );
	//TPoint3D& operator *= ( CONST TPoint3D& );
	//TPoint3D& operator /= ( CONST TPoint3D& );
	TPoint3D& operator += ( Type );
	TPoint3D& operator -= ( Type );
	TPoint3D& operator *= ( Type );
	TPoint3D& operator /= ( Type );

	// unary operators
	TPoint3D operator + () const;
	TPoint3D operator - () const;

	// binary operators
	TPoint3D operator + ( CONST TPoint3D& ) const;
	TPoint3D operator - ( CONST TPoint3D& ) const;
	//TPoint3D operator * ( CONST TPoint3D& ) const;
	//TPoint3D operator / ( CONST TPoint3D& ) const;
	TPoint3D operator + ( Type ) const;
	TPoint3D operator - ( Type ) const;
	TPoint3D operator * ( Type ) const;
	TPoint3D operator / ( Type ) const;

	BOOL operator == ( CONST TPoint3D& ) const;
	BOOL operator != ( CONST TPoint3D& ) const;
	BOOL operator <  ( CONST TPoint3D& ) const;
	BOOL operator >  ( CONST TPoint3D& ) const;

	////////////////////////////////////////////////////////
	Type LengthSquared () const;	///> 获取长度的平方，本函数，因为运算简单，效率相对高出许多

	TPoint3D& SwapXY();
	TPoint3D& SwapYZ();
	TPoint3D& SwapXZ();


	////////////////////////////////////////////////////////
	inline friend TPoint3D operator / ( const Type fScalar, const TPoint3D& rkVector )
	{
		TPoint3D ret;
		return ret(	 fScalar / rkVector.x,	fScalar / rkVector.y, fScalar / rkVector.z);
	}

	inline friend TPoint3D operator + (const Type f , const TPoint3D& v)
	{
		TPoint3D ret;
		return ret(		f + v.x ,		f + v.y ,		f + v.z);
	}

	inline friend TPoint3D operator - (const Type f , const TPoint3D& v)
	{
		TPoint3D ret;
		return ret(		f - v.x ,		f - v.y,		f - v.z);
	}
	inline friend TPoint3D operator * (const Type f , const TPoint3D& v)
	{
		TPoint3D ret;
		return ret(		f * v.x ,		f * v.y,		f * v.z);
	}

};

template<typename Type>
struct TPoint4D
{
	union
	{
		struct
		{
			 Type x;
			 Type y;
			 Type z;
			 Type w;
		};
		Type m[4];
		Type m_Element[4];
	};
};

template<typename Type>
struct TPlaneStruct
{
	union
	{
		struct
		{
			TPoint3D<Type> m_Normal;
			Type				m_fD;
		};
		struct
		{
			 Type a;
			 Type b;
			 Type c;
			 Type d;
		};
		Type m[4];
		Type m_Element[4];
	};
};

template<typename Type>
struct TColor4DStruct
{
	union
	{
		struct
		{
			 Type r;
			 Type g;
			 Type b;
			 Type a;
		};
		struct
		{
			 Type red;
			 Type green;
			 Type blue;
			 Type alpha;
		};
		Type m[4];
		Type m_Element[4];
	};
};


////////////////////////////////////////////////////
template<typename Type>
struct TMatrix3Struct 
{
	union 
	{
		struct 
		{
			Type    _11, _12, _13;
			Type    _21, _22, _23;
			Type    _31, _32, _33;
		};
		struct 
		{
			Type    M11, M12, M13;
			Type    M21, M22, M23;
			Type    M31, M32, M33;
		};
		Type	m[3][3];
		Type	m_Element[3][3];
		Type	m_Array[9];
    };
};


////////////////////////////////////////////////////
template<typename Type>
struct TMatrix4Struct 
{
	union 
	{
#ifdef OPTIMIZATION_MATRIX
		struct 
		{
			__m128 _L1, _L2, _L3, _L4;
		};
#endif

		struct 
		{
			Type    _11, _12, _13, _14;
			Type    _21, _22, _23, _24;
			Type    _31, _32, _33, _34;
			Type    _41, _42, _43, _44;
		};
		struct 
		{
			Type    M11, M12, M13, M14;
			Type    M21, M22, M23, M24;
			Type    M31, M32, M33, M34;
			Type    M41, M42, M43, M44;
		};
		Type	m[4][4];
		Type	m_Element[4][4];
		Type	m_Array[16];
    };
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

};//namespace math

#include "MathStructDecl.inl"

typedef math::TPoint2D<int>			SPoint2I,POINT2I,*LPPOINT2I;
typedef math::TPoint2D<REAL>			SPoint2D,POINT2D,*LPPOINT2D;

typedef math::TPoint3D<int>			SPoint3I,POINT3I,*LPPOINT3I;
typedef math::TPoint3D<REAL>			SPoint3D,POINT3D,*LPPOINT3D;
typedef math::TPoint3D<REAL>			VECTOR3D;

typedef math::TPoint4D<int>			SPoint4I,POINT4I,*LPPOINT4I;
typedef math::TPoint4D<REAL>			SPoint4D,POINT4D,*LPPOINT4D;
typedef math::TPoint4D<REAL>			SQuaternion,QUATERNION,*LPQUATERNION;
typedef math::TPoint4D<REAL>			VECTOR4D;

//typedef math::TPlaneStruct<REAL>		SPlane,PLANE,*LPPLANE;

typedef math::TMatrix4Struct<REAL>	SMatrix,MATRIX,*LPMATRIX;
typedef math::TMatrix4Struct<REAL>	MATRIX4;

typedef math::TMatrix3Struct<REAL>	SMatrix3,MATRIX3,*LPMATRIX3;

#endif //__MATHSTRUCT_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TLinkedList.h
创建日期：2008年1月15日
最后更新：2008年1月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TLINKEDLIST_H__
#define __TLINKEDLIST_H__
#pragma once



#include "TMemoryPoolFactory.h"

using namespace util;

typedef void* POS;


namespace util
{



template <class T, class numType = DWORD >
class TLinkedList
{
public:
	TLinkedList(DWORD dwBlockSize = 10) : m_pHead(0), m_pTail(0), m_nCount(0)
	{
		m_pNodePool = new TMemoryPoolFactory<NODE>;
		m_pNodePool->Initialize(dwBlockSize, dwBlockSize/2+1);
	}
	virtual ~TLinkedList()
	{
		DeleteAll();
		m_pNodePool->Release();
		delete m_pNodePool;
	}
	
protected:
    class NODE
    {
	public:
        T           pElement;
        NODE *		pPrev;
        NODE *		pNext;
        NODE() { pPrev=0; pElement=0; pNext=0; }
    };
    
    NODE *	m_pHead;
    NODE *	m_pTail;
    numType	m_nCount;
	TMemoryPoolFactory<NODE>	* m_pNodePool;

public:
    // Get element of head
    inline T GetHead(){ return m_pHead->pElement; }
    
    // Add to linked list head
    void AddHead(T pElement)
    {
        NODE *newNode = m_pNodePool->Alloc();
        newNode->pPrev = 0;
        newNode->pElement = pElement;
        newNode->pNext = 0;
        
        if (0 == m_pHead)
        {
            m_pHead = newNode;
            m_pHead->pNext = 0;
            m_pTail = m_pHead;
        }
        else
		{
            m_pHead->pPrev = newNode;
            newNode->pNext = m_pHead;
            m_pHead = newNode;
        }
        ++m_nCount;
    }
    
    // Delete head
    T DeleteHead()
    {
		if (!m_pHead) return 0;

        T pElement = 0;
        if( m_pHead == m_pTail )
        {
            pElement = m_pTail->pElement;
            
            m_pNodePool->Free(m_pHead);
            m_pHead = m_pTail = 0;
            m_nCount = 0;
        }
        else 
		{
            NODE *pCurrent = m_pHead;
            m_pHead = m_pHead->pNext;
            m_pHead->pPrev = 0;
            
            pElement = pCurrent->pElement;
            
            m_pNodePool->Free(pCurrent);
            --m_nCount;
        }
        return pElement;
    }
    

    
    inline T GetTail(){ return m_pTail->pElement; }
    
    // Add to linked list tail
    void AddTail(T pElement)
    {
        NODE *newNode = m_pNodePool->Alloc();
        newNode->pPrev = 0;
        newNode->pElement = pElement;
        newNode->pNext = 0;
        
        if (0 == m_pTail)
        {
            m_pTail = newNode;
            m_pHead = m_pTail;
        }
        else
		{
            m_pTail->pNext = newNode;
            newNode->pPrev = m_pTail;
            m_pTail = newNode;
        }
        ++m_nCount;
    }
    
    // Delete tail
    T DeleteTail()
    {
		if (!m_pTail) return 0;

        T pElement = 0;
        if( m_pTail == m_pHead )
        {
            pElement = m_pTail->pElement;
            
            m_pNodePool->Free(m_pTail);
            m_pTail = m_pHead = 0;
            m_nCount = 0;
        }
        else 
		{
            NODE *pCurrent = 0;
            NODE *pAfter = m_pHead;
            while(1)
            {
                pCurrent = pAfter->pNext;
                if( pCurrent->pNext == 0 )
					break;
                pAfter = pCurrent;
            }
            pAfter->pNext = 0;
            m_pTail = pAfter;
            
            pElement = pCurrent->pElement;
            
            m_pNodePool->Free(pCurrent);
            --m_nCount;
        }
        return pElement;
    }
    

    
    // Insert element before index
    numType InsertBefore(T pElement, numType index)
    {
        if (index == 0) AddHead(pElement);
        else {
            NODE *pAfter = 0;
            NODE *pCurrent = m_pHead;
            while (0 < index-- && pCurrent) {
                pAfter = pCurrent;
                pCurrent = pCurrent->pNext;
            }
        
            NODE *newNode = m_pNodePool->Alloc();
            newNode->pPrev = pCurrent->pPrev;
            newNode->pElement = pElement;
            newNode->pNext = pCurrent;
        
            pCurrent->pPrev = newNode;
            pAfter->pNext = newNode;
        
            ++m_nCount;
        }
        return 0;
    }
    
    // Insert element after index
    numType InsertpAfter(T pElement, numType index)
    {
        if( index == m_nCount - 1 ) AddTail(pElement);
        else
		{
            NODE *pAfter = 0;
            NODE *pCurrent = m_pHead;
            while( 0 < index-- && pCurrent )
			{
                pAfter = pCurrent;
                pCurrent = pCurrent->pNext;
            }
            
            NODE *newNode = m_pNodePool->Alloc();
            newNode->pPrev = pCurrent;
            newNode->pElement = pElement;
            newNode->pNext = pCurrent->pNext;
            
            pAfter->pPrev = newNode;
            pCurrent->pNext = newNode;
            
            ++m_nCount;
        }
        return 0;
    }

  
    inline POS GetFirstPos() { return (POS)m_pHead; }
    inline POS GetLastPos() { return (POS)m_pTail; }
    
    // Pos to index
    numType GetPosIndex(POS pos)
    {
        NODE *pCurrent = m_pHead;
        for( numType i = 0 ; pCurrent ; ++i )
        {
            if( pos == (POS)pCurrent )
				return i;
            pCurrent = pCurrent->pNext;
        }
        return -1;
    }
    
    // Index to pos
    POS GetIndexPos(numType index)
    {
        NODE *pCurrent = m_pHead;
        while( 0 < index-- && pCurrent )
            pCurrent = pCurrent->pNext;
        
        return (POS)pCurrent;
    }
	// taiyo test add
	T GetAtPos(POS& pos)
	{
		T pElement;
        if (pos)
        {
            pElement = ((NODE*)pos)->pElement;
        }
        else 
		{
            pElement = 0;
        }
        return pElement;
	}
    
    // Get previous position of node
    T GetPrevPos(POS& pos)
    {
        T pElement;
        if (pos)
        {
            pElement = ((NODE*)pos)->pElement;
            pos = (POS)((NODE*)pos)->pPrev;
        }
        else
		{
            pElement = 0;
            pos = 0;
        }
        return pElement;
    }
    
    // Get next position of node
    T GetNextPos(POS& pos)
    {
        T pElement;
        if (pos)
        {
            pElement = ((NODE*)pos)->pElement;
            pos = (POS)((NODE*)pos)->pNext;
        }
        else
		{
            pElement = 0;
            pos = 0;
        }
        return pElement;
    }
    
    // Delete position of node
    T DeletePos(POS& pos)
    {
        T pElement = 0;
        if (m_pHead == (NODE*)pos)
        {
            pElement = DeleteHead();
            pos = (POS)m_pHead;
        }
        else if (m_pTail == (NODE*)pos)
        {
            pElement = DeleteTail();
            pos = (POS)m_pTail;
        }
        else
		{
            NODE *pAfter = m_pHead;
            NODE *pCurrent = m_pHead->pNext;
            while (pCurrent != (NODE*)pos)
            {
                pAfter = pCurrent;
                pCurrent = pCurrent->pNext;
            }
            pCurrent->pNext->pPrev = pAfter;
            pAfter->pNext = pCurrent->pNext;
            pos = (POS)pAfter;
            
            pElement = pCurrent->pElement;
            
            m_pNodePool->Free(pCurrent);
            --m_nCount;
        }
        return pElement;
    }
    
    
    // Get total number of node
    inline numType GetCount() { return m_nCount; }
    
    // Get node by index number : 0base
    T GetAt(numType index)
    {
        NODE *pCurrent = m_pHead;
        while (0 < index-- && pCurrent)
            pCurrent = pCurrent->pNext;
        
        return pCurrent->pElement;
    }
    
    // Delete at
    T DeleteAt(numType index)
    {
        if (index == 0) return DeleteHead();
        else if( index == m_nCount - 1 ) return DeleteTail();
        else
		{
            NODE *pAfter = m_pHead;
            NODE *pCurrent = m_pHead->pNext;
            while( index-- )
            {
                pAfter = pCurrent;
                pCurrent = pCurrent->pNext;
            }
            pCurrent->pNext->pPrev = pAfter;
            pAfter->pNext = pCurrent->pNext;
            
            T pElement = pCurrent->pElement;
            
            m_pNodePool->Free(pCurrent);
            --m_nCount;
            
            return pElement;
        }
        return 0;
    }
    
    // Delete all node
    void DeleteAll()
    {
        NODE *pCurrent = m_pHead;
        NODE *pAfter = 0;
        while( pCurrent )
        {
            pAfter = pCurrent->pNext;
            m_pNodePool->Free(pCurrent);
            pCurrent = pAfter;
        }
        m_pHead = m_pTail = 0;
        m_nCount = 0;
    }

};


} /// namespace util



#endif //__TLINKEDLIST_H__
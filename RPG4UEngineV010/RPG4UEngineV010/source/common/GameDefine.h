/*////////////////////////////////////////////////////////////////////////
文 件 名：GameDefine.h
创建日期：2007年9月24日
最后更新：2007年9月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEDEFINE_H__
#define __GAMEDEFINE_H__
#pragma once

#include "ChatDefine.h"



//路径方向
enum PATH_DIR
{
	 DIR_0
	,DIR_45
	,DIR_90
	,DIR_135
	,DIR_180
	,DIR_225
	,DIR_270
	,DIR_315
	,DIR_MAX
};

static DWORD gs_DIRDEGREEs[DIR_MAX]=
{
	 0
	,45
	,90
	,135
	,180
	,225
	,270
	,315
};



enum eVOBJ_SHOW_FLAG
{
	 VOBJ_SHOW_NOSHADOW			= _BIT(1)	// 脚下有阴影
	,VOBJ_SHOW_NOCAMERACHECK	= _BIT(2)	// 不做Camera Alpha检测
};


enum eFIGHT_FLAG
{
	 FIGHTFLAG_NORMAL		= 0			// 普通状态
	,FIGHTFLAG_FIGHTING	= _BIT(0)	// 战斗中
};


#define MOVEVELOCITY			(3.0f)						// 每秒移动格数，也就是人物移动速率
#define ONESTEPCOST			(1000.0f/MOVEVELOCITY)	//一步消耗的时间，毫秒


/////////////////////////////////////////////////////
// 战斗状态标志
enum eFIGHT_STATE
{
	 eFIGHT_NOFLAG			= 0			// 没有特殊状态
	,eFIGHT_RELIVE			= _BIT(0)	// 复活
	,eFIGHT_MISS			= _BIT(1)	// 攻击未命中
	,eFIGHT_BEKILLED		= _BIT(2)	// 被杀
	,eFIGHT_BACKSTRIKE	= _BIT(3)	// 背击
	,eFIGHT_CRITICALHIT	= _BIT(4)	// 暴击
	,eFIGHT_DEADLYHIT		= _BIT(5)	// 致命一击
	,eFIGHT_CANCEL			= _BIT(6)	// 取消
	,eFIGHT_ERROR			= _BIT(7)	// 战斗出错
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////


const DWORD ATTACK_STAND_TIME						= 5000;
const DWORD MAX_ITEM_INFO							= 1000;
const DWORD MAX_ITEM_TYPE							= 50  ;
const DWORD MAX_NPC_INFO							= 1000;
const DWORD MAX_SKILL_INFO							= 1000;
const DWORD MAX_SKILL_INVENTORY_STORE_INFO	= 1000;
const DWORD MAX_STYLE_INFO							= 1000;
const DWORD MAX_MAP_INFO							= 100;
const DWORD MAX_SHOP_INFO							= 1000;
const DWORD MAX_ITEM_COMPOSITE_INFO				= 100  ;
const DWORD MAX_ITEM_MAKE_INFO					= 200  ;
const DWORD MAX_RATE_INFO							= 50  ;
const DWORD MAX_QUESTITEM_DROPINFO				= 100  ;
const DWORD MAX_ITEM_RANDATTR_INFO				= 100  ;
const DWORD MAX_ITEM_DOUBLEATTR_INFO			= 100	 ;
const DWORD MAX_ITEM_RANDATTR_TABLE				= 50	 ;
const DWORD MAX_SOCKET_DESC_INFO					= 50	 ;
const DWORD MAX_GRAPHIC_INFO						= 2000; 
const DWORD MAX_SCRIPT_INFO						= 100  ;
const DWORD MAX_EFFECT_INFO						= 100  ;
const DWORD MAX_HORSE_INFO							= 20  ;
const DWORD MAX_BGM									= 100  ;
const DWORD MAX_STYLE_QUICK_REGISTER			= 100;
const DWORD MAX_ITEM_SOUND_INFO					= 50 ;
const DWORD MAX_NPC_PORTAL_INFO					= 50 ;
const DWORD MAX_ENCHANT_INFO						= 100;

const DWORD MAX_MISSION_MESSAGE_LENGTH			= 1024;
const DWORD MAX_MESSAGE_LENGTH					= 512  ;
const DWORD MAX_EVENT_LINE							= 4;
const DWORD GROUP_ID_NONE							= 0 ;

const DWORD MAX_ICON_IMAGE				=	50000		  ;




const DWORD MAX_SLOTINFO_LINE			=	30	;
const DWORD MAX_SLOTINFO_LENGTH		=	256;
const DWORD MAX_TEMP_VARIABLE			=	20	;

const COLOR TOOLTIP_FONT_COLOR		=		COLOR_RGBA(0xff,0xff,0xff,0xff);
const COLOR TOOLTIP_BG_COLOR			=		COLOR_RGBA(0,0,0,200);



const float DISTANCE_NPC_MEET					=	6.0f	;	
const float DISTANCE_NPC_MEET_EXT			=	0.70f	;	
const float DISTANCE_SPACE_GET_ITEM			=	5.0f	;
const float DISTANCE_TAB_TARGET_MONSTER	=	30.0f	;
const float DISTANCE_TRADING					=	6.0f  ;
const float DISTANCE_FOLLOW_PLAYER			=	3.0f	;


#endif //__GAMEDEFINE_H__
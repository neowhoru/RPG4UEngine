/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstItem.h
创建日期：2008年2月27日
最后更新：2008年2月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTITEM_H__
#define __CONSTITEM_H__
#pragma once






/*////////////////////////////////////////////////////////////////////////
武器音效类型
/*////////////////////////////////////////////////////////////////////////
enum eWEAPONSOUNDKIND
{
	 eWEAPONSOUNDKIND_NULL					= -1
	,eWEAPONSOUNDKIND_TWOHANDED			= 0
	,eWEAPONSOUNDKIND_ONEHANDSWORD		= 1
	,eWEAPONSOUNDKIND_SPEAR					= 2
	,eWEAPONSOUNDKIND_WHIP					= 3
	,eWEAPONSOUNDKIND_ETHERCLAW			= 4
	,eWEAPONSOUNDKIND_ARROW					= 5
	,eWEAPONSOUNDKIND_ETHERBULLET			= 6
	,eWEAPONSOUNDKIND_STAFF					= 7
	,eWEAPONSOUNDKIND_MAGICBULLET			= 8
	,eWEAPONSOUNDKIND_UNDEFINED			= 9
	,eWEAPONSOUNDKIND_MONSTERPHYSICAL	= 10
	,eWEAPONSOUNDKIND_MONSTERMAGICAL		= 11
	,eWEAPONSOUNDKIND_SHIELD				= 12
	,eWEAPONSOUNDKIND_DAGGER				= 13

	,eWEAPONSOUNDKIND_MAX 
};




/*////////////////////////////////////////////////////////////////////////
///药品类型
/*////////////////////////////////////////////////////////////////////////
enum eITEM_WASTE_TYPE
{
	 ITEMWASTE_HPPOTION								= 1 ///< HP恢复
	,ITEMWASTE_MPPOTION								= 2 ///< MP恢复
	,ITEMWASTE_MOVE_SPEED_INC						= 3 ///< 移动加速
	,ITEMWASTE_BASE_PHYSICAL_ATTACKPWR_INC		= 4 ///< 物攻加强
	,ITEMWASTE_BASE_MAGICAL_ATTACKPWR_INC		= 5 ///< 魔攻加强
	,ITEMWASTE_BASE_PHYSICAL_DEFENSEPWR_INC	= 6 ///< 物防加强
	,ITEMWASTE_BASE_MAGICAL_DEFENSEPWR_INC		= 7 ///< 魔防加强
	,ITEMWASTE_EXPPOTION								= 8 ///< 增强经验值
	,ITEMWASTE_STATUS_POTION						= 9 ///< 状态药物值
	,ITEMWASTE_HPMPPOTION							= 10///< HPMP恢复
	,ITEMWASTE_HPRATE_POTION						= 11///< HP恢复百分比
	,ITEMWASTE_MPRATE_POTION						= 12///< MP恢复百分比
	,ITEMWASTE_HPMPRATE_POTION						= 13///< HPMP恢复百分比
	,ITEMWASTE_MAX

};


/*////////////////////////////////////////////////////////////////////////
/// 物品合成类型
/*////////////////////////////////////////////////////////////////////////
enum eITEM_COMPOSITE_TYPE
{
	 ITEMCOMPOSITE_NONE					=	0	// 空
	,ITEMCOMPOSITE_LOW_ENCHANT			=	1	// 初级强化
	,ITEMCOMPOSITE_MEDIUM_ENCHANT		=	2	// 中级强化
	,ITEMCOMPOSITE_HIGH_ENCHANT		=	3	// 高级强化
	,ITEMCOMPOSITE_RANK_DOWN			=	4	// 
	,ITEMCOMPOSITE_EXTRACT_SOCKET		=	6	// 拆除宝石
	,ITEMCOMPOSITE_ADD_SOCKET			=	7	// 加槽
	,ITEMCOMPOSITE_LOW_RING				=	8	// 初级戒指
	,ITEMCOMPOSITE_HIGH_RING			=	9	// 高级戒指
	,ITEMCOMPOSITE_LOW_NECKLACE		=  10 // 初级项链
	,ITEMCOMPOSITE_HIGH_NECKLACE		=  11 // 高级项链
	,ITEMCOMPOSITE_SOCKET_FILL			=	12	// 镶嵌宝石
	,ITEMCOMPOSITE_ITEM_COMPOUND		=	13	// 普通物品合成
	,ITEMCOMPOSITE_ITEM_MAKE			=	14	// 物品打造
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eVITEM_SHOW_TYPE
{
	 VITEMSHOW_AUTO		= 0///< 自动选择
	,VITEMSHOW_ICON		= 1///< 原图标
	,VITEMSHOW_ICONBIG	= 2///< 大图标
	,VITEMSHOW_MODEL		= 3///< 模式
	,VITEMSHOW_HIDE		= 4///< 隐藏光标
	,VITEMSHOW_MAX

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eVITEMTYPE
{
	 VITEMTYPE_NONE				=	0	//
	,VITEMTYPE_RESTORE 			=	1	//恢复药品
	,VITEMTYPE_WEAPON				=	2	//武器
	,VITEMTYPE_ARMOUR				=	3	//防具
	,VITEMTYPE_SKILL				=	4	//技能
	,VITEMTYPE_OTHER				=	5	//其他
	,VITEMTYPE_STONE				=	6	//符石
	,VITEMTYPE_STATUS				=	7	//状态
	,VITEMTYPE_ACTION				=	8	//基本动作
	,VITEMTYPE_REEL				=	9	//卷轴
	,VITEMTYPE_MATERIAL			=	10	//原料
	,VITEMTYPE_MEDAL				=	11	//勋章
	,VITEMTYPE_TASK				=	12	//任务
	,VITEMTYPE_TOOL				=	13	//工具
	,VITEMTYPE_GEM					=	14	//宝石
	,VITEMTYPE_CREATEVITEMRULE	=	15	//合成配方
	,VITEMTYPE_CARD				=	16	// 卡片
	,VITEMTYPE_CRYSTAL	      =	17	//水晶
	,VITEMTYPE_ICON				=	18	//图标    
	,VITEMTYPE_MAX
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eITEM_TYPE
{
	 ITEMTYPE_INVALID				= 0	
	// 武器
	,ITEMTYPE_WEAPON_MIN			= 1
	,ITEMTYPE_TWOHANDAXE			= 1 	//	双手斧
	,ITEMTYPE_TWOHANDSWORD		= 2 	//	双手剑
	,ITEMTYPE_ONEHANDSWORD		= 3 	//	单手剑
	,ITEMTYPE_SPEAR				= 4 	//	矛
	,ITEMTYPE_WHIP					= 5 	//	鞭
	,ITEMTYPE_ETHERCLAW			= 6 	//	轻爪
	,ITEMTYPE_CROSSBOW			= 7 	//	单手弓
	,ITEMTYPE_ETHERWEAPON		= 8 	//	原力枪
	,ITEMTYPE_SCIMITAR			= 9 	//	弯刀
	,ITEMTYPE_STAFF				= 10	//	杖
	,ITEMTYPE_ORB					= 11	//	球
	,ITEMTYPE_BOW					= 12	//	弓
	,ITEMTYPE_COMBAT				= 13	// 徒手格斗
	,ITEMTYPE_DAGGER				= 14	// 匕首
	,ITEMTYPE_SHIELD				= 15	// 盾牌
	,ITEMTYPE_ONEHANDAXE			= 16	//	单手斧
	,ITEMTYPE_BOXING				= 17	//	拳击
	,ITEMTYPE_WEAPON_MAX			= 100

	// 防具
	,ITEMTYPE_ARMOR_MIN			= 101
	,ITEMTYPE_ARMOR				= 101	//	盔甲
	,ITEMTYPE_PROTECTOR			= 201	//	护肩
	,ITEMTYPE_HELMET				= 301	//	头盔
	,ITEMTYPE_PANTS				= 401	//	裤
	,ITEMTYPE_BOOTS				= 501	//	靴
	,ITEMTYPE_GLOVE				= 601	//	手套
	,ITEMTYPE_BELT					= 701	//	腰带
	,ITEMTYPE_SHIRTS				= 801	//	衬衣
	,ITEMTYPE_CAPE					= 802	//	披风
	,ITEMTYPE_ARMOR_MAX			= 810

	// 饰物
	,ITEMTYPE_SUPPLY_MIN					= 811
	,ITEMTYPE_RING							= 811	//	戒指 
	,EITEMTYPE_BANGEL						= 812	//	手镯 
	,EITEMTYPE_ACCOUNTERMENT			= 813	//	饰品
	,ITEMTYPE_NECKLACE					= 821	//	项链
	,ITEMTYPE_HEADWARE					= 822	//	头饰
	,ITEMTYPE_HAIR							= 823	//	发型
	,ITEMTYPE_FACE							= 824	//	脸型
	,ITEMTYPE_WARRIOR_SACCESSORY		= 831	// 力士辅助
	,ITEMTYPE_PALADIN_SACCESSORY		= 841	//	游侠辅助
	,ITEMTYPE_STABBER_SACCESSORY		= 851	//	刺客辅助
	,ITEMTYPE_POWWOW_SACCESSORY		= 861	//	术士辅助
	,ITEMTYPE_ELEMENTALIST_SACCESSORY= 871	//	巫师辅助
	,ITEMTYPE_SUPPLY_MAX					= 910

	,ITEMTYPE_WASTE_MIN					= 911
	,ITEMTYPE_WASTE						= 911		//	药物
	,ITEMTYPE_TASK							= 912		//	任务物品
	,ITEMTYPE_REEL							= 913		//	卷轴
	,ITEMTYPE_MATERIAL					= 914		//	原料
	,ITEMTYPE_ENCHANT						= 921		//	加强挤
	,ITEMTYPE_CRYSTAL						= 931		//	辅助宝石
	,ITEMTYPE_SOCKET						= 941		//	可镶嵌宝石
	,ITEMTYPE_ARROW						= 951		//	箭支
	,ITEMTYPE_SOCKETMATERIAL			= 961		//	可安装原料
	,ITEMTYPE_ACCESSORYMATERIAL		= 971		//	辅助型原料
	,ITEMTYPE_SACCESSORYMATERIAL		= 981		//	辅助型原料
	,ITEMTYPE_WASTE_MAX					= 1000

	,ITEMTYPE_TOPMAX						= 1000	//

};


/*////////////////////////////////////////////////////////////////////////
武器类型
/*////////////////////////////////////////////////////////////////////////
enum eWEAPON_TYPE
{
	 WEAPONTYPE_INVALID				= 0	///<
	,WEAPONTYPE_TWOHANDAXE			= 1	///<双手斧
	,WEAPONTYPE_TWOHANDSWORD		= 2	///<双手剑
	,WEAPONTYPE_ONEHANDSWORD		= 3	///<单手剑
	,WEAPONTYPE_SPEAR					= 4	///<矛
	,WEAPONTYPE_WHIP					= 5	///<鞭
	,WEAPONTYPE_ETHERCLAW			= 6	///<轻爪
	,WEAPONTYPE_CROSSBOW				= 7	///<单手弓
	,WEAPONTYPE_ETHERWEAPON			= 8	///<原力枪
	,WEAPONTYPE_SCIMITAR				= 9	///<弯刀  
	,WEAPONTYPE_STAFF					= 10	///<杖
	,WEAPONTYPE_ORB					= 11	///<球
	,WEAPONTYPE_BOW					= 13	///<
	,WEAPONTYPE_COMBAT				= 13 // 徒手格斗
	,WEAPONTYPE_DAGGER				= 14 // 匕首
	,WEAPONTYPE_SHIELD				= 15 // 盾牌
	,WEAPONTYPE_ONEHANDAXE			= 16	//	单手斧
	,WEAPONTYPE_BOXING				= 17	//	拳击    
};											  				 

/*////////////////////////////////////////////////////////////////////////
装备质地
/*////////////////////////////////////////////////////////////////////////
enum	eARMOUR_TEX_TYPE
{
	 ARMOUR_TEX_NULL					= -1
	,ARMOUR_TEX_CLOTH					= 0
	,ARMOUR_TEX_FEATHER				= 1
	,ARMOUR_TEX_LEATHER				= 2
	,ARMOUR_TEX_STONE					= 3
	,ARMOUR_TEX_IRON					= 4
	,ARMOUR_TEX_MAX					= 5
};

/*////////////////////////////////////////////////////////////////////////
装备硬度
/*////////////////////////////////////////////////////////////////////////
enum eARMOR_TYPE
{
	 eARMOR_HARD		= 1	// HEAVY
	,eARMOR_MEDIUM		= 2	// MEDIUM
	,eARMOR_SOFT		= 3	// sLIGHT_INFO
	,eARMOR_SIEGE		= 4
	,eARMOR_UNARMOR	= 5

	,eARMOR_TYPE_MAX
};


/////////////////////////////////////////////////////////////////////////////////////////
// Item
/////////////////////////////////////////////////////////////////////////////////////////
enum eITEM_FUNCTIONAL_TYPE
{
	 ITEM_FUNCTYPE_ENCHANT					= _BIT(0)
	,ITEM_FUNCTYPE_RANKUP					= _BIT(1)
	,ITEM_FUNCTYPE_ADDSOCKET				= _BIT(2)
	,ITEM_FUNCTYPE_FILLSOCKET				= _BIT(3)
	,ITEM_FUNCTYPE_EXTRACTSOCKET			= _BIT(4)
	,ITEM_FUNCTYPE_CREATEACCESSORY		= _BIT(5)
	,ITEM_FUNCTYPE_USE_WAREHOUSE			= _BIT(6)
	,ITEM_FUNCTYPE_USE_TRADE				= _BIT(7)
	,ITEM_FUNCTYPE_USE_VENDOR				= _BIT(8)
	,ITEM_FUNCTYPE_USE_GUILD				= _BIT(9)
	,ITEM_FUNCTYPE_ALL						= _MASK(10)
};


/*////////////////////////////////////////////////////////////////////////
///打造原料类型
/*////////////////////////////////////////////////////////////////////////
enum eMATERIAL_TYPE
{
	 MATERIALTYPE_NONE			= 0 ///< 未知材料
	,MATERIALTYPE_STONE			= 1 ///< 矿石
	,MATERIALTYPE_WOOD			= 2 ///< 木材
	,MATERIALTYPE_SKIN			= 3 ///< 皮料
	,MATERIALTYPE_BONE			= 4 ///< 骨材
	,MATERIALTYPE_TENDON			= 5 ///< 筋材
	,MATERIALTYPE_GEM				= 6 ///< 玉石
	,MATERIALTYPE_VEGETABLE		= 7 ///< 素类
	,MATERIALTYPE_MEAT			= 8 ///< 肉类
	,MATERIALTYPE_LIQUID			= 9 ///< 液料
	,MATERIALTYPE_ACCESSORY		= 10///< 辅助物
	,MATERIALTYPE_MAX
};



/*////////////////////////////////////////////////////////////////////////
物品用途类型
/*////////////////////////////////////////////////////////////////////////
enum eITEM_USE_TYPE
{
	 ITEMUSETYPE_NONE			= 0 // 未定
	,ITEMUSETYPE_WEAPON		= _BIT(0) //1 武器
	,ITEMUSETYPE_ARMOR		= _BIT(1) //2 防具
	,ITEMUSETYPE_ACCESSORY	= _BIT(2) //4  辅助

	,ITEMUSETYPE_USE			= _BIT(4) //16 可用
	,ITEMUSETYPE_COMPOUND	= _BIT(5) //32 可合成

	,ITEMUSETYPE_EQUIP		= _MASK(4)//15 可换装			ITEMUSETYPE_WEAPON|ITEMUSETYPE_ARMOR 
	,ITEMUSETYPE_EQUIP_USE	= _MASK(5)//31 两者				ITEMUSETYPE_EQUIP|ITEMUSETYPE_USE
	,ITEMUSETYPE_INDEX_MAX	= 6
};

/*////////////////////////////////////////////////////////////////////////
物品附加属性
/*////////////////////////////////////////////////////////////////////////
enum eITEM_OPTION										 
{															  
	 ITEM_OPTION1
	,ITEM_OPTION2
	,ITEM_OPTION3
	,ITEM_OPTION4
	,ITEM_OPTION5
	,ITEM_OPTION_MAX
};



/*////////////////////////////////////////////////////////////////////////
	潜隐属性
/*////////////////////////////////////////////////////////////////////////
enum ePOTENTIAL
{
	 POTENTIAL_0 = 0
	,POTENTIAL_1
#ifdef MORE_POTENIAL
	,POTENTIAL_2
	,POTENTIAL_3
#endif
	,POTENTIAL_MAX
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum	eENCHANT_MATERIAL_TYPE
{
	  ENCHANTMATERIAL_NONE			= -1
	, ENCHANTMATERIAL_WEAPON		= 0	/// 武器类
	, ENCHANTMATERIAL_ARMOR			= 1	/// 防具类
	, ENCHANTMATERIAL_ACCESSORY	= 2	/// 辅助物
	, ENCHANTMATERIAL_WASTE			= 3	/// 药物类
	, ENCHANTMATERIAL_COMPOUND		= 4	/// 原料类
	, ENCHANTMATERIAL_MAX
};


/*////////////////////////////////////////////////////////////////////////
	物品增强分段
/*////////////////////////////////////////////////////////////////////////
enum eENCHANTLEVEL
{
	ENCHANT_LOW,		//< 0 ~ 6
	ENCHANT_MIDDLE,	//< 7 ~ 9
	ENCHANT_HIGH,		//< 10 ~ 12
};


/*////////////////////////////////////////////////////////////////////////
	品质级别
/*////////////////////////////////////////////////////////////////////////
enum eRANK
{
	 RANK_NIL = -1		///< 普通料
	,RANK_9 = 0			///< 九品
	,RANK_8
	,RANK_7
	,RANK_6

	,RANK_5
	,RANK_4				///< 四品
	,RANK_3

	,RANK_2
	,RANK_1				///< 一品
	,RANK_0				///< 极品

	,RANK_MAX
	,RANK_AMOUNT = RANK_MAX + 1
};

enum eRANK_LEVEL
{
	 RANK_LOW		= 0	///<	NIL 9品
	,RANK_MIDDLE	= 1	///<	8 ~ 6品
	,RANK_HIGH		= 2	///<	5 ~ 3品
	,RANK_HIGHER	= 3	///<	2 ~ 1品
	,RANK_RARE		= 4	///<	极品
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eITEM_SOCKET_TYPE
{
	  ITEMSOCKETTYPE_NORMAL			= 0	/// 普通宝石
	 ,ITEMSOCKETTYPE_RANDOM			= 1	/// 随机属性宝石
	 ,ITEMSOCKETTYPE_DOUBLE			= 2	/// 攻防孖宝石
	 ,ITEMSOCKETTYPE_ELEMENT		= 3	/// 五行宝石
	 ,ITEMSOCKETTYPE_POTENTIAL		= 4	/// 潜隐宝石/激活潜隐属性
	 ,ITEMSOCKETTYPE_MAX
};
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSOCKET_LEVEL
{
	 SOCKETLV_LOW = 0
	,SOCKETLV_HIGH
	,SOCKETLV_MAX
};
//#endif

enum eSOCKET_TYPE
{
	 eSOCKET_TYPE_DOUBLE	/// 攻防孖属性
	,eSOCKET_TYPE_RANDOM	/// 随机属性	
};

enum eSOCKET_VALUE
{
	 eSOCKET_VALUE_1		/// 攻属性		主属性
	,eSOCKET_VALUE_2		/// 攻属性Max	随机属性1
	,eSOCKET_VALUE_3		/// 防属性		随机属性2
	,eSOCKET_VALUE_MAX
};


enum eSOCKET
{
	 SOCKET_1 = 0
	,SOCKET_2
	,SOCKET_3
	,SOCKET_MAX
};

enum eSOCKET_OPTION
{
	 SOCKET_OPTION_1 = 0
	,SOCKET_OPTION_2
	,SOCKET_OPTION_3
	,SOCKET_OPTION_MAX
};



//-------------------------------------------------------------------------------
enum eNUMERIC_TYPE
{
	 TYPE_NUMERIC		= 1
	,TYPE_PERCENTAGE	= 2
};


//-------------------------------------------------------------------------------
// 对外开放属性接口
//-------------------------------------------------------------------------------
static const eATTRIBUTE_TYPE gs_ItemOptions[] = 
{
	 ATTRIBUTE_TYPE_INVALID								//  0	无属性
	,ATTRIBUTE_ADD_SKILL_ATTACK_POWER				//	 1	技能攻击力
	,ATTRIBUTE_ADD_SKILL_DAMAGE_RATIO				//	 2	技能伤害率      
	,ATTRIBUTE_OPTION_PHYSICAL_ATTACK_POWER		//  3 物理攻击
	,ATTRIBUTE_OPTION_MAGICAL_ATTACK_POWER			//  4 魔法攻击
	,ATTRIBUTE_OPTION_ALL_ATTACK_POWER				//  5 全体攻击
	,ATTRIBUTE_BASE_MELEE_MIN_ATTACK_POWER			//  6 近身min攻击
	,ATTRIBUTE_BASE_MELEE_MAX_ATTACK_POWER			//  7 近身max攻击
	,ATTRIBUTE_BASE_RANGE_MIN_ATTACK_POWER			//  8 远距min攻击
	,ATTRIBUTE_BASE_RANGE_MAX_ATTACK_POWER			//  9 远距max攻击
	,ATTRIBUTE_BASE_TRAP_MIN_ATTACK_POWER			// 10 陷阱min攻击
	,ATTRIBUTE_BASE_TRAP_MAX_ATTACK_POWER			// 11 陷阱max攻击
	,ATTRIBUTE_MAGICAL_GOLD_ATTACK_POWER			//	12 金系攻击
	,ATTRIBUTE_MAGICAL_WATER_ATTACK_POWER			//	13 水系攻击
	,ATTRIBUTE_MAGICAL_WOOD_ATTACK_POWER			//	14 木系攻击
	,ATTRIBUTE_MAGICAL_FIRE_ATTACK_POWER			//	15 火系攻击
	,ATTRIBUTE_MAGICAL_EARTH_ATTACK_POWER			//	16 土系攻击
	,ATTRIBUTE_MAGICAL_WIND_ATTACK_POWER			//	17 风系攻击
	,ATTRIBUTE_MAGICAL_DIVINE_ATTACK_POWER			//	18 光系攻击
	,ATTRIBUTE_MAGICAL_DARKNESS_ATTACK_POWER		//	19 暗系攻击
	,ATTRIBUTE_OPTION_PHYSICAL_DEFENSE_POWER		// 20 物理防御附加
	,ATTRIBUTE_OPTION_MAGICAL_DEFENSE_POWER		// 21 魔法防御
	,ATTRIBUTE_OPTION_ALL_DEFENSE_POWER				//	22	全体防御属性
	,ATTRIBUTE_BASE_MELEE_DEFENSE_POWER				// 23 近身防御
	,ATTRIBUTE_BASE_RANGE_DEFENSE_POWER				// 24 远距防御
	,ATTRIBUTE_BASE_TRAP_DEFENSE_POWER				// 25 陷阱防御
	,ATTRIBUTE_MAGICAL_GOLD_DEFENSE_POWER			//	26	金系防御
	,ATTRIBUTE_MAGICAL_WATER_DEFENSE_POWER			//	27	水系防御
	,ATTRIBUTE_MAGICAL_WOOD_DEFENSE_POWER			//	28	木系防御
	,ATTRIBUTE_MAGICAL_FIRE_DEFENSE_POWER			//	29	火系防御          
	,ATTRIBUTE_MAGICAL_EARTH_DEFENSE_POWER			//	30	土系防御
	,ATTRIBUTE_MAGICAL_WIND_DEFENSE_POWER			//	31	风系防御
	,ATTRIBUTE_MAGICAL_DIVINE_DEFENSE_POWER		//	32	光系防御
	,ATTRIBUTE_MAGICAL_DARKNESS_DEFENSE_POWER		//	33	暗系防御
	,ATTRIBUTE_DEL_PHYSICAL_TARGET_DEFENSE_RATIO	//	34 破物防机率
	,ATTRIBUTE_DEL_GOLD_TARGET_DEFENSE_RATIO		//	35	破金防机率
	,ATTRIBUTE_DEL_WATER_TARGET_DEFENSE_RATIO		//	36	破水防机率
	,ATTRIBUTE_DEL_WOOD_TARGET_DEFENSE_RATIO		//	37	破木防机率
	,ATTRIBUTE_DEL_FIRE_TARGET_DEFENSE_RATIO		//	38 破火防机率
	,ATTRIBUTE_DEL_EARTH_TARGET_DEFENSE_RATIO		//	39	破土防机率
	,ATTRIBUTE_DEL_WIND_TARGET_DEFENSE_RATIO		//	40	破风防机率
	,ATTRIBUTE_DEL_DIVINE_TARGET_DEFENSE_RATIO	//	41	破光防机率
	,ATTRIBUTE_DEL_DARKNESS_TARGET_DEFENSE_RATIO	//	42	破暗防机率
	,ATTRIBUTE_ADD_PHYSICAL_CRITICAL_RATIO			// 43 致命一击(物理爆击)
	,ATTRIBUTE_ADD_MAGICAL_CRITICAL_RATIO			// 44 会心一击(魔法爆击)
	,ATTRIBUTE_ADD_ALL_CRITICAL_RATIO				// 45 爆击 
	,ATTRIBUTE_ADD_ATTACK_INC_RATIO					// 46 攻击比
	,ATTRIBUTE_ADD_DEFENSE_INC_RATIO					// 47 防御比
	,ATTRIBUTE_ATTACK_SPEED								// 48 攻击加速%
	,ATTRIBUTE_MOVE_SPEED								//	49	移动加速%
	,ATTRIBUTE_PHYSICAL_ATTACK_SUCCESS_RATIO		//	50 命中率
	,ATTRIBUTE_PHYSICAL_ATTACK_BLOCK_RATIO			//	51	闪避率          
	,ATTRIBUTE_PHYSICAL_ATTACK_SUCCESS_PERCENT	//	52 命中率%
	,ATTRIBUTE_PHYSICAL_ATTACK_BLOCK_PERCENT		//	53	闪避率%
	,ATTRIBUTE_STR											//	54	增加力量
	,ATTRIBUTE_DEX											//	55	增加敏捷
	,ATTRIBUTE_VIT											//	56	增加体质
	,ATTRIBUTE_INT											//	57	增加智慧
	,ATTRIBUTE_SPR											//	58	增加精神
	,ATTRIBUTE_LRN											//	59	增加悟力
	,ATTRIBUTE_CRE											//	60	增加信用
	,ATTRIBUTE_MAX_HP										//	61	增加生命值
	,ATTRIBUTE_MAX_MP										//	62	增加精气值
	,ATTRIBUTE_RECOVERY_HP								// 63 生命回
	,ATTRIBUTE_RECOVERY_MP								// 64 精气回
	,ATTRIBUTE_RECOVERY_HP_PER							// 65 生命回%
	,ATTRIBUTE_RECOVERY_MP_PER							// 66 精气回%
	,ATTRIBUTE_MAX_HP_PER								//	67	增加生命值%
	,ATTRIBUTE_MAX_MP_PER								//	68	增加精气值%
	,ATTRIBUTE_ABSORB_HP									// 69 吸魔
	,ATTRIBUTE_ABSORB_MP									// 70 吸命
	,ATTRIBUTE_SKILL_ATTACK_RANGE						// 71 攻距
	,ATTRIBUTE_SIGHT_RANGE								// 72 视野                  
};

const DWORD MAX_ITEM_OPTIONS	= sizeof(gs_ItemOptions)/sizeof(eATTRIBUTE_TYPE);


#endif //__CONSTITEM_H__
#ifndef __D3DINCLUDE_H__
#define __D3DINCLUDE_H__

#pragma once



#ifdef _USE_DX9

#define DIRECTINPUT_VERSION 0x0900

//#include <dxerr8.h>
#include <D3D9.h>
#include <d3dx9math.h>
#include <d3dx9.h>
#include <dinput.h>
#include <d3d9types.h>
#include <d3dx9core.h>
#include <ddraw.h>


#pragma comment(lib,"d3dx9dt.lib")
//#pragma comment(lib,"dxe9rr8.lib")
#pragma comment(lib,"d3dx9dt.lib")
#pragma comment(lib,"d3d9.lib")
#pragma comment(lib,"d3dxof.lib")
#pragma comment(lib,"dxguid.lib")

#else

#define DIRECTINPUT_VERSION 0x0800

#include <D3D8.h>
#include <d3dx8math.h>
#include <d3dx8.h>
#include <dinput.h>
#include <d3d8types.h>
#include <d3dx8core.h>
#include <ddraw.h>
#include <dxerr8.h>


#pragma comment(lib,"d3dx8dt.lib")
//#pragma comment(lib,"dxe9rr8.lib")
#pragma comment(lib,"d3dx8dt.lib")
#pragma comment(lib,"d3d8.lib")
#pragma comment(lib,"d3dxof.lib")
#pragma comment(lib,"dxguid.lib")
#endif


#endif //__D3DINCLUDE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：BAArray.inl
创建日期：2008年1月25日
最后更新：2008年1月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BAARRAY_INL__
#define __BAARRAY_INL__
#pragma once


template <class T>
BAArray<T>::BAArray()
{
	m_uCount = 0;
	m_uSize = 16;
	m_pBufferPtr = new T [m_uSize];
}


template <class T>
BAArray<T>::~BAArray()
{
	delete [] m_pBufferPtr;
}


template <class T>
void BAArray<T>::Clear( void)
{
	m_uCount = 0;
	ZeroMemory( m_pBufferPtr, m_uSize * sizeof ( T));
}


template <class T>
void BAArray<T>::InvalidateAll( void)
{
	m_uCount = 0;
}

template <class T>
void BAArray<T>::InvalidateAndShrink( void)
{
	delete [] m_pBufferPtr;
	m_uCount = 0;
	m_uSize = 16;
	m_pBufferPtr = new T [m_uSize];
}


template <class T>
void BAArray<T>::EnlargeSize( UINT iSize)
{
	_UpdateDimensionSize( iSize);
}




template <class T>
T& BAArray<T>::Get( UINT iIndex)
{
	if ( iIndex >= m_uSize)
	{	
		static T NullData;
		memset( &NullData, 0, sizeof( T));

		return ( NullData);
	}

	return  m_pBufferPtr[iIndex];
}

template <class T>
void BAArray<T>::Set( UINT iIndex, const T& Data)
{
	_UpdateDimensionSize( iIndex);
	m_pBufferPtr[iIndex] = Data;
	m_uCount = max( iIndex + 1, m_uCount);

	//return ( Data);
}


template <class T>
int BAArray<T>::Add( const T& Data)
{
	Set( m_uCount, Data);
	return ( m_uCount - 1);
}


template <class T>
void BAArray<T>::Remove( UINT iIndex)
{
	if ( --m_uCount > iIndex)
	{
		T *pBufferNew = new T [m_uCount - iIndex];
		memcpy( pBufferNew, &m_pBufferPtr[iIndex + 1], ( m_uCount - iIndex) * sizeof ( T));
		memcpy( &m_pBufferPtr[iIndex], pBufferNew, ( m_uCount - iIndex) * sizeof ( T));
		delete [] pBufferNew;
	}
}

template <class T>
void BAArray<T>::InvalidateFrom( UINT iIndex)
{
	m_uCount = min( iIndex, m_uCount);
}


template <class T>
int BAArray<T>::Find(const T& Data)
{
	for ( int i = 0; i < m_uCount; ++i)
	{
		if ( m_pBufferPtr[i] == Data)
			return ( i);
	}

	return ( -1);
}


template <class T>
void BAArray<T>::_UpdateDimensionSize( UINT iIndex)
{
	if ( iIndex >= m_uSize)
	{
		UINT iNewSize;
		for (iNewSize = m_uSize; iNewSize <= iIndex; iNewSize *= 2)
		{
		}

		T *pBufferNew = new T [iNewSize];
		memcpy( pBufferNew, m_pBufferPtr, m_uSize * sizeof ( T));
		delete [] m_pBufferPtr;
		m_pBufferPtr = pBufferNew;
		m_uSize = iNewSize;
	}
}


template <class T>
BAArray<T>& BAArray<T>::operator = ( const BAArray<T>& RValue)
{
	if ( &RValue == this)
	{
		return ( *this);
	}

	delete [] m_pBufferPtr;

	_UpdateDimensionSize( m_uCount);
	m_uCount = RValue.m_uCount;
	memcpy( m_pBufferPtr, RValue.m_pBufferPtr, m_uCount * sizeof ( T));

	return ( *this);
}

#endif //__BAARRAY_INL__
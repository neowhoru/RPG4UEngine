/*////////////////////////////////////////////////////////////////////////
文 件 名：StructItem.h
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTITEM_H__
#define __STRUCTITEM_H__
#pragma once

#include "ConstItem.h"

#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sVITEMINFO_BASE
{
	CODETYPE	m_Code;				//id	
	VRSTR		m_sName;				//物品名字	
	CODETYPE	m_DescID;			//物品描述	
	DWORD		m_dwType;			//物品类型	 eVITEMTYPE
	CODETYPE	m_IconID;			//物品在物品栏中得icon文件	
	CODETYPE	m_BigIconID;		//物品在鼠标上的Icon文件
	BYTE		m_byShowMode;		//显示模式 eVITEM_SHOW_TYPE
	CODETYPE	m_ModelID;			//物品掉在地上时的模型文件id	
	BYTE		m_byShowEffect;	//是否显示装备特效	

	/////////////////////////////////////////////////
	//以下为各在物品各自数据
	union
	{
	///武器相关
	struct 
	{
		CODETYPE		m_RHModelID;			///< 右手模型ID
		BYTE			m_byTwoHand;			///< 是否双手武器(0/1) 
		CODETYPE		m_LHModelID;			///< 左手模型ID
	}WEAPON;


	/// 防具相关
	struct 
	{
		CODETYPE		m_ArmourModelID;		///< 模型ID	
		BYTE			m_byArmourTexture;	///< 防具质地	eARMOUR_TEX_TYPE
		BYTE			m_byCape;				///< 是否为袍子	
		BYTE			m_byHeadWear;			///< 是否为头饰	
				;	///< 
	}ARMOUR;


	///宝石类 gem
	struct 
	{
	}GEM;


	///卷轴 reel
	struct 
	{
	}REEL;


	///原料material
	struct 
	{
	}MATERIAL;


	/// 符石ID	
	struct 
	{
	}STONE;
	};////union


	eVITEMTYPE GetIconType();
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sITEMOPTION_BASE
{
	WORD			m_wOptionKind;
	WORD			m_wOption;	
};

struct sITEMTYPE_BASE
{
	WORD  		m_wType;		// eITEM_TYPE
	VRSTR			m_sName;		// 类型名称
	VRSTR			m_sGroup;	// 组名称
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sITEMINFO_MODIFY
{
	char			m_Name[MAX_ITEMNAME2_LENGTH+1];
	BYTE			m_PotentialUsed;	///潜隐激活数量
	BYTE			m_Enchant;
	BYTE			m_Series;
	BYTE			m_Set;
	BYTE			m_DamageLV;
	FLOAT			m_Dura;
	SHORT			m_LimitPTY;
	SHORT			m_LimitDEX;
	SHORT			m_LimitSTR;
	SHORT			m_LimitVIT;
	SHORT			m_LimitINT;
	SHORT			m_LimitLevel;
	INT			m_Speed;
	INT			m_AttackSpeed;
	INT			m_MoveSpeed;
	INT			m_AttackRatio;	///攻击机率
	INT			m_AttSuccessRatio;	///命中
	INT			m_AttBlockRatio;		///闪避
	INT			m_AttackMin;
	INT			m_AttackMax;
	INT			m_PhyAttackMin;
	INT			m_PhyAttackMax;
	INT			m_RangeAttackMin;
	INT			m_RangeAttackMax;
	INT			m_MagicAttackMin;
	INT			m_MagicAttackMax;
	INT			m_Defense;
	INT			m_PhyDef;
	INT			m_RangeDef;
	INT			m_MagicDef;				///魔防 
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sITEMINFO_BASE
{
	SLOTCODE  	m_Code;									// 
	VRSTR  		m_ItemName;  
	CODETYPE  	m_VItemCode;							// 

	WORD  		m_wType;									// eITEM_TYPE
	LEVELTYPE 	m_LV;		                        // 
	DURATYPE 	m_Dura;									// 耐久度
	LEVELTYPE	m_DropLV;								// 掉弃时物品参考等级
	BYTE			m_byIsDrop;								// 

	BYTE			m_byMaterialType;						// eITEM_USE_TYPE

	LEVELTYPE	m_LimitEqLevel;						///< 物品要求等级
	WORD			m_wLimitStr;							///< 要求力量
	WORD			m_wLimitDex;							///< 敏捷要求								
	WORD			m_wLimitInt;							///< 智力要求 
	WORD			m_wLimitVit;							///< 体质要求 
	WORD			m_wLimitSkil1;							///< 技能要求
	WORD			m_wLimitSkil2;

	WORD			m_wMinDamage;
	WORD			m_wMaxDamage;

	WORD			m_wPhyAttMinDamgage;
	WORD			m_wPhyAttMaxDamgage;
	WORD			m_wRangeAttMinDamgage;
	WORD			m_wRangeAttMaxDamgage;
	WORD			m_wPhyAttRate;
	WORD			m_wPhyAttSpeed;
	WORD			m_wPhyDef;
	WORD			m_wPhyAvoid;
	WORD			m_wAttType;
	WORD			m_wAttRange;
	WORD			m_wDefType;									///< 防御类型		eARMOR_TYPE
	BYTE			m_bySeries;									///< 元素攻击类型 eATTACK_KIND
	WORD			m_wMagicAttMinDamgage;
	WORD			m_wMagicAttMaxDamgage;
	WORD			m_wMagicAttSpeed;
	WORD			m_wMagicDef;								///< 魔法防御
	SHORT			m_wElementDef		[ELEMENTKIND_MAX];			///< 元素防御
	SHORT			m_wElementAttack	[ELEMENTKIND_MAX];			///< 元素攻击

	SHORT			m_nMoveSpeed;								///< 移动速度

	WORD			m_wEquipClass;								///< 装备职业类别 参考：GetFirstAvailableClass

	WORD			m_wEqPos;									///< 装备槽位置 eEQUIP_POS_INDEX 
	WORD			m_wMaxRank;									// 
	WORD			m_SocketNum;

	
	sITEMOPTION_BASE	m_arOptions[ITEM_OPTION_MAX];



	BYTE			m_byDupNum;									///< 最大重叠数量

	BYTE			m_byWasteType;								///< 消耗品类型 eITEM_WASTE_TYPE
	WORD			m_wHealHP;									///< HP补给
	DWORD			m_wHealHPTime;								///< HP补给时间
	
	WORD			m_wTimes;									///< 补给次数
	DWORD			m_dCoolTime;								///< 冷却时间



	///////////////////////////////////////////////////////
	eENCHANT_MATERIAL_TYPE GetEnchantMaterialType();

	BOOL		IsSocket()			;
	BOOL		IsMaterialWeapon();
	BOOL		IsMaterialArmour();

	BOOL		IsWeapon()	;
	BOOL		IsArmor()	;

	BOOL		IsAccessary()		;

	BOOL		IsRing()				;

	BOOL		IsNecklace()		;
	BOOL		IsCanEquip()		;
	BOOL		IsCanUseWaste()	;
	BOOL		IsCanCompound()	;
	BOOL		IsPotion()			;

	ePLAYER_TYPE GetFirstAvailableClass()	;
};
typedef const sITEMINFO_BASE		sITEMINFO_BASEC;

#pragma pack(pop)



#endif //__STRUCTITEM_H__
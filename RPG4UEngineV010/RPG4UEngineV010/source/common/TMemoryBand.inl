/*////////////////////////////////////////////////////////////////////////
文 件 名：TMemoryBand.inl
创建日期：	2007.11.4
最后更新：	2007.11.4
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TMEMORYBAND_INL__
#define __TMEMORYBAND_INL__
#pragma once

namespace util
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD IMemoryBand::GetAvailableNumberOfObjects()
{
	return m_FreeIdx;
}

inline DWORD IMemoryBand::GetMaxNumberOfObjects()
{
	return m_BandObjectMax;
}


inline void *	IMemoryBand::GetAt	(DWORD dwIdx )
{
	return &m_pObjectPools[dwIdx*m_dwObjectSize];
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class DataType>
TMemoryBand<DataType>::TMemoryBand()
{
	SetObjectSize(sizeof(DataType));
}

template <class DataType>
TMemoryBand<DataType>::~TMemoryBand()
{
	Destroy();
#ifdef USE_PAUSE//
	m_pPrevBand=NULL;
	m_pNextBand=NULL;
	delete [] (m_ppDatas);
	if(m_bConstructData)
		delete [] (m_pDataPool);
	else
		delete [] ((BYTE*)m_pDataPool);	///避免对DataType析构
#endif

}

template <class DataType>
void TMemoryBand<DataType>::Destroy()
{
	if(m_pObjectPools)
	{
		if(m_bConstructData)
		{
			for(DWORD i = 0 ; i < m_BandObjectMax ; i++)
			{
				DataType* pObject = (DataType*)GetAt(i);
				DestructInPlace(pObject);
			}
		}
		_SUPER::Destroy();
	}
}



template <class DataType>
IMemoryBand * 
TMemoryBand<DataType>::AllocBand	(IMemoryBand *& head
                                 ,DWORD          nMaxObject
											,BOOL           bConstructData)
{
	IMemoryBand* pp;
	
	pp		= new TMemoryBand;

	if(pp == NULL || !pp->OnAllocBand( head, nMaxObject, bConstructData ))
		return NULL;

	if(pp->IsConstructData())
	{
		for(DWORD i = 0 ; i < pp->GetMaxNumberOfObjects() ; i++)
		{
			DataType* pObject = (DataType*)pp->GetAt(i);
			ConstructInPlace(pObject);
		}
	}

#ifdef USE_PAUSE//
	pp->m_ppDatas			= new DataType*[nMaxObject];
	pp->m_bConstructData	= bConstructData;

	memset(pp->m_ppDatas, 0, sizeof(DataType*)*nMaxObject);

	if(bConstructData)
		pp->m_pDataPool = new DataType[nMaxObject];		//允许构造对象
	else
		pp->m_pDataPool = (DataType*)new BYTE[sizeof(DataType) * nMaxObject];	//避免构造对象

	for(DWORD i = 0 ; i < nMaxObject ; i++)
	{
		pp->m_ppDatas[i] = &pp->m_pDataPool[i];
	}

	pp->m_BandObjectMax	= nMaxObject;
	pp->m_FreeIdx			= nMaxObject;
	pp->m_pNextBand				= head;
	if(head)
		head->m_pPrevBand			= pp;
	head = pp;
#endif
	return pp;
}

template <class DataType>
void TMemoryBand<DataType>::FreeBand( IMemoryBand * pDelBand )
{
	//head node one call, all delete;
	IMemoryBand * db = pDelBand;
	while(db)
	{
		IMemoryBand * tmp = db;
		db = db->m_pNextBand;
		delete tmp;
	}
}

#ifdef USE_PAUSE// 
template <class DataType>
DataType * TMemoryBand::GetAt( DWORD dwIdx )
{
	return &m_pDataPool[dwIdx];
}


template <class DataType>
DataType * TMemoryBand::AlloObject()
{
	//	TASSERT(m_FreeIdx-1 >= 0);
	if(--m_FreeIdx == -1)
	{
		m_FreeIdx = 0;
		return NULL;
	}
	else
		return m_ppDatas[m_FreeIdx];
}

template <class DataType>
BOOL TMemoryBand::FreeObject(DataType * data)
{
	//	TASSERT(m_FreeIdx+1 < nMaxNum);
	if(m_FreeIdx  < m_BandObjectMax )
		m_ppDatas[m_FreeIdx++] = data;
	else
		return FALSE;
	return TRUE;
}
#endif

};//namespace util

#endif //__TMEMORYBAND_INL__


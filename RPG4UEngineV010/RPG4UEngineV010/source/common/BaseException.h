/*////////////////////////////////////////////////////////////////////////
文 件 名：BaseException.h
创建日期：2007年4月8日
最后更新：2007年4月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
    一个异常类，传递异常信息


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __BASEEXCEPTION_H__
#define __BASEEXCEPTION_H__
#pragma once

#include "StringEx.h"

namespace Common
{
    class BaseException
    {
    public:
        BaseException();
        virtual ~BaseException();

        BaseException(int nCode, StringEx strMsg);
        BaseException(const BaseException& exception);
        const BaseException& operator = (const BaseException& exception);

        BaseException(int nCode, const TCHAR* pszFormat, ...);

    public:
        void SetMsg(StringEx strMsg);
        void SetCode(int nCode);

        const StringEx& GetMsg() const;
        int GetCode() const;

    protected:
        StringEx m_strMsg;          //异常文字描述信息
        int m_nCode;                //异常编号
    };
};




#endif //__BASEEXCEPTION_H__
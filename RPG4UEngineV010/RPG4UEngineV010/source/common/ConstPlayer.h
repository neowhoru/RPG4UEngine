/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstPlayer.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTPLAYER_H__
#define __CONSTPLAYER_H__
#pragma once



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSEX_TYPE
{
	 SEX_MALE = 0				///> 男性
	,SEX_FEMALE					///> 女性
	,SEX_MAX
};


/*////////////////////////////////////////////////////////////////////////

/*////////////////////////////////////////////////////////////////////////
///职业类型
enum ePLAYER_TYPE
{
	 PLAYERTYPE_BASE				= 1					///< 

	,PLAYERTYPE_WARRIOR			= PLAYERTYPE_BASE	///< 力士
	,PLAYERTYPE_PALADIN			= 2					///< 游侠
	,PLAYERTYPE_POWWOW			= 3					///< 术士(救)
	,PLAYERTYPE_STABBER			= 4					///< 刺客
	,PLAYERTYPE_NECROMANCER		= 5					///< 巫师(咒)

	,PLAYERTYPE_MAX
	,PLAYERTYPE_NUM	= PLAYERTYPE_MAX-PLAYERTYPE_BASE
};


/*////////////////////////////////////////////////////////////////////////
/// 职业类型标志，方便进入职业类型组合
/*////////////////////////////////////////////////////////////////////////
enum ePLAYER_JOB_TYPE
{
	 PLAYERJOB_NONE					= 0
	,PLAYERJOB_WARRIOR				= _BIT(0)
	,PLAYERJOB_PALADIN				= _BIT(1)
	,PLAYERJOB_POWWOW					= _BIT(2)
	,PLAYERJOB_STABBER				= _BIT(3)
	,PLAYERJOB_NECROMANCER			= _BIT(4)
	,PLAYERJOB_ALL						= _MASK(5)
};



/*////////////////////////////////////////////////////////////////////////
	立场关系
/*////////////////////////////////////////////////////////////////////////
enum eRELATION_TYPE
{
	 RELATION_NONE
	,RELATION_ASSISTANT
	,RELATION_ENEMY
	,RELATION_FRIEND
   ,RELATION_VENDOR
};


/*////////////////////////////////////////////////////////////////////////
	攻击系列相生相克关系
/*////////////////////////////////////////////////////////////////////////
enum eSERIES_RELATION_TYPE
{
	 SERIES_RELATION_NORMAL				///< 普通
	,SERIES_RELATION_REGEN				///< 相生
	,SERIES_RELATION_OPPRESS			///< 相克
	,SERIES_RELATION_REGENED			///< 被生
	,SERIES_RELATION_OPPRESSED			///< 被克
	,SERIES_RELATION_MAJOR				///< 主材
	,SERIES_RELATION_MAX		
};



/*////////////////////////////////////////////////////////////////////////
/// 参考 MAX_EQUIPMENT_SLOT_NUM
/// 以下顺序不可变更... 资源表将直接使用定义值
/// 装备位置
/*////////////////////////////////////////////////////////////////////////
enum eEQUIP_POS_INDEX
{
	 EQUIPPOS_WEAPON		= 0	///武器
	,EQUIPPOS_ARMOR		= 1	///盔甲
	,EQUIPPOS_PROTECTOR	= 2	///护肩 位置
	,EQUIPPOS_HELMET		= 3	///头盔
	,EQUIPPOS_PANTS		= 4	///护腿

	,EQUIPPOS_BOOTS		= 5	///靴子
	,EQUIPPOS_GLOVE		= 6	///护手
	,EQUIPPOS_BELT			= 7	///腰带
	,EQUIPPOS_SHIRTS		= 8	///衬衫
	,EQUIPPOS_RING1		= 9	

	,EQUIPPOS_RING2		= 10
	,EQUIPPOS_NECKLACE	= 11
	,EQUIPPOS_HAIR			= 12
	,EQUIPPOS_FACE			= 13
	,EQUIPPOS_HEADWEAR	= 14

	,EQUIPPOS_WEAPON2		= 15
	,EQUIPPOS_SHIELD		= 16
#ifdef MORE_EQUIPPOS
	,EQUIPPOS_BANGLE		= 17
	,EQUIPPOS_CAPE			= 18
	,EQUIPPOS_STONE1		= 19

	,EQUIPPOS_STONE2		= 20
	,EQUIPPOS_STONE3		= 21
#endif

	,EQUIPPOS_MAX
	,EQUIPPOS_INVALID		= 99
};



/*////////////////////////////////////////////////////////////////////////
///< 魔法类型，即元素类型，五行 金水木火土  /  地水火风毒光暗雷
/*////////////////////////////////////////////////////////////////////////
enum eELEMENT_KIND
{
	 ELEMENTKIND_GOLD		= 0		///< 金/雷/电
	,ELEMENTKIND_WATER	= 1		///< 水/冰/雨
	,ELEMENTKIND_WOOD		= 2		///< 木/毒
	,ELEMENTKIND_FIRE		= 3		///< 火/火
	,ELEMENTKIND_EARTH	= 4		///< 土/地
	,ELEMENTKIND_WIND		= 5		///<   /风
	,ELEMENTKIND_LIGHT	= 6		///<   /光/神圣
	,ELEMENTKIND_DARKNESS= 7		///<   /暗   

	,ELEMENTKIND_MAX		= 8		///< 元素数量

	,ELEMENTKIND_NONE		= 8		///< 无元素系别
};							  
							  

/*////////////////////////////////////////////////////////////////////////
攻击系别
/*////////////////////////////////////////////////////////////////////////
enum eATTACK_KIND		  
{							  
	 ATTACKKIND_ALL_OPTION			= 0	// 全部攻击附加运算
							  
	 ///普通攻击
	,ATTACKKIND_MELEE					= 1	// 近身攻击
	,ATTACKKIND_RANGE					= 2	// 远距攻击
	,ATTACKKIND_TRAP					= 3	// 陷阱攻击 +
	,ATTACKKIND_PHY4					= 4	// 物理4保留+
	,ATTACKKIND_PHY5					= 5	// 物理5保留+

	///魔法攻击
	,ATTACKKIND_GOLD					= 6	// 金系攻击 +
	,ATTACKKIND_WATER					= 7	// 水系攻击
	,ATTACKKIND_WOOD					= 8	// 木系攻击 +
	,ATTACKKIND_FIRE					= 9	// 火系攻击
	,ATTACKKIND_EARTH					= 10	// 土系攻击
	,ATTACKKIND_WIND					= 11	// 风系攻击
	,ATTACKKIND_DIVINE				= 12	// 光系攻击
	,ATTACKKIND_DARKNESS				= 13	// 暗系攻击
	
	///攻击选项
	,ATTACKKIND_PHYSICAL_OPTION	= 14	// 物理附加运算（适用所有物理攻击，只是物理附加运算，单一物理不处理）
	,ATTACKKIND_MAGIC_OPTION		= 15	// 魔法附加运算（适用所有魔法攻击，只是魔法附加运算，单一元素不处理）
	,ATTACKKIND_ALL_MAGIC			= 16	// 遍历所有元素运算（即每一元素都做运算）
											  
	,ATTACKKIND_MAX
	,ATTACKKIND_INVALID				

	/////////////////////////////////////
	//攻击分类分段信息
	,ATTACKKIND_PHYBASE			=	ATTACKKIND_MELEE				// 攻击类数量
	,ATTACKKIND_PHYMAX			=	ATTACKKIND_GOLD					// 攻击类数量
	,ATTACKKIND_MAGICBASE		=	ATTACKKIND_GOLD					// 攻击类数量
	,ATTACKKIND_MAGICMAX			=	ATTACKKIND_PHYSICAL_OPTION	// 攻击类数量

	,ATTACKKIND_ATTBASE			=	ATTACKKIND_PHYBASE				// 攻击类数量
	,ATTACKKIND_ATTMAX			=	ATTACKKIND_MAGICMAX			// 攻击类数量



};

// VItem图标
enum ePRIFIX_KIND
{
	 PREFIXKIND_VENDOR				= 50
	,PREFIXKIND_NPC_FUNC				= 60	// 功能NPC
};

/*////////////////////////////////////////////////////////////////////////
近身攻击类型
/*////////////////////////////////////////////////////////////////////////
enum eMELEE_TYPE
{
	 MELEETYPE_SLASH	= 1	//1 砍杀
	,MELEETYPE_PIERCE			//2 刺穿
	,MELEETYPE_HIT				//3 击打
	,MELEETYPE_MAX
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eBASE_ATTRIBTUE
{
	 eBASE_ATTR_STR			= 1	// 力量
	,eBASE_ATTR_DEX					// 敏捷
	,eBASE_ATTR_VIT					// 体质
	,eBASE_ATTR_SPR					// 精神
	,eBASE_ATTR_INT					// 智力
	,eBASE_ATTR_LRN					// 悟力
	,eBASE_ATTR_CRE					// 信用
};


/*////////////////////////////////////////////////////////////////////////
攻击流水号 1->2->3->1
/*////////////////////////////////////////////////////////////////////////
enum eATTACK_SEQUENCE
{
	 ATTACK_SEQUENCE_FIRST
	,ATTACK_SEQUENCE_SECOND
	,ATTACK_SEQUENCE_THIRD
	,ATTACK_SEQUENCE_MAX
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCHAR_CONDITION
{
	 CHAR_CONDITION_STANDUP
	,CHAR_CONDITION_SITDOWN
	,CHAR_CONDITION_MAX
};


/*////////////////////////////////////////////////////////////////////////
物品分配方式
/*////////////////////////////////////////////////////////////////////////
enum eITEM_DISTRIBUTION_TYPE
{
     ITEM_DISTRIBUTION_DEFAULT
    ,ITEM_DISTRIBUTION_RANDOM
    ,ITEM_DISTRIBUTION_FIFO_PRIORITY
    ,ITEM_DISTRIBUTION_MASTER_PRIORITY
    ,ITEM_DISTRIBUTION_CLASS_PRIORITY

    ,ITEM_DISTRIBUTION_MAX
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum ePLAYER_BEHAVE_STATE
{
	 PLAYER_BEHAVE_IDLE		
	,PLAYER_BEHAVE_TRADE_READY
	,PLAYER_BEHAVE_TRADE		
	,PLAYER_BEHAVE_VENDOR_ESTABLISHER
	,PLAYER_BEHAVE_VENDOR_OBSERVER
	,PLAYER_BEHAVE_NPCSHOP
};

enum ePLAYER_TITLE
{
	 PLAYER_NONE_TITLE		= 0
};




#endif //__CONSTPLAYER_H__
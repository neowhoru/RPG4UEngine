/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstNPC.h
创建日期：2007年7月27日
最后更新：2007年7月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTNPC_H__
#define __CONSTNPC_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eNPCVOICEKIND
{
	 eNPCVOICEKIND_NONE	= 0
	,eNPCVOICEKIND_WAIT	= 1
	,eNPCVOICEKIND_MEET	= 2
	,eNPCVOICEKIND_LEAVE	= 3
};


/*////////////////////////////////////////////////////////////////////////
/// NPC移动类型
/*////////////////////////////////////////////////////////////////////////
enum eNPCMOVETYPE
{
	 eNPCMOVETYPE_NONE			= 0	///静止
	,eNPCMOVETYPE_PATROL			= 1	///巡逻
	,eNPCMOVETYPE_RANDOMMOVE	= 2	///随机移动
	,eNPCMOVETYPE_ROUNTE			= 3	///按路线
	,eNPCMOVETYPE_MAX
	
};


/*////////////////////////////////////////////////////////////////////////
//怪物肢足数量
/*////////////////////////////////////////////////////////////////////////
enum eFOOTNUM
{
	 FOOTNUM_SINGLE=1
	,FOOTNUM_TWO	=2
	,FOOTNUM_FOUR	=4
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eNPC_GRADE
{
	 NPC_GRADE_GENERAL			= 1 // 1 : 普通怪物
	,NPC_GRADE_ELITE				= 2 // 2 : 精锐怪物
	,NPC_GRADE_LEADER				= 3 // 3 : 怪物头目
	,NPC_GRADE_SUBBOSS			= 4 // 4 : 中等BOSS
	,NPC_GRADE_BOSS				= 5 // 5 : BOSS
	,NPC_GRADE_MERCHANT			= 6 // 6 : NPC(商人)
	,NPC_GRADE_SUMMON				= 7 // 7 : NPC(宠物)
	,NPC_GRADE_MAPOBJ				= 8 // 8 : NPC(地图物件)
	,NPC_GRADE_BUILDING			= 9 // 9 : 建筑  
	,NPC_GRADE_TREASURE			= 10// 10: 宝箱
	,NPC_GRADE_METERIAL			= 11// 11: 矿种
	,NPC_GRADE_MINE				= 12// 12: 地雷
	,NPC_GRADE_MAX
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eNPC_STATE
{
	NPC_STATE_HELP_RESPONSE
};

/*////////////////////////////////////////////////////////////////////////
NPC功能类型
/*////////////////////////////////////////////////////////////////////////
enum eNPC_FUNC_TYPE
{
	 NPC_FUNC_NONE				= 0 // 普通NPC
	,NPC_FUNC_STORE			= 1 // 药品商人
	,NPC_FUNC_WEAPON			= 2 // 武器商人
	,NPC_FUNC_SHIELD			= 3 // 盾牌商人
	,NPC_FUNC_DRESS			= 4 // 服装商人
	,NPC_FUNC_MATERIAL		= 5 // 材料商人
	,NPC_FUNC_TRANSPORT		= 6 // 传送npc
	,NPC_FUNC_RECORD			= 7 // 记录点npc
	,NPC_FUNC_BANK				= 8 // 仓库npc
	,NPC_FUNC_GUIDEPOST		= 9 // 路牌npc
	,NPC_FUNC_ITEMCOMPOUND	=10 // 物品制造NPC
	,NPC_FUNC_OTHER			=11 // 其它
	,NPC_FUNC_MAKE_ZONE		=12 // 副本NPC
	,NPC_FUNC_GUARD			=13 // 卫士
   ,NPC_FUNC_GUILD			=14 // 工会       
	,NPC_FUNC_ITEMMAKE		=15 // 物品打造
	,NPC_FUNC_ITEMINLAY		=16 // 物品镶嵌
	,NPC_FUNC_ITEMENCHANT	=17 // 物品强化
	,NPC_FUNC_MAX

	,NPC_FUNC_MONSTER			=0xfe
	,NPC_FUNC_SUMMONED		=0xff
};	

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eNPC_SPECIAL_ACTION_TYPE
{
	 NPC_SPECIAL_ACTION_HELPREQUEST		= 1	// 求救
	,NPC_SPECIAL_ACTION_TRANSFORMATION	= 2	// 信息传递
	,NPC_SPECIAL_ACTION_SKILL				= 3	// 使用技能
	,NPC_SPECIAL_ACTION_MAX
};

/*////////////////////////////////////////////////////////////////////////
宠物指令
/*////////////////////////////////////////////////////////////////////////
enum eSUMMON_COMMAND
{
	 SUMMON_COMMAND_FOLLOW
	,SUMMON_COMMAND_ATTACK
	,SUMMON_COMMAND_SKILL_ATTACK
	,SUMMON_COMMAND_DELEGATE_ATTACK
	,SUMMON_COMMAND_DELEGATE_DEFENSE
	,SUMMON_COMMAND_DESTROY
	,SUMMON_COMMAND_MAX
};


#endif //__CONSTNPC_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TMemoryPoolFactory.inl
创建日期：	2007.11.4
最后更新：	2007.11.4
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TMEMORYPOOLFACTORY_INL__
#define __TMEMORYPOOLFACTORY_INL__
#pragma once

namespace util
{ 

inline DWORD IMemoryPoolFactory::GetPoolExtendSize()
{
	return m_dwPoolExtendSize;
}

inline DWORD IMemoryPoolFactory::GetPoolBasicSize()
{
	return m_pBandHead->GetMaxNumberOfObjects();
}


inline DWORD IMemoryPoolFactory::GetNumberOfBands()
{
	return m_pBandHead->GetNumberOfBands();

#ifdef USE_PAUSE
	DWORD dwTotalNum = 0;
	TMemoryBand<Type> *	pHead = m_pBandHead;
	while( pHead )
	{
		++dwTotalNum;
		pHead = pHead->m_pNextBand;
	}
	return dwTotalNum;
#endif
}

inline DWORD IMemoryPoolFactory::GetAvailableNumberOfTypes()
{
	return m_pBandHead->GetAvailableNumberOfTypes();
#ifdef USE_PAUSE
	DWORD dwTotalNum = 0;
	TMemoryBand<Type> * pHead = m_pBandHead;
	while(pHead)
	{
		dwTotalNum += pHead->GetAvailableNumberOfObjects();
		pHead = pHead->m_pNextBand;
	}
	return dwTotalNum;
#endif
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class Type>
TMemoryPoolFactory<Type>::TMemoryPoolFactory()
{
}

template <class Type>
TMemoryPoolFactory<Type>::TMemoryPoolFactory(BOOL bConstructInPlace)
	:	IMemoryPoolFactory( bConstructInPlace )
{
}

template <class Type>
TMemoryPoolFactory<Type>::~TMemoryPoolFactory()
{
	Release();
}

template <class Type>
BOOL TMemoryPoolFactory<Type>::Initialize( DWORD dwPoolBasicSize, DWORD dwPoolExtendSize )
{
	__BOOL_CALL(OnInitBand( dwPoolBasicSize, dwPoolExtendSize ));

	return TRUE;

#ifdef USE_PAUSE
	if( 0 == dwPoolExtendSize )
		m_dwPoolExtendSize = dwPoolBasicSize/2+1;
	else
		m_dwPoolExtendSize = dwPoolExtendSize;

	if( NULL == m_pBandHead )
	{
		TMemoryBand<Type>::AllocBand( m_pBandHead, dwPoolBasicSize, !m_bConstructInPlace );
		m_pFreeBand = m_pBandHead;
		if( !m_pFreeBand ) 
			return FALSE;
		return TRUE;
	}
	return FALSE;
#endif
}

template <class Type>
void TMemoryPoolFactory<Type>::Release()
{
	if( m_pBandHead )
	{
		TMemoryBand<Type>::FreeBand( m_pBandHead );
		//m_pBandHead->FreeBand();
		m_pBandHead = NULL;
		m_pFreeBand = NULL;
	}
}

template <class Type>
Type * TMemoryPoolFactory<Type>::Alloc()
{
	Type * pn;
	pn = (Type *)OnAllocAt(/*m_pFreeBand*/);

#ifdef USE_PAUSE
	Type * pn = m_pFreeBand->AlloObject();
	if(pn == NULL)
	{
		if( !m_pFreeBand->m_pPrevBand )
		{
			TMemoryBand<Type>::AllocBand( m_pBandHead, m_dwPoolExtendSize, !m_bConstructInPlace );
			m_pFreeBand = m_pBandHead;
			pn = (Type*)m_pFreeBand->AlloObject();
		}
		else
		{
			m_pFreeBand = m_pFreeBand->m_pPrevBand;
			pn = (Type*)m_pFreeBand->AlloObject();
		}
	}
#endif
	ASSERT( !IsBadReadPtr( pn, sizeof(Type) ) );
	ASSERT( !IsBadWritePtr( pn, sizeof(Type) ) );

	if(pn && m_bConstructInPlace)
	{
		ConstructInPlace(pn);
	}
	return pn;
}

template <class Type>
void TMemoryPoolFactory<Type>::Free(Type * pNode)
{
	ASSERT( !IsBadReadPtr( pNode, sizeof(Type) ) );
	ASSERT( !IsBadWritePtr( pNode, sizeof(Type) ) );

	if(m_bConstructInPlace)
	{
		DestructInPlace(pNode);
	}
	OnFreeAt	(/*m_pFreeBand,*/ pNode);

#ifdef USE_PAUSE
	if(!m_pFreeBand->FreeObject(pNode))
	{
		if( !m_pFreeBand->m_pNextBand )
		{
			LOGINFO( "TMemoryPoolFactory null NextBand.\n" );
			return;
		}

		m_pFreeBand = m_pFreeBand->m_pNextBand;

		if(!m_pFreeBand->FreeObject(pNode))	
		{
		}
	}
#endif
}

template <class Type>
IMemoryBand* 	TMemoryPoolFactory<Type>::AllocBand(DWORD dwPoolSize)
{
	TMemoryBand<Type>::AllocBand( m_pBandHead, dwPoolSize, !m_bConstructInPlace );
	return m_pBandHead;
}



};//namespace util


#endif //__TMEMORYPOOLFACTORY_INL__
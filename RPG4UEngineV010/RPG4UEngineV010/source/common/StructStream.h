/*////////////////////////////////////////////////////////////////////////
文 件 名：StructStream.h
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTSTREAM_H__
#define __STRUCTSTREAM_H__
#pragma once

#include "ConstData.h"

#pragma pack(push,1)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sITEM_INFO
{
	DWORD		dwSerial;
	SLOTCODE	wCode;
	BYTE		byDura;
	BYTE		bySeries;	///攻击系列 eATTACK_KIND，近远五行
};

struct sVITEM_INFO
{
//	DWORD		dwSerial;		//< 
	SLOTCODE	wCode;
	BYTE		byDura;
	BYTE		bySeries;	///攻击系列 eATTACK_KIND，近远五行
};

/*/////////////////////////////////////
/*/////////////////////////////////////
//struct RANKOPTION
//{
//	WORD	Index	: 5;	///< 为RankOption索引，最大为 32  参考_MAX_ITEM_RANK_OPTION_NUM
//	WORD	Value	: 11;	///< RankValue值，最大为2000  11Bit
//};


/*////////////////////////////////////////////////////////////////////////
	DOUBLE_ATTR Decode格式：	Code	Level变值	攻1变值		攻2变值		防变值
	RANDOM_ATTR Decode格式：	Code	Level变值   主属性变值	随机属性类1	序号	变值	随机属性类2	序号	变值
/*////////////////////////////////////////////////////////////////////////
struct sSOCKET_INFO
{
	///共 7byte
	enum{RANK_NUM = SOCKET_OPTION_MAX};//,DOUBLE_ATTR=0,RANDOM_ATTR=1};

	/// 2byte
	WORD		SocketType	: 1;		///< DOUBLE_ATTR / RANDOM_ATTR
	WORD		OptionIndex	: 7;		///< Option索引，范围[0,125]
	WORD		LimitLevel	: 5;		///< 限制级别，	可变范围 [0,32)

	union
	{
		struct
		{
			// 3byte
			BYTE	AttackAttr	:	7;		///< 攻属性，		可变范围 [0,64)
			BYTE	AttackAttr2	:	7;		///< 攻属性Max，	可变范围 [0,64)
			BYTE	DefenceAttr	:	7;		///< 防属性，		可变范围 [0,64)
		}DOUBLE;
		struct
		{
			///5byte
			BYTE	MainAttr			:	8;		///< 主属性，			可变范围 [0,256)

			WORD	RandomAttrNo	:	7;		///< 随机属性序号1，	可变范围 [0,126)
			WORD	RandomAttr		:	9;		///< 随机属性1变值，	可变范围 [0,512)
#ifdef MORE_RANDATTR
			WORD	RandomAttr2No	:	7;		///< 随机属性序号2，	可变范围 [0,126)
			WORD	RandomAttr2		:	9;		///< 随机属性2变值，	可变范围 [0,512)
#endif
		}RANDOM;
	};
};

#ifdef USE_DISABLE
/*//////////////////////////////////
/*//////////////////////////////////
struct sOPTION_INFO
{
	enum{RANK_NUM = RANK_MAX-1};
	enum{NAME_LEN = MAX_ITEMNAME2_LENGTH};

	////////////////////////////////////////////////
	//共 62 byte = 

	/// 12 byte
	char				szItemName	[NAME_LEN];	///为安全起见，建议通过 strncpy(NAME_LEN)拷贝，没预留结尾符位置

	/// 8 Byete
	UINT64		Enchant			: 4;///< 最高强化级别	12
	UINT64		Rank				: 4;///< 最高Rank数量	9  
	UINT64		RankOption1		: 7;	///< Rank索引1	，范围[0,125]
	UINT64		RankOption2		: 7;	///< 
	UINT64		RankOption3		: 7;	///< 
	UINT64		RankOption4		: 7;	///< 
	UINT64		RankOption5		: 7;	///< 
	UINT64		RankOption6		: 7;	///< 
	UINT64		RankOption7		: 7;	///< 
	UINT64		RankOption8		: 7;	///< 


	UINT64		RankValue1		: 8;	///< 	Rank变值1 ，范围[0,255]
	UINT64		RankValue2		: 8;	///< 
	UINT64		RankValue3		: 8;	///< 
	UINT64		RankValue4		: 8;	///< 
	UINT64		RankValue5		: 8;	///< 
	UINT64		RankValue6		: 8;	///< 
	UINT64		RankValue7		: 8;	///< 
	UINT64		RankValue8		: 8;	///< 

	/// 8byte
	UINT64		RankValue9		: 8;	///< 
	UINT64		Set				: 4;	///< 套装级别Max		15
	UINT64		DamageLV			: 4;	///破损程度
	UINT64		LimitDEX			: 8;	///敏捷要求 修改值[-128,128)
	UINT64		LimitSTR			: 8;	///力量要求 修改值[-128,128)
	UINT64		LimitVIT			: 8;	///体质要求 修改值[-128,128)
	UINT64		LimitINT			: 8;	///智力要求 修改值[-128,128)
	UINT64		LimitLevel		: 6;	///等级		修改值[-32,31)
	UINT64		SocketNum		: 2;  ///装备槽数量Max  3
	UINT64		AttackSpeed		: 8;	///攻速		修改值[-50,206)

	/// 8byte
	UINT64		MoveSpeed		: 8;	///移速		修改值[-50,206)
	UINT64		AttSuccessRatio: 8;	///命中		修改值[-128,128)
	UINT64		AttBlockRatio	: 8;	///闪避		修改值[-128,128)
	UINT64		PhyAttackMin	: 8;	///物理小攻 修改值[-128,128)
	UINT64		PhyAttackMax	: 8;	///物理大攻 修改值[-128,128)
	UINT64		RangeAttackMin	: 8;	///远程小攻 修改值[-128,128)
	UINT64		RangeAttackMax	: 8;	///远程大攻 修改值[-128,128)
	UINT64		MagicAttackMin	: 8;	///魔法小攻 修改值[-128,128)

	//8byte
	UINT64		MagicAttackMax	: 8;	///魔法大攻 修改值[-128,128)
	UINT64		PhyDef			: 8;	///物防		修改值[-128,128)
	UINT64		RangeDef			: 8;	///远防		修改值[-128,128)
	UINT64		MagicDef			: 8;	///魔防		修改值[-128,128)
	UINT64		PotentialUsed	: 2;	///<潜隐激活数量
	UINT64		Potential		: 2;	///< 潜隐级别 [0,1)
	UINT64		RankOption9		: 7;	///<
	UINT64		PotentialOpt1	: 7;	///< Potential索引1	同Rank
	UINT64		PotentialOpt2	: 7;	///< 
	UINT64		PotentialOpt3	: 7;	///< 

	BYTE			PotentialVal1	: 8;	///< Potential变值1 ，同Rank
	BYTE			PotentialVal2	: 8;	///< 
	BYTE			PotentialVal3	: 8;	///< 
	//UINT64		Unused			: 2;	///未用

	/// 21 =  3 * 7 byte
	sSOCKET_INFO	SocketOption[SOCKET_MAX];
};
#endif


/*//////////////////////////////////
/*//////////////////////////////////
struct sOPTION_INFO
{
	enum{RANK_NUM = RANK_MAX-1};
	enum{NAME_LEN = MAX_ITEMNAME2_LENGTH};

	////////////////////////////////////////////////
	//共 62 byte = 

	/// 10 byte
	char			szItemName	[NAME_LEN];	///为安全起见，建议通过 strncpy(NAME_LEN)拷贝，没预留结尾符位置

	/// 8 Byete
	UINT64		Enchant			: 4;///< 最高强化级别	12
	UINT64		Rank				: 4;///< 最高Rank数量	9  
	UINT64		RankOption1		: 7;	///< Rank索引1	，范围[0,125]
	UINT64		RankOption2		: 7;	///< 
	UINT64		RankOption3		: 7;	///< 
	UINT64		RankOption4		: 7;	///< 
	UINT64		RankOption5		: 7;	///< 
	UINT64		RankOption6		: 7;	///< 
	UINT64		RankOption7		: 7;	///< 
	UINT64		RankOption8		: 7;	///< 

	/// 8 Byete
	UINT64		RankValue1		: 8;	///< 	Rank变值1 ，范围[0,255]
	UINT64		RankValue2		: 8;	///< 
	UINT64		RankValue3		: 8;	///< 
	UINT64		RankValue4		: 8;	///< 
	UINT64		RankValue5		: 8;	///< 
	UINT64		RankValue6		: 8;	///< 
	UINT64		RankValue7		: 8;	///< 
	UINT64		RankValue8		: 8;	///< 

	/// 8byte
	UINT64		RankValue9		: 8;	///< 
	UINT64		LimitPTY			: 8;	///属性要求 修改值[-128,128) 影响敏捷力量体质智力
	UINT64		LimitLevel		: 6;	///等级		修改值[-32,31)
	UINT64		SocketNum		: 2;  ///装备槽数量Max  3
	UINT64		Speed				: 8;	///速度		修改值[-50,206)  影响攻速移速
	UINT64		AttackRatio		: 8;	///命中		修改值[-128,128)	影响命中,闪避
	UINT64		AttackMin		: 8;	///小攻		修改值[-128,128)  影响所有攻击
	UINT64		AttackMax		: 8;	///大攻		修改值[-128,128)  影响所有攻击
	UINT64		Defense			: 8;	///防御		修改值[-128,128)  影响所有基本防御

	//8byte
	DWORD			Set				: 4;	///< 套装级别Max		15
	DWORD			DamageLV			: 4;	///破损程度
	DWORD			RankOption9		: 7;	///<
	DWORD			PotentialOpt1	: 7;	///< Potential索引1	同Rank
	DWORD			PotentialVal1	: 8;	///< Potential变值1 ，同Rank
#ifdef MORE_POTENIAL
	DWORD			PotentialUsed	: 2;	///<潜隐激活数量
	DWORD			Potential		: 2;	///< 潜隐级别 [0,1)
	UINT64		PotentialOpt2	: 7;	///< 
	UINT64		PotentialOpt3	: 7;	///< 
	UINT64		PotentialVal2	: 8;	///< 
	UINT64		PotentialVal3	: 8;	///< 
#else
	DWORD			PotentialUsed	: 1;	///<潜隐激活数量
	DWORD			Potential		: 1;	///< 潜隐级别 [0,1)
#endif

	/// 21 =  3 * 7 byte
	sSOCKET_INFO	SocketOption[SOCKET_MAX];
};


/*////////////////////////////////////////////////////////////////////////
// Buy, Pick, Obtain,其它玩家装备信息...将使用的数据结构
/*////////////////////////////////////////////////////////////////////////
union ITEM_STREAM
{
	sITEM_INFO		ItemPart;
	BYTE				pItemStream[sizeof(sITEM_INFO)];
};


union ITEMOPT_STREAM
{
	struct
	{
		sITEM_INFO		ItemPart;
		sOPTION_INFO	OptionPart;
	};
	BYTE				pItemStream[sizeof(sITEM_INFO)+sizeof(sOPTION_INFO)];
};


union VITEM_STREAM
{
	sVITEM_INFO		RenderItemPart;
	BYTE				pItemStream[sizeof(sVITEM_INFO)];
} ;


union VITEMOPT_STREAM
{
	struct
	{
		sVITEM_INFO			RenderItemPart;
		sOPTION_INFO		OptionPart;
	};
	BYTE						pItemStream[sizeof(sVITEM_INFO)+sizeof(sOPTION_INFO)];
};



/*////////////////////////////////////////////////////////////////////////
	技能信息
/*////////////////////////////////////////////////////////////////////////
struct sSKILL_INFO
{
	WORD		wCode;
};
//-------------------------------------------------------------------------
union SKILL_STREAM
{
	sSKILL_INFO	SkillPart;
	BYTE			pSkillStream[sizeof(sSKILL_INFO)];
};



/*////////////////////////////////////////////////////////////////////////
Quick
/*////////////////////////////////////////////////////////////////////////
struct sQUICK_INFO
{
	SLOTINDEX		SlotIdx;
	SLOTCODE		Code;
	BYTE			Pos;
};

//-------------------------------------------------------------------------
union QUICK_STREAM
{
	sQUICK_INFO	QuickPart;
	BYTE			pQuickStream[sizeof(sQUICK_INFO)];
};


/*////////////////////////////////////////////////////////////////////////
Style
/*////////////////////////////////////////////////////////////////////////
struct sSTYLE_INFO
{
	SLOTCODE		Code;
};

//-------------------------------------------------------------------------
union STYLE_STREAM
{
	sSTYLE_INFO	StylePart;
	BYTE			pStyleStream[sizeof(sSTYLE_INFO)];
}  ;

/*////////////////////////////////////////////////////////////////////////
Quest
/*////////////////////////////////////////////////////////////////////////
struct sQUESTSTREAM_INFO
{
	BYTE		byState;
};

//-------------------------------------------------------------------------
union QUEST_STREAM
{
	sQUESTSTREAM_INFO	QuestPart;
	BYTE					pQuestStream[sizeof(sQUESTSTREAM_INFO)];
}  ;


#pragma pack(pop)


/*////////////////////////////////////////////////////////////////////////
DB Field Size
/*////////////////////////////////////////////////////////////////////////
const DWORD 	  MAX_STYLESTREAM_SIZE				=  8 ;		// 4*2
const DWORD 	  MAX_QUICKSTREAM_SIZE				=  192 ;		// DB Field Size
const DWORD 	  MAX_SKILLSTREAM_SIZE				=  200 ;		// DB Field Size
const DWORD 	  MAX_MISSIONSTREAM_SIZE			=  128 ;		// DB = 4*32
const DWORD 	  MAX_QUESTSTREAM_SIZE				=  MAX_QUEST_SIZE * sizeof(BYTE);		// DB 


const DWORD		SIZEOF_ITEMOPT_STREAM				= sizeof(ITEMOPT_STREAM);
const DWORD 	MAX_INVENTORYITEMSTREAM_SIZE		= SIZEOF_ITEMOPT_STREAM * MAX_INVENTORY_SLOT_NUM ;	
const DWORD 	MAX_INVENTORYITEMSTREAM_PAGESIZE	= SIZEOF_ITEMOPT_STREAM * MAX_INVENTORY_SLOT_NUM_PERPAGE ;	
const DWORD 	MAX_TEMPINVENTORYITEMSTREAM_SIZE	= SIZEOF_ITEMOPT_STREAM * MAX_TEMPINVENTORY_SLOT_NUM;
const DWORD 	MAX_EQUIPITEMSTREAM_SIZE			= SIZEOF_ITEMOPT_STREAM * MAX_EQUIPMENT_SLOT_NUM ;
const DWORD 	MAX_WAREHOUSESTREAM_SIZE			= SIZEOF_ITEMOPT_STREAM * MAX_WAREHOUSE_SLOT_NUM ;
const DWORD 	MAX_WAREHOUSESTREAM_PAGESIZE		= SIZEOF_ITEMOPT_STREAM * MAX_WAREHOUSE_SLOT_NUM_PERPAGE ;



#endif //__STRUCTSTREAM_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：BABinTree.inl
创建日期：2008年1月27日
最后更新：2008年1月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BABINTREE_INL__
#define __BABINTREE_INL__
#pragma once


template <class T>
BABinTree<T>::BABinTree()
{
	m_pLeft		= 
	m_pRight		= 
	m_pParent	= NULL;
}

template <class T>
BABinTree<T>::~BABinTree()
{
}

template <class T>
inline void BABinTree<T>::SetLeft( BABinTree<T> *pLeft)	
{
	m_pLeft = pLeft;
	pLeft->m_pParent = this;
}

template <class T>
inline void BABinTree<T>::SetRight( BABinTree<T> *pRight)	
{
	m_pRight = pRight;
	pRight->m_pParent = this;
}

template <class T>
inline BABinTree<T>* BABinTree<T>::GetLeft( void)	
{
	return ( m_pLeft);
}

template <class T>
inline BABinTree<T>* BABinTree<T>::GetRight( void)	
{
	return ( m_pRight); 
}

template <class T>
inline BABinTree<T>* BABinTree<T>::GetParent( void)
{
	return ( m_pParent);
}

template <class T>
inline void	BABinTree<T>::SetData( T Data)	
{
	m_Data = Data; 
}

template <class T>
inline T		BABinTree<T>::GetData( void)	
{
	return ( m_Data);
}



template <class T>
BABinTree<T>* BABinTree<T>::AddLeft( void)
{
	assert( !m_pLeft);
	BABinTree<T> *pNew = new BABinTree<T>;
	SetLeft( pNew);
	return ( pNew);
}

template <class T>
BABinTree<T>* BABinTree<T>::AddRight( void)
{
	assert( !m_pRight);
	BABinTree<T> *pNew = new BABinTree<T>;
	SetRight( pNew);
	return ( pNew);
}


template <class T>
void BABinTree<T>::Destroy( void)
{
	if ( m_pLeft)
	{
		m_pLeft->Destroy();
		delete m_pLeft;
	}

	if ( m_pRight)
	{
		m_pRight->Destroy();
		delete m_pRight;
	}
}



#endif //__BABINTREE_INL__
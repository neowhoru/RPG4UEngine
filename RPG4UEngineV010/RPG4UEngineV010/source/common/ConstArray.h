/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstArray.h
创建日期：2008年5月20日
最后更新：2008年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTARRAY_H__
#define __CONSTARRAY_H__
#pragma once

#include "ConstDefine.h"

#define MAX_CONST_CATEGORY		10	///最大种类数量
enum eTYPE_CATEGORY
{
	 eTYPELABEL_CharType			= 1
	,eTYPELABEL_SexType			= 2
	,eTYPELABEL_VItemType		= 3
	,eTYPELABEL_EquipName		= 4
	,eTYPELABEL_EquipPos			= 5
	,eTYPELABEL_SceneObjType	= 6
	,eTYPELABEL_V3DExt			= 7
	,eTYPELABEL_V3DFileTitle	= 8
	,eTYPELABEL_PathInfo			= 9
	,eTYPELABEL_TipType			= 10
};


/*////////////////////////////////////////////////////////////////////////
	获取名字	LPCSTR	Get##Name
	取值		Type		Get##Name##Value
	Category	主要配合 PathInfoParser做类别Label解释
/*////////////////////////////////////////////////////////////////////////

#define CONSTARRAY_DECLARE2(Name,Type,TypeBase,TypeMax,Category, API)\
	class API ConstArray##Name\
	{\
		public:\
		enum{CATEGORY_INDEX =  Category};\
		static LPCSTR ms_sz##Name##s[TypeMax];\
		static inline LPCSTR* GetLabels()\
		{\
			return ms_sz##Name##s;\
	   }\
		static inline LPCSTR GetLabel(Type nIndex)\
		{\
			__CHECK2_RANGE2	(nIndex\
									,TypeBase\
									,TypeMax\
									,NULL);\
			return ms_sz##Name##s[nIndex];\
		}\
		static inline Type GetValue(LPCSTR szType)\
		{\
			for(INT n=TypeBase; n<TypeMax; n++)\
			{\
				if(lstrcmpi(ms_sz##Name##s[n],szType) == 0) return (Type)n;\
			}\
			return TypeMax;\
		}\
	};\
	inline LPCSTR* Get##Name##Labels(){return ConstArray##Name::GetLabels();}\
	inline LPCSTR Get##Name##Label(Type nIndex){return ConstArray##Name::GetLabel(nIndex);}\
	inline Type Get##Name##Value(LPCSTR szType){return ConstArray##Name::GetValue(szType);}



#define CONSTARRAY_DECLARE(Name,Type,TypeBase,TypeMax, API)\
								CONSTARRAY_DECLARE2(Name,Type,TypeBase,TypeMax,0, API)

#define CONSTARRAY_IMPL(Name,TypeMax)\
								LPCSTR ConstArray##Name::ms_sz##Name##s[TypeMax]=





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

#define CONSTARRAY_DECLARE_EX2(Name,Type,TypeNull,TypeMax,Category, API)\
	CONSTARRAY_DECLARE2(Name,Type,0,TypeMax,Category, API)\
	class API ConstArray##Name##Ex : public ConstArray##Name\
	{\
		public:\
		static Type  ms_dw##Name##s[TypeMax];\
		static inline Type* GetValues()\
		{\
			return ms_dw##Name##s;\
	   }\
		static inline Type GetValueAt(INT nIndex)\
		{\
			__CHECK2_RANGE	(nIndex\
								,TypeMax\
								,TypeNull);\
			return ms_dw##Name##s[nIndex];\
		}\
		static inline INT GetValueIndex(Type dat)\
		{\
			for(INT n=0; n<TypeMax; n++)\
			{\
				if(ms_dw##Name##s[n] == dat) return n;\
			}\
			return -1;\
		}\
		static inline LPCTSTR GetLabelByValue(Type dat)\
		{\
			INT n=GetValueIndex(dat);\
			if(n == -1)	return NULL;\
			return ms_sz##Name##s[n];\
		}\
	};\
	inline LPCSTR Get##Name##LabelByValue(Type dat){return ConstArray##Name##Ex::GetLabelByValue(dat);}\
	inline Type* Get##Name##Values(){return ConstArray##Name##Ex::GetValues();}\
	inline Type  Get##Name##ValueAt(INT nIndex){return ConstArray##Name##Ex::GetValueAt(nIndex);}\
	inline INT   Get##Name##ValueIndex(Type dat){return ConstArray##Name##Ex::GetValueIndex(dat);}


#define CONSTARRAY_DECLARE_EX(Name,Type,TypeNull,TypeMax, API)\
								CONSTARRAY_DECLARE_EX2(Name,Type,TypeNull,TypeMax,0, API)

#define CONSTARRAY_IMPL_EX(Name,Type,TypeMax)\
					Type ConstArray##Name##Ex::ms_dw##Name##s[TypeMax]=






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CONSTARRAY_DECLARE2	(CharType
                     ,ePLAYER_TYPE
							,PLAYERTYPE_BASE
							,PLAYERTYPE_MAX
							,eTYPELABEL_CharType
							,_BASE_API);


CONSTARRAY_DECLARE2	(SexType
                     ,eSEX_TYPE
							,SEX_MALE
							,SEX_MAX
							,eTYPELABEL_SexType
							,_BASE_API);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CONSTARRAY_DECLARE2	(VItemType
                     ,eVITEMTYPE
							,VITEMTYPE_NONE
							,VITEMTYPE_MAX
							,eTYPELABEL_VItemType
							,_BASE_API);




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CONSTARRAY_DECLARE2	(TipType
                     ,DWORD
							,0
							,eTIP_MAX
							,eTYPELABEL_TipType
							,_BASE_API);


#endif //__CONSTARRAY_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：THandleManager.inl
创建日期：2008年8月18日
最后更新：2008年8月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __THANDLEMANAGER_INL__
#define __THANDLEMANAGER_INL__
#pragma once


template <class T>
THandleManager<T>::THandleManager()
{
	m_nCountFreeIndex = 0;
	m_nMaxIndex = (WORD)-1;
	m_nNewMagicNumber = 1;
	m_arData.EnlargeSize( 65535);
	m_arMagicNumber.EnlargeSize( 65535);
}

template <class T>
THandleManager<T>::~THandleManager()
{
}

template <class T>
void THandleManager<T>::Clear( void)
{
	m_arData.Clear();
	//m_lstFreeIndex.RemoveAll();
	m_nCountFreeIndex = 0;
	m_arMagicNumber.Clear();
}


template <class T>
inline int	THandleManager<T>::Idx_GetCount( void)	
{
	return ( m_arData.GetCount());
}

template <class T>
inline T		THandleManager<T>::Idx_GetData( int iIndex)	
{
	return ( m_arData.Get( iIndex));
}
template <class T>
inline WORD THandleManager<T>::Idx_GetMagicNumber( int iIndex)	
{
	return ( m_arMagicNumber.Get( iIndex)); 
}

template <class T>
inline void	THandleManager<T>::Idx_Release( int iIndex)	
{
	St_Handle Handle; 
	Handle.m_nIndex = iIndex;
	Handle.m_nMagicNumber = Idx_GetMagicNumber( iIndex); Release( ( HANDLE)Handle.m_dwHandle); 
}


template <class T>
HANDLE THandleManager<T>::Add( T Data)
{
	St_Handle Handle={0};

	//if ( m_lstFreeIndex.GetCount() > 0)
	if ( m_nCountFreeIndex > 0)
	{	
		WORD i;
		//Handle.m_nIndex = m_lstFreeIndex.RemoveHead();
		for ( i = 0; i <= m_nMaxIndex; ++i)
		{
			if ( 0 == m_arMagicNumber.Get( i))
			{
				Handle.m_nIndex = i;
				--m_nCountFreeIndex;
				break;
			}
		}
		assert( i <= m_nMaxIndex || !"THandleManager<T>::Add - Index range exceed");
	}
	else
	{
		if ( m_nMaxIndex == 65534)
		{
			return ( INVALID_HANDLE_VALUE);
		}

		Handle.m_nIndex = ++m_nMaxIndex;
	}

	if ( m_nNewMagicNumber >= 65535)
	{
		m_nNewMagicNumber = 1;
	}
	Handle.m_nMagicNumber = m_nNewMagicNumber++;

	/////////////////////
	m_arData.Set( Handle.m_nIndex, Data);
	m_arMagicNumber.Set( Handle.m_nIndex, Handle.m_nMagicNumber);

	return ( ( HANDLE)Handle.m_dwHandle);
}

template <class T>
BOOL THandleManager<T>::Release( HANDLE hHandle)
{
	if ( !CheckHandleValidity( hHandle))
	{
		return ( FALSE);
	}

	St_Handle Handle;
	Handle.m_dwHandle = ( DWORD)hHandle;
	m_arMagicNumber.Set( Handle.m_nIndex, 0);
	T NullData;
	memset( &NullData, 0, sizeof( T));
	m_arData.Set( Handle.m_nIndex, NullData);

	//m_lstFreeIndex.AddTail( Handle.m_nIndex);
	++m_nCountFreeIndex;

	//if ( m_arData.GetCount() == m_lstFreeIndex.GetCount())
	if ( m_arData.GetCount() == m_nCountFreeIndex)
	{
		Clear();
	}

	return ( TRUE);
}

template <class T>
T THandleManager<T>::GetData( HANDLE hHandle)
{
	if ( !CheckHandleValidity( hHandle))
	{
		T NullData;	
		memset( &NullData, 0, sizeof( T));

		return ( NullData);
	}

	St_Handle Handle;
	Handle.m_dwHandle = ( DWORD)hHandle;
	return ( m_arData.Get( Handle.m_nIndex));
}

template <class T>
BOOL THandleManager<T>::CheckHandleValidity( HANDLE hHandle)
{
	St_Handle Handle={0};
	Handle.m_dwHandle = ( DWORD)hHandle;
	if ( hHandle == INVALID_HANDLE_VALUE || 0 == Handle.m_nMagicNumber)
	{
		return ( FALSE);
	}

	WORD nMagicNumber = m_arMagicNumber.Get( Handle.m_nIndex);
	if ( nMagicNumber != Handle.m_nMagicNumber)
	{	
		return ( FALSE);
	}

	return ( TRUE);
}



#endif //__THANDLEMANAGER_INL__
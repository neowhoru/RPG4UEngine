/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstItemComposite.h
创建日期：2008年6月18日
最后更新：2008年6月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTITEMCOMPOSITE_H__
#define __CONSTITEMCOMPOSITE_H__
#pragma once


const SLOTPOS	ITEMCOMPOUND_MATERAIL_MAIN	= INVALID_POSTYPE;//0;
const SLOTPOS	ITEMCOMPOUND_MATERAIL_BASE	= 0;//1;
const DWORD		MAX_ENCHANT_MATERIAL_NUM	= 5;
const DWORD		MATERIAL_CONTAINER_COL		= 6;
const DWORD		MATERIAL_CONTAINER_ROW		= 5;
const DWORD		MATERIAL_ROW_SHIFT			= 3;	///MATERIAL_CONTAINER_ROW最大位数
const DWORD		MATERIAL_ROW_MASK				= 0x7;///MATERIAL_CONTAINER_ROW最大位数

enum
{
	 eITEMENCHANT_MAIN_MATERIAL		= 0
	,eITEMENCHANT_MAX_SLOT				= 6*5	///原料容器大小  基本原料2*6  打造原料5*6
	,eITEMENCHANT_TARGET_MAX_SLOT		= 1	///目标槽大小
	,eITEMENCHANT_SUBRESULT_MAX_SLOT	= 6	///副物品容器大小
	,eITEMENCHANT_SOCKET_MAX_SLOT		= 5	///插槽容器大小
	,eITEMLOG_SOCKET_MAX_SLOT			= 5	///物品日志容器大小

	,eCOMPOSITE_MATERIAL_MAX			= 5	///原料最大数量
	,eCOMPOSITE_SUBRESULT_MAX			= 6	///产生副物品最大数量
	,eCOMPOSITE_RESULT_MAX				= 1	///产生正物品最大数量
	,eITEMENCHANT_SUBRESULT_SOCKET	= 1	///Socket物品拆除中，Socket物品在SubResult位置
	,eITEMENCHANT_SUBRESULT_TARGET	= 0	///Socket物品拆除中，目标物品在SubResult位置
};


enum eITEMENCHANT_TYPE
{
	 eITEMENCHANT_COMPOUND
	,eITEMENCHANT_INLAY
	,eITEMENCHANT_MAKE
	,eITEMENCHANT_ENCHANT
};

enum eSUCCEEDRATE
{
	 eSUCCEEDRATE_25
	,eSUCCEEDRATE_50
	,eSUCCEEDRATE_75
	,eSUCCEEDRATE_100
	,eSUCCEEDRATE_NUM
};



#endif //__CONSTITEMCOMPOSITE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ResultCode.h
创建日期：2008年9月27日
最后更新：2008年9月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __RESULTCODE_H__
#define __RESULTCODE_H__
#pragma once

#define _RC(x)					RC_##x
#define _RC_CHR(x)			_RC( CHARINFO_##x)
#define _RC_SEND(x)			_RC( SEND_##x)
#define _RC_SEC(x)			_RC( SECTOR_##x)
#define _RC_ITEM(x)			_RC( ITEM_##x)
#define _RC_WH(x)				_RC( WAREHOUSE_##x)
#define _RC_TRADE(x)			_RC( TRADE_##x)
#define _RC_VEN(x)			_RC( VENDOR_##x)
#define _RC_SKILL(x)			_RC( SKILL_##x)
#define _RC_STY(x)			_RC( STYLE_##x)
#define _RC_RM(x)				_RC( ROOM_##x)
#define _RC_LOUT(x)			_RC( LOGOUT_##x)
#define _RC_ZON(x)			_RC( ZONE_##x)
#define _RC_CON(x)			_RC( CONNECTION_##x)
#define _RC_PTY(x)			_RC( PARTY_##x)
#define _RC_GM(x)				_RC( GM_##x)
#define _RC_WAR(x)			_RC( BATTLE_##x)
#define _RC_SUM(x)			_RC( SUMMON_##x)
#define _RC_GID(x)			_RC( GUILD_##x)
#define _RC_EVT(x)			_RC( EVENT_##x)
#define _RC_SYN(x)			_RC( SYNC_##x)
#define _RC_PVP(x)			_RC( PVP_##x)
#define _RC_COD(x)			_RC( CONDITION_##x)
#define _RC_QUE(x)			_RC( QUEST_##x)

///////////////////////////////////////////////
namespace RC
{
	///////////////////////////////////////////////
	enum eCHARINFO_RESULT
	{
		 _RC_CHR(FAILED)			= -1
		,_RC_CHR(OK)				= 0
		,_RC_CHR(EXISTED)			= 1
		,_RC_CHR(FULL)				= 2
		,_RC(INVALID_SLOT)		= 54
		,_RC(DATABASE_FAILED)	= 55
		,_RC_CHR(STATEINVALID)	= 56
		,_RC(INVALID_SECURITY)	= 57

	};

	///////////////////////////////////////////////
	enum eSEND_RESULT
	{
		 _RC_SEND(SUCCESS)
		,_RC(SEND_FAILED)
		
	};

	///////////////////////////////////////////////
	enum eSECTOR_REULT
	{
		 _RC_SEC(ADD_SUCCESS)
		,_RC_SEC(ADD_FAILED)
		,_RC_SEC(REMOVE_SUCCESS)
		,_RC_SEC(REMOVE_FAILED)
		
	};

	///////////////////////////////////////////////
	enum eITEM_RESULT
	{
		 _RC_ITEM(FAILED)
		,_RC_ITEM(SUCCESS)
		,_RC_ITEM(INVALIDSTATE)
		,_RC_ITEM(INVALIDPOS)
		,_RC_ITEM(EMPTYSLOT)					
		,_RC_ITEM(NOTEXISTITEMATFIELD)	
		,_RC_ITEM(NOINFO)						
		,_RC_ITEM(NOSPACEININVENTORY)		
		,_RC_ITEM(INVALIDSTATEOFPLAYER)	
		,_RC_ITEM(INVALIDPOSFORDROPITEM)	
		,_RC_ITEM(DBP_ASYNC)					
		,_RC_ITEM(UNKNOWNERROR)				
		,_RC_ITEM(COOLTIME_ERROR)			

		,_RC_ITEM(ITEMCODENOTEQUAL)	

		,_RC_ITEM(ISNOTWASTEITEM)	

		,_RC_ITEM(INVALIDSHOPLISTID)
		,_RC_ITEM(OUTOFSHOPITEMINDEX)	
		,_RC_ITEM(HAVENOTMONEY)			

		
		,_RC_ITEM(ISNOTEMPTYSLOT)	
		,_RC_ITEM(HAVENOTSPACE)		
		,_RC_ITEM(INVALIDVALUE)		

		
		
		,_RC_ITEM(ERRORDROPMONEY)

		
		,_RC_ITEM(ENCHANT_SUCCESS)
		
		,_RC_ITEM(ENCHANT_FAILED)	
		,_RC_ITEM(ENCHANT_DOWN_FAILED)
		,_RC_ITEM(ENCHANT_CRACK_FAILED)
		,_RC_ITEM(ENCHANT_INVALID_ITEMLEVEL)	
		,_RC_ITEM(ENCHANT_INVALID_RATE_INDEX)	
		,_RC_ITEM(ENCHANT_HAVENOTMATERIALS)

		
		,_RC_ITEM(INVALID_CONDITION)	
		,_RC_ITEM(CANNT_RANKUP_ITEM)
		,_RC_ITEM(NO_MORE_RANK)
		,_RC_ITEM(INSUFFICIENT_MONEY)	
		
		,_RC_ITEM(FULLSOCKET)			

		
		,_RC_ITEM(EXTRACTSOCKET_SUCCESS)
		,_RC_ITEM(EXTRACTSOCKET_FAILED)

		
		,_RC_ITEM(MAKE_SUCCESS)
		
		,_RC_ITEM(MAKE_FAILED)	
		,_RC_ITEM(MAKE_INVALID_ITEMLEVEL)			
		,_RC_ITEM(MAKE_NOINFO)							
		,_RC_ITEM(MAKE_INVALID_ROWNUM)				
		,_RC_ITEM(MAKE_INVALID_MATERIALSLOTINFO)	


		,_RC_ITEM(UNUSABLE_FUNCTION)		
		,_RC_ITEM(CANNT_REPAIR_DURA)
		,_RC_ITEM(CANNT_REPAIR_AT_CONTAINER)
		,_RC_ITEM(PLAYER_HP_FULL)
		,_RC_ITEM(PLAYER_MP_FULL)
		,_RC_ITEM(PLAYER_HPMP_FULL)
	};


	///////////////////////////////////////////////
	enum eWAREHOUSE_RESULT
	{
		 _RC_WH(NOT_AVAILABLE_SERVICE)		
		,_RC_WH(INSUFFICIENT_MONEY)			
		,_RC_WH(INVALID_STATE)					
	};

	///////////////////////////////////////////////
	enum eTRADE_RESULT
	{
		 _RC_TRADE(SUCCESS)
		,_RC_TRADE(FAILED)
		,_RC_TRADE(CANCEL)
		,_RC_TRADE(PLAYER1_HAVENOTSPACE)
		,_RC_TRADE(PLAYER1_HAVENOTMONEY)
		,_RC_TRADE(PLAYER2_HAVENOTSPACE)
		,_RC_TRADE(PLAYER2_HAVENOTMONEY)
		,_RC_TRADE(COUNTERPARTNER_NOTACCEPT)
		,_RC_TRADE(COUNTERPARTNER_NOTPROPOSAL)
		,_RC_TRADE(INVALID_STATE)
		,_RC_TRADE(PLAYER1_NOT_EXIST)
		,_RC_TRADE(PLAYER1_NOT_EXIST_ATFIELD)
		,_RC_TRADE(PLAYER2_NOT_EXIST)
		,_RC_TRADE(PLAYER2_NOT_EXIST_ATFIELD)
		,_RC_TRADE(PLAYER1_NOT_IDLE)
		,_RC_TRADE(PLAYER2_NOT_IDLE)
		,_RC_TRADE(CREATE_FAIELD)
		,_RC_TRADE(PUT_FAIELD)
		,_RC_TRADE(PUT_MONEY_FAIELD)
		,_RC_TRADE(GET_FAIELD)
		,_RC_TRADE(GET_MONEY_FAIELD)
		,_RC_TRADE(MODIFY_FAIELD)
		,_RC_TRADE(PROPOSAL_FAIELD)
		,_RC_TRADE(TOOFAR)
	};

	///////////////////////////////////////////////
	enum eVENDOR_RESULT
	{
		 _RC_VEN(SUCCESS				)
		,_RC_VEN(FAILED				)
		,_RC_VEN(INVALID_STATE		)
		,_RC_VEN(INVALID_VENDORSLOT)
		,_RC_VEN(NOSPACEININVENTORY)
		,_RC_VEN(INSUFFICIENT_MONEY)
		,_RC_VEN(JUST_SHOW	)
	};
	
	///////////////////////////////////////////////
	enum eSKILL_RESULT
	{
		 _RC_SKILL(SUCCESS)
		,_RC_SKILL(FAILED)									

		,_RC_SKILL(BASEINFO_NOTEXIST)						

		,_RC_SKILL(STATE_WHERE_CANNOT_ATTACK_ENEMY)	
		,_RC_SKILL(COOLTIME_ERROR)							
		,_RC_SKILL(HPMP_RUN_SHORT)							
		,_RC_SKILL(CHAR_CLASS_LIMIT)					
		,_RC_SKILL(WEAPON_LIMIT)						
		,_RC_SKILL(SEALING_STATE)						
		,_RC_SKILL(OUT_OF_RANGE)						

		,_RC_SKILL(REQUIRE_LEVEL_LIMIT)				
		,_RC_SKILL(REQUIRE_SKILLSTAT_LIMIT)			
		,_RC_SKILL(DOES_NOT_HAVE)						
		,_RC_SKILL(REMAIN_SKILLPOINT_LACK)			
		,_RC_SKILL(MAX_LEVEL_LIMIT)					
		,_RC_SKILL(ALREADY_EXIST_SKILL)				

		,_RC_SKILL(INVALID_STATE)						
		,_RC_SKILL(NOTEXIST)							
		,_RC_SKILL(INVLIDPOS)							

		,_RC_SKILL(FIGHTING_ENERGY_FULL)				

		,_RC_SKILL(POSITION_INVALID)					

		,_RC_SKILL(SUMMONED_NOTEXIST)					
		,_RC_SKILL(TARGET_NOTEXIST)					
		,_RC_SKILL(DEPENDENCE_NOTFOUND)					
	};

	///////////////////////////////////////////////
	enum eQUICK_RESULT
	{
		 _RC(QUICK_SUCCESS)
		,_RC(QUICK_FAILED)
		,_RC(QUICK_INVALIDSTATE)
		,_RC(QUICK_INVALIDPOS)
		,_RC(QUICK_INVALIDSLOTIDX)
		,_RC(QUICK_UNKNOWNERROR)
	};

	///////////////////////////////////////////////
	enum eSTYLE_RESULT
	{
		 _RC_STY(SUCCESS)
		,_RC_STY(FAILED)
		,_RC_STY(INVALIDSTATE)
		,_RC_STY(INVALIDPOS)
		,_RC_STY(INVALIDSLOTIDX)
		,_RC_STY(UNKNOWNERROR)
	};

	///////////////////////////////////////////////
	enum eROOM_RESULT
	{
		 _RC_RM(SUCCESS)
		,_RC_RM(FAILED)
		,_RC_RM(NOTEXISTLOBBY)		
		,_RC_RM(NOTEXISTROOM)
		,_RC_RM(ISNOTMATERPLAYER)				
		,_RC_RM(BEFOREENTERZONE)				
		,_RC_RM(INVALIDROOMTYPE)				
		,_RC_RM(INVALIDPREVLOBBY)				

		,_RC_RM(INVALID_ROOM_TYPE)				
		,_RC_RM(ALREADY_EXIST_PLAYER)			
		,_RC_RM(ALREADY_DOING_TRANSACTION)		
		,_RC_RM(CANNOT_JOIN_ROOMTYPE)			
		,_RC_RM(NOTEQUAL_PASSWORD)				
		,_RC_RM(FULL_ROOM)						
		,_RC_RM(INVALID_MAPCODE)				
		,_RC_RM(INVALID_LIMITLEVEL)				
		,_RC_RM(INVALID_LIMITCLASS)				
		,_RC_RM(INVALID_LIMITCLASS_FOR_ME)		
		,_RC_RM(INVALID_LIMITLEVEL_FOR_ME)		
		,_RC_RM(INVALID_LIMIT_NUMBEROFPLAYER)	
		,_RC_RM(INVALID_BONUS_VALUE)			
		,_RC_RM(INVALID_FIELD_VALUE)			
		,_RC_RM(INVALID_DIFFICULTY_VALUE)		
		,_RC_RM(INVALID_LASTROOMTYPE)			
		,_RC_RM(NOTEXISTVILLAGE)				
		,_RC_RM(ALREADY_READY_STATE)			
		,_RC_RM(ALREADY_NOT_READY_STATE)		
		,_RC_RM(IS_NOT_ALL_READY)				
		,_RC_RM(INSUFFICIENT_USER_NUM)			
		,_RC_RM(NOT_CMD_FOR_MASTER)				
		,_RC_RM(NOT_CMD_FOR_MEMBER)				
		,_RC_RM(NOT_EXIST_MEMBER)				
		,_RC_RM(NOT_EXIST_ROOM)					
		,_RC_RM(CANNOT_CREATE_ROOM_FOR_LIMIT)	
		,_RC_RM(INVALID_PLAYER_STATE)			
		,_RC_RM(NEED_CHAR_LEVEL_10_FOR_PVP)		
		,_RC_RM(NEED_EVEN_NUMBER)				
		,_RC_RM(NOTEXISTHUNTINGROOM)			
		,_RC_RM(INVALID_TEAM)					
		,_RC_RM(INVALID_RULE_VALUE)				
		,_RC_RM(INVALID_MODE_VALUE)				
		,_RC_RM(INVALID_LIMITUSERNUM)			
		,_RC_RM(INSUFFICIENT_MONEY)				
		,_RC_RM(PREVIOUS_MISSION_CLEAR)			
		,_RC_RM(LIMITMAP_FOR_ME)				
		,_RC_RM(SUCCESS_FOR_GENERAL_JOIN)		
		,_RC_RM(SUCCESS_FOR_RECOMMEND_JOIN)		
	};

	///////////////////////////////////////////////
	enum eLOGOUT_RESULT
	{
		 _RC_LOUT(NORMAL)						
		,_RC_LOUT(WRONG_VERSION)				
		,_RC_LOUT(AUTH_REQUEST)					
		,_RC_LOUT(WRONG_AUTHKEY)				
		,_RC_LOUT(HACKING_USER)					
		,_RC_LOUT(DB_ERROR)						
	};

	///////////////////////////////////////////////
	enum eZONE_RESULT
	{
		 _RC_ZON(SUCCESS)
		,_RC_ZON(FAILED)
		,_RC_ZON(ALREADY_IN_ZONE)
		,_RC_ZON(DISCONNECTED_DBPROXY)		

		,_RC(CHARSELECT_OUTOFRANGEINDEX)
		,_RC_CHR(DUPLICATEDREQ)
		,_RC_CON(NOTYETSERVICE)				
		,_RC_CON(REQUESTFROMGAMESERVER)	
		,_RC_CON(DONOTEXIST_LINKSERVER)
		,_RC_CON(ROOM_ALREADY_DOING_TRANSACTION)
		,_RC_CON(MAP_NOTFOUND)
		,_RC_CON(ZONESTATE_WRONG)
		,_RC_CHR(DBINFO_FAILED)
	};

	///////////////////////////////////////////////
    enum ePARTY_RESULT
    {
		 _RC_PTY(SUCCESS)

		,_RC_PTY(PLAYER_NOTEXIST)			
		,_RC_PTY(ISNOT_MASTEROFPARTY)		
		,_RC_PTY(ISNOT_MEMBEROFPARTY)		
		,_RC_PTY(PARTY_NOTEXIST)			
		,_RC_PTY(REJECT_INVITEMENT)			
		,_RC_PTY(ISNOT_QUALIFIED_LEVEL)		
		,_RC_PTY(INVALID_ROOMTYPE)			
		,_RC_PTY(CANNOT_CHANGE_MASTER)		
		,_RC_PTY(ALREADY_IS_MEMBEROFPARTY)	
		,_RC_PTY(WASNOT_REQUEST_PARTY)		
		,_RC_PTY(INVALID_DATATYPE)			
		,_RC_PTY(PLAYER_NOTEXIST_TO_FIELD)	
		,_RC_PTY(CANNOT_FIND_TARGET)		
		,_RC_PTY(TARGET_ISNOT_ENEMY)		
		,_RC_PTY(PLAYER_OBSERVER_MODE)		

		,_RC_PTY(UNKNOWN_REASON)			
    };

	///////////////////////////////////////////////
	enum eGM_RESULT
	{
		 _RC_GM(ROOM_NOT_EXIST)				
		,_RC_GM(INVALID_ARGUMENT)				
		,_RC_GM(ISNOT_GM)						
		,_RC_GM(NOTEXIST_USER)				
		,_RC_GM(CANNOT_RESURRECTION)			
		,_RC_GM(CANNOT_RECOVERY)				
		,_RC_GM(PLAYER_NOTEXIST)				

	};

	///////////////////////////////////////////////
	enum eBATTLE_RESULT
	{
		 _RC_WAR(SUCCESS)

		,_RC_WAR(PLAYER_NOTEXIST_TO_FIELD)					
		,_RC_WAR(INVALID_MAINTARGET)						
		,_RC_WAR(PLAYER_STATE_WHERE_CANNOT_ATTACK_ENEMY)	
		,_RC_WAR(VKR_RELOAD_COUNT_LACK)					
		,_RC_WAR(TRAGET_STATE_WHERE_CANNOT_ATTACKED)		
		,_RC_WAR(OUT_OF_RANGE)								
		,_RC_WAR(INVLIDPOS)								
		,_RC_WAR(THRUST_DIST_OVER)							
		,_RC_WAR(ALREADY_DOING_ACTION)						
		,_RC_WAR(BASEINFO_NOTEXIST)						
		,_RC_WAR(STYLECODE_WHERE_DONOT_SELECT)				
		,_RC_WAR(CHAR_CLASS_LIMIT)							
		,_RC_WAR(WEAPON_LIMIT)								
	};

	///////////////////////////////////////////////
	enum eCONDITION_RESULT
	{
		 _RC_COD(SUCCESS)
		,_RC_COD(ALREADY_SAME_CONDITION)				
		,_RC_COD(INVALID_CONDITION)						
	};

	///////////////////////////////////////////////
	enum ePVP_RESULT
	{
		 _RC_PVP(SUCCESS)
		,_RC_PVP(NOT_AT_ZONE)
		,_RC_PVP(INVALID_USER_STATE)
	};

	///////////////////////////////////////////////
	enum eSUMMON_RESULT
	{
		 _RC_SUM(SUCCESS)
		,_RC_SUM(INVALID_COMMAND)				
		,_RC_SUM(SUMMONED_NPC_NOTEXIST)		
		,_RC_SUM(SAME_COMMAND)					
	};

	///////////////////////////////////////////////
	enum eGUILD_RESULT
	{
		 _RC_GID(SUCCESS)
		,_RC_GID(FAILED)
		,_RC_GID(INVALID_STATE)
		,_RC_GID(CANNOT_CREAT_GUILD_STATE)
		,_RC_GID(CANNOT_CREAT_LEVEL_STATE)
		,_RC_GID(CANNOT_CREAT_MONEY_STATE)

		,_RC_GID(CANNOT_DESTROY_STATE)
		,_RC_GID(CANNOT_DESTROY_NOT_MASTER_STATE)
		,_RC_GID(CANNOT_DESTROY_EXIST_MEMBER_STATE)

		,_RC_GID(NOT_EXIST_TARGET_STATE)			
		,_RC_GID(CANNOT_INVITE_STATE)				
		,_RC_GID(DONT_HAVE_RIGHTS_STATE)			
		,_RC_GID(FULL_MEMBER_STATE)					

		,_RC_GID(NOT_EXIST_HOST_STATE)				

		,_RC_GID(CANNOT_WITHDRAW_STATE)
		
		,_RC_GID(NOT_EXIST_GUILD)

		,_RC_GID(CANNOT_JOIN_STATE)

		,_RC_GID(CANNOT_CHANGEPOSITION_STATE)

		,_RC_GID(CANNOT_SELECT_GUILD_STATE)			

		,_RC_GID(SUCCESS_SELECT)					
		,_RC_GID(DISCONNECTED_GUILD_SERVER)
		,_RC_GID(NEED_SELECT)						
		,_RC_GID(WAIT_A_WHILE_THEN_TRY_AGAIN)		
		,_RC_GID(ALREADY_SELECT_QUERY)				
	};

	///////////////////////////////////////////////
	enum eEVENT_RESULT
	{
		 _RC_EVT(SUCCESS)
		,_RC_EVT(FAILED)
		,_RC_EVT(ALREADY_ANOTHER_TRANSACTION)		
		,_RC_EVT(CANNOT_MOVE_TO_INVENTORY)
		,_RC_EVT(FULL_INVENTORY)					
		,_RC_EVT(NOTEXIST_EVENTITEM)				
	};

	///////////////////////////////////////////////
	enum eSYNC_RESULT
	{
		 _RC_SYN(SUCCESS)
		,_RC_SYN(PLAYER_IS_DEAD)				
		,_RC_SYN(FIELD_IS_NULL)				
		,_RC_SYN(ROUTE_SIZE_EXCESS)			
		,_RC_SYN(PLAYER_IS_UNMOVABLE_STATE)
		,_RC_SYN(SETPATH_ERROR)				
		,_RC_SYN(INVALID_MOVESATE)			
		,_RC_SYN(CUR_POSITION_IS_INVALID)	
		,_RC_SYN(DEST_POSITION_IS_INVALID)	
	};

	enum eQUEST_RESULT
	{
		 _RC_QUE(SUCCESS)
		,_RC_QUE(PLAYER_NOT_EXISTED)
		,_RC_QUE(INFO_NOT_EXISTED)
		,_RC_QUE(CANNT_DISCARD)
		,_RC_QUE(NOT_ACTIVATED)
		,_RC_QUE(INVALID_ACTION)
	};
};





#endif //__RESULTCODE_H__
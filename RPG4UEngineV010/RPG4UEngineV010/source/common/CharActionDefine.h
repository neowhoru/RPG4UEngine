/*////////////////////////////////////////////////////////////////////////
文 件 名：CharActionDefine.h
创建日期：2008年4月1日
最后更新：2008年4月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTIONDEFINE_H__
#define __CHARACTIONDEFINE_H__
#pragma once

namespace info
{ 
	enum  eMESH_ACTION_TYPE
	{
		 MESH_ACTION_idle				//待机   
		,MESH_ACTION_idle1			//待机1   
		,MESH_ACTION_idle2			//待机2   
		,MESH_ACTION_attack_idle	//战斗待机
		,MESH_ACTION_hurt				//被击
		,MESH_ACTION_run				//奔跑
		,MESH_ACTION_walk				//行走

		,MESH_ACTION_sit				//坐下    
		,MESH_ACTION_standup			//站起

		,MESH_ACTION_dead				//原地倒地
		,MESH_ACTION_dead1			//  


		,MESH_ACTION_Attack1				// 普通攻击1
		,MESH_ACTION_Attack2				// 普通攻击2
		,MESH_ACTION_Attack3				// 普通攻击2

		,MESH_ACTION_walk_attack		//行走攻击  
		,MESH_ACTION_run_attack			//奔跑攻击  

		//双手武器		
		,MESH_ACTION_double_attack_idle	// 战斗待机
		,MESH_ACTION_double_attack2		//普通攻击2 

		,MESH_ACTION_double_defend			//防御      
		,MESH_ACTION_double_recital		//武器吟唱  
		,MESH_ACTION_double_fire			//武器师法  
		,MESH_ACTION_double_hurt			//被击      
		,MESH_ACTION_double_dead			//死亡      
		,MESH_ACTION_double_attack1		//普通攻击1 
		,MESH_ACTION_double_scabbard		//收武器    
		,MESH_ACTION_double_draw			//拔出武器  

		//单手武器
		,MESH_ACTION_single_attack_idle		//战斗待机      
		,MESH_ACTION_single_idle				//非战斗待机    

		,MESH_ACTION_single_attackr1			//右手普通攻击1 
		,MESH_ACTION_single_attackl1			//左手普通攻击2   
		,MESH_ACTION_single_attackr2			//右手普通攻击3 
		,MESH_ACTION_single_attackl2			//左手普通攻击4 

		,MESH_ACTION_single_recital			//武器吟唱           
		,MESH_ACTION_single_fire				//武器师法           
		,MESH_ACTION_single_hurt				//被击               
		,MESH_ACTION_single_dead				//死亡               
		,MESH_ACTION_jump							//跳起               
		,MESH_ACTION_drop							//落地               
		,MESH_ACTION_glide						//停空               
		,MESH_ACTION_falltothegroundrun		//落地跑             
		,MESH_ACTION_single_double_draw		//双手拔出武器       
		,MESH_ACTION_single_double_scabbard	//双手收起武器       
		,MESH_ACTION_single_draw				//单手收起武器       
		,MESH_ACTION_single_scabbard			//单手拔出武器       
		,MESH_ACTION_single_walk_attackr1	//行走右手攻击       
		,MESH_ACTION_single_walk_attackl1	//行走左手攻击       
		,MESH_ACTION_single_run_attackr1		//跑步右手攻击       
		,MESH_ACTION_single_run_attackl1		//跑步右手攻击        
		,MESH_ACTION_run_fire					//跑步施法           
		,MESH_ACTION_walk_fire					//行走施法       



		,MESH_ACTION_Max
	};

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	enum eCHAR_SEQUENCE_TYPE
	{
		 NullAction = -1
		,Model
		,Idle1
		,Idle2			// 闲置
		,Walk1
		,WalkBackWards
		,Run1
		,Rush
		,Pelt  //冲击
		,Swim
		,SwimIdle
		,Drunk
		,Decline
		,Combat
		,Resist
		,Miss
		,Sway
		,BeatBack
		,Hold
		,Death

		,shuffle
		,shuffleleft
		,shuffleright
		,mountShuffleleft
		,mountShuffleright

		//////////////////////////////////////////////
		,Pick			//采药
		,Hack			//砍伐
		,Dig			//采矿
		,Recital		//吟唱
		,Fire			//师法
		,Defend		//防御

		///////////////////////////////////////////////////
		// 魔法动作
		,MagicBorn
		,MagicDead
		,MagicRun
		,MagicExplode
		,MagicFire

		,Run			// 跑
		,Walk			// 走
		,Idle			// 待机
		,Dead			// 死亡
		,Stand		// 起身
		,Tired		// 疲劳
		,Hurt			// 被击
		,Miss1		// 失效
		,Intonate	// 吟唱

		,walk_attack	//行走攻击  
		,run_attack		//奔跑攻击  
		,Attack_idle	//(战斗待机)new
		,Attack1			// 普通攻击1
		,Attack2			// 普通攻击2
		,Attack3			// 普通攻击2

		//////////////////////////////////////////////////////
		//双手武器		
		,double_attack_idle	// 战斗待机
		,double_attack2		//普通攻击2  

		,double_defend		//防御       
		,double_recital		//武器吟唱   
		,double_fire			//武器师法   
		,double_hurt			//被击       
		,double_dead			//死亡       
		,double_attack1		//普通攻击1  
		,double_scabbard		//收武器        
		,double_draw			//拔出武器      
		,double_rush			

		///////////////////////////////////////////
		//单手武器
		,single_rush			
		,single_attack_idle	//战斗待机      
		,single_idle			//非战斗待机    

		,single_attackr1		//右手普通攻击1 
		,single_attackl1		//左手普通攻击2   
		,single_attackr2		//右手普通攻击3 
		,single_attackl2		//左手普通攻击4 


		,single_recital		//武器吟唱     
		,single_fire			//武器师法     
		,single_hurt			//被击         
		,single_dead			//死亡              

		,jump						//跳起         
		,drop						//落地         
		,glide					//停空         
		,falltothegroundrun	//落地跑       

		,jump1
		,drop1
		,glide1
		,falltothegroundrun1

		,single_double_draw			//双手拔出武器        
		,single_double_scabbard		//双手收起武器        
		,single_draw					//单手收起武器        
		,single_scabbard				//单手拔出武器        
		,single_walk_attackr1		//行走右手攻击        
		,single_walk_attackl1		//行走左手攻击        
		,single_run_attackr1			//跑步右手攻击        
		,single_run_attackl1			//跑步右手攻击         
		,run_fire						//跑步施法            
		,walk_fire						//行走施法        

		//特殊动作
		,strike						//冲击        
		,sting						//刺甲        
		,brisance					//震裂        
		,frenzy						//狂暴        

		,MaxAction
	};






};//namespace info


#endif //__CHARACTIONDEFINE_H__
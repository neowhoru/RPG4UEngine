/*////////////////////////////////////////////////////////////////////////
文 件 名：StructBase.h
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTBASE_H__
#define __STRUCTBASE_H__
#pragma once



#include <DataTypeDefine.h>
#include <ConstItem.h>
#include <MathStructDecl.h>
#include <StructCharacter.h>
#include <StructItem.h>
#include <StructSkill.h>
#include <StructMap.h>
#include <StructStream.h>


#pragma pack(push,1)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
union uDataChunkCursor
{
	bool*			boolean;
	char*			c;
	BYTE*			byte;
	short*		s;
	WORD*			w;
	int*			i;
	DWORD*		dw;
	void*			p;
	float*		f;
	double*		d;
	__int64*    i64;   

	uDataChunkCursor(void* in) :p(in)
	{
	}
	uDataChunkCursor():p(NULL)
	{}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sFORMULA_RATIOINFO_BASE
{
	WORD		m_InfoCode;	
	float		m_fRatio;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sGUILD_INFO_BASE
{
	GUILDGUID	m_GuildGuid;
	TCHAR			m_szGuildName	[MAX_GUILDNAME_LENGTH];
	BYTE			m_GuildGrade;
	BYTE			m_MarkBinary	[MAX_GUILDMARKSTREAM_SIZE];
	INT			m_MasterCharGuid;
	TCHAR			m_szMasterCharName[MAX_CHARNAME_LENGTH];
	TCHAR			m_szGuildNotice	[MAX_GUILDNOTICE_LENGTH];
	GPTYPE		m_GuildPoint;
	UPTYPE		m_UserPoint;
	INT			m_RestrictedRight;
	INT64			m_GuildMoney;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sGUILD_MEMBER_INFO_BASE
{
	CHARGUID		m_CharGuid;
	TCHAR			m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE			m_byClass;
	LEVELTYPE	m_LV;
	BYTE			m_byPosition;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sGUILD_MEMBER_INFO : public sGUILD_MEMBER_INFO_BASE
{
	BYTE		m_byIsLogin;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sGUILD_INFO_PACKET
{
	TCHAR			m_szGuildName[MAX_GUILDNAME_LENGTH];
	BYTE			m_GuildGrade;
	INT			m_MasterCharGuid;
	TCHAR			m_szMasterCharName[MAX_CHARNAME_LENGTH];
	GPTYPE		m_GuildPoint;
	UPTYPE		m_UserPoint;
	INT			m_RestrictedRight;
	INT64			m_GuildMoney;
	WORD			m_GuildMemberNum;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sGUILD_MEMBER_INFO_PACKET
{
	TCHAR		m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE		m_byClass;
	LEVELTYPE	m_LV;
	BYTE		m_byPosition;
	BYTE		m_byOnline;	
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sFRIEND_INFO_DB
{
	enum { INFO_FRIEND = 0, INFO_BLOCK = 1, };
	DWORD			dwFriendGuid;
	TCHAR			szFriendName[MAX_CHARNAME_LENGTH];
	BYTE			byClass;	
	LEVELTYPE	Level;
	BYTE			Sts;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sFRIEND_INFO_PACKET
{
	TCHAR			szFriendName[MAX_CHARNAME_LENGTH];	
	BOOL			bOnline;			
	BYTE			byClass;	
	LEVELTYPE	Level;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sFRIEND_INFO_BASE
{
	DWORD		dwFriendGuid;
	sFRIEND_INFO_PACKET	sPacketInfo;

	sFRIEND_INFO_PACKET* GetPacketInfo() { return &sPacketInfo; }
};


/*////////////////////////////////////////////////////////////////////////
黑名单
/*////////////////////////////////////////////////////////////////////////
struct sBLOCK_INFO_BASE
{
	DWORD		dwBlockGuid;
	TCHAR		szBlockName[MAX_CHARNAME_LENGTH];
};


typedef sBLOCK_INFO_BASE	sBLOCK_INFO_DB;


struct sBLOCK_INFO_PACKET
{
	TCHAR		szBlockName[MAX_CHARNAME_LENGTH];
};


#include "StructBase.inl"

#pragma pack(pop)




#endif //__STRUCTBASE_H__
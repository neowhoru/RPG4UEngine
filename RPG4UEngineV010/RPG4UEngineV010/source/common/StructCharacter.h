/*////////////////////////////////////////////////////////////////////////
文 件 名：StructCharacter.h
创建日期：2007年5月27日
最后更新：2007年5月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRUCTCHARACTER_H__
#define __STRUCTCHARACTER_H__
#pragma once


#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sCHARINFO_INIT
{
	BYTE			m_ClassCode;
	VRSTR			m_sClassName;

	LEVELTYPE	m_LV;
	DWORD			m_dwMaxHP;
	DWORD			m_dwMaxMP;
	DWORD			m_dwExp;
	DWORD			m_dwRemainStat;
	DWORD			m_dwRemainSkill;

	DWORD			m_dwRegion;
	SHORT			m_sLocationX;
	SHORT			m_sLocationY;
	SHORT			m_sLocationZ;

	SHORT			m_sStrength;
	SHORT			m_sDexterity;
	SHORT			m_sVitality;
	SHORT			m_sInteligence;
	SHORT			m_sSpirit;
	SHORT			m_sLearn;
	SHORT			m_sCredit;
	SHORT			m_sSkillStat1;
	SHORT			m_sSkillStat2;
};





/*////////////////////////////////////////////////////////////////////////
玩家基本信息
/*////////////////////////////////////////////////////////////////////////
struct sPLAYERINFO_BASE
{
	USERGUID		m_UserGuid;
	BYTE			m_bySlot;
	BYTE			m_byClassCode;
	CHARGUID		m_CharGuid;
	TCHAR			m_szCharName[MAX_CHARNAME_LENGTH];

	LEVELTYPE	m_LV;
	DWORD			m_dwExp;
	DWORD			m_dwMaxHP;
	DWORD			m_dwHP;
	DWORD			m_dwMaxMP;
	DWORD			m_dwMP;
	MONEY			m_Money;
	DWORD			m_dwRemainStat;	
	DWORD			m_dwRemainSkill;
	WORD			m_wSelectStyleCode;
	BYTE			m_byPKState;
	BYTE			m_byCharState;
	INT64			m_StateTime;
	DWORD			m_dwRegion;

	SHORT			m_sLocationX;
	SHORT			m_sLocationY;
	SHORT			m_sLocationZ;
	TCHAR			m_szTitleID[MAX_TITLE_LENGTH];
	INT64			m_TitleTime;
	BOOL			m_bInventoryLock;

	SHORT			m_sStrength;
	SHORT			m_sDexterity;
	SHORT			m_sVitality;
	SHORT			m_sInteligence;
	SHORT			m_sSpirit;
	SHORT			m_sLearn;
	SHORT			m_sCredit;
	SHORT			m_sSkillStat1;
	SHORT			m_sSkillStat2;

	BYTE			m_byHeight;
	BYTE			m_byFace;
	BYTE			m_byHair;
	BYTE			m_byHairColor;
	BYTE			m_bySex;
	INT			m_PlayLimitedTime;

	// PVP
	DWORD			m_dwPVPPoint;
	DWORD			m_dwPVPScore;
	BYTE			m_byPVPGrade;
	//DWORD		m_dwPVPRanking;
	DWORD			m_dwPVPMaxSeries;
	DWORD			m_dwPVPMaxKill;
	DWORD			m_dwPVPMaxDie;
	DWORD			m_dwPVPTotalKill;
	DWORD			m_dwPVPTotalDie;
	DWORD			m_dwPVPTotalDraw;

	UPTYPE		m_UserPoint;
	BYTE			m_byInvisibleOptFlag;			//< eINVISIBLE_OPTION_FLAG 

	GUILDGUID	m_GuildGuid;
	TCHAR			m_szGuildName[MAX_GUILDNAME_LENGTH];
	BYTE			m_GuildPosition;
	TCHAR			m_szGuildNickName[MAX_CHARNAME_LENGTH];
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sSPECIAL_CONDITION
{
	BYTE			byCondition;
	WORD			wConditionParam;
	WORD			wActionType;
	WORD			wActionParam;
	BYTE			byActionRate;			
};

/*////////////////////////////////////////////////////////////////////////
// 状态抗性
/*////////////////////////////////////////////////////////////////////////
struct sRESISTANCE_CONDITION
{
	WORD			m_wStateCode;			// 状态代码
	BYTE			m_byRatio;				// 抵抗成功率
	TAG_CODE		m_EffectTag;	// 特效
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sNPCINFO_BASE
{
	enum {MAX_DROPITEMCODE_NUM = 3,ACTION_NUM=2,SKILL_NUM=3};

	MONSTERCODE		m_MonsterCode;
	MONSTERCODE		m_vMonsterCode;			//VNPCINFO编号

	VRSTR				m_NpcName;
	LEVELTYPE		m_Level;
	LEVELTYPE		m_DisplayLevel;
	BYTE				m_byChangeTargetRatio;		// 变换目标频率
	DWORD				m_dwMaxHP;
	DWORD				m_dwMaxMP;
	DWORD				m_dwHP;
	DWORD				m_dwMP;
	CODETYPE			m_AIInfoCode;					// AiInfo编号
	BYTE				m_AIInfoRate;					// AIInfo机率,满100
	BYTE				m_byGrade;						// 怪物类别 参考：eNPC_GRADE
	WORD				m_wSize;							// 身形大小
	//WORD				m_wAttProperty;				// 攻击属性(MELEE近距物攻,远距物攻...) eATTACK_KIND

	BYTE				m_byCriticalRatio;			// 爆击概率
	DWORD				m_dwMinAttackPower;			// 最小攻	
	DWORD				m_dwMaxAttackPower;			// 最大攻
	DWORD				m_dwPhyDef;						// 物防
	DWORD				m_dwMagicDef;					//	魔防 
	WORD				m_wAttitude;					// 攻击AI(后攻、优先进攻、条件优先进攻...) 见：eATTACK_ATTITUDE
	WORD				m_wMoveAttitude;				// 移动AI(游荡型、监视型...) 见eNPC_MOVE_ATTITUDE
	DWORD				m_dwMoveAreaID;				// 移动区域 ID
	WORD				m_wClass;						// 怪物分类

	BYTE				m_bySeries;						// 攻击系别 eATTACK_KIND
	SHORT				m_wElementDef		[ELEMENTKIND_MAX];	///< 各类元素抗性
	SHORT				m_wElementAttack	[ELEMENTKIND_MAX];	///< 各类元素攻击力

	WORD				m_wPhyAttRate;					// 物理攻击成功率
	WORD				m_wPhyAttAvoid;				// 物理攻击闪避率
	WORD				m_wAttType;						// 攻击类型 (1:砍杀,2:刺穿,3:击打)
	WORD				m_wArmorType;					// 装备类型(1:Hard, 2:Medium, 3:Soft, 4:Siege, 5:Unarmer)
	float				m_fAttRange;					// 攻击范围	
	float				m_fViewRange;					// 视野范围
	float				m_fMoveRange;					// 移动范围
	WORD				m_wStandMin;					// 攻击后站立最短时间
	WORD				m_wStandMax;					// 攻击后站立最长时间
	float				m_fWalkSpeed;					// 步速	
	float				m_fRunSpeed;					// 跑速	
	WORD				m_wAttSpeed			[ATTACK_SEQUENCE_MAX];	// 攻速
	//WORD				m_wProjectileCode	[ATTACK_SEQUENCE_MAX];			// 抛射体编号
	BOOL				m_bJump;							// 跳跃
	TAG_CODE			m_SpawnAniCode;			// 出生动画编号
	DWORD          m_dwSpawnTime;					// 出生时间

	sSPECIAL_CONDITION		m_SpecialCondition	[ACTION_NUM];			// 特殊动作
	sRESISTANCE_CONDITION	m_ResistanceCondition[ACTION_NUM];	// 状态反抗

	WORD				m_wSkillUpdateTime;				// 检查技能条件的周期时间
					
	BYTE				m_byReviveCondition;				// 复活条件
	WORD				m_wReviveConditionParam;		// 复活参数
	BYTE				m_byReviveRate;					// 复活概率
	SLOTCODE			m_wReviveCode;						// 复活技能编号
	WORD				m_wReviveDelay;					// 复活动画延迟
					
	BYTE				m_byHealCondition;				// 治疗条件
	WORD				m_wHealConditionParam;			// 治疗参数
	BYTE				m_byHealRate;						// 治疗概率
	SLOTCODE			m_wHealCode;						// 治疗技能编号
	WORD				m_wHealDelay;						// 治疗动画延迟
					
	BYTE				m_bySummonCondition;				// 召唤条件
	WORD				m_wSummonConditionParam;		// 召唤参数
	BYTE				m_bySummonRate;					// 召唤概率
	SLOTCODE			m_wSummonCode;						// 召唤技能编号
	WORD				m_wSummonDelay;					// 召唤动画延迟
					
	BYTE				m_bySkillUsePossible;			// 非召唤技能使用概率(普通攻击)
	SLOTCODE			m_wSkillCode[SKILL_NUM];		// 使用技能
	BYTE				m_bySkillRate[SKILL_NUM];		// 技能概率
	WORD				m_wSkillDelay[SKILL_NUM];		// 使用间隔
					
	//WORD				m_wHateSkill;					// 仇恨技能
	//WORD				m_wRevengeSkill;				// 报复技艺
	//BYTE				m_byRevengeRate;				// 报复概率
					
	WORD				m_byItemDropRate;				// 物品掉落概率

	SLOTCODE			m_ItemCode		[MAX_DROPITEMCODE_NUM];		// 物品编号
	WORD				m_wDropItemRate[MAX_DROPITEMCODE_NUM];	// 物品掉落率




	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	BOOL CanAttack();
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sVNPCINFO_BASE
{
	MONSTERCODE		m_vMonsterCode;				/// 	v怪物索引
	VRSTR				m_sMonsterName;				/// 	怪物名

	RESOURCE_ID		m_resModelID;					/// 	模型ID
	WORD				m_wSoundTempl;					/// 	声音模板，见 CharSoundInfo
	DWORD				m_MonsterType;					///	类别  eNPC_GRADE
	DWORD				m_SkillCode[ATTACK_SEQUENCE_MAX];		///	技能1 技能2 技能3
	float				m_fBodySize;					///	体型修正
	VRSTR				m_sSkin;							///	皮肤
	float				m_fAlpha;						///	透明度	
	float				m_ElementSizes	[ELEMENTKIND_MAX];	///< 系别体型修正
	VRSTR				m_ElementSkins	[ELEMENTKIND_MAX];	///< 系别皮肤
	float				m_ElementAlphas[ELEMENTKIND_MAX];	///< 系别透明
	BYTE				m_byWeaponSound;				///	武器音效
	BYTE				m_byArmourTexture;			///	防具质地
	INT				m_nFootNum;						///	脚数量	人形怪2 四足怪4
	DWORD				m_dwIdleSoundPeriod;			///	闲时音效周期 0不播放
	BYTE				m_byPlayBattleVoiceRate;	/// 战斗对话机率
	BYTE				m_byPlayIdleVoiceRate;		/// Idle对话机率
	BYTE				m_byPlayHurtVoiceRate;		/// 受伤对话机率
	BYTE				m_byPlayDieVoiceRate;		/// 死亡对话机率
};


/*////////////////////////////////////////////////////////////////////////
功能Npc信息
/*////////////////////////////////////////////////////////////////////////
struct sNPCINFO_FUNC
{
	CODETYPE			m_ExtraCode;
	VRSTR				m_Name;
	CODETYPE			m_MapCode;
	FIELDID			m_FieldID;

	eNPC_FUNC_TYPE	m_eNPCTYPE;
	CODETYPE			m_NPCCODE;
	BOOL				m_LocalNPC;
	CODETYPE			m_ClientScriptCode;
	CODETYPE			m_ServerScriptCode;
	VECTOR3D			m_vPos;

	INT				m_nAngle;	///角度
	DWORD				m_dwMoveDelayMin;
	DWORD				m_dwMoveDelayMax;
	int				m_iMoveType;
	union
	{
		float			m_fRange;
		INT			m_nStepNum;
	};
	VECTOR3D			m_arMoveNodes[MAX_ROUTE_SIZE-1];
};

#pragma pack(pop)



#endif //__STRUCTCHARACTER_H__
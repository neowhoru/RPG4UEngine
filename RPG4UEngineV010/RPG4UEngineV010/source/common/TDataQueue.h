/*////////////////////////////////////////////////////////////////////////
文 件 名：TDataQueue.h
创建日期：2007年11月10日
最后更新：2007年11月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TDATAQUEUE_H__
#define __TDATAQUEUE_H__
#pragma once


//////////////////////////////////////////////////////////////////////////
template< typename _DataType, const int _MaxCount >
class TDataQueue
{
public:
    TDataQueue()
    {
        m_Counter = 0;
        m_Pointer = 0;
    }
    
    void Reset()
    {
        m_Counter = 0;
        m_Pointer = 0;
    }
    
    void Copy( _DataType &Dst, _DataType &Src )
    {
        memcpy( &Dst, &Src, sizeof(_DataType) );
    }

    void Join( _DataType &Data )
    {
        Copy( m_DataQ[m_Pointer], Data );
        m_Pointer++;
        m_Counter++;
        if( m_Pointer >= _MaxCount )
            m_Pointer = 0;
        if( m_Counter >= _MaxCount )
            m_Counter = _MaxCount;
    }
    
    _DataType *KickOut()
    {
        if( m_Counter <= 0 )
            return NULL;

        m_Pointer--;
        if( m_Pointer < 0 )
            m_Pointer = _MaxCount - 1;
        m_Counter--;
        return &m_DataQ[m_Pointer];
    }
    int Count() { return m_Counter; }

protected:

private:

    // 数据区
    _DataType m_DataQ[_MaxCount];
    // 当前总数
    int m_Counter;
    // 当前指向的位置，就是增加时加入到的位置
    int m_Pointer;
};


#endif //__TDATAQUEUE_H__
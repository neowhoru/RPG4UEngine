/*////////////////////////////////////////////////////////////////////////
文 件 名：THashTable.h
创建日期：2008年1月11日
最后更新：2008年1月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __THASHTABLE_H__
#define __THASHTABLE_H__
#pragma once

//
// THashTable<TTT> * hashTable = new THashTable<TTT>;
// hashTable->Initialize(100);
// hashTable->RemoveAll();
// delete hashTable;

////////////////////////////////////////////////////////////////////////////


#include "CommonDefine.h"
#include "TMemoryPoolFactory.h"

namespace util
{
#ifdef USE_PAUSE
template <class T, class Type=DWORD >
class THashBucketDat;

template <class T, class Type=DWORD >
class	THashTable;
#endif

class THashBucket;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API THashNode
{
public:
	THashNode()
		:m_pNext(NULL),m_pPrev(NULL),m_pBucket(NULL){}

	THashNode *				m_pNext;
	THashNode *				m_pPrev;
	THashBucket *			m_pBucket;
};

class _BASE_API THashBucket
{
public:
	THashBucket()
		:m_pList(NULL),m_pNext(NULL),m_pPrev(NULL){}
	THashNode *				m_pList;	
	THashBucket *			m_pNext;
	THashBucket *			m_pPrev;
};


#ifdef USE_PAUSE
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class T, class Type=DWORD >
class THashNode
{
public:
	THashNode *					m_pNext;
	THashNode *					m_pPrev;
	THashBucketDat<T,Type> *	m_pBucket;
};
#endif


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class T, class Type >
class THashBucketDat : public THashBucket
{
public:
	Type						m_dwKey;
	T							m_Data;
#ifdef USE_PAUSE
	THashNode<T,Type> *	m_pList;	
	THashBucketDat *			m_pNext;
	THashBucketDat *			m_pPrev;
	void SetData(T pvoid, Type key) { m_Data = pvoid;m_dwKey = key; m_pNext = NULL; m_pPrev = NULL; }
#endif

	void SetData(T pvoid, Type key) { m_Data = pvoid;m_dwKey = key;}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class T, class Type=DWORD >
class TIterator
{
#ifdef USE_PAUSE
	friend class			THashTable<T,Type>;
	THashNode<T,Type> *	m_pCursor;
#endif

public:
	TIterator();
	TIterator( const TIterator & itr );
	~TIterator();

public:
	TIterator & operator++();
	TIterator & operator++( int i );
	TIterator & operator--();
	TIterator & operator--( int i );

	BOOL operator!=( TIterator & it );
	BOOL operator==( TIterator & it );
	T operator*();

public:
	THashNode *		m_pCursor;

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API THashTableBase
{
public:
	THashTableBase();
	~THashTableBase();

public:
	virtual BOOL	Initialize	(DWORD dwMaxBucketNum);
	virtual void	RemoveAll();


protected:
	virtual THashBucket*	_AllocBucket()=0;
	virtual void			_FreeBucket(THashBucket*)=0;

protected:
	THashBucket*			_OnAddData	(DWORD64 index);
	/// Iterate Bucket Chain List
	void						_RemoveCurBucketData	(DWORD64 dwIndex);

	/// delete bucket, list for (dwKey)
	void						_Remove(DWORD64 dwIndex, THashBucket*	cur);

	BOOL						_RemoveHead();
	/// delete all of Bucket, List Node

	THashNode *				_AddList		(THashBucket * pBucket);
	void						_RemoveList	(THashNode * pList);

protected:
	THashBucket *				m_pCurBucket;
	THashBucket*				m_pLastBucket;

	BOOL							m_bInited;
	VG_DWORD_GET_PROPERTY	(DataNum);
	DWORD							m_dwMaxBucketNum;
	THashBucket **				m_ppBucketTable;

	/// for Iterator
	THashNode *					m_pListTail;
	THashNode * 				m_pListHead;
	THashNode *					m_pListCur;

	TMemoryPoolFactory<THashNode>	*		m_pHashListPool;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class T, class Type=DWORD >
class THashTable : public THashTableBase
{	
public:
	typedef THashBucketDat<T,Type>			HashBucket;
#ifdef USE_PAUSE
	typedef THashNode<T,Type>		HashNode;
#endif
	typedef THashNode								HashNode;

	typedef typename TIterator<T,Type>		Iterator;
	typedef typename TIterator<T,Type>		iterator;
public:

	THashTable();

	THashTable( DWORD dwMaxBucketNum );
	
	virtual ~THashTable();
	
	
public:
	BOOL	Initialize	(DWORD dwMaxBucketNum);

	/// don't check for the same key
	BOOL	Add(T TData, Type dwKey);
	
	T		GetData(Type dwKey);
	
	T		GetHeadData();
	T		GetTailData();
	
	/// Iterate List
	void		SetFirst();
	T			GetNext();

	void		SetLast();
	T			GetPrev();


	/// Iterate Bucket Chain List
	void		SetBucketFirst			(Type dwKey);
	T			GetBucketNext			();
	void		RemoveCurBucketData	();

	
	/// delete bucket, list for (dwKey)
	void		Remove(Type dwKey);

	T			RemoveHead();
	/// delete all of Bucket, List Node
	void		RemoveAll();
	
public:

	Iterator find	( Type dwKey );
	Iterator begin	();
	Iterator end	();

	void erase		( Iterator & it );
	void clear		();
	
protected:
	THashBucket*	_AllocBucket();
	void				_FreeBucket(THashBucket*);
	
protected:
	/// for bucket Iterator
	Type					m_dwBucketKey;

	// memorypool
	TMemoryPoolFactory<HashBucket>	*	m_pHashBucketPool;
};


} /// namespace util


#include "THashTable.inl"



#endif //__THASHTABLE_H__
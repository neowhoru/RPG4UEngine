/*////////////////////////////////////////////////////////////////////////
文 件 名：MathConst.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	数据库常量定义

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHCONST_H__
#define __MATHCONST_H__
#pragma once


namespace math
{ 
	const float cBOX_PLUS	= 1e10f;
	const float cBOX_MINUS	= -1e10f;
	const float cEPSILON		= 0.000001f;
	const float cINFINITE	= 1000000000000.0f;
	const float cZERO			= 1e-08f;
	const float cZERO_T2		= cZERO*cZERO;

	//角度基本定义 V为Div  X为Mul
	const float cPI		= 3.1415926535897932384626433832795028841971693993751f;
	const float cPI_TWO	= cPI*2.0f;
	const float cPI_HALF	= cPI*0.5f;

	const float cPI_X1	= cPI;			//180度
	const float cPI_X2	= cPI*2.0f;		//360度
	const float cPI_270	= cPI_HALF*3.f;//270度

	
	const float cPI_V1	= cPI/1.0f;		//180度
	const float cPI_V2	= cPI/2.0f;		//90度
	const float cPI_V3	= cPI/3.0f;		//60度
	const float cPI_V4	= cPI/4.0f;		//45度
	const float cPI_V5	= cPI/5.0f;		//36度
	const float cPI_V6	= cPI/6.0f;		//30度
	const float cPI_V180	= cPI/180.0f;	//每1角度的弧度值

	const float cPI_INV		= 1.0f/cPI;		//PI的倒数
	const float cPI_INV180	= 180.0f/cPI;	//  180/PI

	const float	cDEGREE_ROUND		= 360.0f;
	const float	cDEGREE_HALFROUND	= 180.0f;
	const float	cDEGREE_ZERO		= 0.0f;

};//namespace math


#define MATH_PI			math::cPI
#define MATH_EPSILON		math::cEPSILON
#define MATH_INFINITE	math::cINFINITE


#endif //__MATHCONST_H__


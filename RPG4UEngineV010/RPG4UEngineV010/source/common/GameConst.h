/*////////////////////////////////////////////////////////////////////////
文 件 名：GameConst.h
创建日期：2007年9月2日
最后更新：2007年9月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMECONST_H__
#define __GAMECONST_H__
#pragma once


const DWORD TOOLTIP_INFO_NAME                   = _BIT(0);
const DWORD TOOLTIP_INFO_NAME_DISABLE           = _BIT(1);
const DWORD TOOLTIP_INFO_REQUIRED_SKILLSTAT0    = _BIT(2);
const DWORD TOOLTIP_INFO_REQUIRED_SKILLSTAT1    = _BIT(3);
const DWORD TOOLTIP_INFO_REQUIRED_SKILLPOINT    = _BIT(4);
const DWORD TOOLTIP_INFO_NEXT_SKILLSTAT0        = _BIT(5);
const DWORD TOOLTIP_INFO_NEXT_SKILLSTAT1        = _BIT(6);
const DWORD TOOLTIP_INFO_NEXT_SKILLPOINT        = _BIT(7);
const DWORD TOOLTIP_INFO_EXT_EFFECT0            = _BIT(8);
const DWORD TOOLTIP_INFO_EXT_EFFECT1            = _BIT(9);
const DWORD TOOLTIP_INFO_DESC0                  = _BIT(10);
const DWORD TOOLTIP_INFO_DESC1                  = _BIT(11);
const DWORD TOOLTIP_INFO_REQUIRED_SKILLLEVEL    = _BIT(12);
const DWORD TOOLTIP_INFO_NEXT_SKILLLEVEL        = _BIT(13);
const DWORD TOOLTIP_INFO_MAX_SKILLLEVEL         = _BIT(14);


const DWORD TOOLTIP_STYLE_INFO_NAME						= _BIT(0);
const DWORD TOOLTIP_STYLE_INFO_NAME_DISABLE			= _BIT(1);
const DWORD TOOLTIP_STYLE_INFO_REQUIRED_LEVEL		= _BIT(2);
const DWORD TOOLTIP_STYLE_INFO_REQUIRED_SKILLPOINT	= _BIT(3);
const DWORD TOOLTIP_STYLE_INFO_NEXT_LEVEL				= _BIT(4);
const DWORD TOOLTIP_STYLE_INFO_NEXT_SKILLPOINT		= _BIT(5);
const DWORD TOOLTIP_STYLE_INFO_EXT_EFFECT0			= _BIT(6);
const DWORD TOOLTIP_STYLE_INFO_EXT_EFFECT1			= _BIT(7);
const DWORD TOOLTIP_STYLE_INFO_DESC0					= _BIT(8);
const DWORD TOOLTIP_STYLE_INFO_DESC1					= _BIT(9);

const DWORD TOOLTIP_ITEM_INFO_NAME						= _BIT(0);   // 
const DWORD TOOLTIP_ITEM_INFO_TYPE                 = _BIT(1);   // 
const DWORD TOOLTIP_ITEM_INFO_ADDITIONAL_INFO      = _BIT(2);   // 
const DWORD TOOLTIP_ITEM_INFO_ADINFO_PRICE         = _BIT(3);   // 
const DWORD TOOLTIP_ITEM_INFO_ADINFO_PRICE_USED    = _BIT(4);   // 
const DWORD TOOLTIP_ITEM_INFO_DURABLE              = _BIT(5);   // 

const DWORD TOOLTIP_EXTRA_INFO_DONOT_CHANGE_LINE	= _BIT(20);   // 
const DWORD TOOLTIP_EXTRA_INFO_DT_LEFT					= _BIT(21);   // 
const DWORD TOOLTIP_EXTRA_INFO_DT_CENTER				= _BIT(22);   // 
const DWORD TOOLTIP_EXTRA_INFO_DT_RIGHT				= _BIT(23);   // 
const DWORD TOOLTIP_EXTRA_ICON_DT_ORGIN				= _BIT(24);   // 
const DWORD TOOLTIP_EXTRA_ICON_DT_LEFT					= _BIT(25);   // 
const DWORD TOOLTIP_EXTRA_ICON_DT_RIGHT				= _BIT(26);   // 



//-----------------------------------------------------------------------------
// ESC 处理类型
//-----------------------------------------------------------------------------
enum eESC_PROC_TYPE
{
	 ESC_PROC_SYSTEM_DIALOG
	,ESC_PROC_AREA_POINTER
	,ESC_PROC_TARGET
	,ESC_PROC_AUTO_ATTACK
	,ESC_PROC_AUTO_RUN
	,ESC_PROC_DRAG_ITEM

	,ESC_PROC_DIALOG_BANK
	,ESC_PROC_DIALOG_SHOP
	,ESC_PROC_DIALOG_ENCHANT
	,ESC_PROC_DIALOG_RANKUP
	,ESC_PROC_DIALOG_EVENT_INVENTORY
	,ESC_PROC_DIALOG_INVENTORY
	,ESC_PROC_DIALOG_SKILL
	,ESC_PROC_DIALOG_CHAT
	,ESC_PROC_DIALOG_TRADE
	,ESC_PROC_DIALOG_VENDOR
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSKILLEFFECT_POS
{
	 SKILLEFFECT_POS_NONE = 0
	,SKILLEFFECT_POS_HEAD = 1
	,SKILLEFFECT_POS_BODY
	,SKILLEFFECT_POS_FOOT
	,SKILLEFFECT_POS_HAND

};

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
enum eMOUSE_OVER_TYPE
{
	 eMOUSE_OVER_SLOT = 0
	,eMOUSE_OVER_SKILL
   ,eMOUSE_OVER_STYLE

	,eMOUSE_OVER_MAX
};




#endif //__GAMECONST_H__
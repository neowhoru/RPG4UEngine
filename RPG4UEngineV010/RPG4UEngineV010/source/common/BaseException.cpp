/*////////////////////////////////////////////////////////////////////////
文 件 名：BaseException.cpp
创建日期：2007年4月8日
最后更新：2007年4月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "BaseException.h"



namespace Common
{
BaseException::BaseException()
{
}

BaseException::~BaseException()
{
}

BaseException::BaseException(int nCode, StringEx strMsg):m_nCode(nCode)
{
    m_strMsg = strMsg;
}

BaseException::BaseException(const BaseException& exception):m_nCode(exception.GetCode())
{
    m_strMsg = exception.GetMsg();
}

const BaseException& BaseException::operator = (const BaseException& exception)
{
    m_nCode = exception.GetCode();
    m_strMsg = exception.GetMsg();
    return *this;
}

BaseException::BaseException(int nCode, const TCHAR* pszFormat, ...):m_nCode(nCode)
{
	va_list args;
	va_start(args, pszFormat);

#ifdef _UNICODE
    unsigned int nLength = _vscwprintf(pszFormat, args);
    m_strMsg.resize(nLength);
    _vsnwprintf_s(const_cast<wchar_t*>(m_strMsg.c_str()), _TRUNCATE, m_strMsg.length(), pszFormat, args);
#else
    unsigned int nLength = _vscprintf(pszFormat, args);
    m_strMsg.resize(nLength);
	_vsnprintf_s(const_cast<char*>(m_strMsg.c_str()), _TRUNCATE, m_strMsg.length(), pszFormat, args);
#endif

	va_end(args);	
}

void BaseException::SetMsg(StringEx strMsg)
{
    m_strMsg = strMsg;
}

void BaseException::SetCode(int nCode)
{
    m_nCode = nCode;
}

const StringEx& BaseException::GetMsg() const
{
    return m_strMsg;
}

int BaseException::GetCode() const
{
    return m_nCode;
}
};
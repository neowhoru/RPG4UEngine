#ifndef     __GAMESTRUCT_H__
#define     __GAMESTRUCT_H__


#include "GameConst.h"

//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------
struct sOPTIONPARAM
{
	int			ScreenX,
					ScreenY;
	int			iBPP;
	int			iScreenIndex;
	int			iTextureQuality;
	int			iNormalMapQuality;
	int			iShadowQuality;
	int			LOD;


	BOOL		bBlurState;
	BOOL		bFogState;
	BOOL		bMapSpecular;


	float		fBGMVolume;		
	float		fAEffVolume;
	float		fEffVolume;	
	BOOL		bSystemVoice;
	BOOL		bNPCVoice;

	BOOL        bShowHelm;		
	BOOL        bAutoMove;		
	BOOL        bHelp;		
	int         iDamageOutput;	
	BOOL        bShowPlayerName;
	BOOL			bShowGrade;
	BOOL        bShowNPCName;	

};


//----------------------------------------------------------------------------




//------------------------------------------------------------------------------
#endif      // __GAMESTRUCT_H__

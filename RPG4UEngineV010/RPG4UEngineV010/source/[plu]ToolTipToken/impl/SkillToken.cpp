/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillToken.cpp
创建日期：2009年5月17日
最后更新：2009年5月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillToken.h"
#include "ToolTipManager.h"
#include "SkillSlot.h"
#include "ConstSlot.h"
#include "ConstTextRes.h"
#include "ItemManager.h"
#include "Hero.h"
#include "FormularManager.h"
#include "TextResManager.h"
#include "PlayerAttributes.h"
#include "SeriesInfoParser.h"
#include "ItemAttrInfoParser.h"
#include "ConstItem.h"
#include "SkillInfoParser.h"
#include "SeriesInfoParser.h"
#include "StateInfoParser.h"
#include "ScriptWord.h"

using namespace util;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillName, SkillName)
{
	////////////////////////////////////////
	LPCSTR			szSkillName;
	DWORD				dwTypeID;
	DWORD				dwUserType;

	szSkillName	= (LPCTSTR)pSkillInfo->m_SkillName;
	szTipText	= NULL;

	if(szContent[0] == 0)
		szContent = " %s %s";

	//if( pSkillSlot->GetSet() == 1 )
	//{
	//	sprintf(szExtraInfo,"%s%s",szExtraInfo,_STRING(TEXTRES_DEVINE_NAME) );
	//}

	
	if( pSkillSlot->IsSKill() )
	{
		sSKILLINFO_BASE*	pSkillBase = (sSKILLINFO_BASE*)pSkillInfo;

		dwTypeID		= pSkillBase->m_bySkillType + TEXTRES_SKILL_TYPE_NONE;
		dwColor		= token.dwColor2;
		dwUserType	= pSkillBase->m_bySkillUserType + TEXTRES_SKILL_USER_PLAYER;
	}
	else
	{
		dwColor		= token.dwColor1;
		dwTypeID		= TEXTRES_SKILL_STYLE;
		dwUserType	= 0;
	}


	szTipText	= FMSTR(szContent,szSkillName,_STRING(dwUserType), _STRING(dwTypeID));


	///获得五行图标
	//sSERIES_RELATION*	pInfo;
	//pInfo	= theSeriesInfoParser.GetSeriesInfo((eATTACK_KIND)pSkillInfo->m_bySeries);
	//if(pInfo)
	//	vitemCode = pInfo->m_VSkillCode;

}
END_SKILLTOOLTIPTOKEN(SkillName, SkillName);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillLevel, SkillLevel)
{

	if(szContent[0] == 0)
		szContent = "%d/%d";

	szTipText = FMSTR(szContent,pSkillInfo->m_wSkillLV, pSkillInfo->m_wMaxLV);

	if( pSkillSlot->IsSKill() )
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;

}
END_SKILLTOOLTIPTOKEN(SkillLevel, SkillLevel);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillEqClass, SkillEqClass)
{
	__CHECK(		pSkillInfo->m_dwClassDefine  );


	int		iClassNameCode[PLAYERTYPE_MAX] =
	{
		 TEXTRES_LIMITCLASS_WARRIOR
		,TEXTRES_LIMITCLASS_WARRIOR		
		,TEXTRES_LIMITCLASS_PALADIN		
		,TEXTRES_LIMITCLASS_ASSASSIN		
		,TEXTRES_LIMITCLASS_NECROMANCER	
		,TEXTRES_LIMITCLASS_POWWOW			
	};

	LPCSTR szClassNames[PLAYERTYPE_MAX] = 
	{
		"","","","","",""
	};


	dwColor = token.dwColor2;

	//if( _BIT_INDEX(pSkillInfo->m_wEquipClass,  theHero.GetCharInfo()->m_byClassCode) )
	if( pSkillInfo->m_dwClassDefine ==  theHero.GetCharInfo()->m_byClassCode )
		dwColor = token.dwColor1;//WzNormalColor;

	if(szContent[0] == 0)
		szContent = "%s %s %s %s %s";

	for( UINT i = PLAYERTYPE_BASE ; i < PLAYERTYPE_MAX ; ++i )
	{
		//if( _BIT_INDEX(pSkillInfo->m_wEquipClass,  i) )
		if( pSkillInfo->m_dwClassDefine ==  i )
		{
			szClassNames[i] = _STRING(iClassNameCode[i]);
			break;
		}
	}	//	for( int i = 0 ; i < PLAYERTYPE_MAX ; ++i )

	szTipText = FMSTR	(szContent
							, szClassNames[PLAYERTYPE_WARRIOR		]
							, szClassNames[PLAYERTYPE_PALADIN		]
							, szClassNames[PLAYERTYPE_POWWOW			]
							, szClassNames[PLAYERTYPE_STABBER		]
							, szClassNames[PLAYERTYPE_NECROMANCER	]
	);
}
END_SKILLTOOLTIPTOKEN(SkillEqClass, SkillEqClass);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillLimitLevel, SkillLimitLevel)
{
	LPCSTR		szLevel;
	LEVELTYPE	limitLV;

	const int LIMIT_LEVEL_STRING_INDEX = TEXTRES_ACCESSORY_LIMIT_LEVEL_MSG;

	limitLV	= pSkillInfo->m_wRequireLV;	

	if(szContent[0] == 0)
	{
		szContent	= _T("%s %ld");
		szLevel		= _STRING(LIMIT_LEVEL_STRING_INDEX);
		szTipText	= FMSTR(szContent, szLevel, limitLV);
	}
	else
	{
		szTipText	= FMSTR(szContent, limitLV);
	}


	if ( theHero.GetLevel() >= limitLV ) 
		dwColor = token.dwColor1;
	else
		dwColor = token.dwColor2;

}
END_SKILLTOOLTIPTOKEN(SkillLimitLevel, SkillLimitLevel);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillCoolTime, SkillCoolTime)
{
	__CHECK(pSkillInfo->IsSkill());

	sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;

	//__CHECK(	pSKillBase->m_bySkillUserType < SKILLUSER_ACTION);


	/////////////////////////////////////////////////
	const int	iCoolTime = TEXTRES_SKILL_COOLTIME;
	float			fCoolTime = 0;

	fCoolTime = pSKillBase->m_wCoolTime;

	if( fCoolTime > 0 )
	{
		fCoolTime = (fCoolTime / 1000.0f);
	}

	if(szContent[0] == 0)
		szContent = _STRING(iCoolTime);

	szTipText= FMSTR	(szContent
							,(int)(fCoolTime/60)
							,fmod(fCoolTime,60.f));


}
END_SKILLTOOLTIPTOKEN(SkillCoolTime, SkillCoolTime);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillCastTime, SkillCastTime)
{
	__CHECK(pSkillInfo->IsSkill());

	sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;

	__CHECK(	pSKillBase->m_bySkillUserType < SKILLUSER_ACTION);

	////////////////////////////////////////////
	const int	iCoolTime = TEXTRES_SKILL_COOLTIME;
	float			fCoolTime = 0;

	fCoolTime = pSKillBase->m_wSkillCasting;

	if( fCoolTime > 0 )
	{
		fCoolTime = (fCoolTime / 1000.0f);
	}

	if(szContent[0] == 0)
		szContent = _STRING(iCoolTime);

	szTipText= FMSTR	(szContent
							,(int)(fCoolTime/60)
							,fmod(fCoolTime,60.f));


}
END_SKILLTOOLTIPTOKEN(SkillCastTime, SkillCastTime);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillRequireSkillPT, SkillRequireSkillPT)
{
	if(szContent[0] == 0)
		szContent	= _T("SkillRequireSkillPT:%ld");

	szTipText	= FMSTR(szContent, pSkillInfo->m_byRequireSkillPoint);


	if ( theHero.GetCharInfo()->m_dwRemainSkill >= pSkillInfo->m_byRequireSkillPoint ) 
		dwColor = token.dwColor1;
	else
		dwColor = token.dwColor2;

}
END_SKILLTOOLTIPTOKEN(SkillRequireSkillPT, SkillRequireSkillPT);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillStatType, SkillStatType)
{
	DWORD		dwTextID;
	__CHECK(pSkillInfo->IsSkill());

	sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;

	__CHECK(pSKillBase->m_bySkillStatType > 0);

	if(szContent[0] == 0)
		szContent	= _T("SkillStatType:%ld");
	
	dwTextID		= pSKillBase->m_bySkillStatType + TEXTRES_SKILL_STATE_INNER-1;

	szTipText	= FMSTR(szContent, _STRING(dwTextID));


	//if ( theHero.GetCharInfo()->m_dwRemainSkill >= pSkillInfo->m_byRequireSkillPoint ) 
	//	dwColor = token.dwColor1;
	//else
	//	dwColor = token.dwColor2;

}
END_SKILLTOOLTIPTOKEN(SkillStatType, SkillStatType);




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillRequireStat, SkillRequireStat)
{
	__CHECK(pSkillInfo->IsSkill());

	BOOL					bMatched	(FALSE);
	INT					nCounter	(0);
	StringHandle		arText	[sSKILLINFO_BASE::OVERSTAT_NUM];
	sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;

	///////////////////////////////////////////////////
	for(INT n=0; n<sSKILLINFO_BASE::OVERSTAT_NUM; n++)
	{
		if(pSKillBase->m_wRequireSkillStat[n] == 0)
			continue;
		arText[n].Format	(_T("%s-%d")
								,_STRING(TEXTRES_SKILL_STATE_INNER +n)
								,pSKillBase->m_wRequireSkillStat[n]);
		nCounter++;
	}
	__CHECK(nCounter > 0);


	///////////////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("SkillRequireStat:%s %s");


	///////////////////////////////////////////////
	sPLAYERINFO_BASE*	pHeroInfo = theHero.GetCharInfo();
	if(	pHeroInfo->m_sSkillStat1 >= pSKillBase->m_wRequireSkillStat[0]
		&&	pHeroInfo->m_sSkillStat2 >= pSKillBase->m_wRequireSkillStat[1])
			bMatched = TRUE;

	if(arText[0].IsBlank())
		szTipText	= FMSTR	(szContent
									,(LPCTSTR)arText[1]
									,(LPCTSTR)arText[0]);
	else
		szTipText	= FMSTR	(szContent
									,(LPCTSTR)arText[0]
									,(LPCTSTR)arText[1]);

	///////////////////////////////////////////////
	if ( bMatched ) 
		dwColor = token.dwColor1;
	else
		dwColor = token.dwColor2;

}
END_SKILLTOOLTIPTOKEN(SkillRequireStat, SkillRequireStat);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillRequireWeapon, SkillRequireWeapon)
{
	INT*	arWeapons	=	NULL;
	INT	nNum			= 0;
	BOOL	bMatched		= FALSE;
	DWORD dwWeaponKind;
	INT	nCounter		(0);
	LPCTSTR	arNames[]=
	{
		 _T("")
		,_T("")
		,_T("")
		,_T("")
	};

	////////////////////////////////////////

	////////////////////////////////////////
	if(pSkillSlot->IsSKill())
	{
		sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;
		
		__CHECK(		pSKillBase->m_bySkillUserType != SKILLUSER_ACTION	
					&& pSKillBase->m_bySkillUserType != SKILLUSER_EMOTICON);

		arWeapons	= pSKillBase->m_WeaponDefines;
		nNum			= sSKILLINFO_BASE::WEAPONDEFINE_NUM;
	}
	else
	{
		arWeapons	= &((sSTYLEINFO_BASE*)pSkillInfo)->m_WeaponDefines;
		nNum			= 1;
	}

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("SkillRequireWeapon:%s %s %s %s");



	dwWeaponKind = theHero.GetWeaponKind();
	
	////////////////////////////////////////
	for( int i = 0; i < nNum; ++i )
	{
		if(arWeapons[i] != 0)
			nCounter++;

		///////////////////////////////////////////
		if(	(INT)dwWeaponKind == arWeapons[i] 
			|| arWeapons[i] == -1)
		{
			bMatched |= TRUE;
		}

		///////////////////////////////////////////
		if(arWeapons[i] == -1 )
			arNames[i] = _STRING(TEXTRES_EQUIP_SingleSword);
		else if(arWeapons[i] )
		{
			arNames[i] = _STRING(arWeapons[i] + TEXTRES_EQUIP_SingleSword_0);
		}
	}

	//__CHECK(nCounter > 0);

	if ( bMatched ) 
		dwColor = token.dwColor1;
	else
		dwColor = token.dwColor2;

	szTipText	= FMSTR	(szContent
								,arNames[0] 
								,arNames[1] 
								,arNames[2] 
								,arNames[3] 
								);

}
END_SKILLTOOLTIPTOKEN(SkillRequireWeapon, SkillRequireWeapon);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillSpendHPMP, SkillSpendHPMP)
{
	__CHECK(pSkillInfo->IsSkill());

	sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;

	if(szContent[0] == 0)
		szContent	= _T("SkillSpendHPMP:%d %d");
	
	szTipText	= FMSTR	(szContent
								,pSKillBase->m_wHPSpend
								,pSKillBase->m_wMPSpend);

	if (	theHero.GetHP() >= pSKillBase->m_wHPSpend 
		&& theHero.GetMP() >= pSKillBase->m_wMPSpend ) 
		dwColor = token.dwColor1;
	else
		dwColor = token.dwColor2;

}
END_SKILLTOOLTIPTOKEN(SkillSpendHPMP, SkillSpendHPMP);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillRange, SkillRange)
{
	float fRange(0);
	INT	nRangeType(0);
	float fArea(0);

	if(pSkillInfo->IsSkill())
	{
		sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;
		__CHECK(	pSKillBase->m_bySkillUserType < SKILLUSER_ACTION);

		fRange		= (float)pSKillBase->m_wSkillRange /10.f;
		nRangeType	= pSKillBase->m_byAttRangeform;
		fArea			= (float)pSKillBase->m_wSkillArea/10.f;
	}
	else
	{
		sSTYLEINFO_BASE*	pSKillBase = (sSTYLEINFO_BASE*)pSkillInfo;
		fRange		= (float)pSKillBase->m_fAttRange;
		nRangeType	= pSKillBase->m_byAttRangeform;
		fArea			= (float)pSKillBase->m_fStyleArea;
	}


	if(szContent[0] == 0)
		szContent	= _T("SkillSpendHPMP:%d %d");
	
	szTipText	= FMSTR	(szContent
								,fRange
								,_STRING(nRangeType + TEXTRES_SKILLAREA_FOWARD_ONE - 1)
								,fArea);
}
END_SKILLTOOLTIPTOKEN(SkillRange, SkillRange);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(SkillTarget, SkillTarget)
{
	__CHECK(pSkillInfo->IsSkill());

	INT	nNum(0);

	if(pSkillInfo->IsSkill())
	{
		sSKILLINFO_BASE*	pSKillBase = (sSKILLINFO_BASE*)pSkillInfo;
		__CHECK(	pSKillBase->m_bySkillUserType < SKILLUSER_ACTION);

		nNum	= pSKillBase->m_byMaxTargetNo;
	}
	else
	{
		//sSTYLEINFO_BASE*	pSKillBase = (sSTYLEINFO_BASE*)pSkillInfo;
		//nNum	= pSKillBase->m_wMaxTargetNo;
	}

	__CHECK(nNum > 0);

	if(szContent[0] == 0)
		szContent	= _T("SkillTarget:%d");
	
	szTipText	= FMSTR	(szContent
								,nNum);
}
END_SKILLTOOLTIPTOKEN(SkillTarget, SkillTarget);



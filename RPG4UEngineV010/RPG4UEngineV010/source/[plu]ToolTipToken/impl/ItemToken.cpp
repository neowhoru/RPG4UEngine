/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemToken.cpp
创建日期：2008年7月10日
最后更新：2008年7月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ItemToken.h"
#include "ToolTipManager.h"
#include "ItemSlot.h"
#include "ConstSlot.h"
#include "ConstTextRes.h"
#include "ItemManager.h"
#include "Hero.h"
#include "FormularManager.h"
#include "TextResManager.h"
#include "PlayerAttributes.h"
#include "SeriesInfoParser.h"
#include "ItemAttrInfoParser.h"
#include "ConstItem.h"
#include "DummyItemSlot.h"
#include "ShopDialog.h"
#include "ScriptWord.h"

#pragma warning(disable:4702)
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemName, ItemName)
{
	////////////////////////////////////////
	LPCSTR			szItemName;
	char				szExtraInfo[128]={0};

	szItemName	= (LPCTSTR)pItemInfo->m_ItemName;//m_pToolTipManager->GetItemName(pItemInfo->m_VItemCode);
	szTipText	= NULL;

	if(szContent[0] == 0)
		szContent = " %s %s";

	//if (pItemInfo->IsWeapon() || pItemInfo->IsArmor() )
	{
		//LPCSTR szDevineName = "";
		//LPCSTR szDamageName = "";

		if( pItemSlot->GetSet() == 1 )
		{
			sprintf(szExtraInfo,"%s%s",szExtraInfo,_STRING(TEXTRES_DEVINE_NAME) );
		}

		//if(pItemInfo->m_bySeries)
		//{
		//	LPCSTR szName;
		//	szName = theSeriesInfoParser.GetSeriesName((eATTACK_KIND)pItemInfo->m_bySeries);
		//	if(szName)
		//		sprintf(szExtraInfo,"%s[%s]",szExtraInfo, szName);
		//}

		if( pItemSlot->GetDamageLV() )
		{
			sprintf	(szExtraInfo
						,"%s[%s-%d]"
						,szExtraInfo
						,_STRING(TEXTRES_ITEM_DAMAGED)
						,pItemSlot->GetDamageLV()  );
		}
	}

	szTipText		= FMSTR(szContent,szItemName, szExtraInfo);


	if( pItemSlot->GetSet() == 1 )
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;

	///获得五行图标
	sSERIES_RELATION*	pInfo;
	pInfo	= theSeriesInfoParser.GetSeriesInfo((eATTACK_KIND)pItemInfo->m_bySeries);
	if(pInfo)
		vitemCode = pInfo->m_VItemCode;

}
END_ITEMTOOLTIPTOKEN(ItemName, ItemName);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemUserName, ItemUserName)
{
	////////////////////////////////////////
	LPCSTR			szItemName;

	szItemName	=	(LPCSTR)pItemSlot->GetName();
	if(szItemName == NULL || szItemName[0]==0)
		return TRUE;

	szTipText	= NULL;

	if(szContent[0] == 0)
		szContent = "(%s)";

	szTipText		= FMSTR(szContent,szItemName);


	if( pItemSlot->GetSet() == 1 )
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;

}
END_ITEMTOOLTIPTOKEN(ItemUserName, ItemUserName);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemRankLvl, ItemRankLvl)
{
	__CHECK	(	pItemInfo->IsWeapon() 
				|| pItemInfo->IsArmor() 
				|| pItemInfo->IsAccessary());

	eRANK				itemRank;
	const int		RANK_E_STRING_INDEX = TEXTRES_RANK_F;
	const int		RANK_S_STRING_INDEX = TEXTRES_RANK_PS;
	INT				iStringIndex;

	LPCSTR			szRankName;

	itemRank					= pItemSlot->GetRank();
	iStringIndex			= RANK_E_STRING_INDEX  + (itemRank - RANK_9);

	if (iStringIndex > RANK_S_STRING_INDEX)
		iStringIndex = RANK_S_STRING_INDEX;

	if (iStringIndex < RANK_E_STRING_INDEX)
		iStringIndex = RANK_E_STRING_INDEX;

		szRankName	= theTextResManager.GetString(iStringIndex);
		szTipText	= szRankName;

		//	+ text / text
		if(szContent[0] == 0)
			szContent = "%s+%d";

		BYTE byEnchant = 0;

		if( pItemInfo->IsAccessary() == FALSE )
		{
			byEnchant = pItemSlot->GetEnchant();
		}
		szTipText = FMSTR(szContent,szRankName, byEnchant);

	if( pItemSlot->GetSet() == 1 )
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;

}
END_ITEMTOOLTIPTOKEN(ItemRankLvl, ItemRankLvl);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemSocketNum, ItemSocketNum)
{
	__CHECK	(	pItemInfo->IsWeapon() 
				|| pItemInfo->IsArmor() );

	INT		nSocketNum;
	INT		nUsed;

	nSocketNum		= pItemSlot->GetSocketNum();

	//	+ text / text
	if(szContent[0] == 0)
		szContent = "Num:%d/%d";

	nUsed = 0;
	for( int sock = SOCKET_1 ; sock < pItemSlot->GetSocketNum() ; ++sock )
	{
		const sITEM_ATTR_INFO * pSocketOption = pItemSlot->GetSocketAttrDesc((eSOCKET)sock);
		if ( pSocketOption == NULL )
			continue;
		nUsed++;
	}

	szTipText = FMSTR(szContent,nUsed,nSocketNum);
}
END_ITEMTOOLTIPTOKEN(ItemSocketNum, ItemSocketNum);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemEqClass, ItemEqClass)
{
	__CHECK (pItemInfo->IsWeapon() || pItemInfo->IsArmor());

	__CHECK(		pItemInfo->m_wEquipClass  );



	int		iClassNameCode[PLAYERTYPE_MAX] =
	{
		 TEXTRES_LIMITCLASS_WARRIOR
		,TEXTRES_LIMITCLASS_WARRIOR		
		,TEXTRES_LIMITCLASS_PALADIN		
		,TEXTRES_LIMITCLASS_ASSASSIN		
		,TEXTRES_LIMITCLASS_NECROMANCER	
		,TEXTRES_LIMITCLASS_POWWOW			
	};


	LPCSTR szClassNames[PLAYERTYPE_MAX] = 
	{
		"","","","","",""
	};



	dwColor = token.dwColor2;


	if( _BIT_INDEX(pItemInfo->m_wEquipClass,  theHero.GetCharInfo()->m_byClassCode) )
		dwColor = token.dwColor1;//WzNormalColor;

	if(szContent[0] == 0)
		szContent = "%s %s %s %s %s";

	for( int i = PLAYERTYPE_BASE ; i < PLAYERTYPE_MAX ; ++i )
	{

		if( _BIT_INDEX(pItemInfo->m_wEquipClass,  i) )
		{
			szClassNames[i] = _STRING(iClassNameCode[i]);
		}
	}	//	for( int i = 0 ; i < PLAYERTYPE_MAX ; ++i )

	szTipText = FMSTR	(szContent
							, szClassNames[PLAYERTYPE_WARRIOR		]
							, szClassNames[PLAYERTYPE_PALADIN		]
							, szClassNames[PLAYERTYPE_POWWOW			]
							, szClassNames[PLAYERTYPE_STABBER		]
							, szClassNames[PLAYERTYPE_NECROMANCER	]
	);
}
END_ITEMTOOLTIPTOKEN(ItemEqClass, ItemEqClass);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemLimitLevel, ItemLimitLevel)
{
	LPCSTR		szLevel;
	LEVELTYPE	limitLV;

	const int LIMIT_LEVEL_STRING_INDEX = TEXTRES_ACCESSORY_LIMIT_LEVEL_MSG;

	limitLV	= pItemSlot->GetLimitLevel();	
	//limitLV = theFormularManager.CalcLimitLVAccessory(pItemInfo->m_LimitEqLevel
 //                                          ,(eRANK)pItemSlot->GetRank());

	if(szContent[0] == 0)
	{
		szContent	= _T("%s %ld");
		szLevel		= _STRING(LIMIT_LEVEL_STRING_INDEX);
		szTipText	= FMSTR(szContent, szLevel, limitLV);
	}
	else
	{
		szTipText	= FMSTR(szContent, limitLV);
	}


	if ( theHero.GetLevel() < limitLV ) 
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;

}
END_ITEMTOOLTIPTOKEN(ItemLimitLevel, ItemLimitLevel);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemCoolTime, ItemCoolTime)
{
	__CHECK(pItemInfo->IsPotion());

	const int	iCoolTime = TEXTRES_SKILL_COOLTIME;
	float			fCoolTime = 0;

	if( pItemInfo->m_dCoolTime > 0 )
	{
		fCoolTime = ((float)pItemInfo->m_dCoolTime / 1000.0f);
	}

	if(szContent[0] == 0)
		szContent = _STRING(iCoolTime);

	szTipText= FMSTR	(szContent
							,(int)(fCoolTime/60)
							,fmod(fCoolTime,60.f));


}
END_ITEMTOOLTIPTOKEN(ItemCoolTime, ItemCoolTime);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemAttackSpeed, ItemAttackSpeed)
{
	__CHECK(pItemInfo->IsWeapon() || pItemInfo->IsMaterialWeapon());


	LPCSTR		szAttackSpeed;
	const int	c_iAttackSpeed = TEXTRES_ATTACK_SPEED;
	DWORD			dwAttSpeed;

	dwAttSpeed  = pItemSlot->GetAttackSpeed();

	if(szContent[0] == 0)
	{	
		szAttackSpeed = _STRING(c_iAttackSpeed);
		szContent		= _T("%s : %d");
		szTipText		= FMSTR(szContent, szAttackSpeed,dwAttSpeed);
	}
	else
	{
		szTipText		= FMSTR(szContent, dwAttSpeed);
	}

}
END_ITEMTOOLTIPTOKEN(ItemAttackSpeed, ItemAttackSpeed);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemAttack, ItemAttack)
{
	__CHECK(pItemInfo->IsWeapon()|| pItemInfo->IsMaterialWeapon());

	LPCSTR		szAttack;
	const int PHYATTACK_STRING_INDEX = TEXTRES_PHY_ATTACK;//358;


	DWORD dwPhyMinAttackPower = 0;
	DWORD dwPhyMaxAttackPower = 0;
	DWORD dwPhyMinDamage = pItemInfo->m_wMinDamage;
	DWORD dwPhyMaxDamage = pItemInfo->m_wMaxDamage;
	if (pItemSlot->GetSet() == 1)
	{
		dwPhyMinDamage = theFormularManager.CalcMinAttackPowerForDivineWeapon(dwPhyMinDamage, (eITEM_TYPE)pItemInfo->m_wType, pItemInfo->m_LV );
		dwPhyMaxDamage = theFormularManager.CalcMaxAttackPowerForDivineWeapon(dwPhyMaxDamage, (eITEM_TYPE)pItemInfo->m_wType, pItemInfo->m_LV );
	}

	dwPhyMinAttackPower = theFormularManager.CalcAttackPower(dwPhyMinDamage, pItemSlot->GetEnchant(), pItemInfo->m_LV );
	dwPhyMaxAttackPower = theFormularManager.CalcAttackPower(dwPhyMaxDamage, pItemSlot->GetEnchant(), pItemInfo->m_LV );

	__CHECK (dwPhyMinAttackPower || dwPhyMaxAttackPower);

	if(szContent[0] == 0)
	{	
		szAttack			= _STRING(PHYATTACK_STRING_INDEX);
		szContent		= _T("%s : %ld-%ld");
		szTipText		= FMSTR(szContent,szAttack,dwPhyMinAttackPower,dwPhyMaxAttackPower);			
	}
	else
	{
		szTipText		= FMSTR(szContent, dwPhyMinAttackPower,dwPhyMaxAttackPower);
	}

}
END_ITEMTOOLTIPTOKEN(ItemAttack, ItemAttack);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemMagicAttack, ItemMagicAttack)
{
	__CHECK(pItemInfo->IsWeapon()|| pItemInfo->IsMaterialWeapon());
	LPCSTR		szAttack;
	const int	MAGATTACK_STRING_INDEX = TEXTRES_MAGIC_ATTACK;

	DWORD dwMagicMinAttackPower = pItemSlot->GetMagicAttack(FALSE);
	DWORD dwMagicMaxAttackPower = pItemSlot->GetMagicAttack(TRUE);

	__CHECK(dwMagicMinAttackPower || dwMagicMaxAttackPower);


	if(szContent[0] == 0)
	{	
		szAttack			= _STRING(MAGATTACK_STRING_INDEX);
		szContent		= _T("%s : %ld-%ld");
		szTipText		= FMSTR(szContent,szAttack,dwMagicMinAttackPower,dwMagicMaxAttackPower);			
	}
	else
	{
		szTipText		= FMSTR(szContent, dwMagicMinAttackPower,dwMagicMaxAttackPower);
	}
}
END_ITEMTOOLTIPTOKEN(ItemMagicAttack, ItemMagicAttack);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemMeleeAttack, ItemMeleeAttack)
{
	__CHECK(pItemInfo->IsWeapon()|| pItemInfo->IsMaterialWeapon());
	LPCSTR		szAttack;
	const int	MAGATTACK_STRING_INDEX = TEXTRES_MAGIC_ATTACK;

	DWORD dwMeleeMinAttackPower = pItemSlot->GetMeleeAttack(FALSE);
	DWORD dwMeleeMaxAttackPower = pItemSlot->GetMeleeAttack(TRUE);

	__CHECK(dwMeleeMinAttackPower || dwMeleeMaxAttackPower);


	if(szContent[0] == 0)
	{	
		szAttack			= _STRING(MAGATTACK_STRING_INDEX);
		szContent		= _T("%s : %ld-%ld");
		szTipText		= FMSTR(szContent,szAttack,dwMeleeMinAttackPower,dwMeleeMaxAttackPower);			
	}
	else
	{
		szTipText		= FMSTR(szContent, dwMeleeMinAttackPower,dwMeleeMaxAttackPower);
	}
}
END_ITEMTOOLTIPTOKEN(ItemMeleeAttack, ItemMeleeAttack);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemRangeAttack, ItemRangeAttack)
{
	__CHECK(pItemInfo->IsWeapon()|| pItemInfo->IsMaterialWeapon());
	LPCSTR		szAttack;
	const int	MAGATTACK_STRING_INDEX = TEXTRES_MAGIC_ATTACK;

	DWORD dwRangeMinAttackPower = pItemSlot->GetRangeAttack(FALSE);
	DWORD dwRangeMaxAttackPower = pItemSlot->GetRangeAttack(TRUE);

	__CHECK(dwRangeMinAttackPower || dwRangeMaxAttackPower);


	if(szContent[0] == 0)
	{	
		szAttack			= _STRING(MAGATTACK_STRING_INDEX);
		szContent		= _T("%s : %ld-%ld");
		szTipText		= FMSTR(szContent,szAttack,dwRangeMinAttackPower,dwRangeMaxAttackPower);			
	}
	else
	{
		szTipText		= FMSTR(szContent, dwRangeMinAttackPower,dwRangeMaxAttackPower);
	}
}
END_ITEMTOOLTIPTOKEN(ItemRangeAttack, ItemRangeAttack);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemPhyDefense, ItemPhyDefense)
{
	__CHECK(pItemInfo->IsArmor() || pItemInfo->IsMaterialArmour());

	LPCSTR		szDefense;
	const int	PHY_DEFRATING_STRING_INDEX = TEXTRES_DEFRATING;//364;

	DWORD dwPhyDefense;

	dwPhyDefense = pItemSlot->GetPhyDef();

	__CHECK( dwPhyDefense > 0 );

	if(szContent[0] == 0)
	{	
		szDefense		= _STRING(PHY_DEFRATING_STRING_INDEX);
		szContent		= _T("%s : %ld");
		szTipText		= FMSTR(szContent,szDefense,dwPhyDefense);			
	}
	else
	{
		szTipText		= FMSTR(szContent, dwPhyDefense);
	}

}
END_ITEMTOOLTIPTOKEN(ItemPhyDefense, ItemPhyDefense);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemMagicDefense, ItemMagicDefense)
{
	__CHECK(pItemInfo->IsArmor() || pItemInfo->IsMaterialArmour());

	LPCSTR		szDefense;
	const int	PHY_DEFRATING_STRING_INDEX = TEXTRES_DEFRATING;//364;

	DWORD dwPhyDefense;

	dwPhyDefense = pItemSlot->GetMagicDef();

	__CHECK( dwPhyDefense > 0 );

	if(szContent[0] == 0)
	{	
		szDefense		= _STRING(PHY_DEFRATING_STRING_INDEX);
		szContent		= _T("%s : %ld");
		szTipText		= FMSTR(szContent,szDefense,dwPhyDefense);			
	}
	else
	{
		szTipText		= FMSTR(szContent, dwPhyDefense);
	}

}
END_ITEMTOOLTIPTOKEN(ItemMagicDefense, ItemMagicDefense);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemElementAttack, ItemElementAttack)
{
	__CHECK(pItemInfo->IsWeapon() || pItemInfo->IsMaterialWeapon());

	if(szContent[0] == 0)
		szContent = "s:%c%s";

	INT					nValue		= 0;
	LPCTSTR				szOptionName= NULL;
	eATTACK_KIND		attack;
	sSERIES_RELATION*	pSeriesInfo;


	const eATTRIBUTE_TYPE*	pOptionIndex;
	INT							nIndex;

	pOptionIndex = std::find(gs_ItemOptions
                           ,gs_ItemOptions + MAX_ITEM_OPTIONS
									,ATTRIBUTE_MAGICAL_GOLD_ATTACK_POWER);
	if(pOptionIndex == gs_ItemOptions + MAX_ITEM_OPTIONS)
		nIndex = -1;
	else
		nIndex = pOptionIndex - gs_ItemOptions;

	////////////////////////////////////////////////////////
	for( int kind = 0 ; kind < ELEMENTKIND_MAX ; ++kind )
	{
		nValue	= pItemInfo->m_wElementAttack[kind];
		if ( nValue == 0 )
			continue;

		if(nIndex == -1)
			szOptionName=_T("");
		else
			szOptionName= theItemAttrInfoParser.GetOptionName(kind + nIndex );
		attack			= (eATTACK_KIND)(kind + ATTACKKIND_GOLD);

		pSeriesInfo		= theSeriesInfoParser.GetSeriesInfo(attack);
		if(pSeriesInfo)
			vitemCode	= pSeriesInfo->m_VItemCode;
		else
			vitemCode	= 0;


		szTipText = FMSTR	(szContent
								,szOptionName
								,nValue >= 0 ? '+' : '-'
								,math::Abs(nValue));

		m_pToolTipManager->AddToolTip(szTipText
													 ,-1
													 ,0
													 ,0
													 ,token.dwColor1
													 ,TOOLTIP_BG_COLOR
													 ,0
													 ,vitemCode);//pSocketOption->m_SocketItemCode);

	}
	return TRUE;
}
END_ITEMTOOLTIPTOKEN(ItemSockets, ItemSockets);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemElementDefense, ItemElementDefense)
{
	__CHECK(pItemInfo->IsArmor() || pItemInfo->IsMaterialArmour());

	if(szContent[0] == 0)
		szContent = "%s:%c%s";
	

	INT							nValue		= 0;
	LPCTSTR						szOptionName= NULL;
	eATTACK_KIND				attack;
	sSERIES_RELATION*			pSeriesInfo;
	const eATTRIBUTE_TYPE*	pOptionIndex;
	INT							nIndex;

	pOptionIndex = std::find(gs_ItemOptions
                           ,gs_ItemOptions + MAX_ITEM_OPTIONS
									,ATTRIBUTE_MAGICAL_GOLD_DEFENSE_POWER);
	if(pOptionIndex == gs_ItemOptions + MAX_ITEM_OPTIONS)
		nIndex = -1;
	else
		nIndex = pOptionIndex - gs_ItemOptions;

	////////////////////////////////////////////////////////
	for( int kind = 0 ; kind < ELEMENTKIND_MAX ; ++kind )
	{
		nValue	= pItemInfo->m_wElementDef[kind];
		if ( nValue == 0 )
			continue;

		if(nIndex == -1)
			szOptionName=_T("");
		else
			szOptionName= theItemAttrInfoParser.GetOptionName(kind + nIndex );
		attack			= (eATTACK_KIND)(kind + ATTACKKIND_GOLD);

		pSeriesInfo		= theSeriesInfoParser.GetSeriesInfo(attack);
		if(pSeriesInfo)
			vitemCode	= pSeriesInfo->m_VItemCode;
		else
			vitemCode	= 0;


		szTipText = FMSTR	(szContent
								,szOptionName
								,nValue >= 0 ? '+' : '-'
								,math::Abs(nValue));

		m_pToolTipManager->AddToolTip(szTipText
													 ,-1
													 ,0
													 ,0
													 ,token.dwColor1
													 ,TOOLTIP_BG_COLOR
													 ,0
													 ,vitemCode);//pSocketOption->m_SocketItemCode);

	}
	return TRUE;
}
END_ITEMTOOLTIPTOKEN(ItemElementDefense, ItemElementDefense);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemWasteType, ItemWasteType)
{
	__CHECK	(	pItemInfo->IsCanUseWaste() 
				&& pItemInfo->m_byWasteType > 0);

	LPCSTR		szWasteType;
	float			fTime;
	const int	iWasteType = TEXTRES_WASTE_HPPOTION-1;//70231;

	szWasteType	= _STRING(iWasteType + pItemInfo->m_byWasteType);

	/// 补蓝500[分5次,时间%d分%g秒]
	///补给类型

	if(szContent[0] == 0)
	{	
		szContent		= _T("WasteType:%s %d [%ld,%d%g]");
	}
	fTime		 = (float)pItemInfo->m_wHealHPTime/1000.f;	
	szTipText = FMSTR	(szContent
							, szWasteType
							, pItemInfo->m_wHealHP
							, pItemInfo->m_wTimes
							, (int)(fTime/60.0f)
							, fmod(fTime,60.0f)
							);

}
END_ITEMTOOLTIPTOKEN(ItemWasteType, ItemWasteType);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemDesc, ItemDesc)
{
	//__CHECK	(	pItemInfo);

	LPCTSTR			szDesc;

	szDesc	= m_pToolTipManager->GetItemDesc(pItemInfo->m_VItemCode);

	ScriptWord		words(szDesc, '\n');


	if(szContent[0] == 0)
		szContent		= _T("%s");

	for(INT n=0; n<words.GetWordCount(); n++)
	{
		szTipText = FMSTR	(szContent , (LPCTSTR)words[n]);

		m_pToolTipManager->AddToolTip	(szTipText
												,-1
												,0
												,0
												,token.dwColor1
												,TOOLTIP_BG_COLOR
												,0);
	}
	return FALSE;
}
END_ITEMTOOLTIPTOKEN(ItemDesc, ItemDesc);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemLimitStr, ItemLimitStr)
{
	__CHECK	( pItemInfo->IsCanEquip());


	WORD						wLimitStrWeapon = 0;
	PlayerAttributes *	pCharInfo;

	pCharInfo			= theHero.GetPlayerAttributes();
	wLimitStrWeapon	= (WORD)pItemSlot->GetLimitSTR( pItemInfo->GetFirstAvailableClass() );

	if( 0 == wLimitStrWeapon )
		return TRUE;

	if ( pCharInfo->GetBaseSTR() < wLimitStrWeapon ) 
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;						
	

	if(szContent[0] == 0)
	{	
		LPCSTR szLimitText	= _STRING(TEXTRES_LIMIT_STR);
		szContent				= _T("%s %ld");
		szTipText				= FMSTR(szContent,szLimitText, wLimitStrWeapon );
	}
	else
		szTipText				= FMSTR(szContent, wLimitStrWeapon );
}
END_ITEMTOOLTIPTOKEN(ItemLimitStr, ItemLimitStr);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemLimitDex, ItemLimitDex)
{
	__CHECK	( pItemInfo->IsCanEquip());


	WORD						wLimitDexWeapon = 0;
	PlayerAttributes *	pCharInfo;
	
	pCharInfo			= theHero.GetPlayerAttributes();
	wLimitDexWeapon	= (WORD)pItemSlot->GetLimitDEX(pItemInfo->GetFirstAvailableClass());
	if( 0 ==  wLimitDexWeapon)
		return TRUE;



	if ( pCharInfo->GetBaseDEX() < wLimitDexWeapon ) 
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;						
	

	if(szContent[0] == 0)
	{	
		LPCSTR szLimitText	= _STRING(TEXTRES_LIMIT_DEX);
		szContent				= _T("%s %ld");
		szTipText				= FMSTR(szContent,szLimitText, wLimitDexWeapon );
	}
	else
		szTipText				= FMSTR(szContent, wLimitDexWeapon );
}
END_ITEMTOOLTIPTOKEN(ItemLimitDex, ItemLimitDex);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemLimitInt, ItemLimitInt)
{
	__CHECK	( pItemInfo->IsCanEquip());

	PlayerAttributes *	pCharInfo;
	WORD						wLimitIntWeapon;

	pCharInfo			= theHero.GetPlayerAttributes();
	wLimitIntWeapon	= (WORD)pItemSlot->GetLimitINT(pItemInfo->GetFirstAvailableClass());
	__CHECK(wLimitIntWeapon > 0);



	if ( pCharInfo->GetBaseINT() < wLimitIntWeapon ) 
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;						
	

	if(szContent[0] == 0)
	{	
		LPCSTR szLimitText	= _STRING(TEXTRES_LIMIT_INT);
		szContent				= _T("%s %ld");
		szTipText				= FMSTR(szContent,szLimitText, wLimitIntWeapon );
	}
	else
		szTipText				= FMSTR(szContent, wLimitIntWeapon );
}
END_ITEMTOOLTIPTOKEN(ItemLimitInt, ItemLimitInt);



int GetLimitClass(sITEMINFO_BASE* pItemInfo)
{
	for( int i =  PLAYERTYPE_BASE; i < PLAYERTYPE_MAX ; ++i )
	{
		if( _BIT_INDEX(pItemInfo->m_wEquipClass, i ) )
			return i;
	}
	return -1;

}

BOOL CheckItemLimitSkill	(sITEMINFO_BASE* pItemInfo
                           ,ItemSlot*    pItemSlot
									,WORD           wLimitSkill
									,LPCSTR&        szContent
									,LPCSTR&        szTipText
									,DWORD&         dwColor
									,const sTOOLTIP_TOKEN& token)
{
	LPCSTR		szLimitText;

	const int	c_iNumLimitSkill		= 2;
	const int	c_iLimitSkillNCode	= TEXTRES_SKILL_ACTIVE1_WARRIOR;
	int			iLimitSkill;
	int			iLimitEqClass;

	PlayerAttributes *	pCharInfo = theHero.GetPlayerAttributes();
	//sPLAYERINFO_BASE *		pPlayInfo = theHero.GetCharInfo();

	iLimitEqClass		= GetLimitClass(pItemInfo);
	WORD	wLimit		= theFormularManager.CalcLimitStatWeapon(wLimitSkill
                                           ,pItemSlot->GetEnchant()
														 ,pItemSlot->GetRank()
														 ,pItemSlot->GetItemInfo()->m_LV);

	iLimitSkill = c_iLimitSkillNCode + (iLimitEqClass * c_iNumLimitSkill);

	szLimitText		= _STRING(iLimitSkill);
	if(szContent[0] == 0)
		szContent		= _T("%s %ld");
	szTipText		= FMSTR(szContent,szLimitText, wLimit );
	//}
	//else
	//	szTipText				= FMSTR(szContent, wLimit );

	if(	theHero.GetClass() != (iLimitEqClass + 1) 
		|| pCharInfo->GetBaseExperty1() < wLimit )
	{
		dwColor = token.dwColor2;
	}
	else
	{
		dwColor = token.dwColor1;
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemLimitSkill1, ItemLimitSkill1)
{
	__CHECK	( pItemInfo->m_wLimitSkil1);
	__CHECK	( pItemInfo->IsWeapon() || pItemInfo->IsArmor());

	CheckItemLimitSkill(pItemInfo
                      ,pItemSlot
							 ,pItemInfo->m_wLimitSkil1
							 ,szContent
							 ,szTipText
							 ,dwColor
							 ,token);

}
END_ITEMTOOLTIPTOKEN(ItemLimitSkill1, ItemLimitSkill1);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemLimitSkill2, ItemLimitSkill2)
{
	__CHECK	( pItemInfo->m_wLimitSkil2);
	__CHECK	( pItemInfo->IsWeapon() || pItemInfo->IsArmor());

	CheckItemLimitSkill(pItemInfo
                      ,pItemSlot
							 ,pItemInfo->m_wLimitSkil2
							 ,szContent
							 ,szTipText
							 ,dwColor
							 ,token);

}
END_ITEMTOOLTIPTOKEN(ItemLimitSkill2, ItemLimitSkill2);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemOptions, ItemOptions)
{
	__CHECK	( !pItemInfo->IsSocket());
	//__CHECK	( pItemInfo->IsWeapon() || pItemInfo->IsArmor());

	LPCSTR szOptionName;
	if(szContent[0] == 0)
		szContent = _T("%s +%d");

	//if( pItemInfo->m_wOptionKind1 > 0 )
	for(INT n=0; n<ITEM_OPTION_MAX; n++)
	{
		WORD  wKind = pItemInfo->m_arOptions[n].m_wOptionKind;
		if(wKind  == 0 )
			continue;

		//szOptionName = _STRING( TEXTRES_EXTRA_NONE +  wKind  );
		szOptionName	= theItemAttrInfoParser.GetOptionName(wKind);
		szTipText	= FMSTR(szContent
						, szOptionName
						, pItemInfo->m_arOptions[n].m_wOption );

		m_pToolTipManager->AddToolTip(szTipText
                                        ,-1
													 ,0
													 ,0
													 ,dwColor
													 ,TOOLTIP_BG_COLOR
													 ,0);
	}
	return TRUE;

}
END_ITEMTOOLTIPTOKEN(ItemOptions, ItemOptions);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemRanks, ItemRanks)
{
	//const int iRankNameStringID = TEXTRES_RANK_F;

	//BYTE 			byIndex = 0;
	//eATTRIBUTE_TYPE	attr;
	//LPCSTR		szRankName;


	//if (RANK_9 != pItemSlot->GetRank() )
	//{
	//	szRankName = _STRING(TEXTRES_RANK_F + pItemSlot->GetRank());
	//	m_pToolTipManager->AddToolTip(szRankName,
	//		-1,
	//		0,
	//		0,
	//		token.dwColor1,
	//		TOOLTIP_BG_COLOR,
	//		0);
	//}

	if(szContent[0] == 0)
		szContent = "%s (+%d%s)";
	for ( BYTE rank = (BYTE)RANK_8; rank <= pItemSlot->GetRank(); ++rank)
	{
		INT nValue = pItemSlot->GetRankAttrValue((eRANK)rank);

		const sRANK_OPTION * pRankOption = pItemSlot->GetRankAttrDesc( (eRANK)rank );
		if (pRankOption)
		{
			LPCSTR szPercent = "";

			if( TYPE_NUMERIC != pRankOption->m_NumericType )
				 szPercent = "%";
			szTipText = FMSTR( szContent, (LPCSTR)pRankOption->m_sOptionName, nValue,szPercent);

			m_pToolTipManager->AddToolTip(szTipText
                                           ,-1
														 ,0
														 ,0
														 ,token.dwColor1
														 ,TOOLTIP_BG_COLOR
														 ,0);
		}
	}
	return TRUE;

}
END_ITEMTOOLTIPTOKEN(ItemRanks, ItemRanks);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemPotentials, ItemPotentials)
{
	//BYTE 			byIndex = 0;
	//eATTRIBUTE_TYPE	attr;
	//LPCSTR		szRankName;

	//if (POTENTIAL_0 != pItemSlot->GetPotential() )
	//{
	//	szRankName = _STRING(TEXTRES_RANK_F + pItemSlot->GetRank());
	//	m_pToolTipManager->AddToolTip(szRankName,
	//		-1,
	//		0,
	//		0,
	//		token.dwColor1,
	//		TOOLTIP_BG_COLOR,
	//		0);
	//}

	if(szContent[0] == 0)
		szContent = "%s (+%d%s)";
	for ( BYTE poten = (BYTE)POTENTIAL_1; poten <= pItemSlot->GetPotential(); ++poten)
	{
		INT nValue = pItemSlot->GetPotentialAttrValue((ePOTENTIAL)poten);

		const sRANK_OPTION * pPotentialOption = pItemSlot->GetPotentialAttrDesc( (ePOTENTIAL)poten );
		if (pPotentialOption)
		{
			LPCSTR szPercent = "";

			if( TYPE_NUMERIC != pPotentialOption->m_NumericType )
				 szPercent = "%";
			szTipText = FMSTR( szContent, (LPCSTR)pPotentialOption->m_sOptionName, nValue,szPercent);

			if(poten-POTENTIAL_1 < pItemSlot->GetPotentialUsed() )
				dwColor = token.dwColor1;
			else
				dwColor = token.dwColor2;

			m_pToolTipManager->AddToolTip(szTipText
                                           ,-1
														 ,0
														 ,0
														 ,dwColor
														 ,TOOLTIP_BG_COLOR
														 ,0);
		}
	}
	return TRUE;

}
END_ITEMTOOLTIPTOKEN(ItemPotentials, ItemPotentials);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemSockets, ItemSockets)
{
	__CHECK(pItemInfo->IsWeapon() || pItemInfo->IsArmor() || pItemInfo->IsSocket() );

	if(szContent[0] == 0)
		szContent = "%d#[%s %s%s]";

	LPCSTR	szOptionName;
	INT		nValue		= 0;
	INT		nValue2		= 0;
	INT		nValueType	= 0;
	INT		nAttrIndex	= 0;

	//注册潜隐信息
	if(pItemInfo->m_byMaterialType == ITEMSOCKETTYPE_POTENTIAL)
	{
		//TEXTRES_ITEM_POTENTIAL									=	1011	,//	潜隐石

		szTipText = FMSTR	("[%s]"
								,(LPCSTR) _STRING(TEXTRES_ITEM_POTENTIAL));

		m_pToolTipManager->AddToolTip(szTipText
													 ,-1
													 ,0
													 ,0
													 ,token.dwColor1
													 ,TOOLTIP_BG_COLOR
													 ,0
													 ,pItemInfo->m_VItemCode);//m_Code);
	}


	for( int sock = SOCKET_1 ; sock < pItemSlot->GetSocketNum() ; ++sock )
	{
		const sITEM_ATTR_INFO * pSocketOption = pItemSlot->GetSocketAttrDesc((eSOCKET)sock);
		//if( pItemSlot->GetSocketAttrIndex((eSOCKET)sock,eSOCKET_VALUE_1) == 0 )
		if ( pSocketOption == NULL )
			continue;

		INT	nAttrNum;
		BOOL	bMinMax;
		nAttrNum	= pItemSlot->GetSocketAttrNum ((eSOCKET)sock);

		for(INT val=eSOCKET_VALUE_1; val < nAttrNum; val++)
		{
			char	szValue[126];
			LPCSTR szPercent = "";

			/// 进行孖属性做攻防匹配
			if(!pItemSlot->CanUseSocketAttr((eSOCKET)sock, (eSOCKET_VALUE)val ))
				continue;

			nAttrIndex	= pItemSlot->GetSocketAttrIndex	((eSOCKET)sock, (eSOCKET_VALUE)val );
			if(nAttrIndex == 0)
				continue;


			bMinMax		= pItemSlot->IsMinMaxAttr			((eSOCKET)sock);
			nValue		= pItemSlot->GetSocketAttrValue	((eSOCKET)sock, (eSOCKET_VALUE)val );
			nValueType	= pItemSlot->GetSocketValueType	((eSOCKET)sock, (eSOCKET_VALUE)val );

			if( TYPE_NUMERIC != nValueType )
				 szPercent = "%";

			//szOptionName = _STRING( TEXTRES_EXTRA_NONE +  nAttrIndex  );
			szOptionName	= theItemAttrInfoParser.GetOptionName(nAttrIndex);


			if(val == eSOCKET_VALUE_1 && bMinMax)
			{
				nValue2		= pItemSlot->GetSocketAttrValue	((eSOCKET)sock, eSOCKET_VALUE_3 );
				sprintf(szValue,"+%d ~ +%d",nValue, nValue2);
			}
			else
			{
				sprintf(szValue,"+%d",nValue);
			}

			szTipText = FMSTR	(szContent
									,sock
									,(LPCSTR)szOptionName
									,szValue
									,szPercent);

			sITEMINFO_BASE*		pItem;
			CODETYPE				vitemCode(0);
			pItem = theItemInfoParser.GetItemInfo(pSocketOption->m_SocketItemCode);
			if(pItem)
				vitemCode	= pItem->m_VItemCode;
			m_pToolTipManager->AddToolTip(szTipText
														 ,-1
														 ,0
														 ,0
														 ,token.dwColor1
														 ,TOOLTIP_BG_COLOR
														 ,0
														 ,vitemCode);//pSocketOption->m_SocketItemCode);

		}//for(INT val=eSOCKET_VALUE_1; val < eSOCKET_VALUE_MAX; val++)
	}
	return TRUE;
}
END_ITEMTOOLTIPTOKEN(ItemSockets, ItemSockets);


/*////////////////////////////////////////////////////////////////////////
//		 重叠数量
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemOverlap, ItemOverlap)
{
	__CHECK (pItemSlot->IsOverlap());

	if(szContent[0] == 0)
	{
		szContent = "%s : %d";
		LPCSTR szText = _STRING(TEXTRES_ITEMCOUNT);
		szTipText = FMSTR(szContent, szText, pSlot->GetNum() );
	}
	else
		szTipText = FMSTR(szContent, pSlot->GetNum() );
}
END_ITEMTOOLTIPTOKEN(ItemOverlap, ItemOverlap);


/*////////////////////////////////////////////////////////////////////////
//		 耐久
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemDura, ItemDura)
{
	__CHECK( pItemInfo->IsArmor() || pItemInfo->IsWeapon() );

	BYTE	byMaxDura	= pItemSlot->GetMaxDura();
	float	CurrentDura = pItemSlot->GetfDura();
	if( CurrentDura > byMaxDura )
		CurrentDura = byMaxDura;

	if(szContent[0] == 0)
	{
		szContent = "%s : %.0f/%d ";
		LPCSTR szText = _STRING(TEXTRES_DURABILITY);
		szTipText = FMSTR(szContent, szText, CurrentDura, byMaxDura );
	}
	else
		szTipText = FMSTR(szContent, CurrentDura, byMaxDura );

	if( pItemSlot->GetfDura() == 0.f )
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;
}
END_ITEMTOOLTIPTOKEN(ItemDura, ItemDura);


/*////////////////////////////////////////////////////////////////////////
//		 Buy价格
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemBuyPrice, ItemBuyPrice)
{
	//__CHECK( pItemInfo->IsArmor() || pItemInfo->IsWeapon() );
	__CHECK(pItemSlot->IsShopItem());

	MONEY PriceOfGoods;
	PriceOfGoods = pItemSlot->GetPriceForBuy();

	if(szContent[0] == 0)
		szContent = _STRING(TEXTRES_ITEM_BUY_PRICE);

	szTipText = FMSTR(szContent,  PriceOfGoods );

	if(theHero.GetMoney() < PriceOfGoods)
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;
}
END_ITEMTOOLTIPTOKEN(ItemBuyPrice, ItemBuyPrice);

/*////////////////////////////////////////////////////////////////////////
//		 Repair价格
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemRepairPrice, ItemRepairPrice)
{
	__CHECK( theShopDialog.IsStarted() );

	__CHECK( pItemInfo->IsArmor() || pItemInfo->IsWeapon() );

	MONEY PriceOfGoods;
	PriceOfGoods = pItemSlot->GetPriceForRepair();

	if(szContent[0] == 0)
		szContent = _STRING(TEXTRES_ITEM_REPAIR_PRICE);

	szTipText = FMSTR(szContent,  PriceOfGoods );

	if(theHero.GetMoney() < PriceOfGoods)
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;
}
END_ITEMTOOLTIPTOKEN(ItemRepairPrice, ItemRepairPrice);

/*////////////////////////////////////////////////////////////////////////
//		 Sell价格
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemSellPrice, ItemSellPrice)
{
	//__CHECK( pItemInfo->IsArmor() || pItemInfo->IsWeapon() );
	__CHECK( theShopDialog.IsStarted() );

	MONEY PriceOfGoods;
	PriceOfGoods = pItemSlot->GetPriceForSell();

	if(szContent[0] == 0)
		szContent = _STRING(TEXTRES_ITEM_SELL_PRICE);

	szTipText = FMSTR(szContent,  PriceOfGoods );

	//if(theHero.GetMoney() < PriceOfGoods)
	//	dwColor = token.dwColor2;
	//else
	//	dwColor = token.dwColor1;
}
END_ITEMTOOLTIPTOKEN(ItemSellPrice, ItemSellPrice);

/*////////////////////////////////////////////////////////////////////////
//		 Sell价格
/*////////////////////////////////////////////////////////////////////////
BEGIN_ITEMTOOLTIPTOKEN(ItemVendorPrice, ItemVendorPrice)
{
	__CHECK(pItemSlot->GetSlotType() == ST_ITEMHANDLE);

	DummyItemSlot*	pVendorSlot = (DummyItemSlot*)pItemSlot;


	MONEY		PriceOfGoods;
	PriceOfGoods = pVendorSlot->GetExtraData();

	if(szContent[0] == 0)
		szContent = _STRING(TEXTRES_ITEM_SELL_PRICE);

	if(PriceOfGoods == 0)
		szTipText = _STRING(TEXTRES_VENDOR_JUST_SHOW_TOOLTIP);
	else
		szTipText = FMSTR(szContent,  PriceOfGoods );

	if(theHero.GetMoney() < PriceOfGoods || PriceOfGoods == 0)
		dwColor = token.dwColor2;
	else
		dwColor = token.dwColor1;
}
END_ITEMTOOLTIPTOKEN(ItemVendorPrice, ItemVendorPrice);


/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillToken.cpp
创建日期：2009年5月17日
最后更新：2009年5月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillToken.h"
#include "ToolTipManager.h"
#include "SkillSlot.h"
#include "ConstSlot.h"
#include "ConstTextRes.h"
#include "ItemManager.h"
#include "Hero.h"
#include "FormularManager.h"
#include "TextResManager.h"
#include "PlayerAttributes.h"
#include "SeriesInfoParser.h"
#include "ItemAttrInfoParser.h"
#include "ConstItem.h"
#include "SkillInfoParser.h"
#include "SeriesInfoParser.h"
#include "StateInfoParser.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleAttack, StyleAttack)
{
	__CHECK(pSkillSlot->IsStyle());

	BOOL					bMatched	(FALSE);
	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("攻击  增加：1#+%d 2#+%d 3#+%d");

	
	////////////////////////////////////////
	for( int i = 0; i < ATTACK_SEQUENCE_MAX; ++i )
	{
		///////////////////////////////////////////
		if( pStyleInfo->m_iAddDamage[i] != 0)
		{
			bMatched |= TRUE;
		}
	}
	__CHECK(bMatched);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_iAddDamage[0] 
								,pStyleInfo->m_iAddDamage[1] 
								,pStyleInfo->m_iAddDamage[2] 
								);

}
END_SKILLTOOLTIPTOKEN(StyleAttack, StyleAttack);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleDamaged, StyleDamaged)
{
	__CHECK(pSkillSlot->IsStyle());

	BOOL					bMatched	(FALSE);
	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("伤害  增加：1#+%2.1f%% 2#+%2.1f%% 3#+%2.1f%%");

	
	////////////////////////////////////////
	for( int i = 0; i < ATTACK_SEQUENCE_MAX; ++i )
	{
		///////////////////////////////////////////
		if( pStyleInfo->m_fDamagePercent[i] != 0)
		{
			bMatched |= TRUE;
		}
	}
	__CHECK(bMatched);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_fDamagePercent[0]  * 100.f
								,pStyleInfo->m_fDamagePercent[1]  * 100.f
								,pStyleInfo->m_fDamagePercent[2]  * 100.f
								);

}
END_SKILLTOOLTIPTOKEN(StyleDamaged, StyleDamaged);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define STYLE_TOKEN_IMPL(Name,FIELD,FORMAT, OVERMAX)\
	BEGIN_SKILLTOOLTIPTOKEN(Style##Name, Style##Name)\
	{\
		__CHECK(pSkillSlot->IsStyle());\
	\
		sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;\
	\
		if(szContent[0] == 0)\
			szContent	= _T(FORMAT);\
	\
		__CHECK(pStyleInfo->FIELD > 0);\
	\
		szTipText	= FMSTR	(szContent\
									,pStyleInfo->FIELD * OVERMAX);\
	}\
	END_SKILLTOOLTIPTOKEN(Style##Name, Style##Name);



STYLE_TOKEN_IMPL(SucceedRatio		,m_iAttackRate		,"命中：+%2d%%", 1);
STYLE_TOKEN_IMPL(AvoidRatio		,m_iAvoidRate		,"闪避：+%2d%%", 1);
STYLE_TOKEN_IMPL(AttackSpeed		,m_iAttackSpeed	,"：%dms"		, 1);
STYLE_TOKEN_IMPL(BonusDefence		,m_iBonusDefence	,"：%dms"		, 1);
STYLE_TOKEN_IMPL(CriticalBonus	,m_wCriticalBonus	,"：+%2d%%"		, 1);
STYLE_TOKEN_IMPL(DefenseIgnore	,m_fDefenseIgnore	,"：+%2d%%"		, 1);
STYLE_TOKEN_IMPL(Down				,m_fDownRate	,"：+%2.1f%%"		, 100);


///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
//BEGIN_SKILLTOOLTIPTOKEN(StyleSucceedRatio, StyleSucceedRatio)
//{
//	__CHECK(pSkillSlot->IsStyle());
//
//	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;
//
//	////////////////////////////////////////
//	if(szContent[0] == 0)
//		szContent	= _T("命  中  率：+%2d%%");
//
//	
//	__CHECK(pStyleInfo->m_iAttackRate > 0);
//
//	///////////////////////////////////////////
//
//	szTipText	= FMSTR	(szContent
//								,pStyleInfo->m_iAttackRate);
//
//}
//END_SKILLTOOLTIPTOKEN(StyleSucceedRatio, StyleSucceedRatio);
//
//

///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StylePierce, StylePierce)
{
	__CHECK(pSkillSlot->IsStyle());

	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("：Rate %2.1f%% Range=%2.1f ");

	
	__CHECK(		pStyleInfo->m_fPierceRate != 0 
				&& pStyleInfo->m_fPierceRange > 0);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_fPierceRate * 100.f
								,pStyleInfo->m_fPierceRange);

}
END_SKILLTOOLTIPTOKEN(StylePierce, StylePierce);


///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleStun, StyleStun)
{
	__CHECK(pSkillSlot->IsStyle());

	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("：Rate %2.1f%% 时间=%dms ");

	
	__CHECK(		pStyleInfo->m_fStunRate != 0 
				&& pStyleInfo->m_wStunTime > 0);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_fStunRate* 100.f
								,pStyleInfo->m_wStunTime);

}
END_SKILLTOOLTIPTOKEN(StyleStun, StyleStun);



///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleKnockBack, StyleKnockBack)
{
	__CHECK(pSkillSlot->IsStyle());

	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("：Rate %2.1f%% Range=%2.1f ");

	
	__CHECK(		pStyleInfo->m_fKnockBackRate  != 0 
				&& pStyleInfo->m_fKnockBackRange > 0);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_fKnockBackRate * 100.f 
								,pStyleInfo->m_fKnockBackRange );

}
END_SKILLTOOLTIPTOKEN(StyleKnockBack, StyleKnockBack);

///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleDelay, StyleDelay)
{
	__CHECK(pSkillSlot->IsStyle());

	//sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	//if(szContent[0] == 0)
	//	szContent	= _T("：Rate %2.1f%% Range=%2.1f ");

	
	//__CHECK(		pStyleInfo->m_fDelayReduce != 0 
	//			&& pStyleInfo->m_fDelayOccur  != 0);

	///////////////////////////////////////////

	//szTipText	= FMSTR	(szContent
	//							,pStyleInfo->m_fDelayOccur * 100.f 
	//							,pStyleInfo->m_fDelayReduce  * 100.f);

}
END_SKILLTOOLTIPTOKEN(StyleDelay, StyleDelay);

///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleHPAbsorb, StyleHPAbsorb)
{
	__CHECK(pSkillSlot->IsStyle());

	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("：%d Rate %2.1f%% ");

	
	__CHECK(		pStyleInfo->m_wHPAbsorb	 != 0 
				|| pStyleInfo->m_fHPAbsorbPer != 0);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_wHPAbsorb
								,pStyleInfo->m_fHPAbsorbPer  * 100.f);

}
END_SKILLTOOLTIPTOKEN(StyleHPAbsorb, StyleHPAbsorb);


///*////////////////////////////////////////////////////////////////////////
///*////////////////////////////////////////////////////////////////////////
BEGIN_SKILLTOOLTIPTOKEN(StyleMPAbsorb, StyleMPAbsorb)
{
	__CHECK(pSkillSlot->IsStyle());

	sSTYLEINFO_BASE*	pStyleInfo = (sSTYLEINFO_BASE*)pSkillInfo;

	////////////////////////////////////////
	if(szContent[0] == 0)
		szContent	= _T("：%d Rate %2.1f%% ");

	
	__CHECK(		pStyleInfo->m_wMPAbsorb	 != 0 
				|| pStyleInfo->m_fMPAbsorbPer != 0);

	///////////////////////////////////////////

	szTipText	= FMSTR	(szContent
								,pStyleInfo->m_wMPAbsorb
								,pStyleInfo->m_fMPAbsorbPer  * 100.f);

}
END_SKILLTOOLTIPTOKEN(StyleMPAbsorb, StyleMPAbsorb);



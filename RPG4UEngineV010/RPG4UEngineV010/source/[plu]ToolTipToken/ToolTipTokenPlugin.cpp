// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "LogSystem.h"
#include "GameUIManager.h"
#include "ItemToken.h"
#include "ToolTipManager.h"
#include "SkillToken.h"




#define REG_TOOLTIPTOKEN(Name)\
	static ToolTip##Name##Token s##Name;\
	theToolTipManager.RegisterToken(&s##Name);

/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_TOOLTIPTOKEN_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{

	//////////////////////////////////////////////////
	REG_TOOLTIPTOKEN(ItemName);
	REG_TOOLTIPTOKEN(ItemUserName);
	REG_TOOLTIPTOKEN(ItemRankLvl);
	REG_TOOLTIPTOKEN(ItemSocketNum);
	REG_TOOLTIPTOKEN(ItemEqClass);
	REG_TOOLTIPTOKEN(ItemLimitLevel);
	REG_TOOLTIPTOKEN(ItemCoolTime);
	REG_TOOLTIPTOKEN(ItemAttackSpeed);
	REG_TOOLTIPTOKEN(ItemAttack);
	REG_TOOLTIPTOKEN(ItemMeleeAttack);
	REG_TOOLTIPTOKEN(ItemRangeAttack);
	REG_TOOLTIPTOKEN(ItemMagicAttack);
	REG_TOOLTIPTOKEN(ItemPhyDefense);
	REG_TOOLTIPTOKEN(ItemMagicDefense);
	REG_TOOLTIPTOKEN(ItemElementAttack );
	REG_TOOLTIPTOKEN(ItemElementDefense);
	REG_TOOLTIPTOKEN(ItemWasteType);
	REG_TOOLTIPTOKEN(ItemDesc);
	REG_TOOLTIPTOKEN(ItemLimitStr);
	REG_TOOLTIPTOKEN(ItemLimitDex);
	REG_TOOLTIPTOKEN(ItemLimitInt);
	REG_TOOLTIPTOKEN(ItemLimitSkill1);
	REG_TOOLTIPTOKEN(ItemLimitSkill2);
	REG_TOOLTIPTOKEN(ItemOptions);
	REG_TOOLTIPTOKEN(ItemRanks);
	REG_TOOLTIPTOKEN(ItemPotentials);
	REG_TOOLTIPTOKEN(ItemSockets);
	REG_TOOLTIPTOKEN(ItemOverlap);
	REG_TOOLTIPTOKEN(ItemDura);
	REG_TOOLTIPTOKEN(ItemBuyPrice);
	REG_TOOLTIPTOKEN(ItemSellPrice);
	REG_TOOLTIPTOKEN(ItemRepairPrice);
	REG_TOOLTIPTOKEN(ItemVendorPrice);

	//////////////////////////////////////////////////
	REG_TOOLTIPTOKEN(SkillName			);
	REG_TOOLTIPTOKEN(SkillLevel		);
	REG_TOOLTIPTOKEN(SkillEqClass		);
	REG_TOOLTIPTOKEN(SkillLimitLevel	);
	REG_TOOLTIPTOKEN(SkillCoolTime	);
	REG_TOOLTIPTOKEN(SkillCastTime	);
	REG_TOOLTIPTOKEN(SkillRequireSkillPT	);
	REG_TOOLTIPTOKEN(SkillStatType		);
	REG_TOOLTIPTOKEN(SkillRequireStat	);
	REG_TOOLTIPTOKEN(SkillRequireWeapon	);
	REG_TOOLTIPTOKEN(SkillSpendHPMP		);
	REG_TOOLTIPTOKEN(SkillRange			);
	REG_TOOLTIPTOKEN(SkillTarget			);
	REG_TOOLTIPTOKEN(SkillAbilityTitle	);
	REG_TOOLTIPTOKEN(SkillAbility			);

	REG_TOOLTIPTOKEN(StyleAttack			);
	REG_TOOLTIPTOKEN(StyleDamaged			);
	REG_TOOLTIPTOKEN(StyleSucceedRatio	);
	REG_TOOLTIPTOKEN(StyleAvoidRatio		);
	REG_TOOLTIPTOKEN(StyleAttackSpeed	);
	REG_TOOLTIPTOKEN(StyleBonusDefence	);
	REG_TOOLTIPTOKEN(StyleCriticalBonus	);
	REG_TOOLTIPTOKEN(StyleDefenseIgnore	);
	REG_TOOLTIPTOKEN(StylePierce			);
	REG_TOOLTIPTOKEN(StyleStun				);
	REG_TOOLTIPTOKEN(StyleKnockBack		);
	REG_TOOLTIPTOKEN(StyleDown				);
	REG_TOOLTIPTOKEN(StyleDelay			);
	REG_TOOLTIPTOKEN(StyleHPAbsorb		);
	REG_TOOLTIPTOKEN(StyleMPAbsorb		);


	LOGINFO("Plugin ToolTipToken insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_TOOLTIPTOKEN_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	return gamemain::ePluginOK;
}

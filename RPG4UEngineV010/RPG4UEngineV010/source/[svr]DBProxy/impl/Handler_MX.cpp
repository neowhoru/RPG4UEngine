#include "stdafx.h"
#include "Handler_MX.h"
#include <Macro.h>
#include "ServerSession.h"


#include <Protocol_MX.h>
#include <PacketStruct_MX.h>

#include <Protocol_DM.h>
#include <PacketStruct_DM.h>
#include "DBProxyServer.h"

Handler_MX::Handler_MX()
{
}

Handler_MX::~Handler_MX()
{
}

VOID Handler_MX::OnDM_CONNECTION_SERVER_INFO_CMD( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize )
{
	MSG_DM_CONNECTION_SERVER_INFO_CMD * pRecvMsg = (MSG_DM_CONNECTION_SERVER_INFO_CMD *)pMsg;

	g_DBProxyServer.SetServerGUID( pRecvMsg->m_dwServerGUID );
	g_DBProxyServer.StartServerListen( pRecvMsg->m_szDBProxyServerIP, pRecvMsg->m_wDBProxyServerPort );
}

VOID Handler_MX::OnMX_CONNECTION_REQ_SERVER_INFO_SYN( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize )
{
	ExecuteServerInfoQuery( pServer, 1 );
}

VOID Handler_MX::ExecuteServerInfoQuery( ServerSession * pServer, BYTE Page )
{	
	TCHAR szQueryBuff[64];
	_sntprintf( szQueryBuff, 64, "S_ServerInfoList %u, %u", Page, ServerInfoQuery::SERVERINFO_ROW_NUM );

	ServerInfoQuery * pQuery = QUERYALLOC(ServerInfoQuery);
	ZeroMemory( pQuery->pServerInfo, sizeof(ServerInfoQuery::tag_SERVERINFO)*ServerInfoQuery::SERVERINFO_ROW_NUM );
	pQuery->SetIndex( MAKEDWORD( (WORD)MX_CONNECTION, (WORD)MX_CONNECTION_REQ_SERVER_INFO_DBR ) );
	pQuery->SetVoidObject( pServer );
	pQuery->SetQuery( szQueryBuff );
	pQuery->SetCurPage(Page);
	pServer->DBQuery( pQuery );
}

VOID Handler_MX::OnMX_CONNECTION_REQ_SERVER_INFO_DBR( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize )
{
	MSG_DBPROXY_RESULT * pResult	= (MSG_DBPROXY_RESULT *)pMsg;

	ServerInfoQuery * pQResult		= (ServerInfoQuery *)pResult->m_pData;

	if( pQResult->GetResultRowNum() != 0 )
	{
		ASSERT( MSG_MX_CONNECTION_REQ_SERVER_INFO_ACK::_MAX_SERVERINFO_ARRAY_NUM != ServerInfoQuery::SERVERINFO_ROW_NUM );
		ASSERT( pQResult->GetResultRowNum() < MSG_MX_CONNECTION_REQ_SERVER_INFO_ACK::_MAX_SERVERINFO_ARRAY_NUM );

		MSG_MX_CONNECTION_REQ_SERVER_INFO_ACK msg;
		msg.m_byCategory			= MX_CONNECTION;
		msg.m_byProtocol			= MX_CONNECTION_REQ_SERVER_INFO_ACK;
		msg.m_dwNumberOfServerInfo  = pQResult->GetResultRowNum();
		for( DWORD i = 0 ; i < msg.m_dwNumberOfServerInfo ; ++i )
		{
			msg.m_ServerInfo[i].ServerKey.Set( pQResult->pServerInfo[i].byWorldID,
				pQResult->pServerInfo[i].byChannelID,
				pQResult->pServerInfo[i].byServerType,
				pQResult->pServerInfo[i].byServerID );
			msg.m_ServerInfo[i].dwServerGUID			= pQResult->pServerInfo[i].dwServerGUID;
			msg.m_ServerInfo[i].wPort					= (WORD)pQResult->pServerInfo[i].iPort;
			msg.m_ServerInfo[i].wInnerPort				= (WORD)pQResult->pServerInfo[i].iInnerPort;
			strncpy( msg.m_ServerInfo[i].szIP,			pQResult->pServerInfo[i].pszIP, MAX_IPADDRESS_SIZE );
			strncpy( msg.m_ServerInfo[i].szInnerIP,		pQResult->pServerInfo[i].pszInnerIP, MAX_IPADDRESS_SIZE );
		}
		pServer->Send( (BYTE *)&msg, msg.GetPacketSize() );
	}

	if( pQResult->GetResultRowNum() == 0 || pQResult->GetResultRowNum() < ServerInfoQuery::SERVERINFO_ROW_NUM )
	{
		/// 마지막 데이터 이다.
		MSG_MX_CONNECTION_REQ_SERVER_INFO_CMD msg;
		msg.m_byCategory	= MX_CONNECTION;
		msg.m_byProtocol	= MX_CONNECTION_REQ_SERVER_INFO_CMD;
		pServer->Send( (BYTE *)&msg, sizeof(msg) );
	}
	else
	{
		ExecuteServerInfoQuery( pServer, pQResult->GetCurPage()+1 );
	}

	QUERYFREE( ServerInfoQuery, pQResult );
}

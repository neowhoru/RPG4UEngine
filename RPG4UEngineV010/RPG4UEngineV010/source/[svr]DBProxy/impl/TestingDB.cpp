#include "stdafx.h"
#include "TestingDB.h"
#include "DBServerUtil.h"
#include <ConstInfoSql.h>
#include <DatabaseProxyQuery.h>

#ifndef NONE_TESTING

const	WORD	TESTING_ID	= 123;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(TestingSession, ()  , gamemain::eInstPrioGameFunc);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void TestingSession::InitTesting()
{
	Init();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID	TestingSession::DBResult( BYTE cate, BYTE ptcl, Query * pData )
{
	if(cate == TESTING_ID)
	{
		switch(ptcl)
		{
		case 1:
			{
				TestingQuery* pQuery = (TestingQuery*)pData;
				DISPMSG( "Result:%s \n" ,pQuery->GetResult()? _T("[x]") : _T("[OK]"));

				TestingQuery::FREE(pQuery);
			}
			break;

		case 2:
			{
				QueryT2* pQuery = (QueryT2*)pData;

				if(pQuery->GetResultRowNum() > 0)
				{
					DISPMSG	("%s	(ID:%d, Money:%I64u, Time:%s\n"
								,pQuery->pCharacter->m_szCharName
								,pQuery->pCharacter->m_UserGuid
								,pQuery->pCharacter->m_Money
								,DBServerUtil::TimeStampText(pQuery->pCharacter->m_StateTime)
								);
				}
				else
				{
					DISPMSG( "Result:%d \n" ,pQuery->GetResultRowNum());
				}
				QueryT2::FREE(pQuery);
			}
			break;

		case 3:
			{
				Query* pQuery = (Query*)pData;
				sRESULT_SELECTCHAR*	pResults = (sRESULT_SELECTCHAR*)pQuery->GetQueryResult();

				if(pQuery->GetResultRowNum() > 0)
				{
					DISPMSG	("%s	(ID:%d, Money:%I64u, Time:%s\n"
								,pResults->m_szCharName
								,pResults->m_UserGuid
								,pResults->m_Money
								,DBServerUtil::TimeStampText(pResults->m_StateTime)
								);
				}
				else
				{
					DISPMSG( "Result:%d \n" ,pQuery->GetResultRowNum());
				}

				SAFE_FREEQUERY(pQuery);
			}
			break;

		case 4:
			{
				UpdateCharacterQueryTest* pQuery = (UpdateCharacterQueryTest*)pData;

				if( UpdateCharacterQueryTest::QUERY_RESULT_SUCCEEDED != pQuery->pCharacterParam[0].m_Result )
				{
					DISPMSG( "[Charinfo Update Failed (%s)\n", pQuery->GetQuery() );
				}
				else
				{
					DISPMSG( "Result: OK \n" );
				}
				UpdateCharacterQueryTest::FREE(pQuery);
			}
			break;

		case 5:
			{
				UpdateCharacterQuery* pQuery = (UpdateCharacterQuery*)pData;

				if( UpdateCharacterQuery::QUERY_RESULT_SUCCEEDED != pQuery->pCharacterParam[0].m_Result )
				{
					DISPMSG( "[Charinfo Update Failed (%d)\n",pQuery->pCharacterParam[0].m_Result);//, pQuery->GetQuery() );
				}
				else
				{
					DISPMSG( "Result: OK \n" );
				}
				QUERYFREE(UpdateCharacterQuery,pQuery);
			}
			break;
		}
	}

}

void TestingSession::TestingSql(int nType)
{
	static BOOL  gInitDB = FALSE;

	Query* pQuery;
	if(!gInitDB)
	{
		gInitDB = TRUE;
		InitTesting();
	}

	switch(nType)
	{
	case 1:
		pQuery = SAFE_ALLOCQUERY(TestingQuery);
		pQuery->SetQueryF( "{?=call Char_Destroy(%u, %u)}", 1,7 );
		break;
	case 2:
		pQuery = SAFE_ALLOCQUERY(QueryT2);
		pQuery->SetQueryF( "Char_SelectTest 6", 1,7 );
		break;
	case 3:
		pQuery = SAFE_ALLOCQUERY(SelectCharQuery);
		pQuery->SetQueryF("Char_Select 6", 1,7 );
		break;
	case 4:
		{
			UpdateCharacterQueryTest*	pQueryL = UpdateCharacterQueryTest::ALLOC();
			pQuery = pQueryL;
			pQueryL->SetQueryF("{?=call Char_UpdateTest(7,99,?,?,?,?,?,?)}" );
			//pQueryL->SetQueryF("{?=call Char_UpdateTest1(7,99,?)}" );

			__ZERO(pQueryL->pCharacterParam[0].m_pInventoryItem);
			__ZERO(pQueryL->pCharacterParam[0].m_pEquipItem);

			pQueryL->pCharacterParam[0].m_Result				= 0;


			INT nMax(10);
			nMax = sizeof(pQueryL->pCharacterParam[0].m_pEquipItem);
			for(INT n=0; n<nMax; n++)
				pQueryL->pCharacterParam[0].m_pEquipItem[n] = (n/SIZEOF_ITEMOPT_STREAM)+1;

			nMax = sizeof(pQueryL->pCharacterParam[0].m_pInventoryItem);
			for(INT n=0; n<nMax; n++)
				pQueryL->pCharacterParam[0].m_pInventoryItem[n] = (n/SIZEOF_ITEMOPT_STREAM)+1;

		}
		break;
	case 5:
		{
			Query * pQueryL = SAFE_ALLOCQUERY(UpdateCharacterQuery);
			pQuery = pQueryL;
			pQueryL->SetQueryF("{?=call Char_Update(7,88,25,12,15,15,24,0,0,0,0,0,10,104,100,48,0,1000,0,0,0,1,1,'2009-4-21 16:46:0',10001,500,510,0,'title','2009-4-21 16:46:0',1,0,0,0,0,0,0,0,0,0,0,0,?,?,?,?,?,?,?,?,?,?,?,?)}" );
			pQueryL->pCharacterParam[0].m_Result		= -1;
			__ZERO(pQueryL->pCharacterParam[0].m_pEquipItem);
			__ZERO(pQueryL->pCharacterParam[0].m_pInventoryItem);
			__ZERO(pQueryL->pCharacterParam[0].m_pTempInventoryItem);
			__ZERO(pQueryL->pCharacterParam[0].m_pSkill	);
			__ZERO(pQueryL->pCharacterParam[0].m_pQuest	);
			__ZERO(pQueryL->pCharacterParam[0].m_pMission);
			__ZERO(pQueryL->pCharacterParam[0].m_pQuick	);
			__ZERO(pQueryL->pCharacterParam[0].m_pStyle	);

			INT nMax(10);
			nMax = sizeof(pQueryL->pCharacterParam[0].m_pEquipItem);
			for(INT n=0; n<nMax; n++)
				pQueryL->pCharacterParam[0].m_pEquipItem[n] = (n/SIZEOF_ITEMOPT_STREAM)+1;

			nMax = sizeof(pQueryL->pCharacterParam[0].m_pInventoryItem);
			for(INT n=0; n<nMax; n++)
				pQueryL->pCharacterParam[0].m_pInventoryItem[n] = (n/SIZEOF_ITEMOPT_STREAM)+1;

			nMax = sizeof(pQueryL->pCharacterParam[0].m_pTempInventoryItem);
			for(INT n=0; n<nMax; n++)
				pQueryL->pCharacterParam[0].m_pTempInventoryItem[n] = (n/SIZEOF_ITEMOPT_STREAM)+1;
		}
		break;
	}

	pQuery->SetIndex( MAKEDWORD( TESTING_ID, nType ) );
	pQuery->SetObjectPtr( this );
	//pQuery->SetSlotIndex( 0 );
	//pQuery->SetUserKey( 11 );
	LOGINFO(_T("%s\n"), pQuery->GetQuery());

	RunQuery( pQuery );
}

#endif
#include "stdafx.h"
#include ".\handler_dg_charinfo.h"
#include <Protocol_DG.h>
#include <PacketStruct_DG.h>
#include <Macro.h>
#include "ServerSession.h"
#include "Player.h"
#include "GameServerSession.h"
#include <SCSlot.h>
#include <SCSlotContainer.h>
#include <VarPacket.h>
#include "DBProxyServer.h"

Handler_DG_CHARINFO::Handler_DG_CHARINFO()
{
}

Handler_DG_CHARINFO::~Handler_DG_CHARINFO()
{

}


VOID Handler_DG_CHARINFO::OnDG_CHARINFO_REQ_SYN( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize )
{
	MSG_DG_CHARINFO_REQ_SYN * pRecvMsg = (MSG_DG_CHARINFO_REQ_SYN *)pMsg;
	
	Player * pPlayer = DBProxyServer::Instance()->GetPlayer( pRecvMsg->m_dwID );
	if( !pPlayer )
	{
		// send nack msg
		return;
	}
	pPlayer->SelectUser( pRecvMsg->m_bySelectedCharIndex );

	char szQueryBuff[64];
	sprintf( szQueryBuff, "S_SelectChar %d", pPlayer->GetCharGuid() );
	CharacterQuery * pQuery = QUERYALLOC(CharacterQuery);
	pQuery->SetIndex( MAKEDWORD( (WORD)DG_CHARINFO, (WORD)DG_CHARINFO_REQ_DBR ) );
	pQuery->SetVoidObject( pServerSession );
	pQuery->SetQuery( szQueryBuff );
	pQuery->SetUserID( pRecvMsg->m_dwID );

	pServerSession->DBQuery( pQuery );
	
	DISPMSG( "[ID:%d]:%s\n", pPlayer->GetUserID(), szQueryBuff );

}

VOID Handler_DG_CHARINFO::OnDG_CHARINFO_REQ_DBR( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize )
{
	MSG_DBPROXY_RESULT * pResult	= (MSG_DBPROXY_RESULT *)pMsg;

	CharacterQuery * pQResult		= (CharacterQuery *)pResult->m_pData;
	if( pQResult->GetResultRowNum() != 1 )
	{
		DISPMSG( "캐릭터 정보 디비에서 읽어 오지 못했습니다. UserID(%d)\n", pQResult->GetUserID() );
		// send nack msg
		return ;
	}

	// 캐쉬 처리 코드 -------------------------------------------------------------------------
	Player * pPlayer = DBProxyServer::Instance()->GetPlayer( pQResult->GetUserID() );
	if( !pPlayer )
	{
		// send nack msg
		MSG_DG_CHARINFO_REQ_NAK errorMsg;
		errorMsg.m_byCategory	= DG_CHARINFO;
		errorMsg.m_byProtocol	= DG_CHARINFO_REQ_ACK;
		errorMsg.m_dwID			= pPlayer->GetUserID();
		pServerSession->Send( (BYTE *)&errorMsg, sizeof(MSG_DG_CHARINFO_REQ_NAK) );
		return;
	}
	pPlayer->CopyQUERY_CHARACTERTo( pPlayer->GetSelectedCharIndex(), &pQResult->pCharacter[0] );
	QUERYFREE( CharacterQuery, pQResult );
	pQResult = NULL;
	// 캐쉬 처리 코드 -------------------------------------------------------------------------



	DISPMSG( "[ID:%d]:MSG_DG_CHARINFO_REQ_ACK\n", pPlayer->GetUserID() );
	// Game Server로 캐릭터 정보 보냄
	// 캐릭정보
	// 아이템 정보
	// 스킬정보
	// 개인창고정보?
	// 각종 아이템 옵션?
	MSG_DG_CHARINFO_REQ_ACK msg;
	msg.m_byCategory	= DG_CHARINFO;
	msg.m_byProtocol	= DG_CHARINFO_REQ_ACK;
	msg.m_dwID			= pPlayer->GetUserID();
	pPlayer->SerializeCharInfo( msg.CharacterInfo, SERIALIZE_LOAD );
	pPlayer->SerializeEquipItemStream( msg.pEquipStream,  MAX_EQUIPITEMSTREAM_SIZE, SERIALIZE_LOAD );
	pPlayer->SerializeInventoryItemStream( msg.pInventoryStream,  MAX_INVENTORYITEMSTREAM_SIZE, SERIALIZE_LOAD );
	pPlayer->SerializeSkillStream( msg.pSkillStream, MAX_SKILLSTREAM_SIZE );
	pServerSession->Send( (BYTE *)&msg, sizeof(msg) );
}

VOID Handler_DG_CHARINFO::OnDG_CHARINFO_UPDATE2DBP_CMD( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize )
{
	
	MSG_DG_CHARINFO_UPDATE2DBP_CMD * pRecvMsg = (MSG_DG_CHARINFO_UPDATE2DBP_CMD *)pMsg;

	Player * pPlayer = DBProxyServer::Instance()->GetPlayer( pRecvMsg->m_dwID );
	ASSERT( pPlayer );
	pPlayer->SerializeCharInfo( pRecvMsg->m_charInfo, SERIALIZE_STORE );
	pPlayer->SerializeEquipItemStream( pRecvMsg->m_pEquipStream, MAX_EQUIPITEMSTREAM_SIZE, SERIALIZE_STORE );
	pPlayer->SerializeInventoryItemStream( pRecvMsg->m_pInventoryStream, MAX_INVENTORYITEMSTREAM_SIZE, SERIALIZE_STORE );
	pPlayer->SerializeSkillStream( pRecvMsg->m_pSkillStream, MAX_SKILLSTREAM_SIZE, SERIALIZE_STORE );

}
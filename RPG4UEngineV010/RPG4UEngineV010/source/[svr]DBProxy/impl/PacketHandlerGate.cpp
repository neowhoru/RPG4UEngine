/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGate.cpp
创建日期：2009年3月31日
最后更新：2009年3月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerGate.h"
#include <PacketStruct_Gate_DBP.h>
#include "ServerSession.h"
#include "DBUser.h"
#include "DBUserManager.h"
#include "DBProxyShell.h"
#include "ResultCode.h"
#include "PacketHandlerManager.h"
#include "ProxyQueryDefine.h"
#include "QueryStruct.h"
#include <ConstInfoSql.h>
#include "TextResManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
static void ProcessItems	(ITEMOPT_STREAM *	pEquipStream
									,sITEM_SLOTEX *		pSlot
									,BYTE &				nTotal)
{
	nTotal			= 0;
	for(SLOTPOS pos=0;pos<MAX_EQUIPMENT_SLOT_NUM;++pos)
	{
		if( pEquipStream[pos].ItemPart.dwSerial == 0 )
			continue;

		pSlot[nTotal].m_ItemPos = pos;
		memcpy(&pSlot[nTotal].m_Stream
            ,&pEquipStream[pos]
				,sizeof(ITEMOPT_STREAM));
		++nTotal;
	}
}

PACKETHANDLER_SERVER_AGENT( AD_CHARINFO_CHARLISTREQ_SYN )
{
	MSG_AD_CHARINFO_CHARLISTREQ_SYN * pRecvMsg = (MSG_AD_CHARINFO_CHARLISTREQ_SYN *)pMsg;
	
	//------------------------------------------------------------------------------------------------
	DWORD UserKey	= pRecvMsg->m_dwKey;
	DWORD UserGUID = pRecvMsg->m_dwUserGUID;

	Query * pQuery = SAFE_ALLOCQUERY(SelectUserQuery);
	__CHECK_PTR(pQuery);

	pQuery->SetIndex			( AD_CHARINFO,AD_CHARINFO_CHARLISTREQ_DBR  );
	pQuery->SetObjectPtr		( pServerSession );
	pQuery->SetQueryF			( SQL_SELECTUSER//_T("User_Select %u")
									, UserGUID );
	pQuery->SetUserKey		( UserKey );	

	pServerSession->RunQuery( pQuery );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AGENT( AD_CHARINFO_CHARLISTREQ_DBR )
{
	MSG_DBRESULT_BASE * pResult		= (MSG_DBRESULT_BASE *)pMsg;
	//SelectUserQuery * pQResult			= (SelectUserQuery *)pResult->m_pData;
	Query * pQResult						= (Query*)pResult->m_pData;

	DWORD dwUserKey		= pQResult->GetUserKey();
	BYTE  nResultNum		= (BYTE)pQResult->GetResultRowNum();


	///////////////////////////////
	// Gate
	sRESULT_SELECTUSER*					pQueryResult;
	MSG_AD_CHARINFO_CHARLISTREQ_ACK	msg;
	msg.m_dwKey			= dwUserKey;
	msg.m_byCount		= nResultNum;

	///////////////////////////////////////
	pServerSession->m_arMsgBufs[0]	= (BYTE*)&msg;
	pServerSession->m_arMsgSizes[0]	= (WORD)msg.GetSize();

	///////////////////////////////////////
	for( BYTE i = 0 ; i < nResultNum ; ++i )
	{
		ASSERT( sizeof(sPLAYER_SERVER_PART) < sRESULT_MSG::MAX_BUFFER_SIZE );
		sPLAYER_SERVER_PART&  charInfo = (sPLAYER_SERVER_PART&)(pServerSession->m_arResultMsgs[i].m_byMsg[0]);

		pQueryResult	= (sRESULT_SELECTUSER*)pQResult->GetQueryResult(i);
		if(!pQueryResult)
			continue;

		memcpy(charInfo.m_szCharName
				, pQueryResult->m_szCharName
				, MAX_CHARNAME_LENGTH*sizeof(TCHAR) );

		charInfo.m_CharGuid			= pQueryResult->m_CharGuid;
		charInfo.m_bySlot				= pQueryResult->m_bySlot;
		charInfo.m_byHeight			= pQueryResult->m_byHeight;
		charInfo.m_byFace				= pQueryResult->m_byFace;
		charInfo.m_byHair				= pQueryResult->m_byHair;
		charInfo.m_byHairColor		= pQueryResult->m_byHairColor;
		charInfo.m_bySex				= pQueryResult->m_bySex;
		charInfo.m_byClass			= pQueryResult->m_byClass;
		charInfo.m_LV					= pQueryResult->m_LV;
		charInfo.m_dwRegion			= pQueryResult->m_dwRegion;
		charInfo.m_wX					= pQueryResult->m_wX;
		charInfo.m_wY					= pQueryResult->m_wY;
		charInfo.m_wZ					= pQueryResult->m_wZ;
		charInfo.m_GuildGuid			= pQueryResult->m_GuildGuid;
		charInfo.m_GuildPosition	= pQueryResult->m_GuildPosition;

		memcpy(charInfo.m_szGuildNick
				, pQueryResult->m_szGuildNick
				, MAX_CHARNAME_LENGTH*sizeof(TCHAR) );


		///////////////////////////////////////////////////////
		ProcessItems((ITEMOPT_STREAM *)pQueryResult->m_pEquipItem
						,charInfo.m_EquipItemInfo.m_Slot
						,charInfo.m_EquipItemInfo.m_Count);


		pServerSession->m_arResultMsgs[i].m_wSize = charInfo.GetSize();

		///////////////////////////////////////////////////////
		//Next
		pServerSession->m_arMsgBufs[1+i]	= pServerSession->m_arResultMsgs[i].m_byMsg;
		pServerSession->m_arMsgSizes[1+i]= pServerSession->m_arResultMsgs[i].m_wSize;
	}

	SAFE_FREEQUERY(pQResult);

	/////////////////////////////////////////////
	pServerSession->Send	(nResultNum + 1
								,&pServerSession->m_arMsgBufs[0]
								,&pServerSession->m_arMsgSizes[0]);
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AGENT( AD_CHARINFO_CREATE_SYN )
{


	MSG_AD_CHARINFO_CREATE_SYN * pRcvMsg = (MSG_AD_CHARINFO_CREATE_SYN *)pMsg;

	DWORD UserKey = pRcvMsg->m_dwKey;
	if( theDBUserManager.FindCacheUser(UserKey) )
		theDBUserManager.DestroyCacheUser(UserKey);


	///////////////////////////////////////////
	StringHandle	szTempUserID;
	StringHandle	szTempCharName;
	szTempUserID	.Set(pRcvMsg->m_szUserID	, MAX_ID_LENGTH);
	szTempCharName	.Set(pRcvMsg->m_szCharName	, MAX_CHARNAME_LENGTH);


	Query * pQuery = SAFE_ALLOCQUERY(CreateCharQuery);
	__CHECK_PTR(pQuery);

	pQuery->SetIndex( AD_CHARINFO, AD_CHARINFO_CREATE_DBR  );
	pQuery->SetObjectPtr( pServerSession );


	pQuery->SetQueryF	(SQL_CREATECHAR
							,pRcvMsg->m_dwUserGUID
                     ,(LPCTSTR)szTempUserID
							,(LPCTSTR)szTempCharName
							,pRcvMsg->m_byClass
							,pRcvMsg->m_byHeight
							,pRcvMsg->m_byFace
							,pRcvMsg->m_byHair
							,pRcvMsg->m_byHairColor
							,pRcvMsg->m_bySex);
	pQuery->SetUserKey( pRcvMsg->m_dwKey );
	pServerSession->RunQuery( pQuery );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AGENT( AD_CHARINFO_CREATE_DBR )
{
	MSG_DBRESULT_BASE *	pResult	= (MSG_DBRESULT_BASE *)pMsg;
	//CreateCharQuery *	pQResult	= (CreateCharQuery *)pResult->m_pData;
	Query *					pQResult	= (Query*)pResult->m_pData;

	DWORD						dwUserKey	= pQResult->GetUserKey();
	BYTE						nResultNum	= (BYTE)pQResult->GetResultRowNum();

	/////////////////////////////////////////////////
	if( pQResult->GetResult() != 0 )
	{
		switch( pQResult->GetResult() )
		{
		case RC::RC_CHARINFO_EXISTED:
			{
				LOGMSG(LOG_FULL,  "[( AD_CHARINFO_CREATE_DBR )] [U:%u][R:%u]名称已存在(%s)", dwUserKey, pQResult->GetResult(), pQResult->GetQuery() );
			} break;
		case RC::RC_CHARINFO_FULL:
			{
				LOGMSG(LOG_FULL,  "[( AD_CHARINFO_CREATE_DBR )] [U:%u][R:%u]人物已满(%s)", dwUserKey, pQResult->GetResult(), pQResult->GetQuery() );
			} break;
		default:
			{
				LOGMSG(LOG_FULL,  "[( AD_CHARINFO_CREATE_DBR )] [U:%u][R:%u]创建人物出错(%s)", dwUserKey, pQResult->GetResult(), pQResult->GetQuery() );
			}break;
		}
		
		MSG_AD_CHARINFO_CREATE_NAK msgNAK;
		msgNAK.m_dwKey			= dwUserKey;
		msgNAK.m_dwErrorCode	= pQResult->GetResult();
		pServerSession->Send( (BYTE *)&msgNAK, sizeof(msgNAK) );

		SAFE_FREEQUERY(pQResult);
		return TRUE;
	}

	/////////////////////////////////////////////////
	__CHECK(nResultNum == 1);


	/////////////////////////////////////////////////
	MSG_AD_CHARINFO_CREATE_ACK msg;
	sRESULT_CREATECHAR*			pQueryResult;
	sPLAYER_SERVER_PART&			charInfo = msg.m_CharInfo;

	pQueryResult	=	(sRESULT_CREATECHAR*)pQResult->GetQueryResult();
	__VERIFY_PTR(pQueryResult
					,_T("Invalid sRESULT_CREATECHAR Ptr"));

	//////////////////////////////////////////////
	msg.m_dwKey					= dwUserKey;
	charInfo.m_CharGuid		= pQueryResult->m_CharGuid;
	charInfo.m_bySlot			= pQueryResult->m_bySlot;
	charInfo.m_byHeight		= pQueryResult->m_byHeight;
	charInfo.m_byFace			= pQueryResult->m_byFace;
	charInfo.m_byHair			= pQueryResult->m_byHair;
	charInfo.m_byHairColor	= pQueryResult->m_byHairColor;
	charInfo.m_bySex			= pQueryResult->m_bySex;
	charInfo.m_byClass		= pQueryResult->m_byClass;
	charInfo.m_LV				= pQueryResult->m_LV;
	charInfo.m_dwRegion		= pQueryResult->m_iRegion;
	charInfo.m_wX				= pQueryResult->m_sX;
	charInfo.m_wY				= pQueryResult->m_sY;
	charInfo.m_wZ				= pQueryResult->m_sZ;

	memcpy(charInfo.m_szCharName
			, pQueryResult->m_szCharName
			, MAX_CHARNAME_LENGTH*sizeof(TCHAR) );

	///////////////////////////////////////////
	charInfo.m_GuildGuid			= 0;
	charInfo.m_GuildPosition	= 0;
	charInfo.m_szGuildNick[0]	= 0;


	///////////////////////////////////////////
	ProcessItems	((ITEMOPT_STREAM *)pQueryResult->m_pEquipItem
                  ,charInfo.m_EquipItemInfo.m_Slot
						,charInfo.m_EquipItemInfo.m_Count);


	//-----------------------------------------------------------------------------------------
	SAFE_FREEQUERY(pQResult);

	pServerSession->Send( (BYTE *)&msg, msg.GetSize() );
	//	LOGMSG(LOG_FULL,  "[U:%d]:\n", dwUserKey );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AGENT( AD_CHARINFO_DESTROY_SYN )
{
	MSG_AD_CHARINFO_DESTROY_SYN * pRcvMsg = (MSG_AD_CHARINFO_DESTROY_SYN *)pMsg;

	DWORD UserKey = pRcvMsg->m_dwKey;

	if( theDBUserManager.FindCacheUser(UserKey) )
		theDBUserManager.DestroyCacheUser(UserKey);

	//------------------------------------------------------------------------------------------------
	Query * pQuery = SAFE_ALLOCQUERY(DestroyCharQuery);
	__CHECK_PTR(pQuery);

	pQuery->SetIndex		( AD_CHARINFO, AD_CHARINFO_DESTROY_DBR );
	pQuery->SetObjectPtr( pServerSession );
	pQuery->SetUserParam	(pRcvMsg->m_SelectedSlotIndex);
	pQuery->SetQueryF		( SQL_DESTROYCHAR			//_T("{?=call Char_Destroy(%u, %u)}")
								, pRcvMsg->m_dwUserGUID
								, pRcvMsg->m_dwCharGUID);
	pQuery->SetUserKey	( UserKey );

	pServerSession->RunQuery( pQuery );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AGENT( AD_CHARINFO_DESTROY_DBR )
{
	MSG_DBRESULT_BASE *	pResult	= (MSG_DBRESULT_BASE *)pMsg;
	Query *					pQResult	= (Query *)pResult->m_pData;

	DWORD dwUserKey		= pQResult->GetUserKey();
	BYTE  bySlotIdx		= (BYTE)pQResult->GetUserParam();

		/////////////////////////////////////////////
	if( pQResult->GetResult() != 0 )
	{
		LOGMSG(LOG_FULL
				,"[AD_CHARINFO_DESTROY_DBR ] [ID:%u][R:%u]DESTROY Failed(%s)"
				,dwUserKey
				,pQResult->GetResult()
				,pQResult->GetQuery() );

		MSG_AD_CHARINFO_DESTROY_NAK msgNAK;
		msgNAK.m_dwKey			= dwUserKey;
		msgNAK.m_dwErrorCode	= pQResult->GetResult();
		pServerSession->Send( (BYTE *)&msgNAK, sizeof(msgNAK) );
	}
	else
	{
		/////////////////////////////////////////////
		MSG_AD_CHARINFO_DESTROY_ACK msg;
		msg.m_dwKey					= dwUserKey;
		msg.m_SelectedSlotIndex	= bySlotIdx;

		pServerSession->Send( (BYTE *)&msg, sizeof(msg) );
	}

	SAFE_FREEQUERY(pQResult);
}
PACKETHANDLER_SERVER_END();




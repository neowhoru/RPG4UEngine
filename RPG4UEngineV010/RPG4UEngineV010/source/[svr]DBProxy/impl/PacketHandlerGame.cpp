/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGame.cpp
创建日期：2009年3月31日
最后更新：2009年3月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerGame.h"
#include <PacketStruct_DBP_Game.h>
#include "ServerSession.h"
#include "DBUser.h"
#include "ResultCode.h"
#include "DBUserManager.h"
#include "GameSession.h"
#include <BaseSlot.h>
#include <BaseContainer.h>
#include "DBProxyShell.h"
#include "PacketHandlerManager.h"
#include <ConstInfoSql.h>
#include <DatabaseProxyQuery.h>
#include <QueryHelper.h>
#include <DBProxyDefine.h>
#include <QueryStruct.h>


using namespace RC;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _CHECK_USER(NAME)\
					DBUser * pUser;\
					pUser = theDBUserManager.FindUser( dwUserKey );\
					__VERIFY_PTR(pUser\
									,FMSTR(_T("(") #NAME _T(") [UserGUID:%u]"), dwUserKey));

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _CHARINFO_INT(NAME)\
					DWORD		dwUserKey	= pRecvMsg->m_dwKey;\
					BYTE		slotIdx;\
					_CHECK_USER(NAME);\
					slotIdx = pUser->GetSelectedSlotIndex()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_CHAR_ITEM_CMD)
{
	_CHARINFO_INT(DG_CHARINFO_CHAR_ITEM_CMD);




	/////////////////////////////////////////////////
	pUser->SerializeCharInfo		(slotIdx
                                 ,pRecvMsg->m_CharacterInfo
											,SERIALIZE_STORE);

	pUser->ClearEquipItems			(slotIdx);
	pUser->ClearInventoryItems		(slotIdx);
	pUser->ClearTempInventoryItems(slotIdx);


	/////////////////////////////////////////////////
	sITEM_SLOTEX * pSlot		= pRecvMsg->m_ItemInfo.m_Slot;
	SLOTPOS			posFirst = 0;
	SLOTPOS			posTotal	= pRecvMsg->m_ItemInfo.m_EquipCount;

	/////////////////////////////////////////////////
	for(SLOTPOS i=posFirst;i<posTotal;++i)
		pUser->SetEquipItem(slotIdx
                         ,pSlot[i].m_ItemPos
								 ,&pSlot[i].m_Stream);

	/////////////////////////////////////////////////
	posFirst = posTotal;
	posTotal += pRecvMsg->m_ItemInfo.m_InvenCount;
	for(SLOTPOS i=posFirst;i<posTotal;++i)
		pUser->SetInventoryItem(slotIdx
                             ,pSlot[i].m_ItemPos
									  ,&pSlot[i].m_Stream);


	/////////////////////////////////////////////////
	posFirst = posTotal;
	posTotal += pRecvMsg->m_ItemInfo.m_TmpInvenCount;
	for(SLOTPOS i=posFirst;i<posTotal;++i)
		pUser->SetTempInventoryItem(slotIdx
                                 ,pSlot[i].m_ItemPos
											,&pSlot[i].m_Stream);
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_SKILL_CMD)
{
	_CHARINFO_INT(DG_CHARINFO_SKILL_CMD);



	///////////////////////////////////////////////
	pUser->m_PacketFlags |= DBPFLAG_SKILL_CMD;

	pUser->ClearSkills(slotIdx);


	///////////////////////////////////////////////
	sSKILL_SLOT *	pSlot		= pRecvMsg->m_SkillInfo.m_Slot;
	SLOTPOS			posFirst = 0;
	SLOTPOS			posTotal = pRecvMsg->m_SkillInfo.m_Count;

	for(SLOTPOS i=posFirst;i<posTotal;++i)
	{
		pUser->SetSkill(slotIdx
                     ,pSlot[i].m_SkillPos
							,&pSlot[i].m_Stream);
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_QUICK_CMD)
{
	_CHARINFO_INT(DG_CHARINFO_QUICK_CMD);



	///////////////////////////////////////////////
	pUser->m_PacketFlags |= DBPFLAG_QUICK_CMD;

	pUser->ClearQuicks(slotIdx);


	///////////////////////////////////////////////
	sQUICK_SLOT *	pSlot		= pRecvMsg->m_QuickInfo.m_Slot;
	SLOTPOS			posFirst = 0;
	SLOTPOS			posTotal = pRecvMsg->m_QuickInfo.m_Count;
	for(SLOTPOS i=posFirst;i<posTotal;++i)
	{
		pUser->SetQuick(slotIdx
                     ,pSlot[i].m_QuickPos
							,&pSlot[i].m_Stream);
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_STYLE_CMD)
{
	_CHARINFO_INT(DG_CHARINFO_STYLE_CMD);



	///////////////////////////////////////////////
	pUser->m_PacketFlags |= DBPFLAG_STYLE_CMD;

	pUser->ClearStyles(slotIdx);

	///////////////////////////////////////////////
	sSTYLE_SLOT *	pSlot		= pRecvMsg->m_StyleInfo.m_Slot;
	SLOTPOS			posFirst = 0;
	SLOTPOS			posTotal = pRecvMsg->m_StyleInfo.m_Count;
	for(SLOTPOS i=posFirst;i<posTotal;++i)
	{
		pUser->SetStyle(slotIdx
                     ,pSlot[i].m_StylePos
							,&pSlot[i].m_Stream);
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_QUEST_CMD)
{
	_CHARINFO_INT(DG_CHARINFO_QUEST_CMD);



	///////////////////////////////////////////////
	pUser->m_PacketFlags |= DBPFLAG_QUEST_CMD;


	pUser->SerializeQuests	(slotIdx
                           ,pRecvMsg->m_pQuestStream
									,MAX_QUESTSTREAM_SIZE
									,SERIALIZE_STORE);
	pUser->SerializeMissions(slotIdx
                           ,pRecvMsg->m_pMissionStream
									,MAX_MISSIONSTREAM_SIZE
									,SERIALIZE_STORE);
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_WAREHOUSE_CMD)
{
	_CHARINFO_INT(DG_CHARINFO_WAREHOUSE_CMD);



	///////////////////////////////////////////////
	pUser->m_PacketFlags |= DBPFLAG_WAREHOUSE_CMD;

	pUser->ClearWarehouseItems();

	///////////////////////////////////////////////
	sITEM_SLOTEX * pSlot		= pRecvMsg->m_ItemInfo.m_Slot;
	SLOTPOS			posFirst = 0;
	SLOTPOS			posTotal = pRecvMsg->m_ItemInfo.m_Count;

	for(SLOTPOS i=posFirst;i<posTotal;++i)
		pUser->SetWarehouseItem(pSlot[i].m_ItemPos
                             ,&pSlot[i].m_Stream)		;

	pUser->SetWarehouseMoney(pRecvMsg->m_WarehouseMoney);

	pUser->SetWarehouseModified( TRUE );
}
PACKETHANDLER_SERVER_END();




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_UDPATE_CMD)
{
	DWORD									dwUserKey	= pRecvMsg->m_dwKey;

	return ProcessCharInfoUpdate(pServerSession,dwUserKey,TRUE);
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR2(DG_CHARINFO_UDPATE_CMD_DBR)
{
	//参见 DG_CHARINFO_UDPATE_DBR
}
PACKETHANDLER_SERVER_END();


BOOL PacketHandlerGame::ProcessCharInfoUpdate(ServerSession * pServerSession
												,DWORD           dwUserKey
												,BOOL				  bUpdateCmd)
{
	_CHECK_USER(DG_CHARINFO_UDPATE_CMD);


	if(bUpdateCmd)
	{
		if( !pUser->IsValidSlot( pUser->GetSelectedSlotIndex() ) )
		{
			theDBUserManager.DestroyUser(dwUserKey);
			LOGMSG(LOG_FULL,  "(DG_CHARINFO_UDPATE_CMD) IsValidSlot\n");
			return FALSE;
		}
	}

	//////////////////////////////////////
	pUser->SetDBSaveState( DBSAVE_PREPARE );
	theDBUserManager.UserNormalToCache( dwUserKey );

	theDBProxyShell.RunUpdateWarehouseQuery( pUser );


	//////////////////////////////////////
	Query *	pQuery	= SAFE_ALLOCQUERY(UpdateCharacterQuery);
	//UpdateItemsQuery *		pQuery2	= QUERYALLOC(UpdateItemsQuery);
	//UpdateItemsQuery *		pQuery3	= QUERYALLOC(UpdateItemsQuery);


	pUser->MakeUpdateCharacterQuery(pQuery);//,pQuery2,pQuery3);
	pQuery->SetIndex			( DG_CHARINFO, DG_CHARINFO_UDPATE_DBR  );
	pQuery->SetObjectPtr		( pServerSession );
	pQuery->SetUserParam		(bUpdateCmd);
	pServerSession->RunQuery( pQuery );

	//pQuery2->SetIndex			( MAKEDWORD( (WORD)DG_CHARINFO, (WORD)DG_CHARINFO_UDPATE2_DBR ) );
	//pQuery2->SetObjectPtr	( pServerSession );
	//pQuery2->SetUpdateCmd	(bUpdateCmd);
	//pServerSession->RunQuery	( pQuery2 );

	//pQuery3->SetIndex			( MAKEDWORD( (WORD)DG_CHARINFO, (WORD)DG_CHARINFO_UDPATE2_DBR ) );
	//pQuery3->SetObjectPtr	( pServerSession );
	//pQuery3->SetUpdateCmd	(bUpdateCmd);
	//pServerSession->RunQuery	( pQuery3 );
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_UDPATE_SYN)
{
	DWORD dwUserKey = pRecvMsg->m_dwKey;

	return ProcessCharInfoUpdate	( pServerSession
											,dwUserKey
											,FALSE);

}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR2(DG_CHARINFO_UDPATE_DBR)
{
	MSG_DBRESULT_BASE *		pResult		= (MSG_DBRESULT_BASE *)pMsg;
	Query *	pQResult		= (Query *)pResult->m_pData;
	BOOL							bUpdateCmd;
	DWORD							dwUserKey;
	DBUser *						pUser;

	dwUserKey	= pQResult->GetUserKey();
	bUpdateCmd	= (BOOL)pQResult->GetUserParam();//IsUpdateCmd();


	pUser = theDBUserManager.FindCacheUser( dwUserKey );
	if( !pUser ) 
	{
		LOGMSG(LOG_FULL,  "(DG_CHARINFO_UDPATE_DBR) User is NULL\n ");
	}
	else
	{
		pUser->SetDBSaveState( DBSAVE_ACTIONED );
	}

	//////////////////////////////////////////////////////////////
	if( !pQResult->IsResultSucceed() )
	{
		LOGMSG(LOG_FULL
				, "(DG_CHARINFO_UDPATE_DBR) (%s)"
				, pQResult->GetQuery() );
	}


	/////////////////////////////////////////////////
	SAFE_FREEQUERY(pQResult);


	/////////////////////////////////////////////////
	if(!bUpdateCmd)
	{
		MSG_DG_CHARINFO_UDPATE_ACK msgACK;
		msgACK.m_dwKey = dwUserKey;
		pServerSession->Send( (BYTE *)&msgACK, sizeof(msgACK) );
	}
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR2(DG_CHARINFO_UDPATE2_DBR)
{
	//MSG_DBRESULT_BASE *		pResult		= (MSG_DBRESULT_BASE *)pMsg;
	//UpdateItemsQuery *		pQResult		= (UpdateItemsQuery *)pResult->m_pData;
	//BOOL							bUpdateCmd;

	//DWORD dwUserKey		= pQResult->GetUserKey();
	////BYTE Type			= pQResult->GetType();
	//bUpdateCmd			= pQResult->IsUpdateCmd();

	//DBUser * pUser = theDBUserManager.FindCacheUser( dwUserKey );
	//if( !pUser ) 
	//{
	//	LOGMSG(LOG_FULL,  "[(DG_CHARINFO_UDPATE_DBR)] ");
	//}
	//else
	//{
	//	pUser->SetDBSaveState( DBSAVE_ACTIONED );
	//}

	//if( UpdateItemsQuery::QUERY_RESULT_SUCCEEDED != pQResult->pCharacterParam[0].m_Result )
	//{
	//	LOGMSG(LOG_FULL,  "[(DG_CHARINFO_UDPATE_DBR)] (%s)", pQResult->GetQuery() );
	//}

	//QUERYFREE( UpdateItemsQuery, pQResult );
	//pQResult = NULL;

	
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID PacketHandlerGame::RunSelectCharQuery	(ServerSession * pServerSession
                                                ,DWORD           DBIndex
																,DWORD           CharGUID
																,DWORD           dwUserKey
																,BYTE            Type)
{
	Query* pQuery = SAFE_ALLOCQUERY(SelectCharQuery);

	pQuery->SetIndex		(DBIndex );
	pQuery->SetObjectPtr	(pServerSession );
	pQuery->SetQueryF		(SQL_SELECTCHAR,CharGUID);	//"Char_Select %u", CharGUID
	pQuery->SetUserKey	(dwUserKey );
	pQuery->SetUserParam	(Type );
	pServerSession->RunQuery( pQuery );

	//LOGMSG(LOG_FULL,  "[ID:%d]:%s", dwUserKey, szQueryBuff );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR(DG_CHARINFO_ALL_REQ_SYN)
{
	DWORD		dwUserKey	= pRecvMsg->m_dwKey;
	BOOL		bCache		= FALSE;
	DBUser * pUser;

	///////////////////////////////////////////////////////////
	pUser = theDBUserManager.FindCacheUser(dwUserKey);
	if( pUser )
		bCache = TRUE;

	///////////////////////////////////////////////////////////
	if( !pUser )
	{
		pUser = theDBUserManager.CreateUser(dwUserKey);
		if( !pUser ) 
		{
			LOGMSG(LOG_FULL,  "[(DG_CHARINFO_ALL_REQ_SYN) 创建DBUser失败." );
			return FALSE;
		}
	}


	///////////////////////////////////////////////////////////
	if(!pUser->IsValidSlot(pRecvMsg->m_SelectedSlotIndex) )
	{
		////////////////////////////////////////////////
		if(bCache)
			theDBUserManager.UserCacheToNormal( dwUserKey );

		pUser->SetSelectedSlotIndex( pRecvMsg->m_SelectedSlotIndex );
		pUser->SetDBSaveState		( DBSAVE_NONE );
		pUser->SetDBLoadState		( DBLOAD_PREPARE );

		RunSelectCharQuery(pServerSession
                        ,MAKEDWORD(DG_CHARINFO,DG_CHARINFO_ALL_REQ_DBR )
								,pRecvMsg->m_dwCharGUID
								,dwUserKey
								,pRecvMsg->m_byType);
	}
	else if(bCache)
	{
		//////////////////////////////////////////
		theDBUserManager.UserCacheToNormal( dwUserKey );

		//////////////////////////////
		pUser->_Reset();
		pUser->SetSelectedSlotIndex( pRecvMsg->m_SelectedSlotIndex );
		pUser->SetDBSaveState		( DBSAVE_NONE );
		pUser->SendAllToGameShell	( pServerSession );
		pUser->SetDBLoadState		( DBLOAD_ACTIONED );

		//////////////////////////////
		MSG_DG_CHARINFO_ALL_REQ_ACK msgACK;
		msgACK.m_dwKey		= dwUserKey;
		msgACK.m_byType	= pRecvMsg->m_byType;
		pServerSession->Send( (BYTE *)&msgACK, sizeof(msgACK) );
	}
	else
	{
		MSG_DG_CHARINFO_ALL_REQ_NAK msgNAK;
		msgNAK.m_dwKey				= dwUserKey;
		msgNAK.m_dwErrorCode		= RC_CHARINFO_DBINFO_FAILED;
		msgNAK.m_byType			= pRecvMsg->m_byType;
		pServerSession->Send( (BYTE *)&msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	pUser->SetServerSessionIndex( pServerSession->GetSessionIndex() );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_CHAR2(DG_CHARINFO_ALL_REQ_DBR )
{
	MSG_DBRESULT_BASE *	pResult	= (MSG_DBRESULT_BASE *)pMsg;
	Query *					pQResult	= (Query *)pResult->m_pData;
	DWORD						dwUserKey= pQResult->GetUserKey();
	BYTE 						byType	= (BYTE)pQResult->GetUserParam();
	DBUser *					pUser;

	//////////////////////////////
	pUser = theDBUserManager.FindUser( dwUserKey );
	if( !pUser )
	{
		SAFE_FREEQUERY(pQResult);
		return FALSE;
	}

	//////////////////////////////
	if( pQResult->GetResultRowNum() != 1 )
	{
		theDBUserManager.DestroyUser(dwUserKey);
		LOGMSG(LOG_FULL,  "(DG_CHARINFO_ALL_REQ_DBR ) 人物enterWorld信息查询失败. UserID(%u)(%s)", dwUserKey, pQResult->GetQuery() );

		MSG_DG_CHARINFO_ALL_REQ_NAK msgNAK;
		msgNAK.m_dwKey				= dwUserKey;
		msgNAK.m_dwErrorCode		= RC_CHARINFO_DBINFO_FAILED;
		msgNAK.m_byType			= byType;
		pServerSession->Send( (BYTE *)&msgNAK, sizeof(msgNAK) );

		SAFE_FREEQUERY(pQResult);
		return FALSE;
	}


	//////////////////////////////
	QueryHelper::GhostCharQueryTo(pUser
                                ,pUser->GetSelectedSlotIndex()
										  ,(sRESULT_SELECTCHAR*)pQResult->GetQueryResult());
	SAFE_FREEQUERY(pQResult);


	//////////////////////////////
	pUser->SetDBLoadState		( DBLOAD_ACTIONED );
	pUser->SetOccupiedSlot		( pUser->GetSelectedSlotIndex(), TRUE );
	pUser->SendAllToGameShell	( pServerSession );


	//////////////////////////////
	MSG_DG_CHARINFO_ALL_REQ_ACK msgACK;
	msgACK.m_dwKey		= dwUserKey;
	msgACK.m_byType	= byType;
	pServerSession->Send( (BYTE *)&msgACK, sizeof(msgACK) );
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_ITEM(DG_ITEM_SERIAL_SYN)
{
	MSG_DG_ITEM_SERIAL_ACK msgACK;

	msgACK.m_dwKey		= pRecvMsg->m_dwKey;
	msgACK.m_SlotIndex= pRecvMsg->m_SlotIndex;
	msgACK.m_ItemPos	= pRecvMsg->m_ItemPos;

	msgACK.m_DBSerial	= theDBProxyShell.AllocSerial();
	if( msgACK.m_DBSerial != 0 )
	{
		pServerSession->Send( (BYTE *)&msgACK, sizeof(msgACK) );
	}
	else
	{
		LOGMSG(LOG_FULL,  "(DG_ITEM_SERIAL_SYN) m_DBSerial is zero." );
	}
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_WARE(DG_WAREHOUSE_START_SYN)
{
	DWORD		dwUserKey = pRecvMsg->m_dwKey;
	Query*	pQuery;

	_CHECK_USER(DG_WAREHOUSE_START_SYN);


	pQuery = SAFE_ALLOCQUERY(SelectWarehouseQuery);

	//////////////////////////////////////////
	pQuery->PrepareQuery	( );
	pQuery->SetIndex		( DG_WAREHOUSE, DG_WAREHOUSE_START_DBR );
	pQuery->SetObjectPtr	( pServerSession );
	pQuery->SetQueryF		(SQL_SELECTWAREHOUSE, dwUserKey);//"Warehouse_Select %u", dwUserKey
	pQuery->SetUserKey	( dwUserKey );

	pServerSession->RunQuery( pQuery );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_WARE2(DG_WAREHOUSE_START_DBR)
{
	MSG_DBRESULT_BASE *			pResult	= (MSG_DBRESULT_BASE *)pMsg;
	Query *							pQResult	= (Query *)pResult->m_pData;
	MSG_DG_WAREHOUSE_START_NAK msgNAK;
	DBUser *							pUser;

	/////////////////////////////////////////////////////
	if( pQResult->GetResultRowNum() != 1 )
	{
		LOGMSG(LOG_FULL
				, "[DG_WAREHOUSE_START_DBR] [x]:UserGuid[%u] invalid warehouse info.(%s)"
				, pQResult->GetUserKey()
				, pQResult->GetQuery() );

		msgNAK.m_dwKey		= pQResult->GetUserKey();
		pServerSession->Send( (BYTE *)&msgNAK, sizeof(msgNAK) );

		SAFE_FREEQUERY(pQResult );
		return FALSE;
	}

	/////////////////////////////////////////////////
	pUser = theDBUserManager.FindUser( pQResult->GetUserKey() );
	if( !pUser )
	{
		msgNAK.m_dwKey		= pQResult->GetUserKey();
		pServerSession->Send( (BYTE *)&msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	sRESULT_SELECTWAREHOUSE*	pQueryResult = (sRESULT_SELECTWAREHOUSE*)pQResult->GetQueryResult();
	__VERIFY_PTR(pQueryResult,_T("Invalid pQueryResult"));

	/////////////////////////////////////////////////
	pUser->SetWarehouseMoney		(pQueryResult->m_Money );
	pUser->SerializeWarehouseItems(pQueryResult->m_byaWarehouseItem
                                 ,MAX_WAREHOUSESTREAM_SIZE
											,SERIALIZE_STORE);
	SAFE_FREEQUERY(pQResult );

	
	/////////////////////////////////////////////////
	MSG_DG_WAREHOUSE_START_ACK msgACK;
	msgACK.m_dwKey					= pUser->GetUserKey();
	msgACK.m_WarehouseMoney		= pUser->GetWarehouseMoney();
	msgACK.m_ItemInfo.m_Count	= 0;

	sITEM_SLOTEX * pSlot		= msgACK.m_ItemInfo.m_Slot;
	SLOTPOS			posTotal = 0;

	/////////////////////////////////////////////////
	for(SLOTPOS i=0;i<MAX_WAREHOUSE_SLOT_NUM;++i)
	{
		if(!pUser->ExistWarehouseItem(i))
			continue;
		pSlot[posTotal].m_ItemPos = i;
		pUser->GetWarehouseItem(pSlot[posTotal].m_ItemPos
                             ,&pSlot[posTotal].m_Stream)			;
		++posTotal;
		++msgACK.m_ItemInfo.m_Count;
	}
	pServerSession->Send( (BYTE *)&msgACK, msgACK.GetSize() );

}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GAMESERVER_WARE(DG_WAREHOUSE_END_SYN)
{
	DWORD								dwUserKey	= pRecvMsg->m_dwKey;

	_CHECK_USER(DG_WAREHOUSE_END_SYN);

	////////////////////////////////////////////////////////////////////////
	theDBProxyShell.RunUpdateWarehouseQuery( pUser );

	////////////////////////////////////////////////////////////////////////
	MSG_DG_WAREHOUSE_END_ACK msgACK;
	msgACK.m_dwKey		= dwUserKey;
	pServerSession->Send( (BYTE *)&msgACK, sizeof(msgACK) );
}
PACKETHANDLER_SERVER_END();



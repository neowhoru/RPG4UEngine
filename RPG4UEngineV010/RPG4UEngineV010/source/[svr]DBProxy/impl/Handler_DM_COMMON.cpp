#include "stdafx.h"
#include ".\handler_dm_common.h"
#include <Protocol_DM.h>
#include <PacketStruct_DM.h>
#include <Macro.h>
#include "ServerSession.h"


Handler_DM_COMMON::Handler_DM_COMMON()
{
}

Handler_DM_COMMON::~Handler_DM_COMMON()
{
}

VOID Handler_DM_COMMON::OnDM_COMMON_REQ_SERVER_INFO_SYN( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize )
{
	//MSG_DM_COMMON_REQ_SERVER_INFO_SYN * pRecvMsg = (MSG_DM_COMMON_REQ_SERVER_INFO_SYN *)pMsg;

	char szQueryBuff[64];
	sprintf( szQueryBuff, "select * from ServerInfo" );
	ServerInfoQuery * pQuery = QUERYALLOC(ServerInfoQuery);
	pQuery->SetIndex( MAKEDWORD( (WORD)DM_COMMON, (WORD)DM_COMMON_REQ_SERVER_INFO_DBR ) );
	pQuery->SetVoidObject( pServer );
	pQuery->SetQuery( szQueryBuff );
	pServer->DBQuery( pQuery );
}

VOID Handler_DM_COMMON::OnDM_COMMON_REQ_SERVER_INFO_DBR( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize )
{
	MSG_DBPROXY_RESULT * pResult	= (MSG_DBPROXY_RESULT *)pMsg;

	ServerInfoQuery * pQResult		= (ServerInfoQuery *)pResult->m_pData;

	if( pQResult->GetResultRowNum() != 0 )
	{
		ASSERT( MSG_DM_COMMON_REQ_SERVER_INFO_ACK::_MAX_SERVERINFO_ARRAY_NUM != ServerInfoQuery::SERVERINFO_ROW_NUM );
		ASSERT( pQResult->GetResultRowNum() < MSG_DM_COMMON_REQ_SERVER_INFO_ACK::_MAX_SERVERINFO_ARRAY_NUM );

		
		MSG_DM_COMMON_REQ_SERVER_INFO_ACK sendMsg;
		ZeroMemory( &sendMsg, sizeof(MSG_DM_COMMON_REQ_SERVER_INFO_ACK) );
		sendMsg.m_byCategory			= DM_COMMON;
		sendMsg.m_byProtocol			= DM_COMMON_REQ_SERVER_INFO_ACK;
		sendMsg.m_dwNumberOfServerInfo  = pQResult->GetResultRowNum();
		for( DWORD i = 0 ; i < sendMsg.m_dwNumberOfServerInfo ; ++i )
		{
			sendMsg.m_ServerInfo[i].ServerKeyInfo.byWorldID		= pQResult->pServerInfo[i].byWorldID;
			sendMsg.m_ServerInfo[i].ServerKeyInfo.byChannelID	= pQResult->pServerInfo[i].byChannelID;
			sendMsg.m_ServerInfo[i].ServerKeyInfo.byServerType	= pQResult->pServerInfo[i].byServerType;
			sendMsg.m_ServerInfo[i].ServerKeyInfo.byServerID	= pQResult->pServerInfo[i].byServerID;
			sendMsg.m_ServerInfo[i].wPort						= pQResult->pServerInfo[i].sPort;
			sendMsg.m_ServerInfo[i].wInnerPort					= pQResult->pServerInfo[i].sInnerPort;
			strncpy( sendMsg.m_ServerInfo[i].szIP,				pQResult->pServerInfo[i].pszIP, MAX_IPADDRESS_SIZE );
			strncpy( sendMsg.m_ServerInfo[i].szInnerIP,			pQResult->pServerInfo[i].pszInnerIP, MAX_IPADDRESS_SIZE );
		}
		pServer->Send( (BYTE *)&sendMsg, sendMsg.GetPacketSize() );
	}

	if( pQResult->GetResultRowNum() == 0 || pQResult->GetResultRowNum() < ServerInfoQuery::SERVERINFO_ROW_NUM )
	{
		/// 마지막 데이터 이다.
		MSG_DM_COMMON_REQ_SERVER_INFO_CMD sendCMDMsg;
		sendCMDMsg.m_byCategory			= DM_COMMON;
		sendCMDMsg.m_byProtocol			= DM_COMMON_REQ_SERVER_INFO_CMD;
		pServer->Send( (BYTE *)&sendCMDMsg, sizeof(MSG_DM_COMMON_REQ_SERVER_INFO_CMD) );
	}
	else
	{
		/// 다시 DBQuery 수행!
		char szQueryBuff[64];
		sprintf( szQueryBuff, "select * from ServerInfo" );
		ServerInfoQuery * pQuery = QUERYALLOC(ServerInfoQuery);
		pQuery->SetIndex( MAKEDWORD( (WORD)DM_COMMON, (WORD)DM_COMMON_REQ_SERVER_INFO_DBR ) );
		pQuery->SetVoidObject( pServer );
		pQuery->SetQuery( szQueryBuff );
		pServer->DBQuery( pQuery );
	}

	QUERYFREE( ServerInfoQuery, pQResult );
}
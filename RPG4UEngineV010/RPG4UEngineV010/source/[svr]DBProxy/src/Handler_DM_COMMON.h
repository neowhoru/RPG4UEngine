#ifndef __HANDLER_DM_COMMON_H__
#define __HANDLER_DM_COMMON_H__

#pragma once


#include <PacketStruct.h>

class ServerSession;

class Handler_DM_COMMON
{
public:
	Handler_DM_COMMON();
	~Handler_DM_COMMON();


	static VOID OnDM_COMMON_REQ_SERVER_INFO_SYN( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize );
	static VOID OnDM_COMMON_REQ_SERVER_INFO_DBR( ServerSession * pServer, MSG_BASE * pMsg, WORD wSize );
};


#endif // __HANDLER_DM_COMMON_H__
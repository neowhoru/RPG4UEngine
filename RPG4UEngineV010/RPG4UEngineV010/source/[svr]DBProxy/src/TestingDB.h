/*////////////////////////////////////////////////////////////////////////
文 件 名：TestingDB.h
创建日期：2008年4月12日
最后更新：2008年4月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TESTINGDB_H__
#define __TESTINGDB_H__
#pragma once

#ifndef NONE_TESTING

#include "ServerSession.h"
#include "QueryPoolFactoryDefine.h"
#include "DatabaseProxyDefine.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class QueryT2 : public QueryForUser
{
	QUERY_POOL_DECL(QueryT2)
public:
	enum
	{ 
		RESULT_ROW_NUM = 1,
		RESULT_COL_NUM = 6,
	};
	struct sQUERY_RESULT
	{
		USERGUID	m_UserGuid;
		TCHAR		m_szCharName[MAX_CHARNAME_LENGTH];
		BYTE		m_byHeight;
		SHORT		m_sStrength;
		MONEY		m_Money;
		TIMESTAMP_STRUCT		m_StateTime;

	} pCharacter[RESULT_ROW_NUM];
	ULONG uLength[RESULT_COL_NUM];

	//////////////////////////////////////////
	_RESULT_BINDING_BEGIN(sQUERY_RESULT, pCharacter, uLength, RESULT_ROW_NUM, RESULT_COL_NUM)		
		_RESULT_BINDING_COL		(0, m_UserGuid)
		_RESULT_BINDING_PTR	(1, m_szCharName)
		_RESULT_BINDING_COL		(2,m_byHeight )
		_RESULT_BINDING_COL		(3,m_sStrength )
		_RESULT_BINDING_COL		(4,m_Money )
		_RESULT_BINDING_COL		(5,m_StateTime )

	_RESULT_BINDING_END()
	//////////////////////////////////////////

public:
	inline BYTE GetType()				{ return m_Type; }
	inline VOID SetType( BYTE type )	{ m_Type=type; }
private:
	BYTE m_Type;
};
QUERY_FACTORY_DECLARE(QueryT2);




////////////////////////////////////////////////////////////////////////
class UpdateCharacterQueryTest : public QueryForUser
{
	QUERY_POOL_DECL(UpdateCharacterQueryTest)
public:
	enum
	{ 
		UPDATE_CHARACTER_PARAM_ROW_NUM	= 1,
		UPDATE_CHARACTER_PARAM_COL_NUM	= 7,
		QUERY_RESULT_SUCCEEDED			= 100,
		QUERY_RESULT_FAILED				= 1111,
	};
	struct sQUERY_CHARACTER_PARAM
	{
		INT		m_Result;
		union
		{
			BYTE		m_pInventoryItem	[MAX_INVENTORYITEMSTREAM_PAGESIZE * MAX_DB_INVENTORY_PAGE_NUM];
		struct
		{
			BYTE		m_pInventory0		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
			BYTE		m_pInventory1		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
			BYTE		m_pInventory2		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
			BYTE		m_pInventory3		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
			BYTE		m_pInventory4		[MAX_INVENTORYITEMSTREAM_PAGESIZE];
		};
		};
		BYTE		m_pEquipItem		[MAX_EQUIPITEMSTREAM_SIZE];
	} pCharacterParam[UPDATE_CHARACTER_PARAM_ROW_NUM];


	//////////////////////////////////////
	_PARAM_BINDING_BEGIN(sQUERY_CHARACTER_PARAM, pCharacterParam, UPDATE_CHARACTER_PARAM_COL_NUM)
	_PARAM_BINDING		(0, m_Result, SQL_PARAM_OUTPUT)
	_PARAM_PTR_BINDING(1, m_pEquipItem , SQL_PARAM_INPUT)
	_PARAM_PTR_BINDING(2, m_pInventory0, SQL_PARAM_INPUT)
	_PARAM_PTR_BINDING(3, m_pInventory1, SQL_PARAM_INPUT)
	_PARAM_PTR_BINDING(4, m_pInventory2, SQL_PARAM_INPUT)
	_PARAM_PTR_BINDING(5, m_pInventory3, SQL_PARAM_INPUT)
	_PARAM_PTR_BINDING(6, m_pInventory4, SQL_PARAM_INPUT)
	_PARAM_BINDING_END()

	_PARAM_BINDING_INIT();


public:
	VOID SetType( BYTE byType ) { m_byType = byType;	}
	BYTE GetType() { return m_byType;	}
private:
	BYTE m_byType;
};
QUERY_FACTORY_DECLARE(UpdateCharacterQueryTest);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class TestingQuery : public QueryForUser
{
	enum
	{ 
		UPDATE_DELETE_PARAM_ROW_NUM	= 1,
		UPDATE_DELETE_PARAM_COL_NUM	= 1,
	};
	QUERY_POOL_DECL(TestingQuery)
public:
	struct sQUERY_DELETE_PARAM
	{
		INT		m_Result;
	} pParam[UPDATE_DELETE_PARAM_ROW_NUM];
public:
	_PARAM_BINDING_BEGIN(sQUERY_DELETE_PARAM, pParam, UPDATE_DELETE_PARAM_COL_NUM)
	_PARAM_BINDING( 0, m_Result, SQL_PARAM_OUTPUT )
	_PARAM_BINDING_END()

	_PARAM_BINDING_INIT()

	inline INT GetResult()				{ return pParam[0].m_Result; }
	inline VOID SetSlotIndex( BYTE v )	{ m_bySlotIndex = v; }
	inline BYTE GetSlotIndex()			{ return m_bySlotIndex;	}
private:
	BYTE m_bySlotIndex;
};
QUERY_FACTORY_DECLARE(TestingQuery);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class TestingQuery2 : public QueryForUser
{
	enum
	{ 
		UPDATE_DELETE_PARAM_ROW_NUM	= 1,
		UPDATE_DELETE_PARAM_COL_NUM	= 1,
	};
	QUERY_POOL_DECL(TestingQuery2)
public:
	struct sQUERY_DELETE_PARAM
	{
		INT		m_Result;
	} pParam[UPDATE_DELETE_PARAM_ROW_NUM];
public:
	_PARAM_BINDING_BEGIN(sQUERY_DELETE_PARAM, pParam, UPDATE_DELETE_PARAM_COL_NUM)
	_PARAM_BINDING( 0, m_Result, SQL_PARAM_OUTPUT )
	_PARAM_BINDING_END()

	_PARAM_BINDING_INIT()

	inline INT GetResult()				{ return pParam[0].m_Result; }
	inline VOID SetSlotIndex( BYTE v )	{ m_bySlotIndex = v; }
	inline BYTE GetSlotIndex()			{ return m_bySlotIndex;	}
private:
	BYTE m_bySlotIndex;
};
QUERY_FACTORY_DECLARE(TestingQuery2);




////////////////////////////
class TestingSession : public ServerSession
{
public:
	void InitTesting();
	VOID	DBResult( BYTE cate, BYTE ptcl, Query * pData );
	void TestingSql(int nType);

public:
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(TestingSession , API_NULL);
//extern API_NULL TestingSession& singleton::GetTestingSession();
#define theTestingSession  singleton::GetTestingSession()


#endif


#endif //__TESTINGDB_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGame.h
创建日期：2009年3月31日
最后更新：2009年3月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETHANDLERGAME_H__
#define __PACKETHANDLERGAME_H__
#pragma once


#include "PacketStruct_Base.h"
#include "PacketHandlerDefine.h"
class ServerSession;


#define PACKETHANDLER_GAMESERVER_DECL(protocol)\
							PACKETHANDLER_DECL(protocol)

#define PACKETHANDLER_GAMESERVER_IMPL(protocol,cat)\
							PACKETHANDLER_SERVER_BEGIN(PacketHandlerGame, protocol, cat,GAME_SHELL);\
							ServerSession*	pServerSession	= (ServerSession*)lpParam;\
							pServerSession;

#define 	PACKETHANDLER_GAMESERVER_MSG(p)			MSG_##p *	pRecvMsg		= (MSG_##p *)pMsg


#define PACKETHANDLER_GAMESERVER_WARE2(protocol)	PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_WAREHOUSE)
#define PACKETHANDLER_GAMESERVER_ITEM2(protocol)	PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_ITEM)
#define PACKETHANDLER_GAMESERVER_CHAR2(protocol)	PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_CHARINFO)
#define PACKETHANDLER_GAMESERVER_EVET2(protocol)	PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_EVENT)
#define PACKETHANDLER_GAMESERVER_CONN2(protocol)	PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_CONNECTION)

#define PACKETHANDLER_GAMESERVER_WARE(protocol)		PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_WAREHOUSE)	;	PACKETHANDLER_GAMESERVER_MSG(protocol);
#define PACKETHANDLER_GAMESERVER_ITEM(protocol)		PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_ITEM)			;	PACKETHANDLER_GAMESERVER_MSG(protocol);
#define PACKETHANDLER_GAMESERVER_CHAR(protocol)		PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_CHARINFO)	;	PACKETHANDLER_GAMESERVER_MSG(protocol);
#define PACKETHANDLER_GAMESERVER_EVET(protocol)		PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_EVENT)		;	PACKETHANDLER_GAMESERVER_MSG(protocol);
#define PACKETHANDLER_GAMESERVER_CONN(protocol)		PACKETHANDLER_GAMESERVER_IMPL(protocol,DG_CONNECTION)	;	PACKETHANDLER_GAMESERVER_MSG(protocol);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PacketHandlerGame
{
public:
	PACKETHANDLER_GAMESERVER_DECL(DG_WAREHOUSE_START_SYN	);
	PACKETHANDLER_GAMESERVER_DECL(DG_WAREHOUSE_START_DBR	);

	PACKETHANDLER_GAMESERVER_DECL(DG_WAREHOUSE_END_SYN		);
	//PACKETHANDLER_GAMESERVER_DECL(DG_WAREHOUSE_END_DBR);


	PACKETHANDLER_GAMESERVER_DECL(DG_ITEM_SERIAL_SYN		);

	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_ALL_REQ_SYN	);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_ALL_REQ_DBR	);


	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_CHAR_ITEM_CMD	);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_SKILL_CMD		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_QUICK_CMD		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_STYLE_CMD		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_QUEST_CMD		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_WAREHOUSE_CMD	);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_UDPATE_CMD		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_UDPATE_SYN		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_UDPATE_DBR		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_UDPATE2_DBR		);
	PACKETHANDLER_GAMESERVER_DECL(DG_CHARINFO_UDPATE_CMD_DBR	);

	static VOID RunSelectCharQuery( ServerSession * pServerSession, DWORD DBIndex, DWORD CharGUID, DWORD UserKey, BYTE Type );
	static BOOL ProcessCharInfoUpdate(ServerSession * pServerSession
												,DWORD           UserKey
												,BOOL				  bUpdateCmd);

};




#endif //__PACKETHANDLERGAME_H__
#ifndef __HANDLER_DG_CHARINFO_H__
#define __HANDLER_DG_CHARINFO_H__

#pragma once


#include <PacketStruct.h>

class ServerSession;

class Handler_DG_CHARINFO
{
public:
	Handler_DG_CHARINFO();
	~Handler_DG_CHARINFO();

	static VOID OnDG_CHARINFO_REQ_SYN( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize );
	static VOID OnDG_CHARINFO_REQ_DBR( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize );

	static VOID OnDG_CHARINFO_UPDATE2DBP_CMD( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize );
};


#endif // __HANDLER_DG_CHARINFO_H__
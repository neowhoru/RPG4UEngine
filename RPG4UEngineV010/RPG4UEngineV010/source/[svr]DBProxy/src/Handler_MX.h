#ifndef __HANDLER_MX_H__
#define __HANDLER_MX_H__

#pragma once


#include <PacketStruct.h>

class ServerSession;

// Account DB Proxy Server ��
class Handler_MX
{
public:
	Handler_MX();
	~Handler_MX();

	static VOID OnDM_CONNECTION_SERVER_INFO_CMD( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize );

	static VOID OnMX_CONNECTION_REQ_SERVER_INFO_SYN( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize );
	static VOID OnMX_CONNECTION_REQ_SERVER_INFO_DBR( ServerSession * pServerSession, MSG_BASE * pMsg, WORD wSize );


	static VOID ExecuteServerInfoQuery( ServerSession * pServer, BYTE Page );
};


#endif // __HANDLER_MX_H__
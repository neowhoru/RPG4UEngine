// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "GameUIManager.h"
#include "ApplicationDefine.h"
#include "LogSystem.h"
#include "RootListener.h"
#include "SceneMoviePlayUIListener.h"
#include "SceneStartUIListener.h"
#include "SceneLoginUIListener.h"
#include "SceneLoadUIListener.h"
#include "SceneCharSelectUIListener.h"
#include "SceneVillageUIListener.h"
#include "SceneUITestUIListener.h"
#include "GameInPlayingUIListener.h"

using namespace gameui;

#define REG_LITENER(NAME)\
	static NAME##Listener s##NAME##Listener;\
		theGameUIManager.AddUIListener(&s##NAME##Listener);

#define REG_UILITENER(NAME)\
	static NAME##UIListener s##NAME##UIListener;\
		theGameUIManager.AddUIListener(&s##NAME##UIListener);




/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_CLIENTUI_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{
	//注册各类Listener
	REG_LITENER(Root);					//RootListener
	REG_UILITENER(SceneMoviePlay);	//SceneMoviePlayUIListener
	REG_UILITENER(SceneStart);			//SceneStartUIListener
	REG_UILITENER(SceneLogin);			//SceneLoginUIListener
	REG_UILITENER(SceneLoad);			//SceneLoadUIListener
	REG_UILITENER(SceneCharSelect);	//SceneCharSelectUIListener
	REG_UILITENER(SceneVillage);		//SceneVillageUIListener   
	REG_UILITENER(SceneUITest);		//SceneUITestUIListener
	REG_UILITENER(GameInPlaying);		//GameInPlayingUIListener
	//REG_UILITENER(Scene);			//SceneUIListener
//	REG_UILITENER(Scene);			//SceneUIListener


	LOGINFO("Plugin ClientUI insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_CLIENTUI_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	return gamemain::ePluginOK;
}


//BOOL APIENTRY DllMain(HANDLE hModule, DWORD ul_reason_for_call, LPVOID lpReserved)
//{
//    printf("hModule.%p lpReserved.%p \n", hModule, lpReserved);
//
//    switch (ul_reason_for_call)
//    {
//        case DLL_PROCESS_ATTACH:
//            printf("Process attach. \n");
//            break;
//
//        case DLL_PROCESS_DETACH:
//            printf("Process detach. \n");
//            break;
//
//        case DLL_THREAD_ATTACH:
//            printf("Thread attach. \n");
//            break;
//
//        case DLL_THREAD_DETACH:
//            printf("Thread detach. \n");
//            break;
//    }
//
//    return (TRUE);
//}

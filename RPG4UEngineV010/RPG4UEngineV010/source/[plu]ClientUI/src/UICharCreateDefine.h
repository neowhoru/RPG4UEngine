/*////////////////////////////////////////////////////////////////////////
文 件 名：UICharCreateDefine.h
创建日期：2008年10月13日
最后更新：2008年10月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UICHARCREATEDEFINE_H__
#define __UICHARCREATEDEFINE_H__
#pragma once


#ifndef ID_FRAME_CREATE
#define ID_FRAME_CREATE		"ID_FRAME_CREATE"
#endif  ID_FRAME_CREATE

#ifndef ID_BUTTON_TURN_L
#define ID_BUTTON_TURN_L		"ID_BUTTON_TURN_L"
#endif  ID_BUTTON_TURN_L

#ifndef ID_BUTTON_TRUN_R
#define ID_BUTTON_TRUN_R		"ID_BUTTON_TRUN_R"
#endif  ID_BUTTON_TRUN_R

#ifndef ID_BUTTON_CLOTH_L
#define ID_BUTTON_CLOTH_L		"ID_BUTTON_CLOTH_L"
#endif  ID_BUTTON_CLOTH_L

#ifndef ID_BUTTON_CLOTH_R
#define ID_BUTTON_CLOTH_R		"ID_BUTTON_CLOTH_R"
#endif  ID_BUTTON_CLOTH_R

#ifndef ID_BUTTON_OK
#define ID_BUTTON_OK		"ID_BUTTON_OK"
#endif  ID_BUTTON_OK
#ifndef ID_BUTTON_CANCEL
#define ID_BUTTON_CANCEL		"ID_BUTTON_CANCEL"
#endif  ID_BUTTON_CANCEL
#ifndef ID_LISTIMG_HairColor
#define ID_LISTIMG_HairColor		"ID_LISTIMG_HairColor"
#endif  ID_LISTIMG_HairColor
#ifndef ID_PICTURE_Property
#define ID_PICTURE_Property		"ID_PICTURE_Property"
#endif  ID_PICTURE_Property
#ifndef ID_BUTTON_camera
#define ID_BUTTON_camera		"ID_BUTTON_camera"
#endif  ID_BUTTON_camera
#ifndef ID_EDIT_NAME
#define ID_EDIT_NAME		"ID_EDIT_NAME"
#endif  ID_EDIT_NAME
#ifndef ID_CHECKBOX_BOY
#define ID_CHECKBOX_BOY		"ID_CHECKBOX_BOY"
#endif  ID_CHECKBOX_BOY
#ifndef ID_CHECKBOX_GIRL
#define ID_CHECKBOX_GIRL		"ID_CHECKBOX_GIRL"
#endif  ID_CHECKBOX_GIRL
#ifndef ID_BUTTON_HAIR_L
#define ID_BUTTON_HAIR_L		"ID_BUTTON_HAIR_L"
#endif  ID_BUTTON_HAIR_L
#ifndef ID_BUTTON_HAIR_R
#define ID_BUTTON_HAIR_R		"ID_BUTTON_HAIR_R"
#endif  ID_BUTTON_HAIR_R
#ifndef ID_BUTTON_HEAD_R
#define ID_BUTTON_HEAD_R		"ID_BUTTON_HEAD_R"
#endif  ID_BUTTON_HEAD_R
#ifndef ID_BUTTON_HEAD_L
#define ID_BUTTON_HEAD_L		"ID_BUTTON_HEAD_L"
#endif  ID_BUTTON_HEAD_L
#ifndef ID_CHECKBOX_Robber
#define ID_CHECKBOX_Robber		"ID_CHECKBOX_Robber"
#endif  ID_CHECKBOX_Robber
#ifndef ID_CHECKBOX_Warrior
#define ID_CHECKBOX_Warrior		"ID_CHECKBOX_Warrior"
#endif  ID_CHECKBOX_Warrior
#ifndef ID_CHECKBOX_Archer
#define ID_CHECKBOX_Archer		"ID_CHECKBOX_Archer"
#endif  ID_CHECKBOX_Archer
#ifndef ID_CHECKBOX_Taoist
#define ID_CHECKBOX_Taoist		"ID_CHECKBOX_Taoist"
#endif  ID_CHECKBOX_Taoist
#ifndef ID_CHECKBOX_Wizard
#define ID_CHECKBOX_Wizard		"ID_CHECKBOX_Wizard"
#endif  ID_CHECKBOX_Wizard
#ifndef ID_TEXT_ProInfo
#define ID_TEXT_ProInfo		"ID_TEXT_ProInfo"
#endif  ID_TEXT_ProInfo
#ifndef ID_TEXT_Property
#define ID_TEXT_Property		"ID_TEXT_Property"
#endif  ID_TEXT_Property
#ifndef ID_TEXT_ProName
#define ID_TEXT_ProName		"ID_TEXT_ProName"
#endif  ID_TEXT_ProName

#ifndef ID_CHECKBOX_WOMAN
#define ID_CHECKBOX_WOMAN		"ID_CHECKBOX_WOMAN"
#endif  ID_CHECKBOX_WOMAN

#ifndef ID_CHECKBOX_MAN
#define ID_CHECKBOX_MAN		"ID_CHECKBOX_MAN"
#endif  ID_CHECKBOX_MAN

#ifndef ID_BUTTON_CheckName
#define ID_BUTTON_CheckName	"ID_BUTTON_CheckName"
#endif ID_BUTTON_CheckName


#endif //__UICHARCREATEDEFINE_H__
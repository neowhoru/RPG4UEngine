/*////////////////////////////////////////////////////////////////////////
文 件 名：UIItemEnchant.h
DOC 文件：uidata\UIItemEnchant.uidoc
UI  名称：ItemEnchant
创建日期：2008年6月18日
最后更新：2008年6月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIITEMENCHANT_H__
#define __UIITEMENCHANT_H__
#pragma once

#include "VUCtrlInclude.h"
#include "UIItemEnchantDefine.h"
#include "UIListener.h"
#include "UIItemInlay.h"

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ItemEnchantUIListener : public ItemInlayUIListener
{
public:
	ItemEnchantUIListener();
	~ItemEnchantUIListener();


	//虚函数
	virtual LPCSTR GetName();
	virtual EGameUIType GetType();

	virtual void SetData(DWORD dwType,LPARAM dwData);
	virtual BOOL GetData(DWORD /*dwType*/,void* /*pRet*/);
	virtual void Refresh(BOOL bExtra=FALSE);
	virtual void TriggerFunc(ETriggerData eData,LPARAM lpData=0);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIItemEnchant : public UIItemInlay
{
public:	
	// Frame
	UIItemEnchant();
	~UIItemEnchant();

public:	
	// Member
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	BOOL ID_BUTTON_CLOSEOnButtonClick 		(VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_HELPOnButtonClick  		(VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CANCELOnButtonClick		(VUCtrlObject* /*pSender*/ );
	BOOL ID_LISTIMG_RESULTOnIconDropTo		(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/);
	BOOL ID_LISTIMG_RESULTOnIconLDBClick 	(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/);
	BOOL ID_LISTIMG_RESULTOnIconRButtonUp	(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ );
	BOOL ID_LISTIMG_MATERIALOnIconDropTo 	(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/);
	BOOL ID_LISTIMG_MATERIALOnIconLDBClick  (VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/);
	BOOL ID_LISTIMG_MATERIALOnIconRButtonUp (VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ );
	BOOL ID_BUTTON_CLEAROnButtonClick 		(VUCtrlObject* /*pSender*/ );
	BOOL ID_LISTIMG_SUBRESULTOnIconDropTo	(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/);
	BOOL ID_LISTIMG_SUBRESULTOnIconLDBClick (VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/);
	BOOL ID_LISTIMG_SUBRESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ );
	void ID_CHECKBOX_RATEOnCheckBoxCheck	(VUCtrlObject* pSender, BOOL*						pbChecked);
	//BOOL ID_BUTTON_REMOVEOnButtonClick		(VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ENCHANTOnButtonClick 	(VUCtrlObject* /*pSender*/ );
	BOOL ID_LISTIMG_SOCKETSOnIconDropTo  	(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/);
	BOOL ID_LISTIMG_SOCKETSOnIconLDBClick	(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/);
	BOOL ID_LISTIMG_SOCKETSOnIconRButtonUp  (VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ );
	void ID_EDIT_NAMEOnEditEnter				(VUCtrlObject* /*pSender*/, LPCSTR 				/*szData*/ );


public:	
	BOOL LoadUI();					// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();				// 卸载UI
	BOOL IsVisible();				// 是否可见
	void SetVisible( const BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible();
	void CloseWindow();

	void Refresh();
	void RefreshSockets();
	void RefreshComposite();

public:	
	virtual BOOL StartAction();
	virtual void OnItemCompoundFinished(MSG_OBJECT_BASE * pRecvPacket);
	virtual void DoChangeTargetState	(ItemSlot* pItemSlot);
public:
	virtual void ChangeTargetItem		(BaseSlot* pSlot, BOOL bAdd);
	virtual void ChangeSubResultItem	(BaseSlot* pSlot, BOOL bAdd);
	virtual void ChangeMaterialItem	(BaseSlot* pSlot, BOOL bAdd);

protected:
	VUCtrlFrame*			m_pID_FRAME_ITEMENCHANT;

	//VUCtrlButton  		*m_pID_BUTTON_CLOSE;
	//VUCtrlButton  		*m_pID_BUTTON_HELP;
	//VUCtrlButton  		*m_pID_BUTTON_CANCEL;
	//VUCtrlListImg 		*m_pID_LISTIMG_RESULT;
	//VUCtrlPicture 		*m_pID_PICTURE_MODEL;
	//VUCtrlListImg 		*m_pID_LISTIMG_MATERIAL;
	//VUCtrlButton  		*m_pID_BUTTON_CLEAR;
	//VUCtrlText 			*m_pID_TEXT_MONEY;
	//VUCtrlListImg 		*m_pID_LISTIMG_SUBRESULT;
	//VUCtrlCheckBox		*m_pID_CHECKBOX_RATE0;
	//VUCtrlCheckBox		*m_pID_CHECKBOX_RATE1;
	//VUCtrlCheckBox		*m_pID_CHECKBOX_RATE2;
	//VUCtrlCheckBox		*m_pID_CHECKBOX_RATE3;
	//VUCtrlButton  		*m_pID_BUTTON_REMOVE;
	//VUCtrlButton  		*	m_pID_BUTTON_ENCHANT;
	//VUCtrlEdit 			*m_pID_EDIT_NAME;
	VUCtrlText 				*m_pID_TEXT_ENCHANT_LEVEL;
	#define m_pID_BUTTON_ENCHANT	m_pID_BUTTON_INLAY
};

};//namespace ui


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(gameui::UIItemEnchant , UIItemEnchant , API_NULL);
//extern _CLIENTMAIN_API UIItemEnchant& singleton::GetUIItemEnchant();
#define theUIItemEnchant  singleton::GetUIItemEnchant()

#endif //__UIITEMENCHANT_H__


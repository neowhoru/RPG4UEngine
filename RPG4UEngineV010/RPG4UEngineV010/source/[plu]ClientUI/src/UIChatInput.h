/*////////////////////////////////////////////////////////////////////////
文 件 名：UIChatInput.h
创建日期：2007年12月24日
最后更新：2007年12月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UICHATINFOBOX_H__
#define __UICHATINFOBOX_H__
#pragma once


#include "GameDefine.h"
#include "VUCtrlInclude.h"
#include "UIChatLog.h"
#include "VUIControl.h"
#include <time.h>
#include "UIChatInputDefine.h"


using namespace std;
using namespace util;

class ScriptWord;
typedef clock_t			TIMER_TYPE;
typedef vector<string>					PrivateInfoArray;
typedef PrivateInfoArray::iterator	PrivateInfoArrayIt;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIChatInput : public VUIControl
{
public:
	enum CHAT_TYPE			//对应发送
	{
		CHANNEL_NUM = gameui::CHANNEL_SYSTEM+1
	};

public:
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//构造函数
	 UIChatInput();
	 virtual ~UIChatInput(){}

 	/////////////////////////////////////////////////////////////////////////////////////////////////////
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//	输入框
	void ID_EDIT_INPUTOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );

	BOOL ID_BUTTON_ChatGuildOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ChatGroupOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ChatPublicOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ChatPrivateOnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL ID_BUTTON_MoreHieghtOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_LessHightOnButtonClick( VUCtrlObject* /*pSender*/ );

	void ID_CHECKBOX_NormalOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_TeamOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_GuildOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_PlayerOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );

	BOOL ID_BUTTON_ChatOutTypeOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_TurnPageOnButtonClick( VUCtrlObject* /*pSender*/ );


	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//	类管理
	BOOL LoadUI();									// 载入UI
	BOOL InitControls();						// 关连控件
	BOOL UnLoadUI();								// 卸载UI
	BOOL IsVisible();								// 是否可见

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	//功能函数
public:
	void	SetInputText		(LPCSTR  szText);
	void	SetChatSendType	(LPCSTR  szText);

	void	Printf	( LPCSTR  string, ... );
	void	AddInfo	( const string& str, COLOR color = 0xffffffff, UINT nType = gameui::CHANNEL_SYSTEM, char *szToName = NULL );

	BOOL	IsEditInputVisable();

	LPCSTR 	GetPrivatenName()		{ return m_szPrivateTarget; }
	void	AutoSwitchChatChannel();	

	void	BeginPrivateChat( LPCSTR  szStr);

	BOOL	MsgProc					( UINT msg, WPARAM wParam, LPARAM lParam, BOOL bMsgUsed );
	void	AddPrivateUser			(LPCSTR szUser);			//添加私聊对象

	BOOL ProcessCommandHighShout	(ScriptWord& words);
	BOOL ProcessCommandShout		(ScriptWord& words);
	BOOL ProcessCommandPrivate		(ScriptWord& words);
	BOOL ProcessCommandGuild		(ScriptWord& words);
	BOOL ProcessCommandParty		(ScriptWord& words);


public:
	void	_SetupWidget		();
	void	_SetChatShowType	(BOOL bShow);
	void	_BeginInput			();
	void	_ReadyPrivate		(LPCSTR  szName);
	void	_SetInputWidget	( BOOL bShow = TRUE);
	
	void	SetEditFocus();


	///////////////////////////////////////////////
	//--快捷聊天信息操作
	void		_LoadShortcutInfo	(LPCSTR  szPath);
	void		_WriteShortcutInfo(LPCSTR  szPath);
	void		_WriteShortcutInfo(int nID, LPCSTR  szStr);
	LPCSTR  _GetShortcutInfo	(int nID);
	

	///////////////////////////////////////////////
	static	void EditInputOnTab( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/);


	BOOL loadXmlChatColdTime(LPCSTR szSetting);	//加载频道冷却时间 从xml文件读取

public:
	int	GetDefalutChannel() const				{ return m_nChannelDefault; }
	int	GetCurrentChannel()	const				{ return m_nChannelCurrent; }
	void	SetCurrentChannel(int nChannle);

public:
	string			m_ChatHistory[LAST_INFO_MAX];
	int				m_nCurrentLatest;				
	int				m_nCurrentPrivateAmount;		
	POINT				m_ptMouseMove;
	int				m_nRepeatCount;	

	StringHandle		m_szPrivateTarget;	//保存私聊对象名字
	PrivateInfoArray	m_PrivateInfos;	//私聊名字列表
	BOOL					m_bEditActive;		//记录输入框时候激活

	string					m_strPath;		
	map<int, string>		m_ShortcutInfos;	//保存快捷聊天信息 0-9 
	BOOL						m_bActiveInput;	

	//频道的控制
	int						m_nChannelDefault;
	int						m_nChannelCurrent;

	Timer			m_NormalLastTimer;
	Timer			m_GuildLastTimer;
	Timer			m_TeamLastTimer;
	Timer			m_PrivateLastTimer;
	Timer			m_ShoutLastTimer;		//大喊 9宫格
	Timer			m_HighShoutLastTimer;	//高喊



	//--------------------------

	string		m_strNormalInfo;	
	string		m_strGuildInfo;	
	string		m_strTeamInfo;		
	string		m_strPrivateInfo;	
	string		m_strShoutInfo;	
	string		m_strHighShoutInfo;	
	//-----------------------------------------	


public:	
	/////////////////////////////////////////////////////////////////////////////////////////////////////
	 VUCtrlFrame*	m_pChatInfoBox;
	 VUCtrlList*	m_pID_INFOLIST;
	 VUCtrlEdit*	m_pID_EDIT_INPUT;
	 VUCtrlButton*	m_pID_BUTTON_ChatGuild;
	 VUCtrlButton*	m_pID_BUTTON_ChatGroup;
	 VUCtrlButton*	m_pID_BUTTON_ChatPublic;
	 VUCtrlButton*	m_pID_BUTTON_ChatPrivate;
	 VUCtrlButton*	m_pID_BUTTON_TurnPage;
	 VUCtrlCheckBox*	m_pID_CHECKBOX_Normal;
	 VUCtrlCheckBox*	m_pID_CHECKBOX_Team;
	 VUCtrlCheckBox*	m_pID_CHECKBOX_Guild;
	 VUCtrlCheckBox*	m_pID_CHECKBOX_Player;
	 VUCtrlButton*		m_pID_BUTTON_ChatOutType;
	 VUCtrlPicture*	m_pID_PICTURE_Ground;
	 VUCtrlPicture*	m_nPic_EditBG;	
	 VUCtrlButton*		m_pID_BUTTON_Face;


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UIChatInput , _CLIENTUI_API);
//extern _CLIENTUI_API UIChatInput& GetUIChatInput();
#define theUIChatInput GetUIChatInput()


_CLIENTUI_API BOOL ChatMsgProc(UINT msg, WPARAM wParam, LPARAM lParam, BOOL bMsgUsed);
_CLIENTUI_API BOOL BUTTON_ROLLMsgPorc( UINT msg, WPARAM wParam, LPARAM lParam, BOOL bMsgUsed );


#endif //__UICHATINFOBOX_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：UICharCreate.h
创建日期：2007年11月14日
最后更新：2007年11月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UICHARCREATE_H__
#define __UICHARCREATE_H__
#pragma once


#include "VUCtrlInclude.h"
#include "UICharCreateDefine.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UICharCreate
{
public:
	UICharCreate();

public:
	BOOL HairColorOnButtonClick( OUT VUCtrlObject* pSender, OUT IN IconDragListImg* pItem );


	// Frame
	BOOL  OnFrameMove(DWORD dwTick);
	BOOL  OnRender(DWORD dwTick);

	// Button
	BOOL  ID_BUTTON_HEAD_LOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_HEAD_ROnButtonClick( VUCtrlObject* /*pSender*/ );

	void  ID_EDIT_NAMEOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	void  ID_EDIT_NAMEOnEditUpate( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	// CheckBox
	void  ID_CHECKBOX_BOYOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * bChecked );
	void  ID_CHECKBOX_GIRLOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * bChecked );

	// Button
	BOOL  ID_BUTTON_TURN_LOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_TRUN_ROnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_HAIR_LOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_HAIR_ROnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_CLOTH_LOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_CLOTH_ROnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL  ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL  ID_LISTIMG_HairColorOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItemDrag, IconDragListImg* pItemDest );
	BOOL  ID_LISTIMG_HairColorOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL  BtnCheckNameOnClick(VUCtrlObject* /*pSender*/ );


	BOOL ID_BUTTON_cameraOnButtonClick( VUCtrlObject* /*pSender*/ );

	void  ID_CHECKBOX_RobberOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );
	void  ID_CHECKBOX_WarriorOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );
	void  ID_CHECKBOX_ArcherOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );
	void  ID_CHECKBOX_TaoistOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );
	void  ID_CHECKBOX_WizardOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );

	void  ID_CHECKBOX_WOMANOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );
	void  ID_CHECKBOX_MANOnCheckBoxCheck( VUCtrlObject* pSender,  BOOL * pbChecked  );

public:
	BOOL  OnCheckCharType	(ePLAYER_TYPE charType);

public:
	BOOL  LoadUI();				// 载入UI
	BOOL  InitControls();	// 关连控件
	BOOL  UnLoadUI();			// 卸载UI
	BOOL  IsVisible();			// 是否可见
	void  SetVisible( const  BOOL  bVisible );			// 设置是否可视
	void  Refresh(  BOOL  bUseDefault );

	HIMC GetImeInstance();
	void  SetImeInstance(HIMC hIme);


public:
	void ShowWindow	(BOOL bShow);

public:
	// Member
	VUCtrlFrame*	m_pID_FRAME_CREATE;
	VUCtrlButton*	m_pID_BUTTON_TURN_L;
	VUCtrlButton*	m_pID_BUTTON_TRUN_R;
	VUCtrlButton*	m_pID_BUTTON_CLOTH_L;
	VUCtrlButton*	m_pID_BUTTON_CLOTH_R;
	VUCtrlButton*	m_pID_BUTTON_OK;
	VUCtrlButton*	m_pID_BUTTON_CANCEL;
	VUCtrlListImg*	m_pID_LISTIMG_HairColor;
	VUCtrlPicture*	m_pID_PICTURE_Property;
	VUCtrlEdit*		m_pID_EDIT_NAME;
	VUCtrlCheckBox*	m_pID_CHECKBOX_BOY;
	VUCtrlCheckBox*	m_pID_CHECKBOX_GIRL;
	VUCtrlButton*		m_pID_BUTTON_HAIR_L;
	VUCtrlButton*		m_pID_BUTTON_HAIR_R;
	VUCtrlButton*		m_pID_BUTTON_HEAD_R;
	VUCtrlButton*		m_pID_BUTTON_HEAD_L;
	VUCtrlText*			m_pID_TEXT_ProInfo;
	VUCtrlText*			m_pID_TEXT_Property;
	VUCtrlText*			m_pID_TEXT_ProName;
	VUCtrlButton*		m_pID_BUTTON_camera;
	VUCtrlButton*		m_pID_BUTTON_CheckName;

	VUCtrlCheckBox*	m_pID_CHECKBOX_WOMAN;
	VUCtrlCheckBox*	m_pID_CHECKBOX_MAN;

	union
	{
		VUCtrlCheckBox*	m_arCharTypes[PLAYERTYPE_NUM];
		struct
		{
	VUCtrlCheckBox*	m_pID_CHECKBOX_Warrior;
	VUCtrlCheckBox*	m_pID_CHECKBOX_Robber;
	VUCtrlCheckBox*	m_pID_CHECKBOX_Wizard;
	VUCtrlCheckBox*	m_pID_CHECKBOX_Archer;
	VUCtrlCheckBox*	m_pID_CHECKBOX_Taoist;
		};
	};

	HIMC					m_hIMC;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UICharCreate , API_NULL);
//extern API_NULL UICharCreate& GetUICharCreate();
#define theUICharCreate  GetUICharCreate()






#endif //__UICHARCREATE_H__
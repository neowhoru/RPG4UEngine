/*////////////////////////////////////////////////////////////////////////
文 件 名：UIWearHourse.h
创建日期：2008年6月8日
最后更新：2008年6月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UISHOP_H__
#define __UISHOP_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "VUCtrlInclude.h"
#include "ShopDialogDefine.h"

_NAMESPACEU(Player,			object);
class BaseSlot;
class BaseContainer;

const DWORD	MAX_SHOP_PAGE_NUM			= 5;
const DWORD	MAX_SLOT_PER_SHOPPAGE	= 4*6;
const DWORD	SHOP_PAGE_GROUP_ID		= TAG('ware');
const DWORD	METHOD_PAGE_GROUP_ID		= TAG('meth');

#ifndef ID_FRAME_SHOP
#define ID_FRAME_SHOP		"ID_FRAME_SHOP"
#endif  
#ifndef ID_BUTTON_CLOSE
#define ID_BUTTON_CLOSE		"ID_BUTTON_CLOSE"
#endif  
#ifndef ID_TEXT_MONEY
#define ID_TEXT_MONEY		"ID_TEXT_MONEY"
#endif  
#ifndef ID_LISTIMG_SHOP
#define ID_LISTIMG_SHOP		"ID_LISTIMG_SHOP"
#endif  
#ifndef ID_BUTTON_LOCK
#define ID_BUTTON_LOCK		"ID_BUTTON_LOCK"
#endif  
#ifndef ID_BUTTON_UNLOCK
#define ID_BUTTON_UNLOCK		"ID_BUTTON_UNLOCK"
#endif  
#ifndef ID_CHECKBOX_SELL
#define ID_CHECKBOX_SELL		"ID_CHECKBOX_SELL"
#endif  
#ifndef ID_CHECKBOX_BUY
#define ID_CHECKBOX_BUY		"ID_CHECKBOX_BUY"
#endif  
#ifndef ID_CHECKBOX_REPAIR
#define ID_CHECKBOX_REPAIR		"ID_CHECKBOX_REPAIR"
#endif  
#ifndef ID_BUTTON_REPAIRALL
#define ID_BUTTON_REPAIRALL		"ID_BUTTON_REPAIRALL"
#endif  

#ifndef ID_CHECKBOX_PAGE
#define ID_CHECKBOX_PAGE		"ID_CHECKBOX_PAGE"
#endif  


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UIShop
{
public:
	UIShop();

	// Member
public:	
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	BOOL ID_BUTTON_CLOSEOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_LISTIMG_SHOPOnIconDragOn		( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItemDrag, IconDragListImg* pItemSrc );
	BOOL ID_LISTIMG_SHOPOnIconLDBClick	( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL ID_LISTIMG_SHOPOnIconRButtonUp	( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	
	// CheckBox
	void PageOnCheckBoxCheck					( VUCtrlObject* pSender,  BOOL * bChecked );
	void MethodOnCheckBoxCheck					( VUCtrlObject* pSender,  BOOL * bChecked );

	BOOL ID_BUTTON_REPAIRALLOnButtonClick	( VUCtrlObject* /*pSender*/ );

	BOOL LoadUI					();				// 载入UI
	BOOL InitControls			();	// 关连控件
	BOOL UnLoadUI				();			// 卸载UI
	BOOL IsVisible				();			// 是否可见
	void SetVisible			( BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible	();
	void Refresh				();

	void SetMoney				(MONEY*	pMoney);
public:
	void OnInstallContainer	(BaseContainer* pContainer);
	void ChangePage			(UINT nPage);
	void ChangeMethod			(eSHOP_METHOD eMethod);
	void PushGoods				(BaseSlot* pSlot);
	void PopGoods				(BaseSlot* pSlot );

public:

	//////////////////////////////////////
	//Page
	VUCtrlCheckBox*	m_arCheckBoxPtrs[MAX_SHOP_PAGE_NUM];

	VUCtrlFrame*		m_pID_FRAME_SHOP;
	VUCtrlText*			m_pID_TEXT_MONEY;
	VUCtrlButton*		m_pID_BUTTON_CLOSE;
	VUCtrlButton*		m_pID_BUTTON_REPAIRALL;

	VUCtrlCheckBox*	m_arMethodPtrs[SHOP_METHOD_MAX];

	VUCtrlListImg*		m_pID_LISTIMG_SHOP;

protected:
	INT					m_nCurrentMethod;
	INT					m_nCurrentPage;
	VG_PTR_PROPERTY	(Player, Player);

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(UIShop , API_NULL);
//extern API_NULL UIShop& singleton::GetUIShop();
#define theUIShop  singleton::GetUIShop()


#endif //__UISHOP_H__


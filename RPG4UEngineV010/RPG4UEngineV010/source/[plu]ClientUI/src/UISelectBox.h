/*////////////////////////////////////////////////////////////////////////
文 件 名：UISelectBox.h
DOC 文件：uidata\UISelectBox.uidoc
UI  名称：SelectBox
创建日期：2008年7月2日
最后更新：2008年7月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UISELECTBOX_H__
#define __UISELECTBOX_H__
#pragma once

#include "VUCtrlInclude.h"
#include "UISelectBoxDefine.h"
#include "UIListener.h"

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class SelectBoxUIListener : public UIListener
{
public:
	SelectBoxUIListener();
	~SelectBoxUIListener();


public:
	//虚函数
	virtual LPCSTR GetName();
	virtual EGameUIType GetType();

	virtual void SetData(DWORD dwType,LPARAM dwData);
	virtual BOOL GetData(DWORD /*dwType*/,void* /*pRet*/);
	virtual void Refresh(BOOL bExtra=FALSE);

	void ShowSelectBox	(LPCSTR                    szText
								,const sUILIST_ITEM*			pItem
								,INT                       nItemCount
								,LPCSTR                    szCaption	=""
								,BOOL                      bModal		=TRUE
								,void*					      procCallback=NULL
								,void*                     pData			=NULL
								,INT                       nDataLength	=0);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UISelectBox
{
public:	
	// Frame
	UISelectBox();
	~UISelectBox();

public:	
	// Member
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	BOOL ID_BUTTON_OKOnButtonClick 			(VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CLOSEOnButtonClick 		(VUCtrlObject* /*pSender*/ );

	void ID_LIST_SELECTOnListSelectChange	(VUCtrlObject* /*pSender*/, sUILIST_ITEM*	/*pItem*/);
	void ID_TEXT_HyperLinkClick				(VUCtrlObject* pSender, LPCSTR szData);


public:	
	BOOL LoadUI();					// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();				// 卸载UI
	BOOL IsVisible();				// 是否可见
	void SetVisible( const BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible();

	void Refresh();

public:

	void Show	(LPCSTR                    szText
					,const sUILIST_ITEM*			pItem
					,INT                       nItemCount
					,LPCSTR                    szCaption	=""
					,BOOL                      bModal		=TRUE
					,PROC_CALLBACK_SELECTBOX	procCallback=NULL
					,void*                     pData			=NULL
					,INT                       nDataLength	=0);


protected:
	BOOL _PopSelectBox();

protected:
	VUCtrlFrame*		m_pID_SelectBoxFrame;

	VUCtrlButton		*m_pID_BUTTON_OK;
	VUCtrlText  		*m_pID_TEXT_CAPTION;
	VUCtrlText  		*m_pID_TEXT_Info;
	VUCtrlList  		*m_pID_LIST_SELECT;
	VUCtrlButton		*m_pID_BUTTON_CLOSE;

	PROC_CALLBACK_SELECTBOX		m_ProcCallback;
	void*								m_pData;
	vector<sSELECTBOX>			m_SelectBoxInfos;

};

};//namespace ui


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(gameui::UISelectBox , UISelectBox , API_NULL);
//extern _CLIENTMAIN_API UISelectBox& singleton::GetUISelectBox();
#define theUISelectBox  singleton::GetUISelectBox()
//#define s_UI_SelectBox  GetUI_SelectBox()

#endif //__UISELECTBOX_H__


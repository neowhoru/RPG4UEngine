/********************************************************************
	Created by UIEditor.exe
	FileName: E:\Code\RunTime\CLIENT\Data\UI\Hotkey.h
*********************************************************************/
#pragma once
#include "../../HRUI/HR_UI_Include.h"

#ifndef ID_FRAME_HOTKEY
#define ID_FRAME_HOTKEY		"ID_FRAME_HOTKEY"
#endif  ID_FRAME_HOTKEY
#ifndef ID_LISTIMG_Hotkey
#define ID_LISTIMG_Hotkey		"ID_LISTIMG_Hotkey"
#endif  ID_LISTIMG_Hotkey

namespace UI_ID_FRAME_HOTKEY
{
	// Member
	extern CHR_UI_Frame*	m_pID_FRAME_HOTKEY;
	extern CHR_UI_ListImg*	m_pID_LISTIMG_Hotkey;

	// Frame
	bool ID_FRAME_HOTKEYOnFrameRun();
	bool ID_FRAME_HOTKEYOnFrameRender();
	// ListImg / ListEx
	bool ID_LISTIMG_HotkeyOnIconDragOn( CHR_UI_Object* pSender, CHR_UI_Object* pMe,
											CHR_UI_IconDrag::S_ListImg* pItemDrag,
											CHR_UI_IconDrag::S_ListImg* pItemSrc );
	//bool ID_LISTIMG_HotkeyOnIconDragOff( CHR_UI_Object* pSender, CHR_UI_Object* pMe,
	//										CHR_UI_IconDrag::S_ListImg* pItem );
	bool ID_LISTIMG_HotkeyOnIconLDBClick( CHR_UI_Object* pSender, CHR_UI_IconDrag::S_ListImg* pItem );
	bool ID_LISTIMG_HotkeyOnIconRButtonUp( CHR_UI_Object* pSender, CHR_UI_IconDrag::S_ListImg* pItem );

	bool LoadUI();				// 载入UI
	bool DoControlConnect();	// 关连控件
	bool UnLoadUI();			// 卸载UI
	bool IsVisable();			// 是否可见
	void SetVisable( const bool bVisable );			// 设置是否可视

	void Refeash();
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBankUI.h
创建日期：2007年11月4日
最后更新：2007年11月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIBANKUI_H__
#define __UIBANKUI_H__
#pragma once

#include "VUCtrlInclude.h"

#ifndef ID_FRAME_BANK
#define ID_FRAME_BANK		"ID_FRAME_BANK"
#endif  ID_FRAME_BANK
#ifndef ID_TEXT_CURR_COIN
#define ID_TEXT_CURR_COIN		"ID_TEXT_CURR_COIN"
#endif  ID_TEXT_CURR_COIN
#ifndef ID_TEXT_GOLD_MAX
#define ID_TEXT_GOLD_MAX		"ID_TEXT_GOLD_MAX"
#endif  ID_TEXT_GOLD_MAX
#ifndef ID_BUTTON_Deposit
#define ID_BUTTON_Deposit		"ID_BUTTON_Deposit"
#endif  ID_BUTTON_Deposit
#ifndef ID_BUTTON_Get
#define ID_BUTTON_Get		"ID_BUTTON_Get"
#endif  ID_BUTTON_Get
#ifndef ID_BUTTON_CLOSE
#define ID_BUTTON_CLOSE		"ID_BUTTON_CLOSE"
#endif  ID_BUTTON_CLOSE
#ifndef ID_TEXT_CURR_GOLD
#define ID_TEXT_CURR_GOLD		"ID_TEXT_CURR_GOLD"
#endif  ID_TEXT_CURR_GOLD
#ifndef ID_BUTTON_UPDATE
#define ID_BUTTON_UPDATE		"ID_BUTTON_UPDATE"
#endif  ID_BUTTON_UPDATE
#ifndef ID_TEXT_COIN_MAX
#define ID_TEXT_COIN_MAX		"ID_TEXT_COIN_MAX"
#endif  ID_TEXT_COIN_MAX

class _CLIENTUI_API CUI_ID_FRAME_BANK
{
public:
	CUI_ID_FRAME_BANK();
	// Member
public:	
	VUCtrlFrame*	m_pID_FRAME_BANK;
	VUCtrlText*	m_pID_TEXT_CURR_COIN;
	VUCtrlText*	m_pID_TEXT_GOLD_MAX;
	VUCtrlButton*	m_pID_BUTTON_Deposit;
	VUCtrlButton*	m_pID_BUTTON_Get;
	VUCtrlButton*	m_pID_BUTTON_CLOSE;
	VUCtrlText*	m_pID_TEXT_CURR_GOLD;
	VUCtrlButton*	m_pID_BUTTON_UPDATE;
	VUCtrlText*	m_pID_TEXT_COIN_MAX;

	// Frame
	BOOL OnFrameRun(DWORD dwTick);
	BOOL OnFrameRender(DWORD dwTick);
	BOOL ID_BUTTON_DepositOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_GetOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_UPDATEOnButtonClick( VUCtrlObject* pSender );
    
	DWORD		m_UPGTADEMONEY;
	DWORD		m_MONEY_ON_BANK;
	void Refresh();
	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL(CUI_ID_FRAME_BANK , API_NULL);
#else
extern API_NULL CUI_ID_FRAME_BANK& GetCUI_ID_FRAME_BANK();
#endif
#define s_CUI_ID_FRAME_BANK  GetCUI_ID_FRAME_BANK()

#endif //__UIBANKUI_H__
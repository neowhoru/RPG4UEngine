/*////////////////////////////////////////////////////////////////////////
文 件 名：UITipLog.h
DOC 文件：uidata\UITipLog.uidoc
UI  名称：TipLog
创建日期：2008年7月31日
最后更新：2008年7月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UITIPLOG_H__
#define __UITIPLOG_H__
#pragma once

#include "VUCtrlInclude.h"
#include "UITipLogDefine.h"
#include "UIListener.h"
#include "UITipLogItem.h"
#include "TipLogDefine.h"
#include "TipLogItemManager.h"

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class TipLogUIListener	: public UIListener
{
public:
	TipLogUIListener();
	~TipLogUIListener();


	//虚函数
	virtual LPCSTR GetName();
	virtual EGameUIType GetType();

	virtual void SetData(DWORD dwType,LPARAM dwData);
	virtual BOOL GetData(DWORD /*dwType*/,void* /*pRet*/);
	virtual void TriggerFunc(ETriggerData eData,LPARAM lpData=0);
	virtual void Refresh(BOOL bExtra=FALSE);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

const DWORD TIPITEM_ID		= TAG('tip');
const DWORD TIPITEMBTN_ID	= TAG('tipb');

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UITipLog : public TipLogItemManager
{
public:	
	// Frame
	UITipLog();
	~UITipLog();

public:	
	// Member
	BOOL OnFrameMove		(DWORD dwTick);
	BOOL OnRender	(DWORD dwTick);

	BOOL ID_BUTTON_TIPOnButtonClick	(VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_TIPOnRButtonClick	(VUCtrlObject* /*pSender*/ );


public:	
	BOOL LoadUI				();					// 载入UI
	BOOL UnLoadUI			();					// 卸载UI

	BOOL InitControls();					// 关连控件
	void Refresh			();

	BOOL IsVisible			();					// 是否可见
	void SetVisible		( BOOL bVisible );// 设置是否可视
	void SetLayoutVisible();


public:
	/////////////////////////////////////////
	//TipLogItemManager实现函数
	void	SetLogTip		(TipLogItem*	pFrame
								,LPCSTR			szTip
                        ,sTIP_SETTING*	pSetting
								,PROC_TIPLOG	func
								,BOOL				bAgree
								,LPCSTR			szTip2);

	//TipLogItem*		PopTipItem	();

	//void  ReadyTip2List	(TipLogItem* pReady);
	void  UpdateTipList	(TipLogItem* pTipItem,BOOL bReadyTip);

	void	ProcessEvent	(VUCtrlObject* pSender, BOOL bAgree);

protected:
	TipLogItem*		_CreateTipItem		(INT nIndex);
	void				_DestroyTipItem	(TipLogItem* pItem);
	void				_GetLogTipPos	(POINT& ptStart
									      ,POINT& ptDest);
	void				_OnLogTip		(const POINT& ptStart
                                 ,const POINT& ptDest);

	void				_OnReadyTip2List	(TipLogItem* pReady);
	void				_OnUpdateTipList	(TipLogItem* pTipItem, BOOL bShow);


protected:
	VUCtrlFrame*		m_pID_FRAME_TIPLOG;

	VUCtrlPicture		*m_pID_TIPSTART;
	VUCtrlPicture		*m_pID_TIPPOINT;
	VUCtrlPicture		*m_pID_TIPEND;
	//VUCtrlText			*m_pID_TEXT_BACK;
	//VUCtrlText			*m_pID_TEXT_TIP;
	//VUCtrlButton 		*m_pID_BUTTON_TIP;

};

};//namespace ui


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(gameui::UITipLog , UITipLog , API_NULL);
//extern _CLIENTMAIN_API UITipLog& singleton::GetUITipLog();
#define theUITipLog  singleton::GetUITipLog()

#endif //__UITIPLOG_H__


/*////////////////////////////////////////////////////////////////////////
文 件 名：UIInputBox.h
创建日期：2007年8月5日
最后更新：2007年8月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIINPUTBOX_H__
#define __UIINPUTBOX_H__
#pragma once


#include "VUCtrlInclude.h"

#ifndef ID_InputBox
#define ID_InputBox		"ID_InputBox"
#endif  ID_InputBox

#ifndef ID_EDIT_INPUT
#define ID_EDIT_INPUT		"ID_EDIT_INPUT"
#endif  ID_EDIT_INPUT

#ifndef ID_BUTTON_OK
#define ID_BUTTON_OK		"ID_BUTTON_OK"
#endif  ID_BUTTON_OK

#ifndef ID_BUTTON_CANCEL
#define ID_BUTTON_CANCEL		"ID_BUTTON_CANCEL"
#endif  ID_BUTTON_CANCEL

#ifndef ID_TEXT_CAPTION
#define ID_TEXT_CAPTION		"ID_TEXT_CAPTION"
#endif  ID_TEXT_CAPTION

#ifndef ID_BUTTON_UP
#define ID_BUTTON_UP		"ID_BUTTON_UP"
#endif  ID_BUTTON_UP

#ifndef ID_BUTTON_DOWN
#define ID_BUTTON_DOWN		"ID_BUTTON_DOWN"
#endif  ID_BUTTON_DOWN

using namespace std;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UIInputBox
{
public:
	UIInputBox();

public:
	typedef  BOOL (*PROC_BOX_CALLBACK)( LPCSTR  szInputData,  void *pData );

	///////////////////////////////////////////////////
	struct sINPUTBOX
	{
		sINPUTBOX()
		{
			
			m_bModal			= TRUE;
			m_ProcCallback= NULL;
			m_pData			= NULL;
			m_bNumber		= TRUE;
			m_bPassword		= FALSE;
			m_nDataSize		= 0;
			
		}
		string		m_strCaption;
		BOOL			m_bModal;
		BOOL			m_bNumber;
		BOOL			m_bPassword;
		UIInputBox::PROC_BOX_CALLBACK	m_ProcCallback;
		void	*		m_pData;
		UINT			m_nDataSize;
	};


public:
	void Show	(LPCSTR            szCaption
               ,BOOL              bNumber=TRUE
					,BOOL              bModal=TRUE
					,PROC_BOX_CALLBACK procCallback=NULL
					,void*             pData=NULL
					,int               nDataLength=0
					,BOOL              bPassword=FALSE);

	void Show	(DWORD             dwCaptionID
               ,BOOL              bNumber=TRUE
					,BOOL              bModal=TRUE
					,PROC_BOX_CALLBACK procCallback=NULL
					,void*             pData=NULL
					,int               nDataLength=0
					,BOOL              bPassword=FALSE);


	// Frame
	BOOL OnFrameMove	(DWORD dwTick);
	BOOL OnRender		(DWORD dwTick);

	// Edit
	void ID_EDIT_INPUTOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	BOOL ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL ID_BUTTON_UPOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_DOWNOnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL IsEditInputVisable();


	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( const  BOOL bVisible );			// 设置是否可视

	void Refresh();


public:
	// Member
	VUCtrlFrame*	m_pID_INPUTBOX_FRAME;
	VUCtrlEdit*		m_pID_EDIT_INPUT;
	VUCtrlText*		m_pID_TEXT_CAPTION;
	VUCtrlButton*	m_pID_BUTTON_OK;
	VUCtrlButton*	m_pID_BUTTON_CANCEL;
	VUCtrlButton*	m_pID_BUTTON_UP;
	VUCtrlButton*	m_pID_BUTTON_DOWN;

	PROC_BOX_CALLBACK	m_ProcCallback;
	void*					m_pData;
	UINT					m_nDataSize;
	vector<sINPUTBOX> m_InputInfos;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UIInputBox , API_NULL);
//API_NULL UIInputBox& GetUIInputBox();
#define theUIInputBox  GetUIInputBox()



#endif //__UIINPUTBOX_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TaskUIListener.h
创建日期：2006年9月6日
最后更新：2006年9月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TASKUILISTENER_H__
#define __TASKUILISTENER_H__
#pragma once

#include "UIListener.h"
namespace gameui
{


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class TaskUIListener : public UIListener
{
public:
	TaskUIListener();
	~TaskUIListener();

	//虚函数
	virtual LPCSTR GetName();
	virtual EGameUIType GetType();

	//virtual void SetData(DWORD dwType,LPARAM dwData);
	//virtual BOOL GetData(DWORD dwType,void* pRet);
	virtual void Refresh(BOOL bExtra=FALSE);
	virtual void TriggerFunc(ETriggerData /*eData*/,LPARAM lpData=0);
};
};//namespace gameui



#endif //__TASKUILISTENER_H__
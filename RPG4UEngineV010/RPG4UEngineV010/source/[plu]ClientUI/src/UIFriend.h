/*////////////////////////////////////////////////////////////////////////
文 件 名：UIFriend.h
创建日期：2007年11月6日
最后更新：2007年11月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIFRIEND_H__
#define __UIFRIEND_H__
#pragma once

#include "VUCtrlInclude.h"
#include "TextResManager.h"
#include "VUIControl.h"

#ifndef ID_FRAME_Friend
#define ID_FRAME_Friend		"ID_FRAME_Friend"
#endif  ID_FRAME_Friend
#ifndef ID_LIST_FriendInfo
#define ID_LIST_FriendInfo		"ID_LIST_FriendInfo"
#endif  ID_LIST_FriendInfo
#ifndef ID_BUTTON_AddFriend
#define ID_BUTTON_AddFriend		"ID_BUTTON_AddFriend"
#endif  ID_BUTTON_AddFriend
#ifndef ID_BUTTON_DelFriend
#define ID_BUTTON_DelFriend		"ID_BUTTON_DelFriend"
#endif  ID_BUTTON_DelFriend
#ifndef ID_BUTTON_Close
#define ID_BUTTON_Close	"ID_BUTTON_Close"
#endif ID_BUTTON_Close

#ifndef ID_BUTTON_chat
#define ID_BUTTON_chat	"ID_BUTTON_chat"
#endif ID_BUTTON_chat
#ifndef ID_BUTTON_team
#define ID_BUTTON_team	"ID_BUTTON_team"
#endif ID_BUTTON_team
#ifndef ID_BUTTON_delete
#define ID_BUTTON_delete	"ID_BUTTON_delete"
#endif ID_BUTTON_delete


using namespace std;

/////////////////////////////////////////////////////
struct sFRIENDINFO
{
	string	m_strName;
	BOOL		m_bOnline;
};

typedef map<int, sFRIENDINFO>		FriendInfoMap;
typedef FriendInfoMap::iterator	FriendInfoMapIt;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIFriend : public VUIControl
{
public:	
	 UIFriend();
	 ~UIFriend();

public:	
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	void ID_LIST_FriendInfoOnListSelectChange( VUCtrlObject* pSender, sUILIST_ITEM* pItem );
	BOOL ID_BUTTON_AddFriendOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_DelFriendOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CloseOnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL ID_BUTTON_chatOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_teamOnButtonClick( VUCtrlObject* /*pSender*/ );

	static void ID_List_OnRBClick( VUCtrlObject* pSender, sUILIST_ITEM* pItem );

	/////////////////////////////////
	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible();
	void Refresh();


	//////////////////////////////////////
	int  GetFriendID	(LPCSTR  szName);
	BOOL CheckOnline	(LPCSTR  szName);
	BOOL CheckUseName	(LPCSTR  szName);


protected:
	FriendInfoMap	m_FirendInfos;


	// Member
public:	
	 VUCtrlFrame*	m_pID_FRAME_Friend;
	 VUCtrlList*	m_pID_LIST_FriendInfo;
	 VUCtrlButton*	m_pID_BUTTON_AddFriend;
	 VUCtrlButton*	m_pID_BUTTON_DelFriend;
	 VUCtrlButton* m_pID_BUTTON_Close;
	 VUCtrlButton* m_pID_BUTTON_chat;
	 VUCtrlButton* m_pID_BUTTON_team;

	VUCtrlButton*	m_pID_SendInfo;		//发送信息
	VUCtrlButton*	m_pID_RequestParty;	//邀请组队
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UIFriend , API_NULL);
//extern API_NULL UIFriend& GetUIFriend();
#define theUIFriend  GetUIFriend()


#endif //__UIFRIEND_H__
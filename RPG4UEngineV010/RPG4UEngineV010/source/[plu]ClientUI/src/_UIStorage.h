/*////////////////////////////////////////////////////////////////////////
文 件 名：UIStorage.h
创建日期：2007年12月15日
最后更新：2007年12月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __UISTORAGE_H__
#define __UISTORAGE_H__
#pragma once


#include "VUCtrlInclude.h"

#ifndef ID_FRAME_Storage
#define ID_FRAME_Storage		"ID_FRAME_Storage"
#endif  ID_FRAME_Storage
#ifndef ID_BUTTON_Close
#define ID_BUTTON_Close		"ID_BUTTON_Close"
#endif  ID_BUTTON_Close
#ifndef ID_BUTTON_Why
#define ID_BUTTON_Why		"ID_BUTTON_Why"
#endif  ID_BUTTON_Why
#ifndef ID_LIST_city
#define ID_LIST_city		"ID_LIST_city"
#endif  ID_LIST_city
#ifndef ID_LIST_price
#define ID_LIST_price		"ID_LIST_price"
#endif  ID_LIST_price
#ifndef ID_PICTURE_23489
#define ID_PICTURE_23489		"ID_PICTURE_23489"
#endif  ID_PICTURE_23489
#ifndef ID_LISTIMG_Main
#define ID_LISTIMG_Main		"ID_LISTIMG_Main"
#endif  ID_LISTIMG_Main
#ifndef ID_PICTURE_19060
#define ID_PICTURE_19060		"ID_PICTURE_19060"
#endif  ID_PICTURE_19060
#ifndef ID_PICTURE_2908
#define ID_PICTURE_2908		"ID_PICTURE_2908"
#endif  ID_PICTURE_2908
#ifndef ID_PICTURE_2909
#define ID_PICTURE_2909		"ID_PICTURE_2909"
#endif  ID_PICTURE_2909
#ifndef ID_BUTTON_LOCK
#define ID_BUTTON_LOCK		"ID_BUTTON_LOCK"
#endif  ID_BUTTON_LOCK
#ifndef ID_BUTTON_UNLOCK
#define ID_BUTTON_UNLOCK		"ID_BUTTON_UNLOCK"
#endif  ID_BUTTON_UNLOCK
#ifndef ID_BUTTON_ClearPwd
#define ID_BUTTON_ClearPwd		"ID_BUTTON_ClearPwd"
#endif  ID_BUTTON_ClearPwd


class _CLIENTUI_API CUI_ID_FRAME_Storage
{
public:
	CUI_ID_FRAME_Storage();
	// Member
public:	
	 VUCtrlFrame*	m_pID_FRAME_Storage;
	 VUCtrlButton*	m_pID_BUTTON_Close;
	 VUCtrlButton*	m_pID_BUTTON_Why;
	 VUCtrlList*	m_pID_LIST_city;
	 VUCtrlList*	m_pID_LIST_price;
	 VUCtrlPicture*	m_pID_PICTURE_23489;
	 VUCtrlListImg*	m_pID_LISTIMG_Main;
	 VUCtrlPicture*	m_pID_PICTURE_19060;
	 VUCtrlPicture*	m_pID_PICTURE_2908;
	 VUCtrlPicture*	m_pID_PICTURE_2909;
	 VUCtrlButton*	m_pID_BUTTON_LOCK;
	 VUCtrlButton*	m_pID_BUTTON_UNLOCK;
	 VUCtrlButton*	m_pID_BUTTON_ClearPwd;

	// Frame
	BOOL OnFrameRun(DWORD dwTick);
	BOOL OnFrameRender(DWORD dwTick);
	BOOL ID_BUTTON_CloseOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_WhyOnButtonClick( VUCtrlObject* pSender );
	void ID_LIST_cityOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem );
	void ID_LIST_priceOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem );
	BOOL ID_LISTIMG_MainOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe, IconDragListImg* pItemDrag, IconDragListImg* pItemDest );
	BOOL ID_LISTIMG_MainOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pMe, IconDragListImg* pItem );
	BOOL ID_LISTIMG_MainOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem );
	BOOL ID_LISTIMG_MainOnIconRDBClick( VUCtrlObject* pSender, IconDragListImg* pItem );
	BOOL ID_BUTTON_LOCKOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_UNLOCKOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_ClearPwdOnButtonClick( VUCtrlObject* pSender );

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
	void Refresh(); //刷新仓库信息
	void GetNpcSysID( unsigned int nId );
	DWORD m_dwChangeCount;
	unsigned short m_ustDstIndex;
	int nNpcID;
	BOOL m_bInUIFrameLock;
	BOOL m_bInUIFrameUnLock;
	char szItem_PrePWD[ggdat::PWD_OF_ITEM];
	char szItem_LastPWD[ggdat::PWD_OF_ITEM];
	char szItem_PWD[ggdat::PWD_OF_ITEM];
	int m_nNpcSysId;
	BOOL m_bQuestNpc;

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL(CUI_ID_FRAME_Storage , API_NULL);
#else
extern API_NULL CUI_ID_FRAME_Storage& GetCUI_ID_FRAME_Storage();
#endif
#define s_CUI_ID_FRAME_Storage  GetCUI_ID_FRAME_Storage()

#endif //__UISTORAGE_H__
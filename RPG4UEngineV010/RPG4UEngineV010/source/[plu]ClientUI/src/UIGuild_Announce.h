/********************************************************************
	Created by UIEditor.exe
	FileName: E:\Code\RunTime\CLIENT\Data\UI\Guild_Announce.h
*********************************************************************/
#pragma once
#include "../../HRUI/HR_UI_Include.h"

#ifndef ID_FRAME_Guild_Announce
#define ID_FRAME_Guild_Announce		"ID_FRAME_Guild_Announce"
#endif  ID_FRAME_Guild_Announce
#ifndef ID_EDIT_Info
#define ID_EDIT_Info		"ID_EDIT_Info"
#endif  ID_EDIT_Info
#ifndef ID_BUTTON_Info
#define ID_BUTTON_Info		"ID_BUTTON_Info"
#endif  ID_BUTTON_Info
#ifndef ID_BUTTON_Member
#define ID_BUTTON_Member		"ID_BUTTON_Member"
#endif  ID_BUTTON_Member
#ifndef ID_BUTTON_Mgr
#define ID_BUTTON_Mgr		"ID_BUTTON_Mgr"
#endif  ID_BUTTON_Mgr
#ifndef ID_BUTTON_CLOSE
#define ID_BUTTON_CLOSE		"ID_BUTTON_CLOSE"
#endif  ID_BUTTON_CLOSE

namespace UI_ID_FRAME_Guild_Announce
{
	// Member
	extern CHR_UI_Frame*	m_pID_FRAME_Guild_Announce;
	extern CHR_UI_Edit*	m_pID_EDIT_Info;
	extern CHR_UI_Button*	m_pID_BUTTON_Info;
	extern CHR_UI_Button*	m_pID_BUTTON_Member;
	extern CHR_UI_Button*	m_pID_BUTTON_Mgr;
	extern CHR_UI_Button*	m_pID_BUTTON_CLOSE;

	// Frame
	bool ID_FRAME_Guild_AnnounceOnFrameRun();
	bool ID_FRAME_Guild_AnnounceOnFrameRender();
	// Edit
	void ID_EDIT_InfoOnEditEnter( CHR_UI_Object* pSender, const char* szData );
	// Button
	bool ID_BUTTON_InfoOnButtonClick( CHR_UI_Object* pSender );
	// Button
	bool ID_BUTTON_MemberOnButtonClick( CHR_UI_Object* pSender );
	// Button
	bool ID_BUTTON_MgrOnButtonClick( CHR_UI_Object* pSender );
	// Button
	bool ID_BUTTON_CLOSEOnButtonClick( CHR_UI_Object* pSender );

	bool LoadUI();				// 载入UI
	bool DoControlConnect();	// 关连控件
	bool UnLoadUI();			// 卸载UI
	bool IsVisable();			// 是否可见
	void SetVisable( const bool bVisable );			// 设置是否可视
}

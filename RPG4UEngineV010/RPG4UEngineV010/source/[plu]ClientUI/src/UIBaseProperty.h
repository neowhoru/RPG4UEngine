/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBaseProperty.h
创建日期：2007年11月28日
最后更新：2007年11月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIBASEPROPERTY_H__
#define __UIBASEPROPERTY_H__
#pragma once

#include "UIBasePropertyDefine.h"
#include "VUCtrlInclude.h"
#include "VUIControl.h"

class PlayerAttributes;
namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIBaseProperty :public VUIControl
{
public:
	UIBaseProperty();
	// Member

public:
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	BOOL ID_BUTTON_CLOSEOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_UPGRADEOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ActionOnButtonClick	( VUCtrlObject* /*pSender*/ );
	//BOOL ID_BUTTON_PropertyOnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible();

	void Refresh			();				// Refresh

	void UpdateStateBtns	();
	BOOL ProcessStateBtn	(UINT nIndex);

public:	
	 VUCtrlFrame*	m_pID_FRAME_BaseProperty;
	 VUCtrlButton*	m_pID_BUTTON_CLOSE;
	 VUCtrlText*	m_pID_TEXT_Name;
	 VUCtrlText*	m_pID_TEXT_Title;
	 VUCtrlText*	m_pID_TEXT_Guild;
	 VUCtrlText*	m_pID_TEXT_Level;
	 VUCtrlText*	m_pID_TEXT_INT;
	 VUCtrlText*	m_pID_TEXT_VIT;
	 VUCtrlText*	m_pID_TEXT_STR;
	 VUCtrlText*	m_pID_TEXT_DEX;
	 VUCtrlText*	m_pID_TEXT_KilledNum;
	 VUCtrlText*	m_pID_TEXT_Work;
	 VUCtrlText*	m_pID_TEXT_Official;
	 VUCtrlText*	m_pID_TEXT_Marry;
	 VUCtrlText*	m_pID_TEXT_Distinction;
	 VUCtrlButton*	m_pID_BUTTON_Action;
	 VUCtrlText*	m_pID_TEXT_SPR;
	 VUCtrlText*	m_pID_TEXT_LRN;
	 VUCtrlText*	m_pID_TEXT_CRE;
	 VUCtrlText*	m_pID_TEXT_EXPERTY1;
	 VUCtrlText*	m_pID_TEXT_EXPERTY2;
	 VUCtrlText*	m_pID_TEXT_REMAINSTAT;

	 VUCtrlText*	m_pID_TEXT_MATTACK_FIRE;
	 VUCtrlText*	m_pID_TEXT_MDEF_FIRE;
	 VUCtrlText*	m_pID_TEXT_MATTACK_WATER;
	 VUCtrlText*	m_pID_TEXT_MDEF_WATER;
	 VUCtrlText*	m_pID_TEXT_MATTACK_WOOD;
	 VUCtrlText*	m_pID_TEXT_MDEF_WOOD;
	 VUCtrlText*	m_pID_TEXT_MATTACK_GOLD;
	 VUCtrlText*	m_pID_TEXT_MDEF_GOLD;
	 VUCtrlText*	m_pID_TEXT_MATTACK_EARTH;
	 VUCtrlText*	m_pID_TEXT_MDEF_EARTH;
	 VUCtrlText*	m_pID_TEXT_Hp;
	 VUCtrlText*	m_pID_TEXT_Mp;
	 VUCtrlText*	m_pID_TEXT_PHYATTACK;
	 VUCtrlText*	m_pID_TEXT_PHYDEF;
	 VUCtrlText*	m_pID_TEXT_MAGIC_DEF;
	 VUCtrlText*	m_pID_TEXT_MAGICATTACK;
	 VUCtrlText*	m_pID_TEXT_RangeAttack;
	 VUCtrlText*	m_pID_TEXT_Hit;
	 VUCtrlText*	m_pID_TEXT_Dodge;

	 VUCtrlButton*	m_arStateBtns[UI_ATTRIBUTE_MAX];

};
};//namespace gameui

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(gameui::UIBaseProperty , UIBaseProperty , API_NULL);
//extern API_NULL UIBaseProperty& singleton::GetUIBaseProperty();
#define theUIBaseProperty  singleton::GetUIBaseProperty()


#endif //__UIBASEPROPERTY_H__


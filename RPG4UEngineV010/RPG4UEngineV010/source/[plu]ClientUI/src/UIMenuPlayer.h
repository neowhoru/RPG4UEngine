/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMenuPlayer.h
创建日期：2007年11月24日
最后更新：2007年11月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIMENUPLAYER_H__
#define __UIMENUPLAYER_H__
#pragma once



#include "CommonDefine.h"
#include "VUCtrlInclude.h"

#ifndef ID_FRAME_RBTNMENU
#define ID_FRAME_RBTNMENU		"ID_FRAME_RBTNMENU"
#endif  
#ifndef ID_BUTTON_TERM
#define ID_BUTTON_TERM		"ID_BUTTON_TERM"
#endif  
#ifndef ID_BUTTON_EXCHANGE
#define ID_BUTTON_EXCHANGE		"ID_BUTTON_EXCHANGE"
#endif  
#ifndef ID_BUTTON_FRD
#define ID_BUTTON_FRD		"ID_BUTTON_FRD"
#endif  
#ifndef ID_BUTTON_FOLLOWME
#define ID_BUTTON_FOLLOWME		"ID_BUTTON_FOLLOWME"
#endif  
#ifndef ID_BUTTON_KICKOUT
#define ID_BUTTON_KICKOUT		"ID_BUTTON_KICKOUT"
#endif  
#ifndef ID_BUTTON_Private
#define ID_BUTTON_Private	"ID_BUTTON_Private"
#endif 
#ifndef ID_BUTTON_CHANGEMASTER
#define ID_BUTTON_CHANGEMASTER	"ID_BUTTON_CHANGEMASTER"
#endif 

using namespace std;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIMenuPlayer
{
	// Member
public:
	UIMenuPlayer();

public:	
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	BOOL ID_BUTTON_TERMOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_EXCHANGEOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_FRDOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_FOLLOWMEOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_KICKOUTOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_PrivateOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CHANGEMASTEROnButtonClick( VUCtrlObject* /*pSender*/ );

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible, BOOL bGroupClick = FALSE );			// 设置是否可视


	void		SetPrivateName	( LPCSTR  szName)	{ m_szName = szName; }
	LPCSTR	GetPrivateName	() const				{ return m_szName.c_str(); 	}
	VOID		SetPos			(INT x, INT y);



public:	
	VUCtrlFrame*	m_pID_FRAME_RBTNMENU;
	VUCtrlButton*	m_pID_BUTTON_TERM;
	VUCtrlButton*	m_pID_BUTTON_EXCHANGE;
	VUCtrlButton*	m_pID_BUTTON_FRD;
	VUCtrlButton*	m_pID_BUTTON_FOLLOWME;
	VUCtrlButton*	m_pID_BUTTON_KICKOUT;
	VUCtrlButton*	m_pID_BUTTON_Private;
	VUCtrlButton*	m_pID_BUTTON_CHANGEMASTER;

	VG_INT_PROPERTY			(GroupID);
	VG_INT_PROPERTY			(PlayerID);
	vector<VUCtrlButton*>	m_BtnInfos;
	string						m_szName;	
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UIMenuPlayer , API_NULL);
//extern API_NULL UIMenuPlayer& GetUIMenuPlayer();
#define theUIMenuPlayer  GetUIMenuPlayer()




#endif //__UIMENUPLAYER_H__
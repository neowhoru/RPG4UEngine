/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBargaining.h
创建日期：2007年12月14日
最后更新：2007年12月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIBARGAINING_H__
#define __UIBARGAINING_H__
#pragma once

#include "VUCtrlInclude.h"
#include "UIBargainingSlotUIListener.h"

#ifndef ID_FRAME_Bargaining
#define ID_FRAME_Bargaining		"ID_FRAME_Bargaining"
#endif  
#ifndef ID_FRAME_CHGPASS
#define ID_FRAME_CHGPASS		"ID_FRAME_CHGPASS"
#endif  
#ifndef ID_BUTTON_OK
#define ID_BUTTON_OK			"ID_BUTTON_OK"
#endif  
#ifndef ID_BUTTON_Cancel
#define ID_BUTTON_Cancel		"ID_BUTTON_Cancel"
#endif  
#ifndef ID_BUTTON_Complete
#define ID_BUTTON_Complete		"ID_BUTTON_Complete"
#endif  
#ifndef ID_LISTIMG_Me
#define ID_LISTIMG_Me			"ID_LISTIMG_Me"
#endif  
#ifndef ID_LISTIMG_Player
#define ID_LISTIMG_Player		"ID_LISTIMG_Player"
#endif  
#ifndef ID_EDIT_Me
#define ID_EDIT_Me				"ID_EDIT_Me"
#endif  
#ifndef ID_TEXT_Player
#define ID_TEXT_Player			"ID_TEXT_Player"
#endif  
#ifndef ID_TEXT_Caption
#define ID_TEXT_Caption			"ID_TEXT_Caption"
#endif  
//#ifndef ID_EDIT_gold
//#define ID_EDIT_gold		"ID_EDIT_gold"
//#endif  
//#ifndef ID_TEXT_gold
//#define ID_TEXT_gold		"ID_TEXT_gold"
//#endif  
#ifndef ID_PICTURE_SelfLock
#define ID_PICTURE_SelfLock		"ID_PICTURE_SelfLock"
#endif  
#ifndef ID_PICTURE_PlayerLock
#define ID_PICTURE_PlayerLock		"ID_PICTURE_PlayerLock"
#endif  
#ifndef ID_TEXT_SelfName
#define ID_TEXT_SelfName		"ID_TEXT_SelfName"
#endif  
#ifndef ID_TEXT_PlayerName
#define ID_TEXT_PlayerName		"ID_TEXT_PlayerName"
#endif  


_NAMESPACEU(Player, object);

#define	MAX_UIBARGAINING_CONTAINER		2

using namespace gameui;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UIBargaining
{
public:
	UIBargaining();

public:
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	// Button
	BOOL ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CancelOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_CompleteOnButtonClick( VUCtrlObject* /*pSender*/ );

	// ListImg / ListEx
	BOOL ID_LISTIMG_MeOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl,
											IconDragListImg* pItemDrag,
											IconDragListImg* pItemDest );
	BOOL ID_LISTIMG_MeOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL ID_LISTIMG_MeOnIconRButtonUp( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );

	// ListImg / ListEx
	BOOL ID_LISTIMG_PlayerOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl,
											IconDragListImg* pItemDrag,
											IconDragListImg* pItemDest );
	BOOL ID_LISTIMG_PlayerOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL ID_LISTIMG_PlayerOnIconRButtonUp( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );

	// Edit
	void ID_EDIT_MeOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );


	static void SendCancelMsg();
	void SetCanbeChange( BOOL b );


	BOOL LoadUI			();				// 载入UI
	BOOL InitControls	();	// 关连控件
	BOOL UnLoadUI		();			// 卸载UI
	BOOL IsVisible		();			// 是否可见
	void SetVisible	( BOOL bVisible);			// 设置是否可视

	void SetPlayer			( LPCSTR  szPlayerName, int nID );

	void Clear				();
	void Refresh			();

public:
	virtual void AttachToItemCompound();
	virtual void OnInstallContainer	(BaseContainer* pContainer);
	virtual void PushGoods				(BaseSlot* pSlot,INT nContainerIndex);
	virtual void PopGoods				(BaseSlot* pSlot,INT nContainerIndex);
	virtual void SetGoodsState			(BaseSlot* pSlot ,DWORD dwState,INT nContainerIndex );

public:
	// Member
	VUCtrlFrame*	m_pID_FRAME_Bargaining;
	VUCtrlButton*	m_pID_BUTTON_OK;
	VUCtrlButton*	m_pID_BUTTON_Cancel;
	VUCtrlButton*	m_pID_BUTTON_Complete;
	VUCtrlEdit*		m_pID_EDIT_Me;
	VUCtrlText*		m_pID_TEXT_Player;
	VUCtrlText*		m_pID_TEXT_Caption;
	//VUCtrlEdit*		m_pID_EDIT_gold;
	//VUCtrlText*		m_pID_TEXT_gold;
	VUCtrlPicture*	m_pID_PICTURE_SelfLock;
	VUCtrlPicture*	m_pID_PICTURE_PlayerLock;
	VUCtrlText*		m_pID_TEXT_SelfName;
	VUCtrlText*		m_pID_TEXT_PlayerName;

	int				m_nPlayerId ;
	BOOL				m_bOnClose;

	union
	{
		struct
		{
		VUCtrlListImg*		m_pID_LISTIMG_Me;
		VUCtrlListImg*		m_pID_LISTIMG_Player;
		};
		VUCtrlListImg		*m_arContainers[MAX_UIBARGAINING_CONTAINER];
	};

	UIBargainingSlotUIListener		m_Listeners[MAX_UIBARGAINING_CONTAINER];

	VG_PTR_PROPERTY(Player, Player);

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(UIBargaining , API_NULL);
//extern API_NULL UIBargaining& singleton::GetUIBargaining();
#define theUIBargaining  singleton::GetUIBargaining()


#endif //__UIBARGAINING_H__
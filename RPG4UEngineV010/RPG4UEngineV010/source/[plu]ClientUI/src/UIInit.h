/********************************************************************
	Created by UIEditor.exe
	FileName: E:\Work\XSanGuoRuntime\Client\UI\Init.h
*********************************************************************/
#pragma once
#include "../../HRUI/HR_UI_Include.h"

#ifndef ID_FRAME_Init
#define ID_FRAME_Init		"ID_FRAME_Init"
#endif  ID_FRAME_Init
#ifndef ID_BUTTON_Web
#define ID_BUTTON_Web		"ID_BUTTON_Web"
#endif  ID_BUTTON_Web
#ifndef ID_BUTTON_BBS
#define ID_BUTTON_BBS		"ID_BUTTON_BBS"
#endif  ID_BUTTON_BBS
#ifndef ID_BUTTON_Login
#define ID_BUTTON_Login		"ID_BUTTON_Login"
#endif  ID_BUTTON_Login
#ifndef ID_BUTTON_UpData
#define ID_BUTTON_UpData		"ID_BUTTON_UpData"
#endif  ID_BUTTON_UpData
#ifndef ID_LIST_Bull
#define ID_LIST_Bull		"ID_LIST_Bull"
#endif  ID_LIST_Bull
#ifndef ID_BUTTON_LOGIN
#define ID_BUTTON_LOGIN		"ID_BUTTON_LOGIN"
#endif  ID_BUTTON_LOGIN
#ifndef ID_BUTTON_EXIT
#define ID_BUTTON_EXIT		"ID_BUTTON_EXIT"
#endif  ID_BUTTON_EXIT

class CUI_ID_FRAME_Init 
{
	// Member
public:
	CUI_ID_FRAME_Init();
	~CUI_ID_FRAME_Init(){;}
public:	
	 CHR_UI_Frame*	m_pID_FRAME_Init;
	 CHR_UI_Button*	m_pID_BUTTON_Web;
	 CHR_UI_Button*	m_pID_BUTTON_BBS;
	 CHR_UI_Button*	m_pID_BUTTON_Login;
	 CHR_UI_Button*	m_pID_BUTTON_UpData;
	 CHR_UI_List*	m_pID_LIST_Bull;
	 CHR_UI_Button*	m_pID_BUTTON_LOGIN;
	 CHR_UI_Button*	m_pID_BUTTON_EXIT;

	// Frame
	bool OnFrameRun();
	bool OnFrameRender();
	bool ID_BUTTON_WebOnButtonClick( CHR_UI_Object* pSender );
	bool ID_BUTTON_BBSOnButtonClick( CHR_UI_Object* pSender );
	bool ID_BUTTON_LoginOnButtonClick( CHR_UI_Object* pSender );
	bool ID_BUTTON_UpDataOnButtonClick( CHR_UI_Object* pSender );
	void ID_LIST_BullOnListSelectChange( CHR_UI_Object* pSender, CHR_UI_List::S_List* pItem );
	bool ID_BUTTON_LOGINOnButtonClick( CHR_UI_Object* pSender );
	bool ID_BUTTON_EXITOnButtonClick( CHR_UI_Object* pSender );

	bool LoadUI();				// 载入UI
	bool DoControlConnect();	// 关连控件
	bool UnLoadUI();			// 卸载UI
	bool IsVisable();			// 是否可见
	void SetVisable( const bool bVisable );			// 设置是否可视
};
extern CUI_ID_FRAME_Init s_CUI_ID_FRAME_Init;
/*////////////////////////////////////////////////////////////////////////
文 件 名：UITarget.h
创建日期：2007年11月25日
最后更新：2007年11月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UITARGET_H__
#define __UITARGET_H__
#pragma once

#include "VUCtrlInclude.h"
#include "VUIControl.h"
#include "Timer.h"

#ifndef ID_FRAME_Target
#define ID_FRAME_Target		"ID_FRAME_Target"
#endif  ID_FRAME_Target
#ifndef ID_TEXT_Name
#define ID_TEXT_Name		"ID_TEXT_Name"
#endif  ID_TEXT_Name
#ifndef ID_PROGRESS_Hp
#define ID_PROGRESS_Hp		"ID_PROGRESS_Hp"
#endif  ID_PROGRESS_Hp
#ifndef ID_PROGRESS_HpTail
#define ID_PROGRESS_HpTail		"ID_PROGRESS_HpTail"
#endif  ID_PROGRESS_HpTail
#ifndef ID_LISTIMG_Status
#define ID_LISTIMG_Status		"ID_LISTIMG_Status"
#endif  ID_LISTIMG_Status

#ifndef ID_TEXT_GROUP
#define ID_TEXT_GROUP		"ID_TEXT_GROUP"
#endif  ID_TEXT_GROUP

#ifndef ID_TEXT_Lvl
#define ID_TEXT_Lvl		"ID_TEXT_Lvl"
#endif  ID_TEXT_Lvl
#ifndef ID_TEXT_TargetName
#define ID_TEXT_TargetName	"ID_TEXT_TargetName"
#endif ID_TEXT_TargetName




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UITarget :public VUIControl
{
	// Member
public:
	UITarget();
	~UITarget();


public:
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	BOOL ID_LISTIMG_StatusOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItemDrag, IconDragListImg* pItemDest );
	BOOL ID_LISTIMG_StatusOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItem );
	BOOL ID_LISTIMG_StatusOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );

public:
	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI	();			// 卸载UI
	BOOL IsVisible	();			// 是否可见
	void SetVisible(BOOL bVisible );			// 设置是否可视
	void Refresh	(DWORD dwTick);

public:
	void		SetPlayer		( VOBJID nID );
	VOBJID	GetPlayerID		() { return m_targetID; }	
	LPCSTR	GetName			()	{ return m_pID_TEXT_Name->GetText(); }
	void		setTargetName( LPCSTR  szName = NULL );

	static BOOL ID_FRAME_Target_OnClick( VUCtrlObject* /*pSender*/ );

public:	
	VUCtrlFrame*		m_pID_FRAME_Target;
	VUCtrlText*			m_pID_TEXT_Name;
	VUCtrlProgress*	m_pID_PROGRESS_Hp;
	VUCtrlProgress*	m_pID_PROGRESS_HpTail;
	VUCtrlListImg*		m_pID_LISTIMG_Status;

	VUCtrlText*			m_pID_TEXT_GROUP;
	VUCtrlText*			m_pID_TEXT_Lvl;
	VUCtrlText*			m_pID_Text_TargetName;

protected:
	VOBJID				m_targetID;
	BOOL					m_bUpdating;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL(UITarget , API_NULL);
#else
extern API_NULL UITarget& GetUITarget();
#endif
#define theUITarget  GetUITarget()

#endif //__UITARGET_H__
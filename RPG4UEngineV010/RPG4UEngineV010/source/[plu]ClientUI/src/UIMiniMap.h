/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMiniMap.h
创建日期：2007年9月18日
最后更新：2007年9月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIMINIMAP_H__
#define __UIMINIMAP_H__
#pragma once


#include "VUCtrlInclude.h"
#include "VUIControl.h"

#ifndef ID_FRAME_MiniMap
#define ID_FRAME_MiniMap		"ID_FRAME_MiniMap"
#endif  ID_FRAME_MiniMap

#ifndef ID_PICTURE_Map
#define ID_PICTURE_Map		"ID_PICTURE_Map"
#endif  ID_PICTURE_Map
#ifndef ID_BUTTON_BigMap
#define ID_BUTTON_BigMap		"ID_BUTTON_BigMap"
#endif  ID_BUTTON_BigMap
#ifndef ID_TEXT_Name
#define ID_TEXT_Name		"ID_TEXT_Name"
#endif  ID_TEXT_Name
#ifndef ID_BUTTON_MapSize
#define ID_BUTTON_MapSize		"ID_BUTTON_MapSize"
#endif  ID_BUTTON_MapSize
#ifndef ID_BUTTON_MapSize0
#define ID_BUTTON_MapSize0		"ID_BUTTON_MapSize0"
#endif  ID_BUTTON_MapSize0

#ifndef ID_TEXT_PlayerPos
#define ID_TEXT_PlayerPos		"ID_TEXT_PlayerPos"
#endif  ID_TEXT_PlayerPos
#ifndef ID_TEXT_Land
#define ID_TEXT_Land		"ID_TEXT_Land"
#endif  ID_TEXT_Land	
#ifndef ID_CHECKBOX_Angle
#define ID_CHECKBOX_Angle		"ID_CHECKBOX_Angle"
#endif  ID_CHECKBOX_Angle


using namespace std;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UIMiniMap : public VUIControl
{
	__DISABLE_COPY(UIMiniMap);

public:
	UIMiniMap();

public:
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	// Button
	BOOL ID_BUTTON_BigMapOnButtonClick( VUCtrlObject* /*pSender*/ );

	// Button
	BOOL ID_BUTTON_MapSizeOnButtonClick( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_MapSize0OnButtonClick( VUCtrlObject* /*pSender*/ );

	void ID_CHECKBOX_AngleOnClick(VUCtrlObject* pSender,  BOOL* pbChecked);

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible(  BOOL bVisible );			// 设置是否可视
	//
	void Refresh();

	float GetZoomScale();
	void SetCheckAngle(BOOL bChecked);
	void ZoomChange	(BOOL bZoomIn);


public:
	// Member
	VUCtrlFrame*		m_pID_FRAME_MiniMap;
	VUCtrlPicture*		m_pID_PICTURE_Map;

	VUCtrlButton*		m_pID_BUTTON_BigMap;
	VUCtrlText*			m_pID_TEXT_Name;
	VUCtrlButton*		m_pID_BUTTON_MapSize;
	VUCtrlButton*		m_pID_BUTTON_MapSize0;
	VUCtrlText*			m_pID_TEXT_PlayerPos;
	VUCtrlText*			m_pID_TEXT_Land;
	VUCtrlCheckBox*	m_pID_CHECKBOX_Angle;
	INT					m_nScaleStep;
	float					m_fZoomScale;
	BOOL					m_bZoomIn;

};
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UIMiniMap , API_NULL);
//extern API_NULL UIMiniMap& GetUIMiniMap();
#define theUIMiniMap  GetUIMiniMap()


#endif //__UIMINIMAP_H__
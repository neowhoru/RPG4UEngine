/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMainDefine.h
创建日期：2007年11月22日
最后更新：2007年11月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIMAINDEFINE_H__
#define __UIMAINDEFINE_H__
#pragma once


#ifndef ID_FRAME_MAIN
#define ID_FRAME_MAIN		"ID_FRAME_MAIN"
#endif  ID_FRAME_MAIN
#ifndef ID_BUTTON_Function
#define ID_BUTTON_Function		"ID_BUTTON_Function"
#endif  ID_BUTTON_Function
#ifndef ID_BUTTON_FightState
#define ID_BUTTON_FightState		"ID_BUTTON_FightState"
#endif  ID_BUTTON_FightState
#ifndef ID_PROGRESS_Exp
#define ID_PROGRESS_Exp		"ID_PROGRESS_Exp"
#endif  ID_PROGRESS_Exp
#ifndef ID_TEXT_Exp
#define ID_TEXT_Exp		"ID_TEXT_Exp"
#endif  ID_TEXT_Exp
#ifndef ID_LISTIMG_Hotkey
#define ID_LISTIMG_Hotkey		"ID_LISTIMG_Hotkey"
#endif  ID_LISTIMG_Hotkey
#ifndef ID_BUTTON_OUT
#define ID_BUTTON_OUT		"ID_BUTTON_OUT"
#endif  ID_BUTTON_OUT
#ifndef ID_BUTTON_IN
#define ID_BUTTON_IN		"ID_BUTTON_IN"
#endif  ID_BUTTON_IN
#ifndef ID_PICTURE_BG
#define ID_PICTURE_BG		"ID_PICTURE_BG"
#endif  ID_PICTURE_BG
#ifndef ID_BUTTON_State
#define ID_BUTTON_State		"ID_BUTTON_State"
#endif  ID_BUTTON_State
#ifndef ID_BUTTON_Pack
#define ID_BUTTON_Pack		"ID_BUTTON_Pack"
#endif  ID_BUTTON_Pack
#ifndef ID_BUTTON_Skill
#define ID_BUTTON_Skill		"ID_BUTTON_Skill"
#endif  ID_BUTTON_Skill
#ifndef ID_BUTTON_Task
#define ID_BUTTON_Task		"ID_BUTTON_Task"
#endif  ID_BUTTON_Task
#ifndef ID_BUTTON_Map
#define ID_BUTTON_Map		"ID_BUTTON_Map"
#endif  ID_BUTTON_Map
#ifndef ID_BUTTON_System
#define ID_BUTTON_System		"ID_BUTTON_System"
#endif  ID_BUTTON_System
#ifndef ID_TEXT_Level
#define ID_TEXT_Level			"ID_TEXT_Level"
#endif ID_TEXT_Level

#ifndef ID_BUTTON_Guild
#define ID_BUTTON_Guild		"ID_BUTTON_Guild"
#endif  ID_BUTTON_Guild
#ifndef ID_BUTTON_Fiend
#define ID_BUTTON_Fiend		"ID_BUTTON_Fiend"
#endif  ID_BUTTON_Fiend



#endif //__UIMAINDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：UIQuest.h
创建日期：2007年12月4日
最后更新：2007年12月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __UITASK_H__
#define __UITASK_H__
#pragma once

#include "VUCtrlInclude.h"

#include "VUIControl.h"
#include "NpcCoordinateManager.h"
#include "QuestDefine.h"

#ifndef ID_FRAME_Task
#define ID_FRAME_Task		"ID_FRAME_Task"
#endif  ID_FRAME_Task
#ifndef ID_BUTTON_CLOSE
#define ID_BUTTON_CLOSE		"ID_BUTTON_CLOSE"
#endif  ID_BUTTON_CLOSE
#ifndef ID_LIST_Task
#define ID_LIST_Task		"ID_LIST_Task"
#endif  ID_LIST_Task
//#ifndef ID_TEXT_Level
//#define ID_TEXT_Level		"ID_TEXT_Level"
//#endif  ID_TEXT_Level
//#ifndef ID_EDIT_Info
//#define ID_EDIT_Info		"ID_EDIT_Info"
//#endif  ID_EDIT_Info
//#ifndef ID_EDIT_State
//#define ID_EDIT_State		"ID_EDIT_State"
//#endif  ID_EDIT_State
#ifndef ID_BUTTON_AbandonTask
#define ID_BUTTON_AbandonTask		"ID_BUTTON_AbandonTask"
#endif  ID_BUTTON_AbandonTask
#ifndef ID_BUTTON_Action
#define ID_BUTTON_Action		"ID_BUTTON_Action"
#endif  ID_BUTTON_Action
#ifndef ID_BUTTON_Property
#define ID_BUTTON_Property		"ID_BUTTON_Property"
#endif  ID_BUTTON_Property
#ifndef ID_TEXT_TaskInfo
#define ID_TEXT_TaskInfo		"ID_TEXT_TaskInfo"
#endif ID_TEXT_TaskInfo
#ifndef ID_TEXT_Taskstate
#define ID_TEXT_Taskstate	"ID_TEXT_Taskstate"
#endif ID_TEXT_Taskstate


using namespace quest;
struct TrackInfo
{
    TrackInfo()
    {
        dwColor = 0x00000000;
        szInfo[0] = 0;
    }
    DWORD dwColor;
    char szInfo[50];
};

class _CLIENTUI_API CUI_ID_FRAME_Task :public VUIControl
{
public:
    enum
    {
        MaxTrackNum = 5
    };
	// Member
public:
	CUI_ID_FRAME_Task();
public:	
	 VUCtrlFrame*	m_pID_FRAME_Task;
	 VUCtrlButton*	m_pID_BUTTON_CLOSE;
	 //VUCtrlListEx*	m_pID_LISTEX_Task;
	 VUCtrlList*		m_pID_LIST_Task;
	 //VUCtrlText*	m_pID_TEXT_Level;
	 //VUCtrlEdit*	m_pID_EDIT_Info;
	 //VUCtrlEdit*	m_pID_EDIT_State;
	 VUCtrlText*	m_pID_TEXT_Info;
	 VUCtrlText*	m_pID_TEXT_State;
	 VUCtrlButton*	m_pID_BUTTON_AbandonTask;
	 VUCtrlButton*	m_pID_BUTTON_Action;
	 VUCtrlButton*	m_pID_BUTTON_Property;

	// Frame
	BOOL OnFrameRun(DWORD dwTick);
	BOOL OnFrameRender(DWORD dwTick);
	BOOL ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_LISTEX_TaskOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe, IconDragListImg* pItemDrag, IconDragListImg* pItemDest );
	BOOL ID_LISTEX_TaskOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pMe, IconDragListImg* pItem );
	//BOOL ID_LISTEX_TaskOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem );
	BOOL ID_LIST_TaskOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem);
	//BOOL ID_LISTEX_TaskOnIconRDBClick( VUCtrlObject* pSender, IconDragListImg* pItem );
	void ID_EDIT_InfoOnEditEnter( VUCtrlObject* pSender, const char* szData );
	void ID_EDIT_StateOnEditEnter( VUCtrlObject* pSender, const char* szData );
	BOOL ID_BUTTON_AbandonTaskOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_ActionOnButtonClick( VUCtrlObject* pSender );
	BOOL ID_BUTTON_PropertyOnButtonClick( VUCtrlObject* pSender );

	void ID_TEXT_Info_HyperLinkClick(VUCtrlObject* pSender, const char* szData);
	void ID_TEXT_State_HyperLinkClick(VUCtrlObject* pSender, const char* szData);

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible();
	void HideUnderLine();

	void Refresh();
	void Refresh(const std::vector<sHERO_QUESTINFO>& vtQuest );

	static BOOL AbandonTaskCallBackFun( BOOL bPressYesButton, void *pData );
    static void ID_LIST_TaskOnRBClick( VUCtrlObject* pSender, VUCtrlList::S_List* pItem );
	BOOL m_bLoad;
	COLORREF col;
	COLORREF	colCurrent;
	int			_nCurIndex;
	DWORD		m_dwLastRefreshTime;

    std::vector<int> m_vTrackList;

    void UpdateTrackInfo();
};

_CLIENTUI_API BOOL UITask_ShowInfo( VUCtrlObject* pSender, VUCtrlList::S_List* pItem, COLORREF col);
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL(CUI_ID_FRAME_Task , API_NULL);
#else
extern API_NULL CUI_ID_FRAME_Task& GetCUI_ID_FRAME_Task();
#endif
#define s_CUI_ID_FRAME_Task  GetCUI_ID_FRAME_Task()

//
//namespace UI_ID_FRAME_Task
//{
//	// Member
//	extern VUCtrlFrame*	m_pID_FRAME_Task;
//	extern VUCtrlButton*	m_pID_BUTTON_CLOSE;
//	extern VUCtrlListEx*	m_pID_LISTEX_Task;
//	extern VUCtrlText*	m_pID_TEXT_Level;
//	extern VUCtrlEdit*	m_pID_EDIT_Info;
//	extern VUCtrlEdit*	m_pID_EDIT_State;
//	extern VUCtrlButton*	m_pID_BUTTON_AbandonTask;
//	extern VUCtrlButton*	m_pID_BUTTON_Action;
//	extern VUCtrlButton*	m_pID_BUTTON_Property;
//
//	// Frame
//	BOOL ID_FRAME_TaskOnFrameRun(DWORD dwTick);
//	BOOL ID_FRAME_TaskOnFrameRender(DWORD dwTick);
//	// Button
//	BOOL ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender );
//	// ListImg / ListEx
//	BOOL ID_LISTEX_TaskOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe,
//											IconDragListImg* pItemDrag,
//											IconDragListImg* pItemDest );
//	BOOL ID_LISTEX_TaskOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem );
//	BOOL ID_LISTEX_TaskOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem );
//	// Edit
//	void ID_EDIT_InfoOnEditEnter( VUCtrlObject* pSender, const char* szData );
//	// Edit
//	void ID_EDIT_StateOnEditEnter( VUCtrlObject* pSender, const char* szData );
//	// Button
//	BOOL ID_BUTTON_AbandonTaskOnButtonClick( VUCtrlObject* pSender );
//	BOOL ID_BUTTON_ActionOnButtonClick( VUCtrlObject* pSender );
//	BOOL ID_BUTTON_PropertyOnButtonClick( VUCtrlObject* pSender );
//
//	BOOL LoadUI();				// 载入UI
//	BOOL InitControls();	// 关连控件
//	BOOL UnLoadUI();			// 卸载UI
//	BOOL IsVisible();			// 是否可见
//	void SetVisible( BOOL bVisible );			// 设置是否可视
//
//	void Refresh();
//	void Refresh( std::vector<sHERO_QUESTINFO>& vtQuest );
//}


#endif //__UITASK_H__
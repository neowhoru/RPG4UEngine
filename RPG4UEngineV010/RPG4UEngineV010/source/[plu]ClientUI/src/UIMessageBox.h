/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMessageBox.h
创建日期：2007年7月24日
最后更新：2007年7月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIMESSAGEBOX_H__
#define __UIMESSAGEBOX_H__
#pragma once

#include "VUCtrlInclude.h"

#ifndef ID_FRAME_MESSAGE
#define ID_FRAME_MESSAGE		"ID_FRAME_MESSAGE"
#endif  ID_FRAME_MESSAGE

#ifndef ID_EDIT_INFO
#define ID_EDIT_INFO		"ID_EDIT_INFO"
#endif  ID_EDIT_INFO

#ifndef ID_BUTTON_YES
#define ID_BUTTON_YES		"ID_BUTTON_YES"
#endif  ID_BUTTON_YES

#ifndef ID_BUTTON_NO
#define ID_BUTTON_NO		"ID_BUTTON_NO"
#endif  ID_BUTTON_NO

#ifndef ID_BUTTON_COMFIRM
#define ID_BUTTON_COMFIRM		"ID_BUTTON_COMFIRM"
#endif  ID_BUTTON_COMFIRM

#ifndef ID_TEXT_CAPTION
#define ID_TEXT_CAPTION		"ID_TEXT_CAPTION"
#endif  ID_TEXT_CAPTION


using namespace std;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UIMessageBox
{
public:
	UIMessageBox();

public:
	typedef  BOOL (*PROC_BOX_CALLBACK)(BOOL bPressYesButton,  void *pData );
	// struct
	struct sMESSAGE_BOX
	{
		sMESSAGE_BOX()
		{
			m_nType = MB_OK;	// MB_OK/MB_OKCANCEL
			m_ProcCallback = NULL;
			m_bModal = TRUE;
			m_pData = NULL;
		}

		int					m_nType;
		BOOL					m_bModal;
		string				m_strCaption;
		string				m_strContent;
		PROC_BOX_CALLBACK	m_ProcCallback;
		void*					m_pData;
		DWORD					m_dwDataSize;
	};


public:
	void Show	(LPCSTR            szText
               ,LPCSTR            szCaption=""
					,int               nType=MB_OK
					,BOOL              bModal=TRUE
					,PROC_BOX_CALLBACK procCallback=NULL
					,void*             pData=NULL
					,int               nDataLength=0);

	void Show	(DWORD             dwTextID
               ,DWORD             dwCaptionID=-1
					,int               nType=MB_OK
					,BOOL              bModal=TRUE
					,PROC_BOX_CALLBACK procCallback=NULL
					,void*             pData=NULL
					,int               nDataLength=0);

	/////////////////////////////////////
	void ClearData();	
	void Refresh();

	/////////////////////////////////////
	// Frame
	BOOL OnFrameMove	(DWORD dwTick);
	BOOL OnRender		(DWORD dwTick);

	// Button
	BOOL ID_BUTTON_YESOnButtonClick( VUCtrlObject* pThisCtrl );
	BOOL ID_BUTTON_NOOnButtonClick( VUCtrlObject* pThisCtrl );
	BOOL ID_BUTTON_COMFIRMOnButtonClick(VUCtrlObject* pThisCtrl);

	/////////////////////////////////////
	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible(   BOOL bVisible );			// 设置是否可视

public:
	// Member
	VUCtrlFrame*	m_pID_FRAME_MESSAGE;
	VUCtrlEdit*		m_pID_EDIT_INFO;
	VUCtrlButton*	m_pID_BUTTON_YES;
	VUCtrlButton*	m_pID_BUTTON_NO;
	VUCtrlButton*   m_pID_BUTTON_COMFIRM;
	VUCtrlText*		m_pID_TEXT_CAPTION;

	BOOL								m_bUnLoad;
	PROC_BOX_CALLBACK				m_ProcCallback ;
	void*								m_pData ;
	DWORD								m_dwDataSize;
	vector<sMESSAGE_BOX>			m_MessageInfos;

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UIMessageBox , API_NULL);
//extern API_NULL UIMessageBox& GetUIMessageBox();
#define theUIMessageBox  GetUIMessageBox()


#endif //__UIMESSAGEBOX_H__
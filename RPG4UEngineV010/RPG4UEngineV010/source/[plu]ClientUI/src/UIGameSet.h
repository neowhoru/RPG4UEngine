/*////////////////////////////////////////////////////////////////////////
文 件 名：UIGameSet.h
创建日期：2007年12月12日
最后更新：2007年12月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIGAMESET_H__
#define __UIGAMESET_H__
#pragma once

#include "VUCtrlInclude.h"

#ifndef ID_FRAME_GameSetup
#define ID_FRAME_GameSetup		"ID_FRAME_GameSetup"
#endif  ID_FRAME_GameSetup
#ifndef ID_BUTTON_Close
#define ID_BUTTON_Close		"ID_BUTTON_Close"
#endif  ID_BUTTON_Close
#ifndef ID_BUTTON_FullSrc
#define ID_BUTTON_FullSrc		"ID_BUTTON_FullSrc"
#endif  ID_BUTTON_FullSrc
#ifndef ID_BUTTON_Window
#define ID_BUTTON_Window		"ID_BUTTON_Window"
#endif  ID_BUTTON_Window
#ifndef ID_COMBOBOX_SrcWH
#define ID_COMBOBOX_SrcWH		"ID_COMBOBOX_SrcWH"
#endif  ID_COMBOBOX_SrcWH
#ifndef ID_CHECKBOX_Graph_Lvl1
#define ID_CHECKBOX_Graph_Lvl1		"ID_CHECKBOX_Graph_Lvl1"
#endif  ID_CHECKBOX_Graph_Lvl1
#ifndef ID_CHECKBOX_Graph_Lvl2
#define ID_CHECKBOX_Graph_Lvl2		"ID_CHECKBOX_Graph_Lvl2"
#endif  ID_CHECKBOX_Graph_Lvl2
#ifndef ID_CHECKBOX_Graph_Lvl3
#define ID_CHECKBOX_Graph_Lvl3		"ID_CHECKBOX_Graph_Lvl3"
#endif  ID_CHECKBOX_Graph_Lvl3
#ifndef ID_CHECKBOX_ShowHead
#define ID_CHECKBOX_ShowHead		"ID_CHECKBOX_ShowHead"
#endif  ID_CHECKBOX_ShowHead
#ifndef ID_CHECKBOX_Music
#define ID_CHECKBOX_Music		"ID_CHECKBOX_Music"
#endif  ID_CHECKBOX_Music
#ifndef ID_CHECKBOX_Sound
#define ID_CHECKBOX_Sound		"ID_CHECKBOX_Sound"
#endif  ID_CHECKBOX_Sound
#ifndef ID_SCROLLBAREX_View
#define ID_SCROLLBAREX_View		"ID_SCROLLBAREX_View"
#endif  ID_SCROLLBAREX_View
#ifndef ID_SCROLLBAREX_RoleView
#define ID_SCROLLBAREX_RoleView		"ID_SCROLLBAREX_RoleView"
#endif  ID_SCROLLBAREX_RoleView
#ifndef ID_SCROLLBAREX_MusicMax
#define ID_SCROLLBAREX_MusicMax		"ID_SCROLLBAREX_MusicMax"
#endif  ID_SCROLLBAREX_MusicMax
#ifndef ID_SCROLLBAREX_SoundMax
#define ID_SCROLLBAREX_SoundMax		"ID_SCROLLBAREX_SoundMax"
#endif  ID_SCROLLBAREX_SoundMax
#ifndef ID_SCROLLBAREX_mipmap
#define ID_SCROLLBAREX_mipmap		"ID_SCROLLBAREX_mipmap"
#endif  ID_SCROLLBAREX_mipmap
#ifndef ID_BUTTON_App
#define ID_BUTTON_App		"ID_BUTTON_App"
#endif  ID_BUTTON_App
#include "XmlIOItem.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIGameSetSetting :public XmlIOItem
{
public:
	UIGameSetSetting();
	~UIGameSetSetting();
public:
	virtual bool loadXMLSettings( const char *path  );
	virtual bool loadXMLSettings( XMLElement *element);
	virtual bool exportXMLSettings( std::ofstream &xmlFile  );
	virtual bool exportXMLSettings( LPCSTR  xmlPath );

public:
	BOOL			ApplySetting();

public:
	INT			m_SaveMusicVal;
	INT			m_SaveSoundVal;
	INT			m_SaveRoleView;

	VG_DWORD_PROPERTY	(MultiSample);
	VG_FLOAT_PROPERTY	(ViewDistance);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UIGameSet
{
public:		
	 UIGameSet();


public:		
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	BOOL ID_BUTTON_CloseOnButtonClick( VUCtrlObject* /*pSender*/ );
	
	void ID_BUTTON_FullSrcOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_BUTTON_WindowOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_COMBOBOX_SrcWHOnComboBoxChange( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	void ID_CHECKBOX_Graph_Lvl1OnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_Graph_Lvl2OnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_Graph_Lvl3OnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_ShowHeadOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_MusicOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_CHECKBOX_SoundOnCheckBoxCheck( VUCtrlObject* pSender, BOOL* pbChecked  );
	void ID_SCROLLBAREX_ViewOnScrollBarExUpdatePos( VUCtrlObject* pSender, int n );
	void ID_SCROLLBAREX_RoleViewOnScrollBarExUpdatePos( VUCtrlObject* pSender, int n );
	void ID_SCROLLBAREX_MusicMaxOnScrollBarExUpdatePos( VUCtrlObject* pSender, int n );
	void ID_SCROLLBAREX_SoundMaxOnScrollBarExUpdatePos( VUCtrlObject* pSender, int n );

	BOOL ID_BUTTON_AppOnButtonClick( VUCtrlObject* /*pSender*/ );

	void ID_SCROLLBAREX_mipmapOnScrollBarExUpdatePos( VUCtrlObject* pSender, int n );

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视

public:		
	 VUCtrlFrame*			m_pID_FRAME_GameSetup;
	 VUCtrlButton*			m_pID_BUTTON_Close;

	 VUCtrlCheckBox*		m_pID_BUTTON_FullSrc;
	 VUCtrlCheckBox*		m_pID_BUTTON_Window;
	 VUCtrlComboBox*		m_pID_COMBOBOX_SrcWH;
	 VUCtrlCheckBox*		m_pID_CHECKBOX_Graph_Lvl1;
	 VUCtrlCheckBox*		m_pID_CHECKBOX_Graph_Lvl2;
	 VUCtrlCheckBox*		m_pID_CHECKBOX_Graph_Lvl3;
	 VUCtrlCheckBox*		m_pID_CHECKBOX_ShowHead;
	 VUCtrlCheckBox*		m_pID_CHECKBOX_Music;
	 VUCtrlCheckBox*		m_pID_CHECKBOX_Sound;
	 VUCtrlScrollBarEx*	m_pID_SCROLLBAREX_View;
	 VUCtrlScrollBarEx*	m_pID_SCROLLBAREX_RoleView;

	 VUCtrlScrollBarEx*	m_pID_SCROLLBAREX_MusicMax;
	 VUCtrlScrollBarEx*	m_pID_SCROLLBAREX_SoundMax;

	 VUCtrlScrollBarEx*	m_pID_SCROLLBAREX_mipmap;
	 VUCtrlButton*			m_pID_BUTTON_App;
	 
	 UIGameSetSetting		m_GameSetting;

	 
	 INT m_MusicValue;
	 INT m_SoundValue;
	 INT m_HeroViewRange;
	 INT m_MiniMapValue;

	 BOOL m_DisableMusic;
	 BOOL m_DisableSound;
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(UIGameSet , API_NULL);
//extern API_NULL UIGameSet& singleton::GetUIGameSet();
#define theUIGameSet  singleton::GetUIGameSet()


#endif //__UIGAMESET_H__
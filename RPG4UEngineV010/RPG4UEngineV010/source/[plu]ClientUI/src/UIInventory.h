/*////////////////////////////////////////////////////////////////////////
文 件 名：UIInventory.h
创建日期：2007年11月30日
最后更新：2007年11月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIINVENTORY_H__
#define __UIINVENTORY_H__
#pragma once

#include "VUCtrlInclude.h"
#include "UIModelCtrl.h"
#include "UIInventoryDefine.h"

using  vobject::VCharacterAnimationCtrl;
_NAMESPACEU(Player,			object);
class BaseSlot;
class BaseContainer;


namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UIInventory
{
public:	
	// Frame
	UIInventory();
	~UIInventory();

public:	
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);

	BOOL ID_BUTTON_CLOSEOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_LOCKOnButtonClick		( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_UNLOCKOnButtonClick	( VUCtrlObject* /*pSender*/ );

	BOOL ID_LISTIMG_PACKOnIconDropTo			( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItemDrag, IconDragListImg* pItemDest );
	BOOL ID_LISTIMG_PACKOnIconLDBClick		( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL ID_LISTIMG_PACKOnIconRButtonUp		( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );

	BOOL EquipOnIconDropTo			( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItemDrag, IconDragListImg* pItemDest );
	BOOL EquipOnIconLDBClick		( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL EquipOnIconRButtonUp		( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );

	// CheckBox
	void PageOnCheckBoxCheck				( VUCtrlObject* pSender,  BOOL * bChecked );

	BOOL ID_BUTTON_LeftOnButtonClick		( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_RightOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_FrontOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_NextOnButtonClick		( VUCtrlObject* /*pSender*/ );

	BOOL ID_BUTTON_ChangeWeaponOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ShoppingOnButtonClick		( VUCtrlObject* /*pSender*/ );
	BOOL ID_LISTIMG_RubbishOnIconDropTo			(VUCtrlObject* pSender, VUCtrlObject* pThisCtrl, IconDragListImg* pItemDrag, IconDragListImg* pItemDest);



	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
	void SetLayoutVisible();

	void Refresh();
	void OnPackInfoChange();

	void SetGoodsState		(BaseSlot* pSlot ,DWORD dwState );

	void OnInstallContainer	(BaseContainer* pContainer);
	void ChangePage	(UINT nPage);

	void PushGoods		(CODETYPE	itemID, UINT nIndex=-1,UINT nCount=1);
	void PopGoods		(UINT			nIndex );

	void PushGoods		(BaseSlot* pSlot);
	void PopGoods		(BaseSlot* pSlot );
	void ForceDragFrom(SLOTPOS slotPos);
	void ShowToolTip	(SLOTPOS slotPos, DWORD dwTipKey);

	void PushEquip		(CODETYPE	itemID, UINT nIndex=-1,UINT nCount=1);
	void PopEquip		(UINT			nIndex );

	void PushEquip		(BaseSlot* pSlot);
	void PopEquip		(BaseSlot* pSlot );


	BOOL UnEquip		(IconDragListImg*	pItemDest
                     ,int                          nEquipWhere
							,int                          nDstIndex);


	BOOL RefreshEquip		( BOOL bUpdataEquipView = TRUE );
	BOOL ClearListImg		();

	//BOOL InlayGem( BOOL bPressYesButton, void *pData );
	BOOL RenderViewport	();
	BOOL MovePackItem		( LPCSTR  szInputData, void *pData );
	

	void UpdataEquipView	();
	VOID Destroy			();


private:

public:
	BOOL					m_bInventoryLock;
	BOOL					m_bInventoryUnlock;
	UINT					m_CurrentAction;
	float					m_fRotateZ;

	BOOL					m_bRenderViewport;


	VUCtrlText*			m_pID_TEXT_MONEY;

	VUCtrlFrame*		m_pID_FRAME_PACK;
	VUCtrlButton*		m_pID_BUTTON_CLOSE;
	VUCtrlButton*		m_pID_BUTTON_LOCK;
	VUCtrlButton*  	m_pID_BUTTON_UNLOCK;
	VUCtrlListImg*		m_pID_LISTIMG_PACK;
	VUCtrlListImg*		m_pID_LISTIMG_Rubbish;	//垃圾桶

	////////////////////////////
	//装备槽
	VUCtrlListImg*		m_pID_LISTIMG_Accouterment;
	VUCtrlListImg*		m_pID_LISTIMG_Accouterment1;
	union
	{
		///顺序参考 eEQUIP_POS_INDEX
		struct
		{
			VUCtrlListImg*		m_pID_LISTIMG_rWeapon;
			VUCtrlListImg*		m_pID_LISTIMG_Armour;
			VUCtrlListImg*		m_pID_LISTIMG_Protector;	//护肩
			VUCtrlListImg*		m_pID_LISTIMG_Helmet;
			VUCtrlListImg*		m_pID_LISTIMG_Pants;
			VUCtrlListImg*		m_pID_LISTIMG_Boots;
			VUCtrlListImg*		m_pID_LISTIMG_Glove;
			VUCtrlListImg*		m_pID_LISTIMG_Belt;		//腰带
			VUCtrlListImg*		m_pID_LISTIMG_Shirts;
			VUCtrlListImg*		m_pID_LISTIMG_Ring1;
			VUCtrlListImg*		m_pID_LISTIMG_Ring2;
			VUCtrlListImg*		m_pID_LISTIMG_Necklace;

			VUCtrlListImg*		m_pID_LISTIMG_Hair;
			VUCtrlListImg*		m_pID_LISTIMG_Face;
			VUCtrlListImg*		m_pID_LISTIMG_HeadWare;
			VUCtrlListImg*		m_pID_LISTIMG_lWeapon;
			VUCtrlListImg*		m_pID_LISTIMG_Shield;

#ifdef MORE_EQUIPPOS
			VUCtrlListImg*		m_pID_LISTIMG_Bangle;

			VUCtrlListImg*		m_pID_LISTIMG_Cape;
			VUCtrlListImg*		m_pID_LISTIMG_Stone1;
			VUCtrlListImg*		m_pID_LISTIMG_Stone2;
			VUCtrlListImg*		m_pID_LISTIMG_Stone3;
#endif
		};
		VUCtrlListImg*		m_arEquips[EQUIPPOS_MAX];
	};


	//////////////////////////////////////
	//Page
	VUCtrlCheckBox*	m_arCheckBoxPtrs[MAX_PACK_PAGE_NUM];
	INT					m_nCurrentPage;

	UIModelCtrl			m_playerModelCtrl;
	VG_PTR_PROPERTY	(Player, Player);
};

};//namespace ui


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(gameui::UIInventory , UIInventory , API_NULL);
//extern _CLIENTMAIN_API UIInventory& singleton::GetUIInventory();
#define theUIInventory  singleton::GetUIInventory()

#endif //__UIINVENTORY_H__


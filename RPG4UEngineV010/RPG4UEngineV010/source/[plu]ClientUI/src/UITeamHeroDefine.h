/*////////////////////////////////////////////////////////////////////////
文 件 名：UITeamHeroDefine.h
创建日期：2008年10月13日
最后更新：2008年10月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UITEAMHERODEFINE_H__
#define __UITEAMHERODEFINE_H__
#pragma once

#ifndef ID_FRAME_TeamHero
#define ID_FRAME_TeamHero		"ID_FRAME_TeamHero"
#endif  ID_FRAME_TeamHero
#ifndef ID_TEXT_Name
#define ID_TEXT_Name		"ID_TEXT_Name"
#endif  ID_TEXT_Name
#ifndef ID_PROGRESS_Hp
#define ID_PROGRESS_Hp		"ID_PROGRESS_Hp"
#endif  ID_PROGRESS_Hp
#ifndef ID_PROGRESS_Mp
#define ID_PROGRESS_Mp		"ID_PROGRESS_Mp"
#endif  ID_PROGRESS_Mp
#ifndef ID_TEXT_HP
#define ID_TEXT_HP		"ID_TEXT_HP"
#endif  ID_TEXT_HP
#ifndef ID_TEXT_MP
#define ID_TEXT_MP		"ID_TEXT_MP"
#endif  ID_TEXT_MP
#ifndef ID_PROGRESS_Exp
#define ID_PROGRESS_Exp "ID_PROGRESS_Exp"
#endif  ID_PROGRESS_Exp
#ifndef ID_TEXT_Exp 
#define ID_TEXT_Exp		"ID_TEXT_Exp"
#endif  ID_TEXT_Exp
#ifndef ID_TEXT_level		
#define ID_TEXT_level		"ID_TEXT_level"
#endif	ID_TEXT_level		

#ifndef ID_PICTURE_Head		
#define ID_PICTURE_Head		"ID_PICTURE_Head"
#endif	ID_PICTURE_Head		
#ifndef ID_PROGRESS_RAGE
#define ID_PROGRESS_RAGE		"ID_PROGRESS_RAGE"
#endif  ID_PROGRESS_RAGE
#ifndef ID_BUTTON_PKNONE
#define ID_BUTTON_PKNONE		"ID_BUTTON_PKNONE"
#endif  ID_BUTTON_PKNONE
#ifndef ID_PICTURE_RAGE1
#define ID_PICTURE_RAGE1		"ID_PICTURE_RAGE1"
#endif  ID_PICTURE_RAGE1
#ifndef ID_BUTTON_PKALL
#define ID_BUTTON_PKALL		"ID_BUTTON_PKALL"
#endif  ID_BUTTON_PKALL
#ifndef ID_BUTTON_PKTEAM
#define ID_BUTTON_PKTEAM		"ID_BUTTON_PKTEAM"
#endif  ID_BUTTON_PKTEAM
#ifndef ID_BUTTON_PKGUILD
#define ID_BUTTON_PKGUILD		"ID_BUTTON_PKGUILD"
#endif  ID_BUTTON_PKGUILD
#ifndef ID_PICTURE_RAGE2
#define ID_PICTURE_RAGE2		"ID_PICTURE_RAGE2"
#endif  ID_PICTURE_RAGE2
#ifndef ID_PICTURE_RAGE3
#define ID_PICTURE_RAGE3		"ID_PICTURE_RAGE3"
#endif  ID_PICTURE_RAGE3
#ifndef ID_PICTURE_RAGE4
#define ID_PICTURE_RAGE4		"ID_PICTURE_RAGE4"
#endif  ID_PICTURE_RAGE4
#ifndef ID_PICTURE_RAGE5
#define ID_PICTURE_RAGE5		"ID_PICTURE_RAGE5"
#endif  ID_PICTURE_RAGE5
#ifndef ID_PICTURE_pet
#define ID_PICTURE_pet		"ID_PICTURE_pet"
#endif  ID_PICTURE_pet
#ifndef ID_TEXT_petname
#define ID_TEXT_petname		"ID_TEXT_petname"
#endif  ID_TEXT_petname
#ifndef ID_TEXT_petlevel
#define ID_TEXT_petlevel		"ID_TEXT_petlevel"
#endif  ID_TEXT_petlevel
#ifndef ID_PROGRESS_petMp
#define ID_PROGRESS_petMp		"ID_PROGRESS_petMp"
#endif  ID_PROGRESS_petMp
#ifndef ID_PROGRESS_petHp
#define ID_PROGRESS_petHp		"ID_PROGRESS_petHp"
#endif  ID_PROGRESS_petHp
#ifndef ID_TEXT_pethp
#define ID_TEXT_pethp		"ID_TEXT_pethp"
#endif  ID_TEXT_pethp
#ifndef ID_TEXT_petmp
#define ID_TEXT_petmp		"ID_TEXT_petmp"
#endif  ID_TEXT_petmp
#ifndef ID_PICTURE_HeaderMe
#define ID_PICTURE_HeaderMe	"ID_PICTURE_HeaderMe"
#endif ID_PICTURE_HeaderMe
#ifndef ID_LISTIMG_PETSTATE
#define ID_LISTIMG_PETSTATE		"ID_LISTIMG_PETSTATE"
#endif  ID_LISTIMG_PETSTATE


#endif //__UITEAMHERODEFINE_H__
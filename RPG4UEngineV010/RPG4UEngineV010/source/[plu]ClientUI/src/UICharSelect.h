/*////////////////////////////////////////////////////////////////////////
文 件 名：UICharSelect.h
创建日期：2007年11月17日
最后更新：2007年11月17日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UICHARSELECT_H__
#define __UICHARSELECT_H__
#pragma once



#include "VUCtrlInclude.h"
//
//#include "V3DScene.h"
#include "CharacterSceneDefine.h"

#ifndef ID_FRAME_SELECT
#define ID_FRAME_SELECT		"ID_FRAME_SELECT"
#endif  

#ifndef ID_BUTTON_CREATE
#define ID_BUTTON_CREATE		"ID_BUTTON_CREATE"
#endif  
#ifndef ID_BUTTON_EXIT
#define ID_BUTTON_EXIT		"ID_BUTTON_EXIT"
#endif  
#ifndef ID_BUTTON_ENTER
#define ID_BUTTON_ENTER		"ID_BUTTON_ENTER"
#endif  
#ifndef ID_BUTTON_DELETE
#define ID_BUTTON_DELETE		"ID_BUTTON_DELETE"
#endif  
#ifndef ID_BUTTON_LEAVE
#define ID_BUTTON_LEAVE		"ID_BUTTON_LEAVE"
#endif  
#ifndef ID_TEXT_NAME
#define ID_TEXT_NAME		"ID_TEXT_NAME"
#endif  
#ifndef ID_TEXT_LEVEL
#define ID_TEXT_LEVEL		"ID_TEXT_LEVEL"
#endif  
#ifndef ID_TEXT_BIRTHDAY
#define ID_TEXT_BIRTHDAY		"ID_TEXT_BIRTHDAY"
#endif  
#ifndef ID_LIST_PLAYER
#define ID_LIST_PLAYER		"ID_LIST_PLAYER"
#endif  
#ifndef ID_TEXT_JOB
#define ID_TEXT_JOB		"ID_TEXT_JOB"
#endif 


#define MAX_PLAYER_COUNT 5//3

using namespace scene;
namespace gameui
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTUI_API UICharSelect
{
public:
	UICharSelect();

public:
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	// Button
	BOOL ID_BUTTON_CREATEOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_EXITOnButtonClick		( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_ENTEROnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_DELETEOnButtonClick	( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_LEAVEOnButtonClick	( VUCtrlObject* /*pSender*/ );

	// List
	void ID_LIST_PLAYEROnListSelectChange( VUCtrlObject* pSender, sUILIST_ITEM* pItem );
	void ID_LIST_PLAYEROnListLDBClick( VUCtrlObject* pSender, sUILIST_ITEM* pItem );
	void ID_LIST_PLAYEROnListRBClick( VUCtrlObject* pSender, sUILIST_ITEM* pItem);

public:
	BOOL LoadUI();					// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();				// 卸载UI
	BOOL IsVisible();				// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视

	void Refresh();
	void Clear();

public:
	void SelectPlayer(CharListInfo* pPlayerInfo,BOOL bNotifyScene=TRUE);
	void AddPlayer(CharListInfo* pPlayerInfo);
	void RemovePlayer(CharListInfo* pPlayerInfo);
	static BOOL ProcDeletePlayer( LPCSTR  szInputData, void *pData );

public:
	// Member
	VUCtrlFrame*		m_pID_FRAME_SELECT;
	VUCtrlButton*		m_pID_BUTTON_CREATE;
	VUCtrlButton*		m_pID_BUTTON_EXIT;
	VUCtrlButton*		m_pID_BUTTON_ENTER;
	VUCtrlButton*		m_pID_BUTTON_DELETE;
	VUCtrlButton*		m_pID_BUTTON_LEAVE;

	VUCtrlText*			m_pID_TEXT_NAME;
	VUCtrlText*			m_pID_TEXT_LEVEL;
	VUCtrlText*			m_pID_TEXT_Job;
	VUCtrlText*			m_pID_TEXT_BIRTHDAY;
	VUCtrlList*			m_pID_LIST_PLAYER;

public:
};	 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UICharSelect , API_NULL);
//extern API_NULL UICharSelect& GetUICharSelect();
#define theUICharSelect  gameui::GetUICharSelect()

};//gameui




#endif //__UICHARSELECT_H__
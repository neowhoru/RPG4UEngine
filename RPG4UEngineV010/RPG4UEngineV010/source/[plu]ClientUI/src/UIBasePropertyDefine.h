/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBasePropertyDefine.h
创建日期：2007年11月28日
最后更新：2007年11月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIBASEPROPERTYDEFINE_H__
#define __UIBASEPROPERTYDEFINE_H__
#pragma once

#ifndef ID_FRAME_BaseProperty
#define ID_FRAME_BaseProperty		"ID_FRAME_BaseProperty"
#endif  

#ifndef ID_BUTTON_CLOSE
#define ID_BUTTON_CLOSE		"ID_BUTTON_CLOSE"
#endif  

#ifndef ID_TEXT_Name
#define ID_TEXT_Name		"ID_TEXT_Name"
#endif  

#ifndef ID_TEXT_Title
#define ID_TEXT_Title		"ID_TEXT_Title"
#endif  

#ifndef ID_TEXT_Guild
#define ID_TEXT_Guild		"ID_TEXT_Guild"
#endif  

#ifndef ID_TEXT_Level
#define ID_TEXT_Level		"ID_TEXT_Level"
#endif  

#ifndef ID_TEXT_INT
#define ID_TEXT_INT		"ID_TEXT_INT"
#endif  

#ifndef ID_TEXT_VIT
#define ID_TEXT_VIT		"ID_TEXT_VIT"
#endif  

#ifndef ID_TEXT_STR
#define ID_TEXT_STR		"ID_TEXT_STR"
#endif  

#ifndef ID_TEXT_DEX
#define ID_TEXT_DEX		"ID_TEXT_DEX"
#endif  

#ifndef ID_TEXT_KilledNum
#define ID_TEXT_KilledNum		"ID_TEXT_KilledNum"
#endif  

#ifndef ID_TEXT_Work
#define ID_TEXT_Work		"ID_TEXT_Work"
#endif  

#ifndef ID_TEXT_Official
#define ID_TEXT_Official		"ID_TEXT_Official"
#endif  

#ifndef ID_TEXT_Marry
#define ID_TEXT_Marry		"ID_TEXT_Marry"
#endif  

#ifndef ID_TEXT_Distinction
#define ID_TEXT_Distinction		"ID_TEXT_Distinction"
#endif  

#ifndef ID_BUTTON_Action
#define ID_BUTTON_Action		"ID_BUTTON_Action"
#endif  
#ifndef ID_TEXT_SPR
#define ID_TEXT_SPR		"ID_TEXT_SPR"
#endif  

#ifndef ID_TEXT_LRN
#define ID_TEXT_LRN		"ID_TEXT_LRN"
#endif  
#ifndef ID_TEXT_CRE
#define ID_TEXT_CRE		"ID_TEXT_CRE"
#endif  


#ifndef ID_BUTTON_Property
#define ID_BUTTON_Property		"ID_BUTTON_Property"
#endif  ID_BUTTON_Property

#ifndef ID_TEXT_MATTACK_FIRE
#define ID_TEXT_MATTACK_FIRE		"ID_TEXT_MATTACK_FIRE"
#endif  
#ifndef ID_TEXT_MDEF_FIRE
#define ID_TEXT_MDEF_FIRE		"ID_TEXT_MDEF_FIRE"
#endif  
#ifndef ID_TEXT_MATTACK_WATER
#define ID_TEXT_MATTACK_WATER		"ID_TEXT_MATTACK_WATER"
#endif  
#ifndef ID_TEXT_MDEF_WATER
#define ID_TEXT_MDEF_WATER		"ID_TEXT_MDEF_WATER"
#endif  
#ifndef ID_TEXT_MATTACK_WOOD
#define ID_TEXT_MATTACK_WOOD		"ID_TEXT_MATTACK_WOOD"
#endif  
#ifndef ID_TEXT_MDEF_WOOD
#define ID_TEXT_MDEF_WOOD		"ID_TEXT_MDEF_WOOD"
#endif  

#ifndef ID_TEXT_MATTACK_GOLD
#define ID_TEXT_MATTACK_GOLD		"ID_TEXT_MATTACK_GOLD"
#endif  
#ifndef ID_TEXT_MDEF_GOLD
#define ID_TEXT_MDEF_GOLD		"ID_TEXT_MDEF_GOLD"
#endif  

#ifndef ID_TEXT_MATTACK_EARTH
#define ID_TEXT_MATTACK_EARTH		"ID_TEXT_MATTACK_EARTH"
#endif  
#ifndef ID_TEXT_MDEF_EARTH
#define ID_TEXT_MDEF_EARTH		"ID_TEXT_MDEF_EARTH"
#endif  

#ifndef ID_TEXT_Hp
#define ID_TEXT_Hp		"ID_TEXT_Hp"
#endif  
#ifndef ID_TEXT_Mp
#define ID_TEXT_Mp		"ID_TEXT_Mp"
#endif  
#ifndef ID_TEXT_PHYATTACK
#define ID_TEXT_PHYATTACK		"ID_TEXT_PHYATTACK"
#endif  
#ifndef ID_TEXT_PHYDEF
#define ID_TEXT_PHYDEF		"ID_TEXT_PHYDEF"
#endif  
#ifndef ID_TEXT_MAGIC_DEF
#define ID_TEXT_MAGIC_DEF		"ID_TEXT_MAGIC_DEF"
#endif  
#ifndef ID_TEXT_MAGICATTACK
#define ID_TEXT_MAGICATTACK		"ID_TEXT_MAGICATTACK"
#endif  
#ifndef ID_TEXT_RangeAttack
#define ID_TEXT_RangeAttack		"ID_TEXT_RangeAttack"
#endif  
#ifndef ID_TEXT_Hit
#define ID_TEXT_Hit		"ID_TEXT_Hit"
#endif  
#ifndef ID_TEXT_Dodge
#define ID_TEXT_Dodge		"ID_TEXT_Dodge"
#endif  
#ifndef ID_TEXT_EXPERTY1
#define ID_TEXT_EXPERTY1		"ID_TEXT_EXPERTY1"
#endif  
#ifndef ID_TEXT_EXPERTY2
#define ID_TEXT_EXPERTY2		"ID_TEXT_EXPERTY2"
#endif  
#ifndef ID_TEXT_REMAINSTAT
#define ID_TEXT_REMAINSTAT		"ID_TEXT_REMAINSTAT"
#endif  
#ifndef ID_BUTTON_UPGRADE
#define ID_BUTTON_UPGRADE		"ID_BUTTON_UPGRADE"
#endif  

const DWORD ATTRIBUTE_STAT_BASE = ATTRIBUTE_STR;
enum
{
	 UI_ATTRIBUTE_STR		
	,UI_ATTRIBUTE_DEX		
	,UI_ATTRIBUTE_VIT		
	,UI_ATTRIBUTE_INT		
	,UI_ATTRIBUTE_SPR		
	,UI_ATTRIBUTE_LRN		
	,UI_ATTRIBUTE_CRE		

	,UI_ATTRIBUTE_EXPERTY1
	,UI_ATTRIBUTE_EXPERTY2
	,UI_ATTRIBUTE_MAX
};

#endif //__UIBASEPROPERTYDEFINE_H__
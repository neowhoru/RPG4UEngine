/*////////////////////////////////////////////////////////////////////////
文 件 名：UIInventoryDefine.h
DOC 文件：uidata\UIInventory.uidoc
UI  名称：Pack
创建日期：2008年8月4日
最后更新：2008年8月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIINVENTORYDEFINE_H__
#define __UIINVENTORYDEFINE_H__
#pragma once

#include "VUCtrlInclude.h"

namespace gameui
{ 
const DWORD	MAX_PACK_PAGE_NUM			= 5;
const DWORD	MAX_SLOT_PER_PACKPAGE	= 4*6;
const DWORD	PACK_PAGE_GROUP_ID		= TAG('pack');


#define UIINVENTORY_ID_BUTTON_CLOSE				"ID_BUTTON_CLOSE"
#define UIINVENTORY_ID_LISTIMG_Helmet 			"ID_LISTIMG_Helmet"
#define UIINVENTORY_ID_LISTIMG_Armour 			"ID_LISTIMG_Armour"
#define UIINVENTORY_ID_LISTIMG_rWeapon			"ID_LISTIMG_rWeapon"
#define UIINVENTORY_ID_LISTIMG_Belt				"ID_LISTIMG_Belt"
#define UIINVENTORY_ID_LISTIMG_Boots  			"ID_LISTIMG_Boots"
#define UIINVENTORY_ID_LISTIMG_Ring1  			"ID_LISTIMG_Ring1"
#define UIINVENTORY_ID_LISTIMG_Protector 		"ID_LISTIMG_Protector"
#define UIINVENTORY_ID_LISTIMG_Glove  			"ID_LISTIMG_Glove"
#define UIINVENTORY_ID_LISTIMG_Shield 			"ID_LISTIMG_Shield"
#define UIINVENTORY_ID_LISTIMG_Pants  			"ID_LISTIMG_Pants"
#define UIINVENTORY_ID_LISTIMG_Accouterment1	"ID_LISTIMG_Accouterment1"
#define UIINVENTORY_ID_LISTIMG_Ring2  			"ID_LISTIMG_Ring2"
#define UIINVENTORY_ID_LISTIMG_Accouterment 	"ID_LISTIMG_Accouterment"
#define UIINVENTORY_ID_TEXT_MONEY  				"ID_TEXT_MONEY"
#define UIINVENTORY_ID_LISTIMG_lWeapon			"ID_LISTIMG_lWeapon"
#define UIINVENTORY_ID_LISTIMG_PACK				"ID_LISTIMG_PACK"
#define UIINVENTORY_ID_LISTIMG_rubbish			"ID_LISTIMG_rubbish"
#define UIINVENTORY_ID_LISTIMG_Necklace  		"ID_LISTIMG_Necklace"
#define UIINVENTORY_ID_CHECKBOX_PAGE1 			"ID_CHECKBOX_PAGE1"
#define UIINVENTORY_ID_CHECKBOX_PAGE2 			"ID_CHECKBOX_PAGE2"
#define UIINVENTORY_ID_CHECKBOX_PAGE3 			"ID_CHECKBOX_PAGE3"
#define UIINVENTORY_ID_CHECKBOX_PAGE4 			"ID_CHECKBOX_PAGE4"
#define UIINVENTORY_ID_CHECKBOX_PAGE5 			"ID_CHECKBOX_PAGE5"
#define UIINVENTORY_ID_BUTTON_UNLOCK  			"ID_BUTTON_UNLOCK"
#define UIINVENTORY_ID_BUTTON_LOCK 				"ID_BUTTON_LOCK"
#define UIINVENTORY_ID_CHECKBOX_PAGE				"ID_CHECKBOX_PAGE"


};

#endif //__UIINVENTORYDEFINE_H__



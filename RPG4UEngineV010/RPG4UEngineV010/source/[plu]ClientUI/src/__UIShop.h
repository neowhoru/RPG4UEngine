/*////////////////////////////////////////////////////////////////////////
文 件 名：UIShop.h
创建日期：2007年12月15日
最后更新：2007年12月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __UISHOP_H__
#define __UISHOP_H__
#pragma once


#include "VUCtrlInclude.h"

#ifndef ID_FRAME_SHOP
#define ID_FRAME_SHOP		"ID_FRAME_SHOP"
#endif  ID_FRAME_SHOP
#ifndef ID_BUTTON_CLOSE
#define ID_BUTTON_CLOSE		"ID_BUTTON_CLOSE"
#endif  ID_BUTTON_CLOSE
#ifndef ID_LISTIMG_SHOP
#define ID_LISTIMG_SHOP		"ID_LISTIMG_SHOP"
#endif  ID_LISTIMG_SHOP

#ifndef ID_LIST_store
#define ID_LIST_store		"ID_LIST_store"
#endif  ID_LIST_store
#ifndef ID_LIST_price
#define ID_LIST_price		"ID_LIST_price"
#endif  ID_LIST_price
#ifndef ID_LISTIMG_process
#define ID_LISTIMG_process		"ID_LISTIMG_process"
#endif  ID_LISTIMG_process

#ifndef ID_SCROLLBAR_process
#define ID_SCROLLBAR_process	"ID_SCROLLBAR_process"
#endif ID_SCROLLBAR_process

#ifndef ID_BUTTON_buy1
#define ID_BUTTON_buy1		"ID_BUTTON_buy1"
#endif  ID_BUTTON_buy1
#ifndef ID_BUTTON_buy2
#define ID_BUTTON_buy2		"ID_BUTTON_buy2"
#endif  ID_BUTTON_buy2
#ifndef ID_BUTTON_buy3
#define ID_BUTTON_buy3		"ID_BUTTON_buy3"
#endif  ID_BUTTON_buy3
#ifndef ID_BUTTON_buy4
#define ID_BUTTON_buy4		"ID_BUTTON_buy4"
#endif  ID_BUTTON_buy4
#ifndef ID_BUTTON_buy5
#define ID_BUTTON_buy5		"ID_BUTTON_buy5"
#endif  ID_BUTTON_buy5
#ifndef ID_BUTTON_buy6
#define ID_BUTTON_buy6		"ID_BUTTON_buy6"
#endif  ID_BUTTON_buy6
#ifndef ID_BUTTON_buy7
#define ID_BUTTON_buy7		"ID_BUTTON_buy7"
#endif  ID_BUTTON_buy7
#ifndef ID_BUTTON_buy8
#define ID_BUTTON_buy8		"ID_BUTTON_buy8"
#endif  ID_BUTTON_buy8
#ifndef ID_BUTTON_buy9
#define ID_BUTTON_buy9		"ID_BUTTON_buy9"
#endif  ID_BUTTON_buy9
#ifndef ID_SCROLLBAR_process
#define ID_SCROLLBAR_process	"ID_SCROLLBAR_process"
#endif ID_SCROLLBAR_process
#ifndef ID_BUTTON_mend
#define ID_BUTTON_mend	"ID_BUTTON_mend"
#endif ID_BUTTON_mend


class _CLIENTUI_API UI_ID_FRAME_SHOP
{
public:
	struct S_ItemCost
	{
		int nId;		// ID
		int nCost;		// 价格
		int nPerCount;	// 每次收购的最多数量
	};

	  std::vector<S_ItemCost> m_vtItemCost;
	 //S_ItemCost* GetItemCout(int nId);

	// Member
	  VUCtrlFrame*	m_pID_FRAME_SHOP;
	  VUCtrlButton*	m_pID_BUTTON_CLOSE;
	  VUCtrlListImg*	m_pID_LISTIMG_SHOP;
	  VUCtrlList*		m_pID_LIST_store;
	  VUCtrlList*		m_pID_LIST_price;
	// VUCtrlScrollBar* m_pID_Scroll_pro;

	  VUCtrlButton*	m_pID_BUTTON_buy1;
	  VUCtrlButton*	m_pID_BUTTON_buy2;
	  VUCtrlButton*	m_pID_BUTTON_buy3;
	  VUCtrlButton*	m_pID_BUTTON_buy4;
	  VUCtrlButton*	m_pID_BUTTON_buy5;
	  VUCtrlButton*	m_pID_BUTTON_buy6;
	  VUCtrlButton*	m_pID_BUTTON_buy7;
	  VUCtrlButton*	m_pID_BUTTON_buy8;
	  VUCtrlButton*	m_pID_BUTTON_buy9;

	  VUCtrlButton*	m_pID_BUTTON_Mend;	//修理
	  VUCtrlScrollBar* m_pID_ScrollBar_Shop;


	  BYTE				m_byShopType;
	  BOOL				m_bCtrlSell;

	  static BOOL BuyGoodsCallBackFun( const char* szInputData, void *pData );
	  static	BOOL SellGoods(  BOOL bPressYesButton, void *pData  );
	  
	  S_ItemCost* GetItemCout(int nId);


	  UI_ID_FRAME_SHOP();
	// Frame
	 BOOL OnFrameRun(DWORD dwTick);
	 BOOL OnFrameRender(DWORD dwTick);
	// Button
	 BOOL ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender );
	// ListImg / ListEx
	 BOOL ID_LISTIMG_SHOPOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe,
											IconDragListImg* pItemDrag,
											IconDragListImg* pItemDest );
	//BOOL ID_LISTIMG_SHOPOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pMe,
	//										IconDragListImg* pItem );
	 BOOL ID_LISTIMG_SHOPOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem );
	 BOOL ID_LISTIMG_SHOPOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem );
	 BOOL ID_BUTTON_REFRESHOnButtonClick( VUCtrlObject* pSender );
	 void ID_LIST_storeOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem );
	 void ID_LIST_priceOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem );
	 void ID_SCROLLBAR_processScrollChange( VUCtrlObject* pSender, int nValue );
	 BOOL ID_BUTTON_buy1OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy2OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy3OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy4OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy5OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy6OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy7OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy8OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_buy9OnButtonClick( VUCtrlObject* pSender );
	 BOOL ID_BUTTON_mendOnButtonClick( VUCtrlObject* pSender );


	 BOOL LoadUI();				// 载入UI
	 BOOL InitControls();	// 关连控件
	 BOOL UnLoadUI();			// 卸载UI
	 BOOL IsVisible();			// 是否可见
	 void SetVisible( BOOL bVisible );			// 设置是否可视

	 void SetCtrlSell(BOOL bCtrlSell = TRUE);
	 void RefreshBtn();
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL(UI_ID_FRAME_SHOP , API_NULL);
#else
extern API_NULL UI_ID_FRAME_SHOP& GetUI_ID_FRAME_SHOP();
#endif
#define s_UI_ID_FRAME_SHOP  GetUI_ID_FRAME_SHOP()


#endif //__UISHOP_H__
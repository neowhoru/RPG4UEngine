/*////////////////////////////////////////////////////////////////////////
文 件 名：UILogin.h
创建日期：2007年11月15日
最后更新：2007年11月15日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UILOGIN_H__
#define __UILOGIN_H__
#pragma once

#include "VUCtrlInclude.h"
#include "VUIControl.h"
#include "ServerListInfo.h"
#include "UILoginDefine.h"
#include "LogListener.h"


class V3DSceneAnimation	;
class V3DScene				;
class V3DCamera				;

namespace gameui
{
class UIServerGroup;

class _CLIENTUI_API UILogin :public VUIControl , public LogListener
{
public:
	UILogin();
	~UILogin();

public:
	virtual bool Output			( LPCTSTR szError, ... );
	virtual bool OutputWithTime( LPCTSTR szError,... );

public:
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);
	void ID_EDIT_IDOnEditEnter						( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	void ID_EDIT_PASOnEditEnter					( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	BOOL ID_BUTTON_OKOnButtonClick				( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_EXITOnButtonClick				( VUCtrlObject* /*pSender*/ );
	BOOL ID_BUTTON_WEBOnButtonClick				( VUCtrlObject* /*pSender*/ );
	void ID_COMBOBOX_servelistOnComboBoxChange( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ );
	void ID_CHECKBOX_SAVEOnCheckBoxCheck		( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ );
	BOOL ID_BUTTON_CANCELOnButtonClick			( VUCtrlObject* /*pSender*/ );

	BOOL LoadUI();				// 载入UI
	BOOL InitControls();	// 关连控件
	BOOL UnLoadUI();			// 卸载UI
	BOOL IsVisible();			// 是否可见
	void SetVisible( BOOL bVisible );			// 设置是否可视
	void SetEnable( BOOL bEnable );

public:
	void LoadServerList();
	BOOL ChangeGameServer( int nBigArea, int nArea = 0, int nServer = 0 );
	void ChangeGameServerOnly(int nServer);
	void ListShowServerInfo(int nServer);
	int  GetBigAreaCount();
	BOOL ProcessLog( );

public:
	void ShowConnectedDlg();
	void ShowAccountInput(BOOL bShow=TRUE);
	void ShowServerList	(BOOL bShow=TRUE);
	void LockUI	(BOOL bLock=TRUE);

	void SetLoginInfo		(LPCTSTR	szText);
	void SetLoginWaitText(LPCSTR  szText);
	void SetLoginWaitShow(BOOL bShow = TRUE);


public:
	void SetDefaultServer(int defaultserver)	{ m_nServer = defaultserver; }



private:
	void _ReadSetting		();
	void _WriteSetting	();

	VG_PTR_GET_PROPERTY	(UIServerGroup, UIServerGroup);
	VG_BOOL_PROPERTY		(LoginWait);
	VG_BOOL_PROPERTY		(UserReconnect);
	VG_INT_PROPERTY		(ControlMode);			//操作方式 用于 选择界面  在这里读取 保存而已。
	VG_FLOAT_PROPERTY		(SmallMapScale);		//小地图的缩放比例
public:

	VUCtrlFrame*		m_pID_FRAME_LOGIN;
	VUCtrlEdit*			m_pID_EDIT_ID;
	VUCtrlEdit*			m_pID_EDIT_PAS;
	VUCtrlButton*		m_pID_BUTTON_OK;
	VUCtrlText*			m_pID_TEXT_SEREV;
	VUCtrlButton*		m_pID_BUTTON_EXIT;
	VUCtrlButton*		m_pID_BUTTON_WEB;
	VUCtrlComboBox*	m_pID_LIST_SERVER;
	VUCtrlCheckBox*	m_pID_CHECKBOX_SAVE;
	VUCtrlButton*		m_pID_BUTTON_CANCEL;
	VUCtrlText*			m_pID_LoginWait;
	VUCtrlPicture*		m_pID_PICTURE_BACK;

	VUCtrlText*			m_pID_Text_LoginInfo;

protected:	
	ServerListInfo	m_serverList;
	vector<string>	m_BigAreaInfos;
	vector<string>	m_AreaInfos;
	vector<string>	m_NameInfos;


	string			m_strIP;
	DWORD				m_dwPort;

private:
	sLOGIN_ACCOUNT	m_stAccount;
	int				m_nBigAreaIndex;
	int				m_nAreaIndex;
	int				m_nServer;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(UILogin , API_NULL);
//extern API_NULL UILogin& GetUILogin();
#define theUILogin  gameui::GetUILogin()

};//gameui


#endif //__UILOGIN_H__
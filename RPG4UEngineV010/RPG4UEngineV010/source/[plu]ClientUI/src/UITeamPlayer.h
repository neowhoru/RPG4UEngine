/*////////////////////////////////////////////////////////////////////////
文 件 名：UITeamPlayer1.h
创建日期：2007年10月20日
最后更新：2007年10月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIGROUP_PLAYER1_H__
#define __UIGROUP_PLAYER1_H__
#pragma once

#include "VUCtrlInclude.h"

#define ID_FRAME_Group_Player		"ID_FRAME_Group_Player%d"

#ifndef ID_TEXT_Name
#define ID_TEXT_Name		"ID_TEXT_Name"
#endif  ID_TEXT_Name
#ifndef ID_PROGRESS_Hp
#define ID_PROGRESS_Hp		"ID_PROGRESS_Hp"
#endif  ID_PROGRESS_Hp
#ifndef ID_LISTIMG_Status
#define ID_LISTIMG_Status		"ID_LISTIMG_Status"
#endif  ID_LISTIMG_Status
#ifndef ID_PROGRESS_Mp
#define ID_PROGRESS_Mp		"ID_PROGRESS_Mp"
#endif  ID_PROGRESS_Mp
#ifndef ID_TEXT_LVL
#define ID_TEXT_LVL		"ID_TEXT_LVL"
#endif  ID_TEXT_LVL
#ifndef ID_BUTTON_kick
#define ID_BUTTON_kick	"ID_BUTTON_kick"
#endif ID_BUTTON_kick
#ifndef ID_BUTTON_changeHeader
#define ID_BUTTON_changeHeader	"ID_BUTTON_changeHeader"
#endif ID_BUTTON_changeHeader

#define ID_PICTURE_Header	"ID_PICTURE_Header"
#define ID_PICTURE_choise	"ID_PICTURE_choise"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class UITeamPlayer
{
public:
	UITeamPlayer(int nIndex);

public:
	enum	{	TEAM_PLAYER_NUM	= MAX_PARTYMEMBER_NUM	};

public:
	// Frame
	BOOL OnFrameMove(DWORD dwTick);
	BOOL OnRender(DWORD dwTick);


	// ListImg / ListEx
	BOOL ID_LISTIMG_StatusOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pThisCtrl,
											IconDragListImg* pItemDrag,
											IconDragListImg* pItemDest );

	BOOL ID_LISTIMG_StatusOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );
	BOOL ID_LISTIMG_StatusOnIconRButtonUp( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ );

	BOOL OnButtonClickHead( VUCtrlObject* /*pSender*/ );

	BOOL OnBtnCLickKick			( VUCtrlObject* /*pSender*/ );
	BOOL OnbtnClickChangeHeader( VUCtrlObject* /*pSender*/ );
	BOOL OnSelectLookPlayer		( VUCtrlObject* /*pSender*/ );
	BOOL OnSelecMemeberPlayer	( );



	static void UpdateMemberSelect	();

	BOOL LoadUI			();				// 载入UI
	BOOL InitControls	();	// 关连控件
	BOOL UnLoadUI		();			// 卸载UI
	BOOL IsVisible		();			// 是否可见
	void SetVisible	( const  BOOL bVisible );			// 设置是否可视
	void SetPicHeader	(  BOOL bHeader);
	void SetKickEnable(  BOOL bEnable);

	void setID(int nID);


public:
	// Member
	VUCtrlFrame*		m_pID_FRAME_Player;
	VUCtrlText*			m_pID_TEXT_Name;
	VUCtrlProgress*	m_pID_PROGRESS_Hp;
	VUCtrlListImg*		m_pID_LISTIMG_Status;
	VUCtrlProgress*	m_pID_PROGRESS_Mp;
	VUCtrlText*			m_pID_TEXT_LVL;

	VUCtrlPicture*		m_pPicHeader;
	VUCtrlPicture*		m_pPicSelector;

	VUCtrlButton*		m_pBtnKickOff;			//取消邀请 
	VUCtrlButton*		m_pID_BUTTON_changeHeader;

	 int					m_nPlayerID;		//玩家id

	 int					m_nPlayerIndex;

	 static UITeamPlayer*	ms_arGroupPlayer[MAX_PARTYMEMBER_NUM+1];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer1 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer2 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer3 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer4 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer5 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer6 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer7 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer8 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer9 , API_NULL);
GLOBALINST_DECL2(UITeamPlayer, UITeamPlayer10, API_NULL);

#define theUITeamPlayer1  GetUITeamPlayer1()
#define theUITeamPlayer2  GetUITeamPlayer2()
#define theUITeamPlayer3  GetUITeamPlayer3()
#define theUITeamPlayer4  GetUITeamPlayer4()
#define theUITeamPlayer5  GetUITeamPlayer5()
#define theUITeamPlayer6  GetUITeamPlayer6()
#define theUITeamPlayer7  GetUITeamPlayer7()
#define theUITeamPlayer8  GetUITeamPlayer8()
#define theUITeamPlayer9  GetUITeamPlayer9()
#define theUITeamPlayer10 GetUITeamPlayer10()

#endif //__UIGROUP_PLAYER1_H__
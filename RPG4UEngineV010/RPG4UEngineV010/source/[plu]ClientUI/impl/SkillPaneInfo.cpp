/*////////////////////////////////////////////////////////////////////////
文 件 名：UISkillTreeConfig.cpp
创建日期：2008年5月18日
最后更新：2008年5月18日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
本引擎遵照<RPG4U引擎使用许可协议书>约定。
当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
http://www.rpg4u.com 下载。

如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
本引擎本身存在相关侵权问题，则由我们来承担责任。

以下有三类授权许可约定：
1.个人使用授权
在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
献者，则视为自动接受个人使用授权。
你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
共享或授权于其他任何贡献者使用。
当然，不可用于商业用途。

2.公益使用授权
如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
可参考个人使用授权。

3. 商业使用授权
你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
不可做引擎二次商业授权。
不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "SkillPaneInfo.h"
#include "SkillStorageManagerDefine.h"
#include "TableTextFile.h"

SkillPaneInfo::SkillPaneInfo()
{
	m_bLoaded	= FALSE;
}

BOOL	SkillPaneInfo::Load(LPCTSTR szFile)
{
	if(m_bLoaded)
		return TRUE;

	util::TableTextFile file;
	__CHECK(file.OpenFile(szFile) );

	m_bLoaded	= TRUE;

	DWORD	dwCharType;
	DWORD	dwPage;
	DWORD	dwPageIndex(0);
	DWORD	dwNum;
	//	职业	页号	背景图	技能数量	技能0	技能1	技能2	技能3	技能4	技能5	技能6	技能7	技能8	技能9	技能10	技能11	技能12	技能13	技能14	技能15	技能16	技能17	技能18	技能19	技能20	技能21	技能22	技能23	技能24
	for( ;file.GotoNextLine(); )
	{
		file.GetInt();

		file >> dwCharType;
				if(dwCharType >= PLAYERTYPE_MAX || dwCharType < PLAYERTYPE_BASE)
				{
					LOGINFO("SkillPaneInfo invalid chartype %d\n",dwCharType);
					continue;
				}

		file >> dwPage;
				if(dwPage != dwPageIndex)
				{
					LOGINFO("SkillPaneInfo invalid chartype %d  page %d\n",dwCharType, dwPage);
					continue;
				}

		//////////////////////////////////////
		sSKILLPANE_TYPE&	type = m_SkillTypes[dwCharType-PLAYERTYPE_BASE];
		type.m_Pages.resize(type.m_Pages.size() + 1);
		sSKILLPANE_PAGE&	page = type.m_Pages.back();

		//////////////////////////////////////
					page.m_nPageIndex = dwPage;
		file	>> page.m_nSkillType;
		file	>> page.m_szBackgroundPath;
		file	>> dwNum;

		if(dwNum == 0)
			continue;
		page.m_arIcons.resize(dwNum);
		for(UINT n=0; n<dwNum; n++)
		{
			sSKILLPANE_ICON&	icon = page.m_arIcons[n];
			file	>> icon.m_SkillClassID;

			icon.m_Visible	= (icon.m_SkillClassID != 0);
		}

	}
	file.CloseFile();


	return TRUE;
}

sSKILLPANE_PAGE*	 SkillPaneInfo::GetPanePage(ePLAYER_TYPE charType, UINT nPage )
{
	__CHECK2_RANGE2(charType, PLAYERTYPE_BASE , PLAYERTYPE_MAX, NULL);
	UINT	nIndex = (INT)charType-1;

	__CHECK2_RANGE(nPage, m_SkillTypes[nIndex].m_Pages.size(), NULL);

	return &m_SkillTypes[nIndex].m_Pages[nPage];
}






#include "stdafx.h"
/********************************************************************
	Created by UIEditor.exe
	FileName: E:\Code\RunTime\CLIENT\Data\UI\Guild_Announce.cpp
*********************************************************************/
#include <assert.h>
#include "..\..\HRUI\HR_UI_Mgr.h"
#include "Guild_Announce.h"
#include "Guild_Info.h"
#include "Guild_Member.h"
#include "..\..\Common\FuncPerformanceLog.h"
namespace UI_ID_FRAME_Guild_Announce
{
	// Member
	CHR_UI_Frame*	m_pID_FRAME_Guild_Announce = NULL;
	CHR_UI_Edit*	m_pID_EDIT_Info = NULL;
	CHR_UI_Button*	m_pID_BUTTON_Info = NULL;
	CHR_UI_Button*	m_pID_BUTTON_Member = NULL;
	CHR_UI_Button*	m_pID_BUTTON_Mgr = NULL;
	CHR_UI_Button*	m_pID_BUTTON_CLOSE = NULL;

	// Frame
	bool ID_FRAME_Guild_AnnounceOnFrameRun()
	{
		return true;
	}
	bool ID_FRAME_Guild_AnnounceOnFrameRender()
	{
		return true;
	}
	// Edit
	void ID_EDIT_InfoOnEditEnter( CHR_UI_Object* pSender, const char* szData )
	{
	}
	// Button
	bool ID_BUTTON_InfoOnButtonClick( CHR_UI_Object* pSender )
	{
		guardfunc;
		UI_ID_FRAME_Guild_Info::SetVisable( true );
		return true;
		unguard;
	}
	// Button
	bool ID_BUTTON_MemberOnButtonClick( CHR_UI_Object* pSender )
	{
		guardfunc;
		UI_ID_FRAME_Guild_Member::SetVisable( true );
		return true;
		unguard;
	}
	// Button
	bool ID_BUTTON_MgrOnButtonClick( CHR_UI_Object* pSender )
	{
		return true;
	}
	// Button
	bool ID_BUTTON_CLOSEOnButtonClick( CHR_UI_Object* pSender )
	{
		return false;
	}

	// 装载UI
	bool LoadUI()
	{
		guardfunc;
		DWORD dwResult = theHRUIMgr.AddFrame( "interface\\Guild_Announce.itf" );
		if ( dwResult == 0 )
		{
			MESSAGE_BOX("读取文件[UI\\Guild_Announce.UI]失败")
			return false;
		}
		else if ( dwResult != 29566/*文件版本号*/ )
		{
			MESSAGE_BOX("读取文件[UI\\Guild_Announce.UI]与源代码版本不一样")
		}
		return DoControlConnect();
		unguard;
	}
	// 关连控件
	bool DoControlConnect()
	{
		guardfunc;
		theHRUIMgr.OnFrameRun( ID_FRAME_Guild_Announce, ID_FRAME_Guild_AnnounceOnFrameRun );
		theHRUIMgr.OnFrameRender( ID_FRAME_Guild_Announce, ID_FRAME_Guild_AnnounceOnFrameRender );
		theHRUIMgr.OnEditEnter( ID_FRAME_Guild_Announce, ID_EDIT_Info, ID_EDIT_InfoOnEditEnter );
		theHRUIMgr.OnButtonClick( ID_FRAME_Guild_Announce, ID_BUTTON_Info, ID_BUTTON_InfoOnButtonClick );
		theHRUIMgr.OnButtonClick( ID_FRAME_Guild_Announce, ID_BUTTON_Member, ID_BUTTON_MemberOnButtonClick );
		theHRUIMgr.OnButtonClick( ID_FRAME_Guild_Announce, ID_BUTTON_Mgr, ID_BUTTON_MgrOnButtonClick );
		theHRUIMgr.OnButtonClick( ID_FRAME_Guild_Announce, ID_BUTTON_CLOSE, ID_BUTTON_CLOSEOnButtonClick );

		m_pID_FRAME_Guild_Announce = (CHR_UI_Frame*)theHRUIMgr.FindFrame( ID_FRAME_Guild_Announce );
		m_pID_EDIT_Info = (CHR_UI_Edit*)theHRUIMgr.FindControl( ID_FRAME_Guild_Announce, ID_EDIT_Info );
		m_pID_BUTTON_Info = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Guild_Announce, ID_BUTTON_Info );
		m_pID_BUTTON_Member = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Guild_Announce, ID_BUTTON_Member );
		m_pID_BUTTON_Mgr = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Guild_Announce, ID_BUTTON_Mgr );
		m_pID_BUTTON_CLOSE = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Guild_Announce, ID_BUTTON_CLOSE );
		assert( m_pID_FRAME_Guild_Announce );
		assert( m_pID_EDIT_Info );
		assert( m_pID_BUTTON_Info );
		assert( m_pID_BUTTON_Member );
		assert( m_pID_BUTTON_Mgr );
		assert( m_pID_BUTTON_CLOSE );
		return true;
		unguard;
	}
	// 卸载UI
	bool UnLoadUI()
	{
		return theHRUIMgr.RemoveFrame( "interface\\Guild_Announce.itf" );
	}
	// 是否可视
	bool IsVisable()
	{
		guardfunc;
		return m_pID_FRAME_Guild_Announce->IsVisable();
		unguard;
	}
	// 设置是否可视
	void SetVisable( const bool bVisable )
	{
		guardfunc;
		if ( bVisable == true )
		{
			RECT rc={0,0,0,0};
			if ( UI_ID_FRAME_Guild_Info::IsVisable() == true )
			{
				UI_ID_FRAME_Guild_Info::m_pID_FRAME_Guild_Info->GetRealRect( &rc );
				UI_ID_FRAME_Guild_Info::SetVisable( false );
			}
			else if ( UI_ID_FRAME_Guild_Member::IsVisable() == true )
			{
				UI_ID_FRAME_Guild_Member::m_pID_FRAME_Guild_Member->GetRealRect( &rc );
				UI_ID_FRAME_Guild_Member::SetVisable( false );
			}
			m_pID_FRAME_Guild_Announce->SetPos( rc.left, rc.top );
		}
		m_pID_FRAME_Guild_Announce->SetVisable( bVisable );
		unguard;
	}
}

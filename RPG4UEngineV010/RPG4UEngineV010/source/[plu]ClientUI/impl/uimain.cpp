/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMain.cpp
创建日期：2007年11月22日
最后更新：2007年11月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIExitGame.h"
#include "UISkill.h"
#include "UIInventory.h"
#include "ApplicationSetting.h"
#include "UIFriend.h"
#include "MapViewManager.h"
#include "UIChatInput.h"
#include "UIExitGame.h"
#include "VUCtrlIconDragManager.h"
#include "UIHeroRelive.h"
#include "UIAction.h"
#include "PathGameMap.h"
#include "UIBaseProperty.h"
#include "V3DConfig.h"
#include "UITarget.h"
#include "VUILuaFuns.h"
#include "VUILuaScriptManager.h"
#include "UIBargaining.h"
#include "UIMessageBox.h"
#include "TextResManager.h"
#include "UImain.h"
#include "MainUIListener.h"
#include "VHero.h"
#include "Hero.h"
#include "ItemManager.h"
#include "MouseHandler.h"
#include "FormularManager.h"
#include "QuickPaneSlot.h"
#include "UIQuest.h"
#include "GlobalInstancePriority.h"

using namespace gameui;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::MainUIListener& GetUIMainMainUIListenerUIListener()
{
	static gameui::MainUIListener staticMainUIListener;
	return staticMainUIListener;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIMain, ()  , gamemain::eInstPrioClientUI/*UIMain*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE	( theUIMain, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIMain, OnRender )
	UIPROC_BUTTON_CLICK		( theUIMain, ID_BUTTON_FunctionOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIMain, ID_BUTTON_FightStateOnButtonClick )

	UIPROC_ICON_RBUTTON_UP	( theUIMain, ID_LISTIMG_HotkeyOnIconRButtonUp )
	UIPROC_ICON_DRAG_FROM	( theUIMain, ID_LISTIMG_HotkeyOnIconDragFrom )
	UIPROC_ICON_DROP_TO		( theUIMain, ID_LISTIMG_HotkeyOnIconDropTo )
	UIPROC_ICON_LDB_CLICK	( theUIMain, ID_LISTIMG_HotkeyOnIconLDBClick )

	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_StateOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_PackOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_SkillOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_TaskOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_MapOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_SystemOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_GuildOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIMain, ID_BUTTON_FiendOnButtonClick )
};//namespace uicallback
using namespace uicallback;


BOOL UIMain_MsgProc( UINT msg, WPARAM wParam, LPARAM lParam, BOOL bMsgUsed )
{
	

	if ( bMsgUsed == TRUE ||
		theUICtrlManager.GetFocus() == theUIChatInput.m_pID_EDIT_INPUT)
		return FALSE;

	switch( msg ) 
	{
	//case WM_LBUTTONUP:
	//	break;
	//return TRUE;

	case WM_KEYUP:
		{
			VUILuaScript *pScript = (VUILuaScript*)theLuaScriptManager.GetUIScript(SCRIPTFILE_UISCRIPT);

			if( pScript )
				pScript->GetGlobalFunction(LUA_GLOBAL_UIPROC_ONMAINKEYDOWN)( wParam, lParam );
		}
		break;

	case WM_KEYDOWN:
		{
			if( theUICtrlManager.GetFocus() )
			{
				if(theUICtrlManager.GetFocus()->GetControlType() == Type_Edit)	
					break;
			}
			
			theUIMain.OnKeyDown(wParam, lParam);
		}
		break;

	}
	return FALSE;
	
}


UIMain::UIMain()
:VUIControl(gameui::eUIMain)
{
	
	// Member
	m_pID_FRAME_MAIN = NULL;
	m_pID_BUTTON_Function = NULL;
	m_pID_PROGRESS_Exp = NULL;
	m_pID_TEXT_Exp = NULL;
	m_pID_LISTIMG_Hotkey = NULL;
	m_pID_BUTTON_OUT = NULL;
	m_pID_BUTTON_IN = NULL;
	m_pID_PICTURE_BG = NULL;
	m_pID_BUTTON_State = NULL;
	m_pID_BUTTON_Pack = NULL;
	m_pID_BUTTON_Skill = NULL;
	m_pID_BUTTON_Task = NULL;
	m_pID_BUTTON_Map = NULL;
	m_pID_BUTTON_System = NULL;

	m_pID_TEXT_Level = NULL;
	

}
// Frame
BOOL UIMain::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}
BOOL UIMain::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

void UIMain::OnInstallQuick	(BaseContainer* pContainer)
{
	m_pID_LISTIMG_Hotkey->SetSlotContainer(pContainer);
}


void UIMain::PushQuick(CODETYPE	itemID,INT nType, UINT nIndex,UINT nCount)
{
	assert(m_pPlayer);

	sITEMINFO_BASE*		pInfo;
	sSKILLINFO_COMMON*	pSkill;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;
	CODETYPE				vItemCode(INVALID_CODETYPE);
	BOOL					bShowCount(FALSE);

	if(nType == SI_SKILL)
	{
		pSkill	= theSkillInfoParser.GetInfo((SLOTCODE)itemID);
		if(pSkill)
			vItemCode = pSkill->m_VItemCode;
		pTimer = m_pPlayer->GetCoolTimer(pSkill->m_SkillClassCode);
	}
	else
	{
		pInfo			= theItemInfoParser.GetItemInfo(itemID);
		if(pInfo)
			vItemCode= pInfo->m_VItemCode;
		pTimer		= m_pPlayer->GetPlayerCookie().GetItemCoolTimer(pInfo->m_byWasteType);
		bShowCount	= pInfo->IsCanUseWaste();
	}

	pVInfo = theItemInfoParser.GetVItemInfo(vItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;



	stListItem.SetTimer(pTimer);

	stListItem.SetData(itemID
                     ,nCount
							,pVInfo->GetIconType()
							,TRUE
							,bShowCount);

	m_pID_LISTIMG_Hotkey->SetItem( &stListItem, nIndex );
}


void UIMain::PopQuick( const UINT nIndex,BOOL bDeactivate )
{
	IconDragListImg* pCell;

	pCell = m_pID_LISTIMG_Hotkey->GetItemAt(nIndex);

	if(bDeactivate)
	{
		pCell->SetDeactivate(TRUE);
	}
	else
	{
		m_pID_LISTIMG_Hotkey->RemoveItemAt(  nIndex );
	}

	//RefreshHotNumber();
}


void UIMain::PushQuick(BaseSlot *pSlot)
{
	assert(m_pPlayer && pSlot);

	CODETYPE				itemID;
	INT					nType;
	UINT					nIndex;
	UINT					nCount;
	BOOL					bDeactivate;

	sITEMINFO_BASE*		pInfo;
	sSKILLINFO_COMMON*	pSkill;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;
	CODETYPE				vItemCode(INVALID_CODETYPE);
	BOOL					bShowCount(FALSE);

	QuickPaneSlot& slotDat = (QuickPaneSlot&)*pSlot;

	itemID		= slotDat.GetOrgCode();
	nType			= slotDat.GetOrgSlotIndex();
	nIndex		= slotDat.GetPos();
	nCount		= slotDat.GetOverlapCount();
	bDeactivate = slotDat.IsDeactivate();
	if(bDeactivate)
		nCount	= 0;

	if(nType == SI_SKILL)
	{
		pSkill	= theSkillInfoParser.GetInfo((SLOTCODE)itemID);
		if(pSkill)
			vItemCode = pSkill->m_VItemCode;
		pTimer = m_pPlayer->GetCoolTimer(pSkill->m_SkillClassCode);
	}
	else
	{
		pInfo			= theItemInfoParser.GetItemInfo(itemID);
		if(pInfo)
			vItemCode= pInfo->m_VItemCode;
		pTimer		= m_pPlayer->GetPlayerCookie().GetItemCoolTimer(pInfo->m_byWasteType);
		bShowCount	= pInfo->IsCanUseWaste();
	}

	pVInfo = theItemInfoParser.GetVItemInfo(vItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;



	stListItem.SetTimer(pTimer);
	stListItem.SetSlot (pSlot, pVInfo->GetIconType());

	stListItem.SetData(itemID
                     ,nCount
							,pVInfo->GetIconType()
							,TRUE
							,bShowCount);

	m_pID_LISTIMG_Hotkey->SetItem( &stListItem, nIndex );
}


void UIMain::PopQuick(BaseSlot *pSlot)
{
	assert(m_pPlayer && pSlot);

	UINT					nIndex;
	BOOL					bDeactivate;

	IconDragListImg* pCell;
	QuickPaneSlot& slotDat = (QuickPaneSlot&)*pSlot;

	nIndex			= slotDat.GetPos();
	bDeactivate		= slotDat.IsDeactivate();

	pCell = m_pID_LISTIMG_Hotkey->GetItemAt(nIndex);

	if(bDeactivate)
	{
		pCell->SetDeactivate(TRUE);
	}
	else
	{
		m_pID_LISTIMG_Hotkey->RemoveItemAt(  nIndex );
	}

	//
	//RefreshHotNumber();
}

void UIMain::RefreshHotKey()
{
}


void UIMain::RefreshHotKeyEnableSetting()
{
}

void UIMain::Refresh()
{
	float fExpPercent;
	DWORD destexp		= theHero.GetNextExp();
	DWORD curaccumexp	= theFormularManager.GetExpAccumlate(theHero.GetLevel());
	DWORD curexp		= theHero.GetExp();

	
	INT64 iXpForNextLevel			= (INT64)destexp - (INT64)curaccumexp;
	INT64 iCurrentXpForNextLevel	= (INT64)curexp  - (INT64)curaccumexp;

	if (iXpForNextLevel < 1)
		iXpForNextLevel = 1;

	if (iCurrentXpForNextLevel < 0)
		iCurrentXpForNextLevel = 0;

	fExpPercent = ((float)iCurrentXpForNextLevel / (float)iXpForNextLevel) * 100.0f;
	fExpPercent = min(100.0f, fExpPercent);


	m_pID_PROGRESS_Exp->SetValue( fExpPercent/100.f );
	*m_pID_TEXT_Exp	= FMSTR(" %.2f%% ", fExpPercent);


}
// Button
BOOL UIMain::ID_BUTTON_SystemOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	if ( theHero.GetHP() <= 0 )
	{
		theUIHeroRelive.Process();
	}
	else
	{	
		if ( theUIExitGame.IsVisible() )
		{
			theUIExitGame.SetVisible( FALSE );
		}
		else
		{
			theUIExitGame.SetVisible( TRUE );
		}
	}
	return TRUE;
}


// Button
BOOL UIMain::ID_BUTTON_StateOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	if(theUIAction.IsVisible())
		theUIAction.SetLayoutVisible();
	else
		theUIBaseProperty.SetLayoutVisible();

	return TRUE;
	
}
// Button
BOOL UIMain::ID_BUTTON_PackOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	

	if(theUIInventory.IsVisible())
		theUIInventory.SetVisible(FALSE);
	else
		theUIInventory.SetVisible(TRUE);
	return TRUE;
	
}
// Button
BOOL UIMain::ID_BUTTON_SkillOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	if(theUISkill.IsVisible())
		theUISkill.SetVisible(FALSE);
	else
		theUISkill.SetVisible(TRUE);
	return TRUE;
	
}

BOOL UIMain::ID_BUTTON_FunctionOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}
BOOL UIMain::ID_BUTTON_FiendOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	if( theUIFriend.IsVisible())
		theUIFriend.SetVisible( FALSE );
	else
		theUIFriend.SetVisible( TRUE );
	return TRUE;
}

//// Button
BOOL UIMain::ID_BUTTON_GuildOnButtonClick( VUCtrlObject* /*pSender*/ )
{

	return TRUE;
}
// Button
BOOL UIMain::ID_BUTTON_TaskOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	if( theUIQuest.IsVisible() )
		theUIQuest.SetVisible( FALSE);
	else
		theUIQuest.SetVisible( TRUE );
	return TRUE;
}

BOOL UIMain::ID_BUTTON_FightStateOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	//VCharacter *pThisCtrl = singleton::GetPlayerManager().GetMe();
	//MSG_CHANGE_FIGHTSTATE_REQStruct msg;
	//if( theVHero.HasFightFlag(eFIGHT_FLAG::FIGHTFLAG_FIGHTING) )
	//{
	//	msg.bFight = FALSE;
	//	m_pID_BUTTON_FightState->SetupPicNormal();
	//	singleton::GetPlayerManager().SetLockNpc( -1 );
	//}
	//else
	//{
	//	msg.bFight = TRUE;
	//	m_pID_BUTTON_FightState->SetupPicMousePress();
	//}
	//GetNetworkInput().SendMsg( &msg );
	//
	if( !theVHero.HasFightFlag(FIGHTFLAG_FIGHTING) )
	{
	}
	return TRUE;
	
}


// ListImg / ListEx
BOOL UIMain::ID_LISTIMG_HotkeyOnIconDragFrom	(VUCtrlObject*              /*pSender*/
																			,VUCtrlObject*              /*pThisCtrl*/
																			,IconDragListImg* pItemDrag
																			,IconDragListImg* /*pItemDest*/)
{
	if ( pItemDrag->IsNull() )
		return FALSE;
	return FALSE;
}


BOOL UIMain::ID_LISTIMG_HotkeyOnIconDropTo	(VUCtrlObject*              /*pSender*/
                                                      ,VUCtrlObject*              /*pThisCtrl*/
																		,IconDragListImg* pItemDrag
																		,IconDragListImg* /*pItemDest*/)
{
	if ( pItemDrag->IsNull() )
		return FALSE;


	return FALSE;
}

BOOL UIMain::ID_LISTIMG_HotkeyOnIconDragOff( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
													   IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIMain::ID_LISTIMG_HotkeyOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIMain::ID_LISTIMG_HotkeyOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
{
	if ( !pItem->IsNull() )
	{
		if(pSender == m_pID_LISTIMG_Hotkey)
		{
			SLOTPOS	atPos =(SLOTPOS) m_pID_LISTIMG_Hotkey->GetItemIndex(pItem);
			theMouseHandler.DoItemUsing(SI_QUICK, atPos);
		}
	}
	return TRUE;

}


// Button
BOOL UIMain::ID_BUTTON_OUTOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	FunctionBtnOutIn(TRUE);
	return TRUE;
	
}

// Button
BOOL UIMain::ID_BUTTON_INOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	FunctionBtnOutIn(FALSE);
	return TRUE;
	
}
//extern BOOL theApplicationSetting.m_ShowAdvancedMiniMap;
BOOL UIMain::ID_BUTTON_MapOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
//	UIMap::SetVisible( !UIMap::IsVisible() );

	theApplicationSetting.m_ShowAdvancedMiniMap = !theApplicationSetting.m_ShowAdvancedMiniMap;
	return TRUE;
	
}
// 装载UI
BOOL UIMain::LoadUI()
{
	
	GetUIMainMainUIListenerUIListener().RegisterMe();

	m_pID_FRAME_MAIN = theUICtrlManager.LoadFrame( UIDOC_PATH( "Main"), TRUE, TRUE );
	if ( m_pID_FRAME_MAIN == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "Main")"]失败")
			return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UIMain::InitControls()
{
	m_pID_PROGRESS_Exp		= (VUCtrlProgress*)m_pID_FRAME_MAIN->FindControl( ID_PROGRESS_Exp );
	m_pID_TEXT_Exp				= (VUCtrlText*)m_pID_FRAME_MAIN->FindControl		( ID_TEXT_Exp );
	m_pID_LISTIMG_Hotkey		= (VUCtrlListImg*)m_pID_FRAME_MAIN->FindControl	( ID_LISTIMG_Hotkey );
	m_pID_PICTURE_BG			= (VUCtrlPicture*)m_pID_FRAME_MAIN->FindControl	( ID_PICTURE_BG );
	//m_pID_BUTTON_FightState	= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_FightState );
	m_pID_BUTTON_State		= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_State );
	m_pID_BUTTON_Pack			= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_Pack );
	m_pID_BUTTON_Skill		= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_Skill );
	m_pID_BUTTON_Task			= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_Task );
	m_pID_BUTTON_Map			= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_Map );
	m_pID_BUTTON_System		= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_System );
	m_pID_BUTTON_Guild		= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_Guild );
	m_pID_BUTTON_Fiend		= (VUCtrlButton*)m_pID_FRAME_MAIN->FindControl	( ID_BUTTON_Fiend );
	m_pID_TEXT_Level			= (VUCtrlText*)m_pID_FRAME_MAIN->FindControl		( ID_TEXT_Level );

	assert( m_pID_PROGRESS_Exp );
	assert( m_pID_TEXT_Exp );
	assert( m_pID_LISTIMG_Hotkey );


	assert( m_pID_BUTTON_State		);
	assert( m_pID_BUTTON_Pack		);
	assert( m_pID_BUTTON_Skill		);
	assert( m_pID_BUTTON_Task		);
	assert( m_pID_BUTTON_Map		);
	assert( m_pID_BUTTON_System	);
	assert( m_pID_BUTTON_Guild		);
	assert( m_pID_BUTTON_Fiend		);
	
	m_pID_FRAME_MAIN->SetProcOnFrameMove		(  theUIMainOnFrameMove );
	m_pID_FRAME_MAIN->SetProcOnRender			(  theUIMainOnRender );

	//m_pID_BUTTON_Function	->SetProcOnButtonClick		(theUIMainID_BUTTON_FunctionOnButtonClick );
	//m_pID_BUTTON_FightState	->SetProcOnButtonClick		(theUIMainID_BUTTON_FightStateOnButtonClick );
	m_pID_LISTIMG_Hotkey		->SetProcOnDragFrom			(theUIMainID_LISTIMG_HotkeyOnIconDragFrom );
	m_pID_LISTIMG_Hotkey		->SetProcOnDropTo				(theUIMainID_LISTIMG_HotkeyOnIconDropTo );
	m_pID_LISTIMG_Hotkey		->SetProcOnLButtonDBClick	(theUIMainID_LISTIMG_HotkeyOnIconLDBClick );
	m_pID_LISTIMG_Hotkey		->SetProcOnRButtonUp			(theUIMainID_LISTIMG_HotkeyOnIconRButtonUp );

	m_pID_BUTTON_State	->SetProcOnButtonClick(theUIMainID_BUTTON_StateOnButtonClick );
	m_pID_BUTTON_Pack		->SetProcOnButtonClick(theUIMainID_BUTTON_PackOnButtonClick );
	m_pID_BUTTON_Skill	->SetProcOnButtonClick(theUIMainID_BUTTON_SkillOnButtonClick );
	m_pID_BUTTON_Task		->SetProcOnButtonClick(theUIMainID_BUTTON_TaskOnButtonClick );
	m_pID_BUTTON_Map		->SetProcOnButtonClick(theUIMainID_BUTTON_MapOnButtonClick );
	m_pID_BUTTON_System	->SetProcOnButtonClick(theUIMainID_BUTTON_SystemOnButtonClick );
	m_pID_BUTTON_Guild	->SetProcOnButtonClick(theUIMainID_BUTTON_GuildOnButtonClick );
	m_pID_BUTTON_Fiend	->SetProcOnButtonClick(theUIMainID_BUTTON_FiendOnButtonClick );


	m_pID_LISTIMG_Hotkey->SetShowAllInfo( TRUE );
	m_pID_FRAME_MAIN->SetProcOnMsgProc( UIMain_MsgProc );


	m_pID_BUTTON_State	->SetCanFocus(FALSE);
	m_pID_BUTTON_Pack		->SetCanFocus(FALSE);
	m_pID_BUTTON_Skill	->SetCanFocus(FALSE);
	m_pID_BUTTON_Task		->SetCanFocus(FALSE);
	m_pID_BUTTON_Map		->SetCanFocus(FALSE);
	m_pID_BUTTON_System	->SetCanFocus(FALSE);
	m_pID_BUTTON_Guild	->SetCanFocus(FALSE);
	m_pID_BUTTON_Fiend	->SetCanFocus(FALSE);

	m_pID_LISTIMG_Hotkey->SetPickingDisabled( TRUE );

	UISCRIPT_ENABLE( UIOBJ_GAMEIN, m_pID_FRAME_MAIN );
	return TRUE;
	
}

//
BOOL UIMain::FunctionBtnOutIn( BOOL bVisible )
{
	
	if( bVisible )
	{
		m_pID_BUTTON_OUT->SetVisible( FALSE );
		m_pID_BUTTON_IN->SetVisible( TRUE );
	}
	else
	{
		m_pID_BUTTON_OUT->SetVisible( TRUE );
		m_pID_BUTTON_IN->SetVisible( FALSE );
	}
	//m_pID_PICTURE_BG->SetVisible( bVisible );
	m_pID_BUTTON_State->SetVisible( bVisible );
	m_pID_BUTTON_Pack->SetVisible( bVisible );
	m_pID_BUTTON_Skill->SetVisible( bVisible );
	m_pID_BUTTON_Task->SetVisible( bVisible );
	m_pID_BUTTON_Map->SetVisible( bVisible );
	m_pID_BUTTON_System->SetVisible( bVisible );	

	return TRUE;
	
}

// 卸载UI
BOOL UIMain::UnLoadUI()
{
	
	UISCRIPT_DISABLE( UIOBJ_GAMEIN );
	GetUIMainMainUIListenerUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Main") );
	
}
// 是否可视
BOOL UIMain::IsVisible()
{
	
	return m_pID_FRAME_MAIN->IsVisible();
	
}
// 设置是否可视
void UIMain::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_MAIN->SetVisible( bVisible );
	
}




void UIMain::RefreshHotNumber()
{
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UISkill.cpp
创建日期：2007年12月3日
最后更新：2007年12月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UISkill.h"




#include "TextResManager.h"
#include "VUILayoutManager.h"
#include "GlobalInstancePriority.h"
#include "UISkillItemUnitRender.h"
#include "VHero.h"
#include "Hero.h"

using namespace gameui;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UISkill, ()  , gamemain::eInstPrioClientUI/*UISkill*/);

//GLOBALINST_LOCAL(UISkillItemUnitRender,UISkillItemUnitRender,eInstPrioClientUI);
	

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE						( theUISkill, OnFrameMove )
	UIPROC_FRAME_RENDER					( theUISkill, OnRender )
	UIPROC_BUTTON_CLICK		( theUISkill, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUISkill, ID_BUTTON_LevelUpOnButtonClick )
	UIPROC_ICON_RBUTTON_UP	( theUISkill, ID_LISTIMG_SKILL_RButton )
};//namespace uicallback
using namespace uicallback;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SkillUIListener& GetUISkillSkillUIListener()
{
	static SkillUIListener staticSkillUIListener;
	return staticSkillUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SkillUIListener::SkillUIListener()
{
}
SkillUIListener::~SkillUIListener()
{
}

LPCSTR SkillUIListener::GetName()
{
	return "SkillUIListener";
}

EGameUIType SkillUIListener::GetType()
{
	return eUISkill;
}

void SkillUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUISkill.SetVisible((BOOL)dwData);
		}
		break;
	}
}

BOOL SkillUIListener::GetData(DWORD dwType,void* pRet)
{
	switch(dwType)
	{
	case eGetVisible:
		{
			*(BOOL*)pRet = theUISkill.IsVisible();
		}
		break;
	}
	return TRUE;
}

void SkillUIListener::Refresh(BOOL /*bExtra*/)
{
	theUISkill.Refresh();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UISkill::UISkill()
:VUIControl(eUISkill)
{
	
	m_pID_FRAME_SKILL = NULL;
	m_pID_BUTTON_CLOSE = NULL;
	m_pID_TEXT_SkillPoint = NULL;
	m_pID_PICTURE_0_0 = NULL;

	__ZERO(m_SkillImgs);
	__ZERO(m_SkillLevelUpBtns);


	m_nSkillCategory = SKILL_TYPE_INITIATIVE;
}


BOOL UISkill::ID_LISTIMG_SKILL_RButton	(VUCtrlObject*              pSender
													,IconDragListImg* pItem)
{	
	
	theUISkillItemUnitRender.OnRButtonUP( pSender, pItem );

	//theUISkillItemUnitRender.UpdateSkillPage( m_nSkillCategory );
	//Refresh();
	return FALSE;
	
}

// Frame
BOOL UISkill::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UISkill::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}


// Button
BOOL UISkill::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	//SetVisible( FALSE );
	SetLayoutVisible();
	return TRUE;
}


void UISkill::Refresh()
{
	theUISkillItemUnitRender.UpdateSkillPage( );

	if(singleton::ExistHero())
	{
		*m_pID_TEXT_SkillPoint	= theHero.GetCharInfo()->m_dwRemainSkill;
	}
	else
	{
		*m_pID_TEXT_SkillPoint	= "0";
	}
}

// Button
BOOL UISkill::ID_BUTTON_LevelUpOnButtonClick( VUCtrlObject* pSender )
{
	//DWORD dwIndex = (DWORD)pSender->GetUserData();
	//__CHECK_RANGE(dwIndex, MAX_SKILL_NUM);

	theUISkillItemUnitRender.OnLevelUpSkill( pSender );
	//theUISkillItemUnitRender.OnLevelUpSkill( pSender
	//													, m_SkillImgs[dwIndex]->GetItem(0,0) );
	
	return TRUE;
}

// 装载UI
BOOL UISkill::LoadUI()
{
	GetUISkillSkillUIListener().RegisterMe();
	
	m_pID_FRAME_SKILL = theUICtrlManager.LoadFrame( UIDOC_PATH( "Skill") );
	if ( m_pID_FRAME_SKILL == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Skill") "]失败")
			return FALSE;
	}

	m_pID_FRAME_SKILL->SetVisible(FALSE);
	theUILayoutManager.AddFrame( m_pID_FRAME_SKILL );

	return InitControls();
	
}
// 关连控件
BOOL UISkill::InitControls()
{
	
	VUIControl::InitControls();

	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)	m_pID_FRAME_SKILL->FindControl( ID_BUTTON_CLOSE );
	m_pID_TEXT_SkillPoint	= (VUCtrlText*)	m_pID_FRAME_SKILL->FindControl( ID_TEXT_SkillPoint );
	m_pID_PICTURE_0_0			= (VUCtrlPicture*)m_pID_FRAME_SKILL->FindControl( ID_PICTURE_0_0 );

	assert( m_pID_FRAME_SKILL );
	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_TEXT_SkillPoint );
	assert( m_pID_PICTURE_0_0 );


	/// 查找各类图标
	LPCSTR szText;
	for(int i = 0; i<MAX_SKILL_NUM; ++i)
	{
		szText = FMSTR( ID_LISTIMG_SKILL "_%d", i);
		m_SkillImgs[i] = (VUCtrlListImg*)m_pID_FRAME_SKILL->FindControl( szText );
		assert(m_SkillImgs[i]);

		szText = FMSTR( ID_BUTTON_skill "_%d", i);
		m_SkillLevelUpBtns[i] = (VUCtrlButton*)m_pID_FRAME_SKILL->FindControl( szText );
		assert(m_SkillLevelUpBtns[i]);

		m_SkillLevelUpBtns[i]->SetVisible(FALSE);

		///////////////////////////////////////////
		//if(m_SkillImgs[i])
		//	m_SkillImgs[i]->SetUserData(i);
		//if(m_SkillLevelUpBtns[i])
		//	m_SkillLevelUpBtns[i]->SetUserData(i);
	}


	m_pID_FRAME_SKILL->SetProcOnFrameMove		(theUISkillOnFrameMove);
	m_pID_FRAME_SKILL->SetProcOnRender			(theUISkillOnRender, FALSE);
	m_pID_BUTTON_CLOSE->SetProcOnButtonClick	(theUISkillID_BUTTON_CLOSEOnButtonClick );


	theUISkillItemUnitRender.SetBackgroudPic(m_pID_PICTURE_0_0);

	/// 各类图标注册事件
	for(int i=0; i<MAX_SKILL_NUM; ++i)
	{
		theUICtrlManager.OnIconRButtonUp(ID_FRAME_SKILL
                                        ,FMSTR("ID_LISTIMG_SKILL_%d", i)
													 ,theUISkillID_LISTIMG_SKILL_RButton);

		theUICtrlManager.OnButtonClick	(ID_FRAME_SKILL
                                       ,FMSTR("ID_BUTTON_skill_%d", i)
													,theUISkillID_BUTTON_LevelUpOnButtonClick);
	}



	for(int i=0; i<MAX_SKILL_NUM; ++i)
	{
		theUISkillItemUnitRender.AddSkillInfo	( m_SkillImgs[i]
																, m_SkillLevelUpBtns[i]);
		//theUISkillItemUnitRender.AddButtonSkillUp	( m_SkillLevelUpBtns[i] );
	}

	//m_pID_LISTEX_Skill->SetVisible( FALSE );

	UISCRIPT_ENABLE ( UIOBJ_SKILL, m_pID_FRAME_SKILL );
	return TRUE;
	
}

BOOL UISkill::InitSkillTree()
{
	ePLAYER_TYPE nPro = PLAYERTYPE_WARRIOR;
	if(singleton::ExistHero())
		nPro = (ePLAYER_TYPE)theHero.GetCharInfo()->m_byClassCode;

	theUISkillItemUnitRender.LoadCharType( nPro );

	return TRUE;
}

// 卸载UI
BOOL UISkill::UnLoadUI()
{
	GetUISkillSkillUIListener().UnregisterMe();

	theUISkillItemUnitRender.Release();
	UISCRIPT_DISABLE( UIOBJ_SKILL );
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Skill") );
}

BOOL UISkill::IsVisible()
{
	return m_pID_FRAME_SKILL->IsVisible();
}

void UISkill::SetVisible( BOOL bVisible )
{
	if( m_pID_FRAME_SKILL )
	{
		if( bVisible != IsVisible() )
			SetLayoutVisible();
		else
			m_pID_FRAME_SKILL->SetVisible( bVisible );
	}
}


void UISkill::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_SKILL);
}


BOOL UISkill::IsSkillList( const VUCtrlObject* pObject )
{
	for(int i=0; i<MAX_SKILL_NUM; i++)
	{
		if(m_SkillImgs[i] == pObject)
			return TRUE;
	}
	return FALSE;
}


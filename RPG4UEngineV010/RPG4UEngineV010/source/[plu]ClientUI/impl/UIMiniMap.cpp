/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMiniMap.cpp
创建日期：2007年9月18日
最后更新：2007年9月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIMiniMap.h"
#include "MapViewManager.h"
#include "V3Dterrain.h"
#include "V3DGameWorld.h"
#include "ScreenTipManager.h"
#include "SoundLayer.h"
#include "SkillSpecialEffectManager.h"
#include "UICharSelect.h"
#include "UILogin.h"
#include "MiniMapUIListener.h"
#include "VHero.h"
#include "MapViewManager.h"

#include "ApplicationSetting.h"
#include "VHero.h"
#include "MediaPathManager.h"
#include "GameUIManager.h"
#include "ColorDefine.h"
#include "ColorManager.h"
#include "VHeroInputData.h"
#include "SoundLayer.h"


const float MINIMAP_MAX_ZOOM = 2.0f;

using namespace vobject;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::MiniMapUIListener& GetUIMiniMapMiniMapUIListenerUIListener()
{
	static gameui::MiniMapUIListener staticMiniMapUIListener;
	return staticMiniMapUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIMiniMap, ()  , gamemain::eInstPrioClientUI/*UIMiniMap*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE  (theUIMiniMap, OnFrameMove )
	UIPROC_FRAME_RENDER		(theUIMiniMap, OnRender )
	UIPROC_BUTTON_CLICK     (theUIMiniMap, ID_BUTTON_BigMapOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIMiniMap, ID_BUTTON_MapSizeOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIMiniMap, ID_BUTTON_MapSize0OnButtonClick ); 
	UIPROC_CHECK_BOX_CHECK  (theUIMiniMap,	ID_CHECKBOX_AngleOnClick);
};//namespace uicallback
using namespace uicallback;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIMiniMap::UIMiniMap()
:VUIControl(gameui::eUIMiniMap)
{
	// Member
	m_pID_FRAME_MiniMap = NULL;
	m_pID_PICTURE_Map = NULL;


	m_pID_BUTTON_BigMap		= NULL;
	m_pID_TEXT_Name			= NULL;
	m_pID_BUTTON_MapSize		= NULL;
	m_pID_BUTTON_MapSize0	= NULL;
	m_pID_TEXT_PlayerPos		= NULL;
	m_pID_TEXT_Land			= NULL;	
	m_pID_CHECKBOX_Angle		= NULL;

	m_bZoomIn		= TRUE;
	m_fZoomScale	= 1.0f;
	m_nScaleStep	= 10;
}

float UIMiniMap::GetZoomScale()
{
	return m_fZoomScale;
}

// Frame
BOOL UIMiniMap::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}

BOOL UIMiniMap::OnRender(DWORD dwTick)
{
	if (	!theApplicationSetting.m_ShowAdvancedMiniMap
		&& singleton::ExistVHero())
	{
		RECT	rcMinModeWindow;

		theUIMiniMap.m_pID_PICTURE_Map->GetGlobalRect(& rcMinModeWindow);

		if(theMapViewManager.ReadyRender(mapinfo::MAPMODE_MINI
													,rcMinModeWindow
													,theVHero.GetTilePos()
													,theApplicationSetting.m_MiniMapSight))
		{
			theMapViewManager.Render	(dwTick);
		}
	}
	return TRUE;
}

// Button
BOOL UIMiniMap::ID_BUTTON_BigMapOnButtonClick( VUCtrlObject* /*pSender*/ )
{

	if(m_fZoomScale < MINIMAP_MAX_ZOOM)
	{
		m_fZoomScale += 0.2f;
	}
	return TRUE;
	
}



// Button
BOOL UIMiniMap::ID_BUTTON_MapSize0OnButtonClick( VUCtrlObject* /*pSender*/ )
{
	ZoomChange(FALSE);
	return TRUE;
}

// Button
BOOL UIMiniMap::ID_BUTTON_MapSizeOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	ZoomChange(TRUE);
	return TRUE;
}

void UIMiniMap::ZoomChange	(BOOL bZoomIn)
{
	if(bZoomIn)
	{
		if(m_fZoomScale < 1.0f)
			m_fZoomScale += 0.1f;
		else if(m_fZoomScale < 5.f)
			m_fZoomScale += 0.5f;
	}
	else
	{
		if(m_fZoomScale > 1.f)
			m_fZoomScale -= 0.5f;
		else if(m_fZoomScale > 0.5f)
			m_fZoomScale -= 0.1f;
	}
}


// 装载UI
BOOL UIMiniMap::LoadUI()
{
	
	GetUIMiniMapMiniMapUIListenerUIListener().RegisterMe();

	m_pID_FRAME_MiniMap = theUICtrlManager.LoadFrame( UIDOC_PATH( "MiniMap"), TRUE, TRUE );
	if ( m_pID_FRAME_MiniMap == 0 )
	{
		UIMessageLog("读取文件[UI\\MiniMap.UI]失败")
		return FALSE;
	}

	return InitControls();
	
}

void UIMiniMap::ID_CHECKBOX_AngleOnClick(VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/)
{
	theHero.OnQuestDone(0);
	//if(m_pID_CHECKBOX_Angle->IsChecked())
	//{
	//	g_InputData.m_LockCameraMode = TRUE;
	//}
	//else
	//{
	//	g_InputData.m_LockCameraMode = FALSE;
	//}
}


// 关连控件
BOOL UIMiniMap::InitControls()
{
	m_pID_PICTURE_Map				= (VUCtrlPicture*)m_pID_FRAME_MiniMap->FindControl(  ID_PICTURE_Map );
	m_pID_BUTTON_BigMap			= (VUCtrlButton*)m_pID_FRAME_MiniMap->FindControl(  ID_BUTTON_BigMap );
	m_pID_TEXT_Name				= (VUCtrlText*)m_pID_FRAME_MiniMap->FindControl(  ID_TEXT_Name );
	m_pID_BUTTON_MapSize			= (VUCtrlButton*)m_pID_FRAME_MiniMap->FindControl(  ID_BUTTON_MapSize );
	m_pID_BUTTON_MapSize0		= (VUCtrlButton*)m_pID_FRAME_MiniMap->FindControl(  ID_BUTTON_MapSize0 );
	m_pID_TEXT_PlayerPos			= (VUCtrlText*)m_pID_FRAME_MiniMap->FindControl(  ID_TEXT_PlayerPos );
	m_pID_TEXT_Land				= (VUCtrlText*)m_pID_FRAME_MiniMap->FindControl(  ID_TEXT_Land );
	m_pID_CHECKBOX_Angle			= (VUCtrlCheckBox*)m_pID_FRAME_MiniMap->FindControl(  ID_CHECKBOX_Angle);

	assert( m_pID_PICTURE_Map );
	assert( m_pID_BUTTON_BigMap );
	assert( m_pID_TEXT_Name );
	assert( m_pID_BUTTON_MapSize );
	assert( m_pID_BUTTON_MapSize0 );
	assert( m_pID_CHECKBOX_Angle );
	

	m_pID_FRAME_MiniMap->SetProcOnFrameMove(  theUIMiniMapOnFrameMove );
	m_pID_FRAME_MiniMap->SetProcOnRender(  theUIMiniMapOnRender,TRUE );

	m_pID_BUTTON_BigMap	->SetProcOnButtonClick	(  theUIMiniMapID_BUTTON_BigMapOnButtonClick );
	m_pID_BUTTON_MapSize	->SetProcOnButtonClick	(  theUIMiniMapID_BUTTON_MapSizeOnButtonClick );
	m_pID_BUTTON_MapSize0->SetProcOnButtonClick	(  theUIMiniMapID_BUTTON_MapSize0OnButtonClick );
	m_pID_CHECKBOX_Angle	->SetProcOnCheck			(  theUIMiniMapID_CHECKBOX_AngleOnClick);


	m_pID_CHECKBOX_Angle->SetShowTip();


	//SetCheckAngle(g_InputData.m_LockCameraMode);

	m_fZoomScale = theUILogin.GetSmallMapScale();

	//取[0,2] 之间
	m_fZoomScale = m_fZoomScale < 1.0f ? 1.0f : m_fZoomScale > MINIMAP_MAX_ZOOM ? MINIMAP_MAX_ZOOM : m_fZoomScale; 
	
	return TRUE;
	
}
// 卸载UI
BOOL UIMiniMap::UnLoadUI()
{
	
	GetUIMiniMapMiniMapUIListenerUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "MiniMap") );
	
}
// 是否可视
BOOL UIMiniMap::IsVisible()
{
	
	return m_pID_FRAME_MiniMap->IsVisible();
	
}
// 设置是否可视
void UIMiniMap::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_MiniMap->SetVisible( bVisible );
	
}

//
void UIMiniMap::Refresh()
{
	
	//GameLandConfig::SMapSetting *pMapName = NULL;
	static string strLastName = "";
	string strAliasName;
	string strZoneName;
	string strBackMusic;
	V3DGameMap *pLevel = theGameWorld.GetHeroPlotMap();
	if( pLevel )
	{
		INT nX = -1,
			nY = -1;
		if( pLevel->GetTerrain() )
			pLevel->GetTerrain()->GetWorldMapXY( nX, nY );
		LPCSTR  pZoneName = theGameWorld.GetZoneName( nX, nY );
		if ( pZoneName )
		{
			strZoneName = pZoneName;
		}
		LPCSTR  pAliasName = theGameWorld.GetMapAlias( nX, nY );
		if( pAliasName )
		{
			strAliasName = pAliasName;
		}
		LPCSTR  pMusicName = theGameWorld.GetBGMusicFile( nX, nY );
		if ( pMusicName )
			strBackMusic = FMSTR("%s%s",_MEDIAPATH(ePATH_MUSIC),pMusicName);
		else
			strBackMusic.clear();
	}

	if ( strLastName != strAliasName )
	{
		theScreenTipManager.SetTile( strZoneName.c_str(), _COLOR(COLOR_SCREEN_TITLE), 5000 );
		theScreenTipManager.AddInfo( strAliasName.c_str(), _COLOR(COLOR_SCREEN_TEXT), 3000, 1 );
		strLastName			= strAliasName;
		*m_pID_TEXT_Name	= strAliasName;
		// Music
		if ( !strBackMusic.empty() )
		{
		//	StopMusic( );
			theSoundLayer.PlayMusic( strBackMusic.c_str(), TRUE, TRUE );
		//	GetMusic()->LoadFile( strBackMusic.c_str() );
		//	GetMusic()->RePlay();
		}
		else
		{
			theSoundLayer.StopMusic( );
			//GetMusic()->Stop();
		}
	}


	//leo
	if( m_pID_TEXT_PlayerPos )
		*m_pID_TEXT_PlayerPos  = FMSTR("%d %d"
                                    ,(INT)(theVHero.GetPosition().x/MAPTILESIZE)
												,(INT)(theVHero.GetPosition().y/MAPTILESIZE));
}

void UIMiniMap::SetCheckAngle(BOOL bChecked)
{
	m_pID_CHECKBOX_Angle->SetCheck( bChecked );
}

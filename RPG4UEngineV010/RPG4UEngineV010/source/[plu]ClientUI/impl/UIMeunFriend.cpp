/*////////////////////////////////////////////////////////////////////////
文 件 名：UIFriRBMeun.cpp
创建日期：2007年11月6日
最后更新：2007年11月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIMeunFriend.h"
#include "UIChatInput.h"
#include "UIAction.h"
#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(UIMeunFriend, ()  , gamemain::eInstPrioClientUI/*UIMeunFriend*/);
#else
#endif

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIMeunFriend, OnFrameMove )
		UIPROC_FRAME_RENDER( theUIMeunFriend, OnRender )
		UIPROC_BUTTON_CLICK( theUIMeunFriend, ID_BUTTON_SendOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMeunFriend, ID_BUTTON_ReqTeamOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMeunFriend, ID_BUTTON_DelFriOnButtonClick )
};//namespace uicallback
using namespace uicallback;


UIMeunFriend::UIMeunFriend()
{
	m_pID_FRAME_MENU_FRIEND = NULL;
	m_pID_BUTTON_Send = NULL;
	m_pID_BUTTON_ReqTeam = NULL;
	m_pID_BUTTON_DelFriend = NULL;
	

}
// Frame
BOOL UIMeunFriend::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIMeunFriend::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// Button
BOOL UIMeunFriend::ID_BUTTON_SendOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	if( m_strName.length() > 0 )
		theUIChatInput.BeginPrivateChat( m_strName.c_str() );

	SetVisible(FALSE);

	return TRUE;
	
}


// Button
BOOL UIMeunFriend::ID_BUTTON_ReqTeamOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}
// Button
BOOL UIMeunFriend::ID_BUTTON_DelFriOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	return TRUE;
	
}

// 装载UI
BOOL UIMeunFriend::LoadUI()
{
	
	m_pID_FRAME_MENU_FRIEND = theUICtrlManager.LoadFrame( UIDOC_PATH( "MenuFriend") );
	if ( m_pID_FRAME_MENU_FRIEND == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "MenuFriend")"]失败")
			return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UIMeunFriend::InitControls()
{
	m_pID_BUTTON_Send = (VUCtrlButton*)m_pID_FRAME_MENU_FRIEND->FindControl(  ID_BUTTON_Send );
	m_pID_BUTTON_ReqTeam = (VUCtrlButton*)m_pID_FRAME_MENU_FRIEND->FindControl(  ID_BUTTON_ReqTeam );
	m_pID_BUTTON_DelFriend = (VUCtrlButton*)m_pID_FRAME_MENU_FRIEND->FindControl(  ID_BUTTON_DelFri );

	assert( m_pID_BUTTON_Send );
	assert( m_pID_BUTTON_ReqTeam );
	assert( m_pID_BUTTON_DelFriend );
	
	m_pID_FRAME_MENU_FRIEND->SetProcOnFrameMove(  theUIMeunFriendOnFrameMove );
	m_pID_FRAME_MENU_FRIEND->SetProcOnRender(  theUIMeunFriendOnRender );

	m_pID_BUTTON_Send			->SetProcOnButtonClick(theUIMeunFriendID_BUTTON_SendOnButtonClick );
	m_pID_BUTTON_ReqTeam		->SetProcOnButtonClick(theUIMeunFriendID_BUTTON_ReqTeamOnButtonClick );
	m_pID_BUTTON_DelFriend	->SetProcOnButtonClick(theUIMeunFriendID_BUTTON_DelFriOnButtonClick );




	m_pID_FRAME_MENU_FRIEND->SetVisible(FALSE);
	return TRUE;
	
}
// 卸载UI
BOOL UIMeunFriend::UnLoadUI()
{
	
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "MenuFriend") );
	
}
// 是否可视
BOOL UIMeunFriend::IsVisible()
{
	
	return m_pID_FRAME_MENU_FRIEND->IsVisible();
	
}
// 设置是否可视
void UIMeunFriend::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_MENU_FRIEND->SetVisible( bVisible );

	if( bVisible )
	{
		theUICtrlManager.BringToTop( m_pID_FRAME_MENU_FRIEND );
		theUICtrlManager.UpdateControls();
	}
	

}

void UIMeunFriend::SetPos(int x, int y)
{
	
	m_pID_FRAME_MENU_FRIEND->SetPos( x, y );
	
}
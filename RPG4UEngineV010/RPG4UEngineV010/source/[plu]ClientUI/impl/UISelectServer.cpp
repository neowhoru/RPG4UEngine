/*////////////////////////////////////////////////////////////////////////
文 件 名：UISelectServer.cpp
创建日期：2007年4月23日
最后更新：2007年4月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UISelectServer.h"
#include "UILogin.h"
#include "UISelectBox.h"
#include "VUIObjectManager.h"
#include "ApplicationSetting.h"
#include "ServerListInfo.h"
#include "PathInfoParser.h"
#include "IV3DEngine.h"
#include "V3DScene.h"
#include "V3DGameWorld.h"
#include "V3DSceneAnimation.h"
#include "TinyXml.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UISelectServer, ()  , gamemain::eInstPrioClientUI/*UISelectServer*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE		( theUISelectServer, OnFrameMove )
	UIPROC_FRAME_RENDER			( theUISelectServer, OnRender )
	UIPROC_LIST_SELECT_CHANGE	( theUISelectServer, ID_LIST_LOBBYOnListSelectChange )
	UIPROC_LIST_SELECT_CHANGE	( theUISelectServer, ID_LIST_GROUPOnListSelectChange )
	UIPROC_LIST_SELECT_CHANGE	( theUISelectServer, ID_LIST_SERVEROnListSelectChange )
	UIPROC_LIST_LDB_CLICK		( theUISelectServer, ID_LIST_SERVEROnListLDBClick)

	UIPROC_BUTTON_CLICK( theUISelectServer, ID_BUTTON_OKOnButtonClick )
	UIPROC_BUTTON_CLICK( theUISelectServer, ID_BUTTON_CANCELOnButtonClick )
};//namespace uicallback
using namespace uicallback;


UISelectServer::UISelectServer()
{
	// Member
	m_pID_FRAME_SELECTSERVER = NULL;
	m_pID_LIST_GROUP = NULL;
	m_pID_LIST_SERVER = NULL;
	m_pID_BUTTON_OK = NULL;
	m_pID_BUTTON_CANCEL = NULL;
	
	m_pID_LIST_LOBBY = NULL; 
	m_bIsPathLoad = FALSE;

	m_nSelectIndex = 0;
	m_pScene				=	NULL;	 
	m_vPlayerPos	=	Vector3(0,0,0);

}

	// Frame
BOOL UISelectServer::/*ID_FRAME_SELECTSERVER*/OnFrameMove(DWORD /*dwTick*/)
	{
		return TRUE;
	}


	// List
	void UISelectServer::ID_LIST_LOBBYOnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
	{
		
		ChangeGameServer( m_pID_LIST_LOBBY->GetCurSelIndex(), 0, 0 );
		
	}
	// List
	void UISelectServer::ID_LIST_GROUPOnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
	{
		
		ChangeGameServer( m_pID_LIST_LOBBY->GetCurSelIndex(),
							m_pID_LIST_GROUP->GetCurSelIndex(), 0 );
		
	}
	// List
	void UISelectServer::ID_LIST_SERVEROnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
	{
		
		ChangeGameServer( m_pID_LIST_LOBBY->GetCurSelIndex(),
			m_pID_LIST_GROUP->GetCurSelIndex(),
			m_pID_LIST_SERVER->GetCurSelIndex());
		
	}
	void UISelectServer::ID_LIST_SERVEROnListLDBClick( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
	{
		
		ID_BUTTON_OKOnButtonClick( NULL );
		
	}
	// Button
	BOOL UISelectServer::ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		char szTemp[512] = {0};
		sprintf(szTemp, "%s-%s", m_pID_LIST_GROUP->GetCurSelItem()->m_szText, 
			m_pID_LIST_SERVER->GetCurSelItem()->m_szText );

		UnLoadUI();

		theUILogin.LoadUI();

		return TRUE;
		
	}
	// Button
	BOOL UISelectServer::ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		//关闭游戏窗口	
		PostMessage(GetActiveWindow(),WM_CLOSE,0,0);
		return TRUE;
		
	}

	// 装载UI
	BOOL UISelectServer::LoadUI()
	{

		m_pID_FRAME_SELECTSERVER = theUICtrlManager.LoadFrame( UIDOC_PATH( "SelectServer") );
		if ( m_pID_FRAME_SELECTSERVER == 0 )
		{
			UIMessageLog("读取文件[UI\\SelectServer.UI]失败")
			return FALSE;
		}

		return InitControls();
		
	}

	BOOL UISelectServer::ChangeGameServer( int nBigArea, int nArea, int nServer )
	{
		ServerListInfo::sNODE_INFO *pNode = NULL;

		m_BigAreaInfos.clear();
		m_AreaInfos.clear();
		m_NameInfos.clear();

		// BigAreas
		for ( int n=0; n<m_serverList.GetNodeCount(); n++ )
		{
			pNode = m_serverList.GetNode(n);
			BOOL bFound = FALSE;
			for ( UINT m=0; m<m_BigAreaInfos.size(); m++ )
			{
				if( pNode->GetBigArea() == m_BigAreaInfos[m] )
				{
					bFound = TRUE;
					break;
				}
			}
			if ( bFound == TRUE )
				continue;
			m_BigAreaInfos.push_back( pNode->GetBigArea() );
		}
		//
		if ( m_BigAreaInfos.size() > 0 )
		{
			for ( int m=0; m<m_serverList.GetNodeCount(); m++ )
			{
				pNode = m_serverList.GetNode(m);
				if ( pNode->GetBigArea() == m_BigAreaInfos[nBigArea] )
				{
					BOOL bFound = FALSE;
					for ( UINT i=0; i<m_AreaInfos.size(); i++ )
					{
						if ( m_AreaInfos[i] == pNode->GetArea() )
						{
							bFound = TRUE;
							break;
						}
					}
					if ( bFound == FALSE )
					{
						m_AreaInfos.push_back( pNode->GetArea() );
					}
				}
			}
		}
		//
		if ( m_AreaInfos.size() > 0 && m_BigAreaInfos.size() > 0 )
		{
			for ( int m=0; m<m_serverList.GetNodeCount(); m++ )
			{
				pNode = m_serverList.GetNode(m);
				if ( pNode->GetBigArea() == m_BigAreaInfos[nBigArea] &&
					pNode->GetArea() == m_AreaInfos[nArea] )
				{
					m_NameInfos.push_back( pNode->GetName() );
					//
					if ( m_NameInfos.size()-1 == (DWORD)nServer )
					{
						m_strIP = pNode->GetIpAddr();
						m_dwPort = pNode->wPort;
					}
				}
			}
		}
		
		char szTemp[512] = {0};
		sprintf(szTemp, "%s-%s", m_BigAreaInfos[nBigArea].c_str(), 
			m_AreaInfos[nArea].c_str() );

		ListShowServerInfo(nBigArea, nArea, nServer);
		return TRUE;
		
	}

	void UISelectServer::ListShowServerInfo(int /*nBigArea*/, int /*nArea*/, int nServer)
	{
		if( !m_bIsPathLoad)
		{
			m_pID_LIST_SERVER->Clear();
			sUILIST_ITEM stList;

			//}
			for ( UINT n=0; n<m_NameInfos.size(); n++ )
			{
				stList.SetData( m_NameInfos[n].c_str() );
				m_pID_LIST_SERVER->AddItem( &stList );
			}
			m_pID_LIST_SERVER->SetCurSelIndex( nServer );
		}
	}



	BOOL UISelectServer::OnRender(DWORD dwTick)
	{
		
		IV3DRenderer* r = the3DEngine.GetRenderer();

		if( theGameWorld.GetDayNightAnimation() )
			theGameWorld.UpdateDayNightAnimation();

		if( m_pSceneObject )
		{	
			int nPos = m_pSceneObject->GetAnimationCtrl().GetMeshModel()->GetSkeleton()->LookupBone( "bone03" );
			if( nPos != -1 )
				theGameWorld.RenderSky(dwTick, m_pSceneObject->GetAnimationCtrl().GetBonePosAtWorld( nPos ) );
		}

		if( m_pSceneObject )
		{
			int nEyeBoneID = 
				m_pSceneObject->GetAnimationCtrl().GetMeshModel()->GetSkeleton()->LookupBone( "eye01" );
			int nLookAtBoneID =
				m_pSceneObject->GetAnimationCtrl().GetMeshModel()->GetSkeleton()->LookupBone( "lookat01" );
			if( nEyeBoneID != -1 &&
				nLookAtBoneID != -1 )
			{
				m_pCamera->SetViewParams(m_pSceneObject->GetAnimationCtrl().GetBonePosAtWorld( nEyeBoneID    )
                                    ,m_pSceneObject->GetAnimationCtrl().GetBonePosAtWorld( nLookAtBoneID )
												,Vector3D::UNIT_Z);

			}
		}

		r->SetTransform( D3DTS_VIEW, (float*)&m_pCamera->GetViewMatrix() );
		r->SetTransform( D3DTS_PROJECTION, (float*)&m_pCamera->GetProjMatrix() );
		if( m_pScene && m_pSceneObject )
		{			
			m_pScene->RenderSceneObjects();
		}
		return TRUE;
		
	}

	// 关连控件
	BOOL UISelectServer::InitControls()
	{
		m_pID_LIST_GROUP		= (VUCtrlList*)m_pID_FRAME_SELECTSERVER->FindControl(  ID_LIST_GROUP );
		m_pID_LIST_SERVER		= (VUCtrlList*)m_pID_FRAME_SELECTSERVER->FindControl(  ID_LIST_SERVER );
		m_pID_BUTTON_OK		= (VUCtrlButton*)m_pID_FRAME_SELECTSERVER->FindControl(  ID_BUTTON_OK );
		m_pID_BUTTON_CANCEL	= (VUCtrlButton*)m_pID_FRAME_SELECTSERVER->FindControl(  ID_BUTTON_CANCEL );

		assert( m_pID_LIST_GROUP );
		assert( m_pID_LIST_SERVER );
		assert( m_pID_BUTTON_OK );
		assert( m_pID_BUTTON_CANCEL );

		
		m_pID_FRAME_SELECTSERVER->SetProcOnFrameMove	(  theUISelectServerOnFrameMove );
		m_pID_FRAME_SELECTSERVER->SetProcOnRender		(  theUISelectServerOnRender, TRUE );
		
		m_pID_LIST_SERVER	->SetProcOnSelectChange		(	theUISelectServerID_LIST_SERVEROnListSelectChange );
		m_pID_LIST_SERVER	->SetProcOnLButtonDBClick	(	theUISelectServerID_LIST_SERVEROnListLDBClick );
		m_pID_BUTTON_OK	->SetProcOnButtonClick		(	theUISelectServerID_BUTTON_OKOnButtonClick );
		m_pID_BUTTON_CANCEL->SetProcOnButtonClick		(	theUISelectServerID_BUTTON_CANCELOnButtonClick );

		
		if(!m_bIsPathLoad)
		{
			m_serverList.LoadFromFile(_PATH_SETTING("ServerList.ini"));

			LoadBigAreaInfo("UI\\patch\\config.xml");
			ChangeGameServer( m_nSelectIndex, 0, 0 );
		}
		else
		{
			m_bIsPathLoad = FALSE;
			LoadBigAreaInfo("UI\\patch\\config.xml");
			ListShowServerInfo(m_nSelectIndex, 0, 0);
		}


		UISCRIPT_ENABLE ( UIOBJ_SELECTSERVER, m_pID_FRAME_SELECTSERVER );
		m_pCamera = new V3DCamera;

		Vector3 vFromPt	= Vector3( 0.0f, 300.0f, 200.0f );
		Vector3 vLookatPt	= Vector3( 0.0f, 0.0f, 0.0f );
		Vector3 vUpVec		= Vector3( 0.0f, 0.0f, 1.0f );
		m_pCamera->SetViewParams( vFromPt, vLookatPt, Vector3D::UNIT_Z );

		float fAspect = (float)SCREEN_WIDTH/SCREEN_HEIGHT;
		m_pCamera->SetProjParams( math::cPI*(50/45.0f)/4, fAspect,  1.0f, 18000.0f );

		

		theGameWorld.LoadWorld	( theGeneralGameParam.GetLoginSceneName()
										, 1
										, 1 );
		if( theGameWorld.GetHeroPlotMap() )
		{
			m_pScene = theGameWorld.GetHeroPlotMap()->GetScene();
			m_pSceneObject = (V3DSceneAnimation*)m_pScene->FindSceneObjectByFile(_PATH_SCENEMODEL("interface\\debarkation_1\\debarkation_1.mesh") );
		}

		return TRUE;
		
	}

	// 卸载UI
	BOOL UISelectServer::UnLoadUI()
	{
		
		theGameWorld.ReleaseWorkingData();
		theGameWorld.Shutdown();	
		if( m_pCamera )
		{
			delete m_pCamera;
			m_pCamera = NULL;
		}
		return theUICtrlManager.RemoveFrame( UIDOC_PATH( "SelectServer") );
		
	}

	// 是否可视
	BOOL UISelectServer::IsVisible()
	{
		
		return m_pID_FRAME_SELECTSERVER->IsVisible();
		
	}
	// 设置是否可视
	void UISelectServer::SetVisible( BOOL bVisible )
	{
		
		m_pID_FRAME_SELECTSERVER->SetVisible( bVisible );
		
	}

	void UISelectServer::LoadServerList()
	{
		m_serverList.LoadFromFile(_PATH_SETTING("serverlist.ini"));
		
	}

	void UISelectServer::IsPatchLoad()
	{
		
		m_bIsPathLoad = TRUE;
		
	}

	void UISelectServer::SetSelectIndex(int nIndex)
	{
		
		m_nSelectIndex = nIndex;
		
	}

	void UISelectServer::LoadBigAreaInfo(LPCSTR  szName)
	{
		
		TiXmlDocument doc(szName);
		if( !doc.LoadFile() )
		{
			UIMessageLog("[SelectServer.cpp LoadBigAreaInfo]读取文件[config.xml]失败")
			return;
		}
		int nValue = 0;

		TiXmlElement* lpRoot	= doc.FirstChildElement("selectserver");
		if(lpRoot)
		{
			lpRoot->Attribute("bigarea", &nValue);
			if(nValue < m_serverList.GetNodeCount())
				SetSelectIndex(nValue);
		}
		
	}

	int UISelectServer::GetBigAreaCount()
	{
		
		if(m_serverList.GetBigAreaCount() > 0)
		{
			int nCount = m_serverList.GetBigAreaCount();
			return nCount;
		}
		return 0;
		
	}



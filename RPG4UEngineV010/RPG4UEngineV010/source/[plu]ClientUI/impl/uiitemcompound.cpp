/*////////////////////////////////////////////////////////////////////////
文 件 名：UIItemCompound.h
DOC 文件：uidata\UIItemCompound.uidoc
UI  名称：ItemCompound
创建日期：2008年6月14日
最后更新：2008年6月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UIItemCompound.h"
#include "VUCtrlManager.h"
#include "VUILayoutManager.h"
#include "UIInventory.h"
#include "BaseContainer.h"
#include "ItemManager.h"
#include "Player.h"
#include "ItemCompoundDialog.h"
#include "UIItemCompositeSlotUIListener.h"
#include "InputLayer.h"
#include "ItemInfoParser.h"
#include "Hero.h"
#include "HeroTipLayer.h"
#include "VUIIconMouseTip.h"
#include "VUCtrlIconDragManager.h"
#include "ItemSlot.h"
#include "ItemSlotContainer.h"
#include "ItemCompositeParser.h"
#include "ConstTextRes.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIItemCompound, ()  , gamemain::eInstPrioClientUI);

#define USE_ITEMCOMPOUND_LAYOUT

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemCompoundUIListener& GetUIItemCompoundItemCompoundUIListener()
{
	static ItemCompoundUIListener staticItemCompoundUIListener;
	return staticItemCompoundUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIItemCompound, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIItemCompound, OnRender )
	UIPROC_BUTTON_CLICK  				( theUIItemCompound, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK  				( theUIItemCompound, ID_BUTTON_HELPOnButtonClick )
	UIPROC_LIST_SELECT_CHANGE  		( theUIItemCompound, ID_LIST_DIRECTORYOnListSelectChange )
	UIPROC_SCROLL_BAR_UPDATE_POS  	( theUIItemCompound, ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos )
	UIPROC_BUTTON_CLICK  				( theUIItemCompound, ID_BUTTON_CANCELOnButtonClick )
	UIPROC_ICON_DROP_TO  				( theUIItemCompound, ID_LISTIMG_RESULTOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemCompound, ID_LISTIMG_RESULTOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemCompound, ID_LISTIMG_RESULTOnIconRButtonUp )
	UIPROC_COMBO_BOX_CHANGE 			( theUIItemCompound, ID_COMBOBOX_GROUPOnComboBoxChange )
	UIPROC_COMBO_BOX_CHANGE 			( theUIItemCompound, ID_COMBOBOX_SUBGROUPOnComboBoxChange )
	UIPROC_ICON_DROP_TO  				( theUIItemCompound, ID_LISTIMG_DIRECTORYOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemCompound, ID_LISTIMG_DIRECTORYOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemCompound, ID_LISTIMG_DIRECTORYOnIconRButtonUp )
	UIPROC_ICON_DROP_TO  				( theUIItemCompound, ID_LISTIMG_MATERIALOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemCompound, ID_LISTIMG_MATERIALOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemCompound, ID_LISTIMG_MATERIALOnIconRButtonUp )
	UIPROC_SCROLL_BAREX_UPDATE_POS	( theUIItemCompound, ID_SCROLLBAREX_AMOUNTOnScrollBarExUpdatePos )
	UIPROC_BUTTON_CLICK  				( theUIItemCompound, ID_BUTTON_MAKEOnButtonClick )
	UIPROC_BUTTON_CLICK  				( theUIItemCompound, ID_BUTTON_CLEAROnButtonClick )
	//UIPROC_SCROLL_BAREX_UPDATE_POS	( theUIItemCompound, ID_SCROLLBAREX_SUCCEEDRATEOnScrollBarExUpdatePos )
	UIPROC_CHECK_BOX_CHECK  			( theUIItemCompound, ID_CHECKBOX_RATEOnCheckBoxCheck )

	UIPROC_EDIT_ENTER 					( theUIItemCompound, ID_EDIT_NAMEOnEditEnter )
	UIPROC_ICON_DROP_TO  				( theUIItemCompound, ID_LISTIMG_SUBRESULTOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemCompound, ID_LISTIMG_SUBRESULTOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemCompound, ID_LISTIMG_SUBRESULTOnIconRButtonUp )
	UIPROC_EDIT_ENTER 					( theUIItemCompound, ID_EDIT_AMOUNTOnEditEnter )

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemCompoundUIListener::ItemCompoundUIListener()
{
}

ItemCompoundUIListener::~ItemCompoundUIListener()
{
}

LPCSTR ItemCompoundUIListener::GetName()
{
	return "ItemCompoundUIListener";
}

EGameUIType ItemCompoundUIListener::GetType()
{
	return eUIItemCompound;
}

void ItemCompoundUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUIItemCompound.SetVisible((BOOL)dwData);
		}break;

	case eSetMoney:
		{
			theUIItemCompound.SetMoney(*(MONEY*)dwData);
		}break;
	}
}

BOOL ItemCompoundUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void ItemCompoundUIListener::Refresh(BOOL /*bExtra*/)
{
	theUIItemCompound.Refresh();
}

void ItemCompoundUIListener::TriggerFunc(ETriggerData eData,LPARAM lpData)
{
	switch(eData)
	{
	case eFillNeedMaterials:
		{
			theUIItemCompound.FillNeedMaterials();
		}break;
	case eOnItemCompoundFinished:
		{
			theUIItemCompound.OnItemCompoundFinished((MSG_OBJECT_BASE*)lpData);
		}break;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIItemCompound::UIItemCompound()
{
	m_pID_FRAME_ITEMCOMPOUND		= NULL;
	m_pID_LIST_DIRECTORY				= NULL;
	m_pID_LISTIMG_DIRECTORY			= NULL;
	m_pID_SCROLLBAR_DIRECTORY 		= NULL;
	m_pID_COMBOBOX_GROUP				= NULL;
	m_pID_COMBOBOX_SUBGROUP			= NULL;
	m_pID_SCROLLBAREX_AMOUNT  		= NULL;
	m_pID_BUTTON_MAKE					= NULL;
	m_pID_TEXT_SUBRATE  				= NULL;
	m_pID_EDIT_AMOUNT					= NULL;

	m_pCurrentGroup					= 0;
	m_pCurrentSubGroup				= 0;
}

UIItemCompound::~UIItemCompound()
{
	//UnLoadUI();
	
}


// Frame
BOOL UIItemCompound::OnFrameMove(DWORD dwTick)
{
	_SUPER::OnFrameMove(dwTick);
	return TRUE;
	
}


BOOL UIItemCompound::OnRender(DWORD dwTick)
{
	_SUPER::OnRender(dwTick);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UIItemCompound::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	//ItemCompoundDialog* pDialog = (ItemCompoundDialog*)theUIItemCompositeSlotUIListener.GetContainer();
	theItemCompoundDialog.CloseWindow();
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_HELP::OnButtonClick
BOOL UIItemCompound::ID_BUTTON_HELPOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LIST_DIRECTORY::OnListSelectChange
void UIItemCompound::ID_LIST_DIRECTORYOnListSelectChange(VUCtrlObject* pSender, sUILIST_ITEM*	/*pItem*/)
{
	if(pSender == m_pID_LIST_DIRECTORY)
	{
		sUILIST_ITEM *pList = m_pID_LIST_DIRECTORY->GetCurSelItem();
		m_pCurrentComposite	= (sITEMCOMPOSITE*)pList->m_pData;
		RefreshComposite();
	}
}

////////////////////////////////////////////////////
//ID_SCROLLBAR_DIRECTORY::OnScrollBarUpdatePos
void UIItemCompound::ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos(VUCtrlObject* pSender, int nValue)
{
	if(pSender == m_pID_SCROLLBAR_DIRECTORY)
	{
		INT nMaxShowCount;
		int nCnt;

		nMaxShowCount	= m_pID_LIST_DIRECTORY->GetHeight() / m_pID_LIST_DIRECTORY->GetListItemHeight();
		nCnt				= m_pID_LIST_DIRECTORY->GetListItemCnt();

		if( nCnt > nMaxShowCount )
		{
			m_pID_SCROLLBAR_DIRECTORY->SetMaxValue(nCnt - nMaxShowCount);
			m_pID_SCROLLBAR_DIRECTORY->SetStepValue(1);
		}
		if( m_pID_LIST_DIRECTORY->GetListItemCnt() > nMaxShowCount )
		{
			m_pID_LIST_DIRECTORY->SetStartIndex( nValue );
			m_pID_LISTIMG_DIRECTORY->SetStartIndex( nValue );
		}
	}
}

////////////////////////////////////////////////////
//ID_BUTTON_CANCEL::OnButtonClick
BOOL UIItemCompound::ID_BUTTON_CANCELOnButtonClick(VUCtrlObject* pSender )
{
	ID_BUTTON_CLOSEOnButtonClick(pSender);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconDropTo
BOOL UIItemCompound::ID_LISTIMG_RESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconLDBClick
BOOL UIItemCompound::ID_LISTIMG_RESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconRButtonUp
BOOL UIItemCompound::ID_LISTIMG_RESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_COMBOBOX_GROUP::OnComboBoxChange
void UIItemCompound::ID_COMBOBOX_GROUPOnComboBoxChange(VUCtrlObject* pSender, LPCSTR 				/*szData*/)
{
	if(pSender == m_pID_COMBOBOX_GROUP)
	{
		sUILIST_ITEM *pList = m_pID_COMBOBOX_GROUP->GetListBox().GetCurSelItem();
		m_pCurrentGroup	= (sITEMCOMPOSITE_GROUP*)pList->m_pData;
		RefreshSubGroup();
		//m_pID_COMBOBOX_GROUP
	}
}

////////////////////////////////////////////////////
//ID_COMBOBOX_SUBGROUP::OnComboBoxChange
void UIItemCompound::ID_COMBOBOX_SUBGROUPOnComboBoxChange(VUCtrlObject* pSender, LPCSTR 				/*szData*/)
{
	if(pSender == m_pID_COMBOBOX_SUBGROUP)
	{
		sUILIST_ITEM *pList = m_pID_COMBOBOX_SUBGROUP->GetListBox().GetCurSelItem();
		m_pCurrentSubGroup	= (sITEMCOMPOSITE_SUBGROUP*)pList->m_pData;
		RefreshDirectory();
		//m_pID_COMBOBOX_GROUP
	}
}

////////////////////////////////////////////////////
//ID_LISTIMG_DIRECTORY::OnIconDropTo
BOOL UIItemCompound::ID_LISTIMG_DIRECTORYOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_DIRECTORY::OnIconLDBClick
BOOL UIItemCompound::ID_LISTIMG_DIRECTORYOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_DIRECTORY::OnIconRButtonUp
BOOL UIItemCompound::ID_LISTIMG_DIRECTORYOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconDropTo
BOOL UIItemCompound::ID_LISTIMG_MATERIALOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconLDBClick
BOOL UIItemCompound::ID_LISTIMG_MATERIALOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconRButtonUp
BOOL UIItemCompound::ID_LISTIMG_MATERIALOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_SCROLLBAREX_AMOUNT::OnScrollBarExUpdatePos
void UIItemCompound::ID_SCROLLBAREX_AMOUNTOnScrollBarExUpdatePos(VUCtrlObject* pSender, int							nValue)
{
	if(pSender == m_pID_SCROLLBAREX_AMOUNT)
	{
		*m_pID_EDIT_AMOUNT = FMSTR("%d", nValue/10);
	}
}

////////////////////////////////////////////////////
//ID_BUTTON_MAKE::OnButtonClick
BOOL UIItemCompound::ID_BUTTON_MAKEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	if(m_pCurrentComposite == NULL)
	{
		OUTPUTTOP(TEXTRES_SELECT_ITEM_FOR_COMPOUND);
		return FALSE;
	}

	if(m_nActionMode == COMPOSITE_WORKING)
	{
		//IconDragListImg*	pMaterial;
		//pMaterial = m_pID_LISTIMG_MATERIAL->GetItemAt(sITEMCOMPOSITERESULT::RESULT_SUBBASE);
		//if(pMaterial && !pMaterial->IsDeactivate())
		//{
		//	m_nActionMode = COMPOSITE_WAITING;
		//	theItemCompoundDialog.Enchant();
		//	return TRUE;
		//}
		if(!theItemCompoundDialog.CheckValidMaterails())
		{
			OUTPUTTOP(TEXTRES_RC_ITEM_INVALID_CONDITION);
			return FALSE;
		}

		m_nActionMode = COMPOSITE_WAITING;
		if(!theItemCompoundDialog.Enchant())
		{
			m_nActionMode = COMPOSITE_WORKING;
			OUTPUTTOP(TEXTRES_NOT_ENCHANT_ITEM);
			return FALSE;
		}

		return TRUE;
	}

	if(m_nActionMode == COMPOSITE_NEXT)
	{
		m_nActionMode = COMPOSITE_WORKING;
		theIconMouseTip.SetLockKey(TIP_LOCKEKY,0);
		theIconMouseTip.SetVisible(FALSE);

		RefreshComposite();
		//m_pID_LIST_DIRECTORY->SetEnable(TRUE);
		//m_pID_COMBOBOX_GROUP->SetEnable(TRUE);
		//m_pID_COMBOBOX_SUBGROUP->SetEnable(TRUE);
		//m_pID_BUTTON_MAKE->SetEnable(TRUE);
	}
	
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLEAR::OnButtonClick
BOOL UIItemCompound::ID_BUTTON_CLEAROnButtonClick(VUCtrlObject* /*pSender*/ )
{
	// *m_pID_EDIT_AMOUNT	= "1";
	// *m_pID_TEXT_MONEY		= ""; 
	RefreshComposite();
	return FALSE;
}


////////////////////////////////////////////////////
//ID_CHECKBOX_RATE::OnCheckBoxCheck
void UIItemCompound::ID_CHECKBOX_RATEOnCheckBoxCheck(VUCtrlObject* pSender, BOOL*						/*pbChecked*/)
{

	if(pSender->GetGroupID() == SUCCEEDRATE_GROUP)
	{
		ChangeSucceedRate(pSender->GetIndexAtGroup());
	//eSUCCEEDRATE_NUM
	}
}



////////////////////////////////////////////////////
//ID_EDIT_NAME::OnEditEnter
void UIItemCompound::ID_EDIT_NAMEOnEditEnter(VUCtrlObject* /*pSender*/, LPCSTR 				/*szData*/ )
{
	
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconDropTo
BOOL UIItemCompound::ID_LISTIMG_SUBRESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconLDBClick
BOOL UIItemCompound::ID_LISTIMG_SUBRESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconRButtonUp
BOOL UIItemCompound::ID_LISTIMG_SUBRESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_EDIT_AMOUNT::OnEditEnter
void UIItemCompound::ID_EDIT_AMOUNTOnEditEnter(VUCtrlObject* pSender, LPCSTR 				szData )
{
	if(pSender == m_pID_EDIT_AMOUNT)
	{
		m_pID_SCROLLBAREX_AMOUNT->SetValue(atoi(szData) * 10);
	}
}



// 设置是否可视
void UIItemCompound::SetVisible( const BOOL bVisible )
{
	_SUPER::SetVisible(bVisible);

#ifdef USE_ITEMCOMPOUND_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_ITEMCOMPOUND->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		theIconMouseTip.SetLockKey(TIP_LOCKEKY, FALSE);
		theIconMouseTip.SetVisible(FALSE);
		m_pID_FRAME_ITEMCOMPOUND->SetArrangeMode( ArrayMode_Normal );
	}
	else
	{
		//Refresh();
	}
#else
		m_pID_FRAME_ITEMCOMPOUND->SetVisible( bVisible );
#endif

	theUIInventory.SetVisible(bVisible);

	theInputLayer.SetKBActivated(!bVisible);
	//m_pID_FRAME_ITEMCOMPOUND->SetForceFocus(bVisible);
	//m_pID_FRAME_ITEMCOMPOUND->SetActivate(bVisible);

}

void UIItemCompound::SetLayoutVisible()
{
#ifdef USE_ITEMCOMPOUND_LAYOUT
	theUILayoutManager.SetVisible(m_pID_FRAME_ITEMCOMPOUND);
#endif
}

void UIItemCompound::CloseWindow()
{
	theItemCompoundDialog.CloseWindow();
}

void UIItemCompound::Refresh()
{
	RefreshGroup();
}

void UIItemCompound::RefreshGroup()
{
	sITEMCOMPOSITE_GROUP*	pGroup;

	VUCtrlList& groupList		= m_pID_COMBOBOX_GROUP->GetListBox();
	VUCtrlEdit& groupEdit		= m_pID_COMBOBOX_GROUP->GetEditInput();
	groupList.Clear();
	groupEdit.SetText("");


	ItemCompositeGroupTable* pGroupTable;
	pGroupTable = theItemCompositeParser.GetCompositeGroupTable();
	__VERIFY2_PTR(pGroupTable, "无效的GroupTable\n",;);

	pGroupTable->SetFirst();
	while( pGroup = pGroupTable->GetNext() )
	{	
		sUILIST_ITEM	item;
		item.SetData((LPCSTR)pGroup->sName,0,pGroup);
		groupList.AddItem(&item);
	}

	m_pCurrentGroup = pGroupTable->GetHeadData();
	if(m_pCurrentGroup)
		groupEdit.SetText((LPCSTR)m_pCurrentGroup->sName);

	RefreshSubGroup();
}


void UIItemCompound::RefreshSubGroup()
{
	sITEMCOMPOSITE_SUBGROUP*	pSubGroup;

	VUCtrlList& subGroupList	= m_pID_COMBOBOX_SUBGROUP->GetListBox();
	VUCtrlEdit& subGroupEdit	= m_pID_COMBOBOX_SUBGROUP->GetEditInput();
	subGroupList.Clear();
	subGroupEdit.SetText("");

	if(m_pCurrentGroup == NULL)
		return;

	m_pCurrentGroup->subGroupTable.SetFirst();
	while( pSubGroup = m_pCurrentGroup->subGroupTable.GetNext() )
	{	
		sUILIST_ITEM	item;
		item.SetData((LPCSTR)pSubGroup->sName,0,pSubGroup);
		subGroupList.AddItem(&item);
	}

	m_pCurrentSubGroup = m_pCurrentGroup->subGroupTable.GetHeadData();
	if(m_pCurrentSubGroup)
		subGroupEdit.SetText((LPCSTR)m_pCurrentSubGroup->sName);

	RefreshDirectory();
}

void UIItemCompound::RefreshDirectory()
{
	sITEMCOMPOSITE*	pComposite;
	//CODETYPE				itemID;

	m_pID_LISTIMG_DIRECTORY->Clear();
	m_pID_LIST_DIRECTORY->Clear();

	if(m_pCurrentSubGroup)
	{
		m_pCurrentSubGroup->itemCompositeTable.SetFirst();
		while(pComposite = m_pCurrentSubGroup->itemCompositeTable.GetNext())
		{
			IconDragListImg	stListItem;
			stListItem.SetData	(pComposite->m_vItemID
										,1
										,VITEMTYPE_ICON);

			m_pID_LISTIMG_DIRECTORY->InsertItem( &stListItem );

			sUILIST_ITEM	item;
			item.SetData((LPCSTR)pComposite->m_sName,0,pComposite);
			m_pID_LIST_DIRECTORY->AddItem(&item);
		}

		m_pCurrentComposite = m_pCurrentSubGroup->itemCompositeTable.GetHeadData();
		if(m_pCurrentComposite)
		{
			m_pID_LISTIMG_DIRECTORY->SetCurSelItem(0);

			RefreshComposite();
		}
	}

	m_pID_SCROLLBAR_DIRECTORY->SetValue(0);
	ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos(m_pID_SCROLLBAR_DIRECTORY, 0);


}

void UIItemCompound::RefreshComposite()
{
	m_pID_LISTIMG_RESULT		->Clear();
	m_pID_LISTIMG_MATERIAL	->Clear();
	//*m_pID_EDIT_NAME			= "";
	//m_itemModel.ReloadMesh(-1);

	_SUPER::RefreshComposite();

	*m_pID_EDIT_AMOUNT			= "1";
	m_pID_SCROLLBAREX_AMOUNT	->SetValue(10);


	if(m_pCurrentComposite == NULL || m_pCurrentResult == NULL)
		return;


	SetMainResult(m_pCurrentResult->m_MainResult);

	*m_pID_EDIT_AMOUNT	= "1";

	//////////////////////////////////
	///处理副结果
	*m_pID_TEXT_SUBRATE	= FMSTR("%d%%", 100 - m_pCurrentResult->m_byMainRate);

}

void UIItemCompound::SetMainResult(CODETYPE itemCode)
{
	/////////////////////////////////////
	///处理主结果信息
	sVITEMINFO_BASE*	pVMainItem;
	sITEMINFO_BASE*	pMainItem;

	pMainItem  = theItemInfoParser.GetItemInfo(itemCode);
	__VERIFY2_PTR(pMainItem, "GetCompositeResultInfo Main Result Is NULL\n", );

	pVMainItem = theItemInfoParser.GetVItemInfoByItem(itemCode);
	__VERIFY2_PTR(pVMainItem, "GetCompositeResultInfo Main Result Is NULL\n", );

	IconDragListImg	mainResult;
	mainResult.SetData(itemCode
							,1
							,pVMainItem->GetIconType()
							);
	m_pID_LISTIMG_RESULT->SetItem(&mainResult,0);


	/// 处理结果模型 
	DWORD dwModelID;
	//switch(pVMainItem->m_dwType)
	{
	//case VITEMTYPE_ARMOUR:	dwModelID = pVMainItem->ARMOUR.m_ArmourModelID;	break;
	//case VITEMTYPE_WEAPON:	dwModelID = pVMainItem->WEAPON.m_RHModelID;	break;
	//default:	
	dwModelID = pVMainItem->m_ModelID;
	//break;
	}
	ReloadMesh(dwModelID);


	///处理结果名称
	*m_pID_EDIT_NAME		= (LPCSTR)pMainItem->m_ItemName;
}


void UIItemCompound::FillNeedMaterials()
{
	_SUPER::FillNeedMaterials();
}

void UIItemCompound::OnItemCompoundFinished(MSG_OBJECT_BASE * pMsgBase)
{
	m_nActionMode	= COMPOSITE_NEXT;

	switch (pMsgBase->m_byProtocol)
	{
		///合成失败...
	case CG_ITEM_ACCESSORY_CREATE_NAK:
		{
			return;
		}
		break;
	}

	//////////////////////////////////////////////

	MSG_CG_ITEM_ACCESSORY_CREATE_ACK * pRecvPacket = (MSG_CG_ITEM_ACCESSORY_CREATE_ACK *)pMsgBase;
	__CHECK2_PTR(pRecvPacket,;);

	INT					nResultPos;
	IconDragListImg*	pResult	= NULL;
	VUCtrlIconDrag*	pListImg = NULL;
	INT					nIndex	= 0;
	SLOTCODE				itemCode	= 0;

	nResultPos		= pRecvPacket->m_byResultPos;

	if(m_pID_LISTIMG_MATERIAL)
		m_pID_LISTIMG_MATERIAL->Clear();


	for(INT nSub=0; nSub < sITEMCOMPOSITERESULT::MAX_COMPOSITE_RESULT_NUM; nSub++)
	{
		IconDragListImg*	subResult;
		subResult = m_pID_LISTIMG_SUBRESULT->GetItemAt(nSub);
		subResult->SetEnable(nResultPos == nSub + sITEMCOMPOSITERESULT::RESULT_SUBBASE);

		if(subResult->IsEnable())
		{	
			pResult	= subResult;
			pListImg	= m_pID_LISTIMG_SUBRESULT;
			nIndex	= nSub;
			itemCode	= m_pCurrentResult->m_ResultItemCode[nSub];
		}
	}

	IconDragListImg*	mainResult;
	mainResult = m_pID_LISTIMG_RESULT->GetItemAt(0);
	mainResult->SetEnable(nResultPos == sITEMCOMPOSITERESULT::RESULT_MAIN);

	if(mainResult->IsEnable())
	{
		pResult	= mainResult;
		pListImg	= m_pID_LISTIMG_RESULT;
		nIndex	= 0;
		itemCode	= m_pCurrentResult->m_MainResult;
	}


	if(pResult)
	{
		sITEMINFO_BASE*	pItem;
		pListImg->ShowToolTip(nIndex, TIP_LOCKEKY);

		pItem = theItemInfoParser.GetItemInfo(itemCode);
		if(pItem)
			OUTPUTTOPF(TEXTRES_ITEM_CREATE, pItem->m_ItemName);

		///拖动物品到鼠标上
		ItemSlotContainer*	pContainer;
		SLOTPOS					atPos;
		//IconDragListImg*		pUnitImg;

		atPos			= pRecvPacket->m_ItemInfo.m_Slot[0].m_ItemPos;
		pContainer	= (ItemSlotContainer*)theItemManager.GetContainer(SI_INVENTORY);
		if(!pContainer->IsEmpty(atPos))
		{
			theUIInventory.ForceDragFrom(atPos);
		}
	}
}

void UIItemCompound::ChangeSucceedRate(UINT nRate,BOOL bForceUpdate)
{
	_SUPER::ChangeSucceedRate(nRate, bForceUpdate);
}


void UIItemCompound::ChangeTargetItem		(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}

void UIItemCompound::ChangeSubResultItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}

void UIItemCompound::ChangeMaterialItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}


// 装载UI
BOOL UIItemCompound::LoadUI()
{
	GetUIItemCompoundItemCompoundUIListener().RegisterMe();

	m_pID_FRAME_ITEMCOMPOUND = theUICtrlManager.LoadFrame( UIDOC_PATH( "ItemCompound") );
	if ( m_pID_FRAME_ITEMCOMPOUND == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "ItemCompound") "]失败");
		return FALSE;
	}

#ifdef USE_ITEMCOMPOUND_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_ITEMCOMPOUND);
#endif
	m_pID_FRAME_ITEMCOMPOUND->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIItemCompound::InitControls()
{
	//INT n;

	m_pID_FRAME_ITEMCOMPOUND->SetProcOnFrameMove			(theUIItemCompoundOnFrameMove);
	m_pID_FRAME_ITEMCOMPOUND->SetProcOnRender		(theUIItemCompoundOnRender, FALSE);

	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_LIST_DIRECTORY				= (VUCtrlList*) 			m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_LIST_DIRECTORY );
	m_pID_SCROLLBAR_DIRECTORY	 	= (VUCtrlScrollBar*)  	m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_SCROLLBAR_DIRECTORY );
	m_pID_COMBOBOX_GROUP				= (VUCtrlComboBox*)		m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_COMBOBOX_GROUP );
	m_pID_COMBOBOX_SUBGROUP			= (VUCtrlComboBox*)		m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_COMBOBOX_SUBGROUP );
	m_pID_LISTIMG_DIRECTORY			= (VUCtrlListImg*) 		m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_LISTIMG_DIRECTORY );
	m_pID_SCROLLBAREX_AMOUNT	  	= (VUCtrlScrollBarEx*)	m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_SCROLLBAREX_AMOUNT );
	m_pID_BUTTON_MAKE					= (VUCtrlButton*)  		m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_BUTTON_MAKE );
	m_pID_TEXT_SUBRATE	  			= (VUCtrlText*) 			m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_TEXT_SUBRATE );
	m_pID_EDIT_AMOUNT					= (VUCtrlEdit*) 			m_pID_FRAME_ITEMCOMPOUND->FindControl( UIITEMCOMPOUND_ID_EDIT_AMOUNT );
	

	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_LIST_DIRECTORY );
	assert( m_pID_SCROLLBAR_DIRECTORY );
	assert( m_pID_COMBOBOX_GROUP );
	assert( m_pID_COMBOBOX_SUBGROUP );
	assert( m_pID_LISTIMG_DIRECTORY );
	assert( m_pID_SCROLLBAREX_AMOUNT );
	assert( m_pID_BUTTON_MAKE );
	
	assert( m_pID_TEXT_SUBRATE );
	assert( m_pID_EDIT_AMOUNT );

	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_LIST_DIRECTORY				->SetProcOnSelectChange	( theUIItemCompoundID_LIST_DIRECTORYOnListSelectChange );
	m_pID_SCROLLBAR_DIRECTORY 		->SetProcOnUpdatePos		( theUIItemCompoundID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos );
	m_pID_COMBOBOX_GROUP				->SetProcOnChange			( theUIItemCompoundID_COMBOBOX_GROUPOnComboBoxChange );
	m_pID_COMBOBOX_SUBGROUP			->SetProcOnChange			( theUIItemCompoundID_COMBOBOX_SUBGROUPOnComboBoxChange );
	m_pID_LISTIMG_DIRECTORY			->SetProcOnDropTo			( theUIItemCompoundID_LISTIMG_DIRECTORYOnIconDropTo );
	m_pID_LISTIMG_DIRECTORY			->SetProcOnLButtonDBClick 		( theUIItemCompoundID_LISTIMG_DIRECTORYOnIconLDBClick );
	m_pID_LISTIMG_DIRECTORY			->SetProcOnRButtonUp		( theUIItemCompoundID_LISTIMG_DIRECTORYOnIconRButtonUp );
	m_pID_SCROLLBAREX_AMOUNT  		->SetProcOnUpdatePos		( theUIItemCompoundID_SCROLLBAREX_AMOUNTOnScrollBarExUpdatePos );
	m_pID_BUTTON_MAKE					->SetProcOnButtonClick 	( theUIItemCompoundID_BUTTON_MAKEOnButtonClick );
	m_pID_EDIT_AMOUNT					->SetProcOnEnter 			( theUIItemCompoundID_EDIT_AMOUNTOnEditEnter );


	/////////////////////////////////////////////
	_SUPER::InitControls(m_pID_FRAME_ITEMCOMPOUND);
	for(INT n=0; n<eSUCCEEDRATE_NUM; n++)
	{
		m_arSucceedRate[n]		->SetProcOnCheck 	( theUIItemCompoundID_CHECKBOX_RATEOnCheckBoxCheck );
	}
	m_pID_BUTTON_CLOSE  				->SetProcOnButtonClick 	( theUIItemCompoundID_BUTTON_CLOSEOnButtonClick );
	m_pID_BUTTON_HELP					->SetProcOnButtonClick 	( theUIItemCompoundID_BUTTON_HELPOnButtonClick );
	m_pID_BUTTON_CANCEL 				->SetProcOnButtonClick 	( theUIItemCompoundID_BUTTON_CANCELOnButtonClick );
	m_pID_LISTIMG_RESULT				->SetProcOnDropTo			( theUIItemCompoundID_LISTIMG_RESULTOnIconDropTo );
	m_pID_LISTIMG_RESULT				->SetProcOnLButtonDBClick 		( theUIItemCompoundID_LISTIMG_RESULTOnIconLDBClick );
	m_pID_LISTIMG_RESULT				->SetProcOnRButtonUp		( theUIItemCompoundID_LISTIMG_RESULTOnIconRButtonUp );
	m_pID_LISTIMG_MATERIAL 			->SetProcOnDropTo			( theUIItemCompoundID_LISTIMG_MATERIALOnIconDropTo );
	m_pID_LISTIMG_MATERIAL 			->SetProcOnLButtonDBClick 		( theUIItemCompoundID_LISTIMG_MATERIALOnIconLDBClick );
	m_pID_LISTIMG_MATERIAL 			->SetProcOnRButtonUp		( theUIItemCompoundID_LISTIMG_MATERIALOnIconRButtonUp );
	m_pID_BUTTON_CLEAR  				->SetProcOnButtonClick 	( theUIItemCompoundID_BUTTON_CLEAROnButtonClick );
	m_pID_EDIT_NAME  					->SetProcOnEnter 			( theUIItemCompoundID_EDIT_NAMEOnEditEnter );
	m_pID_LISTIMG_SUBRESULT			->SetProcOnDropTo			( theUIItemCompoundID_LISTIMG_SUBRESULTOnIconDropTo );
	m_pID_LISTIMG_SUBRESULT			->SetProcOnLButtonDBClick 		( theUIItemCompoundID_LISTIMG_SUBRESULTOnIconLDBClick );
	m_pID_LISTIMG_SUBRESULT			->SetProcOnRButtonUp		( theUIItemCompoundID_LISTIMG_SUBRESULTOnIconRButtonUp );


	/////////////////////////////////////////////

	m_pID_COMBOBOX_GROUP->GetListBox().SetSelectionInfo(TRUE);
	m_pID_COMBOBOX_SUBGROUP->GetListBox().SetSelectionInfo(TRUE);
	m_pID_LIST_DIRECTORY->SetSelectionInfo(TRUE);

	m_pID_LISTIMG_DIRECTORY->SetCanPicking(FALSE);

	m_pID_COMBOBOX_GROUP		->GetListBox().SetMargin(8,8,8,8);
	m_pID_COMBOBOX_SUBGROUP	->GetListBox().SetMargin(8,8,8,8);


	/////////////////////////////////////////////
	m_pID_SCROLLBAR_DIRECTORY->SetValue(0);
	ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos(m_pID_SCROLLBAR_DIRECTORY, 0);

	m_pID_SCROLLBAREX_AMOUNT	->SetValue(10);
	*m_pID_EDIT_AMOUNT			= "1";

	return TRUE;
}



// 卸载UI
BOOL UIItemCompound::UnLoadUI()
{
	m_itemModel.Release();
	GetUIItemCompoundItemCompoundUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ItemCompound") );
	
}

// 是否可视
BOOL UIItemCompound::IsVisible()
{
	if( m_pID_FRAME_ITEMCOMPOUND )
		return m_pID_FRAME_ITEMCOMPOUND->IsVisible();

	return FALSE;
}


void UIItemCompound::SetMoney(MONEY money)
{
	_SUPER::SetMoney(money);
}


void UIItemCompound::OnInstallContainer	(BaseContainer* pContainer)
{
	_SUPER::OnInstallContainer	( pContainer);
}

void UIItemCompound::PushGoods(BaseSlot* pSlot)
{
	_SUPER::PushGoods(pSlot);
}



void UIItemCompound::PopGoods( BaseSlot* pSlot )
{
	_SUPER::PopGoods(pSlot);
}

void UIItemCompound::SetGoodsState( BaseSlot* pSlot,DWORD dwState )
{
	_SUPER::SetGoodsState(  pSlot, dwState);
}



};//namespace gameui


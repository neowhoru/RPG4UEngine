/*////////////////////////////////////////////////////////////////////////
文 件 名：UICreate.cpp
创建日期：2007年11月14日
最后更新：2007年11月14日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UICharCreate.h"
#include "UIMessageBox.h"
#include "CharacterCreator.h"
#include "GlobalInstancePriority.h"
#include "TextFilterManager.h"
#include "CharInfoParser.h"
#include "CharCreateUIListener.h"

#include "VUIObjectManager.h"
#include "ApplicationBase.h"
#include "CharacterScene.h"
#include "ConstTextRes.h"
#include "HeroActionInput.h"
#include "HeroMouseInputLayer.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UICharCreate, ()  , gamemain::eInstPrioClientUI/*UICharCreate*/);

gameui::CharCreateUIListener& GetUISelectCharCreateUIListener()
{
	static gameui::CharCreateUIListener staticCharCreateUIListener;
	return staticCharCreateUIListener;
}

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE  ( theUICharCreate, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUICharCreate, OnRender )

	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_TURN_LOnButtonClick  )
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_TRUN_ROnButtonClick  )
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_HEAD_LOnButtonClick  )
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_CLOTH_ROnButtonClick )
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_HEAD_ROnButtonClick  )
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_CANCELOnButtonClick  )

	UIPROC_ICON_DROP_TO     (theUICharCreate,ID_LISTIMG_HairColorOnIconDropTo );
	UIPROC_ICON_LDB_CLICK   (theUICharCreate,ID_LISTIMG_HairColorOnIconLDBClick )
	UIPROC_EDIT_ENTER			(theUICharCreate,ID_EDIT_NAMEOnEditEnter );
	UIPROC_EDIT_UPDATE		(theUICharCreate,ID_EDIT_NAMEOnEditUpate );

	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_BOYOnCheckBoxCheck      );
	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_GIRLOnCheckBoxCheck );
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_CLOTH_LOnButtonClick );
	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_RobberOnCheckBoxCheck );
	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_WarriorOnCheckBoxCheck );

	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_HAIR_LOnButtonClick );
	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_HAIR_ROnButtonClick );

	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_OKOnButtonClick );

	UIPROC_BUTTON_CLICK     (theUICharCreate, ID_BUTTON_cameraOnButtonClick );

	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_ArcherOnCheckBoxCheck );
	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_TaoistOnCheckBoxCheck );
	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_WizardOnCheckBoxCheck );

	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_MANOnCheckBoxCheck);
	UIPROC_CHECK_BOX_CHECK  (theUICharCreate, ID_CHECKBOX_WOMANOnCheckBoxCheck);
	UIPROC_BUTTON_CLICK     (theUICharCreate, BtnCheckNameOnClick );
	UIPROC_ICON_LDB_CLICK   (theUICharCreate, HairColorOnButtonClick );
};//namespace uicallback
using namespace uicallback;






UICharCreate::UICharCreate()
{
	// Member
	m_pID_FRAME_CREATE	= NULL;
	m_pID_BUTTON_HEAD_L	= NULL;
	m_pID_BUTTON_HEAD_R	= NULL;
	m_pID_EDIT_NAME		= NULL;
	m_pID_CHECKBOX_BOY	= NULL;
	m_pID_CHECKBOX_GIRL	= NULL;
	m_pID_BUTTON_TURN_L	= NULL;
	m_pID_BUTTON_TRUN_R	= NULL;
	m_pID_BUTTON_HAIR_L	= NULL;
	m_pID_BUTTON_HAIR_R	= NULL;
	m_pID_BUTTON_CLOTH_L = NULL;
	m_pID_BUTTON_CLOTH_R = NULL;
	m_pID_BUTTON_OK		= NULL;
	m_pID_BUTTON_CANCEL	= NULL;

	m_pID_BUTTON_camera	= NULL;

	m_pID_LISTIMG_HairColor		= NULL;
	m_pID_TEXT_ProInfo			= NULL;
	m_pID_PICTURE_Property		= NULL;
	m_pID_TEXT_Property			= NULL; 
	m_pID_TEXT_ProName			= NULL;

	m_pID_CHECKBOX_Robber		= NULL;
	m_pID_CHECKBOX_Warrior		= NULL;
	m_pID_CHECKBOX_Archer		= NULL;
	m_pID_CHECKBOX_Taoist		= NULL;
	m_pID_CHECKBOX_Wizard		= NULL;

	m_pID_CHECKBOX_WOMAN			= NULL;
	m_pID_CHECKBOX_MAN			= NULL;
	m_pID_BUTTON_CheckName					= NULL;

}


// Frameu
BOOL UICharCreate::OnFrameMove(DWORD /*dwTick*/)
{
	HeroMouseInputLayer*	pMouseLayer;

	pMouseLayer = theHeroActionInput.GetMouseLayer();
	if(pMouseLayer)
	{
		const	sMOUSE_INFO& info	= theInputLayer.GetMouseBuffer();
		pMouseLayer->ProcessCamera(info);
	}

	//if(info.lZ != 0)
	//{
	//	theCamera.MoveFrontBack( info.lZ /** SCALE_MESH */);
	//	//bUpdate = TRUE;
	//}

	return TRUE;
}

BOOL UICharCreate::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

BOOL UICharCreate::HairColorOnButtonClick( OUT VUCtrlObject* /*pSender*/, OUT IN IconDragListImg* pItem )
{
	int nHairColor = m_pID_LISTIMG_HairColor->GetItemIndex( pItem );
	theCharacterCreator.SetHairColor(nHairColor);

	Refresh( FALSE );
	return TRUE;
}


// Button
BOOL UICharCreate::ID_BUTTON_HEAD_LOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ChangeFace(FALSE);

	Refresh( FALSE );
	return TRUE;
}

// Button

BOOL UICharCreate::ID_BUTTON_HEAD_ROnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ChangeFace(TRUE);
	Refresh( FALSE );
	return TRUE;
}

// Edit
void UICharCreate::ID_EDIT_NAMEOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR 				szData )
{
	theCharacterCreator.UpdateName(szData);
	Refresh( FALSE );
}

void UICharCreate::ID_EDIT_NAMEOnEditUpate( VUCtrlObject* /*pSender*/, LPCSTR 				szData )
{
	theCharacterCreator.UpdateName(szData);
	//Refresh( FALSE );
}


// CheckBox
void UICharCreate::ID_CHECKBOX_BOYOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*bChecked*/ )
{
}
// CheckBox
void UICharCreate::ID_CHECKBOX_GIRLOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*bChecked*/ )
{

}

// Button
BOOL UICharCreate::ID_BUTTON_TURN_LOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.RotatePlayer(-1);
	return FALSE;
}
// Button

BOOL UICharCreate::ID_BUTTON_TRUN_ROnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.RotatePlayer(1);
	return FALSE;
}

// Button
BOOL UICharCreate::ID_BUTTON_HAIR_LOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ChangeHair(FALSE);
	Refresh( FALSE );
	return TRUE;
}

// Button
BOOL UICharCreate::ID_BUTTON_HAIR_ROnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ChangeHair(TRUE);
	Refresh( FALSE );
	return TRUE;
}
// Button
BOOL UICharCreate::ID_BUTTON_CLOTH_LOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ChangeHairColor(FALSE);

	Refresh( FALSE );
	return TRUE;
}

// Button
BOOL UICharCreate::ID_BUTTON_CLOTH_ROnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ChangeHairColor(TRUE);

	Refresh( FALSE );
	return TRUE;
}

// Button
BOOL UICharCreate::ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	StringHandle szName ( m_pID_EDIT_NAME->GetText(), TRUE);

	DWORD dwTextID(TEXTRES_CreateNameError);

	if( GetTextFilterManager().ExistFilterWord(szName) )
		goto laFailed;

	/////////////////////////////////////
	if (szName.IsBlank())
	{
		dwTextID = TEXTRES_NameIsBlank;
		goto laFailed;
	}

	/////////////////////////////////////
	if ( szName.Length() > MAX_NAME_LENGTH )
	{
		dwTextID = TEXTRES_NameTooLong;
		goto laFailed;
	}


	/////////////////////////////////////
	theCharacterCreator.UpdateName(szName);
	Refresh( FALSE );

	theCharacterScene.SendCreateCharacter();

	return TRUE;


laFailed:
	OUTPUTTOP(dwTextID);
	OUTPUTTIP(dwTextID);
	//theUIMessageBox.Show(  dwTextID  );
	return FALSE;
}

// Button

BOOL UICharCreate::ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.Close(FALSE/*TRUE*/);
	return TRUE;
}

BOOL  UICharCreate::OnCheckCharType	(ePLAYER_TYPE charType)
{
	__CHECK_RANGE2(charType,PLAYERTYPE_BASE,PLAYERTYPE_MAX);
	__CHECK_PTR(m_arCharTypes[charType]);

	/////////////////////////////
	INT nIndex;
	nIndex = charType - PLAYERTYPE_BASE;

	for(INT n=0; n<PLAYERTYPE_NUM; n++)
		m_arCharTypes[n]->SetCheck	( n == nIndex );


	/////////////////////////////
	sCHARINFO *pProInfo;

	pProInfo		= theCharInfoParser.GetCharInfo(charType);
	if(pProInfo)
	{
		*m_pID_TEXT_ProInfo			= (LPCSTR)pProInfo->m_Desc;
		*m_pID_TEXT_Property			= (LPCSTR)pProInfo->m_CreateInfo;
		*m_pID_TEXT_ProName			= (LPCSTR)pProInfo->m_NameShow;
	}
	else
	{
		*m_pID_TEXT_ProInfo			= _T("");
		*m_pID_TEXT_Property			= _T("");
		*m_pID_TEXT_ProName			= _T("");
	}

	theCharacterCreator.Clear();
	theCharacterCreator.SetCharType(charType);

	Refresh(TRUE);

	return TRUE;
}


void UICharCreate::ID_CHECKBOX_RobberOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/  )
{
	OnCheckCharType(PLAYERTYPE_PALADIN);

}

void UICharCreate::ID_CHECKBOX_WarriorOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/  )
{
	OnCheckCharType(PLAYERTYPE_WARRIOR);

}

void UICharCreate::ID_CHECKBOX_ArcherOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/  )
{
	OnCheckCharType(PLAYERTYPE_STABBER);

}

void UICharCreate::ID_CHECKBOX_TaoistOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/  )
{
	OnCheckCharType(PLAYERTYPE_NECROMANCER);

}


void UICharCreate::ID_CHECKBOX_WizardOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/  )
{
	OnCheckCharType(PLAYERTYPE_POWWOW);

}



BOOL UICharCreate::ID_LISTIMG_HairColorOnIconDropTo( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/, IconDragListImg* /*pItemDrag*/, IconDragListImg* /*pItemDest*/ )
{
	return TRUE;
}

BOOL UICharCreate::ID_LISTIMG_HairColorOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return TRUE;
}


// 装载UI
BOOL UICharCreate::LoadUI()
{
	GetUISelectCharCreateUIListener().RegisterMe();
	m_pID_FRAME_CREATE = theUICtrlManager.LoadFrame( UIDOC_PATH( "CharCreate") );
	if ( m_pID_FRAME_CREATE == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "CharCreate")"]失败")
			return FALSE;
	}

	return InitControls();
}

// Button
BOOL UICharCreate::ID_BUTTON_cameraOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterCreator.ToggleCamera();
	return TRUE;
}
// 关连控件
BOOL UICharCreate::InitControls()
{
	m_pID_BUTTON_HEAD_L	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_HEAD_L );
	m_pID_BUTTON_HEAD_R	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_HEAD_R );
	m_pID_EDIT_NAME		= (VUCtrlEdit*)		m_pID_FRAME_CREATE->FindControl( ID_EDIT_NAME );
	m_pID_CHECKBOX_BOY	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_BOY );
	m_pID_CHECKBOX_GIRL	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_GIRL );
	m_pID_BUTTON_TURN_L	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_TURN_L );
	m_pID_BUTTON_TRUN_R	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_TRUN_R );
	m_pID_BUTTON_HAIR_L	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_HAIR_L );
	m_pID_BUTTON_HAIR_R	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_HAIR_R );
	m_pID_BUTTON_CLOTH_L = (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_CLOTH_L );
	m_pID_BUTTON_CLOTH_R = (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_CLOTH_R );
	m_pID_BUTTON_OK		= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_OK );
	m_pID_BUTTON_CANCEL	= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_CANCEL );
	m_pID_LISTIMG_HairColor = (VUCtrlListImg*)	m_pID_FRAME_CREATE->FindControl( ID_LISTIMG_HairColor );
	m_pID_PICTURE_Property	= (VUCtrlPicture*)	m_pID_FRAME_CREATE->FindControl( ID_PICTURE_Property );
	m_pID_CHECKBOX_Robber	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_Robber );
	m_pID_CHECKBOX_Warrior	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_Warrior );
	m_pID_CHECKBOX_Archer	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_Archer );
	m_pID_CHECKBOX_Taoist	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_Taoist );
	m_pID_CHECKBOX_Wizard	= (VUCtrlCheckBox*)	m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_Wizard );
	m_pID_TEXT_ProInfo		= (VUCtrlText*)		m_pID_FRAME_CREATE->FindControl( ID_TEXT_ProInfo );
	m_pID_TEXT_Property		= (VUCtrlText*)		m_pID_FRAME_CREATE->FindControl( ID_TEXT_Property );
	m_pID_TEXT_ProName		= (VUCtrlText*)		m_pID_FRAME_CREATE->FindControl( ID_TEXT_ProName );
	m_pID_BUTTON_camera		= (VUCtrlButton*)		m_pID_FRAME_CREATE->FindControl( ID_BUTTON_camera );

	m_pID_CHECKBOX_MAN	= (VUCtrlCheckBox*)		m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_MAN );
	m_pID_CHECKBOX_WOMAN = (VUCtrlCheckBox*)		m_pID_FRAME_CREATE->FindControl( ID_CHECKBOX_WOMAN );
	m_pID_BUTTON_CheckName			= (VUCtrlButton*)			m_pID_FRAME_CREATE->FindControl( ID_BUTTON_CheckName);


	assert(m_pID_BUTTON_CheckName);

	m_pID_CHECKBOX_MAN->SetCheck( TRUE );
	assert(m_pID_CHECKBOX_MAN);
	assert(m_pID_CHECKBOX_WOMAN);

	assert( m_pID_FRAME_CREATE );
	assert( m_pID_BUTTON_TURN_L );
	assert( m_pID_BUTTON_TRUN_R );
	assert( m_pID_BUTTON_CLOTH_L );
	assert( m_pID_BUTTON_CLOTH_R );
	assert( m_pID_BUTTON_OK );
	assert( m_pID_BUTTON_CANCEL );
	assert( m_pID_LISTIMG_HairColor );
	assert( m_pID_PICTURE_Property );
	assert( m_pID_EDIT_NAME );
	assert( m_pID_CHECKBOX_BOY );
	assert( m_pID_CHECKBOX_GIRL );
	assert( m_pID_BUTTON_HAIR_L );
	assert( m_pID_BUTTON_HAIR_R );
	assert( m_pID_BUTTON_HEAD_R );
	assert( m_pID_BUTTON_HEAD_L );
	assert( m_pID_CHECKBOX_Robber );
	assert( m_pID_CHECKBOX_Warrior );
	assert( m_pID_CHECKBOX_Archer );
	assert( m_pID_CHECKBOX_Taoist );
	assert( m_pID_CHECKBOX_Wizard );
	assert( m_pID_TEXT_ProInfo );
	assert( m_pID_TEXT_Property );
	assert( m_pID_TEXT_ProName );
	assert( m_pID_BUTTON_camera );


	m_pID_FRAME_CREATE->SetProcOnFrameMove		( theUICharCreateOnFrameMove );
	m_pID_FRAME_CREATE->SetProcOnRender	( theUICharCreateOnRender, TRUE );

	m_pID_BUTTON_TURN_L	->SetProcOnButtonClick(theUICharCreateID_BUTTON_TURN_LOnButtonClick );
	m_pID_BUTTON_TRUN_R	->SetProcOnButtonClick(theUICharCreateID_BUTTON_TRUN_ROnButtonClick );
	m_pID_BUTTON_HEAD_L	->SetProcOnButtonClick(theUICharCreateID_BUTTON_HEAD_LOnButtonClick );
	m_pID_BUTTON_CLOTH_R ->SetProcOnButtonClick(theUICharCreateID_BUTTON_CLOTH_ROnButtonClick );
	m_pID_BUTTON_HEAD_R	->SetProcOnButtonClick(theUICharCreateID_BUTTON_HEAD_ROnButtonClick );
	m_pID_BUTTON_CANCEL	->SetProcOnButtonClick(theUICharCreateID_BUTTON_CANCELOnButtonClick );

	m_pID_LISTIMG_HairColor	->SetProcOnDropTo				( theUICharCreateID_LISTIMG_HairColorOnIconDropTo );
	m_pID_LISTIMG_HairColor	->SetProcOnLButtonDBClick	( theUICharCreateID_LISTIMG_HairColorOnIconLDBClick );
	m_pID_EDIT_NAME			->SetProcOnEnter				(theUICharCreateID_EDIT_NAMEOnEditEnter );
	m_pID_EDIT_NAME			->SetProcOnUpdate				(theUICharCreateID_EDIT_NAMEOnEditUpate );

	m_pID_BUTTON_CLOTH_L		->SetProcOnButtonClick(theUICharCreateID_BUTTON_CLOTH_LOnButtonClick );
	m_pID_BUTTON_HAIR_L		->SetProcOnButtonClick(theUICharCreateID_BUTTON_HAIR_LOnButtonClick );
	m_pID_BUTTON_HAIR_R		->SetProcOnButtonClick(theUICharCreateID_BUTTON_HAIR_ROnButtonClick );
	m_pID_BUTTON_OK			->SetProcOnButtonClick(theUICharCreateID_BUTTON_OKOnButtonClick );
	m_pID_BUTTON_camera		->SetProcOnButtonClick(theUICharCreateID_BUTTON_cameraOnButtonClick );
	m_pID_BUTTON_CheckName	->SetProcOnButtonClick(theUICharCreateBtnCheckNameOnClick );

	m_pID_CHECKBOX_BOY		->SetProcOnCheck(theUICharCreateID_CHECKBOX_BOYOnCheckBoxCheck );
	m_pID_CHECKBOX_GIRL		->SetProcOnCheck(theUICharCreateID_CHECKBOX_GIRLOnCheckBoxCheck );
	m_pID_CHECKBOX_Robber	->SetProcOnCheck(theUICharCreateID_CHECKBOX_RobberOnCheckBoxCheck );
	m_pID_CHECKBOX_Warrior	->SetProcOnCheck(theUICharCreateID_CHECKBOX_WarriorOnCheckBoxCheck );
	m_pID_CHECKBOX_Archer	->SetProcOnCheck(theUICharCreateID_CHECKBOX_ArcherOnCheckBoxCheck );
	m_pID_CHECKBOX_Taoist	->SetProcOnCheck(theUICharCreateID_CHECKBOX_TaoistOnCheckBoxCheck );
	m_pID_CHECKBOX_Wizard	->SetProcOnCheck(theUICharCreateID_CHECKBOX_WizardOnCheckBoxCheck );

	m_pID_CHECKBOX_MAN		->SetProcOnCheck(theUICharCreateID_CHECKBOX_MANOnCheckBoxCheck);
	m_pID_CHECKBOX_WOMAN		->SetProcOnCheck(theUICharCreateID_CHECKBOX_WOMANOnCheckBoxCheck);

	m_pID_LISTIMG_HairColor	->SetProcOnButtonClick( theUICharCreateHairColorOnButtonClick );

	m_pID_EDIT_NAME->SetMaxLength( MAX_NAME_LENGTH );
	m_pID_FRAME_CREATE->SetWidth( SCREEN_WIDTH );
	m_pID_FRAME_CREATE->SetHeight( SCREEN_HEIGHT );


	m_pID_CHECKBOX_BOY->SetCheck( TRUE );


	//////////////////////////////////////////////
	for(INT n=1; n<PLAYERTYPE_NUM; n++)
	{
		if(m_arCharTypes[n])
			m_arCharTypes[n]->SetVisible(FALSE);
	}

	if(m_pID_CHECKBOX_Robber)
		m_pID_CHECKBOX_Robber->SetVisible(FALSE);


	//////////////////////////////////////////////
	UISCRIPT_ENABLE( UIOBJ_CHARCREATE, m_pID_FRAME_CREATE );
	if(	!m_hIMC )
		m_hIMC = ImmGetContext(g_hWnd);

	ImmReleaseContext( g_hWnd, m_hIMC );
	ImmAssociateContext( g_hWnd, m_hIMC );

	return TRUE;
	
}

// 卸载UI

BOOL UICharCreate::UnLoadUI()
{

	UISCRIPT_DISABLE( UIOBJ_CHARCREATE );
	GetUISelectCharCreateUIListener().UnregisterMe();

	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "CharCreate") );
}

// 是否可视
BOOL UICharCreate::IsVisible()
{
	return m_pID_FRAME_CREATE->IsVisible();
}

// 设置是否可视
void UICharCreate::SetVisible( BOOL bVisible )
{
	m_pID_FRAME_CREATE->SetVisible( bVisible );
}

void UICharCreate::Refresh( BOOL bUseDefault )
{
	theCharacterCreator.Update(bUseDefault);

}


void UICharCreate::ID_CHECKBOX_WOMANOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* pbChecked  )
{
	*pbChecked = TRUE;
	m_pID_CHECKBOX_MAN->SetCheck( FALSE );
	theCharacterCreator.SetSex(SEX_FEMALE);
	//m_nSex = 1;
	Refresh( FALSE );
}

void UICharCreate::ID_CHECKBOX_MANOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* pbChecked  )
{
	*pbChecked = TRUE;
	m_pID_CHECKBOX_WOMAN->SetCheck( FALSE );
	theCharacterCreator.SetSex(SEX_MALE);
	Refresh( FALSE );
}


HIMC UICharCreate::GetImeInstance()
{
	return m_hIMC;
}

void UICharCreate::SetImeInstance(HIMC hIme)
{
	m_hIMC = hIme;
}


BOOL UICharCreate::BtnCheckNameOnClick(VUCtrlObject* /*pSender*/ )
{
	LPCSTR szName = m_pID_EDIT_NAME->GetText();
	if( szName ) 
	{
		theCharacterCreator.CheckCharacterName(szName);
	}
	return TRUE;
}

void UICharCreate::ShowWindow	(BOOL bShow)
{
	SetVisible(bShow);
}




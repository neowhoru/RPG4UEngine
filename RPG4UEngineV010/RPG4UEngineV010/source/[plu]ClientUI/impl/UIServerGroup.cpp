/*////////////////////////////////////////////////////////////////////////
文 件 名：UIServerGroup.cpp
创建日期：2008年3月22日
最后更新：2008年3月22日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UIServerGroup.h"
#include "LoginSceneDefine.h"
#include "UIServerChannel.h"


namespace gameui
{
	const INT SERVERCHANNEL_MARGIN = 3;
	//UIPROC_BUTTON_CLICK( s_UIServerGroup, ID_BUTTON_OKOnButtonClick )
	//UIPROC_BUTTON_CLICK( s_UIServerGroup, ID_BUTTON_EXITOnButtonClick )
	//UIPROC_BUTTON_CLICK( s_UIServerGroup, ID_BUTTON_CANCELOnButtonClick )
	//UIPROC_LIST_SELECT_CHANGE( s_UIServerGroup, ID_LIST_GROUPLISTOnListSelectChange )

	UIServerGroup::UIServerGroup()
	{
		// Member
		m_pID_FRAME_SERVERGROUP = NULL;
		//m_pID_BUTTON_OK = NULL;
		//m_pID_BUTTON_EXIT = NULL;
		//m_pID_BUTTON_CANCEL = NULL;
		m_pID_LIST_GROUPLIST = NULL;
		m_pID_TEXT_TITLE = NULL;
		m_pID_TEXT_CAPTION = NULL;

		m_nGroupIndex	= -1;
		m_nCurSelGroup	= -1;
		m_ptChannelBasePos.x = 0;
		m_ptChannelBasePos.y = 0;

	}
	UIServerGroup::~UIServerGroup()
	{
	}


	// Frame
	BOOL UIServerGroup::OnFrameMove(DWORD /*dwTick*/)
	{
		return TRUE;
	}
	BOOL UIServerGroup::OnRender(DWORD /*dwTick*/)
	{
		return TRUE;
	}
	//// Button
	//BOOL UIServerGroup::ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ )
	//{
	//	return TRUE;
	//}
	//// Button
	//BOOL UIServerGroup::ID_BUTTON_EXITOnButtonClick( VUCtrlObject* /*pSender*/ )
	//{
	//	return TRUE;
	//}
	//// Button
	//BOOL UIServerGroup::ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ )
	//{
	//	return TRUE;
	//}
	// List
	void UIServerGroup::ID_LIST_GROUPLISTOnListSelectChange( VUCtrlObject* pSender, sUILIST_ITEM* pItem )
	{
		//assert(0 && "生成服务线控件... ");
		//#pragma message(__FILE__  "(67) 生成服务线控件... " )
		if(pSender == m_pID_LIST_GROUPLIST)
		{
			const SServerGroupInfo* pInfo = (const SServerGroupInfo*)pItem->m_pData;
			UpdateChannelBasePosition(*pInfo);
			UpdateChannels(pInfo);
		}
	}

	void UIServerGroup::ProcListSelectChange(OUT VUCtrlObject* pSender,OUT IN sUILIST_ITEM* pItem )
	{
		assert(pSender);
		UIServerGroup* pFrame = (UIServerGroup*)pSender->GetUserData();
		if(pFrame)
		{
			pFrame->ID_LIST_GROUPLISTOnListSelectChange( pSender, pItem );
		}
	}


	// 装载UI
	BOOL UIServerGroup::LoadUI(VUCtrlFrame* pParent)
	{
		if(pParent)
			m_pID_FRAME_SERVERGROUP = pParent->LoadChildByFrame( UIDOC_PATH( "ServerGroup"), NULL );
		else
			m_pID_FRAME_SERVERGROUP = theUICtrlManager.LoadFrame( UIDOC_PATH( "ServerGroup") );

		if ( m_pID_FRAME_SERVERGROUP == 0 )
		{
			UIMessageLog("读取文件[ServerGroup.UI]失败")
				return FALSE;
		}

		return InitControls();
	}
	// 关连控件
	BOOL UIServerGroup::InitControls()
	{
		m_pID_LIST_GROUPLIST = (VUCtrlList*)	m_pID_FRAME_SERVERGROUP->FindControl( ID_LIST_GROUPLIST );
		m_pID_TEXT_TITLE		= (VUCtrlText*)	m_pID_FRAME_SERVERGROUP->FindControl( ID_TEXT_TITLE );
		m_pID_TEXT_CAPTION	= (VUCtrlText*)	m_pID_FRAME_SERVERGROUP->FindControl( ID_TEXT_CAPTION );

		assert( m_pID_LIST_GROUPLIST );
		assert( m_pID_TEXT_TITLE );
		assert( m_pID_TEXT_CAPTION );

		if(m_pID_LIST_GROUPLIST)
		{
			m_pID_LIST_GROUPLIST->SetUserData((LPARAM)this);
			m_pID_LIST_GROUPLIST->SetProcOnSelectChange(ProcListSelectChange);
		}

		//UpdateChannelBasePosition();

		return TRUE;
	}
	// 卸载UI
	BOOL UIServerGroup::UnLoadUI()
	{
		UnLoadChannels();

		BOOL bRet;
		bRet = theUICtrlManager.RemoveFrame( UIDOC_PATH( "ServerGroup") );
		m_pID_FRAME_SERVERGROUP = NULL;
		return bRet;
	}
	// 是否可视
	BOOL UIServerGroup::IsVisible()
	{
		return m_pID_FRAME_SERVERGROUP->IsVisible();
	}

	// 设置是否可视
	void UIServerGroup::SetVisible( BOOL bVisible )
	{
		m_pID_FRAME_SERVERGROUP->SetVisible( bVisible );

		if(bVisible)
		{
			if(m_pID_LIST_GROUPLIST)
			{
				sUILIST_ITEM* pItem = m_pID_LIST_GROUPLIST->GetCurSelItem();
				if(pItem)
				{
					const SServerGroupInfo* pInfo = (const SServerGroupInfo*)pItem->m_pData;
					UpdateChannelBasePosition(*pInfo);
					UpdateChannels(pInfo);
				}
			}
		}
		else
		{
			for(UINT n=0; n<m_arChannels.size(); n++)
			{
				if(m_arChannels[n])
					m_arChannels[n]->SetVisible(FALSE);
			}
		}
	}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	void UIServerGroup::SetGroupSelectAt(INT nAt)
	{
		if(!m_pID_LIST_GROUPLIST)
			return;
		m_pID_LIST_GROUPLIST->SelectItemAt(nAt, TRUE);
	}

	void UIServerGroup::ClearGroupInfo()
	{
		m_nCurSelGroup = m_pID_LIST_GROUPLIST->GetCurSelIndex();
		m_pID_LIST_GROUPLIST->Clear();
		UpdateChannels(NULL);
	}

	void UIServerGroup::AddGroupInfo(const SServerGroupInfo& groupInfo)
	{
		if(!m_pID_LIST_GROUPLIST)
			return;
		sUILIST_ITEM sItem;
		sItem.SetData(groupInfo.m_sGroupName,0, (void*)&groupInfo);

		INT nIndex;
		nIndex = m_pID_LIST_GROUPLIST->GetListItemCnt();
		m_pID_LIST_GROUPLIST->AddItem(&sItem);

		if(nIndex == m_nCurSelGroup && m_nCurSelGroup >= 0)
			UpdateChannels(&groupInfo);
	}


	void UIServerGroup::UpdateChannels(const SServerGroupInfo* pGroupInfo)
	{
		UIServerChannel*	pChannel(NULL);
		UINT					nSize(0),
								nChannelSize(0),
								nLoopSize(0),
								n(0);

		nSize				= pGroupInfo?pGroupInfo->m_arChannelInfo.size() : 0;
		nChannelSize	= m_arChannels.size();
		nLoopSize		= min(nSize,nChannelSize);

		if(nSize >= nChannelSize)
		{
			for(n=nChannelSize; n< nSize; n++)
			{
				pChannel = AddChannelInfo(pGroupInfo->m_arChannelInfo[n],n);
				if(pChannel && pGroupInfo)
				{
					if(nChannelSize == 0)
						UpdateChannelBasePosition(*pGroupInfo);

					pChannel->UpdateChannelInfo(&pGroupInfo->m_arChannelInfo[n]);
					pChannel->FitPositionBy(m_ptChannelBasePos.x, m_ptChannelBasePos.y);
					pChannel->SetVisible(TRUE);
				}
			}
		}
		//隐藏不用到的Channel
		else
		{
			for(n=nSize; n<nChannelSize ; n++)
			{
				if(m_arChannels[n])
				{	
					m_arChannels[n]->SetVisible(FALSE);
				}
			}
		}

		for(n=0; n< nLoopSize; n++)
		{
			pChannel = m_arChannels[n];
			if(pChannel && pGroupInfo)
			{
				pChannel->UpdateChannelInfo(&pGroupInfo->m_arChannelInfo[n]);
				pChannel->FitPositionBy(m_ptChannelBasePos.x, m_ptChannelBasePos.y);
				pChannel->SetVisible(TRUE);
			}
		}
	}

	UIServerChannel* UIServerGroup::AddChannelInfo(const SServerChannelInfo& /*groupInfo*/,INT nChannelIndex)
	{
		UIServerChannel*	pChannel = new UIServerChannel;
		if(pChannel->LoadUI(nChannelIndex, m_pID_FRAME_SERVERGROUP))
		{
			//m_pID_FRAME_SERVERGROUP->AppendControl(pChannel->GetFrame());
			pChannel->SetVisible(FALSE);
			m_arChannels.push_back(pChannel);
		}
		else
		{
			delete pChannel;
			pChannel = NULL;
		}
		return pChannel;
	}

	void UIServerGroup::UnLoadChannels()
	{
		UINT nChannelSize = m_arChannels.size();
		UINT n;

			for(n=0; n< nChannelSize; n++)
			{
				if(m_arChannels[n])
				{
					m_arChannels[n]->UnLoadUI();
					delete m_arChannels[n];
					m_arChannels[n] = NULL;
				}
			}
			m_arChannels.clear();
	}

	void UIServerGroup::UpdateChannelBasePosition(const SServerGroupInfo& groupInfo)
	{
		int x,y,w;
		RECT rcSelect;

		w	= m_pID_FRAME_SERVERGROUP->GetWidth();
		m_pID_FRAME_SERVERGROUP->GetPos(x,y,FALSE);
		m_ptChannelBasePos.x = x + w + SERVERCHANNEL_MARGIN;

		if(m_pID_LIST_GROUPLIST->GetItemRect(-1, rcSelect))
		{
			//m_ptChannelBasePos.x = rcSelect.right + SERVERCHANNEL_MARGIN;
			m_ptChannelBasePos.y = rcSelect.top   + SERVERCHANNEL_MARGIN;
			int nSize = groupInfo.m_arChannelInfo.size();
			if(nSize && m_arChannels.size())
			{
				if(nSize *  m_arChannels[0]->GetChannelHeight() < (UINT)SCREEN_HEIGHT)
				{
					return;
				}
			}
		}
		//如果服务线过底，则取主窗口y位置
		m_ptChannelBasePos.y = y + SERVERCHANNEL_MARGIN;
	}



};
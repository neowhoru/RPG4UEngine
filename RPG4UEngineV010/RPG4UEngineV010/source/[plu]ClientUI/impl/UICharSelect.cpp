/*////////////////////////////////////////////////////////////////////////
文 件 名：UISelect.cpp
创建日期：2007年11月17日
最后更新：2007年11月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UICharSelect.h"
#include "UICharCreate.h"
#include "UILogin.h"
#include "UIMessageBox.h"
#include "ConstTextRes.h"

#include "V3DSceneUtil.h"

#include "ApplicationSetting.h"
#include "CharSelectUIListener.h"
#include "UIInputBox.h"
#include "TextResManager.h"
#include "CharacterScene.h"
#include "SceneLogicFlow.h"
#include "CharInfoParser.h"
#include "NetworkSystem.h"


namespace gameui
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UICharSelect, ()  , gamemain::eInstPrioClientUI/*UICharSelect*/);


gameui::CharSelectUIListener& GetUISelectCharSelectUIListener()
{
	static gameui::CharSelectUIListener staticCharSelectUIListener;
	return staticCharSelectUIListener;
}


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE		( theUICharSelect, OnFrameMove )
	UIPROC_FRAME_RENDER			( theUICharSelect, OnRender )
	UIPROC_LIST_SELECT_CHANGE	( theUICharSelect, ID_LIST_PLAYEROnListSelectChange )
	UIPROC_LIST_LDB_CLICK		( theUICharSelect, ID_LIST_PLAYEROnListLDBClick)
	UIPROC_LIST_LDB_CLICK		( theUICharSelect, ID_LIST_PLAYEROnListRBClick)

	UIPROC_BUTTON_CLICK			( theUICharSelect, ID_BUTTON_CREATEOnButtonClick )
	UIPROC_BUTTON_CLICK			( theUICharSelect, ID_BUTTON_EXITOnButtonClick )
	UIPROC_BUTTON_CLICK			( theUICharSelect, ID_BUTTON_ENTEROnButtonClick )
	UIPROC_BUTTON_CLICK			( theUICharSelect, ID_BUTTON_DELETEOnButtonClick )
	UIPROC_BUTTON_CLICK			( theUICharSelect, ID_BUTTON_LEAVEOnButtonClick )

};//namespace uicallback
using namespace uicallback;


UICharSelect::UICharSelect()
{

	m_pID_FRAME_SELECT	= NULL;
	m_pID_BUTTON_CREATE	= NULL;
	m_pID_BUTTON_EXIT		= NULL;
	m_pID_BUTTON_ENTER	= NULL;
	m_pID_BUTTON_DELETE	= NULL;
	m_pID_BUTTON_LEAVE	= NULL;
	m_pID_TEXT_NAME		= NULL;
	m_pID_TEXT_LEVEL		= NULL;
	m_pID_TEXT_Job			= NULL;
	m_pID_TEXT_BIRTHDAY	= NULL;
	m_pID_LIST_PLAYER		= NULL;

}



BOOL UICharSelect::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}

BOOL UICharSelect::OnRender(DWORD /*dwTick*/)
{

	return TRUE;
}


// Button
BOOL UICharSelect::ID_BUTTON_CREATEOnButtonClick( VUCtrlObject* /*pSender*/ )
{

	if(!theCharacterScene.DoCreateCharacter())
	{
		OUTPUTTOP(TEXTRES_CharacterFull);
		OUTPUTTIP(TEXTRES_CharacterFull);
		//theUIMessageBox.Show( TEXTRES_CharacterFull, TEXTRES_UI_ask, MB_OK );
		return FALSE;
	}
	return TRUE;
}

BOOL UICharSelect::ID_BUTTON_EXITOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theApplication.Exit();
	return TRUE;
}


// Button
BOOL UICharSelect::ID_BUTTON_ENTEROnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theCharacterScene.DoEnterWorld();
	return TRUE;

}


BOOL UICharSelect::ProcDeletePlayer( LPCSTR  szInputData, void * /*pData*/ )
{

	if(szInputData == 0 || szInputData[0]== 0 )
	{
		OUTPUTTIP(TEXTRES_PwdNull);
		return FALSE;
	}

	if(!theCharacterScene.DoDeleteCharacter((LPCSTR)szInputData))
	{
		OUTPUTTOP(TEXTRES_SelectCharacter);
		OUTPUTTIP(TEXTRES_SelectCharacter);
		//theUIMessageBox.Show( TEXTRES_SelectCharacter, TEXTRES_UI_ask, MB_OK );
		return FALSE;
	}

	return TRUE;
}




// Button
BOOL UICharSelect::ID_BUTTON_DELETEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
		if(!theCharacterScene.IsHaveSelectCharacter())
		{
			OUTPUTTOP(TEXTRES_SelectCharacter);
			OUTPUTTIP(TEXTRES_SelectCharacter);
			//theUIMessageBox.Show( TEXTRES_SelectCharacter, TEXTRES_UI_ask, MB_OK );
			return FALSE;
		}

	//请输入数量
	theUIInputBox.Show( TEXTRES_LOGIN_DeletePlayerConfirm,
						FALSE,
						TRUE,
						ProcDeletePlayer,
						NULL,
						0,
						TRUE);

		return TRUE;
}


// Button
BOOL UICharSelect::ID_BUTTON_LEAVEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theSceneLogicFlow.GotoLoginScene();
	//theNetworkClient.Disconnect(CK_GAMESERVER);
	return TRUE;
}


// List
void UICharSelect::ID_LIST_PLAYEROnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* pItem )
{
	CharListInfo* pInfo = (CharListInfo*)pItem->m_pData;
	SelectPlayer(pInfo);
}

void UICharSelect::ID_LIST_PLAYEROnListLDBClick( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
{
	ID_BUTTON_ENTEROnButtonClick( NULL );
}

void UICharSelect::ID_LIST_PLAYEROnListRBClick( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/)
{
}


void UICharSelect::SelectPlayer(CharListInfo* pPlayerInfo,BOOL bNotifyScene)
{
	if(theCharacterScene.IsLockPlayer())
		return;

	if(pPlayerInfo)
	{
		*m_pID_TEXT_NAME	= pPlayerInfo->CharInfo.m_szCharName;
		*m_pID_TEXT_LEVEL = FMSTR("%d", pPlayerInfo->CharInfo.m_LV);

		sCHARINFO *pProInfo;
		pProInfo				= theCharInfoParser.GetCharInfo((ePLAYER_TYPE)pPlayerInfo->CharInfo.m_byClass);
		if( pProInfo )
			*m_pID_TEXT_Job = (LPCSTR)pProInfo->m_NameShow;


		*m_pID_TEXT_BIRTHDAY = FMSTR("公元元年元月1日");

		m_pID_LIST_PLAYER->SetCurSelIndex(pPlayerInfo->CharInfo.m_bySlot);
	}
	else
	{
		*m_pID_TEXT_NAME		= "";
		*m_pID_TEXT_LEVEL		= "";
		*m_pID_TEXT_Job		= "";
		*m_pID_TEXT_BIRTHDAY = "";
	}

	if(bNotifyScene)
		theCharacterScene.SelectCharacter(pPlayerInfo);
}


// 装载UI
BOOL UICharSelect::LoadUI()
{
	
	GetUISelectCharSelectUIListener().RegisterMe();

	m_pID_FRAME_SELECT = theUICtrlManager.LoadFrame( UIDOC_PATH( "CharSelect") );
	if(m_pID_FRAME_SELECT  == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "CharSelect") "]失败")
			return FALSE;
	}

	return InitControls();
	
}


// 关连控件
BOOL UICharSelect::InitControls()
{

	m_pID_BUTTON_CREATE	= (VUCtrlButton*)		m_pID_FRAME_SELECT->FindControl( ID_BUTTON_CREATE );
	m_pID_BUTTON_EXIT		= (VUCtrlButton*)		m_pID_FRAME_SELECT->FindControl( ID_BUTTON_EXIT);
	m_pID_BUTTON_ENTER	= (VUCtrlButton*)		m_pID_FRAME_SELECT->FindControl( ID_BUTTON_ENTER );
	m_pID_BUTTON_DELETE	= (VUCtrlButton*)		m_pID_FRAME_SELECT->FindControl( ID_BUTTON_DELETE );
	m_pID_BUTTON_LEAVE	= (VUCtrlButton*)		m_pID_FRAME_SELECT->FindControl( ID_BUTTON_LEAVE );
	m_pID_TEXT_NAME		= (VUCtrlText*)		m_pID_FRAME_SELECT->FindControl( ID_TEXT_NAME );
	m_pID_TEXT_LEVEL		= (VUCtrlText*)		m_pID_FRAME_SELECT->FindControl( ID_TEXT_LEVEL );
	m_pID_TEXT_Job			= (VUCtrlText*)		m_pID_FRAME_SELECT->FindControl( ID_TEXT_JOB);
	m_pID_TEXT_BIRTHDAY	= (VUCtrlText*)		m_pID_FRAME_SELECT->FindControl( ID_TEXT_BIRTHDAY);
	m_pID_LIST_PLAYER		= (VUCtrlList*)		m_pID_FRAME_SELECT->FindControl( ID_LIST_PLAYER );
															
	assert( m_pID_BUTTON_CREATE );
	assert( m_pID_BUTTON_EXIT );
	assert( m_pID_BUTTON_ENTER );
	assert( m_pID_BUTTON_DELETE );
	assert( m_pID_BUTTON_LEAVE );
	assert( m_pID_TEXT_NAME );
	assert( m_pID_TEXT_LEVEL );
	assert( m_pID_TEXT_Job );
	assert( m_pID_TEXT_BIRTHDAY );
	assert( m_pID_LIST_PLAYER );

	m_pID_FRAME_SELECT->SetProcOnFrameMove				( theUICharSelectOnFrameMove );
	m_pID_FRAME_SELECT->SetProcOnRender			( theUICharSelectOnRender, TRUE );

	m_pID_BUTTON_CREATE->SetProcOnButtonClick	(theUICharSelectID_BUTTON_CREATEOnButtonClick );
	m_pID_BUTTON_EXIT->SetProcOnButtonClick	(theUICharSelectID_BUTTON_EXITOnButtonClick );
	m_pID_BUTTON_ENTER->SetProcOnButtonClick	(theUICharSelectID_BUTTON_ENTEROnButtonClick );
	m_pID_BUTTON_DELETE->SetProcOnButtonClick	(theUICharSelectID_BUTTON_DELETEOnButtonClick );
	m_pID_BUTTON_LEAVE->SetProcOnButtonClick	(theUICharSelectID_BUTTON_LEAVEOnButtonClick );

	m_pID_LIST_PLAYER->SetProcOnSelectChange	(theUICharSelectID_LIST_PLAYEROnListSelectChange );
	m_pID_LIST_PLAYER->SetProcOnLButtonDBClick		(theUICharSelectID_LIST_PLAYEROnListLDBClick );
	m_pID_LIST_PLAYER->SetProcOnRButtonDBClick			(theUICharSelectID_LIST_PLAYEROnListRBClick);
	

	m_pID_FRAME_SELECT->SetWidth( SCREEN_WIDTH );
	m_pID_FRAME_SELECT->SetHeight( SCREEN_HEIGHT );


	for(INT n=0; n<MAX_PLAYER_COUNT; n++)
	{
		sUILIST_ITEM stList;
		stList.SetData("[空]");
		m_pID_LIST_PLAYER->AddItem( &stList );
	}

	UISCRIPT_ENABLE ( UIOBJ_CHARSELECT, m_pID_FRAME_SELECT );

	return TRUE;
}


BOOL UICharSelect::UnLoadUI()
{
	UISCRIPT_DISABLE( UIOBJ_CHARSELECT );

	GetUISelectCharSelectUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "CharSelect") );
}


// 是否可视
BOOL UICharSelect::IsVisible()
{
	return m_pID_FRAME_SELECT->IsVisible();
}


// 设置是否可视
void UICharSelect::SetVisible( BOOL bVisible )
{
	m_pID_FRAME_SELECT->SetVisible( bVisible );
}


void UICharSelect::Clear()
{
	m_pID_TEXT_NAME		->Clear();
	m_pID_TEXT_LEVEL		->Clear();
	m_pID_TEXT_Job			->Clear();
	m_pID_TEXT_BIRTHDAY	->Clear();
	//m_pID_LIST_PLAYER		->Clear();
	m_pID_LIST_PLAYER		->SetCurSelIndex(NULL);
}

void UICharSelect::Refresh()
{
}


void UICharSelect::AddPlayer( scene::CharListInfo* pPlayerInfo  )
{
	sUILIST_ITEM item;

	item.SetData(pPlayerInfo->CharInfo.m_szCharName,
					pPlayerInfo->CharInfo.m_bySlot, 
					pPlayerInfo );
	m_pID_LIST_PLAYER->SetItemAt(pPlayerInfo->CharInfo.m_bySlot, &item);
}

void UICharSelect::RemovePlayer( scene::CharListInfo* pPlayerInfo  )
{
	sUILIST_ITEM item;

	item.SetData("[空]");
	m_pID_LIST_PLAYER->SetItemAt(pPlayerInfo->CharInfo.m_bySlot, &item);
	Clear();
}




};//gameui
/*////////////////////////////////////////////////////////////////////////
文 件 名：UISkillItemUnitRender.cpp
创建日期：2008年5月18日
最后更新：2008年5月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "UISkill.h"
#include "UISkillItemUnitRender.h"
#include "SkillInfoParser.h"
#include "TextResManager.h"
#include "VUILayoutManager.h"
#include "GlobalInstancePriority.h"
#include "SkillStorageManager.h"
#include "ItemManager.h"
#include "ConstArray.h"
#include "BaseContainer.h"
#include "SkillSlot.h"
#include "Player.h"
#include "MouseHandler.h"
#include "PacketInclude.h"


const DWORD SKILL_GROUP_ID		= TAG('skil');

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UISkillItemUnitRender, ()  , gamemain::eInstPrioClientUI);



UISkillItemUnitRender::UISkillItemUnitRender()
{
	m_pContainer	= NULL;
	m_pPlayer		= NULL;
	m_nCharType		= PLAYERTYPE_WARRIOR;
	m_nSkillType	= SKILL_KIND_ACTIVE1;
	m_nSkillPage	= SKILL_TYPE_INITIATIVE;
}

UISkillItemUnitRender::~UISkillItemUnitRender()
{
	Release();
}

BOOL 	UISkillItemUnitRender::Init	(  DWORD /*dwNum*/ )
{
	__VERIFY_PTR(m_pContainer, "还没安装到Container中...");

	return TRUE;
}

void UISkillItemUnitRender::Release()
{
	m_arSkillUnits	.clear();

}

BOOL	UISkillItemUnitRender::PrevInstall	()
{
	theSkillStorageManager.SetUISkillContainer(m_nSkillType);
	return TRUE;
}


void	UISkillItemUnitRender::OnInstallTo(BaseContainer* pContainer)
{
	__VERIFY2_PTR(pContainer, "Container不能为NULL...",;);

	m_pContainer = pContainer;


	for(UINT n=0; n<m_arSkillUnits.size(); n++)
	{
		m_arSkillUnits[n].pIcon->SetSlotContainer	(m_pContainer);
		m_arSkillUnits[n].pIcon->SetGroupID			(SKILL_GROUP_ID);
		m_arSkillUnits[n].pIcon->SetGroupContainer(TRUE);
		m_arSkillUnits[n].pIcon->SetIndexAtGroup	(n);
	}

	Update();
}

VOID	UISkillItemUnitRender::FlushSlotListener()
{
}

VUCtrlIconDrag *UISkillItemUnitRender::GetIconDragCtrl(SLOTPOS /*atPos*/)
{
	return NULL;
}

VOID	UISkillItemUnitRender::Update(SLOTPOS pos, UINT /*nUpdateNum*/)
{
	__VERIFY2_PTR(m_pContainer, "Container不能为NULL...",;);
	if(pos != INVALID_POSTYPE)
	{
		if(m_pContainer->IsEmpty(pos))
		{
			//BaseSlot & slotDat = m_pContainer->GetSlot( pos );
			OnSlotRemove	(pos);
		}
		else
		{
		//	BaseSlot & slotDat = m_pContainer->GetSlot( pos );
		//	UIAction.PushEmotion	(slotDat.GetCode()
		//								,slotDat.GetPos()
		//								,slotDat.GetNum()
		//								);
		}
	}
	else
	{
		UpdateSkillPage( );
		//for(INT n=0; n<m_pContainer->GetSlotMaxSize(); n++)
		//{
		//	if(m_pContainer->IsEmpty(n))
		//	{
		//		UIAction.PopEmotion	(n);
		//		continue;
		//	}

		//	BaseSlot & slotDat = m_pContainer->GetSlot( n );
		//	UIAction.PushEmotion	(slotDat.GetCode()
		//								,slotDat.GetPos()
		//								,slotDat.GetNum()
		//								);
		//}
	}
}

VOID	UISkillItemUnitRender::Render()
{
}

void	UISkillItemUnitRender::OnSlotAdd(SERIALTYPE       /*serial*/
                                         ,CODETYPE       /*code*/
													  ,const BaseSlot * /*pSlot*/)
{
}

VOID	UISkillItemUnitRender::OnSlotRemove	(SERIALTYPE /*serial*/ )
{
}

void	UISkillItemUnitRender::OnSlotAdd(const BaseSlot&		slotDat)
{
	Update(slotDat.GetPos());
}

VOID	UISkillItemUnitRender::OnSlotRemove	(const BaseSlot&		slotDat )
{
	//const SkillSlot&	skillSlot = (const SkillSlot&)slotDat;
	SLOTPOS					atPos = slotDat.GetPos();
	VUCtrlListImg*			pIcon;
	INT						nIndex;

	nIndex = FindSkillIndex(atPos);
	__CHECK2(nIndex != -1, ;);

		//IconDragListImg* pCell;


	pIcon	=	m_arSkillUnits[nIndex].pIcon;

	//清除空图标
	if(m_pContainer->IsEmpty(atPos) || slotDat.GetNum() <= 1)
	{
		m_arSkillUnits[nIndex].pBtnUpgrade->SetVisible(FALSE);
		pIcon->RemoveItemAt(  nIndex );
	}

	//更新数量
	else
	{
		//UIAction.PushEmotion	(slotDat.GetCode()
		//							,atPos
		//							,slotDat.GetNum()
		//							);
	}
}


UISkillItemUnitRender::RenderUnit *	
UISkillItemUnitRender::GetItemUnit		(SERIALTYPE /*serial*/ )
{
	return NULL;
}



SLOTPOS UISkillItemUnitRender::FindSkillIndex	(VUCtrlListImg* pImg)
{
	for(SLOTPOS n=0; n<m_arSkillUnits.size(); n++)
	{
		if(m_arSkillUnits[n].pIcon == pImg)
			return n;
	}
	return INVALID_SLOTPOS;
}

SLOTPOS UISkillItemUnitRender::FindSkillIndex	(SLOTPOS atPos)
{
	for(SLOTPOS n=0; n<m_arSkillUnits.size(); n++)
	{
		if(m_arSkillUnits[n].posAtContainer == atPos)
			return n;
	}
	return INVALID_SLOTPOS;
}

SLOTPOS	UISkillItemUnitRender::FindSkillSlotIndex	(CODETYPE skillID)
{
	__VERIFY2_PTR(m_pContainer
					 ,"Container不能为NULL..."
					 ,INVALID_POSTYPE);


	for(SLOTPOS n=0; n<m_pContainer->GetSlotMaxSize(); n++)
	{
		if(m_pContainer->IsEmpty(n))
			continue;

		SkillSlot & slotDat = (SkillSlot &)m_pContainer->GetSlot( n );
		//if(slotDat.GetCode() == skillID)
		if(slotDat.GetSkillClassCode() == skillID)
			return n;
	}

	return INVALID_POSTYPE;
}



BOOL	UISkillItemUnitRender::OnRButtonUP	(VUCtrlObject*    pSender
                                          ,IconDragListImg* pItem)
{
	__CHECK_PTR(pItem);


	__CHECK		(! pItem->IsNull() );

	//if ((pItem->GetIconInfo()->GetCount()) < 10 )

	SLOTPOS nPos = (SLOTPOS)pSender->GetUserData();//FindSkillIndex((VUCtrlListImg*)pItem);

	theSkillStorageManager.SetUISkillContainer(m_nSkillType);
	if(theSkillStorageManager.CheckSkillCanUsing(nPos))
	{
		SLOTPOS	atPos = m_arSkillUnits[nPos].posAtContainer;
		assert(atPos != INVALID_POSTYPE);
		theMouseHandler.DoItemUsing(SI_SKILL, atPos);
	}


//#		pragma message(__FILE__  "(93) 技能升级处理 触发 " )
	//if ( pSkill->ustSkillID == pItem->GetIconInfo()->GetIconID() )
	//{
	//	if( /*GetPlayerRole().m_pBaseInfo->liveinfo.stSkillPoint >= pSkillItem->stNeedPoitToLearskill*/
	//		GetPlayerRole().m_pBaseInfo->TemporaryInfo.dwSkillExp >= pSkillItem->nLearnNeedExp)
	//	{
	//		/*pSkill->stSkillLevel++;*/
	//		GetPlayerRole().m_pSkillBag[n].stSkillLevel++;
	//		/*GetPlayerRole().m_pBaseInfo->liveinfo.stSkillPoint-= pSkillItem->stNeedPoitToLearskill;*/
	//		GetPlayerRole().m_pBaseInfo->TemporaryInfo.dwSkillExp -= pSkillItem->nLearnNeedExp;
	//	}
	//	break;
	//}

	theUISkill.Refresh();

	return TRUE;
}

BOOL	UISkillItemUnitRender::OnLevelUpSkill	(VUCtrlObject*    pSender)
{
	__CHECK_PTR(pSender);

	SLOTPOS nPos = (SLOTPOS)pSender->GetUserData();

	if(nPos == INVALID_SLOTPOS)
		return FALSE;

	SLOTCODE	skillCode;
	theSkillStorageManager.SetUISkillContainer(m_nSkillType);
	skillCode = theSkillStorageManager.GetLevelUpSkillCode(nPos);
	if(skillCode != INVALID_SLOTCODE)
	{
		SkillSlot& slot = (SkillSlot&)m_pContainer->GetSlot(nPos);

		MSG_CG_SKILL_SELECT_SKILLPOINT_SYN	msgLevelUp;
		msgLevelUp.m_dwKey		= theHero.GetObjectKey();
		msgLevelUp.m_SkillCode	= skillCode;
		msgLevelUp.m_bSkill		= slot.IsSKill()?true:false;

		theHero.SendPacket(&msgLevelUp,sizeof(msgLevelUp));
	}


	return TRUE;
}




BOOL	UISkillItemUnitRender::AddSkillInfo	(VUCtrlListImg*	pImg
														,VUCtrlButton*		pBtn
														,SLOTPOS				atPos)
{
	sSKILLPANE_UNIT info;
	info.pBtnUpgrade		= pBtn;
	info.pIcon				= pImg;
	info.posAtContainer	= atPos;

	m_arSkillUnits.push_back(info);
	return TRUE;
}


BOOL UISkillItemUnitRender::UpdateSkillPage( eSKILL_PAGE	skillPage )
{
	if(skillPage == SKILL_TYPE_NULL)
		skillPage = m_nSkillPage;

	__CHECK_RANGE(skillPage, SKILL_TYPE_UIMAX );

	m_nSkillPage = skillPage;


	//////////////////////////////////////////////////////////////////////////
#ifdef USE_DISALE
	sSKILLPANE_PAGE *pSkillPage = m_SkillPaneInfo.GetPanePage(m_nCharType,skillPage);
	__CHECK_PTR(pSkillPage);

	LPCTSTR					szBackgroundPath		= pSkillPage->m_szBackgroundPath;
	SkillPaneIconArray&	arSkillIcons			= pSkillPage->m_arIcons;

	__VERIFY	(arSkillIcons.size() <= m_arSkillUnits.size()
					,"UISkillItemUnitRender::UpdateSkillPage Skill信息数量不能大于图标数量\n"
					);
#endif


	/// 处理图标背景


#ifdef USE_DISALE
	if( m_pBackgroudPic )
	{
		m_pBackgroudPic->SetPicInfo( szBackgroundPath );
		m_pBackgroudPic->SetVisible( TRUE );
	}
#endif

	/// 处理图标升级按钮
	for(UINT i=0; i<m_arSkillUnits.size(); ++i)
	{
		m_arSkillUnits[i].pBtnUpgrade->SetVisible(FALSE);
	}

	
	//////////////////////////////////////////////////
	SLOTPOS						atPos(0);
	sSKILLINFO_COMMON*		pSkill;
	BOOL							bLevelUpEnable;
	SkillStorageInfoC*		pSkillInfo;
	util::Timer*				pTimer;


	//theSkillStorageManager.SetUISkillContainer(m_nSkillType);

	/////////////////////////////////////////////////////////
	//预备技能库信息
	theSkillStorageManager.ResetLevelUpInfo();
#ifdef USE_DISALE
	if(m_nSkillType == SKILL_KIND_ACTIVE1)
	{
		//theSkillStorageManager.SlotUpdate_Active1();
	}
	else
	{
		theSkillStorageManager.ResetLevelUpInfo_Active2();
		//theSkillStorageManager.SlotUpdate_Active1();
	}
	theSkillStorageManager.ResetLevelUpInfo_Passive();
	//theSkillStorageManager.SlotUpdate_Passive();
	//theSkillStorageManager.ResetLevelUpInfo_Style();
	size_t nMax = arSkillIcons.size();
#endif

	/////////////////////////////////////////////////////////
	/// 处理技能图标
	UINT				nMax;
	SLOTPOS			nSlotMax;
	nSlotMax		= m_pContainer?m_pContainer->GetSlotMaxSize() : 0;
	nMax			= (UINT)m_arSkillUnits.size();

	/////////////////////////////////////////////////////////
	for(UINT  i  = 0; i<nMax; i++ )
	{
		IconDragListImg	stItem;
		BOOL					bEnable			= TRUE;
		BOOL 					bShowNextInfo	= TRUE;
		sSKILLPANE_UNIT&	info				= m_arSkillUnits[i];
		sSKILLPANE_ICON&	icon				= info.iconInfo;

		info.pIcon->Clear();

		//////////////////////////////////////////
		//atPos = FindSkillSlotIndex(icon.m_SkillClassID);
		if(i >= nSlotMax)
		{
			//LOGINFO("%d#Skill配置在容器没找到信息\n", i);
			continue;
		}
		atPos = (SLOTPOS)i;

		///////////////////////////////////////////////
		///获得技能槽
		SkillSlot& skillSlot = (SkillSlot&)m_pContainer->GetSlot(atPos);
		
		//pSkill = theSkillInfoParser.GetInfo((SLOTCODE)icon.m_SkillClassID);
		pSkill	= skillSlot.GetInfo();
		if(pSkill == NULL)
		{
			LOGINFO("%d#Skill配置中没找到SkillInfo注册信息\n", i);
			continue;
		}

		///////////////////////////////////////////////
		bShowNextInfo	= TRUE;
		bEnable			= FALSE;
		bLevelUpEnable	= FALSE;

		pSkillInfo = theSkillStorageManager.GetSkillStorageAt(atPos);



		///////////////////////////////////////////////
		if(pSkillInfo)
		{
			bEnable			= (pSkillInfo->state != SLOT_UISTATE_DEACTIVATED);
			bLevelUpEnable	= pSkillInfo->bEnableLevelUp;
			pSkill			= (sSKILLINFO_COMMON*)pSkillInfo->pCurrSkillInfo;	
		}
		else
		{
			pSkill	= skillSlot.GetInfo();	
		}

		////////////////////////////////////////
		pTimer = m_pPlayer->GetCoolTimer(pSkill->m_SkillClassCode);


		stItem.SetTimer(pTimer);
		stItem.SetSlot(&skillSlot,VITEMTYPE_SKILL);

		stItem.SetData	(pSkill->m_SkillCode//icon.m_SkillID
                     ,pSkill->m_wSkillLV
							,VITEMTYPE_SKILL
							,bEnable
							,bEnable
							,0
							,bShowNextInfo
							,pSkill->m_wMaxLV);

		//stItem.SetExtraInfo( szNeedInfo );						
		info.pIcon->SetItem	( &stItem, 0 );
		info.pIcon->SetPos	( icon.x, icon.y, FALSE );


		//info.pIcon->SetLevelInfo( pSkill->m_wSkillLV, pSkill->m_wMaxLV );
		info.pIcon->SetVisible( icon.m_Visible );

		info.pIcon			->SetUserData(atPos);
		info.pBtnUpgrade	->SetUserData(atPos);
		if(bLevelUpEnable)
		{
			info.pBtnUpgrade->SetPos( icon.nBtnX, icon.nBtnY, FALSE );
			//info.pBtnUpgrade->SetPos( pIcon->x - 12, pIcon->y, FALSE );
			info.pBtnUpgrade->SetVisible(TRUE);
		}
	}
	return TRUE;
}

BOOL UISkillItemUnitRender::LoadCharType( ePLAYER_TYPE charType)
{
	__CHECK_RANGE2(charType, PLAYERTYPE_BASE , PLAYERTYPE_MAX);

	m_nCharType	= charType;



	return _LoadSkillPaneData();
}


BOOL	UISkillItemUnitRender::_LoadSkillPaneData()
{
	VUCtrlFrame*				pSkillFrame;
	LPCSTR						szName;
	LPCSTR						szCharType;




	szCharType	= GetCharTypeLabel(m_nCharType);

	szName		= FMSTR	("Skill.Template[%s%d]" UIDATA_FILEEXT
								,szCharType
								,m_nSkillType);

	pSkillFrame = theUICtrlManager.LoadFrame( _PATH_UIDOC(szName ) );
	__CHECK_PTR(pSkillFrame);

	if(!m_pContainer)
	{
		m_pContainer = theSkillStorageManager.GetUISkillContainer(m_nSkillType);
		__CHECK_PTR(m_pContainer);
	}


	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	VUCtrlObject*	pIcon;
	VUCtrlObject*	pBtn;
	VUCtrlObject*	pIconTarget	;
	VUCtrlObject*	pBtnTarget	;
	VUCtrlObject*	pBack;
	LPCSTR			szText;

	UINT				nSize = m_pContainer->GetSlotMaxSize() ;


	//处理背景图片信息
	pBack = pSkillFrame->FindControl( ID_PICTURE_0_0 );
	assert(pBack);
	if(pBack == NULL )
	{
		LOGINFO("Skill %s backpic " ID_PICTURE_0_0 " not existed\n",szName);
		return FALSE;
	}



	//处理图标信息
	for( UINT i  = 0; i<MAX_SKILL_NUM; i++ )
	{

		/////////////////////////////////////
		szText = FMSTR( ID_LISTIMG_SKILL "_%d", i);
		pIcon			= pSkillFrame->FindControl( szText );
		pIconTarget = theUISkill.m_pID_FRAME_SKILL->FindControl( szText );
		if(!pIcon || !pIconTarget)
		{
			LOGINFO	(_T("UISkillPane[%s] %s not found\n")
						,szName
						,szText);
			continue;
		}

		/////////////////////////////////////
		szText = FMSTR( ID_BUTTON_skill "_%d", i);
		pBtn			= pSkillFrame->FindControl( szText );
		pBtnTarget	= theUISkill.m_pID_FRAME_SKILL->FindControl( szText );
		if(!pBtn || !pBtnTarget)
		{
			LOGINFO	(_T("UISkillPane[%s] %s not found\n")
						,szName
						,szText);
			continue;
		}


		////////////////////////////////////
		SLOTCODE			skillClassCode(INVALID_CODETYPE);
		sUIINFO_BASE*	pImgDat;

		if(i < nSize)
		{

			SkillSlot&			skillSot		= (SkillSlot&)m_pContainer->GetSlot((SLOTPOS)i);
			sSKILLPANE_ICON&	iconInfo		= m_arSkillUnits[i].iconInfo;

			iconInfo.m_SkillClassID			= skillSot.GetSkillClassCode();

			pImgDat		= pIcon->GetData();
			if(pImgDat)
			{
				sSKILLINFO_COMMON*	pSkillInfo;
				pSkillInfo = theSkillInfoParser.GetInfo(pImgDat->m_szCaption);
				if(pSkillInfo)
					skillClassCode	= pSkillInfo->m_SkillClassCode;

				if(skillClassCode != INVALID_CODETYPE)
					iconInfo.m_SkillClassID = skillClassCode;
			}


			skillClassCode			= iconInfo.m_SkillClassID;
			iconInfo.m_Visible	= FALSE;

			//////////////////////////////////////////////
			if(	iconInfo.m_SkillClassID != INVALID_CODETYPE
				&&	theSkillInfoParser.GetInfo(iconInfo.m_SkillClassID))
			{
				iconInfo.m_Visible	= TRUE;
				pIcon->	GetPos(iconInfo.x,		iconInfo.y,			FALSE);
				pBtn->	GetPos(iconInfo.nBtnX,	iconInfo.nBtnY,	FALSE);
				pIconTarget->	SetVisible(TRUE);
				pBtnTarget->	SetVisible(TRUE);
			}
		}

		if(skillClassCode == INVALID_CODETYPE)
		{
			pIconTarget->	SetVisible(FALSE);
			pBtnTarget->	SetVisible(FALSE);
		}
	}

	theUICtrlManager.RemoveFrame( pSkillFrame );
	return TRUE;
}



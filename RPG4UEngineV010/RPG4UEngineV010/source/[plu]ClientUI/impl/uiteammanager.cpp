/*////////////////////////////////////////////////////////////////////////
文 件 名：UITeamManager.cpp
创建日期：2007年11月8日
最后更新：2007年11月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UITeamManager.h"
#include "UITeamHero.h"
#include "UITeamPlayer.h"
#include "VObject.h"
#include "UIHeroState.h"
#include "HeroParty.h"

#include "TeamUIListener.h"
#include "VHero.h"
#include "Hero.h"
#include "VObjectManager.h"
#include "VMonster.h"
#include "ObjectUtil.h"
#include "Character.h"
#include "Player.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "ClientUIFunc.h"
#include "GlobalInstancePriority.h"

GLOBALINST_IMPL(UITeamManager, ()  , gamemain::eInstPrioClientUI);

using namespace vobject;
using namespace object;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
static gameui::TeamUIListener& GetUIListener()
{
	static gameui::TeamUIListener staticTeamUIListener;
	return staticTeamUIListener;
}


#define	MAX_STATUS_SPARETIME		999*1000



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UITeamManager::UITeamManager()
{
	m_curSelPlayerID = INVALID_VOBJ_ID;
}

void	UITeamManager::UnLoadUI()
{
	theUITeamHero.UnLoadUI();
	theUITeamPlayer1.UnLoadUI();
	theUITeamPlayer2.UnLoadUI();
	theUITeamPlayer3.UnLoadUI();
	theUITeamPlayer4.UnLoadUI();
	theUITeamPlayer5.UnLoadUI();
	theUITeamPlayer6.UnLoadUI();
	theUITeamPlayer7.UnLoadUI();

	GetUIListener().UnregisterMe();
}

void UITeamManager::LoadUI()
{
	////////////////////////////////////////////////////
	GetUIListener().RegisterMe();
	theUITeamHero.LoadUI();

	////////////////////////////////////////////////////
	m_UIMembers[0]	= &theUITeamPlayer1;
	m_UIMembers[1]	= &theUITeamPlayer2;
	m_UIMembers[2]	= &theUITeamPlayer3;
	m_UIMembers[3]	= &theUITeamPlayer4;
	m_UIMembers[4]	= &theUITeamPlayer5;
	m_UIMembers[5]	= &theUITeamPlayer6;
	m_UIMembers[6]	= &theUITeamPlayer7;
	m_UIMembers[7]	= &theUITeamPlayer8;
	m_UIMembers[8]	= &theUITeamPlayer9;
	m_UIMembers[9]	= &theUITeamPlayer10;

	int	nOffsetY = 0;
	int	nHeight	= 0;
	RECT	rc,rc2;

	for(INT n=0; n<MAX_PARTYMEMBER_NUM; n++)
	{
		m_UIMembers[n]->LoadUI();
		if(m_UIMembers[n] == NULL)
			continue;
		if(n == 0)
		{
			m_UIMembers[n]->m_pID_FRAME_Player->GetGlobalRect( &rc );
			nOffsetY = rc.top;
			//nHeight	= rc.bottom - rc.top;

			m_UIMembers[n]->m_pID_LISTIMG_Status->GetGlobalRect( &rc2 );
			nHeight	= rc2.bottom - rc.top + 3;
		}
		else
		{
			m_UIMembers[n]->m_pID_FRAME_Player->SetPosY( nOffsetY + n * nHeight );
		}
		m_UIMembers[n]->SetVisible( FALSE );
	}

	////////////////////////////////////////////////////

}





void UITeamManager::RenderMiniMap( void )
{
}




void UITeamManager::ResetPartyUI(BOOL bVisible)
{
	for(INT n=0; n<MAX_PARTYMEMBER_NUM; n++)
	{
		m_UIMembers[n]->SetVisible( bVisible );
	}
	
}

void UITeamManager::Refresh()
{
	UpdateHeroInfo		();
	UpdateHeroStatus	();
	UpdateHeroExtra	();
	UpdateTeamInfo		();
}

	
void UITeamManager::UpdateHeroInfo()
{
	if(!singleton::ExistHero())
		return;
	///////////////////////////////////////
	//处理HP信息...
	int			nHp		= theHero.GetHP();
	int			nMp		= theHero.GetMP();
	int			nMaxHp	= math::Max(theHero.GetMaxHP(), (DWORD)1);
	int			nMaxMp	= math::Max(theHero.GetMaxMP(), (DWORD)1);

	*theUITeamHero.m_pID_TEXT_HP		= UIUtil::GetShowText(nHp, nMaxHp);
	*theUITeamHero.m_pID_TEXT_MP		= UIUtil::GetShowText(nMp, nMaxMp);
	*theUITeamHero.m_pID_TEXT_LVL		= FMSTR(_T("%d"), theHero.GetLevel());
	*theUITeamHero.m_pID_TEXT_Name	= theHero.GetName();

	theUITeamHero.m_pID_PROGRESS_Hp->SetValue( (float)nHp / (float)nMaxHp  );
	theUITeamHero.m_pID_PROGRESS_Mp->SetValue( (float)nMp / (float)nMaxMp  );

	theUITeamHero.SetPicHeader(theHeroParty.IsHeroMaster());
}


void UITeamManager::UpdateHeroStatus()
{
	if(!singleton::ExistHero())
		return;

	BOOL bUpdated;
	bUpdated = UIUtil::UpateEffectStates(&theHero
													,theUIHeroState.m_pID_LISTIMG_HEROSTATE)		;
	theUIHeroState.SetVisible(bUpdated);
}



void UITeamManager::UpdateHeroExtra()
{
	int nValue;

	if(!singleton::ExistHero())
		return;

	///////////////////////////////////////////////////////////
	if(! theVHero.HavePet() )
		return;

	//assert(0 && " theVHero.HavePet[]  宠物信息更新待调整 ");
	#pragma message(__FILE__  "(395) theVHero.HavePet[] 宠物信息更新待调整  " )


	VObject *pObject = theVObjectManager.FindByID( theVHero.GetPetID() );
	
	if( pObject  &&pObject->IsMonster() && !pObject->IsDead() )
	{
		VMonster* pPetPlayer = (VMonster*)pObject;

		theUITeamHero.SetPetInfoVisable(TRUE);

		theUITeamHero.m_pID_TEXT_petname->SetText( pPetPlayer->GetName() );

		nValue = 0;
		nValue = pPetPlayer->GetLevel();
		char szLevel[32] = {0};
		_snprintf( szLevel, 32, "%d", nValue );
		theUITeamHero.m_pID_TEXT_petlevel->SetText( szLevel );

		nValue = 0;
		int nHp = pPetPlayer->GetHp();
		int nHpMax = pPetPlayer->GetHpMax();
		if( nHpMax != 0 )
			nValue = nHp*1000/nHpMax;
		theUITeamHero.m_pID_PROGRESS_petHp->SetValue((float) nValue );

		char szCurHP[32] = {0};
		_snprintf( szCurHP, 32, "%d/%d", nHp, nHpMax );
		theUITeamHero.m_pID_TEXT_pethp->SetText(szCurHP);

	}
	else
	{
		theUITeamHero.SetPetInfoVisable(FALSE);
		theVHero.ReleasePet();
	}
}


void UITeamManager::UpdateTeamMember	(UINT          nIndex
													,sPARTY_MEMBER_INFO*	pMemberInfo)
{
	if(!pMemberInfo)
		return;

	if(nIndex >= MAX_PARTYMEMBER_NUM)
		return;

	Player *pPlayer;
	
	pPlayer = (Player*)theObjectManager.GetObject( pMemberInfo->m_dwObjKey );

	if (	!pPlayer 
		|| !pPlayer->IsPlayer() )
		return;


	/////////////////////////////////////////////////
	float				fHPValue(0);
	float				fMPValue(0);
	UITeamPlayer&	teamPlayer = *m_UIMembers[nIndex];


	/////////////////////////////////////////////////
	if(pPlayer->GetMaxHP() != 0)
		fHPValue	=	(float)pPlayer->GetHP() /(float) pPlayer->GetMaxHP();
		//fHPValue	=	(float)pMemberInfo->m_dwHP /(float) pMemberInfo->m_dwHPMax;

	if(pPlayer->GetMaxMP() != 0)
		fMPValue	=	(float)pPlayer->GetMP() /(float) pPlayer->GetMaxMP();
		//fMPValue	=	(float)pMemberInfo->m_dwMP /(float) pMemberInfo->m_dwMPMax;

	//////////////////////////////////////////////
	*teamPlayer.m_pID_TEXT_LVL		= FMSTR(_T("%d"), pMemberInfo->m_Level);
	*teamPlayer.m_pID_TEXT_Name	= (LPCTSTR)pMemberInfo->m_CharName;
	teamPlayer.m_pID_PROGRESS_Hp	->SetValue(fHPValue );
	teamPlayer.m_pID_PROGRESS_Mp	->SetValue(fMPValue );

	teamPlayer.setID			(pMemberInfo->m_dwObjKey);
	teamPlayer.SetPicHeader	(pMemberInfo->m_bMaster );
	teamPlayer.SetKickEnable(theHeroParty.IsHeroMaster());

	teamPlayer.SetVisible( TRUE );

	///////////////////////////////////////////////////

	{
		UIUtil::UpateEffectStates	(pPlayer
											,teamPlayer.m_pID_LISTIMG_Status);
	}
}

void UITeamManager::UpdateTeamInfo()
{
	//隐藏组队界面
	ResetPartyUI(FALSE);

	UINT						nCount;
	sPARTY_MEMBER_INFO*	pMember;
	UINT						nCounter(0);

	nCount	= theHeroParty.GetMemberCount();

	//-------------------------------------------------------------------------
	for ( UINT n=0; n<nCount; n++ )
	{
		pMember	= theHeroParty.GetMemberAt(n);
		if(!pMember)
			continue;

		if(	singleton::ExistHero() 
			&& pMember->m_dwObjKey == theHero.GetObjectKey())
			continue;

		UpdateTeamMember(nCounter,pMember);
		nCounter++;
	}
}



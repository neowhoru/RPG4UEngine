/*////////////////////////////////////////////////////////////////////////
文 件 名：UISpecialSkillProgress.cpp
创建日期：2007年11月5日
最后更新：2007年11月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIActionProgress.h"
#include "LogSystem.h"
#include "ActionProgressUIListener.h"
#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::ActionProgressUIListener& GetUISpecialSkillProgressActionProgressUIListener()
{
	static gameui::ActionProgressUIListener staticActionProgressUIListener;
	return staticActionProgressUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIActionProgress, ()  , gamemain::eInstPrioClientUI/*UIActionProgress*/);


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE     ( theUIActionProgress, OnFrameMove )
		UIPROC_FRAME_RENDER  ( theUIActionProgress, OnRender )
};//namespace uicallback
using namespace uicallback;


const DWORD MAX_ACTION_TIME	= 1400;
const DWORD STEP_ACTION_TIME	= 2;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIActionProgress::UIActionProgress()
{
	// Member
	m_pSpecialSkillFrame = NULL;
	m_pSpecialSkillProcess = NULL;
	m_pHoldTimeProcess = NULL;


	m_StartTimeAtMillage = 0;	// 开始时的千分率
	m_nCurrentTimeAtMillage = 0;		// 当前的千分率

	m_dwStartTime = 0;			// 开始计算的时间
	m_dwStopTime = 0;			// 停止计算的时间
	m_dwPeriod = 0;

	m_dwStartFlashTime = 0;
	m_bStart = FALSE;
	m_bStop = FALSE;
	m_bCountDown = FALSE;
	m_bReadyComplete = FALSE;
}


// Frame
BOOL UIActionProgress::OnFrameMove(DWORD /*dwTick*/)
{

	
	static int nValue = 0;
	if( m_bReadyComplete )
	{
		if( base::GetRunTime() - m_dwStartFlashTime > MAX_ACTION_TIME )
		{
			nValue = (INT)m_pHoldTimeProcess->GetValue();
			nValue -= STEP_ACTION_TIME;

			if( nValue < 0 )
			{
				m_pSpecialSkillFrame->SetVisible( FALSE, TRUE );
				m_pSpecialSkillProcess->SetVisible( FALSE );
				m_pHoldTimeProcess->SetVisible( FALSE );
				m_bReadyComplete = FALSE;
			}
			else
			{
				m_pHoldTimeProcess->SetValue((float) nValue );
			}
		}
	}
	else
	{
		if( !m_bStart )
		{
			return FALSE;
		}

		nValue = (INT)m_pSpecialSkillProcess->GetValue();

		if( !m_bStop )
		{
			nValue += STEP_ACTION_TIME;
		}

		if( m_bStop )
		{
			m_pSpecialSkillProcess->SetValue((float)( m_nCurrentTimeAtMillage - (base::GetRunTime()-m_dwStopTime)*1000/m_dwPeriod ));
			if( m_pSpecialSkillProcess->GetValue() <= 0 )
			{
				m_bStart = FALSE;
				m_pSpecialSkillFrame->SetVisible( FALSE, TRUE );
				m_pSpecialSkillProcess->SetVisible( FALSE, TRUE );
			}
		}
		else
		{
			m_nCurrentTimeAtMillage = m_StartTimeAtMillage + (base::GetRunTime()-m_dwStartTime)*1000/m_dwPeriod;
			m_pSpecialSkillProcess->SetValue( (float)m_nCurrentTimeAtMillage );
		}
	}
	return TRUE;
	
}

BOOL UIActionProgress::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

void UIActionProgress::Cancel( DWORD dwStopTime )
{
	m_bStop = TRUE;
	m_dwStopTime = dwStopTime;
}

void UIActionProgress::CountDown()
{
	m_bCountDown = TRUE;
	m_bReadyComplete = TRUE;
	m_dwStartFlashTime = base::GetRunTime();
	m_pSpecialSkillFrame->SetVisible( TRUE );
	m_pHoldTimeProcess->SetVisible( TRUE );
	m_pHoldTimeProcess->SetValue( 1000 );
	m_pSpecialSkillProcess->SetVisible( FALSE );
	m_pSpecialSkillProcess->SetValue( 0 );

	m_pSpecialSkillFrame->StartFlash( 1, FALSE, FALSE );
	m_pSpecialSkillProcess->StartFlash( 1, FALSE, FALSE );
	m_pHoldTimeProcess->StartFlash( 1, FALSE, FALSE );
}

// 装载UI
BOOL UIActionProgress::LoadUI()
{
	
	GetUISpecialSkillProgressActionProgressUIListener().RegisterMe();


	m_pSpecialSkillFrame = theUICtrlManager.LoadFrame( UIDOC_PATH( "ActionProgress") );
	if ( m_pSpecialSkillFrame == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "ActionProgress")"]失败")
			return FALSE;
	}

	return InitControls();
	
}

// 关连控件
BOOL UIActionProgress::InitControls()
{
	m_pSpecialSkillFrame->SetProcOnFrameMove	(theUIActionProgressOnFrameMove);
	m_pSpecialSkillFrame->SetProcOnRender		(theUIActionProgressOnRender);
	
	m_pSpecialSkillProcess		= (VUCtrlProgress*)m_pSpecialSkillFrame->FindControl(  ID_SPECIALSKILLPROGRESS );
	m_pHoldTimeProcess			= (VUCtrlProgress*)m_pSpecialSkillFrame->FindControl(  ID_SPECIALSKILLHOLDTIMEPROGRESS );

	assert( m_pSpecialSkillProcess );
	assert( m_pHoldTimeProcess );

	GetUISpecialSkillProgressActionProgressUIListener().UnregisterMe();
	return TRUE;
	
}

// 卸载UI
BOOL UIActionProgress::UnLoadUI()
{
	
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ActionProgress") );
	
}

// 是否可视
BOOL UIActionProgress::IsVisible()
{
	
	return m_pSpecialSkillFrame->IsVisible();
	
}

// 设置是否可视
void UIActionProgress::SetVisible( BOOL bVisible )
{
	
	if( m_pSpecialSkillFrame )
		m_pSpecialSkillFrame->SetVisible( bVisible );
	if( m_pSpecialSkillProcess )
		m_pSpecialSkillProcess->SetVisible( bVisible );
	if( m_pHoldTimeProcess )
		m_pHoldTimeProcess->SetVisible( bVisible );
	
}

void UIActionProgress::StartReadytoLaunch	(DWORD dwStartTime
                                          ,DWORD dwPeriod
														,short nStartTime)
{
	
	m_bStart = TRUE;
	m_bStop = FALSE;
	m_bReadyComplete = FALSE;

	m_dwStartTime = dwStartTime;
	m_dwPeriod = dwPeriod;
	m_StartTimeAtMillage = nStartTime;

	m_pSpecialSkillProcess->SetValue( 0 );
	m_pSpecialSkillFrame->SetVisible( TRUE );
	m_pSpecialSkillProcess->SetVisible( TRUE );
	m_pHoldTimeProcess->SetVisible( FALSE );
	
}

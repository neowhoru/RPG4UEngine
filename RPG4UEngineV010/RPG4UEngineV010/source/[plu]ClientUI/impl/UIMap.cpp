/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMap.cpp
创建日期：2007年9月26日
最后更新：2007年9月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIMap.h"
#include "VUIFontDDraw.h"
#include "GlobalTexture.h"
#include "IV3DEngine.h"
#include "VUIObjectManager.h"
#include "MapUIListener.h"
#include "RenderUtil.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::MapUIListener& GetUIMapMapUIListenerUIListener()
{
	static gameui::MapUIListener staticMapUIListener;
	return staticMapUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIMap, ()  , gamemain::eInstPrioClientUI/*UIMap*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE  ( theUIMap, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIMap, OnRender )
	UIPROC_BUTTON_CLICK     (theUIMap,  ID_BUTTON_CLOSEOnButtonClick);
};//namespace uicallback
using namespace uicallback;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIMap::UIMap()
{
	// Member
	m_pID_FRAME_Map = NULL;
	m_pID_BUTTON_CLOSE = NULL;
	m_pID_TEXT_CityName = NULL;
	m_pID_TEXT_MayorName = NULL;

}


 // Frame
BOOL UIMap::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIMap::OnRender(DWORD /*dwTick*/)
{
	
	*m_pID_TEXT_CityName = "";
	*m_pID_TEXT_MayorName = "";
//	*m_pID_TEXT_Land = "";
	RECT rcFrame;
	RECT rcCity;
	m_pID_FRAME_Map->GetGlobalRect( &rcFrame );
	for ( UINT n=0; n<m_CityInfos.size(); n++ )
	{
		rcCity.left = rcFrame.left + m_CityInfos[n].m_ptPos.x-10;
		rcCity.top = rcFrame.top + m_CityInfos[n].m_ptPos.y-10;
		rcCity.right = rcCity.left + 20;
		rcCity.bottom = rcCity.top + 20;
		//
		uidraw::DrawRect2D( rcCity, 0x7fff0000 );
		//
		std::string strCountry;
		std::string strCityName = "";
		DWORD colorCity = 0;


		if ( m_CityInfos[n].m_strCityName.size() > 0 )
		{
			strCityName = string("守卫:") + m_CityInfos[n].m_strCityName;
			RenderUtil::RenderPic( theGlobalTexture.GetTexture( GLOBALTEX_BANNER ),
						rcCity,
						colorCity );
		}

		if ( PtInRect( &rcCity, theUICtrlManager.m_MousePos ) == TRUE )
		{
			//CCitySlk::SCity *pstCity = theCitySlk.FindCityById( m_CityInfos[n].m_nCityID );
			//if ( !pstCity )
			//{
			//	assert(FALSE);
			//	return FALSE;
			//}
			//*m_pID_TEXT_CityName = strCountry+pstCity->szCityName;
			//*m_pID_TEXT_MayorName = strCityName;
			//uidraw::DrawTextARGBEx( rcCity.left, rcCity.top-12,
			//							pstCity->szCityName,
			//							0xffffffff, 0, 0, 0 );
		}
	}
	return TRUE;
	
}

// Button
BOOL UIMap::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	return FALSE;
	
}

// 装载UI
BOOL UIMap::LoadUI()
{
	
	GetUIMapMapUIListenerUIListener().RegisterMe();

	m_pID_FRAME_Map = theUICtrlManager.LoadFrame( UIDOC_PATH( "Map") );
	if ( m_pID_FRAME_Map == 0 )
	{
		UIMessageLog("读取文件[UI\\Map.UI]失败")
		return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UIMap::InitControls()
{
	m_pID_BUTTON_CLOSE = (VUCtrlButton*)m_pID_FRAME_Map->FindControl(  ID_BUTTON_CLOSE );
	m_pID_TEXT_CityName = (VUCtrlText*)m_pID_FRAME_Map->FindControl(  ID_TEXT_CityName );
	m_pID_TEXT_MayorName = (VUCtrlText*)m_pID_FRAME_Map->FindControl(  ID_TEXT_MayorName );

	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_TEXT_CityName );
	assert( m_pID_TEXT_MayorName );
	
	m_pID_FRAME_Map->SetProcOnFrameMove	(  theUIMapOnFrameMove );
	m_pID_FRAME_Map->SetProcOnRender		(  theUIMapOnRender );
	m_pID_BUTTON_CLOSE->SetProcOnButtonClick( theUIMapID_BUTTON_CLOSEOnButtonClick );


	UISCRIPT_ENABLE( UIOBJ_MAP, m_pID_FRAME_Map );
	return TRUE;
	
}
// 卸载UI
BOOL UIMap::UnLoadUI()
{
	
	UISCRIPT_DISABLE( UIOBJ_MAP );
	GetUIMapMapUIListenerUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Map") );
	
}
// 是否可视
BOOL UIMap::IsVisible()
{
	
	return m_pID_FRAME_Map->IsVisible();
	
}
// 设置是否可视
void UIMap::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_Map->SetVisible( bVisible );
	//
	if ( bVisible == TRUE )
	{
		static BOOL s_bQueryCityInfo = FALSE;
		if ( s_bQueryCityInfo == FALSE )
		{
			s_bQueryCityInfo = TRUE;
			// 请求信息
		}
	}
	
}



/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMsg.cpp
创建日期：2007年11月16日
最后更新：2007年11月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIWaiting.h"
#include "UICharSelect.h"
#include "WaitingUIListener.h"
#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::WaitingUIListener& GetUIMsgWaitingUIListenerUIListener()
{
	static gameui::WaitingUIListener staticWaitingUIListener;
	return staticWaitingUIListener;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIWaiting, ()  , gamemain::eInstPrioClientUI/*UIWaiting*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIWaiting, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIWaiting, OnRender )
	UIPROC_BUTTON_CLICK( theUIWaiting, ID_BUTTON_YESOnButtonClick )
	UIPROC_EDIT_ENTER( theUIWaiting, ID_EDIT_INFOOnEditEnter )
};//namespace uicallback
using namespace uicallback;

UIWaiting::UIWaiting()
{
	// Member
	m_pID_FRAME_MESSAGE = NULL;
	m_pID_TEXT_CAPTION = NULL;
	m_pID_BUTTON_YES = NULL;
}

// Frame
BOOL UIWaiting::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}
BOOL UIWaiting::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

// Button
BOOL UIWaiting::ID_BUTTON_YESOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	theUICharSelect.ID_BUTTON_LEAVEOnButtonClick(NULL);
	SetVisible(FALSE);
	return TRUE;
	
}


// Edit
void UIWaiting::ID_EDIT_INFOOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ )
{
}

// 装载UI
BOOL UIWaiting::LoadUI()
{
	
	GetUIMsgWaitingUIListenerUIListener().RegisterMe();

	m_pID_FRAME_MESSAGE = theUICtrlManager.LoadFrame( UIDOC_PATH( "SelectRoleMsg") );
	if ( m_pID_FRAME_MESSAGE == 0 )
	{
		UIMessageLog("读取文件[SelectRoleMsg]失败")
			return FALSE;
	}

	return InitControls();
	
}

// 关连控件
BOOL UIWaiting::InitControls()
{
	m_pID_TEXT_CAPTION = (VUCtrlText*)m_pID_FRAME_MESSAGE->FindControl(  ID_TEXT_CAPTION );
	m_pID_BUTTON_YES = (VUCtrlButton*)m_pID_FRAME_MESSAGE->FindControl(  ID_BUTTON_YES );

	assert( m_pID_TEXT_CAPTION );
	assert( m_pID_BUTTON_YES );

	
	m_pID_FRAME_MESSAGE->SetProcOnFrameMove(  theUIWaitingOnFrameMove );
	m_pID_FRAME_MESSAGE->SetProcOnRender	(  theUIWaitingOnRender );
	m_pID_BUTTON_YES->SetProcOnButtonClick	( theUIWaitingID_BUTTON_YESOnButtonClick );

	return TRUE;
	
}
// 卸载UI
BOOL UIWaiting::UnLoadUI()
{
	
	GetUIMsgWaitingUIListenerUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Waiting") );
	
}
// 是否可视
BOOL UIWaiting::IsVisible()
{
	
	return m_pID_FRAME_MESSAGE->IsVisible();
	
}
// 设置是否可视
void UIWaiting::SetVisible( BOOL bVisible )
{
	
	if( m_pID_FRAME_MESSAGE )
		m_pID_FRAME_MESSAGE->SetVisible( bVisible );
	
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UITarget.cpp
创建日期：2007年11月25日
最后更新：2007年11月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VObject.h"
#include "VCharacter.h"
#include "VHero.h"
#include "VObjectManager.h"
#include "UITarget.h"
#include "VUCtrlManager.h"
#include "TextResManager.h"

#include "UIChatInput.h"
#include "UIMenuPlayer.h"
#include "UIExitGame.h"
#include "UImain.h"
#include "UITeamPlayer.h"
#include "TargetUIListener.h"
#include "VHeroActionInput.h"
#include "VMonster.h"
#include "Monster.h"
#include "ObjectManager.h"
#include "ConstTextRes.h"
#include "GlobalInstancePriority.h"
#include "ClientUIFunc.h"
#include "UITeamPlayer.h"

using namespace vobject;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::TargetUIListener& GetUITargetTargetUIListener()
{
	static gameui::TargetUIListener staticTargetUIListener;
	return staticTargetUIListener;
}

const float HP_TAIL_SPEED			= 0.20f;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UITarget, ()  , gamemain::eInstPrioClientUI/*UITarget*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUITarget, OnFrameMove )
	UIPROC_FRAME_RENDER( theUITarget, OnRender )
	UIPROC_ICON_DROP_TO( theUITarget, ID_LISTIMG_StatusOnIconDropTo )
	UIPROC_ICON_LDB_CLICK( theUITarget, ID_LISTIMG_StatusOnIconLDBClick )
};//namespace uicallback
using namespace uicallback;

UITarget::UITarget()
:VUIControl(gameui::eUITarget)
{
	// Member
	m_pID_FRAME_Target		= NULL;
	m_pID_TEXT_Name			= NULL;
	m_pID_PROGRESS_Hp			= NULL;
	m_pID_PROGRESS_HpTail	= NULL;
	m_pID_LISTIMG_Status		= NULL;
	m_pID_TEXT_GROUP			= NULL;

	m_pID_TEXT_Lvl				= NULL;
	m_targetID					= VOBJID_NULL;

	m_bUpdating					= FALSE;
}

UITarget::~UITarget()
{
}

// Frame
BOOL UITarget::OnFrameMove(DWORD dwTick)
{
	Refresh(dwTick);
	return TRUE;
}


void UITarget::Refresh(DWORD dwTick)
{
	UITeamPlayer::UpdateMemberSelect();

	if(!IsVisible())
		return;

	if (	m_targetID != VOBJID_NULL)
	{
		Object*	pObject = theObjectManager.GetObject( m_targetID );

		if (	pObject 
			&& pObject->IsKindOfObject(CHARACTER_OBJECT))
		{
			float	fHPCurrent	= 0;
			float	fHPLast		= 0;
			float fProgress	= 1.0;

			Character* pChar = (Character*)pObject;

			if(	pChar->IsKindOfObject(MONSTER_OBJECT) 
				||	pChar->IsKindOfObject(PLAYER_OBJECT) )
			{
				fProgress = (float)pChar->GetHP() / (float)pChar->GetMaxHP();
			}

			fHPLast	= m_pID_PROGRESS_HpTail->GetValue();
			if(fHPLast != fProgress)
			{
				m_bUpdating = TRUE;
				m_pID_PROGRESS_Hp->SetValue( fProgress );

				fHPCurrent	= m_pID_PROGRESS_Hp->GetValue();
				fHPLast		= m_pID_PROGRESS_HpTail->GetValue();

				if( fHPLast > fHPCurrent )
				{
					float fStep = 0;
					fStep = (HP_TAIL_SPEED * (float)dwTick/1000.f);
					if( fStep < 0.f )
						fStep = 0.f;
					fHPLast -= fStep;
				}
				else
				{
					fHPLast = fHPCurrent;
				}

				m_pID_PROGRESS_HpTail->SetValue( fHPLast );
			}
			else
			{
				m_bUpdating = FALSE;
			}

			UIUtil::UpateEffectStates(pChar, m_pID_LISTIMG_Status);
		}//if (pObject && pObject->IsKindOfObject(CHARACTER_OBJECT))

	}//if ( m_targetID != INVALID_VOBJ_ID )

}


BOOL UITarget::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

// ListImg / ListEx
BOOL UITarget::ID_LISTIMG_StatusOnIconDropTo( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
														IconDragListImg* /*pItemDrag*/,
														IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
}

BOOL UITarget::ID_LISTIMG_StatusOnIconDragOff( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
														 IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UITarget::ID_LISTIMG_StatusOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}


// 装载UI
BOOL UITarget::LoadUI()
{
	GetUITargetTargetUIListener().RegisterMe();

	m_pID_FRAME_Target = theUICtrlManager.LoadFrame( UIDOC_PATH( "Target") );
	if ( m_pID_FRAME_Target == 0 )
	{
		UIMessageLog("读取文件[UITarget.UI]失败")
			return FALSE;
	}


	return InitControls();
}
// 关连控件
BOOL UITarget::InitControls()
{
	m_pID_TEXT_Name			= (VUCtrlText*)m_pID_FRAME_Target->FindControl		(  ID_TEXT_Name );
	m_pID_PROGRESS_Hp			= (VUCtrlProgress*)m_pID_FRAME_Target->FindControl	(  ID_PROGRESS_Hp );
	m_pID_PROGRESS_HpTail	= (VUCtrlProgress*)m_pID_FRAME_Target->FindControl	(  ID_PROGRESS_HpTail );
	m_pID_LISTIMG_Status		= (VUCtrlListImg*)m_pID_FRAME_Target->FindControl	(  ID_LISTIMG_Status );
	m_pID_TEXT_GROUP			= (VUCtrlText*)m_pID_FRAME_Target->FindControl		(  ID_TEXT_GROUP );
	m_pID_TEXT_Lvl				= (VUCtrlText*)m_pID_FRAME_Target->FindControl(  ID_TEXT_Lvl );
	m_pID_Text_TargetName	= (VUCtrlText*)m_pID_FRAME_Target->FindControl(  ID_TEXT_TargetName );

	assert(m_pID_Text_TargetName);
	assert( m_pID_TEXT_Name );
	assert( m_pID_PROGRESS_Hp );
	assert( m_pID_PROGRESS_HpTail );
	assert( m_pID_LISTIMG_Status );
	assert( m_pID_TEXT_GROUP );
	
	m_pID_FRAME_Target->SetProcOnFrameMove(  theUITargetOnFrameMove );
	m_pID_FRAME_Target->SetProcOnRender(  theUITargetOnRender );

	m_pID_LISTIMG_Status->SetProcOnDropTo			(theUITargetID_LISTIMG_StatusOnIconDropTo );
	m_pID_LISTIMG_Status->SetProcOnLButtonDBClick(theUITargetID_LISTIMG_StatusOnIconLDBClick );
	m_pID_FRAME_Target->SetProcOnButtonDown( ID_FRAME_Target_OnClick );


	SetVisible( FALSE );



	UISCRIPT_ENABLE ( UIOBJ_TARGET, m_pID_FRAME_Target );
	return TRUE;
	
}
// 卸载UI
BOOL UITarget::UnLoadUI()
{
	GetUITargetTargetUIListener().UnregisterMe();
	UISCRIPT_DISABLE( UIOBJ_TARGET );
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Target") );
}

// 是否可视
BOOL UITarget::IsVisible()
{
	
	return m_pID_FRAME_Target->IsVisible();
	
}


// 设置是否可视
void UITarget::SetVisible( BOOL bVisible )
{
	VUIControl::SetVisible( bVisible );
	if ( bVisible != m_pID_FRAME_Target->IsVisible() )
	{
		m_pID_FRAME_Target->SetVisibleWhole( bVisible, TRUE );
	}
	if( !bVisible )
	{
		theUITeamPlayer1.m_pPicSelector->SetVisible(FALSE);
		theUITeamPlayer2.m_pPicSelector->SetVisible(FALSE);
		theUITeamPlayer3.m_pPicSelector->SetVisible(FALSE);
		theUITeamPlayer4.m_pPicSelector->SetVisible(FALSE);
	}
	else
	{
		if( theUITeamPlayer1.m_nPlayerID != (INT)m_targetID )
			theUITeamPlayer1.m_pPicSelector->SetVisible(FALSE);
		if( theUITeamPlayer2.m_nPlayerID != (INT)m_targetID )
			theUITeamPlayer2.m_pPicSelector->SetVisible(FALSE);
		if( theUITeamPlayer3.m_nPlayerID != (INT)m_targetID )
			theUITeamPlayer3.m_pPicSelector->SetVisible(FALSE);
		if( theUITeamPlayer4.m_nPlayerID != (INT)m_targetID )
			theUITeamPlayer4.m_pPicSelector->SetVisible(FALSE);
	}
}

void UITarget::SetPlayer( VOBJID nID )
{
	if(m_targetID == nID)
		return;

	m_targetID = nID;
	Object*	pObject = theObjectManager.GetObject( m_targetID );
	if ( pObject  && pObject->IsKindOfObject(CHARACTER_OBJECT))
	{
		Character* pChar = (Character*)pObject;

		if(pChar->IsKindOfObject(MONSTER_OBJECT))
		{
			Monster* pMonster = (Monster*)pChar;
			sNPCINFO_BASE*	pInfo		= pMonster->GetMonsterInfo();

			//if(pMonster)
			//{
			//	if(pMonster->nIsInitiative == 1)
			//	{
			//		m_pID_TEXT_Name->SetText(pObject->GetName(), 0xFFFF7979);
			//	}
			//	else
			//		m_pID_TEXT_Name->SetText(pObject->GetName(), 0xFFFFFFFF);
			//}

			m_pID_TEXT_Name->SetText(pChar->GetName(), 0xFFFFFFFF);
			*m_pID_TEXT_GROUP = theTextResManager.GetString(TEXTRES_NPC_GENERAL + pInfo->m_byGrade-1);
		}
		else
		{
			*m_pID_TEXT_GROUP = "";
			m_pID_TEXT_Name->SetText(pObject->GetName(), 0xFFFFFFFF);
		}
		

		SetVisible(TRUE);
		//
		*m_pID_TEXT_Lvl = FMSTR("%d",pChar->GetLevel());

	}
	else
	{
		m_targetID = VOBJID_NULL;
		SetVisible(FALSE);
	}

	Refresh(g_FrameCostTime);
}

BOOL UITarget::ID_FRAME_Target_OnClick( VUCtrlObject* pSender )
{
	Object* pObject = theObjectManager.GetObject( theUITarget.GetPlayerID() );

	if(pObject)
	{
		if( pObject->IsKindOfObject(PLAYER_OBJECT))
		{
			if( !theUIMenuPlayer.IsVisible() )
			{
				RECT rc;
				pSender->GetGlobalRect(&rc);
				theUIMenuPlayer.SetPos( rc.right, rc.bottom);
				theUIMenuPlayer.SetPrivateName(pObject->GetName());
				//theUIMenuPlayer.SetPlayerID( pObject->GetID() );
				theUIMenuPlayer.SetVisible( TRUE );
			}
			else
				theUIMenuPlayer.SetVisible( FALSE );
		}
	}

	return TRUE;
}

void UITarget::setTargetName( LPCSTR  szName)
{
	if( szName == NULL)
		m_pID_Text_TargetName->SetText("");
	else
		m_pID_Text_TargetName->SetText(szName);
}
/*////////////////////////////////////////////////////////////////////////
文 件 名：UIProgress.cpp
创建日期：2007年12月23日
最后更新：2007年12月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIProgress.h"
#include "ProgressUIListener.h"
#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::ProgressUIListener& GetUIProgressProgressUIListener()
{
	static gameui::ProgressUIListener staticProgressUIListener;
	return staticProgressUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIProgress, ()  , gamemain::eInstPrioClientUI/*UIProgress*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE     ( theUIProgress, OnFrameMove )
		UIPROC_FRAME_RENDER  ( theUIProgress, OnRender )
};//namespace uicallback
using namespace uicallback;



UIProgress::UIProgress()
{

	// Member
	m_pProgressFrame					= NULL;
	m_pID_PROGRESS				= NULL;
	m_pID_INTERRUPT = NULL;
	m_pID_Text_Name			= NULL;

	m_bFadeIn		= FALSE;
	m_dwStartTime	= 0;
	m_dwPeriod		= 0;
	m_bInverse		= TRUE;
	m_nFlashAmount	= 0;
	m_bAutoClose	= FALSE;
	m_bInterrupt = FALSE; 
}

BOOL UIProgress::IsInterrupt()
{
	return m_bInterrupt;
}


// Frame
BOOL UIProgress::OnFrameMove(DWORD /*dwTick*/)
{
	if (	m_bAutoClose == TRUE 
		&& m_enShowType != SHOWTYPE_NORMAL)
	{
		if ( m_bInverse == TRUE )
		{
			if( m_pID_PROGRESS->GetValue() >= 1.0 )
			{
				m_pProgressFrame->StartFlash( m_nFlashAmount, TRUE, m_bFadeIn );
				m_pID_Text_Name->SetVisible( FALSE, TRUE );
				m_enShowType	= SHOWTYPE_NORMAL;
				return TRUE;
			}
		}
		else
		{
			if( m_pID_PROGRESS->GetValue() <= 0 )
			{
				m_pProgressFrame->StartFlash( m_nFlashAmount, TRUE, m_bFadeIn );
				m_enShowType = SHOWTYPE_NORMAL;
				return TRUE;
			}
		}

	}

	////////////////////////////////////////
	switch( m_enShowType ) 
	{
	case SHOWTYPE_TIMER:
		{
			float fRate = (float)(base::GetRunTime()-m_dwStartTime) / (float)m_dwPeriod;
			if ( m_bInverse == TRUE )
			{
				m_pID_PROGRESS->SetValue( fRate );
			}
			else
			{
				m_pID_PROGRESS->SetValue( 1- fRate);
			}
		}
		break;
	default:
	    break;
	}

	return TRUE;
}


BOOL UIProgress::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}


BOOL UIProgress::LoadUI()
{
	GetUIProgressProgressUIListener().RegisterMe();

	m_pProgressFrame = theUICtrlManager.LoadFrame( UIDOC_PATH( "Progress") );
	if ( m_pProgressFrame == 0 )
	{
		UIMessageLog("读取文件[UI\\Progress.UI]失败")
		return FALSE;
	}

	return InitControls();
}


// 关连控件
BOOL UIProgress::InitControls()
{
	m_pProgressFrame->SetProcOnFrameMove( theUIProgressOnFrameMove );
	m_pProgressFrame->SetProcOnRender	( theUIProgressOnRender, FALSE );

	m_pID_PROGRESS		= (VUCtrlProgress*)	m_pProgressFrame->FindControl(  ID_PROGRESS );
	m_pID_INTERRUPT	= (VUCtrlProgress*)	m_pProgressFrame->FindControl(  ID_INTONATEINTERRUPT );
	m_pID_Text_Name	= (VUCtrlText*)		m_pProgressFrame->FindControl(  ID_TEXT_NAME );

	assert( m_pID_Text_Name );
	assert( m_pID_INTERRUPT );
	assert( m_pID_PROGRESS );

	return TRUE;
}


// 卸载UI
BOOL UIProgress::UnLoadUI()
{
	GetUIProgressProgressUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Progress") );
}

// 是否可视
BOOL UIProgress::IsVisible()
{
	return m_pProgressFrame->IsVisible();
}

// 设置是否可视
void UIProgress::SetVisible( BOOL bVisible )
{
	if( !m_pID_PROGRESS )
		return;

	if( m_pProgressFrame )
		m_pProgressFrame->SetVisible( bVisible );
	if( m_pID_Text_Name )
		m_pID_Text_Name->SetVisible( bVisible );


	if ( bVisible == FALSE )
		m_enShowType = SHOWTYPE_NORMAL;
	else
		m_pID_PROGRESS->SetVisible( bVisible );

	if( m_pID_INTERRUPT )
		m_pID_INTERRUPT->SetVisible( FALSE );
}


void UIProgress::ShowByValue	(int  nValue
                              ,int  nValueMin
										,int  nValueMax
										,int  nFlashAmount
										,BOOL bAutoClose
										,BOOL bFadeIn)
{
	if( !m_pID_PROGRESS )
		return;

	m_enShowType	= SHOWTYPE_VALUE;
	m_nFlashAmount	= nFlashAmount;
	m_bAutoClose	= bAutoClose;
	m_bFadeIn		= bFadeIn;

	m_pID_PROGRESS->SetValue( (float)(nValue-nValueMin)/(float)(nValueMax-nValueMin) );

	SetVisible( TRUE );
}


void UIProgress::ShowByTime	(DWORD       dwStartTime
                              ,DWORD       dwPeriod
										,BOOL        bInverse
										,int         nFlashAmount
										,BOOL        bAutoClose
										,BOOL        bFadeIn
										,LPCSTR  pName
										,BOOL        bInterrupt)
{
	if( !m_pID_PROGRESS )
		return;
	m_enShowType	= SHOWTYPE_TIMER;
	m_dwStartTime	= dwStartTime;
	m_dwPeriod		= dwPeriod;
	m_nFlashAmount	= nFlashAmount;
	m_bAutoClose	= bAutoClose;
	m_bFadeIn		= bFadeIn;
	m_bInverse				= bInverse;
	if ( pName )
		*m_pID_Text_Name = pName;
	else
		*m_pID_Text_Name = "";

	m_bInterrupt = bInterrupt;

	m_pProgressFrame->StartFlash( 0, FALSE, FALSE );

	if ( m_bInverse == TRUE )
		m_pID_PROGRESS->SetValue( 0 );
	else
		m_pID_PROGRESS->SetValue( 1.0f );

	SetVisible( TRUE );

	if( dwPeriod < 16 )
	{
		m_pID_PROGRESS->SetVisible( FALSE );
		if( m_pID_INTERRUPT )
			m_pID_INTERRUPT->SetVisible( TRUE );
	}
}

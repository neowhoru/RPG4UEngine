/*////////////////////////////////////////////////////////////////////////
文 件 名：UIShop.cpp
创建日期：2007年12月15日
最后更新：2007年12月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIShop.h"
#include "UIpack.h"



#include "UIMessageBox.h"
#include "FuncProfiler.h"
#include "UIInputBox.h"
#include "UINpcDialog.h"
#include "VUIControl.h"
#include "TextResManager.h"


#include "UIChatInput.h"


#include "VUILayout.h"
#include "VUIIconInfoMouseTip.h"
#include "ShopUIListener.h"
#include "VHero.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::ShopUIListener& GetUIShopShopUIListener()
{
	static gameui::ShopUIListener staticShopUIListener;
	return staticShopUIListener;
}



#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(UI_ID_FRAME_SHOP, ()  , gamemain::eInstPrioClientUI/*UI_ID_FRAME_SHOP*/);
#else
#endif

namespace uicallback
{
	MAP_FRAME_RUN( s_UI_ID_FRAME_SHOP, OnFrameRun )
		MAP_FRAME_RENDER( s_UI_ID_FRAME_SHOP, OnFrameRender )

		MAP_BUTTON_CLICK_CALLBACK		  (s_UI_ID_FRAME_SHOP, ID_BUTTON_CLOSEOnButtonClick)

		MAP_ICON_DROP_TO_CALLBACK		  (s_UI_ID_FRAME_SHOP, ID_LISTIMG_SHOPOnIconDropTo)
		MAP_ICON_LDB_CLICK_CALLBACK	  (s_UI_ID_FRAME_SHOP, ID_LISTIMG_SHOPOnIconLDBClick)
		MAP_ICON_RBUTTON_UP_CALLBACK	  (s_UI_ID_FRAME_SHOP, ID_LISTIMG_SHOPOnIconRButtonUp)
		MAP_BUTTON_CLICK_CALLBACK		  (s_UI_ID_FRAME_SHOP, ID_BUTTON_REFRESHOnButtonClick)

		MAP_LIST_SELECT_CHANGE_CALLBACK		  (s_UI_ID_FRAME_SHOP, ID_LIST_storeOnListSelectChange)
		MAP_LIST_SELECT_CHANGE_CALLBACK		  (s_UI_ID_FRAME_SHOP, ID_LIST_priceOnListSelectChange)

		MAP_SCROLL_BAR_UPDATE_POS_CALLBACK	  (s_UI_ID_FRAME_SHOP, ID_SCROLLBAR_processScrollChange)

		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy1OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy2OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy3OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy4OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy5OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy6OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy7OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy8OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_buy9OnButtonClick)
		MAP_BUTTON_CLICK_CALLBACK  (s_UI_ID_FRAME_SHOP, ID_BUTTON_mendOnButtonClick)
};//namespace uicallback
using namespace uicallback;


#define Max_ShowCnt 9

UI_ID_FRAME_SHOP::UI_ID_FRAME_SHOP()
{
	//std::vector<S_ItemCost> m_vtItemCost;
	// Member
	m_pID_FRAME_SHOP = NULL;
	m_pID_BUTTON_CLOSE = NULL;
	m_pID_LISTIMG_SHOP = NULL;

	m_pID_LIST_store = NULL;
	m_pID_LIST_price = NULL;

	m_pID_BUTTON_buy1 = NULL;
	m_pID_BUTTON_buy2 = NULL;
	m_pID_BUTTON_buy3 = NULL;
	m_pID_BUTTON_buy4 = NULL;
	m_pID_BUTTON_buy5 = NULL;
	m_pID_BUTTON_buy6 = NULL;
	m_pID_BUTTON_buy7 = NULL;
	m_pID_BUTTON_buy8 = NULL;
	m_pID_BUTTON_buy9 = NULL;
	m_pID_ScrollBar_Shop = NULL;
	m_pID_BUTTON_Mend = NULL;
	
	m_bCtrlSell = FALSE;		
	m_byShopType = 0xFF;
	//VUCtrlButton*	m_pID_BUTTON_REFRESH = NULL;
}

BOOL UI_ID_FRAME_SHOP::BuyGoodsCallBackFun( const char* szInputData, void *pData )
	{
		
		int nGoodsCount = atoi( szInputData );
		if ( nGoodsCount <= 0 )
		{
			OutputDebugString("物品为0\n");
			return FALSE;
		}
		
		MSG_BUYSELLGOODS_SYNStruct* pMsg = (MSG_BUYSELLGOODS_SYNStruct*)pData;
		giteminfo::SItemCommon *pItem = GetGameItemDetail().GetItemByID(pMsg->ustItemId);
		if ( !pItem )
		{
			OutputDebugString("找不到ID\n");
			return FALSE;
		}
		
		if( pItem->IsExclusive() )
		{
			pMsg->ustCount = 1;
		}
		else
		{
			if ( nGoodsCount > pMsg->ustCount )
			{
				return FALSE;
			}
			pMsg->ustCount = nGoodsCount;
			S_ItemCost *pItemCost = NULL;
			pItemCost = s_UI_ID_FRAME_SHOP.GetItemCout( pItem->ustItemID );
			if ( pItemCost )
			{
				if ( pMsg->ustCount > pItemCost->nPerCount )
				{
					pMsg->ustCount = pItemCost->nPerCount;
				}
			}
		}
		OutputDebugString("发送消息\n");
		GetNetworkInput().SendMsg( pMsg );
		return TRUE;
		
	}

	// Frame
BOOL UI_ID_FRAME_SHOP::OnFrameRun(DWORD /*dwTick*/)
	{
		return TRUE;
	}
	BOOL UI_ID_FRAME_SHOP::OnFrameRender(DWORD /*dwTick*/)
	{
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender )
	{
		
		SetVisible(FALSE);
		//GetVUILayout().SetVisible(m_pID_FRAME_SHOP);
		//
		if ( theUINpcDialog.m_nNpcId != -1 )
		{
			//theUIPack.SetVisible(FALSE);
			MSG_EXESCRIPT_SYNStruct msg;
			msg.stNpcId = theUINpcDialog.m_nNpcId;
			GetNetworkInput().SendMsg( &msg );
		}
		return TRUE;
		
	}
	BOOL UI_ID_FRAME_SHOP::SellGoods(  BOOL bPressYesButton, void *pData  )
	{
		if ( bPressYesButton == TRUE )
		{
			BuyGoodsCallBackFun( "1", pData );
		}
		return TRUE;
	}
	// ListImg / ListEx
	BOOL UI_ID_FRAME_SHOP::ID_LISTIMG_SHOPOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe,
											IconDragListImg* pItemDrag,
											IconDragListImg* pItemDest )
	{
		
		// 买东东
		if ( pSender == theUIPack.m_pID_LISTIMG_PACK )
		{
			if ( !pItemDrag->IsNull() )
			{
				int nPackIndex = theUIPack.m_pID_LISTIMG_PACK->GetItemIndex( pItemDrag );
				//如果物品已经加锁 return			
				ggdat::SCharItem stItem;
				GetPlayerRole().m_bag.GetItem( nPackIndex, &stItem );
				if(stItem.storageinfo.bIsLocked)
				{
					theUIMessageBox.Show( theTextResManager.GetString( eText_ItemHaveBeenLocked ) );
					return FALSE;
				}

				giteminfo::SItemCommon *pItem = GetGameItemDetail().GetItemByID(pItemDrag->GetIconInfo()->Id());
				if ( !pItem )
					return FALSE;
				// 是否可买卖
				if ( !pItem->bIsCanSellShop )
				{
					theUIMessageBox.Show( "对不起,此物品不能卖给商店!" );
					return FALSE;
				}
				if ( m_byShopType != pItem->ucItemType &&
					m_byShopType != 0xff )
				{
					switch( m_byShopType )
					{
					case giteminfo::ITEMTYPE_RESTORE: //恢复药品
						theUIMessageBox.Show( "对不起,本小店只回收恢复药品!" );
						break;
					case giteminfo::ITEMTYPE_WEAPON:	 //武器
						theUIMessageBox.Show( "对不起,本小店只回收武器!" );
						break;
					case giteminfo::ITEMTYPE_ARMOUR:     //防具
						theUIMessageBox.Show( "对不起,本小店只回收防具!" );
						break;
					case giteminfo::ITEMTYPE_MATERIAL:	 //原料
						theUIMessageBox.Show( "对不起,本小店只回收原料!" );
						break;
					case giteminfo::ITEMTYPE_GEM:				//宝石
						theUIMessageBox.Show( "对不起,本小店只回收宝石!" );
						break;
					case giteminfo::ITEMTYPE_CREATEITEMRULE:	//合成配方
						theUIMessageBox.Show( "对不起,本小店只回收合成配方!" );
						break;
					default:
						theUIMessageBox.Show( "对不起,本小店不回收此类物品!" );
						break;
					}
					return FALSE;
				}
				MSG_BUYSELLGOODS_SYNStruct msg;
				msg.ustItemId = pItem->ustItemID;
				msg.bIsBuy = FALSE;
				msg.ustPackIndex = nPackIndex;
				msg.ustCount = pItemDrag->GetIconInfo()->Count();
				if(m_bCtrlSell)
				{
					char szCnt[56] = {0};
					sprintf( szCnt, "%d", pItemDrag->GetIconInfo()->Count());
					BuyGoodsCallBackFun(szCnt, &msg);
				}
				else
				{
					if(pItem->IsExclusive())
					{
						theUIMessageBox.Show( theTextResManager.GetString(eText_IsSellItem), "询问", MB_YESNO, TRUE,
											SellGoods, &msg, sizeof(msg) );
					}
					else
					{
						if(pItemDrag->GetIconInfo()->Count() > 1)
						{
							S_ItemCost *pItemCost = NULL;
							pItemCost = GetItemCout( pItem->ustItemID );
							std::string strInfo = theTextResManager.GetString(eText_InputSellItemCnt);
							if ( !pItemCost )
							{
								strInfo += theTextResManager.GetString(eText_ItemNoBuy);//"(注意:本店将低价收购此物品!)";
							}
							theUIInputBox.Show( strInfo.c_str(),
												TRUE, TRUE, BuyGoodsCallBackFun,
												&msg, sizeof(msg) );
							theUIInputBox.m_pID_EDIT_INPUT->SetText( pItemDrag->GetIconInfo()->Count() );
						}
						else
							theUIMessageBox.Show( theTextResManager.GetString(eText_IsSellItem), "询问", MB_YESNO, TRUE, SellGoods, &msg, sizeof(msg) );
					}
				}
			}
		}
		return FALSE;
		
	}

	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_REFRESHOnButtonClick( VUCtrlObject* pSender )
	{
		
		static BOOL bReq = TRUE;
		if ( bReq )
		{
			bReq = FALSE;
			MSG_OPENSHOP_SYNStruct msg;
			GetNetworkInput().SendMsg( &msg );
		}
		else
		{
			static DWORD dwLastTiem = timeGetTime();
			if ( timeGetTime() - dwLastTiem >= 15*1000 )
			{
				dwLastTiem = timeGetTime();
				bReq = TRUE;
			}
		}
		return TRUE;
		
	}
	//BOOL ID_LISTIMG_SHOPOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pMe,
	//										IconDragListImg* pItem )
	//{
	//	return FALSE;
	//}
	BOOL UI_ID_FRAME_SHOP::ID_LISTIMG_SHOPOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem )
	{
		return FALSE;
	}
	BOOL UI_ID_FRAME_SHOP::ID_LISTIMG_SHOPOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
	{
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(pSender, NULL, pItem, NULL);
		return FALSE;
	}
	void UI_ID_FRAME_SHOP::ID_LIST_storeOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem )
	{

	}
	void UI_ID_FRAME_SHOP::ID_LIST_priceOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem )
	{

	}
	
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy1OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy2OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 1);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy3OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 2);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy4OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 3);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy5OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 4);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy6OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 5);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy7OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 6);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy8OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 7);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}
	// Button
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_buy9OnButtonClick( VUCtrlObject* pSender )
	{
		int nIndex = m_pID_LISTIMG_SHOP->GetStartIndex();
		IconDragListImg* pItem = m_pID_LISTIMG_SHOP->GetItemByIndex(nIndex + 8);
		theUIPack.ID_LISTIMG_PACKOnIconDropTo(m_pID_LISTIMG_SHOP, NULL, pItem, NULL);
		return TRUE;
	}

	// 装载UI
	BOOL UI_ID_FRAME_SHOP::LoadUI()
	{
		ON_FUNC_PROFILER(__FUNCDNAME__);
		GetUIShopShopUIListener().RegisterMe();

		DWORD dwResult = GetVUCtrlManager().AddFrame( UIDOC_PATH( "Shop") );
		if ( dwResult == 0 )
		{
			MESSAGE_BOX("读取文件[UI\\Shop.UI]失败")
			return FALSE;
		}
		else if ( dwResult != 11092/*文件版本号*/ )
		{
			MESSAGE_BOX("读取文件[UI\\Shop.UI]与源代码版本不一样")
		}
		return InitControls();
		
	}
	// 关连控件
	BOOL UI_ID_FRAME_SHOP::InitControls()
	{
		ON_FUNC_PROFILER(__FUNCDNAME__);
		GetVUCtrlManager().OnFrameRun( ID_FRAME_SHOP, s_UI_ID_FRAME_SHOPOnFrameRun );
		GetVUCtrlManager().OnFrameRender( ID_FRAME_SHOP, s_UI_ID_FRAME_SHOPOnFrameRender );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_CLOSE, s_UI_ID_FRAME_SHOPID_BUTTON_CLOSEOnButtonClick );
		GetVUCtrlManager().OnIconDropTo( ID_FRAME_SHOP, ID_LISTIMG_SHOP,	s_UI_ID_FRAME_SHOPID_LISTIMG_SHOPOnIconDropTo );
//		GetVUCtrlManager().OnIconDragOff( ID_FRAME_SHOP, ID_LISTIMG_SHOP, s_UI_ID_FRAME_SHOPID_LISTIMG_SHOPOnIconDragOff );
		GetVUCtrlManager().OnIconLDBClick( ID_FRAME_SHOP, ID_LISTIMG_SHOP, s_UI_ID_FRAME_SHOPID_LISTIMG_SHOPOnIconLDBClick );
		GetVUCtrlManager().OnIconRButtonUp( ID_FRAME_SHOP, ID_LISTIMG_SHOP, s_UI_ID_FRAME_SHOPID_LISTIMG_SHOPOnIconRButtonUp );
		//GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_REFRESH, s_UI_ID_FRAME_SHOPID_BUTTON_REFRESHOnButtonClick );
		GetVUCtrlManager().OnListSelectChange( ID_FRAME_SHOP, ID_LIST_store, s_UI_ID_FRAME_SHOPID_LIST_storeOnListSelectChange );
		GetVUCtrlManager().OnListSelectChange( ID_FRAME_SHOP, ID_LIST_price, s_UI_ID_FRAME_SHOPID_LIST_priceOnListSelectChange );
		GetVUCtrlManager().OnScrollBarUpdatePos( ID_FRAME_SHOP, ID_SCROLLBAR_process, s_UI_ID_FRAME_SHOPID_SCROLLBAR_processScrollChange);
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy1, s_UI_ID_FRAME_SHOPID_BUTTON_buy1OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy2, s_UI_ID_FRAME_SHOPID_BUTTON_buy2OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy3, s_UI_ID_FRAME_SHOPID_BUTTON_buy3OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy4, s_UI_ID_FRAME_SHOPID_BUTTON_buy4OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy5, s_UI_ID_FRAME_SHOPID_BUTTON_buy5OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy6, s_UI_ID_FRAME_SHOPID_BUTTON_buy6OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy7, s_UI_ID_FRAME_SHOPID_BUTTON_buy7OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy8, s_UI_ID_FRAME_SHOPID_BUTTON_buy8OnButtonClick );
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_buy9, s_UI_ID_FRAME_SHOPID_BUTTON_buy9OnButtonClick);
		GetVUCtrlManager().OnButtonClick( ID_FRAME_SHOP, ID_BUTTON_mend, s_UI_ID_FRAME_SHOPID_BUTTON_mendOnButtonClick );

		m_pID_FRAME_SHOP = (VUCtrlFrame*)GetVUCtrlManager().FindFrame( ID_FRAME_SHOP );
		m_pID_BUTTON_CLOSE = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_CLOSE );
		m_pID_LISTIMG_SHOP = (VUCtrlListImg*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_LISTIMG_SHOP );
		m_pID_LIST_store = (VUCtrlList*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_LIST_store );
		m_pID_LIST_price = (VUCtrlList*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_LIST_price );
		m_pID_BUTTON_buy1 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy1 );
		m_pID_BUTTON_buy2 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy2 );
		m_pID_BUTTON_buy3 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy3 );
		m_pID_BUTTON_buy4 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy4 );
		m_pID_BUTTON_buy5 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy5 );
		m_pID_BUTTON_buy6 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy6 );
		m_pID_BUTTON_buy7 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy7 );
		m_pID_BUTTON_buy8 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy8 );
		m_pID_BUTTON_buy9 = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_buy9 );
		m_pID_ScrollBar_Shop = (VUCtrlScrollBar*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_SCROLLBAR_process);
		m_pID_BUTTON_Mend = ( VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_SHOP, ID_BUTTON_mend );
		assert(m_pID_BUTTON_Mend);
		
		assert( m_pID_FRAME_SHOP );
		m_pID_FRAME_SHOP->SetVisible(FALSE);
		GetVUILayout().AddFrame( m_pID_FRAME_SHOP );
		assert( m_pID_BUTTON_CLOSE );
		assert( m_pID_LISTIMG_SHOP );
		assert(m_pID_LIST_store);
		assert(m_pID_LIST_price);
		assert( m_pID_BUTTON_buy1 );
		assert( m_pID_BUTTON_buy2 );
		assert( m_pID_BUTTON_buy3 );
		assert( m_pID_BUTTON_buy4 );
		assert( m_pID_BUTTON_buy5 );
		assert( m_pID_BUTTON_buy6 );
		assert( m_pID_BUTTON_buy7 );
		assert( m_pID_BUTTON_buy8 );
		assert( m_pID_BUTTON_buy9 );
		assert( m_pID_ScrollBar_Shop );
		

		//m_pID_LIST_store->setSc
		m_pID_LISTIMG_SHOP->SetIsShop( TRUE );
		m_pID_LISTIMG_SHOP ->SetScrollBarEnable( FALSE );
		m_pID_LIST_price->SetScrollBarEnable( FALSE );

		m_pID_LIST_price->HaveSelBar(FALSE);
		m_pID_LIST_price->SetItemMaxCount(m_pID_LISTIMG_SHOP->GetItemCnt());
		//m_pID_LIST_store->HaveSelBar(FALSE);

		//SetVisible(TRUE);
		RefreshBtn();

		USE_SCRIPT ( eUI_OBJECT_Shop, m_pID_FRAME_SHOP );
		return TRUE;
		
	}
	// 卸载UI
	BOOL UI_ID_FRAME_SHOP::UnLoadUI()
	{
		
		CLOSE_SCRIPT( eUI_OBJECT_Shop );
		GetUIShopShopUIListener().UnregisterMe();
		return GetVUCtrlManager().RemoveFrame( UIDOC_PATH( "Shop") );
		
	}
	// 是否可视
	BOOL UI_ID_FRAME_SHOP::IsVisible()
	{
		
		return m_pID_FRAME_SHOP->IsVisible();
		
	}
	// 设置是否可视
	void UI_ID_FRAME_SHOP::SetVisible( BOOL bVisible)
	{
		
		GetVUIIconInfoMouseTip().m_bShopOpen = bVisible;
		if ( m_pID_FRAME_SHOP )
		{
			if( bVisible != IsVisible() )
				GetVUILayout().SetVisible(m_pID_FRAME_SHOP);
			else
				m_pID_FRAME_SHOP->SetVisible( bVisible );
		}
		if ( bVisible == TRUE )
		{
			//theUIPack.m_pID_FRAME_PACK->SetArrayMode( ArrayMode_Top );
			//m_pID_FRAME_SHOP->SetArrayMode( ArrayMode_Top );
			if(!theUIPack.IsVisible())
				theUIPack.SetLayoutVisible();
		}
		else
		{
			theUIPack.m_pID_FRAME_PACK->SetArrayMode( ArrayMode_Normal );
		}
		
		m_pID_ScrollBar_Shop->SetValue(0);
		ID_SCROLLBAR_processScrollChange(NULL, 0);
		RefreshBtn();
		
	}
	BOOL UI_ID_FRAME_SHOP::ID_BUTTON_mendOnButtonClick( VUCtrlObject* pSender )
	{//修理啦...
		
		

		////-- 这里要检索 equpe 里面有的物品都发一次修理消息
		MsgRepairItem msg;		

		//zizi add 这里计算金钱够不够
		DWORD dwMoney = GetPlayerRole().GetData(PlayerRole::TYPE_MONEY);
		int nItemHp = 0, nItemMaxHp = 0, nHpPoint = 0, mendPri = 0;

		int nAllSprice = 0;
		for( int i = 0; i < ggdat::VISUAL_MAX; ++i)
		{
			if( i == giteminfo::SItemCanEquip::eHair || i == giteminfo::SItemCanEquip::eFace )
				continue;

			if(GetPlayerRole().m_charinfo.visual.equipitem[i].itembaseinfo.ustItemID != ErrorUnsignedShortID)
			{
				nItemHp = GetPlayerRole().m_charinfo.visual.equipitem[i].equipdata.usHP;
				nItemMaxHp = GetPlayerRole().m_charinfo.visual.equipitem[i].equipdata.usHpMax;
				if( nItemHp >= 0 && nItemHp < nItemMaxHp )
				{//这里说明耐久已经下降了　应该计算修理
					//nHpPoint = nItemMaxHp - nItemHp;
					giteminfo::SItemCanEquip *pItem = (giteminfo::SItemCanEquip *)GetGameItemDetail().GetItemByID(GetPlayerRole().m_charinfo.visual.equipitem[i].itembaseinfo.ustItemID ); 
					if( pItem )
					{
						//nAllSprice += pItem->stRepairPrice * nHpPoint;		//pItem->stRepairPrice 物品单个点数的修理价格
						//dwCost   修理价格＝武器价格*系数*(1－现有耐久/ 最大耐久)	
						//mendPri = pItem->dwCost * /*系数**/ (float)(1 - nItemHp/nItemMaxHp);
						//nAllSprice += mendPri;

						//VCharacter *pMe = singleton::GetPlayerManager().GetMe();
						//if(pMe)
						{
							//std::string szRoleJob = theVHero.GetProfession();
							string szProfession = GetSystemConfig().GetProessionInfoFromID(theVHero.GetProfession())->GetProShow();
							giteminfo::SCharModulus *pCharModulus = GetGameItemDetail().GetCharModulusByName( szProfession.c_str() );
							if(pCharModulus)
							{
								//pCharModulus->fMendRage;
								mendPri = pItem->dwCost * pCharModulus->fMendRage * (float)(1 - nItemHp/nItemMaxHp);
							}
						}
						nAllSprice += mendPri;
					}
				}
			}
		}

		if( nAllSprice > dwMoney )
		{
			theUIChatInput.AddInfo( theTextResManager.GetString(eText_MoneyNotEnoungh), SYSTEM_COLOR );
			return FALSE;
		}

		//-----------
		//VCharacter* pMe = singleton::GetPlayerManager().GetMe();
	//	GetPlayerRole().OnAddTeamMember()
		//if(pMe)
			msg.header.stID =  theVHero.GetID();
		for (int nLoop = 0; nLoop < ggdat::VISUAL_MAX;nLoop++)
		{
			if (GetPlayerRole().m_charinfo.visual.equipitem[nLoop].itembaseinfo.ustItemID != ErrorUnsignedShortID)
			{
				if (GetPlayerRole().m_charinfo.visual.equipitem[nLoop].equipdata.usHP >= 0 && 
					GetPlayerRole().m_charinfo.visual.equipitem[nLoop].equipdata.usHP < GetPlayerRole().m_charinfo.visual.equipitem[nLoop].equipdata.usHpMax )
				{
					msg.nHp = GetPlayerRole().m_charinfo.visual.equipitem[nLoop].equipdata.usHpMax;
					msg.stEquipIndex = nLoop;
					GetNetworkInput().SendMsg(&msg);
				}
			}
		}

		//这里减去金钱
		dwMoney -= nAllSprice;
		GetPlayerRole().SetMoneyOnPlayer( dwMoney );
		theUIPack.Refresh();

		char szTemp[128] = {0};
		sprintf( szTemp, "修理完成！共花费%d铜。", nAllSprice);
		theUIMessageBox.Show( szTemp );

		return TRUE;
		
	}

	//
	UI_ID_FRAME_SHOP::S_ItemCost* UI_ID_FRAME_SHOP::GetItemCout(int nId)
	{
		
		for ( unsigned int n=0; n<m_vtItemCost.size(); n++ )
		{
			if( m_vtItemCost[n].nId == nId )
			{
				return &m_vtItemCost[n];
			}
		}
		return NULL;
		
	}

	void UI_ID_FRAME_SHOP::SetCtrlSell(BOOL bCtrlSell)
	{
		m_bCtrlSell = bCtrlSell;
	}
	
	void UI_ID_FRAME_SHOP::ID_SCROLLBAR_processScrollChange( VUCtrlObject* pSender, int nValue )
	{//这里在处理别的 控件相应的 scrollbar 的操作
		int nCnt = m_pID_LIST_price->GetListItemCnt();
		if( nCnt > Max_ShowCnt )
		{
			m_pID_ScrollBar_Shop->SetMaxValue(nCnt - Max_ShowCnt);
			m_pID_ScrollBar_Shop->SetStepValue(1);
		}
		if( m_pID_LIST_price->GetListItemCnt() > Max_ShowCnt )
		{
			m_pID_LISTIMG_SHOP->SetStartIndex( nValue );
			m_pID_LIST_price->SetStartIndex( nValue );
			RefreshBtn();
		}
	}

	void UI_ID_FRAME_SHOP::RefreshBtn()
	{
		m_pID_BUTTON_buy1->SetVisible(TRUE); m_pID_BUTTON_buy2->SetVisible(TRUE); m_pID_BUTTON_buy3->SetVisible(TRUE);
		m_pID_BUTTON_buy4->SetVisible(TRUE); m_pID_BUTTON_buy5->SetVisible(TRUE); m_pID_BUTTON_buy6->SetVisible(TRUE);
		m_pID_BUTTON_buy7->SetVisible(TRUE); m_pID_BUTTON_buy8->SetVisible(TRUE); m_pID_BUTTON_buy9->SetVisible(TRUE);

		if(m_pID_LIST_price->GetListItem().size() > 0)
		{
			int nShowCnt = m_pID_LIST_price->GetListItem().size() - m_pID_LIST_price->GetStartIndex();
			nShowCnt = nShowCnt < 0 ? 0 : nShowCnt;
			if(nShowCnt < Max_ShowCnt)
			{
				switch(Max_ShowCnt - nShowCnt)
				{
				case 1:
					m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 2:
					m_pID_BUTTON_buy8->SetVisible(FALSE); m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 3:
					m_pID_BUTTON_buy7->SetVisible(FALSE); m_pID_BUTTON_buy8->SetVisible(FALSE); m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 4:
					m_pID_BUTTON_buy6->SetVisible(FALSE); m_pID_BUTTON_buy7->SetVisible(FALSE); m_pID_BUTTON_buy8->SetVisible(FALSE);
					m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 5:
					m_pID_BUTTON_buy5->SetVisible(FALSE); m_pID_BUTTON_buy6->SetVisible(FALSE); m_pID_BUTTON_buy7->SetVisible(FALSE);
					m_pID_BUTTON_buy8->SetVisible(FALSE); m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 6:
					m_pID_BUTTON_buy4->SetVisible(FALSE); m_pID_BUTTON_buy5->SetVisible(FALSE); m_pID_BUTTON_buy6->SetVisible(FALSE);
					m_pID_BUTTON_buy7->SetVisible(FALSE); m_pID_BUTTON_buy8->SetVisible(FALSE); m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 7:
					m_pID_BUTTON_buy3->SetVisible(FALSE); m_pID_BUTTON_buy4->SetVisible(FALSE); m_pID_BUTTON_buy5->SetVisible(FALSE);
					m_pID_BUTTON_buy6->SetVisible(FALSE); m_pID_BUTTON_buy7->SetVisible(FALSE); m_pID_BUTTON_buy8->SetVisible(FALSE);
					m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 8:
					m_pID_BUTTON_buy2->SetVisible(FALSE); m_pID_BUTTON_buy3->SetVisible(FALSE); m_pID_BUTTON_buy4->SetVisible(FALSE);
					m_pID_BUTTON_buy5->SetVisible(FALSE); m_pID_BUTTON_buy6->SetVisible(FALSE); m_pID_BUTTON_buy7->SetVisible(FALSE);
					m_pID_BUTTON_buy8->SetVisible(FALSE); m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				case 9:
					m_pID_BUTTON_buy1->SetVisible(FALSE); m_pID_BUTTON_buy2->SetVisible(FALSE); m_pID_BUTTON_buy3->SetVisible(FALSE);
					m_pID_BUTTON_buy4->SetVisible(FALSE); m_pID_BUTTON_buy5->SetVisible(FALSE); m_pID_BUTTON_buy6->SetVisible(FALSE);
					m_pID_BUTTON_buy7->SetVisible(FALSE); m_pID_BUTTON_buy8->SetVisible(FALSE); m_pID_BUTTON_buy9->SetVisible(FALSE);
					break;
				}
			}
		}
	}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIQuestTrack.h
DOC 文件：uidata\UIQuestTrack.uidoc
UI  名称：QuestTrack
创建日期：2008年7月6日
最后更新：2008年7月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIQuestTrack.h"
#include "VUILayoutManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIQuestTrack, ()  , gamemain::eInstPrioClientUI);

//#define USE_UIQUESTTRACK_LAYOUT

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
QuestTrackUIListener& GetUIQuestTrackQuestTrackUIListener()
{
	static QuestTrackUIListener staticQuestTrackUIListener;
	return staticQuestTrackUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIQuestTrack, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIQuestTrack, OnRender )
	UIPROC_LIST_SELECT_CHANGE	( theUIQuestTrack, ID_LIST_TRACKOnListSelectChange )

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
QuestTrackUIListener::QuestTrackUIListener()
{
}
QuestTrackUIListener::~QuestTrackUIListener()
{
}

LPCSTR QuestTrackUIListener::GetName()
{
	return "QuestTrackUIListener";
}

EGameUIType QuestTrackUIListener::GetType()
{
	return eUIQuestTrack;
}

void QuestTrackUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUIQuestTrack.SetVisible((BOOL)dwData);
		}
		break;
	}
}

BOOL QuestTrackUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void QuestTrackUIListener::Refresh(BOOL /*bExtra*/)
{
	theUIQuestTrack.Refresh();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIQuestTrack::UIQuestTrack()
{
	m_pID_FRAME_TRACK		= NULL;
	m_pID_LIST_TRACK		= NULL;

}

// Frame
BOOL UIQuestTrack::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
	
}


BOOL UIQuestTrack::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

////////////////////////////////////////////////////
//ID_LIST_TRACK::OnListSelectChange
void UIQuestTrack::ID_LIST_TRACKOnListSelectChange(VUCtrlObject* /*pSender*/, sUILIST_ITEM*	/*pItem*/)
{
	
}




// 设置是否可视
void UIQuestTrack::SetVisible( BOOL bVisible )
{
#ifdef USE_UIQUESTTRACK_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_TRACK->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		m_pID_FRAME_TRACK->SetArrangeMode( ArrayMode_Normal );
	}
	else
	{
		Refresh();
	}
#else
		m_pID_FRAME_TRACK->SetVisible( bVisible );
#endif
	
}
void UIQuestTrack::SetLayoutVisible()
{
#ifdef USE_UIQUESTTRACK_LAYOUT
	theUILayoutManager.SetVisible(m_pID_FRAME_TRACK);
#endif
}

void UIQuestTrack::Refresh()
{
}


// 装载UI
BOOL UIQuestTrack::LoadUI()
{
	GetUIQuestTrackQuestTrackUIListener().RegisterMe();

	m_pID_FRAME_TRACK = theUICtrlManager.LoadFrame( UIDOC_PATH( "QuestTrack") );
	if ( m_pID_FRAME_TRACK == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "QuestTrack") "]失败");
		return FALSE;
	}

#ifdef USE_UIQUESTTRACK_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_TRACK);
#endif
	m_pID_FRAME_TRACK->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIQuestTrack::InitControls()
{
	m_pID_FRAME_TRACK->SetProcOnFrameMove	(theUIQuestTrackOnFrameMove);
	m_pID_FRAME_TRACK->SetProcOnRender		(theUIQuestTrackOnRender, FALSE);

	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_LIST_TRACK	= (VUCtrlList*)	m_pID_FRAME_TRACK->FindControl( UIQUESTTRACK_ID_LIST_TRACK );
	
	
	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_LIST_TRACK );
	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_LIST_TRACK	->SetProcOnSelectChange	( theUIQuestTrackID_LIST_TRACKOnListSelectChange );


	m_pID_FRAME_TRACK->SetMsgHangUp		(FALSE);	//不接受消息
	m_pID_LIST_TRACK->SetMsgHangUp		(FALSE);	//不接受消息
	m_pID_LIST_TRACK->SetSelectionInfo	(FALSE);	

	return TRUE;
}


UIQuestTrack::~UIQuestTrack()
{
	//UnLoadUI();
	
}


// 卸载UI
BOOL UIQuestTrack::UnLoadUI()
{
	GetUIQuestTrackQuestTrackUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "QuestTrack") );
	
}

// 是否可视
BOOL UIQuestTrack::IsVisible()
{
	if( m_pID_FRAME_TRACK )
		return m_pID_FRAME_TRACK->IsVisible();

	return FALSE;
}

};//namespace gameui


/*////////////////////////////////////////////////////////////////////////
文 件 名：UIContainHelper.cpp
创建日期：2007年9月17日
最后更新：2007年9月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "UIContainHelper.h"
#include "VUCtrlListImg.h"
#include "Player.h"
#include "ItemInfoParser.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIContainHelper::UIContainHelper	(VUCtrlListImg*	pListImg
											,Player* pPlayer
											,UINT					nNumPerPage
											,INT					nCurrentPage
											,UIContainHelperListener*	pListener)
:m_pListImg(pListImg)
,m_ppListImg(NULL)
,m_pPlayer(pPlayer)
,m_nCurrentPage(nCurrentPage)
,m_nNumPerPage(nNumPerPage)
,m_pListener(pListener)
{
	assert(m_pListImg);
	assert(m_pPlayer);

	m_nSlotIndexAt		= 0;
	m_nSlotPageAt		= 0;
	m_nSlotWorkingAt	= 0;
}

UIContainHelper::UIContainHelper	(VUCtrlListImg**	ppListImg
											,Player* pPlayer
											,UINT					nNumPerPage
											,INT					nCurrentPage
											,UIContainHelperListener*	pListener
											,BOOL					bPageContainer)
:m_pListImg(NULL)
,m_ppListImg(ppListImg)
,m_pPlayer(pPlayer)
,m_nCurrentPage(nCurrentPage)
,m_nNumPerPage(nNumPerPage)
,m_pListener(pListener)
,m_bPageContainer(bPageContainer)
{
	assert(m_ppListImg);
	assert(m_pPlayer);

	m_nSlotIndexAt		= 0;
	m_nSlotPageAt		= 0;
	m_nSlotWorkingAt	= 0;
}


UIContainHelper::~UIContainHelper()
{
}


BOOL UIContainHelper::PushItemEx	(BaseSlot& slot,BOOL bKeepInvisible,BOOL bNeedDetail)
{
	__BOOL_CALL(PushItem(slot,bNeedDetail));

	DWORD dwState = slot.GetState();
	if(!bKeepInvisible)
	{
		_BIT_REMOVE(dwState, SLOT_UISTATE_INVISIBLE);
	}

	return SetItemState(slot, dwState);
}

BOOL UIContainHelper::PopItemEx	(BaseSlot& slot)
{
	__BOOL_CALL(PopItem(slot));
	return SetItemState(slot, 0);
}

BOOL UIContainHelper::PushItem	(BaseSlot& slot,BOOL bNeedDetail)
{
	CODETYPE				itemID;
	UINT					nCount;
	sITEMINFO_BASE*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;


	__CHECK(CalcSlotWorkingAt(slot));



	itemID	= slot.GetCode();
	nCount	= slot.GetNum();


	pInfo = theItemInfoParser.GetItemInfo(itemID);
	__VERIFY_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						);



	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;

	if(bNeedDetail)
	{
		if(slot.GetSlotType() == ST_ITEM)
		{
			stListItem.SetDamaged( ((ItemSlot&)slot).GetDamageLV() > 0 );
		}

		if(pInfo->IsPotion())
		{
			pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer(pInfo->m_byWasteType);

			stListItem.SetTimer(pTimer);
		}
	}

	////////////////////////////////////////
	stListItem.SetSlot(&slot, pVInfo->GetIconType());

	stListItem.SetData	(itemID
                        ,nCount
								,pVInfo->GetIconType()
								,TRUE
								,pInfo->IsCanUseWaste());


	///////////////////////////////////////////
	m_pListImg->SetItem( &stListItem, m_nSlotWorkingAt );

	if(m_pListener)
		m_pListener->OnItemSlot(slot, TRUE);
	return TRUE;
}


BOOL UIContainHelper::PopItem	(BaseSlot& slot)
{
	__CHECK(CalcSlotWorkingAt(slot));



	//////////////////////////////////////////////

	if(	slot.GetSerial() == 0 
		|| slot.GetNum() <= 1
		|| !slot.IsOverlap())
	{
		m_pListImg->RemoveItemAt(  m_nSlotWorkingAt );
		if(m_pListener)
			m_pListener->OnItemSlot(slot, FALSE);
	}
	else//if(slot.GetNum())
	{
		IconDragListImg*	pImg;
		IVUIIconInfo*		pIcon;
		pImg = m_pListImg->GetItemAt(m_nSlotWorkingAt);
		if(pImg)
		{
			pIcon = pImg->GetIconInfo();
			if(pIcon)
				pIcon->SetCount(slot.GetNum() - 1);
		}
	}

	return TRUE;
}

BOOL		UIContainHelper::CalcSlotAt(BaseSlot& slot)
{
	SLOTPOS posSlot = slot.GetPos();
	if(posSlot == INVALID_POSTYPE)
		return FALSE;

	if(m_nNumPerPage)
	{
		m_nSlotPageAt	= posSlot/m_nNumPerPage;
		m_nSlotIndexAt	= posSlot%m_nNumPerPage;

		if(	m_nCurrentPage >= 0 
			&& m_nSlotPageAt != m_nCurrentPage)
			return FALSE;
	}
	else
	{
		m_nSlotPageAt	= 0;
		m_nSlotIndexAt	= posSlot;
	}

	return TRUE;
}

BOOL UIContainHelper::CalcSlotWorkingAt	(BaseSlot& slot)
{
	__CHECK(CalcSlotAt(slot));

	m_nSlotWorkingAt	= m_nSlotIndexAt;

	if(m_ppListImg)
	{
		if(m_bPageContainer)
		{
			m_pListImg			= m_ppListImg[m_nSlotPageAt];
		}
		else
		{
			m_pListImg			= m_ppListImg[m_nSlotIndexAt];
			m_nSlotWorkingAt	= 0;
		}
	}

	if(!m_pListImg)
		return FALSE;

	return TRUE;
}


BOOL UIContainHelper::SetItemState(BaseSlot& slot, DWORD dwState)
{
	__CHECK(CalcSlotWorkingAt(slot));


	///////////////////////////////////////////////
	IconDragListImg*	pImg;
	pImg = m_pListImg->GetItemAt(m_nSlotWorkingAt);
	if(pImg)
	{
		//pImg->SetDeactivate	(dwState == SLOT_UISTATE_DEACTIVATED);
		pImg->SetEnable		( !_BIT_TEST(dwState,SLOT_UISTATE_DISABLE) );
		//pImg->SetDamaged		(dwState != SLOT_UISTATE_DAMAGED);
		pImg->SetInvisible	( _BIT_TEST(dwState ,SLOT_UISTATE_INVISIBLE), TRUE);

		pImg->SetDeactivate	(_BIT_TEST(dwState ,SLOT_UISTATE_DEACTIVATED ));
	}
	return TRUE;
}

BOOL UIContainHelper::SetReadOnlyState(BaseSlot& slot, DWORD dwState)
{
	__CHECK(CalcSlotWorkingAt(slot));


	///////////////////////////////////////////////
	IconDragListImg*	pImg;
	pImg = m_pListImg->GetItemAt(m_nSlotWorkingAt);
	if(pImg)
	{
		pImg->SetEnable		( !_BIT_TEST(dwState,SLOT_UISTATE_DISABLE) );
		//pImg->SetDamaged		(dwState != SLOT_UISTATE_DAMAGED);
		//pImg->SetInvisible	( _BIT_TEST(dwState ,SLOT_UISTATE_INVISIBLE), TRUE);

		pImg->SetDeactivate	(_BIT_TEST(dwState ,SLOT_UISTATE_DEACTIVATED ));
	}
	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：uiitemcomposite.cpp
创建日期：2006年12月27日
最后更新：2006年12月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "UIItemComposite.h"
#include "ItemCompoundDialog.h"
#include "UIItemCompound.h"
#include "VUCtrlManager.h"
#include "VUILayoutManager.h"
#include "UIInventory.h"
#include "BaseContainer.h"
#include "ItemManager.h"
#include "Player.h"
#include "ItemCompoundDialog.h"
#include "InputLayer.h"

#include "Hero.h"
#include "HeroTipLayer.h"
#include "VUIIconMouseTip.h"
#include "VUCtrlIconDragManager.h"
#include "ItemSlot.h"
#include "UIItemCompositeSlotUIListener.h"
#include "UIItemCompositeSubResultSlotUIListener.h"
#include "UIItemCompositeTargetSlotUIListener.h"
#include "UIItemCompositeSocketSlotUIListener.h"
#include "UIContainHelper.h"

namespace gameui
{
static DWORD gs_arSlotNumPerPage[]=
{
	 eITEMENCHANT_MAX_SLOT
	,eITEMENCHANT_TARGET_MAX_SLOT
	,eITEMENCHANT_SUBRESULT_MAX_SLOT
	,eITEMENCHANT_SOCKET_MAX_SLOT
};

inline int GetLocalIndex(SLOTINDEX idx)
{
	switch(idx)
	{
	case SI_ENCHANT:				return 0;
	case SI_ENCHANT_TARGET:		return 1;
	case SI_ENCHANT_SUBRESULT:	return 2;
	case SI_ENCHANT_SOCKET:		return 3;
	default:
		assert(!"未支持的SlotIdx");
		break;
	}
	return 0;
}

static ItemCompositeFunc gs_arCallbacks[]=
{
	 &UIItemComposite::ChangeMaterialItem
	,&UIItemComposite::ChangeTargetItem
	,&UIItemComposite::ChangeSubResultItem
	,&UIItemComposite::ChangeSocketItem
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIItemComposite::UIItemComposite()
{
	m_pID_BUTTON_CLOSE  				= NULL;
	m_pID_BUTTON_HELP					= NULL;
	m_pID_BUTTON_CANCEL 				= NULL;
	m_pID_LISTIMG_RESULT				= NULL;
	m_pID_PICTURE_MODEL 				= NULL;
	m_pID_LISTIMG_MATERIAL 			= NULL;
	m_pID_BUTTON_CLEAR  				= NULL;
	m_pID_TEXT_MONEY 					= NULL;
	m_pID_EDIT_NAME  					= NULL;
	m_pID_LISTIMG_SUBRESULT			= NULL;
	m_pID_LISTIMG_SOCKETS			= NULL;

	memset(m_arSucceedRate, 0, sizeof(m_arSucceedRate));

	memset(m_arCurrentPage, 0, sizeof(m_arCurrentPage));
	m_pCurrentComposite				= 0;
	m_pCurrentResult					= 0;
	m_nCurrentSucceedRate			= eSUCCEEDRATE_100;

	m_nActionMode						= COMPOSITE_WORKING;
}

UIItemComposite::~UIItemComposite()
{
}

// Frame
BOOL UIItemComposite::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
	
}

BOOL UIItemComposite::OnRender(DWORD /*dwTick*/)
{
	DWORD dwTick(0);
	if(m_itemModel.IsModelCanRender())
	{
		INT x,y;
		m_pID_PICTURE_MODEL->GetPos(x,y,TRUE);
		m_itemModel.SetViewRect	(x
										,y
										,m_pID_PICTURE_MODEL->GetWidth()
										,m_pID_PICTURE_MODEL->GetHeight());

		m_itemModel.Render(dwTick);
	}
	return TRUE;
}


BOOL UIItemComposite::InitControls(VUCtrlFrame*	pFrame)
{
	m_pID_BUTTON_CLOSE	  			= (VUCtrlButton*)  		pFrame->FindControl( UIITEMCOMPOUND_ID_BUTTON_CLOSE );
	m_pID_BUTTON_HELP					= (VUCtrlButton*)  		pFrame->FindControl( UIITEMCOMPOUND_ID_BUTTON_HELP );
	m_pID_BUTTON_CANCEL	 			= (VUCtrlButton*)  		pFrame->FindControl( UIITEMCOMPOUND_ID_BUTTON_CANCEL );
	m_pID_LISTIMG_RESULT				= (VUCtrlListImg*) 		pFrame->FindControl( UIITEMCOMPOUND_ID_LISTIMG_RESULT );
	m_pID_PICTURE_MODEL	 			= (VUCtrlPicture*) 		pFrame->FindControl( UIITEMCOMPOUND_ID_PICTURE_MODEL );
	m_pID_BUTTON_CLEAR	  			= (VUCtrlButton*)  		pFrame->FindControl( UIITEMCOMPOUND_ID_BUTTON_CLEAR );
	m_pID_TEXT_MONEY	 				= (VUCtrlText*) 			pFrame->FindControl( UIITEMCOMPOUND_ID_TEXT_MONEY );
	m_pID_EDIT_NAME	  				= (VUCtrlEdit*) 			pFrame->FindControl( UIITEMCOMPOUND_ID_EDIT_NAME );
	m_pID_LISTIMG_SUBRESULT			= (VUCtrlListImg*) 		pFrame->FindControl( UIITEMCOMPOUND_ID_LISTIMG_SUBRESULT );
	m_pID_LISTIMG_MATERIAL	 		= (VUCtrlListImg*) 		pFrame->FindControl( UIITEMCOMPOUND_ID_LISTIMG_MATERIAL );

	for(UINT n=0; n<eSUCCEEDRATE_NUM; n++)
	{
		m_arSucceedRate[n]	  	= (VUCtrlCheckBox*)		pFrame->FindControl( FMSTR(UIITEMCOMPOUND_ID_CHECKBOX_RATE,n) );
		if(m_arSucceedRate[n] == NULL)
			break;
		assert( m_arSucceedRate[n] );
		//m_arSucceedRate[n]		->SetProcOnCheck 	( theUIItemCompoundID_CHECKBOX_RATEOnCheckBoxCheck );
		m_arSucceedRate[n]		->SetCheck		(n == m_nCurrentSucceedRate);
		m_arSucceedRate[n]		->SetGroupID		(SUCCEEDRATE_GROUP);
		m_arSucceedRate[n]		->SetIndexAtGroup	(n);
		//m_arSucceedRate[n]		->SetEnable	(FALSE);
	}

	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_BUTTON_HELP );
	assert( m_pID_BUTTON_CANCEL );
	assert( m_pID_LISTIMG_RESULT );
	assert( m_pID_PICTURE_MODEL );
	//assert( m_pID_LISTIMG_MATERIAL );
	assert( m_pID_BUTTON_CLEAR );
	assert( m_pID_TEXT_MONEY );
	assert( m_pID_EDIT_NAME );
	//assert( m_pID_LISTIMG_SUBRESULT );




	/////////////////////////////////////////////
	if(m_pID_LISTIMG_SUBRESULT)
		m_pID_LISTIMG_SUBRESULT->SetCanPicking(FALSE);
	m_pID_LISTIMG_RESULT->SetCanPicking(FALSE);

	if(m_pID_LISTIMG_MATERIAL)
		m_pID_LISTIMG_MATERIAL->SetCanPicking(FALSE);

	/////////////////////////////////////////////
	m_itemModel.Init(10);

	INT x,y;
	m_pID_PICTURE_MODEL->GetPos(x,y);
	m_itemModel.SetViewRect	(x
                           ,y
									,m_pID_PICTURE_MODEL->GetWidth()
									,m_pID_PICTURE_MODEL->GetHeight());

	return TRUE;
}


////////////////////////////////////////////////////
//ID_PICTURE_MODEL::OnButtonClick
BOOL UIItemComposite::ID_PICTURE_MODELOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	//ItemCompoundDialog* pDialog = (ItemCompoundDialog*)theUIItemCompositeSlotUIListener.GetContainer();
	//pDialog->CloseWindow();
	m_pID_LISTIMG_RESULT->ForceDropTo(0);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UIItemComposite::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	//ItemCompoundDialog* pDialog = (ItemCompoundDialog*)theUIItemCompositeSlotUIListener.GetContainer();
	//pDialog->CloseWindow();
	theItemCompoundDialog.CloseWindow();
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_HELP::OnButtonClick
BOOL UIItemComposite::ID_BUTTON_HELPOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}




////////////////////////////////////////////////////
//ID_BUTTON_CANCEL::OnButtonClick
BOOL UIItemComposite::ID_BUTTON_CANCELOnButtonClick(VUCtrlObject* pSender)
{
	ID_BUTTON_CLOSEOnButtonClick(pSender);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconDropTo
BOOL UIItemComposite::ID_LISTIMG_RESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconLDBClick
BOOL UIItemComposite::ID_LISTIMG_RESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconRButtonUp
BOOL UIItemComposite::ID_LISTIMG_RESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}


////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconDropTo
BOOL UIItemComposite::ID_LISTIMG_MATERIALOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconLDBClick
BOOL UIItemComposite::ID_LISTIMG_MATERIALOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconRButtonUp
BOOL UIItemComposite::ID_LISTIMG_MATERIALOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}



////////////////////////////////////////////////////
//ID_BUTTON_CLEAR::OnButtonClick
BOOL UIItemComposite::ID_BUTTON_CLEAROnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}


////////////////////////////////////////////////////
//ID_CHECKBOX_RATE::OnCheckBoxCheck
void UIItemComposite::ID_CHECKBOX_RATEOnCheckBoxCheck(VUCtrlObject* pSender, BOOL*						/*pbChecked*/)
{
	if(pSender->GetGroupID() == SUCCEEDRATE_GROUP)
	{
		ChangeSucceedRate(pSender->GetIndexAtGroup());
	}
}



////////////////////////////////////////////////////
//ID_EDIT_NAME::OnEditEnter
void UIItemComposite::ID_EDIT_NAMEOnEditEnter(VUCtrlObject* /*pSender*/, LPCSTR 				/*szData*/ )
{
	
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconDropTo
BOOL UIItemComposite::ID_LISTIMG_SUBRESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconLDBClick
BOOL UIItemComposite::ID_LISTIMG_SUBRESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconRButtonUp
BOOL UIItemComposite::ID_LISTIMG_SUBRESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

void UIItemComposite::ReloadMesh(DWORD dwModelID)
{
	m_itemModel.ReloadMesh(dwModelID);
	INT x,y;
	m_pID_PICTURE_MODEL->GetPos(x,y,TRUE);
	m_itemModel.SetViewRect	(x
                           ,y
									,m_pID_PICTURE_MODEL->GetWidth()
									,m_pID_PICTURE_MODEL->GetHeight());
}


void UIItemComposite::OnClearTarget()
{
	theIconMouseTip.SetLockKey(TIP_LOCKEKY,0);
	theIconMouseTip.SetVisible(FALSE);
	RefreshComposite();
}

void UIItemComposite::RefreshComposite()
{
	MONEY moneyCost;

	m_pID_TEXT_MONEY			->Clear();
	*m_pID_EDIT_NAME			= "";
	m_itemModel.ReloadMesh(INVALID_DWORD_ID);

	if(m_pCurrentComposite == NULL)
		return;

	///填充原料清单
	ChangeSucceedRate(m_nCurrentSucceedRate, TRUE);

	///手续费

	moneyCost				= theItemCompoundDialog.GetRequiredMoney();
	*m_pID_TEXT_MONEY		= FMSTR("%I64u",moneyCost);

	if(m_pID_LISTIMG_SUBRESULT)
	{
		//////////////////////////////////////////////////////
		///获得生成结果信息
		m_pCurrentResult	= theItemCompositeParser.GetCompositeResultInfo((SLOTCODE)m_pCurrentComposite->m_ResultCode);
		//__VERIFY2_PTR(m_pCurrentResult, "GetCompositeResultInfo Is NULL\n", );

		if(m_pCurrentResult)
		{
			if(m_pCurrentResult->m_MainResult != 0)
			{
				///填充副结果
				sVITEMINFO_BASE*	pVSubItem;

				for(INT nSub=0; nSub < sITEMCOMPOSITERESULT::MAX_COMPOSITE_RESULT_NUM; nSub++)
				{
					pVSubItem = theItemInfoParser.GetVItemInfoByItem(m_pCurrentResult->m_ResultItemCode[nSub]);
					__VERIFY2_PTR(pVSubItem, "GetCompositeResultInfo Sub Result Is NULL\n", );

					IconDragListImg	subResult;
					subResult.SetData	(m_pCurrentResult->m_ResultItemCode[nSub]
											,1 
											,pVSubItem->GetIconType()
											);
					m_pID_LISTIMG_SUBRESULT->SetItem(&subResult,nSub);
				}
			}
		}
	}
}


void UIItemComposite::FillNeedMaterials()
{
	__CHECK2_PTR(m_pCurrentComposite,;);

	assert(m_pID_LISTIMG_MATERIAL);
	m_pID_LISTIMG_MATERIAL->Clear();
	//BOOL	bRet = FALSE;

	sVITEMINFO_BASE* pVItem;
	int i = 0;
	for (; i < m_pCurrentComposite->m_byMatCount; ++i)
	{
		CODETYPE		ItemCode			= m_pCurrentComposite->m_sCompositeMaterials[i].m_dwCompositeMaterialCode;
		int			iMaterialCount = m_pCurrentComposite->m_sCompositeMaterials[i].m_bySpendMaterialNum; 

		pVItem = theItemInfoParser.GetVItemInfoByItem(ItemCode);
		if(pVItem == NULL)
			continue;

		IconDragListImg	materialItem;
		materialItem.SetData	(ItemCode
                           ,iMaterialCount
									,pVItem->GetIconType());
		materialItem.SetDeactivate(TRUE);
		m_pID_LISTIMG_MATERIAL->InsertItem(&materialItem,i);
	}
}

void UIItemComposite::OnItemCompoundFinished(MSG_OBJECT_BASE * /*pRecvPacket*/)
{
	m_nActionMode = COMPOSITE_NEXT;
}


void UIItemComposite::ChangeSucceedRate(UINT nRate,BOOL bForceUpdate)
{
	eSUCCEEDRATE	minRate;
	UINT				n;

	minRate = theItemCompoundDialog.GetMinSucceedRate();
	//assert(nRate >= minRate);
	if(nRate < (UINT)minRate)
		nRate = minRate;

	if(m_arSucceedRate[0])
	{
		for(n=0; n<(UINT)minRate; n++)
		{
			m_arSucceedRate[n]->SetVisible(FALSE);
		}

		for(n=minRate; n<eSUCCEEDRATE_NUM; n++)
		{
			m_arSucceedRate[n]->SetVisible(TRUE);
			m_arSucceedRate[n]->SetCheck(n == nRate);
		}
	}

	if(nRate == m_nCurrentSucceedRate && !bForceUpdate)
		return;

	m_nCurrentSucceedRate = nRate;
	//FillNeedMaterials();

	theItemCompoundDialog.SetSucceedType((eSUCCEEDRATE)nRate);
	theItemCompoundDialog.ReTry(m_pCurrentComposite->m_Code);

	MONEY moneyCost;

	moneyCost = theItemCompoundDialog.GetRequiredMoney();

	LPCSTR szMoney = FMSTR("%I64u",moneyCost);
	///检测金钱需量
	if (singleton::ExistHero() && theHero.GetMoney() >= moneyCost )
	{
		m_pID_TEXT_MONEY->SetText	(szMoney,0xFFFFFFFF);
	}
	else
	{
		m_pID_TEXT_MONEY->SetText	(szMoney,0xFF0000FF);
	}
}

void UIItemComposite::SetMoney(MONEY money)
{
	if( m_pID_TEXT_MONEY )
		*m_pID_TEXT_MONEY = (DWORD)money;
}

// 设置是否可视
void UIItemComposite::SetVisible( const BOOL bVisible )
{
	if(bVisible)
	{
		if(theUIItemCompositeSlotUIListener.GetItemComposite() != this)
		{
			//theUIItemCompositeSlotUIListener.SetItemComposite(this);
			AttachToItemCompound();
			OnInstallContainer(theUIItemCompositeSlotUIListener.GetContainer() );
		}
	}
	else
	{
		m_nActionMode		= COMPOSITE_WORKING;
	}
}

void UIItemComposite::ChangeTargetItem		(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
	if(m_nActionMode == COMPOSITE_WORKING)
	{
		RefreshComposite();
	}
	///更新到
	//m_pCurrentComposite	= theItemCompositeParser.;
}

void UIItemComposite::ChangeSubResultItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}

void UIItemComposite::ChangeMaterialItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}

void UIItemComposite::ChangeSocketItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}


void UIItemComposite::AttachToItemCompound()
{
	theUIItemCompositeSlotUIListener				.SetItemComposite(this);
	theUIItemCompositeSubResultSlotUIListener	.SetItemComposite(this);
	theUIItemCompositeTargetSlotUIListener		.SetItemComposite(this);
	theUIItemCompositeSocketSlotUIListener		.SetItemComposite(this);

	theHero.InstallSlotListener(SI_ENCHANT,					&theUIItemCompositeSlotUIListener);
	theHero.InstallSlotListener(SI_ENCHANT_SUBRESULT,	&theUIItemCompositeSubResultSlotUIListener);
	theHero.InstallSlotListener(SI_ENCHANT_TARGET,		&theUIItemCompositeTargetSlotUIListener);
	theHero.InstallSlotListener(SI_ENCHANT_SOCKET,		&theUIItemCompositeSocketSlotUIListener);
}


void UIItemComposite::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		int slotType = GetLocalIndex(pContainer->GetSlotIndex());
		if(m_arContainers[slotType])
		{
			m_arContainers[slotType]->SetSlotContainer(pContainer);
		}
	}
}

void UIItemComposite::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	int nSlotType = GetLocalIndex(pSlot->GetSlotIndex());

	if(m_arContainers[nSlotType] == NULL)
		return;

	UIContainHelper	helper(m_arContainers[nSlotType]
									,m_pPlayer
									,gs_arSlotNumPerPage[nSlotType]
									,m_arCurrentPage[nSlotType]
									,this);
	helper.PushItem(*pSlot);


}


void UIItemComposite::OnItemSlot(BaseSlot& slot, BOOL bAdd)
{
	int nSlotType = GetLocalIndex(slot.GetSlotIndex());
	(this->*gs_arCallbacks[nSlotType])(&slot, bAdd);
}

void UIItemComposite::PopGoods( BaseSlot* pSlot )
{
	int nSlotType = GetLocalIndex(pSlot->GetSlotIndex());

	if(m_arContainers[nSlotType] == NULL)
		return;

	UIContainHelper	helper(m_arContainers[nSlotType]
									,m_pPlayer
									,gs_arSlotNumPerPage[nSlotType]
									,m_arCurrentPage[nSlotType]
									,this);
	helper.PopItem(*pSlot);



}


void UIItemComposite::SetGoodsState( BaseSlot* pSlot,DWORD dwState )
{
	int nSlotType = GetLocalIndex(pSlot->GetSlotIndex());

	if(m_arContainers[nSlotType] == NULL)
		return;

	UIContainHelper	helper(m_arContainers[nSlotType]
									,m_pPlayer
									,gs_arSlotNumPerPage[nSlotType]
									,m_arCurrentPage[nSlotType]
									,this);
	helper.SetReadOnlyState(*pSlot, dwState);


}

VUCtrlIconDrag* UIItemComposite::GetMaterialDragCtrl(SLOTPOS /*atPos*/)
{
	return m_pID_LISTIMG_MATERIAL;
}


};//namespace gameui


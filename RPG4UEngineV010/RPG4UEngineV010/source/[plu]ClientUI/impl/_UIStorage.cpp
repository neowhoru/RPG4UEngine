/*////////////////////////////////////////////////////////////////////////
文 件 名：UIStorage.cpp
创建日期：2007年12月15日
最后更新：2007年12月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIStorage.h"
#include "FuncProfiler.h"

#include "TextResManager.h"




#include "UIpack.h"
#include "UIInputBox.h"
#include "UIMessageBox.h"
#include "UIChatInput.h"
#include "VUCtrlList.h"
#include "VUICursorManager.h"
#include "NpcCoordinateManager.h"
#include "StorageUIListener.h"
#include "VHero.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::StorageUIListener& GetUIStorageStorageUIListener()
{
	static gameui::StorageUIListener staticStorageUIListener;
	return staticStorageUIListener;
}



//-----------仓库收费参数设置------------------------------------
#define StorageRulePara_C  0.08
#define StorageRulePara_D  0.05
//-----------------------------------------------------------
//extern GameClientApplication* theApp;

#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(CUI_ID_FRAME_Storage, ()  , gamemain::eInstPrioClientUI/*CUI_ID_FRAME_Storage*/);
#else
#endif

namespace uicallback
{
	MAP_FRAME_RUN( s_CUI_ID_FRAME_Storage, OnFrameRun )
		MAP_FRAME_RENDER( s_CUI_ID_FRAME_Storage, OnFrameRender )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Storage, ID_BUTTON_CloseOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Storage, ID_BUTTON_WhyOnButtonClick )
		MAP_LIST_SELECT_CHANGE_CALLBACK( s_CUI_ID_FRAME_Storage, ID_LIST_cityOnListSelectChange )
		MAP_LIST_SELECT_CHANGE_CALLBACK( s_CUI_ID_FRAME_Storage, ID_LIST_priceOnListSelectChange )
		MAP_ICON_DROP_TO_CALLBACK( s_CUI_ID_FRAME_Storage, ID_LISTIMG_MainOnIconDropTo )
		MAP_ICON_DRAG_OFF_CALLBACK( s_CUI_ID_FRAME_Storage, ID_LISTIMG_MainOnIconDragOff )
		MAP_ICON_LDB_CLICK_CALLBACK( s_CUI_ID_FRAME_Storage, ID_LISTIMG_MainOnIconLDBClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Storage, ID_BUTTON_LOCKOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Storage, ID_BUTTON_UNLOCKOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Storage, ID_BUTTON_ClearPwdOnButtonClick )
//MAP_ICON_RBUTTON_UP_CALLBACK( s_CUI_ID_FRAME_Storage, ID_LISTIMG_MainOnIconRDBClick )
};//namespace uicallback
using namespace uicallback;


CUI_ID_FRAME_Storage::CUI_ID_FRAME_Storage()
{
	
	// Member
	m_pID_FRAME_Storage = NULL;
	m_pID_BUTTON_Close = NULL;
	m_pID_BUTTON_Why = NULL;
	m_pID_LIST_city = NULL;
	m_pID_LIST_price = NULL;
	m_pID_PICTURE_23489 = NULL;									 
	m_pID_LISTIMG_Main = NULL;
	m_pID_PICTURE_19060 = NULL;
	m_pID_PICTURE_2908 = NULL;
	m_pID_PICTURE_2909 = NULL;
	m_pID_BUTTON_LOCK = NULL;
	m_pID_BUTTON_UNLOCK = NULL;
	m_pID_BUTTON_ClearPwd = NULL;
	m_bInUIFrameLock = FALSE;
	m_bInUIFrameUnLock = FALSE;
	strcpy(szItem_PrePWD,"");
	strcpy(szItem_LastPWD,"");
	strcpy(szItem_PWD,"");
	m_ustDstIndex = 0;
	m_nNpcSysId = -1;
	m_bQuestNpc = FALSE;
	
}
struct S_DstSrcIndex
{
	unsigned int nDstIndex;
	unsigned int nSrcIndex;
};
// Frame
BOOL CUI_ID_FRAME_Storage::OnFrameRun(DWORD /*dwTick*/)
{
	
	if ( m_nNpcSysId < 0 )
		return FALSE;
	//获得玩家自己的坐标
	VCharacter* pMe = NULL;
	//pMe = singleton::GetPlayerManager().GetMe();
	//if ( !pMe )
	//	return FALSE;
	float fMeX,fMeY,fMeZ;
	theVHero.GetPos( &fMeX, &fMeY, &fMeZ );
	//获得NPC的坐标
	VCharacter* pNpc = singleton::GetPlayerManager().FindByID( m_nNpcSysId );
	if ( !pNpc )
	{
		if(s_CUI_ID_FRAME_Storage.IsVisible())
		{
			s_CUI_ID_FRAME_Storage.SetVisible(FALSE);
			GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
			m_bQuestNpc = FALSE;
		}
		return FALSE;
	}	
	float fNpcX, fNpcY, fNpcZ;
	pNpc->GetPos( &fNpcX, &fNpcY, &fNpcZ );
	float x = fNpcX - fMeX;
	float y = fNpcY - fMeY;
	//计算玩家与NPC之间的距离
	float dist = sqrtf( x*x + y*y );
	//如果玩家和NPC的距离大于8.0f就关闭仓库和包裹交易窗口,并把光标设置为原先的状态
	if( dist > 8.0f && m_bQuestNpc )
	{
		if(s_CUI_ID_FRAME_Storage.IsVisible())
		{
			s_CUI_ID_FRAME_Storage.SetVisible(FALSE);
			GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
			m_bQuestNpc = FALSE;
		}
	}
	return TRUE;
	
}
BOOL CUI_ID_FRAME_Storage::OnFrameRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// Button
BOOL CUI_ID_FRAME_Storage::ID_BUTTON_CloseOnButtonClick( VUCtrlObject* pSender )
{
				
	//光标置回原来的状态
	GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
	SetVisible(FALSE);
	theUIPack.SetVisible(FALSE);
	return TRUE;
	
}
// Button
BOOL CUI_ID_FRAME_Storage::ID_BUTTON_WhyOnButtonClick( VUCtrlObject* pSender )
{
	
	return TRUE;
	
}
// List
void CUI_ID_FRAME_Storage::ID_LIST_cityOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem )
{
}
// List
void CUI_ID_FRAME_Storage::ID_LIST_priceOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem )
{
}
BOOL PackItemToStorage_DefCallBackFun( const char* szInputData, void *pData )
{
	
	S_DstSrcIndex *pDstSrcIndex = (S_DstSrcIndex*)pData;
	int nGoodsCount = atoi( szInputData );
	if ( nGoodsCount <= 0 )
		return FALSE;
	if(nGoodsCount > s_CUI_ID_FRAME_Storage.m_dwChangeCount)
	{
		theUIMessageBox.Show( theTextResManager.GetString(eText_OverCount) );
		return FALSE;
	}
	//最大存储限制99个
	if(nGoodsCount>ggdat::STORAGE_ITEMMAX_IN_GRIDS)
	{
		nGoodsCount = ggdat::STORAGE_ITEMMAX_IN_GRIDS;
		char szInfo[128] = {0};
		//物品最大存储数目:  99 
		sprintf(szInfo, "%s %d", theTextResManager.GetString( eText_ItemMaxStoreNum ) , 
			nGoodsCount);
		theUIMessageBox.Show( szInfo );
	}

	MsgMoveStorageItem msg;
	msg.enMoveType = MsgMoveStorageItem::Type_PackToStorage;
	msg.ustDstIndex = pDstSrcIndex->nDstIndex;
	msg.ustSrcIndex = pDstSrcIndex->nSrcIndex;
	msg.ustCount = nGoodsCount;
	msg.nNpcID = GetPlayerRole().m_nNpcID;
	GetNetworkInput().SendMsg( &msg );

	ggdat::SCharItem stPackItem;
	ggdat::SCharStorageData* pStorageItem = NULL; 

	pStorageItem = &GetPlayerRole().m_stStorage;

	GetPlayerRole().m_bag.GetItem( pDstSrcIndex->nSrcIndex, &stPackItem );

	stPackItem.itembaseinfo.ustItemCount = nGoodsCount;
	stPackItem.storageinfo.nStorageNpcId = GetPlayerRole().m_nNpcID; 
	//将物品标志设为 仓库1
	stPackItem.storageinfo.nWhoIs = 1;

	pStorageItem->stStorageItems[pDstSrcIndex->nDstIndex] = stPackItem;
	GetPlayerRole().m_bag.ClearItem(pDstSrcIndex->nSrcIndex,nGoodsCount);

	//通过npc的id 得到 npc 名字 
	const char* szName = GetNpcCoordinateManager().GetNpcName(GetPlayerRole().m_nNpcID);
	if(szName)
	{
		strcpy(GetPlayerRole().CityName[pDstSrcIndex->nDstIndex], szName);
	}

	s_CUI_ID_FRAME_Storage.Refresh();	
	theUIPack.Refresh();
	return TRUE;
	
}
//
BOOL PackToStorage_DefCallBackFun( BOOL bPressYesButton, void *pData )
{
	
	if ( bPressYesButton == TRUE )
	{
		PackItemToStorage_DefCallBackFun( "1", pData );
	}
	return TRUE;
	
}
// ListImg / ListEx
BOOL CUI_ID_FRAME_Storage::ID_LISTIMG_MainOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe,
													   IconDragListImg* pItemDrag,
													   IconDragListImg* pItemDest )
{
	
	if ( pItemDrag->IsNull() /*|| !pItemDest->IsNull() */)
	{
		return FALSE;
	}
	unsigned int nDstIndex = m_pID_LISTIMG_Main->GetItemIndex( pItemDest );
	unsigned int nSrcIndex = 0;
	// Me
	if ( pSender == pMe )
	{

		nSrcIndex = m_pID_LISTIMG_Main->GetItemIndex( pItemDrag );
		//
		ggdat::SCharStorageData* pStorageItem = NULL;
		pStorageItem = &GetPlayerRole().m_stStorage;

		//同一物品
		if((pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemID == pStorageItem->stStorageItems[nDstIndex].itembaseinfo.ustItemID)&&
			(pStorageItem->stStorageItems[nDstIndex].itembaseinfo.ustItemID!=ErrorUnsignedShortID)
			&& (pStorageItem->stStorageItems[nSrcIndex].storageinfo.bIsLocked 
			== pStorageItem->stStorageItems[nDstIndex].storageinfo.bIsLocked)
			)
		{
			giteminfo::SItemWeapon *pItemWeapon = NULL;
			pItemWeapon	= (giteminfo::SItemWeapon *)GetGameItemDetail().GetItemByID(pStorageItem->stStorageItems[nDstIndex].itembaseinfo.ustItemID);
			if(!pItemWeapon)
				return FALSE;
			if(pItemWeapon->IsExclusive())
			{
				return FALSE;
			}
			else
			{
				//同一城市
				if(pStorageItem->stStorageItems[nDstIndex].storageinfo.nStorageNpcId 
					== pStorageItem->stStorageItems[nSrcIndex].storageinfo.nStorageNpcId)
				{
					int nTempCount = pStorageItem->stStorageItems[nDstIndex].itembaseinfo.ustItemCount + pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemCount;
					if(nTempCount <= ggdat::STORAGE_ITEMMAX_IN_GRIDS)
					{
						MsgMoveStorageItem msg;
						msg.enMoveType = MsgMoveStorageItem::Type_StorageToStorage; 	
						msg.ustDstIndex = nDstIndex;
						msg.ustSrcIndex = nSrcIndex;
						msg.ustCount = pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemCount;
						GetNetworkInput().SendMsg( &msg );

						pStorageItem->stStorageItems[nDstIndex].itembaseinfo.ustItemCount += pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemCount;
						pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemID = ErrorUnsignedShortID;	
						//价格清空
						GetPlayerRole().fCostOfFetchItem[nSrcIndex] = 0;
						//城市名字清空
						strcpy_s( GetPlayerRole().CityName[nSrcIndex], MsgUpdateStorage::NPCSTRING_LEGTH, 
							"" );
						s_CUI_ID_FRAME_Storage.Refresh();
					}
					else
					{
						return FALSE;
					}
				}
				else
				{
					return FALSE;
				}

			}

		}
		//不同物品
		else if(pStorageItem->stStorageItems[nDstIndex].itembaseinfo.ustItemID == ErrorUnsignedShortID)
		{
			MsgMoveStorageItem msg;
			msg.enMoveType = MsgMoveStorageItem::Type_StorageToStorage; 	
			msg.ustDstIndex = nDstIndex;
			msg.ustSrcIndex = nSrcIndex;
			msg.ustCount = pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemCount;
			GetNetworkInput().SendMsg( &msg );

			//物品付给
			pStorageItem->stStorageItems[nDstIndex] = pStorageItem->stStorageItems[nSrcIndex];
			//价格付给
			GetPlayerRole().fCostOfFetchItem[nDstIndex] = GetPlayerRole().fCostOfFetchItem[nSrcIndex];
			//城市付给
			strcpy(GetPlayerRole().CityName[nDstIndex],GetPlayerRole().CityName[nSrcIndex]);
			GetPlayerRole().fCostOfFetchItem[nSrcIndex] = 0;
			strcpy(GetPlayerRole().CityName[nSrcIndex],"");
			pStorageItem->stStorageItems[nSrcIndex].itembaseinfo.ustItemID = ErrorUnsignedShortID;	
			s_CUI_ID_FRAME_Storage.Refresh();
		}
		else if((pItemDest->GetIconInfo()->Id() != pItemDrag->GetIconInfo()->Id())||
				(pStorageItem->stStorageItems[nSrcIndex].storageinfo.bIsLocked 
				!= pStorageItem->stStorageItems[nDstIndex].storageinfo.bIsLocked))//物品交换
		{
			//发送消息给服务端
			MSG_CHANGEITEMPOS_REQStruct msg;
			msg.ustDstIndex = nDstIndex;
			msg.ustSrcIndex = nSrcIndex;
			msg.nWhichIs = 1;
			GetNetworkInput().SendMsg( &msg );
			//---客户端处理
			//---取得原先物品的信息 与 城市名称信息
			ggdat::SCharItem stItemDst; 
			ggdat::SCharItem stItemSrc; 

			char szCityInfoDst[128];
			char szCityInfoSrc[128];

			stItemDst = pStorageItem->stStorageItems[nDstIndex];
			stItemSrc = pStorageItem->stStorageItems[nSrcIndex];

			strcpy(szCityInfoDst,GetPlayerRole().CityName[nDstIndex]);
			strcpy(szCityInfoSrc,GetPlayerRole().CityName[nSrcIndex]);
			//---清空原先的物品信息与城市信息

			pStorageItem->stStorageItems[nDstIndex] = stItemSrc;
			pStorageItem->stStorageItems[nSrcIndex] = stItemDst;

			strcpy(GetPlayerRole().CityName[nDstIndex],szCityInfoSrc);
			strcpy(GetPlayerRole().CityName[nSrcIndex],szCityInfoDst);
			//---交换

			s_CUI_ID_FRAME_Storage.Refresh();
			return TRUE;
		}
	}
	// Pack @PS: this is only way to push item into storage
	else if ( pSender == theUIPack.m_pID_LISTIMG_PACK )
	{
		nSrcIndex = theUIPack.m_pID_LISTIMG_PACK->GetItemIndex( pItemDrag );

		S_DstSrcIndex stDstSrcIndex;
		stDstSrcIndex.nDstIndex = nDstIndex;
		stDstSrcIndex.nSrcIndex = nSrcIndex;
		//-------
		if (nDstIndex>= ggdat::STORAGE_Grigs_MAX ||
			nSrcIndex>= ggdat::ITEM_BAGMAX )
			return FALSE;

		int nStorageEmptyIndex = -1;
		if(pItemDest == NULL)
		{
			nStorageEmptyIndex = m_pID_LISTIMG_Main->GetNullItem();
			if(nStorageEmptyIndex == -1)
			{//说明没有 空位了
				theUIMessageBox.Show( theTextResManager.GetString(eText_ItemNoPlace) );
			}
		}
		else
		{
			if ( !pItemDest->IsNull() )
				return FALSE;
			nStorageEmptyIndex = m_pID_LISTIMG_Main->GetItemIndex( pItemDest );
		}

		giteminfo::SItemCommon *pItem = GetGameItemDetail().GetItemByID(pItemDrag->GetIconInfo()->Id());
		if ( !pItem )
			return FALSE;
		// 物品是否可以放入仓库
		if ( !pItem->bIsCanPushInStorage )
		{
			theUIMessageBox.Show( theTextResManager.GetString(eText_CanNotPushItemIntoStorage) );
			return FALSE;
		}
		m_dwChangeCount	= pItemDrag->GetIconInfo()->Count();

		if( pItem->IsExclusive() )
		{
			theUIMessageBox.Show( theTextResManager.GetString(eText_Confirm), "",
				MB_YESNO, TRUE, PackToStorage_DefCallBackFun,
				&stDstSrcIndex, sizeof(stDstSrcIndex) );
		}
		else
		{
			//请输入要购买的物品数量
			theUIInputBox.Show( theTextResManager.GetString(eText_Input_SaveCnt),
				TRUE, TRUE, PackItemToStorage_DefCallBackFun,
				&stDstSrcIndex, sizeof(stDstSrcIndex) );
			theUIInputBox.m_pID_EDIT_INPUT->SetText( m_dwChangeCount );
		}
	}
	return TRUE;
	
}
BOOL CUI_ID_FRAME_Storage::ID_LISTIMG_MainOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pMe,
														IconDragListImg* pItem )
{
	return FALSE;
}
BOOL CUI_ID_FRAME_Storage::ID_LISTIMG_MainOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem )
{
	return FALSE;
}
//BOOL CUI_ID_FRAME_Storage::ID_LISTIMG_MainOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
//{
//	return FALSE;
//}
// Button
BOOL CUI_ID_FRAME_Storage::ID_BUTTON_LOCKOnButtonClick( VUCtrlObject* pSender )
{
	
	//物品加锁 鼠标标志切换，相应开关打开
	if(m_bInUIFrameLock)
	{
		GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
		m_bInUIFrameLock = FALSE;
		if(s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock)
		{
			s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock = FALSE;
		}
	}
	else
	{
		GetVUICursorManager().SetUICursor( cursor::Type_LockItem );
		m_bInUIFrameLock = TRUE;
		if(s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock)
		{
			s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock = FALSE;
		}	
	}
	return TRUE;
	
}
// Button
BOOL CUI_ID_FRAME_Storage::ID_BUTTON_UNLOCKOnButtonClick( VUCtrlObject* pSender )
{
	
	//物品解锁 鼠标标志切换，相应开关打开
	if(m_bInUIFrameUnLock)
	{
		GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
		m_bInUIFrameUnLock = FALSE;
		if(s_CUI_ID_FRAME_Storage.m_bInUIFrameLock)
		{
			s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		}
	}
	else
	{
		GetVUICursorManager().SetUICursor( cursor::Type_UnLockItem );
		s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock = TRUE;
		if(s_CUI_ID_FRAME_Storage.m_bInUIFrameLock)
		{
			s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		}
	}
	return TRUE;
	
}
BOOL AskClearPwdOfItem_DefCallBackFun( BOOL bPressYesButton, void *pData )
{
	
	if ( bPressYesButton == TRUE )
	{
		theUIMessageBox.Show( theTextResManager.GetString(eText_Succ_Of_App_ClearPwd));
		GetPlayerRole().bIfApp_ClearPwd = TRUE;
		//发送消息给服务端，开通清除密码开关
		MsgApplyClearPwd msg;	
		GetNetworkInput().SendMsg( &msg );
	}
	return TRUE;
	
}
BOOL CUI_ID_FRAME_Storage::ID_BUTTON_ClearPwdOnButtonClick( VUCtrlObject* pSender )
{
	
	//根据玩家身上的信息知道，玩家是否申请该功能
	int nHaveSetPwd = strcmp(GetPlayerRole().szItem_PWD,"");
	if(nHaveSetPwd != 0)
	{
		if(GetPlayerRole().bIfApp_ClearPwd)
		{
			theUIMessageBox.Show( theTextResManager.GetString(eText_YouHaveAlreadyApplied));
			return FALSE;
		}
		else
		{
			theUIMessageBox.Show( theTextResManager.GetString(eText_IfAskClearPwd), "",
				MB_YESNO, TRUE, AskClearPwdOfItem_DefCallBackFun,
				NULL, 0 );
		}
	
	}
	return TRUE;
	
}
// 装载UI
BOOL CUI_ID_FRAME_Storage::LoadUI()
{
	GetUIStorageStorageUIListener().RegisterMe();

	DWORD dwResult = GetVUCtrlManager().AddFrame( UIDOC_PATH( "Storage") );
	if ( dwResult == 0 )
	{
		MESSAGE_BOX("读取文件[UI\\Storage.UI]失败")
			return FALSE;
	}
	else if ( dwResult != 140262/*文件版本号*/ )
	{
		MESSAGE_BOX("读取文件[UI\\Storage.UI]与源代码版本不一样")
	}
	return InitControls();
}
BOOL CheckPwd(const char* szInputData, void *pData)
{
	
	sprintf(s_CUI_ID_FRAME_Storage.szItem_LastPWD, "%s", szInputData );
	if(strcmp(s_CUI_ID_FRAME_Storage.szItem_LastPWD,"") == 0)
	{
		theUIMessageBox.Show(  theTextResManager.GetString(eText_PwdNull) );
		s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		theUIPack.m_bInUIFrameLock = FALSE;
		return FALSE;
	}
	//光标置回原来的状态
	GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
	//判别两个字符串是否相等
	int nCompare = strcmp(s_CUI_ID_FRAME_Storage.szItem_PrePWD,s_CUI_ID_FRAME_Storage.szItem_LastPWD);
	if(nCompare == 0)
	{
		GetPlayerRole().m_stStorage.stStorageItems[s_CUI_ID_FRAME_Storage.m_ustDstIndex].storageinfo.bIsLocked = TRUE;
		strcpy(GetPlayerRole().szItem_PWD,s_CUI_ID_FRAME_Storage.szItem_PrePWD); 
		//发送消息给服务端保存密码保存到这个结构中 ggdat::SCharOtherDataBlob
		MsgLockItem msg;
		msg.ustDstIndex = s_CUI_ID_FRAME_Storage.m_ustDstIndex;
		strcpy(msg.szItem_PWD,s_CUI_ID_FRAME_Storage.szItem_PrePWD); 
		GetNetworkInput().SendMsg( &msg );
		s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		theUIPack.m_bInUIFrameLock =FALSE;
		s_CUI_ID_FRAME_Storage.Refresh();
	}
	else
	{
		//两次输入的密码不一样！
		theUIMessageBox.Show(  theTextResManager.GetString(eText_InputPwdNotTheSame) );
		s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		theUIPack.m_bInUIFrameLock = FALSE;
		return FALSE;
	}
	return TRUE;
	
}
BOOL ItemLock(const char* szInputData, void *pData)
{
		
	sprintf(s_CUI_ID_FRAME_Storage.szItem_PrePWD, "%s", szInputData );
	theUIInputBox.Show( theTextResManager.GetString( eText_InputItemPwdAgain ), FALSE, TRUE,CheckPwd,NULL,0,TRUE);
	return TRUE;
	
}
BOOL ItemUnLock(const char* szInputData, void *pData)
{
	
	ggdat::SCharStorageData* pStorageItem = NULL;
	pStorageItem = &GetPlayerRole().m_stStorage;
	sprintf(s_CUI_ID_FRAME_Storage.szItem_PWD, "%s", szInputData );
	int nCmpPwd = strcmp(s_CUI_ID_FRAME_Storage.szItem_PWD,GetPlayerRole().szItem_PWD);
	//如果密码正确
	if(nCmpPwd == 0 )
	{
		pStorageItem->stStorageItems[s_CUI_ID_FRAME_Storage.m_ustDstIndex].storageinfo.bIsLocked = FALSE;
		//发送消息给服务端设置为unlock状态
		MsgUnLockItem msg;
		msg.ustDstIndex = s_CUI_ID_FRAME_Storage.m_ustDstIndex;
		GetNetworkInput().SendMsg( &msg );
		s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock = FALSE;
		theUIPack.m_bInUIFrameUnLock = FALSE;
		s_CUI_ID_FRAME_Storage.Refresh();
	}
	else
	//如果密码错误
	{
		theUIMessageBox.Show(  theTextResManager.GetString(eText_ItemPwdError) );
		s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock = FALSE;
		theUIPack.m_bInUIFrameUnLock = FALSE;
		return FALSE;		
	}
	return FALSE;
	
}
BOOL ItemLock_DefCallBackFun(BOOL bPressYesButton, void *pData )
{
	
	MsgLockItem *pLIMsg = (MsgLockItem*)pData;
	if ( bPressYesButton == TRUE )
	{
		GetNetworkInput().SendMsg( pLIMsg );
		GetPlayerRole().m_stStorage.stStorageItems[s_CUI_ID_FRAME_Storage.m_ustDstIndex].storageinfo.bIsLocked = TRUE;
		s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		theUIPack.m_bInUIFrameLock = FALSE;
		GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
		s_CUI_ID_FRAME_Storage.Refresh();
		return TRUE;
	}
	else
	{
		s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
		theUIPack.m_bInUIFrameLock = FALSE;
		GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
		s_CUI_ID_FRAME_Storage.Refresh();	
	}
	return FALSE;
	
}
//物品加锁解锁
BOOL IconOnClick( VUCtrlObject* pSender, IconDragListImg* pItem )
{	
	
	unsigned int nWhichItemIndex = s_CUI_ID_FRAME_Storage.m_pID_LISTIMG_Main->GetItemIndex(pItem);
	ggdat::SCharStorageData* pStorageItem = NULL;
	pStorageItem = &GetPlayerRole().m_stStorage;
	//物品加锁
	if(s_CUI_ID_FRAME_Storage.m_bInUIFrameLock||theUIPack.m_bInUIFrameLock)
	{
		if ( pStorageItem->stStorageItems[nWhichItemIndex].itembaseinfo.ustItemID == ErrorUnsignedShortID )
			return FALSE;
	
		giteminfo::SItemCommon *pCheckItem = GetGameItemDetail().GetItemByID(pItem->GetIconInfo()->Id());
		if(!pCheckItem->bIsCanLocked)
		{ 
			theUIMessageBox.Show( theTextResManager.GetString(eText_ItemCannotBeenLocked) );
			return FALSE;	
		}

		if(GetPlayerRole().m_bag.m_bEnables[nWhichItemIndex] == FALSE)
		{
			theUIMessageBox.Show( theTextResManager.GetString(eText_ItemCannotBeenLocked) );
			return FALSE;	
		}

		//if(pStorageItem->stStorageItems[nWhichItemIndex].itembaseinfo.ustItemID)
		//如果物品已经加锁 return
		if(!pStorageItem->stStorageItems[nWhichItemIndex].storageinfo.bIsLocked)
		{
			s_CUI_ID_FRAME_Storage.m_ustDstIndex = nWhichItemIndex;
			//请输入您的物品加密密码
			int nPwdCmp = strcmp(GetPlayerRole().szItem_PWD,"");
			if(nPwdCmp == 0)
			{
				theUIInputBox.Show( theTextResManager.GetString( eText_InputItemPwd ), FALSE, TRUE,ItemLock,NULL,0,TRUE);
			}
			else
			{
				MsgLockItem msg;
				msg.ustDstIndex = s_CUI_ID_FRAME_Storage.m_ustDstIndex;
				strcpy(msg.szItem_PWD,GetPlayerRole().szItem_PWD); 
				theUIMessageBox.Show( theTextResManager.GetString(eText_Confirm), "",
					MB_YESNO, TRUE, ItemLock_DefCallBackFun,&msg, sizeof(msg));
			}
			return TRUE;
		}
		else
		{	
			s_CUI_ID_FRAME_Storage.m_bInUIFrameLock = FALSE;
			theUIPack.m_bInUIFrameLock = FALSE;
			GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
			return FALSE;
		}
	}
	else if(s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock||theUIPack.m_bInUIFrameUnLock)//物品解锁
	{
		//如果物品已经未处于lock状态则 return
		if(pStorageItem->stStorageItems[nWhichItemIndex].storageinfo.bIsLocked)
		{
			s_CUI_ID_FRAME_Storage.m_ustDstIndex = nWhichItemIndex;
			//光标置回原来的状态
			GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
			//请输入您的物品加密密码
			theUIInputBox.Show( theTextResManager.GetString( eText_InputItemPwd ), FALSE, TRUE,ItemUnLock,NULL,0,TRUE);
			return TRUE;	
		}
		else
		{
			s_CUI_ID_FRAME_Storage.m_bInUIFrameUnLock = FALSE;
			theUIPack.m_bInUIFrameUnLock = FALSE;
			GetVUICursorManager().SetUICursor(cursor::Type_Arrow);
			return FALSE;
		}
	}
	else
	{
		return FALSE;

	}
	return FALSE;
	
}

// 关连控件
BOOL CUI_ID_FRAME_Storage::InitControls()
{
	GetVUCtrlManager().OnFrameRun( ID_FRAME_Storage, s_CUI_ID_FRAME_StorageOnFrameRun );
	GetVUCtrlManager().OnFrameRender( ID_FRAME_Storage, s_CUI_ID_FRAME_StorageOnFrameRender );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Storage, ID_BUTTON_Close, s_CUI_ID_FRAME_StorageID_BUTTON_CloseOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Storage, ID_BUTTON_Why, s_CUI_ID_FRAME_StorageID_BUTTON_WhyOnButtonClick );
	GetVUCtrlManager().OnListSelectChange( ID_FRAME_Storage, ID_LIST_city, s_CUI_ID_FRAME_StorageID_LIST_cityOnListSelectChange );
	GetVUCtrlManager().OnListSelectChange( ID_FRAME_Storage, ID_LIST_price, s_CUI_ID_FRAME_StorageID_LIST_priceOnListSelectChange );
	GetVUCtrlManager().OnIconDropTo( ID_FRAME_Storage, ID_LISTIMG_Main, s_CUI_ID_FRAME_StorageID_LISTIMG_MainOnIconDropTo );
//	GetVUCtrlManager().OnIconDragOff( ID_FRAME_Storage, ID_LISTIMG_Main, s_CUI_ID_FRAME_StorageID_LISTIMG_MainOnIconDragOff );
	GetVUCtrlManager().OnIconLDBClick( ID_FRAME_Storage, ID_LISTIMG_Main, s_CUI_ID_FRAME_StorageID_LISTIMG_MainOnIconLDBClick );
//	GetVUCtrlManager().OnIconRDBClick( ID_FRAME_Storage, ID_LISTIMG_Main, s_CUI_ID_FRAME_StorageID_LISTIMG_MainOnIconRDBClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Storage, ID_BUTTON_LOCK, s_CUI_ID_FRAME_StorageID_BUTTON_LOCKOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Storage, ID_BUTTON_UNLOCK, s_CUI_ID_FRAME_StorageID_BUTTON_UNLOCKOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Storage, ID_BUTTON_ClearPwd, s_CUI_ID_FRAME_StorageID_BUTTON_ClearPwdOnButtonClick );

	m_pID_FRAME_Storage = (VUCtrlFrame*)GetVUCtrlManager().FindFrame( ID_FRAME_Storage );
	m_pID_BUTTON_Close = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_BUTTON_Close );
	m_pID_BUTTON_Why = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_BUTTON_Why );
	m_pID_LIST_city = (VUCtrlList*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_LIST_city );
	m_pID_LIST_price = (VUCtrlList*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_LIST_price );
	m_pID_PICTURE_23489 = (VUCtrlPicture*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_PICTURE_23489 );
	m_pID_LISTIMG_Main = (VUCtrlListImg*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_LISTIMG_Main );
	m_pID_PICTURE_19060 = (VUCtrlPicture*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_PICTURE_19060 );
	m_pID_PICTURE_2908 = (VUCtrlPicture*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_PICTURE_2908 );
	m_pID_PICTURE_2909 = (VUCtrlPicture*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_PICTURE_2909 );
	m_pID_BUTTON_LOCK = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_BUTTON_LOCK );
	m_pID_BUTTON_UNLOCK = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_BUTTON_UNLOCK );
	m_pID_BUTTON_ClearPwd = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Storage, ID_BUTTON_ClearPwd );
	assert( m_pID_FRAME_Storage );
	assert( m_pID_BUTTON_Close );
	assert( m_pID_BUTTON_Why );
	assert( m_pID_LIST_city );
	assert( m_pID_LIST_price );
	assert( m_pID_PICTURE_23489 );
	assert( m_pID_LISTIMG_Main );
	assert( m_pID_PICTURE_19060 );
	assert( m_pID_PICTURE_2908 );
	assert( m_pID_PICTURE_2909 );
	assert( m_pID_BUTTON_LOCK );
	assert( m_pID_BUTTON_UNLOCK );
	assert( m_pID_BUTTON_ClearPwd );
	m_pID_LISTIMG_Main->SetButtonClickFun( IconOnClick );
	m_pID_LIST_city->HaveSelBar(FALSE);
	m_pID_LIST_price->HaveSelBar(FALSE);

	return TRUE;
}
// 卸载UI
BOOL CUI_ID_FRAME_Storage::UnLoadUI()
{
	
	GetUIStorageStorageUIListener().UnregisterMe();
	return GetVUCtrlManager().RemoveFrame( UIDOC_PATH("Data\\UI\\Storage") );
	
}
// 是否可视
BOOL CUI_ID_FRAME_Storage::IsVisible()
{
	
	return m_pID_FRAME_Storage->IsVisible();
	
}
// 设置是否可视
void CUI_ID_FRAME_Storage::SetVisible( BOOL bVisible )
{
	
	if ( m_pID_FRAME_Storage )
	{
		m_pID_FRAME_Storage->SetVisible( bVisible );	
	}
	if ( bVisible == TRUE )
	{
		INT nStorageX = SCREEN_WIDTH*2 / 3;
		//INT nPackX = SCREEN_WIDTH * 1 / 8;

		m_pID_FRAME_Storage->SetPos( nStorageX, 80, TRUE );
		//theUIPack.m_pID_FRAME_PACK->SetPos( nPackX, 60, TRUE );
		theUIPack.SetLayoutVisible();
		m_bQuestNpc = TRUE;
	}
	else
	{
		m_pID_FRAME_Storage->SetArrayMode( ArrayMode_Normal );
		theUIPack.m_pID_FRAME_PACK->SetArrayMode( ArrayMode_Normal );

	}
	
}
void CUI_ID_FRAME_Storage::GetNpcSysID( unsigned int nId )
{
	
	m_nNpcSysId = nId;
	
}
void CUI_ID_FRAME_Storage::Refresh()
{
#ifdef USE_OLD
	
	m_pID_LISTIMG_Main->Clear();
	m_pID_LIST_city->Clear();
	m_pID_LIST_price->Clear();
	ggdat::SCharStorageData* pStorageItem = NULL;
	pStorageItem = &GetPlayerRole().m_stStorage;
	VUCtrlList::S_List	stList;
	char szCount[56] = {0};
	for( int n=0; n<ggdat::STORAGE_Grigs_MAX; n++)
	{
		if ( pStorageItem->stStorageItems[n].itembaseinfo.ustItemID == ErrorUnsignedShortID )
		{//这里 增加list控件空项
			memset(szCount, 0, sizeof(char)*56);
			stList.SetData( szCount );
			m_pID_LIST_city->AddItem(&stList);
			m_pID_LIST_price->AddItem(&stList);
			continue;
		}
		//物品设置
		IconDragListImg stStorageItem;

		stStorageItem.SetData( &pStorageItem->stStorageItems[n]);

		m_pID_LISTIMG_Main->SetItem( &stStorageItem, n );
		//------取物品所花费用---------------
		//物品重量
		float fValue = pStorageItem->stStorageItems[n].itembaseinfo.nCost;
		//不同城市取物品所花费用
		float fCostOfDistance = GetPlayerRole().fCostOfFetchItem[n]; // Npcsdistance(theUIPack.nTempNpcId,GetPlayerRole().m_nNpcID);
		//计算取物品所花费用
		float fGetItemCost = fCostOfDistance*StorageRulePara_C + fValue*StorageRulePara_D;
		//物品单价
		int nUnitPrice;
		//取物品价格
		float fUnitPrice = fGetItemCost;	
		nUnitPrice = (int)fUnitPrice;
		//物品城市名字
		sprintf(szCount, "%s", GetPlayerRole().CityName[n]);
		stList.SetData( szCount);
		m_pID_LIST_city->AddItem(&stList);

		sprintf(szCount, "%d", nUnitPrice);
		stList.SetData( szCount);
		m_pID_LIST_price->AddItem(&stList);
	}
	

#endif
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIActionUI.cpp
创建日期：2007年11月18日
最后更新：2007年11月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIAction.h"
#include "UIChatInput.h"
#include "VObject.h"

#include "VObjectManager.h"
#include "UIInputBox.h"
#include "FuncUtil.h"
#include "PathGameMap.h"
#include "UIInventory.h"
#include "UIBaseProperty.h"
#include "TextResManager.h"
#include "UIBaseProperty.h"
#include "UIFriend.h"
#include "UIMeunFriend.h"
#include "UIMessageBox.h"
#include "VUILayoutManager.h"
#include "ActionUIListener.h"
#include "VHero.h"
#include "VHeroAutoInput.h"
#include "VHeroActionInput.h"
#include "MouseHandler.h"
#include "ItemManager.h"
#include "GlobalInstancePriority.h"
#include "SkillStorageManager.h"
#include "Player.h"
#include "UIActionSlotUIListener.h"
#include "UIStyleSlotUIListener.h"
#include "SkillSlot.h"

using namespace vobject;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_IMPL(UIAction, ()  , gamemain::eInstPrioClientUI/*UIAction*/);


namespace gameui
{ 

static ActionUIListener& GetUIListener()
{
	static gameui::ActionUIListener s;
	return s;
}


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE	( theUIAction, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIAction, OnRender )

	UIPROC_BUTTON_CLICK		( theUIAction, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIAction, ID_BUTTON_PropertyOnButtonClick )

	UIPROC_ICON_DROP_TO		( theUIAction, ID_LISTIMG_EmotionOnIconDropTo )
	UIPROC_ICON_LDB_CLICK	( theUIAction, ID_LISTIMG_EmotionOnIconLDBClick )
	UIPROC_ICON_LDB_CLICK	( theUIAction, ID_LISTIMG_EmotionOnIconRButtonUp )

	UIPROC_ICON_DROP_TO		( theUIAction, ID_LISTIMG_StyleOnIconDropTo )
	UIPROC_ICON_LDB_CLICK	( theUIAction, ID_LISTIMG_StyleOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIAction, ID_LISTIMG_StyleOnIconRButtonUp );

	UIPROC_ICON_DROP_TO		( theUIAction, ID_LISTIMG_ActionOnIconDropTo )
	UIPROC_ICON_LDB_CLICK	( theUIAction, ID_LISTIMG_ActionOnIconLDBClick )
	UIPROC_ICON_LDB_CLICK	( theUIAction, ID_LISTIMG_ActionOnIconRButtonUp )

};
using namespace uicallback;

UIAction::UIAction()
:VUIControl(gameui::eUIActionUI)
{
	m_pID_FRAME_ActionUI		= NULL;
	m_pID_BUTTON_CLOSE		= NULL;
	m_pID_BUTTON_Property	= NULL;
	m_pID_LISTIMG_Emotion	= NULL;
	m_pID_LISTIMG_Style			= NULL;
	m_pID_LISTIMG_Action =	 NULL;

	m_nCurFunction = -1;
	m_enCursorType = cursor::CURSORTYPE_DEFAULT;
}

// Frame
BOOL UIAction::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}

BOOL UIAction::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

// Button
BOOL UIAction::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	SetVisible( FALSE );
	return TRUE;
}

// Button
BOOL UIAction::ID_BUTTON_PropertyOnButtonClick( VUCtrlObject* /*pSender*/ )
{
//	SetVisible( FALSE );
	SetLayoutVisible();
	theUIBaseProperty.SetLayoutVisible();
	INT x,y;
	m_pID_FRAME_ActionUI->GetPos(x,y);
	theUIBaseProperty.m_pID_FRAME_BaseProperty->SetPos(x,y);

	return TRUE;
}


// ListImg / ListEx
BOOL UIAction::ID_LISTIMG_EmotionOnIconDropTo(VUCtrlObject*              /*pSender*/
                                                          ,VUCtrlObject*              /*pThisCtrl*/
																			 ,IconDragListImg* /*pItemDrag*/
																			 ,IconDragListImg* /*pItemDest*/)
{
	return FALSE;
}
BOOL UIAction::ID_LISTIMG_EmotionOnIconDragOff(VUCtrlObject*              /*pSender*/
                                                           ,VUCtrlObject*              /*pThisCtrl*/
																			  ,IconDragListImg* /*pItem*/)
{
	return FALSE;
}

BOOL UIAction::ID_LISTIMG_EmotionOnIconLDBClick(VUCtrlObject*              /*pSender*/
																				 ,IconDragListImg* /*pItem*/)
{
	return FALSE;
}

BOOL UIAction::ID_LISTIMG_EmotionOnIconRButtonUp(VUCtrlObject*              pSender
																				 ,IconDragListImg* pItem)
{
	if ( !pItem->IsNull() )
	{
		if(pSender == m_pID_LISTIMG_Emotion)
		{
			theSkillStorageManager.SetUISkillContainer(SKILL_KIND_EMOTION);

			SLOTPOS	atPos = (SLOTPOS)m_pID_LISTIMG_Emotion->GetItemIndex(pItem);
			theMouseHandler.DoItemUsing(SI_SKILL, atPos);
		}
	}
	return TRUE;
}


// ListImg / ListEx
BOOL UIAction::ID_LISTIMG_StyleOnIconDropTo( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
													  IconDragListImg* /*pItemDrag*/,
													  IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
}

BOOL UIAction::ID_LISTIMG_StyleOnIconDragOff( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
													   IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIAction::ID_LISTIMG_StyleOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}



// ListImg / ListEx
BOOL UIAction::ID_LISTIMG_ActionOnIconDropTo( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
														  IconDragListImg* /*pItemDrag*/,
														  IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
}


BOOL UIAction::ID_LISTIMG_ActionOnIconDragOff( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
														   IconDragListImg* /*pItem*/ )
{
	return FALSE;
}


BOOL UIAction::ID_LISTIMG_ActionOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIAction::ID_LISTIMG_ActionOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
{
	if ( !pItem->IsNull() )
	{
		if(pSender == m_pID_LISTIMG_Action)
		{
			theSkillStorageManager.SetUISkillContainer(SKILL_KIND_ACTION);

			SLOTPOS	atPos = (SLOTPOS)m_pID_LISTIMG_Action->GetItemIndex(pItem);
			theMouseHandler.DoItemUsing(SI_SKILL, atPos);
		}
	}
	return TRUE;
}

BOOL UIAction::UseFunction()
{
	return FALSE;
}


BOOL UIAction::ID_LISTIMG_StyleOnIconRButtonUp	(VUCtrlObject*    pSender
																,IconDragListImg* pItem)
{
	if ( !pItem->IsNull() 
		&& pItem->IsEnable())
	{
		if(pSender == m_pID_LISTIMG_Style)
		{
			theSkillStorageManager.SetUISkillContainer(SKILL_KIND_STYLE);
			SLOTPOS	atPos = (SLOTPOS)m_pID_LISTIMG_Style->GetItemIndex(pItem);
			theMouseHandler.DoItemUsing(SI_SKILL, atPos);
		}
	}
	return TRUE;
	//theMouseHandler.DoItemUsing(SI_SKILL, );

}



// 装载UI
BOOL UIAction::LoadUI(  )
{
	GetUIListener().RegisterMe();

	m_pID_FRAME_ActionUI = theUICtrlManager.LoadFrame( UIDOC_PATH( "Action") );
	if (m_pID_FRAME_ActionUI  == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Action") "]失败")
			return FALSE;
	}

	return InitControls(  );
}


// 关连控件
BOOL UIAction::InitControls(  )
{
	

	VUIControl::InitControls();

	m_pID_FRAME_ActionUI->SetProcOnFrameMove(theUIActionOnFrameMove);
	m_pID_FRAME_ActionUI->SetProcOnRender	(theUIActionOnRender, FALSE);
	m_pID_FRAME_ActionUI->SetProcOnMsgProc	( ActionUIMsgProc );


	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)m_pID_FRAME_ActionUI->FindControl( ID_BUTTON_CLOSE );
	m_pID_BUTTON_Property	= (VUCtrlButton*)m_pID_FRAME_ActionUI->FindControl( ID_BUTTON_Property );
	m_pID_LISTIMG_Emotion	= (VUCtrlListImg*)m_pID_FRAME_ActionUI->FindControl( ID_LISTIMG_Emotion );
	m_pID_LISTIMG_Style		= (VUCtrlListImg*)m_pID_FRAME_ActionUI->FindControl( ID_LISTIMG_Style );
	m_pID_LISTIMG_Action		= (VUCtrlListImg*)m_pID_FRAME_ActionUI->FindControl( ID_LISTIMG_Action );

	m_pID_FRAME_ActionUI->SetVisible(FALSE);
	theUILayoutManager.AddFrame(m_pID_FRAME_ActionUI);
	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_BUTTON_Property );
	assert( m_pID_LISTIMG_Emotion );
	assert( m_pID_LISTIMG_Style );
	assert( m_pID_LISTIMG_Action );


	m_pID_BUTTON_CLOSE->		SetProcOnButtonClick( theUIActionID_BUTTON_CLOSEOnButtonClick );
	m_pID_BUTTON_Property->	SetProcOnButtonClick( theUIActionID_BUTTON_PropertyOnButtonClick );

	m_pID_LISTIMG_Emotion->SetProcOnDropTo				(theUIActionID_LISTIMG_EmotionOnIconDropTo);
	m_pID_LISTIMG_Emotion->SetProcOnLButtonDBClick	(theUIActionID_LISTIMG_EmotionOnIconLDBClick);
	m_pID_LISTIMG_Emotion->SetProcOnRButtonUp			(theUIActionID_LISTIMG_EmotionOnIconRButtonUp);

	m_pID_LISTIMG_Style->SetProcOnDropTo			(theUIActionID_LISTIMG_StyleOnIconDropTo);
	m_pID_LISTIMG_Style->SetProcOnLButtonDBClick	(theUIActionID_LISTIMG_StyleOnIconLDBClick);
	m_pID_LISTIMG_Style->SetProcOnRButtonUp		(theUIActionID_LISTIMG_StyleOnIconRButtonUp);

	m_pID_LISTIMG_Action->SetProcOnDropTo			(theUIActionID_LISTIMG_ActionOnIconDropTo);
	m_pID_LISTIMG_Action->SetProcOnLButtonDBClick(theUIActionID_LISTIMG_ActionOnIconLDBClick);
	m_pID_LISTIMG_Action->SetProcOnRButtonUp		(theUIActionID_LISTIMG_ActionOnIconRButtonUp);

	

	SetVisible( FALSE );

	IconDragListImg	stItem;


	//////////////////////////////////////////////////
	// 指令初始化....

	UISCRIPT_ENABLE( UIOBJ_ACTION, m_pID_FRAME_ActionUI );
	
	return TRUE;
	
}


void UIAction::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_ActionUI);
}


// 卸载UI
BOOL UIAction::UnLoadUI()
{
	UISCRIPT_DISABLE( UIOBJ_ACTION );
	GetUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Action") );
}


// 是否可视
BOOL UIAction::IsVisible()
{
	return m_pID_FRAME_ActionUI->IsVisible();
}


// 设置是否可视
void UIAction::SetVisible( BOOL bVisible)
{
	VUIControl::SetVisible( bVisible );
	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_ActionUI->SetVisible( bVisible );

}


void UIAction::Refresh()
{
	if(m_pPlayer == NULL)
		return;

	/////////////////////////////////////////
	///刷新Style状态
	for(SLOTPOS n=0; n<m_pID_LISTIMG_Style->GetItemCount(); n++)
	{
		BOOL						bEnable = TRUE;
		BOOL						bFocus  = FALSE;
		IconDragListImg*		pListItem;
		CODETYPE					itemID;

		pListItem = m_pID_LISTIMG_Style->GetItemAt( n );

		if(pListItem->GetIconInfo() == NULL)
			continue;

		itemID = (CODETYPE)pListItem->GetIconInfo()->GetIconID();
		if(m_pPlayer->GetCurrentAttackStyle() == itemID)
			bFocus = TRUE;

		/////////////////////////////////////////////
		///物品信息显示到UI中
		if(!theUIStyleSlotUIListener.GetContainer())
			continue;

		SkillSlot&				slot = (SkillSlot&)theUIStyleSlotUIListener.GetContainer()->GetSlot(n);
		sSKILLINFO_COMMON*	pInfo= slot.GetInfo();
		if(!pInfo)
			continue;
		bEnable = (slot.GetState() == SLOT_UISTATE_ACTIVATED);
		if(bEnable)
			bEnable = m_pPlayer->CheckAttackStyle(pInfo->m_SkillCode);


		pListItem->GetIconInfo()->SetCount(pInfo->m_wSkillLV);
		pListItem->SetEnable(bEnable);
		pListItem->SetFocus	(bFocus, TRUE );
		pListItem->SetNoPick (bFocus, TRUE );

	}

}


void UIAction::SetCurFunction( const int /*nFunction*/ )
{
}


void UIAction::Process()
{

}



BOOL ActionUIMsgProc( UINT /*msg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL /*bMsgUsed*/ )
{
	return FALSE;
}

void UIAction::OnInstallContainer	(BaseContainer* pContainer, DWORD dwSkillType)
{
	assert(pContainer);
	if(pContainer)
	{
		switch(dwSkillType)
		{
		case SKILL_KIND_ACTION:
			{
				m_pID_LISTIMG_Action->SetSlotContainer(pContainer);
			}
			break;
		case SKILL_KIND_STYLE:
			{
				m_pID_LISTIMG_Style->SetSlotContainer(pContainer);
			}
			break;
		case SKILL_KIND_EMOTION:
			{
				m_pID_LISTIMG_Emotion->SetSlotContainer(pContainer);
			}
			break;
		}
	}
}


void UIAction::PushAction(CODETYPE	itemID, UINT nIndex,UINT nCount)
{
	assert(m_pPlayer);

	sSKILLINFO_COMMON*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	pInfo = theSkillInfoParser.GetInfo((SLOTCODE)itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;


	pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer((BYTE)pInfo->m_SkillClassCode);

	stListItem.SetTimer(pTimer);

	stListItem.SetData(itemID
                     ,nCount
							,VITEMTYPE_SKILL//pVInfo->GetIconType()
							,TRUE);

	m_pID_LISTIMG_Action->SetItem( &stListItem, nIndex );
}


void UIAction::PopAction( const UINT nIndex,BOOL bDeactivate )
{
	IconDragListImg* pCell;

	pCell = m_pID_LISTIMG_Emotion->GetItemAt(nIndex);

	if(bDeactivate)
	{
		pCell->SetDeactivate(TRUE);
	}
	else
	{
		m_pID_LISTIMG_Action->RemoveItemAt(  nIndex );
	}
}

void UIAction::PushStyle(CODETYPE	itemID, UINT nIndex,UINT nCount)
{
	assert(m_pPlayer);

	sSKILLINFO_COMMON*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	pInfo = theSkillInfoParser.GetInfo((SLOTCODE)itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	BOOL bEnable = TRUE;
	BOOL bFocus  = TRUE;
	if(m_pPlayer->GetCurrentAttackStyle() == itemID)
		bFocus = FALSE;
	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;


	pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer((BYTE)pInfo->m_SkillClassCode);

	stListItem.SetTimer(pTimer);

	stListItem.SetData(itemID
                     ,nCount
							,VITEMTYPE_SKILL//pVInfo->GetIconType()
							,bEnable);
	stListItem.SetFocus(bFocus);
	stListItem.SetNoPick(bFocus);

	m_pID_LISTIMG_Style->SetItem( &stListItem, nIndex );
}


void UIAction::PopStyle( const UINT nIndex,BOOL bDeactivate )
{
	IconDragListImg* pCell;

	pCell = m_pID_LISTIMG_Style->GetItemAt(nIndex);

	if(bDeactivate)
	{
		pCell->SetDeactivate(TRUE);
	}
	else
	{
		m_pID_LISTIMG_Style->RemoveItemAt(  nIndex );
	}
}


void UIAction::PushEmotion(CODETYPE	itemID, UINT nIndex,UINT nCount)
{
	assert(m_pPlayer);

	sSKILLINFO_COMMON*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	pInfo = theSkillInfoParser.GetInfo((SLOTCODE)itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;


	pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer((BYTE)pInfo->m_SkillClassCode);

	stListItem.SetTimer(pTimer);

	stListItem.SetData(itemID
                     ,nCount
							,VITEMTYPE_SKILL//pVInfo->GetIconType()
							,TRUE);

	m_pID_LISTIMG_Emotion->SetItem( &stListItem, nIndex );
}


void UIAction::PopEmotion( const UINT nIndex,BOOL bDeactivate )
{
	IconDragListImg* pCell;

	pCell = m_pID_LISTIMG_Emotion->GetItemAt(nIndex);

	if(bDeactivate)
	{
		pCell->SetDeactivate(TRUE);
	}
	else
	{
		m_pID_LISTIMG_Emotion->RemoveItemAt(  nIndex );
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void UIAction::PushAction(BaseSlot* pSlot)
{
	assert(m_pPlayer);

	CODETYPE				itemID;
	UINT					nIndex;
	UINT					nCount;

	sSKILLINFO_COMMON*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	itemID	= pSlot->GetCode();
	nIndex	= pSlot->GetPos();
	nCount	= pSlot->GetNum();

	pInfo = theSkillInfoParser.GetInfo((SLOTCODE)itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;


	pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer((BYTE)pInfo->m_SkillClassCode);

	stListItem.SetTimer(pTimer);
	stListItem.SetSlot(pSlot, VITEMTYPE_SKILL);


	stListItem.SetData(itemID
                     ,nCount
							,VITEMTYPE_SKILL//pVInfo->GetIconType()
							,TRUE);

	m_pID_LISTIMG_Action->SetItem( &stListItem, nIndex );
}


void UIAction::PopAction( BaseSlot* pSlot )
{
	assert(pSlot);

	UINT nIndex			= pSlot->GetPos();
	//IconDragListImg* pCell;

	//pCell = m_pID_LISTIMG_Emotion->GetItemAt(nIndex);

	//if(bDeactivate)
	//{
	//	pCell->m_bEnable = FALSE;
	//}
	//else
	{
		m_pID_LISTIMG_Action->RemoveItemAt(  nIndex );
	}
}

void UIAction::PushStyle(BaseSlot* pSlot)
{
	assert(m_pPlayer);

	CODETYPE				itemID;
	UINT					nIndex;
	UINT					nCount;

	sSKILLINFO_COMMON*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	itemID	= pSlot->GetCode();
	nIndex	= pSlot->GetPos();
	nCount	= pSlot->GetNum();

	pInfo = theSkillInfoParser.GetInfo((SLOTCODE)itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	BOOL	bEnable	= FALSE;
	BOOL	bFocus	= FALSE;
	if(m_pPlayer->GetCurrentAttackStyle() == itemID)
		bFocus = TRUE;
	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;


	pTimer	= m_pPlayer->GetPlayerCookie().GetItemCoolTimer((BYTE)pInfo->m_SkillClassCode);

	bEnable	= (pSlot->GetState() == SLOT_UISTATE_ACTIVATED);
	if(bEnable)
		bEnable = m_pPlayer->CheckAttackStyle(pInfo->m_SkillCode);
	stListItem.SetTimer(pTimer);
	stListItem.SetSlot(pSlot, VITEMTYPE_SKILL);

	stListItem.SetData(itemID
                     ,pInfo->m_wSkillLV
							,VITEMTYPE_SKILL//pVInfo->GetIconType()
							,bEnable
							,TRUE
							,0
							,TRUE
							,pInfo->m_wMaxLV);

	stListItem.SetFocus(bFocus, TRUE);
	stListItem.SetNoPick(bFocus, TRUE);
	//m_pID_LISTIMG_Style->SetLevelInfo( pInfo->m_wSkillLV, pInfo->m_wMaxLV );
	m_pID_LISTIMG_Style->SetItem( &stListItem, nIndex );

	//IconDragListImg* pCell;
	//pCell = m_pID_LISTIMG_Style->GetItemAt(nIndex);
	//pCell->SetFocus(bFocus, TRUE);
	//pCell->SetNoPick(bFocus, TRUE);
}


void UIAction::PopStyle( BaseSlot* pSlot )
{
	const UINT nIndex = pSlot->GetPos();

	IconDragListImg* pCell;

	pCell = m_pID_LISTIMG_Style->GetItemAt(nIndex);

	//if(bDeactivate)
	//{
	//	pCell->m_bEnable = FALSE;
	//}
	//else
	{
		m_pID_LISTIMG_Style->RemoveItemAt(  nIndex );
	}
}


void UIAction::PushEmotion(BaseSlot* pSlot)
{
	assert(m_pPlayer);

	CODETYPE				itemID;
	UINT					nIndex;
	UINT					nCount;

	sSKILLINFO_COMMON*	pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	itemID	= pSlot->GetCode();
	nIndex	= pSlot->GetPos();
	nCount	= pSlot->GetNum();

	pInfo = theSkillInfoParser.GetInfo((SLOTCODE)itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;


	pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer((BYTE)pInfo->m_SkillClassCode);

	stListItem.SetTimer(pTimer);
	stListItem.SetSlot(pSlot, VITEMTYPE_SKILL);

	stListItem.SetData(itemID
                     ,nCount
							,VITEMTYPE_SKILL//pVInfo->GetIconType()
							,TRUE);

	m_pID_LISTIMG_Emotion->SetItem( &stListItem, nIndex );
}


void UIAction::PopEmotion( BaseSlot* pSlot )
{
	const UINT nIndex = pSlot->GetPos();
	IconDragListImg* pCell;

	pCell = m_pID_LISTIMG_Emotion->GetItemAt(nIndex);

	//if(bDeactivate)
	//{
	//	pCell->m_bEnable = FALSE;
	//}
	//else
	{
		m_pID_LISTIMG_Emotion->RemoveItemAt(  nIndex );
	}
}


};//namespace gameui

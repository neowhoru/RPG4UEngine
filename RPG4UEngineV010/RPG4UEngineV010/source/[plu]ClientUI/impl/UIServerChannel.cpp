/*////////////////////////////////////////////////////////////////////////
文 件 名：UIServerChannel.cpp
创建日期：2008年3月22日
最后更新：2008年3月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "UIServerChannel.h"
#include "LoginSceneDefine.h"
#include "LoginScene.h"
#include "TextResDefine.h"


namespace gameui
{
//UIPROC_BUTTON_CLICK( s_UIServerChannel, ID_BUTTON_CHANNELOnButtonClick )
const INT SERVERCHANNEL_SPACE		= 8;
const INT UPDATE_DELAY				= 3000;

UIServerChannel::UIServerChannel()
{
	// Member
	m_pID_FRAME_SERVERCHANNEL	= NULL;
	m_pID_PROGRESS_CHANNEL		= NULL;
	m_pID_BUTTON_CHANNEL			= NULL;
	m_pID_TEXT_STATUS				= NULL;
	m_pID_TEXT_CHANNEL			= NULL;

	m_nChannelIndex				= -1;
	m_pChannelInfo					= NULL;


}

BOOL UIServerChannel::OnFrameMove(DWORD /*dwTick*/)
{
	if(IsVisible())
	{
		if(m_timerUpdate.IsExpired())
			UpdateChannelInfo(NULL);
	}
	return TRUE;
}

// Button
	BOOL UIServerChannel::ID_BUTTON_CHANNELOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
//#pragma message(__FILE__  "(36) @@@@@@@@  UIServerChannel 触发ServerSelect " )
//		assert(0 && "触发ServerSelect");
		singleton::GetLoginScene().SelectChannelAt(*GetChannelInfo());
		return TRUE;
	}

BOOL UIServerChannel::ProcOnButtonClick(OUT VUCtrlObject* pSender)
{
	assert(pSender);
	UIServerChannel* pFrame = (UIServerChannel*)pSender->GetUserData();
	if(pFrame)
	{
		pFrame->ID_BUTTON_CHANNELOnButtonClick( pSender );
	}
	return TRUE;
}

BOOL UIServerChannel::ProcOnFrameMove(DWORD dwTick,OUT VUCtrlObject* pSender)
{
	assert(pSender);
	UIServerChannel* pFrame = (UIServerChannel*)pSender->GetUserData();
	if(pFrame)
	{
		pFrame->OnFrameMove( dwTick);
	}
	return TRUE;
}

VUCtrlFrame* UIServerChannel::LoadUI(INT nChannel,VUCtrlFrame* pParent)
	{
		assert(nChannel >= 0);
		m_nChannelIndex = nChannel;
		LPCSTR szNewName = FMSTR(ID_FRAME_SERVERCHANNEL"%d",nChannel);
		if(pParent)
			m_pID_FRAME_SERVERCHANNEL = pParent->LoadChildByFrame( UIDOC_PATH( "ServerChannel"), szNewName );
		else
			m_pID_FRAME_SERVERCHANNEL = theUICtrlManager.LoadFrame( UIDOC_PATH( "ServerChannel"),TRUE,FALSE, szNewName );

		if ( m_pID_FRAME_SERVERCHANNEL == 0 )
		{
			UIMessageLog("读取文件[ServerChannel.UI]失败")
			return NULL;
		}
		InitControls();

		return m_pID_FRAME_SERVERCHANNEL;
	}


	// 关连控件
	BOOL UIServerChannel::InitControls()
	{
		m_pID_PROGRESS_CHANNEL	= (VUCtrlProgress*)	m_pID_FRAME_SERVERCHANNEL->FindControl(ID_PROGRESS_CHANNEL );
		m_pID_BUTTON_CHANNEL		= (VUCtrlButton*)		m_pID_FRAME_SERVERCHANNEL->FindControl(ID_BUTTON_CHANNEL );
		m_pID_TEXT_CHANNEL		= (VUCtrlText*)		m_pID_FRAME_SERVERCHANNEL->FindControl(ID_TEXT_CHANNEL );
		m_pID_TEXT_STATUS			= (VUCtrlText*)		m_pID_FRAME_SERVERCHANNEL->FindControl(ID_TEXT_STATUS );

		assert( m_pID_PROGRESS_CHANNEL );
		assert( m_pID_BUTTON_CHANNEL );
		assert( m_pID_TEXT_CHANNEL );
		assert( m_pID_TEXT_STATUS );

		m_pID_FRAME_SERVERCHANNEL->SetProcOnFrameMove2(ProcOnFrameMove);
		m_pID_FRAME_SERVERCHANNEL->SetUserData((LPARAM)this);

		if(m_pID_BUTTON_CHANNEL)
		{
			m_pID_BUTTON_CHANNEL->SetUserData((LPARAM)this);
			m_pID_BUTTON_CHANNEL->SetProcOnButtonClick(ProcOnButtonClick);
		}

		m_timerUpdate.SetTimer(UPDATE_DELAY);
		m_timerUpdate.Reset();
		return TRUE;
	}

	// 卸载UI
BOOL UIServerChannel::UnLoadUI()
	{
		return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ServerChannel") );
	}

	// 是否可视
	BOOL UIServerChannel::IsVisible()
	{
		return m_pID_FRAME_SERVERCHANNEL->IsVisible();
	}

	// 设置是否可视
	void UIServerChannel::SetVisible( BOOL bVisible )
	{
		m_pID_FRAME_SERVERCHANNEL->SetVisible( bVisible );
	}

	void UIServerChannel::FitPositionBy(int x, int y)
	{
		assert(m_nChannelIndex >= 0);
		int nPosX = x;
		int nPosY = y + m_nChannelIndex* GetChannelHeight();
		m_pID_FRAME_SERVERCHANNEL->SetPos(nPosX, nPosY,FALSE);
	}

	void UIServerChannel::UpdateChannelInfo(const SServerChannelInfo* pInfo)
	{
		if(!pInfo)
			pInfo = GetChannelInfo();
		else
			SetChannelInfo((SServerChannelInfo*)pInfo);

		if(!pInfo)
			return;

		m_pID_PROGRESS_CHANNEL->SetValue(pInfo->m_fProgress);
	
	//	以下为BYTE中，数值意义：
	//	0 - 3   闲状态
	//	4 - 6   中状态
	//	7 - 8   繁忙状态
	//	9		  满状态
	//	其它	  则不处理


		DWORD dwTextID(TEXTRES_SERVERGROUP_FULL);
		if(pInfo->m_fProgress < 0.3)
			dwTextID	= TEXTRES_SERVERGROUP_IDLE;
		else if(pInfo->m_fProgress < 0.6)
			dwTextID	= TEXTRES_SERVERGROUP_GOOD;
		else if(pInfo->m_fProgress < 0.9)
			dwTextID	= TEXTRES_SERVERGROUP_BUSINESS;

		StringHandle	sFormat = _STRING(dwTextID);
		if(sFormat.FindChar('%',0 ) < 0)
			sFormat = _T("%3d%%");
		
		m_pID_TEXT_STATUS		->SetText(FMSTR(sFormat,(int)(pInfo->m_fProgress*100.f)));
		m_pID_TEXT_CHANNEL	->SetText(pInfo->m_sChannelName);
	}

	UINT UIServerChannel::GetChannelHeight()
	{
		return m_pID_FRAME_SERVERCHANNEL->GetHeight() + SERVERCHANNEL_SPACE;
	}

};
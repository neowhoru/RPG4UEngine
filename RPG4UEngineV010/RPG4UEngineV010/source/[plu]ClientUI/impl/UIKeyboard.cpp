/*////////////////////////////////////////////////////////////////////////
文 件 名：UIKeyboard.cpp
创建日期：2007年12月13日
最后更新：2007年12月13日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIKeyboard.h"


#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(UIKeyboard, ()  , gamemain::eInstPrioClientUI/*UIKeyboard*/);
#else
#endif

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIKeyboard, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIKeyboard, OnRender )
	UIPROC_CHECK_BOX_CHECK( theUIKeyboard, ID_CHECKBOX_ViewChangeOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIKeyboard, ID_CHECKBOX_CloseTipsOnCheckBoxCheck )
	UIPROC_BUTTON_CLICK( theUIKeyboard, ID_BUTTON_ReSetOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIKeyboard, ID_BUTTON_CloseOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIKeyboard, ID_BUTTON_AppOnButtonClick )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIKeyboard, ID_SCROLLBAREX_CAMERAROTATEOnScrollBarExUpdatePos )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIKeyboard, ID_SCROLLBAREX_CAMERAINERTIAOnScrollBarExUpdatePos )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIKeyboard, ID_SCROLLBAREX_CAMERAZOOMMAXOnScrollBarExUpdatePos )
};//namespace uicallback
using namespace uicallback;


UIKeyboard::UIKeyboard()
{
	
	// Member
	m_pID_FRAME_KeyBoard = NULL;
	m_pID_CHECKBOX_ViewChange = NULL;
	m_pID_CHECKBOX_CloseTips = NULL;
	m_pID_BUTTON_ReSet = NULL;
	m_pID_BUTTON_Close = NULL;
	m_pID_BUTTON_App = NULL;
	m_pID_SCROLLBAREX_CAMERAROTATE = NULL;
	m_pID_SCROLLBAREX_CAMERAINERTIA = NULL;
	m_pID_SCROLLBAREX_CAMERAZOOMMAX = NULL;
	

}
// Frame
BOOL UIKeyboard::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIKeyboard::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// CheckBox
void UIKeyboard::ID_CHECKBOX_ViewChangeOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	m_pID_CHECKBOX_ViewChange->SetVisible( FALSE );

	
}
// CheckBox
void UIKeyboard::ID_CHECKBOX_CloseTipsOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	m_pID_CHECKBOX_CloseTips->SetVisible( FALSE );

	
}

// Button
BOOL UIKeyboard::ID_BUTTON_AppOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	return TRUE;
	
}

// Button
BOOL UIKeyboard::ID_BUTTON_ReSetOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	return TRUE;
	
}

BOOL UIKeyboard::ID_BUTTON_CloseOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	SetVisible( FALSE );
	return TRUE;
	
}
// 装载UI
BOOL UIKeyboard::LoadUI()
{
	
	m_pID_FRAME_KeyBoard = theUICtrlManager.LoadFrame( UIDOC_PATH( "Keyboard") );
	if ( m_pID_FRAME_KeyBoard == 0 )
	{
		assert(FALSE);
		UIMessageLog("读取文件[Keyboard.UIDoc]失败")
			return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UIKeyboard::InitControls()
{
	m_pID_CHECKBOX_ViewChange			= (VUCtrlCheckBox*)m_pID_FRAME_KeyBoard->FindControl(  ID_CHECKBOX_ViewChange );
	m_pID_CHECKBOX_CloseTips			= (VUCtrlCheckBox*)m_pID_FRAME_KeyBoard->FindControl(  ID_CHECKBOX_CloseTips );
	m_pID_BUTTON_ReSet					= (VUCtrlButton*)m_pID_FRAME_KeyBoard->FindControl(  ID_BUTTON_ReSet );
	m_pID_BUTTON_Close					= (VUCtrlButton*)m_pID_FRAME_KeyBoard->FindControl(  ID_BUTTON_Close );
	m_pID_BUTTON_App						= (VUCtrlButton*)m_pID_FRAME_KeyBoard->FindControl(  ID_BUTTON_App );
	m_pID_SCROLLBAREX_CAMERAROTATE	= (VUCtrlScrollBarEx*)m_pID_FRAME_KeyBoard->FindControl(  ID_SCROLLBAREX_CAMERAROTATE );
	m_pID_SCROLLBAREX_CAMERAINERTIA	= (VUCtrlScrollBarEx*)m_pID_FRAME_KeyBoard->FindControl(  ID_SCROLLBAREX_CAMERAINERTIA );
	m_pID_SCROLLBAREX_CAMERAZOOMMAX	= (VUCtrlScrollBarEx*)m_pID_FRAME_KeyBoard->FindControl(  ID_SCROLLBAREX_CAMERAZOOMMAX );
	
	m_pID_FRAME_KeyBoard->SetProcOnFrameMove(  theUIKeyboardOnFrameMove );
	m_pID_FRAME_KeyBoard->SetProcOnRender(  theUIKeyboardOnRender );

	m_pID_CHECKBOX_ViewChange	->SetProcOnCheck(   theUIKeyboardID_CHECKBOX_ViewChangeOnCheckBoxCheck );
	m_pID_CHECKBOX_CloseTips	->SetProcOnCheck(  theUIKeyboardID_CHECKBOX_CloseTipsOnCheckBoxCheck );
	m_pID_BUTTON_ReSet			->SetProcOnButtonClick	(  theUIKeyboardID_BUTTON_ReSetOnButtonClick );
	m_pID_BUTTON_Close			->SetProcOnButtonClick	(  theUIKeyboardID_BUTTON_CloseOnButtonClick );
	m_pID_BUTTON_App				->SetProcOnButtonClick	(  theUIKeyboardID_BUTTON_AppOnButtonClick );

	m_pID_SCROLLBAREX_CAMERAROTATE ->SetProcOnUpdatePos(  theUIKeyboardID_SCROLLBAREX_CAMERAROTATEOnScrollBarExUpdatePos );
	m_pID_SCROLLBAREX_CAMERAINERTIA->SetProcOnUpdatePos(   theUIKeyboardID_SCROLLBAREX_CAMERAINERTIAOnScrollBarExUpdatePos );
	m_pID_SCROLLBAREX_CAMERAZOOMMAX->SetProcOnUpdatePos(   theUIKeyboardID_SCROLLBAREX_CAMERAZOOMMAXOnScrollBarExUpdatePos );



	assert( m_pID_FRAME_KeyBoard );
	assert( m_pID_CHECKBOX_ViewChange );
	assert( m_pID_CHECKBOX_CloseTips );
	assert( m_pID_BUTTON_ReSet );
	assert( m_pID_BUTTON_Close );
	assert( m_pID_BUTTON_App );
	assert( m_pID_SCROLLBAREX_CAMERAROTATE );
	assert( m_pID_SCROLLBAREX_CAMERAINERTIA );
	assert( m_pID_SCROLLBAREX_CAMERAZOOMMAX );
	SetVisible( FALSE );
	return TRUE;
	
}
// 卸载UI
BOOL UIKeyboard::UnLoadUI()
{
	
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Keyboard") );
	
}
// 是否可视
BOOL UIKeyboard::IsVisible()
{
	
	return m_pID_FRAME_KeyBoard->IsVisible();
	
}
// 设置是否可视
void UIKeyboard::SetVisible( BOOL bVisible )
{
	m_pID_FRAME_KeyBoard->SetVisible( bVisible );
	m_pID_CHECKBOX_ViewChange->SetVisible( bVisible );
	m_pID_CHECKBOX_CloseTips->SetVisible( bVisible );
}


void UIKeyboard::ID_SCROLLBAREX_CAMERAROTATEOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int /*n*/ )
{
	
}


void UIKeyboard::ID_SCROLLBAREX_CAMERAINERTIAOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int /*n*/ )
{
	
}

void UIKeyboard::ID_SCROLLBAREX_CAMERAZOOMMAXOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int /*n*/ )
{
	
}
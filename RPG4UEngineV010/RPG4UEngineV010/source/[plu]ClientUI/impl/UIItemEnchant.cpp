/*////////////////////////////////////////////////////////////////////////
文 件 名：UIItemEnchant.h
DOC 文件：uidata\UIItemEnchant.uidoc
UI  名称：ItemEnchant
创建日期：2008年6月18日
最后更新：2008年6月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIItemEnchant.h"
#include "VUILayoutManager.h"
#include "ConstTextRes.h"
#include "UIInventory.h"
#include "InputLayer.h"
#include "ItemManager.h"
#include "ItemSlot.h"
#include "ItemSlotContainer.h"
#include "HeroTipLayer.h"
#include "ItemCompoundDialog.h"
#include "VUIIconMouseTip.h"
#include "MouseHandler.h"
#include "VUCtrlIconDragManager.h"
#include "TextResManager.h"
#include "ItemCompositeParser.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIItemEnchant, ()  , gamemain::eInstPrioClientUI);

#define USE_UIITEMENCHANT_LAYOUT

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemEnchantUIListener& GetUIItemEnchantItemEnchantUIListener()
{
	static ItemEnchantUIListener staticItemEnchantUIListener;
	return staticItemEnchantUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIItemEnchant, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIItemEnchant, OnRender )

	UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_PICTURE_MODELOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_BUTTON_HELPOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_BUTTON_CANCELOnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIItemEnchant, ID_LISTIMG_RESULTOnIconDropTo )
	UIPROC_ICON_LDB_CLICK 	( theUIItemEnchant, ID_LISTIMG_RESULTOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIItemEnchant, ID_LISTIMG_RESULTOnIconRButtonUp )
	UIPROC_ICON_DROP_TO		( theUIItemEnchant, ID_LISTIMG_MATERIALOnIconDropTo )
	UIPROC_ICON_LDB_CLICK 	( theUIItemEnchant, ID_LISTIMG_MATERIALOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIItemEnchant, ID_LISTIMG_MATERIALOnIconRButtonUp )
	UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_BUTTON_CLEAROnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIItemEnchant, ID_LISTIMG_SUBRESULTOnIconDropTo )
	UIPROC_ICON_LDB_CLICK 	( theUIItemEnchant, ID_LISTIMG_SUBRESULTOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIItemEnchant, ID_LISTIMG_SUBRESULTOnIconRButtonUp )
	UIPROC_CHECK_BOX_CHECK	( theUIItemEnchant, ID_CHECKBOX_RATEOnCheckBoxCheck )
	//UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_BUTTON_REMOVEOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIItemEnchant, ID_BUTTON_ENCHANTOnButtonClick ) //
	UIPROC_ICON_DROP_TO		( theUIItemEnchant, ID_LISTIMG_SOCKETSOnIconDropTo )
	UIPROC_ICON_LDB_CLICK 	( theUIItemEnchant, ID_LISTIMG_SOCKETSOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIItemEnchant, ID_LISTIMG_SOCKETSOnIconRButtonUp )
	UIPROC_EDIT_ENTER  		( theUIItemEnchant, ID_EDIT_NAMEOnEditEnter )

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemEnchantUIListener::ItemEnchantUIListener()
{
}
ItemEnchantUIListener::~ItemEnchantUIListener()
{
}

LPCSTR ItemEnchantUIListener::GetName()
{
	return "ItemEnchantUIListener";
}

EGameUIType ItemEnchantUIListener::GetType()
{
	return eUIItemEnchant;
}

void ItemEnchantUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUIItemEnchant.SetVisible((BOOL)dwData);
		}
		break;

	case eSetMoney:
		{
			theUIItemEnchant.SetMoney(*(MONEY*)dwData);
		}break;
	}
}

BOOL ItemEnchantUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void ItemEnchantUIListener::Refresh(BOOL /*bExtra*/)
{
	theUIItemEnchant.Refresh();
}

void ItemEnchantUIListener::TriggerFunc(ETriggerData eData,LPARAM lpData)
{
	switch(eData)
	{
	case eFillNeedMaterials:
		{
			theUIItemEnchant.FillNeedMaterials();
		}break;
	case eOnItemCompoundFinished:
		{
			theUIItemEnchant.OnItemCompoundFinished((MSG_OBJECT_BASE*)lpData);
		}break;
	case eOnClearTarget:
		{
			theUIItemEnchant.OnClearTarget();
		}break;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIItemEnchant::UIItemEnchant()
{
	m_pID_FRAME_ITEMENCHANT		= NULL;
	//m_pID_BUTTON_CLOSE  		= NULL;
	//m_pID_BUTTON_HELP			= NULL;
	//m_pID_BUTTON_CANCEL 		= NULL;
	//m_pID_LISTIMG_RESULT		= NULL;
	//m_pID_PICTURE_MODEL 		= NULL;
	//m_pID_LISTIMG_MATERIAL 	= NULL;
	//m_pID_BUTTON_CLEAR  		= NULL;
	//m_pID_TEXT_MONEY 			= NULL;
	//m_pID_LISTIMG_SUBRESULT	= NULL;
	//m_pID_CHECKBOX_RATE0		= NULL;
	//m_pID_CHECKBOX_RATE1		= NULL;
	//m_pID_CHECKBOX_RATE2		= NULL;
	//m_pID_CHECKBOX_RATE3		= NULL;
	//m_pID_BUTTON_REMOVE 		= NULL;
	//m_pID_BUTTON_ENCHANT  		= NULL;
	//m_pID_LISTIMG_SOCKETS  	= NULL;
	//m_pID_EDIT_NAME  			= NULL;
	m_pID_TEXT_ENCHANT_LEVEL	= NULL;

}

// Frame
BOOL UIItemEnchant::OnFrameMove(DWORD dwTick)
{
	__BOOL_SUPER(OnFrameMove(dwTick));
	return TRUE;
	
}


BOOL UIItemEnchant::OnRender(DWORD dwTick)
{
	__BOOL_SUPER(OnRender(dwTick));
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UIItemEnchant::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* pSender )
{
	__BOOL_SUPER(ID_BUTTON_CLOSEOnButtonClick(pSender ) );
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_HELP::OnButtonClick
BOOL UIItemEnchant::ID_BUTTON_HELPOnButtonClick(VUCtrlObject* pSender )
{
	__BOOL_SUPER(ID_BUTTON_HELPOnButtonClick(pSender ) );
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CANCEL::OnButtonClick
BOOL UIItemEnchant::ID_BUTTON_CANCELOnButtonClick(VUCtrlObject* pSender )
{
	__BOOL_SUPER(ID_BUTTON_CANCELOnButtonClick(pSender ) );
	return TRUE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconDropTo
BOOL UIItemEnchant::ID_LISTIMG_RESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconLDBClick
BOOL UIItemEnchant::ID_LISTIMG_RESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconRButtonUp
BOOL UIItemEnchant::ID_LISTIMG_RESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconDropTo
BOOL UIItemEnchant::ID_LISTIMG_MATERIALOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconLDBClick
BOOL UIItemEnchant::ID_LISTIMG_MATERIALOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconRButtonUp
BOOL UIItemEnchant::ID_LISTIMG_MATERIALOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLEAR::OnButtonClick
BOOL UIItemEnchant::ID_BUTTON_CLEAROnButtonClick(VUCtrlObject* pSender )
{
	_SUPER::ID_BUTTON_CLEAROnButtonClick(pSender ) ;
	//RefreshComposite();

	return TRUE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconDropTo
BOOL UIItemEnchant::ID_LISTIMG_SUBRESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconLDBClick
BOOL UIItemEnchant::ID_LISTIMG_SUBRESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SUBRESULT::OnIconRButtonUp
BOOL UIItemEnchant::ID_LISTIMG_SUBRESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_CHECKBOX_RATE0::OnCheckBoxCheck
void UIItemEnchant::ID_CHECKBOX_RATEOnCheckBoxCheck(VUCtrlObject* pSender, BOOL*						pbChecked)
{
	_SUPER::ID_CHECKBOX_RATEOnCheckBoxCheck(pSender ,pbChecked);
}



////////////////////////////////////////////////////
//ID_BUTTON_REMOVE::OnButtonClick
//BOOL UIItemEnchant::ID_BUTTON_REMOVEOnButtonClick(VUCtrlObject* /*pSender*/ )
//{
//}

////////////////////////////////////////////////////
//ID_BUTTON_ENCHANT::OnButtonClick
BOOL UIItemEnchant::ID_BUTTON_ENCHANTOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	if(m_nActionMode == COMPOSITE_WORKING)
	{
		//if(!theItemCompoundDialog.ExistEmptySocket())
		//{
		//	OUTPUTTOP(TEXTRES_RC_ITEM_FULLSOCKET);
		//	return FALSE;
		//}
		//if(!theItemCompoundDialog.ExistSocketItem(TRUE))
		//{
		//	OUTPUTTOP(TEXTRES_NOT_EXIST_SOCKET_FOR_INSERT);
		//	return FALSE;
		//}
	}

	return StartAction();
}


BOOL UIItemEnchant::StartAction( )
{
	if(m_pCurrentComposite == NULL)
	{
		OUTPUTTOP(TEXTRES_SELECT_ITEM_FOR_COMPOUND);
		return FALSE;
	}

	if(m_nActionMode == COMPOSITE_WORKING)
	{
		if(!theItemCompoundDialog.ExistTargetItem())
		{
			OUTPUTTOP(TEXTRES_TARGET_SLOT_IS_EMPTY);
			return FALSE;
		}

		if(!theItemCompoundDialog.CheckValidMaterails(eCOMPOSITE_CHECK_MONEY))
		{
			OUTPUTTOP(TEXTRES_NOT_ENOUGH_MONEY);
			return FALSE;
		}

		if(!theItemCompoundDialog.CheckValidMaterails(eCOMPOSITE_CHECK_MATERIAL))
		{
			OUTPUTTOP(TEXTRES_RC_ITEM_ENCHANT_HAVENOTMATERIALS);
			return FALSE;
		}

		m_nActionMode = COMPOSITE_WAITING;

		if(!theItemCompoundDialog.Enchant())
		{
			m_nActionMode = COMPOSITE_WORKING;
			OUTPUTTOP(TEXTRES_RC_ITEM_INVALID_CONDITION);
			return FALSE;
		}

		return TRUE;
	}

	if(m_nActionMode == COMPOSITE_NEXT)
	{
		m_nActionMode = COMPOSITE_WORKING;
		theIconMouseTip.SetLockKey(TIP_LOCKEKY,0);
		theIconMouseTip.SetVisible(FALSE);

		theMouseHandler.CancelHandlingItem();
		theIconDragManager.ClearDrag();
		RefreshComposite();
	}
	
	return TRUE;
}


////////////////////////////////////////////////////
//ID_LISTIMG_SOCKETS::OnIconDropTo
BOOL UIItemEnchant::ID_LISTIMG_SOCKETSOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SOCKETS::OnIconLDBClick
BOOL UIItemEnchant::ID_LISTIMG_SOCKETSOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_SOCKETS::OnIconRButtonUp
BOOL UIItemEnchant::ID_LISTIMG_SOCKETSOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_EDIT_NAME::OnEditEnter
void UIItemEnchant::ID_EDIT_NAMEOnEditEnter(VUCtrlObject* /*pSender*/, LPCSTR 				/*szData*/ )
{
	
}




 //设置是否可视
void UIItemEnchant::SetVisible( const BOOL bVisible )
{
	_SUPER::SetVisible(bVisible);

//#ifdef USE_UIITEMENCHANT_LAYOUT
//	if(bVisible != IsVisible())
//		SetLayoutVisible();
//	else
//		m_pID_FRAME_ITEMENCHANT->SetVisible( bVisible );
//
//	if( bVisible == FALSE )
//	{
//		theMouseHandler.SetMouseState(eMOUSE_NONE);
//
//		theIconMouseTip.SetLockKey(TIP_LOCKEKY, FALSE);
//		theIconMouseTip.SetVisible(FALSE);
//		m_pID_FRAME_ITEMENCHANT->SetArrangeMode( ArrayMode_Normal );
//	}
//	else
//	{
//		Refresh();
//	}
//#else
//		m_pID_FRAME_ITEMENCHANT->SetVisible( bVisible );
//#endif
//	theUIInventory.SetVisible(bVisible);
//	theInputLayer.SetKBActivated(!bVisible);
}

void UIItemEnchant::SetLayoutVisible()
{
	_SUPER::SetLayoutVisible();
//#ifdef USE_UIITEMENCHANT_LAYOUT
//	theUILayoutManager.SetVisible(m_pID_FRAME_ITEMENCHANT);
//#endif
}

void UIItemEnchant::CloseWindow()
{
	_SUPER::CloseWindow();
	//theItemCompoundDialog.CloseWindow();
}

void UIItemEnchant::Refresh()
{
}



// 装载UI
BOOL UIItemEnchant::LoadUI()
{
	GetUIItemEnchantItemEnchantUIListener().RegisterMe();

	m_pID_FRAME_ITEMENCHANT = theUICtrlManager.LoadFrame( UIDOC_PATH( "ItemEnchant") );
	if ( m_pID_FRAME_ITEMENCHANT == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "ItemEnchant") "]失败");
		return FALSE;
	}

	m_pID_FRAME_ITEMINLAY	= m_pID_FRAME_ITEMENCHANT;

#ifdef USE_UIITEMENCHANT_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_ITEMENCHANT);
#endif
	m_pID_FRAME_ITEMENCHANT->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIItemEnchant::InitControls()
{
	//INT n;

	m_pID_FRAME_ITEMENCHANT->SetProcOnFrameMove			(theUIItemEnchantOnFrameMove);
	m_pID_FRAME_ITEMENCHANT->SetProcOnRender		(theUIItemEnchantOnRender, FALSE);
	//m_pID_FRAME_ITEMENCHANT->SetProcOnMsgProc		( UIItemEnchant_MsgProc );

	
	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	//m_pID_BUTTON_CLOSE	  	= (VUCtrlButton*)  	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_BUTTON_CLOSE );
	//m_pID_BUTTON_HELP			= (VUCtrlButton*)  	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_BUTTON_HELP );
	//m_pID_BUTTON_CANCEL	 	= (VUCtrlButton*)  	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_BUTTON_CANCEL );
	//m_pID_LISTIMG_RESULT		= (VUCtrlListImg*) 	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_LISTIMG_RESULT );
	//m_pID_PICTURE_MODEL	 	= (VUCtrlPicture*) 	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_PICTURE_MODEL );
	//m_pID_LISTIMG_MATERIAL	 = (VUCtrlListImg*) 	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_LISTIMG_MATERIAL );
	//m_pID_BUTTON_CLEAR	  	= (VUCtrlButton*)  	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_BUTTON_CLEAR );
	//m_pID_TEXT_MONEY	 		= (VUCtrlText*) 		m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_TEXT_MONEY );
	//m_pID_LISTIMG_SUBRESULT	= (VUCtrlListImg*) 	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_LISTIMG_SUBRESULT );
	//m_pID_CHECKBOX_RATE0		= (VUCtrlCheckBox*)	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_CHECKBOX_RATE0 );
	//m_pID_CHECKBOX_RATE1		= (VUCtrlCheckBox*)	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_CHECKBOX_RATE1 );
	//m_pID_CHECKBOX_RATE2		= (VUCtrlCheckBox*)	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_CHECKBOX_RATE2 );
	//m_pID_CHECKBOX_RATE3		= (VUCtrlCheckBox*)	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_CHECKBOX_RATE3 );
	//m_pID_BUTTON_REMOVE	 	= (VUCtrlButton*)  	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_BUTTON_REMOVE );
	m_pID_BUTTON_ENCHANT	  		= (VUCtrlButton*)  	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_BUTTON_ENCHANT );
	m_pID_LISTIMG_SOCKETS		= (VUCtrlListImg*) 	m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_LISTIMG_SOCKETS );
	m_pID_TEXT_ENCHANT_LEVEL	= (VUCtrlText*) 		m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_TEXT_ENCHANT_LEVEL );
	//m_pID_EDIT_NAME	  		= (VUCtrlEdit*) 		m_pID_FRAME_ITEMENCHANT->FindControl( UIITEMENCHANT_ID_EDIT_NAME );
	
	
	/////////////////////////////////////////////////////
	//assert 
	//assert( m_pID_BUTTON_CLOSE );
	//assert( m_pID_BUTTON_HELP );
	//assert( m_pID_BUTTON_CANCEL );
	//assert( m_pID_LISTIMG_RESULT );
	//assert( m_pID_PICTURE_MODEL );
	//assert( m_pID_LISTIMG_MATERIAL );
	//assert( m_pID_BUTTON_CLEAR );
	//assert( m_pID_TEXT_MONEY );
	//assert( m_pID_LISTIMG_SUBRESULT );
	//assert( m_pID_CHECKBOX_RATE0 );
	//assert( m_pID_CHECKBOX_RATE1 );
	//assert( m_pID_CHECKBOX_RATE2 );
	//assert( m_pID_CHECKBOX_RATE3 );
	//assert( m_pID_BUTTON_REMOVE );
	assert( m_pID_BUTTON_ENCHANT );
	assert( m_pID_LISTIMG_SOCKETS );
	assert( m_pID_TEXT_ENCHANT_LEVEL );
	//assert( m_pID_EDIT_NAME );
	
	
	/////////////////////////////////////////////////////
	UIItemComposite::InitControls(m_pID_FRAME_ITEMENCHANT);

	for(INT n=0; n<eSUCCEEDRATE_NUM; n++)
	{
		m_arSucceedRate[n]		->SetProcOnCheck 	( theUIItemEnchantID_CHECKBOX_RATEOnCheckBoxCheck );
	}

	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_PICTURE_MODEL 		->SetProcOnButtonClick	( theUIItemEnchantID_PICTURE_MODELOnButtonClick );
	m_pID_BUTTON_CLOSE  		->SetProcOnButtonClick	( theUIItemEnchantID_BUTTON_CLOSEOnButtonClick );
	m_pID_BUTTON_HELP			->SetProcOnButtonClick	( theUIItemEnchantID_BUTTON_HELPOnButtonClick );
	m_pID_BUTTON_CANCEL 		->SetProcOnButtonClick	( theUIItemEnchantID_BUTTON_CANCELOnButtonClick );
	m_pID_LISTIMG_RESULT		->SetProcOnDropTo  		( theUIItemEnchantID_LISTIMG_RESULTOnIconDropTo );
	m_pID_LISTIMG_RESULT		->SetProcOnLButtonDBClick		( theUIItemEnchantID_LISTIMG_RESULTOnIconLDBClick );
	m_pID_LISTIMG_RESULT		->SetProcOnRButtonUp  	( theUIItemEnchantID_LISTIMG_RESULTOnIconRButtonUp );
	m_pID_LISTIMG_MATERIAL 	->SetProcOnDropTo  		( theUIItemEnchantID_LISTIMG_MATERIALOnIconDropTo );
	m_pID_LISTIMG_MATERIAL 	->SetProcOnLButtonDBClick		( theUIItemEnchantID_LISTIMG_MATERIALOnIconLDBClick );
	m_pID_LISTIMG_MATERIAL 	->SetProcOnRButtonUp  	( theUIItemEnchantID_LISTIMG_MATERIALOnIconRButtonUp );
	m_pID_BUTTON_CLEAR  		->SetProcOnButtonClick	( theUIItemEnchantID_BUTTON_CLEAROnButtonClick );
	m_pID_LISTIMG_SUBRESULT	->SetProcOnDropTo  		( theUIItemEnchantID_LISTIMG_SUBRESULTOnIconDropTo );
	m_pID_LISTIMG_SUBRESULT	->SetProcOnLButtonDBClick		( theUIItemEnchantID_LISTIMG_SUBRESULTOnIconLDBClick );
	m_pID_LISTIMG_SUBRESULT	->SetProcOnRButtonUp  	( theUIItemEnchantID_LISTIMG_SUBRESULTOnIconRButtonUp );
	//m_pID_BUTTON_REMOVE 		->SetProcOnButtonClick	( theUIItemEnchantID_BUTTON_REMOVEOnButtonClick );
	m_pID_BUTTON_ENCHANT  		->SetProcOnButtonClick	( theUIItemEnchantID_BUTTON_ENCHANTOnButtonClick );
	m_pID_LISTIMG_SOCKETS  	->SetProcOnDropTo  		( theUIItemEnchantID_LISTIMG_SOCKETSOnIconDropTo );
	m_pID_LISTIMG_SOCKETS  	->SetProcOnLButtonDBClick		( theUIItemEnchantID_LISTIMG_SOCKETSOnIconLDBClick );
	m_pID_LISTIMG_SOCKETS  	->SetProcOnRButtonUp  	( theUIItemEnchantID_LISTIMG_SOCKETSOnIconRButtonUp );
	m_pID_EDIT_NAME  			->SetProcOnEnter			( theUIItemEnchantID_EDIT_NAMEOnEditEnter );

	
	m_pID_LISTIMG_SOCKETS	->SetCanPicking(FALSE);
	m_pID_LISTIMG_RESULT		->SetCanPicking(TRUE);
	m_pID_EDIT_NAME			->SetReadOnly(TRUE);
	//m_pID_LISTIMG_SUBRESULT	->SetCanPicking(TRUE);

	return TRUE;
}


UIItemEnchant::~UIItemEnchant()
{
	//UnLoadUI();
	
}


// 卸载UI
BOOL UIItemEnchant::UnLoadUI()
{
	GetUIItemEnchantItemEnchantUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ItemEnchant") );
	
}

// 是否可视
BOOL UIItemEnchant::IsVisible()
{
	if( m_pID_FRAME_ITEMENCHANT )
		return m_pID_FRAME_ITEMENCHANT->IsVisible();

	return FALSE;
}



void UIItemEnchant::RefreshSockets()
{
}

void UIItemEnchant::RefreshComposite()
{
	//SLOTCODE					itemCode;
	//sVITEMINFO_BASE*		pVItem;
	//ItemSlotContainer*	pContainer;	

	_SUPER::RefreshComposite();

	//pContainer = (ItemSlotContainer*)theItemManager.GetContainer(SI_ENCHANT_TARGET);

	//ItemSlot& itemSlot =  (ItemSlot&)pContainer->GetSlot(0);
	//itemCode = itemSlot.GetCode();
	//pVItem	= theItemInfoParser.GetVItemInfoByItem(itemCode);

	//if(pVItem)
	//{
	//	///变更模型
	//	ReloadMesh(pVItem->m_ModelID);

	//	///变更名称
	//	*m_pID_EDIT_NAME = (LPCSTR)pVItem->m_sName;
	//}
}

void UIItemEnchant::OnItemCompoundFinished(MSG_OBJECT_BASE * pMsgBase)
{
	m_nActionMode	= COMPOSITE_NEXT;

	switch (pMsgBase->m_byProtocol)
	{
		///失败...
	case CG_ITEM_ENCHANT_NAK: 
	case CG_ITEM_ENCHANT_FAILED_ACK:
		{
			theMouseHandler.RemoveMouseState(eMOUSE_SOCKET_REMOVE);
			theMouseHandler.CancelHandlingItem();
			theIconDragManager.ClearDrag();
			*m_pID_EDIT_NAME = "";
			//DoChangeTargetState();
		}break;

	case CG_ITEM_ENCHANT_SUCCESS_ACK:
		{
			MSG_CG_ITEM_ENCHANT_SUCCESS_ACK * pRecvPacket = (MSG_CG_ITEM_ENCHANT_SUCCESS_ACK *)pMsgBase;
			__CHECK2_PTR(pRecvPacket,;);

			//VUCtrlIconDrag*		pListImg = NULL;
			//ItemSlotContainer*	pContainer;
			//SLOTPOS					atPos;

			//theMouseHandler.RemoveMouseState(eMOUSE_SOCKET_REMOVE);
			//theMouseHandler.CancelHandlingItem();
			//theIconDragManager.ClearDrag();

			/////拖动物品到鼠标上
			m_pID_LISTIMG_RESULT->ShowToolTip(0, TIP_LOCKEKY);
			m_pID_LISTIMG_RESULT->ForceDragFrom(0);

		}break;
	}
}

void UIItemEnchant::DoChangeTargetState(ItemSlot* pItemSlot)
{
	MATERIALTYPE CompositeCode;
	
	///更新到镶嵌模式 按物品等级
	//if(theMouseHandler.GetMouseState() == eMOUSE_SOCKET_REMOVE)
	//{
	//	CompositeCode = ITEMCOMPOSITE_EXTRACT_SOCKET;
	//	m_pID_LISTIMG_SUBRESULT	->SetCanPicking(FALSE);
	//}
	//else
	//{
	//	CompositeCode = ITEMCOMPOSITE_SOCKET_FILL;
	//	m_pID_LISTIMG_SUBRESULT	->SetCanPicking(FALSE);
	//}
	switch(pItemSlot->GetEnchantLV())
	{
	case ENCHANT_LOW:		CompositeCode = ITEMCOMPOSITE_LOW_ENCHANT;		break;
	case ENCHANT_MIDDLE:	CompositeCode = ITEMCOMPOSITE_MEDIUM_ENCHANT;	break;
	case ENCHANT_HIGH:	CompositeCode = ITEMCOMPOSITE_HIGH_ENCHANT;		break;
	default:
		assert(0);
		return;
	}

	*m_pID_TEXT_ENCHANT_LEVEL = _STRING(TEXTRES_LOW_ENCHANT_TEXT + pItemSlot->GetEnchantLV() - ENCHANT_LOW);


	m_pCurrentComposite	= theItemCompositeParser.GetCompositeInfo(CompositeCode);//ITEMCOMPOSITE_EXTRACT_SOCKET);

	RefreshComposite();
	RefreshSockets();
}

void UIItemEnchant::ChangeTargetItem(BaseSlot* pSlot, BOOL /*bAdd*/)
{
	//SLOTCODE				itemCode;
	//sITEMINFO_BASE*		pItem;



	//itemCode = pSlot->GetCode();
	//pItem		= theItemInfoParser.GetItemInfo(itemCode);
	if(m_nActionMode == COMPOSITE_WORKING)
	{
		assert(pSlot->GetSlotType() == ST_ITEM || pSlot->GetSlotType() == ST_ITEMHANDLE);
		DoChangeTargetState((ItemSlot*)pSlot);
	}
	else
	{
		if(pSlot->GetCode() == 0)
		{
			m_itemModel.ReloadMesh(INVALID_DWORD_ID);
		}
	}

	//_SUPER::ChangeTargetItem( pSlot,  bAdd);
}

void UIItemEnchant::ChangeSubResultItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}

void UIItemEnchant::ChangeMaterialItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}



};//namespace gameui


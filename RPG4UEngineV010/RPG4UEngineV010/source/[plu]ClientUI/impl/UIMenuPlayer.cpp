/*////////////////////////////////////////////////////////////////////////
文 件 名：UIRbtnMenu.cpp
创建日期：2007年11月24日
最后更新：2007年11月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIMenuPlayer.h"
#include "UIAction.h"
#include "UITeamManager.h"
#include "UIChatInput.h"
#include "UIFriend.h"
#include "MenuPlayerUIListener.h"
#include "UITeamManager.h"
#include "HeroParty.h"
#include "TradeDialog.h"
#include "GlobalInstancePriority.h"

using namespace gameui;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::MenuPlayerUIListener& GetUIRbtnMenuMenuPlayerUIListener()
{
	static gameui::MenuPlayerUIListener staticMenuPlayerUIListener;
	return staticMenuPlayerUIListener;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(UIMenuPlayer, ()  , gamemain::eInstPrioClientUI/*UIMenuPlayer*/);
#else
#endif

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIMenuPlayer, OnFrameMove )
		UIPROC_FRAME_RENDER( theUIMenuPlayer, OnRender )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_TERMOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_EXCHANGEOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_FRDOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_FOLLOWMEOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_KICKOUTOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_PrivateOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuPlayer, ID_BUTTON_CHANGEMASTEROnButtonClick )
};//namespace uicallback
using namespace uicallback;

UIMenuPlayer::UIMenuPlayer()
{
	
	// Member
	m_pID_FRAME_RBTNMENU = NULL;
	m_pID_BUTTON_TERM = NULL;
	m_pID_BUTTON_EXCHANGE = NULL;
	m_pID_BUTTON_FRD = NULL;
	m_pID_BUTTON_FOLLOWME = NULL;
	m_pID_BUTTON_KICKOUT = NULL;
//	m_pID_BUTTON_CLOSE = NULL;
	m_pID_BUTTON_Private = NULL;
	m_pID_BUTTON_CHANGEMASTER = NULL;

	m_szName.clear();
	m_nPlayerID = -1;
	
}

// Frame
BOOL UIMenuPlayer::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIMenuPlayer::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// Button
BOOL UIMenuPlayer::ID_BUTTON_TERMOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	SetVisible( FALSE );
	theHeroParty.Invite(theHeroActionInput.GetCurrentTarget());

	return TRUE;
}

// Button
BOOL UIMenuPlayer::ID_BUTTON_EXCHANGEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	SetVisible( FALSE );
	theTradeDialog.RequestTrade(theHeroActionInput.GetCurrentTarget());
	return TRUE;
}
// Button
BOOL UIMenuPlayer::ID_BUTTON_FRDOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	SetVisible( FALSE );

	if( theUIFriend.CheckUseName(m_szName.c_str()) )
	{
		return TRUE;
	}
	return FALSE;
	
}
// Button
BOOL UIMenuPlayer::ID_BUTTON_FOLLOWMEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theHeroActionInput.SetFollowState(TRUE, theHeroActionInput.GetCurrentTarget());
	
	SetVisible( FALSE );
	return TRUE;
	
}
//// Button
//BOOL UIMenuPlayer::ID_BUTTON_SEEOnButtonClick( VUCtrlObject* /*pSender*/ )
//{
//	
//	VCharacter* pTarget = singleton::GetPlayerManager().GetMouseTargetPlayer();
//	if( pTarget )
//	{
//		s_CUI_ID_FRAME_OtheActor.SetPlayer( pTarget->GetID() );
//		s_CUI_ID_FRAME_OtheActor.SetVisible( TRUE );
//	}
//	SetVisible( FALSE );
//	return TRUE;
//	
//}
// Button
BOOL UIMenuPlayer::ID_BUTTON_KICKOUTOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	SetVisible( FALSE );
	theHeroParty.KickOff(theHeroActionInput.GetCurrentTarget());
	return TRUE;
	
}

BOOL UIMenuPlayer::ID_BUTTON_PrivateOnButtonClick( VUCtrlObject* /*pSender*/ )
{//私聊
	if( m_szName.length() > 0 )
		theUIChatInput.BeginPrivateChat( m_szName.c_str() );
	SetVisible( FALSE );
	return TRUE;
}

BOOL UIMenuPlayer::ID_BUTTON_CHANGEMASTEROnButtonClick( VUCtrlObject* /*pSender*/ )
{

	SetVisible( FALSE );
	theHeroParty.ChangeMaster(theHeroActionInput.GetCurrentTarget());
	return TRUE;
}

// 装载UI
BOOL UIMenuPlayer::LoadUI()
{
	
	GetUIRbtnMenuMenuPlayerUIListener().RegisterMe();

	m_pID_FRAME_RBTNMENU = theUICtrlManager.LoadFrame( UIDOC_PATH( "MenuPlayer") );
	if ( m_pID_FRAME_RBTNMENU == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "MenuPlayer")"]失败")
			return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UIMenuPlayer::InitControls()
{
	
	m_pID_BUTTON_TERM			= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_TERM );
	m_pID_BUTTON_EXCHANGE	= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_EXCHANGE );
	m_pID_BUTTON_FRD			= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_FRD );
	m_pID_BUTTON_FOLLOWME	= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_FOLLOWME );
	m_pID_BUTTON_KICKOUT		= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_KICKOUT );
	m_pID_BUTTON_Private		= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_Private );
	m_pID_BUTTON_CHANGEMASTER	= (VUCtrlButton*)m_pID_FRAME_RBTNMENU->FindControl(ID_BUTTON_CHANGEMASTER );
	
	assert( m_pID_BUTTON_TERM );
	assert( m_pID_BUTTON_EXCHANGE );
	assert( m_pID_BUTTON_FRD );
	assert( m_pID_BUTTON_FOLLOWME );
	assert( m_pID_BUTTON_KICKOUT );
	assert( m_pID_BUTTON_Private );
	assert( m_pID_BUTTON_CHANGEMASTER );

	m_pID_FRAME_RBTNMENU->SetProcOnFrameMove	(  theUIMenuPlayerOnFrameMove );
	m_pID_FRAME_RBTNMENU->SetProcOnRender		(  theUIMenuPlayerOnRender );

	m_pID_BUTTON_TERM		->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_TERMOnButtonClick );
	m_pID_BUTTON_EXCHANGE->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_EXCHANGEOnButtonClick );
	m_pID_BUTTON_FRD		->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_FRDOnButtonClick );
	m_pID_BUTTON_FOLLOWME->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_FOLLOWMEOnButtonClick );
	m_pID_BUTTON_KICKOUT	->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_KICKOUTOnButtonClick );
	m_pID_BUTTON_Private	->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_PrivateOnButtonClick );
	m_pID_BUTTON_CHANGEMASTER	->SetProcOnButtonClick(theUIMenuPlayerID_BUTTON_CHANGEMASTEROnButtonClick );


	m_BtnInfos.push_back(m_pID_BUTTON_TERM);
	m_BtnInfos.push_back(m_pID_BUTTON_EXCHANGE);
	m_BtnInfos.push_back(m_pID_BUTTON_FRD);
	m_BtnInfos.push_back(m_pID_BUTTON_FOLLOWME);
	m_BtnInfos.push_back(m_pID_BUTTON_KICKOUT);
	m_BtnInfos.push_back(m_pID_BUTTON_Private);
	m_BtnInfos.push_back(m_pID_BUTTON_CHANGEMASTER);


	//m_pID_BUTTON_KICKOUT->SetVisible( FALSE );

	SetVisible( FALSE );
	return TRUE;
	
}
// 卸载UI
BOOL UIMenuPlayer::UnLoadUI()
{
	
	m_BtnInfos.clear();
	GetUIRbtnMenuMenuPlayerUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "MenuPlayer") );
	
}

VOID UIMenuPlayer::SetPos(INT x, INT y)
{
	m_pID_FRAME_RBTNMENU->SetPos( x, y );
}
// 是否可视
BOOL UIMenuPlayer::IsVisible()
{
	
	if( m_pID_FRAME_RBTNMENU )
		return m_pID_FRAME_RBTNMENU->IsVisible();
	return TRUE;
	
}
// 设置是否可视
void UIMenuPlayer::SetVisible( BOOL bVisible, BOOL /*bGroupClick*/ )
{
	if( !m_pID_FRAME_RBTNMENU )
		return ;
	m_pID_FRAME_RBTNMENU->SetVisible( bVisible );
	if(!bVisible)
		return;

	if(theHeroParty.GetMemberBy(m_nPlayerID))
	{
		m_pID_BUTTON_TERM				->SetVisible( FALSE ); 
		m_pID_BUTTON_KICKOUT			->SetVisible( theHeroParty.IsHeroMaster() ); 
		m_pID_BUTTON_CHANGEMASTER	->SetVisible( theHeroParty.IsHeroMaster() ); 
	}
	else
	{
		m_pID_BUTTON_TERM				->SetVisible( TRUE ); 
		m_pID_BUTTON_KICKOUT			->SetVisible( FALSE ); 
		m_pID_BUTTON_CHANGEMASTER	->SetVisible( FALSE ); 
	}


	RECT rc;
	m_pID_FRAME_RBTNMENU->GetGlobalRect(&rc);
	int nShowCnt = 0;
	for(UINT i=0; i<m_BtnInfos.size(); ++i)
	{
		if(m_BtnInfos[i]->IsVisible())
		{
			m_BtnInfos[i]->SetPos(rc.left
                              ,rc.top + nShowCnt*m_BtnInfos[i]->GetHeight())				;
			++nShowCnt;
		}
	}
	
}


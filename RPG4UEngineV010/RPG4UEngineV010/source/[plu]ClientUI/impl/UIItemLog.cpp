/*////////////////////////////////////////////////////////////////////////
文 件 名：UIItemLog.h
DOC 文件：uidata\UIItemLog.uidoc
UI  名称：ItemLog
创建日期：2008年7月5日
最后更新：2008年7月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIItemLog.h"
#include "VUILayoutManager.h"
#include "DummyItemSlot.h"
#include "BaseContainer.h"
#include "ItemManager.h"
#include "UIItemLogSlotUIListener.h"
#include "ConstTextRes.h"
#include "TextResManager.h"
#include "GameStruct.h"
#include "GameParameter.h"
#include "ItemLogManager.h"
#include "ItemInfoParser.h"
#include "UIContainHelper.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIItemLog, ()  , gamemain::eInstPrioClientUI);

//#define USE_UIITEMLOG_LAYOUT

namespace gameui
{ 

const DWORD SHOW_DELAY		= 3000;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemLogUIListener& GetUIItemLogItemLogUIListener()
{
	static ItemLogUIListener staticItemLogUIListener;
	return staticItemLogUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIItemLog, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIItemLog, OnRender )
	UIPROC_LIST_SELECT_CHANGE	( theUIItemLog, ID_LIST_LOGOnListSelectChange )
	UIPROC_ICON_DROP_TO			( theUIItemLog, ID_LISTIMG_LOGOnIconDropTo )
	UIPROC_ICON_LDB_CLICK 		( theUIItemLog, ID_LISTIMG_LOGOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP		( theUIItemLog, ID_LISTIMG_LOGOnIconRButtonUp )
	UIPROC_BUTTON_CLICK			( theUIItemLog, ID_BUTTON_CLOSEOnButtonClick )

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemLogUIListener::ItemLogUIListener()
{
}
ItemLogUIListener::~ItemLogUIListener()
{
}

LPCSTR ItemLogUIListener::GetName()
{
	return "ItemLogUIListener";
}

EGameUIType ItemLogUIListener::GetType()
{
	return eUIItemLog;
}

void ItemLogUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUIItemLog.SetVisible((BOOL)dwData);
		}
		break;
	}
}

BOOL ItemLogUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void ItemLogUIListener::Refresh(BOOL /*bExtra*/)
{
	theUIItemLog.Refresh();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIItemLog::UIItemLog()
{
	m_pID_FRAME_ITEMLOG	= NULL;
	m_pID_LIST_LOG 		= NULL;
	m_pID_LISTIMG_LOG 	= NULL;
	m_pID_BUTTON_CLOSE	= NULL;

	m_nCurrentPage			= 0;
	m_pPlayer				= NULL;

	m_nShowTimer			= 0;
	m_bAutoHide				= TRUE;
}

// Frame
BOOL UIItemLog::OnFrameMove(DWORD /*dwTick*/)
{
	if(IsVisible() && m_bAutoHide)
	{
		if(m_nShowTimer)
		{
			POINT pt;
			GetCursorPos(&pt);
			ScreenToClient(g_hWndMain, &pt);
			if(!m_pID_FRAME_ITEMLOG->PtInObject(pt.x,pt.y))
			{
				if(g_CurTime - m_nShowTimer >= theGeneralGameParam.GetItemLogShowDelay())
				{
					m_nShowTimer = 0;
					SetVisible(FALSE);
				}
			}
		}
	}
	return TRUE;
	
}


BOOL UIItemLog::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

////////////////////////////////////////////////////
//ID_LIST_LOG::OnListSelectChange
void UIItemLog::ID_LIST_LOGOnListSelectChange(VUCtrlObject* /*pSender*/, sUILIST_ITEM*	/*pItem*/)
{
	
}

////////////////////////////////////////////////////
//ID_LISTIMG_LOG::OnIconDropTo
BOOL UIItemLog::ID_LISTIMG_LOGOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_LOG::OnIconLDBClick
BOOL UIItemLog::ID_LISTIMG_LOGOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_LOG::OnIconRButtonUp
BOOL UIItemLog::ID_LISTIMG_LOGOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UIItemLog::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}




// 设置是否可视
void UIItemLog::SetVisible( const BOOL bVisible )
{
#ifdef USE_UIITEMLOG_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_ITEMLOG->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		m_pID_FRAME_ITEMLOG->SetArrangeMode( ArrayMode_Normal );
	}
	else
	{
		Refresh();
	}
#else
		//m_pID_FRAME_ITEMLOG->SetVisible( bVisible);
	if(bVisible ^ IsVisible())
		m_pID_FRAME_ITEMLOG->SetVisibleWhole( bVisible ,TRUE);
#endif
	
}
void UIItemLog::SetLayoutVisible()
{
#ifdef USE_UIITEMLOG_LAYOUT
	theUILayoutManager.SetVisible(m_pID_FRAME_ITEMLOG);
#endif
}

void UIItemLog::Refresh()
{
}


// 装载UI
BOOL UIItemLog::LoadUI()
{
	GetUIItemLogItemLogUIListener().RegisterMe();

	m_pID_FRAME_ITEMLOG = theUICtrlManager.LoadFrame( UIDOC_PATH( "ItemLog") );
	if ( m_pID_FRAME_ITEMLOG == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "ItemLog") "]失败");
		return FALSE;
	}

#ifdef USE_UIITEMLOG_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_ITEMLOG);
#endif
	m_pID_FRAME_ITEMLOG->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIItemLog::InitControls()
{
	//INT n;

	m_pID_FRAME_ITEMLOG->SetProcOnFrameMove			(theUIItemLogOnFrameMove);
	m_pID_FRAME_ITEMLOG->SetProcOnRender		(theUIItemLogOnRender, FALSE);
	//m_pID_FRAME_ITEMLOG->SetProcOnMsgProc		( UIItemLog_MsgProc );

	
	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_LIST_LOG	 	= (VUCtrlList*)		m_pID_FRAME_ITEMLOG->FindControl( UIITEMLOG_ID_LIST_LOG );
	m_pID_LISTIMG_LOG	 = (VUCtrlListImg*)	m_pID_FRAME_ITEMLOG->FindControl( UIITEMLOG_ID_LISTIMG_LOG );
	m_pID_BUTTON_CLOSE	= (VUCtrlButton*) 	m_pID_FRAME_ITEMLOG->FindControl( UIITEMLOG_ID_BUTTON_CLOSE );
	
	
	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_LIST_LOG );
	assert( m_pID_LISTIMG_LOG );
	assert( m_pID_BUTTON_CLOSE );
	
	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_LIST_LOG 		->SetProcOnSelectChange	( theUIItemLogID_LIST_LOGOnListSelectChange );
	m_pID_LISTIMG_LOG 	->SetProcOnDropTo			( theUIItemLogID_LISTIMG_LOGOnIconDropTo );
	m_pID_LISTIMG_LOG 	->SetProcOnLButtonDBClick 		( theUIItemLogID_LISTIMG_LOGOnIconLDBClick );
	m_pID_LISTIMG_LOG 	->SetProcOnRButtonUp		( theUIItemLogID_LISTIMG_LOGOnIconRButtonUp );
	m_pID_BUTTON_CLOSE	->SetProcOnButtonClick 	( theUIItemLogID_BUTTON_CLOSEOnButtonClick );

	

	sUILIST_ITEM	stList;
	stList.SetData("");
	m_pID_LIST_LOG->ResetItem(&stList,eITEMLOG_SOCKET_MAX_SLOT);


	return TRUE;
}


UIItemLog::~UIItemLog()
{
	//UnLoadUI();
	
}


// 卸载UI
BOOL UIItemLog::UnLoadUI()
{
	GetUIItemLogItemLogUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ItemLog") );
	
}

// 是否可视
BOOL UIItemLog::IsVisible()
{
	if( m_pID_FRAME_ITEMLOG )
		return m_pID_FRAME_ITEMLOG->IsVisible();

	return FALSE;
}

void UIItemLog::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		m_pID_LISTIMG_LOG->SetSlotContainer(pContainer);
	}
}

void UIItemLog::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	UIContainHelper	helper(m_pID_LISTIMG_LOG
									,m_pPlayer
									,eITEMLOG_SOCKET_MAX_SLOT
									,m_nCurrentPage);
	helper.PushItem(*pSlot);


}


void UIItemLog::OnItemSlot(BaseSlot& slot, BOOL bAdd)
{
	if(!bAdd)
	{
		sUILIST_ITEM	stList;
		m_pID_LIST_LOG->SetItemAt(slot.GetPos(), &stList);
	}
}


void UIItemLog::PopGoods( BaseSlot* pSlot )
{
	UIContainHelper	helper(m_pID_LISTIMG_LOG
									,m_pPlayer
									,eITEMLOG_SOCKET_MAX_SLOT
									,m_nCurrentPage
									,this);
	helper.PopItem(*pSlot);


}


void UIItemLog::SetGoodsState( BaseSlot* pSlot,DWORD itemOpr )
{
	SLOTPOS posSlot = pSlot->GetPos();
	if(posSlot == INVALID_POSTYPE)
		return;

	DWORD dwState(0);


	assert(pSlot->GetSlotType() == ST_ITEMHANDLE	);
	DummyItemSlot*		pItemDummy = (DummyItemSlot*)pSlot;

	INT	nIndex;
	INT	nPage ;

	nPage		= posSlot / eITEMLOG_SOCKET_MAX_SLOT;
	nIndex	= posSlot % eITEMLOG_SOCKET_MAX_SLOT;
	if(nPage != m_nCurrentPage)
		return;

	IconDragListImg*	pImg;
	pImg = m_pID_LISTIMG_LOG->GetItemAt(nIndex);
	if(!pImg)
		return;

	switch(itemOpr)
	{
	case eITEMLOG_DELETE:	dwState = SLOT_UISTATE_DISABLE;break;
	case eITEMLOG_REQUIRE:	dwState = SLOT_UISTATE_DAMAGED;break;
	}


	pImg->SetEnable	(dwState != SLOT_UISTATE_DISABLE);
	pImg->SetDamaged	(dwState == SLOT_UISTATE_DAMAGED);

	m_pID_LISTIMG_LOG->SetCurSelItem(0,nIndex);


	///////////////////////////////////////////
	//
	DWORD					dwTextID  = TEXTRES_ITEMLOG_OPR_GET;
	sITEMINFO_BASE*	pInfo;
	DWORD					dwColor(0);

	pInfo	= theItemInfoParser.GetItemInfo(pSlot->GetCode());
	__VERIFY2_PTR(pInfo,"sVITEMINFO_BASE is null\n",);

	switch(itemOpr)
	{
	case eITEMLOG_DELETE:	
		{
			dwTextID = TEXTRES_ITEMLOG_OPR_DELETE;		
			dwColor	= 0xFFff7f1f;
		}break;
	case eITEMLOG_TOREWARD:
		{
			dwTextID = TEXTRES_ITEMLOG_OPR_TOREWARD;	
			dwColor	= 0xFF1fff7f;
		}break;
	case eITEMLOG_REQUIRE:
		{
			dwTextID = TEXTRES_ITEMLOG_OPR_REQUIRE;	
			dwColor	= 0xFF7f7f7f;
		}break;
	}

	sUILIST_ITEM	stList;
	LPCSTR					szText;
	szText		= FMSTR(_STRING(dwTextID), (LPCSTR)pInfo->m_ItemName, pItemDummy->GetExtraData());
	stList.SetData(szText,0,0,dwColor);
	m_pID_LIST_LOG->SetItemAt(nIndex, &stList);

	m_pID_LIST_LOG->SetCurSelIndex(nIndex);


	m_nShowTimer = g_CurTime;
	SetVisible(TRUE);

}

};//namespace gameui


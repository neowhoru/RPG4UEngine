/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBaseProperty.cpp
创建日期：2007年11月28日
最后更新：2007年11月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIBaseProperty.h"
#include "CharInfoParser.h"
#include "UISkill.h"
#include "UIAction.h"
#include "UIQuest.h"
#include "VUILayoutManager.h"
#include "BasePropertyListener.h"
#include "Hero.h"
#include "PlayerAttributes.h"
#include "CharInfoParser.h"
#include "ConstTextRes.h"
#include "TextResManager.h"
#include "PacketInclude.h"
#include "SeriesInfoParser.h"
#include "ClientUIFunc.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIBaseProperty, ()  , gamemain::eInstPrioClientUI);



namespace gameui
{

const DWORD STATE_GROUP	= TAG('BASE');

static gameui::BasePropertyListener& GetUIListener()
{
	static gameui::BasePropertyListener s;
	return s;
}



namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE		( theUIBaseProperty, OnFrameMove )
	UIPROC_FRAME_RENDER	( theUIBaseProperty, OnRender )
	UIPROC_BUTTON_CLICK( theUIBaseProperty, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIBaseProperty, ID_BUTTON_UPGRADEOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIBaseProperty, ID_BUTTON_ActionOnButtonClick )
};//namespace uicallback
using namespace uicallback;


UIBaseProperty::UIBaseProperty()
:VUIControl( gameui::eUIBaseProperty )
{
	
	// Member
	m_pID_FRAME_BaseProperty = NULL;
	m_pID_BUTTON_CLOSE = NULL;
	m_pID_TEXT_Name = NULL;
	m_pID_TEXT_Title = NULL;
	m_pID_TEXT_Guild = NULL;
	m_pID_TEXT_Level = NULL;
	m_pID_TEXT_INT = NULL;
	m_pID_TEXT_VIT = NULL;
	m_pID_TEXT_STR = NULL;
	m_pID_TEXT_DEX = NULL;
	m_pID_TEXT_KilledNum = NULL;
	m_pID_TEXT_Work = NULL;
	m_pID_TEXT_Official = NULL;
	m_pID_TEXT_Marry = NULL;
	m_pID_TEXT_Distinction = NULL;
	m_pID_BUTTON_Action = NULL;
	m_pID_TEXT_SPR = NULL;
	m_pID_TEXT_LRN = NULL;
	m_pID_TEXT_CRE = NULL;
	m_pID_TEXT_EXPERTY1 = NULL;
	m_pID_TEXT_EXPERTY2 = NULL;
	m_pID_TEXT_REMAINSTAT = NULL;

	m_pID_TEXT_MATTACK_FIRE = NULL;
	m_pID_TEXT_MDEF_FIRE = NULL;
	m_pID_TEXT_MATTACK_WATER = NULL;
	m_pID_TEXT_MDEF_WATER = NULL;
	m_pID_TEXT_MATTACK_WOOD = NULL;
	m_pID_TEXT_MDEF_WOOD = NULL;
	m_pID_TEXT_MATTACK_GOLD = NULL;
	m_pID_TEXT_MDEF_GOLD = NULL;
	m_pID_TEXT_MATTACK_EARTH = NULL;
	m_pID_TEXT_MDEF_EARTH = NULL;
	m_pID_TEXT_Hp = NULL;
	m_pID_TEXT_Mp = NULL;
	m_pID_TEXT_PHYATTACK = NULL;
	m_pID_TEXT_PHYDEF = NULL;
	m_pID_TEXT_MAGIC_DEF = NULL;
	m_pID_TEXT_MAGICATTACK = NULL;
	m_pID_TEXT_RangeAttack = NULL;
	m_pID_TEXT_Hit = NULL;
	m_pID_TEXT_Dodge = NULL;
	
	__ZERO(m_arStateBtns);

}
// Frame
BOOL UIBaseProperty::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIBaseProperty::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// Button
BOOL UIBaseProperty::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	//SetVisible( FALSE );
	SetLayoutVisible();
	return TRUE;
}

// Button
BOOL UIBaseProperty::ID_BUTTON_UPGRADEOnButtonClick( VUCtrlObject* pSender )
{
	if(pSender->GetGroupID() == STATE_GROUP)
	{
		ProcessStateBtn(pSender->GetIndexAtGroup());
	}
	return TRUE;
}

// Button
BOOL UIBaseProperty::ID_BUTTON_ActionOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	SetLayoutVisible();
	theUIAction.SetLayoutVisible();
	//UIAction.SetVisible( TRUE );
	INT x,y;
	m_pID_FRAME_BaseProperty->GetPos(x,y);
	theUIAction.m_pID_FRAME_ActionUI->SetPos(x,y);

	
	return TRUE;
	
}


BOOL UIBaseProperty::ProcessStateBtn	(UINT nIndex)
{
	__CHECK_RANGE(nIndex, UI_ATTRIBUTE_MAX);

	__CHECK(singleton::ExistHero());
	__CHECK(theHero.GetCharInfo()->m_dwRemainStat > 0);

	MSG_CG_STATUS_STAT_SELECT_SYN	 msgStat;
	msgStat.m_AttrType	= (BYTE)nIndex + ATTRIBUTE_STAT_BASE;

	theHero.SendPacket(&msgStat,sizeof(msgStat));

	return TRUE;
}

void UIBaseProperty::UpdateStateBtns()
{
	if(!singleton::ExistHero())
		return;

	DWORD dwStates;

	dwStates = theHero.GetCharInfo()->m_dwRemainStat;
	for(INT n=0; n<UI_ATTRIBUTE_MAX; n++)
	{
		if(m_arStateBtns[n] == NULL)
			continue;
		m_arStateBtns[n]->SetVisible(dwStates > 0);
	}
}

// 装载UI
BOOL UIBaseProperty::LoadUI()
{
	GetUIListener().RegisterMe();

	m_pID_FRAME_BaseProperty = theUICtrlManager.LoadFrame( UIDOC_PATH( "Property") );
	if ( m_pID_FRAME_BaseProperty == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Property") "]失败")
			return FALSE;
	}

	theUILayoutManager.AddFrame(m_pID_FRAME_BaseProperty);

	return InitControls();
}


BOOL UIBaseProperty::InitControls()
{
	__VERIFY_PTR(m_pID_FRAME_BaseProperty
					,"BaseProperty Frame无效...\n");

	m_pID_FRAME_BaseProperty->SetProcOnFrameMove		(theUIBasePropertyOnFrameMove);
	m_pID_FRAME_BaseProperty->SetProcOnRender	(theUIBasePropertyOnRender, FALSE);

	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)m_pID_FRAME_BaseProperty->FindControl( ID_BUTTON_CLOSE );
	m_pID_BUTTON_Action		= (VUCtrlButton*)m_pID_FRAME_BaseProperty->FindControl( ID_BUTTON_Action );

	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_BUTTON_Action );

	m_pID_BUTTON_CLOSE	->SetProcOnButtonClick(theUIBasePropertyID_BUTTON_CLOSEOnButtonClick);
	m_pID_BUTTON_Action	->SetProcOnButtonClick(theUIBasePropertyID_BUTTON_ActionOnButtonClick);



	m_pID_TEXT_Name			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Name );
	m_pID_TEXT_Title			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Title );
	m_pID_TEXT_Guild			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Guild );
	m_pID_TEXT_Level			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Level );
	m_pID_TEXT_INT				= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_INT );
	m_pID_TEXT_VIT = (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_VIT );
	m_pID_TEXT_STR		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_STR );
	m_pID_TEXT_DEX		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_DEX );
	m_pID_TEXT_KilledNum		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_KilledNum );
	m_pID_TEXT_Work			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Work );
	m_pID_TEXT_Official		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Official );
	m_pID_TEXT_Marry			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Marry );
	m_pID_TEXT_Distinction	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Distinction );
	m_pID_BUTTON_Action		= (VUCtrlButton*)m_pID_FRAME_BaseProperty->FindControl( ID_BUTTON_Action );
	m_pID_TEXT_SPR			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_SPR );
	m_pID_TEXT_LRN			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_LRN );
	m_pID_TEXT_CRE			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_CRE );
	m_pID_TEXT_EXPERTY1	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_EXPERTY1 );
	m_pID_TEXT_EXPERTY2	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_EXPERTY2 );
	m_pID_TEXT_REMAINSTAT= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_REMAINSTAT );


	m_pID_TEXT_MATTACK_FIRE	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MATTACK_FIRE );
	m_pID_TEXT_MDEF_FIRE		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MDEF_FIRE );
	m_pID_TEXT_MATTACK_WATER= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MATTACK_WATER );
	m_pID_TEXT_MDEF_WATER	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MDEF_WATER );
	m_pID_TEXT_MATTACK_WOOD	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MATTACK_WOOD );
	m_pID_TEXT_MDEF_WOOD		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MDEF_WOOD );
	m_pID_TEXT_MATTACK_GOLD	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MATTACK_GOLD );
	m_pID_TEXT_MDEF_GOLD		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MDEF_GOLD );
	m_pID_TEXT_MATTACK_EARTH= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MATTACK_EARTH );
	m_pID_TEXT_MDEF_EARTH	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MDEF_EARTH );
	m_pID_TEXT_Hp				= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Hp );
	m_pID_TEXT_Mp				= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Mp );
	m_pID_TEXT_PHYATTACK		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_PHYATTACK );
	m_pID_TEXT_PHYDEF			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_PHYDEF );
	m_pID_TEXT_MAGIC_DEF		= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MAGIC_DEF );
	m_pID_TEXT_MAGICATTACK	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_MAGICATTACK );
	m_pID_TEXT_RangeAttack	= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_RangeAttack );
	m_pID_TEXT_Hit				= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Hit );
	m_pID_TEXT_Dodge			= (VUCtrlText*)m_pID_FRAME_BaseProperty->FindControl( ID_TEXT_Dodge );


	for(INT n=0; n<UI_ATTRIBUTE_MAX; n++)
	{
		m_arStateBtns[n] = (VUCtrlButton*)m_pID_FRAME_BaseProperty->FindControl( FMSTR(ID_BUTTON_UPGRADE _T("%d"), n) );
		assert( m_arStateBtns[n] );
		if(m_arStateBtns[n])
		{
			m_arStateBtns[n]->SetGroupID(STATE_GROUP);
			m_arStateBtns[n]->SetIndexAtGroup(n);
			m_arStateBtns[n]->SetProcOnButtonClick(theUIBasePropertyID_BUTTON_UPGRADEOnButtonClick);
		}
	}

	m_pID_FRAME_BaseProperty->SetVisible(FALSE);
	m_pID_FRAME_BaseProperty->SetVisible(FALSE);

	assert( m_pID_TEXT_Name );
	assert( m_pID_TEXT_Title );
	assert( m_pID_TEXT_Guild );
	assert( m_pID_TEXT_Level );
	assert( m_pID_TEXT_INT );
	assert( m_pID_TEXT_VIT );
	assert( m_pID_TEXT_STR );
	assert( m_pID_TEXT_DEX );
	assert( m_pID_TEXT_KilledNum );
	assert( m_pID_TEXT_Work );
	assert( m_pID_TEXT_Official );
	assert( m_pID_TEXT_Marry );
	assert( m_pID_TEXT_Distinction );
	assert( m_pID_TEXT_SPR );
	assert( m_pID_TEXT_LRN );
	assert( m_pID_TEXT_CRE );
	assert( m_pID_TEXT_EXPERTY1 );
	assert( m_pID_TEXT_EXPERTY2 );
	assert( m_pID_TEXT_REMAINSTAT );

	assert( m_pID_TEXT_MATTACK_FIRE );
	assert( m_pID_TEXT_MDEF_FIRE );
	assert( m_pID_TEXT_MATTACK_WATER );
	assert( m_pID_TEXT_MDEF_WATER );
	assert( m_pID_TEXT_MATTACK_WOOD );
	assert( m_pID_TEXT_MDEF_WOOD );
	assert( m_pID_TEXT_MATTACK_GOLD );
	assert( m_pID_TEXT_MDEF_GOLD );
	assert( m_pID_TEXT_MATTACK_EARTH );
	assert( m_pID_TEXT_MDEF_EARTH );
	assert( m_pID_TEXT_Hp );
	assert( m_pID_TEXT_Mp );
	assert( m_pID_TEXT_PHYATTACK );
	assert( m_pID_TEXT_PHYDEF );
	assert( m_pID_TEXT_MAGIC_DEF );
	assert( m_pID_TEXT_MAGICATTACK );
	assert( m_pID_TEXT_RangeAttack );
	assert( m_pID_TEXT_Hit );
	assert( m_pID_TEXT_Dodge );

	UISCRIPT_ENABLE ( UIOBJ_BASEPROPERTY, m_pID_FRAME_BaseProperty );


	return TRUE;
}
// 卸载UI
BOOL UIBaseProperty::UnLoadUI()
{
	UISCRIPT_DISABLE( UIOBJ_BASEPROPERTY );
	GetUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Property") );
}
// 是否可视
BOOL UIBaseProperty::IsVisible()
{
	
	return m_pID_FRAME_BaseProperty->IsVisible();
	
}
// 设置是否可视
void UIBaseProperty::SetVisible( BOOL bVisible )
{
	
	//VUIControl::SetVisible( bVisible );
	//m_pID_FRAME_BaseProperty->SetVisible( bVisible );
	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_BaseProperty->SetVisible( bVisible );
	
}
void UIBaseProperty::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_BaseProperty);
}




void UIBaseProperty::Refresh()
{
	if(!singleton::ExistHero())
		return;

	UpdateStateBtns();

	sCHARINFO*	pProf;


	sPLAYERINFO_BASE*	pBaseInfo	= theHero.GetCharInfo();
	PlayerAttributes*	pAttr			= theHero.GetPlayerAttributes();


	*m_pID_TEXT_Guild = pBaseInfo->m_szGuildName;
	*m_pID_TEXT_Name	= pBaseInfo->m_szCharName;
	*m_pID_TEXT_Title = pBaseInfo->m_szTitleID ;

	*m_pID_TEXT_Level = FMSTR("%d", pBaseInfo->m_LV);

	 pProf				= theCharInfoParser.GetCharInfo((ePLAYER_TYPE)pBaseInfo->m_byClassCode);
	*m_pID_TEXT_Work	= (LPCSTR)pProf->m_NameShow;
	
	*m_pID_TEXT_VIT			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_VIT]);
	*m_pID_TEXT_STR			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_STR]);
	*m_pID_TEXT_INT			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_INT]);
	*m_pID_TEXT_DEX			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_DEX]);
	*m_pID_TEXT_SPR			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_SPR]);
	*m_pID_TEXT_LRN			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_LRN]);
	*m_pID_TEXT_CRE			= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_CRE]);
	*m_pID_TEXT_EXPERTY1		= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_EXPERTY1]);
	*m_pID_TEXT_EXPERTY2		= UIUtil::GetShowText( (*pAttr)[ATTRIBUTE_EXPERTY2]);
	*m_pID_TEXT_REMAINSTAT	= FMSTR("%d", pBaseInfo->m_dwRemainStat);

	*m_pID_TEXT_Hp			= UIUtil::GetShowText( theHero.GetHP(), theHero.GetMaxHP());
	*m_pID_TEXT_Mp			= UIUtil::GetShowText( theHero.GetMP(), theHero.GetMaxMP());


	//////////////////////////////////////////
	int iMinPhyAttackPower		= pAttr->GetOptionPhysicalAttPower();
	int iMaxPhyAttackPower		= pAttr->GetOptionPhysicalAttPower();
	int iMinRangeAttackPower	= pAttr->GetOptionPhysicalAttPower();
	int iMaxRangeAttackPower	= pAttr->GetOptionPhysicalAttPower();
	int iMinMagicAttackPower	= pAttr->GetBaseMagicMinAttPower();
	int iMaxMagicAttackPower	= pAttr->GetBaseMagicMaxAttPower();

	iMinPhyAttackPower	+= pAttr->GetBaseMeleeMinAttPower();
	iMaxPhyAttackPower	+= pAttr->GetBaseMeleeMaxAttPower();
	iMinRangeAttackPower += pAttr->GetBaseRangeMinAttPower();
	iMaxRangeAttackPower += pAttr->GetBaseRangeMaxAttPower();

	iMinMagicAttackPower += pAttr->GetOptionMagicAttPower();
	iMaxMagicAttackPower += pAttr->GetOptionMagicAttPower();
	//iMinMagicAttackPower += pAttr->GetMagicalAttackPower( eAttackType );
	//iMaxMagicAttackPower += pAttr->GetMagicalAttackPower( eAttackType );


	*m_pID_TEXT_PHYATTACK			= FMSTR("%d-%d",iMinPhyAttackPower, iMaxPhyAttackPower);
	*m_pID_TEXT_MAGICATTACK			= FMSTR("%d-%d",iMinMagicAttackPower, iMaxMagicAttackPower);
	*m_pID_TEXT_RangeAttack	= FMSTR("%d-%d",iMinRangeAttackPower, iMaxRangeAttackPower);


	//////////////////////////////////////////
	int iPhyDefPower		= pAttr->GetOptionPhysicalDefPower();
	int iRangeDefPower	= pAttr->GetOptionPhysicalDefPower();
	int iMagicDefPower	= pAttr->GetOptionMagicDefPower();

	iPhyDefPower			+= pAttr->GetBaseMeleeDefPower();
	iRangeDefPower			+= pAttr->GetBaseRangeDefPower();
	iMagicDefPower			+= pAttr->GetBaseMagicDefPower();

	*m_pID_TEXT_PHYDEF		= UIUtil::GetShowText(iPhyDefPower, iPhyDefPower);
	*m_pID_TEXT_MAGIC_DEF	= UIUtil::GetShowText(iMagicDefPower, iMagicDefPower);


	//////////////////////////////////////////
	*m_pID_TEXT_Hit			= FMSTR("%d+(%d%%)"	,pAttr->GetPhysicalAttackRate() 
																,pAttr->GetPhysicalAttackRatePer() );
	*m_pID_TEXT_Dodge			= FMSTR("%d+(%d%%)"	,pAttr->GetPhysicalAvoidRate()
																,pAttr->GetPhysicalAvoidRatePer());


	/////////////////////////////////////////////////
	//五行攻防
	VUCtrlText*	arMDefs[]=
	{
		 m_pID_TEXT_MDEF_GOLD
		,m_pID_TEXT_MDEF_WATER
		,m_pID_TEXT_MDEF_WOOD
		,m_pID_TEXT_MDEF_FIRE
		,m_pID_TEXT_MDEF_EARTH
	};
	VUCtrlText*	arMAttacks[]=
	{
		 m_pID_TEXT_MATTACK_GOLD
		,m_pID_TEXT_MATTACK_WATER
		,m_pID_TEXT_MATTACK_WOOD
		,m_pID_TEXT_MATTACK_FIRE
		,m_pID_TEXT_MATTACK_EARTH
	};

	for(INT n=ATTACKKIND_GOLD; n<=ATTACKKIND_EARTH; n++)
	{
		INT				nIndex	= n - ATTACKKIND_GOLD;
		eATTACK_KIND	attack	= (eATTACK_KIND)n;
		eATTACK_KIND	oppresed	= theSeriesInfoParser.GetSeriesOppressed(attack);
		*arMDefs[nIndex]			= pAttr->GetMagicalDefense(oppresed);
		*arMAttacks[nIndex]		= pAttr->GetMagicalAttackPower(attack);
	}


}

};//namespace gameui

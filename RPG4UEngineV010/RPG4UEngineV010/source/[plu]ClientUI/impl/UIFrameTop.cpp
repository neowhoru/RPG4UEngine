/*////////////////////////////////////////////////////////////////////////
文 件 名：UIFrameTop.cpp
创建日期：2007年11月26日
最后更新：2007年11月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIFrameTop.h"
#include "ApplicationSetting.h"
#include "VUIFontDDraw.h"
#include "FuncProfiler.h"
#include "GraphicSetting.h"
#include "UIFrameTopUIListener.h"
#include "GlobalInstancePriority.h"

#define	OFFSET_Y	2

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::UIFrameTopUIListener& GetUIFrameTopUIFrameTopUIListener()
{
	static gameui::UIFrameTopUIListener staticUIFrameTopUIListener;
	return staticUIFrameTopUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIFrameTop, ()  , gamemain::eInstPrioClientUI/*UIFrameTop*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIFrameTop, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIFrameTop, OnRender )
};//namespace uicallback
using namespace uicallback;


UIFrameTop::UIFrameTop()
{
	
	// Member
	m_pID_FRAME_FRAMETOP	= NULL;
	m_pID_TEXT_TITLE		= NULL;
	
}

// Frame
BOOL UIFrameTop::OnFrameMove(DWORD /*dwTick*/)
{
	if ( m_MoveInfo.m_nCount > 0 )
	{
		RECT rc;
		m_pID_FRAME_FRAMETOP->GetGlobalRect( &rc );
		m_MoveInfo.m_nY = rc.top + OFFSET_Y;

		int nUseTime	= base::GetRunTime() - m_MoveInfo.m_dwStartTime;
		int nMove		= (INT)(nUseTime*m_MoveInfo.m_fMoveSpeed);

		m_MoveInfo.m_nX = rc.right - nMove;
		//
		if ( m_MoveInfo.m_nX < rc.left-m_MoveInfo.m_nWidth )
		{
			m_MoveInfo.m_nCount--;
			m_MoveInfo.m_dwStartTime = base::GetRunTime();
			if ( m_MoveInfo.m_nCount == 0 )
			{
				SetControlVisable( TRUE );
			}
		}
	}
	return TRUE;
}

BOOL UIFrameTop::OnRender(DWORD /*dwTick*/)
{
	
	if ( m_MoveInfo.m_nCount > 0 )
	{
		DrawText	(m_MoveInfo.m_nX
               ,m_MoveInfo.m_nY
					,m_MoveInfo.m_strText.c_str()
					,m_MoveInfo.m_dwColor
					,m_pID_FRAME_FRAMETOP->GetFontIndex());
	}
	return TRUE;
}


// 装载UI
BOOL UIFrameTop::LoadUI()
{
	
	GetUIFrameTopUIFrameTopUIListener().RegisterMe();

	m_pID_FRAME_FRAMETOP = theUICtrlManager.LoadFrame( UIDOC_PATH( "FrameTop"), TRUE, TRUE );
	if ( m_pID_FRAME_FRAMETOP == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "FrameTop")"]失败")
			return FALSE;
	}

	return InitControls();
	
}


// 关连控件
BOOL UIFrameTop::InitControls()
{
	m_pID_TEXT_TITLE		= (VUCtrlText*)m_pID_FRAME_FRAMETOP->FindControl(  ID_TEXT_TITLE );
	m_pID_SCREENTITLE		= m_pID_FRAME_FRAMETOP->FindControl(  ID_SCREENTITLE );
	m_pID_SCREENTEXT		= m_pID_FRAME_FRAMETOP->FindControl(  ID_SCREENTEXT );

	assert( m_pID_TEXT_TITLE );
	assert( m_pID_SCREENTITLE);
	assert( m_pID_SCREENTEXT );
	
	m_pID_FRAME_FRAMETOP->SetProcOnFrameMove	(theUIFrameTopOnFrameMove );
	m_pID_FRAME_FRAMETOP->SetProcOnRender		(theUIFrameTopOnRender );


	m_pID_FRAME_FRAMETOP->SetWidth( SCREEN_WIDTH );

	*m_pID_TEXT_TITLE = FMSTR("%s[%d]",_GAMENAME_,_GAMEVERSION_);

	m_pID_SCREENTEXT	->SetVisible(FALSE);
	m_pID_SCREENTITLE	->SetVisible(FALSE);
	return TRUE;
	
}
// 卸载UI
BOOL UIFrameTop::UnLoadUI()
{
	
	GetUIFrameTopUIFrameTopUIListener().UnregisterMe();

	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "FrameTop") );
	
}
// 是否可视
BOOL UIFrameTop::IsVisible()
{
	

	return m_pID_FRAME_FRAMETOP->IsVisible();
	
}
// 设置是否可视
void UIFrameTop::SetVisible( BOOL bVisible )
{
	

	m_pID_FRAME_FRAMETOP->SetVisible( bVisible );
	
}


void UIFrameTop::SetMoveText( const char *szText, int nCount )
{
	
	assert( szText );
	if( !szText )
	{
		return;
	}
	int nLen = strlen( szText );
	if ( nLen <= 0 )
		return;
	RECT rc;
	m_pID_FRAME_FRAMETOP->GetGlobalRect( &rc );
	m_MoveInfo.m_strText		= szText;
	m_MoveInfo.m_nCount		= nCount;
	m_MoveInfo.m_nWidth		= nLen*m_pID_FRAME_FRAMETOP->GetFontSize()/2;
	m_MoveInfo.m_nX				= rc.right;
	m_MoveInfo.m_nY				= rc.top + OFFSET_Y;
	m_MoveInfo.m_fMoveSpeed	= 0.05f;
	m_MoveInfo.m_dwStartTime = base::GetRunTime();
	//
	SetControlVisable( FALSE );
	
}


void UIFrameTop::SetControlVisable( BOOL b )
{
	
	m_pID_TEXT_TITLE->SetVisible( b );
	
}


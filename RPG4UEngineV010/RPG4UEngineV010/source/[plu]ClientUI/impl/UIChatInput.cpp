/*////////////////////////////////////////////////////////////////////////
文 件 名：UIChatInfoBox.cpp
创建日期：2007年12月24日
最后更新：2007年12月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIChatInput.h"
#include "ColorManager.h"
#include "TableTextFile.h"
#include "UIExitGame.h"
#include "VUILuaScript.h"
#include "VUILuaFuns.h"
#include "VUILuaScriptManager.h"
#include "TextResManager.h"
#include "ScriptWord.h"
#include "tinyxml.h"
#include "UIMenuPlayer.h"
#include "UICharCreate.h"
#include "TextFilterManager.h"
#include "UISystemLog.h"
#include "UIMenuHero.h"
#include "UIMessageBox.h"
#include "ChatInputUIListener.h"
#include "VHero.h"
#include "PathInfoParser.h"
#include "GlobalInstancePriority.h"
#include "ConstTextRes.h"
#include "NetworkLayer.h"
#include "PacketInclude.h"
#include "TextResManager.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIChatInput, ()  , gamemain::eInstPrioClientUI/*ePriorityNormal*/);

////////////////////////////////////////////////////////////////////////
#define	CHAT_PRIVATE_NIL		_STRING(TEXTRES_CHAT_PRIFIX_PRIVATE_NIL)//_T("/to ")
#define	CHAT_PRIVATE_TO		_STRING(TEXTRES_CHAT_PRIFIX_PRIVATE_TO)	//_T("/to %s ")
#define	CHAT_GUILD				_STRING(TEXTRES_CHAT_PRIFIX_GUILD)		//_T("/guild ")
#define	CHAT_TEAM				_STRING(TEXTRES_CHAT_PRIFIX_TEAM)			//_T("/party ")	


////////////////////////////////////////////////////////////////////////
static gameui::ChatInputUIListener& GetUIListener()
{
	static gameui::ChatInputUIListener s;
	return s;
}


#define ChatInfoBox				"ChatInfoBox"
#define ID_LIST_INFO			"ID_LIST_INFO"
#define ID_EDIT_INPUT			"ID_EDIT_INPUT"
#define ID_BUTTON_ChatGuild		"ID_BUTTON_ChatGuild"
#define ID_BUTTON_ChatGroup		"ID_BUTTON_ChatGroup"
#define ID_BUTTON_ChatPublic	"ID_BUTTON_ChatPublic"
#define ID_BUTTON_ChatPrivate	"ID_BUTTON_ChatPrivate"

#define ID_BUTTON_face			"ID_BUTTON_face"
#define ID_BUTTON_TurnPage		"ID_BUTTON_TurnPage"
#define ID_BUTTON_ChatOutType	"ID_BUTTON_ChatOutType"

#define ID_CHECKBOX_Normal		"ID_CHECKBOX_Normal"
#define ID_CHECKBOX_Team		"ID_CHECKBOX_Team"
#define ID_CHECKBOX_Guild		"ID_CHECKBOX_Guild"
#define ID_CHECKBOX_Player		"ID_CHECKBOX_Player"

#define ID_PICTURE_10574		"ID_PICTURE_10574"
#define ID_PICTURE_3095			"ID_PICTURE_3095"


#define DEFAULT_LISTINFO_SHOWNUM	7
#define WIDTH_LIST_SCROLLBAR		16
#define MAX_CHAT_COUNT				60

const TIMER_TYPE COLD_TIME		= 10*1000;
const TIMER_TYPE REPEAT_START	= 3*1000;

#define XML_COLDTIME_PATH	_PATH_SETTING("UIChatInfo.txt")



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIChatInput, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIChatInput, OnRender )
	UIPROC_EDIT_ENTER( theUIChatInput, ID_EDIT_INPUTOnEditEnter )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_ChatGuildOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_ChatGroupOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_ChatPublicOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_ChatPrivateOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_MoreHieghtOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_LessHightOnButtonClick )
	UIPROC_CHECK_BOX_CHECK( theUIChatInput, ID_CHECKBOX_NormalOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIChatInput, ID_CHECKBOX_TeamOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIChatInput, ID_CHECKBOX_GuildOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIChatInput, ID_CHECKBOX_PlayerOnCheckBoxCheck )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_ChatOutTypeOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIChatInput, ID_BUTTON_TurnPageOnButtonClick)
};//namespace uicallback
using namespace uicallback;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIChatInput::UIChatInput()
:VUIControl(gameui::eUIChatInfoBox)
{
	
	// Member
	m_pChatInfoBox = NULL;
	m_pID_INFOLIST = NULL;
	m_pID_EDIT_INPUT = NULL;
	m_pID_BUTTON_ChatGuild = NULL;
	m_pID_BUTTON_ChatGroup = NULL;
	m_pID_BUTTON_ChatPublic = NULL;
	m_pID_BUTTON_ChatPrivate = 	NULL;
	m_pID_BUTTON_TurnPage = NULL;
	m_pID_CHECKBOX_Normal	= NULL;
	m_pID_CHECKBOX_Team		= NULL;
	m_pID_CHECKBOX_Guild	= NULL;
	m_pID_CHECKBOX_Player	= NULL;
	m_pID_BUTTON_ChatOutType = NULL;
	m_nCurrentLatest				= -1;
	m_bEditActive				= FALSE;
	m_bActiveInput				= FALSE;

	m_nChannelDefault	= gameui::CHAT_TYPE_NORMAL;
	m_nChannelCurrent	= gameui::CHAT_TYPE_NORMAL;

	m_szPrivateTarget.Clear();
	//clear LastInfo
	for( int i = 0; i < LAST_INFO_MAX; i++ )
	{
		m_ChatHistory[i].clear();
	}
	m_nCurrentPrivateAmount = 0;




	
}

// Frame
BOOL UIChatInput::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIChatInput::OnRender(DWORD /*dwTick*/)
{
	
	//渐渐隐藏
	//	FadeFlash();
	//FadeColor();
	//
	if(m_pID_EDIT_INPUT->IsActivated())
		return TRUE;

	RECT rt = {0}, OtherRt = {0};
	m_pChatInfoBox->GetGlobalRect( &rt );

	if( PtInRect( &rt, m_ptMouseMove ) || PtInRect( &OtherRt, m_ptMouseMove ) )
	{
		static DWORD dwStartTime = timeGetTime();
		static DWORD dwEndTime = 0;
		DWORD dwDelayTime  = 0;

		dwEndTime = timeGetTime();
		dwDelayTime = dwEndTime - dwStartTime;//timeGetTime();

		if( dwDelayTime > 1000 )
		{
			dwStartTime = dwEndTime;
		}			
	}	
	else
	{
		static DWORD dwStartTime = timeGetTime();
		static DWORD dwEndTime = 0;
		DWORD dwDelayTime  = 0;

		dwEndTime = timeGetTime();
		dwDelayTime = dwEndTime - dwStartTime;//timeGetTime();

		if( dwDelayTime > 1000 )
		{	
			dwStartTime = dwEndTime;
		}
	}

	return TRUE;	
	
}


void UIChatInput::AddPrivateUser(LPCSTR szUser)
{
	
	_ReadyPrivate(szUser);

	BOOL  bRepeat = FALSE;
	for(PrivateInfoArrayIt it = m_PrivateInfos.begin(); it != m_PrivateInfos.end(); ++it)
	{
		if(szUser == *it)
			bRepeat = TRUE;
	}

	if( !bRepeat)
	{
		if(m_PrivateInfos.size() < LAST_INFO_MAX)
		{
			m_PrivateInfos.push_back(szUser);
		}
		else
		{
			for(UINT it=0; it<m_PrivateInfos.size()-1; ++it)
			{
				m_PrivateInfos[it] = m_PrivateInfos[it+1];
			}
			m_PrivateInfos.pop_back();
			m_PrivateInfos.push_back(szUser);
		}
	}

	for(PrivateInfoArrayIt it = m_PrivateInfos.begin(); it != m_PrivateInfos.end(); ++it)
	{
		sUILIST_ITEM	stItem;
		stItem.SetData( (*it).c_str() );
	}
	
}

// Edit
void UIChatInput::ID_EDIT_INPUTOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR				szData )
{
	
	if( GetTextFilterManager().ExistFilterWord(szData) )
	{
		AddInfo( WARN_STRING );
		return;
	}

	if(!m_bActiveInput )
		return;

	//////////////////////////////////////
	if( szData[0] != '/')
	{
		if(!m_NormalLastTimer.IsExpired())
			goto laEnd;
	}
	
	/////////////////////////////////////////////
	if ( szData[0] != 0 )
	{
		VUILuaScript *pScript = (VUILuaScript*)theLuaScriptManager.GetUIScript(SCRIPTFILE_CHATSCRIPT);
		if( pScript )
		{
			pScript->GetGlobalFunction(LUA_GLOBAL_UIPROC_ONCHATENTER)(szData);
		}
	}

	/////////////////////////////////////////////
	for( int n=LAST_INFO_MAX-1; n>=1; n-- )
	{
		m_ChatHistory[n] = m_ChatHistory[n-1];
	}
	m_ChatHistory[0]	= szData;
	m_nCurrentLatest	= -1;
	*m_pID_EDIT_INPUT = "";
	
laEnd:
	m_pID_EDIT_INPUT->SetVisible( FALSE );
	m_pID_EDIT_INPUT->SetActivate( FALSE );
	_SetInputWidget(FALSE);
	// Disable IME (input method editor) 
	ImmAssociateContext( g_hWndMain, NULL );
}

// CheckBox
void UIChatInput::ID_CHECKBOX_NormalOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	//ChangeChatChannel();
	
}
// CheckBox
void UIChatInput::ID_CHECKBOX_TeamOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	//ChangeChatChannel();
	
}
// CheckBox
void UIChatInput::ID_CHECKBOX_GuildOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	//ChangeChatChannel();
	
}
// CheckBox
void UIChatInput::ID_CHECKBOX_PlayerOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	//ChangeChatChannel();
	
}
// Button
BOOL UIChatInput::ID_BUTTON_ChatOutTypeOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	if(m_pID_BUTTON_ChatGuild->IsVisible() && m_pID_BUTTON_ChatGroup->IsVisible() && m_pID_BUTTON_ChatPublic->IsVisible())
	{
		_SetChatShowType(FALSE);
	}
	else
	{
		_SetChatShowType(TRUE);
	}

	return TRUE;
	
}

// Button
BOOL UIChatInput::ID_BUTTON_MoreHieghtOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	int nHeight = m_pID_INFOLIST->GetListItemHeight();
	RECT rcItem = {0};
	m_pChatInfoBox->GetGlobalRect( &rcItem );

	if(m_pID_INFOLIST->GetHeight() < nHeight*10)
	{
		m_pID_INFOLIST->SetHeight(nHeight*10);
		m_pID_INFOLIST->SetShowMaxCount(10);
		m_pID_INFOLIST->SetPosY( rcItem.top - nHeight*(10 - DEFAULT_LISTINFO_SHOWNUM) );
		m_pID_INFOLIST->SetScrollPos( -WIDTH_LIST_SCROLLBAR, 0 );
		m_pID_INFOLIST->SetScrollHeight(nHeight*10);
	}
	else if(m_pID_INFOLIST->GetHeight() == nHeight*10)
	{
		m_pID_INFOLIST->SetHeight(nHeight*15);
		m_pID_INFOLIST->SetShowMaxCount(15);
		m_pID_INFOLIST->SetPosY( rcItem.top -  nHeight*8);
		m_pID_INFOLIST->SetScrollPos( -WIDTH_LIST_SCROLLBAR,0);
		m_pID_INFOLIST->SetScrollHeight(nHeight*15);
	}
	else
	{
		m_pID_INFOLIST->SetHeight(nHeight*DEFAULT_LISTINFO_SHOWNUM);
		m_pID_INFOLIST->SetShowMaxCount(DEFAULT_LISTINFO_SHOWNUM);
		m_pID_INFOLIST->SetPosY( rcItem.top );
		m_pID_INFOLIST->SetScrollPos( -WIDTH_LIST_SCROLLBAR, 0);
		m_pID_INFOLIST->SetScrollHeight(nHeight*DEFAULT_LISTINFO_SHOWNUM);
	}
	return TRUE;
	
}


BOOL UIChatInput::ID_BUTTON_TurnPageOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	ID_BUTTON_MoreHieghtOnButtonClick(NULL);
	return TRUE;
}


// Button
BOOL UIChatInput::ID_BUTTON_LessHightOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	int nHeight = m_pID_INFOLIST->GetListItemHeight();
	RECT rcItem = {0};
	m_pID_INFOLIST->GetGlobalRect( &rcItem );

	if(m_pID_INFOLIST->GetHeight() == nHeight*15)
	{
		m_pID_INFOLIST->SetHeight(nHeight*10);
		m_pID_INFOLIST->SetShowMaxCount(10);
		m_pID_INFOLIST->SetPosY( rcItem.top +  nHeight*5);
		m_pID_INFOLIST->SetScrollPos( -WIDTH_LIST_SCROLLBAR, nHeight*5);
		m_pID_INFOLIST->SetScrollHeight(nHeight*10);
	}
	else if(m_pID_INFOLIST->GetHeight() == nHeight*10)
	{
		m_pID_INFOLIST->SetHeight(nHeight*DEFAULT_LISTINFO_SHOWNUM);
		m_pID_INFOLIST->SetShowMaxCount(DEFAULT_LISTINFO_SHOWNUM);
		m_pID_INFOLIST->SetPosY( rcItem.top + nHeight*(10 - DEFAULT_LISTINFO_SHOWNUM));
		m_pID_INFOLIST->SetScrollPos( -WIDTH_LIST_SCROLLBAR, nHeight*(10 - DEFAULT_LISTINFO_SHOWNUM));
		m_pID_INFOLIST->SetScrollHeight(nHeight*DEFAULT_LISTINFO_SHOWNUM);
	}	

	return TRUE;
	
}

// Button
BOOL UIChatInput::ID_BUTTON_ChatPublicOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_nChannelDefault = gameui::CHAT_TYPE_NORMAL;
	SetChatSendType("全体");
	m_pID_EDIT_INPUT->SetVisible( TRUE );
	m_pID_EDIT_INPUT->SetActivate( TRUE );
	m_pID_EDIT_INPUT->SetDefaultColor(AREA_BROADCAST_COLOR);
	SetInputText("");
	theUICtrlManager.BringToTop(m_pChatInfoBox);
	SetEditFocus();
	theUICtrlManager.UpdateControls();

	_SetChatShowType(FALSE);

	ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );
	return TRUE;
	
}
// Button
BOOL UIChatInput::ID_BUTTON_ChatGuildOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_nChannelDefault = gameui::CHAT_TYPE_GUILD;	
	SetChatSendType("公会");
	m_pID_EDIT_INPUT->SetVisible( TRUE );
	m_pID_EDIT_INPUT->SetActivate( TRUE );
	m_pID_EDIT_INPUT->SetDefaultColor(AREA_BROADCAST_COLOR);
	SetInputText(CHAT_GUILD);
	theUICtrlManager.BringToTop(m_pChatInfoBox);
	SetEditFocus();
	theUICtrlManager.UpdateControls();

	_SetChatShowType(FALSE);

	ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );
	return TRUE;
	
}


// Button
BOOL UIChatInput::ID_BUTTON_ChatGroupOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_nChannelDefault = gameui::CHAT_TYPE_TERM;
	SetChatSendType("队伍");
	m_pID_EDIT_INPUT->SetVisible( TRUE );
	m_pID_EDIT_INPUT->SetActivate( TRUE );
	m_pID_EDIT_INPUT->SetDefaultColor(TEAM_CHAT_COLOR);
	SetInputText(CHAT_TEAM);
	theUICtrlManager.BringToTop(m_pChatInfoBox);
	SetEditFocus();
	theUICtrlManager.UpdateControls();

	_SetChatShowType(FALSE);

	ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );
	return TRUE;
	
}
BOOL UIChatInput::ID_BUTTON_ChatPrivateOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_nChannelDefault = gameui::CHAT_TYPE_PRIVATE;
	SetChatSendType("私聊");
	m_pID_EDIT_INPUT->SetVisible( TRUE );
	m_pID_EDIT_INPUT->SetActivate( TRUE );
	m_pID_EDIT_INPUT->SetDefaultColor(PRIVATE_CHAT_COLOR);
	SetInputText(CHAT_PRIVATE_NIL);
	theUICtrlManager.BringToTop(m_pChatInfoBox);
	SetEditFocus();
	theUICtrlManager.UpdateControls();

	_SetChatShowType(FALSE);

	ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );
	return TRUE;
	
}


void UIChatInput::_BeginInput()
{
	ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );

	static VUCtrlObject *pLastObject = NULL;
	if ( theUICtrlManager.GetFocus() == m_pID_EDIT_INPUT )
	{
		m_pID_BUTTON_ChatOutType->SetVisible( FALSE );
		_SetInputWidget(FALSE);
		if(m_bEditActive)
			m_bEditActive = !m_bEditActive;
	}
	else
	{
		pLastObject = theUICtrlManager.BringToTop( m_pChatInfoBox );
		m_pID_EDIT_INPUT->SetActivate(TRUE);
		m_pID_EDIT_INPUT->SetVisible(TRUE);
		_SetInputWidget(TRUE);
		if( !m_bEditActive)
			m_bEditActive = !m_bEditActive;
	}
	
}


BOOL UIChatInput::MsgProc( UINT msg, WPARAM wParam, LPARAM lParam, BOOL bMsgUsed )
{
	

	if ( (bMsgUsed == TRUE && WM_MOUSEMOVE != msg))
		return FALSE;
	switch( msg ) 
	{
		case WM_LBUTTONDOWN:
		{
			break;
		}
		case WM_MOUSEMOVE:
		{
			POINT pt = {0};
			pt.x = LOWORD( lParam );
			pt.y = HIWORD( lParam );
			m_ptMouseMove = pt;

			break;
		}
		case WM_KEYDOWN:
		{
			switch( wParam ) 
			{
				case VK_RETURN:
				{
					VUCtrlObject*	pFocus = theUICtrlManager.GetFocus();

					////////////////////////////////////
					if ( pFocus == m_pID_EDIT_INPUT )
					{
						m_bActiveInput = TRUE;

						return FALSE;
					}
					//else if(theUICtrlManager.GetFocus() !=m_pID_EDIT_INPUT )//== NULL)
					else if(		pFocus == NULL 
								||	pFocus->GetControlType() != Type_Edit)
					{
						m_bActiveInput = FALSE;
						
						_BeginInput();

						if(!m_bEditActive)
						{
							m_pID_EDIT_INPUT->SetVisible( FALSE );
							m_pID_EDIT_INPUT->SetActivate( FALSE );
							_SetInputWidget(FALSE);
							ImmAssociateContext( g_hWndMain, NULL );
						}
						else
						{
							m_pID_EDIT_INPUT->SetActivate(TRUE);

							ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );

							if( m_nChannelDefault == gameui::CHAT_TYPE_NORMAL)
								SetInputText("");
							else if( m_nChannelDefault == gameui::CHAT_TYPE_GUILD )
								SetInputText(CHAT_GUILD);
							else if( m_nChannelDefault == gameui::CHAT_TYPE_TERM )
								SetInputText(CHAT_TEAM);
							else if( m_nChannelDefault == gameui::CHAT_TYPE_PRIVATE )
							{
								if( m_szPrivateTarget.Length() > 0)
								{
									StringHandle	szTemp;
									szTemp.Format(CHAT_PRIVATE_TO, (LPCTSTR)m_szPrivateTarget);
									SetInputText(szTemp);
								}
								else
									SetInputText(CHAT_PRIVATE_NIL);
							}
						}

						return TRUE;
					}
				}
				break;


				case VK_UP:
				{
					if( m_nChannelDefault == gameui::CHAT_TYPE_PRIVATE || 
						( m_pID_EDIT_INPUT->GetText()[0] == '/' &&  m_pID_EDIT_INPUT->GetText()[1] == 'w') )
					{
						StringHandle	szTemp;
						if( (INT)m_PrivateInfos.size() > ++m_nCurrentPrivateAmount )
						{
							szTemp.Format( CHAT_PRIVATE_TO, m_PrivateInfos[m_PrivateInfos.size() - 1 - m_nCurrentPrivateAmount].c_str());
						}
						else
						{
							if( m_PrivateInfos.size() > 0)
							{
								szTemp.Format( CHAT_PRIVATE_TO, m_PrivateInfos[m_PrivateInfos.size() - 1].c_str());
							}
							else
							{
								 szTemp =CHAT_PRIVATE_NIL;
							}
							m_nCurrentPrivateAmount = 0;
						}
						SetInputText(szTemp);
					}
					else
					{
						if( ++m_nCurrentLatest >= LAST_INFO_MAX )
						{
							m_nCurrentLatest = LAST_INFO_MAX-1;
						}
						if ( m_ChatHistory[m_nCurrentLatest].size() > 0 )
						{
							*m_pID_EDIT_INPUT = m_ChatHistory[m_nCurrentLatest];
							return TRUE;
						}
					}
				}
				break;


				case VK_DOWN:
				{
					if( m_nChannelDefault == gameui::CHAT_TYPE_PRIVATE || ( m_pID_EDIT_INPUT->GetText()[0] == '/' &&  m_pID_EDIT_INPUT->GetText()[1] == 'w')) 
					{	
						StringHandle	szTemp;
						if( (INT)m_PrivateInfos.size() > ++m_nCurrentPrivateAmount )
						{
							szTemp.Format( CHAT_PRIVATE_TO, m_PrivateInfos[m_PrivateInfos.size() - 1 - m_nCurrentPrivateAmount].c_str());
							SetInputText(szTemp);
						}
						else
						{
							if( m_PrivateInfos.size() > 0)
							{
								szTemp.Format(CHAT_PRIVATE_TO, m_PrivateInfos[m_PrivateInfos.size() - 1].c_str());
							}
							else
							{
								szTemp = CHAT_PRIVATE_NIL;
							}
							m_nCurrentPrivateAmount = 0;
						}
						SetInputText(szTemp);
					}
					else
					{
						if( --m_nCurrentLatest < -1 )
						{
							m_nCurrentLatest = -1;
						}
						if ( m_nCurrentLatest == -1 )
						{
							*m_pID_EDIT_INPUT = "";
						}
						else if ( m_ChatHistory[m_nCurrentLatest].size() > 0 )
						{
							*m_pID_EDIT_INPUT = m_ChatHistory[m_nCurrentLatest];
							return TRUE;
						}
					}
				}
				break;
			}
		}
		break;
	}
	return FALSE;
	
}



// 装载UI
BOOL UIChatInput::LoadUI()
{
	
	GetUIListener().RegisterMe();
	theUIChatLog.LoadUI();
	m_pChatInfoBox = theUICtrlManager.LoadFrame( UIDOC_PATH( "ChatInput"), TRUE, TRUE );
	if ( m_pChatInfoBox == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "ChatInput")"]失败")
		return FALSE;
	}

	return InitControls();
	
}

// 关连控件
BOOL UIChatInput::InitControls()
{
	
	VUIControl::InitControls();

	m_pID_INFOLIST = (VUCtrlList*)m_pChatInfoBox->FindControl(  ID_LIST_INFO );
	m_pID_EDIT_INPUT = 	(VUCtrlEdit*)m_pChatInfoBox->FindControl(  ID_EDIT_INPUT );
	m_pID_BUTTON_ChatPublic = (VUCtrlButton*)m_pChatInfoBox->FindControl(  ID_BUTTON_ChatPublic );
	m_pID_BUTTON_ChatGuild = (VUCtrlButton*)m_pChatInfoBox->FindControl(  ID_BUTTON_ChatGuild );
	m_pID_BUTTON_ChatGroup = (VUCtrlButton*)m_pChatInfoBox->FindControl(  ID_BUTTON_ChatGroup );
	m_pID_BUTTON_ChatPrivate = (VUCtrlButton*)m_pChatInfoBox->FindControl(  ID_BUTTON_ChatPrivate );
	m_pID_BUTTON_TurnPage = (VUCtrlButton*)m_pChatInfoBox->FindControl(  ID_BUTTON_TurnPage );
	m_pID_CHECKBOX_Normal = (VUCtrlCheckBox*)m_pChatInfoBox->FindControl(  ID_CHECKBOX_Normal );
	m_pID_CHECKBOX_Team = (VUCtrlCheckBox*)m_pChatInfoBox->FindControl(  ID_CHECKBOX_Team );
	m_pID_CHECKBOX_Guild = (VUCtrlCheckBox*)m_pChatInfoBox->FindControl(  ID_CHECKBOX_Guild );
	m_pID_CHECKBOX_Player = (VUCtrlCheckBox*)m_pChatInfoBox->FindControl(  ID_CHECKBOX_Player );
	m_pID_BUTTON_ChatOutType = (VUCtrlButton*)m_pChatInfoBox->FindControl(  ID_BUTTON_ChatOutType );
	m_pID_PICTURE_Ground = (VUCtrlPicture*)m_pChatInfoBox->FindControl(  ID_PICTURE_10574 );


	m_nPic_EditBG	= (VUCtrlPicture*)m_pChatInfoBox->FindControl( ID_PICTURE_3095);
	m_pID_BUTTON_Face = (VUCtrlButton*)m_pChatInfoBox->FindControl( ID_BUTTON_face);


	assert(m_nPic_EditBG);
	assert(m_pID_BUTTON_Face);
	assert( m_pChatInfoBox);
	assert( m_pID_INFOLIST);
	assert( m_pID_EDIT_INPUT);
	assert( m_pID_BUTTON_ChatPublic);
	assert( m_pID_BUTTON_ChatPrivate);
	assert( m_pID_BUTTON_ChatGuild);
	assert( m_pID_BUTTON_ChatGroup);
	assert( m_pID_BUTTON_TurnPage );
	assert( m_pID_CHECKBOX_Normal );
	assert( m_pID_CHECKBOX_Team );
	assert( m_pID_CHECKBOX_Guild );
	assert( m_pID_CHECKBOX_Player );
	assert(m_pID_PICTURE_Ground);
	assert( m_pID_BUTTON_ChatOutType );

	m_pChatInfoBox->SetProcOnFrameMove(  theUIChatInputOnFrameMove );
	m_pChatInfoBox->SetProcOnRender(  theUIChatInputOnRender );
	m_pID_EDIT_INPUT				->SetProcOnEnter	(  theUIChatInputID_EDIT_INPUTOnEditEnter );
	m_pID_BUTTON_ChatPublic		->SetProcOnButtonClick(  theUIChatInputID_BUTTON_ChatPublicOnButtonClick );
	m_pID_BUTTON_ChatPrivate	->SetProcOnButtonClick(  theUIChatInputID_BUTTON_ChatPrivateOnButtonClick );
	m_pID_BUTTON_ChatGuild		->SetProcOnButtonClick(  theUIChatInputID_BUTTON_ChatGuildOnButtonClick );
	m_pID_BUTTON_ChatGroup		->SetProcOnButtonClick(  theUIChatInputID_BUTTON_ChatGroupOnButtonClick );
	m_pID_CHECKBOX_Normal		->SetProcOnCheck(theUIChatInputID_CHECKBOX_NormalOnCheckBoxCheck );
	m_pID_CHECKBOX_Team			->SetProcOnCheck(theUIChatInputID_CHECKBOX_TeamOnCheckBoxCheck );
	m_pID_CHECKBOX_Guild			->SetProcOnCheck(theUIChatInputID_CHECKBOX_GuildOnCheckBoxCheck );
	m_pID_CHECKBOX_Player		->SetProcOnCheck(theUIChatInputID_CHECKBOX_PlayerOnCheckBoxCheck );
	m_pID_BUTTON_ChatOutType	->SetProcOnButtonClick(  theUIChatInputID_BUTTON_ChatOutTypeOnButtonClick );
	m_pID_BUTTON_TurnPage		->SetProcOnButtonClick(  theUIChatInputID_BUTTON_TurnPageOnButtonClick);



	_SetupWidget();

	if( !theUICharCreate.GetImeInstance() )
		theUICharCreate.SetImeInstance( ImmGetContext( g_hWndMain ) );

	ImmReleaseContext( g_hWndMain, theUICharCreate.GetImeInstance()  );

	_LoadShortcutInfo		(_PATH_SETTING("config\\shortcutchat.xml"));
	loadXmlChatColdTime	(XML_COLDTIME_PATH);

	UISCRIPT_ENABLE( UIOBJ_CHATINPUT, m_pChatInfoBox );
	return TRUE;
	
}

// 卸载UI
BOOL UIChatInput::UnLoadUI()
{
	
	theUIChatLog.UnLoadUI();
	//s_CUI_ID_FRAME_FACE.UnLoadUI();
	UISCRIPT_DISABLE( UIOBJ_CHATINPUT );
	GetUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ChatInput") );
	
}

// 是否可视
BOOL UIChatInput::IsVisible()
{
	
	return m_pChatInfoBox->IsVisible();
	
}

void UIChatInput::Printf( LPCSTR string, ... )
{
	
	if( !string )
		return;
	va_list	va;
	static char data[1024];
	va_start( va, string );
	wvsprintf((char*)data, string, va);
	va_end( va );
	AddInfo( data, 0xffffffff );
	

}


void UIChatInput::AutoSwitchChatChannel()
{
	//if( m_nChannelDefault == gameui::CHAT_TYPE_NORMAL )
	//{
	//	SetChatSendType("公会");
	//	m_nChannelDefault = gameui::CHAT_TYPE_GUILD;
	//}
	//else if( m_nChannelDefault == gameui::CHAT_TYPE_GUILD)
	//{
	//	SetChatSendType("队伍");
	//	m_nChannelDefault = gameui::CHAT_TYPE_TERM;
	//}
	//else if( m_nChannelDefault == gameui::CHAT_TYPE_TERM )
	//{
	//	SetChatSendType("私聊");
	//	m_nChannelDefault = gameui::CHAT_TYPE_PRIVATE;
	//}
	//else if( m_nChannelDefault == gameui::CHAT_TYPE_PRIVATE)
	//{
	//	SetChatSendType("全体");
	//	m_nChannelDefault = gameui::CHAT_TYPE_NORMAL;
	//}
}


void UIChatInput::AddInfo(const string& str
                         ,COLOR        col
								 ,UINT         nType
								 ,char *szToName  )
{	
	
	int nConver;
	
	if( nType == gameui::CHANNEL_SYSTEM)
	{
		theUISystemLog.AddInfo( str.c_str(), SYSTEM_COLOR );
		return;
	}

	switch ( nType )
	{
	case gameui::CHAT_TYPE_NORMAL:	//普通频道
		{
			if( !m_pID_CHECKBOX_Normal->IsChecked() )
				return;
			nConver = gameui::CHAT_TYPE_NORMAL;
		}
		break;
	case gameui::CHAT_TYPE_TERM:		//队伍频道
		{
			if ( !m_pID_CHECKBOX_Team->IsChecked() )
				return;
			nConver = gameui::CHAT_TYPE_TERM;
		}
		break;

	case gameui::CHAT_TYPE_GUILD:	//公会频道
		{
			if( !m_pID_CHECKBOX_Guild->IsChecked() )
				return;
			nConver = gameui::CHAT_TYPE_GUILD;
		}
		break;
	case gameui::CHAT_TYPE_PRIVATE:		//私聊频道
		{
			if( !m_pID_CHECKBOX_Player->IsChecked() )
				return;
			nConver = gameui::CHAT_TYPE_PRIVATE;
		}
		break;
	default:
		nConver = gameui::CHAT_TYPE_NORMAL;
		break;
	}


	/////////////////////////////////////////////
	if ( str.size() > 0 && m_pID_INFOLIST )
	{
		sUILIST_ITEM	stItem;
		int nWidthByte = m_pID_INFOLIST->GetWidth()*2/m_pID_INFOLIST->GetFontSize();
		if ( (int)str.size() > 	nWidthByte )
		{
			char *pszInfo = new char[nWidthByte+1];

			memset( pszInfo, 0, nWidthByte+1 );
			int nChineseByteCount = 0;

			for ( int n=0; n<nWidthByte; n++ )
			{
				if ( str[n] & 0x80 )
				{
					nChineseByteCount++;
				}
			}
			if ( nChineseByteCount & 0x1 )
			{
				nWidthByte--;
			}
			memcpy( pszInfo, str.c_str(), nWidthByte );

			stItem.SetData( pszInfo , nConver, NULL, col, szToName );
			m_pID_INFOLIST->AddItem( &stItem );
			delete[] pszInfo;

			AddInfo( &str[nWidthByte], col, nType );
		}
		else
		{
			//		if( szToName )
			stItem.SetData( str.c_str(), nConver, NULL, col, szToName );
			m_pID_INFOLIST->AddItem( &stItem );
		}

		{
			VUCtrlScrollBar* pScrollBar = m_pID_INFOLIST->GetScrollBarCtrl();
			if( pScrollBar )
			{
				int nMax = pScrollBar->GetMaxValue();
				if( nMax < 1000 && nMax > 0)
					pScrollBar->SetMaxValue( ++nMax );
				pScrollBar->SetStepValue( 1 );
			}
		}
	}
	
}

void UIChatInput::_SetupWidget()
{
	
	m_pChatInfoBox->SetMsgHangUp(FALSE);	//不接受消息
	m_pChatInfoBox->SetProcOnMsgProc(ChatMsgProc);

	m_pID_INFOLIST->SetMsgHangUp(FALSE);	//不接受消息
	m_pID_INFOLIST->SetSelectionInfo( FALSE );	//没有绿色cur选中地图
	m_pID_INFOLIST->SetScrollPos( -WIDTH_LIST_SCROLLBAR, 0);
	VUCtrlScrollBar* pScrollBar = m_pID_INFOLIST->GetScrollBarCtrl();
	if( pScrollBar )
	{
		pScrollBar->SetMaxValue( 1 );
		pScrollBar->SetStepValue( 1 );
	}

	m_pID_EDIT_INPUT->SetMaxCount(MAX_CHAT_COUNT);
	m_pID_EDIT_INPUT->SetProcOnTab(EditInputOnTab);

	m_pID_CHECKBOX_Normal->SetCheck(TRUE);
	m_pID_CHECKBOX_Team->SetCheck(TRUE);
	m_pID_CHECKBOX_Guild->SetCheck(TRUE);
	m_pID_CHECKBOX_Player->SetCheck(TRUE);

	_SetChatShowType(FALSE);
	_SetInputWidget(FALSE);

	m_pID_BUTTON_ChatOutType->SetCaption("全体");

	
}


void UIChatInput::_SetChatShowType(BOOL bShow)
{
	
	m_pID_BUTTON_ChatGuild->SetVisible(bShow);
	m_pID_BUTTON_ChatGroup->SetVisible(bShow);
	m_pID_BUTTON_ChatPublic->SetVisible(bShow);
	m_pID_BUTTON_ChatPrivate->SetVisible(bShow);
	
}


BOOL	UIChatInput::IsEditInputVisable()
{
	
	if( m_pID_EDIT_INPUT )
	{
		return (theUICtrlManager.GetFocus() ==m_pID_EDIT_INPUT);
	}
	return FALSE;
	
}
void UIChatInput::SetEditFocus()
{
	
	theUICtrlManager.SetFocus(m_pID_EDIT_INPUT);
	
}


BOOL UIChatInput::ProcessCommandHighShout(ScriptWord& /*words*/)
{
	{
		if(m_HighShoutLastTimer.IsExpired())
		{
			//AddInfo( m_strHighShoutInfo.c_str() );
			return TRUE;
		}

		OUTPUTTIP(TEXTRES_CHAT_COOLTIME_SOUNT);
	}
	return FALSE;
}

BOOL UIChatInput::ProcessCommandShout(ScriptWord& /*words*/)
{
	{
		if(m_ShoutLastTimer.IsExpired())
		{
			//AddInfo( m_strShoutInfo.c_str() );
			return TRUE;
		}

		OUTPUTTIP(TEXTRES_CHAT_COOLTIME_SOUNT);
	}
	return FALSE;
}

BOOL UIChatInput::ProcessCommandPrivate(ScriptWord& words)
{
	{
		SetCurrentChannel(gameui::CHAT_TYPE_PRIVATE);
		AddPrivateUser( words[1] );

		if(lstrcmp(m_szPrivateTarget, theVHero.GetName()) == 0)
		{
			OUTPUTTIP(TEXTRES_CHAT_CANNOT_SEND_WHISPER_YOURSELF);
			return FALSE;
		}

		if( m_PrivateLastTimer.IsExpired())
		{
			Object*						pPlayerTo;
			LPCTSTR						szData	= words[2];

			pPlayerTo	= theObjectManager.GetObject(words[1]);
			if(!pPlayerTo)
			{
				OUTPUTTIP(TEXTRES_FindNotPlayer);
				return FALSE;
			}

			//////////////////////////////////
			MSG_CW_WHISPER_SYN	msgPublic;
			msgPublic.byMsgLen		= (BYTE)(lstrlen(szData)+1 );
			lstrcpyn	(msgPublic.szCharNameTo
                  ,words[1]
						,MAX_CHARNAME_LENGTH);
			lstrcpyn	(msgPublic.szWhisperMsg
						,szData
						,MAX_CHAT_LENGTH);

			//////////////////////////////////
			if(theNetworkLayer.SendChatPacket(&msgPublic,sizeof(msgPublic)))
			{
				OUTPUTCHAT	(szData
								,&theHero
								,pPlayerTo
								,gameui::CHAT_TYPE_PRIVATE);
			}
			return TRUE;
		}

		OUTPUTTIP(TEXTRES_CHAT_COOLTIME_PRIVATE);
	}
	return FALSE;
}

BOOL UIChatInput::ProcessCommandGuild(ScriptWord& /*words*/)
{
	{
		SetCurrentChannel(gameui::CHAT_TYPE_GUILD);

		if(m_GuildLastTimer.IsExpired())
		{
			//AddInfo( m_strGuildInfo.c_str() );
			return TRUE;
		}

		OUTPUTTIP(TEXTRES_CHAT_COOLTIME_GUILD);
	}
	return FALSE;
}


BOOL UIChatInput::ProcessCommandParty(ScriptWord& /*words*/)
{
	{
		SetCurrentChannel(gameui::CHAT_TYPE_TERM);

		if(m_TeamLastTimer.IsExpired())
		{
			//AddInfo( m_strTeamInfo.c_str() );
			return FALSE;
		}

		OUTPUTTIP(TEXTRES_CHAT_COOLTIME_TEAM);
	}

	return TRUE;
}

//BOOL UIChatInput::ProcessCommandShout(ScriptWord& words)
//{
//	return TRUE;
//}




void UIChatInput::_WriteShortcutInfo(int nID, LPCSTR szStr)
{
	
	if(nID >= 0 && nID < 10)
	{
		m_ShortcutInfos[nID] = szStr;
	}
	_WriteShortcutInfo( m_strPath.c_str() );
	
}


LPCSTR UIChatInput::_GetShortcutInfo(int nID)
{
	
	if(nID >= 0 && nID < 10)
	{
		map<int, string>::iterator iter = m_ShortcutInfos.find(nID);
		if(iter != m_ShortcutInfos.end())
			return iter->second.c_str();
	}
	return NULL;
	
}
void UIChatInput::_LoadShortcutInfo(LPCSTR szPath)
{
	
	m_strPath = szPath;


	TiXmlDocument	doc;
	if( !doc.LoadFile(szPath) )
	{
		assert(FALSE && "Login.cpp::LoadConfigXml 读取文件不错");
		return;
	}

	TiXmlElement* lpRoot = doc.FirstChildElement("root");
	if(lpRoot)
	{
		LPCSTR szVaule = NULL;
		int			nID;
		string str;
		TiXmlElement* lpChat = lpRoot->FirstChildElement("chat");
		while(lpChat)
		{
			lpChat->Attribute("id", &nID);
			szVaule = lpChat->Attribute("str");
			if(szVaule)
			{
				str = szVaule;
				m_ShortcutInfos[nID] = str;
			}

			lpChat = lpChat->NextSiblingElement();
		}
	}
	
}
void UIChatInput::_WriteShortcutInfo(LPCSTR szPath)
{
	
	SetFileAttributes( szPath, FILE_ATTRIBUTE_NORMAL );

	TiXmlDocument doc;
	TiXmlDeclaration decl("1.0", "gb123", "yes");
	doc.InsertEndChild(decl);

	TiXmlElement pRoot("root");

	map<int, string>::iterator iter = m_ShortcutInfos.begin();
	for( ; iter != m_ShortcutInfos.end(); ++iter )
	{
		TiXmlElement pChat("chat");
		pChat.SetAttribute("id", iter->first);
		pChat.SetAttribute("str", iter->second.c_str());
		pRoot.InsertEndChild(pChat);
	}

	doc.InsertEndChild(pRoot);

	doc.SaveFile(szPath);
	

}
BOOL ChatMsgProc(UINT msg, WPARAM wParam, LPARAM lParam, BOOL bMsgUsed)
{
	
	return theUIChatInput.MsgProc(msg, wParam, lParam, bMsgUsed);
	
}
void UIChatInput::_ReadyPrivate(LPCSTR szName)	
{
	
	m_szPrivateTarget = szName;
	string strDes = "你对" + m_szPrivateTarget + "悄悄地说:";
	m_pID_EDIT_INPUT->SetRTFText(strDes.c_str());
	
}


void UIChatInput::BeginPrivateChat( LPCSTR szStr)
{
	
	if( szStr)
		m_szPrivateTarget = szStr;

	m_pChatInfoBox->SetActivate(TRUE);
	m_pID_EDIT_INPUT->SetVisible( TRUE );
	m_pID_EDIT_INPUT->SetActivate( TRUE );

	_SetInputWidget(TRUE);
	StringHandle	szTemp;
	szTemp.Format( CHAT_PRIVATE_TO, szStr);
	SetInputText(szTemp);
	theUICtrlManager.BringToTop(m_pChatInfoBox);
	theUICtrlManager.UpdateControls();
	SetEditFocus();

	ImmAssociateContext( g_hWndMain, theUICharCreate.GetImeInstance() );
	

}


void UIChatInput::_SetInputWidget( BOOL bShow )
{
	
	m_pID_PICTURE_Ground->SetVisible(bShow);
	m_nPic_EditBG->SetVisible(bShow);
	m_pID_BUTTON_Face->SetVisible(bShow);
	m_pID_BUTTON_ChatOutType->SetVisible(bShow);
	m_pID_BUTTON_TurnPage->SetVisible(bShow);
	m_pID_CHECKBOX_Guild->SetVisible(bShow);
	m_pID_CHECKBOX_Player->SetVisible(bShow);
	m_pID_CHECKBOX_Normal->SetVisible(bShow);
	m_pID_CHECKBOX_Team->SetVisible(bShow);

	if( bShow == FALSE)
	{
		_SetChatShowType(FALSE);
	}
	
}


void UIChatInput::EditInputOnTab( VUCtrlObject* /*pSender*/, LPCSTR /*szData*/)
{
	if( theUIChatInput.GetDefalutChannel() == gameui::CHAT_TYPE_NORMAL )
	{
		theUIChatInput.ID_BUTTON_ChatGuildOnButtonClick(NULL);
	}
	else if( theUIChatInput.GetDefalutChannel() == gameui::CHAT_TYPE_GUILD)
	{
		theUIChatInput.ID_BUTTON_ChatGroupOnButtonClick(NULL);
	}
	else if( theUIChatInput.GetDefalutChannel() == gameui::CHAT_TYPE_TERM )
	{
		theUIChatInput.ID_BUTTON_ChatPrivateOnButtonClick(NULL);
	}
	else if( theUIChatInput.GetDefalutChannel() == gameui::CHAT_TYPE_PRIVATE)
	{
		theUIChatInput.ID_BUTTON_ChatPublicOnButtonClick(NULL);
	}
	
}


void UIChatInput::SetInputText(LPCSTR  szText)
{
	if(szText)
		m_pID_EDIT_INPUT->SetText(szText);
}
void UIChatInput::SetChatSendType(LPCSTR  szText)
{
	if(szText)
		m_pID_BUTTON_ChatOutType->SetCaption(szText);
}

void	UIChatInput::SetCurrentChannel(int nChannle)
{
	if( nChannle < gameui::CHAT_TYPE_NORMAL || nChannle >= CHANNEL_NUM)
		return;
	m_nChannelCurrent = nChannle; 
}


BOOL UIChatInput::loadXmlChatColdTime(LPCSTR szSetting)
{

	util::TableTextFile ini;
	__CHECK(ini.OpenFile(szSetting,TRUE));

	int		nName		= -1;
	INT		nColdTime	= 0;
	LPCSTR	szValue;

	for(;ini.GotoNextLine();)
	{
		//	Type	Name	Time	Desc
		ini.GetInt();
		ini >> nName;
		ini >> nColdTime;
		ini >> szValue;

			if(nColdTime <= 0 )
			{
				LOGINFO("UIChatInfo %d Cool time <= 0\n",nName);
				continue;
			}

			////////////////////////////////////////////
			switch(nName)
			{
			case gameui::CHAT_TYPE_NORMAL:
				m_NormalLastTimer.SetTimer(nColdTime);
				break;

			case gameui::CHAT_TYPE_GUILD:
				m_GuildLastTimer.SetTimer(nColdTime);
				break;

			case gameui::CHAT_TYPE_TERM:
				m_TeamLastTimer.SetTimer(nColdTime);
				break;

			case gameui::CHAT_TYPE_PRIVATE:
				m_PrivateLastTimer.SetTimer(nColdTime);
				break;

			case gameui::CHAT_TYPE_SHOUT:
				m_ShoutLastTimer.SetTimer(nColdTime);
				break;

			case gameui::CHAT_TYPE_HIGHSHOUT:
				m_HighShoutLastTimer.SetTimer(nColdTime);
				break;
			}


	}
	return TRUE;

	
}


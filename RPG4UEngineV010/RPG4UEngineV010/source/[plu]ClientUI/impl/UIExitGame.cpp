
#include "stdafx.h"
#include "VUCtrlManager.h"




#include "V3DScene.h"
#include "V3DGameWorld.h"
#include "UIGameSet.h"
#include "UIHeroRelive.h"
#include "UIKeyboard.h"
#include "UIExitGame.h"
#include "V3DGameWorld.h"
#include "VHero.h"
#include "ApplicationBase.h"
#include "GlobalInstancePriority.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIExitGame, ()  , gamemain::eInstPrioClientUI/*UIExitGame*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE     ( theUIExitGame, OnFrameMove )
	UIPROC_FRAME_RENDER  ( theUIExitGame, OnRender )
	UIPROC_BUTTON_CLICK     (theUIExitGame, ID_BUTTON_GameSetOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIExitGame, ID_BUTTON_RestartOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIExitGame, ID_BUTTON_ExitGameOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIExitGame, ID_BUTTON_ContinueGameOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIExitGame, ID_BUTTON_keyboardGameOnButtonClick ); 
};//namespace uicallback
using namespace uicallback;


UIExitGame::UIExitGame()
{
	// Member
	m_pID_FRAME_ExitGame = NULL;
	m_pID_BUTTON_GameSet = NULL;
	m_pID_BUTTON_Restart = NULL;
	m_pID_BUTTON_ExitGame = NULL;
	m_pID_BUTTON_ContinueGame = NULL;
	m_pID_BUTTON_keyboard	=	NULL;
}

	BOOL UIExitGame::MsgProc( UINT /*msg*/, WPARAM /*wParam*/, LPARAM /*lParam*/, BOOL /*bMsgUsed*/ )
	{
		return FALSE;
	}

	// Frame
	BOOL UIExitGame::OnFrameMove(DWORD /*dwTick*/)
	{
		
		return TRUE;
		
	}
	BOOL UIExitGame::OnRender(DWORD /*dwTick*/)
	{
		
		return TRUE;
		
	}
	// Button
	BOOL UIExitGame::ID_BUTTON_GameSetOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		theUIGameSet.SetVisible( TRUE );
		SetVisible( FALSE );
		return TRUE;
	}

	// Button
	BOOL UIExitGame::ID_BUTTON_RestartOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		SetVisible( FALSE );
		return TRUE;
		//Sleep( 1 );
		//return TRUE;
		
	}
	// Button 
	BOOL UIExitGame::ID_BUTTON_keyboardGameOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		theUIKeyboard.SetVisible( TRUE );
		SetVisible( FALSE ) ;
		return TRUE;
		
	}
	
	// Button
	BOOL UIExitGame::ID_BUTTON_ExitGameOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		theApplication.Exit();
		

		return TRUE;
		
	}
	// Button
	BOOL UIExitGame::ID_BUTTON_ContinueGameOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		SetVisible( FALSE );
		return TRUE;
		
	}

	// 装载UI
	BOOL UIExitGame::LoadUI()
	{
		
		m_pID_FRAME_ExitGame = theUICtrlManager.LoadFrame( UIDOC_PATH( "ExitGame") );
		if ( m_pID_FRAME_ExitGame == 0 )
		{
			UIMessageLog("读取文件[UI\\ExitGame.UI]失败")
			return FALSE;
		}

		return InitControls();
		
	}
	// 关连控件
	BOOL UIExitGame::InitControls()
	{
		m_pID_BUTTON_GameSet			= (VUCtrlButton*)m_pID_FRAME_ExitGame->FindControl(ID_BUTTON_GameSet );
		m_pID_BUTTON_Restart			= (VUCtrlButton*)m_pID_FRAME_ExitGame->FindControl(ID_BUTTON_Restart );
		m_pID_BUTTON_ExitGame		= (VUCtrlButton*)m_pID_FRAME_ExitGame->FindControl(ID_BUTTON_ExitGame );
		m_pID_BUTTON_ContinueGame	= (VUCtrlButton*)m_pID_FRAME_ExitGame->FindControl(ID_BUTTON_ContinueGame );
		m_pID_BUTTON_keyboard		= (VUCtrlButton*)m_pID_FRAME_ExitGame->FindControl(ID_BUTTON_keyboard );

		assert( m_pID_FRAME_ExitGame );
		assert( m_pID_BUTTON_GameSet );
		assert( m_pID_BUTTON_Restart );
		assert( m_pID_BUTTON_ExitGame );
		assert( m_pID_BUTTON_ContinueGame );
		assert( m_pID_BUTTON_keyboard );


		m_pID_FRAME_ExitGame->SetProcOnFrameMove(  theUIExitGameOnFrameMove );
		m_pID_FRAME_ExitGame->SetProcOnRender(  theUIExitGameOnRender );

		m_pID_BUTTON_GameSet			->SetProcOnButtonClick(theUIExitGameID_BUTTON_GameSetOnButtonClick );
		m_pID_BUTTON_Restart			->SetProcOnButtonClick(theUIExitGameID_BUTTON_RestartOnButtonClick );
		m_pID_BUTTON_ExitGame		->SetProcOnButtonClick(theUIExitGameID_BUTTON_ExitGameOnButtonClick );
		m_pID_BUTTON_ContinueGame	->SetProcOnButtonClick(theUIExitGameID_BUTTON_ContinueGameOnButtonClick );
		m_pID_BUTTON_keyboard		->SetProcOnButtonClick(theUIExitGameID_BUTTON_keyboardGameOnButtonClick );

		SetVisible( FALSE );
		m_pID_FRAME_ExitGame->SetProcOnMsgProc( MsgProc );
		//m_pID_BUTTON_Restart->SetEnable( FALSE );

		UISCRIPT_ENABLE( UIOBJ_SYSTEMMENU, m_pID_FRAME_ExitGame );
		return TRUE;
		
	}
	// 卸载UI
	BOOL UIExitGame::UnLoadUI()
	{
		
		UISCRIPT_DISABLE( UIOBJ_SYSTEMMENU );
		return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ExitGame") );
		
	}
	// 是否可视
	BOOL UIExitGame::IsVisible()
	{
		
		if( m_pID_FRAME_ExitGame )
			return m_pID_FRAME_ExitGame->IsVisible();
		return FALSE;
		
	}
	// 设置是否可视
	void UIExitGame::SetVisible( BOOL bVisible )
	{
		
		if( m_pID_FRAME_ExitGame )
			m_pID_FRAME_ExitGame->SetVisible( bVisible );
		
	}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIInputBox.cpp
创建日期：2007年8月5日
最后更新：2007年8月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIInputBox.h"
#include "TextResManager.h"
#include "InputBoxUIListener.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::InputBoxUIListener& GetUIInputBoxInputBoxUIListenerUIListener()
{
	static gameui::InputBoxUIListener staticInputBoxUIListener;
	return staticInputBoxUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

const DWORD MAX_CLICK_COUNT = 100-1;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIInputBox, ()  , gamemain::eInstPrioClientUI/*UIInputBox*/);

namespace uicallback
{
	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	UIPROC_FRAME_FRAMEMOVE  ( theUIInputBox, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIInputBox, OnRender )
	UIPROC_EDIT_ENTER       (theUIInputBox,ID_EDIT_INPUTOnEditEnter );
	UIPROC_BUTTON_CLICK     (theUIInputBox,ID_BUTTON_OKOnButtonClick );  
	UIPROC_BUTTON_CLICK     (theUIInputBox,ID_BUTTON_CANCELOnButtonClick);  
	UIPROC_BUTTON_CLICK     (theUIInputBox,ID_BUTTON_UPOnButtonClick );  
	UIPROC_BUTTON_CLICK     (theUIInputBox,ID_BUTTON_DOWNOnButtonClick  );  
};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIInputBox::UIInputBox()
{
	m_ProcCallback = NULL;
	m_pData			= NULL;
	m_nDataSize		= 0;
}

	// Frame
	BOOL UIInputBox::OnFrameMove(DWORD /*dwTick*/)
	{
		return TRUE;
	}
	BOOL UIInputBox::OnRender(DWORD /*dwTick*/)
	{
		
		return TRUE;
		
	}
	// Edit
	void UIInputBox::ID_EDIT_INPUTOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ )
	{
		
		ID_BUTTON_OKOnButtonClick(NULL);
		
	}


	// Button
	BOOL UIInputBox::ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		if ( m_ProcCallback )
		{
			m_ProcCallback( m_pID_EDIT_INPUT->GetText(), m_pData );
		}
		if ( m_pData && m_nDataSize > 0 )
		{
			delete[] m_pData;
		}
		m_pData		= NULL;
		m_nDataSize = 0;


		//m_InputInfos.pop_back();
		if ( m_InputInfos.size() > 0 )
		{
			m_InputInfos.erase( m_InputInfos.begin() );
		}
		Refresh();
		return TRUE;
		
	}


	// Button
	BOOL UIInputBox::ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		if ( m_pData && m_nDataSize > 0 )
		{
			delete[] m_pData;
		}
		m_pData		= NULL;
		m_nDataSize = 0;


		if ( m_InputInfos.size() > 0 )
		{
			m_InputInfos.erase( m_InputInfos.begin() );
		}
		Refresh();
		return TRUE;
		
	}


	// Button
	BOOL UIInputBox::ID_BUTTON_UPOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		
		INT nCnt = atoi( m_pID_EDIT_INPUT->GetText() );
		if(nCnt >= MAX_CLICK_COUNT)
			return TRUE;

		nCnt ++;
		m_pID_EDIT_INPUT->SetText( nCnt );
		return TRUE;
		
	}
	// Button
	BOOL UIInputBox::ID_BUTTON_DOWNOnButtonClick( VUCtrlObject* /*pSender*/ )
	{
		INT nCnt = atoi( m_pID_EDIT_INPUT->GetText() );
		if(nCnt<=0)
			return TRUE;
		nCnt --;
		m_pID_EDIT_INPUT->SetText( nCnt );
		return TRUE;
	}

	// 装载UI
	BOOL UIInputBox::LoadUI()
	{
		GetUIInputBoxInputBoxUIListenerUIListener().RegisterMe();

		m_pID_INPUTBOX_FRAME = theUICtrlManager.LoadFrame( UIDOC_PATH( "InputBox") );
		if ( m_pID_INPUTBOX_FRAME == 0 )
		{
			UIMessageLog("读取文件[UI\\InputBox.UI]失败")
			return FALSE;
		}

		return InitControls();
	}


	// 关连控件
	BOOL UIInputBox::InitControls()
	{
		m_pID_INPUTBOX_FRAME->SetProcOnFrameMove	(theUIInputBoxOnFrameMove);
		m_pID_INPUTBOX_FRAME->SetProcOnRender		(theUIInputBoxOnRender);

		m_pID_EDIT_INPUT		= (VUCtrlEdit*)m_pID_INPUTBOX_FRAME->FindControl	(ID_EDIT_INPUT );
		m_pID_TEXT_CAPTION	= (VUCtrlText*)m_pID_INPUTBOX_FRAME->FindControl	(ID_TEXT_CAPTION );
		m_pID_BUTTON_OK		= (VUCtrlButton*)m_pID_INPUTBOX_FRAME->FindControl	(ID_BUTTON_OK );
		m_pID_BUTTON_CANCEL	= (VUCtrlButton*)m_pID_INPUTBOX_FRAME->FindControl	(ID_BUTTON_CANCEL );
		m_pID_BUTTON_UP		= (VUCtrlButton*)m_pID_INPUTBOX_FRAME->FindControl	(ID_BUTTON_UP );
		m_pID_BUTTON_DOWN		= (VUCtrlButton*)m_pID_INPUTBOX_FRAME->FindControl	(ID_BUTTON_DOWN );

		assert( m_pID_EDIT_INPUT );
		assert( m_pID_TEXT_CAPTION );
		assert( m_pID_BUTTON_OK );
		assert( m_pID_BUTTON_CANCEL );
		assert( m_pID_BUTTON_UP );
		assert( m_pID_BUTTON_DOWN );

		m_pID_EDIT_INPUT->SetProcOnEnter				( theUIInputBoxID_EDIT_INPUTOnEditEnter );
		m_pID_BUTTON_OK->SetProcOnButtonClick		( theUIInputBoxID_BUTTON_OKOnButtonClick );
		m_pID_BUTTON_CANCEL->SetProcOnButtonClick	( theUIInputBoxID_BUTTON_CANCELOnButtonClick );
		m_pID_BUTTON_UP->SetProcOnButtonClick		( theUIInputBoxID_BUTTON_UPOnButtonClick );
		m_pID_BUTTON_DOWN->SetProcOnButtonClick	( theUIInputBoxID_BUTTON_DOWNOnButtonClick );


		return TRUE;
	}
	// 卸载UI
	BOOL UIInputBox::UnLoadUI()
	{
		GetUIInputBoxInputBoxUIListenerUIListener().UnregisterMe();
		return theUICtrlManager.RemoveFrame( UIDOC_PATH( "InputBox") );
	}
	// 是否可视
	BOOL UIInputBox::IsVisible()
	{
		
		return m_pID_INPUTBOX_FRAME->IsVisible();
		
	}
	// 设置是否可视
	void UIInputBox::SetVisible( BOOL bVisible )
	{
		
		if( m_pID_INPUTBOX_FRAME )
			m_pID_INPUTBOX_FRAME->SetVisible( bVisible );

		if(bVisible)
		{
			theUICtrlManager.BringToTop( m_pID_INPUTBOX_FRAME );
			m_pID_EDIT_INPUT->SetForceFocus();
			m_pID_EDIT_INPUT->SetActivate();
		}
		
	}

	void UIInputBox::Show	(LPCSTR                  szCaption
                           ,BOOL                    bNumber
									,BOOL                    bModal
									,PROC_BOX_CALLBACK procCallback
									,void*                   pData
									,int							 nDataLength
									,BOOL                    bPassword)
	{
		
		sINPUTBOX inputInfo;

		inputInfo.m_strCaption		= szCaption;
		inputInfo.m_ProcCallback	= procCallback;
		inputInfo.m_bModal			= bModal;
		inputInfo.m_bNumber			= bNumber;
		inputInfo.m_bPassword		= bPassword;

		if ( pData && nDataLength > 0 )
		{
			inputInfo.m_nDataSize	= nDataLength;
			inputInfo.m_pData			= new BYTE[nDataLength];
			memcpy( inputInfo.m_pData, pData, nDataLength );
		}
		else
		{
			inputInfo.m_nDataSize	= 0;
			inputInfo.m_pData			= pData;
		}

		m_InputInfos.push_back( inputInfo );
		Refresh();
		
	}


	void UIInputBox::Show	(DWORD                   dwCaptionID
                           ,BOOL                    bNumber
									,BOOL                    bModal
									,PROC_BOX_CALLBACK procCallback
									,void*                   pData
									,int                     nDataLength
									,BOOL                    bPassword)
	{
		Show(theTextResManager.GetString(dwCaptionID),
			bNumber,
			bModal,
			procCallback,
			pData,
			nDataLength,
			bPassword);
	}
	

	BOOL UIInputBox::IsEditInputVisable()
	{
		
		return m_pID_EDIT_INPUT->IsVisible();
		
	}


	void UIInputBox::Refresh()
	{
		
		if ( m_InputInfos.size() > 0 )
		{
			sINPUTBOX *	pInputInfo;

			pInputInfo = &m_InputInfos[0];
			*m_pID_TEXT_CAPTION	= pInputInfo->m_strCaption;
			*m_pID_EDIT_INPUT		= "";
			m_ProcCallback			= pInputInfo->m_ProcCallback;
			m_pData					= pInputInfo->m_pData;
			m_nDataSize				= pInputInfo->m_nDataSize;

			if ( pInputInfo->m_bModal == TRUE )
			{
				m_pID_INPUTBOX_FRAME->SetArrangeMode( ArrayMode_Top );
			}
			else
			{
				m_pID_INPUTBOX_FRAME->SetArrangeMode( ArrayMode_Normal );
			}

			if(!pInputInfo->m_bNumber)
			{
				m_pID_BUTTON_UP->SetVisible(FALSE);
				m_pID_BUTTON_DOWN->SetVisible(FALSE);
			}

			m_pID_EDIT_INPUT->SetIsNumber( pInputInfo->m_bNumber );
			m_pID_EDIT_INPUT->SetIsPassword( pInputInfo->m_bPassword );

			SetVisible( TRUE );
			theUICtrlManager.UpdateControls();
			m_pID_EDIT_INPUT->SetActivate();
		}
		else
		{
			SetVisible( FALSE );
		}
		
	}


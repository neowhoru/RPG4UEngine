/*////////////////////////////////////////////////////////////////////////
文 件 名：UIVendor.h
创建日期：2008年6月8日
最后更新：2008年6月8日
编 写 者：亦哥(Leo/李亦)
       liease@163.com
		 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "UIVendor.h"
#include "VUCtrlManager.h"
#include "VUILayoutManager.h"
#include "VendorUIListener.h"
#include "BaseContainer.h"
#include "Hero.h"
#include "ItemManager.h"
#include "VendorDialog.h"
#include "UIVendorSlotUIListener.h"
#include "VendorDialog.h"
#include "UIInventory.h"
#include "VUCtrlIconDragManager.h"
#include "UIContainHelper.h"
#include "ShopDialog.h"
#include "ConstTextRes.h"

//#include "UIVendor_CallBack.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UIVendor, ()  , gamemain::eInstPrioGameFunc);

gameui::VendorUIListener& GetUIVendorVendorUIListener()
{
static gameui::VendorUIListener staticVendorUIListener;
return staticVendorUIListener;
}

namespace callback
{
	UIPROC_FRAME_FRAMEMOVE	( theUIVendor, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIVendor, OnRender )
	UIPROC_EDIT_ENTER			( theUIVendor, ID_EDIT_NAMEOnEditEnter )
	UIPROC_BUTTON_CLICK		( theUIVendor, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIVendor, ID_LISTIMG_VENDOROnIconDragOn )
	UIPROC_ICON_LDB_CLICK	( theUIVendor, ID_LISTIMG_VENDOROnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIVendor, ID_LISTIMG_VENDOROnIconRButtonUp )
	UIPROC_CHECK_BOX_CHECK	( theUIVendor, PageOnCheckBoxCheck )

	UIPROC_CHECK_BOX_CHECK	( theUIVendor, MethodOnCheckBoxCheck )
	UIPROC_BUTTON_CLICK		( theUIVendor, ID_BUTTON_ENDOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIVendor, ID_BUTTON_STARTOnButtonClick )
};

using namespace callback;

UIVendor::UIVendor()
{
	m_pPlayer					= NULL;

	m_pID_FRAME_VENDOR		= NULL;
	m_pID_BUTTON_CLOSE		= NULL;
	m_pID_TEXT_MONEY			= NULL;
	m_pID_EDIT_NAME			= NULL;
	m_pID_LISTIMG_VENDOR		= NULL;
	m_pID_LIST_LOGINFO		= NULL;
	m_pID_BUTTON_END			= NULL;
	m_pID_BUTTON_START		= NULL;

	m_nCurrentPage				= 0;
	m_nCurrentMethod			= VENDOR_METHOD_NONE;

	__ZERO(m_arCheckBoxPtrs);
	__ZERO(m_arMethodPtrs);

}


// Frame
BOOL UIVendor::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}
BOOL UIVendor::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}
// Button
BOOL UIVendor::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theVendorDialog.CloseWindow();
	return TRUE;
}


// ListImg / ListEx
BOOL UIVendor::ID_LISTIMG_VENDOROnIconDragOn(VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/, IconDragListImg* /*pItemDrag*/, IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
}

BOOL UIVendor::ID_LISTIMG_VENDOROnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIVendor::ID_LISTIMG_VENDOROnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
{
	if(pSender == m_pID_LISTIMG_VENDOR)
	{
		if(theVendorDialog.IsVendorView())
		{
			//INT nIndex = m_pID_LISTIMG_VENDOR->GetItemIndex(pItem);
			if(m_nCurrentMethod == VENDOR_METHOD_NONE)
				ChangeMethod(VENDOR_METHOD_BUY);
			else
				ChangeMethod(VENDOR_METHOD_NONE);

			//theMouseHandler.DoItemHandling	(SI_NPCVENDOR
			//											,m_nCurrentPage*MAX_SLOT_PER_VENDORPAGE + nIndex
			//											,NULL);
		}
		else
		{
			INT nPos = m_pID_LISTIMG_VENDOR->GetItemIndex(pItem);
			if(nPos >= 0)
				theVendorDialog.OpenModifyDialog((SLOTPOS)nPos);
		}
	}

	return FALSE;
}


// Edit
void UIVendor::ID_EDIT_NAMEOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR szData )
{
	theVendorDialog.VendingRename(szData);
}



BOOL UIVendor::ID_BUTTON_ENDOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theVendorDialog.VendingEnd();
	return TRUE;
}

BOOL UIVendor::ID_BUTTON_STARTOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_pID_BUTTON_START->SetVisible(FALSE);
	theVendorDialog.VendingStart();
	return TRUE;
}

void UIVendor::PageOnCheckBoxCheck ( VUCtrlObject* pSender,  BOOL * /*bChecked*/ )
{
	if(pSender->GetGroupID() == VENDOR_PAGE_GROUP_ID)
	{
		ChangePage(pSender->GetIndexAtGroup());
	}
}

void UIVendor::MethodOnCheckBoxCheck ( VUCtrlObject* pSender,  BOOL * /*bChecked*/ )
{
	if(pSender->GetGroupID() == METHOD_PAGE_GROUP_ID2)
	{
		ChangeMethod(VENDOR_METHOD_BUY);//(eSHOP_METHOD)pSender->GetIndexAtGroup());
	}
}


// 装载UI
BOOL UIVendor::LoadUI()
{
	GetUIVendorVendorUIListener().RegisterMe();
	m_pID_FRAME_VENDOR = theUICtrlManager.LoadFrame( UIDOC_PATH( "Vendor") );
	if ( m_pID_FRAME_VENDOR == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Vendor") "]失败")
			return FALSE;
	}

	theUILayoutManager.AddFrame(m_pID_FRAME_VENDOR);
	m_pID_FRAME_VENDOR->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIVendor::InitControls()
{
	int n;

	m_pID_FRAME_VENDOR->SetProcOnFrameMove(  theUIVendorOnFrameMove );
	m_pID_FRAME_VENDOR->SetProcOnRender	(  theUIVendorOnRender, FALSE );

	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)	m_pID_FRAME_VENDOR->FindControl(ID_BUTTON_CLOSE );
	m_pID_TEXT_MONEY			= (VUCtrlText*)	m_pID_FRAME_VENDOR->FindControl(ID_TEXT_MONEY );
	m_pID_EDIT_NAME			= (VUCtrlEdit*)	m_pID_FRAME_VENDOR->FindControl(ID_EDIT_NAME );
	m_pID_LISTIMG_VENDOR		= (VUCtrlListImg*)m_pID_FRAME_VENDOR->FindControl(ID_LISTIMG_VENDOR );
	m_pID_LIST_LOGINFO		= (VUCtrlList*)	m_pID_FRAME_VENDOR->FindControl(ID_LIST_LOGINFO );

	m_pID_BUTTON_END	= (VUCtrlButton*)	m_pID_FRAME_VENDOR->FindControl(ID_BUTTON_END );
	m_pID_BUTTON_START	= (VUCtrlButton*)	m_pID_FRAME_VENDOR->FindControl(ID_BUTTON_START );

	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_TEXT_MONEY );
	assert( m_pID_EDIT_NAME );
	assert( m_pID_LISTIMG_VENDOR );
	assert( m_pID_LIST_LOGINFO );
	assert( m_pID_BUTTON_END );
	assert( m_pID_BUTTON_START );

	m_pID_EDIT_NAME			->SetProcOnEnter				(theUIVendorID_EDIT_NAMEOnEditEnter );
	m_pID_BUTTON_CLOSE		->SetProcOnButtonClick		(theUIVendorID_BUTTON_CLOSEOnButtonClick );
	m_pID_LISTIMG_VENDOR		->SetProcOnDropTo				(theUIVendorID_LISTIMG_VENDOROnIconDragOn );
	m_pID_LISTIMG_VENDOR		->SetProcOnLButtonDBClick	(theUIVendorID_LISTIMG_VENDOROnIconLDBClick );
	m_pID_LISTIMG_VENDOR		->SetProcOnRButtonUp			(theUIVendorID_LISTIMG_VENDOROnIconRButtonUp );
	m_pID_BUTTON_END			->SetProcOnButtonClick		(theUIVendorID_BUTTON_ENDOnButtonClick );
	m_pID_BUTTON_START		->SetProcOnButtonClick		(theUIVendorID_BUTTON_STARTOnButtonClick );


	///分页控件
	for(n=0; n<MAX_VENDOR_PAGE_NUM; n++)
	{
		LPCSTR szName = FMSTR(ID_CHECKBOX_PAGE "%d", n+1);
		m_arCheckBoxPtrs[n]	= (VUCtrlCheckBox*)	m_pID_FRAME_VENDOR->FindControl( szName);
		assert( m_arCheckBoxPtrs[n] );

		m_arCheckBoxPtrs[n]->SetProcOnCheck(theUIVendorPageOnCheckBoxCheck);
		m_arCheckBoxPtrs[n]->SetCheck(n==0);
		m_arCheckBoxPtrs[n]->SetGroupID		(VENDOR_PAGE_GROUP_ID);
		m_arCheckBoxPtrs[n]->SetIndexAtGroup(n);
		m_arCheckBoxPtrs[n]->SetVisible(FALSE);
	}



	///操作方法
	static LPCSTR szMethods[]=
	{
		 ID_CHECKBOX_BUY
		//,ID_CHECKBOX_SELL
		//,ID_CHECKBOX_REPAIR
	};
	for(n=0; n<1; n++)
	{
		m_arMethodPtrs[n]	= (VUCtrlCheckBox*)	m_pID_FRAME_VENDOR->FindControl( szMethods[n]);
		assert( m_arMethodPtrs[n] );

		m_arMethodPtrs[n]->SetProcOnCheck(theUIVendorMethodOnCheckBoxCheck);
		m_arMethodPtrs[n]->SetCheck(FALSE);
		m_arMethodPtrs[n]->SetGroupID(METHOD_PAGE_GROUP_ID2);
		m_arMethodPtrs[n]->SetIndexAtGroup(n);
	}


	return TRUE;
}


// 卸载UI
BOOL UIVendor::UnLoadUI()
{
	GetUIVendorVendorUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Vendor") );
}

// 是否可视
BOOL UIVendor::IsVisible()
{
	return m_pID_FRAME_VENDOR->IsVisible();
}

// 设置是否可视
void UIVendor::SetVisible(  BOOL bVisible )
{
	//if(bVisible)
	//{
	//	BaseContainer*	pContainer;
	//	pContainer = (BaseContainer*)m_pID_LISTIMG_VENDOR->GetSlotContainer();
	//	if(pContainer)
	//		ChangePage(pContainer->GetCurrentPage());
	//}
	if(!bVisible)
	{
		if(m_nCurrentMethod != VENDOR_METHOD_NONE)
			ChangeMethod(VENDOR_METHOD_NONE);
	}

	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_VENDOR->SetVisible( bVisible );

	theUIInventory.SetVisible(bVisible);

	if(bVisible)
	{
		m_pID_EDIT_NAME->SetActivate();

		///////////////////////////////////////////
		if(!theVendorDialog.IsVendorView())
		{
			if(!theVendorDialog.IsVendorWorking())
			{
				theVendorDialog.SetVendorMessage(m_pID_EDIT_NAME->GetText());
			}
		}

		//m_pID_LISTIMG_VENDOR->SetCanDrag(m_nCurrentMethod == VENDOR_METHOD_NONE);
	}
}

void UIVendor::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_VENDOR);
}

void UIVendor::Refresh()
{
	BOOL	bView			= theVendorDialog.IsVendorView();
	BOOL	bWorking		= theVendorDialog.IsVendorWorking();

	m_pID_BUTTON_START	->SetVisible	(!bView && !bWorking);
	m_pID_BUTTON_END		->SetVisible	(!bView && bWorking);
	m_pID_EDIT_NAME		->SetReadOnly	(bView);
	//m_pID_LISTIMG_VENDOR	->SetCanPicking(!bView);

	/////////////////////////////////////
	if(bWorking || bView)
	{
		m_pID_EDIT_NAME		->SetText		(theVendorDialog.GetVendorMessage());
	}
	else
	{
		StringHandle	sName;
		sName.Format(_STRING(TEXTRES_VENDOR_TITLE), theHero.GetName());
		m_pID_EDIT_NAME		->SetText	(sName);
	}

	/////////////////////////////////////
	StringHandle	sText;
	sText.Format(_STRING(TEXTRES_VENDOR_TOTALINFO)
					,theVendorDialog.GetTotalMoneyBuy()
					,theVendorDialog.GetTotalMoneySell()
					);
	*m_pID_TEXT_MONEY	= (LPCTSTR)sText;
	
}

void UIVendor::SetMoney(MONEY*	pMoney)
{
	if(!pMoney)
		return;

	if( m_pID_TEXT_MONEY )
		*m_pID_TEXT_MONEY = (INT)*pMoney;
}

void UIVendor::AddTipLog(LPCTSTR szTipLog)
{
	if(!m_pID_LIST_LOGINFO)
		return;

	sUILIST_ITEM	datInfo;
	datInfo.SetData(szTipLog);
	m_pID_LIST_LOGINFO->AddItem(&datInfo,FALSE);
}



void UIVendor::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		switch(pContainer->GetSlotIndex())
		{
		case SI_VENDOR_BUY:
		case SI_VENDOR_SELL:
			{
				m_pID_LISTIMG_VENDOR->SetSlotContainer(pContainer);

				//INT nDisable = pContainer->GetSlotMaxSize()/MAX_SLOT_PER_VENDORPAGE;
				//for(INT n=0; n<MAX_VENDOR_PAGE_NUM;n++)
				//{
				//	m_arCheckBoxPtrs[n]->SetVisible(n <= nDisable);
				//}
				pContainer->SetCurrentPage(0);
				pContainer->SetSlotNumPerPage(MAX_SLOT_PER_VENDORPAGE);

			}
			break;
		}
	}
}

void UIVendor::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	UIContainHelper	helper(m_pID_LISTIMG_VENDOR
									,m_pPlayer
									,MAX_SLOT_PER_VENDORPAGE
									,m_nCurrentPage);
	helper.PushItem(*pSlot);

}


void UIVendor::PopGoods( BaseSlot* pSlot )
{
	UIContainHelper	helper(m_pID_LISTIMG_VENDOR
									,m_pPlayer
									,MAX_SLOT_PER_VENDORPAGE
									,m_nCurrentPage);
	helper.PopItem(*pSlot);


	Refresh();
}

void UIVendor::ChangePage	(UINT nPage)
{
	if(nPage >= MAX_VENDOR_PAGE_NUM)
		return;
	UINT n;

	for(n=0; n<MAX_VENDOR_PAGE_NUM; n++)
	{
		m_arCheckBoxPtrs[n]->SetCheck(nPage == n);
	}
	m_nCurrentPage = nPage;

	theUIVendorSlotUIListener.GetContainer()->SetCurrentPage((SLOTPOS)m_nCurrentPage);
	/// 最后更新背包数据
	m_pID_LISTIMG_VENDOR->Clear();
	theUIVendorSlotUIListener.Update((SLOTPOS)(m_nCurrentPage*MAX_SLOT_PER_VENDORPAGE), MAX_SLOT_PER_VENDORPAGE);
	Refresh();
}


void UIVendor::ChangeMethod	(eSHOP_METHOD eMethod)
{
	//if(eMethod >= VENDOR_METHOD_MAX)
	//	return;
	INT n;

	for(n=0; n<1; n++)
	{
		m_arMethodPtrs[n]->SetCheck(eMethod == VENDOR_METHOD_BUY);
	}
	m_nCurrentMethod = eMethod;

	//VendorDialog*	pVendor = (VendorDialog*)theUIVendorSlotUIListener.GetContainer();

	theShopDialog.SetCheckTrade((eSHOP_METHOD)m_nCurrentMethod);

	if(m_nCurrentMethod == VENDOR_METHOD_NONE)
	{
		theIconDragManager.ClearDrag();
	}
}

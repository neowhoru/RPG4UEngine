/*////////////////////////////////////////////////////////////////////////
文 件 名：UIWareHouse.h
创建日期：2008年6月8日
最后更新：2008年6月8日
编 写 者：亦哥(Leo/李亦)
       liease@163.com
		 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "UIWareHouse.h"
#include "VUCtrlManager.h"
#include "VUILayoutManager.h"
#include "ConstTextRes.h"
#include "WareHouseUIListener.h"
#include "BaseContainer.h"
#include "Hero.h"
#include "ItemManager.h"
#include "WareHouseDialog.h"
#include "UIWareHouseSlotUIListener.h"
#include "UIInventory.h"
#include "UIWareHouse_CallBack.h"
#include "ItemInfoParser.h"
#include "UIContainHelper.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UIWareHouse, ()  , gamemain::eInstPrioGameFunc);

gameui::WareHouseUIListener& GetUIWareHouseWareHouseUIListener()
{
static gameui::WareHouseUIListener staticWareHouseUIListener;
return staticWareHouseUIListener;
}

namespace callback
{
	UIPROC_FRAME_FRAMEMOVE	( theUIWareHouse, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIWareHouse, OnRender )
	UIPROC_BUTTON_CLICK		( theUIWareHouse, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIWareHouse, ID_LISTIMG_WAREHOUSEOnIconDragOn )
	UIPROC_ICON_LDB_CLICK	( theUIWareHouse, ID_LISTIMG_WAREHOUSEOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIWareHouse, ID_LISTIMG_WAREHOUSEOnIconRButtonUp )
	UIPROC_CHECK_BOX_CHECK	( theUIWareHouse, PageOnCheckBoxCheck )

	UIPROC_BUTTON_CLICK		( theUIWareHouse, ID_BUTTON_LOCKOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIWareHouse, ID_BUTTON_UNLOCKOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIWareHouse, ID_BUTTON_DEPOSITOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIWareHouse, ID_BUTTON_WITHDRAWOnButtonClick )
};

using namespace callback;

UIWareHouse::UIWareHouse()
{
	m_pPlayer					= NULL;
	m_pID_FRAME_WAREHOUSE	= NULL;
	m_pID_BUTTON_CLOSE		= NULL;
	m_pID_TEXT_MONEY			= NULL;
	m_pID_LISTIMG_WAREHOUSE = NULL;
	m_pID_BUTTON_LOCK			= NULL;
	m_pID_BUTTON_UNLOCK		= NULL;
	m_pID_BUTTON_DEPOSIT		= NULL;
	m_pID_BUTTON_WITHDRAW	= NULL;

	__ZERO(m_arCheckBoxPtrs);

}

// Frame
BOOL UIWareHouse::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}
BOOL UIWareHouse::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

// Button
BOOL UIWareHouse::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theWareHouseDialog.CloseWindow();
	return TRUE;
}


// ListImg / ListEx
BOOL UIWareHouse::ID_LISTIMG_WAREHOUSEOnIconDragOn(VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/, IconDragListImg* /*pItemDrag*/, IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
}

BOOL UIWareHouse::ID_LISTIMG_WAREHOUSEOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIWareHouse::ID_LISTIMG_WAREHOUSEOnIconRButtonUp( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}


// Button
BOOL UIWareHouse::ID_BUTTON_LOCKOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}

// Button
BOOL UIWareHouse::ID_BUTTON_UNLOCKOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}

// Button
BOOL UIWareHouse::ID_BUTTON_DEPOSITOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theUIInputBox.Show	( TEXTRES_UI_InputSaveMoney
                        ,TRUE
								,TRUE
								,SaveMoneyToBank);
	return TRUE;
}

// Button
BOOL UIWareHouse::ID_BUTTON_WITHDRAWOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theUIInputBox.Show	( TEXTRES_UI_Take_Money
                        ,TRUE
								,TRUE
								,GetMoneyFromBank);
	return TRUE;
}

void UIWareHouse::PageOnCheckBoxCheck ( VUCtrlObject* pSender,  BOOL * /*bChecked*/ )
{
	if(pSender->GetGroupID() == WARE_PAGE_GROUP_ID)
	{
		ChangePage(pSender->GetIndexAtGroup());
	}
}


// 装载UI
BOOL UIWareHouse::LoadUI()
{
	GetUIWareHouseWareHouseUIListener().RegisterMe();
	m_pID_FRAME_WAREHOUSE = theUICtrlManager.LoadFrame( UIDOC_PATH( "WareHouse") );
	if ( m_pID_FRAME_WAREHOUSE == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "WareHouse") "]失败")
			return FALSE;
	}

	theUILayoutManager.AddFrame(m_pID_FRAME_WAREHOUSE);
	m_pID_FRAME_WAREHOUSE->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIWareHouse::InitControls()
{
	int n;

	m_pID_FRAME_WAREHOUSE->SetProcOnFrameMove		(  theUIWareHouseOnFrameMove );
	m_pID_FRAME_WAREHOUSE->SetProcOnRender	(  theUIWareHouseOnRender, FALSE );

	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)	m_pID_FRAME_WAREHOUSE->FindControl(ID_BUTTON_CLOSE );
	m_pID_TEXT_MONEY			= (VUCtrlText*)	m_pID_FRAME_WAREHOUSE->FindControl(ID_TEXT_MONEY );
	m_pID_LISTIMG_WAREHOUSE = (VUCtrlListImg*)m_pID_FRAME_WAREHOUSE->FindControl(ID_LISTIMG_WAREHOUSE );
	m_pID_BUTTON_LOCK			= (VUCtrlButton*)	m_pID_FRAME_WAREHOUSE->FindControl(ID_BUTTON_LOCK );
	m_pID_BUTTON_UNLOCK		= (VUCtrlButton*)	m_pID_FRAME_WAREHOUSE->FindControl(ID_BUTTON_UNLOCK );
	m_pID_BUTTON_DEPOSIT		= (VUCtrlButton*)	m_pID_FRAME_WAREHOUSE->FindControl(ID_BUTTON_DEPOSIT );
	m_pID_BUTTON_WITHDRAW	= (VUCtrlButton*)	m_pID_FRAME_WAREHOUSE->FindControl(ID_BUTTON_WITHDRAW );


	for(n=0; n<MAX_WAREHOUSE_UIPAGE_NUM; n++)
	{
		LPCSTR szName = FMSTR(ID_CHECKBOX_PAGE "%d", n+1);
		m_arCheckBoxPtrs[n]	= (VUCtrlCheckBox*)	m_pID_FRAME_WAREHOUSE->FindControl( szName);
		assert( m_arCheckBoxPtrs[n] );
	}

	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_TEXT_MONEY );
	assert( m_pID_LISTIMG_WAREHOUSE );
	assert( m_pID_BUTTON_LOCK );
	assert( m_pID_BUTTON_UNLOCK );
	assert( m_pID_BUTTON_DEPOSIT );
	assert( m_pID_BUTTON_WITHDRAW );

	m_pID_BUTTON_CLOSE		->SetProcOnButtonClick		(theUIWareHouseID_BUTTON_CLOSEOnButtonClick );
	m_pID_LISTIMG_WAREHOUSE	->SetProcOnDropTo				(theUIWareHouseID_LISTIMG_WAREHOUSEOnIconDragOn );
	m_pID_LISTIMG_WAREHOUSE	->SetProcOnLButtonDBClick	(theUIWareHouseID_LISTIMG_WAREHOUSEOnIconLDBClick );
	m_pID_LISTIMG_WAREHOUSE	->SetProcOnRButtonUp			(theUIWareHouseID_LISTIMG_WAREHOUSEOnIconRButtonUp );
	m_pID_BUTTON_LOCK			->SetProcOnButtonClick		(theUIWareHouseID_BUTTON_LOCKOnButtonClick );
	m_pID_BUTTON_UNLOCK		->SetProcOnButtonClick		(theUIWareHouseID_BUTTON_UNLOCKOnButtonClick );
	m_pID_BUTTON_DEPOSIT		->SetProcOnButtonClick		(theUIWareHouseID_BUTTON_DEPOSITOnButtonClick );
	m_pID_BUTTON_WITHDRAW	->SetProcOnButtonClick		(theUIWareHouseID_BUTTON_WITHDRAWOnButtonClick );

	for(n=0; n<MAX_WAREHOUSE_UIPAGE_NUM; n++)
	{
		m_arCheckBoxPtrs[n]->SetProcOnCheck(theUIWareHousePageOnCheckBoxCheck);
		m_arCheckBoxPtrs[n]->SetCheck(n==0);
		m_arCheckBoxPtrs[n]->SetGroupID		(WARE_PAGE_GROUP_ID);
		m_arCheckBoxPtrs[n]->SetIndexAtGroup(n);
	}

	return TRUE;
}


// 卸载UI
BOOL UIWareHouse::UnLoadUI()
{
	GetUIWareHouseWareHouseUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "WareHouse") );
}

// 是否可视
BOOL UIWareHouse::IsVisible()
{
	return m_pID_FRAME_WAREHOUSE->IsVisible();
}

// 设置是否可视
void UIWareHouse::SetVisible(  BOOL bVisible )
{
	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_WAREHOUSE->SetVisible( bVisible );
	//if(theUIInventory.IsVisible())
	theUIInventory.SetVisible(bVisible);
}

void UIWareHouse::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_WAREHOUSE);
}

void UIWareHouse::Refresh()
{
}

void UIWareHouse::SetMoney(MONEY*	pMoney)
{
	if(!pMoney)
		return;

	if( m_pID_TEXT_MONEY )
		*m_pID_TEXT_MONEY = (INT)*pMoney;
}


void UIWareHouse::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		switch(pContainer->GetSlotIndex())
		{
		case SI_WAREHOUSE:
			{
				m_pID_LISTIMG_WAREHOUSE->SetSlotContainer(pContainer);

				INT nDisable = pContainer->GetSlotMaxSize()/MAX_SLOT_PER_WAREPAGE;
				for(INT n=0; n<MAX_WAREHOUSE_UIPAGE_NUM;n++)
				{
					m_arCheckBoxPtrs[n]->SetVisible(n <= nDisable);
				}
				pContainer->SetCurrentPage(0);
				pContainer->SetSlotNumPerPage(MAX_SLOT_PER_WAREPAGE);

			}
			break;
		}
	}
}

void UIWareHouse::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	UIContainHelper	helper(m_pID_LISTIMG_WAREHOUSE
									,m_pPlayer
									,MAX_SLOT_PER_WAREPAGE
									,m_nCurrentPage);
	helper.PushItem(*pSlot);



}


void UIWareHouse::PopGoods( BaseSlot* pSlot )
{
	UIContainHelper	helper(m_pID_LISTIMG_WAREHOUSE
									,m_pPlayer
									,MAX_SLOT_PER_WAREPAGE
									,m_nCurrentPage);
	helper.PopItem(*pSlot);



	Refresh();
}

void UIWareHouse::ChangePage	(UINT nPage)
{
	if(nPage >= MAX_WAREHOUSE_UIPAGE_NUM)
		return;
	UINT n;

	for(n=0; n<MAX_WAREHOUSE_UIPAGE_NUM; n++)
	{
		m_arCheckBoxPtrs[n]->SetCheck(nPage == n);
	}
	m_nCurrentPage = (SLOTPOS)nPage;

	theUIWareHouseSlotUIListener.GetContainer()->SetCurrentPage(m_nCurrentPage);
	/// 最后更新背包数据
	m_pID_LISTIMG_WAREHOUSE->Clear();
	theUIWareHouseSlotUIListener.Update((SLOTPOS)(m_nCurrentPage*MAX_SLOT_PER_WAREPAGE), MAX_SLOT_PER_WAREPAGE);
	Refresh();
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIFriendDlg.cpp
创建日期：2007年11月6日
最后更新：2007年11月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIFriend.h"
#include "VUIControl.h"
#include "UIChatInput.h"
#include "UIAction.h"
#include "UIMeunFriend.h"
#include "VUILayoutManager.h"
#include "FriendUIListener.h"
#include "VHero.h"
#include "ConstTextRes.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::FriendUIListener& GetUIListener()
{
	static gameui::FriendUIListener staticUIListener;
	return staticUIListener;
}

#define COLOR_FRIEND_ONLINE	0xff00ff00
#define COLOR_FRIEND_OUTLINE	0xff808080
#define COLOR_FRIEND_ADD		0xffffff00


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIFriend, ()  , gamemain::eInstPrioClientUI/*UIFriend*/);


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIFriend, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIFriend, OnRender )
	UIPROC_LIST_SELECT_CHANGE( theUIFriend, ID_LIST_FriendInfoOnListSelectChange )
	UIPROC_BUTTON_CLICK( theUIFriend, ID_BUTTON_AddFriendOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIFriend, ID_BUTTON_DelFriendOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIFriend, ID_BUTTON_CloseOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIFriend, ID_BUTTON_chatOnButtonClick)
	UIPROC_BUTTON_CLICK( theUIFriend, ID_BUTTON_teamOnButtonClick)
};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIFriend::UIFriend()
:VUIControl(gameui::eUIFriendDlg)
{
	
	// Member
	m_pID_FRAME_Friend = NULL;
	m_pID_LIST_FriendInfo = NULL;
	m_pID_BUTTON_AddFriend = NULL;
	m_pID_BUTTON_DelFriend = NULL;
	m_pID_BUTTON_chat = NULL;
	m_pID_BUTTON_team = NULL;



}
UIFriend::~UIFriend()
{
	
	
}
// Frame
BOOL UIFriend::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIFriend::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// List
void UIFriend::ID_LIST_FriendInfoOnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
{
	//if( CheckOnline(pItem->m_szText) )
	//{
	//	theUIChatInput.SetChatType(UIChatInput::CHAT_TYPE_PRIVATE);
	//	theUIChatInput.SetPrivateName(pItem->m_szText);
	//	theUIChatInput.Begin2Input();
	//	theUIChatInput.SetEditFocus();
	//}
	//else
	//{
	//	theUIChatInput.AddInfo( "好友不在线", COLOR_FRIEND_ADD);
	//}	
	
}


void UIFriend::ID_List_OnRBClick( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
{
	
	//if( theUIFriend.CheckOnline( pItem->m_szText ))
	//{//开右键菜单
	//	theUIMeunFriend.SetVisible(TRUE);
	//	theUIMeunFriend.SetName( pItem->m_szText );
	//	theUIMeunFriend.SetPos( theUICtrlManager.m_MousePos.x, theUICtrlManager.m_MousePos.y );
	//}
	
}

// Button
BOOL UIFriend::ID_BUTTON_AddFriendOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}
BOOL UIFriend::ID_BUTTON_CloseOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	//SetVisible(FALSE);
	SetLayoutVisible();
	return TRUE;
	
}
// Button
BOOL UIFriend::ID_BUTTON_DelFriendOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	sUILIST_ITEM *pItem =  m_pID_LIST_FriendInfo->GetCurSelItem();
	if(pItem)
	{
	}
	return TRUE;
	
}

// 装载UI
BOOL UIFriend::LoadUI()
{
	
	GetUIListener().RegisterMe();
	m_pID_FRAME_Friend = theUICtrlManager.LoadFrame( UIDOC_PATH( "Friend") );
	if ( m_pID_FRAME_Friend == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "Friend")"]失败")
			return FALSE;
	}

	theUILayoutManager.AddFrame( m_pID_FRAME_Friend );
	return InitControls();
	
}
// 关连控件
BOOL UIFriend::InitControls()
{
	m_pID_LIST_FriendInfo	= (VUCtrlList*)  m_pID_FRAME_Friend->FindControl( ID_LIST_FriendInfo );
	m_pID_BUTTON_AddFriend	= (VUCtrlButton*)m_pID_FRAME_Friend->FindControl( ID_BUTTON_AddFriend );
	m_pID_BUTTON_DelFriend	= (VUCtrlButton*)m_pID_FRAME_Friend->FindControl( ID_BUTTON_DelFriend );
	m_pID_BUTTON_Close		= (VUCtrlButton*)m_pID_FRAME_Friend->FindControl( ID_BUTTON_Close);
	m_pID_SendInfo				= (VUCtrlButton*)m_pID_FRAME_Friend->FindControl( ID_BUTTON_chat);
	m_pID_RequestParty		= (VUCtrlButton*)m_pID_FRAME_Friend->FindControl( ID_BUTTON_team);

	assert(m_pID_SendInfo);
	assert(m_pID_RequestParty);

	assert( m_pID_BUTTON_Close );
	assert( m_pID_FRAME_Friend );
	assert( m_pID_LIST_FriendInfo );
	assert( m_pID_BUTTON_AddFriend );
	assert( m_pID_BUTTON_DelFriend );
	
	m_pID_FRAME_Friend->SetProcOnFrameMove	(theUIFriendOnFrameMove );
	m_pID_FRAME_Friend->SetProcOnRender		(theUIFriendOnRender );

	m_pID_LIST_FriendInfo->SetProcOnSelectChange(  theUIFriendID_LIST_FriendInfoOnListSelectChange );
	m_pID_BUTTON_AddFriend	->SetProcOnButtonClick(theUIFriendID_BUTTON_AddFriendOnButtonClick );
	m_pID_BUTTON_DelFriend	->SetProcOnButtonClick(theUIFriendID_BUTTON_DelFriendOnButtonClick );
	m_pID_BUTTON_Close		->SetProcOnButtonClick(theUIFriendID_BUTTON_CloseOnButtonClick);
	//m_pID_BUTTON_chat			->SetProcOnButtonClick(theUIFriendID_BUTTON_chatOnButtonClick);
	//m_pID_BUTTON_team			->SetProcOnButtonClick(theUIFriendID_BUTTON_teamOnButtonClick);


	m_pID_FRAME_Friend->SetVisible(FALSE);

	m_pID_LIST_FriendInfo->SetProcOnRButtonDBClick( ID_List_OnRBClick );

	UISCRIPT_ENABLE ( UIOBJ_FRIEND, m_pID_FRAME_Friend );


	return TRUE;
	
}
// 卸载UI
BOOL UIFriend::UnLoadUI()
{
	
	UISCRIPT_DISABLE( UIOBJ_FRIEND );
	GetUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Friend") );
	
}
// 是否可视
BOOL UIFriend::IsVisible()
{
	
	return m_pID_FRAME_Friend->IsVisible();
	
}
// 设置是否可视
void UIFriend::SetVisible( BOOL bVisible )
{
	
	//m_pID_FRAME_Friend->SetVisible( bVisible );
	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_Friend->SetVisible( bVisible );
	
}
void UIFriend::SetLayoutVisible()
{
	
	theUILayoutManager.SetVisible( m_pID_FRAME_Friend );
	
}
int UIFriend::GetFriendID(const char *szName)
{
	
	FriendInfoMapIt iter = m_FirendInfos.begin();

	for( ; iter != m_FirendInfos.end(); ++iter)
	{
		if(iter->second.m_strName == szName)
			return iter->first;
	}

	return 0;
	
}
BOOL UIFriend::CheckOnline(LPCSTR  szName)
{
	
	FriendInfoMapIt iter = m_FirendInfos.begin();

	for( ; iter != m_FirendInfos.end(); ++iter)
	{
		if( iter->second.m_strName == szName )
		{
			if( iter->second.m_bOnline)
				return TRUE;
		}
	}

	return FALSE;
}


void UIFriend::Refresh()
{
	
	m_pID_LIST_FriendInfo->Clear();

	FriendInfoMapIt iter;
	sUILIST_ITEM	stItem;

	for(iter = m_FirendInfos.begin(); iter != m_FirendInfos.end(); ++iter)
	{
		if( (iter->second).m_bOnline )
		{
			stItem.SetData( (iter->second).m_strName.c_str(), 0, NULL, 0xff00ff00 );
			m_pID_LIST_FriendInfo->AddItem(&stItem);
		}
	}

	for( iter = m_FirendInfos.begin(); iter != m_FirendInfos.end(); ++iter )
	{
		if( !(iter->second).m_bOnline )
		{
			stItem.SetData( (iter->second).m_strName.c_str(),  0, NULL, 0xff808080 );
			m_pID_LIST_FriendInfo->AddItem(&stItem);
		}
	}
	
}
BOOL UIFriend::CheckUseName(LPCSTR  szName)
{
	{
		if(strcmp(theVHero.GetName(), szName) == 0)
		{
			theUIChatInput.AddInfo(theTextResManager.GetString(TEXTRES_NoneAddMe), COLOR_FRIEND_ADD);
			return FALSE;
		}
	}

	FriendInfoMapIt iter = m_FirendInfos.begin();

	for( ; iter != m_FirendInfos.end(); ++iter)
	{
		if( (iter->second).m_strName == szName)
		{
			theUIChatInput.AddInfo(theTextResManager.GetString(TEXTRES_AddFriRepeat), COLOR_FRIEND_ADD);
			return FALSE;
		}
	}

	return TRUE;
	
}


BOOL UIFriend::ID_BUTTON_chatOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	if( m_pID_LIST_FriendInfo->GetCurSelItem() )
	{
		std::string szName = m_pID_LIST_FriendInfo->GetCurSelItem()->m_szText;
		if( theUIFriend.CheckOnline(szName.c_str()) )
		{
			if( szName.length() > 0 )
				theUIChatInput.BeginPrivateChat( szName.c_str() );
		}
	}
	else
	{
		theUIChatInput.AddInfo( "好友不在线,不能发信息", COLOR_FRIEND_ADD);
	}
	return TRUE;
	
}
BOOL UIFriend::ID_BUTTON_teamOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	if( m_pID_LIST_FriendInfo->GetCurSelItem() )
	{
		std::string szName = m_pID_LIST_FriendInfo->GetCurSelItem()->m_szText;
		if( theUIFriend.CheckOnline( szName.c_str() ) )
		{
			char szInfo[128];
			//向 发送组队申请
			sprintf( szInfo, "%s %s %s", theTextResManager.GetString(TEXTRES_UI_YouTo), szName.c_str(), theTextResManager.GetString(TEXTRES_UI_SendTeam));
			theUIChatInput.AddInfo( szInfo, 0xFFFFFF00 );

		}
	}
	else
	{
		theUIChatInput.AddInfo( "好友不在线,不可组队", COLOR_FRIEND_ADD);
	}
	return TRUE;
	
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIGameSet.cpp
创建日期：2007年12月12日
最后更新：2007年12月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "FuncProfiler.h"
#include "V3DTerrainInclude.h"
#include "V3DGameWorld.h"
#include "PathInfoParser.h"
#include "GameUtil.h"
#include "UIGameSet.h"
#include "SoundLayer.h"
#include "GraphicSetting.h"
#include "VObjectManager.h"
#include "ApplicationSetting.h"
#include "ApplicationSetting.h"
#include "GlobalInstancePriority.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UIGameSet, ()  , gamemain::eInstPrioClientUI);



namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIGameSet, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIGameSet, OnRender )
	UIPROC_BUTTON_CLICK( theUIGameSet, ID_BUTTON_CloseOnButtonClick )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_BUTTON_FullSrcOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_BUTTON_WindowOnCheckBoxCheck )
	UIPROC_COMBO_BOX_CHANGE( theUIGameSet, ID_COMBOBOX_SrcWHOnComboBoxChange )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_CHECKBOX_Graph_Lvl1OnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_CHECKBOX_Graph_Lvl2OnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_CHECKBOX_Graph_Lvl3OnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_CHECKBOX_ShowHeadOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_CHECKBOX_MusicOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK( theUIGameSet, ID_CHECKBOX_SoundOnCheckBoxCheck )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIGameSet, ID_SCROLLBAREX_ViewOnScrollBarExUpdatePos )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIGameSet, ID_SCROLLBAREX_RoleViewOnScrollBarExUpdatePos )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIGameSet, ID_SCROLLBAREX_MusicMaxOnScrollBarExUpdatePos )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIGameSet, ID_SCROLLBAREX_SoundMaxOnScrollBarExUpdatePos )
	UIPROC_SCROLL_BAREX_UPDATE_POS( theUIGameSet, ID_SCROLLBAREX_mipmapOnScrollBarExUpdatePos )
	UIPROC_BUTTON_CLICK( theUIGameSet, ID_BUTTON_AppOnButtonClick )
};//namespace uicallback
using namespace uicallback;



const INT NEAR_CHUNK = 4;
const INT MAX_FAR_CHUNK = 13;

UIGameSetSetting::UIGameSetSetting( )
:XmlIOItem("GameSet")
{
	
	m_dwMultiSample = 0;
	m_fViewDistance = 4;
//	m_fRoleViewDistance = 0;
	
}

UIGameSetSetting::~UIGameSetSetting( )
{
	
	
}

bool UIGameSetSetting::loadXMLSettings( const char *path  )
{
	
	return XmlIOItem::loadXMLSettings( path );
	
}

//extern float theApplicationSetting.m_FarPlane;
BOOL UIGameSetSetting::ApplySetting()
{
		

	return TRUE;
	
}

bool UIGameSetSetting::loadXMLSettings( XMLElement *element)
{
	

	char szTemp [MAX_PATH] = {0};

	if(!isSuitable(element))
		return false;

	XMLElement  *child   = NULL;
	//XMLElement  *xmlValues   = NULL;
	//BOOL         success = false;

	child = element->getChildByName("ViewDistance");
	if( child )
	{
		m_fViewDistance = child->getValuef();
		if( m_fViewDistance >= NEAR_CHUNK && m_fViewDistance <= MAX_FAR_CHUNK+NEAR_CHUNK )
		{
			sprintf( szTemp, "setfarplane %d\n", (INT)m_fViewDistance );
			GameUtil::TryGameCommond( szTemp );
			theApplicationSetting.m_FarPlane = theGameWorld.ChangeFarPlane((INT) m_fViewDistance );
		}
	}
	child = element->getChildByName("HeroViewRange");
	if( child )
	{
		//singleton::GetPlayerManager().SetFarRole(child->getValuef());
		theUIGameSet.ID_SCROLLBAREX_RoleViewOnScrollBarExUpdatePos( NULL, child->getValuei() );
		if( theUIGameSet.m_pID_SCROLLBAREX_RoleView )
		{
			theUIGameSet.m_pID_SCROLLBAREX_RoleView->SetValue( child->getValuei() );
		}
	}
	child = element->getChildByName("MipLodBais");
	if( child )
	{
		//g_V3DConfig.SetMipLodBais( child->getValuef() );	
		theUIGameSet.ID_SCROLLBAREX_mipmapOnScrollBarExUpdatePos( NULL, child->getValuei() );
		if( theUIGameSet.m_pID_SCROLLBAREX_mipmap )
		{
			theUIGameSet.m_pID_SCROLLBAREX_mipmap->SetValue( child->getValuei() );
		}
	}
	child = element->getChildByName("SoundVal");
	if( child )
	{
	//	g_V3DConfig.SetMipLodBais( child->getValuef() );	
		theUIGameSet.ID_SCROLLBAREX_SoundMaxOnScrollBarExUpdatePos( NULL, child->getValuei() );
		if( theUIGameSet.m_pID_SCROLLBAREX_SoundMax )
		{
			theUIGameSet.m_pID_SCROLLBAREX_SoundMax->SetValue( child->getValuei() );
		}
	}
	child = element->getChildByName("MusicVal");
	if( child )
	{
		theUIGameSet.ID_SCROLLBAREX_MusicMaxOnScrollBarExUpdatePos( NULL, child->getValuei() );
		if( theUIGameSet.m_pID_SCROLLBAREX_MusicMax )
		{
			theUIGameSet.m_pID_SCROLLBAREX_MusicMax->SetValue( child->getValuei() );
		}
	}
	//Sound
	child = element->getChildByName("IsCloseMusic");
	if( child )
	{
		BOOL bSound = child->getValueb();
		theUIGameSet.ID_CHECKBOX_MusicOnCheckBoxCheck( NULL, &bSound );
		if( theUIGameSet.m_pID_CHECKBOX_Music )
		{
			theUIGameSet.m_pID_CHECKBOX_Music->SetCheck( bSound );
		}
	}

	child = element->getChildByName("IsCloseSound");
	if( child )
	{
		BOOL bSound = child->getValueb();
		theUIGameSet.ID_CHECKBOX_SoundOnCheckBoxCheck( NULL, &bSound );
		if( theUIGameSet.m_pID_CHECKBOX_Sound )
		{
			theUIGameSet.m_pID_CHECKBOX_Sound->SetCheck( bSound );
		}
	}


	ApplySetting();
	return TRUE;

	
}

bool UIGameSetSetting::exportXMLSettings( std::ofstream & /*xmlFile*/  )
{
	

	beginFlag("GameSet");
		writeFloat("ViewDistance", (float)theGameWorld.GetRenderChunk() );		
		writeFloat("HeroViewRange", (float)theUIGameSet.m_HeroViewRange );	
		writeFloat( "MipLodBais", (float)theUIGameSet.m_MiniMapValue );

		INT nSoundVal = theUIGameSet.m_SoundValue;
		writeFloat( "SoundVal",(float)nSoundVal );

		INT nMusicVal = theUIGameSet.m_MusicValue;
		writeFloat( "MusicVal", (float)nMusicVal );

		BOOL IsCloseSound = theUIGameSet.m_DisableSound;
		BOOL IsCloseMusic = theUIGameSet.m_DisableMusic;
		BOOL IsFullSrceen = FALSE;
		writeBool( "IsCloseSound", IsCloseSound?true:false );
		writeBool( "IsCloseMusic", IsCloseMusic?true:false );
		writeBool( "IsFullSrceen", IsFullSrceen?true:false );

		EndParam();
	EndFlag("GameSet");

	return TRUE;
	
}

bool UIGameSetSetting::exportXMLSettings( LPCSTR  xmlPath )
{
		
	
	SetFileAttributes( xmlPath, FILE_ATTRIBUTE_NORMAL );
	DeleteFile( xmlPath );

	openFile(xmlPath);
	BOOL result = exportXMLSettings(m_xmlFile);
	closeFile();
	return result?true:false;
	
}

UIGameSet::UIGameSet()
{
	m_pID_FRAME_GameSetup = NULL;
	m_pID_BUTTON_Close = NULL;
	m_pID_BUTTON_FullSrc = NULL;
	m_pID_BUTTON_Window = NULL;
	m_pID_COMBOBOX_SrcWH = NULL;
	m_pID_CHECKBOX_Graph_Lvl1 = NULL;
	m_pID_CHECKBOX_Graph_Lvl2 = NULL;
	m_pID_CHECKBOX_Graph_Lvl3 = NULL;
	m_pID_CHECKBOX_ShowHead = NULL;
	m_pID_CHECKBOX_Music = NULL;
	m_pID_CHECKBOX_Sound = NULL;
	m_pID_SCROLLBAREX_View = NULL;
	m_pID_SCROLLBAREX_RoleView = NULL;
	m_pID_SCROLLBAREX_MusicMax = NULL; 
	m_pID_SCROLLBAREX_SoundMax = NULL;
	m_pID_BUTTON_App = NULL;

	m_pID_SCROLLBAREX_mipmap = NULL;

	m_HeroViewRange	= 380;
	m_MusicValue		= 500;
	m_SoundValue		= 500;
	m_MiniMapValue		= 700;

	m_DisableMusic = TRUE;
	m_DisableSound = TRUE;

	
}
// Frame
BOOL UIGameSet::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UIGameSet::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// Button
BOOL UIGameSet::ID_BUTTON_CloseOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	SetVisible( FALSE );
	return TRUE;
	
}

// ScrollBarEx
void UIGameSet::ID_SCROLLBAREX_ViewOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int n )
{
	
	char szTemp[MAX_PATH] = {0};

	INT nFar = (INT)(((float)n/1000.0f) * MAX_FAR_CHUNK+NEAR_CHUNK);

	sprintf( szTemp, "setfarplane %d\n", nFar );
	GameUtil::TryGameCommond( szTemp );
	
}


// ScrollBarEx
void UIGameSet::ID_SCROLLBAREX_RoleViewOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int n )
{
	
	m_HeroViewRange = n;
	theVObjectManager.SetFarFromHero( (float)m_HeroViewRange );
	
}


// ScrollBarEx
void UIGameSet::ID_SCROLLBAREX_MusicMaxOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int n )
{
	
	INT Value = n*2;
	m_MusicValue = n;
	theSoundLayer.SetMusicMax( Value);
	
}
// ScrollBarEx
void UIGameSet::ID_SCROLLBAREX_SoundMaxOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int n )
{
	
	INT Value = n*2;
	m_SoundValue = n;
	theSoundLayer.SetSoundMax( Value);
	
}

// CheckBox
void UIGameSet::ID_BUTTON_FullSrcOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	
}
// CheckBox
void UIGameSet::ID_BUTTON_WindowOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	
}
// ComboBox
void UIGameSet::ID_COMBOBOX_SrcWHOnComboBoxChange( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ )
{
	
	
}
// CheckBox
void UIGameSet::ID_CHECKBOX_Graph_Lvl1OnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	m_pID_CHECKBOX_Graph_Lvl1->SetCheck( TRUE );
	m_pID_CHECKBOX_Graph_Lvl2->SetCheck( FALSE );
	m_pID_CHECKBOX_Graph_Lvl3->SetCheck( FALSE );	
	
}
// CheckBox
void UIGameSet::ID_CHECKBOX_Graph_Lvl2OnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	m_pID_CHECKBOX_Graph_Lvl1->SetCheck( FALSE );
	m_pID_CHECKBOX_Graph_Lvl2->SetCheck( TRUE );
	m_pID_CHECKBOX_Graph_Lvl3->SetCheck( FALSE );	
	
}
// CheckBox
void UIGameSet::ID_CHECKBOX_Graph_Lvl3OnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	m_pID_CHECKBOX_Graph_Lvl1->SetCheck( FALSE );
	m_pID_CHECKBOX_Graph_Lvl2->SetCheck( FALSE );
	m_pID_CHECKBOX_Graph_Lvl3->SetCheck( TRUE );	
	
}
// CheckBox
void UIGameSet::ID_CHECKBOX_ShowHeadOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
	
	
}
// CheckBox
void UIGameSet::ID_CHECKBOX_MusicOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* pbChecked )
{
	
	m_DisableMusic = *pbChecked;
	//FCloseMusic()
	theSoundLayer.PauseMusic(m_DisableMusic);

//	FCloseMusic( m_DisableMusic );
	
}
// CheckBox
void UIGameSet::ID_CHECKBOX_SoundOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* pbChecked )
{
	
	m_DisableSound = *pbChecked;
	theSoundLayer.PauseSound( m_DisableSound );
//	FCloseSound( m_DisableSound );
	
}

void UIGameSet::ID_SCROLLBAREX_mipmapOnScrollBarExUpdatePos( VUCtrlObject* /*pSender*/, int n )
{
	

	//2.5 - -2.5
	FLOAT fBias = 2.5f - (FLOAT)n/200.0f;
	m_MiniMapValue = n;
	g_V3DConfig.SetMipLodBais( fBias );
}


BOOL UIGameSet::ID_BUTTON_AppOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	return TRUE;
	
}

// 装载UI
BOOL UIGameSet::LoadUI()
{
	
	m_pID_FRAME_GameSetup = theUICtrlManager.LoadFrame( UIDOC_PATH( "GameSet") );
	if ( m_pID_FRAME_GameSetup == 0 )
	{
		UIMessageLog("读取文件[UI\\GameSet.UI]失败")
			return false;
	}

	return InitControls();
	
}


// 关连控件
BOOL UIGameSet::InitControls()
{
	m_pID_BUTTON_Close			= (VUCtrlButton*)m_pID_FRAME_GameSetup->FindControl(  ID_BUTTON_Close );
	m_pID_BUTTON_FullSrc			= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_BUTTON_FullSrc );
	m_pID_BUTTON_Window			= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_BUTTON_Window );
	m_pID_COMBOBOX_SrcWH			= (VUCtrlComboBox*)m_pID_FRAME_GameSetup->FindControl(  ID_COMBOBOX_SrcWH );
	m_pID_CHECKBOX_Graph_Lvl1	= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_CHECKBOX_Graph_Lvl1 );
	m_pID_CHECKBOX_Graph_Lvl2	= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_CHECKBOX_Graph_Lvl2 );
	m_pID_CHECKBOX_Graph_Lvl3	= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_CHECKBOX_Graph_Lvl3 );
	m_pID_CHECKBOX_ShowHead		= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_CHECKBOX_ShowHead );
	m_pID_CHECKBOX_Music			= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_CHECKBOX_Music );
	m_pID_CHECKBOX_Sound			= (VUCtrlCheckBox*)m_pID_FRAME_GameSetup->FindControl(  ID_CHECKBOX_Sound );

	m_pID_SCROLLBAREX_View		= (VUCtrlScrollBarEx*)m_pID_FRAME_GameSetup->FindControl(  ID_SCROLLBAREX_View );;
	m_pID_SCROLLBAREX_RoleView = (VUCtrlScrollBarEx*)m_pID_FRAME_GameSetup->FindControl(  ID_SCROLLBAREX_RoleView );

	m_pID_SCROLLBAREX_mipmap	= (VUCtrlScrollBarEx*)m_pID_FRAME_GameSetup->FindControl(  ID_SCROLLBAREX_mipmap );
	m_pID_SCROLLBAREX_MusicMax = (VUCtrlScrollBarEx*)m_pID_FRAME_GameSetup->FindControl(  ID_SCROLLBAREX_MusicMax );
	m_pID_SCROLLBAREX_SoundMax = (VUCtrlScrollBarEx*)m_pID_FRAME_GameSetup->FindControl(  ID_SCROLLBAREX_SoundMax );
	m_pID_BUTTON_App				= (VUCtrlButton*)m_pID_FRAME_GameSetup->FindControl(  ID_BUTTON_App );

	
	
	assert( m_pID_FRAME_GameSetup );
	assert( m_pID_BUTTON_Close );
	assert( m_pID_BUTTON_FullSrc );
	assert( m_pID_BUTTON_Window );
	assert( m_pID_COMBOBOX_SrcWH );
	assert( m_pID_CHECKBOX_Graph_Lvl1 );
	assert( m_pID_CHECKBOX_Graph_Lvl2 );
	assert( m_pID_CHECKBOX_Graph_Lvl3 );
	assert( m_pID_CHECKBOX_ShowHead );
	assert( m_pID_CHECKBOX_Music );
	assert( m_pID_CHECKBOX_Sound );
	assert( m_pID_SCROLLBAREX_View );
	assert( m_pID_SCROLLBAREX_RoleView );
	assert( m_pID_SCROLLBAREX_MusicMax );
	assert( m_pID_BUTTON_App );
	assert( m_pID_SCROLLBAREX_SoundMax );
	

	m_pID_FRAME_GameSetup->SetProcOnFrameMove(  theUIGameSetOnFrameMove );
	m_pID_FRAME_GameSetup->SetProcOnRender(  theUIGameSetOnRender );

	m_pID_BUTTON_Close->SetProcOnButtonClick( theUIGameSetID_BUTTON_CloseOnButtonClick );
	m_pID_BUTTON_App->SetProcOnButtonClick(   theUIGameSetID_BUTTON_AppOnButtonClick );

	m_pID_BUTTON_FullSrc			->SetProcOnCheck	(  theUIGameSetID_BUTTON_FullSrcOnCheckBoxCheck );
	m_pID_BUTTON_Window			->SetProcOnCheck	(  theUIGameSetID_BUTTON_WindowOnCheckBoxCheck );
	m_pID_COMBOBOX_SrcWH			->SetProcOnChange	(  theUIGameSetID_COMBOBOX_SrcWHOnComboBoxChange );
	m_pID_CHECKBOX_Graph_Lvl1	->SetProcOnCheck	(  theUIGameSetID_CHECKBOX_Graph_Lvl1OnCheckBoxCheck );
	m_pID_CHECKBOX_Graph_Lvl2	->SetProcOnCheck	(  theUIGameSetID_CHECKBOX_Graph_Lvl2OnCheckBoxCheck );
	m_pID_CHECKBOX_Graph_Lvl3	->SetProcOnCheck	(  theUIGameSetID_CHECKBOX_Graph_Lvl3OnCheckBoxCheck );
	m_pID_CHECKBOX_ShowHead		->SetProcOnCheck	(  theUIGameSetID_CHECKBOX_ShowHeadOnCheckBoxCheck );
	m_pID_CHECKBOX_Music			->SetProcOnCheck	(  theUIGameSetID_CHECKBOX_MusicOnCheckBoxCheck );
	m_pID_CHECKBOX_Sound			->SetProcOnCheck	(  theUIGameSetID_CHECKBOX_SoundOnCheckBoxCheck );
	m_pID_SCROLLBAREX_View		->SetProcOnUpdatePos(  theUIGameSetID_SCROLLBAREX_ViewOnScrollBarExUpdatePos );
	m_pID_SCROLLBAREX_RoleView	->SetProcOnUpdatePos(  	theUIGameSetID_SCROLLBAREX_RoleViewOnScrollBarExUpdatePos );
	m_pID_SCROLLBAREX_MusicMax	->SetProcOnUpdatePos(  	theUIGameSetID_SCROLLBAREX_MusicMaxOnScrollBarExUpdatePos );
	m_pID_SCROLLBAREX_SoundMax	->SetProcOnUpdatePos(  	theUIGameSetID_SCROLLBAREX_SoundMaxOnScrollBarExUpdatePos );
	m_pID_SCROLLBAREX_mipmap	->SetProcOnUpdatePos(  theUIGameSetID_SCROLLBAREX_mipmapOnScrollBarExUpdatePos );
	
	m_pID_BUTTON_App->SetProcOnButtonClick(   theUIGameSetID_BUTTON_AppOnButtonClick );


	m_pID_CHECKBOX_Graph_Lvl1->SetCheck(TRUE);
	m_pID_CHECKBOX_Graph_Lvl2->SetCheck(FALSE);
	m_pID_CHECKBOX_Graph_Lvl3->SetCheck(FALSE);
	SetVisible( FALSE );

	INT iSel = 0;
#pragma message(__FILE__  "(531)  以下注释代码有待调整... " )

	//以下注释代码有待调整
	//for( int i=0; i<GetGameClientApplication().m_DisplayModes.size(); i++ )
	//{
	//	sUILIST_ITEM Item;
	//	Item.m_nID = i;
	//	D3DDISPLAYMODE dispMode = GetGameClientApplication().m_DisplayModes[i];
	//	sprintf( Item.m_szText, "%d x %d Ref:%d", dispMode.Width, 
	//		dispMode.Height, dispMode.RefreshRate );
	//	if( dispMode.Width == SCREEN_WIDTH 
	//		&& dispMode.Height == SCREEN_HEIGHT )
	//	{
	//		iSel = i;
	//	}

	//	m_pID_COMBOBOX_SrcWH->GetListBox().AddItem ( &Item );
	//}
	if( iSel < m_pID_COMBOBOX_SrcWH->GetListBox().GetListItemCnt() )
	{
		sUILIST_ITEM* pList = m_pID_COMBOBOX_SrcWH->GetListBox().GetListItemAt(iSel);
		m_pID_COMBOBOX_SrcWH->GetEditInput().SetText(pList->m_szText);
	}		


	m_GameSetting.loadXMLSettings( _PATH_SETTING("GameSet.xml") );

	return TRUE;
	
}

// 卸载UI
BOOL UIGameSet::UnLoadUI()
{
	
	m_GameSetting.exportXMLSettings(_PATH_SETTING("GameSet.xml"));
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "GameSet") );
	
}
// 是否可视
BOOL UIGameSet::IsVisible()
{
	
	return m_pID_FRAME_GameSetup->IsVisible();
	
}
// 设置是否可视
void UIGameSet::SetVisible( BOOL bVisible )
{
	
	if( m_pID_FRAME_GameSetup )
		m_pID_FRAME_GameSetup->SetVisible( bVisible );
	if( bVisible )
	{
		if( m_pID_SCROLLBAREX_View )
			m_pID_SCROLLBAREX_View->SetValue((INT)( (float)(theGameWorld.GetRenderChunk()-NEAR_CHUNK)/MAX_FAR_CHUNK*1000.0f) );
	}
	
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UITask.cpp
创建日期：2007年12月4日
最后更新：2007年12月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIQuest.h"
#include "UITask_Track.h"
#include "QuestManager.h"
#include "UIAction.h"
#include "UIProperty.h"

#include "UIMessageBox.h"
#include "VUIObjectManager.h"
#include "FuncProfiler.h"
#include "TextResManager.h"

#include "VUILayout.h"
#include "TaskUIListener.h"
#include "GlobalInstancePriority.h"

using namespace gameui;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::TaskUIListener& GetUITaskTaskUIListener()
{
	static gameui::TaskUIListener staticTaskUIListener;
	return staticTaskUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(CUI_ID_FRAME_Task, ()  , gamemain::eInstPrioClientUI/*CUI_ID_FRAME_Task*/);
#else
#endif

namespace uicallback
{
	MAP_FRAME_RUN( s_CUI_ID_FRAME_Task, OnFrameRun )
		MAP_FRAME_RENDER( s_CUI_ID_FRAME_Task, OnFrameRender )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Task, ID_BUTTON_CLOSEOnButtonClick )
		MAP_ICON_DROP_TO_CALLBACK( s_CUI_ID_FRAME_Task, ID_LISTEX_TaskOnIconDropTo )
		MAP_ICON_DRAG_OFF_CALLBACK( s_CUI_ID_FRAME_Task, ID_LISTEX_TaskOnIconDragOff )
		MAP_LIST_SELECT_CHANGE_CALLBACK( s_CUI_ID_FRAME_Task, ID_LIST_TaskOnListSelectChange)
		MAP_EDIT_ENTER_CALLBACK( s_CUI_ID_FRAME_Task, ID_EDIT_InfoOnEditEnter )
		MAP_EDIT_ENTER_CALLBACK( s_CUI_ID_FRAME_Task, ID_EDIT_StateOnEditEnter )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Task, ID_BUTTON_AbandonTaskOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Task, ID_BUTTON_ActionOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Task, ID_BUTTON_PropertyOnButtonClick )
		MAP_TEXT_HYPER_CLICKBACK(s_CUI_ID_FRAME_Task, ID_TEXT_Info_HyperLinkClick)
		MAP_TEXT_HYPER_CLICKBACK(s_CUI_ID_FRAME_Task, ID_TEXT_State_HyperLinkClick)
//MAP_ICON_LDB_CLICK_CALLBACK( s_CUI_ID_FRAME_Task, ID_LISTEX_TaskOnIconLDBClick )
//MAP_ICON_RBUTTON_UP_CALLBACK( s_CUI_ID_FRAME_Task, ID_LISTEX_TaskOnIconRDBClick )
};//namespace uicallback
using namespace uicallback;


CUI_ID_FRAME_Task::CUI_ID_FRAME_Task()
:VUIControl ( eUI_OBJECT_Task )
{
	
	// Member
	m_pID_FRAME_Task = NULL;
	m_pID_BUTTON_CLOSE = NULL;
	//m_pID_LISTEX_Task = NULL;
	//m_pID_TEXT_Level = NULL;
	//m_pID_EDIT_Info = NULL;
	//m_pID_EDIT_State = NULL;
	m_pID_LIST_Task = NULL;
	m_pID_TEXT_Info = NULL;
	m_pID_TEXT_State = NULL;
	m_pID_BUTTON_AbandonTask = NULL;
	m_pID_BUTTON_Action = NULL;
	m_pID_BUTTON_Property = NULL;

	m_bLoad = FALSE;

	_nCurIndex = 0;

    m_vTrackList.clear();
	
}
// Frame
BOOL CUI_ID_FRAME_Task::OnFrameRun(DWORD /*dwTick*/)
{
#ifdef USE_OLD
	BEGIN_FUNC_PROFILER
	// 每5秒刷新一次
	if ( HQ_TimeGetTime() - m_dwLastRefreshTime >= 5000 )
	{
		const std::vector<sHERO_QUESTINFO>& stQuestInfos =
						GetPlayerRole().GetActiveQuestInfo();
		Refresh( stQuestInfos );
		if ( IsVisible() )
		{
			Refresh();
		}
	}
	END_FUNC_PROFILER
#endif
	return TRUE;
}
BOOL CUI_ID_FRAME_Task::OnFrameRender(DWORD /*dwTick*/)
{
	return TRUE;
}

void CUI_ID_FRAME_Task::Refresh()
{
	
	if( !m_bLoad )
		return;
	//UITask_ShowInfo(NULL,m_pID_LIST_Task->GetCurSelItem(), colCurrent);
	
}

void CUI_ID_FRAME_Task::Refresh(const std::vector<sHERO_QUESTINFO>& vtQuest )
{
#ifdef USE_OLD
	
	m_dwLastRefreshTime = HQ_TimeGetTime();
	if( !m_bLoad )
		return;
	int nListIndex = m_pID_LIST_Task->GetCurSelIndex();
	if ( nListIndex <= 0 )
	{
		nListIndex = 0;
	}
	m_pID_LIST_Task->Clear();
	//
	quest::sQUEST_INFO *pQuest = NULL;
	VUCtrlList::S_List stList;
	int nIndex = 0;
	for ( int n=0; n<vtQuest.size(); n++ )
	{
		pQuest = GetQuestManager().LoadQuest( vtQuest[n].m_QuestID );
		if ( !pQuest )
		{
			GetPlayerRole().ReqQuestInfo( vtQuest[n].m_QuestID );
			continue;
		}

        sHERO_QUESTINFO* pInfo = GetPlayerRole().FindQuestInfoById( vtQuest[n].m_QuestID );
        if( !pInfo )
        {
            continue;
        }
		//stListImg.m_stI .m_pIconInfo->Id() = vtQuest[n].m_QuestID;
		//stListImg.m_pIconInfo->m_strIconFileName = UIDOC_PATH( "Icon\\icon_reel\\icon_reel_003.tga";
		//stListImg.m_pIconInfo->SetInfo( pQuest->szName );

		// = 0xFFFFFFFF;
		//	蓝（比玩家等级低7级及以上）
		//	绿（比玩家等级低4级及以上）
		//	黄（和玩家等级上下相差3级以内）
		//	橙（比玩家等级高4级及以上）
		//	红（比玩家等级高7级及以上）
		int nDifLevel = pQuest->m_LimitLV - GetPlayerRole().GetLevel();
		if( nDifLevel <= -7 )
		{
			col = 0xFF42C1FF;//ARGB(0x00,0x00,0xff);
		}
		else if( nDifLevel <= -4 )
		{
			col = 0xFF00FF00;//RGB(0x00,0xff,0x00);
		}
		else if( nDifLevel >= 7 )
		{
			col = 0xFFFF0000;//RGB(0xff,0x00,0x00);
		}
		else if( nDifLevel >= 4 )
		{
			col = 0xFFFF7F00;//RGB(0xff,0x7f,0x00);
		}
		else
		{
			col = 0xFFFFFF00;//RGB(0xff,0xff,0x00);
		}
        
        std::vector<int>::iterator itr;
        itr = std::find(m_vTrackList.begin(), m_vTrackList.end(), pQuest->m_QuestID);

        if(itr != m_vTrackList.end())
        {
            if(pInfo->m_bFinished == TRUE)
            {
                m_vTrackList.erase(itr);
                UpdateTrackInfo();
                stList.SetData( pQuest->szName, pQuest->m_QuestID, NULL, col);
            }
            else
            {
                char szName[dr_MaxQuestDesc] = {0};
                sprintf(szName, "%-18s[*]", pQuest->szName);
                stList.SetData( szName, pQuest->m_QuestID, NULL, col);
            }
        }
        else
        {
		    stList.SetData( pQuest->szName, pQuest->m_QuestID, NULL, col);
        }

		// for update state
		UITask_ShowInfo(NULL, &stList, col);
		//
		m_pID_LIST_Task->AddItem( &stList );

		if( n == _nCurIndex )
		{
			colCurrent = col;
		}
	}

	m_pID_LIST_Task->SetCurSelIndex( _nCurIndex );
	UITask_ShowInfo(NULL, m_pID_LIST_Task->GetCurSelItem(), colCurrent);

    UpdateTrackInfo();
	
#endif
}
// Button
BOOL CUI_ID_FRAME_Task::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender )
{
	//SetVisible( FALSE );
	SetLayoutVisible();
	return TRUE;
}
// ListImg / ListEx
BOOL CUI_ID_FRAME_Task::ID_LISTEX_TaskOnIconDropTo( VUCtrlObject* pSender, VUCtrlObject* pMe,
										IconDragListImg* pItemDrag,
										IconDragListImg* pItemDest )
{
	return FALSE;
}
BOOL CUI_ID_FRAME_Task::ID_LISTEX_TaskOnIconDragOff( VUCtrlObject* pSender, VUCtrlObject* pMe,
										IconDragListImg* pItem )
{
	return FALSE;
}
//BOOL CUI_ID_FRAME_Task::ID_LISTEX_TaskOnIconLDBClick( VUCtrlObject* pSender, IconDragListImg* pItem )
//{
//	return FALSE;
//}
//BOOL CUI_ID_FRAME_Task::ID_LISTEX_TaskOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
//{
//	return FALSE;
//}
// Edit
void CUI_ID_FRAME_Task::ID_EDIT_InfoOnEditEnter( VUCtrlObject* pSender, const char* szData )
{
}
// Edit
void CUI_ID_FRAME_Task::ID_EDIT_StateOnEditEnter( VUCtrlObject* pSender, const char* szData )
{
}

BOOL CUI_ID_FRAME_Task::AbandonTaskCallBackFun( BOOL bPressYesButton, void *pData )
{
	MsgAbandonTask* pMsg = (MsgAbandonTask*)pData;
	if ( bPressYesButton == TRUE )
	{
		GetNetworkInput().SendMsg( pMsg );
	}
	return TRUE;
}
// Button
BOOL CUI_ID_FRAME_Task::ID_BUTTON_AbandonTaskOnButtonClick( VUCtrlObject* pSender )
{
#ifdef USE_OLD
	VUCtrlList::S_List *pList = NULL;
	pList = m_pID_LIST_Task->GetCurSelItem();
	if ( !pList )
		return FALSE;
	int nTaskId = pList->m_nID;
	sHERO_QUESTINFO* pInfo = GetPlayerRole().FindQuestInfoById( nTaskId );
	if ( !pInfo )
		return FALSE;
	if( QuestManager::IsQuestActive( pInfo->m_State ) )
	{
		quest::sQUEST_INFO *pQuest = GetQuestManager().LoadQuest( pInfo->m_QuestID );
		if ( pQuest && pQuest->byLevel >= 100 )
		{
			theUIMessageBox.Show( "此任务不可放弃!" );
			return FALSE;
		}

		MsgAbandonTask msg;
		msg.stTaskId = nTaskId;
		//是否放弃此任务 确认
		theUIMessageBox.Show( theTextResManager.GetString(eText_Ask_Task), theTextResManager.GetString(eText_Confirm),
			MB_YESNO, TRUE, CUI_ID_FRAME_Task::AbandonTaskCallBackFun,
							&msg, sizeof(msg) );
	}
#endif
	return TRUE;
}
// Button
BOOL CUI_ID_FRAME_Task::ID_BUTTON_ActionOnButtonClick( VUCtrlObject* pSender )
{
	//SetVisible( FALSE );
	UIAction.SetVisible( TRUE );
	return TRUE;
}
// Button
BOOL CUI_ID_FRAME_Task::ID_BUTTON_PropertyOnButtonClick( VUCtrlObject* pSender )
{
	//SetVisible( FALSE );
	s_CUI_ID_FRAME_Property.SetVisible( TRUE );
	return TRUE;
}
//BOOL ( VUCtrlObject* pSender, IconDragListImg* pItem );
BOOL UITask_ShowInfo( VUCtrlObject* pSender, VUCtrlList::S_List* pItem, COLORREF col)
{
#ifdef USE_OLD
	//*s_CUI_ID_FRAME_Task.m_pID_TEXT_Level = "";
	//*s_CUI_ID_FRAME_Task.m_pID_EDIT_Info = "";
	//*s_CUI_ID_FRAME_Task.m_pID_EDIT_State = "";
	*s_CUI_ID_FRAME_Task.m_pID_TEXT_Info = "";
	*s_CUI_ID_FRAME_Task.m_pID_TEXT_State = "";
	//
	if ( !pItem )
		return FALSE;
	quest::sQUEST_INFO *pQuest = GetQuestManager().LoadQuest( pItem->m_nID );
	sHERO_QUESTINFO* pInfo = GetPlayerRole().FindQuestInfoById( pItem->m_nID );
	if ( !pQuest || !pInfo )
		return FALSE;
	//		if ( pInfo->m_State < 0 || pInfo->m_State >= pQuest->vectorState.size() )
	//			return FALSE;
	//*s_CUI_ID_FRAME_Task.m_pID_TEXT_Level = pQuest->m_LimitLV;
//	*s_CUI_ID_FRAME_Task.m_pID_EDIT_Info = pQuest->m_QuestDesc;
	std::string strNeedItemInfo;
	quest::sQUEST_STEPINFO* pState = NULL;
	for( int n=0; n<pQuest->m_StepNum; n++ )
	{
		if ( pQuest->m_StepInfos[n].m_StepID == pInfo->m_State )
		{
			pState = &pQuest->m_StepInfos[n];
			break;
		}
	}
	if ( !pState )
		return FALSE;
	pInfo->m_bFinished = TRUE;
	quest::sQUEST_REWARD *pReward = NULL;
	for( int i = 0; i < pState->m_StepRequireNum; i++ )
	{
		pReward = &pState->m_StepRequires[i];
		char szInfo[256] = "";
		switch( pReward->m_RewardType )
		{
		case quest::eQUESTREWARD_ITEM:
			{
				giteminfo::SItemCommon* pItemDetail;
				pItemDetail = GetGameItemDetail().GetItemByName( pReward->szItem );
				if( pItemDetail )
				{
					int nItemCount = GetPlayerRole().m_bag.GetItemCount( pItemDetail->ustItemID );
					//需要任务物品
					sprintf( szInfo, "\n%s %s (%ld/%ld)", theTextResManager.GetString( eText_NeedItem ),
						pReward->szItem, nItemCount, pReward->m_Value );
					//
					if( nItemCount < pReward->m_Value )
					{
						pInfo->m_bFinished = FALSE;
					}
				}
				else
				{
					pInfo->m_bFinished = FALSE;
				}
			}
			break;
		case quest::eQUESTREWARD_VAR:
			{
				int nVar = 0;
				sHERO_QUESTINFO* pQuestInfo = NULL;
				pQuestInfo = GetPlayerRole().FindQuestInfoById( pReward->m_VarID );
				if( pQuestInfo )
					nVar = pQuestInfo->m_State;
				//需要
				sprintf( szInfo, "\n%s %s (%ld/%ld)",  theTextResManager.GetString(eUI_NeedText),
					pReward->szItem, nVar, pReward->m_Value );
				//
				if( nVar < pReward->m_Value )
					pInfo->m_bFinished = FALSE;
			}
			break;
		default:
			assert(FALSE);
			break;
		}
		strNeedItemInfo += szInfo;
	}

	std::string strInfo = pState->m_StepDesc;
	strInfo += strNeedItemInfo;
	//leo NpcCood
	StringText sRet;
	//*s_CUI_ID_FRAME_Task.m_pID_EDIT_State = strInfo;
	GetNpcCoordinateManager().AddHyperLinkInfo(strInfo.c_str(), sRet);
	*s_CUI_ID_FRAME_Task.m_pID_TEXT_State = sRet.c_str();
	
	//
	//std::string strIntro = pQuest->m_QuestDesc;
	//leo NpcCood
	StringText strIntro;
	if ( pItem &&
//		pState->m_StepRequireNum > 0 &&
		pInfo->m_bFinished == TRUE )
	{
		//完成
		string szCut = pQuest->szName;
		szCut += "(";
		szCut += theTextResManager.GetString(eText_complete);
		szCut += ")";
		pItem->SetData( szCut.c_str(), pQuest->m_QuestID, NULL, col, NULL );
	}
	//*s_CUI_ID_FRAME_Task.m_pID_EDIT_Info = strIntro;

	GetNpcCoordinateManager().AddHyperLinkInfo(pQuest->m_QuestDesc, strIntro);
	*s_CUI_ID_FRAME_Task.m_pID_TEXT_Info = strIntro.c_str();
#endif
	return TRUE;
}
// 装载UI
BOOL CUI_ID_FRAME_Task::LoadUI()
{
	
	GetUITaskTaskUIListener().RegisterMe();
	DWORD dwResult = GetVUCtrlManager().AddFrame( UIDOC_PATH( "Task") );
	if ( dwResult == 0 )
	{
		MESSAGE_BOX("读取文件[UI\\Task.UI]失败")
		return FALSE;
	}
	else if ( dwResult != 0/*文件版本号*/ )
	{
		MESSAGE_BOX("读取文件[UI\\Task.UI]与源代码版本不一样")
	}
	m_bLoad = TRUE;
    s_CUI_ID_FRAME_TRACK.LoadUI();
	return InitControls();
	
}
// 关连控件
BOOL CUI_ID_FRAME_Task::InitControls()
{
	
	VUIControl::InitControls();
	GetVUCtrlManager().OnFrameRun( ID_FRAME_Task, s_CUI_ID_FRAME_TaskOnFrameRun );
	GetVUCtrlManager().OnFrameRender( ID_FRAME_Task, s_CUI_ID_FRAME_TaskOnFrameRender );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Task, ID_BUTTON_CLOSE, s_CUI_ID_FRAME_TaskID_BUTTON_CLOSEOnButtonClick );
	//GetVUCtrlManager().OnIconDropTo( ID_FRAME_Task, ID_LISTEX_Task, s_CUI_ID_FRAME_TaskID_LISTEX_TaskOnIconDropTo );
//	GetVUCtrlManager().OnIconDragOff( ID_FRAME_Task, ID_LISTEX_Task, s_CUI_ID_FRAME_TaskID_LISTEX_TaskOnIconDragOff );
	//GetVUCtrlManager().OnIconLDBClick( ID_FRAME_Task, ID_LISTEX_Task, s_CUI_ID_FRAME_TaskID_LISTEX_TaskOnIconLDBClick );
	GetVUCtrlManager().OnListSelectChange( ID_FRAME_Task, ID_LIST_Task, s_CUI_ID_FRAME_TaskID_LIST_TaskOnListSelectChange );
//	GetVUCtrlManager().OnIconRDBClick( ID_FRAME_Task, ID_LISTEX_Task, s_CUI_ID_FRAME_TaskID_LISTEX_TaskOnIconRDBClick );
	//GetVUCtrlManager().OnEditEnter( ID_FRAME_Task, ID_EDIT_Info, s_CUI_ID_FRAME_TaskID_EDIT_InfoOnEditEnter );
	//GetVUCtrlManager().OnEditEnter( ID_FRAME_Task, ID_EDIT_State, s_CUI_ID_FRAME_TaskID_EDIT_StateOnEditEnter );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Task, ID_BUTTON_AbandonTask, s_CUI_ID_FRAME_TaskID_BUTTON_AbandonTaskOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Task, ID_BUTTON_Action, s_CUI_ID_FRAME_TaskID_BUTTON_ActionOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_Task, ID_BUTTON_Property, s_CUI_ID_FRAME_TaskID_BUTTON_PropertyOnButtonClick );
	GetVUCtrlManager().OnTextHyperLinkClick( ID_FRAME_Task, ID_TEXT_TaskInfo, s_CUI_ID_FRAME_TaskID_TEXT_Info_HyperLinkClick);
	GetVUCtrlManager().OnTextHyperLinkClick( ID_FRAME_Task, ID_TEXT_Taskstate, s_CUI_ID_FRAME_TaskID_TEXT_State_HyperLinkClick);


	m_pID_FRAME_Task = (VUCtrlFrame*)GetVUCtrlManager().FindFrame( ID_FRAME_Task );
	m_pID_BUTTON_CLOSE = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_BUTTON_CLOSE );
	//m_pID_LISTEX_Task = (VUCtrlListEx*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_LISTEX_Task );
	//m_pID_TEXT_Level = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_TEXT_Level );
	//m_pID_EDIT_Info = (VUCtrlEdit*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_EDIT_Info );
	//m_pID_EDIT_State = (VUCtrlEdit*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_EDIT_State );
	m_pID_TEXT_Info = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_TEXT_TaskInfo);
	m_pID_TEXT_State = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_TEXT_Taskstate);
	m_pID_BUTTON_AbandonTask = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_BUTTON_AbandonTask );
	m_pID_BUTTON_Action = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_BUTTON_Action );
	m_pID_BUTTON_Property = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_BUTTON_Property );
	m_pID_LIST_Task = (VUCtrlList*)GetVUCtrlManager().FindControl( ID_FRAME_Task, ID_LIST_Task);
	assert( m_pID_LIST_Task );
	assert( m_pID_FRAME_Task );
	m_pID_FRAME_Task->SetVisible(FALSE);
	GetVUILayout().AddFrame( m_pID_FRAME_Task );
	assert( m_pID_BUTTON_CLOSE );
	//assert( m_pID_LISTEX_Task );
	//assert( m_pID_TEXT_Level );
	//assert( m_pID_EDIT_Info );
	//assert( m_pID_EDIT_State );
	assert(m_pID_TEXT_Info);
	assert(m_pID_TEXT_State);
	assert( m_pID_BUTTON_AbandonTask );
	assert( m_pID_BUTTON_Action );
	assert( m_pID_BUTTON_Property );

	//m_pID_LISTEX_Task->SetButtonClickFun( UITask_ShowInfo );
	SetVisible( FALSE );
	USE_SCRIPT ( eUI_OBJECT_Task, m_pID_FRAME_Task );
	
    m_pID_LIST_Task->SetRBClickFun( ID_LIST_TaskOnRBClick );
    s_CUI_ID_FRAME_TRACK.SetVisible(TRUE);
	return TRUE;
	
}
// 卸载UI
BOOL CUI_ID_FRAME_Task::UnLoadUI()
{
	
	GetUITaskTaskUIListener().UnregisterMe();
	m_bLoad = FALSE;
	CLOSE_SCRIPT( eUI_OBJECT_Task );
    s_CUI_ID_FRAME_TRACK.UnLoadUI();
	return GetVUCtrlManager().RemoveFrame( UIDOC_PATH( "Task") );
	
}
// 是否可视
BOOL CUI_ID_FRAME_Task::IsVisible()
{
	
	if( !m_bLoad )
		return FALSE;
	return m_pID_FRAME_Task->IsVisible();
	
}
// 设置是否可视
void CUI_ID_FRAME_Task::SetVisible( BOOL bVisible )
{
	
	if( !m_bLoad )
		return;
	VUIControl::SetVisible( bVisible );
	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_Task->SetVisible( bVisible );
	
}
void CUI_ID_FRAME_Task::SetLayoutVisible()
{
	GetVUILayout().SetVisible( m_pID_FRAME_Task );
}
BOOL CUI_ID_FRAME_Task::ID_LIST_TaskOnListSelectChange( VUCtrlObject* pSender, VUCtrlList::S_List* pItem)
{
#ifdef USE_OLD
	_nCurIndex = m_pID_LIST_Task->GetCurSelIndex();

	const std::vector<sHERO_QUESTINFO>& stQuestInfos =	GetPlayerRole().GetActiveQuestInfo();
	Refresh( stQuestInfos );
#endif

	//UITask_ShowInfo(pSender, pItem, colCurrent);
	return TRUE;
}
void CUI_ID_FRAME_Task::ID_TEXT_Info_HyperLinkClick(VUCtrlObject* pSender, const char* szData)
{
	
	GetNpcCoordinateManager().ProcessHyperLink(szData);
	
}
void CUI_ID_FRAME_Task::ID_TEXT_State_HyperLinkClick(VUCtrlObject* pSender, const char* szData)
{
	
	GetNpcCoordinateManager().ProcessHyperLink(szData);
	
}
void CUI_ID_FRAME_Task::HideUnderLine()
{
	m_pID_TEXT_Info->SetIsShowLine(FALSE);
	m_pID_TEXT_State->SetIsShowLine(FALSE);
}

void CUI_ID_FRAME_Task::ID_LIST_TaskOnRBClick( VUCtrlObject* pSender, VUCtrlList::S_List* pItem )
{
    std::vector<int>::iterator itr;
    itr = std::find(s_CUI_ID_FRAME_Task.m_vTrackList.begin(), s_CUI_ID_FRAME_Task.m_vTrackList.end(), pItem->m_nID);
    if(itr == s_CUI_ID_FRAME_Task.m_vTrackList.end())
    {
        if(s_CUI_ID_FRAME_Task.m_vTrackList.size() < CUI_ID_FRAME_Task::MaxTrackNum)
        {
            s_CUI_ID_FRAME_Task.m_vTrackList.push_back(pItem->m_nID);
        }
    }
    else
    {
        s_CUI_ID_FRAME_Task.m_vTrackList.erase(itr);
    }
    GetPlayerRole().UpdateActiveQuestState();
}

void CUI_ID_FRAME_Task::UpdateTrackInfo()
{
#ifdef USE_OLD
    s_CUI_ID_FRAME_TRACK.m_pID_LIST_TRACK->Clear();

    VUCtrlList::S_List	stItem;
    DWORD dwColor = 0;
    
    std::vector<int>::iterator itr;
    for(itr=m_vTrackList.begin(); itr!=m_vTrackList.end(); itr++)
    {
        TrackInfo info;
        quest::sQUEST_INFO *pQuest = GetQuestManager().LoadQuest( *itr );
        sHERO_QUESTINFO* pInfo = GetPlayerRole().FindQuestInfoById( *itr );
        if( !pQuest || !pInfo )
        {
            break;
        }

        if( pInfo->m_State == 0 )
        {
            m_vTrackList.erase(itr++);
            break;
        }

        quest::sQUEST_STEPINFO* pState = NULL;
        for( int n=0; n<pQuest->m_StepNum; n++ )
        {
            if ( pQuest->m_StepInfos[n].m_StepID == pInfo->m_State )
            {
                pState = &pQuest->m_StepInfos[n];
                break;
            }
        }
        if( !pState )
        {
            break;
        } 

        stItem.SetData( pQuest->szName, 0, NULL, 0xffff8040 );
        s_CUI_ID_FRAME_TRACK.m_pID_LIST_TRACK->AddItem( &stItem );

        char szInfo[dr_MaxQuestDesc] = {0};
        quest::sQUEST_REWARD *pReward = NULL;
        pInfo->m_bFinished = TRUE;
        for( int i = 0; i < pState->m_StepRequireNum; i++ )
        {
            pReward = &pState->m_StepRequires[i];
            switch( pReward->m_RewardType )
            {
            case quest::eQUESTREWARD_ITEM:
                {
                    giteminfo::SItemCommon* pItemDetail;
                    pItemDetail = GetGameItemDetail().GetItemByName( pReward->szItem );
                    if( pItemDetail )
                    {
                        int nItemCount = GetPlayerRole().m_bag.GetItemCount( pItemDetail->ustItemID );

                        //需要任务物品
                        sprintf( szInfo, "- %s %ld/%ld", pReward->szItem, nItemCount, pReward->m_Value );
                        //
                        if(nItemCount < pReward->m_Value)
                        {
                            pInfo->m_bFinished = FALSE;
                        }

                    }
                }
                break;
            case quest::eQUESTREWARD_VAR:
                {
                    int nVar = 0;
                    sHERO_QUESTINFO* pQuestInfo = NULL;
                    pQuestInfo = GetPlayerRole().FindQuestInfoById( pReward->m_VarID );
                    if( pQuestInfo )
                        nVar = pQuestInfo->m_State;

                    //需要
                    sprintf( szInfo, "- %s %ld/%ld", pReward->szItem, nVar, pReward->m_Value );
                    //

                    if(nVar < pReward->m_Value)
                    {
                        pInfo->m_bFinished = FALSE;
                    }
                }
                break;
            default:
                assert(FALSE);
                break;
            }
            if(pInfo->m_bFinished)
            {
                dwColor = 0xff00ff00;
            }
            else
            {
                dwColor = 0xffc8c8c8; 
            }
            stItem.SetData( szInfo, 0, NULL, dwColor );
            s_CUI_ID_FRAME_TRACK.m_pID_LIST_TRACK->AddItem( &stItem );
        }
    }
#endif
}
//
//		m_pID_LISTEX_Task->SetButtonClickFun( ShowInfo );
//		SetVisible( FALSE );
//
//		USE_SCRIPT ( eUI_OBJECT_Task, m_pID_FRAME_Task );
//		return TRUE;
//	}
//	// 卸载UI
//	BOOL UnLoadUI()
//	{
//		return GetVUCtrlManager().RemoveFrame( UIDOC_PATH( "Task") );
//	}
//	// 是否可视
//	BOOL IsVisible()
//	{
//		return m_pID_FRAME_Task->IsVisible();
//	}
//	// 设置是否可视
//	void SetVisible( BOOL bVisible )
//	{
//		if( m_pID_FRAME_Task )
//			m_pID_FRAME_Task->SetVisible( bVisible );
//		//if( UIAction.m_pID_FRAME_ActionUI )
//		//	UIAction.SetVisible(!bVisible);
//		//if( UI_ID_FRAME_Property::m_pID_FRAME_Property )
//		//	UI_ID_FRAME_Property::SetVisible( !bVisible );
//	}
//
//	void Refresh()
//	{
//		ShowInfo(NULL,m_pID_LISTEX_Task->GetCurSelItem());
//	}
//
//	void Refresh( std::vector<sHERO_QUESTINFO>& vtQuest )
//	{
//		m_pID_LISTEX_Task->Clear();
//		//
//		quest::sQUEST_INFO *pQuest = NULL;
//		for ( int n=0; n<vtQuest.size(); n++ )
//		{
//			pQuest = GetQuestManager().LoadQuest( vtQuest[n].m_QuestID );
//			if ( !pQuest )
//			{
//				GetPlayerRole().ReqQuestInfo( vtQuest[n].m_QuestID );
//				continue;
//			}
//			IconDragListImg stListImg;
//			stListImg.m_pIconInfo->Id() = vtQuest[n].m_QuestID;
//			stListImg.m_pIconInfo->m_strIconFileName = UIDATA_PATH "Icon\\icon_reel\\icon_reel_003.tga";
//			stListImg.m_pIconInfo->SetInfo( pQuest->szName );
//			m_pID_LISTEX_Task->SetItem( &stListImg, n );
//		}
//		ShowInfo(NULL,m_pID_LISTEX_Task->GetCurSelItem());
//	}
//
//	BOOL ID_BUTTON_ActionOnButtonClick( VUCtrlObject* pSender )
//	{
//		SetVisible( FALSE );
//		UIAction.SetVisible( TRUE );
//
//		return TRUE;
//	}
//
//	BOOL ID_BUTTON_PropertyOnButtonClick( VUCtrlObject* pSender )
//	{
//		SetVisible( FALSE );
//		s_CUI_ID_FRAME_Property.SetVisible( TRUE );
//
//		return TRUE;
//	}
//}
//


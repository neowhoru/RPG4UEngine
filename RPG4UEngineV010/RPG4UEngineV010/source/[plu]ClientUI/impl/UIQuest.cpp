/*////////////////////////////////////////////////////////////////////////
文 件 名：UIQuest.h
DOC 文件：uidata\UIQuest.uidoc
UI  名称：Quest
创建日期：2008年7月6日
最后更新：2008年7月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIQuestTrack.h"

#include "VUIObjectManager.h"
#include "UIQuest.h"
#include "VUILayoutManager.h"
#include "UIMessageBox.h"
#include "TextResManager.h"
#include "ConstTextRes.h"
#include "ItemManager.h"
#include "Hero.h"
#include "QuestManager.h"
#include "UIAction.h"
#include "NpcCoordinateManager.h"
#include "ItemLogManager.h"
#include "UIItemLog.h"
#include "PacketStruct_ClientGameS_Event.h"
#include "HeroQuestManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace std;
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIQuest, ()  , gamemain::eInstPrioClientUI);

#define USE_UIQUEST_LAYOUT

namespace gameui
{ 

const DWORD COLOR_QUEST_WORKING	= 0xffc7c7c7;
const DWORD COLOR_QUEST_FINISHED	= 0xff00ff00;

const DWORD COLOR_LEVELDIST_7MINUS	= 0xFF42C1FF;
const DWORD COLOR_LEVELDIST_4MINUS	= 0xFF00FF00;
const DWORD COLOR_LEVELDIST_7			= 0xFFFF0000;
const DWORD COLOR_LEVELDIST_4			= 0xFFFF7F00;
const DWORD COLOR_LEVELDIST_0			= 0xFFFFFF00;


LPCSTR COLOR_QUEST_ENABLE	= "FFFF0000";
LPCSTR COLOR_QUEST_DISABLE	= "FF00FF00";


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
QuestUIListener& GetUIQuestQuestUIListener()
{
	static QuestUIListener staticQuestUIListener;
	return staticQuestUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIQuest, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIQuest, OnRender )
	UIPROC_BUTTON_CLICK			( theUIQuest, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK			( theUIQuest, ID_BUTTON_DISCARDOnButtonClick )
	UIPROC_BUTTON_CLICK			( theUIQuest, ID_BUTTON_helpOnButtonClick )
	UIPROC_LIST_SELECT_CHANGE	( theUIQuest, ID_LIST_QUESTOnListSelectChange )
	MAP_TEXT_HYPER_CLICKBACK			( theUIQuest, ID_TEXT_QUESTINFO_HyperLinkClick)
	MAP_TEXT_HYPER_CLICKBACK			( theUIQuest, ID_TEXT_QUESTSTEP_HyperLinkClick)

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
QuestUIListener::QuestUIListener()
{
}
QuestUIListener::~QuestUIListener()
{
}

LPCSTR QuestUIListener::GetName()
{
	return "QuestUIListener";
}

EGameUIType QuestUIListener::GetType()
{
	return eUIQuest;
}

void QuestUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUIQuest.SetVisible((BOOL)dwData);
		}
		break;
	}
}

BOOL QuestUIListener::GetData(DWORD dwType,void* pRet)
{
	switch(dwType)
	{
	case eGetVisible:
		{
			*(BOOL*)pRet = theUIQuest.IsVisible();
		}
		break;
	}
	return TRUE;
}

void QuestUIListener::Refresh(BOOL /*bExtra*/)
{
	theUIQuest.Refresh();
}

void QuestUIListener::TriggerFunc(ETriggerData eData,LPARAM /*lpData*/)
{
	switch(eData)
	{
	case eScriptUISetVisible:
		{
			if(theUIQuest.IsVisible())
			{
				theUIQuest.HideUnderLine();
			}
			theUIQuest.SetLayoutVisible();
		}
		break;
	}


}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIQuest::UIQuest()
{
	m_pID_FRAME_QUEST			= NULL;
	m_pID_BUTTON_CLOSE		= NULL;
	m_pID_BUTTON_DISCARD  	= NULL;
	m_pID_BUTTON_help 		= NULL;
	m_pID_TEXT_QUESTINFO 	= NULL;
	m_pID_TEXT_QUESTSTEP 	= NULL;
	m_pID_LIST_QUEST  		= NULL;

	m_bUILoaded						= FALSE;
	m_SelectIndex					= 0;
    m_TrackQuests.clear();
}

// Frame
BOOL UIQuest::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
	
}


BOOL UIQuest::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UIQuest::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	SetVisible(FALSE);
	return FALSE;
}



BOOL UIQuest::DiscardQuestCallBack( BOOL bPressYesButton, void * pData )
{
	if(!bPressYesButton)
		return FALSE;
	MSG_CG_EVENT_QUESTDATA_CHANGE_SYN	msg;
	msg.m_QuestID	= (CODETYPE)pData;
	msg.m_Data		= 0;
	msg.m_Action	= QUESTACTION_DISCARD;
	theHero.SendPacket(&msg,sizeof(msg), TRUE);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_DISCARD::OnButtonClick
BOOL UIQuest::ID_BUTTON_DISCARDOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	sUILIST_ITEM*	pList			= NULL;
	sHERO_QUESTINFO*		pHeroQuest;

	pList = m_pID_LIST_QUEST->GetCurSelItem();
	__CHECK_PTR(pList);

	CODETYPE				questID		= pList->m_nID;
	
	pHeroQuest = theHero.FindQuestInfoById( questID );
	__CHECK_PTR(pHeroQuest);

	if( QuestManager::IsQuestActive( pHeroQuest->m_QuestData ) )
	{
		sQUEST_INFO *pQuest;
		
		///此任务不可放弃
		pQuest	= theQuestManager.LoadQuest( pHeroQuest->m_QuestID );
		if ( pQuest && pQuest->m_QuestLV >= MAX_QUEST_LEVEL )
		{
			OUTPUTTOP(TEXTRES_QUEST_CANNT_DISCARD);
			OUTPUTTIP(TEXTRES_QUEST_CANNT_DISCARD);
			return FALSE;
		}

		///放弃任务前确认
		theUIMessageBox.Show	(TEXTRES_Ask_Task
                           ,TEXTRES_Confirm
									,MB_YESNO
									,TRUE
									,DiscardQuestCallBack
									,(void*)questID);
	}
	return TRUE;
}


////////////////////////////////////////////////////
//ID_BUTTON_help::OnButtonClick
BOOL UIQuest::ID_BUTTON_helpOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LIST_QUEST::OnListSelectChange
void UIQuest::ID_LIST_QUESTOnListSelectChange(VUCtrlObject* /*pSender*/, sUILIST_ITEM*	/*pItem*/)
{

	m_SelectIndex = m_pID_LIST_QUEST->GetCurSelIndex();

	RefreshHeroQuest();
   UpdateTrackInfo();


}

void UIQuest::ID_TEXT_QUESTINFO_HyperLinkClick(VUCtrlObject* /*pSender*/, LPCSTR szData)
{
	theNpcCoordinateManager.ProcessHyperLink(szData);
}

void UIQuest::ID_TEXT_QUESTSTEP_HyperLinkClick(VUCtrlObject* /*pSender*/, LPCSTR szData)
{
	theNpcCoordinateManager.ProcessHyperLink(szData);
}

void UIQuest::HideUnderLine()
{
	m_pID_TEXT_QUESTINFO->SetShowLine(FALSE);
	m_pID_TEXT_QUESTSTEP->SetShowLine(FALSE);
}


void UIQuest::ID_LIST_QUESTOnRBClick( VUCtrlObject* pSender, sUILIST_ITEM* pItem )
{
	theUIQuest.OnID_LIST_QUESTOnRBClick(pSender, pItem);
}

void UIQuest::OnID_LIST_QUESTOnRBClick( VUCtrlObject* /*pSender*/, sUILIST_ITEM* pItem )
{
    vector<int>::iterator itr;
    itr = std::find(m_TrackQuests.begin(), m_TrackQuests.end(), pItem->m_nID);
    if(itr == m_TrackQuests.end())
    {
        if(m_TrackQuests.size() < MAX_QUEST_TRACK)
        {
            m_TrackQuests.push_back(pItem->m_nID);
        }
    }
    else
    {
        m_TrackQuests.erase(itr);
    }
    theHero.UpdateActiveQuestState();
}


void UIQuest::UpdateTrackInfo()
{
	theUIQuestTrack.m_pID_LIST_TRACK->Clear();

	vector<int>::iterator	itr;
	sUILIST_ITEM		stItem;
	DWORD						dwColor = 0;

	////////////////////////////////////////////////////////////
	for(itr=m_TrackQuests.begin(); itr!=m_TrackQuests.end(); itr++)
	{
		sQUEST_TRACK			info;
		sQUEST_INFO *		pQuest;
		sHERO_QUESTINFO*	pInfo;
		sQUEST_STEPINFO*	pStepInfo = NULL;

		pQuest	= theQuestManager.LoadQuest( *itr );
		pInfo		= theHero.FindQuestInfoById( *itr );

		if( !pQuest || !pInfo )
			break;

		if( pInfo->m_QuestData == 0 )
		{
			m_TrackQuests.erase(itr++);
			break;
		}

		for( UINT n=0; n<pQuest->m_StepNum; n++ )
		{
			if ( (DWORD)pQuest->m_StepInfos[n].m_StepID == pInfo->m_QuestData )
			{
				pStepInfo = &pQuest->m_StepInfos[n];
				break;
			}
		}
		if( !pStepInfo )
			break;

		///////////////////////////////////////////////
		stItem.SetData( pQuest->m_Name, 0, NULL, 0xffff8040 );
		theUIQuestTrack.m_pID_LIST_TRACK->AddItem( &stItem );


		///////////////////////////////////////////////////////
		LPCSTR				szContent	= "";
		sQUEST_REWARD *	pReward	= NULL;
		sITEMINFO_BASE*		pItemInfo;
		BOOL					bFinished = TRUE;

		for( UINT i = 0; i < pStepInfo->m_StepRequireNum; i++ )
		{
			pReward = &pStepInfo->m_StepRequires[i];
			switch( pReward->m_RewardType )
			{
			case quest::eQUESTREWARD_ITEM:
				{
					pItemInfo = theItemInfoParser.GetItemInfo((LPCTSTR)pReward->m_ItemName);
					if( pItemInfo )
					{
						INT nItemCount;
						nItemCount = theItemManager.GetItemToltalAmount(pItemInfo->m_Code);

						//需要任务物品
						if( (DWORD)nItemCount < pReward->m_Value )
							bFinished = FALSE;

						DWORD dwFormatID = bFinished? TEXTRES_QUEST_TRACK_ITEM2 : TEXTRES_QUEST_TRACK_ITEM;
						szContent	= FMSTR	(_STRING(dwFormatID)//" > %s %ld/%ld"
													,(LPCTSTR)pReward->m_ItemName
													,nItemCount
													,pReward->m_Value);

					}
				}
				break;

			case quest::eQUESTREWARD_VAR:
				{
					int	nValue	= 0;
					nValue			= theHero.GetVar(pReward->m_VarID);

					if( (DWORD)nValue < pReward->m_Value )
						bFinished = FALSE;

					DWORD dwFormatID = bFinished? TEXTRES_QUEST_TRACK_MONSTER2 : TEXTRES_QUEST_TRACK_MONSTER;

					szContent =	FMSTR	(_STRING(dwFormatID)// " > %s(%ld/%ld)"
										, (LPCTSTR)pReward->m_ItemName
										, nValue
										, pReward->m_Value );

				}
				break;

			default:
				assert(FALSE);
				break;
			}


			if(bFinished)
				dwColor = COLOR_QUEST_FINISHED;
			else
				dwColor = COLOR_QUEST_WORKING; 

			stItem.SetData( szContent, 0, NULL, dwColor );
			theUIQuestTrack.m_pID_LIST_TRACK->AddItem( &stItem );
		}
	}
}

BOOL UIQuest::ShowQuestDetail( VUCtrlObject* /*pSender*/, sUILIST_ITEM* pItem, COLORREF col)
{
	*m_pID_TEXT_QUESTINFO = "";
	*m_pID_TEXT_QUESTSTEP = "";

	__CHECK(pItem);

	sQUEST_INFO *		pQuest;
	sHERO_QUESTINFO*	pInfo;

	pQuest	= theQuestManager.LoadQuest( pItem->m_nID );
	pInfo		= theHero.FindQuestInfoById( pItem->m_nID );

	__CHECK(pQuest&&pInfo);


	////////////////////////////////////////////
	//任务条件信息
	string				strNeedItemInfo;
	sQUEST_STEPINFO*	pStepInfo			= NULL;
	sQUEST_REWARD *	pReward				= NULL;
	sITEMINFO_BASE*		pItemInfo;

	for( UINT n=0; n<pQuest->m_StepNum; n++ )
	{
		if ( (DWORD)pQuest->m_StepInfos[n].m_StepID == pInfo->m_QuestData )
		{
			pStepInfo = &pQuest->m_StepInfos[n];
			break;
		}
	}

	__CHECK(pStepInfo);

	theItemLogManager.SetEnableLog(TRUE);
	theItemLogManager.ClearLog();
	/////////////////////////////////////////////////////
	//更新任务奖励情况
	for( UINT i = 0; i < pQuest->m_RewardNum; i++ )
	{
		quest::sQUEST_REWARD* r = &pQuest->m_Rewards[i];
		switch( r->m_RewardType )
		{
		case quest::eQUESTREWARD_MONEY:
			{
				theItemLogManager.PushMoney(r->m_Value, eITEMLOG_TOREWARD);
			}break;

		case quest::eQUESTREWARD_EXP:
			{
				theItemLogManager.PushExp(r->m_Value, eITEMLOG_TOREWARD);
			}break;

		case quest::eQUESTREWARD_SKILLEXP:
			{
				theItemLogManager.PushSkillExp(r->m_Value, eITEMLOG_TOREWARD);
			}break;

		case quest::eQUESTREWARD_ITEM:
			{
				theItemLogManager.PushItem((LPCTSTR)r->m_ItemName, r->m_Value, eITEMLOG_TOREWARD);
			}break;
		}
	}


	/////////////////////////////////////////////////////
	//更新任务条件情况
	for( UINT i = 0; i < pStepInfo->m_StepRequireNum; i++ )
	{
		pReward = &pStepInfo->m_StepRequires[i];

		LPCSTR szContent  = "";
		switch( pReward->m_RewardType )
		{
			////////////////////////////////////////
			//物品条件
		case quest::eQUESTREWARD_ITEM:
			{
				pItemInfo = theItemInfoParser.GetItemInfo((LPCTSTR)pReward->m_ItemName);
				if( pItemInfo )
				{
					INT		nItemCount;
					DWORD		dwFormatID;

					nItemCount	= theItemManager.GetItemToltalAmount(pItemInfo->m_Code);
					dwFormatID	= (DWORD)nItemCount < pReward->m_Value?TEXTRES_QUEST_REQUIRE_ITEM2 : TEXTRES_QUEST_REQUIRE_ITEM;

					theItemLogManager.PushItem((LPCTSTR)pReward->m_ItemName, pReward->m_Value, eITEMLOG_REQUIRE);

					//需要任务物品
					szContent	= FMSTR	(_STRING(dwFormatID)//"\n{#%s%s %s (%ld/%ld)#}"
												,(LPCTSTR)pReward->m_ItemName
												,nItemCount
												,pReward->m_Value);
				}
			}break;

			/////////////////////////////////////////
			//变量条件
		case quest::eQUESTREWARD_VAR:
			{
				int		nValue		= 0;
				DWORD		dwFormatID	= (DWORD)nValue < pReward->m_Value?TEXTRES_QUEST_REQUIRE_MONSTER2 : TEXTRES_QUEST_REQUIRE_MONSTER;

				nValue	= theHero.GetVar(pReward->m_VarID);


				szContent =	FMSTR	(_STRING(dwFormatID)// "\n{#%s=%s %s (%ld/%ld)#}"
										,(LPCTSTR) pReward->m_ItemName
										, nValue
										, pReward->m_Value );
			}
			break;

		default:
			assert(FALSE);
			break;
		}
		strNeedItemInfo += _T("\n");
		strNeedItemInfo += szContent;
	}


	theItemLogManager.SetEnableLog(FALSE);

	/////////////////////////////////////////////////////
	//Npc链接信息处理
	StringHandle	sRet;
	std::string		strInfo;
	StringHandle	strIntro;
	
	strInfo += pStepInfo->m_StepDesc;
	strInfo += strNeedItemInfo;

	theNpcCoordinateManager.ConvertHyperLink(strInfo.c_str(), sRet);
	*m_pID_TEXT_QUESTSTEP = (LPCTSTR)sRet;
	

	/////////////////////////////////////////////////
	//处理任务完成结果
	if (pInfo->m_bFinished == TRUE )
	{
		pItem->SetData	( FMSTR("%s(%s)", pQuest->m_Name, _STRING(TEXTRES_complete))
							, pQuest->m_QuestID
							, NULL
							, col
							, NULL );
	}

	theNpcCoordinateManager.ConvertHyperLink(pQuest->m_QuestDesc, strIntro);
	*m_pID_TEXT_QUESTINFO = (LPCTSTR)strIntro;

	return TRUE;
}

// 设置是否可视
void UIQuest::SetVisible(BOOL bVisible )
{
#ifdef USE_UIQUEST_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_QUEST->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		theItemLogManager.ClearLog();
		theItemLogManager.SetEnableLog(FALSE);
		theUIItemLog.SetAutoHide(TRUE);
		theUIItemLog.SetVisible(FALSE);
		m_pID_FRAME_QUEST->SetArrangeMode( ArrayMode_Normal );
	}
	else
	{
		theHero.UpdateActiveQuestState();

		//m_pID_FRAME_QUEST->SetMarginRight();
		theHero.OnQuestDone(0);
		*m_pID_TEXT_QUESTINFO = "";
		*m_pID_TEXT_QUESTSTEP = "";
		theUIItemLog.SetAutoHide(FALSE);


		Refresh();
	}


#else
		m_pID_FRAME_QUEST->SetVisible( bVisible );
#endif
	
}
void UIQuest::SetLayoutVisible()
{
#ifdef USE_UIQUEST_LAYOUT
	theUILayoutManager.SetVisible(m_pID_FRAME_QUEST);
#endif
}

void UIQuest::Refresh()
{
	RefreshHeroQuest();
   UpdateTrackInfo();
}

void UIQuest::RefreshHeroQuest()
{
	__CHECK2(IsVisible(),);
	__CHECK2(m_bUILoaded,);

	int nListIndex;

	m_dwLastRefreshTime = base::GetRunTime();

	nListIndex = m_pID_LIST_QUEST->GetCurSelIndex();
	if ( nListIndex < 0 )
		nListIndex = 0;

	m_pID_LIST_QUEST->Clear();



	sHERO_QUESTINFO*		pHeroQuest;
	int						nDifLevel;
	sUILIST_ITEM	stList;
	sQUEST_INFO *			pQuest		= NULL;
	//int						nIndex		= 0;
	HeroQuestInfoArray&	questInfos	= theHero.GetActiveQuestInfo();

	for (UINT n=0; n<questInfos.size(); n++ )
	{
		pQuest = theQuestManager.LoadQuest( questInfos[n].m_QuestID );
		if ( !pQuest )
		{
			theHero.ReqQuestInfo( questInfos[n].m_QuestID );
			continue;
		}

		pHeroQuest = theHero.FindQuestInfoById( questInfos[n].m_QuestID );
		if( !pHeroQuest )
			continue;

		theHeroQuestManager.UpdateQuestState(pHeroQuest);


		// 白 = 0xFFFFFFFF;
		//	蓝（比玩家等级低7级及以上）
		//	绿（比玩家等级低4级及以上）
		//	黄（和玩家等级上下相差3级以内）
		//	橙（比玩家等级高4级及以上）
		//	红（比玩家等级高7级及以上）

		nDifLevel = pQuest->m_LimitLV - theHero.GetLevel();

		if( nDifLevel <= -7 )			m_Color = COLOR_LEVELDIST_7MINUS	;
		else if( nDifLevel <= -4 )		m_Color = COLOR_LEVELDIST_4MINUS	;
		else if( nDifLevel >= 7 )		m_Color = COLOR_LEVELDIST_7		;
		else if( nDifLevel >= 4 )		m_Color = COLOR_LEVELDIST_4		;
		else									m_Color = COLOR_LEVELDIST_0		;
    
		////////////////////////
		//查找Quest是否在Track中
		vector<int>::iterator itr;
		itr = std::find(m_TrackQuests.begin(), m_TrackQuests.end(), pQuest->m_QuestID);

		if(itr != m_TrackQuests.end())
		{
			if(pHeroQuest->m_bFinished == TRUE)
			{
				m_TrackQuests.erase(itr);
				UpdateTrackInfo();
				stList.SetData( pQuest->m_Name, pQuest->m_QuestID, NULL, m_Color);
			}
			else
			{
				stList.SetData(FMSTR("%-18s>>>", pQuest->m_Name)
                          ,pQuest->m_QuestID
								  ,NULL
								  ,m_Color);
			}
		}
		else
		{
			stList.SetData( pQuest->m_Name, pQuest->m_QuestID, NULL, m_Color);
		}

		ShowQuestDetail(NULL, &stList, m_Color);
		m_pID_LIST_QUEST->AddItem( &stList );

		if( n == (UINT)m_SelectIndex )
		{
			m_ColorCurrent = m_Color;
		}
	}

	m_pID_LIST_QUEST->SetCurSelIndex( m_SelectIndex );
	ShowQuestDetail(NULL, m_pID_LIST_QUEST->GetCurSelItem(), m_ColorCurrent);

}


// 装载UI
BOOL UIQuest::LoadUI()
{
	GetUIQuestQuestUIListener().RegisterMe();

	m_pID_FRAME_QUEST = theUICtrlManager.LoadFrame( UIDOC_PATH( "Quest") );
	if ( m_pID_FRAME_QUEST == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Quest") "]失败");
		return FALSE;
	}

#ifdef USE_UIQUEST_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_QUEST);
#endif

	m_bUILoaded = TRUE;
    theUIQuestTrack.LoadUI();

	m_pID_FRAME_QUEST->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIQuest::InitControls()
{
	//INT n;

	m_pID_FRAME_QUEST->SetProcOnFrameMove			(theUIQuestOnFrameMove);
	m_pID_FRAME_QUEST->SetProcOnRender		(theUIQuestOnRender, FALSE);
	//m_pID_FRAME_QUEST->SetProcOnMsgProc		( UIQuest_MsgProc );

	
	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)	m_pID_FRAME_QUEST->FindControl( UIQUEST_ID_BUTTON_CLOSE );
	m_pID_BUTTON_DISCARD		= (VUCtrlButton*)	m_pID_FRAME_QUEST->FindControl( UIQUEST_ID_BUTTON_DISCARD );
	m_pID_BUTTON_help	 		= (VUCtrlButton*)	m_pID_FRAME_QUEST->FindControl( UIQUEST_ID_BUTTON_help );
	m_pID_TEXT_QUESTINFO		= (VUCtrlText*)  	m_pID_FRAME_QUEST->FindControl( UIQUEST_ID_TEXT_QUESTINFO );
	m_pID_TEXT_QUESTSTEP		= (VUCtrlText*)  	m_pID_FRAME_QUEST->FindControl( UIQUEST_ID_TEXT_QUESTSTEP );
	m_pID_LIST_QUEST	  		= (VUCtrlList*)  	m_pID_FRAME_QUEST->FindControl( UIQUEST_ID_LIST_QUEST );
	
	
	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_BUTTON_DISCARD );
	assert( m_pID_BUTTON_help );
	assert( m_pID_TEXT_QUESTINFO );
	assert( m_pID_TEXT_QUESTSTEP );
	assert( m_pID_LIST_QUEST );
	
	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_BUTTON_CLOSE		->SetProcOnButtonClick 	( theUIQuestID_BUTTON_CLOSEOnButtonClick );
	m_pID_BUTTON_DISCARD  	->SetProcOnButtonClick 	( theUIQuestID_BUTTON_DISCARDOnButtonClick );
	m_pID_BUTTON_help 		->SetProcOnButtonClick 	( theUIQuestID_BUTTON_helpOnButtonClick );
	m_pID_LIST_QUEST  		->SetProcOnSelectChange	( theUIQuestID_LIST_QUESTOnListSelectChange );

	m_pID_TEXT_QUESTINFO		->SetProcOnHyperLinkClick(theUIQuestID_TEXT_QUESTINFO_HyperLinkClick);
	m_pID_TEXT_QUESTSTEP		->SetProcOnHyperLinkClick(theUIQuestID_TEXT_QUESTSTEP_HyperLinkClick);
	
    m_pID_LIST_QUEST->SetProcOnRButtonDBClick( ID_LIST_QUESTOnRBClick );
    theUIQuestTrack.SetVisible(TRUE);

	UISCRIPT_ENABLE ( UIOBJ_QUEST, m_pID_FRAME_QUEST );
	return TRUE;
}


UIQuest::~UIQuest()
{
	//UnLoadUI();
	
}


// 卸载UI
BOOL UIQuest::UnLoadUI()
{
	GetUIQuestQuestUIListener().UnregisterMe();
	m_bUILoaded	= FALSE;
	UISCRIPT_DISABLE( UIOBJ_QUEST );
   theUIQuestTrack.UnLoadUI();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Quest") );
	
}

// 是否可视
BOOL UIQuest::IsVisible()
{
	if( m_pID_FRAME_QUEST )
		return m_pID_FRAME_QUEST->IsVisible();

	return FALSE;
}


};//namespace gameui


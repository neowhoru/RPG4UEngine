/*////////////////////////////////////////////////////////////////////////
文 件 名：UIChatSystem.cpp
创建日期：2007年11月29日
最后更新：2007年11月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UISystemLog.h"
#include "ColorManager.h"


#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(UISystemLog, ()  , gamemain::eInstPrioClientUI/*UISystemLog*/);
#else
#endif

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUISystemLog, OnFrameMove )
		UIPROC_FRAME_RENDER( theUISystemLog, OnRender )
		UIPROC_LIST_SELECT_CHANGE( theUISystemLog, ID_LIST_SystemInfoOnListSelectChange )
};//namespace uicallback
using namespace uicallback;


UISystemLog::UISystemLog()
{
	
	// Member
	m_pID_FRAME_ChatSystem = NULL;
	m_pID_LIST_SystemInfo = NULL;
	
}
// Frame
BOOL UISystemLog::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UISystemLog::OnRender(DWORD /*dwTick*/)
{
	
	RECT rt = {0}, rc = {0};
	m_pID_LIST_SystemInfo->GetGlobalRect(&rt);
	if( m_pID_LIST_SystemInfo->GetScrollBarCtrl() )
		m_pID_LIST_SystemInfo->GetScrollBarCtrl()->GetGlobalRect(&rc);
	
	if(	PtInRect( &rt, theUICtrlManager.m_MousePos) || PtInRect( &rc, theUICtrlManager.m_MousePos))
	{
		static DWORD startTime = timeGetTime();
		static DWORD endTime = 0;
		DWORD delayTime  = 0;

		endTime = timeGetTime();
		delayTime = endTime - startTime;//timeGetTime();

		if( delayTime > 1000 )
		{
			startTime = endTime;
			m_pID_LIST_SystemInfo->SetScrollVisable();
		}	
	}
	else
	{
		static DWORD startTime = timeGetTime();
		static DWORD endTime = 0;
		DWORD delayTime  = 0;

		endTime = timeGetTime();
		delayTime = endTime - startTime;//timeGetTime();

		if( delayTime > 1000 )
		{	
			startTime = endTime;
			m_pID_LIST_SystemInfo->SetScrollVisable(FALSE);
		}
	}
	return TRUE;
	
}
// List
void UISystemLog::ID_LIST_SystemInfoOnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
{
	
	
}

// 装载UI
BOOL UISystemLog::LoadUI()
{
	
	m_pID_FRAME_ChatSystem = theUICtrlManager.LoadFrame(UIDOC_PATH( "SystemLog") , TRUE, TRUE );
	if ( m_pID_FRAME_ChatSystem == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "SystemLog")"]失败")
			return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UISystemLog::InitControls()
{
	m_pID_LIST_SystemInfo = (VUCtrlList*)m_pID_FRAME_ChatSystem->FindControl(  ID_LIST_SystemInfo );
	assert( m_pID_LIST_SystemInfo );
	
	m_pID_FRAME_ChatSystem->SetProcOnFrameMove(  theUISystemLogOnFrameMove );
	m_pID_FRAME_ChatSystem->SetProcOnRender(  theUISystemLogOnRender );
	m_pID_LIST_SystemInfo->SetProcOnSelectChange(   theUISystemLogID_LIST_SystemInfoOnListSelectChange );


	m_pID_LIST_SystemInfo->SetSelectionInfo( FALSE );
	m_pID_FRAME_ChatSystem->SetMsgHangUp( FALSE );

	m_pID_LIST_SystemInfo->SetFadeChildren(TRUE);
	m_pID_LIST_SystemInfo->GetScrollBarCtrl()->SetFadeChildren(TRUE);
	m_pID_LIST_SystemInfo->SetMsgHangUp( FALSE );

	VUCtrlScrollBar* pScrollBar = m_pID_LIST_SystemInfo->GetScrollBarCtrl();
	if( pScrollBar )
	{
		pScrollBar->SetMaxValue( 1 );
		pScrollBar->SetStepValue( 1 );
	}

	return TRUE;
	
}
// 卸载UI
BOOL UISystemLog::UnLoadUI()
{
	
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "SystemLog") );
	
}
// 是否可视
BOOL UISystemLog::IsVisible()
{
	
	return m_pID_FRAME_ChatSystem->IsVisible();
	
}
// 设置是否可视
void UISystemLog::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_ChatSystem->SetVisible( bVisible );
	
}
void UISystemLog::AddInfo( LPCSTR  szStr, COLOR col)
{
	if(col == 0)
		col = SYSTEM_BROADCAST_COLOR;


	int nLen = strlen(szStr);
	if ( nLen > 0 && m_pID_LIST_SystemInfo )
	{
		sUILIST_ITEM	stItem;
		int nWidthByte = m_pID_LIST_SystemInfo->GetWidth()*2/m_pID_LIST_SystemInfo->GetFontSize();
		if ( (int)nLen > nWidthByte )
		{
			char *pszInfo = new char[nWidthByte+1];

			memset( pszInfo, 0, nWidthByte+1 );
			int nChineseByteCount = 0;

			for ( int n=0; n<nWidthByte; n++ )
			{
				if ( szStr[n] & 0x80 )
				{
					nChineseByteCount++;
				}
			}
			if ( nChineseByteCount & 0x1 )
			{
				nWidthByte--;
			}
			memcpy( pszInfo, szStr, nWidthByte );

			stItem.SetData( pszInfo , 0, NULL, col );
			m_pID_LIST_SystemInfo->AddItem( &stItem );
			delete[] pszInfo;

			AddInfo( &szStr[nWidthByte], col );
		}
		else
		{
			stItem.SetData( szStr, 0, NULL, col);
			m_pID_LIST_SystemInfo->AddItem( &stItem );
		}

	}

	{
		VUCtrlScrollBar* pScrollBar = m_pID_LIST_SystemInfo->GetScrollBarCtrl();
		if( pScrollBar )
		{
			int nMax = pScrollBar->GetMaxValue();
			if( nMax < 1000 && nMax > 0)
				pScrollBar->SetMaxValue( ++nMax );
			pScrollBar->SetStepValue( 1 );
		}
	}
	
}
/*////////////////////////////////////////////////////////////////////////
文 件 名：UITeamHero.cpp
创建日期：2007年9月20日
最后更新：2007年9月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UITeamHero.h"
#include "MediaPathManager.h"
#include "PathInfoParser.h"
#include "UIMenuHero.h"
#include "UITeamManager.h"
#include "UITeamHeroUIListener.h"
#include "VHero.h"
#include "VHeroActionInput.h"
#include "GlobalInstancePriority.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UITeamHero, ()  , gamemain::eInstPrioClientUI);


namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::UITeamHeroUIListener& GetUITeamHeroUITeamHeroUIListener()
{
	static gameui::UITeamHeroUIListener staticUITeamHeroUIListener;
	return staticUITeamHeroUIListener;
}


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE  ( theUITeamHero, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUITeamHero, OnRender )
	UIPROC_BUTTON_CLICK     (theUITeamHero, GroupME_PKNone_OnButtonClick); 
	UIPROC_BUTTON_CLICK     (theUITeamHero, GroupME_PKAll_OnButtonClick); 
	UIPROC_BUTTON_CLICK     (theUITeamHero, GroupME_PKTeam_OnButtonClick); 
	UIPROC_BUTTON_CLICK     (theUITeamHero, GroupME_PKGuild_OnButtonClick); 
	UIPROC_BUTTON_CLICK     (theUITeamHero, ID_FRAME_TeamHeroOnClick); 
	UIPROC_BUTTON_CLICK     (theUITeamHero, OnButtonClickHead); 
	UIPROC_ICON_LDB_CLICK   (theUITeamHero, OnPetStateClick );
};//namespace uicallback
using namespace uicallback;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UITeamHero::UITeamHero()
{

	// Member
	m_pID_FRAME_TeamHero	= NULL;
	m_pID_TEXT_Name		= NULL;
	m_pID_PROGRESS_Hp		= NULL;
	m_pID_PROGRESS_Mp		= NULL;
	m_pID_TEXT_HP			= NULL;
	m_pID_TEXT_MP			= NULL;
	m_pID_PROGRESS_Exp	= NULL;
	m_pID_TEXT_Exp			= NULL;
	m_pID_TEXT_LVL			= NULL;
	m_pID_Pic_Head			= NULL;

	m_pID_Progress_Rage	= NULL;
	m_pID_Btn_PKNone		= NULL;
	m_pID_Btn_PKAll		= NULL;
	m_pID_Btn_PKTeam		= NULL;
	m_pID_Btn_PKGuild		= NULL;
	m_pID_Pic_Rage1		= NULL;
	m_pID_Pic_Rage2		= NULL;
	m_pID_Pic_Rage3		= NULL;
	m_pID_Pic_Rage4		= NULL;
	m_pID_Pic_Rage5		= NULL;

	m_pID_PICTURE_pet			= NULL;
	m_pID_TEXT_petname		= NULL;
	m_pID_TEXT_petlevel		= NULL;
	m_pID_PROGRESS_petMp		= NULL;
	m_pID_PROGRESS_petHp		= NULL;
	m_pID_TEXT_pethp			= NULL;
	m_pID_TEXT_petmp			= NULL;
	m_picHeaderMe				= NULL;
	m_pID_LISTIMG_PETSTATE	= NULL;


}

BOOL UITeamHero::OnButtonClickHead( OUT VUCtrlObject* /*pSender*/ )
{
	if( !theUIMenuHero.IsVisible() )
		theUIMenuHero.SetVisible(TRUE);
	else
		theUIMenuHero.SetVisible(FALSE);

	RECT rt;
	m_pID_FRAME_TeamHero->GetGlobalRect( &rt );
	theUIMenuHero.m_pID_FRAME_SelfRBMenu->SetPos(rt.right,rt.bottom)		;
	//theUIMenuHero.m_pID_FRAME_SelfRBMenu->SetPos(theVUCtrlManager.m_MousePos.x
 //                                              ,theVUCtrlManager.m_MousePos.y + 10)		;

	theUITeamManager.Refresh();
	return TRUE;
	
}

BOOL UITeamHero::ID_FRAME_TeamHeroOnClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}

// Frame
BOOL UITeamHero::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UITeamHero::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}

BOOL UITeamHero::GroupME_PKNone_OnButtonClick(VUCtrlObject* /*pSender*/)
{
	
	m_pID_Btn_PKNone->SetVisible(FALSE);
	m_pID_Btn_PKAll->SetVisible(TRUE);
	m_pID_Btn_PKTeam->SetVisible(FALSE);
	m_pID_Btn_PKGuild->SetVisible(FALSE);

	return FALSE;
	
}
BOOL UITeamHero::GroupME_PKAll_OnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	m_pID_Btn_PKNone->SetVisible(FALSE);
	m_pID_Btn_PKAll->SetVisible(FALSE);
	m_pID_Btn_PKTeam->SetVisible(TRUE);
	m_pID_Btn_PKGuild->SetVisible(FALSE);

	return FALSE;
	
}
BOOL UITeamHero::GroupME_PKTeam_OnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	m_pID_Btn_PKNone->SetVisible(FALSE);
	m_pID_Btn_PKAll->SetVisible(FALSE);
	m_pID_Btn_PKTeam->SetVisible(FALSE);
	m_pID_Btn_PKGuild->SetVisible(TRUE);
	return FALSE;
	
}
BOOL UITeamHero::GroupME_PKGuild_OnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	m_pID_Btn_PKNone->SetVisible(TRUE);
	m_pID_Btn_PKAll->SetVisible(FALSE);
	m_pID_Btn_PKTeam->SetVisible(FALSE);
	m_pID_Btn_PKGuild->SetVisible(FALSE);

	return FALSE;
	
}
// 装载UI
BOOL UITeamHero::LoadUI()
{
	GetUITeamHeroUITeamHeroUIListener().RegisterMe();

	m_pID_FRAME_TeamHero = theUICtrlManager.LoadFrame( UIDOC_PATH( "TeamHero"),TRUE, TRUE );
	if ( m_pID_FRAME_TeamHero == 0 )
	{
		UIMessageLog("读取文件[UI\\TeamHero.UI]失败")
			return FALSE;
	}

	m_pID_FRAME_TeamHero = (VUCtrlFrame*)theUICtrlManager.FindFrame( ID_FRAME_TeamHero );

	return InitControls();
	
}

BOOL UITeamHero::OnPetStateClick( VUCtrlObject* /*pSender*/, IconDragListImg* pItem )
{
	assert(0 && " UITeamHero..OnPetStateClick 宠物控制...");
#pragma message(__FILE__  "(213) UITeamHero..OnPetStateClick 宠物控制... " )

	if ( !pItem )
		return FALSE;

	return TRUE;
}


// 关连控件
BOOL UITeamHero::InitControls()
{
	m_pID_FRAME_TeamHero->SetProcOnFrameMove	(theUITeamHeroOnFrameMove);
	m_pID_FRAME_TeamHero->SetProcOnRender		(theUITeamHeroOnRender ,FALSE);


	m_pID_TEXT_Name	= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_Name );
	m_pID_PROGRESS_Hp = (VUCtrlProgress*)	m_pID_FRAME_TeamHero->FindControl( ID_PROGRESS_Hp );
	m_pID_PROGRESS_Mp = (VUCtrlProgress*)	m_pID_FRAME_TeamHero->FindControl( ID_PROGRESS_Mp );
	m_pID_TEXT_HP		= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_HP );
	m_pID_TEXT_MP		= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_MP );
	m_pID_PROGRESS_Exp= (VUCtrlProgress*)	m_pID_FRAME_TeamHero->FindControl( ID_PROGRESS_Exp );
	m_pID_TEXT_Exp		= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_Exp );
	m_pID_TEXT_LVL		= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_level );
	m_pID_Pic_Head		= (VUCtrlPicture*)	m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_Head );

	m_pID_Progress_Rage	=(VUCtrlProgress*)m_pID_FRAME_TeamHero->FindControl( ID_PROGRESS_RAGE);
	m_pID_Btn_PKNone		= (VUCtrlButton*)	m_pID_FRAME_TeamHero->FindControl( ID_BUTTON_PKNONE);
	m_pID_Btn_PKAll		= (VUCtrlButton*)	m_pID_FRAME_TeamHero->FindControl( ID_BUTTON_PKALL);
	m_pID_Btn_PKTeam		= (VUCtrlButton*)	m_pID_FRAME_TeamHero->FindControl( ID_BUTTON_PKTEAM);
	m_pID_Btn_PKGuild		= (VUCtrlButton*)	m_pID_FRAME_TeamHero->FindControl( ID_BUTTON_PKGUILD);
	m_pID_Pic_Rage1		= (VUCtrlPicture*)m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_RAGE1);
	m_pID_Pic_Rage2		= (VUCtrlPicture*)m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_RAGE2);
	m_pID_Pic_Rage3		= (VUCtrlPicture*)m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_RAGE3);
	m_pID_Pic_Rage4		= (VUCtrlPicture*)m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_RAGE4);
	m_pID_Pic_Rage5		= (VUCtrlPicture*)m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_RAGE5);


	m_pID_PICTURE_pet			 = (VUCtrlPicture*)	m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_pet );
	m_pID_TEXT_petname		= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_petname );
	m_pID_TEXT_petlevel		= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_petlevel );
	m_pID_PROGRESS_petMp		= (VUCtrlProgress*)	m_pID_FRAME_TeamHero->FindControl( ID_PROGRESS_petMp );
	m_pID_PROGRESS_petHp		= (VUCtrlProgress*)	m_pID_FRAME_TeamHero->FindControl( ID_PROGRESS_petHp );
	m_pID_TEXT_pethp			= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_pethp );
	m_pID_TEXT_petmp			= (VUCtrlText*)		m_pID_FRAME_TeamHero->FindControl( ID_TEXT_petmp );
	m_pID_LISTIMG_PETSTATE	= (VUCtrlListImg*)	m_pID_FRAME_TeamHero->FindControl( ID_LISTIMG_PETSTATE );
	m_picHeaderMe				= (VUCtrlPicture*)	m_pID_FRAME_TeamHero->FindControl( ID_PICTURE_HeaderMe);

	assert( m_pID_TEXT_Name );
	assert( m_pID_PROGRESS_Hp );
	assert( m_pID_PROGRESS_Mp );
	assert( m_pID_TEXT_HP );
	assert( m_pID_TEXT_MP );
	assert( m_pID_PROGRESS_Exp );
	assert( m_pID_TEXT_Exp );
	assert( m_pID_Pic_Head );
	assert(m_pID_Progress_Rage);
	assert(m_pID_Btn_PKNone);
	assert(m_pID_Btn_PKAll);
	assert(m_pID_Btn_PKTeam);
	assert(m_pID_Btn_PKGuild);
	assert(m_pID_Pic_Rage1);
	assert(m_pID_Pic_Rage2);
	assert(m_pID_Pic_Rage3);
	assert(m_pID_Pic_Rage4);
	assert(m_pID_Pic_Rage5);
	assert( m_pID_PICTURE_pet );
	assert( m_pID_TEXT_petname );
	assert( m_pID_TEXT_petlevel );
	assert( m_pID_PROGRESS_petMp );
	assert( m_pID_PROGRESS_petHp );
	assert( m_pID_TEXT_pethp );
	assert( m_pID_TEXT_petmp );
	assert( m_pID_LISTIMG_PETSTATE );
	assert(m_picHeaderMe);


	m_pID_Btn_PKNone	->SetProcOnButtonClick(theUITeamHeroGroupME_PKNone_OnButtonClick);
	m_pID_Btn_PKAll	->SetProcOnButtonClick(theUITeamHeroGroupME_PKAll_OnButtonClick);
	m_pID_Btn_PKTeam	->SetProcOnButtonClick(theUITeamHeroGroupME_PKTeam_OnButtonClick);
	m_pID_Btn_PKGuild	->SetProcOnButtonClick(theUITeamHeroGroupME_PKGuild_OnButtonClick);

	m_pID_LISTIMG_PETSTATE	->SetProcOnButtonClick	( theUITeamHeroOnPetStateClick );
	m_pID_FRAME_TeamHero		->SetProcOnClick			( theUITeamHeroID_FRAME_TeamHeroOnClick );
	m_pID_Pic_Head				->SetProcOnButtonClick	( theUITeamHeroOnButtonClickHead );

	m_picHeaderMe->SetVisible(FALSE);
	m_pID_Btn_PKAll->SetVisible(FALSE);
	//m_pID_Btn_PKAll->SetEnable(FALSE);
	m_pID_Btn_PKTeam->SetVisible(FALSE);
	//m_pID_Btn_PKTeam->SetEnable(FALSE);
	m_pID_Btn_PKGuild->SetVisible(FALSE);
	//m_pID_Btn_PKGuild->SetEnable(FALSE);

	m_pID_Pic_Rage1->SetVisible(FALSE);
	m_pID_Pic_Rage2->SetVisible(FALSE);
	m_pID_Pic_Rage3->SetVisible(FALSE);
	m_pID_Pic_Rage4->SetVisible(FALSE);
	m_pID_Pic_Rage5->SetVisible(FALSE);


	SetPetInfoVisable(FALSE);



	SetVisible(TRUE);
	return TRUE;
}


void UITeamHero::SetPetInfoVisable( BOOL bShow )
{
	
	m_pID_PICTURE_pet			->SetVisible(bShow);
	m_pID_TEXT_petname		->SetVisible(bShow);
	m_pID_TEXT_petlevel		->SetVisible(bShow);
	m_pID_PROGRESS_petHp		->SetVisible(bShow);
	m_pID_PROGRESS_petMp		->SetVisible(bShow);
	m_pID_TEXT_pethp			->SetVisible(bShow);
	m_pID_TEXT_petmp			->SetVisible(bShow);
	m_pID_LISTIMG_PETSTATE	->SetVisible(bShow);
	
}

// 卸载UI
BOOL UITeamHero::UnLoadUI()
{
	GetUITeamHeroUITeamHeroUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "TeamHero") );
}

// 是否可视
BOOL UITeamHero::IsVisible()
{
	return m_pID_FRAME_TeamHero->IsVisible();
}


// 设置是否可视
void UITeamHero::SetVisible( BOOL bVisible )
{
	LPCSTR szPath;

	INT nProfession(PLAYERTYPE_WARRIOR);
	INT nSex(SEX_MALE);

	if(singleton::ExistVHero())
	{
		nProfession	= theVHero.GetProfession();
		nSex			= theVHero.GetSex();
	}

	szPath			= thePathInfoParser.GetPlayerHeader( nProfession,  nSex);


	m_pID_Pic_Head->SetPicInfo( szPath );

	m_pID_FRAME_TeamHero->SetVisible( bVisible );
}


void UITeamHero::Refresh()
{
}

void UITeamHero::Updata()
{
}


void UITeamHero::SetPicHeader( BOOL bHeader)		// 设置是否可视
{
	
	if( bHeader )
		m_picHeaderMe->SetVisible(TRUE);
	else
		m_picHeaderMe->SetVisible(FALSE);
	
}


};//namespace gameui

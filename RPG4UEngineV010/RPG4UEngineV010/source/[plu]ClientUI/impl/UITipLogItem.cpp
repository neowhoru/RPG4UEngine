/*////////////////////////////////////////////////////////////////////////
文 件 名：UITipLogItem.h
DOC 文件：uidata\UITipLogItem.uidoc
UI  名称：TipLogItem
创建日期：2008年8月1日
最后更新：2008年8月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UITipLogItem.h"
#include "VUILayoutManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;

namespace gameui
{ 


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UITipLogItem::UITipLogItem()
{
	m_pID_FRAME_TIPLOGITEM			= NULL;
	m_pID_TEXT_TIP  	= NULL;
	m_pID_BUTTON_TIP	= NULL;

	__ZERO(m_arID_TEXT_BACKs);
}

// Frame
BOOL UITipLogItem::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
	
}


BOOL UITipLogItem::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_TIP::OnButtonClick
BOOL UITipLogItem::ID_BUTTON_TIPOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}




// 设置是否可视
void UITipLogItem::SetVisible( BOOL bVisible )
{
#ifdef USE_UITIPLOGITEM_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_TIPLOGITEM->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		m_pID_FRAME_TIPLOGITEM->SetArrangeMode( ArrayMode_Normal );
	}
	else
	{
		Refresh();
	}
#else
	m_pID_FRAME_TIPLOGITEM->SetVisible( bVisible );
#endif
	
}
void UITipLogItem::SetLayoutVisible()
{
#ifdef USE_UITIPLOGITEM_LAYOUT
	theUILayoutManager.SetVisible(m_pID_FRAME_TIPLOGITEM);
#endif
}

void UITipLogItem::SetText(LPCSTR szText,DWORD dwTextColor, DWORD dwShadowColor)
{
	//assert( m_pID_TEXT_BACK );
	if(! m_pID_TEXT_TIP )
		return;

	//for(UINT n=0; n<BACK_ITEM_NUM;n++)
	//	m_arID_TEXT_BACKs[n]->SetText(szText,dwShadowColor);

	m_pID_TEXT_TIP->SetStrokeText(szText,dwTextColor,dwShadowColor );


}


void UITipLogItem::Refresh()
{
}


// 装载UI
BOOL UITipLogItem::LoadUI(INT nIndex)
{
	m_nIndex = nIndex;
	//GetUITipLogItemTipLogItemUIListener().RegisterMe();

	m_pID_FRAME_TIPLOGITEM = theUICtrlManager.LoadFrame ( UIDOC_PATH( "TipLogItem")
																			,TRUE
																			,FALSE
																			,FMSTR(UITIPLOGITEM_ID_FRAME_TIPLOGITEM, nIndex));
	if ( m_pID_FRAME_TIPLOGITEM == 0 )
	{
		UIMessageLog("读取文件["  "UITipLogItem" "]失败");
		return FALSE;
	}

#ifdef USE_UITIPLOGITEM_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_TIPLOGITEM);
#endif
	m_pID_FRAME_TIPLOGITEM->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UITipLogItem::InitControls()
{
	//INT n;

	//m_pID_FRAME_TIPLOGITEM->SetProcOnFrameMove			(theUITipLogItemOnFrameMove);
	//m_pID_FRAME_TIPLOGITEM->SetProcOnRender		(theUITipLogItemOnRender, FALSE);
	//m_pID_FRAME_TIPLOGITEM->SetProcOnMsgProc		( UITipLogItem_MsgProc );

	
	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_TEXT_TIP		= (VUCtrlText*)  	m_pID_FRAME_TIPLOGITEM->FindControl( UITIPLOGITEM_ID_TEXT_TIP );
	m_pID_BUTTON_TIP	= (VUCtrlButton*)	m_pID_FRAME_TIPLOGITEM->FindControl( UITIPLOGITEM_ID_BUTTON_TIP );
	
	for(UINT n=0; n<BACK_ITEM_NUM;n++)
	{
		m_arID_TEXT_BACKs[n]	= (VUCtrlText*) 	m_pID_FRAME_TIPLOGITEM->FindControl( FMSTR(UITIPLOGITEM_ID_TEXT_BACK,n+1) );
		assert( m_arID_TEXT_BACKs[n] );
		m_arID_TEXT_BACKs[n]->SetVisible(FALSE);
	}
	
	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_TEXT_TIP );
	assert( m_pID_BUTTON_TIP );
	
	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	//m_pID_BUTTON_TIP	->SetProcOnButtonClick	( theUITipLogItemID_BUTTON_TIPOnButtonClick );

	m_pID_FRAME_TIPLOGITEM->SetMsgHangUp(FALSE);
	m_pID_TEXT_TIP->SetMsgHangUp(FALSE);

	for(UINT n=0; n<BACK_ITEM_NUM;n++)
	{
		m_arID_TEXT_BACKs[n]->SetMsgHangUp(FALSE);
	}


	return TRUE;
}


UITipLogItem::~UITipLogItem()
{
	//UnLoadUI();
	
}

BOOL UITipLogItem::Init		()
{
	__BOOL_SUPER(Init());
	return TRUE;
}

void UITipLogItem::Release	()
{
	UnLoadUI();
	_SUPER::Release();
}


// 卸载UI
BOOL UITipLogItem::UnLoadUI()
{
	//GetUITipLogItemTipLogItemUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( m_pID_FRAME_TIPLOGITEM );
	//return theUICtrlManager.RemoveFrame( UIDOC_PATH( "TipLogItem") );
	
}

// 是否可视
BOOL UITipLogItem::IsVisible()
{
	if( m_pID_FRAME_TIPLOGITEM )
		return m_pID_FRAME_TIPLOGITEM->IsVisible();

	return FALSE;
}

};//namespace gameui


/*////////////////////////////////////////////////////////////////////////
文 件 名：UIItemMake.h
DOC 文件：uidata\UIItemMake.uidoc
UI  名称：ItemMake
创建日期：2008年6月14日
最后更新：2008年6月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "UIItemMake.h"
#include "VUCtrlManager.h"
#include "VUILayoutManager.h"
#include "UIInventory.h"
#include "BaseContainer.h"
#include "ItemManager.h"
#include "Player.h"
#include "ItemCompoundDialog.h"
#include "UIItemCompositeSlotUIListener.h"
#include "InputLayer.h"
#include "ItemMakeParser.h"
#include "Hero.h"
#include "HeroTipLayer.h"
#include "VUIIconMouseTip.h"
#include "VUCtrlIconDragManager.h"
#include "ItemSlot.h"
#include "TextResManager.h"
#include "MouseHandler.h"
#include "ItemSlotContainer.h"
#include "ItemCompositeParser.h"
#include "ConstTextRes.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIItemMake, ()  , gamemain::eInstPrioClientUI);

#define USE_ITEMMAKE_LAYOUT

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemMakeUIListener& GetUIItemMakeItemMakeUIListener()
{
	static ItemMakeUIListener staticItemMakeUIListener;
	return staticItemMakeUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIItemMake, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIItemMake, OnRender )
	UIPROC_BUTTON_CLICK  				( theUIItemMake, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_BUTTON_CLICK  				( theUIItemMake, ID_BUTTON_HELPOnButtonClick )
	UIPROC_LIST_SELECT_CHANGE  		( theUIItemMake, ID_LIST_DIRECTORYOnListSelectChange )
	UIPROC_SCROLL_BAR_UPDATE_POS  	( theUIItemMake, ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos )
	UIPROC_BUTTON_CLICK  				( theUIItemMake, ID_BUTTON_CANCELOnButtonClick )
	UIPROC_ICON_DROP_TO  				( theUIItemMake, ID_LISTIMG_RESULTOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemMake, ID_LISTIMG_RESULTOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemMake, ID_LISTIMG_RESULTOnIconRButtonUp )
	UIPROC_COMBO_BOX_CHANGE 			( theUIItemMake, ID_COMBOBOX_GROUPOnComboBoxChange )
	UIPROC_COMBO_BOX_CHANGE 			( theUIItemMake, ID_COMBOBOX_SUBGROUPOnComboBoxChange )
	UIPROC_ICON_DROP_TO  				( theUIItemMake, ID_LISTIMG_DIRECTORYOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemMake, ID_LISTIMG_DIRECTORYOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemMake, ID_LISTIMG_DIRECTORYOnIconRButtonUp )
	UIPROC_ICON_DROP_TO  				( theUIItemMake, ID_LISTIMG_MATERIALOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemMake, ID_LISTIMG_MATERIALOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemMake, ID_LISTIMG_MATERIALOnIconRButtonUp )
	//UIPROC_SCROLL_BAREX_UPDATE_POS	( theUIItemMake, ID_SCROLLBAREX_AMOUNTOnScrollBarExUpdatePos )
	UIPROC_BUTTON_CLICK  				( theUIItemMake, ID_BUTTON_MAKEOnButtonClick )
	UIPROC_BUTTON_CLICK  				( theUIItemMake, ID_BUTTON_CLEAROnButtonClick )
	//UIPROC_SCROLL_BAREX_UPDATE_POS	( theUIItemMake, ID_SCROLLBAREX_SUCCEEDRATEOnScrollBarExUpdatePos )
	UIPROC_CHECK_BOX_CHECK  			( theUIItemMake, ID_CHECKBOX_RATEOnCheckBoxCheck )
	UIPROC_CHECK_BOX_CHECK  			( theUIItemMake, ID_CHECKBOX_MATERIALOnCheckBoxCheck )

	UIPROC_EDIT_ENTER 					( theUIItemMake, ID_EDIT_NAMEOnEditEnter )
	UIPROC_ICON_DROP_TO  				( theUIItemMake, ID_LISTIMG_MAINOnIconDropTo )
	UIPROC_ICON_LDB_CLICK				( theUIItemMake, ID_LISTIMG_MAINOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP  			( theUIItemMake, ID_LISTIMG_MAINOnIconRButtonUp )
	//UIPROC_EDIT_ENTER 					( theUIItemMake, ID_EDIT_AMOUNTOnEditEnter )

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ItemMakeUIListener::ItemMakeUIListener()
{
}

ItemMakeUIListener::~ItemMakeUIListener()
{
}

LPCSTR ItemMakeUIListener::GetName()
{
	return "ItemMakeUIListener";
}

EGameUIType ItemMakeUIListener::GetType()
{
	return eUIItemMake;
}

void ItemMakeUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUIItemMake.SetVisible((BOOL)dwData);
		}break;

	case eSetMoney:
		{
			theUIItemMake.SetMoney(*(MONEY*)dwData);
		}break;
	}
}

BOOL ItemMakeUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void ItemMakeUIListener::Refresh(BOOL /*bExtra*/)
{
	theUIItemMake.Refresh();
}

void ItemMakeUIListener::TriggerFunc(ETriggerData eData,LPARAM lpData)
{
	switch(eData)
	{
	case eFillNeedMaterials:
		{
			theUIItemMake.FillNeedMaterials();
		}break;
	case eOnItemCompoundFinished:
		{
			theUIItemMake.OnItemCompoundFinished((MSG_OBJECT_BASE*)lpData);
		}break;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIItemMake::UIItemMake()
{
	//m_pID_FRAME_ITEMMAKE				= NULL;
	//m_pID_LIST_DIRECTORY				= NULL;
	//m_pID_LISTIMG_DIRECTORY			= NULL;
	//m_pID_SCROLLBAR_DIRECTORY 		= NULL;
	//m_pID_COMBOBOX_GROUP				= NULL;
	//m_pID_COMBOBOX_SUBGROUP			= NULL;
	////m_pID_SCROLLBAREX_AMOUNT  		= NULL;
	//m_pID_BUTTON_MAKE					= NULL;
	//m_pID_TEXT_SUBRATE  				= NULL;
	//m_pID_EDIT_AMOUNT					= NULL;
	m_pID_TEXT_RESULT  				= NULL;

	m_pID_LISTIMG_MAIN				= NULL;
	//m_pID_TEXT_RESULT					= NULL;

	__ZERO_ARRAY(m_arMaterialCheck);
	__ZERO_ARRAY(m_arMaterialContainer);
	__ZERO_ARRAY(m_arMaterialBack);

	m_nCurrentLevel					= 1;
	//m_pItemMakeGroup					= 0;
	//m_pCurrentSubGroup				= 0;
}

UIItemMake::~UIItemMake()
{
	//UnLoadUI();
	
}


// Frame
BOOL UIItemMake::OnFrameMove(DWORD dwTick)
{
	_SUPER::OnFrameMove(dwTick);
	return TRUE;
	
}


BOOL UIItemMake::OnRender(DWORD dwTick)
{
	_SUPER::OnRender(dwTick);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UIItemMake::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* pSender )
{
	_SUPER::ID_BUTTON_CLOSEOnButtonClick( pSender);

	//ItemMakeDialog* pDialog = (ItemMakeDialog*)theUIItemCompositeSlotUIListener.GetContainer();
	//pDialog->CloseWindow();
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_HELP::OnButtonClick
BOOL UIItemMake::ID_BUTTON_HELPOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LIST_DIRECTORY::OnListSelectChange
void UIItemMake::ID_LIST_DIRECTORYOnListSelectChange(VUCtrlObject* pSender, sUILIST_ITEM*	/*pItem*/)
{
	//_SUPER::ID_LIST_DIRECTORYOnListSelectChange( pSender, pItem);
	if(pSender == m_pID_LIST_DIRECTORY)
	{
		sUILIST_ITEM *pList = m_pID_LIST_DIRECTORY->GetCurSelItem();
		m_pItemMakeLevel	= (sITEMMAKELEVEL*)pList->m_pData;
		RefreshComposite();
	}
}

////////////////////////////////////////////////////
//ID_SCROLLBAR_DIRECTORY::OnScrollBarUpdatePos
void UIItemMake::ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos(VUCtrlObject* pSender, int nValue)
{
	_SUPER::ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos( pSender,nValue);

	//if(pSender == m_pID_SCROLLBAR_DIRECTORY)
	//{
	//	INT nMaxShowCount;
	//	int nCnt;

	//	nMaxShowCount	= m_pID_LIST_DIRECTORY->GetHeight() / m_pID_LIST_DIRECTORY->GetListItemHeight();
	//	nCnt				= m_pID_LIST_DIRECTORY->GetListItemCnt();

	//	if( nCnt > nMaxShowCount )
	//	{
	//		m_pID_SCROLLBAR_DIRECTORY->SetMaxValue(nCnt - nMaxShowCount);
	//		m_pID_SCROLLBAR_DIRECTORY->SetStepValue(1);
	//	}
	//	if( m_pID_LIST_DIRECTORY->GetListItemCnt() > nMaxShowCount )
	//	{
	//		m_pID_LIST_DIRECTORY->SetStartIndex( nValue );
	//		m_pID_LISTIMG_DIRECTORY->SetStartIndex( nValue );
	//	}
	//}
}

////////////////////////////////////////////////////
//ID_BUTTON_CANCEL::OnButtonClick
BOOL UIItemMake::ID_BUTTON_CANCELOnButtonClick(VUCtrlObject* pSender )
{
	ID_BUTTON_CLOSEOnButtonClick(pSender);
	return TRUE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconDropTo
BOOL UIItemMake::ID_LISTIMG_RESULTOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconLDBClick
BOOL UIItemMake::ID_LISTIMG_RESULTOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_RESULT::OnIconRButtonUp
BOOL UIItemMake::ID_LISTIMG_RESULTOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_COMBOBOX_GROUP::OnComboBoxChange
void UIItemMake::ID_COMBOBOX_GROUPOnComboBoxChange(VUCtrlObject* pSender, LPCSTR 				/*szData*/)
{
	//_SUPER::ID_COMBOBOX_GROUPOnComboBoxChange( pSender,szData);

	if(pSender == m_pID_COMBOBOX_GROUP)
	{
		sUILIST_ITEM *pList = m_pID_COMBOBOX_GROUP->GetListBox().GetCurSelItem();
		m_pItemMakeGroup	= (sITEMMAKE_GROUP*)pList->m_pData;
		RefreshSubGroup();
		//m_pID_COMBOBOX_GROUP
	}
}

////////////////////////////////////////////////////
//ID_COMBOBOX_SUBGROUP::OnComboBoxChange
void UIItemMake::ID_COMBOBOX_SUBGROUPOnComboBoxChange(VUCtrlObject* pSender, LPCSTR  /*szData*/)
{
	//_SUPER::ID_COMBOBOX_SUBGROUPOnComboBoxChange( pSender, szData);
	if(pSender == m_pID_COMBOBOX_SUBGROUP)
	{
		sUILIST_ITEM *pList = m_pID_COMBOBOX_SUBGROUP->GetListBox().GetCurSelItem();
		m_nCurrentLevel	= pList->m_nID;
		RefreshDirectory();
		//m_pID_COMBOBOX_GROUP
	}
}

////////////////////////////////////////////////////
//ID_LISTIMG_DIRECTORY::OnIconDropTo
BOOL UIItemMake::ID_LISTIMG_DIRECTORYOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_DIRECTORY::OnIconLDBClick
BOOL UIItemMake::ID_LISTIMG_DIRECTORYOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_DIRECTORY::OnIconRButtonUp
BOOL UIItemMake::ID_LISTIMG_DIRECTORYOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconDropTo
BOOL UIItemMake::ID_LISTIMG_MATERIALOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconLDBClick
BOOL UIItemMake::ID_LISTIMG_MATERIALOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MATERIAL::OnIconRButtonUp
BOOL UIItemMake::ID_LISTIMG_MATERIALOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

//////////////////////////////////////////////////////
////ID_SCROLLBAREX_AMOUNT::OnScrollBarExUpdatePos
//void UIItemMake::ID_SCROLLBAREX_AMOUNTOnScrollBarExUpdatePos(VUCtrlObject* pSender, int							nValue)
//{
//	if(pSender == m_pID_SCROLLBAREX_AMOUNT)
//	{
//		*m_pID_EDIT_AMOUNT = FMSTR("%d", nValue/10);
//	}
//}

////////////////////////////////////////////////////
//ID_BUTTON_MAKE::OnButtonClick
BOOL UIItemMake::ID_BUTTON_MAKEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	if(m_pCurrentComposite == NULL)
	{
		OUTPUTTOP(TEXTRES_SELECT_ITEM_FOR_COMPOUND);
		return FALSE;
	}

	if(m_nActionMode == COMPOSITE_WORKING)
	{
		if(!theItemCompoundDialog.CheckValidMaterails(eCOMPOSITE_CHECK_MATERIAL))
		{
			OUTPUTTOP(TEXTRES_RC_ITEM_INVALID_CONDITION);
			return FALSE;
		}

		if(!theItemCompoundDialog.CheckValidMaterails(eCOMPOSITE_CHECK_MONEY))
		{
			OUTPUTTOP(TEXTRES_NOT_ENOUGH_MONEY);
			return FALSE;
		}

		m_nActionMode = COMPOSITE_WAITING;
		theItemCompoundDialog.SetItemName(m_pID_EDIT_NAME->GetText());
		if(!theItemCompoundDialog.Enchant())
		{
			m_nActionMode = COMPOSITE_WORKING;
			OUTPUTTOP(TEXTRES_NOT_ENCHANT_ITEM);
			return FALSE;
		}

		return TRUE;
	}

	if(m_nActionMode == COMPOSITE_NEXT)
	{
		m_nActionMode = COMPOSITE_WORKING;
		theIconMouseTip.SetLockKey(TIP_LOCKEKY,0);
		theIconMouseTip.SetVisible(FALSE);

		RefreshComposite();
		//m_pID_LIST_DIRECTORY->SetEnable(TRUE);
		//m_pID_COMBOBOX_GROUP->SetEnable(TRUE);
		//m_pID_COMBOBOX_SUBGROUP->SetEnable(TRUE);
		//m_pID_BUTTON_MAKE->SetEnable(TRUE);
	}
	
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_CLEAR::OnButtonClick
BOOL UIItemMake::ID_BUTTON_CLEAROnButtonClick(VUCtrlObject* /*pSender*/ )
{
	if(theItemCompoundDialog.GetTargetContainer())
		theItemCompoundDialog.GetTargetContainer()->ClearAll();

	theIconMouseTip.SetLockKey(TIP_LOCKEKY,0);
	theIconMouseTip.SetVisible(FALSE);
	theIconDragManager.ClearDrag();
	theMouseHandler.CancelHandlingItem();

	RefreshComposite();
	return FALSE;
}


////////////////////////////////////////////////////
//ID_CHECKBOX_RATE::OnCheckBoxCheck
void UIItemMake::ID_CHECKBOX_RATEOnCheckBoxCheck(VUCtrlObject* pSender, BOOL*						pbChecked)
{
	_SUPER::ID_CHECKBOX_RATEOnCheckBoxCheck( pSender, pbChecked);
	//if(pSender->GetGroupID() == SUCCEEDRATE_GROUP)
	//{
	//	ChangeSucceedRate(pSender->GetIndexAtGroup());
	////eSUCCEEDRATE_NUM
	//}
}

////////////////////////////////////////////////////
//ID_CHECKBOX_MATERIAL::OnCheckBoxCheck
void UIItemMake::ID_CHECKBOX_MATERIALOnCheckBoxCheck(VUCtrlObject* pSender, BOOL*						/*pbChecked*/)
{
	if(pSender->GetGroupID() == GROUP_MATERIAL_CHECK)
	{
		//ChangeSucceedRate(pSender->GetIndexAtGroup());
	//eSUCCEEDRATE_NUM
	}
}


////////////////////////////////////////////////////
//ID_EDIT_NAME::OnEditEnter
void UIItemMake::ID_EDIT_NAMEOnEditEnter(VUCtrlObject* /*pSender*/, LPCSTR 				/*szData*/ )
{
	
}

////////////////////////////////////////////////////
//ID_LISTIMG_MAIN::OnIconDropTo
BOOL UIItemMake::ID_LISTIMG_MAINOnIconDropTo(VUCtrlObject* /*pSender*/, VUCtrlObject*			/*pCtrlThis*/, IconDragListImg*	/*pItemDrag*/, IconDragListImg*	/*pItemDest*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MAIN::OnIconLDBClick
BOOL UIItemMake::ID_LISTIMG_MAINOnIconLDBClick(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/)
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_LISTIMG_MAIN::OnIconRButtonUp
BOOL UIItemMake::ID_LISTIMG_MAINOnIconRButtonUp(VUCtrlObject* /*pSender*/, IconDragListImg*		/*pItem*/ )
{
	return FALSE;
}

////////////////////////////////////////////////////
//ID_EDIT_AMOUNT::OnEditEnter
//void UIItemMake::ID_EDIT_AMOUNTOnEditEnter(VUCtrlObject* /*pSender*/, LPCSTR 				/*szData*/ )
//{
//	if(pSender == m_pID_EDIT_AMOUNT)
//	{
//		m_pID_SCROLLBAREX_AMOUNT->SetValue(atoi(szData) * 10);
//	}
//}



// 设置是否可视
void UIItemMake::SetVisible( const BOOL bVisible )
{
	_SUPER::SetVisible(bVisible);
}


void UIItemMake::SetLayoutVisible()
{
	_SUPER::SetLayoutVisible();
}

void UIItemMake::CloseWindow()
{
	theItemCompoundDialog.CloseWindow();
}

void UIItemMake::Refresh()
{
	RefreshGroup();
}

void UIItemMake::RefreshGroup()
{
	sITEMMAKE_GROUP*	pGroup;

	VUCtrlList& groupList		= m_pID_COMBOBOX_GROUP->GetListBox();
	VUCtrlEdit& groupEdit		= m_pID_COMBOBOX_GROUP->GetEditInput();
	groupList.Clear();
	groupEdit.SetText("");


	ItemMakeGroupTable* pGroupTable;
	pGroupTable = theItemMakeParser.GetMakeGroupTable();
	__VERIFY2_PTR(pGroupTable, "无效的GroupTable\n",;);

	pGroupTable->SetFirst();
	while( pGroup = pGroupTable->GetNext() )
	{	
		sUILIST_ITEM	item;
		item.SetData((LPCSTR)pGroup->sName,0,pGroup);
		groupList.AddItem(&item);
	}

	m_pItemMakeGroup = pGroupTable->GetHeadData();
	if(m_pItemMakeGroup)
		groupEdit.SetText((LPCSTR)m_pItemMakeGroup->sName);

	RefreshSubGroup();
}


void UIItemMake::RefreshSubGroup()
{
	//sITEMCOMPOSITE_SUBGROUP*	pSubGroup;

	LPCSTR szLabel;

	VUCtrlList& subGroupList	= m_pID_COMBOBOX_SUBGROUP->GetListBox();
	VUCtrlEdit& subGroupEdit	= m_pID_COMBOBOX_SUBGROUP->GetEditInput();
	subGroupList.Clear();
	subGroupEdit.SetText("");

	if(m_pItemMakeGroup == NULL)
		return;

	szLabel = _STRING(TEXTRES_ITEMMAKE_LEVEL);
	//m_pItemMakeGroup->subGroupTable.SetFirst();
	//while( pSubGroup = m_pItemMakeGroup->subGroupTable.GetNext() )
	for(INT n=0; n<MAX_ITEM_LEVEL; n++)
	{	
		sUILIST_ITEM	item;
		INT						nLevel = n+1;
		LPCSTR					szText;

		if(GetMakeNumByLV(nLevel) == 0)
			continue;

		szText = FMSTR(szLabel,nLevel);

		item.SetData(szText, nLevel);
		subGroupList.AddItem(&item);

		if(m_nCurrentLevel == nLevel)
			subGroupEdit.SetText(szText);
	}

	//m_pCurrentSubGroup = m_pItemMakeGroup->subGroupTable.GetHeadData();
	//if(m_pCurrentSubGroup)
		//subGroupEdit.SetText((LPCSTR)m_pCurrentSubGroup->sName);

	RefreshDirectory();
}


INT  UIItemMake::GetMakeNumByLV(INT nLevel)
{
	sITEMMAKE*			pMakeInfo;
	sITEMMAKELEVEL*	pMakeLV;
	//CODETYPE				itemID;
	sVITEMINFO_BASE*	pVItemInfo;		
	INT					nCount(0);

	__CHECK2(nLevel >= 1 && nLevel <= MAX_ITEM_LEVEL, 0);


	m_pItemMakeGroup->itemMakeTable.SetFirst();
	while(pMakeInfo = m_pItemMakeGroup->itemMakeTable.GetNext())
	{
		pMakeLV = &pMakeInfo->m_arMakeLVs[nLevel-1];
		if(pMakeLV->m_ItemCode	== 0)
			continue;

		pVItemInfo = theItemInfoParser.GetVItemInfoByItem(pMakeLV->m_ItemCode);
		if(pVItemInfo	== NULL)
			continue;

		nCount++;
	}

	return nCount;
}

void UIItemMake::RefreshDirectory()
{
	sITEMMAKE*			pMakeInfo;
	sITEMMAKELEVEL*	pMakeLV;
	//ItemMakeGroupTable*	pGroupTable;
	//CODETYPE				itemID;
	sVITEMINFO_BASE*	pVItemInfo;		
	sITEMINFO_BASE*	pItemInfo;		
	LPCSTR				szLabel;

	m_pID_LISTIMG_DIRECTORY->Clear();
	m_pID_LIST_DIRECTORY->Clear();

	m_pItemMakeLevel = NULL;
	if(m_nCurrentLevel >= 1 && m_nCurrentLevel <= MAX_ITEM_LEVEL)
	{
		szLabel	= _STRING(TEXTRES_ITEMMAKE_NAME);

		m_pItemMakeGroup->itemMakeTable.SetFirst();
		while(pMakeInfo = m_pItemMakeGroup->itemMakeTable.GetNext())
		{
			pMakeLV = &pMakeInfo->m_arMakeLVs[m_nCurrentLevel-1];
			if(pMakeLV->m_ItemCode	== 0)
				continue;
			if(m_pItemMakeLevel == NULL)
				m_pItemMakeLevel = pMakeLV;

			pItemInfo = theItemInfoParser.GetItemInfo(pMakeLV->m_ItemCode);
			if(pItemInfo	== NULL)
				continue;

			pVItemInfo = theItemInfoParser.GetVItemInfoByItem(pMakeLV->m_ItemCode);
			if(pVItemInfo	== NULL)
				continue;

			IconDragListImg	stListItem;
			stListItem.SetData	(pMakeLV->m_ItemCode
										,1
										,pVItemInfo->GetIconType());

			m_pID_LISTIMG_DIRECTORY->InsertItem( &stListItem );

			sUILIST_ITEM	item;
			item.SetData( FMSTR(szLabel,(LPCSTR)pItemInfo->m_ItemName)
							, 0
							, pMakeLV);
			m_pID_LIST_DIRECTORY->AddItem(&item);
		}

		if(m_pItemMakeLevel)
		{
			m_pID_LISTIMG_DIRECTORY->SetCurSelItem(0);

			RefreshComposite();
		}
	}

	m_pID_SCROLLBAR_DIRECTORY->SetValue(0);
	ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos(m_pID_SCROLLBAR_DIRECTORY, 0);
}


void UIItemMake::RefreshComposite()
{
	//MONEY moneyCost;

	m_pID_LISTIMG_RESULT		->Clear();
	m_pID_TEXT_MONEY			->Clear();
	*m_pID_EDIT_NAME			= "";
	m_itemModel.ReloadMesh(INVALID_DWORD_ID);

	for(INT n=0; n<MATERIAL_CONTAINER_COL; n++)
		m_arMaterialContainer[n]->Clear();

	if(m_pItemMakeGroup == NULL || m_pItemMakeLevel == NULL)
		return;

	RefreshMaterialContainer();

	//UIItemComposite::RefreshComposite();
	assert(m_pCurrentComposite);
	///填充原料清单
	ChangeSucceedRate(m_nCurrentSucceedRate, TRUE);

	//moneyCost				= theItemCompoundDialog.GetRequiredMoney();
	//*m_pID_TEXT_MONEY		= FMSTR("%I64u",moneyCost);


	SetMainResult(m_pItemMakeLevel->m_ItemCode);

}



void UIItemMake::RefreshMaterialContainer()
{
	__CHECK2_PTR(m_pItemMakeLevel,;);

	INT*		pIndexs;
	INT		nMaterialBase = MATERIALTYPE_NONE + 1;
	BYTE*		pMaterials	=	m_pItemMakeLevel->m_arMaterialNum;

	pIndexs = theItemCompoundDialog.RefreshMakeMaterialContainer(m_pItemMakeLevel
                                                               ,MATERIAL_CONTAINER_COL
																					,MATERIAL_CONTAINER_ROW);

	__CHECK2_PTR(pIndexs,;);
	//////////////////////////////////////////////////
	//依据arIndex刷新UI
	//	1.刷新 CheckBox

	for(INT n=nMaterialBase; n<MATERIALTYPE_MAX;n++)
	{
		INT nIndex = n;// - (nMaterialBase);
		m_arMaterialCheck[nIndex]->SetVisible(FALSE);
	}

	for(INT n=0; n<MATERIAL_CONTAINER_COL; n++)
	{
		INT nIndex = pIndexs[n];


		if(nIndex > 0)
		{
			m_arMaterialCheck[nIndex]->SetVisible(TRUE);
			m_arMaterialCheck[nIndex]->SetPos(m_arHeaderPos[n].x,m_arHeaderPos[n].y, FALSE);
			//更新材料槽数量
			INT nMarginX,nMarginY;
			INT nHeight;

			m_arMaterialContainer[n]->GetIconMargin(nMarginX,nMarginY);
			nHeight = pMaterials[nIndex] * (m_arMaterialContainer[n]->GetIconSize() + nMarginY);
			m_arMaterialContainer[n]->SetHeight	( nHeight);
			m_arMaterialBack		[n]->SetHeight	( nHeight);
			m_arMaterialContainer[n]->SetVisible( TRUE);
			m_arMaterialBack		[n]->SetVisible( TRUE);


			///主材高亮处理
			IconDragListImg* pImg;
			pImg = m_arMaterialContainer[n]->GetItemAt(0);
			pImg->SetFocus(m_pItemMakeLevel->m_byMainIndex == nIndex, TRUE);
		}
		else
		{
			m_arMaterialCheck[n + nMaterialBase]->SetVisible(TRUE);
			m_arMaterialCheck[n + nMaterialBase]->SetPos(m_arHeaderPos[n].x,m_arHeaderPos[n].y, FALSE);
			m_arMaterialContainer[n]->SetVisible( FALSE);
			m_arMaterialBack		[n]->SetVisible( FALSE);
		}
	}


}

void UIItemMake::FillNeedMaterials()
{
	//_SUPER::FillNeedMaterials();
	assert(0);//填充原料槽
}

void UIItemMake::OnItemCompoundFinished(MSG_OBJECT_BASE * pMsgBase)
{
	m_nActionMode	= COMPOSITE_NEXT;

	switch (pMsgBase->m_byProtocol)
	{
	case CG_ITEM_MAKE_ACK:
		{
			MSG_CG_ITEM_MAKE_ACK *	pRecvPacket = (MSG_CG_ITEM_MAKE_ACK *)pMsgBase;
			///拖动物品到鼠标上
			ItemSlotContainer*	pContainer;
			SLOTPOS					atPos;
			//IconDragListImg*		pUnitImg;

			atPos			= pRecvPacket->m_ItemInfo.m_Slot[0].m_ItemPos;
			pContainer	= (ItemSlotContainer*)theItemManager.GetContainer(SI_INVENTORY);
			if(!pContainer->IsEmpty(atPos))
			{
				 theUIInventory.ShowToolTip(atPos,TIP_LOCKEKY);
				theUIInventory.ForceDragFrom(atPos);
			}

		}break;
	case CG_ITEM_MAKE_NAK:
		{
		}break;
	}

	m_pID_EDIT_NAME			->SetReadOnly(TRUE);

}


void UIItemMake::ChangeSucceedRate(UINT nRate,BOOL bForceUpdate)
{
	_SUPER::ChangeSucceedRate(nRate, bForceUpdate);
}


void UIItemMake::ChangeTargetItem(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
	//m_pCurrentComposite	= theItemCompositeParser.GetCompositeInfo(ITEMCOMPOSITE_ITEM_MAKE);

	assert(m_pCurrentComposite);
	RefreshComposite();
	//RefreshSockets();
}

void UIItemMake::ChangeSubResultItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
}

void UIItemMake::ChangeMaterialItem	(BaseSlot* /*pSlot*/, BOOL /*bAdd*/)
{
	m_pID_EDIT_NAME			->SetReadOnly(FALSE);
}


// 装载UI
BOOL UIItemMake::LoadUI()
{
	GetUIItemMakeItemMakeUIListener().RegisterMe();

	m_pID_FRAME_ITEMMAKE = theUICtrlManager.LoadFrame( UIDOC_PATH( "ItemMake") );
	if ( m_pID_FRAME_ITEMMAKE == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "ItemMake") "]失败");
		return FALSE;
	}
	m_pID_FRAME_ITEMCOMPOUND = m_pID_FRAME_ITEMMAKE;

#ifdef USE_ITEMMAKE_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_ITEMMAKE);
#endif
	m_pID_FRAME_ITEMMAKE->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIItemMake::InitControls()
{
	//INT n;

	m_pID_FRAME_ITEMMAKE->SetProcOnFrameMove			(theUIItemMakeOnFrameMove);
	m_pID_FRAME_ITEMMAKE->SetProcOnRender		(theUIItemMakeOnRender, FALSE);

	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_LIST_DIRECTORY				= (VUCtrlList*) 			m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_LIST_DIRECTORY );
	m_pID_SCROLLBAR_DIRECTORY	 	= (VUCtrlScrollBar*)  	m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_SCROLLBAR_DIRECTORY );
	m_pID_COMBOBOX_GROUP				= (VUCtrlComboBox*)		m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_COMBOBOX_GROUP );
	m_pID_COMBOBOX_SUBGROUP			= (VUCtrlComboBox*)		m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_COMBOBOX_SUBGROUP );
	m_pID_LISTIMG_DIRECTORY			= (VUCtrlListImg*) 		m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_LISTIMG_DIRECTORY );
	//m_pID_SCROLLBAREX_AMOUNT	  	= (VUCtrlScrollBarEx*)	m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_SCROLLBAREX_AMOUNT );
	m_pID_BUTTON_MAKE					= (VUCtrlButton*)  		m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_BUTTON_MAKE );
	//m_pID_TEXT_SUBRATE	  			= (VUCtrlText*) 			m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_TEXT_SUBRATE );
	//m_pID_EDIT_AMOUNT					= (VUCtrlEdit*) 			m_pID_FRAME_ITEMMAKE->FindControl( UIITEMCOMPOUND_ID_EDIT_AMOUNT );
	m_pID_LISTIMG_MAIN				= (VUCtrlListImg*) 		m_pID_FRAME_ITEMMAKE->FindControl( UIITEMMAKE_ID_LISTIMG_MAIN );
	m_pID_TEXT_RESULT		  			= (VUCtrlText*) 			m_pID_FRAME_ITEMMAKE->FindControl( UIITEMMAKE_ID_TEXT_RESULT );
	

	for(INT n=0; n<MATERIAL_CONTAINER_COL;n++)
	{
		m_arMaterialContainer[n]	= (VUCtrlListImg*) 		m_pID_FRAME_ITEMMAKE->FindControl( FMSTR(UIITEMMAKE_ID_LISTIMG_MATERIAL,n) );
		assert(m_arMaterialContainer[n]);
		m_arMaterialContainer[n]	->SetProcOnDropTo			( theUIItemMakeID_LISTIMG_MATERIALOnIconDropTo );
		m_arMaterialContainer[n]	->SetProcOnLButtonDBClick 		( theUIItemMakeID_LISTIMG_MATERIALOnIconLDBClick );
		m_arMaterialContainer[n]	->SetProcOnRButtonUp		( theUIItemMakeID_LISTIMG_MATERIALOnIconRButtonUp );
		m_arMaterialContainer[n]	->SetGroupID(GROUP_MATERIAL_LIST);
		m_arMaterialContainer[n]	->SetIndexAtGroup(n);
		m_arMaterialContainer[n]	->SetCanPicking(TRUE);

		m_arMaterialBack[n]			= (VUCtrlPicture*) 		m_pID_FRAME_ITEMMAKE->FindControl( FMSTR(UIITEMMAKE_ID_PICTURE_MATERIAL,n) );
		assert(m_arMaterialBack[n]);
		m_arMaterialBack[n]			->SetGroupID(GROUP_MATERIAL_BACK);
		m_arMaterialBack[n]			->SetIndexAtGroup(n);
		//m_arMaterialContainer[n]	->SetHva
	}

	for(INT n=MATERIALTYPE_NONE + 1; n<MATERIALTYPE_MAX;n++)
	//for(INT n=0; n<MATERIALTYPE_MAX;n++)
	{
		INT nIndex	= n;// - (MATERIALTYPE_NONE + 1);
		INT nCol		= n-(MATERIALTYPE_NONE + 1);

		m_arMaterialCheck[nIndex]			= (VUCtrlCheckBox*) 		m_pID_FRAME_ITEMMAKE->FindControl( FMSTR(UIITEMMAKE_ID_CHECKBOX_MATERIAL,n) );
		assert(m_arMaterialCheck[nIndex]);
		m_arMaterialCheck[nIndex]->SetProcOnCheck(theUIItemMakeID_CHECKBOX_MATERIALOnCheckBoxCheck);
		m_arMaterialCheck[nIndex]->SetGroupID(GROUP_MATERIAL_CHECK);
		m_arMaterialCheck[nIndex]->SetIndexAtGroup(n);
		m_arMaterialCheck[nIndex]->SetVisible( nCol <  MATERIAL_CONTAINER_COL);

		if(nCol < MATERIAL_CONTAINER_COL)
		{
			INT x,y;
			m_arMaterialCheck[nIndex]->GetPos(x,y, FALSE);

			m_arHeaderPos[nCol].x = x;
			m_arHeaderPos[nCol].y = y;
		}
	}

	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_LIST_DIRECTORY );
	assert( m_pID_SCROLLBAR_DIRECTORY );
	assert( m_pID_COMBOBOX_GROUP );
	assert( m_pID_COMBOBOX_SUBGROUP );
	assert( m_pID_LISTIMG_DIRECTORY );
	//assert( m_pID_SCROLLBAREX_AMOUNT );
	assert( m_pID_BUTTON_MAKE );
	
	//assert( m_pID_TEXT_SUBRATE );
	assert( m_pID_LISTIMG_MAIN );
	//assert( m_pID_EDIT_AMOUNT );

	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_LIST_DIRECTORY				->SetProcOnSelectChange	( theUIItemMakeID_LIST_DIRECTORYOnListSelectChange );
	m_pID_SCROLLBAR_DIRECTORY 		->SetProcOnUpdatePos		( theUIItemMakeID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos );
	m_pID_COMBOBOX_GROUP				->SetProcOnChange			( theUIItemMakeID_COMBOBOX_GROUPOnComboBoxChange );
	m_pID_COMBOBOX_SUBGROUP			->SetProcOnChange			( theUIItemMakeID_COMBOBOX_SUBGROUPOnComboBoxChange );
	m_pID_LISTIMG_DIRECTORY			->SetProcOnDropTo			( theUIItemMakeID_LISTIMG_DIRECTORYOnIconDropTo );
	m_pID_LISTIMG_DIRECTORY			->SetProcOnLButtonDBClick 		( theUIItemMakeID_LISTIMG_DIRECTORYOnIconLDBClick );
	m_pID_LISTIMG_DIRECTORY			->SetProcOnRButtonUp		( theUIItemMakeID_LISTIMG_DIRECTORYOnIconRButtonUp );
	//m_pID_SCROLLBAREX_AMOUNT  	->SetProcOnUpdatePos		( theUIItemMakeID_SCROLLBAREX_AMOUNTOnScrollBarExUpdatePos );
	m_pID_BUTTON_MAKE					->SetProcOnButtonClick 	( theUIItemMakeID_BUTTON_MAKEOnButtonClick );
	//m_pID_EDIT_AMOUNT				->SetProcOnEnter 			( theUIItemMakeID_EDIT_AMOUNTOnEditEnter );


	/////////////////////////////////////////////
	UIItemComposite::InitControls(m_pID_FRAME_ITEMMAKE);
	//for(INT n=0; n<eSUCCEEDRATE_NUM; n++)
	//{
	//	m_arSucceedRate[n]		->SetProcOnCheck 	( theUIItemMakeID_CHECKBOX_RATEOnCheckBoxCheck );
	//}

	m_pID_BUTTON_CLOSE  				->SetProcOnButtonClick 	( theUIItemMakeID_BUTTON_CLOSEOnButtonClick );
	m_pID_BUTTON_HELP					->SetProcOnButtonClick 	( theUIItemMakeID_BUTTON_HELPOnButtonClick );
	m_pID_BUTTON_CANCEL 				->SetProcOnButtonClick 	( theUIItemMakeID_BUTTON_CANCELOnButtonClick );
	m_pID_LISTIMG_RESULT				->SetProcOnDropTo			( theUIItemMakeID_LISTIMG_RESULTOnIconDropTo );
	m_pID_LISTIMG_RESULT				->SetProcOnLButtonDBClick 		( theUIItemMakeID_LISTIMG_RESULTOnIconLDBClick );
	m_pID_LISTIMG_RESULT				->SetProcOnRButtonUp		( theUIItemMakeID_LISTIMG_RESULTOnIconRButtonUp );
	//m_pID_LISTIMG_MATERIAL 			->SetProcOnDropTo			( theUIItemMakeID_LISTIMG_MATERIALOnIconDropTo );
	//m_pID_LISTIMG_MATERIAL 			->SetProcOnLButtonDBClick 		( theUIItemMakeID_LISTIMG_MATERIALOnIconLDBClick );
	//m_pID_LISTIMG_MATERIAL 			->SetProcOnRButtonUp		( theUIItemMakeID_LISTIMG_MATERIALOnIconRButtonUp );
	m_pID_BUTTON_CLEAR  				->SetProcOnButtonClick 	( theUIItemMakeID_BUTTON_CLEAROnButtonClick );
	m_pID_EDIT_NAME  					->SetProcOnEnter 			( theUIItemMakeID_EDIT_NAMEOnEditEnter );
	m_pID_LISTIMG_MAIN				->SetProcOnDropTo			( theUIItemMakeID_LISTIMG_MAINOnIconDropTo );
	m_pID_LISTIMG_MAIN				->SetProcOnLButtonDBClick 		( theUIItemMakeID_LISTIMG_MAINOnIconLDBClick );
	m_pID_LISTIMG_MAIN				->SetProcOnRButtonUp		( theUIItemMakeID_LISTIMG_MAINOnIconRButtonUp );


	/////////////////////////////////////////////

	m_pID_EDIT_NAME			->SetReadOnly(FALSE);
	m_pID_COMBOBOX_GROUP		->GetListBox().SetSelectionInfo(TRUE);
	m_pID_COMBOBOX_SUBGROUP	->GetListBox().SetSelectionInfo(TRUE);
	m_pID_LIST_DIRECTORY		->SetSelectionInfo(TRUE);

	m_pID_LISTIMG_DIRECTORY	->SetCanPicking(FALSE);
	m_pID_LISTIMG_RESULT		->SetCanPicking(FALSE);

	m_pID_COMBOBOX_GROUP		->GetListBox().SetMargin(8,8,8,8);
	m_pID_COMBOBOX_SUBGROUP	->GetListBox().SetMargin(8,8,8,8);


	/////////////////////////////////////////////
	m_pID_SCROLLBAR_DIRECTORY->SetValue(0);
	ID_SCROLLBAR_DIRECTORYOnScrollBarUpdatePos(m_pID_SCROLLBAR_DIRECTORY, 0);


	m_pCurrentComposite	= theItemCompositeParser.GetCompositeInfo(ITEMCOMPOSITE_ITEM_MAKE);


	//for(INT n=MATERIALTYPE_NONE + 1; n<MATERIALTYPE_MAX;n++)
	////for(INT n=0; n<MATERIALTYPE_MAX;n++)
	//{
	//	INT nIndex	= n;// - (MATERIALTYPE_NONE + 1);
	//	INT nCol		= n-(MATERIALTYPE_NONE + 1);

	//	if(nCol < MATERIAL_CONTAINER_COL)
	//	{
	//		INT x,y;
	//		m_arMaterialCheck[nIndex]->GetPos(x,y);

	//		m_arHeaderPos[nCol].x = x;
	//		m_arHeaderPos[nCol].y = y;
	//	}
	//}

	return TRUE;
}



// 卸载UI
BOOL UIItemMake::UnLoadUI()
{
	m_itemModel.Release();
	GetUIItemMakeItemMakeUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ItemMake") );
	
}

// 是否可视
BOOL UIItemMake::IsVisible()
{
	if( m_pID_FRAME_ITEMMAKE )
		return m_pID_FRAME_ITEMMAKE->IsVisible();

	return FALSE;
}


void UIItemMake::SetMoney(MONEY money)
{
	_SUPER::SetMoney(money);
}


void UIItemMake::OnInstallContainer	(BaseContainer* pContainer)
{
	_SUPER::OnInstallContainer	( pContainer);
	switch(pContainer->GetSlotIndex())
	{
	case SI_ENCHANT:
		{
			for(INT n=0; n<MATERIAL_CONTAINER_COL; n++)
			{
				m_arMaterialContainer[n]->SetSlotStartIndex(n * MATERIAL_CONTAINER_ROW);
				m_arMaterialContainer[n]->SetSlotContainer(pContainer);
			}
		}break;
	}

}

void UIItemMake::OnItemSlot(BaseSlot& slot, BOOL bAdd)
{
	///////////////////////////////////
	if(slot.GetSlotIndex() != SI_ENCHANT)
	{
		_SUPER::OnItemSlot( slot, bAdd);
		return;
	}

	///////////////////////////////////
	ChangeMaterialItem(&slot, bAdd);
}

void UIItemMake::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	if(pSlot->GetSlotIndex() != SI_ENCHANT)
	{
		_SUPER::PushGoods(pSlot);
		return;
	}

	////////////////////////////////////
	UIContainHelper	helper(m_arMaterialContainer
									,m_pPlayer
									,MATERIAL_CONTAINER_ROW
									,-1
									,this
									,TRUE);
	helper.PushItem(*pSlot);


}



void UIItemMake::PopGoods( BaseSlot* pSlot )
{
	if(pSlot->GetSlotIndex() != SI_ENCHANT)
	{
		_SUPER::PopGoods(pSlot);
		return;
	}

	////////////////////////////////////
	UIContainHelper	helper(m_arMaterialContainer
									,m_pPlayer
									,MATERIAL_CONTAINER_ROW
									,-1
									,this
									,TRUE);
	helper.PopItem(*pSlot);


}

void UIItemMake::SetGoodsState( BaseSlot* pSlot,DWORD dwState )
{
	if(pSlot->GetSlotIndex() != SI_ENCHANT)
	{
		_SUPER::SetGoodsState(  pSlot, dwState);
		return;
	}

	SLOTPOS posSlot = pSlot->GetPos();
	if(posSlot == INVALID_POSTYPE)
		return;

	INT	nIndex;
	INT	nPage ;
	//BOOL	bRemove;

	nIndex	= posSlot%MATERIAL_CONTAINER_ROW;
	nPage		= posSlot/MATERIAL_CONTAINER_ROW;

	IconDragListImg*	pImg;
	pImg = m_arMaterialContainer[nPage]->GetItemAt(nIndex);
	if(pImg)
	{
		pImg->SetEnable(dwState != SLOT_UISTATE_DISABLE);
		pImg->SetDeactivate(dwState == SLOT_UISTATE_DEACTIVATED);
	}
}


VUCtrlIconDrag* UIItemMake::GetMaterialDragCtrl(SLOTPOS atPos)
{
	INT	nIndex;
	INT	nPage ;

	nIndex	= atPos%MATERIAL_CONTAINER_ROW;
	nPage		= atPos/MATERIAL_CONTAINER_ROW;
	return m_arMaterialContainer[nPage];
}


};//namespace gameui


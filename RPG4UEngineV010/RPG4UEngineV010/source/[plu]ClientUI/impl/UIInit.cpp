
#include "stdafx.h"
/********************************************************************
	Created by UIEditor.exe
	FileName: E:\Work\XSanGuoRuntime\Client\UI\Init.cpp
*********************************************************************/
#include <assert.h>
#include "..\..\HRUI\HR_UI_Mgr.h"
#include "Init.h"
#include "UIMgr.h"
CUI_ID_FRAME_Init s_CUI_ID_FRAME_Init;
MAP_FRAME_RUN( s_CUI_ID_FRAME_Init, OnFrameRun )
MAP_FRAME_RENDER( s_CUI_ID_FRAME_Init, OnFrameRender )
MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Init, ID_BUTTON_WebOnButtonClick )
MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Init, ID_BUTTON_BBSOnButtonClick )
MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Init, ID_BUTTON_LoginOnButtonClick )
MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Init, ID_BUTTON_UpDataOnButtonClick )
MAP_LIST_SELECT_CHANGE_CALLBACK( s_CUI_ID_FRAME_Init, ID_LIST_BullOnListSelectChange )
MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Init, ID_BUTTON_LOGINOnButtonClick )
MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_Init, ID_BUTTON_EXITOnButtonClick )
CUI_ID_FRAME_Init::CUI_ID_FRAME_Init()
{
	// Member
	m_pID_FRAME_Init = NULL;
	m_pID_BUTTON_Web = NULL;
	m_pID_BUTTON_BBS = NULL;
	m_pID_BUTTON_Login = NULL;
	m_pID_BUTTON_UpData = NULL;
	m_pID_LIST_Bull = NULL;
	m_pID_BUTTON_LOGIN = NULL;
	m_pID_BUTTON_EXIT = NULL;

}
// Frame
bool CUI_ID_FRAME_Init::OnFrameRun()
{
	return true;
}
bool CUI_ID_FRAME_Init::OnFrameRender()
{
	return true;
}
// Button
bool CUI_ID_FRAME_Init::ID_BUTTON_WebOnButtonClick( CHR_UI_Object* pSender )
{
	return true;
}
// Button
bool CUI_ID_FRAME_Init::ID_BUTTON_BBSOnButtonClick( CHR_UI_Object* pSender )
{
	return true;
}
// Button
bool CUI_ID_FRAME_Init::ID_BUTTON_LoginOnButtonClick( CHR_UI_Object* pSender )
{
	return true;
}
// Button
bool CUI_ID_FRAME_Init::ID_BUTTON_UpDataOnButtonClick( CHR_UI_Object* pSender )
{
	return true;
}
// List
void CUI_ID_FRAME_Init::ID_LIST_BullOnListSelectChange( CHR_UI_Object* pSender, CHR_UI_List::S_List* pItem )
{
}
// Button
bool CUI_ID_FRAME_Init::ID_BUTTON_LOGINOnButtonClick( CHR_UI_Object* pSender )
{
	return true;
}
// Button
bool CUI_ID_FRAME_Init::ID_BUTTON_EXITOnButtonClick( CHR_UI_Object* pSender )
{
	return true;
}

// 装载UI
bool CUI_ID_FRAME_Init::LoadUI()
{
	DWORD dwResult = theHRUIMgr.AddFrame( "interface\\Init.itf" );
	if ( dwResult == 0 )
	{
		MESSAGE_BOX("读取文件[UI\\Init.UI]失败")
		return false;
	}
	else if ( dwResult != 57121/*文件版本号*/ )
	{
		MESSAGE_BOX("读取文件[UI\\Init.UI]与源代码版本不一样")
	}
	return DoControlConnect();
}
// 关连控件
bool CUI_ID_FRAME_Init::DoControlConnect()
{
//	CUIBase::DoControlConnect();
	theHRUIMgr.OnFrameRun( ID_FRAME_Init, s_CUI_ID_FRAME_InitOnFrameRun );
	theHRUIMgr.OnFrameRender( ID_FRAME_Init, s_CUI_ID_FRAME_InitOnFrameRender );
	theHRUIMgr.OnButtonClick( ID_FRAME_Init, ID_BUTTON_Web, s_CUI_ID_FRAME_InitID_BUTTON_WebOnButtonClick );
	theHRUIMgr.OnButtonClick( ID_FRAME_Init, ID_BUTTON_BBS, s_CUI_ID_FRAME_InitID_BUTTON_BBSOnButtonClick );
	theHRUIMgr.OnButtonClick( ID_FRAME_Init, ID_BUTTON_Login, s_CUI_ID_FRAME_InitID_BUTTON_LoginOnButtonClick );
	theHRUIMgr.OnButtonClick( ID_FRAME_Init, ID_BUTTON_UpData, s_CUI_ID_FRAME_InitID_BUTTON_UpDataOnButtonClick );
	theHRUIMgr.OnListSelectChange( ID_FRAME_Init, ID_LIST_Bull, s_CUI_ID_FRAME_InitID_LIST_BullOnListSelectChange );
	theHRUIMgr.OnButtonClick( ID_FRAME_Init, ID_BUTTON_LOGIN, s_CUI_ID_FRAME_InitID_BUTTON_LOGINOnButtonClick );
	theHRUIMgr.OnButtonClick( ID_FRAME_Init, ID_BUTTON_EXIT, s_CUI_ID_FRAME_InitID_BUTTON_EXITOnButtonClick );

	m_pID_FRAME_Init = (CHR_UI_Frame*)theHRUIMgr.FindFrame( ID_FRAME_Init );
	m_pID_BUTTON_Web = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_BUTTON_Web );
	m_pID_BUTTON_BBS = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_BUTTON_BBS );
	m_pID_BUTTON_Login = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_BUTTON_Login );
	m_pID_BUTTON_UpData = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_BUTTON_UpData );
	m_pID_LIST_Bull = (CHR_UI_List*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_LIST_Bull );
	m_pID_BUTTON_LOGIN = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_BUTTON_LOGIN );
	m_pID_BUTTON_EXIT = (CHR_UI_Button*)theHRUIMgr.FindControl( ID_FRAME_Init, ID_BUTTON_EXIT );

	assert( m_pID_FRAME_Init );
	assert( m_pID_BUTTON_Web );
	assert( m_pID_BUTTON_BBS );
	assert( m_pID_BUTTON_Login );
	assert( m_pID_BUTTON_UpData );
	assert( m_pID_LIST_Bull );
	assert( m_pID_BUTTON_LOGIN );
	assert( m_pID_BUTTON_EXIT );

	USE_SCRIPT( eUI_OBJECT_Init, m_pID_FRAME_Init );
	return true;
}
	// 卸载UI
bool CUI_ID_FRAME_Init::UnLoadUI()
{
	return theHRUIMgr.RemoveFrame( "interface\\Init.itf" );
}
// 是否可视
bool CUI_ID_FRAME_Init::IsVisable()
{
	return m_pID_FRAME_Init->IsVisable();
}
// 设置是否可视
void CUI_ID_FRAME_Init::SetVisable( const bool bVisable )
{
	m_pID_FRAME_Init->SetVisable( bVisable );
}

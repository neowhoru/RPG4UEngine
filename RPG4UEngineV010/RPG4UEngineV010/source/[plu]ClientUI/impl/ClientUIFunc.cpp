/*////////////////////////////////////////////////////////////////////////
文 件 名：ClientUIFunc.cpp
创建日期：2008年5月29日
最后更新：2008年5月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ClientUIFunc.h"
#include "VUCtrlListImg.h"
#include "Character.h"
#include "ItemManager.h"
#include "SkillEffect.h"
#include "PlayerAttributes.h"
#include "ConstTextRes.h"

using namespace object;


BOOL UIUtil::UpateEffectStates(Character* pChar, VUCtrlListImg* pListImg)
{
	__CHECK_PTR(pChar);
	__CHECK_PTR(pListImg);

	SKILL_EFFECT*			pSkillEffect;
	SKILL_EFFECTDAT*	pEffect;

	INT					nCount		(0);
	INT					nCountPrev	(0);
	IconDragListImg*	pImgIcon;

	nCountPrev = pListImg->GetItemCount();

	//pListImg->Clear();
	//pListImg->SetVisible( FALSE );

	pSkillEffect = pChar->GetFirstSkillEffect();
	for(;pSkillEffect;pSkillEffect = pChar->GetNextSkillEffect())
	{
		pEffect = &pSkillEffect->GetEffectData();
		if(pEffect->dwStatusID == 0)
			continue;

		pImgIcon = pListImg->GetItemAt(nCount);
		if(!pImgIcon)
		{
			IconDragListImg	stItem;
			pListImg->SetItem(&stItem,nCount);
			pImgIcon = pListImg->GetItemAt(nCount);
			continue;
		}
		pImgIcon->SetData	(pEffect->iEffectID//pEffect->dwStatusID
							,1
							,VITEMTYPE_STATUS);
		pImgIcon->SetTimer(pSkillEffect->GetTimer());
		//pImgIcon->SetTime	(g_CurTime - pEffect->iRemainTime, pEffect->dwDuration);

		pListImg->SetSpareTime(nCount, pEffect->iRemainTime);

		nCount++;
	}//for(;pEffect;pEffect = pChar->GetNextSkillEffect())

	///////////////////////////////////////////
	for(INT n=nCount; n<nCountPrev; n++)
	{
		pListImg->RemoveItemAt(n);
	}

	pListImg->SetVisible( nCount > 0 );

	return nCount > 0;
}


LPCTSTR UIUtil::GetShowText(AttributeValue&	attr)
{
	return GetShowText(attr[ATTRIBUTE_KIND_BASE],	attr[ATTRIBUTE_KIND_CALC]);
}

LPCTSTR UIUtil::GetShowText(DWORD dwValue1,DWORD dwValue2)
{
	static StringHandle	sShowText;

	LPCSTR		szFormat;
	LPCSTR		szFormat1;
	LPCSTR		szFormat2;

	szFormat1	= _STRING(TEXTRES_EXTRA_BASEPROPERTY);
	szFormat2	= _STRING(TEXTRES_EXTRA_BASEPROPERTY_CHANGED);

	if(szFormat1[0] == 0)
		szFormat1 = "{#FF005fFF=%d#}/{#FF007fFF=%d#}";
	if(szFormat2[0] == 0)
		szFormat2 = "{#FF007fFF=%d#}/{#FF00FF00=%d#}";

	if(dwValue1 ==	dwValue2)
		szFormat = szFormat1;
	else
		szFormat = szFormat2;

	sShowText.Format(szFormat, dwValue1 ,	dwValue2);
	
	return sShowText;
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：UILogin.cpp
创建日期：2007年11月15日
最后更新：2007年11月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "ConstTextRes.h"
#include "VUCtrlManager.h"
#include "UILogin.h"
#include "UISelectServer.h"
#include "RegistryUtil.h"
#include "UIMessageBox.h"
#include "V3DSceneUtil.h"
#include "UICharSelect.h"
#include "UIProgress.h"
#include "UISelectBox.h"
#include "UINpcDialog.h"
#include "TextResManager.h"
#include "NetworkDefine.h"
#include "V3DUtil.h"
#include "PathInfoParser.h"

#include "LoginUIListener.h"
#include "ApplicationSetting.h"
#include "V3DGameWorld.h"
#include "UIServerGroup.h"
#include "LoginScene.h"
#include "GameClientApplication.h"
#include "NetworkLayer.h"
#include "RegistryUtil.h"
#include "GameParameter.h"
#include "NetworkSystem.h"

#define PLAYER_NAME_KEY        TEXT("Software\\RPG4U\\StudyPartyII")
namespace gameui
{

TCHAR*	SEC_ACCOUNT			= TEXT("Account");
TCHAR*	SEC_BIGAREA			= TEXT("ServerArea");
TCHAR*	SEC_SERVERINDEX	= TEXT("ServerNo");

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
LoginUIListener& GetUILoginLoginUIListenerUIListener()
{
	static gameui::LoginUIListener staticLoginUIListener;
	return staticLoginUIListener;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UILogin, ()  , gamemain::eInstPrioClientUI/*UILogin*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE	( theUILogin, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUILogin, OnRender )
	UIPROC_EDIT_ENTER			( theUILogin, ID_EDIT_IDOnEditEnter )
	UIPROC_EDIT_ENTER			( theUILogin, ID_EDIT_PASOnEditEnter )
	UIPROC_BUTTON_CLICK		( theUILogin, ID_BUTTON_OKOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUILogin, ID_BUTTON_EXITOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUILogin, ID_BUTTON_WEBOnButtonClick )
	UIPROC_COMBO_BOX_CHANGE	( theUILogin, ID_COMBOBOX_servelistOnComboBoxChange )
	UIPROC_CHECK_BOX_CHECK	( theUILogin, ID_CHECKBOX_SAVEOnCheckBoxCheck )
	UIPROC_BUTTON_CLICK		( theUILogin, ID_BUTTON_CANCELOnButtonClick )
//UIPROC_LIST_SELECT_CHANGE( theUILogin, ID_LIST_3021OnListSelectChange )
};//namespace uicallback			  
using namespace uicallback;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UILogin::UILogin()
:VUIControl( gameui::eUILogin )
{
	// Member
	m_pID_FRAME_LOGIN = NULL;
	m_pID_EDIT_ID		= NULL;
	m_pID_EDIT_PAS		= NULL;
	m_pID_BUTTON_OK	= NULL;
	m_pID_TEXT_SEREV	= NULL;
	m_pID_BUTTON_EXIT = NULL;
	m_pID_BUTTON_WEB	= NULL;
	m_pID_LIST_SERVER = NULL;
	m_pID_PICTURE_BACK= NULL;

	m_pUIServerGroup	= new UIServerGroup;

	m_pID_CHECKBOX_SAVE = NULL;
	m_pID_BUTTON_CANCEL = NULL;

	m_nBigAreaIndex	= 0;
	m_nAreaIndex		= 0;
	m_nServer			= 0;

	m_bLoginWait	= FALSE;

	m_pID_LoginWait = NULL;
	m_bUserReconnect = FALSE;
	m_nControlMode  = 0;
	m_fSmallMapScale = .0f;
}

UILogin::~UILogin()
{
	SAFE_DELETE(m_pUIServerGroup);
}

bool UILogin::Output( LPCTSTR szError, ... )
{
	SetLoginWaitText(szError);
	return true;
}

bool UILogin::OutputWithTime( LPCTSTR szError,... )
{
	SetLoginWaitText(szError);
	return true;
}


// Frame
BOOL UILogin::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}


BOOL UILogin::OnRender(DWORD /*dwTick*/)
{
	

	return TRUE;
	
}

// Edit
void UILogin::ID_EDIT_IDOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ )
{
	ID_BUTTON_OKOnButtonClick( NULL );
}

// Edit
void UILogin::ID_EDIT_PASOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ )
{
	ID_BUTTON_OKOnButtonClick( NULL );
}
// CheckBox
void UILogin::ID_CHECKBOX_SAVEOnCheckBoxCheck( VUCtrlObject* /*pSender*/, BOOL* /*pbChecked*/ )
{
}

// Button
BOOL UILogin::ID_BUTTON_CANCELOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	//theNetworkLayer.NetworkDisconnect(CK_LOGINSERVER);
	//theNetworkClient.Disconnect(CK_LOGINSERVER);
	theLoginScene.Disconnect();

	SetLoginInfo("");
	SetLoginWaitText("");

	SetEnable( TRUE );

	theUICtrlManager.SetFocus(m_pID_EDIT_PAS);
	return TRUE;
}




// Button
BOOL UILogin::ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	

	StringHandle szID	(m_pID_EDIT_ID->GetText(), TRUE);
	StringHandle szPwd(m_pID_EDIT_PAS->GetText(), TRUE );


	if ( szID.IsBlank() ||  szPwd.IsEmpty())
	{
		theUIMessageBox.Show( (TEXTRES_EnterUsePass), (TEXTRES_Prompt), MB_OK );
		return FALSE;
	}

	if(singleton::GetLoginScene().SendLoginInfo(szID, szPwd))
	{
		strcpy( m_stAccount.szID, szID );
		strcpy( m_stAccount.szPwd, szPwd );

		OUTPUTTIP(TEXTRES_USERLOGINING);
		//*m_pID_LoginWait = _STRING(TEXTRES_USERLOGINING);//"正在登录，请稍候...";

		theUILogin.SetEnable( FALSE );

		_WriteSetting();

		return TRUE;
	}

	OUTPUTTIP(TEXTRES_SelectServerFailed);
	//theGameUIManager.UIMessageBox();
	//*m_pID_LoginWait = _STRING(TEXTRES_SelectServerFailed);//"服务器未连接上...";


	return FALSE;

	
}



BOOL UILogin::ID_BUTTON_EXITOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	

	GetGameClientApplication().Exit();


	return TRUE;
	
}

// Button
BOOL UILogin::ID_BUTTON_WEBOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	return TRUE;
	
}

// ComboBox
void UILogin::ID_COMBOBOX_servelistOnComboBoxChange( VUCtrlObject* /*pSender*/, LPCSTR  /*szData*/ )
{
	
	ChangeGameServerOnly( m_pID_LIST_SERVER->GetListBox().GetCurSelIndex());
	
}



// 装载UI
BOOL UILogin::LoadUI()
{
	
	GetUILoginLoginUIListenerUIListener().RegisterMe();

	m_pID_FRAME_LOGIN = theUICtrlManager.LoadFrame( UIDOC_PATH( "Login") );
	if ( m_pID_FRAME_LOGIN == 0 )
	{
		UIMessageLog("读取文件[UI\\Login.UI]失败")
		return FALSE;
	}

	return InitControls();

	
}

// 关连控件
BOOL UILogin::InitControls()
{
	
	VUIControl::InitControls();

	m_pID_PICTURE_BACK	= (VUCtrlPicture*)	m_pID_FRAME_LOGIN->FindControl( ID_PICTURE_BACK );
	m_pID_EDIT_ID			= (VUCtrlEdit*)		m_pID_FRAME_LOGIN->FindControl( ID_EDIT_ID );
	m_pID_EDIT_PAS			= (VUCtrlEdit*)		m_pID_FRAME_LOGIN->FindControl( ID_EDIT_PAS );
	m_pID_BUTTON_OK		= (VUCtrlButton*)		m_pID_FRAME_LOGIN->FindControl( ID_BUTTON_OK );
	m_pID_TEXT_SEREV		= (VUCtrlText*)		m_pID_FRAME_LOGIN->FindControl( ID_TEXT_SEREV );
	m_pID_BUTTON_EXIT		= (VUCtrlButton*)		m_pID_FRAME_LOGIN->FindControl( ID_BUTTON_EXIT );
	m_pID_BUTTON_WEB		= (VUCtrlButton*)		m_pID_FRAME_LOGIN->FindControl( ID_BUTTON_WEB );
	m_pID_LIST_SERVER		= (VUCtrlComboBox*)	m_pID_FRAME_LOGIN->FindControl( ID_COMBOBOX_servelist );
	m_pID_CHECKBOX_SAVE	= (VUCtrlCheckBox*)	m_pID_FRAME_LOGIN->FindControl( ID_CHECKBOX_SAVE );
	m_pID_LoginWait		= (VUCtrlText*)		m_pID_FRAME_LOGIN->FindControl( ID_TEXT_LoginWait);
	m_pID_BUTTON_CANCEL	= (VUCtrlButton*)		m_pID_FRAME_LOGIN->FindControl( ID_BUTTON_CANCEL );
	m_pID_Text_LoginInfo = (VUCtrlText*)		m_pID_FRAME_LOGIN->FindControl( ID_TEXT_LoginInfo );

	assert( m_pID_PICTURE_BACK );
	assert( m_pID_EDIT_ID );
	assert( m_pID_EDIT_PAS );
	assert( m_pID_BUTTON_OK		);
	assert( m_pID_BUTTON_EXIT	);
	assert( m_pID_BUTTON_WEB	);
	assert( m_pID_LIST_SERVER	);
	assert( m_pID_CHECKBOX_SAVE );
	assert( m_pID_BUTTON_CANCEL );
	assert( m_pID_LoginWait );
	assert( m_pID_Text_LoginInfo );
	assert( m_pID_TEXT_SEREV	);

	m_pID_FRAME_LOGIN->SetProcOnFrameMove			(theUILoginOnFrameMove);
	m_pID_FRAME_LOGIN->SetProcOnRender		(theUILoginOnRender, TRUE);

	m_pID_EDIT_ID			->SetProcOnEnter			(	theUILoginID_EDIT_IDOnEditEnter );
	m_pID_EDIT_PAS			->SetProcOnEnter			(	theUILoginID_EDIT_PASOnEditEnter );
	m_pID_BUTTON_OK		->SetProcOnButtonClick	(	theUILoginID_BUTTON_OKOnButtonClick );
	m_pID_BUTTON_EXIT		->SetProcOnButtonClick	(	theUILoginID_BUTTON_EXITOnButtonClick );
	m_pID_BUTTON_WEB		->SetProcOnButtonClick	(	theUILoginID_BUTTON_WEBOnButtonClick );
	m_pID_LIST_SERVER		->SetProcOnChange			(	theUILoginID_COMBOBOX_servelistOnComboBoxChange );
	m_pID_CHECKBOX_SAVE	->SetProcOnCheck			(	theUILoginID_CHECKBOX_SAVEOnCheckBoxCheck );
	m_pID_BUTTON_CANCEL	->SetProcOnButtonClick	(	theUILoginID_BUTTON_CANCELOnButtonClick );


	m_pID_FRAME_LOGIN->SetWidth( SCREEN_WIDTH );
	m_pID_FRAME_LOGIN->SetHeight( SCREEN_HEIGHT );

	m_pID_EDIT_ID->SetMaxLength( MAX_NAME_LENGTH );
	m_pID_EDIT_PAS->SetMaxLength( MAX_PASSWORD_LENGTH );

	m_pID_BUTTON_CANCEL->SetVisible( FALSE );



	m_pID_EDIT_ID	->SetText(theClientSetting.GetLoginID());
	m_pID_EDIT_PAS	->SetText(theClientSetting.GetLoginPassword());


	if(!m_pUIServerGroup)
		m_pUIServerGroup = new UIServerGroup;

	/*////////////////////////////////////////////////////////////////////////
	// Init
	/*////////////////////////////////////////////////////////////////////////
	_ReadSetting();

	if ( strlen( m_stAccount.szID ) > 0 )
	{
		m_pID_EDIT_ID->SetText( m_stAccount.szID );
		m_pID_CHECKBOX_SAVE->SetCheck( TRUE );
	}
	else
	{
		m_pID_CHECKBOX_SAVE->SetCheck( FALSE );
	}
	UISCRIPT_ENABLE( UIOBJ_LOGIN, m_pID_FRAME_LOGIN );


	if(m_BigAreaInfos.size() > 0 && m_NameInfos.size() > 0)
	{
		m_nBigAreaIndex = (DWORD)m_nBigAreaIndex < m_BigAreaInfos.size() ?  m_nBigAreaIndex : 0;
		m_nServer = (DWORD)m_nServer < m_NameInfos.size() ? m_nServer : -1;

		if(m_nServer != -1)
			ChangeGameServerOnly(m_nServer);
		else
			m_pID_LIST_SERVER->GetEditInput().SetText( theTextResManager.GetString(292) );

		ListShowServerInfo(m_nServer);
	}

	return TRUE;
	
}


// 卸载UI
BOOL UILogin::UnLoadUI()
{

	UISCRIPT_DISABLE( UIOBJ_LOGIN );
	GetUILoginLoginUIListenerUIListener().UnregisterMe();

	BOOL bRet;
	
	bRet = theUICtrlManager.RemoveFrame( UIDOC_PATH( "Login") );
	if(bRet)
	{
		SAFE_DELETE(m_pUIServerGroup);
	}
	return bRet;
}


// 是否可视
BOOL UILogin::IsVisible()
{
	return m_pID_FRAME_LOGIN->IsVisible();
}

// 设置是否可视
void UILogin::SetVisible( BOOL bVisible )
{
	m_pID_FRAME_LOGIN->SetVisible( bVisible );
	if(bVisible)
	{
		theLogSystem.AddLoggerListener(this);
	}
	else
	{
		theLogSystem.RemoveLoggerListener(this);
	}
}

void UILogin::SetEnable( BOOL bEnable )
{
	if(!m_pID_BUTTON_OK)
		return;

	if ( bEnable )
	{
		//m_pID_BUTTON_OK->SetVisible( TRUE );
		m_pID_BUTTON_OK->SetEnable( TRUE );
		//m_pID_BUTTON_CANCEL->SetVisible( FALSE );
		m_pID_BUTTON_CANCEL->SetEnable( FALSE );

		m_pID_EDIT_ID->SetReadOnly( FALSE );
		m_pID_EDIT_PAS->SetReadOnly( FALSE );


		*m_pID_Text_LoginInfo	= "";
		*m_pID_LoginWait			= "";
	}
	else
	{
		//m_pID_BUTTON_OK->SetVisible( FALSE );
		m_pID_BUTTON_OK->SetEnable( FALSE );
		//m_pID_BUTTON_CANCEL->SetVisible( TRUE );
		m_pID_BUTTON_CANCEL->SetEnable( TRUE );

		m_pID_EDIT_ID->SetReadOnly( TRUE );
		m_pID_EDIT_PAS->SetReadOnly( TRUE );
	}
}



void UILogin::_ReadSetting()
{
	HKEY hkey;
	if( ERROR_SUCCESS == RegCreateKeyEx( HKEY_CURRENT_USER, PLAYER_NAME_KEY, 
		0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL ) )
	{
		DWORD dwValue(0);
		RegistryUtil reg;
		reg.ReadStringRegKey	( hkey, SEC_ACCOUNT,			m_stAccount.szID, 128, "" );
		reg.ReadIntRegKey		( hkey, SEC_BIGAREA,			&dwValue, 0 );	m_nBigAreaIndex	= dwValue;
		reg.ReadIntRegKey		( hkey, SEC_SERVERINDEX,	&dwValue, 0 );	m_nServer			= dwValue;
		RegCloseKey( hkey );
	}

	m_nBigAreaIndex = m_nBigAreaIndex < (m_serverList.GetBigAreaCount() - 1) ? m_nBigAreaIndex : m_serverList.GetBigAreaCount() - 1;
}


void UILogin::_WriteSetting()
{
	RegistryUtil reg;
	HKEY hkey;
	if( ERROR_SUCCESS == RegCreateKeyEx( HKEY_CURRENT_USER, PLAYER_NAME_KEY, 
		0, NULL, REG_OPTION_NON_VOLATILE, KEY_ALL_ACCESS, NULL, &hkey, NULL ) )
	{
		if ( m_pID_CHECKBOX_SAVE->IsChecked() )
			reg.WriteStringRegKey( hkey, SEC_ACCOUNT, m_stAccount.szID );
		else
			reg.WriteStringRegKey( hkey, SEC_ACCOUNT, "" );

		reg.WriteIntRegKey		( hkey, SEC_BIGAREA,			m_nBigAreaIndex );
		reg.WriteIntRegKey		( hkey, SEC_SERVERINDEX,	m_nServer );

		RegCloseKey( hkey );
	}
	

}




void UILogin::LoadServerList()
{

	m_serverList.LoadFromFile(_PATH_SETTING("ServerList.ini"));

	ChangeGameServer(m_nBigAreaIndex, m_nAreaIndex, m_nServer);
}


BOOL UILogin::ChangeGameServer( int nBigArea, int nArea, int nServer)
{
	if(nBigArea < 0)
		return FALSE;
	
	ServerListInfo::sNODE_INFO *pNode = NULL;

	m_nBigAreaIndex = nBigArea;
	m_nAreaIndex	= nArea;
	m_nServer		= nServer;

	m_BigAreaInfos.clear();
	m_AreaInfos.clear();
	m_NameInfos.clear();


	// BigAreas
	for ( int n=0; n<m_serverList.GetNodeCount(); n++ )
	{
		pNode = m_serverList.GetNode(n);
		BOOL bFound = FALSE;
		for ( UINT m=0; m<m_BigAreaInfos.size(); m++ )
		{
			if( pNode->GetBigArea() == m_BigAreaInfos[m] )
			{
				bFound = TRUE;
				break;
			}
		}
		if ( bFound == TRUE )
			continue;
		m_BigAreaInfos.push_back( pNode->GetBigArea() );
	}


	//
	if ( m_BigAreaInfos.size() > 0 )
	{
		for ( int m=0; m<m_serverList.GetNodeCount(); m++ )
		{
			pNode = m_serverList.GetNode(m);
			if ( pNode->GetBigArea() == m_BigAreaInfos[nBigArea] )
			{
				BOOL bFound = FALSE;
				for ( UINT i=0; i<m_AreaInfos.size(); i++ )
				{
					if ( m_AreaInfos[i] == pNode->GetArea() )
					{
						bFound = TRUE;
						break;
					}
				}
				if ( bFound == FALSE )
				{
					m_AreaInfos.push_back( pNode->GetArea() );
				}
			}
		}
	}


	//
	if ( m_AreaInfos.size() > 0 && m_BigAreaInfos.size() > 0 )
	{
		for ( int m=0; m<m_serverList.GetNodeCount(); m++ )
		{
			pNode = m_serverList.GetNode(m);
			if ( pNode->GetBigArea() == m_BigAreaInfos[nBigArea] &&
				pNode->GetArea() == m_AreaInfos[nArea] )
			{
				m_NameInfos.push_back( pNode->GetName() );
			}
		}
	}


	//ListShowServerInfo(nServer);
	return TRUE;
	
}


void UILogin::ChangeGameServerOnly(int nServer)
{
	

	ServerListInfo::sNODE_INFO *pNode = NULL;

	m_nServer = nServer;

	std::string szServerName;
	if ( m_AreaInfos.size() > 0 && m_BigAreaInfos.size() > 0 )
	{
		for ( int m=0; m<m_serverList.GetNodeCount(); m++ )
		{
			pNode = m_serverList.GetNode(m);
			if ( pNode->GetBigArea() == m_BigAreaInfos[m_nBigAreaIndex] &&	pNode->GetArea() == m_AreaInfos[m_nAreaIndex] )
			{
				if(m == nServer)
				{
					m_strIP = pNode->GetIpAddr();
					m_dwPort = pNode->wPort;
					szServerName = pNode->GetName();
				}
			}
		}
	}

	char szTemp[512] = {0};
	sprintf(szTemp, "%s-%s", m_BigAreaInfos[m_nBigAreaIndex].c_str(), szServerName.c_str() );
	m_pID_TEXT_SEREV->SetText(szTemp);

	
}


void UILogin::ListShowServerInfo(int nServer)
{
	sUILIST_ITEM stList;

	for ( UINT n=0; n<m_NameInfos.size(); n++ )
	{
		stList.SetData( m_NameInfos[n].c_str() );
		m_pID_LIST_SERVER->GetListBox().AddItem( &stList );
	}

	if( nServer != -1 && (UINT)nServer < m_NameInfos.size())
		m_pID_LIST_SERVER->GetEditInput().SetText( m_NameInfos[nServer].c_str() );
}


int UILogin::GetBigAreaCount()
{
	if(m_serverList.GetBigAreaCount() > 0)
	{
		int nCount = m_serverList.GetBigAreaCount();
		return nCount;
	}
	return 0;
}



void UILogin::ShowAccountInput(BOOL bShow)
{
	//assert(0 && "显示或隐藏 Account输入框");//
	if(!m_pID_BUTTON_OK)
		return;
	m_pID_BUTTON_OK		->SetVisible( bShow );
	m_pID_BUTTON_OK		->SetEnable	( bShow );
	m_pID_BUTTON_CANCEL	->SetVisible( bShow );
	m_pID_BUTTON_CANCEL	->SetEnable	( bShow );

	m_pID_EDIT_ID			->SetVisible( bShow );
	m_pID_EDIT_PAS			->SetVisible( bShow );
	m_pID_PICTURE_BACK	->SetVisible( bShow );
	m_pID_CHECKBOX_SAVE	->SetVisible( bShow );

	if(bShow)
	{
		SetEnable(TRUE);
	}
}

void UILogin::ShowServerList(BOOL bShow)
{
	//assert(0 && "显示或隐藏 ServerList");//
	if(!m_pUIServerGroup->GetFrame())
	{
		if(!m_pUIServerGroup->LoadUI(m_pID_FRAME_LOGIN))
			return;
		//m_pID_FRAME_LOGIN->AppendControl(m_pUIServerGroup->GetFrame());
	}
	m_pUIServerGroup->SetVisible(bShow);
}



void UILogin::LockUI	(BOOL bLock)
{
	//assert(0 && "锁定UI...不能操作");//
	SetEnable(!bLock);

}


void UILogin::SetLoginWaitText(LPCSTR  szText)
{
	if(m_pID_LoginWait)
	{
		StringHandle	sText(szText,TRUE);
		if(!sText.IsBlank(TRUE))
		{
			//OUTPUTTIP(_STRING(TEXTRES_USERLOGINING));
			*m_pID_LoginWait = szText;
		}
	}
}


void UILogin::SetLoginWaitShow(BOOL bShow)
{
	if(bShow)
		m_pID_LoginWait->SetVisible(TRUE);
	else
		m_pID_LoginWait->SetVisible(FALSE);
}






void UILogin::SetLoginInfo(LPCTSTR	szText)
{
	if( m_pID_Text_LoginInfo )
		*m_pID_Text_LoginInfo = szText;
}


void UILogin::ShowConnectedDlg()
{
	theUIMessageBox.Show( TEXTRES_ConnectFail, TEXTRES_ConnectFail , MB_OK );
	*m_pID_Text_LoginInfo = "";
}

// Button
BOOL UILogin::ProcessLog( )
{

	return TRUE;
}	


};//namespace gameui

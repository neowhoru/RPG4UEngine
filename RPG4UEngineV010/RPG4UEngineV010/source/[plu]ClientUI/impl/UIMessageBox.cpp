/*////////////////////////////////////////////////////////////////////////
文 件 名：UIMessageBox.cpp
创建日期：2007年7月24日
最后更新：2007年7月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIMessageBox.h"
#include "MessageBoxUIListener.h"
#include "TextResManager.h"
#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::MessageBoxUIListener& GetUIMessageBoxMessageBoxUIListenerUIListener()
{
	static gameui::MessageBoxUIListener staticMessageBoxUIListener;
	return staticMessageBoxUIListener;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIMessageBox, ()  , gamemain::eInstPrioClientUI/*UIMessageBox*/);


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE     ( theUIMessageBox, OnFrameMove )
		UIPROC_FRAME_RENDER  ( theUIMessageBox, OnRender )
		UIPROC_BUTTON_CLICK     (theUIMessageBox, ID_BUTTON_YESOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIMessageBox, ID_BUTTON_NOOnButtonClick ); 
	UIPROC_BUTTON_CLICK     (theUIMessageBox, ID_BUTTON_COMFIRMOnButtonClick ); 
};//namespace uicallback
using namespace uicallback;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UIMessageBox::UIMessageBox()
{
	m_bUnLoad = FALSE;

	// Member
	m_pID_FRAME_MESSAGE = NULL;
	m_pID_EDIT_INFO = NULL;
	m_pID_BUTTON_YES = NULL;
	m_pID_BUTTON_NO = NULL;
	m_pID_BUTTON_COMFIRM = NULL;
	m_pID_TEXT_CAPTION = NULL;

	m_ProcCallback = NULL;
	m_pData			= NULL;
	m_dwDataSize	= 0;
}

	// Frame
BOOL UIMessageBox::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}

BOOL UIMessageBox::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

// Button
BOOL UIMessageBox::ID_BUTTON_YESOnButtonClick( VUCtrlObject* /*pThisCtrl*/ )
{
	if ( m_ProcCallback )
	{
		m_ProcCallback( TRUE, m_pData );
	}

	if ( m_pData && m_dwDataSize )
	{
		delete[] m_pData;
	}
	m_pData			= NULL;
	m_dwDataSize	= 0;

	if ( m_MessageInfos.size() > 0 )
	{
		m_MessageInfos.erase( m_MessageInfos.begin() );
	}
	Refresh();
	return TRUE;
	
}


// Button
BOOL UIMessageBox::ID_BUTTON_NOOnButtonClick( VUCtrlObject* /*pThisCtrl*/ )
{
	
	if ( m_ProcCallback )
	{
		m_ProcCallback( FALSE, m_pData );
	}

	if ( m_pData && m_dwDataSize )
	{
		delete[] m_pData;
	}
	m_pData			= NULL;
	m_dwDataSize	= 0;


	//m_MessageInfos.pop_back();
	if ( m_MessageInfos.size() > 0 )
	{
		m_MessageInfos.erase( m_MessageInfos.begin() );
	}
	Refresh();
	return TRUE;
	
}


//buttton
BOOL UIMessageBox::ID_BUTTON_COMFIRMOnButtonClick(VUCtrlObject* /*pThisCtrl*/ )
{
	
	if ( m_ProcCallback )
	{
		m_ProcCallback( FALSE, m_pData );
	}

	if ( m_pData && m_dwDataSize )
	{
		delete[] m_pData;
	}
	m_pData			= NULL;
	m_dwDataSize	= 0;


	if ( m_MessageInfos.size() > 0 )
	{
		m_MessageInfos.erase( m_MessageInfos.begin() );
	}
	Refresh();
	return TRUE;
	
}


// 装载UI
BOOL UIMessageBox::LoadUI()
{
	
	GetUIMessageBoxMessageBoxUIListenerUIListener().RegisterMe();

	m_pID_FRAME_MESSAGE = theUICtrlManager.LoadFrame( UIDOC_PATH( "MessageBox") );
	if ( m_pID_FRAME_MESSAGE == 0 )
	{
		UIMessageLog("读取文件[UI\\MessageBox.UI]失败")
		return FALSE;
	}

	m_bUnLoad = FALSE;
	return InitControls();
	
}


// 关连控件
BOOL UIMessageBox::InitControls()
{
	m_pID_EDIT_INFO		= (VUCtrlEdit*)m_pID_FRAME_MESSAGE->FindControl(  ID_EDIT_INFO );
	m_pID_BUTTON_YES		= (VUCtrlButton*)m_pID_FRAME_MESSAGE->FindControl(  ID_BUTTON_YES );
	m_pID_BUTTON_NO		= (VUCtrlButton*)m_pID_FRAME_MESSAGE->FindControl(  ID_BUTTON_NO );
	m_pID_BUTTON_COMFIRM = (VUCtrlButton*)m_pID_FRAME_MESSAGE->FindControl(ID_BUTTON_COMFIRM);
	m_pID_TEXT_CAPTION	= (VUCtrlText*)m_pID_FRAME_MESSAGE->FindControl(  ID_TEXT_CAPTION );

	assert( m_pID_EDIT_INFO );
	assert( m_pID_BUTTON_YES );
	assert( m_pID_BUTTON_NO );
	assert(m_pID_BUTTON_COMFIRM);
	assert( m_pID_TEXT_CAPTION );

	m_pID_FRAME_MESSAGE->SetProcOnFrameMove(  theUIMessageBoxOnFrameMove );
	m_pID_FRAME_MESSAGE->SetProcOnRender(  theUIMessageBoxOnRender );

	m_pID_BUTTON_YES->SetProcOnButtonClick(theUIMessageBoxID_BUTTON_YESOnButtonClick );
	m_pID_BUTTON_NO->SetProcOnButtonClick(theUIMessageBoxID_BUTTON_NOOnButtonClick );
	m_pID_BUTTON_COMFIRM->SetProcOnButtonClick(theUIMessageBoxID_BUTTON_COMFIRMOnButtonClick );

	return TRUE;
	
}
// 卸载UI
BOOL UIMessageBox::UnLoadUI()
{
	
	m_bUnLoad = TRUE;
	GetUIMessageBoxMessageBoxUIListenerUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "MessageBox") );
	
}
// 是否可视
BOOL UIMessageBox::IsVisible()
{
	
	return m_pID_FRAME_MESSAGE->IsVisible();
	
}
// 设置是否可视
void UIMessageBox::SetVisible( BOOL bVisible )
{
	
	if( m_pID_FRAME_MESSAGE )
	{
		m_pID_FRAME_MESSAGE->SetVisible( bVisible );
	}
	
}
void UIMessageBox::ClearData() 
{
	
	if ( m_pData && m_dwDataSize )
	{
		delete[] m_pData;
	}
	m_pData			= NULL;
	m_dwDataSize	= 0;

	if ( m_MessageInfos.size() > 0 )
	{
		m_MessageInfos.erase( m_MessageInfos.begin() );
	}
	Refresh();
	
}


void UIMessageBox::Refresh()
{
	
	if ( m_MessageInfos.size() > 0 )
	{
		sMESSAGE_BOX *pMsgInfo;

		pMsgInfo = &m_MessageInfos[0];
		*m_pID_EDIT_INFO		= pMsgInfo->m_strContent;
		*m_pID_TEXT_CAPTION	= pMsgInfo->m_strCaption;

		switch( pMsgInfo->m_nType ) 
		{
		case MB_OK:
			{
				m_pID_BUTTON_NO->SetVisible(FALSE);
				m_pID_BUTTON_YES->SetVisible(FALSE);
				m_pID_BUTTON_COMFIRM->SetVisible(TRUE);
			}
			break;

		case MB_OKCANCEL:
		case MB_YESNO:
			{ 
				m_pID_BUTTON_YES->SetVisible(TRUE);
				m_pID_BUTTON_NO->SetVisible(TRUE);
				m_pID_BUTTON_COMFIRM->SetVisible(FALSE);
			}
			break;

		default:
			assert(FALSE);
		    break;
		}

		m_ProcCallback = pMsgInfo->m_ProcCallback;
		m_pData			= pMsgInfo->m_pData;
		m_dwDataSize	= pMsgInfo->m_dwDataSize;

		SetVisible( TRUE );

		if ( pMsgInfo->m_bModal == TRUE )
		{
			m_pID_FRAME_MESSAGE->SetArrangeMode( ArrayMode_Top );
		}
		else
		{
			m_pID_FRAME_MESSAGE->SetArrangeMode( ArrayMode_Normal );
		}
		theUICtrlManager.BringToTop( m_pID_FRAME_MESSAGE );
	}
	else
	{
		SetVisible( FALSE );
	}
	
}

void UIMessageBox::Show	(LPCSTR            szText
                        ,LPCSTR            szCaption
								,int               nType
								,BOOL              bModal
								,PROC_BOX_CALLBACK procCallback
								,void*             pData
								,int               nDataLength)
{
	
	sMESSAGE_BOX sMessageInfo;
	sMessageInfo.m_strContent	= szText;
	sMessageInfo.m_strCaption	= szCaption;
	sMessageInfo.m_nType			= nType;
	sMessageInfo.m_ProcCallback= procCallback;
	sMessageInfo.m_bModal		= bModal;

	if ( pData && nDataLength > 0 )
	{
		sMessageInfo.m_pData			= new BYTE[nDataLength];
		sMessageInfo.m_dwDataSize	= nDataLength;
		memcpy( sMessageInfo.m_pData, pData, nDataLength );
	}
	else
	{
		sMessageInfo.m_pData			= pData;
		sMessageInfo.m_dwDataSize	= 0;
	}

	m_MessageInfos.push_back( sMessageInfo );
	Refresh();
	
}

void UIMessageBox::Show	(DWORD             dwTextID
                        ,DWORD             dwCaptionID
								,int               nType
								,BOOL              bModal
								,PROC_BOX_CALLBACK procCallback
								,void*             pData
								,int               nDataLength)
{
	Show	(theTextResManager.GetString(dwTextID)
			,dwCaptionID == INVALID_DWORD_ID?"":theTextResManager.GetString(dwCaptionID)
			,nType
			,bModal
			,procCallback
			,pData
			,nDataLength);
}



/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBankUI.cpp
创建日期：2007年11月4日
最后更新：2007年11月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIInputBox.h"
#include "UIMessageBox.h"
#include "FuncProfiler.h"
#include "UIBankUI.h"



#include "TextResManager.h"

#include "BankUIListener.h"
#include "GlobalInstancePriority.h"

static gameui::BankUIListener& GetUIListener()
{
	static gameui::BankUIListener s;
	return s;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(CUI_ID_FRAME_BANK, ()  , gamemain::eInstPrioClientUI/*CUI_ID_FRAME_BANK*/);
#else
#endif

namespace uicallback
{
	MAP_FRAME_RUN( s_CUI_ID_FRAME_BANK, OnFrameRun )
		MAP_FRAME_RENDER( s_CUI_ID_FRAME_BANK, OnFrameRender )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_BANK, ID_BUTTON_DepositOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_BANK, ID_BUTTON_GetOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_BANK, ID_BUTTON_CLOSEOnButtonClick )
		MAP_BUTTON_CLICK_CALLBACK( s_CUI_ID_FRAME_BANK, ID_BUTTON_UPDATEOnButtonClick )
};//namespace uicallback
using namespace uicallback;

CUI_ID_FRAME_BANK::CUI_ID_FRAME_BANK()
{
	
	// Member
	m_pID_FRAME_BANK = NULL;
	m_pID_TEXT_CURR_COIN = NULL;
	m_pID_TEXT_GOLD_MAX = NULL;
	m_pID_BUTTON_Deposit = NULL;
	m_pID_BUTTON_Get = NULL;
	m_pID_BUTTON_CLOSE = NULL;
	m_pID_TEXT_CURR_GOLD = NULL;
	m_pID_BUTTON_UPDATE = NULL;
	m_pID_TEXT_COIN_MAX = NULL;
	

}
// Frame
BOOL CUI_ID_FRAME_BANK::OnFrameRun(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL CUI_ID_FRAME_BANK::OnFrameRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL SaveMoneyToBank(const char* szInputData, void *pData)
{
	
	int nCount = atoi( szInputData );
	if ( nCount <= 0 )
		return FALSE;
	DWORD dwMoneyOnSelf = GetPlayerRole().GetData(PlayerRole::TYPE_MONEY);
	if(nCount <= dwMoneyOnSelf)
	{
		MsgReqBankTrade msg;
		msg.byOperate = MsgReqBankTrade::ePushMoney;
		msg.dwMoney = nCount;
		GetNetworkInput().SendMsg( &msg );
		//*s_CUI_ID_FRAME_BANK.m_pID_TEXT_Mount = nCount;
		//*s_CUI_ID_FRAME_BANK.m_pID_TEXT_OnSelf = dwMoneyOnSelf - nCount;
		return TRUE;
	}
	else
	{
		//你输入钱的数量超过你身上钱的数量！
		theUIMessageBox.Show(  theTextResManager.GetString(eUI_Money_Error_0) );
		return FALSE;
	}
	
}
// Button
BOOL CUI_ID_FRAME_BANK::ID_BUTTON_DepositOnButtonClick( VUCtrlObject* pSender )
{
	
	// 银行(存)
	//请输入您存钱的数量
	theUIInputBox.Show( theTextResManager.GetString( eUI_InputSaveMoney ), TRUE, TRUE,SaveMoneyToBank);
	return TRUE;
	
}
BOOL GetMoneyFromBank(const char* szInputData, void *pData)
{
	
	int nCount = atoi( szInputData );
	if ( nCount <= 0 )
		return FALSE;
	//DWORD dwMoneyOnBank ;
	//const char *szMoney = s_CUI_ID_FRAME_BANK.m_pID_TEXT_CURR_COIN->GetText();
	//dwMoneyOnBank = atoi(szMoney);
	if(nCount <= s_CUI_ID_FRAME_BANK.m_MONEY_ON_BANK)
	{
		MsgReqBankTrade msg;
		msg.byOperate = MsgReqBankTrade::ePopMoney;
		msg.dwMoney = nCount;
		GetNetworkInput().SendMsg( &msg );
		//*s_CUI_ID_FRAME_BANK.m_pID_TEXT_Mount = nCount;
		//*s_CUI_ID_FRAME_BANK.m_pID_TEXT_OnSelf = dwMoneyOnSelf - nCount;
		return TRUE;
	}
	else
	{
		theUIMessageBox.Show( theTextResManager.GetString( eUI_BackSave_error ) );
		return FALSE;
	}

	
}
// Button
BOOL CUI_ID_FRAME_BANK::ID_BUTTON_GetOnButtonClick( VUCtrlObject* pSender )
{
	
	// 银行(取)
	theUIInputBox.Show( theTextResManager.GetString( eUI_Take_Money ), TRUE, TRUE,GetMoneyFromBank);
	return TRUE;
	
}
// Button
BOOL CUI_ID_FRAME_BANK::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* pSender )
{
	
	SetVisible(FALSE);
	return TRUE;
	
}
BOOL upgradebank( BOOL bPressYes, void *pData)
{
	
	if( bPressYes == TRUE )
	{
		//玩家身上的钱
		DWORD dwMoneyOnSelf = GetPlayerRole().GetData(PlayerRole::TYPE_MONEY); 
		if (s_CUI_ID_FRAME_BANK.m_UPGTADEMONEY<= dwMoneyOnSelf )
		{
			MsgReqBankTrade msg;
			msg.byOperate = MsgReqBankTrade::eUpgradeBank;
			GetNetworkInput().SendMsg( &msg);
			return TRUE;
		}else
		{
			theUIMessageBox.Show( theTextResManager.GetString( eUI_Bank_LevelUp_error ) );
			return FALSE;
		}
	}
	
	return FALSE;
	
}
// Button
BOOL CUI_ID_FRAME_BANK::ID_BUTTON_UPDATEOnButtonClick( VUCtrlObject* pSender )
{
	
	char szAsk[128] = {0};
	//你升级银行需要 钱，确定升级银行?
	sprintf(szAsk, "%s %d %s", theTextResManager.GetString( eUI_Bank_LevelUp_Text_0 ) , 
		s_CUI_ID_FRAME_BANK.m_UPGTADEMONEY, theTextResManager.GetString(eUI_Bank_LevelUp_Text_1) );
	theUIMessageBox.Show( szAsk, theTextResManager.GetString( eUI_ask ), MB_OKCANCEL, TRUE,
		upgradebank);
	return TRUE;
	
}

// 装载UI
BOOL CUI_ID_FRAME_BANK::LoadUI()
{
	
	GetUIListener().RegisterMe();
	DWORD dwResult = GetVUCtrlManager().AddFrame( UIDOC_PATH( "Bank") );
	if ( dwResult == 0 )
	{
		MESSAGE_BOX("读取文件[UI\\Bank.UI]失败")
			return FALSE;
	}
	else if ( dwResult != 82777/*文件版本号*/ )
	{
		MESSAGE_BOX("读取文件[UI\\Bank.UI]与源代码版本不一样")
	}
	return InitControls();
	
}
// 关连控件
BOOL CUI_ID_FRAME_BANK::InitControls()
{
	
	GetVUCtrlManager().OnFrameRun( ID_FRAME_BANK, s_CUI_ID_FRAME_BANKOnFrameRun );
	GetVUCtrlManager().OnFrameRender( ID_FRAME_BANK, s_CUI_ID_FRAME_BANKOnFrameRender );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_BANK, ID_BUTTON_Deposit, s_CUI_ID_FRAME_BANKID_BUTTON_DepositOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_BANK, ID_BUTTON_Get, s_CUI_ID_FRAME_BANKID_BUTTON_GetOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_BANK, ID_BUTTON_CLOSE, s_CUI_ID_FRAME_BANKID_BUTTON_CLOSEOnButtonClick );
	GetVUCtrlManager().OnButtonClick( ID_FRAME_BANK, ID_BUTTON_UPDATE, s_CUI_ID_FRAME_BANKID_BUTTON_UPDATEOnButtonClick );

	m_pID_FRAME_BANK = (VUCtrlFrame*)GetVUCtrlManager().FindFrame( ID_FRAME_BANK );
	m_pID_TEXT_CURR_COIN = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_TEXT_CURR_COIN );
	m_pID_TEXT_GOLD_MAX = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_TEXT_GOLD_MAX );
	m_pID_BUTTON_Deposit = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_BUTTON_Deposit );
	m_pID_BUTTON_Get = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_BUTTON_Get );
	m_pID_BUTTON_CLOSE = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_BUTTON_CLOSE );
	m_pID_TEXT_CURR_GOLD = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_TEXT_CURR_GOLD );
	m_pID_BUTTON_UPDATE = (VUCtrlButton*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_BUTTON_UPDATE );
	m_pID_TEXT_COIN_MAX = (VUCtrlText*)GetVUCtrlManager().FindControl( ID_FRAME_BANK, ID_TEXT_COIN_MAX );
	assert( m_pID_FRAME_BANK );
	assert( m_pID_TEXT_CURR_COIN );
	assert( m_pID_TEXT_GOLD_MAX );
	assert( m_pID_BUTTON_Deposit );
	assert( m_pID_BUTTON_Get );
	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_TEXT_CURR_GOLD );
	assert( m_pID_BUTTON_UPDATE );
	assert( m_pID_TEXT_COIN_MAX );
	return TRUE;
	
}
// 卸载UI
BOOL CUI_ID_FRAME_BANK::UnLoadUI()
{
	
	GetUIListener().UnregisterMe();
	return GetVUCtrlManager().RemoveFrame( UIDOC_PATH( "Bank") );
		
}
// 是否可视
BOOL CUI_ID_FRAME_BANK::IsVisible()
{
	
	return m_pID_FRAME_BANK->IsVisible();
	
}
// 设置是否可视
void CUI_ID_FRAME_BANK::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_BANK->SetVisible( bVisible );
	
}
void CUI_ID_FRAME_BANK::Refresh()
{
	
	DWORD dwMoneyOnSelf = GetPlayerRole().GetData(PlayerRole::TYPE_MONEY);
	
	//DWORD dwMoneyOnBank = 
	//*m_pID_TEXT_Mount = dwMoneyOnBank;
	//*m_pID_TEXT_MONEY = dwMoney%1000;
	//*m_pID_TEXT_MONEY1 = dwMoney/1000;
	

}
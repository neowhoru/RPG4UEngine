/*////////////////////////////////////////////////////////////////////////
文 件 名：UIBargaining.cpp
创建日期：2007年12月14日
最后更新：2007年12月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIBargaining.h"
#include "UIInventory.h"
#include "UIChatInput.h"
#include "TextResManager.h"
#include "FuncProfiler.h"
#include "UIMessageBox.h"
#include "VUILayoutManager.h"
#include "BargainingListener.h"
#include "VHero.h"
#include "VObjectManager.h"
#include "Hero.h"
#include "GameParameter.h"
#include "ConstTextRes.h"
#include "UIContainHelper.h"
#include "TradeDialog.h"

using namespace vobject;

static gameui::BargainingListener& GetUIListener()
{
	static gameui::BargainingListener s;
	return s;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UIBargaining, ()  , gamemain::eInstPrioClientUI);




namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIBargaining, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIBargaining, OnRender )
	UIPROC_BUTTON_CLICK( theUIBargaining, ID_BUTTON_OKOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIBargaining, ID_BUTTON_CancelOnButtonClick )
	UIPROC_BUTTON_CLICK( theUIBargaining, ID_BUTTON_CompleteOnButtonClick )

	UIPROC_ICON_DROP_TO		(theUIBargaining,ID_LISTIMG_MeOnIconDropTo)
	UIPROC_ICON_LDB_CLICK	(theUIBargaining,ID_LISTIMG_MeOnIconLDBClick)
	UIPROC_ICON_RBUTTON_UP	(theUIBargaining,ID_LISTIMG_MeOnIconRButtonUp)

	UIPROC_ICON_DROP_TO		(theUIBargaining,ID_LISTIMG_PlayerOnIconDropTo)
	UIPROC_ICON_LDB_CLICK	(theUIBargaining,ID_LISTIMG_PlayerOnIconLDBClick)
	UIPROC_ICON_RBUTTON_UP	(theUIBargaining,ID_LISTIMG_PlayerOnIconRButtonUp)

	UIPROC_EDIT_ENTER(theUIBargaining,ID_EDIT_MeOnEditEnter)
};//namespace uicallback
using namespace uicallback;



inline int GetLocalIndex(SLOTINDEX idx)
{
	return idx - SI_TRADE;
}


UIBargaining::UIBargaining()
{
	m_pID_FRAME_Bargaining = NULL;//leo
	m_pID_BUTTON_OK = NULL;
	m_pID_BUTTON_Cancel = NULL;
	m_pID_BUTTON_Complete = NULL;
	m_pID_LISTIMG_Me = NULL;
	m_pID_LISTIMG_Player = NULL;
	m_pID_PICTURE_SelfLock = NULL;
	m_pID_PICTURE_PlayerLock = NULL;
	m_pID_EDIT_Me = NULL;
	m_pID_TEXT_Player = NULL;
	m_pID_TEXT_Caption = NULL;
	//m_pID_EDIT_gold = NULL;
	//m_pID_TEXT_gold = NULL;
	m_pID_TEXT_SelfName = NULL;
	m_pID_TEXT_PlayerName = NULL;

	m_pPlayer				 = NULL;

	m_nPlayerId = -1;

	for(INT n=0; n<MAX_UIBARGAINING_CONTAINER; n++)
		m_Listeners[n].SetContainerIndex(n);
}



void UIBargaining::SetCanbeChange( BOOL /*b*/ )
{

}

void UIBargaining::Clear()
{
	
	//m_pID_BUTTON_OK->SetEnable( TRUE );
	//m_pID_BUTTON_Complete->SetEnable( TRUE );
	*m_pID_EDIT_Me = 0;
	//*m_pID_EDIT_gold = 0;
	m_pID_EDIT_Me->SetReadOnly( FALSE );
	//m_pID_EDIT_gold->SetReadOnly( FALSE );
	m_pID_PICTURE_SelfLock->SetVisible( FALSE );
	m_pID_PICTURE_PlayerLock->SetVisible( FALSE );
	*m_pID_TEXT_Player = 0;
	//*m_pID_TEXT_gold = 0;
	*m_pID_TEXT_Caption = "";
	m_pID_LISTIMG_Me->Clear();
	m_pID_LISTIMG_Player->Clear();
	m_bOnClose = FALSE;
	
}

// Frame
BOOL UIBargaining::OnFrameMove(DWORD /*dwTick*/)
{
	
//#ifdef USE_DISABLE
	if (!m_bOnClose && IsVisible() )
	{
		Object* pObject;
		pObject = theObjectManager.GetObject( m_nPlayerId );

		if ( !pObject || !pObject->IsPlayer() )
		{
			ID_BUTTON_CancelOnButtonClick( NULL );
			OUTPUTTIP(TEXTRES_TRADE_PLAYER2_NOT_EXIST);
			return FALSE;
		}

		Player* pPlayer = (Player*)pObject;

		//


		Vector3D vDisc = theHero.GetPosition() - pPlayer->GetPosition();

		const float fDist = theGeneralGameParam.GetDistanceTrading();
		if( vDisc.Length2() > fDist * fDist )
		{
			ID_BUTTON_CancelOnButtonClick( NULL );
			OUTPUTTIP(TEXTRES_TRADE_TOOFAR);
			return FALSE;
		}
	}
//#endif
	return TRUE;
}

BOOL UIBargaining::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}

// Button
BOOL UIBargaining::ID_BUTTON_OKOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	MONEY money = (MONEY)_atoi64( m_pID_EDIT_Me->GetText() );
	if(money > theHero.GetMoney())
	{
		OUTPUTTIP(TEXTRES_TRADE_PLAYER1_HAVENOTMONEY);
		return FALSE;
	}

	if(money != theTradeDialog.GetMoney1())
	{
		theTradeDialog.PutMoney(money);
		return FALSE;
	}

	theTradeDialog.Proposal();


	return TRUE;
	
}

// Button
BOOL UIBargaining::ID_BUTTON_CancelOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_bOnClose = TRUE;
	theTradeDialog.CancelTrade();
	//SetVisible( FALSE );
	return TRUE;
	
}
void UIBargaining::SendCancelMsg()
{
	
	
}
// Button
BOOL UIBargaining::ID_BUTTON_CompleteOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_pID_BUTTON_Complete->SetEnable(FALSE);
	theTradeDialog.Accept();

	//SetCanbeChange( FALSE );
	return TRUE;
	
}


// ListImg / ListEx
BOOL UIBargaining::ID_LISTIMG_MeOnIconDropTo(VUCtrlObject*    /*pSender*/
                                                      ,VUCtrlObject*    /*pThisCtrl*/
																		,IconDragListImg* /*pItemDrag*/
																		,IconDragListImg* /*pItemDest*/)
{
	
	return FALSE;
	
}
BOOL UIBargaining::ID_LISTIMG_MeOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}
BOOL UIBargaining::ID_LISTIMG_MeOnIconRButtonUp(VUCtrlObject*    /*pSender*/
                                               ,IconDragListImg* pItem)
{
	SLOTPOS pos;
	pos = (SLOTPOS)m_pID_LISTIMG_Me->GetItemIndex(pItem);
	theTradeDialog.GetItem(pos);
	return TRUE;
	
}
// ListImg / ListEx
BOOL UIBargaining::ID_LISTIMG_PlayerOnIconDropTo(VUCtrlObject*    /*pSender*/
                                                          ,VUCtrlObject*    /*pThisCtrl*/
																			 ,IconDragListImg* /*pItemDrag*/
																			 ,IconDragListImg* /*pItemDest*/)
{
	
	return FALSE;
	
}
BOOL UIBargaining::ID_LISTIMG_PlayerOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}
BOOL UIBargaining::ID_LISTIMG_PlayerOnIconRButtonUp( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}
// Edit
void UIBargaining::ID_EDIT_MeOnEditEnter( VUCtrlObject* /*pSender*/, LPCSTR  szData )
{
	MONEY money = (MONEY)_atoi64(szData);

	if(money > theHero.GetMoney())
	{
		OUTPUTTIP(TEXTRES_TRADE_PLAYER1_HAVENOTMONEY);
		OUTPUTTOP(TEXTRES_TRADE_PLAYER1_HAVENOTMONEY);
		return;
	}

	if(money == 0)
		return;

	theTradeDialog.PutMoney(money);
}

// 装载UI
BOOL UIBargaining::LoadUI()
{
	
	GetUIListener().RegisterMe();
	m_pID_FRAME_Bargaining = theUICtrlManager.LoadFrame( UIDOC_PATH( "Bargaining") );
	if ( m_pID_FRAME_Bargaining == 0 )
	{
		UIMessageLog("读取文件[UI\\Bargaining.UI]失败")
		return FALSE;
	}
	m_pID_FRAME_Bargaining->SetVisible(FALSE);
	theUILayoutManager.AddFrame( m_pID_FRAME_Bargaining );

	return InitControls();
	
}
// 关连控件
BOOL UIBargaining::InitControls()
{
	m_pID_BUTTON_OK			= (VUCtrlButton*)m_pID_FRAME_Bargaining->FindControl(  ID_BUTTON_OK );
	m_pID_BUTTON_Cancel		= (VUCtrlButton*)m_pID_FRAME_Bargaining->FindControl(  ID_BUTTON_Cancel );
	m_pID_BUTTON_Complete	= (VUCtrlButton*)m_pID_FRAME_Bargaining->FindControl(  ID_BUTTON_Complete );
	m_pID_LISTIMG_Me			= (VUCtrlListImg*)m_pID_FRAME_Bargaining->FindControl(  ID_LISTIMG_Me );
	m_pID_LISTIMG_Player		= (VUCtrlListImg*)m_pID_FRAME_Bargaining->FindControl(  ID_LISTIMG_Player );
	m_pID_EDIT_Me				= (VUCtrlEdit*)m_pID_FRAME_Bargaining->FindControl(  ID_EDIT_Me );
	m_pID_TEXT_Player			= (VUCtrlText*)m_pID_FRAME_Bargaining->FindControl(  ID_TEXT_Player );
	m_pID_TEXT_Caption		= (VUCtrlText*)m_pID_FRAME_Bargaining->FindControl(  ID_TEXT_Caption );
	//m_pID_EDIT_gold			= (VUCtrlEdit*)m_pID_FRAME_Bargaining->FindControl(  ID_EDIT_gold );
	//m_pID_TEXT_gold			= (VUCtrlText*)m_pID_FRAME_Bargaining->FindControl(  ID_TEXT_gold );
	m_pID_PICTURE_SelfLock	= (VUCtrlPicture*)m_pID_FRAME_Bargaining->FindControl(  ID_PICTURE_SelfLock );
	m_pID_PICTURE_PlayerLock= (VUCtrlPicture*)m_pID_FRAME_Bargaining->FindControl(  ID_PICTURE_PlayerLock );
	m_pID_TEXT_SelfName		= (VUCtrlText*)m_pID_FRAME_Bargaining->FindControl(  ID_TEXT_SelfName );
	m_pID_TEXT_PlayerName	= (VUCtrlText*)m_pID_FRAME_Bargaining->FindControl(  ID_TEXT_PlayerName );

	assert( m_pID_BUTTON_OK );
	assert( m_pID_BUTTON_Cancel );
	assert( m_pID_BUTTON_Complete );
	assert( m_pID_LISTIMG_Me );
	assert( m_pID_LISTIMG_Player );
	assert( m_pID_EDIT_Me );
	assert( m_pID_TEXT_Player );
	assert( m_pID_TEXT_Caption );
	//assert( m_pID_EDIT_gold );
	//assert( m_pID_TEXT_gold );
	assert( m_pID_PICTURE_SelfLock );
	assert( m_pID_PICTURE_PlayerLock );
	assert( m_pID_TEXT_SelfName );
	assert( m_pID_TEXT_PlayerName );
	
	m_pID_FRAME_Bargaining->SetProcOnFrameMove(  theUIBargainingOnFrameMove );
	m_pID_FRAME_Bargaining->SetProcOnRender(  theUIBargainingOnRender );

	m_pID_BUTTON_OK			->SetProcOnButtonClick		(  theUIBargainingID_BUTTON_OKOnButtonClick );
	m_pID_BUTTON_Cancel		->SetProcOnButtonClick		(  theUIBargainingID_BUTTON_CancelOnButtonClick );
	m_pID_BUTTON_Complete	->SetProcOnButtonClick		(  theUIBargainingID_BUTTON_CompleteOnButtonClick );
	m_pID_LISTIMG_Me			->SetProcOnDropTo				(  theUIBargainingID_LISTIMG_MeOnIconDropTo );
	m_pID_LISTIMG_Me			->SetProcOnLButtonDBClick	(  theUIBargainingID_LISTIMG_MeOnIconLDBClick );
	m_pID_LISTIMG_Me			->SetProcOnRButtonUp			( theUIBargainingID_LISTIMG_MeOnIconRButtonUp );
	m_pID_LISTIMG_Player		->SetProcOnDropTo				( theUIBargainingID_LISTIMG_PlayerOnIconDropTo );
	m_pID_LISTIMG_Player		->SetProcOnLButtonDBClick	(  theUIBargainingID_LISTIMG_PlayerOnIconLDBClick );
	m_pID_LISTIMG_Player		->SetProcOnRButtonUp			(  theUIBargainingID_LISTIMG_PlayerOnIconRButtonUp );
	m_pID_EDIT_Me				->SetProcOnEnter				(  theUIBargainingID_EDIT_MeOnEditEnter );


	m_pID_EDIT_Me->SetIsNumber(TRUE);
	//m_pID_EDIT_Me->SetMaxLength( 3 );
	//
	Clear();
	UISCRIPT_ENABLE( UIOBJ_TRADE, m_pID_FRAME_Bargaining );
	return TRUE;
	
}
// 卸载UI
BOOL UIBargaining::UnLoadUI()
{
	
	UISCRIPT_DISABLE( UIOBJ_TRADE );
	GetUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Bargaining") );
	
}
// 是否可视
BOOL UIBargaining::IsVisible()
{
	
	return m_pID_FRAME_Bargaining->IsVisible();
	
}
// 设置是否可视
void UIBargaining::SetVisible( BOOL bVisible )
{
	
	if ( bVisible == FALSE )
	{

		Clear();
	}

	if( bVisible != IsVisible() )
		theUILayoutManager.SetVisible(m_pID_FRAME_Bargaining);
	else
		m_pID_FRAME_Bargaining->SetVisible( bVisible );
	
}


void UIBargaining::Refresh()
{
	Player*	pPlayer;
	pPlayer = theTradeDialog.GetPlayerTrading();

	/////////////////////////////////////
	if(pPlayer)
	{

		SetPlayer(pPlayer->GetName(), pPlayer->GetObjectKey());
	}
	else
	{
		SetPlayer(_T(""), 0);
	}

	/////////////////////////////////////
	*m_pID_TEXT_Player	= FMSTR(_T("%I64u"),theTradeDialog.GetMoney2());



	/////////////////////////////////////
	//挡板处理
	m_pID_PICTURE_SelfLock	->SetVisible( theTradeDialog.IsLockTrade1() );
	m_pID_PICTURE_PlayerLock->SetVisible( theTradeDialog.IsLockTrade2() );

	///////////////////////////////////
	if(	theTradeDialog.GetStatus1() >= TRADE_STATUS_PROPOSAL)
	{
		m_pID_PICTURE_SelfLock	->SetVisible( TRUE );
	}

	///////////////////////////////////
	if(	theTradeDialog.GetStatus2() >= TRADE_STATUS_PROPOSAL)
	{
		m_pID_PICTURE_PlayerLock->SetVisible( TRUE );
	}



	///////////////////////////////////
	//按钮处理
	//1.双方确认后，方可显示成交
	DWORD	dwStatus1	= theTradeDialog.GetStatus1();
	DWORD	dwStatus2	= theTradeDialog.GetStatus2();

	///////////////////////////////////
	if(dwStatus1 == TRADE_STATUS_PROPOSAL)
	{
		if(dwStatus2 >= TRADE_STATUS_PROPOSAL)
		{
			m_pID_BUTTON_OK		->SetVisible( FALSE );
			m_pID_BUTTON_Complete->SetVisible( TRUE );
			m_pID_BUTTON_Complete->SetEnable	( TRUE );
		}
		else
		{
			m_pID_BUTTON_OK		->SetVisible( FALSE );
			m_pID_BUTTON_Complete->SetVisible( FALSE );
		}
	}

	///////////////////////////////////
	else if(dwStatus1 == TRADE_STATUS_ACCEPT)
	{
		if(dwStatus2 >= TRADE_STATUS_PROPOSAL)
		{
			m_pID_BUTTON_OK		->SetVisible( FALSE );
			m_pID_BUTTON_Complete->SetVisible( FALSE );
		}
		else
		{
			m_pID_BUTTON_OK		->SetVisible( FALSE );
			m_pID_BUTTON_Complete->SetVisible( TRUE );
			m_pID_BUTTON_Complete->SetEnable	( TRUE );
		}
	}
	///////////////////////////////////
	else
	{
		m_pID_BUTTON_OK		->SetVisible( TRUE );
		m_pID_BUTTON_Complete->SetVisible( FALSE );
	}


}




void UIBargaining::SetPlayer( LPCSTR  szPlayerName, int nID )
{
	assert( szPlayerName );
	m_nPlayerId = nID;

	if(m_nPlayerId)
	{
		LPCTSTR	szStatus;

		////////////////////////////////////
		szStatus = _STRING(theTradeDialog.GetStatus2() + TEXTRES_TRADE_STATE_TRADING);

		*m_pID_TEXT_PlayerName	= FMSTR	(_T("%s[%s]")
													,szPlayerName
													,szStatus);

		////////////////////////////////////
		szStatus = _STRING(theTradeDialog.GetStatus1() + TEXTRES_TRADE_STATE_TRADING);

		*m_pID_TEXT_SelfName		= FMSTR	(_T("%s[%s]")
													,theHero.GetName()
													,szStatus);
	}
	else
	{
		*m_pID_TEXT_PlayerName	= szPlayerName;
		*m_pID_TEXT_SelfName		= theHero.GetName();
	}
	
}



void UIBargaining::AttachToItemCompound()
{
	theHero.InstallSlotListener(SI_TRADE,	&m_Listeners[0]);
	theHero.InstallSlotListener(SI_TRADE2,	&m_Listeners[1]);
}

void UIBargaining::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		int slotType = GetLocalIndex(pContainer->GetSlotIndex());
		if(m_arContainers[slotType])
		{
			m_arContainers[slotType]->SetSlotContainer(pContainer);
		}
	}
}


void UIBargaining::PushGoods(BaseSlot* pSlot,INT nContainerIndex)
{
	assert(m_pPlayer && pSlot);

	int nSlotType = nContainerIndex;//GetLocalIndex(pSlot->GetSlotIndex());

	if(m_arContainers[nSlotType] == NULL)
		return;

	UIContainHelper	helper(m_arContainers[nSlotType]
									,m_pPlayer
									,MAX_TRADE_SLOT_NUM);
	helper.PushItemEx(*pSlot, FALSE);
}


void UIBargaining::PopGoods( BaseSlot* pSlot,INT nContainerIndex )
{
	int nSlotType = nContainerIndex;//GetLocalIndex(pSlot->GetSlotIndex());
	//int nSlotType = GetLocalIndex(pSlot->GetSlotIndex());

	if(m_arContainers[nSlotType] == NULL)
		return;

	UIContainHelper	helper(m_arContainers[nSlotType]
									,m_pPlayer
									,MAX_TRADE_SLOT_NUM);
	helper.PopItem(*pSlot);
}

void UIBargaining::SetGoodsState( BaseSlot* pSlot,DWORD dwState,INT nContainerIndex )
{
	int nSlotType = nContainerIndex;//GetLocalIndex(pSlot->GetSlotIndex());
	//int nSlotType = GetLocalIndex(pSlot->GetSlotIndex());

	if(m_arContainers[nSlotType] == NULL)
		return;

	UIContainHelper	helper(m_arContainers[nSlotType]
									,m_pPlayer
									,MAX_TRADE_SLOT_NUM);
	helper.SetItemState(*pSlot, dwState);
}

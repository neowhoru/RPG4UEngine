/*////////////////////////////////////////////////////////////////////////
文 件 名：NpcDialogUIListener.cpp
创建日期：2006年2月1日
最后更新：2006年2月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NpcDialogUIListener.h"
#include "UINpcDialog.h"
#include "ScriptWord.h"
#include "LogSystem.h"
#include "UIInclude.h"
#include "Hero.h"
#include "ObjectManager.h"
#include "HeroActionInput.h"
#include "MapNPC.h"
#include "QuestManager.h"
#include "TextResManager.h"
#include "ConstTextRes.h"
#include "ItemLogManager.h"
#include "HeroQuestManager.h"

using namespace object;
namespace gameui
{
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NpcDialogUIListener::NpcDialogUIListener()
{
}
NpcDialogUIListener::~NpcDialogUIListener()
{
}
LPCSTR NpcDialogUIListener::GetName()
{
	return "NpcDialogUIListener";
}

EGameUIType NpcDialogUIListener::GetType()
{
	return eUINpcChatDialogBox;
}

BOOL NpcDialogUIListener::GetData(DWORD dwType,void* pRet)
{
	switch(dwType)
	{
	case eGetNpcId:
		*(GameCharID*)pRet = (GameCharID)theUINpcDialog.m_nNpcId;
		break;
	case eGetFontIndex:
		*(int*)pRet = theUINpcDialog.m_pID_TEXT_Info->GetFontIndex();
		break;
	//case :
	//	break;
	}
	return TRUE;
}

void NpcDialogUIListener::SetData(DWORD dwType,LPARAM lpData)
{
	switch(dwType)
	{
	case eSetNpcID:
		theUINpcDialog.SetNpc( (UINT) lpData);
		break;
	case eSetVisible:
		theUINpcDialog.SetVisible( (BOOL) lpData);
		break;
	}
}

void NpcDialogUIListener::TriggerFunc(ETriggerData eData,LPARAM lpData)
{
	switch(eData)
	{
	case eToggleDialogText:
		theUINpcDialog.ShowDialog	((LPCSTR)lpData);
		break;

	case eShowDialogNext:
		theUINpcDialog.ShowNext	((BOOL)lpData);
		break;
	case eShowQuestionText:
		ShowQuestionDialog	((LPCSTR)lpData);
		break;

	case eShowQuestList:
		ShowQuestList	((LPCSTR)lpData);
		break;
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
 static BOOL ScriptSellectCallbackFun( const int nSelectIndex, void * /*pData*/ )
{

	if(singleton::ExistHero())
	{
		//Object*	pObject;
		DWORD		targetID;

		targetID	= theHeroActionInput.GetCurrentTarget();
		//SetButtonEnable( FALSE );
		theItemLogManager.ClearLog();
		theItemLogManager.SetEnableLog(TRUE);

		theHero.RunScript(nSelectIndex,targetID );

		theUISelectBox.SetVisible(FALSE);

	}

	return TRUE;
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

inline BOOL IsHex(BYTE c)
{
	return (c <='9' && c >= '0') || c >= 'a' && c<='f' || c >= 'A' && c<='F';
}

void NpcDialogUIListener::ShowQuestionDialog(LPCSTR szQuestion)
{
	ScriptWord words;
	if( words.Parse(szQuestion ) == 0 )
	{
		assert( FALSE && "npc choise count is 0" );
		return;
	}

	if( words.GetWordCount() > 1 )
	{
		DWORD						dwColor;
		int						nItemCount	= words.GetWordCount();
		sUILIST_ITEM *	pstList		= new sUILIST_ITEM[nItemCount];

		for ( int n=0; n<nItemCount-1; n++ )
		{
			LPCSTR	szText; 
			LPCSTR	szLabel;

			szText	= words.GetWord(n);
			szLabel	= szText;
			dwColor	= 0;
			if(szText[0] == '/' && (szText[1] == 'x' || szText[1] == 'X'))
			{
				char	szColor[16]={0};
				strncpy	(szColor, szText+2,8);
				if(IsHex(szColor[7]))
				{
					sscanf	( szColor, "%x", &dwColor );
					szLabel	= szText + strlen(szColor)+2;
				}
			}
			pstList[n].SetData(szLabel , n ,NULL,dwColor);
		}

		theUINpcDialog.SetVisible( FALSE );
		theUISelectBox.Show	(words.GetWord(nItemCount-1)
                           ,pstList
									,nItemCount-1
									,theUITarget.GetName()
									,TRUE
									,ScriptSellectCallbackFun);
		delete[] pstList;

		//theItemLogManager.EnableLog(FALSE);

	}

}



BOOL ScriptShowQuestDlgCallbackFun( const int nSelectIndex, void *pData )
{
	sUILIST_ITEM *pItem = (sUILIST_ITEM*)pData;

	//theItemLogManager.EnableLog(TRUE);
	theItemLogManager.ClearLog();
	theHero.RunQuestScript(pItem[nSelectIndex].m_nID, 0);
	theUISelectBox.SetVisible(FALSE);

	return TRUE;
}


const int			MAX_LISTITEM_SIZE = 128;

class ShowQuestOnInvisit : public QuestOnInvisit
{
public:
	int						nItemDoneCount;
	int 						nItemNewCount;
	int 						nDoneQuest;
	int 						nNewQuest;
	sUILIST_ITEM	arItemDone	[MAX_LISTITEM_SIZE];
	sUILIST_ITEM	arItemNew	[MAX_LISTITEM_SIZE];
public:
	ShowQuestOnInvisit()
	{
	//arItemDone	[MAX_LISTITEM_SIZE];
	//arItemNew		[MAX_LISTITEM_SIZE];
		nItemDoneCount = 0;
		nItemNewCount	= 0;
		nDoneQuest		= -1;
		nNewQuest		= -1;
	}

	BOOL OnQuestCast	(sQUEST_INFO* pQuest,BOOL bLevelFit)
	{
		if ( bLevelFit )
		{
			nNewQuest = pQuest->m_QuestID;
			arItemNew[nItemNewCount++].SetData	(pQuest->m_Name
                                             ,pQuest->m_QuestID
															,NULL
															,0xffffffff);
		}
		return( nItemNewCount < MAX_LISTITEM_SIZE );
	}
	///////////////////////////////////////////////////////
	BOOL OnQuestEnd	(sQUEST_INFO* pQuest,BOOL bFinished)
	{
		if ( bFinished )
		{
			nDoneQuest = pQuest->m_QuestID;
			return FALSE;
		}
		arItemDone[nItemDoneCount++].SetData	(pQuest->m_Name
                                             ,pQuest->m_QuestID
															,NULL
															,0xff00ff00);
		return( nItemDoneCount < MAX_LISTITEM_SIZE );
	}
};


void NpcDialogUIListener::ShowQuestList(LPCSTR szText)
{
	theUINpcDialog.SetVisible( FALSE );

	//MapNPC*		pNpc;
	DWORD		npcID(0);

	if(theHero.GetScriptChar())
		npcID	= theHero.GetScriptChar()->GetTargetNpcID();

	//pNpc	= (MapNPC*)theObjectManager.GetObject(npcID);
	//__CHECK2(pNpc && pNpc->IsKindOfObject(NPC_OBJECT),;);



	DWORD						dwResID;
	ShowQuestOnInvisit	questOpr;

	///生成“取消”项
	dwResID	= TEXTRES_QUEST_CANCEL_0 + rand()%(TEXTRES_QUEST_CANCEL_9 - TEXTRES_QUEST_CANCEL_0);
	questOpr.arItemDone[questOpr.nItemDoneCount++].SetData	(_STRING(dwResID)
																				,0
																				,NULL
																				,0xff7f7f7f);


	///历遍所有Quest
	theHeroQuestManager.ForEachQuest(npcID,questOpr);


	/////////////////////////////////////////////////////
	//任务信息更新到UI
	int nItemCount = questOpr.nItemDoneCount + questOpr.nItemNewCount;


	// 有可完成任务或没有相关任务
	if ( questOpr.nDoneQuest != -1 || nItemCount == 1 )
	{
		theHero.RunQuestScript(questOpr.nDoneQuest, 0);
		LOGINFO("有可完成任务或相关任务:%d\n", questOpr.nDoneQuest );
	}
	// 有且只有一个新任务
	//else if( nNewQuest != -1 && nItemNewCount == 1 )
	//{
	//	theHero.RunQuestScript(arItemNew[0].m_nID, 0);
	//	LOGINFO("有一个新任务:%d\n", arItemNew[0].m_nID );
	//}
	// 有多个任务
	else if ( nItemCount >= 2 )
	{
		sUILIST_ITEM stItems[MAX_LISTITEM_SIZE*2];
		int nIndex = 0;
		for ( int n=0; n<questOpr.nItemDoneCount; n++ )
		{
			stItems[nIndex++] = questOpr.arItemDone[n];
			LOGINFO("可完成任务:%d\n", questOpr.arItemDone[n].m_nID );
		}
		for ( int n=0; n<questOpr.nItemNewCount; n++ )
		{
			stItems[nIndex++] = questOpr.arItemNew[n];
			LOGINFO("新任务:%d\n", questOpr.arItemNew[n].m_nID );
		}

		//theUISelectBox.Show	(words.GetWord(nItemCount-1)
  //                         ,pstList
		//							,nItemCount
		//							,theUITarget.GetName()
		//							,TRUE
		//							,ScriptSellectCallbackFun);

		//theGameUIManager.UIShowSelectBox(gameui::eUISelectBox,
		theUISelectBox.Show	(szText
                           ,stItems
									,nItemCount
									,_STRING( TEXTRES_Ask_Select )
									,TRUE
									,ScriptShowQuestDlgCallbackFun
									,stItems
									,sizeof(stItems));
	}
	else
	{
		theHero.RunQuestScript((CODETYPE)-1, 0);
		LOGINFO("没有任务信息\n" );
	}
}



};//namespace gameui


/*////////////////////////////////////////////////////////////////////////
文 件 名：UILoadMap.cpp
创建日期：2007年9月2日
最后更新：2007年9月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UILoadMap.h"
#include "GraphicSetting.h"
#include "LogSystem.h"
#include "VUICursorManager.h"
#include "V3DGameWorld.h"
#include "VUIObjectManager.h"
#include "ClientMain.h"
#include "SoundLayer.h"
#include "LoadMapUIListener.h"
#include "ApplicationSetting.h"
#include "GlobalInstancePriority.h"
#include "MediaPathManager.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UILoadMap, ()  , gamemain::eInstPrioClientUI);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::LoadMapUIListener& GetUIListener()
{
	static gameui::LoadMapUIListener staticLoadMapUIListener;
	return staticLoadMapUIListener;
}



namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE     ( theUILoadMap, OnFrameMove )
	UIPROC_FRAME_RENDER			( theUILoadMap, OnRender )
};//namespace uicallback
using namespace uicallback;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UILoadMap::UILoadMap()
{
	// Member
	m_pID_FRAME_LoadMap	= NULL;
	m_pID_PROGRESS_LOAD	= NULL;
	m_pID_PICTURE_BG		= NULL;

}

void	UILoadMap::SetProgress( float fValue )
{
	m_pID_PROGRESS_LOAD->SetValue( fValue );
}

// Frame
BOOL UILoadMap::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
	
}

BOOL UILoadMap::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}


///////////////////////////////////////////////
// 装载UI
BOOL UILoadMap::LoadUI()
{
	
	GetUIListener().RegisterMe();

	m_pID_FRAME_LoadMap = theUICtrlManager.LoadFrame( UIDOC_PATH( "LoadMap") );
	if ( m_pID_FRAME_LoadMap == 0 )
	{
		UIMessageLog("读取文件[UI\\LoadMap.UI]失败")
		return FALSE;
	}

	return InitControls();
	
}

// 关连控件
BOOL UILoadMap::InitControls()
{
	m_pID_FRAME_LoadMap->SetProcOnFrameMove(theUILoadMapOnFrameMove);
	m_pID_FRAME_LoadMap->SetProcOnFrameMove(theUILoadMapOnRender);
	

	m_pID_PROGRESS_LOAD	= (VUCtrlProgress*)m_pID_FRAME_LoadMap->FindControl(ID_PROGRESS_LOAD );
	m_pID_PICTURE_BG		= (VUCtrlPicture*)m_pID_FRAME_LoadMap->FindControl	(ID_PICTURE_BG);

	assert( m_pID_PROGRESS_LOAD );
	assert( m_pID_PICTURE_BG );

	///////////////////////////////////////////
	int nRand = rand() % theApplicationSetting.m_nLoadmapBackPicNum;
	if(nRand)
	{
		StringHandle	sPath;
		sPath.Format("%sLoadMap\\LoadMap%d.tga",_MEDIAPATH(ePATH_UIDOC),nRand);
		m_pID_FRAME_LoadMap->SetPicInfo( sPath);
	}

	///////////////////////////////////////////
	m_pID_FRAME_LoadMap->SetWidth( SCREEN_WIDTH );
	m_pID_FRAME_LoadMap->SetHeight( SCREEN_HEIGHT );

	//UISCRIPT_ENABLE( UIOBJ_LOADMAP, m_pID_FRAME_LoadMap );
	
	return TRUE;
	
}
// 卸载UI
BOOL UILoadMap::UnLoadUI()
{
	
	//UISCRIPT_DISABLE( UIOBJ_LOADMAP );
	GetUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "LoadMap") );
	
}

// 是否可视
BOOL UILoadMap::IsVisible()
{
	return m_pID_FRAME_LoadMap->IsVisible();
}

// 设置是否可视
void UILoadMap::SetVisible( BOOL bVisible )
{
	
	if( m_pID_FRAME_LoadMap )
	{
		m_pID_FRAME_LoadMap->SetVisible( bVisible/*, !bVisible*/ );
		if( bVisible == TRUE )
		{
			theUICtrlManager.BringToTop( m_pID_FRAME_LoadMap );
		}
		else
		{
			theUICtrlManager.BringToBottom( m_pID_FRAME_LoadMap );
		}
		theUICtrlManager.UpdateControls();
	}
	
}




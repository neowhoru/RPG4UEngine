/*////////////////////////////////////////////////////////////////////////
文 件 名：UIShop.h
创建日期：2008年6月8日
最后更新：2008年6月8日
编 写 者：亦哥(Leo/李亦)
       liease@163.com
		 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////


#include "stdafx.h"
#include "UIShop.h"
#include "VUCtrlManager.h"
#include "VUILayoutManager.h"
#include "ShopUIListener.h"
#include "BaseContainer.h"
#include "Hero.h"
#include "ItemManager.h"
#include "ShopDialog.h"
#include "UIShopSlotUIListener.h"
#include "ShopDialog.h"
#include "UIInventory.h"
#include "VUCtrlIconDragManager.h"
#include "UIContainHelper.h"

//#include "UIShop_CallBack.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UIShop, ()  , gamemain::eInstPrioGameFunc);

gameui::ShopUIListener& GetUIShopShopUIListener()
{
static gameui::ShopUIListener staticShopUIListener;
return staticShopUIListener;
}

namespace callback
{
	UIPROC_FRAME_FRAMEMOVE	( theUIShop, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIShop, OnRender )
	UIPROC_BUTTON_CLICK		( theUIShop, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIShop, ID_LISTIMG_SHOPOnIconDragOn )
	UIPROC_ICON_LDB_CLICK	( theUIShop, ID_LISTIMG_SHOPOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIShop, ID_LISTIMG_SHOPOnIconRButtonUp )
	UIPROC_CHECK_BOX_CHECK	( theUIShop, PageOnCheckBoxCheck )

	UIPROC_CHECK_BOX_CHECK	( theUIShop, MethodOnCheckBoxCheck )
	UIPROC_BUTTON_CLICK		( theUIShop, ID_BUTTON_REPAIRALLOnButtonClick )
};

using namespace callback;

UIShop::UIShop()
{
	m_pPlayer					= NULL;

	m_pID_FRAME_SHOP			= NULL;
	m_pID_BUTTON_CLOSE		= NULL;
	m_pID_TEXT_MONEY			= NULL;
	m_pID_LISTIMG_SHOP		= NULL;
	m_pID_BUTTON_REPAIRALL	= NULL;

	m_nCurrentPage				= 0;
	m_nCurrentMethod			= SHOP_METHOD_NONE;

	__ZERO(m_arCheckBoxPtrs);
	__ZERO(m_arMethodPtrs);

}


// Frame
BOOL UIShop::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}
BOOL UIShop::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}
// Button
BOOL UIShop::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	theShopDialog.CloseWindow();
	return TRUE;
}


// ListImg / ListEx
BOOL UIShop::ID_LISTIMG_SHOPOnIconDragOn(VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/, IconDragListImg* /*pItemDrag*/, IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
}

BOOL UIShop::ID_LISTIMG_SHOPOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	return FALSE;
}

BOOL UIShop::ID_LISTIMG_SHOPOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* /*pItem*/ )
{
	if(pSender == m_pID_LISTIMG_SHOP)
	{
		//INT nIndex = m_pID_LISTIMG_SHOP->GetItemIndex(pItem);

		if(m_nCurrentMethod == SHOP_METHOD_NONE)
			ChangeMethod(SHOP_METHOD_BUY);
		else
			ChangeMethod(SHOP_METHOD_NONE);

		//theMouseHandler.DoItemHandling	(SI_NPCSHOP
		//											,m_nCurrentPage*MAX_SLOT_PER_SHOPPAGE + nIndex
		//											,NULL);
	}

	return FALSE;
}





BOOL UIShop::ID_BUTTON_REPAIRALLOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	ShopDialog*	pShop = (ShopDialog*)theUIShopSlotUIListener.GetContainer();
	if(pShop)
		pShop->RepairAll();
	return TRUE;
}


void UIShop::PageOnCheckBoxCheck ( VUCtrlObject* pSender,  BOOL * /*bChecked*/ )
{
	if(pSender->GetGroupID() == SHOP_PAGE_GROUP_ID)
	{
		ChangePage(pSender->GetIndexAtGroup());
	}
}

void UIShop::MethodOnCheckBoxCheck ( VUCtrlObject* pSender,  BOOL * /*bChecked*/ )
{
	if(pSender->GetGroupID() == METHOD_PAGE_GROUP_ID)
	{
		ChangeMethod((eSHOP_METHOD)pSender->GetIndexAtGroup());
	}
}


// 装载UI
BOOL UIShop::LoadUI()
{
	GetUIShopShopUIListener().RegisterMe();
	m_pID_FRAME_SHOP = theUICtrlManager.LoadFrame( UIDOC_PATH( "Shop") );
	if ( m_pID_FRAME_SHOP == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Shop") "]失败")
			return FALSE;
	}

	theUILayoutManager.AddFrame(m_pID_FRAME_SHOP);
	m_pID_FRAME_SHOP->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UIShop::InitControls()
{
	int n;

	m_pID_FRAME_SHOP->SetProcOnFrameMove(  theUIShopOnFrameMove );
	m_pID_FRAME_SHOP->SetProcOnRender	(  theUIShopOnRender, FALSE );

	m_pID_BUTTON_CLOSE		= (VUCtrlButton*)	m_pID_FRAME_SHOP->FindControl(ID_BUTTON_CLOSE );
	m_pID_TEXT_MONEY			= (VUCtrlText*)	m_pID_FRAME_SHOP->FindControl(ID_TEXT_MONEY );
	m_pID_LISTIMG_SHOP		= (VUCtrlListImg*)m_pID_FRAME_SHOP->FindControl(ID_LISTIMG_SHOP );

	m_pID_BUTTON_REPAIRALL	= (VUCtrlButton*)	m_pID_FRAME_SHOP->FindControl(ID_BUTTON_REPAIRALL );

	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_TEXT_MONEY );
	assert( m_pID_LISTIMG_SHOP );
	assert( m_pID_BUTTON_REPAIRALL );

	m_pID_BUTTON_CLOSE		->SetProcOnButtonClick		(theUIShopID_BUTTON_CLOSEOnButtonClick );
	m_pID_LISTIMG_SHOP		->SetProcOnDropTo				(theUIShopID_LISTIMG_SHOPOnIconDragOn );
	m_pID_LISTIMG_SHOP		->SetProcOnLButtonDBClick	(theUIShopID_LISTIMG_SHOPOnIconLDBClick );
	m_pID_LISTIMG_SHOP		->SetProcOnRButtonUp			(theUIShopID_LISTIMG_SHOPOnIconRButtonUp );
	m_pID_BUTTON_REPAIRALL	->SetProcOnButtonClick		(theUIShopID_BUTTON_REPAIRALLOnButtonClick );


	///分页控件
	for(n=0; n<MAX_SHOP_PAGE_NUM; n++)
	{
		LPCSTR szName = FMSTR(ID_CHECKBOX_PAGE "%d", n+1);
		m_arCheckBoxPtrs[n]	= (VUCtrlCheckBox*)	m_pID_FRAME_SHOP->FindControl( szName);
		assert( m_arCheckBoxPtrs[n] );

		m_arCheckBoxPtrs[n]->SetProcOnCheck(theUIShopPageOnCheckBoxCheck);
		m_arCheckBoxPtrs[n]->SetCheck(n==0);
		m_arCheckBoxPtrs[n]->SetGroupID		(SHOP_PAGE_GROUP_ID);
		m_arCheckBoxPtrs[n]->SetIndexAtGroup(n);
	}



	///操作方法
	static LPCSTR szMethods[]=
	{
		 ID_CHECKBOX_BUY
		,ID_CHECKBOX_SELL
		,ID_CHECKBOX_REPAIR
	};
	for(n=0; n<SHOP_METHOD_MAX; n++)
	{
		m_arMethodPtrs[n]	= (VUCtrlCheckBox*)	m_pID_FRAME_SHOP->FindControl( szMethods[n]);
		assert( m_arMethodPtrs[n] );

		m_arMethodPtrs[n]->SetProcOnCheck(theUIShopMethodOnCheckBoxCheck);
		m_arMethodPtrs[n]->SetCheck(FALSE);
		m_arMethodPtrs[n]->SetGroupID(METHOD_PAGE_GROUP_ID);
		m_arMethodPtrs[n]->SetIndexAtGroup(n);
	}


	return TRUE;
}


// 卸载UI
BOOL UIShop::UnLoadUI()
{
	GetUIShopShopUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Shop") );
}

// 是否可视
BOOL UIShop::IsVisible()
{
	return m_pID_FRAME_SHOP->IsVisible();
}

// 设置是否可视
void UIShop::SetVisible(  BOOL bVisible )
{
	//if(bVisible)
	//{
	//	BaseContainer*	pContainer;
	//	pContainer = (BaseContainer*)m_pID_LISTIMG_SHOP->GetSlotContainer();
	//	if(pContainer)
	//		ChangePage(pContainer->GetCurrentPage());
	//}
	if(!bVisible)
	{
		if(m_nCurrentMethod != SHOP_METHOD_NONE)
			ChangeMethod(SHOP_METHOD_NONE);
	}

	if( bVisible != IsVisible() )
		SetLayoutVisible();
	else
		m_pID_FRAME_SHOP->SetVisible( bVisible );

	theUIInventory.SetVisible	(bVisible);
	theUIInventory.ChangePage	((UINT)-1);

	if(bVisible)
	{
		//m_pID_LISTIMG_SHOP->SetCanDrag(m_nCurrentMethod == SHOP_METHOD_NONE);
	}
}

void UIShop::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_SHOP);
}

void UIShop::Refresh()
{
}

void UIShop::SetMoney(MONEY*	pMoney)
{
	if(!pMoney)
		return;

	if( m_pID_TEXT_MONEY )
		*m_pID_TEXT_MONEY = (INT)*pMoney;
}


void UIShop::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		switch(pContainer->GetSlotIndex())
		{
		case SI_NPCSHOP:
			{
				m_pID_LISTIMG_SHOP->SetSlotContainer(pContainer);

				INT nDisable = pContainer->GetSlotMaxSize()/MAX_SLOT_PER_SHOPPAGE;
				for(INT n=0; n<MAX_SHOP_PAGE_NUM;n++)
				{
					m_arCheckBoxPtrs[n]->SetVisible(n <= nDisable);
				}
				pContainer->SetCurrentPage(0);
				pContainer->SetSlotNumPerPage(MAX_SLOT_PER_SHOPPAGE);

			}
			break;
		}
	}
}

void UIShop::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	UIContainHelper	helper(m_pID_LISTIMG_SHOP
									,m_pPlayer
									,MAX_SLOT_PER_SHOPPAGE
									,m_nCurrentPage);
	helper.PushItem(*pSlot);


}


void UIShop::PopGoods( BaseSlot* pSlot )
{
	UIContainHelper	helper(m_pID_LISTIMG_SHOP
									,m_pPlayer
									,MAX_SLOT_PER_SHOPPAGE
									,m_nCurrentPage);
	helper.PopItem(*pSlot);



	Refresh();
}

void UIShop::ChangePage	(UINT nPage)
{
	if(nPage >= MAX_SHOP_PAGE_NUM)
		return;
	UINT n;

	for(n=0; n<MAX_SHOP_PAGE_NUM; n++)
	{
		m_arCheckBoxPtrs[n]->SetCheck(nPage == n);
	}
	m_nCurrentPage = nPage;

	theUIShopSlotUIListener.GetContainer()->SetCurrentPage((SLOTPOS)m_nCurrentPage);
	/// 最后更新背包数据
	m_pID_LISTIMG_SHOP->Clear();
	theUIShopSlotUIListener.Update((SLOTPOS)(m_nCurrentPage*MAX_SLOT_PER_SHOPPAGE), MAX_SLOT_PER_SHOPPAGE);
	Refresh();
}


void UIShop::ChangeMethod	(eSHOP_METHOD eMethod)
{
	if(eMethod >= SHOP_METHOD_MAX)
		return;
	INT n;

	for(n=0; n<SHOP_METHOD_MAX; n++)
	{
		m_arMethodPtrs[n]->SetCheck(eMethod == n);
	}
	m_nCurrentMethod = eMethod;

	ShopDialog*	pShop = (ShopDialog*)theUIShopSlotUIListener.GetContainer();

	pShop->SetCheckTrade((eSHOP_METHOD)m_nCurrentMethod);

	if(m_nCurrentMethod == SHOP_METHOD_NONE)
	{
		theIconDragManager.ClearDrag();
	}
	//m_pID_LISTIMG_SHOP->SetCanDrag(eMethod == SHOP_METHOD_NONE);
}

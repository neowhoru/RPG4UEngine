/*////////////////////////////////////////////////////////////////////////
文 件 名：UITeamPlayer1.cpp
创建日期：2007年10月20日
最后更新：2007年10月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UITeamPlayer.h"
#include "HeroActionInput.h"
#include "UIMenuPlayer.h"
#include "UITarget.h"
#include "GlobalInstancePriority.h"

const DWORD	TEAM_PLAYER_GROUP	= TAG('TEAM');

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer1, (1)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer2, (2)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer3, (3)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer4, (4)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer5, (5)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer6, (6)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer7, (7)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer8, (8)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer9, (9)  , gamemain::eInstPrioClientUI);
GLOBALINST_IMPL2(UITeamPlayer, UITeamPlayer10,(10) , gamemain::eInstPrioClientUI);


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE	(theUITeamPlayer1, OnFrameMove )
	UIPROC_FRAME_RENDER		(theUITeamPlayer1, OnRender )
	UIPROC_ICON_DROP_TO     (theUITeamPlayer1,ID_LISTIMG_StatusOnIconDropTo );   
	UIPROC_ICON_LDB_CLICK   (theUITeamPlayer1,ID_LISTIMG_StatusOnIconLDBClick );
	UIPROC_ICON_RBUTTON_UP  (theUITeamPlayer1,ID_LISTIMG_StatusOnIconRButtonUp );
	UIPROC_BUTTON_CLICK     (theUITeamPlayer1,OnBtnCLickKick);  
	UIPROC_BUTTON_CLICK     (theUITeamPlayer1,OnbtnClickChangeHeader);  
	UIPROC_BUTTON_CLICK     (theUITeamPlayer1,OnButtonClickHead);  
	UIPROC_BUTTON_CLICK     (theUITeamPlayer1,OnSelectLookPlayer);  
};//namespace uicallback
using namespace uicallback;

UITeamPlayer* UITeamPlayer::ms_arGroupPlayer[] = {0};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UITeamPlayer::UITeamPlayer(int nIndex)
{
	// Member
	m_pID_FRAME_Player = NULL;
	m_pID_TEXT_Name = NULL;
	m_pID_PROGRESS_Hp = NULL;
	m_pID_LISTIMG_Status = NULL;
	m_pID_PROGRESS_Mp = NULL;
	m_pID_TEXT_LVL = NULL;
	m_pPicHeader = NULL;
	m_pBtnKickOff = NULL;
	//m_pID_BUTTON_changeHeader = NULL;

	assert(nIndex <= MAX_PARTYMEMBER_NUM && nIndex >= 1);
	m_nPlayerIndex					= nIndex;
	ms_arGroupPlayer[nIndex]	= this;

}
// Frame
BOOL UITeamPlayer::OnFrameMove(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
BOOL UITeamPlayer::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}
// ListImg / ListEx
BOOL UITeamPlayer::ID_LISTIMG_StatusOnIconDropTo(VUCtrlObject*    /*pSender*/
                                                ,VUCtrlObject*    /*pThisCtrl*/
																,IconDragListImg* /*pItemDrag*/
																,IconDragListImg* /*pItemDest*/)
{
	
	return FALSE;
	
}

BOOL UITeamPlayer::ID_LISTIMG_StatusOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}
BOOL UITeamPlayer::ID_LISTIMG_StatusOnIconRButtonUp( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}

// 装载UI
BOOL UITeamPlayer::LoadUI()
{


	m_pID_FRAME_Player = theUICtrlManager.LoadFrame(UIDOC_PATH("TeamPlayer")
                                                  ,TRUE
																  ,FALSE
																  ,FMSTR(ID_FRAME_Group_Player,m_nPlayerIndex));
	if ( m_pID_FRAME_Player == 0 )
	{
		UIMessageLog("读取文件[UI\\TeamPlayer.UI]失败")
		return FALSE;
	}
	
	m_pID_FRAME_Player->SetGroupID		(TEAM_PLAYER_GROUP);
	m_pID_FRAME_Player->SetIndexAtGroup	(m_nPlayerIndex);
	m_pID_FRAME_Player->SetUserData((LPARAM)this);

	return InitControls();
	
}
// 关连控件
BOOL UITeamPlayer::InitControls()
{


	m_pID_TEXT_Name		= (VUCtrlText*)		m_pID_FRAME_Player->FindControl(  ID_TEXT_Name );
	m_pID_PROGRESS_Hp		= (VUCtrlProgress*)	m_pID_FRAME_Player->FindControl(  ID_PROGRESS_Hp );
	m_pID_LISTIMG_Status = (VUCtrlListImg*)	m_pID_FRAME_Player->FindControl(  ID_LISTIMG_Status );
	m_pID_PROGRESS_Mp		= (VUCtrlProgress*)	m_pID_FRAME_Player->FindControl(  ID_PROGRESS_Mp );
	m_pID_TEXT_LVL			= (VUCtrlText*)		m_pID_FRAME_Player->FindControl(  ID_TEXT_LVL );
	m_pBtnKickOff			= (VUCtrlButton*)		m_pID_FRAME_Player->FindControl(  ID_BUTTON_kick);
	m_pPicHeader			= (VUCtrlPicture*)	m_pID_FRAME_Player->FindControl(  ID_PICTURE_Header);
	m_pPicSelector			= (VUCtrlPicture*)	m_pID_FRAME_Player->FindControl(  ID_PICTURE_choise);
	//m_pID_BUTTON_changeHeader	= (VUCtrlButton*)m_pID_FRAME_Player->FindControl(  ID_BUTTON_changeHeader);

	assert(m_pBtnKickOff);
	assert(m_pPicHeader);
	assert(m_pPicSelector);
	assert( m_pID_FRAME_Player );
	assert( m_pID_TEXT_Name );
	assert( m_pID_PROGRESS_Hp );
	assert( m_pID_LISTIMG_Status );
	assert( m_pID_PROGRESS_Mp );
	assert( m_pID_TEXT_LVL );
	//assert( m_pID_BUTTON_changeHeader );

	m_pBtnKickOff->SetVisible(FALSE);
	m_pPicSelector->SetVisible(FALSE);


	m_pID_FRAME_Player->SetProcOnFrameMove		(  theUITeamPlayer1OnFrameMove );
	m_pID_FRAME_Player->SetProcOnRender			(  theUITeamPlayer1OnRender );
	m_pID_LISTIMG_Status			->SetProcOnDropTo	(  theUITeamPlayer1ID_LISTIMG_StatusOnIconDropTo );
	m_pID_LISTIMG_Status			->SetProcOnLButtonDBClick	(  theUITeamPlayer1ID_LISTIMG_StatusOnIconLDBClick );
	m_pID_LISTIMG_Status			->SetProcOnRButtonUp			(  theUITeamPlayer1ID_LISTIMG_StatusOnIconRButtonUp );
	m_pBtnKickOff					->SetProcOnButtonClick		(  theUITeamPlayer1OnBtnCLickKick);
	//m_pID_BUTTON_changeHeader	->SetProcOnButtonClick		(  theUITeamPlayer1OnbtnClickChangeHeader);

	m_pID_FRAME_Player->SetProcOnButtonDown( theUITeamPlayer1OnButtonClickHead );
	m_pID_FRAME_Player->SetProcOnClick( theUITeamPlayer1OnSelectLookPlayer );
	return TRUE;
}

BOOL UITeamPlayer::OnSelectLookPlayer( VUCtrlObject* pSender )
{
	__CHECK(pSender->GetGroupID() == TEAM_PLAYER_GROUP);

	UITeamPlayer*	pUIFrame = (UITeamPlayer*)pSender->GetUserData();
	__CHECK_PTR(pUIFrame);

	return pUIFrame->OnSelecMemeberPlayer();
}

BOOL UITeamPlayer::OnSelecMemeberPlayer(  )
{
	theHeroActionInput.SetCurrentTarget(m_nPlayerID);
	//theUITarget.SetPlayer(m_nPlayerID);

	//UpdateMemberSelect();
	return TRUE;
}

void UITeamPlayer::UpdateMemberSelect		()
{
	UITeamPlayer*	pUIPlayer;
	for(int n=1; n<= MAX_PARTYMEMBER_NUM; n++)
	{
		pUIPlayer = ms_arGroupPlayer[n];
		if(theHeroActionInput.GetCurrentTarget() == (DWORD)pUIPlayer->m_nPlayerID)
		{
			pUIPlayer->m_pPicSelector->SetVisible(TRUE);

			if(theUIMenuPlayer.IsVisible())
			{
				if(pUIPlayer->m_nPlayerID != theUIMenuPlayer.GetPlayerID())
					theUIMenuPlayer.SetVisible(FALSE);
			}
		}
		else
		{
			pUIPlayer->m_pPicSelector->SetVisible(FALSE);
		}
	}
}


BOOL UITeamPlayer::OnButtonClickHead( VUCtrlObject* pSender )
{
	///////////////////////////////////
	__CHECK(pSender->GetGroupID() == TEAM_PLAYER_GROUP);

	UITeamPlayer*	pUIFrame = (UITeamPlayer*)pSender->GetUserData();
	__CHECK_PTR(pUIFrame);


	///////////////////////////////////
	BOOL	bShow(FALSE);

	if(!theUIMenuPlayer.IsVisible() )
	{
		bShow = TRUE;
	}
	else if(theUIMenuPlayer.GetPlayerID() == pUIFrame->m_nPlayerID)
	{
		theUIMenuPlayer.SetVisible(FALSE);
	}
	else
	{
		bShow = TRUE;
	}

	///////////////////////////////////
	if(bShow)
	{
		RECT	rc;
		pSender->GetGlobalRect(&rc);
		theUIMenuPlayer.SetPos( rc.right, rc.top);

		theUIMenuPlayer.SetPlayerID( pUIFrame->m_nPlayerID );
		theUIMenuPlayer.SetVisible(TRUE, TRUE);
	}

	///////////////////////////////////

	return pUIFrame->OnSelecMemeberPlayer();
}

BOOL UITeamPlayer::OnBtnCLickKick( VUCtrlObject* /*pSender*/ )
{
	m_pBtnKickOff->SetVisible(FALSE);
	return TRUE;
	
}
BOOL UITeamPlayer::OnbtnClickChangeHeader( VUCtrlObject* /*pSender*/ )
{
	
	return TRUE;
	
}
// 卸载UI
BOOL UITeamPlayer::UnLoadUI()
{
	
	char szPath[128];
	sprintf(szPath,  "TeamPlayer%d" UIDATA_FILEEXT, m_nPlayerIndex);
	return theUICtrlManager.RemoveFrame( _PATH_UIDOC(szPath) );
	
}
// 是否可视
BOOL UITeamPlayer::IsVisible()
{
	
	return m_pID_FRAME_Player->IsVisible();
	
}
// 设置是否可视
void UITeamPlayer::SetVisible( BOOL bVisible )
{
	
	m_pID_FRAME_Player->SetVisible( bVisible );
	
}
void UITeamPlayer::setID(int nID)
{
	
	m_nPlayerID = nID;
	
}

void UITeamPlayer::SetPicHeader( BOOL bHeader)
{
	if(bHeader)
		m_pPicHeader->SetVisible(TRUE);
	else	
		m_pPicHeader->SetVisible(FALSE);
	
}

void UITeamPlayer::SetKickEnable(  BOOL bEnable)
{
	bEnable = 0;
	if(bEnable)
		m_pBtnKickOff->SetVisible(TRUE);
	else	
		m_pBtnKickOff->SetVisible(FALSE);
	
}

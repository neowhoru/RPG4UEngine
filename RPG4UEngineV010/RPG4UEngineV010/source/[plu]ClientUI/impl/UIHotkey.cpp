
#include "stdafx.h"
/********************************************************************
	Created by UIEditor.exe
	FileName: E:\Code\RunTime\CLIENT\Data\UI\Hotkey.cpp
*********************************************************************/
#include <assert.h>
#include "..\..\HRUI\HR_UI_Mgr.h"
#include "Hotkey.h"
#include "Skill.h"
#include "Pack.h"
#include "../PlayerRole.h"
#include "../GameMain.h"
#include "../PlayerMgr.h"
#include "../../HRUI/HR_UI_IconDragMgr.h"
#include "../../Common/RestoreColdDown.h"
#include "Function.h"

extern GAME_STATE	g_GameState;

namespace UI_ID_FRAME_HOTKEY
{
	// Member
	CHR_UI_Frame*	m_pID_FRAME_HOTKEY = NULL;
	CHR_UI_ListImg*	m_pID_LISTIMG_Hotkey = NULL;

	bool MsgProc( UINT msg, WPARAM wParam, LPARAM lParam, bool bMsgUsed )
	{
		if ( bMsgUsed == true || g_GameState != G_MAIN )
			return false;
		switch( msg ) 
		{
		case WM_LBUTTONUP:
			{
				if ( !theUIIconDragMgr.m_pLastSelItem )
					return false;
//				int nX = theUIIconDragMgr.m_pLastSelItem->m_stIcon.XPos();
//				int nY = theUIIconDragMgr.m_pLastSelItem->m_stIcon.YPos();
//				int nIndex = m_pID_LISTIMG_Hotkey->GetItemIndex( theUIIconDragMgr.m_pLastSelItem );
				int nIndex = theUIIconDragMgr.m_pLastSelItem->m_stIcon.XPos();
				if ( nIndex == -1 )
					return false;
				theUIIconDragMgr.m_pLastSelItem = NULL;

				thePlayerRole.m_pHotKeyBag[nIndex].ItemData.ustItemID = ArabicNights::ErrorUnsignedShortID;
				Refeash();
				return true;
			}
			break;
		case WM_KEYDOWN:
			{
				int nKeyIndex = -1;
				SMouseItem *pHotkey = NULL;
				switch( wParam ) 
				{
				case VK_F1:
					nKeyIndex = 0;
					break;
				case VK_F2:
					nKeyIndex = 1;
					break;
				case VK_F3:
					nKeyIndex = 2;
					break;
				case VK_F4:
					nKeyIndex = 3;
					break;
				case VK_F5:
					nKeyIndex = 4;
					break;
				case VK_F6:
					nKeyIndex = 5;
					break;
				case VK_F7:
					nKeyIndex = 6;
					break;
				case VK_F8:
					nKeyIndex = 7;
					break;
				case VK_F9:
					nKeyIndex = 8;
					break;
				case VK_F10:
					nKeyIndex = 9;
					break;	
				}
				if ( nKeyIndex == -1 )
					return false;
				pHotkey = &thePlayerRole.m_pHotKeyBag[nKeyIndex];
				if ( pHotkey->ItemData.ustItemID == ArabicNights::ErrorUnsignedShortID )
					return false;
				switch( pHotkey->ShortCut.BagType )
				{
				case ArabicNights::BagTypeSkill:
					{
						UINT nSkillIndex = pHotkey->ShortCut.BagIndex;
						if ( nSkillIndex >= ArabicNights::ArabicNights_iMaxSkillKnown )
							return false;
						ArabicNights::SCharSkill *pSkill = NULL;
						pSkill = &thePlayerRole.m_pSkillBag[nSkillIndex];
						theDragoonGame.GetPlayerMgr()->SetCurUseSkill( pSkill->ustSkillID,
																		pSkill->stSkillLevel );
						thePlayerRole.UpdateUI();
					}
					break;
				case ArabicNights::BagTypePack:
					{
						UINT nPackIndex = pHotkey->ShortCut.BagIndex;
						if ( nPackIndex >= ArabicNights::ITEM_BAGMAX )
							return false;

						CRestoreColdDown* pRestoreColdDown = NULL;
						pRestoreColdDown = CRestoreColdDown::GetRestoreColdDown();
						if( pRestoreColdDown->UseRestore( pHotkey->ItemData.ustItemID ) == true )
						{
							MsgUseGoods msg;
							msg.ustItemIndex = nPackIndex;
							theNetworkInput.SendMsg( &msg );
							//
							thePlayerRole.UpdateUI();
						}
					}
					break;
				case ArabicNights::BagTypeFunction:
					{
						UI_ID_FRAME_Function::SetCurFunction( pHotkey->ItemData.ustItemID );
					}
					break;
				default:
					break;
				}
			}
			break;
		}
		return false;
	}

	// Frame
	bool ID_FRAME_HOTKEYOnFrameRun()
	{
		return true;
	}
	bool ID_FRAME_HOTKEYOnFrameRender()
	{
		return true;
	}
	// ListImg / ListEx
	bool ID_LISTIMG_HotkeyOnIconDragOn( CHR_UI_Object* pSender, CHR_UI_Object* pMe,
											CHR_UI_IconDrag::S_ListImg* pItemDrag,
											CHR_UI_IconDrag::S_ListImg* pItemSrc )
	{
		if ( pItemDrag->IsNull() )
			return false;
//		UINT nHotkeyIndex = m_pID_LISTIMG_Hotkey->GetItemIndex( pItemSrc );
		UINT nHotkeyIndex = pItemSrc->m_stIcon.XPos();
		if ( nHotkeyIndex >= ArabicNights::ArabicNights_iHotkey )
			return false;
		// 自己
		if ( pSender == pMe )
		{
			UINT nSrcIndex = m_pID_LISTIMG_Hotkey->GetItemIndex( pItemDrag );
			thePlayerRole.m_pHotKeyBag[nHotkeyIndex] = thePlayerRole.m_pHotKeyBag[nSrcIndex];
		}
		// 如果是技能栏的东东
		else if ( pSender == s_CUI_ID_FRAME_SKILL.m_pID_LISTEX_Skill )
		{
			int nSkillIndex = -1;
			for( int n=0; n<ArabicNights::ArabicNights_iMaxSkillKnown; n++ )
			{
				if( pItemDrag->m_stIcon.Id() == thePlayerRole.m_pSkillBag[n].ustSkillID )
				{
					nSkillIndex = n;
					break;
				}
			}
			if ( nSkillIndex == -1 )
				return false;
			ArabicNights::SMouseItem *pHotkey = &thePlayerRole.m_pHotKeyBag[nHotkeyIndex];
			pHotkey->ShortCut.BagType = ArabicNights::BagTypeSkill;
			pHotkey->ShortCut.BagIndex = nSkillIndex;
			pHotkey->ItemData.ustItemID = pItemDrag->m_stIcon.Id();
		}
		// 背包
		else if ( pSender == s_CUI_ID_FRAME_PACK.m_pID_LISTIMG_PACK )
		{
//			UINT nPackIndex = UI_ID_FRAME_PACK::m_pID_LISTIMG_PACK->GetItemIndex( pItemDrag );
			UINT nPackIndex = thePlayerRole.m_bag.FindItemIndex( pItemDrag->m_stIcon.XPos(),
																pItemDrag->m_stIcon.YPos() );
			if ( nPackIndex >= ArabicNights::ITEM_BAGMAX )
				return false;
			ArabicNights::SMouseItem *pHotkey = &thePlayerRole.m_pHotKeyBag[nHotkeyIndex];
			pHotkey->ShortCut.BagType = ArabicNights::BagTypePack;
			pHotkey->ShortCut.BagIndex = nPackIndex;
			pHotkey->ItemData.ustItemID = pItemDrag->m_stIcon.Id();
		}
		// 功能
		else if ( pSender == UI_ID_FRAME_Function::m_pID_LISTIMG_Function )
		{
			UINT nFunction = UI_ID_FRAME_Function::m_pID_LISTIMG_Function->GetItemIndex( pItemDrag );
			ArabicNights::SMouseItem *pHotkey = &thePlayerRole.m_pHotKeyBag[nHotkeyIndex];
			pHotkey->ShortCut.BagType = ArabicNights::BagTypeFunction;
			pHotkey->ShortCut.BagIndex = nFunction;
			pHotkey->ItemData.ustItemID = pItemDrag->m_stIcon.Id();
		}
		Refeash();
		return false;
	}
	//bool ID_LISTIMG_HotkeyOnIconDragOff( CHR_UI_Object* pSender, CHR_UI_Object* pMe,
	//										CHR_UI_IconDrag::S_ListImg* pItem )
	//{
	//	return false;
	//}
	bool ID_LISTIMG_HotkeyOnIconLDBClick( CHR_UI_Object* pSender, CHR_UI_IconDrag::S_ListImg* pItem )
	{
		return false;
	}
	bool ID_LISTIMG_HotkeyOnIconRButtonUp( CHR_UI_Object* pSender, CHR_UI_IconDrag::S_ListImg* pItem )
	{
		unsigned int nIndex = m_pID_LISTIMG_Hotkey->GetItemIndex( pItem );
		if ( nIndex > 10 )
			return false;
		MsgProc( WM_KEYDOWN, VK_F1+nIndex, 0, false );
		return false;
	}

	// 装载UI
	bool LoadUI()
	{
		DWORD dwResult = theHRUIMgr.AddFrame( "interface\\Hotkey.itf" );
		if ( dwResult == 0 )
		{
			MESSAGE_BOX("读取文件[UI\\Hotkey.UI]失败")
			return false;
		}
		else if ( dwResult != 1497/*文件版本号*/ )
		{
			MESSAGE_BOX("读取文件[UI\\Hotkey.UI]与源代码版本不一样")
		}
		return DoControlConnect();
	}
	// 关连控件
	bool DoControlConnect()
	{
		theHRUIMgr.OnFrameRun( ID_FRAME_HOTKEY, ID_FRAME_HOTKEYOnFrameRun );
		theHRUIMgr.OnFrameRender( ID_FRAME_HOTKEY, ID_FRAME_HOTKEYOnFrameRender );
		theHRUIMgr.OnIconDragOn( ID_FRAME_HOTKEY, ID_LISTIMG_Hotkey, ID_LISTIMG_HotkeyOnIconDragOn );
//		theHRUIMgr.OnIconDragOff( ID_FRAME_HOTKEY, ID_LISTIMG_Hotkey, ID_LISTIMG_HotkeyOnIconDragOff );
		theHRUIMgr.OnIconLDBClick( ID_FRAME_HOTKEY, ID_LISTIMG_Hotkey, ID_LISTIMG_HotkeyOnIconLDBClick );
		theHRUIMgr.OnIconRButtonUp( ID_FRAME_HOTKEY, ID_LISTIMG_Hotkey, ID_LISTIMG_HotkeyOnIconRButtonUp );

		m_pID_FRAME_HOTKEY = (CHR_UI_Frame*)theHRUIMgr.FindFrame( ID_FRAME_HOTKEY );
		m_pID_LISTIMG_Hotkey = (CHR_UI_ListImg*)theHRUIMgr.FindControl( ID_FRAME_HOTKEY, ID_LISTIMG_Hotkey );
		assert( m_pID_FRAME_HOTKEY );
		assert( m_pID_LISTIMG_Hotkey );

		m_pID_FRAME_HOTKEY->SetMsgProcFun( MsgProc );
		m_pID_LISTIMG_Hotkey->UseProgressTime( true );
		m_pID_LISTIMG_Hotkey->SetShowAllInfo( false );

		USE_SCRIPT( eUI_OBJECT_HotKey, m_pID_FRAME_HOTKEY );
		return true;
	}
	// 卸载UI
	bool UnLoadUI()
	{
		return theHRUIMgr.RemoveFrame( "interface\\Hotkey.itf" );
	}
	// 是否可视
	bool IsVisable()
	{
		return m_pID_FRAME_HOTKEY->IsVisable();
	}
	// 设置是否可视
	void SetVisable( const bool bVisable )
	{
		m_pID_FRAME_HOTKEY->SetVisable( bVisable );
	}

	void Refeash()
	{
		m_pID_LISTIMG_Hotkey->Clear();
		SMouseItem *pHotkey = NULL;
		CHR_UI_IconDrag::S_ListImg stItem;
		for(int i=0; i<ArabicNights::ArabicNights_iHotkey;  i++)
		{
			pHotkey = &thePlayerRole.m_pHotKeyBag[i];
			if ( pHotkey->ItemData.ustItemID != ArabicNights::ErrorUnsignedShortID )
			{
				switch( pHotkey->ShortCut.BagType )
				{
				case ArabicNights::BagTypeSkill:
					{
						int nSkillIndex = pHotkey->ShortCut.BagIndex;
						ArabicNights::SCharSkill *pSkill = &thePlayerRole.m_pSkillBag[nSkillIndex];
						if ( pSkill->ustSkillID == ArabicNights::ErrorUnsignedShortID )
						{
							break;
						}
						stItem.SetData( pSkill->ustSkillID, pSkill->stSkillLevel, pSkill->bAvailable, true );
						stItem.SetTime( thePlayerRole.m_stSkillColdTime[nSkillIndex].dwSkillColdStartTime,
										thePlayerRole.m_stSkillColdTime[nSkillIndex].dwSkillColdTime );
						m_pID_LISTIMG_Hotkey->SetItem( &stItem, i );
					}
					break;
				case ArabicNights::BagTypePack:
					{
						int nItemID = pHotkey->ItemData.ustItemID;
						CItemDetail::SItemCommon *pItem = theItemDetail.GetItemByID( nItemID );
						if ( !pItem )
							break;
						if ( pItem->IsExclusive() )
						{
							int nIndex = thePlayerRole.m_bag.GetItemIndex( &pHotkey->ItemData );
							if ( nIndex != -1 )
							{
								pHotkey->ShortCut.BagIndex = nIndex;
								stItem.SetData( &pHotkey->ItemData );
								m_pID_LISTIMG_Hotkey->SetItem( &stItem, i );
							}
						}
						else
						{
							int nCount = thePlayerRole.m_bag.GetItemCount( pHotkey->ItemData.ustItemID );
							int nIndex = thePlayerRole.m_bag.GetItemIndexById( pHotkey->ItemData.ustItemID );
							if ( nCount > 0 )
							{
								pHotkey->ShortCut.BagIndex = nIndex;
								pHotkey->ItemData.extdata.ustItemCount = nCount;
								stItem.SetData( &pHotkey->ItemData );
								//
								CRestoreColdDown* pRestoreColdDown = NULL;
								pRestoreColdDown = CRestoreColdDown::GetRestoreColdDown();
								DWORD dwStartTiem,dwPeriod;
								if( pRestoreColdDown->GetTime( pHotkey->ItemData.ustItemID,
																&dwStartTiem, &dwPeriod ) == true )
								{
									stItem.m_bEnable = false;
									stItem.SetTime( dwStartTiem,dwPeriod );
								}
								//
								m_pID_LISTIMG_Hotkey->SetItem( &stItem, i );
							}
						}
					}
					break;
				case ArabicNights::BagTypeFunction:
					{
						stItem.SetData( &pHotkey->ItemData );
						m_pID_LISTIMG_Hotkey->SetItem( &stItem, i );
					}
					break;
				default:
				//	assert( false );
					break;
				}
			}
		}
	}
}

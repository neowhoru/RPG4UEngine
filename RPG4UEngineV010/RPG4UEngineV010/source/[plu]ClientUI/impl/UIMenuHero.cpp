/*////////////////////////////////////////////////////////////////////////
文 件 名：UISelfRbtnMenu.cpp
创建日期：2007年11月24日
最后更新：2007年11月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIMenuHero.h"
#include "UITeamManager.h"
#include "MenuHeroUIListener.h"
#include "VHero.h"
#include "VHeroActionInput.h"
#include "HeroParty.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::MenuHeroUIListener& GetUISelfRbtnMenuMenuHeroUIListener()
{
	static gameui::MenuHeroUIListener staticMenuHeroUIListener;
	return staticMenuHeroUIListener;
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UIMenuHero, ()  , gamemain::eInstPrioClientUI/*UIMenuHero*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUIMenuHero, OnFrameMove )
		UIPROC_FRAME_RENDER( theUIMenuHero, OnRender )
		UIPROC_BUTTON_CLICK( theUIMenuHero, ID_BUTTON_DisbandOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuHero, ID_BUTTON_GetoutOnButtonClick )
		UIPROC_BUTTON_CLICK( theUIMenuHero, ID_BUTTON_DisMountOnButtonClick )
};//namespace uicallback
using namespace uicallback;


UIMenuHero::UIMenuHero()
{
	// Member
	m_pID_FRAME_SelfRBMenu = NULL;
	m_pID_BUTTON_Disband = NULL;
	m_pID_BUTTON_Getout = NULL;
	m_pID_BUTTON_DisMount = NULL;

}
// Frame
BOOL UIMenuHero::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}
BOOL UIMenuHero::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}
// Button
BOOL UIMenuHero::ID_BUTTON_DisbandOnButtonClick( VUCtrlObject* /*pSender*/ )
{//解散
	
	theHeroParty.Disband();
	SetVisible( FALSE );
	return TRUE;
	
}
// Button
BOOL UIMenuHero::ID_BUTTON_GetoutOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	//SetVisible(FALSE);
	theHeroParty.LeaveHero();
	SetVisible( FALSE );
	return TRUE;
}

//////////////////////////////////////////////////////////////////////////
BOOL UIMenuHero::ID_BUTTON_DisMountOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	theVHero.DisMount();

	//singleton::GetPlayerManager().ChangeDrome( -1 );
	theVHero.ChangeDrome((VOBJID) -1 );
	
	SetVisible(FALSE);
	return TRUE;
	
}

// 装载UI
BOOL UIMenuHero::LoadUI()
{
	
	GetUISelfRbtnMenuMenuHeroUIListener().RegisterMe();
	
	m_pID_FRAME_SelfRBMenu = theUICtrlManager.LoadFrame( UIDOC_PATH( "MenuHero") );
	if ( m_pID_FRAME_SelfRBMenu == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "MenuHero")"]失败")
		return FALSE;
	}

	return InitControls();
	
}

// 关连控件
BOOL UIMenuHero::InitControls()
{
	m_pID_BUTTON_Disband = (VUCtrlButton*)m_pID_FRAME_SelfRBMenu->FindControl(  ID_BUTTON_Disband );
	m_pID_BUTTON_Getout = (VUCtrlButton*)m_pID_FRAME_SelfRBMenu->FindControl(  ID_BUTTON_Getout );

	m_pID_BUTTON_DisMount = (VUCtrlButton*)m_pID_FRAME_SelfRBMenu->FindControl(  ID_BUTTON_DisMount );

	assert( m_pID_BUTTON_Disband );
	assert( m_pID_BUTTON_Getout );
	

	m_pID_FRAME_SelfRBMenu->SetProcOnFrameMove(  theUIMenuHeroOnFrameMove );
	m_pID_FRAME_SelfRBMenu->SetProcOnRender	(  theUIMenuHeroOnRender );


	m_pID_BUTTON_Disband	->SetProcOnButtonClick( theUIMenuHeroID_BUTTON_DisbandOnButtonClick );
	m_pID_BUTTON_Getout	->SetProcOnButtonClick( theUIMenuHeroID_BUTTON_GetoutOnButtonClick );
	m_pID_BUTTON_DisMount->SetProcOnButtonClick( theUIMenuHeroID_BUTTON_DisMountOnButtonClick );

	return TRUE;
	
}
// 卸载UI
BOOL UIMenuHero::UnLoadUI()
{
	
	GetUISelfRbtnMenuMenuHeroUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "MenuHero") );
	
}
// 是否可视
BOOL UIMenuHero::IsVisible()
{
	return m_pID_FRAME_SelfRBMenu->IsVisible();
	
}
// 设置是否可视
void UIMenuHero::SetVisible( BOOL bVisible )
{

	INT	nCount	(0);

	m_pID_BUTTON_Disband	->SetVisible(FALSE);
	m_pID_BUTTON_DisMount->SetVisible(FALSE);
	m_pID_BUTTON_Getout	->SetVisible(FALSE);
	m_pID_BUTTON_Getout	->SetVisible(FALSE);

	///////////////////////////////////////////
	if(theHeroParty.GetMemberCount() > 0)
	{
		m_pID_BUTTON_Getout->SetVisible(TRUE);
		nCount++;
		if(theHeroParty.IsHeroMaster())
		{
			m_pID_BUTTON_Disband->SetVisible(TRUE);
			nCount++;
		}
	}

	///////////////////////////////////////////
	if(bVisible)
	{
		if(nCount == 0)
			bVisible = FALSE;
		//theUICtrlManager.BringToTop(m_pID_FRAME_SelfRBMenu);
	}

	m_pID_FRAME_SelfRBMenu->SetVisible( bVisible );
	if( bVisible )
		m_pID_FRAME_SelfRBMenu->SetActivate( bVisible );
}

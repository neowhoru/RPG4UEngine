/*////////////////////////////////////////////////////////////////////////
文 件 名：UINpcChatDialogBox.cpp
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UINpcDialog.h"
#include "CharActionInfo.h"
#include "UITarget.h"
#include "MediaPathManager.h"
#include "UIProgress.h"
#include "UISelectBox.h"
#include "UIShop.h"
#include "ScreenTipManager.h"
#include "VUILayoutManager.h"
#include "NpcDialogUIListener.h"
#include "Hero.h"
#include "ObjectManager.h"
#include "HeroActionInput.h"
#include "ItemLogManager.h"


#define QUEST_WAIT_TIME 10000

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::NpcDialogUIListener& GetUINpcChatDialogBoxNpcDialogUIListener()
{
	static gameui::NpcDialogUIListener staticNpcDialogUIListener;
	return staticNpcDialogUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(UINpcDialog, ()  , gamemain::eInstPrioClientUI/*UINpcDialog*/);

namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE	( theUINpcDialog, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUINpcDialog, OnRender )
	UIPROC_BUTTON_CLICK		(theUINpcDialog,  ID_BUTTON_NextOnButtonClick );
	UIPROC_BUTTON_CLICK		(theUINpcDialog,  ID_BUTTON_CloseOnButtonClick )
	MAP_TEXT_HYPER_CLICKBACK(theUINpcDialog,	ID_TEXT_HyperLinkClick);
	UIPROC_BUTTON_CLICK		(theUINpcDialog,  ID_BUTTON_EXITOnButtonClick );
};//namespace uicallback
using namespace uicallback;


UINpcDialog::UINpcDialog()
{
	// Member
	m_pID_FRAME_NPCDIALOG	= NULL;
	m_pID_BUTTON_Next			= NULL;
	m_pID_BUTTON_Close		= NULL;
	m_pID_TEXT_Info			= NULL;
	m_pID_TEXT_Name			= NULL;
	m_pID_PICTURE_Head		= NULL;
	m_pID_BUTTON_EXIT			= NULL;


	m_bQuestNpc		= FALSE;
	m_dwLastTime	= 0;
	m_nNpcId			= -1;
}


void UINpcDialog::CancelScript()
{
	BOOL bPopScript = FALSE;
	if(IsVisible())
	{
		CloseDialog();
		bPopScript = TRUE;
	}
	if(theUISelectBox.IsVisible())
	{
		theUISelectBox.SetVisible( FALSE );
		bPopScript = TRUE;
	}

	theItemLogManager.SetEnableLog(FALSE);

	if(bPopScript)
	{
		theHero.RunScript(INVALID_DWORD_ID,INVALID_DWORD_ID);
	}


}


BOOL UINpcDialog::OnFrameMove(DWORD /*dwTick*/)
{
	if ( m_nNpcId < 0 )
		return FALSE;


	return TRUE;
}

BOOL UINpcDialog::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

// Button
BOOL UINpcDialog::ID_BUTTON_NextOnButtonClick( VUCtrlObject* /*pSender*/ )
{

	if(singleton::ExistHero())
	{
		//Object*	pObject;
		DWORD		targetID;

		targetID	= theHeroActionInput.GetCurrentTarget();
		SetButtonEnable( FALSE );
		//theItemLogManager.EnableLog(TRUE);
		theItemLogManager.ClearLog();
		theHero.RunScript(0,targetID );

		SetVisible(FALSE);
	}


	return TRUE;
}


// Button
BOOL UINpcDialog::ID_BUTTON_CloseOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	//SetButtonEnable( FALSE );
	//CloseDialog();
	CancelScript();

	return TRUE;
}
// Button
BOOL UINpcDialog::ID_BUTTON_EXITOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	CancelScript();
	return TRUE;
}

//hyber
void UINpcDialog::ID_TEXT_HyperLinkClick(VUCtrlObject* /*pSender*/, LPCSTR 				szData )
{
	theNpcCoordinateManager.ProcessHyperLink(szData);
	
}
// 装载UI
BOOL UINpcDialog::LoadUI()
{
	
	GetUINpcChatDialogBoxNpcDialogUIListener().RegisterMe();

	m_pID_FRAME_NPCDIALOG = theUICtrlManager.LoadFrame( UIDOC_PATH( "NpcDialog") );
	if ( m_pID_FRAME_NPCDIALOG == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "NpcDialog") "]失败")
		return FALSE;
	}

	return InitControls();
	
}


// 关连控件
BOOL UINpcDialog::InitControls()
{
	m_pID_BUTTON_Next					= (VUCtrlButton*)	m_pID_FRAME_NPCDIALOG->FindControl(  ID_BUTTON_Next );
	m_pID_BUTTON_Close				= (VUCtrlButton*)	m_pID_FRAME_NPCDIALOG->FindControl(  ID_BUTTON_Close );
	m_pID_TEXT_Info					= (VUCtrlText*)	m_pID_FRAME_NPCDIALOG->FindControl(  ID_TEXT_Info );
	m_pID_TEXT_Name					= (VUCtrlText*)	m_pID_FRAME_NPCDIALOG->FindControl(  ID_TEXT_Name );
	m_pID_PICTURE_Head				= (VUCtrlPicture*)m_pID_FRAME_NPCDIALOG->FindControl(  ID_PICTURE_Head );
	m_pID_BUTTON_EXIT					= (VUCtrlButton*)	m_pID_FRAME_NPCDIALOG->FindControl(  ID_BUTTON_EXIT );

	assert( m_pID_BUTTON_Next );
	assert( m_pID_BUTTON_Close );
	assert( m_pID_TEXT_Info );
	assert( m_pID_TEXT_Name );
	assert( m_pID_PICTURE_Head );
	assert( m_pID_BUTTON_EXIT );

	
	m_pID_FRAME_NPCDIALOG->SetProcOnFrameMove		(  theUINpcDialogOnFrameMove );
	m_pID_FRAME_NPCDIALOG->SetProcOnRender			(  theUINpcDialogOnRender );
	m_pID_BUTTON_Next	->SetProcOnButtonClick		(  theUINpcDialogID_BUTTON_NextOnButtonClick );
	m_pID_BUTTON_Close->SetProcOnButtonClick		(  theUINpcDialogID_BUTTON_CloseOnButtonClick );
	m_pID_TEXT_Info	->SetProcOnHyperLinkClick	(  theUINpcDialogID_TEXT_HyperLinkClick);
	m_pID_BUTTON_EXIT	->SetProcOnButtonClick		(  theUINpcDialogID_BUTTON_EXITOnButtonClick );


	m_pID_FRAME_NPCDIALOG->SetVisible(FALSE);
	theUILayoutManager.AddFrame( m_pID_FRAME_NPCDIALOG );

	UISCRIPT_ENABLE( UIOBJ_NPCDIALOG, m_pID_FRAME_NPCDIALOG );
	return TRUE;
	
}

// 卸载UI
BOOL UINpcDialog::UnLoadUI()
{
	
	UISCRIPT_DISABLE( UIOBJ_NPCDIALOG );
	GetUINpcChatDialogBoxNpcDialogUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "NpcDialog") );
	

}
// 是否可视
BOOL UINpcDialog::IsVisible()
{
	
	return m_pID_FRAME_NPCDIALOG->IsVisible();
	
}

// 设置是否可视
void UINpcDialog::SetVisible( BOOL bVisible )
{
	if(!bVisible)
	{
		*m_pID_TEXT_Name = _T("");
	}
	if( bVisible != IsVisible() )
		theUILayoutManager.SetVisible(m_pID_FRAME_NPCDIALOG);
	m_pID_FRAME_NPCDIALOG->SetVisibleWhole( bVisible, TRUE );

	theUICtrlManager.UpdateControls();
	
}


LPCSTR  UINpcDialog::GetNpcName()
{
	return m_pID_TEXT_Name->GetText();
}


void UINpcDialog::SetDialogName( LPCSTR  szName )
{
	*m_pID_TEXT_Name = szName;
}


void UINpcDialog::SetNpc( UINT nID, UINT /*nImgID*/ )
{
	m_nNpcId = nID;
}

void UINpcDialog::SetButtonEnable( BOOL bEnabel )
{
	
	m_pID_BUTTON_Next->SetEnable( bEnabel );
	m_pID_BUTTON_EXIT->SetEnable( bEnabel );
	
}


void UINpcDialog::ShowDialog( LPCSTR szText  )
{
	if(szText == NULL || szText[0] == 0)
	{
		CancelScript();
		return;
	}

	StringHandle strRet;
	theNpcCoordinateManager.ConvertHyperLink(szText,strRet);

	m_pID_TEXT_Info->SetText(strRet);

	//////////////////////////////////
	StringHandle	sName(m_pID_TEXT_Name->GetText());
	if(sName.IsEmpty())
	{
		Object*	pObject;
		pObject = theObjectManager.GetObject(theHeroActionInput.GetCurrentTarget() );
		if(pObject)
			*m_pID_TEXT_Name = pObject->GetName();
	}


	SetButtonEnable( TRUE );
	SetVisible( TRUE );
}

void UINpcDialog::ShowDialog( LPCSTR szText , BOOL bShowNext )
{
	ShowDialog	(szText);
	if(szText != NULL)
		ShowNext		(bShowNext);
}

void	UINpcDialog::ShowNext( BOOL bShowNext )
{
	if ( bShowNext )
	{
		m_pID_BUTTON_Next->SetVisible( TRUE );
		m_pID_BUTTON_EXIT->SetVisible( FALSE );
	}
	else
	{
		m_pID_BUTTON_Next->SetVisible( FALSE );
		m_pID_BUTTON_EXIT->SetVisible( TRUE );
	}
}



void UINpcDialog::CloseDialog()
{
	m_nNpcId = -1;
	SetVisible( FALSE );
}

/*////////////////////////////////////////////////////////////////////////
文 件 名：UIChatListDlg.cpp
创建日期：2007年11月26日
最后更新：2007年11月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIChatLog.h"
#include "VUIObjectManager.h"
#include "GlobalInstancePriority.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_IMPL(UIChatLog, ()  , gamemain::eInstPrioClientUI/*UIChatLog*/);
#else
#endif

namespace uicallback
{
	//UIChatLog theUIChatLog;
	UIPROC_FRAME_FRAMEMOVE( theUIChatLog, OnFrameMove )
	UIPROC_FRAME_RENDER( theUIChatLog, OnRender )
	UIPROC_LIST_SELECT_CHANGE( theUIChatLog, ID_LIST_CHATINFOOnListSelectChange )
};//namespace uicallback
using namespace uicallback;

UIChatLog::UIChatLog()
:VUIControl(gameui::eUIChatSystem)
{
	
	// Member
	m_pID_FRAME_CHATINFODLG = NULL;
	m_pID_LIST_CHATINFO = NULL;
	
}

// Frame
BOOL UIChatLog::OnFrameMove(DWORD /*dwTick*/)
{
	
	if( m_pID_FRAME_CHATINFODLG->IsCovered() )
	{
		m_pID_FRAME_CHATINFODLG->SetCovered( FALSE );
	}
	return TRUE;
	
}

BOOL UIChatLog::OnRender(DWORD /*dwTick*/)
{
	
	return TRUE;
	
}

// List
void UIChatLog::ID_LIST_CHATINFOOnListSelectChange( VUCtrlObject* /*pSender*/, sUILIST_ITEM* /*pItem*/ )
{
	
	
	
}

// 装载UI
BOOL UIChatLog::LoadUI()
{
	
	m_pID_FRAME_CHATINFODLG = theUICtrlManager.LoadFrame( UIDOC_PATH( "ChatLog") );
	if ( m_pID_FRAME_CHATINFODLG == 0 )
	{
		UIMessageLog("读取文件["UIDOC_INFO( "ChatLog")"]失败")
		return FALSE;
	}

	return InitControls();
	
}
// 关连控件
BOOL UIChatLog::InitControls()
{
	
	VUIControl::InitControls();

	m_pID_LIST_CHATINFO = (VUCtrlList*)m_pID_FRAME_CHATINFODLG->FindControl(  ID_LIST_CHATINFO );
	assert( m_pID_LIST_CHATINFO );

	m_pID_FRAME_CHATINFODLG->SetProcOnFrameMove(  theUIChatLogOnFrameMove );
	m_pID_FRAME_CHATINFODLG->SetProcOnRender(  theUIChatLogOnRender );
	m_pID_LIST_CHATINFO->SetProcOnSelectChange( theUIChatLogID_LIST_CHATINFOOnListSelectChange );

	
	m_pID_LIST_CHATINFO->SetBlendColor( 0xAA );
	SetVisible( FALSE );
	
	UISCRIPT_ENABLE( UIOBJ_CHATLOG, m_pID_FRAME_CHATINFODLG );
	return TRUE;
	
}

// 卸载UI
BOOL UIChatLog::UnLoadUI()
{
	
	UISCRIPT_DISABLE( UIOBJ_CHATLOG );
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "ChatLog") );
	
}

// 是否可视
BOOL UIChatLog::IsVisible()
{
	
	return m_pID_FRAME_CHATINFODLG->IsVisible();
	
}

// 设置是否可视
void UIChatLog::SetVisible( BOOL bVisible )
{
	
	VUIControl::SetVisible( bVisible );
	m_pID_FRAME_CHATINFODLG->SetVisible( bVisible );
	if( m_pID_FRAME_CHATINFODLG->IsCovered() )
	{
		m_pID_FRAME_CHATINFODLG->SetCovered( TRUE );
	}
	m_pID_LIST_CHATINFO->SetScrollPos( -20, 0 );
	m_pID_LIST_CHATINFO->SetMsgHangUp( FALSE );
	m_pID_LIST_CHATINFO->SetSelectionInfo( FALSE );	
	
}








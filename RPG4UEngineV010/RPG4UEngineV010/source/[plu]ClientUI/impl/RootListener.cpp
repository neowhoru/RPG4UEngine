/*////////////////////////////////////////////////////////////////////////
文 件 名：RootListener.cpp
创建日期：2007年11月17日
最后更新：2007年11月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "RootListener.h"
#include "UILoadMap.h"
#include "GameClientApplication.h"
#include "UIInclude.h"
#include "VUILayoutManager.h"
#include "MapViewManager.h"
#include "LoginUIListener.h"
#include "GameInPlayingUIListener.h"
#include "UILoadMap.h"
#include "UITipLog.h"
#include "ClientUIDefine.h"
#include "UICommands.h"
#include "GameCommandManager.h"
#include "ScriptWord.h"

namespace gameui
{
RootListener::RootListener()
{
}
RootListener::~RootListener()
{
}

LPCSTR RootListener::GetName()
{
	return "RootListener";
}

EGameUIType RootListener::GetType()
{
	return eUIRoot;
}

void RootListener::OnStartUp(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{
		EGameUIType	nSub;

		nSub = GetUILoginLoginUIListenerUIListener().GetType();
		m_arListeners[nSub] = &GetUILoginLoginUIListenerUIListener();

		GetUILoginLoginUIListenerUIListener().OnStartUp();

		LoadUI(gameui::eUIRoot);

		//////////////////////////////////////////
		REG_GAMECOMMAND(HighShout	);
		REG_GAMECOMMAND(Shout		);
		REG_GAMECOMMAND(Say			);
		REG_GAMECOMMAND(Guild		);
		REG_GAMECOMMAND(Party		);
		REG_GAMECOMMAND(ChatSave	);
		REG_GAMECOMMAND(ChatLoad	);
		REG_GAMECOMMAND(PartyLeave	);
		REG_GAMECOMMAND(ToGM			);

	}
	/////////////////////////////
	else if(m_arListeners[nType])
	{
		m_arListeners[nType]->OnStartUp(nType);
	}
}


void RootListener::OnClose(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{
		UnloadUI(gameui::eUIRoot);
	}
	else if(m_arListeners[nType])
	{
		m_arListeners[nType]->OnClose(nType);
	}
}


#define UI_LOADUI(Name,Show)		Name.LoadUI();Name.SetVisible(Show)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void RootListener::LoadUI(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{
		UI_LOADUI(theUIMessageBox	, FALSE);
		UI_LOADUI(theUIInputBox		, FALSE);
		UI_LOADUI(theUIProgress		, FALSE);
		UI_LOADUI(theUIGameSet		, FALSE);
		UI_LOADUI(theUIWaiting		, FALSE);
		UI_LOADUI(theUILoadMap		, FALSE);
		UI_LOADUI(theUITipLog		, FALSE);

		UI_LOADUI(theUILogin			, FALSE);

		UI_LOADUI(theUICharCreate	, FALSE);
		UI_LOADUI(theUICharSelect	, FALSE);

//#define USE_TEMP
#ifdef USE_TEMP
		theGameInPlayingUIListener.LoadInPlaying(TRUE);

		//UI_LOADUI(theUILoadMap,				TRUE);
		//UI_LOADUI(theUIFrameTop,			TRUE); //FALSE
		//UI_LOADUI(theUIMain,					TRUE); //FALSE
#endif

//#ifdef NONE_TEMP
		//theUILoadMap.LoadUI(); 
//#endif


	}
	else if(m_arListeners[nType])
	{
		m_arListeners[nType]->LoadUI(nType);
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void RootListener::UnloadUI(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{
		theUIMessageBox.UnLoadUI();
		theUIInputBox.UnLoadUI();
		theUIProgress.UnLoadUI();

		theUIGameSet.UnLoadUI();
		theUIWaiting.UnLoadUI();
		theUILoadMap.UnLoadUI();

		theUITipLog.UnLoadUI();

		theUILogin.UnLoadUI();

		theUICharCreate.UnLoadUI();
		theUICharSelect.UnLoadUI();

	}
	else if(m_arListeners[nType])
	{
		m_arListeners[nType]->UnloadUI(nType);
	}
}


void RootListener::OnProcess(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{

	}
	else if(m_arListeners[nType])
	{
		m_arListeners[nType]->OnProcess(nType);
	}
}

void RootListener::FrameMove(EGameUIType	nType,DWORD dwTick)
{
	OnProcess(nType);

	if(nType == GetType() || eUINull == nType)
	{

	}
	else if(m_arListeners[nType])
	{
		m_arListeners[nType]->FrameMove(nType,dwTick);
	}
}

void RootListener::OnEnterWorld()
{

}






}
/*////////////////////////////////////////////////////////////////////////
文 件 名：UITesting.cpp
创建日期：2007年11月15日
最后更新：2007年11月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"

//using namespace gameui;


//#define USE_TESTCHANNEL
#ifdef	USE_TESTCHANNEL

#include "LoginSceneDefine.h"
#include "UIServerGroup.h"

vector<SServerGroupInfo>	g_arGroupInfo;
UIServerGroup					g_UIServerGroup;
static		DWORD				g_dwTimer=0;
void InitGroupInfo()
{
	SServerGroupInfo		info;
	SServerChannelInfo	channel;

	////////////////////////////////
	//A
	info.m_arChannelInfo.clear();

	channel.m_fProgress		= 0.5f;
	channel.m_sChannelName	= "惊鸿";
	info.m_arChannelInfo.push_back(channel);

	channel.m_fProgress		= 0.1f;
	channel.m_sChannelName	= "猛虎";
	info.m_arChannelInfo.push_back(channel);

	channel.m_fProgress		= 0.9f;
	channel.m_sChannelName	= "兔死狐悲";
	info.m_arChannelInfo.push_back(channel);

	info.m_sGroupName		= "动物园";
	g_arGroupInfo.push_back(info);


	////////////////////////////////
	//B
	info.m_arChannelInfo.clear();

	channel.m_fProgress		= 0.2f;
	channel.m_sChannelName	= "花园";
	info.m_arChannelInfo.push_back(channel);

	channel.m_fProgress		= 0.3f;
	channel.m_sChannelName	= "小区";
	info.m_arChannelInfo.push_back(channel);

	channel.m_fProgress		= 0.8f;
	channel.m_sChannelName	= "豪园";
	info.m_arChannelInfo.push_back(channel);

	channel.m_fProgress		= 0.8f;
	channel.m_sChannelName	= "农民房";
	info.m_arChannelInfo.push_back(channel);


	info.m_sGroupName		= "住宅区";
	g_arGroupInfo.push_back(info);


	g_UIServerGroup.LoadUI();

	for(int n=0; n<g_arGroupInfo.size(); n++)
		g_UIServerGroup.AddGroupInfo(g_arGroupInfo[n]);

}

void RandomChannelInfo()
{
	if(GetTickCount() -  g_dwTimer < 3000)
		return;

	static int s=0;
	s += 10;
	//srand(GetTickCount());
	g_dwTimer =  GetTickCount();
	for(int n=0; n<g_arGroupInfo.size(); n++)
	{
		std::vector<SServerChannelInfo>& arChannel = g_arGroupInfo[n].m_arChannelInfo;
		for(int m=0; m<arChannel.size(); m++)
		{
			if(m&1)
				arChannel[m].m_fProgress = (float)((m+1)*s%100)/100.f;
			else
				arChannel[m].m_fProgress = 1.0f;// - (float)((m+1)*s%100)/100.f;
		}
	}
}

static void InitTestingData()
{
	InitGroupInfo();
}
#endif




/*////////////////////////////////////////////////////////////////////////
文 件 名：UISelectBox.h
DOC 文件：uidata\UISelectBox.uidoc
UI  名称：SelectBox
创建日期：2008年7月2日
最后更新：2008年7月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UISelectBox.h"
#include "VUILayoutManager.h"
#include "NpcCoordinateManager.h"
#include "UINpcDialog.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UISelectBox, ()  , gamemain::eInstPrioClientUI);

#define USE_UISELECTBOX_LAYOUT

namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SelectBoxUIListener& GetUISelectBoxSelectBoxUIListener()
{
	static SelectBoxUIListener staticSelectBoxUIListener;
	return staticSelectBoxUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE		( theUISelectBox, OnFrameMove )
	UIPROC_FRAME_RENDER			( theUISelectBox, OnRender )
	UIPROC_BUTTON_CLICK			( theUISelectBox, ID_BUTTON_OKOnButtonClick )
	UIPROC_BUTTON_CLICK			( theUISelectBox, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_LIST_SELECT_CHANGE	( theUISelectBox, ID_LIST_SELECTOnListSelectChange )
	MAP_TEXT_HYPER_CLICKBACK	( theUISelectBox, ID_TEXT_HyperLinkClick);

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
SelectBoxUIListener::SelectBoxUIListener()
{
}
SelectBoxUIListener::~SelectBoxUIListener()
{
}

LPCSTR SelectBoxUIListener::GetName()
{
	return "SelectBoxUIListener";
}

EGameUIType SelectBoxUIListener::GetType()
{
	return eUISelectBox;
}

void SelectBoxUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUISelectBox.SetVisible((BOOL)dwData);
		}
		break;
	}
}

BOOL SelectBoxUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void SelectBoxUIListener::Refresh(BOOL /*bExtra*/)
{
	theUISelectBox.Refresh();
}

void SelectBoxUIListener::ShowSelectBox	 (LPCSTR                    szText
														 ,const sUILIST_ITEM*		 pItemList
														 ,INT                       nItemCount
														 ,LPCSTR                    szCaption
														 ,BOOL                      bModal
														 ,void*					       PROC_BOX_CALLBACK
														 ,void*                     pData
														 ,INT                       nDataLength)
{
	theUISelectBox.Show	(szText
								,(sUILIST_ITEM* )pItemList
								,nItemCount
								,szCaption
								,bModal
								,(PROC_CALLBACK_SELECTBOX)PROC_BOX_CALLBACK
								,pData
								,nDataLength);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UISelectBox::UISelectBox()
{
	m_pID_SelectBoxFrame	= NULL;
	m_pID_BUTTON_OK		= NULL;
	m_pID_TEXT_CAPTION	= NULL;
	m_pID_TEXT_Info		= NULL;
	m_pID_LIST_SELECT 	= NULL;
	m_pID_BUTTON_CLOSE	= NULL;

	m_ProcCallback			= NULL;
	m_pData					= NULL;
}

// Frame
BOOL UISelectBox::OnFrameMove(DWORD /*dwTick*/)
{
	return TRUE;
}


BOOL UISelectBox::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_OK::OnButtonClick
BOOL UISelectBox::ID_BUTTON_OKOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	if ( m_ProcCallback )
	{
		m_ProcCallback( m_pID_LIST_SELECT->GetCurSelIndex(), m_pData );
	}
	if ( m_pData )
	{
		delete[] m_pData;
		m_pData = NULL;
	}

	_PopSelectBox();
	Refresh();

	return FALSE;
}

////////////////////////////////////////////////////
//ID_LIST_SELECT::OnListSelectChange
void UISelectBox::ID_LIST_SELECTOnListSelectChange(VUCtrlObject* pSender, sUILIST_ITEM*	/*pItem*/)
{
		ID_BUTTON_OKOnButtonClick( pSender );
}

////////////////////////////////////////////////////
//ID_BUTTON_CLOSE::OnButtonClick
BOOL UISelectBox::ID_BUTTON_CLOSEOnButtonClick(VUCtrlObject* /*pSender*/ )
{
	theUINpcDialog.CancelScript();
	return FALSE;
}

void UISelectBox::ID_TEXT_HyperLinkClick(VUCtrlObject* /*pSender*/, LPCSTR szData)
{
	theNpcCoordinateManager.ProcessHyperLink(szData);
}


BOOL UISelectBox::_PopSelectBox()
{
	if ( m_SelectBoxInfos.size() <= 0 )
		return FALSE;
	m_SelectBoxInfos.erase( m_SelectBoxInfos.begin() );
	return TRUE;
}



// 设置是否可视
void UISelectBox::SetVisible( const BOOL bVisible )
{
#ifdef USE_UISELECTBOX_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();

	m_pID_SelectBoxFrame->SetVisibleWhole( bVisible, TRUE );

	if( bVisible == FALSE )
	{
		m_pID_SelectBoxFrame->SetArrangeMode( ArrayMode_Normal );
		_PopSelectBox();
	}
	else
	{
		//Refresh();
	}
#else
		m_pID_SelectBoxFrame->SetVisible( bVisible );
#endif
	
}
void UISelectBox::SetLayoutVisible()
{
#ifdef USE_UISELECTBOX_LAYOUT
	theUILayoutManager.SetVisible(m_pID_SelectBoxFrame);
#endif
}

void UISelectBox::Refresh()
{
	int nSelectBoxNum = m_SelectBoxInfos.size();
	if ( nSelectBoxNum > 0 )
	{
		sSELECTBOX *pSelectBox;
		pSelectBox = &m_SelectBoxInfos[nSelectBoxNum-1];

		StringHandle strRet;
		theNpcCoordinateManager.ConvertHyperLink(pSelectBox->m_strContent.c_str(), strRet);
		pSelectBox->m_strContent = (LPCTSTR)strRet;

		*m_pID_TEXT_Info		= pSelectBox->m_strContent;
		*m_pID_TEXT_CAPTION	= pSelectBox->m_strCaption;
		m_ProcCallback			= pSelectBox->m_ProcCallback;
		m_pData					= pSelectBox->m_pData;

		m_pID_LIST_SELECT->Clear();
		m_pID_LIST_SELECT->AddItems( pSelectBox->m_SelectItems, pSelectBox->m_nItemCount, FALSE );

		SetVisible( TRUE );
	}
	else
	{
		SetVisible( FALSE );
	}
	theUICtrlManager.UpdateControls();
}


// 装载UI
BOOL UISelectBox::LoadUI()
{
	GetUISelectBoxSelectBoxUIListener().RegisterMe();

	m_pID_SelectBoxFrame = theUICtrlManager.LoadFrame( UIDOC_PATH( "SelectBox") );
	if ( m_pID_SelectBoxFrame == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "SelectBox") "]失败");
		return FALSE;
	}

#ifdef USE_UISELECTBOX_LAYOUT
	theUILayoutManager.AddFrame(m_pID_SelectBoxFrame);
#endif
	m_pID_SelectBoxFrame->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UISelectBox::InitControls()
{
	//INT n;

	m_pID_SelectBoxFrame->SetProcOnFrameMove	(theUISelectBoxOnFrameMove);
	m_pID_SelectBoxFrame->SetProcOnRender		(theUISelectBoxOnRender, FALSE);

	
	
	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_BUTTON_OK		= (VUCtrlButton*)	m_pID_SelectBoxFrame->FindControl( UISELECTBOX_ID_BUTTON_OK );
	m_pID_TEXT_CAPTION	= (VUCtrlText*)  	m_pID_SelectBoxFrame->FindControl( UISELECTBOX_ID_TEXT_CAPTION );
	m_pID_TEXT_Info		= (VUCtrlText*)  	m_pID_SelectBoxFrame->FindControl( UISELECTBOX_ID_TEXT_Info );
	m_pID_LIST_SELECT	 	= (VUCtrlList*)  	m_pID_SelectBoxFrame->FindControl( UISELECTBOX_ID_LIST_SELECT );
	m_pID_BUTTON_CLOSE	= (VUCtrlButton*)	m_pID_SelectBoxFrame->FindControl( UISELECTBOX_ID_BUTTON_CLOSE );
	
	
	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_BUTTON_OK );
	assert( m_pID_TEXT_CAPTION );
	assert( m_pID_TEXT_Info );
	assert( m_pID_LIST_SELECT );
	assert( m_pID_BUTTON_CLOSE );
	
	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	m_pID_BUTTON_OK		->SetProcOnButtonClick 	( theUISelectBoxID_BUTTON_OKOnButtonClick );
	m_pID_LIST_SELECT 	->SetProcOnSelectChange	( theUISelectBoxID_LIST_SELECTOnListSelectChange );
	m_pID_BUTTON_CLOSE	->SetProcOnButtonClick 	( theUISelectBoxID_BUTTON_CLOSEOnButtonClick );
	m_pID_TEXT_Info		->SetProcOnHyperLinkClick	( theUISelectBoxID_TEXT_HyperLinkClick);


	m_pID_LIST_SELECT->SetSelectionInfo(TRUE);
	return TRUE;
}


UISelectBox::~UISelectBox()
{
	//UnLoadUI();
}


// 卸载UI
BOOL UISelectBox::UnLoadUI()
{
	GetUISelectBoxSelectBoxUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "SelectBox") );
	
}

// 是否可视
BOOL UISelectBox::IsVisible()
{
	if( m_pID_SelectBoxFrame )
		return m_pID_SelectBoxFrame->IsVisible();

	return FALSE;
}

void UISelectBox::Show	(LPCSTR                     szText
								 ,const sUILIST_ITEM*		 pItem
								 ,INT                       nItemCount
								 ,LPCSTR                    szCaption
								 ,BOOL                      bModal
								 ,PROC_CALLBACK_SELECTBOX	 procCallback
								 ,void*                     pData
								 ,INT                       nDataLength)
{
	
	assert( pItem && nItemCount > 0 && nItemCount <= MAX_SELECT_ITEM );

	sSELECTBOX stSelectBox;
	stSelectBox.m_strContent	= szText;
	stSelectBox.m_strCaption	= szCaption?szCaption:"";
	stSelectBox.m_ProcCallback = procCallback;
	stSelectBox.m_bModal			= bModal?true:false;

	memcpy( stSelectBox.m_SelectItems, pItem, sizeof(sUILIST_ITEM)*nItemCount );

	stSelectBox.m_nItemCount = nItemCount;
	if ( pData && nDataLength > 0 )
	{
		stSelectBox.m_pData = new BYTE[nDataLength];
		memcpy( stSelectBox.m_pData, pData, nDataLength );
	}

	m_SelectBoxInfos.push_back( stSelectBox );
	Refresh();
	
}


};//namespace gameui


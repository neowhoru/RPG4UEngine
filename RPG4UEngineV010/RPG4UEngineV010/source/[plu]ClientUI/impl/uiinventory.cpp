/*////////////////////////////////////////////////////////////////////////
文 件 名：UIInventory.cpp
创建日期：2007年11月30日
最后更新：2007年11月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UIInventory.h"
#include "UIWareHouse.h"
#include "UIShop.h"
#include "UIMessageBox.h"
#include "UIInputBox.h"
#include "UIBargaining.h"
#include "CharActionInfo.h"
#include "ConstTextRes.h"
#include "TextResManager.h"
#include "VUCtrlIconDragManager.h"
#include "UIExitGame.h"
#include "VHero.h"
#include "UIChatInput.h"
#include "NpcCoordinateManager.h"
#include "ScreenTipManager.h"
#include "VUICursorManager.h"
#include "IV3DEngine.h"
#include "VUILayoutManager.h"
#include "QuestManager.h"
#include "InventoryUIListener.h"
#include "Map.h"
#include "GameInPlaying.h"
#include "UIInventory.inc.h"
#include "ItemManager.h"
#include "Player.h"
#include "Timer.h"
#include "GraphicResourceList.h"
#include "MouseHandler.h"
#include "GlobalInstancePriority.h"
#include "BaseContainer.h"
#include "Hero.h"
#include "UIInventorySlotUIListener.h"
#include "ItemSlot.h"
#include "UIContainHelper.h"


using namespace info;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UIInventory, ()  , gamemain::eInstPrioClientUI);

namespace gameui
{ 

const DWORD EQUIP_GROUP_ID		= 101;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
gameui::InventoryUIListener& GetUIInventoryPackUIListener()
{
	static gameui::InventoryUIListener staticPackUIListener;
	return staticPackUIListener;
}


namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE	( theUIInventory, OnFrameMove )
	UIPROC_FRAME_RENDER		( theUIInventory, OnRender )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_CLOSEOnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIInventory, ID_LISTIMG_PACKOnIconDropTo )
	UIPROC_ICON_RBUTTON_UP	( theUIInventory, ID_LISTIMG_PACKOnIconRButtonUp )
	UIPROC_ICON_RBUTTON_UP	( theUIInventory, ID_LISTIMG_PACKOnIconLDBClick )

	UIPROC_ICON_DROP_TO		( theUIInventory, EquipOnIconDropTo )
	UIPROC_ICON_LDB_CLICK	( theUIInventory, EquipOnIconLDBClick )
	UIPROC_ICON_RBUTTON_UP	( theUIInventory, EquipOnIconRButtonUp )

	UIPROC_CHECK_BOX_CHECK	( theUIInventory, PageOnCheckBoxCheck )

	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_ChangeWeaponOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_ShoppingOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_LeftOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_RightOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_FrontOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_NextOnButtonClick )
	UIPROC_ICON_DROP_TO		( theUIInventory, ID_LISTIMG_RubbishOnIconDropTo)
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_LOCKOnButtonClick )
	UIPROC_BUTTON_CLICK		( theUIInventory, ID_BUTTON_UNLOCKOnButtonClick )
};//namespace uicallback
using namespace uicallback;


UIInventory::UIInventory()
{
	m_pPlayer							= NULL;
	
	// Member
	m_pID_FRAME_PACK					= NULL;
	m_pID_BUTTON_CLOSE				= NULL;
	m_pID_BUTTON_LOCK					= NULL;
	m_pID_BUTTON_UNLOCK				= NULL;
	m_pID_LISTIMG_PACK				= NULL;
	m_pID_LISTIMG_Accouterment		= NULL;
	m_pID_LISTIMG_Accouterment1	= NULL;
	m_pID_TEXT_MONEY					= NULL;

	__ZERO(m_arEquips);
	__ZERO(m_arCheckBoxPtrs);

	m_fRotateZ				= 0;

	m_CurrentAction		= info::MESH_ACTION_idle;


	m_bInventoryLock		= FALSE;
	m_bInventoryUnlock	= FALSE;

	m_bRenderViewport		= FALSE;
}


void UIInventory::SetGoodsState( BaseSlot* pSlot,DWORD dwState )
{
	UIContainHelper	helper(m_pID_LISTIMG_PACK
									,m_pPlayer
									,MAX_SLOT_PER_PACKPAGE
									,m_nCurrentPage);
	helper.SetItemState(*pSlot, dwState);


}

void UIInventory::OnInstallContainer	(BaseContainer* pContainer)
{
	assert(pContainer);
	if(pContainer)
	{
		switch(pContainer->GetSlotIndex())
		{
		case SI_INVENTORY:
			{
				m_pID_LISTIMG_PACK->SetSlotContainer(pContainer);
				INT nDisable = pContainer->GetSlotMaxSize()/MAX_SLOT_PER_PACKPAGE;
				for(INT n=0; n<MAX_PACK_PAGE_NUM;n++)
				{
					m_arCheckBoxPtrs[n]->SetVisible(n < nDisable);
				}
				pContainer->SetCurrentPage(0);
				pContainer->SetSlotNumPerPage(MAX_SLOT_PER_PACKPAGE);
			}
			break;
		case SI_EQUIPMENT:
			{
				for(INT n=0; n<EQUIPPOS_MAX; n++)
				{
					if(m_arEquips[n])
					{
						m_arEquips[n]->SetSlotContainer(pContainer);
						m_arEquips[n]->SetGroupID(EQUIP_GROUP_ID);
						m_arEquips[n]->SetGroupContainer(TRUE);
						m_arEquips[n]->SetIndexAtGroup(n);
					}
				}
			}
			break;
		}
	}
}


void UIInventory::PushGoods(CODETYPE	itemID, UINT nIndex,UINT nCount)
{
	assert(m_pPlayer);

	sITEMINFO_BASE*		pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	pInfo = theItemInfoParser.GetItemInfo(itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);

	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;

	pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer(pInfo->m_byWasteType);

	stListItem.SetTimer(pTimer);

	stListItem.SetData	(itemID
                        ,nCount
								,pVInfo->GetIconType()
								,TRUE
								,pInfo->IsCanUseWaste());

	m_pID_LISTIMG_PACK->SetItem( &stListItem, nIndex );
}


void UIInventory::PopGoods( const UINT nIndex )
{
	m_pID_LISTIMG_PACK->RemoveItemAt(  nIndex );

	Refresh();
}

void UIInventory::ChangePage	(UINT nPage)
{
	if(!theUIInventorySlotUIListener.GetContainer())
		return;

	if(nPage == -1)
		nPage = m_nCurrentPage;

	if(nPage >= MAX_PACK_PAGE_NUM)
		return;
	UINT n;

	for(n=0; n<MAX_PACK_PAGE_NUM; n++)
	{
		m_arCheckBoxPtrs[n]->SetCheck(nPage == n);
	}
	m_nCurrentPage = nPage;

	theUIInventorySlotUIListener.GetContainer()->SetCurrentPage((SLOTPOS)m_nCurrentPage);
	/// 最后更新背包数据
	m_pID_LISTIMG_PACK->Clear();
	theUIInventorySlotUIListener.Update((SLOTPOS)(m_nCurrentPage*MAX_SLOT_PER_PACKPAGE), MAX_SLOT_PER_PACKPAGE);
	Refresh();
}

void UIInventory::PushGoods(BaseSlot* pSlot)
{
	assert(m_pPlayer && pSlot);

	UIContainHelper	helper(m_pID_LISTIMG_PACK
									,m_pPlayer
									,MAX_SLOT_PER_PACKPAGE
									,m_nCurrentPage);
	helper.PushItemEx(*pSlot);
}


void UIInventory::PopGoods( BaseSlot* pSlot )
{
	UIContainHelper	helper(m_pID_LISTIMG_PACK
									,m_pPlayer
									,MAX_SLOT_PER_PACKPAGE
									,m_nCurrentPage);
	helper.PopItemEx(*pSlot);

	Refresh();
}


void UIInventory::ForceDragFrom(SLOTPOS posSlot)
{
	INT	nPage = posSlot/MAX_SLOT_PER_PACKPAGE;
	INT	nIndex= posSlot%MAX_SLOT_PER_PACKPAGE;
	if(nPage != m_nCurrentPage)
		return;
	m_pID_LISTIMG_PACK->ForceDragFrom(nIndex);
}

void UIInventory::ShowToolTip	(SLOTPOS posSlot, DWORD dwTipKey)
{
	INT	nPage = posSlot/MAX_SLOT_PER_PACKPAGE;
	INT	nIndex= posSlot%MAX_SLOT_PER_PACKPAGE;
	if(nPage != m_nCurrentPage)
		return;
	m_pID_LISTIMG_PACK->ShowToolTip(nIndex,dwTipKey);
}


void UIInventory::PushEquip(CODETYPE	itemID, UINT nIndex,UINT nCount)
{
	//assert(!"处理EquipUI");

	assert(m_pPlayer);

	sITEMINFO_BASE*		pInfo;
	sVITEMINFO_BASE*	pVInfo;
	util::Timer*		pTimer;

	pInfo = theItemInfoParser.GetItemInfo(itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);


	///换上装备信息
	//LPCSTR szText = FMSTR("%s %s"
	//							,theTextResManager. GetString(TEXTRES_UI_EquipItem)
	//							,(LPCSTR)pVInfo->m_sName );

	//theUIChatInput.AddInfo( szText, 0xffffff00 );


	VUCtrlListImg*	pEquip = m_arEquips[nIndex];//pInfo->m_wEqPos];

	if(pEquip == NULL)
	{
		LOGINFO	("PushEquip[%s] at pos[%d], but is NULL\n"
					,(LPCSTR)pInfo->m_ItemName 
					,pInfo->m_wEqPos);
		return;
	}

	/////////////////////////////////////////////
	///物品信息显示到UI中
	IconDragListImg		stListItem;

	if(pInfo->IsPotion())
	{
		pTimer = m_pPlayer->GetPlayerCookie().GetItemCoolTimer(pInfo->m_byWasteType);

		stListItem.SetTimer(pTimer);
	}

	stListItem.SetData	(itemID
                        ,nCount
								,pVInfo->GetIconType()
								,TRUE
								,pInfo->IsCanUseWaste());

	pEquip->SetItem( &stListItem, 0 );
}


void UIInventory::PushEquip(BaseSlot* pSlot)
{
	UIContainHelper	helper(m_arEquips
									,m_pPlayer
									,EQUIPPOS_MAX);
	helper.PushItem(*pSlot);


}


void UIInventory::PopEquip( const UINT nIndex )
{
	__VERIFY2(nIndex < EQUIPPOS_MAX
				,"nIndex >= EQUIPPOS_MAX"
				,;);

	VUCtrlListImg*	pEquip = m_arEquips[nIndex];

	__CHECK2_PTR(pEquip, ;);

	IconDragListImg*		pListImg;
	CODETYPE								itemID;
	sITEMINFO_BASE*						pInfo;
	sVITEMINFO_BASE*					pVInfo;
	
	pListImg = pEquip->GetItemAt(0);
	if(pListImg->IsNull())
		return;


	///删除后处理...
	itemID	= pListImg->GetIconInfo()->GetIconID();

	if(itemID == -1)
		return;

	pInfo = theItemInfoParser.GetItemInfo(itemID);
	__VERIFY2_PTR	(pInfo
						,FMSTR("Item[%d]信息不存在\n")
						,;);

	pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
	__VERIFY2_PTR	(pVInfo
						,FMSTR("VItem[%d]信息不存在\n")
						,;);



	//LPCSTR szText = FMSTR("%s %s"
	//							,theTextResManager. GetString(TEXTRES_UI_UnequipItem)
	//							,(LPCSTR)pVInfo->m_sName );

	//theUIChatInput.AddInfo( szText, 0xffffff00 );


	///删除图标
	pEquip->RemoveItemAt(  0 );

	Refresh();
}

void UIInventory::PopEquip( BaseSlot* pSlot )
{
	UIContainHelper	helper(m_arEquips
									,m_pPlayer
									,EQUIPPOS_MAX);
	helper.PopItem(*pSlot);

	Refresh();

	//PopEquip(pSlot->GetPos());
}




// Frame
BOOL UIInventory::OnFrameMove(DWORD /*dwTick*/)
{
	
	// 死亡后要关闭背包界面
	return TRUE;
	
}


BOOL UIInventory::RenderViewport()
{
	if( !m_bRenderViewport )
		return TRUE;

	if( m_pID_FRAME_PACK == NULL )
		return FALSE;

	RECT rt;

	m_pID_FRAME_PACK->GetGlobalRect( &rt );
	m_playerModelCtrl.SetYaw(m_fRotateZ);
	m_playerModelCtrl.SetModelScale( SCALE_MESH* 10.f );
	m_playerModelCtrl.SetViewRect	(rt.left
											,rt.top 
											,rt.right - rt.left
											,rt.bottom - rt.top);

	m_playerModelCtrl.Render(0);

	m_bRenderViewport = FALSE;

	return TRUE;
}



BOOL UIInventory::OnRender(DWORD /*dwTick*/)
{
	
	RenderViewport();
	return TRUE;
	
}

BOOL UIInventory::RefreshEquip( BOOL /*bUpdataEquipView*/ )
{
	return TRUE;

}

void UIInventory::UpdataEquipView()
{
	if( theVHero.GetAnim() )
	{
		vobject::VCharacterEquipment	equip[ EQUIPPOS_MAX ];

		memcpy( &equip
				, theVHero.GetEquips()
				, sizeof(vobject::VCharacterEquipment)* EQUIPPOS_MAX );

		//m_playerModelCtrl.GetPlayerAnim().SetAvatar( theVHero.GetAnim()->GetAvatar() );
		m_playerModelCtrl.GetPlayerAnim().EquipAll(FALSE
                           ,theVHero.HasFightFlag(FIGHTFLAG_FIGHTING)
									,&equip
									,theVHero.GetModelId()
									,theVHero.GetEquips()[skeleton::eHairPart].iModelID
									,theVHero.GetEquips()[skeleton::eFacePart].iModelID
									,theVHero.GetSex()
									,theVHero.GetProfession()
									,theVHero.GetHairColorIndex());

		m_playerModelCtrl.GetPlayerAnim().UpdateHandItem(theVHero.HasFightFlag(FIGHTFLAG_FIGHTING)
                                 ,(void*)equip);

		m_playerModelCtrl.GetPlayerAnim().SetPosition( 0.0f, 0.0f, 0.0f );

		m_playerModelCtrl.GetPlayerAnim().PlayLowerAnim("idle"
                                ,PLAYMODE_NORMAL
										  ,0
										  ,MAX_ANIMATION_LOOP
										  ,ANIMATION_TIMEOUT
										  ,ANIMATION_BLENDING_TIME
										  ,"idle"
										  ,PLAYMODE_NORMAL);
		
	}
}





BOOL UIInventory::ClearListImg()
{	
	
	m_pID_LISTIMG_lWeapon->Clear();
	m_pID_LISTIMG_rWeapon->Clear();
	m_pID_LISTIMG_Helmet->Clear();
	m_pID_LISTIMG_Armour->Clear();
	m_pID_LISTIMG_Boots->Clear();
	m_pID_LISTIMG_Glove->Clear();
	m_pID_LISTIMG_Necklace->Clear();
	m_pID_LISTIMG_Ring2->Clear();
	m_pID_LISTIMG_Ring1->Clear();
	m_pID_LISTIMG_Accouterment->Clear();
	m_pID_LISTIMG_Accouterment1->Clear();
	m_pID_LISTIMG_Pants->Clear();
	m_pID_LISTIMG_Belt->Clear();

	m_pID_LISTIMG_Protector->Clear();
	m_pID_LISTIMG_Shield->Clear();
	return TRUE;
	
}



//CLOSE Button
BOOL UIInventory::ID_BUTTON_CLOSEOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	//SetVisible(FALSE);
	SetLayoutVisible();
	if(theUIWareHouse.IsVisible())
	{
		theUIWareHouse.SetVisible(FALSE);
		theCursorManager.SetUICursor(cursor::CURSORTYPE_DEFAULT);
	}

	theMouseHandler.CancelHandlingItem();

	return TRUE;
}


//ITEM LOCK BUTTON
BOOL UIInventory::ID_BUTTON_LOCKOnButtonClick(VUCtrlObject* /*pSender*/)
{
	if(m_bInventoryLock)
	{
		theCursorManager.SetUICursor(cursor::CURSORTYPE_DEFAULT);
		m_bInventoryLock = FALSE;
		if(m_bInventoryUnlock)
		{
			m_bInventoryUnlock = FALSE;
		}
	}
	else
	{
		theCursorManager.SetUICursor( cursor::CURSORTYPE_LOCKITEM );
		m_bInventoryLock = TRUE;
		if(m_bInventoryUnlock)
		{
			m_bInventoryUnlock = FALSE;
		}	
	}
	return TRUE;
}


BOOL UIInventory::ID_BUTTON_UNLOCKOnButtonClick(VUCtrlObject * /*pSender*/)
{
	
	//物品解锁 鼠标标志切换，相应开关打开
	if(m_bInventoryUnlock)
	{
		theCursorManager.SetUICursor(cursor::CURSORTYPE_DEFAULT);
		m_bInventoryUnlock = FALSE;
		if(m_bInventoryLock)
		{
			m_bInventoryLock = FALSE;
		}
	}
	else
	{
		theCursorManager.SetUICursor( cursor::CURSORTYPE_UNLOCKITEM );
		m_bInventoryUnlock = TRUE;
		if(m_bInventoryLock)
		{
			m_bInventoryLock = FALSE;
		}
	}
	return TRUE;
	
}



BOOL UIInventory::UnEquip( IconDragListImg* /*pItemDest*/, int /*nEquipWhere*/, int /*nDstIndex*/ )
{
	return TRUE;
}

// ListImg / ListEx
BOOL UIInventory::ID_LISTIMG_PACKOnIconDropTo( VUCtrlObject* /*pSender*/, VUCtrlObject* /*pThisCtrl*/,
													IconDragListImg* /*pItemDrag*/,
													IconDragListImg* /*pItemDest*/ )
{
	return FALSE;
	
}


BOOL UIInventory::ID_LISTIMG_PACKOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}


BOOL UIInventory::ID_LISTIMG_PACKOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
{
	if(theMouseHandler.GetMouseState() != eMOUSE_NONE)
	{
		theUIShop.ChangeMethod(SHOP_METHOD_NONE);
		return TRUE;
	}

	if ( !pItem->IsNull() )
	{
		if(pSender == m_pID_LISTIMG_PACK)
		{
			SLOTPOS	atPos = (SLOTPOS)(m_pID_LISTIMG_PACK->GetItemIndex(pItem) + m_nCurrentPage*MAX_SLOT_PER_PACKPAGE);//m_pID_LISTIMG_PACK->GetStartIndex();
			theMouseHandler.DoItemUsing(SI_INVENTORY, atPos);
		}
	}
	return TRUE;
	
}


BOOL UIInventory::EquipOnIconLDBClick( VUCtrlObject* /*pSender*/, IconDragListImg* /*pItem*/ )
{
	
	return FALSE;
	
}
BOOL UIInventory::EquipOnIconRButtonUp( VUCtrlObject* pSender, IconDragListImg* pItem )
{
	if ( !pItem->IsNull() )
	{
		if(pSender->GetGroupID() == EQUIP_GROUP_ID)
		{
			SLOTPOS	atPos = (SLOTPOS)pSender->GetIndexAtGroup();
			theItemManager.UseEquipmentItem(SI_EQUIPMENT, atPos);
			//theMouseHandler.DoItemUsing(SI_EQUIPMENT, atPos);
		}
	}
	return TRUE;
}

BOOL UIInventory::EquipOnIconDropTo	(VUCtrlObject*    /*pSender*/
                                 ,VUCtrlObject*    /*pThisCtrl*/
											,IconDragListImg* /*pItemDrag*/
											,IconDragListImg* /*pItemDest*/)
{
	
	return FALSE;
}


void UIInventory::PageOnCheckBoxCheck ( VUCtrlObject* pSender,  BOOL * /*bChecked*/ )
{
	if(pSender->GetGroupID() == PACK_PAGE_GROUP_ID)
	{
		ChangePage(pSender->GetIndexAtGroup());
	}
}



// 设置是否可视
void UIInventory::SetVisible( BOOL bVisible )
{
	

	m_bRenderViewport = FALSE;
	//s_UI_ID_FRAME_Compound.SetVisible(TRUE);
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_PACK->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		m_pID_FRAME_PACK->SetArrangeMode( ArrayMode_Normal );
		//if(theUIShop.IsVisible())
		//	theUIShop.ID_BUTTON_CLOSEOnButtonClick( NULL );
	}
	else
	{
		Refresh();
	}
	
}
void UIInventory::SetLayoutVisible()
{
	theUILayoutManager.SetVisible(m_pID_FRAME_PACK);
}


void UIInventory::OnPackInfoChange()
{
	MONEY dwMoney(0);
	if(singleton::ExistHero())
		dwMoney = theHero.GetMoney();

	if( m_pID_TEXT_MONEY )
		*m_pID_TEXT_MONEY = (DWORD)dwMoney;
}

void UIInventory::Refresh()
{
	OnPackInfoChange();

}




// Button
BOOL UIInventory::ID_BUTTON_ChangeWeaponOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}

// Button
BOOL UIInventory::ID_BUTTON_ShoppingOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	return TRUE;
}


//// Button
BOOL UIInventory::ID_BUTTON_RightOnButtonClick( VUCtrlObject* /*pSender*/ )
{
		m_fRotateZ += math::cPI/30.0f;
		if( m_fRotateZ > 2*math::cPI )
		{
			m_fRotateZ = 0;
		}

	return TRUE;
	
}

BOOL UIInventory::ID_BUTTON_LeftOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_fRotateZ -= math::cPI/30.0f;
	if( m_fRotateZ < 0 )
	{
		m_fRotateZ = 2*math::cPI;
	}
	return TRUE;
	
}

// Button
BOOL UIInventory::ID_BUTTON_FrontOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	m_CurrentAction--;
	if( m_CurrentAction < info::MESH_ACTION_idle )
	{
		m_CurrentAction = info::MESH_ACTION_Max-1;
	}

	if( m_CurrentAction >= info::MESH_ACTION_idle )
	{
		m_playerModelCtrl.GetPlayerAnim().PlayAnim2(	m_CurrentAction,
			PLAYMODE_NORMAL,
			1000,
			MAX_ANIMATION_LOOP,
			ANIMATION_TIMEOUT,
			info::MESH_ACTION_idle,
			PLAYMODE_NORMAL );
	}
	return TRUE;
	
}
// Button
BOOL UIInventory::ID_BUTTON_NextOnButtonClick( VUCtrlObject* /*pSender*/ )
{
	
	m_CurrentAction++;
	if( m_CurrentAction >= info::MESH_ACTION_Max )
	{
		m_CurrentAction = info::MESH_ACTION_idle;
	}

	if( m_CurrentAction >= info::MESH_ACTION_idle )
	{
		m_playerModelCtrl.GetPlayerAnim().PlayAnim2(	m_CurrentAction,
			PLAYMODE_NORMAL,
			1000,
			MAX_ANIMATION_LOOP,
			ANIMATION_TIMEOUT,
			info::MESH_ACTION_idle,
			PLAYMODE_NORMAL );
	}

	return TRUE;
	
}

// 装载UI
BOOL UIInventory::LoadUI()
{
	GetUIInventoryPackUIListener().RegisterMe();

	m_pID_FRAME_PACK = theUICtrlManager.LoadFrame( UIDOC_PATH( "Pack") );
	if ( m_pID_FRAME_PACK == 0 )
	{
		UIMessageLog("读取文件[" UIDOC_INFO( "Pack") "]失败")
			return FALSE;
	}

	theUILayoutManager.AddFrame(m_pID_FRAME_PACK);
	m_pID_FRAME_PACK->SetVisible(FALSE);

	return InitControls();
}

// 关连控件
BOOL UIInventory::InitControls()
{
	INT n;

	m_pID_FRAME_PACK->SetProcOnFrameMove			(theUIInventoryOnFrameMove);
	m_pID_FRAME_PACK->SetProcOnRender		(theUIInventoryOnRender, FALSE);
	m_pID_FRAME_PACK->SetProcOnMsgProc		( UIInventory_MsgProc );

	
	m_pID_BUTTON_CLOSE			= (VUCtrlButton*)		m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_BUTTON_CLOSE );
	m_pID_LISTIMG_PACK			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_PACK );
	m_pID_LISTIMG_lWeapon		= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_lWeapon );
	m_pID_LISTIMG_rWeapon		= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_rWeapon );
	m_pID_LISTIMG_Armour			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Armour );
	m_pID_LISTIMG_Boots			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Boots );
	m_pID_LISTIMG_Glove			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Glove );
	m_pID_LISTIMG_Necklace		= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Necklace );
	m_pID_LISTIMG_Ring2			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Ring2 );
	m_pID_LISTIMG_Ring1			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Ring1 );
	m_pID_LISTIMG_Accouterment	= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Accouterment );
	m_pID_LISTIMG_Accouterment1= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Accouterment1 );
	m_pID_LISTIMG_Pants			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Pants );
	m_pID_LISTIMG_Belt			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Belt );
	m_pID_LISTIMG_Protector		= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Protector );
	m_pID_TEXT_MONEY				= (VUCtrlText*)		m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_TEXT_MONEY );
	m_pID_LISTIMG_Helmet			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Helmet );
	m_pID_LISTIMG_Shield			= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_Shield );
	m_pID_LISTIMG_Rubbish		= (VUCtrlListImg*)	m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_LISTIMG_rubbish);

	m_pID_BUTTON_LOCK				= (VUCtrlButton*)		m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_BUTTON_LOCK );
	m_pID_BUTTON_UNLOCK			= (VUCtrlButton*)		m_pID_FRAME_PACK->FindControl(UIINVENTORY_ID_BUTTON_UNLOCK );


	for(n=0; n<MAX_PACK_PAGE_NUM; n++)
	{
		LPCSTR szName = FMSTR(UIINVENTORY_ID_CHECKBOX_PAGE "%d", n+1);
		m_arCheckBoxPtrs[n]	= (VUCtrlCheckBox*)	m_pID_FRAME_PACK->FindControl( szName);
		assert( m_arCheckBoxPtrs[n] );
	}


	assert(m_pID_LISTIMG_Rubbish);

	assert( m_pID_FRAME_PACK );
	assert( m_pID_BUTTON_CLOSE );
	assert( m_pID_LISTIMG_PACK );
	assert( m_pID_LISTIMG_lWeapon );
	assert( m_pID_LISTIMG_rWeapon );
	assert( m_pID_LISTIMG_Armour );
	assert( m_pID_LISTIMG_Boots );
	assert( m_pID_LISTIMG_Glove );
	assert( m_pID_LISTIMG_Necklace );
	assert( m_pID_LISTIMG_Ring2 );
	assert( m_pID_LISTIMG_Ring1 );
	assert( m_pID_LISTIMG_Accouterment );
	assert( m_pID_LISTIMG_Accouterment1 );
	assert( m_pID_LISTIMG_Pants );
	assert( m_pID_LISTIMG_Belt );
	assert( m_pID_LISTIMG_Protector );
	assert( m_pID_TEXT_MONEY );
	assert( m_pID_LISTIMG_Helmet );
	assert( m_pID_LISTIMG_Shield );
	assert( m_pID_BUTTON_LOCK	 );
	assert( m_pID_BUTTON_UNLOCK );


	m_pID_BUTTON_CLOSE			->SetProcOnButtonClick( theUIInventoryID_BUTTON_CLOSEOnButtonClick );

	m_pID_LISTIMG_PACK			->SetProcOnDropTo		(theUIInventoryID_LISTIMG_PACKOnIconDropTo );
	m_pID_LISTIMG_PACK			->SetProcOnRButtonUp	(theUIInventoryID_LISTIMG_PACKOnIconRButtonUp );
	m_pID_LISTIMG_PACK			->SetProcOnLButtonDBClick	(theUIInventoryID_LISTIMG_PACKOnIconLDBClick );

	for(n=0; n<EQUIPPOS_MAX; n++)
	{
		if(m_arEquips[n])
		{
			m_arEquips[n]->SetProcOnRButtonUp	(theUIInventoryEquipOnIconRButtonUp );
			m_arEquips[n]->SetProcOnLButtonDBClick		(theUIInventoryEquipOnIconLDBClick );
			m_arEquips[n]->SetProcOnDropTo		(theUIInventoryEquipOnIconDropTo );
		}
	}

	for(n=0; n<MAX_PACK_PAGE_NUM; n++)
	{
		m_arCheckBoxPtrs[n]->SetProcOnCheck(theUIInventoryPageOnCheckBoxCheck);
		m_arCheckBoxPtrs[n]->SetCheck(n==0);
		m_arCheckBoxPtrs[n]->SetGroupID		(PACK_PAGE_GROUP_ID);
		m_arCheckBoxPtrs[n]->SetIndexAtGroup(n);
	}

	m_pID_BUTTON_LOCK->	SetProcOnButtonClick(theUIInventoryID_BUTTON_LOCKOnButtonClick );
	m_pID_BUTTON_UNLOCK->SetProcOnButtonClick(theUIInventoryID_BUTTON_UNLOCKOnButtonClick );
	m_pID_LISTIMG_PACK->	SetProcOnButtonClick(IconOnClick_Pack);


	m_pID_LISTIMG_Accouterment->SetVisible(FALSE);
	m_pID_LISTIMG_Accouterment1->SetVisible(FALSE);


	RECT rt;
	m_pID_FRAME_PACK->GetGlobalRect( &rt );
	INT nPosY = SCREEN_HEIGHT/2 - rt.bottom/2;
	INT nPosX = SCREEN_WIDTH/2 - rt.right/2;
	m_pID_FRAME_PACK->SetPos( nPosX, nPosY );


	m_playerModelCtrl.Init(10);
	UISCRIPT_ENABLE ( UIOBJ_INVENTORY, m_pID_FRAME_PACK );

	return TRUE;
}


UIInventory::~UIInventory()
{
	//UnLoadUI();
	
}

VOID UIInventory::Destroy()
{
	m_playerModelCtrl.Release();
}


// 卸载UI
BOOL UIInventory::UnLoadUI()
{
		
	UISCRIPT_DISABLE( UIOBJ_INVENTORY );
	GetUIInventoryPackUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "Pack") );
	
}

// 是否可视
BOOL UIInventory::IsVisible()
{
	
	if( m_pID_FRAME_PACK )
		return m_pID_FRAME_PACK->IsVisible();

	return FALSE;
}


BOOL UIInventory::ID_LISTIMG_RubbishOnIconDropTo	(VUCtrlObject*    /*pSender*/
                                             ,VUCtrlObject*    /*pThisCtrl*/
															,IconDragListImg* /*pItemDrag*/
															,IconDragListImg* /*pItemDest*/)
{
	return TRUE;
}

};//namespace gameui

/*////////////////////////////////////////////////////////////////////////
文 件 名：UITipLog.h
DOC 文件：uidata\UITipLog.uidoc
UI  名称：TipLog
创建日期：2008年7月31日
最后更新：2008年7月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "VUCtrlManager.h"
#include "UITipLog.h"
#include "VUILayoutManager.h"
#include "ApplicationSetting.h"
#include "TipLogManager.h"
#include "GraphicResourceList.h"
#include "SoundEffect.h"
#include "MathInc.h"

//theApplicationSetting.m_TipLogMax

using namespace math;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_IMPL(UITipLog, ()  , gamemain::eInstPrioClientUI);

//#define USE_UITIPLOG_LAYOUT
using namespace info;
namespace gameui
{ 

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
TipLogUIListener& GetUITipLogTipLogUIListener()
{
	static TipLogUIListener staticTipLogUIListener;
	return staticTipLogUIListener;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace uicallback
{
	UIPROC_FRAME_FRAMEMOVE( theUITipLog, OnFrameMove )
	UIPROC_FRAME_RENDER( theUITipLog, OnRender )
	UIPROC_BUTTON_CLICK	( theUITipLog, ID_BUTTON_TIPOnButtonClick )
	UIPROC_BUTTON_CLICK	( theUITipLog, ID_BUTTON_TIPOnRButtonClick )

};//namespace uicallback
using namespace uicallback;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
TipLogUIListener::TipLogUIListener()
{
}
TipLogUIListener::~TipLogUIListener()
{
}

LPCSTR TipLogUIListener::GetName()
{
	return "TipLogUIListener";
}

EGameUIType TipLogUIListener::GetType()
{
	return eUITipLog;
}

void TipLogUIListener::SetData(DWORD dwType,LPARAM dwData)
{
	switch(dwType)
	{
	case eSetVisible:
		{
			theUITipLog.SetVisible((BOOL)dwData);
		}
		break;
	}
}

BOOL TipLogUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
{
	//switch(dwType)
	//{
	//case :
	//	{
	//	}break;
	//}
	return TRUE;
}

void TipLogUIListener::TriggerFunc(ETriggerData eData,LPARAM lpData)
{
	switch(eData)
	{
	case eAddTipLog:
		{
			sTIP_LOG* pInfo = (sTIP_LOG*)lpData;
			theUITipLog.LogTip	(pInfo->m_Tip
                              ,pInfo->m_Type
										,pInfo->m_Func
										,pInfo->m_LogType
										,pInfo->m_Sound
										,pInfo->m_bAgree
										,pInfo->m_Tip2);
		}break;
	}
}


void TipLogUIListener::Refresh(BOOL /*bExtra*/)
{
	theUITipLog.Refresh();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UITipLog::UITipLog()
{
	m_pID_FRAME_TIPLOG			= NULL;
	m_pID_TIPSTART  	= NULL;
	m_pID_TIPPOINT  	= NULL;
	m_pID_TIPEND 		= NULL;
	//m_pID_TEXT_BACK 	= NULL;
	//m_pID_TEXT_TIP  	= NULL;
	//m_pID_BUTTON_TIP	= NULL;

}

// Frame
BOOL UITipLog::OnFrameMove(DWORD dwTick)
{
	if(m_bFlying)
	{
		m_nFlyTimer -= dwTick;
		if(m_nFlyTimer <= 0)
		{
			m_bFlying = FALSE;
			((UITipLogItem*)m_pReadyItem)->SetVisible(FALSE);
			ReadyTip2List(m_pReadyItem);
		}
		else
		{
			INT	nPosX,
					nPosY;
			VUCtrlFrame*	pFrame;
			pFrame = ((UITipLogItem*)m_pReadyItem)->m_pID_FRAME_TIPLOGITEM;

			pFrame->GetPos	(nPosX,nPosY);
			pFrame->SetPos	(nPosX + (int)(dwTick*m_vFlySpeed.x)
								,nPosY + (int)(dwTick*m_vFlySpeed.y));
		}
	}

	_SUPER::UpdateTipList();

	return TRUE;
	
}


BOOL UITipLog::OnRender(DWORD /*dwTick*/)
{
	return TRUE;
}

////////////////////////////////////////////////////
//ID_BUTTON_TIP::OnButtonClick
BOOL UITipLog::ID_BUTTON_TIPOnButtonClick(VUCtrlObject* pSender )
{
	if(pSender->GetGroupID() == TIPITEMBTN_ID)
	{
		ProcessEvent(pSender, TRUE);
	}
	return TRUE;
}

BOOL UITipLog::ID_BUTTON_TIPOnRButtonClick(VUCtrlObject* pSender )
{
	if(pSender->GetGroupID() == TIPITEMBTN_ID)
	{
		ProcessEvent(pSender, FALSE);
	}
	return TRUE;
}

void	UITipLog::ProcessEvent	(VUCtrlObject* pSender, BOOL bAgree)
{
	((VUCtrlButton*)pSender)->SetEnable(FALSE);
	
	UITipLogItem* pTipItem;
	pTipItem = (UITipLogItem*)pSender->GetUserData();

	_ProcessEvent(pTipItem, bAgree);
}




// 设置是否可视
void UITipLog::SetVisible( BOOL bVisible )
{
#ifdef USE_UITIPLOG_LAYOUT
	if(bVisible != IsVisible())
		SetLayoutVisible();
	else
		m_pID_FRAME_TIPLOG->SetVisible( bVisible );

	if( bVisible == FALSE )
	{
		m_pID_FRAME_TIPLOG->SetArrangeMode( ArrayMode_Normal );
	}
	else
	{
		Refresh();
	}
#else
		m_pID_FRAME_TIPLOG->SetVisible( bVisible );
#endif
	
}
void UITipLog::SetLayoutVisible()
{
#ifdef USE_UITIPLOG_LAYOUT
	theUILayoutManager.SetVisible(m_pID_FRAME_TIPLOG);
#endif
}

void UITipLog::Refresh()
{
}


// 装载UI
BOOL UITipLog::LoadUI()
{
	GetUITipLogTipLogUIListener().RegisterMe();

	m_pID_FRAME_TIPLOG = theUICtrlManager.LoadFrame( UIDOC_PATH( "TipLog") );
	if ( m_pID_FRAME_TIPLOG == 0 )
	{
		UIMessageLog("读取文件["  "UITipLog" "]失败");
		return FALSE;
	}

#ifdef USE_UITIPLOG_LAYOUT
	theUILayoutManager.AddFrame(m_pID_FRAME_TIPLOG);
#endif
	m_pID_FRAME_TIPLOG->SetVisible(FALSE);

	return InitControls();
}


// 关连控件
BOOL UITipLog::InitControls()
{
	//INT n;

	m_pID_FRAME_TIPLOG->SetProcOnFrameMove			(theUITipLogOnFrameMove);
	m_pID_FRAME_TIPLOG->SetProcOnRender		(theUITipLogOnRender, FALSE);
	//m_pID_FRAME_TIPLOG->SetProcOnMsgProc		( UITipLog_MsgProc );

	/////////////////////////////////////////////////////
	//Connect Control => Variables 
	m_pID_TIPSTART	  = (VUCtrlPicture*)	m_pID_FRAME_TIPLOG->FindControl( UITIPLOG_ID_TIPSTART );
	m_pID_TIPPOINT	  = (VUCtrlPicture*)	m_pID_FRAME_TIPLOG->FindControl( UITIPLOG_ID_TIPPOINT );
	m_pID_TIPEND	 	= (VUCtrlPicture*)	m_pID_FRAME_TIPLOG->FindControl( UITIPLOG_ID_TIPEND );
	//m_pID_TEXT_BACK	 = (VUCtrlText*)		m_pID_FRAME_TIPLOG->FindControl( UITIPLOG_ID_TEXT_BACK );
	//m_pID_TEXT_TIP	  = (VUCtrlText*)		m_pID_FRAME_TIPLOG->FindControl( UITIPLOG_ID_TEXT_TIP );
	//m_pID_BUTTON_TIP	= (VUCtrlButton*) 	m_pID_FRAME_TIPLOG->FindControl( UITIPLOG_ID_BUTTON_TIP );
	
	
	/////////////////////////////////////////////////////
	//assert 
	assert( m_pID_TIPSTART );
	assert( m_pID_TIPPOINT );
	assert( m_pID_TIPEND );
	//assert( m_pID_TEXT_BACK );
	//assert( m_pID_TEXT_TIP );
	//assert( m_pID_BUTTON_TIP );
	
	
	/////////////////////////////////////////////////////
	//Connect the control events ... 
	//m_pID_BUTTON_TIP	->SetProcOnButtonClick	( theUITipLogID_BUTTON_TIPOnButtonClick );
	//m_pID_BUTTON_TIP	->SetProcOnButtonClick	( theUITipLogID_BUTTON_TIPOnRButtonClick ,TRUE);
	//m_pID_BUTTON_TIP->SetVisible(FALSE);
	//m_pID_TEXT_TIP->SetVisible(FALSE);
	//m_pID_TEXT_BACK->SetVisible(FALSE);


	_SUPER::Init();

	UITipLogItem*  pReadyItem = (UITipLogItem*)m_pReadyItem;
	pReadyItem->SetVisible(FALSE);
	//设置默认信息
	INT nPosX,nPosY;
	m_pID_TIPPOINT->GetPos(nPosX,nPosY);
	pReadyItem->m_pID_FRAME_TIPLOGITEM->SetPos(nPosX - pReadyItem->m_pID_FRAME_TIPLOGITEM->GetWidth(),nPosY);



	/////////////////////////////////////////////////////
	//LogTip("测试之中1",eTIP_ADD,NULL);g_CurTime++;
	//LogTip("测试之中2",eTIP_ADD,NULL);g_CurTime++;
	//LogTip("测试之中3",eTIP_ADD,NULL);g_CurTime++;
	//LogTip("测试之中4",eTIP_ADD,NULL);g_CurTime++;
	//LogTip("测试之中5",eTIP_ADD,NULL);g_CurTime++;
	//LogTip("测试之中6",eTIP_ADD,NULL);g_CurTime++;

	return TRUE;
}


UITipLog::~UITipLog()
{
	//UnLoadUI();
	
}


// 卸载UI
BOOL UITipLog::UnLoadUI()
{
	if(m_pReadyItem)
	{
		((UITipLogItem*)m_pReadyItem)->UnLoadUI();
	}

	_SUPER::Release();

	GetUITipLogTipLogUIListener().UnregisterMe();
	return theUICtrlManager.RemoveFrame( UIDOC_PATH( "TipLog") );
	
}

// 是否可视
BOOL UITipLog::IsVisible()
{
	if( m_pID_FRAME_TIPLOG )
		return m_pID_FRAME_TIPLOG->IsVisible();

	return FALSE;
}


void UITipLog::_GetLogTipPos	(POINT& ptStart
								      ,POINT& ptDest)
{
	INT x,y;
	m_pID_TIPSTART->GetPos(x,y);
	ptStart.x = x;
	ptStart.y = y;

	m_pID_TIPPOINT->GetPos(x,y);
	ptDest.x = x;
	ptDest.y = y;
}

void UITipLog::_OnLogTip		(const POINT& ptStart
                              ,const POINT& /*ptDest*/)
{
	UITipLogItem* pReadyItem = ((UITipLogItem*)m_pReadyItem);
	pReadyItem->m_pID_FRAME_TIPLOGITEM->SetPos(ptStart.x - pReadyItem->m_pID_FRAME_TIPLOGITEM->GetWidth()
															,ptStart.y);	
	pReadyItem->SetVisible(TRUE);
}




void	UITipLog::SetLogTip	(TipLogItem*	pFrameSrc
									,LPCSTR			szTip
									,sTIP_SETTING*	pSetting
									,PROC_TIPLOG	/*func*/
									,BOOL				bAgree
									,LPCSTR			/*szTip2*/)
{
	UITipLogItem*	pFrame = (UITipLogItem*)pFrameSrc;

	__CHECK2_PTR(pFrame,;);
	//__CHECK2(pFrame->GetGroupID() == TIPITEM_ID,;);

	pFrame->SetText(szTip,pSetting->m_TextColor,pSetting->m_ShadowColor);

	RESOURCE_INFO*	pRes;
	INT				nIndex;
	nIndex	= bAgree? sTIP_SETTING::STATE_AGREE :  sTIP_SETTING::STATE_DISAGREE;
	pRes		= theGraphicResourceList.GetItemInfo(pSetting->m_Icons[nIndex] ) ;
	if(pRes)
		pFrame->m_pID_BUTTON_TIP->SetPicInfo((LPCSTR)pRes->sFilename);

	theUICtrlManager.BringToBottom( pFrame->m_pID_FRAME_TIPLOGITEM );
}


TipLogItem*	UITipLog::_CreateTipItem(INT nIndex)
{
	//VUCtrlText			*pTEXT_BACK;
	//VUCtrlText			*pTEXT_TIP;
	//VUCtrlButton 		*pBUTTON_TIP;


	UITipLogItem* pFrame = new UITipLogItem;

	pFrame->LoadUI(nIndex);
	pFrame->SetVisible(FALSE);

	pFrame->m_pID_BUTTON_TIP->SetProcOnButtonClick	( theUITipLogID_BUTTON_TIPOnButtonClick );
	pFrame->m_pID_BUTTON_TIP->SetProcOnButtonClick	( theUITipLogID_BUTTON_TIPOnRButtonClick,TRUE );
	pFrame->m_pID_BUTTON_TIP->SetGroupID(TIPITEMBTN_ID);
	pFrame->m_pID_BUTTON_TIP->SetIndexAtGroup(nIndex);
	pFrame->m_pID_BUTTON_TIP->SetUserData((LPARAM)pFrame);

	theUICtrlManager.BringToBottom( pFrame->m_pID_FRAME_TIPLOGITEM );
	return (TipLogItem*)pFrame;
}


void UITipLog::_DestroyTipItem	(TipLogItem* pItem)
{
	SAFE_DELETE(pItem);
}



void  UITipLog::_OnReadyTip2List	(TipLogItem* pReady)
{
	((UITipLogItem*)pReady)->m_pID_BUTTON_TIP->SetEnable(TRUE);
}

void UITipLog::_OnUpdateTipList(TipLogItem* pTipItem, BOOL bShow)
{
	if(!bShow)
	{
		((UITipLogItem*)pTipItem)->SetVisible(FALSE);
	}
}

void  UITipLog::UpdateTipList	(TipLogItem* pTipItemSrc,BOOL bReadyTip)
{

	UITipLogItem* pTipItem = (UITipLogItem*)pTipItemSrc;


	if(m_TipItems.size() == 0)
		return;

	///更新List位置
	TipItemListIt it;
	INT	nPosX,
			nPosY;
	INT	nLastX,
			nLastY;
	INT	nStepX,
			nStepY;
	INT	nIndex;

	nIndex = (bReadyTip?1:0);

	if(pTipItem == NULL)
	{
		pTipItem = (UITipLogItem*)m_TipItems.front();
	}

	if(!pTipItem || !pTipItem->m_pID_FRAME_TIPLOGITEM)
		return;

	m_pID_TIPPOINT	->GetPos(nPosX, nPosY);
	m_pID_TIPEND	->GetPos(nLastX, nLastY);
	nStepX = 0;
	nStepY = (nLastY - nPosY)/theApplicationSetting.m_TipLogMax;
	nStepX = 0;	//Max(Abs(nStepX), pTipItem->m_pID_FRAME_TIPLOGITEM->GetWidth()) * Sign(nStepX);
	nStepY = Max(Abs(nStepY), pTipItem->m_pID_FRAME_TIPLOGITEM->GetHeight()) * Sign(nStepY);

	for(it = m_TipItems.begin();it != m_TipItems.end(); it++,nIndex++)
	{
		pTipItem = (UITipLogItem*)*it;

		pTipItem->m_pID_FRAME_TIPLOGITEM->SetPos	(nPosX - pTipItem->m_pID_FRAME_TIPLOGITEM->GetWidth()
																,nPosY + nIndex * nStepY);
		pTipItem->SetVisible(TRUE);
		theUICtrlManager.BringToBottom( pTipItem->m_pID_FRAME_TIPLOGITEM );
	}
}



};//namespace gameui


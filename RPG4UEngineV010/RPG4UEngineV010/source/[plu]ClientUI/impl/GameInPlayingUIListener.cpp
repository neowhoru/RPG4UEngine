/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInPlayingUIListener.cpp
创建日期：2006年12月20日
最后更新：2006年12月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameInPlayingUIListener.h"
#include "VHero.h"
#include "GameClientApplication.h"
#include "UIInclude.h"
#include "VUILayoutManager.h"
#include "MapViewManager.h"
#include "UIMenuPlayer.h"
#include "Hero.h"
#include "ConstSlot.h"
#include "UIInventorySlotUIListener.h"
#include "UIEquipSlotUIListener.h"
#include "UIQuickSlotUIListener.h"
#include "LoginUIListener.h"
#include "UIActionSlotUIListener.h"
#include "UIEmotionSlotUIListener.h"
#include "UIStyleSlotUIListener.h"
#include "SkillStorageManager.h"
#include "UISkillItemUnitRender.h"
#include "UIWareHouse.h"
#include "UIWareHouseSlotUIListener.h"
#include "UIShop.h"
#include "UIShopSlotUIListener.h"
#include "UIItemCompound.h"
#include "UIItemCompositeSlotUIListener.h"
#include "UIItemCompositeSubResultSlotUIListener.h"
#include "UIItemCompositeTargetSlotUIListener.h"
#include "UIItemLogSlotUIListener.h"
#include "UIItemMake.h"
#include "UIItemInlay.h"
#include "UIItemEnchant.h"
#include "UIItemLog.h"
#include "UIQuest.h"
#include "UITeamManager.h"
#include "UITipLog.h"
#include "UILoadMap.h"
#include "ClientUIDefine.h"
#include "UIBargaining.h"
#include "UIVendor.h"
#include "UIVendorSlotUIListener.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
using namespace gameui;
GLOBALINST_SINGLETON_PTR_IMPL(GameInPlayingUIListener);


namespace gameui
{

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameInPlayingUIListener::GameInPlayingUIListener()
{
	GLOBALINST_SINGLETON_PTR_INIT(GameInPlayingUIListener);
}

GameInPlayingUIListener::~GameInPlayingUIListener()
{
	GLOBALINST_SINGLETON_PTR_FREE(GameInPlayingUIListener);
}

LPCSTR GameInPlayingUIListener::GetName()
{
	return "GameInPlayingUIListener";
}

EGameUIType GameInPlayingUIListener::GetType()
{
	return eUIGameInPlaying;
}


void GameInPlayingUIListener::LoadInPlaying	(BOOL bReady)
{
	const DWORD _UI_SHOW	= TRUE;

	if(bReady)
	{
		UI_LOADUI(theUIMenuPlayer,						_UI_SHOW);
		UI_LOADUI(theUIMenuHero,						_UI_SHOW);

	//#ifndef USE_TEMP
		//UI_LOADUI(theUILoadMap,				TRUE);
	//#endif
		theUITeamManager.LoadUI( );		
		//theUITeamHero.SetVisible(FALSE);

		///////////////////////////////
		//LOADING UP
		//UI_LOADUI(theUITipLog,				_UI_SHOW); //FALSE
	//#ifdef USE_TEMP
		UI_LOADUI(theUIFrameTop,			_UI_SHOW); //FALSE
		UI_LOADUI(theUIMain,					_UI_SHOW); //FALSE
	//#endif
		UI_LOADUI(theUISystemLog,			_UI_SHOW); //FALSE
		UI_LOADUI(theUIItemLog,				FALSE);
		return;
	}

	UI_LOADUI(theUIMiniMap,						_UI_SHOW); //FALSE

	UI_LOADUI(theUIActionProgress,				FALSE);
	UI_LOADUI(theUIInventory			,			FALSE);
	UI_LOADUI(theUIShop			,					FALSE);	
	UI_LOADUI(theUIWareHouse	,					FALSE);

	UI_LOADUI(theUIItemCompound,					FALSE);
	UI_LOADUI(theUIItemMake,						FALSE);
	UI_LOADUI(theUIItemInlay,						FALSE);
	UI_LOADUI(theUIItemEnchant,					FALSE);

	UI_LOADUI(theUIBaseProperty,					FALSE);			
	UI_LOADUI(theUISkill,							FALSE);			
	UI_LOADUI(theUINpcDialog,						FALSE);			
	UI_LOADUI(theUIChatInput,						FALSE);				
	UI_LOADUI(theUIBargaining,						FALSE);			
	UI_LOADUI(theUIVendor,							FALSE);			
	UI_LOADUI(theUIQuest,							FALSE);			
	UI_LOADUI(theUIExitGame,						FALSE);
	UI_LOADUI(theUITarget,							FALSE);
	UI_LOADUI(theUIHeroRelive,						FALSE);
	UI_LOADUI(theUIObjectMouseTip,				FALSE);
	//UI_LOADUI(s_CUI_ID_FRAME_Help,				FALSE);
	UI_LOADUI(theUIKeyboard,						FALSE);
	UI_LOADUI(theUIFriend,							FALSE);
	UI_LOADUI(theUISelectBox,						FALSE);
	UI_LOADUI(theUIMeunFriend,						FALSE);
	UI_LOADUI(theUIHeroState,						FALSE);



	//UI_LOADUI(s_CUI_ID_FRAME_GUILD_CREATE,	FALSE);
	//UI_LOADUI(s_UI_Guild, FALSE);

	theUIAction.LoadUI();			
}

void GameInPlayingUIListener::OnStartUp(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{
		g_V3DConfig.m_bHeroMultiRegTexture = TRUE;

	
		LoadInPlaying(TRUE);

		LoadInPlaying(FALSE);


		///////////////////////////////////////
		/// 背包容器监听设置
		theUIInventory.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_INVENTORY,	&theUIInventorySlotUIListener);
		theHero.InstallSlotListener(SI_EQUIPMENT,	&theUIEquipSlotUIListener);

		/// Quick Bar监听
		theUIMain.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_QUICK,		&theUIQuickSlotUIListener);

		///技能动作监听
		theUIAction.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_SKILL,		&theUIActionSlotUIListener);
		theHero.InstallSlotListener(SI_SKILL,		&theUIStyleSlotUIListener);
		theHero.InstallSlotListener(SI_SKILL,		&theUIEmotionSlotUIListener);


		///技能容器监听
		theUISkill.InitSkillTree();

		theUISkillItemUnitRender.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_SKILL,		&theUISkillItemUnitRender);


		///仓库监听
		theUIWareHouse.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_WAREHOUSE,		&theUIWareHouseSlotUIListener);

		///仓库监听
		theUIShop.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_NPCSHOP,		&theUIShopSlotUIListener);

		//物品日志监听
		theUIItemLog.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_ITEMLOG,		&theUIItemLogSlotUIListener);

		///物品制造监听
		theUIItemCompound.SetPlayer(&theHero);
		theUIItemMake.SetPlayer(&theHero);
		theUIItemInlay.SetPlayer(&theHero);
		theUIItemEnchant.SetPlayer(&theHero);

		theUIItemCompound.AttachToItemCompound();

		///交易监听
		theUIBargaining.SetPlayer(&theHero);
		theUIBargaining.AttachToItemCompound();

		theUIVendor.SetPlayer(&theHero);
		theHero.InstallSlotListener(SI_VENDOR_SELL,	&theUIVendorSlotUIListener);

	}
}

void GameInPlayingUIListener::OnClose(EGameUIType	nType)
{
	if(nType == GetType() || eUINull == nType)
	{
		#define UI_UNLOADUI(Name)		Name.UnLoadUI()

		theUILayoutManager.Restore();
		theMapViewManager.ClearNpcInfo();

		UI_UNLOADUI(theUIInventory);
		UI_UNLOADUI(theUIWareHouse);

		UI_UNLOADUI(theUIItemCompound);
		UI_UNLOADUI(theUIItemMake);
		UI_UNLOADUI(theUIItemInlay);
		UI_UNLOADUI(theUIItemEnchant);
		UI_UNLOADUI(theUIItemLog);

		UI_UNLOADUI(theUIExitGame);
		UI_UNLOADUI(theUIKeyboard);
		UI_UNLOADUI(theUIMenuPlayer);
		UI_UNLOADUI(theUIMenuHero);
		UI_UNLOADUI(theUIShop);
		UI_UNLOADUI(theUIBaseProperty);
		UI_UNLOADUI(theUISkill);
		UI_UNLOADUI(theUINpcDialog);
		UI_UNLOADUI(theUIMain);
		UI_UNLOADUI(theUIChatInput);				
		UI_UNLOADUI(theUIMiniMap);
		UI_UNLOADUI(theUIFrameTop);
		UI_UNLOADUI(theUIBargaining);
		UI_UNLOADUI(theUIVendor);
		UI_UNLOADUI(theUIQuest);
		UI_UNLOADUI(theUIExitGame);
		UI_UNLOADUI(theUITarget);
		UI_UNLOADUI(theUIHeroRelive);
		UI_UNLOADUI(theUIAction);
		UI_UNLOADUI(theUIFriend);
		UI_UNLOADUI(theUISelectBox);
		UI_UNLOADUI(theUIMeunFriend);
		UI_UNLOADUI(theUIHeroState);
		UI_UNLOADUI(theUIObjectMouseTip);
		//UI_UNLOADUI(theUITipLog);

		theUIInventory.Destroy();
		g_V3DConfig.m_bHeroMultiRegTexture = FALSE;
	}
}

void GameInPlayingUIListener::FrameMove(EGameUIType	nType,DWORD /*dwTick*/)
{
	if(nType == GetType() || eUINull == nType)
	{

	}//if(nType == GetType() || eUINull == nType)
}


void GameInPlayingUIListener::OnEnterWorld()
{
	theUITeamHero.SetVisible( TRUE );
	theUIWaiting.SetVisible( FALSE );
	theUILoadMap.SetVisible(FALSE);
}

//void GameInPlayingUIListener::SetData(DWORD dwType,LPARAM dwData)
//{
//	switch()
//	{
//	case :
//		{
//		}
//		break;
//	}
//
//	case eSetVisible:
//		.SetVisable((BOOL)dwData);
//}
//
//BOOL GameInPlayingUIListener::GetData(DWORD /*dwType*/,void* /*pRet*/)
//{
//	switch()
//	{
//	case :
//		{
//		}
//		break;
//	}
//
//	return TRUE;
//}

};//namespace gameui


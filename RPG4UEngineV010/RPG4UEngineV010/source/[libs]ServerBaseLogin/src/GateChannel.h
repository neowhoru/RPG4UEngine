/*////////////////////////////////////////////////////////////////////////
文 件 名：GateChannel.h
创建日期：2007年12月6日
最后更新：2007年12月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __GATECHANNEL_H__
#define __GATECHANNEL_H__
#pragma once

#include <GateChannelDefine.h>
#include <Timer.h>

class GateSession;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASELOGIN_API GateChannel
{
public:
	GateChannel(void);
	virtual ~GateChannel();

public:
	virtual DWORD GetFactoryType() = 0;

public:
	virtual void Init			();
	virtual void Release		();
	virtual void DisplayInfo();
	virtual void Update		()=0;

public:
	virtual void OnGetGameStatus	(DWORD dwTimeStamp, DWORD dwUserCount);

	virtual BOOL SendToGateServer	(VOID* pMsg, WORD wSize)=0;
	virtual DWORD GetServerStatus	()=0;

public:
	VG_DWORD_PROPERTY(TimeStamp);
	VG_DWORD_PROPERTY(GroupID);		//与世界ID一样
	VG_DWORD_PROPERTY(ChannelID);		//与GateID一样
	VG_DWORD_PROPERTY(UserCount);
	VG_DWORD_PROPERTY(MaxUserCount);

	VG_WORD_PROPERTY(GateClientSidePort	);
	VG_CSTR_PROPERTY(GateClientSideIP	, MAX_IP_LENGTH);
	VG_CSTR_PROPERTY(ChannelName			, MAX_CHANNEL_NAME_LEN);

	VG_PTR_PROPERTY(GateSession			, GateSession);

protected:
	util::Timer		m_GateStatusTimer;
	DWORD				m_dwServerStatus;
};


#endif //__GATECHANNEL_H__
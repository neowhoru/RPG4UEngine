/*////////////////////////////////////////////////////////////////////////
文 件 名：User.h
创建日期：2007年2月30日
最后更新：2007年2月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __USER_H__
#define __USER_H__
#pragma once


#include <commonDefine.h>
#include <UserDefine.h>
#include <IUserSession.h>

using namespace std;

struct sFRIEND_INFO_BASE;
struct sBLOCK_INFO_BASE;
struct MSG_OBJECT_BASE;

class User;
class Zone;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASELOGIN_API User : public IUserSession
{
	enum { WAITING_TICK = 1000 * 100 };
	enum { FRIEND_STAT_LOGIN = 0, FRIEND_STAT_LOGOUT = 1 };

public:
	User();
	virtual ~User();

public:
	virtual USER_TYPE GetFactoryType(){return GetUserType();}

public:
	virtual BOOL	Init();
	virtual VOID	Release();
	virtual VOID	Update();
	virtual VOID	OnRedirect(){}

	virtual VOID	Logout					();
	virtual BOOL	LogoutAtGate	(BOOL bChangeChannel=FALSE);
	virtual VOID	ProcessLogout			();

	virtual VOID	OnAccept			( DWORD dwNetworkIndex );
	virtual VOID	OnDisconnect	();
	virtual VOID	OnRecv			( BYTE *pMsg, WORD wSize );

	virtual void	OnLogText		(LPCTSTR pszLog);

	virtual void	SetAccountLower(LPCSTR szID);

public:

	VOID		StartTimeCheck()	;
	BOOL		IsTimeout()			;


protected:
	VG_BYTE_PROPERTY(BillingType);
	VG_CSTR_PROPERTY(BillingInfo,MAX_INFO_LENGTH);
	VG_BUF_PROPERTY (SerialKey,	BYTE, MAX_AUTH_SERIAL_LENGTH);	//序列信息
	VG_BUF_PROPERTY (MD5Key,		BYTE, MAX_MD5_BUF_LENGTH);	//序列信息

	VG_TYPE_PROPERTY(UserType, eUSER_TYPE);
	VG_DWORD_PROPERTY(AgentServerID);
	VG_DWORD_PROPERTY(NetworkIndex);
	VG_TYPE_PROPERTY(Password,		StringHandle);
	VG_TYPE_PROPERTY(CharPassword,StringHandle);
	VG_BYTE_PROPERTY(UserStatus);
	VG_DWORD_PROPERTY(GUID);

	VG_DWORD_PROPERTY(AuthSequence);	//自动生成序列号，用它来关联验证中的用户，READY时生成
	VG_DWORD_PROPERTY(AuthID);			//验证编号
	VG_DWORD_PROPERTY(Status);
	VG_CSTR_PROPERTY(Account, MAX_ID_LENGTH);	//帐号信息，须转换为小写与KeyUserList配合

	BOOL					m_bLogoutProcessed;
	DWORD					m_dwTimeoutTick;
};

#include "User.inl"

#endif //__USER_H__
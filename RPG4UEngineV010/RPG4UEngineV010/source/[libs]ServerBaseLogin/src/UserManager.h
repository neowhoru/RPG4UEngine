/*////////////////////////////////////////////////////////////////////////
文 件 名：UserManager.h
创建日期：2007年2月5日
最后更新：2007年2月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __USERMANAGER_H__
#define __USERMANAGER_H__
#pragma once

#include <GlobalInstanceSingletonDefine.h>
#include <UserDefine.h>
#include <KeyUserList.h>
using namespace std;

class User;
class GuidUserList;
class KeyUserList;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASELOGIN_API UserManager
{
public:
	UserManager(void);
	virtual ~UserManager(void);

public:
	//实例创建管理
	User*					AllocUser	(USER_TYPE type /* =USER_NULL*/)	;
	void              FreeUser		(User* pUser)	;

public:
	virtual VOID		Init		();
	virtual VOID		Release	();
	virtual User*		FindUser	( DWORD dwKey );
	virtual User*		FindUser	( LPCSTR strAccount );

	virtual DWORD		GetKeyUserAmount();

	virtual VOID		AddUser		( User *pUser, BOOL bUserAccount=FALSE );
	virtual VOID		RemoveUser	( User *pUser, BOOL bUserAccount=FALSE );

	template<class OPR>
	void ForEach(OPR& opr);

protected:
	KeyUserList		*m_pKeyUserList;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(UserManager , _SERVERBASELOGIN_API );
//extern API_NULL UserManager& singleton::GetUserManager();
#define theUserManager  singleton::GetUserManager()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<class OPR>
void UserManager::ForEach(OPR& opr)
{
	if(!m_pKeyUserList)
		return;
	m_pKeyUserList->ForEach(opr);
}


#endif //__USERMANAGER_H__
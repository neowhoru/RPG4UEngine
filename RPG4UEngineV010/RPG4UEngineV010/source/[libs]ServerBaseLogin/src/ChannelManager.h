/*////////////////////////////////////////////////////////////////////////
文 件 名：ChannelManager.h
创建日期：2007年12月8日
最后更新：2007年12月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHANNELMANAGER_H__
#define __CHANNELMANAGER_H__
#pragma once

#include "GateChannel.h"
#include <GlobalInstanceSingletonDefine.h>
#include <TMemoryPoolFactory.h>
#include <Timer.h>

using namespace util;

class GateChannel;
class User;

using namespace std;
using namespace util;
typedef map<DWORD, GateChannel*>					ChannelMap;
typedef ChannelMap::iterator						ChannelMapIt;
typedef pair<DWORD, GateChannel*>				ChannelMapValue;
typedef vector<GateChannel*>						ChannelArray;
typedef ChannelArray::iterator					ChannelArrayIt;
typedef ChannelArray::reverse_iterator			ChannelArrayItR;
typedef TMemoryPoolFactory<GateChannel>		GateChannelPool;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASELOGIN_API ChannelManager
{
public:
	ChannelManager(void);
	virtual ~ChannelManager(void);

public:
	virtual VOID				Init		();
	virtual VOID				Release	();
	virtual VOID				Update	()=0;


	virtual GateChannel*		AddChannel		( DWORD dwChannelID );
	virtual VOID				RemoveChannel	( DWORD dwChannelID );
	virtual VOID				RemoveChannel	( GateChannel* pChannel );
	virtual GateChannel*		GetChannel		( DWORD dwChannelID );
	GateChannel*				GetChannelAt	(UINT nIndex);


	virtual VOID				DisplayChannelInfo()=0;
	virtual void				SyncInfoToUsers	()=0;

public:
	UINT					GetChannelCount();

	GateChannel*		AllocChannel	();
	VOID					FreeChannel		( GateChannel *pChannel );

	template<class OPR>	void ForEach(OPR& opr);


private:
	ChannelArray		m_Channels;
	ChannelMap			m_ChannelMap;

protected:
	Timer					m_StatisticsTimer;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(ChannelManager , _SERVERBASELOGIN_API);
//extern _SERVERBASELOGIN_API ChannelManager& singleton::GetChannelManager();
#define theChannelManager  singleton::GetChannelManager()

#include "ChannelManager.inl"

#endif //__CHANNELMANAGER_H__
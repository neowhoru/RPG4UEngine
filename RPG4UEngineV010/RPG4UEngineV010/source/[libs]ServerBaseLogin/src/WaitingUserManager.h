/*////////////////////////////////////////////////////////////////////////
文 件 名：WaitingUserManager.h
创建日期：2007年2月12日
最后更新：2007年2月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __WAITINGUSERMANAGER_H__
#define __WAITINGUSERMANAGER_H__
#pragma once

#pragma once

#include <GlobalInstanceSingletonDefine.h>
#include <hash_map>

class User;
using namespace stdext;
using namespace std;

typedef hash_map<DWORD, User*>		WaitingUserMap;
typedef WaitingUserMap::iterator		WaitingUserMapIt;
typedef pair<DWORD, User*>				WaitingUserMapPair;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASELOGIN_API WaitingUserManager
{
public:
	WaitingUserManager(void);
	virtual ~WaitingUserManager(void);

public:
	virtual VOID		Init			();
	virtual VOID		Release		();
	virtual VOID		Update		();
	virtual VOID		AddUser		(DWORD dwAuthUserID, User *pUser );
	virtual VOID		RemoveUser	(DWORD dwAuthUserID );
	virtual User*		FindUserWithAuthID( DWORD dwAuthUserID );
	virtual User*		FindUserWithGuid( DWORD dwGuid );

	template<class OPR>	void ForEach(OPR& opr);
private:
	WaitingUserMap		m_mapUsers;
	WaitingUserMap		m_mapGuidUsers;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(WaitingUserManager , _SERVERBASELOGIN_API);
//extern API_NULL WaitingUserManager& singleton::GetWaitingUserManager();
#define theWaitingUserManager  singleton::GetWaitingUserManager()



template<class OPR>	void WaitingUserManager::ForEach(OPR& opr)
{
	WaitingUserMapIt it;
	for( it = m_mapUsers.begin(); it != m_mapUsers.end(); ++it )
	{
		opr(it->second);
	}
}


#endif //__WAITINGUSERMANAGER_H__
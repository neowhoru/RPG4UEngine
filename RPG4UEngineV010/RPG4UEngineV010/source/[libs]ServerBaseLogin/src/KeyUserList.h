/*////////////////////////////////////////////////////////////////////////
文 件 名：KeyUserList.h
创建日期：2007年2月19日
最后更新：2007年2月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARNAMEUSERLIST_H__
#define __CHARNAMEUSERLIST_H__
#pragma once


class User;

using namespace std;
typedef map<string, User*>				NameUserMap;
typedef NameUserMap::iterator			NameUserMapIt;
typedef pair<string, User*>			NameUserMapPair;

typedef map<DWORD, User*>				KeyUserMap;
typedef KeyUserMap::iterator			KeyUserMapIt;
typedef pair<DWORD, User*>				KeyUserMapPair;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASELOGIN_API KeyUserList
{
public:
	KeyUserList(void);
	~KeyUserList(void);

public:
	BOOL					Init();
	VOID					Release();

public:
	VOID					Add				(User *pUser );
	VOID					AddUserAccount	(User *pUser);
	VOID					Remove			(DWORD dwKey );
	VOID					RemoveAccount	(LPCTSTR szAccount );
	User*					Find				(DWORD dwKey);
	User*					Find				(LPCSTR szID);
	VOID					SendToAll		( BYTE *pMsg, WORD wSize );
	inline DWORD		GetNumberOfUsers() { return (DWORD)m_mapUsers.size(); }

public:
	template<class OPR>
	void ForEach(OPR& opr);

private:
	KeyUserMap		m_mapUsers;	
	NameUserMap		m_mapUsersByAccount;	
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

template<class OPR>
void KeyUserList::ForEach(OPR& opr)
{
	KeyUserMapIt it;
	for(it = m_mapUsers.begin(); it != m_mapUsers.end(); it++)
	{
		if(!opr(*it->second))
			return;
	}
}

#endif //__CHARNAMEUSERLIST_H__
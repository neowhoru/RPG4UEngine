#include <windows.h>
#include "resource.h"

BOOL CALLBACK MainDlgProc( HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam );
HINSTANCE g_hInst;
HWND g_hDlgMain;
HWND g_hwEditCmd;
HWND g_hwList;

int APIENTRY WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdParam, int nCmdShow )
{
	g_hInst = hInstance;

	DialogBox( g_hInst, MAKEINTRESOURCE(IDD_MAINDIALOG), HWND_DESKTOP, MainDlgProc );

	return 0;
}

BOOL CALLBACK MainDlgProc( HWND hDlg, UINT iMessage, WPARAM wParam, LPARAM lParam )
{
	switch( iMessage )
	{
	case WM_INITDIALOG:
		g_hDlgMain = hDlg;
		g_hwEditCmd = GetDlgItem( hDlg, IDC_EDIT_CMD );
		g_hwList	= GetDlgItem( hDlg, IDC_LIST1 );
		return TRUE;

	case WM_COMMAND:
		switch( LOWORD(wParam) )
		{
		case IDC_BUTTON_INPUT:
			{
			char szCmd[256];
			GetWindowText( g_hwEditCmd, szCmd, 256 );
			SendMessage( g_hwList, LB_ADDSTRING, 0, (LPARAM)szCmd );
			}
			break;

		case IDOK:
		case IDCANCEL:
			EndDialog( hDlg, 0 );
			return TRUE;
		}
	}
	return FALSE;
}
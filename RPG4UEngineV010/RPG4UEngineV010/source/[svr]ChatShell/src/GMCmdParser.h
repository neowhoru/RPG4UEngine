#pragma once

#include <iostream>
#include <string>
#include <vector>
using namespace std;

#define MAX_ARGUMENT	10

class GMCmdParser
{
public:
	GMCmdParser(void);
	~GMCmdParser(void);

	bool Parsing( const char* pszCmd, vector<string>& vecCmd );
};

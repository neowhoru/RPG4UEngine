/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerClient.cpp
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerClient.h"
#include "PacketHandlerManager.h"
#include "User.h"
#include "ChatShell.h"
#include "UserManager.h"

#include "GMInfoParser.h"
#include <PacketStruct_ClientWorldS.h>
#include <Protocol_ClientWorldS.h>

#include "ChannelManager.h"
#include "Channel.h"
#include "Zone.h"
#include "ViewPortWork.h"

#include "IServerSession.h"
#include "ServerSessionManagerInstance.h"
//#include <PacketStruct_Master_Chat.h>
//#include <PacketStruct_DBP_Chat.h>
//#include <Protocol_DBP_Chat.h>
#include "GMCmdManager.h"


PACKETHANDLER_CLIENT_CONN(CW_HEARTBEAT)
{
//	LOGMSG( LOG_CRITICAL, "[%s/%u] HeartBeat Recv", (LPCTSTR)pUser->GetCharName(), pUser->GetGUID() );
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CHAT(CW_WHISPER_SYN)
{
	MSG_CW_WHISPER_SYN *	pRecvMsg;
	User *					pTargetUser;
	StringHandle			szTargetCharName;
	

	/////////////////////////////////////////////
	pRecvMsg = (MSG_CW_WHISPER_SYN*)pMsg;

	szTargetCharName.Set(pRecvMsg->szCharNameTo, MAX_CHARNAME_LENGTH );

	/////////////////////////////////////////////
	if( pRecvMsg->byMsgLen > MAX_CHAT_LENGTH )
	{
		LOGMSG(LOG_CRITICAL,   "Private msg too long(%d)", pRecvMsg->byMsgLen );
		//pUser->Disconnect();
		return FALSE;
	}

	pTargetUser = theUserManager.FindUser( szTargetCharName );

	/////////////////////////////////////////////
	if( !pTargetUser )
	{
		LOGMSG(LOG_CRITICAL
                ,"User not found (%s) to (%s)"
					 ,(LPCTSTR)pUser->GetCharName()
					 ,szTargetCharName);

		MSG_CW_WHISPER_NAK msgNAK;
		msgNAK.byReason = MSG_CW_WHISPER_NAK::USER_NOT_FOUND;
		pUser->Send( (BYTE*)&msgNAK, sizeof(MSG_CW_WHISPER_NAK) );
		return FALSE;
	}


	/////////////////////////////////////////////
	if( pUser == pTargetUser )
		return FALSE;

	/////////////////////////////////////////////
	if( !pTargetUser->IsEnableWhisper() )
	{
		/////////////////////////////////////////////
		sGM_INFO		*pGMInfo;
		pGMInfo = theGMInfoParser.FindGM((const char*)szTargetCharName);

		/////////////////////////////////////////////
		if(pGMInfo != NULL)
		{
			LOGMSG(LOG_CRITICAL,   "GM Message (%s) to (%s)", (LPCTSTR)pUser->GetCharName(), szTargetCharName );

			/////////////////////////////////////////////
			MSG_CW_WHISPER_NAK msgNAK;
			msgNAK.byReason = MSG_CW_WHISPER_NAK::USER_WHISPER_OFF;
			pUser->Send( (BYTE*)&msgNAK, sizeof(MSG_CW_WHISPER_NAK) );

			if( pRecvMsg->byMsgLen >= MAX_CHAT_LENGTH )
			{
				LOGMSG(LOG_CRITICAL, "%s CW_CHAT_WHISPER_SYN Invalid(%u)", (LPCTSTR)pUser->GetCharName(), pRecvMsg->byMsgLen);
				return FALSE;
			}

			/////////////////////////////////////////////
			MSG_CW_WHISPER_ACK resMsg;
			lstrcpyn( resMsg.szCharNameFrom, (LPCTSTR)pUser->GetCharName(), MAX_CHARNAME_LENGTH - 1 );
			resMsg.szCharNameFrom[MAX_CHARNAME_LENGTH-1] = 0;

			__ZERO( resMsg.szWhisperMsg );
			lstrcpyn( resMsg.szWhisperMsg, pRecvMsg->szWhisperMsg, pRecvMsg->byMsgLen );
			resMsg.szWhisperMsg[pRecvMsg->byMsgLen] = 0;
			resMsg.byMsgLen = pRecvMsg->byMsgLen;

			//pUser->Send( (BYTE*)&resMsg, resMsg.GetSize() );
			pTargetUser->Send( (BYTE*)&resMsg, resMsg.GetSize() );
		}
		else
		{
			LOGMSG(LOG_CRITICAL,  " invalid whisper (%s) to (%s)", (LPCTSTR)pUser->GetCharName(), szTargetCharName );
			MSG_CW_WHISPER_NAK msgNAK;
			msgNAK.byReason = MSG_CW_WHISPER_NAK::USER_WHISPER_OFF;
			pUser->Send( (BYTE*)&msgNAK, sizeof(MSG_CW_WHISPER_NAK) );
		}
		return FALSE;
	}

	////////////////////////////////////////////
	MSG_CW_WHISPER_ACK resMsg;
	lstrcpyn( resMsg.szCharNameFrom, (LPCTSTR)pUser->GetCharName(), MAX_CHARNAME_LENGTH );
	resMsg.szCharNameFrom[MAX_CHARNAME_LENGTH-1] = 0;

	__ZERO( resMsg.szWhisperMsg );
	lstrcpyn( resMsg.szWhisperMsg, pRecvMsg->szWhisperMsg, pRecvMsg->byMsgLen );
	resMsg.szWhisperMsg[pRecvMsg->byMsgLen] = 0;

	resMsg.byMsgLen = pRecvMsg->byMsgLen;

	//pUser->Send( (BYTE*)&resMsg, resMsg.GetSize() );
	pTargetUser->Send( (BYTE*)&resMsg, resMsg.GetSize() );



	////////////////////////////////////////////


	GAMELOG.LogWhisperChat(NULL
                           ,(LPCTSTR)pUser->GetCharName()
									,(LPCTSTR)pTargetUser->GetCharName()
									,theServerShell.GetServerKey()
									,resMsg.szWhisperMsg);

	LOGMSG	(LOG_FULL
					,"Whisper (%s) to (%s): %s"
					,(LPCTSTR)pUser->GetCharName()
					,(LPCTSTR)szTargetCharName
					,resMsg.szWhisperMsg);
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_CHAT(CW_CHAT_VILLAGE_SYN)
{
	MSG_CW_CHAT_VILLAGE_SYN	*pRecvMsg = (MSG_CW_CHAT_VILLAGE_SYN*)pMsg;

	if(!pUser)
	{
		LOGMSG(LOG_CRITICAL,  "CW_CHAT_VILLAGE_SYN (pUser == NULL)");
		return FALSE;
	}

	//////////////////////////////////////////////
	Channel *pChannel;
	pChannel = theChannelManager.GetChannel( pUser->GetChannelID() );
	if(!pChannel)
	{
		LOGMSG(LOG_CRITICAL
                ,"%s CW_CHAT_VILLAGE_SYN Invalid (Channel %u)"
					 ,(LPCTSTR)pUser->GetCharName()
					 ,pUser->GetChannelID());
		return FALSE;
	}


	//////////////////////////////////////////////
	Zone *pZone;
	pZone = pChannel->FindVillage(pUser->GetZoneKey());
	if(!pZone)
	{
		LOGMSG	(LOG_CRITICAL
						,"%s CW_CHAT_VILLAGE_SYN Invalid Zone[ZT:%u][ZK:%u]"
						,(LPCTSTR)pUser->GetCharName()
						,pUser->GetStatus()
						,pUser->GetZoneKey());
		return FALSE;
	}

	/////////////////////////////////////////////////
	if(pRecvMsg->m_byMsgLength >= MSG_CW_CHAT_VILLAGE_SYN::_MAX_CHATMSG_SIZE)
	{
		LOGMSG(LOG_CRITICAL
                ,"%s CW_CHAT_VILLAGE_SYN chat too long (%u)"
					 ,(LPCTSTR)pUser->GetCharName()
					 ,pRecvMsg->m_byMsgLength);
		return FALSE;
	}

	/////////////////////////////////////////////////
	MSG_CW_CHAT_VILLAGE_BRD	resMsg;

	resMsg.m_ChatPlayer	= pRecvMsg->m_ChatPlayer;
	resMsg.m_byMsgLength = pRecvMsg->m_byMsgLength;

	__ZERO(resMsg.m_pszChatMsg);
	lstrcpyn	( resMsg.m_pszChatMsg
				, pRecvMsg->m_pszChatMsg
				, pRecvMsg->m_byMsgLength );


	///////////////////////////////////////////////////////
	if( !pChannel->SendToViewPort( pUser, (BYTE*)&resMsg, resMsg.GetSize() ) )
		return FALSE;


	/////////////////////////////////////
	GAMELOG.LogVillageChat(NULL
                           ,pUser->GetCharName()
									,theServerShell.GetServerKey()
									,(MAPCODE)pUser->GetFieldCode()
									,resMsg.m_pszChatMsg);
	LOGMSG	(LOG_FULL
					,"CW_CHAT_VILLAGE_SYN[%s][Guid:%u][C:%u][F:%u]: %s"
					,(LPCTSTR)pUser->GetCharName()
					,pUser->GetGUID()
					,pUser->GetChannelID()
					,pUser->GetFieldCode()
					,resMsg.m_pszChatMsg);
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_GM(CW_GM_STRING_CMD_SYN)
{
	MSG_CW_GM_STRING_CMD_SYN* pRecvMsg = (MSG_CW_GM_STRING_CMD_SYN*)pMsg;
	theGMCmdManager.ParseCommand( pUser, pRecvMsg->m_szStringCmd );
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_CLIENT_VIEW(CW_VIEWPORT_CHARSTATE)
{
	MSG_CW_VIEWPORT_CHARSTATE	*pRecvMsg = (MSG_CW_VIEWPORT_CHARSTATE*)pMsg;

	/////////////////////////////////////
	if( pUser->GetStatus() != ZONETYPE_VILLAGE )
	{
		LOGMSG(LOG_FULL
				,"VIEWPORT_CHARSTATE Error - Status[Guid:%u][CharGuid:%u][Status:%u][F:%u]"
				,pUser->GetGUID()
				,pUser->GetCharGuid()
				,pUser->GetStatus()
				,pRecvMsg->dwFieldCode);
		return FALSE;
	}

	Channel *pChannel;

	//////////////////////////////////////////
	if( !pUser->GetFieldCode() )
	{
		//if(!ViewPortManager::Instance()->AddUser( pUser, pRecvMsg->dwFieldCode, pRecvMsg->dwSectorIndex ))
		
		/////////////////////////////////////////////////////
		pChannel = theChannelManager.GetChannel( pUser->GetChannelID() );
		if( !pChannel )
		{
			LOGMSG	(LOG_CRITICAL
						,"CHARSTATE AddUser Error - No Channel[Guid:%u][CharGuid:%u][C:%u]"
						,pUser->GetGUID()
						,pUser->GetCharGuid()
						,pUser->GetChannelID());
			return FALSE;
		}

		/////////////////////////////////////////////////////
		if( !pChannel->AddViewPortUser(pUser
                                    ,pRecvMsg->dwFieldCode
												,pRecvMsg->dwSectorIndex))
		{
			LOGMSG	(LOG_CRITICAL
						,"AddViewPortUser Error - No Field[Guid:%u][CharGuid:%u][F:%u][S:%u]"
						,pUser->GetGUID()
						,pUser->GetCharGuid()
						,pRecvMsg->dwFieldCode
						,pRecvMsg->dwSectorIndex);
			return FALSE;
		}

		LOGMSG(LOG_FULL, "CHARSTATE AddUser [Guid:%u][CharGuid:%u][F:%u][S:%u]", 
																			pUser->GetGUID(), 
																			pUser->GetCharGuid(), 
																			pRecvMsg->dwFieldCode, 
																			pRecvMsg->dwSectorIndex );
	}

	/////////////////////////////////////////////////////
	else
	{
		//if(!ViewPortManager::Instance()->MoveUser( pUser, pRecvMsg->dwFieldCode, pRecvMsg->dwSectorIndex ))
		/////////////////////////////////////////////////////
		pChannel = theChannelManager.GetChannel( pUser->GetChannelID() );
		if( !pChannel )
		{
			LOGMSG(LOG_CRITICAL
					,"CHARSTATE AddUser Error - No Channel[Guid:%u][CharGuid:%u][C:%u]"
					,pUser->GetGUID()
					,pUser->GetCharGuid()
					,pUser->GetChannelID());
			return FALSE;
		}

		if( !pChannel->MoveViewPortUser(pUser
                                     ,pRecvMsg->dwFieldCode
												 ,pRecvMsg->dwSectorIndex))
		{
			LOGMSG(LOG_CRITICAL
					,"CHARSTATE MoveUser Error - No Field[Guid:%u][CharGuid:%u][F:%u][S:%u]"
					,pUser->GetGUID()
					,pUser->GetCharGuid()
					,pRecvMsg->dwFieldCode
					,pRecvMsg->dwSectorIndex);
			return FALSE;
		}
		LOGMSG(LOG_FULL
				,"CHARSTATE MoveUser [Guid:%u][CharGuid:%u][F:%u][S:%u]"
				,pUser->GetGUID()
				,pUser->GetCharGuid()
				,pRecvMsg->dwFieldCode
				,pRecvMsg->dwSectorIndex);
	}
}
PACKETHANDLER_SERVER_END();


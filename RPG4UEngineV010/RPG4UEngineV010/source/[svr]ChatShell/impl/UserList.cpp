#include "stdafx.h"
#include "UserList.h"
#include "User.h"
#include <assert.h>

UserList::UserList()
{
}

UserList::~UserList()
{
}

VOID UserList::Init()
{
}

VOID UserList::Release()
{
	m_gmapUsers.clear();
	m_cmapUsers.clear();
}

VOID UserList::Add( User *pUser )
{
	// GUID를 키로 유져 추가
	GUSER_MAP_ITER it_g = m_gmapUsers.find( pUser->GetGUID() );
	assert( it_g == m_gmapUsers.end() );
	m_gmapUsers.insert( GUSER_MAP_PAIR( pUser->GetGUID(), pUser ) );

	// CharName을 키로 유져 추가
	CUSER_MAP_ITER it_c = m_cmapUsers.find( pUser->GetCharName() );
	assert( it_c == m_cmapUsers.end() );
	m_cmapUsers.insert( CUSER_MAP_PAIR( pUser->GetCharName(), pUser ) );
}

VOID UserList::Remove( User *pUser )
{
	// GUID를 키로 하는 맵에서 제거
	GUSER_MAP_ITER it_g = m_gmapUsers.find( pUser->GetGUID() );
	if( it_g != m_gmapUsers.end() )
	{
		m_gmapUsers.erase( it_g );
	}

	// CharName을 키로 하는 맵에서 제거
	CUSER_MAP_ITER it_c = m_cmapUsers.find( pUser->GetCharName() );
	if( it_c != m_cmapUsers.end() )
	{
		m_cmapUsers.erase( it_c );
	}
}

User* UserList::Find( DWORD dwGUID )
{
	GUSER_MAP_ITER it = m_gmapUsers.find( dwGUID );

	if( it != m_gmapUsers.end() )
	{
		return it->second;
	}

	return NULL;
}

User* UserList::Find( std::string charName )
{
	CUSER_MAP_ITER it = m_cmapUsers.find( charName );

	if( it != m_cmapUsers.end() )
	{
		return it->second;
	}

	return NULL;
}

VOID UserList::SendToAll( BYTE *pMsg, WORD wSize )
{
	GUSER_MAP_ITER it;
	User *pUser;
	for( it = m_gmapUsers.begin(); it != m_gmapUsers.end(); ++it )
	{
		pUser = it->second;
		pUser->Send( pMsg, wSize );
	}
}
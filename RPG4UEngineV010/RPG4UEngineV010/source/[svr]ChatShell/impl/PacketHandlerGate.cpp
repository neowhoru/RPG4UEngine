/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGate.cpp
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "PacketHandlerGate.h"
#include "ChatShell.h"
#include "GateSession.h"
#include "User.h"
#include "UserManager.h"
#include "WaitingUserManager.h"
#include "Channel.h"
#include "ChannelManager.h"
#include "ServerSessionManagerInstance.h"
#include <PacketStruct_Gate_Chat.h>
#include <Protocol_Gate_Chat.h>
//#include <PacketStruct_Master_Chat.h>
#include <Protocol_Server.h>
#include <PacketStruct_Server.h>
#include "PacketHandlerManager.h"
#include "ChatShellInfoParser.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_CONN(AW_CONNECTION_AGENTSERVER_INFO_SYN)
{
	sSERVER_GROUPC*	pGroup;
	pGroup = theServerInfoParser.GetServerGroup	(pGateShell->GetServerType()
																,pRecvMsg->dwChannelID);
	if(!pGroup)
	{
		LOGINFO("Invalid agent server group at channelID(%d)\n",pRecvMsg->dwChannelID);
		pGateShell->Disconnect();
		return FALSE;
	}

	///////////////////////////////////////////

	pGateShell->SetChannelID( pRecvMsg->dwChannelID );



	if( theChannelManager.GetChannel( pRecvMsg->dwChannelID ) != NULL )
	{
		LOGMSG(LOG_CRITICAL,  "Channel (%d) not existed.", pRecvMsg->dwChannelID );
		pGateShell->Disconnect();
		return FALSE;
	}

	theChannelManager.AddChannel( pRecvMsg->dwChannelID,  pGroup);

}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_CONN(AW_CONNECTION_PREPARE_NEW_USER_SYN)
{
	User *pUser;

	MSG_AW_CONNECTION_PREPARE_NEW_USER_NAK msgNAK;
	if( pGateShell->GetChannelID() == 0 )
	{
		LOGMSG(LOG_CRITICAL,  "no channel AW_PREPARE_NEW_USER_NAK " );
		msgNAK.m_dwKey = pRecvMsg->m_dwKey;
		pGateShell->Send( (BYTE*)&msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	/////////////////////////////////////
	pUser = theUserManager.FindUser( pRecvMsg->dwUserGuid );
	if( pUser )
	{
		LOGMSG	(LOG_CRITICAL
						," User Existed (%s:%u)"
						,(LPCTSTR)pUser->GetCharName()
						,pUser->GetGUID());
		pUser->DoLogout();
		return FALSE;
	}

	/////////////////////////////////////
	pUser = theUserManager.AllocUser( ACTIVE_USER );

	/////////////////////////////////////
	pUser->SetGUID			( pRecvMsg->dwUserGuid );
	pUser->SetChannelID	( pGateShell->GetChannelID() );
	//pUser->SetStatus( (eZONE_TYPE)pRecvMsg->byUserStatus );

	/////////////////////////////////////
	pUser->SetLoginIP	( pRecvMsg->szClientIP );
	pUser->SetLoginUserID	( pRecvMsg->dwAuthUserID );

	//if( strcmp( pRecvMsg->szCharName, "" ) != 0 )
	//{
	//	pUser->SetCharName( pRecvMsg->szCharName );
	//}
	//else
	{
		pUser->SetCharName( "" );
	}


	/////////////////////////////////////
	theWaitingUserManager.AddUser( pRecvMsg->dwAuthUserID, pUser );

	//LOGMSG(LOG_CRITICAL,  "(%d) ID(%d) IP(%s)", 
	//	pUser->GetChannelID(), pUser->GetGUID(), pUser->GetLoginIP() );

	// LoginShell/////////////////////////////////
	MSG_AW_CONNECTION_PREPARE_NEW_USER_ACK ackMsg;
	ackMsg.m_dwKey			= pRecvMsg->m_dwKey;
	pGateShell->Send( (BYTE*)&ackMsg, sizeof(ackMsg) );

	LOGMSG(LOG_FULL
			,"PREPARE_NEW_USER_SYN[Channel:%u][ID:%u]"
			,pUser->GetChannelID()
			,pUser->GetGUID());
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_USER(AW_USER_SET_CHARNAME_SYN)
{
	
	theUserManager.SetUserCharGuid( pRecvMsg->m_dwKey, pRecvMsg->dwCharGuid );
	if( !theUserManager.SetUserCharName( pRecvMsg->m_dwKey, pRecvMsg->szCharName ) )
	{
      User *pUser = theWaitingUserManager.FindKeyUser( pRecvMsg->m_dwKey );
		if( pUser )
		{
			pUser->SetCharGuid( pRecvMsg->dwCharGuid );
			pUser->SetCharName( pRecvMsg->szCharName );
		}
	}
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_USER(AW_USER_UNSET_CHARNAME_SYN)
{
	if( !theUserManager.UnSetCharGuid( pRecvMsg->m_dwKey ) )
	{
		User *pUser = theWaitingUserManager.FindKeyUser( pRecvMsg->m_dwKey );
		if( pUser )
		{
			pUser->SetCharGuid( 0 );
		}
	}
	if( !theUserManager.UnsetUserCharName( pRecvMsg->m_dwKey ) )
	{
		User *pUser = theWaitingUserManager.FindKeyUser( pRecvMsg->m_dwKey );
		if( pUser )
		{
			pUser->SetCharName( "" );
		}
	}
}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_USER(AW_USER_ENTER_ZONE_SYN)
{
	///////////////////////////////////////////////
	User *pUser = theUserManager.FindUser( pRecvMsg->m_dwKey );
	if(! pUser )
		return FALSE;

	Channel *pChannel = theChannelManager.GetChannel( pUser->GetChannelID() );
	if( !pChannel )
	{
		LOGMSG(LOG_CRITICAL
				,"USER_ENTER_ZONE Error - [ID:%u][TYPE:%u][ZoneKey:%u] Channel Not Exist!"
				,pRecvMsg->m_dwKey
				,pRecvMsg->byZoneType
				,pRecvMsg->dwZoneKey);
		//pUser->DoLogout();
		return FALSE;
	}

	switch( pRecvMsg->byZoneType )
	{
	case ZONETYPE_CHARSELECT:
		pChannel->UserEnterCharScene( pUser );
		break;
	default:
		pChannel->_UserEnterZone( pUser, pRecvMsg->dwZoneKey,(eZONE_TYPE)pRecvMsg->byZoneType );
		break;
	}





}
PACKETHANDLER_SERVER_END();

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_USER(AW_USER_LEAVE_ZONE_SYN)
{
	//////////////////////////////////////////
	User *		pUser;
	Channel *	pChannel;


	pUser = theUserManager.FindUser( pRecvMsg->m_dwKey );
	if( pUser )
	{
		
		pChannel = theChannelManager.GetChannel( pUser->GetChannelID() );
		if( !pChannel )
		{
			LOGMSG	(LOG_CRITICAL
						,"USER_LEAVE_ZONE Error - [ID:%u][TYPE:%u][ZoneKey:%u] Channel Not Exist!"
						,pRecvMsg->m_dwKey
						,pRecvMsg->byZoneType
						,pRecvMsg->dwZoneKey);
			//pUser->DoLogout();
			return FALSE;
		}
		


		pChannel->UserLeavePrevZone( pUser );

	}
	else
	{
		pUser = theWaitingUserManager.FindKeyUser( pRecvMsg->m_dwKey );
		if( pUser )
		{
			pChannel = theChannelManager.GetChannel( pUser->GetChannelID() );
			if( !pChannel )
			{
				LOGMSG(LOG_CRITICAL
						,"[ID:%u] Gate Leave[Channel:%u] Error - No Channel"
						,pUser->GetGUID()
						,pUser->GetChannelID());
				return FALSE;
			}

			LOGMSG(LOG_FULL
					,"[ID:%u] Gate Leave Zone[Channel:%u]"
					,pUser->GetGUID()
					,pUser->GetChannelID());

			pChannel->UserLeavePrevZone( pUser );
			//pUser->SetStatus( (eZONE_TYPE)pRecvMsg->byStatus );
			//pUser->SetZoneKey( pRecvMsg->dwZoneKey );
		}
	}

}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_USER(AW_USER_LOGOUT_CMD)
{
	User *pUser;
	pUser = theUserManager.FindUser( pRecvMsg->m_dwKey );

	__CHECK_PTR(pUser);


	LOGMSG(LOG_CRITICAL
			,"Gate DoLogout [%s][ID:%u]"
			,(LPCTSTR)pUser->GetCharName()
			,pUser->GetGUID());

	pUser->DoLogout();
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_GATESHELL_USER(AW_USER_RESTORE_CMD)
{
	User*		pUser;
	DWORD		dwChannelID = pGateShell->GetChannelID();
	Channel* pChannel		= theChannelManager.GetChannel( dwChannelID );

	//////////////////////////////////////////////
	if( !pChannel )
	{
		LOGMSG(LOG_CRITICAL
				,"[C:%u] Gate Restore User[ID:%u] Error - Invalid Channel!!"
				,dwChannelID
				,pRecvMsg->dwUserGUID);
		return FALSE;
	}


	//////////////////////////////////////////////
	pUser = theUserManager.FindUser( pRecvMsg->dwUserGUID );
	if( pUser )
	{
		LOGMSG(LOG_CRITICAL
				,"User existed [%s][ID:%u][TYPE:%u]"
				,(LPCTSTR)pUser->GetCharName()
				,pUser->GetGUID()
				,pRecvMsg->byZoneType);

		pUser->DoLogout();
		return FALSE;
	}

	//////////////////////////////////////////////
	pUser = theUserManager.AllocUser( ACTIVE_USER );

	pUser->SetGUID			( pRecvMsg->dwUserGUID );
	pUser->SetChannelID	( dwChannelID );

	pUser->SetLoginIP		( pRecvMsg->szClientIP );
	pUser->SetLoginUserID( pRecvMsg->dwAuthUserID );


	//////////////////////////////////////////////
	if( pRecvMsg->byZoneType != ZONETYPE_CHARSELECT )
	{
		////////////////////////////////////////////
		pUser->SetCharGuid( pRecvMsg->CharGuid );
		pUser->SetCharName( pRecvMsg->szCharName );
		pUser->SetStatus( (eZONE_TYPE)pRecvMsg->byZoneType );
		pUser->SetZoneKey( pRecvMsg->dwZoneKey );

		LOGMSG(LOG_FULL
				,"[C:%u]LoginShell Restore User[ID:%u][CharGuid:%u][TYPE:%u][ZoneKey:%u]"
				,dwChannelID
				,pRecvMsg->dwUserGUID
				,pRecvMsg->CharGuid
				,pRecvMsg->byZoneType
				,pRecvMsg->dwZoneKey);
	}
	else
	{
		LOGMSG( LOG_FULL, "[C:%u]LoginShell Restore User[ID:%u]", dwChannelID, pRecvMsg->dwUserGUID );
	}

	////////////////////////////////
	theWaitingUserManager.AddUser( pRecvMsg->dwAuthUserID, pUser );

	////////////////////////////////
	MSG_AW_CONNECTION_PREPARE_NEW_USER_ACK ackMsg;
	ackMsg.m_dwKey				= pRecvMsg->dwUserGUID;
	pGateShell->Send( (BYTE*)&ackMsg, sizeof(ackMsg) );

}
PACKETHANDLER_SERVER_END();



#pragma once

#include "serversession.h"

class _SERVERMAINGATE_API GuildSession : public ServerSession
{
public:
	GuildSession(void);
	virtual ~GuildSession(void);

public:
	eSERVER_TYPE		GetServerType() { return GUILD_SHELL;	}

protected:
	virtual VOID		OnConnect( BOOL bSuccess, DWORD dwNetworkIndex );
	virtual VOID		OnRecv	( BYTE * pMsg, WORD wSize );
};

SERVERSESSION_FACTORY_DECLARE(GuildSession);

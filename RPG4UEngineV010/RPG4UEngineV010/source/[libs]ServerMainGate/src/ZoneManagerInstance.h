/*////////////////////////////////////////////////////////////////////////
文 件 名：ZoneManagerInstance.h
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ZONEMANAGERINSTANCE_H__
#define __ZONEMANAGERINSTANCE_H__
#pragma once

#include <ZoneManager.h>


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ZoneManagerInstance : public ZoneManager
{
public:
	ZoneManagerInstance(void);
	virtual ~ZoneManagerInstance(void);

public:
	virtual BOOL Init		();
	virtual VOID Release	();

public:
// ---------------------------------------------------
	VOID			 CreateCharSelect	();
	eROOM_RESULT CreateVillage		(CODETYPE	MapCode
                                 ,IZone **	ppVillage)		;
	eROOM_RESULT CreateLobbyRoom	(KEYTYPE             VillageKey
                                 ,User *        pMasterUser
											,CODETYPE            MapCode
											,eZONE_TYPE          eLobbyType
											,eZONE_OPEN_STATE    ePublic
											,LPCTSTR             szRoomTitle
											,LPCTSTR             szRoomPwd
											,sROOMINFO_BASE & IN RoomInfo
											,LobbyRoom *& OUT    pLobbyRoom);

	eROOM_RESULT CreateHuntingRelay	(KEYTYPE                   PrevHuntingKey
                                       ,User *              pMasterUser
													,CODETYPE                  MapCode
													,eZONE_OPEN_STATE          ePublic
													,LPCTSTR                   szRoomPWD
													,sROOMINFO_BASE & IN       RoomInfo
													,sROOMINFO_ADDITIONAL & IN AdditionalRoomInfo
													,HuntingRoom *& OUT        pHuntingRoom);

	eROOM_RESULT CreateHuntingRoom(KEYTYPE            LobbyKey
                                 ,User *       pMasterUser
											,HuntingRoom *& OUT pHuntingRoom);
	eROOM_RESULT CreateMissionRoom(KEYTYPE            LobbyKey
                                 ,User *       pMasterUser
											,MissionRoom *& OUT pMissionRoom);
	eROOM_RESULT CreatePVPRoom		(KEYTYPE        LobbyKey
                                 ,User *   pMasterUser
											,PVPRoom *& OUT pPVPRoom);

	eROOM_RESULT MoveZone			(eZONE_TYPE       FromZoneType
                                 ,KEYTYPE          FromZoneKey
											,eZONE_TYPE       ToZoneType
											,KEYTYPE          ToZoneKey
											,User *     pUser
											,eZONE_OPEN_STATE RoomPublic=ZONEOPEN_PUBLIC
											,LPCTSTR          szRoomPwd=NULL);

protected:
	BOOL			 _PrevCreateRoom	(IServerSession*&	pServerSession);
	eROOM_RESULT MoveZone			(IZone *  pFromZone
                                 ,IZone *  pToZone
											,User *     pUser
											,eZONE_OPEN_STATE RoomPublic=ZONEOPEN_PUBLIC
											,LPCTSTR          szRoomPwd=NULL);

public:

	// ----------------------------------
	eROOM_RESULT KickUserAtLobby				(User *      pMaster
														,DWORD             TargetPlayerKey
														,User *& OUT pTargetUser);
	eROOM_RESULT ToggleReadyInLobby			(User * pUser
														,BOOL         bReady)		;
	eROOM_RESULT ChangeTeamInLobby			(User * pUser
                                          ,ePARTY_INDEX team)		;
	eROOM_RESULT ChangeMapInLobby				(User * pMaster
                                          ,CODETYPE     MapCode)		;
	eROOM_RESULT ConfigHuntingRoomInLobby	(User *              pMaster
                                          ,sROOMINFO_ADDITIONAL & IN AdditionalInfo)		;
	eROOM_RESULT ConfigPVPRoomInLobby		(User *             pMaster
                                          ,sPVPINFO_ADDITIONAL & IN AdditionalPVPInfo)		;
	eROOM_RESULT SetMaxSlotNumInLobby	(User * pMaster
                                          ,BYTE         MaxUser)		;
	// ------------------------------------------------------------


	VOID GetMissionLobbyRoomList	(User *            pUser
                                 ,BYTE                    ReqCount
											,BYTE                    nMaxCount
											,BYTE & OUT              nCount
											,sROOMINFO_MISSION * OUT pRoomInfoArray);
	VOID GetHuntingLobbyRoomList	(User *            pUser
                                 ,BYTE                    ReqCount
											,BYTE                    nMaxCount
											,BYTE & OUT              nCount
											,sROOMINFO_HUNTING * OUT pRoomInfoArray);
	VOID GetPVPLobbyRoomList		(User *        pUser
                                 ,BYTE                ReqCount
											,BYTE                nMaxCount
											,BYTE & OUT          nCount
											,sROOMINFO_PVP * OUT pRoomInfoArray);
	VOID GetHuntingRoomList			(User *            pUser
                                 ,BYTE                    ReqCount
											,BYTE                    nMaxCount
											,BYTE & OUT              nCount
											,sROOMINFO_HUNTING * OUT pRoomInfoArray);

	IRoom * GetRecommendRoom		(User * pUser
                                 ,eZONE_TYPE   RoomType)		;


	// --------------------------------------------------------------------------------
	VOID Process	();
	VOID Display	();
	VOID DisplayerZonePoolInfo		();
	VOID DestroyZone					( IZone * pZone );

	DWORD GetHuntingRoomAmount	();
	DWORD GetMissionRoomAmount	();

protected:
	virtual VOID			_AddRoom		( IZone * pRoom );
	virtual VOID			_RemoveRoom	( IZone * pRoom );

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ZoneManagerInstance , API_NULL);
//extern API_NULL ZoneManagerInstance& singleton::GetZoneManagerInstance();
#define theZoneManagerInstance  singleton::GetZoneManagerInstance()



#endif //__ZONEMANAGERINSTANCE_H__
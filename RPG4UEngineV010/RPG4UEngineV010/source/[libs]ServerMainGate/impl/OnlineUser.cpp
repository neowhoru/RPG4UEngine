/*////////////////////////////////////////////////////////////////////////
文 件 名：OnlineUser.cpp
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "OnlineUser.h"
//#include "PacketHandler.h"
#include "ServerSessionManager.h"
#include <PacketStruct_Gate_DBP.h>
#include <PacketStruct_Gate_Game.h>
#include <PacketStruct_Gate_Guild.h>
#include <PacketStruct_ClientGameS.h>
#include <PacketStruct_Gate_Chat.h>
#include "UserManager.h"
#include "ServerSession.h"
#include <ResultCode.h>
#include "GateShell.h"
#include "HeartbeatHandler.h"
#include <ThreadableLogger.h>
#include "ZoneManager.h"
#include "IZone.h"
#include "PacketHandlerManager.h"
#include "ServerInfoParser.h"

OnlineUser::OnlineUser():
	m_byCreatedCharAmount(0)
{
	SetUserType(ONLINE_USER);
	SetState(USER_STATE_POOL);

	m_pHeartbeatHandler		= new HeartbeatHandler;
}

OnlineUser::~OnlineUser()
{
	SAFE_DELETE( m_pHeartbeatHandler );
}

BOOL OnlineUser::Init()
{
	_SUPER::Init();

	m_pHeartbeatHandler->Init( TICK_HEARTBEAT, 30000, 600000 );

	UnlinkServer();

	SetZoneType( ZONETYPE_MAX );
	SetZoneState( ZONESTATE_NOT_ATZONE );
	m_bySelectedIndex = MAX_CHARACTER_LIST_NUM;
	m_TransactionState = TK_END;
	SetLobbyReady(FALSE);
	SetMaster(FALSE);
	SetTeam(0);
	SetLogoutType(LOGOUT_NORMAL);
	// 浇废 檬扁拳
	for( int i = 0 ; i < MAX_CHARACTER_SLOT_NUM ; ++i )
		m_arOccupiedSlots[i] = FALSE;

	SetState(USER_STATE_INIT);


	memset( m_CharacterInfos, 0, sizeof(sPLAYER_SERVER_PART)*(MAX_CHARACTER_LIST_NUM+1) );

	return TRUE;
}

VOID OnlineUser::Release()
{
	_SUPER::Release();
	// 钱俊 馆券且锭 碍力肺 努扼捞攫飘甫 立加秦力 矫挪促.
	// case : 霸烙辑滚啊 促款 登菌阑 版快
	Disconnect();			//< tempuser or usersession俊 持瘤 臼绰促.

	SetState(USER_STATE_POOL);
}


VOID OnlineUser::OnRecv( BYTE *pMsg, WORD wSize )
{
	_SUPER::OnRecv( pMsg, wSize );

	//ResetSendIdleCheck();
	//MSG_BASE * pMsgBase = (MSG_BASE *)pMsg;
	//theGateShell.GetPacketLogger()->LOG_Arg( "From[U:%s:C:%d:P:%d]Client", GetUserID(), pMsgBase->m_byCategory, pMsgBase->m_byProtocol );

	//g_PacketHander.ParsePacket_CA( this, (MSG_BASE *)pMsg, wSize );
}


BOOL OnlineUser::ParsePacket		(MSG_BASE*		pMsg,DWORD			dwSize)
{
	if(_SUPER::ParsePacket		(pMsg,dwSize) )
		return TRUE;

#ifdef _DEBUG
	if(pMsg->m_byCategory != CG_CONNECTION)
	{
		LOGINFO("Recv User Packet %d Protocol %d\n",pMsg->m_byCategory,pMsg->m_byProtocol);
	}
#endif

	return SendToLinkedShell( (MSG_OBJECT_BASE *)pMsg, (WORD)dwSize );
}


BOOL OnlineUser::SendPacket( MSG_BASE * pMsg, WORD wSize )
{
	//theGateShell.GetPacketLogger()->LOG_Arg( "To[U:%s:C:%d:P:%d]Client", GetUserID(), pMsg->m_byCategory, pMsg->m_byProtocol );
	return _SUPER::SendPacket( pMsg, wSize );
}
VOID OnlineUser::OnAccept( DWORD dwNetworkIndex )
{
	ASSERT( !"甸绢 棵 荐 绝绰 何盒" );
}

VOID OnlineUser::_LeaveRoom()
{
	if( GetZoneState() & ZONESTATE_ATZONE ||
		GetZoneState() & ZONESTATE_PRE_ENTER_ZONE )
	{
		IZone * pZone = theZoneManager.FindZone( GetZoneKey() );
		pZone->LeaveUser( this, eSEND_NONE );
	}
}

VOID OnlineUser::OnDisconnect()
{
	DISPMSG	("OnlineUser[%s] OnDisconnect...\n"
				,(LPCTSTR)GetUserID());
	if( m_pLinkedShell )
	{
		// to game Server
		MSG_AG_CONNECTION_DISCONNECT_CMD msg;
		SendToLinkedShell( &msg, sizeof(msg) );
	}

	if(	GetZoneState() != ZONESTATE_ATCHARSELECT
		&&	GetZoneState() != ZONESTATE_PRE_ENTER_CHARSELECT
		&&	0					!= GetSelectedCharGuildGuid() )
	{
		MSG_AZ_GUILD_LOGOUT_CHAR_CMD cmsg;
		cmsg.m_GuildGuid = GetSelectedCharGuildGuid();
		cmsg.m_CharGuid = GetSelectedCharGuid();
		SendToGuildShell( &cmsg, sizeof(cmsg) );
	}

	_LeaveRoom();

	
	if( theUserManager.FindUserName( GetSelectedCharName() ) )
	{
		theUserManager.RemoveUserName( this );
	}

	// world
	MSG_AW_USER_LOGOUT_CMD msg;
	msg.m_dwKey = GetUserKey();
	SendToChatShell( &msg, sizeof(msg) );

	sLOGOUT_INFO LogoutAuthData;
	LogoutAuthData.dwAuthUserID = GetAuthID();
	strncpy(LogoutAuthData.szAccountID, GetUserID(), MAX_ID_LENGTH);
	strncpy(LogoutAuthData.szCharName, GetSelectedCharName(), MAX_CHARNAME_LENGTH);

	LogoutAuthData.wSvrCode = theServerShell.GetChannelID();
	LogoutAuthData.charLV = GetSelectedCharLV();
	LogoutAuthData.charType = GetSelectedCharClass();
	LogoutAuthData.byLogoutType = GetLogoutType();
	strncpy( LogoutAuthData.szLogoutTime, GetLoginTime(), MAX_TIMEDATA_SIZE );

	_SUPER::OnDisconnectAtLogin(&LogoutAuthData);
	_SUPER::OnDisconnect();
}

VOID OnlineUser::SelectCharacter( BYTE byIndex )
{
	// 0 ~ 5
	// 急琶茄 浇吩 牢郸胶
	m_bySelectedIndex = byIndex;
}
/*
BOOL OnlineUser::LinkFieldServer()
{
	// 鞘靛辑滚 窍靛 内爹
	m_pLinkedShell = theServerSessionManagerInstance.GetFieldServer();
	if( !m_pLinkedShell )
	{
		LOGMSG( LOG_FULL,  "[版绊]:傅农等 霸烙 辑滚啊 绝促." );
		MSG_CG_CONNECTION_DISCONNECT_CMD msg;
		msg.m_dwErrorCode = RC::RC_CONNECTION_NOTYETSERVICE;
		SendPacket( &msg, sizeof(msg) );

		Disconnect( TRUE );
		return FALSE;
	}
	ASSERT( m_pLinkedShell->GetServerType() == FIELD_SHELL );
	setServerSessionIndex( m_pLinkedShell->GetSessionIndex() );
	ASSERT( m_pLinkedShell );

	return TRUE;
}
*/
VOID OnlineUser::LinkServer( DWORD dwSessionIndex )
{
	IServerSession * pServerSession = theServerSessionManager.FindServer( dwSessionIndex );
	ASSERT( pServerSession );
	if( !pServerSession ) 
		return;
	ASSERT( m_pLinkedShell != pServerSession );
	m_pLinkedShell = pServerSession;
	SetServerSessionIndex( m_pLinkedShell->GetSessionIndex() );
}


VOID OnlineUser::SerializeCharacterInfo(BYTE                          bySlotIndex
                                        ,sPLAYER_SERVER_PART & INOUT CharacterPart
													 ,eSERIALIZE                    se)
{ 
	ASSERT( bySlotIndex < MAX_CHARACTER_SLOT_NUM ); 
	if( se == SERIALIZE_STORE )
		memcpy( &m_CharacterInfos[bySlotIndex], &CharacterPart, CharacterPart.GetSize() );
	else if( se == SERIALIZE_LOAD )
		memcpy( &CharacterPart, &m_CharacterInfos[bySlotIndex], m_CharacterInfos[bySlotIndex].GetSize() );
}

#include "stdafx.h"
#include "FieldSession.h"
//#include "PacketHandler.h"
#include "PacketStruct_Base.h"
#include <MathHelper.h>
#include "ServerSessionManager.h"
#include "GateShell.h"
#include "Village.h"
#include "ZoneManager.h"
#include <MapInfoParser.h>
#include "PacketStruct_Server.h"
#include "User.h"
#include "UserManager.h"
#include "PacketStruct_Common.h"
#include "PacketCryptManager.h"


FieldSession::FieldSession()
{
	m_PacketHandlerType	= GAME_SHELL;
}

FieldSession::~FieldSession()
{
}

BOOL FieldSession::Init()
{
	_SUPER::Init();

	return TRUE;
}

void	FieldSession::OnAddToManager()
{
#ifndef _DEBUG
	theGateShell.StartListen(LISTEN_CLIENT);
#endif

	//创建全部村落场景
	CreateVillages();
}

void	FieldSession::OnRemoveFromManager	()
{
}


VOID FieldSession::CreateVillages()
{
	MapInfoTable & rHash = theMapInfoParser.GetMapInfoHash();
	for( MapInfoTableIt it = rHash.begin() ; it != rHash.end() ; ++it )
	{
		sMAPINFO_BASE * pInfo = (*it);
		if( pInfo->byMKind == ZONETYPE_VILLAGE )
		{
			IZone * pZone = theZoneManager.FindVillage( pInfo->MapCode );
			if( pZone )
			{
				pZone->Create();	
			}
			else
			{
				RC::eROOM_RESULT rt = theZoneManager.CreateVillage( pInfo->MapCode, NULL );
				ASSERT( rt == RC::RC_ROOM_SUCCESS );
			}
		}		
	}
}

VOID FieldSession::OnRecv( BYTE *pMsgBuf, WORD wSize )
{
	//////////////////////////////////
	MSG_BASE*	pMsg = (MSG_BASE*)pMsgBuf;


	//////////////////////////////////
	if(	pMsg->m_byCategory	== CO_COMMON 
		&& pMsg->m_byProtocol	== CO_COMMON_CRYPT_CMD)
	{
		MSG_CO_COMMON_CRYPT_CMD*		pMsgCrypt;
		static BYTE					byDecryptedBuf[10000];


		pMsgCrypt	= (MSG_CO_COMMON_CRYPT_CMD*)byDecryptedBuf;
		wSize			= thePacketCryptManager.PacketDecrypt(byDecryptedBuf
													  ,pMsgCrypt->szCryptBuffer
													  ,pMsgCrypt->wCryptSize);

		pMsg = (MSG_BASE*)byDecryptedBuf;
	}



#ifdef _DEBUG
	if(pMsg->m_byCategory != SVR_SERVER)
	{
		LOGINFO	("RECV Game Server:Cat:%d,Pro:%d\n"
					,pMsg->m_byCategory
					,pMsg->m_byProtocol );
	}
#endif

	//////////////////////////////////
	if(!ParsePacket((MSG_BASE *)pMsg,wSize))
	{
		//////////////////////////////////
		//转发到User
		MSG_OBJECT_BASE * pUserMsg = (MSG_OBJECT_BASE *)pMsg;
		User * pUser = (User*)theUserManager.GetUser( pUserMsg->m_dwKey );
		if( pUser )
		{
			pUser->SendPacket( pMsg, wSize );
		}
	}
	//g_PacketHander.ParsePacket_AG( this, (MSG_BASE *)pMsg, wSize );
}

VOID FieldSession::OnDisconnect()
{
	_RemoveRelatedUsers();

	_SUPER::OnDisconnect();
}

VOID FieldSession::OnLogText( LPCTSTR szLog )
{
	LOGINFO(szLog);
	//LogToFile( "NetworkLog_AgentServer.txt", "[FieldSession] %s", szLog );
}
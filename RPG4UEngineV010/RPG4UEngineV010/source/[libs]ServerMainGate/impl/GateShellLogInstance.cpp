/*////////////////////////////////////////////////////////////////////////
文 件 名：GateShellLogInstance.cpp
创建日期：2009年6月16日
最后更新：2009年6月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "GateShellLogInstance.h"
#include "LogWriteManager.h"
#include "ServerGroupLog.h"
#include "ServerSetting.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GateShellLogInstance, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _GAMELOG_INIT(DATA)\
						if(!IsRun() )	\
							return;\
						DATA.Init();\
						LogTitle(&DATA,szAccountID, szCharName)

#define _GAMELOG_ACTION(code)		_GAMELOG_INIT(m_ActionData );\
												m_ActionData.dwServerCode		= dwServerCode;\
												m_ActionData.dwActionLogCode	= (code)

#define _GAMELOG_SESSION(code)		_GAMELOG_INIT(m_SessionData);\
						m_SessionData.dwServerCode		= dwServerCode;\
						m_SessionData.nConnectType	= (code)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GateShellLogInstance::GateShellLogInstance(void)
{
	m_pConnectLog			= new ServerGroupLog;
}

GateShellLogInstance::~GateShellLogInstance(void)
{
	SAFE_DELETE(m_pConnectLog);
}

VOID GateShellLogInstance::Init		()
{
	StringHandle	sLogFile;
	//StringHandle	sLogDir;
	sLogFile.Format(_T("Connection_%s_%d")
						,theServerSetting.m_szServerName
						,theServerSetting.m_dwServerID);

	m_pConnectLog->Init	(theServerSetting.m_LogInfo.byLogOption
								,theServerSetting.m_LogInfo.byLogFileLevel
								,theServerSetting.m_LogInfo.szLogFilePath 
								,sLogFile);
}

VOID GateShellLogInstance::Release	()
{
}


void GateShellLogInstance::LogConnect(LPCTSTR szFormat,...)
{
    if( NULL == szFormat )
        return;

    int nLength = (int)lstrlen( szFormat );
    if ( nLength <=0 )
        return;

    static char szTextBuf[1024];

    va_list	va;
    va_start( va, szFormat );
    vsnprintf(szTextBuf, sizeof(szTextBuf)-1, szFormat, va );
    va_end( va );

	m_pConnectLog->OutputMsg(szTextBuf);
}

//////////////////////////////////////////////////////////////////////////
void GateShellLogInstance::LogPlayerLogin(LPCTSTR szAccountID
                                 ,LPCTSTR szCharName
											,DWORD   dwServerCode
											,LPCTSTR szIP
											,WORD    wClass
											,WORD    wLevel)
{
	_GAMELOG_SESSION(LOGCONN_LOGIN);

	StringHandle	sTime;


	if( szIP == NULL )	
		szIP = _T("");

	_tcsncpy( m_SessionData.szClientIP, szIP, MAX_IP_LENGTH );

	m_Time.GetTime( TIME_FORMAT_MS, sTime );
	lstrcpyn(m_SessionData.szLoginTime, sTime,MAX_TIMEDATA_SIZE);

	m_SessionData.dwClassType	= (BYTE)wClass;
	m_SessionData.nLevel			= wLevel;

	m_pLogWrite->LogSession( m_SessionData, m_bWriteText, NULL );
}



void GateShellLogInstance::LogPlayerLoginFail(LPCTSTR szAccountID
                                         ,LPCTSTR szCharName
													  ,DWORD dwServerCode
													  ,LPCTSTR szIP)
{
	_GAMELOG_SESSION(LOGCONN_LOGIN_FAIL);

	StringHandle	sTime;

	if( szIP == NULL )	
		szIP = _T("");
	_tcsncpy( m_SessionData.szClientIP, szIP, MAX_IP_LENGTH );

	m_Time.GetTime( TIME_FORMAT_MS, sTime );
	lstrcpyn(m_SessionData.szLoginTime, sTime,MAX_TIMEDATA_SIZE);

	m_pLogWrite->LogSession( m_SessionData, m_bWriteText, NULL );
}



void GateShellLogInstance::LogPlayerLogOut(LPCTSTR szAccountID
                                  ,LPCTSTR szCharName
											,DWORD dwServerCode
											,LPCTSTR szIP
											,LPCTSTR szLoginTime
											,WORD    wClass
											,WORD    wLevel)
{
	_GAMELOG_SESSION(LOGCONN_LOGOUT);
	StringHandle	sTime;


	if( szIP == NULL )	
		szIP = _T("");
	_tcsncpy( m_SessionData.szClientIP, szIP, MAX_IP_LENGTH );

	if( szLoginTime == NULL )	
		szLoginTime = _T("");
	_tcsncpy( m_SessionData.szLoginTime, szLoginTime, MAX_TIMEDATA_SIZE);

	m_Time.GetTime( TIME_FORMAT_MS, sTime );
	lstrcpyn(m_SessionData.szLoginTime, sTime,MAX_TIMEDATA_SIZE);


	if( m_SessionData.szLogoutTime != NULL )
	{
		int nUsedTime = TimeHelper::GetTimeDiff(m_SessionData.szLoginTime
                                             ,m_SessionData.szLogoutTime)			;
		m_SessionData.dwUseTime = nUsedTime;
	}

	m_SessionData.dwClassType = (BYTE)wClass;
	m_SessionData.nLevel = wLevel;

	m_pLogWrite->LogSession( m_SessionData, m_bWriteText, NULL );
}



void GateShellLogInstance::LogCharDisConnect(LPCTSTR szAccountID
                                        ,LPCTSTR szCharName
													 ,DWORD dwServerCode)
{
	_GAMELOG_SESSION(LOGCONN_CHAR_CONNECT);


	m_pLogWrite->LogSession( m_SessionData, m_bWriteText, NULL );
}


void GateShellLogInstance::LogCharacter(LPCTSTR szAccountID
                                   ,LPCTSTR szCharName
											  ,DWORD dwServerCode
											  ,BOOL    bCreate)
{
	_GAMELOG_ACTION(bCreate?LOGACTION_CREATE_CHAR:LOGACTION_DELETE_CHAR);


	m_pLogWrite->LogUserAction( m_ActionData, m_bWriteText, NULL );
}


void GateShellLogInstance::LogMoveVillage(LPCTSTR szAccountID
                                     ,LPCTSTR szCharName
												 ,DWORD dwServerCode
												 ,int     nMapCode)
{
	_GAMELOG_ACTION(LOGACTION_MOVE_TOWN);

	m_ActionData.dwMapCode = nMapCode;

	m_pLogWrite->LogUserAction( m_ActionData, m_bWriteText, NULL );
}
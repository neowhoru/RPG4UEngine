/*////////////////////////////////////////////////////////////////////////
文 件 名：RoomImpl.cpp
创建日期：2006年10月18日
最后更新：2006年10月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "RoomImpl.h"
#include "OnlineUser.h"
#include "IServerSession.h"
#include "ResultCode.h"
#include "LobbyRoom.h"
#include "MapInfoParser.h"
#include "PacketStruct_Gate_Game.h"
#include "LimitedMapPortParser.h"
#include "ZoneManager.h"

using namespace RC;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
RoomImpl::RoomImpl()
{
}
RoomImpl::~RoomImpl()
{
}



BOOL RoomImpl::Create	(User *        pUser
                        ,KEYTYPE             key
								,CODETYPE            MapCode
								,eZONE_OPEN_STATE    ePublic
								,LPCTSTR             szRoomTitle
								,LPCTSTR             szRoomPwd
								,sROOMINFO_BASE & IN BaseRoomInfo
								,IServerSession *    pServerSession)
{

	sMAPINFO_BASE * pInfo;
	pInfo = theMapInfoParser.GetMapInfo(MapCode);


	SetKey			(key);
	SetMapCode		(MapCode);
	SetLimitUserMax( pInfo->byMaxUserNum );

	SetZonePublic	(ePublic);
	SetRoomInfo		(BaseRoomInfo );
	SetRoomTitle	(szRoomTitle);
	SetRoomPWD		(szRoomPwd);

	ChangeToMaster	(pUser);
	SetLinkedShell	(pServerSession );

	//////////////////////////////////////
	SetReusedKey	(FALSE);
	SetReusedServerSession(FALSE);

	return TRUE;
}


eROOM_RESULT RoomImpl::CanJoin(User *     pUserRef
                              ,eZONE_OPEN_STATE RoomPublic
										,LPCTSTR          szRoomPwd)
{
	OnlineUser* pUser = (OnlineUser*)pUserRef;
	/////////////////////////////////////	
	__CHECK2(GetRoomState() == eROOMSTATE_GENERNAL, RC_ROOM_INVALIDPREVLOBBY);

	/////////////////////////////////////	
	if( NULL == FindRelayUser( pUser->GetPlayerKey() ) )
	{
		__CHECK2(GetUserAmount() < m_byLimitUserMax,RC_ROOM_FULL_ROOM);
	}

	/////////////////////////////////////	
	__CHECK2(GetZonePublic() == RoomPublic,RC_ROOM_INVALID_ROOM_TYPE);

	/////////////////////////////////////	
	if( RoomPublic == ZONEOPEN_PRIVATE )
	{
		__CHECK2( GetRoomPWD() == szRoomPwd ,RC_ROOM_NOTEQUAL_PASSWORD);
	}

	/////////////////////////////////////	
	__CHECK2	(CheckCharClass((ePLAYER_TYPE)pUser->GetSelectedCharClass())
				,RC_ROOM_INVALID_LIMITCLASS_FOR_ME);

	/////////////////////////////////////	
	__CHECK2	(CheckRoomLevel(pUser->GetSelectedCharLV())
				,RC_ROOM_INVALID_LIMITLEVEL_FOR_ME);

	/////////////////////////////////////	
	__CHECK2	(CheckJoinMap(pUser)
				,RC_ROOM_LIMITMAP_FOR_ME);

	return RC_ROOM_SUCCESS;
}

VOID RoomImpl::ChangeToMaster( User * pTargetMaster )
{
	if( GetMaster() )
		GetMaster()->SetMaster(FALSE);

	////////////////////////////////////////////
	if( GetMaster() && pTargetMaster ) 
	{
		MSG_AG_ZONE_MASTER_CHANGED_CMD msg;
		msg.m_dwKey		= pTargetMaster->GetUserKey();
		msg.m_RoomKey	= GetKey();
		SendToLinkedShell( &msg, sizeof(msg) );

		MSG_CG_ZONE_MASTER_CHANGED_BRD bmsg;
		bmsg.m_dwMasterPlayerKey = pTargetMaster->GetPlayerKey();
		SendToAll( &bmsg, sizeof(bmsg) );
	}

	////////////////////////////////////////////
	SetMaster(pTargetMaster);
	if( pTargetMaster ) 
	{
		pTargetMaster->SetLobbyReady(FALSE);
		pTargetMaster->SetMaster(TRUE);
	}
}


BOOL RoomImpl::CheckCharClass( ePLAYER_TYPE type )
{
	__CHECK(_BIT_EXIST(m_RoomInfo.m_ClassLimit, type));

	return TRUE;
}

BOOL RoomImpl::CheckRoomLevel( LEVELTYPE LV )
{
	if( LV < m_RoomInfo.m_MinLV || LV > m_RoomInfo.m_MaxLV )
	{
		return FALSE;
	}
	return TRUE;
}


eROOM_RESULT RoomImpl::CheckRecommend( User * pUserRef )
{
	OnlineUser* pUser = (OnlineUser*)pUserRef;
	__CHECK2(GetRoomState() == eROOMSTATE_GENERNAL,RC_ROOM_INVALIDPREVLOBBY);

	eROOM_RESULT rt = CanJoin( pUser, ZONEOPEN_PUBLIC, NULL );
	if( RC_ROOM_SUCCESS == rt )
	{
		if( IsRecommendLevel(pUser->GetSelectedCharLV()) )
			return RC_ROOM_SUCCESS_FOR_RECOMMEND_JOIN;
		else
			return RC_ROOM_SUCCESS_FOR_GENERAL_JOIN;
	}

	return rt;
}

static BOOL CanJoinZoneType(sMAPPORT_INFO * pPort
                           ,eZONE_TYPE EnteringZoneType
									,CODETYPE   EnteringMapCode)
{
	switch(EnteringZoneType)
	{
	case ZONETYPE_HUNTING:
		{
			for( int i = 0 ; i < MAX_HUNTING_PORT_SIZE ; ++i )
			{
				if( pPort->m_arHuntingPorts[i] == EnteringMapCode )
					return TRUE;
			}
		}break;
	case ZONETYPE_MISSION:
		{
			for( int i = 0 ; i < MAX_MISSION_PORT_SIZE ; ++i )
			{
				if( pPort->m_arMissionPorts[i] == EnteringMapCode )
					return TRUE;
			}
		}break;
	case ZONETYPE_PVP:
		{
			for( int i = 0 ; i < MAX_PVP_PORT_SIZE ; ++i )
			{
				if( pPort->m_arPVPPorts[i] == EnteringMapCode )
					return TRUE;
			}
		}break;
	default:
		{
			LOGMSG( LOG_FULL, "CheckJoinMap:甸绢 棵 荐 绝绰 备埃" );
			return TRUE;
		}
	}
	return FALSE;
}

BOOL RoomImpl::CheckJoinMap( User * pUserRef )
{
	OnlineUser* pUser = (OnlineUser*)pUserRef;
	if( pUser->GetZoneState() == ZONESTATE_ATVILLAGE )
	{
		sMAPPORT_INFO * pPort = theLimitedMapPortParser.FindVillage( pUser->GetSelectedCharMapCode() );

		switch(GetType())
		{
		case ZONETYPE_LOBBY:
			{
				LobbyRoom * pLobby = (LobbyRoom *)this;
				return CanJoinZoneType( pPort, pLobby->GetLobbyType(), GetMapCode() );
			}break;
		default:
			{
				return CanJoinZoneType( pPort, GetType(), GetMapCode() );
			}break;
		}
		return FALSE;
	}

	return TRUE;
}

eROOM_RESULT	RoomImpl::CheckCreateRoom	(User *       pMasterUser
											,eZONE_TYPE			  zoneType)
{
	__CHECK2	(GetMaster() == pMasterUser 
				,RC_ROOM_ISNOTMATERPLAYER);
	return RC_ROOM_SUCCESS;
}

VOID		RoomImpl::_OnReadyUser( User * pUser )
{
	_SUPER::_OnReadyUser(pUser);
}


VOID		RoomImpl::_OnEnterUser( User * pUser )
{
	_SUPER::_OnEnterUser(pUser);
}

VOID		RoomImpl::_OnLeaveUser( User * pUser )
{
	_SUPER::_OnLeaveUser(pUser);
}






const DWORD	SLEEP_IDLE		=	1;

//=================================================================================================
ISessionHandle * CreateClientSideAcceptedObject();
ISessionHandle * CreateServerSideAcceptedObject();
VOID DestroyServerSideAcceptedObject( ISessionHandle * pSessionHandle );
VOID DestroyClientSideAcceptedObject( ISessionHandle * pSessionHandle );
VOID DestroyClientSideConnectedObject( ISessionHandle * pSessionHandle );
VOID DestroyServerSideConnectedObject( ISessionHandle * pSessionHandle );
void CallBackCommand( LPCTSTR  szMessage );


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

//------------------------------------------------------------------------------------------------
// 클라이언트 콜백
//------------------------------------------------------------------------------------------------
ISessionHandle * CreateClientSideAcceptedObject()
{
	//LOGMSG( LOG_FULL, "CreateClientSideAcceptedObject()");
	return (ISessionHandle *)theUserManager.AllocUser( TEMP_USER );
}

VOID DestroyClientSideAcceptedObject( ISessionHandle * pSessionHandle )
{
	theUserManager.FreeUser( (User*)pSessionHandle );
}

VOID DestroyClientSideConnectedObject( ISessionHandle * pSessionHandle )
{
	ASSERT( !"not support" );
}

//------------------------------------------------------------------------------------------------
// 서버 콜백
//------------------------------------------------------------------------------------------------
ISessionHandle* CreateServerSideAcceptedObject()
{
	IServerSession * pServerSession = theServerSessionManager.AllocServerSession( TEMP_SHELL );
	return (ISessionHandle*)pServerSession;
}

VOID DestroyServerSideAcceptedObject( ISessionHandle * pSessionHandle )
{
	IServerSession * pServerSession = (IServerSession *)pSessionHandle;
	theServerSessionManager.FreeServerSession( (IServerSession *)pSessionHandle );
}

VOID DestroyServerSideConnectedObject( ISessionHandle * pSessionHandle )
{
	// Nothing to do

	//ServerSession *pServerSession = (ServerSession*)pSessionHandle;
	//ServerSessionFactory::Instance()->FreeServerSession( (ServerSession *)pSessionHandle );
}

//------------------------------------------------------------------------------------------------
void CallBackCommand( LPCTSTR szMessage )
{
	switch(szMessage[0])
	{
	case 'c':
		{
			DISPMSG	( "[CallBackCommand ]Server Session :(%d) \n"
						, theGateShell.GetNetworkGroup()->GetConnectAmount( SERVER_IOHANDLER ) );
		}
		break;
	case 'i':
		{

			
		}
		break;
	}
}

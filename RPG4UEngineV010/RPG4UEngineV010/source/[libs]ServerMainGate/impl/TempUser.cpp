/*////////////////////////////////////////////////////////////////////////
文 件 名：TempUser.cpp
创建日期：2009年6月16日
最后更新：2009年6月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "tempuser.h"
#include "UserManager.h"
#include <Version.h>
#include <ResultCode.h>
#include <PacketStruct_ClientGameS.h>
#include <PacketStruct_Gate_DBP.h>
#include <PacketStruct_Gate_Login.h>
#include <PacketStruct_Gate_Chat.h>

#include "GateShell.h"
#include "CharSelector.h"
#include "ZoneManager.h"
#include "TimeKeeper.h"
#include "ConnectionTimer.h"
#include "OnlineUser.h"
#include "TimeHelper.h"
#include "ServerInfoParser.h"

TempUser::TempUser()
	:	m_bFirstPacket ( TRUE )
{
	SetUserType(TEMP_USER);
}

TempUser::~TempUser()
{
}

VOID TempUser::OnDisconnect()
{
	// Don't remove UserManager
	sLOGOUT_INFO logoutDat;
	logoutDat.dwAuthUserID = GetAuthID();
	strncpy(logoutDat.szAccountID, GetUserID(), MAX_ID_LENGTH);
	logoutDat.szCharName[0]		= 0;
	logoutDat.wSvrCode			= theServerShell.GetChannelID();
	logoutDat.charLV				= 0;
	logoutDat.charType			= 0;
	logoutDat.byLogoutType		= GetLogoutType();
	logoutDat.szLogoutTime[0]	= NULL;

	DISPMSG( "[OnAccept] Client连接断开(%d) \n", logoutDat.dwAuthUserID);

	OnDisconnectAtLogin(&logoutDat);
}


BOOL TempUser::Init()
{

	__BOOL_SUPER(Init());
	return TRUE;
}

VOID TempUser::Release()
{
	_SUPER::Release();

	m_bFirstPacket = TRUE;
}


BOOL TempUser::_CheckUserSession	(UserSession * OUT                   pNewUser
											,MSG_CG_CONNECTION_ENTERSERVER_SYN * pMsg)
{
	DWORD					dwAuthUserID = pMsg->m_dwAuthID;
	ConnectionTimer * pUserInfo;

	/////////////////////////////////////////////////////
	pUserInfo = dynamic_cast<ConnectionTimer*>( theTimeKeeper.PeekTimer( dwAuthUserID ) );
	if( pUserInfo )
	{
		/////////////////////////////////////////////////////
		if(0 != memcmp	(pUserInfo->GetSerialKey()
							,pMsg->m_szSerialKey
							,MAX_AUTH_SERIAL_LENGTH*sizeof(TBYTE)))
		{
			LOGMSG( LOG_FULL, "[TempUser::_CheckUserSession] Login Serial Key:ClientSendKey is differnt!!");
			pUserInfo->Close();
			return FALSE;
		}

		//////////////////////////////////////////////////
		//pMsg->m_szID[MAX_ID_LENGTH-1]=0;
		if( 0 != lstrcmpi(pUserInfo->GetUserID(), pMsg->m_szID) )
		{
			LOGMSG( LOG_FULL	
					,  "[TempUser::_CheckUserSession] User[%s] != MsgID[%s] "
					, (LPCTSTR)pUserInfo->GetUserID()
					, pMsg->m_szID );
			pUserInfo->Close();
			return FALSE;
		}

		////////////////////////////////////////////
		pNewUser->GhostInfo(pUserInfo);

		pUserInfo->Close();
		return TRUE;	
	}
	else
	{
		LOGMSG( LOG_FULL,  "[TempUser::_CheckUserSession] Invalid User AuthID[%d].\n", dwAuthUserID );
	}
	
	return FALSE;
}


VOID TempUser::OnRecv( BYTE * pMsg, WORD wSize )
{
	if( !m_bFirstPacket )
		return;
	m_bFirstPacket = FALSE;


	/////////////////////////////////////////////////////////
	MSG_CG_CONNECTION_ENTERSERVER_NAK	msgNAK;
 	MSG_CG_CONNECTION_ENTERSERVER_SYN * pRecvMsg = (MSG_CG_CONNECTION_ENTERSERVER_SYN *)pMsg;
	OnlineUser *								pNewUser;
	UserSession * 								pExistUser;


	/////////////////////////////////////////////////////////
	if(VERSION_C2S_HIGH_NO		!= pRecvMsg->m_byHighVerNo		||
		VERSION_C2S_MIDDLE_NO	!= pRecvMsg->m_byMiddleVerNo	||
		VERSION_C2S_LOW_NO		>  pRecvMsg->m_byLowVerNo )
	{
		LOGMSG( LOG_FULL, "[TempUser::OnRecv] [VERSION:%d-%d-%d] Client Version[%d][%d][%d] Error!! "
				, VERSION_C2S_HIGH_NO
				, VERSION_C2S_MIDDLE_NO
				, VERSION_C2S_LOW_NO
				, pRecvMsg->m_byHighVerNo
				, pRecvMsg->m_byMiddleVerNo
				, pRecvMsg->m_byLowVerNo );

		SetLogoutType	(LOGOUT_WRONG_VERSION);
		msgNAK.m_dwErrorCode = RC::RC_LOGOUT_WRONG_VERSION;
		goto laFailed;
	}

	/////////////////////////////////////////////////////////
	pNewUser = (OnlineUser * )theUserManager.AllocUser( ONLINE_USER );
	pNewUser->SetUserKey( 0 );
	pNewUser->SetAuthID			( pRecvMsg->m_dwAuthID );
	//pNewUser->SetAuthSequence( pRecvMsg->m_dwAuthSequence );
	pNewUser->SetSessionIndex	( GetSessionIndex() );


	////////////////////////////////////////////////////////
	if(	theServerInfoParser.GetServerInfo()->bEnableLoginShell
		&& !_CheckUserSession( pNewUser, pRecvMsg ) )
	{
		LOGMSG( LOG_FULL
				, "[TempUser::OnRecv] [DISC] Is Not AuthenticatedUser: authID=%d"
				, pRecvMsg->m_dwAuthID );

		SetLogoutType(LOGOUT_WRONG_AUTHKEY);
		msgNAK.m_dwErrorCode = RC::RC_LOGOUT_WRONG_AUTHKEY;
		goto laFailed;
	}




	/////////////////////////////////////////////////////////////////////////////////////
	if( !theServerInfoParser.GetServerInfo()->bEnableLoginShell )
	{
		static int serialKey = 0;
		++serialKey;
		pNewUser->SetUserGUID( theServerInfoParser.GetServerInfo()->dwTestUserGUID );
		pNewUser->SetUserKey	( serialKey );
		pNewUser->SetUserID	( theServerInfoParser.GetServerInfo()->szUserAccount );
	}

	/////////////////////////////////////////////////////////////////////////////////////
	pExistUser = theUserManager.GetUser( pNewUser->GetUserKey() );
	if( NULL != pExistUser )
	{
		LOGMSG( LOG_FULL
				, "[TempUser::OnRecv] [U:%d]User分配出错\n"
				, pNewUser->GetUserKey() );

		pExistUser->SetLogoutType(LOGOUT_ETC);
		pExistUser->Disconnect();

		SetLogoutType(LOGOUT_ETC);
		msgNAK.m_dwErrorCode = RC::RC_LOGOUT_WRONG_AUTHKEY;
		goto laFailed;
	}


	/////////////////////////////////////////////////////////////////////////////////////
	//从GameDBProxy获取人物列表
	{
		MSG_AD_CHARINFO_CHARLISTREQ_SYN msg;
		msg.m_dwKey			= pNewUser->GetUserKey();
		msg.m_dwUserGUID	= pNewUser->GetUserGUID();

		if( FALSE == SendToDBProxy( &msg, sizeof(msg) ) )
		{
			LOGMSG( LOG_FULL,  "[TempUser::OnRecv] [ERROR]DBP invalid." );

			SetLogoutType(LOGOUT_ETC);

			msgNAK.m_dwErrorCode = RC::RC_LOGOUT_DB_ERROR;
			goto laFailed;
		}
	}


	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	Redirect( pNewUser );

	theUserManager.FreeUser( this );
	theUserManager.AddUser( pNewUser );


	DISPMSG( "[OnAccept] Client新连接加入(%s) \n", (LPCTSTR)pNewUser->GetUserID());


	/////////////////////////////////////////////////////////////////////////////////////
	//通知ChatShell
	{
		MSG_AW_CONNECTION_PREPARE_NEW_USER_SYN wmsg;
		wmsg.m_dwKey		= pNewUser->GetUserKey();
		wmsg.dwAuthUserID	= pNewUser->GetAuthID();
		wmsg.dwUserGuid	= pNewUser->GetUserGUID();
		memcpy( wmsg.szClientIP, pNewUser->GetClientIP(), MAX_IP_LENGTH );
		pNewUser->SendToChatShell( &wmsg, sizeof(wmsg) );
	}

	CharSelector * pCharSelect = theZoneManager.FindCharSelector(1);
	ASSERT( pCharSelect );
	if( pCharSelect )
		pCharSelect->EnterUser( pNewUser );



	/////////////////////////////////////////////////////////////////////////////////////
	// GateShell
	{
		MSG_AU_LOGIN_PC_LOGIN_CMD loginMsg;
		loginMsg.dwAuthUserID	= pNewUser->GetAuthID();
		loginMsg.wSvrCode			= theServerShell.GetChannelID();
		lstrcpyn(loginMsg.szID, pNewUser->GetUserID(), MAX_ID_LENGTH);
		theGateShell.SendToLogin( &loginMsg, (WORD)sizeof(loginMsg) );
	}


	/////////////////////////////////////////////////////////////////////////////////////
	pNewUser->BeginTransaction( TK_CHARACTER_LIST );
	pNewUser->WriteLoginTime();


	/////////////////////////////////////////////////////////////////////////////////////
	GAMELOG.LogPlayerLogin((TCHAR*)pNewUser->GetUserID()
                         ,pNewUser->GetSelectedCharName()
								 ,theGateShell.GetServerGUID()
								 ,pNewUser->GetIP()
								 ,pNewUser->GetSelectedCharClass()
								 ,pNewUser->GetSelectedCharLV());

	LOGCONN(FMSTR	("[TempUser::OnRecv] [UserKey:%d, Name:%s]:\n"
						, pNewUser->GetUserKey()
						, pNewUser->GetUserID()) );

	//ResetSendIdleCheck();
	return;

	/////////////////////////////////////////////
laFailed:
	{
		SetUserID		(pRecvMsg->m_szID);

		SendPacket(&msgNAK, sizeof(msgNAK));
		Disconnect	();

		if(pNewUser)
		{
			GAMELOG.LogPlayerLoginFail((TCHAR*)pNewUser->GetUserID()
												 ,pNewUser->GetSelectedCharName()
												 ,theGateShell.GetServerGUID()
												 ,pNewUser->GetIP());
			theUserManager.FreeUser( pNewUser );
		}
	}
}

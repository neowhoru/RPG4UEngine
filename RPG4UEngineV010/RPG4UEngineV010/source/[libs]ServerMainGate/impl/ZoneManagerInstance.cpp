/*////////////////////////////////////////////////////////////////////////
文 件 名：ZoneManagerInstance.cpp
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ZoneManagerInstance.h"
#include "Village.h"
#include "HuntingRoom.h"
#include "MissionRoom.h"
#include "LobbyRoom.h"
#include "PVPRoom.h"
#include "CharSelector.h"
#include "OnlineUser.h"
#include "ServerSessionManagerInstance.h"
#include "IServerSession.h"
#include <MapInfoParser.h>
#include "ServerInfoParser.h"
#include <ZoneFactoryManager.h>

#include <Village.h>
#include <PVPRoom.h>
#include <MissionRoom.h>
#include <LobbyRoom.h>
#include <HuntingRoom.h>
#include <CharSelector.h>

const DWORD		ZONEROME_COUNT	= 400 * 3;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ZoneManagerInstance, ()  , gamemain::eInstPrioGameFunc);



ZoneManagerInstance::ZoneManagerInstance(void)
{
}

ZoneManagerInstance::~ZoneManagerInstance(void)
{
}

BOOL ZoneManagerInstance::Init()
{
	sSERVER_INFOC *pDesc;
	pDesc = theServerInfoParser.GetServerInfo();

	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	const DWORD ROOM_POOL_SIZE = pDesc->dwGateZonePoolSize;
	////////////////////////////////////
	REG_ZONEFACTORY(Village			,ZONETYPE_VILLAGE		,	ROOM_POOL_SIZE);
	REG_ZONEFACTORY(PVPRoom			,ZONETYPE_PVP			,	ROOM_POOL_SIZE);
	REG_ZONEFACTORY(MissionRoom	,ZONETYPE_MISSION		,	ROOM_POOL_SIZE);
	REG_ZONEFACTORY(LobbyRoom		,ZONETYPE_LOBBY		,	ROOM_POOL_SIZE);
	REG_ZONEFACTORY(HuntingRoom	,ZONETYPE_HUNTING		,	ROOM_POOL_SIZE);
	REG_ZONEFACTORY(CharSelector		,ZONETYPE_CHARSELECT	,	ROOM_POOL_SIZE);


	return _SUPER::Init();
}

void  ZoneManagerInstance::Release()
{
	_SUPER::Release();
}



VOID ZoneManagerInstance::CreateCharSelect()
{
	CharSelector * pCharSelect = (CharSelector*)AllocZone(ZONETYPE_CHARSELECT);

	pCharSelect->Create( _AllocKey() );

	_AddRoom( pCharSelect );
}

eROOM_RESULT ZoneManagerInstance::CreateVillage(CODETYPE MapCode
                                       ,IZone **			ppVillage)
{
	Village * pVillage = (Village*)AllocZone(ZONETYPE_VILLAGE);

	IServerSession * pServerSession = theServerSessionManagerInstance.GetFieldServer();
	if( !pServerSession )
	{
		LOGMSG( LOG_FULL, "[CreateVillageRoom] 未发现相连接的FieldServer.1\n" );
		return RC_ROOM_FAILED;
	}
	pVillage->Create( _AllocKey(), MapCode, pServerSession );

	_AddRoom( pVillage );

	if( ppVillage )
		*ppVillage = pVillage;

	return RC_ROOM_SUCCESS;
}

eROOM_RESULT ZoneManagerInstance::CreateLobbyRoom	(KEYTYPE             VillageKey
                                          ,User *        pMasterUser
														,CODETYPE            MapCode
														,eZONE_TYPE          eLobbyType
														,eZONE_OPEN_STATE    ePublic
														,LPCTSTR             szRoomTitle
														,LPCTSTR             szRoomPwd
														,sROOMINFO_BASE & IN RoomInfo
														,LobbyRoom *& OUT    pRoomNew)
{
	///////////////////////////////////////////////
	if( GetZoneRoomCount() >= ZONEROME_COUNT  + GetVillageRoomCount() + GetCharSelectorRoomCount() )
	{
		LOGINFO	( "CreateLobbyRoom [%d]\n"
					, GetZoneRoomCount());
		return RC_ROOM_CANNOT_CREATE_ROOM_FOR_LIMIT;
	}

	///////////////////////////////////////////////
	IZone *				pVillage;
	IServerSession *	pServerSession;
	KEYTYPE				roomKey;
	eROOM_RESULT		rt;

	pVillage = FindZone( VillageKey );
	if( !pVillage || pVillage->GetType() != ZONETYPE_VILLAGE ) 
	{
		LOGINFO("[CreateLobbyRoom] Invalid Village Key! \n" );
		return RC_ROOM_NOTEXISTVILLAGE;
	}


	///////////////////////////////////////////////
	pServerSession = theServerSessionManagerInstance.GetFieldServer();
	if( !pServerSession )
	{
		LOGINFO("[CreateLobbyRoom] Invalid ServerSession.\n" );
		return RC_ROOM_FAILED;
	}

	///////////////////////////////////////////////
	roomKey		= _AllocKey();
	pRoomNew		= (LobbyRoom*)AllocZone(ZONETYPE_LOBBY);
	rt = pRoomNew->Create	((OnlineUser*)pMasterUser
                           ,roomKey
									,MapCode
									,eLobbyType
									,ePublic
									,szRoomTitle
									,szRoomPwd
									,RoomInfo
									,pServerSession);

	if( rt != RC_ROOM_SUCCESS )
		goto laFailed;
	

	/////////////////////////////////////////
	_AddRoom( pRoomNew );

	rt = MoveZone	(pVillage
                  ,pRoomNew
						,pMasterUser
						,ePublic
						,szRoomPwd);
	return rt;


	/////////////////////////////////////////
laFailed:
	{
		_FreeKey( roomKey );
		if(pRoomNew)
			FreeZone( pRoomNew );
		LOGMSG( LOG_FULL,  "[CreateLobbyRoom] Create Lobby Failed.\n");
	}
	return rt;
}



eROOM_RESULT ZoneManagerInstance::CreateHuntingRelay	(KEYTYPE                   PrevHuntingKey
                                                   ,User *              pMasterUser
																	,CODETYPE                  MapCode
																	,eZONE_OPEN_STATE          ePublic
																	,LPCTSTR                   szRoomPWD
																	,sROOMINFO_BASE & IN       RoomInfo
																	,sROOMINFO_ADDITIONAL & IN AdditionalRoomInfo
																	,HuntingRoom *& OUT        pRoomNew)
{
	///////////////////////////////////////////////
	IZone * pRoom = FindZone( PrevHuntingKey );
	if( !pRoom || pRoom->GetType() != ZONETYPE_HUNTING ) 
		return RC_ROOM_NOTEXISTHUNTINGROOM;

	///////////////////////////////////////////////
	HuntingRoom * pPrevRoom = (HuntingRoom *)pRoom;
	if( pPrevRoom->GetMaster() != pMasterUser )
		return RC_ROOM_ISNOTMATERPLAYER;

	///////////////////////////////////////////////
	KEYTYPE			roomKey;
	eROOM_RESULT	rt;

	roomKey			= _AllocKey();
	pRoomNew	= (HuntingRoom*)AllocZone(ZONETYPE_HUNTING);
	rt = pRoomNew->Create((OnlineUser*)pMasterUser
                        ,roomKey
								,MapCode
								,ePublic
								,pPrevRoom->GetRoomTitle()
								,szRoomPWD
								,RoomInfo
								,AdditionalRoomInfo
								,pPrevRoom->GetLinkedShell());

	///////////////////////////////////////////////
	if( rt != RC_ROOM_SUCCESS )
		goto laFailed;

	if( pRoomNew->GetLimitUserMax() < pPrevRoom->GetUserNum() )
	{
		rt = RC_ROOM_FAILED;
		goto laFailed;
	}

	//////////////////////////////////////////////////
	pRoomNew->RelayUser( pPrevRoom );
	pPrevRoom	->SetReusedServerSession(TRUE);

	_AddRoom( pRoomNew );

	//////////////////////////////////////////////////
	rt = MoveZone(pPrevRoom
                ,pRoomNew
					 ,pMasterUser
					 ,ePublic
					 ,szRoomPWD);
	
	return RC_ROOM_SUCCESS;

laFailed:
	{
		_FreeKey( roomKey );
		if(pRoomNew)
			FreeZone( pRoomNew );
	}
	return rt;
}


eROOM_RESULT ZoneManagerInstance::CreateHuntingRoom	(KEYTYPE            roomKey
																		,User *       pMasterUser
																		,HuntingRoom *& OUT pHuntingRoom)
{
	return CreateRoom	(ZONETYPE_HUNTING
							,roomKey
							,pMasterUser
							,(IRoom*&)pHuntingRoom);
}

eROOM_RESULT ZoneManagerInstance::CreateMissionRoom	(KEYTYPE            roomKey
                                             ,User *       pMasterUser
															,MissionRoom *& OUT pMissionRoom)
{
	return CreateRoom	(ZONETYPE_MISSION
							,roomKey
							,pMasterUser
							,(IRoom*&)pMissionRoom);
}


eROOM_RESULT ZoneManagerInstance::CreatePVPRoom	(KEYTYPE        roomKey
                                          ,User *   pMasterUser
														,PVPRoom *& OUT pPVPRoom)
{
	return CreateRoom	(ZONETYPE_PVP
							,roomKey
							,pMasterUser
							,(IRoom*&)pPVPRoom);
}


eROOM_RESULT ZoneManagerInstance::MoveZone	(eZONE_TYPE       FromZoneType
                                    ,KEYTYPE          FromZoneKey
												,eZONE_TYPE       ToZoneType
												,KEYTYPE          ToZoneKey
												,User *     pUser
												,eZONE_OPEN_STATE RoomPublic
												,LPCTSTR          szRoomPwd)
{
	/////////////////////////////////////////////////////
	IZone * pFromZone	= FindZone( FromZoneKey );
	if( !pFromZone || pFromZone->GetType() != FromZoneType )
		return RC_ROOM_NOTEXISTROOM;

	/////////////////////////////////////////////////////
	IZone * pToZone		= FindZone( ToZoneKey );
	if( !pToZone || pToZone->GetType() != ToZoneType )
		return RC_ROOM_NOTEXISTROOM;

	/////////////////////////////////////////////////////
	eROOM_RESULT rt;
	rt = pToZone->CanJoin( pUser, RoomPublic, szRoomPwd );
	if( RC_ROOM_SUCCESS != rt ) 
		return rt;

	return MoveZone(pFromZone
                  ,pToZone
						,pUser
						,RoomPublic
						,szRoomPwd);
}



eROOM_RESULT ZoneManagerInstance::MoveZone	(IZone *  pFromZone
                                    ,IZone *  pToZone
												,User *     pUser
												,eZONE_OPEN_STATE RoomPublic
												,LPCTSTR          szRoomPwd)
{
	if( pFromZone->GetLinkedShell() == pToZone->GetLinkedShell() )
	{
		pFromZone->LeaveUser( pUser, eSEND_CMD );
		pToZone->ReadyUser( pUser, eSEND_CMD );
	}
	else
	{
		pFromZone->LeaveUser( pUser, eSEND_SYN );
	}

	return RC_ROOM_SUCCESS;
}


BOOL ZoneManagerInstance::_PrevCreateRoom(IServerSession*&	pServerSession)
{
	DWORD dwServerSessionIndex = theServerSessionManagerInstance.IncLOFBattleServer();
	if( dwServerSessionIndex == 0 ) 
	{
		LOGMSG( LOG_FULL
				,  "[_PrevCreateRoom] m_pLOFBattleServer doesn't exist!\n" );
		return FALSE;
	}

	pServerSession = theServerSessionManagerInstance.FindServer( dwServerSessionIndex );
	if(	!pServerSession 
		|| pServerSession->GetServerType() != BATTLE_SHELL )
	{
		LOGMSG( LOG_FULL,  "[_PrevCreateRoom] Failed.\n" );
		return FALSE;
	}
	return TRUE;
}





eROOM_RESULT ZoneManagerInstance::SetMaxSlotNumInLobby( User * pMaster, BYTE MaxUser )
{
	if( pMaster->GetZoneState() != ZONESTATE_ATLOBBY )
		return RC_ROOM_INVALIDROOMTYPE;

	LobbyRoom * pRoomNew = FindLobby(pMaster->GetZoneKey());
	if( !pRoomNew ) 
		return RC_ROOM_NOTEXISTLOBBY;

	if( pMaster!= pRoomNew->GetMaster() )
		return RC_ROOM_NOT_CMD_FOR_MEMBER;

	sMAPINFO_BASE * pInfo = theMapInfoParser.GetMapInfo(pRoomNew->GetMapCode());
	if( !pInfo ) 
		return RC_ROOM_INVALID_MAPCODE;

	if( pInfo->byMinUserNum > MaxUser || pInfo->byMaxUserNum < MaxUser )
		return RC_ROOM_INVALID_LIMITUSERNUM;

	pRoomNew->SetLimitUserMax( MaxUser );

	return RC_ROOM_SUCCESS;
}


eROOM_RESULT ZoneManagerInstance::ChangeMapInLobby( User * pMaster, CODETYPE MapCode )
{

	if( pMaster->GetZoneState() != ZONESTATE_ATLOBBY )
		return RC_ROOM_INVALIDROOMTYPE;

	LobbyRoom * pRoomNew = FindLobby(pMaster->GetZoneKey());
	if( !pRoomNew ) 
		return RC_ROOM_NOTEXISTLOBBY;

	if( pMaster != pRoomNew->GetMaster() )
		return RC_ROOM_NOT_CMD_FOR_MEMBER;

	return pRoomNew->ChangeMap( MapCode );
}


eROOM_RESULT ZoneManagerInstance::ConfigHuntingRoomInLobby( User * pMaster, sROOMINFO_ADDITIONAL & IN AdditionalInfo )
{
	if( pMaster->GetZoneState() != ZONESTATE_ATLOBBY )
		return RC_ROOM_INVALIDROOMTYPE;

	LobbyRoom * pRoomNew = FindHuntingLobby(pMaster->GetZoneKey());
	if( !pRoomNew ) 
		return RC_ROOM_NOTEXISTLOBBY;

	if( pMaster != pRoomNew->GetMaster() )
		return RC_ROOM_NOT_CMD_FOR_MEMBER;

	if( AdditionalInfo.m_Bonus >= HUNTINGBONUS_MAX ) 
		return RC_ROOM_INVALID_BONUS_VALUE;

	if( AdditionalInfo.m_Difficulty >= HUNTING_DIFFICULTY_MAX ) 
		return RC_ROOM_INVALID_DIFFICULTY_VALUE;

	pRoomNew->SetAdditionalInfo( AdditionalInfo );

	return RC_ROOM_SUCCESS;
}


eROOM_RESULT ZoneManagerInstance::ConfigPVPRoomInLobby(User *             pMaster
                                              ,sPVPINFO_ADDITIONAL & IN AdditionalPVPInfo)
{

	/////////////////////////////////////////////
	if( pMaster->GetZoneState() != ZONESTATE_ATLOBBY )
		return RC_ROOM_INVALIDROOMTYPE;

	/////////////////////////////////////////////
	LobbyRoom * pRoomNew = FindPVPLobby(pMaster->GetZoneKey());
	if( !pRoomNew ) 
		return RC_ROOM_NOTEXISTLOBBY;

	/////////////////////////////////////////////
	if( pMaster != pRoomNew->GetMaster() )
		return RC_ROOM_NOT_CMD_FOR_MEMBER;

	/////////////////////////////////////////////
	if( AdditionalPVPInfo.m_Rule >= PVPRULE_MAX ) 
		return RC_ROOM_INVALID_RULE_VALUE;
	if( AdditionalPVPInfo.m_Mode >= PVPMODE_MAX ) 
		return RC_ROOM_INVALID_MODE_VALUE;


	/////////////////////////////////////////////
	if( pRoomNew->GetAdditionalPVPInfo().m_Mode != AdditionalPVPInfo.m_Mode )
	{
		pRoomNew->SetAdditionalPVPInfo( AdditionalPVPInfo );
		pRoomNew->AssignTeam();
	}
	else
	{
		pRoomNew->SetAdditionalPVPInfo( AdditionalPVPInfo );
	}

	return RC_ROOM_SUCCESS;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<class TYPE,class INFO>
class TZoneManagerInstanceGetLobbyRoomListOpr
{
public:
	User *   pUser;
	BYTE     nMaxCount;
	BYTE &	nCount;
	INFO*		pRoomInfoArray;

	////////////////////////////////////////////////////////
	BOOL operator()(TYPE*	pRoomNew)
	{
		if( RC_ROOM_SUCCESS == pRoomNew->CanJoin( pUser, ZONEOPEN_PUBLIC, NULL ) )
		{
			pRoomNew->SerializeLoad(pRoomInfoArray[nCount]);
			++nCount;
			if( nCount == nMaxCount )
				return FALSE;
		}
		return TRUE;
	}
};

VOID ZoneManagerInstance::GetMissionLobbyRoomList	(User *            pUser
																	,BYTE                    /*ReqCount*/
																	,BYTE                    nMaxCount
																	,BYTE & OUT              nCount
																	,sROOMINFO_MISSION * OUT pRoomInfoArray)
{
	nCount = 0;
	TZoneManagerInstanceGetLobbyRoomListOpr<LobbyRoom,sROOMINFO_MISSION>	opr={pUser
																			 , nMaxCount
																			 , nCount
																			 , pRoomInfoArray};
	ForEachMissionLobby(opr);

}

VOID ZoneManagerInstance::GetHuntingLobbyRoomList(User *            pUser
                                         ,BYTE                    ReqCount
													  ,BYTE                    nMaxCount
													  ,BYTE & OUT              nCount
													  ,sROOMINFO_HUNTING * OUT pRoomInfoArray)
{
	nCount = 0;
	TZoneManagerInstanceGetLobbyRoomListOpr<LobbyRoom,sROOMINFO_HUNTING>	opr={pUser
																			 , nMaxCount
																			 , nCount
																			 , pRoomInfoArray};
	ForEachHuntingLobby(opr);

}

VOID ZoneManagerInstance::GetPVPLobbyRoomList	(User *        pUser
                                       ,BYTE                ReqCount
													,BYTE                nMaxCount
													,BYTE & OUT          nCount
													,sROOMINFO_PVP * OUT pRoomInfoArray)
{
	nCount = 0;
	TZoneManagerInstanceGetLobbyRoomListOpr<LobbyRoom,sROOMINFO_PVP>	opr={pUser
																			 , nMaxCount
																			 , nCount
																			 , pRoomInfoArray};
	ForEachPVPLobby(opr);

}

VOID ZoneManagerInstance::GetHuntingRoomList(User *            pUser
                                    ,BYTE                    ReqCount
												,BYTE                    nMaxCount
												,BYTE & OUT              nCount
												,sROOMINFO_HUNTING * OUT pRoomInfoArray)
{
	nCount = 0;
	TZoneManagerInstanceGetLobbyRoomListOpr<LobbyRoom,sROOMINFO_HUNTING>	opr={pUser
																			 , nMaxCount
																			 , nCount
																			 , pRoomInfoArray};
	ForEachHuntingLobby(opr);

}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class TZoneManagerInstanceFindRecommendRoomOpr
{
public:
	IRoom*		m_pRoomRet;
	IRoom*		m_pGeneralRoom;
	User*			pUser;
	////////////////////////////////////////////////////////
	BOOL operator()(IRoom*	pRoomNew)
	{
		eROOM_RESULT rt = pRoomNew->CheckRecommend( pUser );
		if( RC_ROOM_SUCCESS_FOR_RECOMMEND_JOIN == rt )
		{
			m_pRoomRet	= pRoomNew;
			return FALSE;
		}
		else if( RC_ROOM_SUCCESS_FOR_GENERAL_JOIN == rt )
		{
			m_pGeneralRoom = (IRoom *)pRoomNew;
		}
		return TRUE;
	}
};

IRoom * ZoneManagerInstance::GetRecommendRoom( User * pUser, eZONE_TYPE RoomType )
{
	TZoneManagerInstanceFindRecommendRoomOpr	opr={NULL,NULL,pUser};

	//IRoom* pGeneralRoom = NULL;
	switch( RoomType )
	{
	case ZONETYPE_PVP:
		{
			ForEachPVPLobby(opr);
			if(opr.m_pRoomRet)
				return opr.m_pRoomRet;

		}
		break;

	////////////////////////////////////////////////
	case ZONETYPE_MISSION:
		{
			ForEachMissionLobby(opr);
			if(opr.m_pRoomRet)
				return opr.m_pRoomRet;


		}
		break;

	////////////////////////////////////////////////
	case ZONETYPE_HUNTING:
		{
			ForEachHuntingLobby(opr);
			if(opr.m_pRoomRet)
				return opr.m_pRoomRet;
			if(opr.m_pGeneralRoom)
				return opr.m_pGeneralRoom;

			/////////////////////////////////////
			ForEachHunting(opr);
			if(opr.m_pRoomRet)
				return opr.m_pRoomRet;


		}
		break;
	}

	return opr.m_pGeneralRoom;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ZoneManagerInstanceProcessOpr
{
public:
	ZoneManagerInstance*	m_pMan;

	BOOL operator()(IZone* pZone)
	{
		if( !pZone->Process() )
		{
			m_pMan->DestroyZone( pZone );
		}
		return TRUE;
	}
};
VOID ZoneManagerInstance::Process()
{
	ZoneManagerInstanceProcessOpr	opr={this};
	ForEachZoneFirst(opr);

}

VOID ZoneManagerInstance::DestroyZone( IZone * pZone )
{
	_RemoveRoom(pZone);

	if( !pZone->IsReusedKey() )
		_FreeKey( pZone->GetKey() );

	eZONE_TYPE eType = pZone->GetType();

	pZone->Destroy();

	FreeZone(pZone);
}




DWORD ZoneManagerInstance::GetHuntingRoomAmount()
{
	return GetHuntingLobbyRoomCount()+GetHuntingRoomCount();
}

DWORD ZoneManagerInstance::GetMissionRoomAmount()
{
	return GetMissionLobbyRoomCount()+GetMissionRoomCount();
}

VOID ZoneManagerInstance::Display()
{
	DWORD lobby	= GetHuntingLobbyRoomUserCount()+GetMissionLobbyRoomUserCount()+GetPVPLobbyRoomUserCount();

	DISPMSG(  "======================================================\n" );
	DISPMSG(  "CHARSELECT : %u\n", GetCharSelectorRoomCount() );
	DISPMSG(  "VILLAGE : %u, User's# :%u\n", GetVillageRoomCount(), GetVillageUserCount() );
	DISPMSG(  "LOBBY   : %u, User's# :%u\n", GetLobbyRoomCount(), lobby );
	DISPMSG(  "MISSION : %u, User's# :%u\n", GetMissionRoomCount(), GetMissionRoomUserCount() );
	DISPMSG(  "HUNTING : %u, User's# :%u\n", GetHuntingRoomCount(), GetHuntingRoomUserCount() );
	DISPMSG(  "PVP	: %u, User's# :%u\n", GetPVPRoomCount(), GetPVPRoomUserCount() );
	DISPMSG(  "======================================================\n" );	
}

VOID ZoneManagerInstance::DisplayerZonePoolInfo()
{
	theZoneFactoryManager.LogAllFactories();
}



eROOM_RESULT ZoneManagerInstance::KickUserAtLobby(User *  pMaster
                                           ,DWORD          TargetPlayerKey
														 ,User *& OUT pTargetUser)
{
	__CHECK2	(pMaster->GetZoneState() == ZONESTATE_ATLOBBY
				,RC_ROOM_INVALIDROOMTYPE);


	////////////////////////////////////////
	LobbyRoom * pLobbyRoom = FindLobby(pMaster->GetZoneKey());
	__CHECK2_PTR	(pLobbyRoom
						,RC_ROOM_NOTEXISTLOBBY);

	////////////////////////////////////////
	__CHECK2	(pMaster == pLobbyRoom->GetMaster()
				,RC_ROOM_NOT_CMD_FOR_MEMBER);


	pTargetUser = pLobbyRoom->FindUser( TargetPlayerKey );


	////////////////////////////////////////
	__CHECK2_PTR	(pTargetUser
						,RC_ROOM_NOT_EXIST_MEMBER);

	pLobbyRoom->LeaveUser( pTargetUser, eSEND_CMD );

	////////////////////////////////////////
	Village * pVillage = FindVillage(((OnlineUser*)pTargetUser)->GetSelectedCharMapCode());
	__CHECK2_PTR	(pVillage
						,RC_ROOM_NOTEXISTVILLAGE);

	pVillage->ReadyUser( pTargetUser, eSEND_CMD );

	return RC_ROOM_SUCCESS;
}


eROOM_RESULT ZoneManagerInstance::ToggleReadyInLobby( User * pUser, BOOL bReady )
{
	////////////////////////////////////////
	__CHECK2	(pUser->GetZoneState() == ZONESTATE_ATLOBBY
				,RC_ROOM_INVALIDROOMTYPE);


	LobbyRoom * pLobbyRoom = FindLobby(pUser->GetZoneKey());
	////////////////////////////////////////
	__CHECK2_PTR(pLobbyRoom
					,RC_ROOM_NOTEXISTLOBBY);


	////////////////////////////////////////
	__CHECK2	(pUser != pLobbyRoom->GetMaster()
				,RC_ROOM_NOT_CMD_FOR_MASTER);


	////////////////////////////////////////
	if( bReady == TRUE )
	{
		if( pUser->IsLobbyReady() )
		{
			return RC_ROOM_ALREADY_READY_STATE;
		}
		pUser->SetLobbyReady(TRUE);
	}
	else
	{
		if( !pUser->IsLobbyReady() )
		{
			return RC_ROOM_ALREADY_NOT_READY_STATE;
		}
		pUser->SetLobbyReady(FALSE);
	}

	return RC_ROOM_SUCCESS;
}

eROOM_RESULT ZoneManagerInstance::ChangeTeamInLobby( User * pUser, ePARTY_INDEX team)
{
	////////////////////////////////////////
	__CHECK2	(pUser->GetZoneState() == ZONESTATE_ATLOBBY
				,RC_ROOM_INVALIDROOMTYPE);

	LobbyRoom * pLobbyRoom = FindPVPLobby(pUser->GetZoneKey());
	////////////////////////////////////////
	__CHECK2_PTR(pLobbyRoom
					,RC_ROOM_NOTEXISTLOBBY);

	////////////////////////////////////////
	__CHECK2	(pLobbyRoom->IsPVPTeamMode()
				,RC_ROOM_INVALID_TEAM);

	////////////////////////////////////////
	__CHECK2	(team < PARTY_INDEX_MAX
				,RC_ROOM_INVALID_TEAM);

	////////////////////////////////////////
	__CHECK2	(pUser->GetTeam() != team
				,RC_ROOM_INVALID_TEAM);

	pUser->SetTeam( team );

	return RC_ROOM_SUCCESS;
}



VOID ZoneManagerInstance::_AddRoom( IZone * pRoom )
{

	switch( pRoom->GetType() )
	{
		/////////////////////////////////////
	case ZONETYPE_VILLAGE:
		{
			Village * pVRoom = (Village *)pRoom;
			ASSERT( NULL == FindVillage( pVRoom->GetMapCode() ) );
			AddVillage( pVRoom, pVRoom->GetMapCode() );
		}
		break;

		/////////////////////////////////////
	case ZONETYPE_LOBBY:
		{
			LobbyRoom * pVRoom = (LobbyRoom *)pRoom;
			switch(pVRoom->GetLobbyType())
			{
			/////////////////////////////////////
			case ZONETYPE_HUNTING:
				{
					ASSERT( NULL == FindHuntingLobby( pVRoom->GetKey() ) );
					AddHuntingLobby( pVRoom, pVRoom->GetKey() );
				}break;
			/////////////////////////////////////
			case ZONETYPE_MISSION:
				{
					ASSERT( NULL == FindMissionLobby( pVRoom->GetKey() ) );
					AddMissionLobby( pVRoom, pVRoom->GetKey() );
				}break;
			/////////////////////////////////////
			case ZONETYPE_PVP:
				{
					ASSERT( NULL == FindPVPLobby( pVRoom->GetKey() ) );
					AddPVPLobby( pVRoom, pVRoom->GetKey() );
				}break;
			}
			ASSERT( NULL == FindLobby( pVRoom->GetKey() ) );
			AddLobby( pVRoom, pVRoom->GetKey() );
		}
		break;

		/////////////////////////////////////
	case ZONETYPE_HUNTING:
		{
			HuntingRoom * pVRoom = (HuntingRoom *)pRoom;
			ASSERT( NULL == FindHunting( pVRoom->GetKey() ) );
			AddHunting( pVRoom, pVRoom->GetKey() );
		}
		break;

		/////////////////////////////////////
	case ZONETYPE_MISSION:
		{
			MissionRoom * pVRoom = (MissionRoom *)pRoom;
			ASSERT( NULL == FindMission( pVRoom->GetKey() ) );
			AddMission( pVRoom, pVRoom->GetKey() );
		}
		break;

		/////////////////////////////////////
	case ZONETYPE_PVP:
		{
			PVPRoom * pVRoom = (PVPRoom *)pRoom;
			ASSERT( NULL == FindPVP( pVRoom->GetKey() ) );
			AddPVP( pVRoom, pVRoom->GetKey() );
		}
		break;

		/////////////////////////////////////
	case ZONETYPE_CHARSELECT:
		{
			CharSelector * pVRoom = (CharSelector *)pRoom;
			ASSERT( NULL == FindCharSelector( pVRoom->GetKey() ) );
			AddCharSelector( pVRoom, pVRoom->GetKey() );
		}
		break;
	}

	/////////////////////////////////////
	ASSERT( NULL == FindZone( pRoom->GetKey() ) );
	AddZone( pRoom, pRoom->GetKey() );
}

VOID ZoneManagerInstance::_RemoveRoom( IZone * pRoom )
{
	switch( pRoom->GetType() )
	{
	/////////////////////////////////////
	case ZONETYPE_VILLAGE:
		{
			Village * pVRoom = (Village *)pRoom;
			ASSERT( NULL != FindVillage( pVRoom->GetMapCode() ) );
			RemoveVillage( pVRoom->GetMapCode() );
		}
		break;

	/////////////////////////////////////
	case ZONETYPE_LOBBY:
		{
			LobbyRoom * pVRoom = (LobbyRoom *)pRoom;
			switch(pVRoom->GetLobbyType())
			{

			/////////////////////////////////////
			case ZONETYPE_HUNTING:
				{
					ASSERT( NULL != FindHuntingLobby( pVRoom->GetKey() ) );
					RemoveHuntingLobby( pVRoom->GetKey() );
				}break;

			/////////////////////////////////////
			case ZONETYPE_MISSION:
				{
					ASSERT( NULL != FindMissionLobby( pVRoom->GetKey() ) );
					RemoveMissionLobby( pVRoom->GetKey() );
				}break;

			/////////////////////////////////////
			case ZONETYPE_PVP:
				{
					ASSERT( NULL != FindPVPLobby( pVRoom->GetKey() ) );
					RemovePVPLobby( pVRoom->GetKey() );
				}break;
			}

			/////////////////////////////////////
			ASSERT( NULL != FindLobby( pVRoom->GetKey() ) );
			RemoveLobby( pVRoom->GetKey() );
		}
		break;

	/////////////////////////////////////
	case ZONETYPE_HUNTING:
		{
			HuntingRoom * pVRoom = (HuntingRoom *)pRoom;
			ASSERT( NULL != FindHunting( pVRoom->GetKey() ) );
			RemoveHunting( pVRoom->GetKey() );
		}
		break;

	/////////////////////////////////////
	case ZONETYPE_MISSION:
		{
			MissionRoom * pVRoom = (MissionRoom *)pRoom;
			ASSERT( NULL != FindMission( pVRoom->GetKey() ) );
			RemoveMission( pVRoom->GetKey() );
		}
		break;

	/////////////////////////////////////
	case ZONETYPE_PVP:
		{
			PVPRoom * pVRoom = (PVPRoom *)pRoom;
			ASSERT( NULL != FindPVP( pVRoom->GetKey() ) );
			RemovePVP( pVRoom->GetKey() );
		}
		break;

	/////////////////////////////////////
	case ZONETYPE_CHARSELECT:
		{
			CharSelector * pVRoom = (CharSelector *)pRoom;
			ASSERT( NULL != FindCharSelector( pVRoom->GetKey() ) );
			RemoveCharSelector( pVRoom->GetKey() );
		}
		break;
	}

	/////////////////////////////////////
	ASSERT( NULL != FindZone( pRoom->GetKey() ) );
	RemoveZone( pRoom->GetKey() );
}

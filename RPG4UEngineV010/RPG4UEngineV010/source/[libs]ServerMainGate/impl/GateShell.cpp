/*////////////////////////////////////////////////////////////////////////
文 件 名：GateShell.cpp
创建日期：2009年4月2日
最后更新：2009年4月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include <NetworkGroup.h>
#include <ISessionHandle.h>
#include <conio.h>
#include "GateShell.h"
#include "FilePathUtil.h"
#include "ServerSetting.h"
#include "ServerInfoParser.h"


#include <PacketStruct_ClientGameS.h>
#include <Protocol_ClientAgent.h>
#include <PacketStruct_Gate_Chat.h>

#include "ServerSessionManager.h"
#include "ServerSession.h"

#include "OnlineUser.h"
#include "UserManager.h"
#include "ConnectionTimerManager.h"
#include "TimeKeeper.h"
#include "ConnectionTimer.h"
#include "LogoutUserManager.h"
#include <Version.h>
#include <BuildVersion.h>
//#include <ServerGlobal.h>
#include <MapInfoParser.h>
#include <LimitedMapPortParser.h>
#include "ZoneManager.h"
#include <RequireFieldLevelInfoParser.h>
#include "GateShell.inc.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GateShell, ()  , gamemain::eInstPrioGameFunc);


GateShell::GateShell()

{
	m_dwSeverEnableFlags	= _BIT(PLAYER_DBPROXY)
								| _BIT(CHAT_SHELL)
								//| _BIT(FIELD_SHELL)
								| _BIT(LOGIN_SHELL)
								| _BIT(GUILD_SHELL)
								;

	SetDBProxyState( SERVER_NORMAL );
	SetServerGUID( GATE_SHELL );

	m_pSpeedHackLogger	= new ThreadableLogger;
	m_pLog					= new ThreadableLogger;
}

GateShell::~GateShell()
{
	SAFE_DELETE(m_pLog);
	SAFE_DELETE( m_pSpeedHackLogger );
}

VOID GateShell::Release()
{
#define DISPRELEASE(x)		DISPMSG(x);	Sleep(1)

	DISPRELEASE( "  Release Resources.. \n" );


	//-------------------------------------------------------------------------------------------------



	//-------------------------------------------------------------------------------------------------


	//-------------------------------------------------------------------------------------------------
	DISPRELEASE( "  Release Users.. \n" );


	_SUPER::Release();

	theTimeKeeper.Release();
	theConnectionTimerManager.Release();

	theUserManager.Release(  );
	//theUserManager.ReleaseFactory();

	//-------------------------------------------------------------------------------------------------
	DISPRELEASE( "  Release Other datas.. \n" );





	DISPRELEASE("Release Finished!");
}

BOOL GateShell::_InitLog	(sSERVERLOG_INFO*	pSvrLogInfo)
{
	__BOOL_SUPER(_InitLog	(pSvrLogInfo));

	__CHECK(_InitGameLog(GAMELOG, theServerSetting.m_szServerName));






	////////////////////////////////////////
	StringHandle	sLogFile;
	StringHandle	sLogDir;

	theGateShellLog.Init();

	////////////////////////////////////////
	sLogFile.Format(_T("SpeedHack_%s_%d")
						,theServerSetting.m_szServerName
						,theServerSetting.m_dwServerID);
	sLogDir.Format	(_T("%s\\SpeedHackerLog")
						,theServerSetting.m_LogInfo.szLogFilePath);

	m_pSpeedHackLogger->Create(sLogDir , sLogFile );

	////////////////////////////////////////
	sLogFile.Format(_T("Packet_%s_%d")
						,theServerSetting.m_szServerName
						,theServerSetting.m_dwServerID);
	
	sLogDir.Format	(_T("%s\\PacketFlow")
						,theServerSetting.m_LogInfo.szLogFilePath);
	__INIT(m_pLog->Create(sLogDir   , sLogFile ) );

	return TRUE;
}


BOOL GateShell::_InitNetwork()
{
	if ( GetServerInfo()->bEnableLoginShell )
		m_dwSeverEnableFlags	|= _BIT(LOGIN_SHELL);

	__BOOL_SUPER(_InitNetwork());

	//_GAMELOGINIT(GameLogAgent)
	sNETWORK_INFO ioDesc[IOHANDLER_MAX];


	ioDesc[0].fnCreateAcceptedSession	= CreateClientSideAcceptedObject;
	ioDesc[0].fnDestroyAcceptedSession	= DestroyClientSideAcceptedObject;
	ioDesc[0].fnDestroyWorkingSession	= DestroyClientSideConnectedObject;

	/////////////////////////////////////////////////

	ioDesc[1].fnCreateAcceptedSession	= CreateServerSideAcceptedObject;
	ioDesc[1].fnDestroyAcceptedSession	= DestroyServerSideAcceptedObject;
	ioDesc[1].fnDestroyWorkingSession	= DestroyServerSideConnectedObject;

	return _InitNetworkCS(ioDesc);



}


BOOL GateShell::Init()
{
	sSERVER_INFOC * pServerEnv;
	
	pServerEnv			= GetServerInfo();


	////////////////////////////////////
	__BOOL_SUPER(Init());


	LOGMSG( LOG_FULL, "Agent Server Start " );

	//-------------------------------------------------------------------------------------------------
	//-------------------------------------------------------------------------------------------------
#ifdef _DEBUG
	//theUserManager.InitFactory			( 10 );
	theUserManager.Init			( 10 );
	theConnectionTimerManager.Init		( 10 );
#else
	//theUserManager.InitFactory( 5000 );
	theUserManager.Init( 5000 );
	theConnectionTimerManager.Init( 5000 );
#endif
	theTimeKeeper.Init	( 5000 );
	theZoneManager.CreateCharSelect();


	////////////////////////////////////////////////
	//网络初始化


	////////////////////////////////////////////////
	//SetMasterServerAddr( pServerEnv->pszMasterServerIP, pServerEnv->wMasterServerPort );
	if(theServerSetting.m_bServerInfoFromFile)
	{

	}
	else
	{

	}


	m_userCountReportTimer.SetTimer( 10000 );
	return TRUE;
}


VOID GateShell::Run(DWORD dwDelay)
{
	if(theServerSetting.m_bServerInfoFromFile)
	{
		//在GameDBProxy连接成功后连接
		//StartListen(LISTEN_SERVER);
	}


	_SUPER::Run(dwDelay);

}

void GateShell::OnRunEnd		()
{
	if(m_pLog)
	{
		m_pLog->Destory();
	}
	if(m_pSpeedHackLogger)
	{
		m_pSpeedHackLogger->Destory();
	}

	if(theGateShellLog.GetConnectLog())
	{
		theGateShellLog.GetConnectLog()->SetDisplayRunning(FALSE);
		theGateShellLog.GetConnectLog()->SetFileRunning	(FALSE);
	}
	_SUPER::OnRunEnd();
}



BOOL GateShell::FrameMove(DWORD dwTick)
{
	__BOOL_SUPER(FrameMove(dwTick));


	// timeout
	theTimeKeeper.Update();

	theZoneManager.Process();

	//if( m_userCountReportTimer.IsExpired() )
	//{
	//	MSG_AW_USERCOUNT_SYN msg;
	//	msg.dwVillageUserCount = theZoneManager.GetVillageUserCount();		
	//	msg.dwHuntingLobbyZoneCount = theZoneManager.GetHuntingLobbyRoomCount();
	//	msg.dwHuntingLobbyUserCount = theZoneManager.GetHuntingLobbyRoomUserCount();
	//	msg.dwMissionLobbyZoneCount = theZoneManager.GetMissionLobbyRoomCount();
	//	msg.dwMissionLobbyUserCount = theZoneManager.GetMissionLobbyRoomUserCount();
	//	msg.dwPVPLobbyZoneCount = theZoneManager.GetPVPLobbyRoomCount();	
	//	msg.dwPVPLobbyUserCount = theZoneManager.GetPVPLobbyRoomUserCount();
	//	msg.dwHuntingZoneCount = theZoneManager.GetHuntingRoomCount();		
	//	msg.dwHuntingUserCount = theZoneManager.GetHuntingRoomUserCount();
	//	msg.dwMissionZoneCount = theZoneManager.GetMissionRoomCount();		
	//	msg.dwMissionUserCount = theZoneManager.GetMissionRoomUserCount();
	//	msg.dwPVPZoneCount = theZoneManager.GetPVPRoomCount();			
	//	msg.dwPVPUserCount = theZoneManager.GetPVPRoomUserCount();
	//	
	//	if( m_pWorldServerSession )
	//	{
	//		m_pWorldServerSession->Send( (BYTE*)&msg, sizeof(msg) );
	//	}

	//	TCHAR szMsg[128];
	//	DWORD nTotal = msg.GetTotalUserNum();
	//	_snprintf(szMsg, sizeof(szMsg), "[GateShell::Update] =  %u", nTotal);
	//	SUNCOUNTLOG->OutputMsg( szMsg, LOG_MIDDLE );
	//}



	return TRUE;
}

VOID GateShell::Shutdown	()
{
	_SUPER::Shutdown();
}

void GateShell::DisconnectUsers()
{
	LOGINFO(_T("Disconnect All Users %\n"));

	OnlineUser * pUser = NULL;
	MSG_CG_CONNECTION_RESTART_CMD msg;

	theUserManager.SetFirst();
	while( pUser = (OnlineUser *)theUserManager.GetNext() )
	{
		pUser->SendPacket( &msg, sizeof(msg) );
	}
}


VOID GateShell::DisplayPoolInfo()
{
	DISPMSG( "================= Pool Info =================\n" );
	theZoneManager.DisplayerZonePoolInfo();
	theUserManager.DisplayPoolInfo();
}

VOID GateShell::DisplayServerInfo()
{


	_SUPER::DisplayServerInfo();

	DISPMSG( "Player Connections		: %d\n", theUserManager.GetUserAmount() );
	theZoneManager.Display();

	DisplayPoolInfo();
}








const sSERVER_INFOC*	GateShell::GetServerInfo() const	
{ 
	return theServerInfoParser.GetServerInfo(); 
}


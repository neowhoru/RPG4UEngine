/*////////////////////////////////////////////////////////////////////////
文 件 名：HuntingRoom.cpp
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "huntingroom.h"
#include "OnlineUser.h"
#include "LobbyRoom.h"
#include "ServerSessionManagerInstance.h"
#include "IServerSession.h"
#include <PacketStruct_Gate_Game.h>
#include <PacketStruct_Gate_Chat.h>
#include <RequireFieldLevelInfoParser.h>
#include <ZoneManager.h>

HuntingRoom::HuntingRoom(void)
{
}

HuntingRoom::~HuntingRoom(void)
{
}

eROOM_RESULT	HuntingRoom::_CanCreate(OnlineUser *              pUser
                                      ,CODETYPE                  MapCode
												  ,eZONE_TYPE                eZoneType
												  ,eZONE_OPEN_STATE          ePublic
												  ,sROOMINFO_BASE & IN       BaseRoomInfo
												  ,sROOMINFO_ADDITIONAL & IN AdditionalRoomInfo)
{
	eROOM_RESULT rt;
	
	rt = _SUPER::_CanCreate( pUser, MapCode, eZoneType, ePublic, BaseRoomInfo );
	if( rt != RC_ROOM_SUCCESS )
		return rt;

	////////////////////////////////////////////
	if( AdditionalRoomInfo.m_Bonus >= HUNTINGBONUS_MAX ) 
		return RC_ROOM_INVALID_BONUS_VALUE;

	if( AdditionalRoomInfo.m_Difficulty >= HUNTING_DIFFICULTY_MAX ) 
		return RC_ROOM_INVALID_DIFFICULTY_VALUE;

	return RC_ROOM_SUCCESS;
}


eROOM_RESULT	HuntingRoom::Create	(OnlineUser *              pUser
                                    ,KEYTYPE                   key
												,CODETYPE                  MapCode
												,eZONE_OPEN_STATE          ePublic
												,LPCTSTR                   szRoomTitle
												,LPCTSTR                   szRoomPwd
												,sROOMINFO_BASE & IN       BaseRoomInfo
												,sROOMINFO_ADDITIONAL & IN AdditionalRoomInfo
												,IServerSession *          pServerSession)
{
	eROOM_RESULT rt;
	rt = _CanCreate(pUser
                  ,MapCode
						,ZONETYPE_HUNTING
						,ePublic
						,BaseRoomInfo
						,AdditionalRoomInfo);

	if( rt != RC_ROOM_SUCCESS )
		return rt;


	////////////////////////////////////////////////
	_SUPER::Create( pUser, 
							key,
							MapCode,
							ePublic,
							szRoomTitle,
							szRoomPwd,
							BaseRoomInfo,
							pServerSession );
	SetAdditionalInfo( AdditionalRoomInfo );

	////////////////////////////////////////////////
	eROOMSTATE_TYPE typeSave;
	
	typeSave =  GetRoomState();

	SetRoomState( eROOMSTATE_GENERNAL );

	rt = _SUPER::CanJoin( pUser, GetZonePublic(), GetRoomPWD() );
	SetRoomState( typeSave );

	if( rt != RC_ROOM_SUCCESS ) 
		return rt;

	_OnCreate();

	return rt;

}

eROOM_RESULT HuntingRoom::AttachRoom(KEYTYPE          key
                                 ,IRoom *          pPrevRoom
											,IServerSession * pServerSession)
{
	__CHECK2	(	pPrevRoom->GetType() == ZONETYPE_HUNTING 
				|| pPrevRoom->GetType() == ZONETYPE_LOBBY
				,RC_ROOM_INVALIDROOMTYPE);

	_SUPER::AttachRoom( key, pPrevRoom, pServerSession );

	/////////////////////////////////////
	if( pPrevRoom->GetType() == ZONETYPE_LOBBY )
	{
		LobbyRoom * pRoom = (LobbyRoom *)pPrevRoom;
		SetAdditionalInfo( pRoom->GetAdditionalInfo() );
	}

	/////////////////////////////////////
	else if( pPrevRoom->GetType() == ZONETYPE_HUNTING )
	{
		HuntingRoom * pRoom = (HuntingRoom *)pPrevRoom;
		SetAdditionalInfo( pRoom->GetAdditionalInfo() );
	}

	////////////////////////////////////////
	_OnCreate();
	RelayUser( pPrevRoom );

	//pPrevRoom->SetReusedKey(TRUE);

	return RC_ROOM_SUCCESS;
}


VOID HuntingRoom::_OnCreate()
{
	_SUPER::_OnCreate();

	MSG_AG_ZONE_HUNTING_CREATE_CMD msg;

	msg.m_dwKey				= GetMaster()->GetUserKey();
	msg.m_MapCode			= GetMapCode();
	msg.m_RoomKey			= GetKey();
	msg.m_RoomInfo			= GetRoomInfo();	
	msg.m_AdditionalInfo = GetAdditionalInfo();
	memcpy( msg.m_szRoomTitle	, GetRoomTitle()	, MAX_ROOMTITLE_LENGTH*sizeof(TCHAR) );
	memcpy( msg.m_szRoomPwd		, GetRoomPWD()		, MAX_ROOMPASSWORD_LENGTH*sizeof(TCHAR) );
	SendToLinkedShell( &msg, sizeof(msg) );
}


VOID HuntingRoom::_OnDestroy()
{
	if( !IsReusedServerSession() )
		theServerSessionManagerInstance.DecLOFBattleServer( GetLinkedShell()->GetSessionIndex() );

	_SUPER::_OnDestroy();
}


VOID HuntingRoom::OnRoomJoinCmd( User * pUser )
{
	MSG_CG_ZONE_HUNTING_JOIN_CMD msgCMD;
	SerializeLoad( msgCMD.m_RoomInfo );
	if( GetZonePublic() == ZONEOPEN_PRIVATE )
		memcpy( msgCMD.m_szRoomPwd, GetRoomPWD(), MAX_ROOMPASSWORD_LENGTH );
	pUser->SendPacket( &msgCMD, msgCMD.GetSize() );
}


VOID HuntingRoom::_OnReadyUser( User * pUser )
{
	_SUPER::_OnReadyUser(pUser);
	theZoneManager.IncHuntingRoomUserCount();

	pUser->SetZoneState( ZONESTATE_PRE_ENTER_HUNTING );
	pUser->SetZoneKey( GetKey() );
	pUser->SetZoneType( ZONETYPE_HUNTING );
}


VOID HuntingRoom::_OnEnterUser( User * pUser )
{
	_SUPER::_OnEnterUser(pUser);
	pUser->SetZoneState( ZONESTATE_ATHUNTING );
}

VOID HuntingRoom::_OnLeaveUser( User * pUser )
{
	_SUPER::_OnLeaveUser(pUser);
	theZoneManager.DecHuntingRoomUserCount();
	
}

VOID HuntingRoom::SerializeLoad( sROOMINFO_HUNTING & OUT rRoomInfo )
{
	rRoomInfo.m_Key					= GetKey();
	rRoomInfo.m_MapCode				= GetMapCode();
	rRoomInfo.m_byRoomPublic		= GetZonePublic();
	rRoomInfo.m_CurUserNum			= (BYTE)GetUserNum();
	rRoomInfo.m_MaxLimitUserNum	= GetLimitUserMax();
	rRoomInfo.m_RoomInfo				= GetRoomInfo();
	rRoomInfo.m_AdditionalInfo		= GetAdditionalInfo();

	memcpy( rRoomInfo.m_szRoomTitle, GetRoomTitle(), MAX_ROOMTITLE_LENGTH*sizeof(TCHAR) );
}

BOOL HuntingRoom::IsRecommendLevel( LEVELTYPE checkLV )
{
	sFIELDREQUIRED_LEVEL * pFieldLVInfo;
	
	pFieldLVInfo = theRequireFieldLevelInfoParser.GetRequiredInfo(GetMapCode() );
	if( !pFieldLVInfo ) 
		return FALSE;

	//,GetAdditionalInfo().m_Difficulty)		;

	if( pFieldLVInfo->m_wMinlv <= checkLV && checkLV <= pFieldLVInfo->m_wMaxlv )
		return TRUE;

	return FALSE;
}
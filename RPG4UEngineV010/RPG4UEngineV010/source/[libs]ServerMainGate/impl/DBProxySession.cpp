#include "stdafx.h"
#include "DBProxySession.h"
//#include "PacketHandler.h"
#include "PacketStruct_Base.h"
#include "GateShell.h"
//#include "ServerSessionFactory.h"
#include "ServerSessionManager.h"
#include "UserManager.h"
#include <PacketStruct_Gate_DBP.h>
#include <Protocol_Gate_DBP.h>
#include "OnlineUser.h"

DBProxySession::DBProxySession()
{
}

DBProxySession::~DBProxySession()
{
}


VOID DBProxySession::OnRecv( BYTE *pMsg, WORD wSize )
{
	ParsePacket((MSG_BASE *)pMsg,wSize);
	//g_PacketHander.ParsePacket_AD( this, (MSG_BASE *)pMsg, wSize );
}
VOID DBProxySession::OnConnect( BOOL bSuccess, DWORD dwNetworkIndex )
{
	_SUPER::OnConnect( bSuccess, dwNetworkIndex );

	if( bSuccess )
	{
		if( SERVER_ABNORMAL_DISCONNECTED == theGateShell.GetDBProxyState() )
		{
			//sendAllUsersInfo();		
		}

		theGateShell.StartListen(LISTEN_SERVER);
#ifdef _DEBUG// 为方便调试，在此启动Client监听；  Release版本将在FieldServer中启动
		theGateShell.StartListen(LISTEN_CLIENT);
#endif
	}
}



VOID DBProxySession::OnDisconnect()
{
	theGateShell.SetDBProxyState( SERVER_ABNORMAL_DISCONNECTED );

	_SUPER::OnDisconnect();
}
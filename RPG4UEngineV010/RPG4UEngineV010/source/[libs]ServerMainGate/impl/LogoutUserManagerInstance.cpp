
#include "stdafx.h"
#include "LogoutUserManagerInstance.h"
#include <ServerStruct.h>
#include "LogoutUserImpl.h"
#include "LogoutUserFactoryManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(LogoutUserManagerInstance, ()  , gamemain::eInstPrioGameFunc);


LogoutUserManagerInstance::LogoutUserManagerInstance()
{
}

LogoutUserManagerInstance::~LogoutUserManagerInstance()
{
}

BOOL LogoutUserManagerInstance::Init( DWORD dwUserSIze )
{
	REG_LOGOUTUSERFACTORY(LogoutUserImpl,LOGOUT_TYPE_DEFAULT,dwUserSIze);
	return _SUPER::Init(dwUserSIze);
}

VOID LogoutUserManagerInstance::Release()
{
	_SUPER::Release();
}


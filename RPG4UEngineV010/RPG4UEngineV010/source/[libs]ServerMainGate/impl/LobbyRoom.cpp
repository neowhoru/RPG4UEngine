/*////////////////////////////////////////////////////////////////////////
文 件 名：LobbyRoom.cpp
创建日期：2009年4月3日
最后更新：2009年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "lobbyroom.h"
#include "OnlineUser.h"
#include <PacketStruct_Gate_Game.h>
#include <PacketStruct_ClientGameS.h>
#include <MapInfoParser.h>
#include <RequireFieldLevelInfoParser.h>
#include <ZoneManager.h>


const INT	LEVEL_LIMIT_MIN	= 10;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
LobbyRoom::LobbyRoom(void)
{
}

LobbyRoom::~LobbyRoom(void)
{
}

VOID LobbyRoom::OnRoomJoinCmd(User* pUser)
{
	switch(GetLobbyType())
	{
	case ZONETYPE_HUNTING:
		{
			MSG_CG_ZONE_HUNTINGLOBBY_JOIN_CMD msgCMD;
			SerializeLoad( msgCMD.m_RoomInfo );
			if( GetZonePublic() == ZONEOPEN_PRIVATE )
				memcpy( msgCMD.m_szRoomPwd, GetRoomPWD(), MAX_ROOMPASSWORD_LENGTH );
			pUser->SendPacket( &msgCMD, msgCMD.GetSize() );
		}break;

	case ZONETYPE_MISSION:
		{
			MSG_CG_ZONE_MISSIONLOBBY_JOIN_CMD msgCMD;
			SerializeLoad( msgCMD.m_RoomInfo );
			if( GetZonePublic() == ZONEOPEN_PRIVATE )
				memcpy( msgCMD.m_szRoomPwd, GetRoomPWD(), MAX_ROOMPASSWORD_LENGTH );
			pUser->SendPacket( &msgCMD, msgCMD.GetSize() );
		}break;

	case ZONETYPE_PVP:
		{
			MSG_CG_ZONE_PVPLOBBY_JOIN_CMD msgCMD;
			SerializeLoad( msgCMD.m_RoomInfo );
			if( GetZonePublic() == ZONEOPEN_PRIVATE )
				memcpy( msgCMD.m_szRoomPwd, GetRoomPWD(), MAX_ROOMPASSWORD_LENGTH );
			pUser->SendPacket( &msgCMD, msgCMD.GetSize() );
		}break;
	}
}


VOID LobbyRoom::_OnCreate()
{
	_SUPER::_OnCreate();

	MSG_AG_ZONE_LOBBY_CREATE_CMD msg;
	msg.m_dwKey		= GetMaster()->GetUserKey();
	msg.m_MapCode	= GetMapCode();
	msg.m_RoomKey	= GetKey();
	msg.m_RoomInfo	= GetRoomInfo();	

	memcpy( msg.m_szRoomTitle	, GetRoomTitle()	, MAX_ROOMTITLE_LENGTH );
	memcpy( msg.m_szRoomPwd		, GetRoomPWD()		, MAX_ROOMPASSWORD_LENGTH );
	SendToLinkedShell( &msg, sizeof(msg) );
}

VOID LobbyRoom::_OnDestroy()
{
	_SUPER::_OnDestroy();
}

VOID LobbyRoom::_OnReadyUser( User * pUser )
{
	_SUPER::_OnReadyUser(pUser);

	switch(GetLobbyType())
	{
	case ZONETYPE_HUNTING: theZoneManager.IncHuntingLobbyRoomUserCount(); break;
	case ZONETYPE_MISSION: theZoneManager.IncMissionLobbyRoomUserCount(); break;
	case ZONETYPE_PVP:		theZoneManager.IncPVPLobbyRoomUserCount(); break;
	}

	pUser->SetZoneState( ZONESTATE_PRE_ENTER_LOBBY );
	pUser->SetZoneKey( GetKey() );
	pUser->SetZoneType( ZONETYPE_LOBBY );

	AssignTeam((OnlineUser *) pUser );
}


VOID LobbyRoom::_OnEnterUser( User * pUser )
{
	_SUPER::_OnEnterUser(pUser);
	pUser->SetZoneState( ZONESTATE_ATLOBBY );
}


VOID LobbyRoom::_OnLeaveUser( User * pUser )
{
	_SUPER::_OnLeaveUser(pUser);

	switch(GetLobbyType())
	{
	case ZONETYPE_HUNTING: theZoneManager.DecHuntingLobbyRoomUserCount(); break;
	case ZONETYPE_MISSION: theZoneManager.DecMissionLobbyRoomUserCount(); break;
	case ZONETYPE_PVP:		theZoneManager.DecPVPLobbyRoomUserCount(); break;
	}

	pUser->SetLobbyReady(FALSE);
	pUser->SetTeam(0);
}

eROOM_RESULT LobbyRoom::ChangeMap( CODETYPE MapCode )
{
	///////////////////////////////////
	sMAPINFO_BASE * pInfo = theMapInfoParser.GetMapInfo(GetMapCode());

	__CHECK2_PTR(pInfo,RC_ROOM_INVALID_MAPCODE);

	///////////////////////////////////
	MapGroup *			pGroup				= theMapInfoParser.GetMapGroup( pInfo->MapGroupIndex );
	sMAPINFO_BASE *	pChangingMapInfo	= pGroup->GetMapInfoCode( MapCode );

	__CHECK2_PTR(pChangingMapInfo,RC_ROOM_INVALID_MAPCODE);

	SetMapCode( MapCode );

	return RC_ROOM_SUCCESS;
}

eROOM_RESULT LobbyRoom::Create	(OnlineUser *        pUser
                                 ,KEYTYPE             key
											,CODETYPE            MapCode
											,eZONE_TYPE          eLobbyType
											,eZONE_OPEN_STATE    ePublic
											,LPCTSTR             szRoomTitle
											,LPCTSTR             szRoomPwd
											,sROOMINFO_BASE &    BaseRoomInfo
											,IServerSession *    pServerSession)
{

	__CHECK2(	eLobbyType == ZONETYPE_HUNTING 
				||	eLobbyType == ZONETYPE_MISSION 
				||	eLobbyType == ZONETYPE_PVP
				,RC_ROOM_INVALID_ROOM_TYPE);

	eROOM_RESULT rt;
	rt = _SUPER::_CanCreate(pUser
                          ,MapCode
								  ,eLobbyType
								  ,ePublic
								  ,BaseRoomInfo);

	__CHECK2(rt == RC_ROOM_SUCCESS,rt);


	//////////////////////////////////////////
	if( eLobbyType == ZONETYPE_PVP )
	{
		__CHECK2	(pUser->GetSelectedCharLV() >= LEVEL_LIMIT_MIN
					,RC_ROOM_NEED_CHAR_LEVEL_10_FOR_PVP);
	}

	//////////////////////////////////////////
	_SUPER::Create	(pUser
                  ,key
						,MapCode
						,ePublic
						,szRoomTitle
						,szRoomPwd
						,BaseRoomInfo
						,pServerSession);

	SetLobbyType( eLobbyType );


	//////////////////////////////////////////
	m_AdditionalInfo.m_Difficulty		= HUNTING_DIFFICULTY_EASY;
	m_AdditionalInfo.m_Bonus			= HUNTINGBONUS_ITEM;
	m_AdditionalPVPInfo.m_Rule			= PVPRULE_10;
	m_AdditionalPVPInfo.m_Mode			= PVPMODE_PERSONAL;

	//////////////////////////////////////////
	eROOMSTATE_TYPE typeSave;

	typeSave =  GetRoomState();
	SetRoomState( eROOMSTATE_GENERNAL );
	rt = _SUPER::CanJoin( pUser, GetZonePublic(), GetRoomPWD() );
	SetRoomState( typeSave );

	if( rt != RC_ROOM_SUCCESS ) 
		return rt;

	//////////////////////////////////////////
	_OnCreate();

	return rt;
}

VOID LobbyRoom::SerializeLoad( sROOMINFO_HUNTING & OUT rRoomInfo )
{
	rRoomInfo.m_Key					= GetKey();
	rRoomInfo.m_MapCode				= GetMapCode();
	rRoomInfo.m_byRoomPublic		= GetZonePublic();
	rRoomInfo.m_CurUserNum			= (BYTE)GetUserAmount();
	rRoomInfo.m_CurUserNum			= (BYTE)GetUserAmount();
	rRoomInfo.m_RoomInfo				= GetRoomInfo();
	rRoomInfo.m_AdditionalInfo		= GetAdditionalInfo();

	memcpy( rRoomInfo.m_szRoomTitle, GetRoomTitle(), MAX_ROOMTITLE_LENGTH*sizeof(TCHAR) );
}

VOID LobbyRoom::SerializeLoad( sROOMINFO_MISSION & OUT rRoomInfo )
{
	ASSERT( GetLobbyType() == ZONETYPE_MISSION );

	rRoomInfo.m_Key					= GetKey();
	rRoomInfo.m_MapCode				= GetMapCode();
	rRoomInfo.m_byRoomPublic		= GetZonePublic();
	rRoomInfo.m_CurUserNum			= (BYTE)GetUserNum();
	rRoomInfo.m_MaxLimitUserNum	= GetLimitUserMax();
	rRoomInfo.m_RoomInfo				= GetRoomInfo();
	memcpy( rRoomInfo.m_szRoomTitle, GetRoomTitle(), MAX_ROOMTITLE_LENGTH*sizeof(TCHAR) );
}


VOID LobbyRoom::SerializeLoad( sROOMINFO_PVP & OUT rRoomInfo )
{
	rRoomInfo.m_Key					= GetKey();
	rRoomInfo.m_MapCode				= GetMapCode();
	rRoomInfo.m_byRoomPublic		= GetZonePublic();
	rRoomInfo.m_CurUserNum			= (BYTE)GetUserNum();
	rRoomInfo.m_MaxLimitUserNum	= GetLimitUserMax();
	rRoomInfo.m_RoomInfo				= GetRoomInfo();
	rRoomInfo.m_AdditionalPVPInfo	= GetAdditionalPVPInfo();

	memcpy( rRoomInfo.m_szRoomTitle, GetRoomTitle(), MAX_ROOMTITLE_LENGTH*sizeof(TCHAR) );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class LobbyRoomIsAllReadyOpr
{
public:
	LobbyRoom*	pRoom;
	BOOL			bRet;
	BOOL operator()(User* pUserRef)
	{
		OnlineUser * pUser = (OnlineUser*)pUserRef;
		if( pRoom->GetMaster() && (pRoom->GetMaster()->GetUserKey() == pUser->GetUserKey()) )
			return FALSE;

		if( !pUser->IsLobbyReady() )
		{
			bRet = FALSE;
			return TRUE;
		}
		return FALSE;
	}
};
BOOL LobbyRoom::IsAllReady()
{
	LobbyRoomIsAllReadyOpr opr={this, TRUE};
	SearchUser(opr);
	return opr.bRet;

}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class LobbyRoomIsPrepareUserNumOpr
{
public:
		BYTE Team1,Team2;
		void operator()(User* pTmpUser)
		{
			if( pTmpUser->GetTeam() == 1 )	
				++Team1;
			else if( pTmpUser->GetTeam() == 2 )
				++Team2;
		}
};

BOOL LobbyRoom::IsPrepareUserNum()
{
	sMAPINFO_BASE * pInfo = theMapInfoParser.GetMapInfo(GetMapCode());
	if( !pInfo )
		return FALSE;

	if( GetUserNum() < pInfo->byMinUserNum ) return FALSE;

	if( GetLobbyType() == ZONETYPE_PVP && IsPVPTeamMode() )
	{
		LobbyRoomIsPrepareUserNumOpr opr={0};
		Foreach(opr);



		if( opr.Team1 == 0 || opr.Team2 == 0 )
			return FALSE;
	}

	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class LobbyRoomAssignTeamOpr
{
public:
	LobbyRoom*	pRoom;
	BYTE			Team;

	void operator()(User* pTmpUser)
	{
		if( !pRoom->IsPVPTeamMode() )
		{
			Team = 0;
		}
		else
		{
			Team = ( Team == 1 ? 2 : 1 );
		}
		pTmpUser->SetTeam(Team);
	}
};

VOID LobbyRoom::AssignTeam( OnlineUser * pTargetUser )
{
	if( NULL != pTargetUser )
	{
		if( GetLobbyType() != ZONETYPE_PVP || !IsPVPTeamMode() )
		{
			pTargetUser->SetTeam(0);
			return;
		}

		LobbyRoomIsPrepareUserNumOpr opr={0};
		Foreach(opr);


		if( opr.Team1 <= opr.Team2 )
			pTargetUser->SetTeam(1);
		else			
			pTargetUser->SetTeam(2);
	}
	else
	{
		if( GetLobbyType() != ZONETYPE_PVP )
			return;

		LobbyRoomAssignTeamOpr opr={this,2};
		Foreach(opr);

	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL LobbyRoom::IsRecommendLevel( LEVELTYPE LV )
{
	BYTE byDiff = 99;
	switch(GetLobbyType())
	{
	case ZONETYPE_PVP:	
		return _SUPER::CheckRoomLevel( LV );

	case ZONETYPE_MISSION: 
		break;

	case ZONETYPE_HUNTING: 
		byDiff = GetAdditionalInfo().m_Difficulty; 
		break;
	default:
		return FALSE;
	}

	////////////////////////////////////////////////
	sFIELDREQUIRED_LEVEL * pFieldLVInfo;
	
	pFieldLVInfo = theRequireFieldLevelInfoParser.GetRequiredInfo((WORD)GetMapCode())		;
	if( !pFieldLVInfo )
		return FALSE;

	if( pFieldLVInfo->m_wMinlv <= LV && LV <= pFieldLVInfo->m_wMaxlv )
		return TRUE;

	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
eROOM_RESULT	LobbyRoom::CheckCreateRoom	(User *       pMasterUser
														,eZONE_TYPE			  zoneType)
{
	eROOM_RESULT	result;

	result = CheckCreateRoom(pMasterUser
									,zoneType);

	if(result !=RC_ROOM_SUCCESS )
		return result;

	__CHECK2	(GetLobbyType() == zoneType 
				,RC_ROOM_INVALID_LASTROOMTYPE);

	__CHECK2	(IsAllReady()
				,RC_ROOM_IS_NOT_ALL_READY );

	__CHECK2	(IsPrepareUserNum()
				,RC_ROOM_INSUFFICIENT_USER_NUM );

	return RC_ROOM_SUCCESS;
}

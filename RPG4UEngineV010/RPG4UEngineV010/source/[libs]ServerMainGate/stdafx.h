// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 및 프로젝트 관련 포함 파일이 
// 들어 있는 포함 파일입니다.
//

#pragma once

#define __NEW_WORLD_SCRIPT


#define _USE_GAME
#include <ServerIIInclude.h>
#include <ServerBaseGateInc.h>
#include <ServerMainGateInc.h>



#include "ConstDefine.h"
#include "PacketStruct_Base.h"
#include <TSingleton.h>
#include <TLinkedList.h>
#include <CriticalSection.h>
#include <TMemoryPoolFactory.h>
#include <ResultCode.h>
#include <AssertUtil.h>
#include <MiscUtil.h>
//#include <SimpleModulus.h>

#include "ServerGroupLog.h"
#include "GateShellLog.h"


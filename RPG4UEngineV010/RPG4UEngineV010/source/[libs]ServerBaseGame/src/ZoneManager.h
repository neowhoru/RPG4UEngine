#pragma once

#include <THashTable.h>
#include <StructMap.h>

class BaseZone;
class BaseRoom;


class _SERVERBASEGAME_API ZoneManager
{
public:
	ZoneManager(void);
	virtual ~ZoneManager(void);

public:
	virtual BOOL	Init		(DWORD dwRoomPoolSize
                           ,DWORD dwPVPPoolSize
                           ,DWORD dwLobbyPoolSize
									,DWORD dwVillagePoolSize)		;
	virtual VOID	Release	();

	virtual BaseZone *		CreateZone	(KEYTYPE           Key
													,CODETYPE          MapCode
													,eZONE_TYPE			 zoneType)=0;
	virtual BaseRoom *		CreateRoom	(KEYTYPE                 Key
													,CODETYPE                MapCode
													,LPCTSTR                 szRoomTitle
													,LPCTSTR                 szRoomPWD
													,sROOMINFO_BASEC *       pRoomInfo
													,eZONE_TYPE					 zoneType
													,LPARAM						 lpParam=0
													)=0;




	virtual VOID	DestroyZoneAll()=0;
	virtual VOID	DestroyZone	(KEYTYPE Key )=0;
	virtual VOID	ReadyPlayer	(KEYTYPE  Key
                              ,Player * pPlayer
										,CODETYPE FieldCode
										,IDTYPE   AreaID)=0;
	virtual VOID	JoinPlayer	(KEYTYPE  Key
                              ,Player * pPlayer
										,CODETYPE FieldCode
										,IDTYPE   AreaID)=0;
	virtual VOID	LeavePlayer	(KEYTYPE  Key
                              ,Player * pPlayer)		=0;
	virtual VOID	KickOutPlayer	(KEYTYPE  Key
                              ,Player * pPlayer
										,CODETYPE FieldCode
										,IDTYPE   AreaID)=0;

	/////////////////////////////////
	virtual BOOL	FieldVersionCheck		(KEYTYPE  Key
                                       ,CODETYPE FieldCode
													,DWORD    checksum)=0;

	virtual VOID			Update			( DWORD dwTick )=0;

	virtual VOID			Display			()=0;
	virtual VOID			DisplayPoolInfo()=0;


	/////////////////////////////////
	template<class OPR>	void ForEach(OPR& opr);

	BaseZone *	GetZone			( KEYTYPE Key );
protected:
	VOID			_AddZone			( BaseZone * pZone );
	VOID			_RemoveZone		( KEYTYPE Key );
	VOID			_RemoveZones	(  );

private:

	util::THashTable<BaseZone *> *	m_pZoneHash;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ZoneManager , _SERVERBASEGAME_API);
//extern API_NULL ZoneManager& singleton::GetGameZoneManager();
#define theZoneManager  singleton::GetZoneManager()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<class OPR>
void ZoneManager::ForEach(OPR& opr)
{
	BaseZone * pZone = NULL;
	m_pZoneHash->SetFirst();
	for(;; )
	{
		 pZone = m_pZoneHash->GetNext();
		 if(pZone == NULL )
			 break;
		opr(pZone);
	}
}

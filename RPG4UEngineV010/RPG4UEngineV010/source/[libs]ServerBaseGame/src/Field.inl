/*////////////////////////////////////////////////////////////////////////
文 件 名：Field.inl
创建日期：2008年5月7日
最后更新：2008年5月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __FIELD_INL__
#define __FIELD_INL__
#pragma once


inline CODETYPE	Field::GetFieldCode() { return m_pFieldInfo->GetFieldCode();	}
inline TileLand *	Field::GetTileLand() { return m_pFieldInfo->GetTileLand();	}

inline Sector * Field::_GetSector( DWORD dwSectorIndex )
{
	__CHECK2_RANGE(dwSectorIndex,m_Sectors.size(), NULL);
	return m_Sectors[dwSectorIndex];
}

inline void Field::_PushSector(Sector* pSector  )
{
	return m_Sectors.push_back(pSector);
}

inline UINT Field::GetSectorAmount(  )
{
	return m_Sectors.size();
}


template<class OPR>
VOID Field::ForEachNPC( OPR & Opr )
{
	NPC* pNPC;
	for( ObjectHashMapIt it = m_pNPCList->Begin(); it != m_pNPCList->End(); ++it )
	{
		pNPC = (NPC*)(it->second);
		Opr( pNPC );
	}
}

template <class OPR>
Character* Field::SearchPlayer( OPR& finder )
{
	BOOL						bFind				= FALSE;
	Sector *					pSector;	
	SectorGroup *			pSectorGroup;
	SectorGroupTableIt it;
	SectorTableIt		itr;

	for( it = m_pSectorGroupList->Begin(); it != m_pSectorGroupList->End(); ++it )
	{
		pSectorGroup = *it;

		if( bFind )	
			break;
		if( !pSectorGroup->GetReferenceCount() )
			continue;

		for(itr = pSectorGroup->Begin(); itr != pSectorGroup->End(); ++itr )
		{
			pSector = *itr;
			if( pSector->SearchPlayer( finder ) )
			{
				bFind = TRUE;
				break;
			}

		}//for(itr = pSectorGroup->Begin(); itr != pSectorGroup->End(); ++itr )

	}//for( it = m_pSectorGroupList->Begin(); it != m_pSectorGroupList->End(); ++it )

	return finder.GetTarget();
}

template <class OPR>
Character* Field::SearchNPC( OPR& finder )
{
	BOOL bFind = FALSE;
	Sector *pSector;
	SectorGroup *pSectorGroup;
	for( SectorGroupTableIt it = m_pSectorGroupList->Begin(); it != m_pSectorGroupList->End(); ++it )
	{
		pSectorGroup = *it;

		if( bFind )		break;
		if( !pSectorGroup->GetReferenceCount() )	continue;

		for( SectorTableIt itr = pSectorGroup->Begin(); itr != pSectorGroup->End(); ++itr )
		{
			pSector = *itr;
			if( pSector->SearchNPC( finder ) )
			{
				bFind = TRUE;
				break;
			}
		}
	}

	return finder.GetTarget();
}

template< class OPR >
BOOL Field::SearchItemAround( DWORD dwSectorIndex, OPR & Opr )
{
	Sector *pSector = _GetSector( dwSectorIndex );

	/// 查找自身...
	if( pSector )
	{
		if( pSector->SearchFieldItem( Opr ) )
			return TRUE;
	}

	/// 查找邻近sector
	SectorInfo * pInfo = m_pFieldInfo->GetSectorInfo( dwSectorIndex );
	ASSERT( pInfo );

#ifdef USE_NEIGHBORLIST
	POS pos = pInfo->GetNeighbors().GetFirstPos();
	while( (pNextInfo = pInfo->GetNeighbors().GetNextPos( pos )) != NULL )
		pSector = _GetSector( pNextInfo->GetSectorIndex() );
	SectorInfo * pNextInfo = NULL;
#endif
	for(DWORD dwIndex=0; dwIndex<pInfo->GetNeighborNum(); dwIndex++)
	{
		pSector = _GetSector( pInfo->GetNeighborAt(dwIndex) );
		if( !pSector )
			continue;

		if( pSector->SearchFieldItem( Opr ) )
		{
			return TRUE;
		}
	}

	return FALSE;
}

template<class OPR>
VOID Field::ForEachPlayer( OPR & Opr )
{
	Sector *pSector;	SectorGroup *pSectorGroup;
	for( SectorGroupTableIt it = m_pSectorGroupList->Begin(); it != m_pSectorGroupList->End(); ++it )
	{
		pSectorGroup = *it;

		if( !pSectorGroup->GetReferenceCount() )	continue;

		for( SectorTableIt itr = pSectorGroup->Begin(); itr != pSectorGroup->End(); ++itr )
		{
			pSector = *itr;
			pSector->ForEachPlayer( Opr );
		}
	}
}

template< class OPR >
VOID Field::ForEachPlayerAround( DWORD dwSectorIndex, OPR & Opr )
{
	Sector *pSector = _GetSector( dwSectorIndex );

	//
	if( pSector )
	{
		pSector->ForEachPlayer( Opr );
	}

	// 查找邻近信息
	SectorInfo * pInfo = m_pFieldInfo->GetSectorInfo( dwSectorIndex );
	ASSERT( pInfo );


#ifdef USE_NEIGHBORLIST
	POS pos = pInfo->GetNeighbors().GetFirstPos();
	while( (pNextInfo = pInfo->GetNeighbors().GetNextPos( pos )) != NULL )
		pSector = _GetSector( pNextInfo->GetSectorIndex() );
	SectorInfo * pNextInfo = NULL;
#endif
	for(DWORD dwIndex=0; dwIndex<pInfo->GetNeighborNum(); dwIndex++)
	{
		pSector = _GetSector( pInfo->GetNeighborAt(dwIndex) );
		if( !pSector )
			continue;

		pSector->ForEachPlayer( Opr );
	}
}

template< class OPR >
VOID Field::ForEachItemAround( DWORD dwSectorIndex, OPR & Opr )
{
	Sector *pSector = _GetSector( dwSectorIndex );

	// 磊扁冀磐 林函狼 贸府甫 茄促.
	if( pSector )
	{
		pSector->ForeachItem( Opr );
	}

	// 林函冀磐 贸府甫 茄促.
	SectorInfo * pInfo = m_pFieldInfo->GetSectorInfo( dwSectorIndex );
	ASSERT( pInfo );
#ifdef USE_NEIGHBORLIST
	POS pos = pInfo->GetNeighbors().GetFirstPos();
	while( (pNextInfo = pInfo->GetNeighbors().GetNextPos( pos )) != NULL )
		pSector = _GetSector( pNextInfo->GetSectorIndex() );
	SectorInfo * pNextInfo = NULL;
#endif
	for(DWORD dwIndex=0; dwIndex<pInfo->GetNeighborNum(); dwIndex++)
	{
		pSector = _GetSector( pInfo->GetNeighborAt(dwIndex) );
		if( !pSector )
			continue;

		pSector->ForeachItem( Opr );
	}
}

template< class OPR >
VOID Field::ForEachNpcAround( DWORD dwSectorIndex, OPR & Opr )
{
	Sector *pSector = _GetSector( dwSectorIndex );

	if( pSector )
	{
		pSector->ForeachNPC( Opr );
	}

	SectorInfo * pInfo = m_pFieldInfo->GetSectorInfo( dwSectorIndex );
	ASSERT( pInfo );
#ifdef USE_NEIGHBORLIST
	POS pos = pInfo->GetNeighbors().GetFirstPos();
	while( (pNextInfo = pInfo->GetNeighbors().GetNextPos( pos )) != NULL )
		pSector = _GetSector( pNextInfo->GetSectorIndex() );
	SectorInfo * pNextInfo = NULL;
#endif
	for(DWORD dwIndex=0; dwIndex<pInfo->GetNeighborNum(); dwIndex++)
	{
		pSector = _GetSector( pInfo->GetNeighborAt(dwIndex) );
		if( !pSector )	
			continue;

		pSector->ForeachNPC( Opr );
	}
}


#endif //__FIELD_INL__

#pragma once
#include "CommonDefine.h"
#include "DataPump.h"

class BaseContainer;
class StyleQuickSlotContainer;

using namespace RC;


class _SERVERBASEGAME_API QuickStyleManager : public DataPump
{
public:
	QuickStyleManager(void);
	virtual ~QuickStyleManager(void);

public:
	virtual DATAPUMP_TYPE GetFactoryType(){return DATAPUMP_QUICKSTYLE;}

public:
	virtual VOID					Init		(Player* pPlayer,SlotManager * pMGR );
	virtual VOID					Release	();

public:
	virtual StyleQuickSlotContainer *	GetQuickStyleSlotContainer()=0;

	virtual BOOL					ValidPos		(SLOTIDX atIndex
                                          ,SLOTPOS atPos
														,BOOL    bEmptyCheck=TRUE)=0;
	virtual BOOL					ValidState	()=0;
	virtual BOOL					IsEmpty		(SLOTPOS pos )=0;

	virtual VOID					SerializeStyleStream	(SLOTPOS            pos
                                                   ,STYLE_STREAM * IN  pStream
																	,eSERIALIZE			  eType = SERIALIZE_LOAD)=0;

	virtual eSTYLE_RESULT		Link	(SLOTCODE code
                                    ,SLOTPOS  toPos)=0;
	virtual eSTYLE_RESULT		Unlink(SLOTPOS atPos )=0;
	virtual eSTYLE_RESULT		Move	(SLOTPOS fromPos
                                    ,SLOTPOS toPos);

	virtual VOID					UpdateStyleSlot()=0;

private:


};




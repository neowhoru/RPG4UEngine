/*////////////////////////////////////////////////////////////////////////
文 件 名：GameLog.h
创建日期：2009年4月7日
最后更新：2009年4月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMELOG_H__
#define __GAMELOG_H__
#pragma once


#include "CommonDefine.h"
#include "GameLogDefine.h"
#include "BaseLog.h"

class Player;
class ItemSlot;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API GameLog : public BaseLog
{
public:
	GameLog(void);
	virtual ~GameLog(void);

public:
	//////////////////////////////////////////////////////////////////////////
	virtual void LogLevelUp	(Player* pPlayer
                           ,DWORD   dwServerCode
									,DWORD   dwOldExp
									,DWORD   dwNewExp
									,WORD    wOldLevel
									,WORD    wNewLevel)=0;

	virtual void LogExtraExp(Player* pPlayer
                           ,DWORD   dwServerCode
									,DWORD   dwOldExp
									,DWORD   dwNewExp
									,WORD    wOldLevel
									,WORD    wNewLevel
									,WORD    wDivideCount)=0;

	virtual void LogCutExp	(Player* pPlayer
                           ,DWORD   dwServerCode
									,DWORD   dwTotalExp
									,DWORD   dwChangeExp
									,WORD    wLevel)=0;

	//////////////////////////////////////////////////////////////////////////
	virtual void LogItemDrop(Player*   pPlayer
                           ,DWORD     dwServerCode
									,BOOL      bPickUp
									,ItemSlot* pItemSlot)=0;
	virtual void LogEnchant	(Player*                   pPlayer
                           ,DWORD dwServerCode
									,BOOL                      bSuccess
									,ItemSlot*                 pItemSlot
									,vector<sLOG_COMPOSITE_MATERIAL> arMatInfos)=0;

	virtual void LogItemRepair	(Player*   pPlayer
                              ,DWORD     dwServerCode
										,ItemSlot* pItemSlot
										,DWORD     dwMoney)=0;

	virtual void LogNPCTrade	(Player*   pPlayer
                              ,DWORD     dwServerCode
										,BOOL      bSell
										,ItemSlot* pItemSlot
										,DWORD     dwMoney)=0;
	virtual void LogPlayerTrade(Player*   pPlayer1
                              ,DWORD     dwServerCode
										,BOOL      bGive
										,ItemSlot* pItemSlot
										,Player*   pPlayer2)=0;

	virtual void LogMoneyTrade	(Player* pPlayer1
                              ,DWORD   dwServerCode
										,BOOL    bGive
										,DWORD   dwMoney
										,Player* pPlayer2)=0;
	virtual void LogItemVendor	(Player*   pPlayer1
                              ,DWORD     dwServerCode
										,BOOL      bSell
										,ItemSlot* pItemSlot
										,DWORD     dwMoney
										,Player*   pPlayer2)=0;

	virtual void LogItemRankUp	(Player*   pPlayer
                              ,DWORD     dwServerCode
										,ItemSlot* pItemSlot
										,DWORD     dwMoney
										,BOOL      bCreateSocket)=0;
	virtual void LogItemWarehouse	(Player*   pPlayer
                                 ,DWORD     dwServerCode
											,BOOL      bKeep
											,ItemSlot* pItemSlot)=0;
	virtual void LogEnchantOpr		(Player*                         pPlayer
                                 ,DWORD                           dwServerCode
											,vector<sLOG_COMPOSITE_MATERIAL> arMatInfos)=0;
	virtual void LogMonsterDrop	(DWORD     dwServerCode
                                 ,ItemSlot* pItemSlot
											,DWORD     dwMonsterCode
											,DWORD     dwMonsterLevel
											,int       nMapCode
											,Vector3C& vPos)=0;
	virtual void LogTradeStatus	(Player* pPlayer1
                                 ,DWORD   dwServerCode
											,Player* pPlayer2
											,int     nType)=0;
	virtual void LogVendorStatus	(Player* pPlayer
                                 ,DWORD   dwServerCode
											,BOOL    bOpen)=0;
	virtual void LogViewVendor		(Player* pPlayer1
                                 ,DWORD   dwServerCode
											,Player* pPlayer2)=0;
	virtual void LogWarehouseInfo	(Player*   pPlayer
                                 ,DWORD     dwServerCode
											,ItemSlot* pItemSlot
											,DWORD     dwMoney)=0;
	virtual void LogVendorInfo		(Player*   pPlayer
                                 ,DWORD     dwServerCode
											,ItemSlot* pItemSlot
											,DWORD     dwMoney)=0;
	virtual void LogItemRankUpOpr	(Player* pPlayer
                                 ,DWORD   dwServerCode
											,SLOTPOS pos1
											,SLOTPOS pos2)=0;


	//////////////////////////////////////////////////////////////////////////
	// 1. 
	virtual void LogPlayerResurrection(Player* pPlayer
                               ,DWORD dwServerCode
										 ,DWORD   dwExp
										 ,int     nMapCode
										 ,Vector3C&   vPos)=0;
	// 2. 
	virtual void LogPlayerDead	(Player* pPlayer
                              ,DWORD dwServerCode
										,DWORD   dwExp
										,int     nMapCode
										 ,Vector3C&   vPos)=0;
	// 3. 
	virtual void LogInvenInfoOnLogin	(Player* pPlayer
                              ,DWORD dwServerCode)		=0;
	// 4. 
	virtual void LogUseStat		(Player* pPlayer
                              ,DWORD dwServerCode
										,byte    byStatType)=0;
	// 5. 
	virtual void LogCharacter	(Player* pPlayer
                              ,DWORD dwServerCode
										,BOOL    bCreate)=0;
	// 6. 
	virtual void LogMoveVillage(Player* pPlayer
                              ,DWORD dwServerCode
										,int     nMapCode)=0;
	// 7. 
	virtual void LogSkillLevelUp	(Player* pPlayer
                              ,DWORD dwServerCode
										,int     nSkillCode
										,BOOL    bLevelUp)=0;



	//////////////////////////////////////////////////////////////////////////
	// 4. 
	virtual void LogCharConnect(Player* pPlayer
                              ,DWORD dwServerCode)		=0;

	// 
	virtual void LogSnapShot			(Player* pPlayer
                              ,DWORD dwServerCode)		=0;





};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(GameLog , _SERVERBASEGAME_API);
//extern API_NULL GameLog& singleton::GetGameLog();
#define theGameLog  singleton::GetGameLog()

#define GAMELOG		theGameLog


#endif //__GAMELOG_H__
#ifndef __BASEEFFECT_H__
#define __BASEEFFECT_H__

#include "CommonDefine.h"
#include "BaseStatus.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD				   BASEEFFECT_KEY;	//ID���͵�Factory
typedef DWORD			      BASEEFFECT_TYPE;
#define BASEEFFECT_NULL    ((BASEEFFECT_TYPE)-1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API BaseEffect : public BaseStatus
{
public:
	BaseEffect();
	virtual ~BaseEffect();

public:
	virtual VOID	Init		(SLOTCODE   skillCode
                           ,int        iApplicationTime
									,int        iPeriod
									,Character* pAttacker
									,Vector3D   vCurPos
									,float      fRadius);

	virtual VOID	Start		() {}
	virtual VOID	End		() {}
	virtual VOID	Execute	();

	/////////////////////////////////////////////
	virtual VOID	SetDamage(eATTACK_KIND /*eType*/
                           ,DAMAGETYPE   /*wDamage*/)	{}

protected:
	VG_PTR_PROPERTY		(Owner		, Character);
	VG_PTR_PROPERTY		(Field		, Field);
	VG_TYPE_PROPERTY		(SkillCode	, SLOTCODE);
	VG_DWORD_PROPERTY		(SectorIndex);
	VG_VECTOR_PROPERTY	(CurPos		,Vector3D);
	VG_FLOAT_PROPERTY		(Radius);
};


#endif



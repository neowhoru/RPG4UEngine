#pragma once

#include "Character.h"

typedef DWORD				   TARGETFINDER_KEY;	//ID类型的Factory
typedef DWORD			      TARGETFINDER_TYPE;
#define TARGETFINDER_NULL    ((TARGETFINDER_TYPE)-1)


/*////////////////////////////////////////////////////////////////////////
	目标寻找器
	TargetFinder
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API TargetFinder
{
public :
	TargetFinder();  

public :
	virtual	DWORD			GetFactoryType(){return 0;}

public :
	virtual	VOID			Init			();
	virtual	Character*	GetTarget	();
	virtual	BOOL			CheckTarget	(Object* pObject );

	virtual VOID	SetOwner			( Character* pOwner );
	virtual VOID	SetTargetType	( eSKILL_TARGET_TYPE eType );
	virtual BOOL	operator ()		( Object* pObject ) = 0;


protected :
	Character*				m_pOwner;
	Character*				m_pTarget;

	eSKILL_TARGET_TYPE	m_eTargetType;	
	float						m_fOwnerSight;
	Vector3D					m_vOwnerPos;
};//class TargetFinder



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "TargetFinder.inl"











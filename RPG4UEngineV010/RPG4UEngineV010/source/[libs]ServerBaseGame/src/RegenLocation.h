/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenLocation.h
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	怪物生成点
		1.记录当前点生成alive怪数量
		2.记录当前点怪物群
		3.记录当前点区域索引

版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENLOCATION_H__
#define __REGENLOCATION_H__
#pragma once

#include "CommonDefine.h"
#include "RegenLocationDefine.h"

struct sREGEN_INFO;
class ObjectGroup;
class NPC;

class _SERVERBASEGAME_API RegenLocation
{
public:
	RegenLocation();
	virtual ~RegenLocation();

public:
	virtual VOID				Init		(sREGEN_INFO* pRegenInfo
												,DWORD       dwAreaKey=0)		;
	virtual VOID				Release	();

	//活动NPC相关操作
	virtual DWORD				GetAliveAmount	(DWORD dwNPCCode );
	virtual VOID				AddAliveNPC		(DWORD dwNPCCode, DWORD dwNum=1 );
	virtual VOID				OnCreateNPC		(DWORD dwNPCCode, DWORD dwNum=1 );
	virtual VOID				OnRemoveNPC		(DWORD dwNPCCode, DWORD dwNum=1 );
	virtual VOID				ClearAlives		();

	virtual VOID				PushAreaKey		(DWORD dwAreaKey )	;
	virtual DWORD				GetRandomArea	();
	virtual DWORD				GetAreaAt		(UINT nAt);
	virtual DWORD				GetAreaAmount	();

	virtual ObjectGroup*		JoinGroup		( NPC* pNPC );

private:
	RegenAliveMap			m_AliveInfos;
	RegenAreaArray			m_RegenAreas;

protected:
	ObjectGroup*			m_ObjectGroups[MAX_OBJECTGROUP_PER_AREA];	

	VG_DWORD_GET_PROPERTY(AreaKey);
	VG_PTR_PROPERTY		(RegenInfo, sREGEN_INFO);
};//class RegenLocation


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "RegenLocation.inl"


#endif //__REGENLOCATION_H__
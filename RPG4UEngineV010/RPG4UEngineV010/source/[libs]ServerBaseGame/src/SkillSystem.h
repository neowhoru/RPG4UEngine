#ifndef __SKILL_FACTORY_H__
#define __SKILL_FACTORY_H__

#pragma once


using namespace util;

class SkillDetailInfo;
class InstantSkill;
class DelayedSkill;
class StyleSkill;
class PassiveSkill;
class NormalAttack;

class _SERVERBASEGAME_API SkillSystem 
{
public:
	SkillSystem();
	virtual ~SkillSystem();

public:
	virtual BOOL	Init		( DWORD dwPlayerPoolSize, DWORD dwNPCPoolSize );
	virtual VOID	Release	();

public:
	Skill*		AllocSkill	(eSKILL_TYPE      eSkillType
                           ,SkillDetailInfo* pBaseSkillInfo=NULL)		;
	VOID			FreeSkill	( Skill* pSkill );

	VOID			DisplayPoolInfo();

private:

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(SkillSystem , _SERVERBASEGAME_API );
//extern API_NULL SkillSystem& singleton::GetSkillSystem();
#define theSkillSystem  singleton::GetSkillSystem()

#endif

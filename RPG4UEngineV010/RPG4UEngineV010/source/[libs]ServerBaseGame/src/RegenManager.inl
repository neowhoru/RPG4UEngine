/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenManager.inl
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENMANAGER_INL__
#define __REGENMANAGER_INL__
#pragma once

inline	VOID		RegenManager::AddRegenLocation( RegenLocation* pLoc )
{
	m_RegenLocations.push_back(pLoc);	
}

template<class OPR>
void	RegenManager::ForEachLocation	(OPR& opr)
{
	for(UINT n=0; n<m_RegenLocations.size(); n++)
	{
		if(!opr(m_RegenLocations[n]))
			break;
	}
}


#endif //__REGENMANAGER_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：BaseZone.h
创建日期：2009年6月11日
最后更新：2009年6月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BASEZONE_H__
#define __BASEZONE_H__
#pragma once

#pragma once
#include "BaseZoneDefine.h"
#include "THashTable.h"

using namespace util;
class PlayerLVSumOpr;
class Map;
class Player;
class AddExpOperator;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API BaseZone
{
public:
	BaseZone(void);
	virtual ~BaseZone(void);

public:
	virtual DWORD			GetFactoryType	() {return GetZoneType();}
	virtual eZONE_TYPE	GetZoneType		() = 0;
	virtual VOID			Reuse				();

public:
	virtual VOID		Init		( KEYTYPE Key, CODETYPE MapCode );
	virtual VOID		Release	();
	virtual VOID		Update	( DWORD dwTick );
	virtual VOID		LoadMap	( int iActiveFieldIndex = -1 );


public:
	virtual VOID		Ready		(Player * pPlayer
                              ,CODETYPE FieldCode
										,IDTYPE   AreaID)=0;
	virtual BOOL		Join		(Player * pPlayer
                              ,CODETYPE FieldCode
										,IDTYPE   AreaID);
	virtual	VOID 		Leave		(Player * pPlayer );
	virtual VOID		KickOut	(Player * pPlayer
                              ,CODETYPE FieldCode
										,IDTYPE   AreaID);

public:
	virtual DWORD		GetEntryAreaID		();
	virtual CODETYPE	GetEntryFieldCode	();
	virtual BOOL		CheckMapVersion	( CODETYPE FieldCode, DWORD checksum );


	//////////////////////////////////////////////
public:
	virtual BOOL		OnDistributeExp	(AddExpOperator& opr);

	virtual BOOL		OnDropItem			(PlayerLVSumOpr& sum
                                       ,FLOAT&          PlusBonusPercent
													,Vector3C *      pOrgPos
													,Field *         pField
													,DWORD           SectorIndex)		;

	virtual BOOL		OnPlayerDead		(Player* pPlayer
                                       ,DWORD   dwKillerObjectKey
													,DWORD   dwKillerType);

	virtual BOOL		OnBattleStart		(LPARAM	lpParam);
	virtual DWORD		OnBattlePressKey	(Player* pPlayer);

	//////////////////////////////////////////////
	template <class OPR>
	VOID				ForEachPlayer	(OPR & opr );
	VOID				AddPlayer		(Player* pPlayer);

protected:
	VG_TYPE_PROPERTY		(Key		, KEYTYPE);
	VG_TYPE_PROPERTY		(MapCode	, CODETYPE);
	VG_PTR_GET_PROPERTY	(Map		, Map);

private:
	THashTable<Player *>			m_Players;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <class OPR>
VOID BaseZone::ForEachPlayer( OPR & opr )
{
	Player * pPlayer;
	m_Players.SetFirst();
	for(;;)
	{
		pPlayer = m_Players.GetNext();
		if(!pPlayer)	break;
		opr( pPlayer );
	}
}


#endif //__BASEZONE_H__
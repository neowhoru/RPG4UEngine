/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenManager.h
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	怪物生成管理


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENMANAGER_H__
#define __REGENMANAGER_H__
#pragma once


#include "RegenLocationDefine.h"
#include "FieldDefine.h"

class		Field;
struct	sREGEN_INFO;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API RegenManager
{
public:
	RegenManager	(  );
	virtual ~RegenManager	();

public:
	virtual DWORD		GetFactoryType(){ return REGEN_TYPE_DEFAULT;}
	virtual void		Reuse	(){Release();}

public:
	virtual VOID		Init		(DWORD dwMapCode
                              ,DWORD dwFieldIndex
										,WORD  wMonType);
	virtual VOID		Release	();
	virtual VOID		Update	();

public:
	virtual RegenLocation*	AllocLocation	();
	virtual void				FreeLocation	(RegenLocation*);

public:
	virtual VOID		CreateNPC(DWORD          dwCode
                              ,DWORD          dwNumberOfNPCs
										,Vector3D*      pvPos
										,RegenLocation* pHomeLocation
										,DWORD          dwGroupID)=0;

	virtual VOID		AddRegenLocation	( RegenLocation* pLoc );


	virtual DWORD		GetRegenAmount	(RegenLocation* pLocation
												,DWORD          dwNPCCode
												,DWORD          dwMaxNum);
	virtual VOID		ProcessRegen	(BOOL				 bSpawnAll=FALSE )=0;


	virtual VOID		_ProcessRegenGroup(BOOL   bBossRegen
													,DWORD  dwAreaTileCnt
													,TILEINDEX* pdwTileIndex);

	virtual VOID		_ProcessRegenSingle	(BOOL   bSpawnAll
														,DWORD  dwAreaTileCnt
														,TILEINDEX* pdwTileIndex);

public:
	template<class OPR>
	void	ForEachLocation	(OPR& opr);

private:
	RegenLocationArray	m_RegenLocations;	//生成点列表

protected:
	util::Timer				m_RegenTimer;		//生成时间发生器

	sREGEN_INFO				*m_pRegenInfo;	//怪物生成资源表
	RegenLocation			*m_pLocation;	//当前生成点

	VG_PTR_PROPERTY2		(Field);
};//class RegenManager

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "RegenManager.inl"

#endif //__REGENMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Player.h
创建日期：2009年6月3日
最后更新：2009年6月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PLAYER_H__
#define __PLAYER_H__
#pragma once



#include "CommonDefine.h"
#include <PlayerCookie.h>
#include "ConstDefine.h"
#include <Timer.h>
#include "StructBase.h"
#include <BaseContainer.h>
#include <PacketStruct_ClientGameS.h>
#include "Character.h"
#include "MiscUtil.h"
#include "IScriptCharacterListener.h"
#include "ServerPlayerDefine.h"

using namespace util;
using namespace RC;

class MissionManager;
class GameServerQuestManager;
class ItemManager;
class ServerSession;
class ITradeContainer;
class IVendorContainer;
class SkillSlot;
class SkillManager;
class SlotManager;
class QuickManager;
class QuickStyleManager;
class IMovementChecker;
class IPlayerWarehouse;
class IEventInventory;
class PVPResult;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API Player	: public Character
											, public PlayerCookie
											, public IScriptCharacterListener
{
	friend class PlayerManager;
	friend class Village;
	friend class GameServerScriptHandler;

public:
	Player();
	virtual ~Player();

public:
	virtual BOOL	Init		();
	virtual VOID	Release	();
	virtual VOID	Create	(ServerSession * pServerSession
                           ,KEYTYPE         UserKey
									,const TCHAR *   pszID)=0;
	virtual VOID	Destroy	()=0;
	virtual VOID	Update	( DWORD dwTick );


public:
	////////////////////////////////////////////////////////////////////////
	//脚本与任务操作
	//virtual BOOL	KeepQuestScript	(DWORD dwScriptType,DWORD dwQuestType)=0;
	virtual BOOL	RunScript			(DWORD dwData, DWORD	npcID,BOOL bRunNow=FALSE )=0;
	virtual BOOL	RunQuestScript		(CODETYPE questID, DWORD dwData,BOOL bRunNow=FALSE)=0;//, DWORD	npcID );

	virtual DWORD	GetQuestState		( CODETYPE	questID )=0;
	virtual void	SetQuestState		( CODETYPE	questID, DWORD state )=0;
	virtual void	DiscardQuest		( CODETYPE	questID)=0;
	virtual BOOL	IsQuestActive		( CODETYPE	questID )=0;
	virtual BOOL	IsQuestDone			( CODETYPE	questID )=0;
	virtual BOOL	CanGetNewQuest		()=0;
	virtual BOOL	IsNeedDropTaskItem(CODETYPE		questID
												,DWORD			taskState
												,SLOTCODE		itemCode)=0;

	virtual void	ChangeQuestData	(CODETYPE questID
                                    ,DWORD    state
												,DWORD    action)=0;
	virtual void	OnSetItemLog		(BOOL bSet)=0;

public:
	/////////////////////////////////////////////
	PLAYER_PTY_VIRTUAL(Strength			,STR );
	PLAYER_PTY_VIRTUAL(Dexterity			,DEX );
	PLAYER_PTY_VIRTUAL(Vitality			,VIT );
	PLAYER_PTY_VIRTUAL(Spirit				,SPR );
	PLAYER_PTY_VIRTUAL(Inteligence		,INT );
	PLAYER_PTY_VIRTUAL(Experty1			,Experty1 );
	PLAYER_PTY_VIRTUAL(Experty2			,Experty2 );

public:
	/////////////////////////////////////////////
	// 基本操作
	virtual BOOL		IsGMPlayer		()=0;
	virtual BOOL		IsSuperPlayer	()=0;
	virtual BOOL		CanChat			()=0;
	virtual BOOL		CanBehave		()=0;
	virtual VOID		SetDBCharState	(eDBCHAR_STATE stat
                                    ,WORD          wMinutes=0)	=0;

	/////////////////////////////////////////////
	virtual sPLAYERINFO_BASE *	GetCharInfo	()=0;
	virtual VOID		GetRenderInfo			( sPLAYERINFO_RENDER		* OUT pRenderInfo )=0;
	virtual VOID		GetVillageInfo			( sPLAYERINFO_VILLAGE	* OUT pVillageInfo )=0;
	virtual VOID		GetGuildRenderInfo	( sGUILDINFO_RENDER		* OUT pRenderInfo )=0;
	virtual VOID		GetVendorRenderInfo	( sVENDORINFO_RENDER		* OUT pRenderInfo )=0;

	/////////////////////////////////////////////
	virtual VOID		ModifyPlayerTitle		( ePLAYER_TITLE removeFlag, ePLAYER_TITLE addFlag )=0;


	/////////////////////////////////////////////
	virtual BOOL		SendPacket			(MSG_OBJECT_BASE * pMsg
                                       ,WORD              wSize
													,BOOL bCrypt =     FALSE);
	virtual BOOL		SendPackets			(DWORD dwNumberOfMessages
                                       ,BYTE** pMsg
													,WORD* pwSize)=0;
	virtual BOOL		SendToDBPorxy		( MSG_OBJECT_BASE * pMsg, WORD wSize )=0;
	virtual BOOL		SendToGuildServer	( MSG_OBJECT_BASE * pMsg, WORD wSize )=0;
	virtual VOID		ForceDisconnect	( eDISCONNECT_REASON reason = DISCONNECT_UNKNOWN_REASON )=0;


	/////////////////////////////////////////////
	virtual eBATTLE_RESULT		CanNormalAttack(BYTE     byAttackType
															,VECTOR3D &vCurPos
															,VECTOR3D &vDestPos)=0;
	virtual eBATTLE_RESULT		CanStyleAttack	(BYTE     byAttackType
															,SLOTCODE StyleCode
															,VECTOR3D &vCurPos
															,VECTOR3D &vDestPos)=0;
	virtual VOID					SetAttackDelay	(BYTE     byAttackType
                                             ,SLOTCODE StyleCode)	=0;
	virtual VOID					OnPunish			()=0;
	virtual BOOL					IsFriend			( CharacterCookie *pTarget )=0;
	

	/////////////////////////////////////////////
	virtual VOID					SetActionDelay	( DWORD delay )=0;
	virtual DWORD					GetActionDelay	()=0;
	virtual BOOL					IsActionExpired()=0;
	virtual BOOL					IsOverPlayingTime()=0;

	/////////////////////////////////////////////
	virtual BOOL					ProcessThrust	(BOOL      bForcedMove
                                             ,Vector3D* pvMoveDir
															,Vector3D  &vThrustPos
															,float     fMoveDistance
															,BOOL      bDownAfterThrust=FALSE);

	/////////////////////////////////////////////
	virtual eITEM_RESULT			PrepareUseItem	( sITEMINFO_BASE * pItemInfo )=0;


	/////////////////////////////////////////////
	DWORD						GetNextExp				() const	;
	CHARGUID					GetCharGuid			() ;
	USERGUID					GetUserGuid			() ;
	ePLAYER_TYPE			GetCharType			() const;
	SLOTCODE					GetSelectedStyle	()		;
	VOID						SetSelectedStyle	(WORD wStyleCode )	;
	BYTE						GetHelmetOption	() ;
	VOID						SetHelmetOption	( BYTE byOption ) ;
protected :
	//////////////////////////////////
	virtual VOID			ReadyNextExp();
	virtual VOID			AutoFitLevel();
	virtual VOID			OnLevelUp	()=0;
	virtual VOID			OnExpChanged( DWORD dwTargetObjKey )=0;


public:
	//////////////////////////////////
	virtual VOID					OnSetCharInfo		()=0;
	virtual sITEMINFO_BASEC*	GetEquipItemInfo	(eEQUIP_POS_INDEX pos) const=0;


	//////////////////////////////////
public:


	/////////////////////////////////////////////
	virtual VOID				AddExp		(INT exp, DWORD dwTargetObjKey=0 );

	/////////////////////////////////////////////
	virtual BOOL				CanPlusMoney	( MONEY plus_money )=0;
	virtual BOOL				CanMinusMoney	( MONEY minus_money )=0;
	virtual BOOL				PlusMoney		( MONEY plus_money )=0;
	virtual BOOL				MinusMoney		( MONEY minus_money )=0;
	virtual VOID				SetMoney			( MONEY money )		=0;
	virtual DWORD				GetWeaponKind				()=0;

	/////////////////////////////////////////////
	//开始位置
	virtual VOID				SaveStartPos	()=0;
	virtual Vector3D			LoadStartPos	( Field * pField )=0;


	/////////////////////////////////////////////
	//属性分配处理
	virtual BOOL				CanChangeStatAttr	(eATTRIBUTE_TYPE attrType) const;
	virtual DWORD				PrepareStatAttr	(eATTRIBUTE_TYPE attrType);

	/////////////////////////////////////////////
	virtual VOID				LevelUp		( DWORD dwPlusLevel );
	virtual VOID				StatPointUp	( DWORD dwBonusStat )=0;
	virtual VOID				SkillPointUp( DWORD dwBonusSkill )=0;


	/////////////////////////////////////////////
	//技能处理
	virtual SLOTCODE			GetDefaultStyle		( DWORD dwWeaponCode )=0;
	virtual SLOTCODE			GetRangeStyle			()=0;//远程攻击心法

	virtual eSKILL_RESULT	PrepareUseSkill		(SLOTCODE               skillCode
                                                ,BOOL bCoolTimerReset = TRUE)		;
	virtual eSKILL_RESULT	CanLevelUpSkill		(BOOL     bSkill
                                                ,SLOTCODE CurSkillCode
																,SLOTCODE &NextSkillCode)=0;
	virtual DWORD				LevelUpSkill			(BOOL        bSkill
                                                ,SLOTCODE    CurSkillCode
																,SLOTCODE    NextSkillCode
																,sSKILL_SLOT &skillSlot)=0;
	virtual BOOL				CheckPassiveSkill		( SLOTCODE skillCode );
	virtual eSKILL_RESULT	CheckSkillCondition	( SLOTCODE skillCode );
	virtual BOOL				CheckClassDefine		(BYTE     byUserType
                                                ,CODETYPE dwClassDefine)	=0	;


	/////////////////////////////////////////////
	//邻近NPC任务信息
	virtual void				ClearAroundQuests	();
	virtual void				PushAroundQuest	(DWORD dwQuestID);
	virtual void				GetAroundQuests	(sTOTALINFO_QUEST& totalInfo);

	//----------------------------------------------------------------------
	virtual IPlayerWarehouse*	GetWarehouseContainer()=0;


protected:
	///////////////////////////////////////////
	VG_DWORD_PROPERTY			(UserKey	);
	VG_CSTR_PROPERTY			(UserID				,MAX_ID_LENGTH);
	VG_PTR_PROPERTY			(ServerSession		,ServerSession);
	VG_INT_GET_PROPERTY		(EnterCounter);

	///////////////////////////////////////////
	Timer							m_DBUpdateTimer;	
	PlayerQuestDataMap		m_AroundQuests;		//玩家附近NPC相关的Quest状态

	DWORD							m_dwNextExp;	


	VG_PTR_GET_PROPERTY		(SkillManager		,SkillManager);
	VG_PTR_GET_PROPERTY		(MissionManager	,MissionManager);
	VG_PTR_GET_PROPERTY		(SlotManager		,SlotManager);
	VG_PTR_GET_PROPERTY		(StyleManager		,QuickStyleManager);
	VG_PTR_GET_PROPERTY		(ItemManager		,ItemManager);
	VG_PTR_GET_PROPERTY		(QuickManager		,QuickManager);

	VG_PTR_PROPERTY			(TradeContainer		,ITradeContainer);
	VG_PTR_PROPERTY			(VendorContainer		,IVendorContainer);
	VG_PTR_GET_PROPERTY		(EventSlotContainer	,IEventInventory);


	VG_PTR_GET_PROPERTY		(MovementChecker	,IMovementChecker);
	VG_BYTE_PROPERTY			(LastAttackSeq);

	VG_BOOL_GET_PROPERTY		(ExpiredPlayer);
	VG_BOOL_GET_PROPERTY		(DoingAction);
	VG_DWORD_PROPERTY			(ReservedValue);

	util::Timer					m_ActionTimer;				// 
	util::Timer					m_PlayingTimer;			//
	util::Timer					m_ShapShotLogTimer;		//

	VG_TYPE_PROPERTY			(Behave		,ePLAYER_BEHAVE_STATE);
	VG_DWORD_GET_PROPERTY	(TitleFlag);

	//----------------------------------------------------------------------
	VG_PTR_GET_PROPERTY		(PVPResult	,PVPResult);

	VG_TYPE_PROPERTY			(InvitationHostKey, PLAYERKEY);
	VG_BYTE_PROPERTY			(ReloadCount);

	VG_FLOAT_PROPERTY			(AddMoveSpeedRatio);

	///////////////////////////////////////////
#ifdef _DEBUG
	DWORD							m_dwLastSendTick;
	DWORD							m_dwTotalSentSize;
#endif

};

//OBJECT_FACTORY_DECLARE(Player);


#include "Player.inl"



#endif //__PLAYER_H__
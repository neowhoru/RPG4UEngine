#pragma once

#include <CommonDefine.h>
#include "IVendorContainer.h"

using namespace RC;
struct sVENDOR_ITEM_SLOTEX;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API VendorManager
{
public:
	VendorManager(void);
	virtual ~VendorManager(void);

public:
	///////////////////////////////////////////
	//开摊收摊
	virtual eVENDOR_RESULT StartVendor	(Player *                           pEstablishedPlayer
                                       ,LPCTSTR                            pTitle
													,sTOTALINFO_ESTABLISHER_VENDOR & IN TotalInfo)=0;
	virtual eVENDOR_RESULT EndVendor		(Player * pEstablishedPlayer )=0;

	///////////////////////////////////////////
	//卖主操作
	virtual eVENDOR_RESULT Rename	(Player * pEstablishedPlayer
                                 ,LPCTSTR  szVendorTitle)		=0;
	virtual eVENDOR_RESULT Swap	(Player * pEstablishedPlayer
                                 ,SLOTPOS  fromPos
											,SLOTPOS  toPos);
	virtual eVENDOR_RESULT Modify	(Player *                  pEstablishedPlayer
                                 ,sVENDOR_ITEM_SLOT & IN    slotInfo
											,sVENDOR_ITEM_SLOTEX & OUT retInfo)=0;
	virtual eVENDOR_RESULT Insert	(Player *                  pEstablishedPlayer
                                 ,sVENDOR_ITEM_SLOT & IN    slotInfo
											,sVENDOR_ITEM_SLOTEX & OUT retInfo)=0;
	virtual eVENDOR_RESULT Delete	(Player * pEstablishedPlayer
                                 ,SLOTPOS  posVendor)		=0;

	///////////////////////////////////////////
	//购买者操作
	virtual eVENDOR_RESULT StartViewVendor	(Player *                pEstablisher
                                          ,Player *                pObserver
														,TCHAR * OUT             szVenderTitle
														,sTOTALINFO_VENDOR & OUT VendorTotalInfo);
	virtual eVENDOR_RESULT EndViewVendor	(Player * pObserver );
	

	virtual eVENDOR_RESULT Buy					(Player *                   pObserver
                                          ,SLOTPOS                    posVendor
														,sTOTALINFO_INVENTORY & OUT TotalInfo);


	///////////////////////////////////////////
	virtual BOOL CheckEstablisher		( Player * pPlayer )=0;
	virtual BOOL CheckObserver			( Player * pPlayer )=0;

protected:
	virtual IVendorContainer *		_Alloc	();
	virtual VOID						_Free		( IVendorContainer * pUnit );


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(VendorManager , _SERVERBASEGAME_API );
//extern API_NULL VendorManager& singleton::GetVendorManager();
#define theVendorManager  singleton::GetVendorManager()


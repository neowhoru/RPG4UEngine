#ifndef __EFFECT_FACTORY_H__
#define __EFFECT_FACTORY_H__

#pragma once

#include <TMemoryPoolFactory.h>

using namespace util;


class BaseEffect;


class _SERVERBASEGAME_API EffectFactory 
{
public:
	EffectFactory();
	virtual ~EffectFactory();

	virtual BOOL			Init		( DWORD dwMaxPoolSize );
	virtual VOID			Release	();

public:
	BaseEffect*		AllocEffect	( eFIELDEFFECT_TYPE eEffectType );
	VOID				FreeEffect	( BaseEffect* pEffect );

private:

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(EffectFactory , _SERVERBASEGAME_API );
//extern API_NULL EffectFactory& singleton::GetEffectFactory();
#define theEffectFactory  singleton::GetEffectFactory()


#endif

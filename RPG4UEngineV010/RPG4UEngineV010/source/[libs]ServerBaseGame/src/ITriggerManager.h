/*////////////////////////////////////////////////////////////////////////
文 件 名：ITriggerManager.h
创建日期：2009年6月11日
最后更新：2009年6月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITRIGGERMANAGER_H__
#define __ITRIGGERMANAGER_H__
#pragma once


#include <THashTable.h>

struct TRIGGER_MSG;
struct TRIGGER_LEAVE_AREA;
struct TRIGGER_ENTER_AREA;
class ITrigger;
class TriggerInfo;
class TriggerGroupInfo;
class TriggerAreaHash;
class Player;

using namespace util;

typedef THashTable<ITrigger *, IDTYPE>		TriggerTable;
typedef TriggerTable::iterator				TriggerTableIt;

#define INVALID_SWITCH_ID_VALUE		255

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API ITriggerManager
{
	enum { MAX_SWITCH_SIZE = 255, };
public:
	ITriggerManager(void);
	virtual ~ITriggerManager(void);


	/////////////////////////////////////////////
public:
	virtual VOID		Init		(TriggerGroupInfo & IN rInfo
                              ,Field *               pField)=0;
	virtual VOID		Release	()=0;
	virtual VOID		Update	()=0;

	virtual VOID		Disconnected	( DWORD dwUserKey )=0;

	virtual VOID		OnMsg				( TRIGGER_MSG * pMsg )=0;


	virtual VOID		ActiveTrigger	( IDTYPE triggerID, BOOL bActive )=0;
	virtual VOID		ActiveTrigger	( IDTYPE triggerID );

	virtual VOID		SetAttribute	( TILEINDEX iTileID, BYTE byAttr )=0;
	virtual VOID		UnsetAttribute	( TILEINDEX iTileID, BYTE byAttr )=0;

	virtual SpecialArea * GetAreaInfo	( INT index )=0;
	virtual VOID		CreateNPC			(CODETYPE   MonsterCode
                                       ,DWORD      dwNumberOfNPC
													,Vector3D * pvPos
													,DWORD      dwAreaID
													,DWORD      dwGroupID)=0;
	virtual VOID		CreateNPC2			(CODETYPE   MonsterCode
                                       ,DWORD      dwNumberOfNPC
													,Vector3D * pvPos
													,DWORD      dwAreaID
													,DWORD      dwGroupID
													,WORD       wAngle)=0;

	virtual Vector3D		GetRandomPosAt		(TILEINDEX iTile )=0;
	virtual INT				GetTileToStand		(Vector3D * pvPos )=0;
	virtual MapObject *	 FindMapObject		(DWORD dwObjectKey )=0;
	virtual BOOL			RemoveMapObject	(DWORD dwObjectKey )=0;

	virtual DWORD		GetPlayerNum		()=0;
	
	virtual VOID		SendToAll			(MSG_OBJECT_BASE * pMsg
                                       ,WORD              wSize)		=0;




	///////////////////////////////////////////////////
	virtual VOID		CreateAreaHash			( INT AreaID )=0;
	virtual VOID		OnLeaveArea				( TRIGGER_LEAVE_AREA * pMsg )=0;
	virtual VOID		OnEnterArea				( TRIGGER_ENTER_AREA * pMsg )=0;
	virtual DWORD		GetPlayerNumAtArea	( INT AreaID )=0;

	virtual	void		OnTriggerDone			(Player*	pPlayer)=0;

	
	///////////////////////////////////////////////////////////////
	BYTE GetSwitchValue	( BYTE nIndex )					;
	VOID SetSwitchValue	( BYTE nIndex, BYTE value )	;


	///////////////////////////////////////////////////////////////
	template <class ObjectOperator>
	VOID		ForEachPlayer( ObjectOperator & Opr );

	template <class OPR>
	VOID		ForeachTrigger( OPR & func );
	template <class OPR>
	VOID		ForeachActiveTrigger( OPR & func );
	template <class OPR>
	VOID		ForeachTriggerArea( OPR & func );

	template <class ObjectOperator>
	VOID		ForEachPlayerInArea( INT AreaID, ObjectOperator & Opr );


//protected:
	///////////////////////////////////////////////////////////////
	VOID		_RemoveActiveTrigger	(ITrigger * pTrigger );
	VOID		_AddActiveTrigger		(ITrigger * pTrigger );

	void		_RemoveTriggers		();
	void		_RemoveTriggerAreas	();
	void		_RemoveActiveTriggers();
	void		_AddTrigger				(ITrigger * pTrigger);
	void		_AddTriggerArea		(TriggerAreaHash * pArea,INT AreaID);

	ITrigger*			GetTrigger			(DWORD dwID);
	TriggerAreaHash*	GetTriggerArea		(DWORD dwID);
	ITrigger*			GetActiveTrigger	(DWORD dwID);

protected:
	///////////////////////////////////////////////////////////////
	Field *					m_pField;
	BYTE						m_FlagValues[MAX_SWITCH_SIZE];

private:
	///////////////////////////////////////////////////////////////
	TriggerTable								m_ActiveTriggers;	
	TriggerTable								m_Triggers;
	THashTable<TriggerAreaHash *, INT>	m_TriggerAreas;
};


#include "ITriggerManager.inl"

#endif //__ITRIGGERMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：VariableMsg.inl
创建日期：2008年5月2日
最后更新：2008年5月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __VARMSG_INL__
#define __VARMSG_INL__
#pragma once


template< class PACKET >
inline PACKET * VariableMsg::StartPacket( Param2Class<PACKET> )
{
	PACKET* pRet = new (m_ppMsg[0])(PACKET); 
	m_byMsgCount  = 1;
	return pRet;
}


inline BYTE ** VariableMsg::GetMsgOutPtrArray()			
{
	return m_ppMsgOut;
}

inline WORD * VariableMsg::GetSizeArray()		
{
	return m_arSizes; 
}




#endif //__VARMSG_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Field.h
创建日期：2009年6月4日
最后更新：2009年6月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __FIELD_H__
#define __FIELD_H__
#pragma once

#include "CommonDefine.h"
#include <TMemoryPoolFactory.h>	 
#include <ResultCode.h>
#include <DataTypeDefine.h>
#include "Sector.h"
#include "MapInfo.h"
#include "ObjectGroupManager.h"
#include "SectorGroup.h"
#include "SectorGroupList.h"
#include "FieldDefine.h"

using namespace util;
using namespace RC;

class Object;
class Character;
class MapNPC;
class Player;
class Map;
class FieldInfo;
class AIManager;
class NPC;
struct APP_MSG;
class ObjectList;
struct MSG_OBJECT_BASE;
struct AIMSG;
class RegenManager;
class ITriggerManager;
class sMAPOBJECT_INFO;
class MapObject;
class ObjectGroupManager;
class EffectManager;
class BaseEffect;
class TargetFinder;
class SectorGroupList;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API Field
{
public:
	Field();
	virtual ~Field();

public:
	virtual DWORD		GetFactoryType()=0;
	virtual void		Reuse	(){Release();}

public:
	virtual VOID	Init			(Map*            pMap
										,FieldInfo *     pFieldInfo
										,LPCTSTR         pszGroupName);
	virtual VOID	Release		()		;
	virtual VOID	Update		( DWORD dwTick );
	virtual void	InitSectorGroups	();
	virtual void	InitSectors			();

public:
	virtual eSEND_RESULT SendAround	(DWORD             dwSectorIndex
                                    ,MSG_OBJECT_BASE * pMsg
												,WORD              wSize
												,DWORD             dwMyObjKey=0)=0;
	virtual eSEND_RESULT SendToAll	(MSG_OBJECT_BASE * pMsg
                                    ,WORD              wSize
												,DWORD             dwMyObjKey=0)=0;

	virtual eSEND_RESULT SendAround	(DWORD  dwSectorIndex
                                    ,DWORD  dwNumberOfMessages
												,BYTE** pMsg
												,WORD*  pwSize
												,DWORD  dwMyObjKey=0)=0;
	virtual eSEND_RESULT SendToAll	(DWORD  dwNumberOfMessages
                                    ,BYTE** pMsg
												,WORD*  pwSize
												,DWORD  dwMyObjKey=0)=0;

public:
	virtual BOOL		EnterField		(Object*    pObject
                                    ,Vector3D * pvPos);
	virtual BOOL		EnterField		(Object*    pObject
                                    ,Vector3D * pvPos
												,WORD       wAngle);
	virtual VOID		LeaveField		(Object* pObject );
	virtual VOID		UpdateSectorBy	(Object* pObject );

public:

	virtual VOID		JumpObjectTo			(Object*    pObject
                                          ,Vector3D * pvPos)	=0;	//瞬移
	virtual BOOL		ProcessMeettingPlayer(Player* pPlayer
                                          ,NPC *   pNpc)		=0;
	virtual DWORD		GetSectorIndex			(Vector3D* pvPos)=0;
	virtual BOOL		IsMovableAt				(Vector3D* pvPos,BOOL bCheckBlocked=TRUE)=0;

public:
	virtual BOOL		CreateNPC		(eOBJECT_TYPE eObjectType
                                    ,DWORD        dwCode
												,DWORD        dwNumberOfNPC
												,Vector3D*    pvPos
												,DWORD        dwAreaID=0
												,DWORD        dwGroupID=0)=0;
	virtual BOOL		CreateNPC2		(eOBJECT_TYPE eObjectType
                                    ,DWORD        dwCode
												,DWORD        dwNumberOfNPC
												,Vector3D*    pvPos
												,DWORD        dwAreaID=0
												,DWORD        dwGroupID=0
												,WORD         wAngle=0)=0;
public:
	virtual BOOL		FindPath			(Character* pChar
                                    ,Vector3D*  pvDest)		;
	virtual BOOL		FindThrustPath	(Character* pChar
                                    ,Vector3D*  pvMoveDir
												,BOOL       bForcedMove=FALSE);

public:
	virtual Character*	SearchTarget	(NPC*                pNPC
                                       ,eTARGET_SEARCH_TYPE eSearchType
													,eSKILL_TARGET_TYPE  eTargetType)=0;
	virtual Character*	FindCharacter	( DWORD dwObjectKey )=0;		
	virtual NPC*			FindNPC			( DWORD dwObjectKey )=0;
	virtual BOOL			AddNPC			( NPC* pNPC )=0;
	virtual BOOL			RemoveNPC		( NPC* pNPC )=0;
	virtual VOID			PushNpcDeadList( Object *pObject )=0;
	
	virtual VOID			RemoveAllNPC		()=0;
	virtual VOID			LoadAllMapNPC		()=0;
	virtual VOID			AddMapObjects		()=0;
	virtual VOID			RemoveMapObjects	()=0;
	virtual BOOL			RemoveMapObject	(DWORD dwMapObjectID )=0;
	virtual MapObject *	FindMapObject		(DWORD dwObjectKey )=0;

public:
	virtual VOID			GetRandomPosAtArea	( IDTYPE areaID, Vector3D & vPosRet );
	virtual Vector3D		GetRandomStartPos		()=0;
	virtual BOOL			CheckMapVersion		( DWORD dwCheckSum )=0;

	virtual VOID			AINoticeAround			(Object* pObject
                                             ,AIMSG* pMsg
															,WORD    wSize
															,DWORD   dwDelay=0)=0;

public:
	CODETYPE					GetFieldCode			() ;
	TileLand *				GetTileLand				() ;

public:
	virtual BYTE			FindTargetsBy		(eSKILL_TARGET_TYPE eTargetType
                                          ,Character*         pAttacker
														,Character**        pTargetArray
														,Vector3D           &vMainTargetPos
														,float              fRange
														,UINT			        uiRangeForm
														,float              fArea=0
														,BYTE               byCountLimit=MAX_TARGET_NUM
														,DWORD              dwExceptTargetKey=0)=0;
	

	virtual BYTE			FindTargetsAround	(Character** pTargetArray
                                          ,DWORD       dwSectorIndex
														,Vector3D    &vMainPos
														,float       fRange
														,BYTE        byCountLimit=MAX_TARGET_NUM)=0;


public:
	virtual VOID			InitRegenManager		(DWORD dwMonsterType=INVALID_DWORD_ID)=0;


public:
	virtual ObjectGroup*	AllocGroup	(DWORD		dwAreaID
                                    ,DWORD		dwGroupID)		=0;
	virtual ObjectGroup*	FindGroup	(DWORD		dwAreaID
                                    ,DWORD		dwGroupID)		=0;
	virtual ObjectGroup*	FindGroup	(GROUPTYPE	groupKey )=0;
	virtual BOOL			RemoveGroup	(ObjectGroup *pGroup )=0;


public:
	virtual DWORD			GetPlayerNum()=0;
	virtual BaseEffect*	AllocEffect	(eFIELDEFFECT_TYPE eEffectType )=0;

public:
	virtual	void OnMonsterDead			(DWORD dwMonsterCode, DWORD dwKillerKey)=0;
	virtual	void OnDisconnected			(DWORD dwUserKey)=0;
	virtual	void OnTriggerDone			(Player*	pPlayer)=0;
	virtual	void OnCharacterDamage		(DWORD dwTargetKey)=0;
	virtual	void OnTriggerClick			(DWORD dwTargetKey, DWORD dwUserKey)=0;
	virtual	void OnTriggerEnterArea		(DWORD dwAreaID	, DWORD dwUserKey)=0;
	virtual	void OnTriggerLeaveArea		(DWORD dwAreaID	, DWORD dwUserKey)=0;

public:

	////////////////////////////////////////////////
	template<class OPR>	VOID			ForEachNPC	( OPR & Opr );
	template<class OPR>	VOID			ForEachPlayer( OPR & Opr );

	template<class OPR>	Character*	SearchPlayer( OPR& finder );
	template<class OPR>	Character*	SearchNPC	( OPR& finder );
	template<class OPR>	BOOL			SearchItemAround		( DWORD dwSectorIndex, OPR & Opr );

	template<class OPR>	VOID			ForEachPlayerAround	( DWORD dwSectorIndex, OPR & Opr );
	template<class OPR>	VOID			ForEachItemAround		( DWORD dwSectorIndex, OPR & Opr );
	template<class OPR>	VOID			ForEachNpcAround		( DWORD dwSectorIndex, OPR & Opr );


	////////////////////////////////////////////////
	virtual	VOID		SetActiveRoom	()=0;

	virtual	VOID		KillAllNPC		(Player* pPlayer
                                    ,BOOL    bIsDrop)	=0;
	virtual	BOOL		KillNPC			(Player* pPlayer
                                    ,NPC*    pNPC
												,BOOL    bIsDrop)=0;
	
	virtual	Vector3D	GetRandPosAround(Vector3C * pOrgPos
                                     ,INT         iDist)	=0;

protected:
	////////////////////////////////////////////////////////////////
	virtual	VOID		_IncPlayerRefCounter	( DWORD dwSectorIndex, Object *pObject )=0;
	virtual	VOID		_DecPlayerRefCounter	( DWORD dwSectorIndex, Object *pObject )=0;

	//Character*	_SearchTarget		( TargetFinder *pFinder );
	Sector*	_GetSector		(DWORD dwSectorIndex );
	void		_PushSector		(Sector* pSector);
	UINT		GetSectorAmount();

private:

	
	SectorArray 		m_Sectors		;
	SectorSet			m_CurSectors	;
	SectorSet			m_PrevSectors	;
	SectorSet			m_EnterSectors	;
	SectorSet			m_LeaveSectors	;


protected:
	VG_DWORD_GET_PROPERTY	(SectorXNum		);
	VG_DWORD_GET_PROPERTY	(SectorYNum		);
	VG_DWORD_GET_PROPERTY	(TotalSectorNum);
	VG_DWORD_GET_PROPERTY	(SectorSize		);

	///////////////////////////////////////////////////
	VG_PTR_GET_PROPERTY	(DeadNPCList	,ObjectList);
	VG_PTR_GET_PROPERTY	(MapObjectList	,ObjectList);
	VG_PTR_GET_PROPERTY	(NPCList			,ObjectList);

	VG_PTR_GET_PROPERTY2	(RegenManager);
	VG_PTR_GET_PROPERTY2	(ObjectGroupManager);
	VG_PTR_GET_PROPERTY2	(EffectManager);
	VG_PTR_GET_PROPERTY	(TriggerManager			,ITriggerManager);

	VG_INT_GET_PROPERTY	(RoomIndex);
	VG_PTR_GET_PROPERTY2	(SectorGroupList);
	VG_PTR_GET_PROPERTY2	(Map);
	VG_PTR_GET_PROPERTY2	(FieldInfo);
};//class Field




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "Field.inl"




#endif //__FIELD_H__
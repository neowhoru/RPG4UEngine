/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenLocation.inl
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENLOCATION_INL__
#define __REGENLOCATION_INL__
#pragma once


inline VOID		RegenLocation::PushAreaKey( DWORD dwAreaKey )	
	{ 
		m_RegenAreas.push_back( dwAreaKey ); 
	}

inline DWORD		RegenLocation::GetAreaAt(UINT nAt )	
	{ 
		if(nAt >= m_RegenAreas.size())
			return INVALID_DWORD_ID;
		return m_RegenAreas[nAt]; 
	}

inline DWORD		RegenLocation::GetAreaAmount()	
	{ 
		return  m_RegenAreas.size();
	}


inline VOID		RegenLocation::ClearAlives(  )	
	{ 
		m_AliveInfos.clear();
	}




#endif //__REGENLOCATION_INL__
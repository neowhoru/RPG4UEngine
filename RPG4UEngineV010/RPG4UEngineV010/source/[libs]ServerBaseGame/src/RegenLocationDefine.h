/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenLocationDefine.h
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENLOCATIONDEFINE_H__
#define __REGENLOCATIONDEFINE_H__
#pragma once

class RegenLocation;

//#include <hash_map>
using namespace stdext;
using namespace std;

typedef hash_map<DWORD, DWORD>			RegenAliveMap;		// <MonsterCode, Count>
typedef RegenAliveMap::iterator			RegenAliveMapIt;
typedef pair<DWORD, DWORD>					RegenAliveMapPair;

typedef vector<DWORD>						RegenAreaArray;
typedef RegenAreaArray::iterator			RegenAreaArrayIt;


typedef vector<RegenLocation*>			RegenLocationArray;
typedef RegenLocationArray::iterator	RegenLocationArrayIt;



enum { MAX_OBJECTGROUP_PER_AREA = 3 };

#endif //__REGENLOCATIONDEFINE_H__
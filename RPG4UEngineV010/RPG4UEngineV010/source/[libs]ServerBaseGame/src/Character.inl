/*////////////////////////////////////////////////////////////////////////
文 件 名：Character.inl
创建日期：2009年5月28日
最后更新：2009年5月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTER_INL__
#define __CHARACTER_INL__
#pragma once


inline BOOL Character::ProcessThrust	(BOOL      bForcedMove
                                    ,Vector3D* pvDestPos
												,Vector3D  &vThrustPos
												,float     fMoveDistance
												,BOOL      bDownAfterThrust)	
{ 
	__UNUSED(pvDestPos);
	__UNUSED(vThrustPos);
	return FALSE; 
}

inline VOID		Character::AddFightingEnergy( SHORT sCount )
{
	m_nFightingEnergies = max( 0, m_nFightingEnergies + sCount );
}
inline VOID		Character::DelFightingEnergy( SHORT sCount )
{
	m_nFightingEnergies = max( 0, m_nFightingEnergies - sCount ); 
}
inline SHORT	Character::GetFightingEnergy()	const	
{
	return m_nFightingEnergies;
}


inline VOID	Character::RecordAttackPoint( Character* pAttacker, int attackPoint ) 
{
	__UNUSED(pAttacker);
	__UNUSED(attackPoint);
}
inline VOID	Character::RecordTotalPoint( Character* pAttacker, int TotalPoint )
{
	__UNUSED(pAttacker); 
	__UNUSED(TotalPoint); 
}
inline BOOL	Character::IsFriend( CharacterCookie *pTarget )
{
	__UNUSED(pTarget);
	return TRUE; 
}

inline VOID	Character::ChangeAIState		(DWORD dwStateID, LPARAM param1 , LPARAM param2 , LPARAM param3)
{
	__UNUSED(dwStateID);
	__UNUSED(param1);
	__UNUSED(param2);
	__UNUSED(param3); 
}

inline eOBJECT_TYPE Character::GetCharacterType() const 
{
	return GetObjectType();
}

inline VOID		Character::SetExp		(DWORD /*exp*/)		{}
inline VOID		Character::SetLevel	(LEVELTYPE /*level*/){}
inline VOID		Character::SetName	(LPCTSTR /*szName*/)	{}
inline LPCTSTR	Character::GetName	()			
{
	return _T("");
}
inline LEVELTYPE	 Character::GetDisplayLevel() const		
{
	return GetLevel(); 
}

inline int Character::GetRegenHP() const	
{
	return 0;
}
inline int Character::GetRegenMP() const		
{
	return 0;
}
inline BOOL Character::IsAlive () const
{
	return GetHP() > 0; 
}
inline BOOL Character::IsDead () const			
{
	return !IsAlive(); 
}

inline SHORT Character::GetExperty1			()	const
{
	return 0; 
}
inline SHORT Character::GetExperty2			()	const	
{
	return 0;
}

inline MONEY Character::GetMoney			()	const	
{
	return 0;
}

inline VOID Character::AddExp( DWORD exp, DWORD dwTargetObjKey ) 
{
	__UNUSED(exp);
	__UNUSED(dwTargetObjKey); 
}

inline  VOID Character::SetShield	( int iShieldHP, int iDecreaseMP, float fRatio ) 
{
	m_iShieldHP = iShieldHP; 
	m_iShieldMP = iDecreaseMP;
	m_fShieldAbsorbRatio = fRatio; 
}

inline VOID Character::UpdateLifeRecover( BOOL bHPUpdate, BOOL bMPUpdate )
{
	__UNUSED(bHPUpdate);
	__UNUSED(bMPUpdate); 
}

inline BOOL Character::SendPacket(MSG_OBJECT_BASE * pMsg
                                 ,WORD              wSize
											,BOOL bCrypt)
{
	__UNUSED(pMsg);
	__UNUSED(wSize);
	return FALSE; 
}
inline BOOL Character::SendPackets(DWORD dwNumberOfMessages
                                   ,BYTE** pMsg
											  ,WORD* pwSize)
{
	__UNUSED(dwNumberOfMessages);
	__UNUSED(pMsg);
	__UNUSED(pwSize);
	return FALSE; 
}

inline VOID Character::AINotice(AIMSG* pMsg
                                ,WORD    wSize
										  ,DWORD   dwDelay)
{ 
	__UNUSED(pMsg);
	__UNUSED(wSize);
	__UNUSED(dwDelay);
}




inline BOOL Character::CheckPassiveSkill	( SLOTCODE /*SkillCode*/ )
{
	return FALSE;
}
inline BOOL Character::CheckClassDefine( BYTE /*byUserType*/, CODETYPE /*dwClassDefine*/ ) 
{
	return TRUE;
}

inline VOID				Character::SetTargetChar	( Character * /*pChar*/,BOOL /*bProcessObserver*/ ) {}
inline Character*		Character::GetTargetChar	()
{
	return NULL;
}


#endif //__CHARACTER_INL__
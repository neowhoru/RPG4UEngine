#pragma once

#include "CommonDefine.h"
#include <ConstSlot.h>
#include <ItemSlot.h>
#include <BaseContainer.h>
#include "DataPump.h"

class Player;


class _SERVERBASEGAME_API SlotManager : public DataPump
{
public:
	SlotManager(void);
	virtual ~SlotManager(void);

public:
	virtual DATAPUMP_TYPE GetFactoryType(){return DATAPUMP_SLOT;}

public:
	virtual VOID	Init		(Player* pPlayer,SlotManager * pMGR );
	virtual VOID	Release	();
	
	virtual BaseContainer *	GetSlotContainer( SLOTIDX Index )=0;


public:
	/////////////////////////////////////////////////////
	virtual BOOL	ValidPos			(SLOTIDX            atIndex
                                 ,SLOTPOS            atPos
											,BOOL						bEmptyCheck=TRUE)=0;
	virtual BOOL	IsEmpty			(SLOTIDX Index
                                 ,SLOTPOS pos)	=0;
	virtual BOOL	ValidState		()=0;
	virtual BOOL	ValidContainer	( SLOTIDX AtIndex )=0;
	
	/////////////////////////////////////////////////////
	template <class OPR>
	BOOL				ForeachSlot	( SLOTIDX idx, OPR & opr );
	template <class OPR>
	BOOL				Foreach		( SLOTIDX idx, OPR & opr );

protected:
	BaseContainer	*			m_pSlotContainer[SI_MAX];
};

#include "SlotManager.inl"



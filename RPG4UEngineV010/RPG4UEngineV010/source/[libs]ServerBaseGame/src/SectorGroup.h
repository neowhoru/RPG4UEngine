#pragma once


#include <CommonDefine.h>
#include "FieldDefine.h"
#include <THashTable.h>

using namespace util;

class Sector;

typedef THashTable<Sector *>				SectorTable;
typedef THashTable<Sector *>::iterator	SectorTableIt;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API SectorGroup
{
	friend class Field;
public:
	SectorGroup();
	virtual ~SectorGroup();

public:
	virtual DWORD		GetFactoryType(){ return SECTORGRP_TYPE_DEFAULT;}
	virtual void		Reuse	(){Release();}

public:
	virtual VOID		Init		( WORD wGroupIndex, DWORD wSectorNumForGroup );
	virtual VOID		Release	();
	virtual VOID		AddSector		(Sector *pSector );
	virtual VOID		SetRefCounter	(int nRefCounter )=0;


public:
	template<class OPR>	BOOL ForEach(OPR& opr);

	SectorTableIt Begin	(){return m_pSectors->begin();}
	SectorTableIt End		(){return m_pSectors->end();}

public:
	VG_DWORD_PROPERTY		(GroupIndex				);
	VG_INT_GET_PROPERTY	(ReferenceCount		);
	VG_BOOL_GET_PROPERTY	(Referenced				);

private:
	SectorTable *			m_pSectors;
};




#include "SectorGroup.inl"

#ifndef __OBJECTGROUP_MANAGER_H__
#define __OBJECTGROUP_MANAGER_H__

#pragma once

#include <TSingleton.h>
#include <THashTable.h>
#include <TMemoryPoolFactory.h>

using namespace util;

class ObjectGroup;
using namespace util;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API ObjectGroupManager
{
public:
	enum { MAX_DEFAULT_POOL_SIZE = 16 };

public:
	ObjectGroupManager();
	~ObjectGroupManager();

public:
	VOID			Init		( DWORD dwMaxPoolSize );
	VOID			Release	();

public:
	ObjectGroup*	AllocGroup	(DWORD dwAreaID
                              ,DWORD dwGroupID)		;
	BOOL				RemoveGroup	(ObjectGroup *pGroup );

	ObjectGroup*	FindGroup	(DWORD dwAreaID
                              ,DWORD dwGroupID)		;
	ObjectGroup*	FindGroup	(GROUPTYPE groupKey )		;

private:
	TMemoryPoolFactory<ObjectGroup> *		m_pGroupPool;
	THashTable<ObjectGroup *, GROUPTYPE> *	m_pGroupList;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline ObjectGroup*	ObjectGroupManager::FindGroup	( GROUPTYPE groupKey )
{
	return m_pGroupList->GetData( groupKey );
}


#endif // __OBJECTGROUP_MANAGER_H__

#ifndef __SUMMON_MANAGER_H__
#define __SUMMON_MANAGER_H__

#pragma once

#include <THashTable.h>
#include <TMemoryPoolFactory.h>

using namespace util;
using namespace RC;

class SummonedRelation;
class Character;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API SummonManager
{
public:
	SummonManager();
	virtual ~SummonManager();

public:
	virtual VOID	Init		( DWORD dwMaxPoolSize )=0;
	virtual VOID	Release	()=0;

public:
	virtual SummonedRelation*	AllocRelation	(Character* pSummoner
                                             ,BOOL       bPlayerSkill)		=0;
	virtual BOOL					FreeRelation	(SummonedRelation *pRelation )=0;
	virtual SummonedRelation*	FindRelation	( DWORD dwSummonKey )	=0;


public:
	virtual VOID				AddSummonedSkill(DWORD    dwSummonKey
                                           ,SLOTCODE skillCode)		=0;
	virtual VOID				DelSummonedSkill(DWORD    dwSummonKey
                                           ,SLOTCODE skillCode)		=0;

	virtual VOID				CancelSummonRelation( Character *pLeaveChar )=0;

	virtual eSUMMON_RESULT	RunCommand	(DWORD           dwSummonKey
													,eSUMMON_COMMAND eCommand
													,DWORD           dwObjectKey=0
													,DWORD           dwTargetKey=0)=0;


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(SummonManager , _SERVERBASEGAME_API);
//extern API_NULL SummonManager& singleton::GetSummonManager();
#define theSummonManager  singleton::GetSummonManager()


#endif // __SUMMON_MANAGER_H__

/*////////////////////////////////////////////////////////////////////////
文 件 名：NPC.h
创建日期：2009年6月2日
最后更新：2009年6月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NPC_H__
#define __NPC_H__
#pragma once


#include "CommonDefine.h"
#include "Character.h"
#include "StructBase.h"
#include <Timer.h>
#include "BattleRecord.h"
#include "ObjectGroup.h"
#include "ConstDefine.h"
#include "RatioSelector.h"
#include "MiscUtil.h"
#include "NpcCookie.h"
#include "AIInfoParserDefine.h"
#include "NPCDefine.h"

class RegenLocation;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API NPC : public Character, public NpcCookie
{
public:
	NPC();
	virtual ~NPC();

public:
	virtual VOID					SetBaseInfo			( sNPCINFO_BASE* pNPCInfo )=0;

	virtual VOID					GetRenderInfo		( sMONSTERINFO_RENDER * OUT pRenderInfo )=0;
	virtual void					GetAroundQuestsTo	(Player* /*pPlayer*/){};

	//////////////////////////////////////////////////////////
	//virtual VOID				SetTargetChar		(Character *pChar )=0;
	virtual Character*		GetTargetChar		();
	virtual VOID				SelectBestTarget	()=0;
	virtual VOID				RemoveEnemy			(DWORD leaveEnemyKey)=0;
	virtual BOOL				IsFriend				(CharacterCookie *pTarget )=0;
	virtual Character*		SelectSkillTarget	(SLOTCODE skillCode, BOOL bHeal=FALSE )=0;
	virtual VOID				SpecialAction		();			
	virtual VOID				StatusResist		();			
	virtual VOID				RegisterBattleRecords();	

	virtual float				GetDistToTarget	() const;
	virtual float				GetDistToTarget	( Character* pNewTargetChar ) const;


	virtual VOID				OnPartyNumberSet	( DWORD dwCurMemberNum ) ;



	////////////////////////////////////////////////
	virtual BOOL				CanRegenHPMP			()=0;

	virtual BOOL				UseSkill					(SLOTCODE skillCode )=0;
	virtual VOID				SelectSkill				();
	virtual VOID				SelectSkill				(Character *pTarget, SLOTCODE skillCode )=0;
	SLOTCODE						GetSelectedSkill		() const ;
	VOID							ResetSelectedSkill	() ;
	WORD							GetSelectedSkillDelay() const ;

	virtual BOOL				IsSameTargetPos		()=0;
	virtual VOID				AllocTrackPos			(Vector3D &vDestPos )=0;

	////////////////////////////////////////////////
	virtual BOOL				IsNearTargetPos		()=0;
	virtual BOOL				CanFallApartTarget	()=0;

	////////////////////////////////////////////////
	virtual BOOL				CanRetreatFromTarget	()=0;

	////////////////////////////////////////////////
	virtual BOOL			ProcessThrust			(BOOL      bForcedMove
                                             ,Vector3D* pvMoveDir
															,Vector3D  &vThrustPos
															,float     fMoveDistance
															,BOOL      bDownAfterThrust);
	virtual BOOL			IsPositionCanJump		()=0;

	// SendPacket
	virtual eSEND_RESULT	SendAround			(MSG_OBJECT_BASE *	pMsg
                                          ,WORD						wSize
														,BOOL						bSendToMe =  TRUE)=0;

	virtual BOOL			MoveBRD				(Vector3D*  pvDestPos
                                          ,eMOVE_TYPE moveState)=0;
	virtual BOOL			MoveThrustBRD		(Vector3D*  pvDestPos
                                          ,eMOVE_TYPE moveState
														,BOOL       &bBlocked
														,Vector3D   &vRotate);

	/////////////////////////////////////////////////////
	virtual	VOID			ChangeAIState		(DWORD  dwStateID
                                          ,LPARAM param1=0
														,LPARAM param2=0
														,LPARAM param3=0)=0;
	virtual BOOL			ProcessAIInfo		(eAI_INFOTYPE type);

	virtual void			AINotice				(AIMSG* pMsg
                                          ,WORD    wSize
														,DWORD   dwDelay=0)=0;
	virtual void			AINoticeGroup		(AIMSG* pMsg			//除自身外的群员
                                          ,WORD    wSize
														,DWORD   dwDelay=0);
	virtual void			AINoticeAround		(AIMSG* pMsg			//除自身外的邻区伙伴
                                          ,WORD    wSize
														,DWORD   dwDelay=0)=0;

	virtual void		GroupCmdBeginAttack	( Character* pTargetChar )=0;
	virtual void		GroupCmdStopAttack	()=0;


	/////////////////////////////////////////////////////
	virtual BOOL			JoinGroup			( ObjectGroup* pGroup )=0;
	virtual BOOL			LeaveGroup			()=0;
	virtual BOOL			IsMemberOfGroup	()=0;
	virtual BOOL			IsLeaderOfGroup	()=0;
	virtual BOOL			IsFollowerOfGroup	()=0;
	virtual BOOL			IsLeaderAlive		()=0;


	/////////////////////////////////////////////////////
	// BattleRecord
	virtual BOOL			HasEnemy			() const=0;
	virtual VOID			RecordAttackPoint	( Character* pAttacker, int attackPoint )=0;
	virtual VOID			RecordTotalPoint	( Character* pAttacker, int TotalPoint )=0;
	virtual Character*	SearchTarget	()=0;


	/////////////////////////////////////////////////////
	virtual VOID			DistributeExp			()=0;

	virtual VOID			SetPlayerRefCounter	(int iCount ) =0;
	virtual BOOL			IsOutOfWanderRange	() const=0;
	virtual BOOL			IsOutOfRegenRange		( Vector3D &vTargetPos ) const =0;
	BOOL						GetNextMovePos			( Vector3D &vDestPos );


	/////////////////////////////////////////////////////
	template <class FINDER>
	Character*				SearchEnemy(FINDER& finder);

	template <class FINDER>
	Character*				SearchFriend(FINDER& finder);

protected:
	virtual BOOL			Init			();
	virtual VOID			Release		();
	virtual VOID			AIProgress	();
	virtual VOID			InitAIStates(DWORD dwStateID, LPARAM param1=NULL );
	virtual VOID			LinkAIStates(DWORD arStates[]);

	virtual VOID			InitDefaultSkill()=0;


protected:
	VG_PTR_GET_PROPERTY		(BaseInfo		, sNPCINFO_BASE);
	VG_INT2_PROPERTY			(TrackSlot		);
	VG_PTR_PROPERTY			(Group			, ObjectGroup);
	VG_PTR_GET_PROPERTY		(BattleRecord	, BattleRecord);
	VG_TYPE_PROPERTY			(RegenPos		, Vector3D);
	VG_PTR_PROPERTY			(RegenLocation	, RegenLocation);

	////////////////////////////////////////////////
	VG_TYPE_GET_PROPERTY		(SkillSelector	, RatioSelector);

	Character*					m_pTargetChar;
	SLOTCODE						m_SelectedSkill;
	WORD							m_wSelectedSkillDelay;

	////////////////////////////////////////////////
	AIStateManager*			m_pAIStateMan;
	AIMsgQueue*					m_pAIMsgQueue;
	AIMsgQueue*					m_pTmpMsgQueue;
	Timer*						m_pAggroTimer;
	Timer*						m_pSkillTimer;

	////////////////////////////////////////////////
	DWORD							m_dwHP;
	DWORD							m_dwMP;
};


#include "NPC.inl"




#endif //__NPC_H__
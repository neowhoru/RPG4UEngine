#pragma once

#include "CommonDefine.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API MoveStateHandle
{
public:
	MoveStateHandle();

public:
	virtual VOID	Init			( Character *pOwner, eMOVE_TYPE eMoveState );
	virtual float	GetMoveSpeed();
	virtual VOID	SetMoveState( eMOVE_TYPE eMoveState );

protected:
	Character*				m_pOwner;
	float						m_fBaseMoveSpeed;
	VG_TYPE_GET_PROPERTY	(MoveState,		eMOVE_TYPE);
};


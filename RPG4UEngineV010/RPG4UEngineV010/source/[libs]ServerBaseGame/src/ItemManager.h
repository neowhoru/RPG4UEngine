/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManager.h
创建日期：2009年6月4日
最后更新：2009年6月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMMANAGER_H__
#define __ITEMMANAGER_H__
#pragma once




#include <ItemManagerDefine.h>
#include <BitHandle.h>
#include <ResultCode.h>
#include <ConstSlot.h>
#include <ItemSlot.h>
#include <SkillSlot.h>
#include "DataPump.h"

using namespace RC;
class Player;
class BaseContainer;
class ItemSlotContainer;
class SlotManager;
struct sITEMMAKELEVEL;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API ItemManager : public DataPump
{	
public:
	ItemManager();
	virtual ~ItemManager();

public:
	virtual DATAPUMP_TYPE GetFactoryType(){return DATAPUMP_ITEM;}

public:
	///////////////////////////////////////////////////
	virtual VOID					Init		(Player* pPlayer,SlotManager * pMGR );
 	virtual VOID					Release	();

	///////////////////////////////////////////////////
	virtual BOOL					ValidPos			(SLOTIDX            atIndex
                                             ,SLOTPOS            atPos
															,BOOL bEmptyCheck = TRUE)=0;
	virtual BOOL					ValidState		()=0;
	virtual BOOL					ValidContainer	(SLOTIDX atIndex )=0;
	virtual BOOL					IsEmpty			(SLOTIDX Index
                                             ,SLOTPOS pos)	=0;

	///////////////////////////////////////////////////
	virtual eITEM_RESULT		Move			(SLOTIDX fromIndex
                                       ,SLOTPOS fromPos
													,SLOTIDX toIndex
													,SLOTPOS toPos);
	virtual eITEM_RESULT		Drop			(SLOTIDX atIndex
                                       ,SLOTPOS atPos)		=0;	
	virtual eITEM_RESULT		Pick			(DWORD                      dwItemObjectKey
                                       ,sTOTALINFO_INVENTORY * OUT pTotalInfo)		;
	virtual eITEM_RESULT		Pick			(SLOTPOS                    NumberOfItems
                                       ,ItemSlot &                 rItemSlot
													,sTOTALINFO_INVENTORY * OUT pTotalInfo)=0;

	virtual eITEM_RESULT		Repair		(SLOTIDX atIndex, SLOTPOS atPos );
	virtual eITEM_RESULT		RepairEquips( );
	virtual eITEM_RESULT		Combine		(SLOTIDX fromIndex
                                       ,SLOTPOS fromPos
													,SLOTIDX toIndex
													,SLOTPOS toPos);
	virtual eITEM_RESULT		Divide		()=0;
	virtual eITEM_RESULT		Use			(SLOTIDX atIndex, SLOTPOS atPos)=0;	
	virtual eITEM_RESULT		Sell			(SLOTIDX atIndex, SLOTPOS atPos )=0;
	virtual eITEM_RESULT		Buy			(DWORD                  dwShopListID
													,BYTE                   ShopTabIndex
													,BYTE                   ShopItemIndex
													,sTOTALINFO_INVENTORY * pTotalInfo)=0;

	virtual eITEM_RESULT		Lose			( SLOTCODE ItemCode, BOOL bAllOfItem )=0;
	virtual VOID				Lose			(BaseContainer * pContainer
                                       ,SLOTPOS         pos
													,BYTE            num
													,BOOL            bSend=FALSE)=0;
	virtual eITEM_RESULT		DropMoney	( MONEY Money )=0;


	////////////////////////////////////////////////////////
	virtual VOID				UpdateAttrByEquips	()=0;
	
	virtual VOID				SerializeItemStream(SLOTIDX             Index
                                              ,SLOTPOS             pos
															 ,ITEMOPT_STREAM * IN pStream
															 ,eSERIALIZE eType =  SERIALIZE_LOAD)=0;
	virtual VOID				SerializeItemStream(SLOTIDX            Index
                                              ,SLOTPOS            pos
															 ,ITEM_STREAM * IN   pStream
															 ,eSERIALIZE eType = SERIALIZE_LOAD)=0;

	////////////////////////////////////////
	virtual VOID				SkillExecute( WORD skillCode )=0;
	virtual BOOL				IsExistItem	( SLOTIDX Index, SLOTCODE code )=0;


	// 物品操作.......................


	/// @name 物品打造
	virtual eITEM_RESULT		MakeItem	(SLOTCODE					destItemCode
                                    ,DWORD						dwMaterialNums
												,BYTE							byMaterialRow
												,SLOTPOS*					parMaterialPos
												,LPCSTR						szItemName
												,sTOTALINFO_INVENTORY&	resultItems);


	virtual eITEM_RESULT		EnchantEx	(SLOTPOS    TargetPos
                                       ,BYTE       SuccessRateIndex
													,BYTE & OUT ResultEnchant);
	virtual eITEM_RESULT		Enchant		(SLOTPOS    TargetPos
                                       ,BYTE & OUT ResultEnchant)		;

	virtual eITEM_RESULT		RankUpEx		(SLOTPOS       TargetPos1
                                       ,SLOTPOS       TargetPos2
													,SLOTPOS & OUT RankUpItemPos
													,SLOTPOS & OUT DeletedItemPos
													,MONEY & OUT   RankUpFare
													,BOOL & OUT    bCreatedSocket);

	//////////////////////////////////////////////
	virtual eITEM_RESULT		FillSocket	(MATERIALTYPE Type
                                       ,SLOTPOS      SocketItemPos[]
													,SLOTPOS      TargetPos);
	virtual eITEM_RESULT		ExtractSocket	(MATERIALTYPE               Type
														,SLOTPOS                    TargetPos
														,DWORD							 dwSocketState
														,DWORD&							 dwSocketResult
														,sTOTALINFO_INVENTORY * OUT pTotalInfo);
	virtual eITEM_RESULT		AddSocket		( SLOTPOS TargetPos );


	//////////////////////////////////////////////
	virtual eITEM_RESULT		CreateAccessory(MATERIALTYPE               Type
														,SLOTPOS                    SocketItemPos
														,BYTE& 							 byResultPos
														,sTOTALINFO_INVENTORY * OUT pTotalInfo)=0;


	static BOOL			RankUpItem				(ItemSlot * pItemSlot );
	static BOOL			CreateItemSocket		(ItemSlot * pItemSlot
                                          ,BYTE       byCurRank);


	//////////////////////////////////////////
	virtual BOOL		IsExistItemDup	(SLOTIDX         idx
                                    ,SLOTCODE        code
												,BYTE            num
												,SLOTPOS * OUT   pPosArray
												,SLOTPOS & INOUT posNum);

	virtual BOOL		IsExistItem	(SLOTIDX         idx
											,SLOTCODE        code
											,DWORD	        dwCheckNum
											,SLOTPOS * OUT   pPosArray
											,DWORD * INOUT  pPosNum);



	virtual BOOL		RegenItem	(ItemSlot&			itemSlot
											,sITEMMAKELEVEL*	pMakeLevel
											,DWORD				dwRegenNum=0
											,DWORD				dwOppressNum=0);


	////////////////////////
	virtual eITEM_RESULT		ObtainDup(SLOTCODE                   ItemCode
                                    ,SLOTPOS                    num
												,sTOTALINFO_INVENTORY * OUT pTotalInfo)=0;
	virtual eITEM_RESULT		ObtainDup(ItemSlot & IN              slot
                                    ,SLOTPOS                    num
												,sTOTALINFO_INVENTORY * OUT pTotalInfo);

	////////////////////////////////////////////////
	virtual eITEM_RESULT		Obtain	(SLOTCODE                   ItemCode
												,SLOTPOS                    num
												,sTOTALINFO_INVENTORY * OUT pTotalInfo)=0;

	virtual eITEM_RESULT		Obtain	(ItemSlot & IN            slot
												,SLOTPOS                    num
												,sTOTALINFO_INVENTORY * OUT pTotalInfo);

	virtual VOID		Obtain(SLOTIDX            SlotIdx
                           ,SLOTCODE           ItemCode
									,SLOTPOS & OUT      PosInfoNum
									,sSLOT_POS * OUT    pPosInfo
									,BYTE & OUT         byCount
									,sITEM_SLOTEX * OUT pItemSlot)=0;

	virtual VOID		Obtain(SLOTIDX            SlotIdx
                           ,ItemSlot & IN      slot
									,SLOTPOS & OUT      PosInfoNum
									,sSLOT_POS * OUT    pPosInfo
									,BYTE & OUT         byCount
									,sITEM_SLOTEX * OUT pItemSlot);

	/////////////////////////////////////////////////////////////
	virtual BOOL				CheckInsertItem			(BaseContainer * pContainer
                                                   ,SLOTPOS & INOUT need_num)		=0;
	virtual BOOL				CheckInsertItemDup		(BaseContainer * pContainer
                                                   ,SLOTCODE        ItemCode
																	,const INT       MAX_DUP_NUM
																	,SLOTPOS & INOUT need_num
																	,SLOTPOS & OUT   PosInfoNum
																	,sSLOT_POS * OUT pPosInfo);
	virtual VOID				CanInsertOverlappedItem	(BaseContainer * pContainer
                                                   ,SLOTCODE        ItemCode
																	,SLOTPOS & INOUT need_num
																	,SLOTPOS * OUT   PosArray
																	,SLOTPOS & OUT   PosNum)=0;
	virtual VOID				CanInsertItem				(BaseContainer * pContainer
                                                   ,SLOTPOS         MAX_DUP_NUM
																	,SLOTPOS & INOUT need_num
																	,SLOTPOS * OUT   PosArray
																	,SLOTPOS & OUT   PosNum)=0;


	virtual BOOL		IsExistSerialCode	( DBSERIAL serial )			=0;
	virtual VOID		RegSerialCode		( SLOTIDX Index, DBSERIAL serial )=0;
	virtual VOID		UnregSerialCode	( DBSERIAL serial )			=0;


	////////////////////////////////////////////////
	virtual ItemSlotContainer *	GetItemSlotContainer( SLOTIDX Index )=0;


	////////////////////////////////////////////////
	template <class OPR>
	BOOL					ForeachSlot	( SLOTIDX idx, OPR & opr );
	template <class OPR>
	BOOL					Foreach		( SLOTIDX idx, OPR & opr );


	////////////////////////////////////////////////
	BOOL					IsEnableFunction( eITEM_FUNCTIONAL_TYPE type ) ;

public:
	BitHandle			m_FunctionalType;

private:

};



#include "ItemManager.inl"



#endif //__ITEMMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Character.h
创建日期：2009年6月1日
最后更新：2009年6月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTER_H__
#define __CHARACTER_H__
#pragma once


#include "Object.h"
#include <CharacterCookie.h>
#include "PacketStruct_Base.h"
#include "ObjectList.h"
#include <bitset>
#include "Attributes.h"
#include "ScriptDefine.h"
#include "ObjectFactoryManager.h"
#include "PartyData.h"

_NAMESPACEU(TileLand, tile);
_NAMESPACEU(PathHandler,	tile);
struct AIMSG;
class Status;
class Skill;
class Attributes;
class SummonedRelation;
class Zone;
class StatusManager;
class IActionStatus;
class Ability;
class SkillList;
class BaseZone;
struct sSTATE_INFO;
class MoveStateHandle;
enum	eAI_INFOTYPE;

using namespace RC;

enum { MAX_ENEMY_SLOT_SIZE = 12 };						


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API Character : public Object, public CharacterCookie
{
public :
	Character();
	virtual ~Character();

protected:
	virtual BOOL				Init		();
	virtual VOID				Release	();	

public :
	virtual VOID				Reuse()		{ Release(); }
	virtual VOID				Update( DWORD dwTick );

	virtual VOID				SetField		(Field* pField);
	virtual VOID				OnEnterField(Field* pField, Vector3D* pvPos);
	virtual VOID				OnLeaveField();

	virtual eZONE_STATE		GetGameZone	(BaseZone *&	OUT pZone
													,CODETYPE &		OUT EnterFieldCodeOut
													,IDTYPE *		OUT pAreaID = NULL)	;
	virtual VOID				SetGameZone	(eZONE_STATE   eState
                                       ,BaseZone *    pZone
													,CODETYPE      EnterFieldCode
													,IDTYPE			AreaID = 0)	;

public :
	/////////////////////////////////////////////////
	//脚本操作
	virtual BOOL					KeepQuestScript	(DWORD dwScriptType,DWORD dwQuestType);
	virtual void					SetQuestChar	(IQuestCharacter*);
	virtual IQuestCharacter*	GetQuestChar	();
	virtual void					SetScriptChar	(IScriptCharacter*);
	virtual IScriptCharacter*	GetScriptChar	();

	virtual void					SetScriptTargetID	( DWORD dwNpcID );
	virtual void					SetVar				( KEYWORD keyVar, int nVar, int oprType = KEYW_SET );
	virtual int						GetVar				( KEYWORD keyVar );
	virtual BOOL					RegisterVar			( KEYWORD keyVar );


public :
	//------------------------------------------------------------------------------------------------
	BOOL						CanMove		();
	virtual VOID			SetPos		( Vector3D* pvPos );
	virtual Vector3C&		GetPosition	( );
	virtual VOID			SetPosition	( Vector3C& );
	BOOL						SetPath		(Vector3D* pwvCur
                                    ,Vector3D* pvDest
												,int*      pTileRoute
												,int       iNumTileRoute);	
	BOOL						PathProgress	( float fMove);
	virtual BOOL			ProcessThrust	(BOOL      bForcedMove
                                       ,Vector3D* pvDestPos
													,Vector3D  &vThrustPos
													,float     fMoveDistance
													,BOOL      bDownAfterThrust=FALSE);
	virtual BOOL			IsMoving			();
	virtual VOID			StopMoving		(BOOL  bSendToPlayers=TRUE
                                       ,WORD* pwAngle=NULL)		;

	virtual VOID			SetActionDelay	( DWORD /*delay*/ )	{}
	BOOL						CanJumpAtFootPos();

	///////////////////////////////////////////////
	int						AllocEnemySlot	(Vector3D* pwvEnemyPos
                                       ,Vector3D  &wvResultPos
													,float     fAreaRadius);
	VOID						FreeEnemySlot	(int iSlot);
	VOID						ModifyPosition	(VECTOR3D &vCurPos
                                       ,VECTOR3D &vDestPos
													,VECTOR3D &vResultCurPos
													,VECTOR3D &vResultDestPos
													,SLOTCODE skillCode=0);

	///////////////////////////////////////////////
	float						GetDist	(Character* pTargetCharacter) const;
	float						GetDist	(const Vector3D& targetPos) const;
	float						GetDist2	(Character* pTargetCharacter) const;	
	float						GetDist2	(const Vector3D& targetPos) const;
	virtual VOID			GetPos	( VECTOR3D&  vOut ) const;
	Vector3D					GetRandPosAround( INT iDist );


	///////////////////////////////////////////////
	virtual eOBJECT_TYPE			GetCharacterType() const ;


	///////////////////////////////////////////////
	virtual Attributes&			GetAttr() = 0;
	virtual const Attributes&	GetAttr() const = 0;

	virtual VOID				SetExp	(DWORD exp)			;
	virtual VOID				SetLevel	(LEVELTYPE level)	;
	virtual VOID				SetName	(LPCTSTR szName)	;
	virtual LPCTSTR			GetName	()	;

	virtual LEVELTYPE			GetLevel			() const = 0;
	virtual LEVELTYPE			GetDisplayLevel() const		;
	virtual float				GetAttackRange	() const = 0;
	virtual eATTACK_KIND		GetAttackKind	(eATTACK_KIND defaultKind=ATTACKKIND_ALL_OPTION) const = 0;
	virtual float				GetSkillRangeBonus() const = 0;
	virtual float				GetSightRange		() const = 0;
	virtual eARMOR_TYPE		GetArmorType		() const = 0;
	virtual eMELEE_TYPE		GetMeleeType		() const = 0;
	virtual DWORD				GetHP					() const = 0;
	virtual DWORD				GetMP					() const = 0;
	virtual DWORD				GetMaxHP				() const = 0;
	virtual DWORD				GetMaxMP				() const = 0;
	virtual LPCTSTR			GetCharName			() const = 0;
	virtual int					GetRegenHP			() const		;
	virtual int					GetRegenMP			() const		;
	virtual BOOL				IsAlive				() const		;
	virtual BOOL				IsDead				() const		;
	virtual DWORD				GetPhysicalAvoidValue() const = 0;
	virtual float				GetPhysicalAttackSpeed() const = 0;
	virtual DWORD				GetExp				() const = 0;
	virtual SHORT				GetExperty1			()	const		;
	virtual SHORT				GetExperty2			()	const		;
	virtual int					GetMoveSpeedRatio	() const = 0;
	virtual int					GetAttSpeedRatio	() const = 0;
	virtual MONEY				GetMoney				()	const;

	virtual VOID				SetAttr		( eATTRIBUTE_TYPE eAttrType, ATTRIBUTE_KIND eAttrKind, int iValue ) = 0;
	virtual VOID				SetHP			( DWORD dwHP ) = 0;
	virtual VOID				SetMP			( DWORD dwMP ) = 0;

	///////////////////////////////////////////////
	virtual DWORD				IncreaseHP	( DWORD dwIncrement );
	virtual DWORD				DecreaseHP	( DWORD dwDecrement );
	virtual DWORD				IncreaseMP	( DWORD dwIncrement );
	virtual DWORD				DecreaseMP	( DWORD dwDecrement );
	virtual VOID				SetMoveState( eMOVE_TYPE state );
	virtual VOID				AddExp		( DWORD exp, DWORD dwTargetObjKey=0 ) ;
	virtual  VOID				SetShield	(int   iShieldHP
                                       ,int   iDecreaseMP
													,float fRatio)	;

	virtual VOID				UpdateLifeRecover	(BOOL bHPUpdate
                                             ,BOOL bMPUpdate)		;
	virtual VOID				SendAttrChanging	(eATTRIBUTE_TYPE attrType
                                             ,DWORD           dwValue)		;

	///////////////////////////////////////////////
	virtual BOOL				ProcessCondition			( BYTE byCondition, WORD wParam );
	virtual BOOL				_ProcessConditionLowerHP( WORD wPercent );
	virtual BOOL				_ProcessConditionLowerMP( WORD wPercent );
	virtual BOOL				_ProcessConditionSameHP	( WORD wPercent );
	virtual BOOL				_ProcessConditionSameMP	( WORD wPercent );


	///////////////////////////////////////////////
	virtual BOOL				SendPacket			(MSG_OBJECT_BASE * pMsg
                                             ,WORD              wSize
															,BOOL					 bCrypt =     FALSE);
	virtual BOOL				SendPackets			(DWORD dwNumberOfMessages
                                             ,BYTE** pMsg
															,WORD* pwSize);
	virtual eSEND_RESULT		SendAround			(MSG_OBJECT_BASE* pMsg
                                             ,WORD             wSize
															,BOOL					bSendToMe =  TRUE);
	virtual eSEND_RESULT		SendAround			(DWORD            dwNumberOfMessages
                                             ,BYTE**           pMsg
															,WORD*            pwSize
															,BOOL					bSendToMe = TRUE);


	/////////////////////////////////////////
	virtual VOID				AddObserver		(Character* pCharacter);
	virtual VOID				RemoveObserver	(DWORD		dwObjKey);
	virtual VOID				SendToObservers(AIMSG*		pMsg
                                          ,WORD			wSize)		;


	///////////////////////////////////////////////
	virtual VOID				AINotice			(AIMSG*		pMsg
                                          ,WORD			wSize
														,DWORD		dwDelay=0);

	/////////////////////////////////////////
	// Status
	virtual BOOL				ExistStatus		( eCHAR_STATE_TYPE eStateID );


	////////////////////////////////////////////////////////
	virtual eSKILL_RESULT	PrepareUseSkill		(SLOTCODE      skillCode
                                                ,BOOL				bCoolTimerReset = TRUE)		;
	virtual BOOL				CheckPassiveSkill		(SLOTCODE /*skillCode*/ )	;
	virtual BOOL				CheckClassDefine		(BYTE     /*byUserType*/
                                                ,CODETYPE /*dwClassDefine*/)		;
	virtual BOOL				CheckSkillRange		(Vector3D &vTargetPos
                                                ,float    fSkillRange)		;
	virtual BOOL				CheckSkillArea			(Vector3D &vTargetPos
                                                ,Vector3D &vMainTargetPos
																,BYTE     byRangeForm
																,float    fArea);

	////////////////////////////////////////////////////////
	virtual VOID				AddFightingEnergy		( SHORT sCount )	;
	virtual VOID				DelFightingEnergy		( SHORT sCount )	;
	virtual SHORT				GetFightingEnergy		()	const			;
	virtual VOID				ApplyMagicShield		( DWORD &dwDecreaseHP );
	virtual VOID				AddSkill					( Skill * pSkill );

	////////////////////////////////////////////////////////
	virtual BOOL				OnResurrection(float fRecoverExp
                                         ,float fRecoverHP
													  ,float fRecoverMP);
	virtual VOID				OnPunish			() {}	
	virtual BOOL				OnDead			();

	virtual BOOL				CanAttack		() const;
	virtual BOOL				CanBeAttacked	() const;
	virtual VOID				OnAttack			(Character* pTarget
                                          ,CODETYPE   skillcode
														,DAMAGETYPE damage);
	virtual VOID				OnAttacked		();		
	virtual DAMAGETYPE		OnDamaged		(Character*   pAttacker
                                          ,eATTACK_KIND attackType
														,DAMAGETYPE   damage);
	virtual BOOL				OnLifeRecover	(int recoverHP
                                          ,int recoverMP)		;
	virtual VOID				OnPeriodicRecover(eCHAR_STATE_TYPE eStateID
                                          ,int              recoverHP
														,int              recoverMP
														,int              iApplicationTime
														,int              iPeriodicTime);


	///////////////////////////////////////////////
	virtual VOID				SetTargetChar		( Character * /*pChar*/, BOOL bProcessObserver=TRUE ) ;
	virtual Character*		GetTargetChar		() ;
	virtual VOID				RecordAttackPoint	( Character* pAttacker, int attackPoint );
	virtual VOID				RecordTotalPoint	( Character* pAttacker, int TotalPoint );
	virtual BOOL				IsFriend				( CharacterCookie *pTarget ) ;
	virtual BOOL				IsSkillTargetType	(Character*         pTarget
                                             ,eSKILL_TARGET_TYPE eTargetType)		;

	///////////////////////////////////////////////
	virtual BOOL				ProcessAIInfo	(eAI_INFOTYPE /*type*/){return TRUE;}
	virtual VOID				ChangeAIState		(DWORD  dwStateID
															,LPARAM param1=0
															,LPARAM param2=0
															,LPARAM param3=0);


	virtual BOOL				CanDiscard		();
	virtual BOOL				OnForceDiscard	();

protected :
	/////////////////////////////////////////////////
	VG_TYPE_GET_PROPERTY	(Observers		,	ObjectList);

	VG_PTR_GET_PROPERTY2	(TileLand);
	VG_PTR_GET_PROPERTY2	(PathHandler);

	/////////////////////////////////////////////////
	VG_PTR_GET_PROPERTY2	(StatusManager);
	VG_PTR_GET_PROPERTY2	(MoveStateHandle);
	VG_DWORD_PROPERTY		(SummonerKey);


	VG_PTR_GET_PROPERTY	(GameZone		,BaseZone);
	VG_TYPE_GET_PROPERTY	(GameZoneState	,eZONE_STATE);
	VG_TYPE_GET_PROPERTY	(GameAreaID		,IDTYPE	 );
	VG_TYPE_GET_PROPERTY	(GameFieldCode	,CODETYPE );

	/////////////////////////////////////////////////
	int						m_EnemySlots[MAX_ENEMY_SLOT_SIZE];	
	VG_INT2_GET_PROPERTY	(ShieldHP);
	VG_INT2_GET_PROPERTY	(ShieldMP);
	VG_FLOAT_GET_PROPERTY(ShieldAbsorbRatio);

	VG_INT_GET_PROPERTY	(FightingEnergies);

	/////////////////////////////////////////////////
	VG_DWORD_GET_PROPERTY(DeadExp);
	SkillList *				m_pSkillList;

	/////////////////////////////////////////////////
	VG_TYPE_GET_PROPERTY	(KillerType, eOBJECT_TYPE);
	VG_DWORD_GET_PROPERTY(KillerObjectKey);

	VG_TYPE_GET_PROPERTY	(PartyData			,	PartyData);

	/////////////////////////////////////////////////

	IQuestCharacter*		m_pQuestChar	;
	IScriptCharacter*		m_pScriptChar	;

};

#include "Character.inl"









#endif //__CHARACTER_H__
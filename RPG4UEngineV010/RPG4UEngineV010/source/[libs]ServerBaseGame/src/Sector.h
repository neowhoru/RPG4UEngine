/*////////////////////////////////////////////////////////////////////////
文 件 名：Sector.h
创建日期：2009年6月4日
最后更新：2009年6月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SECTOR_H__
#define __SECTOR_H__
#pragma once


#include "FieldDefine.h"
#include <ResultCode.h>
#include <CriticalSection.h>
#include "PacketStruct_Base.h"
#include <THashTable.h>
#include "VariableMsg.h"

using namespace util;
using namespace std;
using namespace RC;

class Sector;
class Object;
class Character;
class Player;
class Item;
class NPC;
struct AIMSG;

typedef THashTable<Player *>				SectorPlayerTable;
typedef THashTable<Player *>::iterator	SectorPlayerTableIt;
typedef THashTable<NPC *>					SectorNpcTable;
typedef THashTable<NPC *>::iterator		SectorNpcTableIt;
typedef THashTable<Item *>					SectorItemTable;
typedef THashTable<Item *>::iterator	SectorItemTableIt;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API Sector  
{
public:
	Sector();
	virtual ~Sector();

public:
	DWORD					GetPlayerNum() ;

public:
	virtual DWORD		GetFactoryType(){ return SECTOR_TYPE_DEFAULT;}
	virtual void		Reuse	(){Release();}

public:
	//////////////////////////////////////////
	virtual VOID					Init		(DWORD dwSectorIndex, WORD wGroupIndex );
	virtual VOID					Release	();
	virtual VOID					Update	(DWORD dwTick )=0;
public:
	//////////////////////////////////////////
	Player*							GetPlayer(DWORD dwObjectKey);
	NPC*								GetNPC	(DWORD dwObjectKey);
	Item*								GetItem	(DWORD dwObjectKey);


public:
	//////////////////////////////////////////
	virtual eSECTOR_REULT		EnterObject		(Object * pObject
                                             ,BOOL     bEnterField)		;
	virtual eSECTOR_REULT		LeaveObject		(Object * pObject );

	//////////////////////////////////////////
	virtual eSEND_RESULT			SendEnterInfoAround	(Object * pObject
																	,BOOL     bVillage=FALSE)		=0;
	virtual eSEND_RESULT			SendEnterInfoAround2	(Object * pObject
																	,WORD     wAngle)		=0;
	virtual eSEND_RESULT			SendLeaveInfoAround	(Object * pObject )=0;


	//////////////////////////////////////////
	virtual VOID					SendEnterInfosTo	(Object* pObject
                                                ,BOOL    bVillage=FALSE)		;
	virtual VOID					SendLeaveInfosTo	(Object *pObject );


	//////////////////////////////////////////
	virtual eSEND_RESULT			SendToSector		(MSG_OBJECT_BASE * pMsg
                                                ,WORD              wSize)		=0;					
	virtual eSEND_RESULT			SendToSectorExcept(MSG_OBJECT_BASE * pMsg
                                                ,WORD              wSize
																,DWORD             dwMyObjKey=0)=0;	

	virtual eSEND_RESULT			SendToSector		(DWORD  dwNumberOfMessages
                                                ,BYTE** pMsg
																,WORD*  pwSize)=0;
	virtual eSEND_RESULT			SendToSectorExcept(DWORD  dwNumberOfMessages
                                                ,BYTE** pMsg
																,WORD*  pwSize
																,DWORD  dwMyObjKey=0)=0;


	//////////////////////////////////////////
	virtual Object*				FindObject	(eOBJECT_TYPE eObjecType
                                          ,DWORD        dwObjectKey)	=0	;

	virtual VOID					AINoticeToSectorExcept	(Object* pObject
                                                      ,AIMSG* pMsg
																		,WORD    wSize)=0;


	virtual BYTE					FindTargetsBy	(eSKILL_TARGET_TYPE eTargetType
                                             ,Character*         pAttacker
															,Character**        pTargetArray
															,Vector3D           &vMainTargetPos
															,float              fRange
															,unsigned int       uiRangeForm
															,float              fArea
															,BYTE               byCountLimit
															,DWORD              dwExceptTargetKey)=0;

	virtual BYTE					FindTargetsAround	(Character** pTargetArray
                                                ,Vector3D    &vMainPos
																,float       fRange
																,BYTE        byCountLimit)=0;
    
	///////////////////////////////////////////////////////////
	template<class OPR>	Character *	SearchPlayer	( OPR& opr );
	template<class OPR>	Character *	SearchNPC		( OPR& opr );
	template<class OPR>	BOOL			SearchFieldItem( OPR& opr );
	template<class OPR >	VOID			ForEachPlayer	( OPR & opr );
	template<class OPR >	VOID			ForeachItem		( OPR & opr );
	template<class OPR >	VOID			ForeachNPC		( OPR & opr );


	///////////////////////////////////////////////////////////
	virtual VOID	IncPlayerRefCounter	( Object *pObject );
	virtual VOID	DecPlayerRefCounter	( Object *pObject );

protected:
	///////////////////////////////////////////////////////////
	virtual VOID	OnSendPlayerEnter		(Player *pPlayer, BOOL bVillage=FALSE);
	virtual VOID	OnSendPlayerLeave		(Player *pPlayer)=0;	

	virtual VOID	OnSendFieldItemEnter	(Item *pItem)=0;			
	virtual VOID	OnSendFieldItemLeave	(Item *pItem)=0;		

	virtual VOID	OnSendAroundQuests	(NPC *pNPC);	
	virtual VOID	OnSendNpcEnter			(NPC *pNPC)=0;						
	virtual VOID	OnSendNpcEnter			(NPC *pNPC, WORD wAngle)=0;	
	virtual VOID	OnSendNpcLeave			(NPC *pNPC)=0;					

	virtual VOID	OnSendPlayersInfoTo	( Player *pPlayer, BOOL bVillage );
	virtual VOID	OnSendSummonedInfosTo( Player *pPlayer, NPC *pNearNPC )=0;


protected:
	int							m_nReferencePlayerNum;
	VG_DWORD_GET_PROPERTY	(GroupIndex	);
	VG_DWORD_GET_PROPERTY	(SectorIndex);

private:
	SectorPlayerTable *	m_pPlayers;
	SectorItemTable *		m_pFieldItems;
	SectorNpcTable *		m_pNpcs;

protected:
	static VariableMsg	ms_MsgBuffer;
};

#include "Sector.inl"

#endif //__SECTOR_H__
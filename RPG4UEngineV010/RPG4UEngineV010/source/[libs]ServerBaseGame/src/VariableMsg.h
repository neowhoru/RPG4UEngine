#pragma once

//=======================================================================================================================
#define MAX_MSG_AMOUNT			(512)
#define MAX_MSG_BUFFER_SIZE	(1024)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERBASEGAME_API VariableMsg
{
public:
	VariableMsg(void);
	~VariableMsg(void);

public:
	template< class PACKET >
	PACKET * StartPacket( Param2Class<PACKET> )	;

public:
	BYTE *	StartMsg		();
	BYTE *	StepNextMsg	();
	VOID		SetSize		(WORD wSize)	;
	

	BYTE **	GetMsgOutPtrArray	();
	WORD *	GetSizeArray		();

protected:
	BYTE*		m_ppMsgOut	[MAX_MSG_AMOUNT];
	BYTE		m_ppMsg		[MAX_MSG_AMOUNT][MAX_MSG_BUFFER_SIZE];
	WORD		m_arSizes	[MAX_MSG_AMOUNT];

	VG_BYTE_GET_PROPERTY	(MsgCount);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "VariableMsg.inl"
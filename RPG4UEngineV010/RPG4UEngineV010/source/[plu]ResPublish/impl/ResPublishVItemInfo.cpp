/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishVItemInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "ItemInfoParser.h"
#include "Archive.h"
#include "ConstArray.h"
#include "SkeletonDefine.h"
#include "TextResManager.h"
#include "GraphicResourceList.h"
#include "MeshResManager.h"
#include "SoundResourceList.h"
#include "EquipInfoParser.h"

using namespace skeleton;
using namespace info;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishVItemInfo::GetRefInfo(VRSTR /*sName*/)	
{
	assert(!"VItemInfo not support by  name getter\n");
	return NULL;
}

void*		ResPublishVItemInfo::GetRefInfo(CODETYPE code)
{
	return theItemInfoParser.GetVItemInfo(code);
}


BOOL		ResPublishVItemInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum 
	{
		 REF_DESC
		,REF_GRAP
		,REF_MESH
		,REF_EQUIP
		//,REF_SOUND
	};

	//$(DESC)	$(GRAP)	$(MESH)	$(EQIP)

	BaseInfoParser*	pParser;
	sVITEMINFO_BASE*	pVInfo;

	pVInfo = (sVITEMINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pVInfo);

	////////////////////////////////////////////
	//$(DESC)
	if(m_RefSettings[REF_DESC] && m_RefSettings[REF_DESC]->m_Parser)
	{
		sDESCINFO_BASE*	pRefInfo;
		pRefInfo = theTextResManager.GetDescInfo(pVInfo->m_DescID);
		if(pRefInfo)
		{
			pParser = m_RefSettings[REF_DESC]->m_Parser;
			pParser->CreateRecordBy(pVInfo->m_DescID,(void*)pRefInfo);
		}
	}

	////////////////////////////////////////////
	//$(GRAP)
	if(m_RefSettings[REF_GRAP] && m_RefSettings[REF_GRAP]->m_Parser)
	{
		RESOURCE_INFO* pRefInfo;
		pParser	= m_RefSettings[REF_GRAP]->m_Parser;
		//小图标
		pRefInfo = theGraphicResourceList.GetItemInfo(pVInfo->m_IconID);
		if(pRefInfo)
			pParser->CreateRecordBy(pVInfo->m_IconID,pRefInfo);

		//大图标
		pRefInfo = theGraphicResourceList.GetItemInfo(pVInfo->m_BigIconID);
		if(pRefInfo)
			pParser->CreateRecordBy(pVInfo->m_BigIconID,pRefInfo);
	}


	////////////////////////////////////////////
	//$(MESH)
	if(m_RefSettings[REF_MESH] && m_RefSettings[REF_MESH]->m_Parser)
	{
		sMESH_RECORD* pRefInfo;
		pParser	= m_RefSettings[REF_MESH]->m_Parser;
		//地上模型
		pRefInfo = theMeshResManager.GetRecord(pVInfo->m_ModelID);
		if(pRefInfo)
			pParser->CreateRecordBy(pVInfo->m_ModelID,pRefInfo);

		//装备辅助模型
		switch(pVInfo->m_dwType)
		{
		case VITEMTYPE_WEAPON:
			{
				//pRefInfo = theMeshResManager.GetRecord(pVInfo->WEAPON.m_ScabbardModelID);
				//if(pRefInfo)
				//	pParser->CreateRecordBy(pVInfo->WEAPON.m_ScabbardModelID,pRefInfo);
			}break;

		case VITEMTYPE_ARMOUR:
			{
			}break;
		}
	}

	////////////////////////////////////////////
	//$(REF_EQUIP)
	if(m_RefSettings[REF_EQUIP] && m_RefSettings[REF_EQUIP]->m_Parser)
	{
		sEQUIPINFO* pRefInfo;
		pParser	= m_RefSettings[REF_EQUIP]->m_Parser;

		//装备模型
		switch(pVInfo->m_dwType)
		{
		case VITEMTYPE_WEAPON:
			{
				pRefInfo = theEquipInfoParser.GetEquipInfo(pVInfo->WEAPON.m_RHModelID);
				if(pRefInfo)
					pParser->CreateRecordBy(pVInfo->WEAPON.m_RHModelID,pRefInfo);

				pRefInfo = theEquipInfoParser.GetEquipInfo(pVInfo->WEAPON.m_LHModelID);
				if(pRefInfo)
					pParser->CreateRecordBy(pVInfo->WEAPON.m_LHModelID,pRefInfo);

			}break;

		case VITEMTYPE_ARMOUR:
			{
				pRefInfo = theEquipInfoParser.GetEquipInfo(pVInfo->ARMOUR.m_ArmourModelID);
				if(pRefInfo)
					pParser->CreateRecordBy(pVInfo->ARMOUR.m_ArmourModelID,pRefInfo);
			}break;
		}

	}

	////////////////////////////////////////////
	//$(SOUD)
	//if(m_RefSettings[REF_SOUND] && m_RefSettings[REF_SOUND]->m_Parser)
	//{
	//	RESOURCE_INFO* pRefInfo;
	//	pParser	= m_RefSettings[REF_MESH]->m_Parser;

	//	//装备音效
	//	switch(pVInfo->m_dwType)
	//	{
	//	case VITEMTYPE_WEAPON:
	//		{
	//			pRefInfo = theSoundResourceList.GetItemInfo(pVInfo->WEAPON.m_byWeaponSound);
	//			if(pRefInfo)
	//				pParser->CreateRecordBy(pVInfo->WEAPON.m_byWeaponSound,pRefInfo);
	//		}break;

	//	}
	//}

	return TRUE;
}


BOOL		ResPublishVItemInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sVITEMINFO_BASE*	pInfo;

	pInfo = (sVITEMINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	file.WriteString("\t");

	file << pInfo->m_Code;			
	file << pInfo->m_sName;			
	file << pInfo->m_DescID;			
	//file << pInfo->m_dwCost;		
	//file << pInfo->m_byGrade;	

	file << GetVItemTypeLabel((eVITEMTYPE)pInfo->m_dwType);		

	file << pInfo->m_IconID;	
	file << pInfo->m_BigIconID;	
	file << pInfo->m_byShowMode;	
	file << pInfo->m_ModelID;		
	//file << pInfo->m_byRareRate;	
	file << pInfo->m_byShowEffect;
	//file << pInfo->m_byCanMove;	
	//file << pInfo->m_byCanDiscard;
	//file << pInfo->m_byCanDestroy;
	//file << pInfo->m_byCanTrade;	
	//file << pInfo->m_byCanBuySell;
	//file << pInfo->m_byCanStore;	
	//file << pInfo->m_byCanLock;	
	//file << pInfo->m_wWeight;		

	//以下依据Item类型分读
	switch(pInfo->m_dwType)
	{
	//case VITEMTYPE_RESTORE:
	case VITEMTYPE_WEAPON:
		file << pInfo->WEAPON.m_RHModelID;
		//file << GetEquipPosLabel(pInfo->WEAPON.m_byEquipPos);
		//file << pInfo->WEAPON.m_byWeaponSound;
		file << pInfo->WEAPON.m_byTwoHand;
		file << pInfo->WEAPON.m_LHModelID;		
		//file << pInfo->WEAPON.m_ScabbardModelID;

		break;

	case VITEMTYPE_ARMOUR:
		file << pInfo->ARMOUR.m_ArmourModelID;		
		//file << GetEquipPosLabel(pInfo->ARMOUR.m_byEquipPos);
		file << pInfo->ARMOUR.m_byArmourTexture;
		file << "\t";//.GetInt();
		file << "\t";//.GetInt();
		file << "\t";//.GetInt();
		file << pInfo->ARMOUR.m_byCape;
		file << pInfo->ARMOUR.m_byHeadWear;
		break;

	case VITEMTYPE_STONE:
		file << "\t\t\t\t\t\t";
		//file.GetInt();	// Weapon
		//file.GetString();
		//file.GetString();
		//file.GetInt();
		//file.GetInt();
		//file.GetInt();

		file << "\t\t";
		//file.GetInt();	// armour
		//file.GetInt();

		file << "\t\t";
		//file.GetInt();	//gem
		//file.GetInt();

		file << "\t\t";
		//file.GetInt();	//reel
		//file.GetInt();

		//file << pInfo->STONE.m_wStoneType;
		break;

	case VITEMTYPE_GEM:
	case VITEMTYPE_CARD:
	case VITEMTYPE_CRYSTAL:
		file << "\t\t\t\t\t\t";
		//file.GetInt();	// Weapon
		//file.GetString();
		//file.GetString();
		//file.GetInt();
		//file.GetInt();
		//file.GetInt();

		file << "\t\t";
		//file.GetInt();	// armour
		//file.GetInt();

		//gem
		//file << pInfo->GEM.m_byCanInlay;
		//file << pInfo->GEM.m_byCanEnhance;
		break;

	case VITEMTYPE_REEL:
		file << "\t\t\t\t\t\t";
		//file.GetInt();	// Weapon
		//file.GetString();
		//file.GetString();
		//file.GetInt();
		//file.GetInt();
		//file.GetInt();

		file << "\t\t";
		//file.GetInt();	// armour
		//file.GetInt();

		file << "\t\t";
		//file.GetInt();	//gem
		//file.GetInt();

		//reel
		//file << pInfo->REEL.m_StoneID;
		//file << pInfo->REEL.m_SkillID;

		//...
		break;

	//case VITEMTYPE_MATERIAL:
	//	break;

	}


	file.WriteString("\n");


	return TRUE;
}






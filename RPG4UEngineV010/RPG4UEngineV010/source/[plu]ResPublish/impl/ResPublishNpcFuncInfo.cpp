/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishNpcFuncInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "NpcInfoParser.h"
#include "Archive.h"
#include "MapInfoParser.h"
#include "ScriptResourceList.h"


using namespace info;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishNpcFuncInfo::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishNpcFuncInfo::GetRefInfo not support name getter\n");
	return NULL;
}

void*		ResPublishNpcFuncInfo::GetRefInfo(CODETYPE code)
{
	return theNPCInfoParser.GetExtraInfo(code);
}

BOOL		ResPublishNpcFuncInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	BaseInfoParser*	pParser;
	sNPCINFO_FUNC*		pInfo;

	enum
	{
		 REF_LAND
		,REF_FIELD
		,REF_NPC
		,REF_SCRIPT
	};

	pInfo = (sNPCINFO_FUNC*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	////////////////////////////////////////////////////
	//REF_LAND
	if(m_RefSettings[REF_LAND] && m_RefSettings[REF_LAND]->m_Parser)
	{
		sMAPINFO_BASE*		pRefInfo;
		pParser = m_RefSettings[REF_LAND]->m_Parser;

		pRefInfo = theMapInfoParser.GetMapInfo(pInfo->m_MapCode);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_MapCode,pRefInfo);
	}

	////////////////////////////////////////////////////
	//REF_FIELD
	if(m_RefSettings[REF_FIELD] && m_RefSettings[REF_FIELD]->m_Parser)
	{
		sFIELDINFO_BASE*		pRefInfo;
		pParser = m_RefSettings[REF_FIELD]->m_Parser;

		pRefInfo = theMapInfoParser.GetFieldInfo(pInfo->m_FieldID);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_FieldID,pRefInfo);
	}

	////////////////////////////////////////////////////
	//REF_NPC
	if(m_RefSettings[REF_NPC] && m_RefSettings[REF_NPC]->m_Parser)
	{
		sNPCINFO_BASE*		pRefInfo;
		pParser = m_RefSettings[REF_NPC]->m_Parser;

		pRefInfo = theNPCInfoParser.GetMonsterInfo(pInfo->m_NPCCODE);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_NPCCODE,pRefInfo);
	}

	////////////////////////////////////////////////////
	//REF_SCRIPT
	if(m_RefSettings[REF_SCRIPT] && m_RefSettings[REF_SCRIPT]->m_Parser)
	{
		SCRIPT_INFO*		pRefInfo;
		pParser = m_RefSettings[REF_SCRIPT]->m_Parser;

		//客户端脚本
		pRefInfo = theScriptResourceList.GetScriptInfo(pInfo->m_ClientScriptCode);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_ClientScriptCode,pRefInfo);

		//Server端脚本
		pRefInfo = theScriptResourceList.GetScriptInfo(pInfo->m_ServerScriptCode);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_ServerScriptCode,pRefInfo);
	}

	return TRUE;
}

BOOL		ResPublishNpcFuncInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sNPCINFO_FUNC*		pInfo;
	sNPCINFO_BASE*		pNpcInfo;
	VRSTR					sName;

	pInfo = (sNPCINFO_FUNC*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	pNpcInfo = theNPCInfoParser.GetMonsterInfo(pInfo->m_NPCCODE);
	if(pNpcInfo)
		sName = pNpcInfo->m_NpcName;
	else
		sName	= _V("");


	////////////////////////////////////////////////////////////////
	file <<  "";

	file <<  pInfo->m_ExtraCode;
	file <<  pInfo->m_Name;///注释列
	file <<  pInfo->m_MapCode   ;
	file <<  pInfo->m_FieldID	;
	file <<  pInfo->m_eNPCTYPE	;
	file <<  pInfo->m_NPCCODE	;

	file << pInfo->m_LocalNPC;
	file << pInfo->m_ClientScriptCode;
	file << pInfo->m_ServerScriptCode;

	file << pInfo->m_vPos.x;
	file << pInfo->m_vPos.y;
	file << pInfo->m_vPos.z;

	file << pInfo->m_nAngle;
	file << pInfo->m_dwMoveDelayMin;
	file << pInfo->m_dwMoveDelayMax;

	file <<pInfo->m_iMoveType;
	if(pInfo->m_iMoveType == eNPCMOVETYPE_PATROL)
	{
		file <<pInfo->m_nStepNum;
		for(INT n=0; n<pInfo->m_nStepNum; n++)
		{
			file <<pInfo->m_arMoveNodes[n].x;
			file <<pInfo->m_arMoveNodes[n].y;
			file <<pInfo->m_arMoveNodes[n].z;
		}
	}
	else if(pInfo->m_iMoveType == eNPCMOVETYPE_RANDOMMOVE)
	{
		file <<pInfo->m_fRange;
	}


	////////////////////////////////////////////////////////////////
	file <<  "\n";


	return TRUE;
}




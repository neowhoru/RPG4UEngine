/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishVSkillInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "NpcInfoParser.h"
#include "Archive.h"
#include "ConstArray.h"
#include "SkeletonDefine.h"
#include "TextResManager.h"
#include "GraphicResourceList.h"
#include "MeshResManager.h"
#include "SoundResourceList.h"
#include "EquipInfoParser.h"
#include "SkillInfoParser.h"
#include "EffectResourceList.h"
#include "StateInfoParser.h"

using namespace skeleton;
using namespace info;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishVSkillInfo::GetRefInfo(VRSTR /*sName*/)	
{
	assert(!"VSkillInfo not support by  name getter\n");
	return NULL;
}

void*		ResPublishVSkillInfo::GetRefInfo(CODETYPE code)
{
	return theSkillInfoParser.GetVSkillInfo((SLOTCODE)code);
}


BOOL		ResPublishVSkillInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum 
	{
		 REF_EFFECT
		,REF_STATE
	};

	BaseInfoParser*	pParser;
	sVSKILLINFO_BASE*	pVInfo;

	pVInfo = (sVSKILLINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pVInfo);


	////////////////////////////////////////////
	//sEFFECT_INFO
	if(m_RefSettings[REF_EFFECT] && m_RefSettings[REF_EFFECT]->m_Parser)
	{
		sEFFECT_INFO* pRefInfo;
		pParser	= m_RefSettings[REF_EFFECT]->m_Parser;
		//特效
		for(INT n=0; n<sVSKILLINFO_BASE::ATTACK_NUM; n++)
		{
			//吟唱特效
			pRefInfo = theEffectResourceList.GetEffectInfo(pVInfo->m_arIntonateEffect[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pVInfo->m_arIntonateEffect[n],pRefInfo);

			//攻击者特效
			pRefInfo = theEffectResourceList.GetEffectInfo(pVInfo->m_arAttackerEffect[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pVInfo->m_arAttackerEffect[n],pRefInfo);

			//技能特效
			pRefInfo = theEffectResourceList.GetEffectInfo(pVInfo->m_arSkillEffect[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pVInfo->m_arSkillEffect[n],pRefInfo);

			//目标特效
			pRefInfo = theEffectResourceList.GetEffectInfo(pVInfo->m_arTargetEffect[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pVInfo->m_arTargetEffect[n],pRefInfo);
		}
	}

	////////////////////////////////////////////
	//(REF_STATE)
	//if(m_RefSettings[REF_STATE] && m_RefSettings[REF_STATE]->m_Parser)
	//{
	//	sVSTATEINFO_BASE* pRefInfo;
	//	pParser	= m_RefSettings[REF_STATE]->m_Parser;

	//	////给自方附加状态编号
	//	//pRefInfo = theStateInfoParser.GetVStateInfo(pVInfo->m_ToSelfStatusID);
	//	//if(pRefInfo)
	//	//	pParser->CreateRecordBy(pVInfo->m_ToSelfStatusID,pRefInfo);

	//	////给目标附加状态编号
	//	//pRefInfo = theStateInfoParser.GetVStateInfo(pVInfo->m_ToDestStatusID);
	//	//if(pRefInfo)
	//	//	pParser->CreateRecordBy(pVInfo->m_ToDestStatusID,pRefInfo);

	//}

	return TRUE;
}


BOOL		ResPublishVSkillInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sVSKILLINFO_BASE*	pInfo;

	pInfo = (sVSKILLINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	file << "";
	/////////////////////////////////////////////////

	file << pInfo->m_Code;	
	file << pInfo->m_sName;	
	file << pInfo->m_SkillLV;
			file(sVSKILLINFO_BASE::ACTION_NUM);
	file << pInfo->m_arActions;
			file(0);

	//file << pInfo->m_byFirstActionRate;
	file << pInfo->m_dwActionTimeMin;	
	file << pInfo->m_dwActionTimeMax;	
	file << pInfo->m_dwPrepareTime;		
	file << pInfo->m_sIntonateAction;	

			file(sVSKILLINFO_BASE::ATTACK_NUM);
	file << pInfo->m_arIntonateEffect;
	file << pInfo->m_arAttackerEffect;
	file << pInfo->m_arSkillEffect;
	file << pInfo->m_arTargetEffect;
			file(0);

	//file << pInfo->m_sTargetHitAction;
	//file << pInfo->m_ToSelfStatusID;	
	//file << pInfo->m_ToSelfStatusLV;	
	//file << pInfo->m_ToDestStatusID;	
	//file << pInfo->m_ToDestStatusLV;	
	//file << pInfo->m_wStatusRate;		

	//file << pInfo->m_bySkillType;		
	//file << pInfo->m_wHoldTime;		
	//file << pInfo->m_FSkillRange;		
	file << pInfo->m_byHoldSkill;		
	file << pInfo->m_sHoldAction;		
	file << pInfo->m_HoldEffectID;	
	//file << pInfo->m_byNeedWeapon;	
	//file << pInfo->m_wNeedWeaponType;
	//file << pInfo->m_byLeftHand;		
	file << pInfo->m_byEmission;		
	file << pInfo->m_byAssault;		
	//file << pInfo->m_byHideStatus;	
	//file << pInfo->m_bySummon;			
	//file << pInfo->m_SummonMonster;	
	//file << pInfo->m_wEffectDuration;

	/////////////////////////////////////////////////
	file << "\n";


	return TRUE;
}






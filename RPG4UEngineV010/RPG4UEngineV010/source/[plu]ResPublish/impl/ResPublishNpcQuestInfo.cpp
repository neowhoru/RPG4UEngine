/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishNpcQuestInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "NpcInfoParser.h"
#include "Archive.h"
#include "QuestResourceList.h"
#include "QuestItemDropInfoParser.h"


using namespace quest;
using namespace info;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishNpcQuestInfo::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishNpcQuestInfo::GetRefInfo not support name getter\n");
	return NULL;
}

void*		ResPublishNpcQuestInfo::GetRefInfo(CODETYPE code)
{
	return theNPCInfoParser.GetQuestInfo(code);
}

BOOL		ResPublishNpcQuestInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	BaseInfoParser*	pParser(NULL);
	NPC_QUESTINFO*		pInfo;
	BaseInfoParser*	pParserDrop(NULL);

	enum
	{
		  REF_QUEST
		 ,REF_QUESTDROP
	};

	pInfo = (NPC_QUESTINFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	////////////////////////////////////////////////////
	//REF_QUEST
	if(m_RefSettings[REF_QUEST] && m_RefSettings[REF_QUEST]->m_Parser)
		pParser = m_RefSettings[REF_QUEST]->m_Parser;

	if(m_RefSettings[REF_QUESTDROP] && m_RefSettings[REF_QUESTDROP]->m_Parser)
		pParserDrop = m_RefSettings[REF_QUESTDROP]->m_Parser;


	if(pParser || pParserDrop)
	{
		QUEST_INFO*			pRefInfo;
		sQUEST_DROPGROUP*	pRefInfo2;

		for(UINT n=0; n<pInfo->m_QuestInfos.size();n++)
		{
			CODETYPE questID = pInfo->m_QuestInfos[n].m_QuestID;
			pRefInfo = theQuestResourceList.GetQuestInfo(questID);
			if(pRefInfo && pParser)
				pParser->CreateRecordBy(questID,pRefInfo);

			pRefInfo2 = theQuestItemDropInfoParser.GetQuestDropGroup(questID);
			if(pRefInfo2 && pParserDrop)
				pParserDrop->CreateRecordBy(questID,pRefInfo2);
		}
	}


	return TRUE;
}

BOOL		ResPublishNpcQuestInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	NPC_QUESTINFO*		pInfo;

	pInfo = (NPC_QUESTINFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	////////////////////////////////////////////////////////////////

	for(UINT n=0; n<pInfo->m_QuestInfos.size();n++)
	{

		file <<  "";

		sQUEST_RELATEINFO& questInfo = pInfo->m_QuestInfos[n];

		file << pInfo->m_Code;
		file << questInfo.m_QuestID;
		file << questInfo.m_State;
		file << questInfo.m_Entrance;
		file << questInfo.m_Desc;		///注释列

		file <<  "\n";
	}


	////////////////////////////////////////////////////////////////


	return TRUE;
}




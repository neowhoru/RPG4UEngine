/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishNpcInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "NpcInfoParser.h"
#include "Archive.h"
#include "AIInfoParser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishNpcInfo::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishNpcInfo::GetRefInfo not support name getter\n");
	return NULL;
	//return theNPCInfoParser.GetItemInfo((LPCSTR)sName);
}

void*		ResPublishNpcInfo::GetRefInfo(CODETYPE code)
{
	return theNPCInfoParser.GetMonsterInfo(code);
}

BOOL		ResPublishNpcInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum
	{
		  REF_VNPC
		 ,REF_TIPAI
	};
	//$(TEXT)

	BaseInfoParser*	pParser;
	sNPCINFO_BASE*		pInfo;

	pInfo = (sNPCINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	/////////////////////////////////////////////////
	if(m_RefSettings[REF_VNPC] && m_RefSettings[REF_VNPC]->m_Parser)
	{
		sVNPCINFO_BASE*		pVInfo;
		pVInfo = theNPCInfoParser.GetVMonsterInfo(pInfo->m_vMonsterCode);
		if(pVInfo)
		{
			pParser = m_RefSettings[REF_VNPC]->m_Parser;
			pParser->CreateRecordBy(pVInfo->m_vMonsterCode,pVInfo);
		}
	}

	/////////////////////////////////////////////////
	if(m_RefSettings[REF_TIPAI] && m_RefSettings[REF_TIPAI]->m_Parser)
	{
		sAI_INFO*		pRefInfo;
		pParser = m_RefSettings[REF_TIPAI]->m_Parser;

		pRefInfo = theAIInfoParser.GetAIInfo(pInfo->m_AIInfoCode);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_AIInfoCode,pRefInfo);
	}

	return TRUE;
}

BOOL		ResPublishNpcInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sNPCINFO_BASE*		pInfo;

	pInfo = (sNPCINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	//pVInfo = theNPCInfoParser.GetVMonsterInfo(pInfo->m_vMonsterCode);
	//if(pVInfo)
	//	sName = pVInfo->m_sMonsterName;
	//else
	//	sName	= _V("");


	////////////////////////////////////////////////////////////////
	file <<  "";

	file <<  pInfo->m_MonsterCode;

	file <<  pInfo->m_vMonsterCode;
	file <<  pInfo->m_NpcName;

	file << pInfo->m_Level;
	file << pInfo->m_DisplayLevel;
	file << pInfo->m_dwMaxHP;
	file << pInfo->m_dwMaxMP;
	file << pInfo->m_byGrade;
	file << pInfo->m_wSize;

	//file << pInfo->m_wAttProperty;
	file << pInfo->m_AIInfoCode;
	file << pInfo->m_AIInfoRate;
	file << pInfo->m_byCriticalRatio;

	file << pInfo->m_dwMinAttackPower;
	file << pInfo->m_dwMaxAttackPower;
	file << pInfo->m_dwPhyDef;
	file << pInfo->m_dwMagicDef;
	file << pInfo->m_wClass;

	file << pInfo->m_bySeries;

		   file(ELEMENTKIND_MAX);
	file << pInfo->m_wElementDef;
	file << pInfo->m_wElementAttack;
			file(0);

	file << pInfo->m_wPhyAttRate;
	file << pInfo->m_wPhyAttAvoid;
	file << pInfo->m_wAttType;
	file << pInfo->m_wArmorType;
	file << pInfo->m_fAttRange;
	file << pInfo->m_fViewRange;
	file << pInfo->m_fMoveRange;

	file << pInfo->m_wStandMin;
	file << pInfo->m_wStandMax;
	file << pInfo->m_fWalkSpeed;
	file << pInfo->m_fRunSpeed;

			file(ATTACK_SEQUENCE_MAX);
			//for(INT n=0;n<ATTACK_SEQUENCE_MAX;n++)
	file << pInfo->m_wAttSpeed;//[n];
			//for(INT n=0;n<ATTACK_SEQUENCE_MAX;n++)
	//file << pInfo->m_wProjectileCode;//[n];
			file(0);
	
	file << pInfo->m_bJump;

	file << TAG2STR(pInfo->m_SpawnAniCode.m_ID);//.m_Text;
	file << pInfo->m_dwSpawnTime;

	file << pInfo->m_wAttitude;
	file << pInfo->m_wMoveAttitude;
	file << TAG2STR(pInfo->m_dwMoveAreaID);

	//
	file << pInfo->m_wSkillUpdateTime;
	for( int i = 0; i < 2; ++i )
	{
		file << pInfo->m_SpecialCondition[i].byCondition;
		file << pInfo->m_SpecialCondition[i].wConditionParam;
		file << pInfo->m_SpecialCondition[i].wActionType;
		file << pInfo->m_SpecialCondition[i].wActionParam;
		file << pInfo->m_SpecialCondition[i].byActionRate;
	}

	//
	for( int i = 0; i < 2; ++i )
	{
		file << pInfo->m_ResistanceCondition[i].m_wStateCode;
		file << pInfo->m_ResistanceCondition[i].m_byRatio;
		file << pInfo->m_ResistanceCondition[i].m_EffectTag;
	}

	file << pInfo->m_byChangeTargetRatio;


	file << pInfo->m_byReviveCondition;
	file << pInfo->m_wReviveConditionParam;
	file << pInfo->m_byReviveRate;
	file << pInfo->m_wReviveCode;
	file << pInfo->m_wReviveDelay;

	file << pInfo->m_byHealCondition;
	file << pInfo->m_wHealConditionParam;
	file << pInfo->m_byHealRate;
	file << pInfo->m_wHealCode;
	file << pInfo->m_wHealDelay;

	file << pInfo->m_bySummonCondition;
	file << pInfo->m_wSummonConditionParam;
	file << pInfo->m_bySummonRate;
	file << pInfo->m_wSummonCode;
	file << pInfo->m_wSummonDelay;

	file << pInfo->m_bySkillUsePossible;
	for(int i = 0; i < 3; ++i )
	{
		file << pInfo->m_wSkillCode[i];
		file << pInfo->m_bySkillRate[i];
		file << pInfo->m_wSkillDelay[i];
	}



	//file << pInfo->m_wHateSkill;
	//file << pInfo->m_wRevengeSkill;
	//file << pInfo->m_byRevengeRate;

	file << pInfo->m_byItemDropRate;
	for( int i = 0; i < 3; ++i )
	{
		file << pInfo->m_ItemCode[i];
		file << pInfo->m_wDropItemRate[i];
	}




	////////////////////////////////////////////////////////////////
	file <<  "\n";


	return TRUE;
}




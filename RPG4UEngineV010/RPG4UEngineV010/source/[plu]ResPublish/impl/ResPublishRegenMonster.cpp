/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishRegenMonster.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "RegenParser.h"
#include "MonsterGroupParser.h"
#include "MapInfoParser.h"
#include "Archive.h"
#include "NPCInfoParser.h"


#define	RESINFO_TYPE			sREGEN_INFO
#define	GETREFINFO(CODE)		theRegenParser.GetRegenInfo(CODE)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishRegenMonster::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishRegenMonster::GetRefInfo not support name getter\n");
	return NULL;
	//return theRegenParser.GetItemInfo((LPCSTR)sName);
}

void*		ResPublishRegenMonster::GetRefInfo(CODETYPE code)
{
	return GETREFINFO(code);
}

BOOL		ResPublishRegenMonster::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	BaseInfoParser*		pParser;

	RESINFO_TYPE*			pInfo;

	//$(LAND)	$(FIED)	$(NPC_)	$(REGP)
	enum
	{
		 REF_LAND
		,REF_FIELD
		,REF_NPC
		,REF_REGP
	};

	///////////////////////////////////
	pInfo = (RESINFO_TYPE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	////////////////////////////////////////////////////
	//REF_LAND
	if(m_RefSettings[REF_LAND] && m_RefSettings[REF_LAND]->m_Parser)
	{
		sMAPINFO_BASE*		pRefInfo;
		pParser = m_RefSettings[REF_LAND]->m_Parser;

		pRefInfo = theMapInfoParser.GetMapInfo(pInfo->dwMapCode);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->dwMapCode,pRefInfo);
	}

	////////////////////////////////////////////////////
	//REF_FIELD
	if(m_RefSettings[REF_FIELD] && m_RefSettings[REF_FIELD]->m_Parser)
	{
		sFIELDINFO_BASE*		pRefInfo;
		pParser = m_RefSettings[REF_FIELD]->m_Parser;

		pRefInfo = theMapInfoParser.GetFieldInfo(pInfo->dwFieldIndex);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->dwFieldIndex,pRefInfo);
	}

	////////////////////////////////////////////////////
	//REF_NPC
	if(m_RefSettings[REF_NPC] && m_RefSettings[REF_NPC]->m_Parser)
	{
		sNPCINFO_BASE*		pRefInfo;
		pParser = m_RefSettings[REF_NPC]->m_Parser;

		for(INT n=0; n<MAX_REGEN_MONSTER_KIND; n++)
		{
			if(pInfo->dwMonCode[n] == 0 ||  pInfo->dwMonCode[n] == 0)
				continue;
			pRefInfo = theNPCInfoParser.GetMonsterInfo(pInfo->dwMonCode[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pInfo->dwMonCode[n],pRefInfo);
		}
	}


	////////////////////////////////////////////////////
	//REF_REGP
	if(m_RefSettings[REF_REGP] && m_RefSettings[REF_REGP]->m_Parser)
	{
		sMONSTERGROUP_INFO*		pRefInfo;
		pParser = m_RefSettings[REF_REGP]->m_Parser;

		for(INT n=0; n<MAX_GROUP; n++)
		{
			pRefInfo = theMonsterGroupParser.GetGroupInfo(pInfo->dwGroupID[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pInfo->dwGroupID[n],pRefInfo);
		}
	}



	return TRUE;
}

BOOL		ResPublishRegenMonster::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	RESINFO_TYPE*		pInfo;

	pInfo = (RESINFO_TYPE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	////////////////////////////////////////////////////////////////
	file <<  "";

	file <<  pInfo->dwID;

	file	<<  pInfo->szDesc;
	file	<<  pInfo->dwMapCode	;
	file	<<  pInfo->dwFieldIndex;
	file	<<  pInfo->nLandX;
	file	<<  pInfo->nLandY;

	file << pInfo->tagLocation;
	file << pInfo->vLocationPos.x;
	file << pInfo->vLocationPos.y;
	file << pInfo->vLocationPos.z;

	file	<<  pInfo->wMonsterType;
	file	<<  pInfo->fRegenRate;
	file	<<  pInfo->bRandomArea;

			file(3);
	file	<< pInfo->dwGroupID ;
			//file(0);
	
			file(MAX_REGEN_MONSTER_KIND);
	file	<< pInfo->dwMonCode
			<< pInfo->dwMaxNum;
			file(0);


	////////////////////////////////////////////////////////////////
	file <<  "\n";


	return TRUE;
}




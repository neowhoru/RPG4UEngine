/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishDropMonster.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "ItemDropParser.h"
#include "GroupItemDrop.h"
#include "MonsterItemDrop.h"
#include "Archive.h"
#include "ItemInfoParser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishDropMonster::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishDropMonster::GetRefInfo not support name getter\n");
	return NULL;
	//return theRegenParser.GetItemInfo((LPCSTR)sName);
}

void*		ResPublishDropMonster::GetRefInfo(CODETYPE code)
{
	return theMonsterItemDrop.GetDropInfo(code);
}

BOOL		ResPublishDropMonster::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;


	enum 
	{
		  REF_ITEM
		 ,REF_GROUP
	};

	BaseInfoParser*		pParser;
	sMONSTERDROP_INFO*	pInfo;

	pInfo = (sMONSTERDROP_INFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	//////////////////////////////////////////////
	if(m_RefSettings[REF_ITEM] && m_RefSettings[REF_ITEM]->m_Parser)
	{
		sITEMINFO_BASE*	pRefInfo;
		pParser	= m_RefSettings[REF_ITEM]->m_Parser;

		//商品
		for(UINT n=0;n<pInfo->arDropInfos.size();n++)
		{
			sITEMDROP_INFO&	item = pInfo->arDropInfos[n];
			pRefInfo = theItemInfoParser.GetItemInfo(item.dwItemCode);
			if(pRefInfo)
				pParser->CreateRecordBy(item.dwItemCode,pRefInfo);
		}
	}

	//////////////////////////////////////////////
	if(m_RefSettings[REF_GROUP] && m_RefSettings[REF_GROUP]->m_Parser)
	{
		sGROUPDROP_INFO*	pRefInfo;
		pParser	= m_RefSettings[REF_ITEM]->m_Parser;

		//商品
		for(UINT n=0;n<pInfo->arGroupIndices.size();n++)
		{
			sGROUPDROP_ARRAY&	group = pInfo->arGroupIndices[n];
			for(UINT m=0;m<MAX_DROPGROUP_COUNT;m++)
			{
				pRefInfo = theGroupItemDrop.GetDropInfo(group.arGroupInfos[m]);
				if(pRefInfo)
					pParser->CreateRecordBy(group.arGroupInfos[m],pRefInfo);
			}
		}
	}


	return TRUE;
}

BOOL		ResPublishDropMonster::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sMONSTERDROP_INFO*		pInfo;

	pInfo = (sMONSTERDROP_INFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	////////////////////////////////////////////////////////////////
		//	怪物	注释	物品	机率	最大机率	最小数量	最大数量	可重复	组机率	物品1组	物品2组	物品3组	物品4组	物品5组
		//10	怪物1掉落	7705	500	10000	1	2	0	4000	11		
		//		7706	300					2000	12		
		//			0					3000	21		
		//			0					500	13		
		//			0					500	14		
		//12	引用怪物1
	UINT	nMax	= math::Max(pInfo->arDropInfos.size(), pInfo->arGroupIndices.size());

	for(UINT n=0; n<nMax; n++)
	{
		file <<  "";

		if(n == 0)
		{
			file << pInfo->dwMonsterCode	;
			file << pInfo->sName	;
		}
		else
		{
			file <<  "";
			file <<  "";
		}

		////////////////////////////////////////////////////////////////
		if(n < pInfo->arDropInfos.size())
		{
			sITEMDROP_INFO&	item = pInfo->arDropInfos[n];

			file << item.dwItemCode	;
			file << item.nProbability	;
		}
		else
		{
			file <<  "";
			file <<  "";
		}

		////////////////////////////////////////////////////////////////
		if(n == 0)
		{
			file << pInfo->nMaxProbability;
			file << pInfo->nMinDropCount	;
			file << pInfo->nMaxDropCount	;
			file << pInfo->bRepeat			;
		}

		////////////////////////////////////////////////////////////////
		if(n < pInfo->arGroupIndices.size())
		{
			sGROUPDROP_ARRAY&	group = pInfo->arGroupIndices[n];
			file << group.nProbability	;
					file(MAX_DROPGROUP_COUNT);
			file << group.arGroupInfos	;
					file(0);
		}


		////////////////////////////////////////////////////////////////
		file <<  "\n";
	}


	return TRUE;
}




/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishItemInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "ItemInfoParser.h"
#include "Archive.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CODETYPE		ResPublishItemInfo::GetRefCode(VRSTR sName)	
{
	sITEMINFO_BASE* pInfo;
	pInfo = theItemInfoParser.GetItemInfo((LPCSTR)sName);
	if(pInfo)
		return pInfo->m_Code;
	return INVALID_CODETYPE;
}

void*		ResPublishItemInfo::GetRefInfo(VRSTR sName)	
{
	return theItemInfoParser.GetItemInfo((LPCSTR)sName);
}

void*		ResPublishItemInfo::GetRefInfo(CODETYPE code)
{
	return theItemInfoParser.GetItemInfo(code);
}

BOOL		ResPublishItemInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	BaseInfoParser*	pParser;
	sITEMINFO_BASE*		pInfo;
	sVITEMINFO_BASE*	pVInfo;

	if(m_RefSettings[0] && m_RefSettings[0]->m_Parser)
	{
		pInfo = (sITEMINFO_BASE*)pRecord->m_InfoDataPtr;
		__CHECK_PTR(pInfo);

		pVInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
		if(pVInfo)
		{
			pParser = m_RefSettings[0]->m_Parser;
			pParser->CreateRecordBy(pVInfo->m_Code,pVInfo);
		}
	}

	return TRUE;
}

BOOL		ResPublishItemInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sITEMINFO_BASE*		pInfo;
	//sVITEMINFO_BASE*	pVInfo;
	//VRSTR					sName;

	pInfo = (sITEMINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	//	10701	厚布衣	10701	101	1	10000	5	1	2	1	0	0	0	0	0	0	0	0	0	0	0	0	0	0	12	0	0	0	0	1	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	1	0	3	0	0	0	0	0	0	0	0	0	0	0	0	0	1	0	0	0	0	0	0	0

	file.WriteString("\t");

	file <<  pInfo->m_Code;

	file <<  pInfo->m_ItemName;
	file <<  pInfo->m_VItemCode;

	file <<  pInfo->m_wType;
	file <<  pInfo->m_LV;

	file <<  pInfo->m_Dura;
	file <<  pInfo->m_DropLV;
	file <<  pInfo->m_byIsDrop;
	file <<  pInfo->m_byMaterialType;

	file <<  pInfo->m_LimitEqLevel;
	file <<  pInfo->m_wLimitStr;
	file <<  pInfo->m_wLimitDex;
	file <<  pInfo->m_wLimitInt;
	file <<  pInfo->m_wLimitVit;
	file <<  pInfo->m_wLimitSkil1;
	file <<  pInfo->m_wLimitSkil2;

	file <<  pInfo->m_wMinDamage;
	file <<  pInfo->m_wMaxDamage;

	file <<  pInfo->m_wPhyAttMinDamgage;
	file <<  pInfo->m_wPhyAttMaxDamgage;
	file <<  pInfo->m_wRangeAttMinDamgage;
	file <<  pInfo->m_wRangeAttMaxDamgage;
	file <<  pInfo->m_wPhyAttRate;
	file <<  pInfo->m_wPhyAttSpeed;
	file <<  pInfo->m_wPhyDef;
	file <<  pInfo->m_wPhyAvoid;
	file <<  pInfo->m_wAttType;
	file <<  pInfo->m_wAttRange;
	file <<  pInfo->m_wDefType;

	file <<  pInfo->m_bySeries;
	file <<  pInfo->m_wMagicAttMinDamgage;
	file <<  pInfo->m_wMagicAttMaxDamgage;
	file <<  pInfo->m_wMagicAttSpeed;
	file <<  pInfo->m_wMagicDef;

			 file(ELEMENTKIND_MAX);
			//for(INT e=0;e<ELEMENTKIND_MAX;e++)
	file <<  pInfo->m_wElementDef;
	file <<  pInfo->m_wElementAttack;
			 file(0);

	file <<  pInfo->m_nMoveSpeed	;
	file <<  pInfo->m_wEquipClass;

	file <<  pInfo->m_wEqPos;
	file <<  pInfo->m_wMaxRank;
	file <<  pInfo->m_SocketNum;

	for(INT o=0; o<ITEM_OPTION_MAX; o++)
	{
		file <<  pInfo->m_arOptions[o].m_wOptionKind;
		file <<  pInfo->m_arOptions[o].m_wOption;									
	}
	//file <<  pInfo->m_wOptionKind1;
	//file <<  pInfo->m_wOption1;									
	//file <<  pInfo->m_wOptionKind2;
	//file <<  pInfo->m_wOption2;

	//file <<  pInfo->m_wMissile;
	//file <<  pInfo->m_bHeadType;

	//file <<  pInfo->m_wCustomize;


	file <<  pInfo->m_byDupNum;									
	file <<  pInfo->m_byWasteType;
	file <<  pInfo->m_wHealHP;
	file <<  pInfo->m_wHealHPTime;

	file <<  pInfo->m_wTimes;
	file <<  pInfo->m_dCoolTime;


	//file <<  pInfo->m_wSpecialEffect;
	//file <<  pInfo->m_MaterialCode;

	file.WriteString("\n");


	return TRUE;
}




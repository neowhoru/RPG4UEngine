/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishImpl.cpp
创建日期：2007年4月5日
最后更新：2007年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishImpl.h"
#include "FilePathUtil.h"
#include "FormatString.h"
#include "FileFinder.h"
#include "MeshResManager.h"
#include "PathInfoParser.h"
#include "MediaPathManager.h"
#include "TableTextFile.h"
#include "ResPublishManager.h"
#include "IMeshModel.h"
#include "IMeshTexture.h"
#include "MeshConfig.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ResPublishImpl, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
ResPublishImpl::ResPublishImpl()
{
	m_pPublish = NULL;
}
ResPublishImpl::~ResPublishImpl()
{
}


BOOL ResPublishImpl::PublishTexture		(LPCSTR szResFile)
{
	assert(m_pPublish);

	LPCSTR			szFile;
	LPCSTR			szDest;
	LPCSTR			szFilePath;
	static LPCSTR szExts[]=
	{
		 ".dds"
		,".ddk"
		,".png"	//缩略图
	};

	FilePathUtil filePath;
	filePath.Split(szResFile);

	for(INT n=0; n<_countof(szExts); n++)
	{
		if(stricmp(filePath.GetExt(), szExts[n]) != 0)
		{
			filePath.SetExt(szExts[n]);
		}
		szFile = filePath.ConcatFullPath();

		szFilePath	= _PATH_ROOT((LPCSTR)szFile);
		szDest		= FMSTR	("%s%s"
									,(LPCSTR)m_pPublish->GetDestPath()
									,szFile);

		if(filePath.IsExist(szFilePath))
		{
			OnPublishFile(szFilePath,szDest);
			//break;
		}
	}
	return TRUE;
}


BOOL ResPublishImpl::PublishNormalRes(LPCSTR szResPath)
{
	assert(m_pPublish);

	string			szDest;
	string			szFilePath;

	//szFilePath	= _PATH_ROOT((LPCSTR)szResPath);
	szFilePath	= FMSTR	("%s%s"
								,(LPCSTR)_MEDIAPATH(ePATH_ROOT)
								,szResPath);

	szDest		= FMSTR	("%s%s"
								,(LPCSTR)m_pPublish->GetDestPath()
								,szResPath);

	if(FilePathUtil::IsExist(szFilePath.c_str()))
	{
		OnPublishFile(szFilePath.c_str(),szDest.c_str());
	}
	return TRUE;
}

BOOL ResPublishImpl::PublishDir(LPCSTR szResPath,INT nExtNum,LPCSTR* ppExts)
{
	assert(m_pPublish);

	LPCSTR			szDest;
	LPCSTR			szFilePath;
	BOOL				res;
	//string			sFilter;
	LPCSTR			szRoot;
	INT				nRootLen;
	LPCSTR			szExt;				

	///遍历目录下所有文件
	FileFinder finder;

	szRoot		= _MEDIAPATH(ePATH_ROOT);
	nRootLen		= strlen(szRoot);
	szFilePath	= FilePathUtil::GetFileDir(szResPath);
	res			= finder.FindFile( FMSTR("%s*.*",szFilePath) );
	while( res )
	{
		res = finder.FindNextFile();
		if(finder.IsDots())
			continue;

		if(finder.IsDirectory())
			continue;

		szFilePath	= finder.GetFilePath();
		if(nExtNum)
		{
			szExt		= strrchr(szFilePath,'.');
			if(!szExt)
				continue;
			INT n;
			for(n=0; n<nExtNum; n++)
			{
				if(stricmp(szExt+1, ppExts[n]) == 0)
					break;
			}
			if(n == nExtNum)
				continue;
		}

		szDest		= FMSTR	("%s%s"
									,(LPCSTR)m_pPublish->GetDestPath()
									,szFilePath + nRootLen);

		OnPublishFile(szFilePath,szDest);
	}
	finder.Close();
	return TRUE;
}


BOOL ResPublishImpl::PublishMesh(LPCSTR szPath)
{
	BOOL						bTempMesh(FALSE);
	INT						nMeshID;
	IMeshModel*				pMesh		(NULL);
	IMeshTextureArray*		pTexArray;
	IMeshTexture*			pTexture;
	string					sPathDir;
	string					sFilePath;
	sMESH_RECORD*			pRecord;

	///检测Mesh是否存在
	nMeshID = theMeshResManager.GetMeshIDBy(szPath);

	if(nMeshID != -1)
		pMesh	= theMeshResManager.GetMeshModel(nMeshID);


	if(nMeshID == -1 || pMesh == NULL)
	{
		bTempMesh = TRUE;
		pRecord = theMeshResManager.CreateMesh(szPath);
		__CHECK_PTR(pRecord);
		nMeshID = pRecord->nID;
	}
	else
	{
		pRecord = theMeshResManager.GetRecord(nMeshID);
		__CHECK_PTR(pRecord);
	}


	//获取Mesh实例
	pMesh	= theMeshResManager.GetMeshModel(nMeshID);
	__CHECK_PTR(pMesh);


	pTexArray = pMesh->GetTextures();
	__CHECK_PTR(pTexArray);


	//1.纹理输出处理
	sPathDir = FilePathUtil::GetFileDir(szPath);

	for(INT n=0; n<pTexArray->GetTextureCount(); n++)
	{
		pTexture = pTexArray->GetTexture(n);
		if(!pTexture) continue;
		sFilePath = FMSTR("%s%s",sPathDir.c_str(), pTexture->GetName());
		PublishTexture(sFilePath.c_str());
	}

	//2.从cookie中获取纹理
	if(pRecord->res.pMeshConfig)
	{
		MeshConfigAvatarSetting*			pSetting;
		MeshConfigAvatarSettingArray&	settings = pRecord->res.pMeshConfig->m_AvatarSettingInfos;
		for( int i = 0; i < (int)settings.m_AvatarInfos.size(); i++ )
		{
			pSetting = settings.m_AvatarInfos[i];
			if(!pSetting)
				continue;
			for(UINT m=0; m< pSetting->m_ReplacableTextures.size();m++)
			{
				sREPLACABLE_TEXTURE& tex = pSetting->m_ReplacableTextures[m];
				sFilePath = FMSTR("%s%s",sPathDir.c_str(), tex.szFileName);
				PublishTexture(sFilePath.c_str());
			}
		}
	}


	//3.1.cookie配置文件输出
	sFilePath = FMSTR("%sCookie",szPath);
	PublishNormalRes(sFilePath.c_str());

	//3.2.meshinfo配置文件输出
	sFilePath = FMSTR("%sinfo",szPath);
	PublishNormalRes(sFilePath.c_str());

	//4.自身文件输出
	PublishNormalRes(szPath);


	///删除临时Mesh
	if(bTempMesh)
	{
		theMeshResManager.DestroyMesh(nMeshID);
	}

	return TRUE;
}


void ResPublishImpl::OnPublishReady	(DWORD dwAmount)
{
	__UNUSED(dwAmount);
}

void ResPublishImpl::OnPublishStep		(DWORD dwStep)	
{
	__UNUSED(dwStep);
}

void ResPublishImpl::OnPublishFinished()
{
}
void ResPublishImpl::OnPublishFile(LPCSTR szResFile, LPCSTR szResTo)
{
	FilePathUtil::MakeDirectory(FilePathUtil::GetFileDir(szResTo));
	::CopyFile(szResFile,szResTo,TRUE);
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishHorseInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "HorseInfoParser.h"
#include "Archive.h"
#include "MeshResManager.h"
#include "EffectResourceList.h"
#include "StateInfoParser.h"


using namespace info;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishHorseInfo::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishHorseInfo::GetRefInfo not support name getter\n");
	return NULL;
	//return theNPCInfoParser.GetItemInfo((LPCSTR)sName);
}

void*		ResPublishHorseInfo::GetRefInfo(CODETYPE code)
{
	return theHorseInfoParser.GetHorseInfo(code);
}

BOOL		ResPublishHorseInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum 
	{
		 REF_MESH
		,REF_EFFECT
		,REF_STATE
	};

	BaseInfoParser*	pParser;
	sHORSE_INFO*		pInfo;

	pInfo = (sHORSE_INFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	//////////////////////////////////////////////
	if(m_RefSettings[REF_EFFECT] && m_RefSettings[REF_EFFECT]->m_Parser)
	{
		sEFFECT_INFO* pRefInfo;
		pParser	= m_RefSettings[REF_EFFECT]->m_Parser;
		//特效
		pRefInfo = theEffectResourceList.GetEffectInfo(pInfo->m_MountEffect);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_MountEffect,pRefInfo);

		//特效
		pRefInfo = theEffectResourceList.GetEffectInfo(pInfo->m_DismountEffect);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_DismountEffect,pRefInfo);
	}

	//////////////////////////////////////////////
	if(m_RefSettings[REF_MESH] && m_RefSettings[REF_MESH]->m_Parser)
	{
		sMESH_RECORD* pRefInfo;
		pParser	= m_RefSettings[REF_MESH]->m_Parser;
		//模型
		pRefInfo = theMeshResManager.GetRecord(pInfo->m_ModelID);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_ModelID,pRefInfo);
	}

	//////////////////////////////////////////////
	if(m_RefSettings[REF_STATE] && m_RefSettings[REF_STATE]->m_Parser)
	{
		sSTATEINFO_BASE* pRefInfo;
		pParser	= m_RefSettings[REF_STATE]->m_Parser;
		//状态
		pRefInfo = theStateInfoParser.GetStateInfo(pInfo->m_StateID);
		if(pRefInfo)
			pParser->CreateRecordBy(pInfo->m_StateID,pRefInfo);
	}

	return TRUE;
}


BOOL		ResPublishHorseInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sHORSE_INFO*		pInfo;

	pInfo = (sHORSE_INFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	////////////////////////////////////////////////////////////////
	file <<  "";

	file << pInfo->m_Code;
	file << pInfo->m_Name;
	file << pInfo->m_ModelID;	///< 
	file << pInfo->m_Type;
	file << pInfo->m_Aility;
	file << pInfo->m_FlyHeight;
	file << pInfo->m_Inertia;
	file << pInfo->m_Speed;
	file << pInfo->m_RunDist;
	file << pInfo->m_WalkDist;
	file << pInfo->m_JumpHeight;
	file << GetHorseMoveTypeLabel((eHORSE_MOVETYPE)pInfo->m_MoveType);

	file << pInfo->m_Acceleration;
	file << pInfo->m_AccelerationTime;
	file << pInfo->m_UseTime;
	file << pInfo->m_Skin;
	file << pInfo->m_StateID;
	file << pInfo->m_MountEffect;
	file << pInfo->m_DismountEffect; 


	////////////////////////////////////////////////////////////////
	file <<  "\n";


	return TRUE;
}




/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishVNpcInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "NpcInfoParser.h"
#include "Archive.h"
#include "ConstArray.h"
#include "SkeletonDefine.h"
#include "TextResManager.h"
#include "GraphicResourceList.h"
#include "MeshResManager.h"
#include "SoundResourceList.h"
#include "EquipInfoParser.h"
#include "SkillInfoParser.h"
#include "CharSoundInfoParser.h"

using namespace skeleton;
using namespace info;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishVNpcInfo::GetRefInfo(VRSTR /*sName*/)	
{
	assert(!"VNpcInfo not support by  name getter\n");
	return NULL;
}

void*		ResPublishVNpcInfo::GetRefInfo(CODETYPE code)
{
	return theNPCInfoParser.GetVMonsterInfo(code);
}


BOOL		ResPublishVNpcInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum 
	{
		 REF_MESH
		,REF_SKILL
		,REF_SOUNDCHAR
	};

	BaseInfoParser*	pParser;
	sVNPCINFO_BASE*		pVInfo;

	pVInfo = (sVNPCINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pVInfo);


	////////////////////////////////////////////
	//$(MESH)
	if(m_RefSettings[REF_MESH] && m_RefSettings[REF_MESH]->m_Parser)
	{
		sMESH_RECORD* pRefInfo;
		pParser	= m_RefSettings[REF_MESH]->m_Parser;
		//模型
		pRefInfo = theMeshResManager.GetRecord(pVInfo->m_resModelID);
		if(pRefInfo)
			pParser->CreateRecordBy(pVInfo->m_resModelID,pRefInfo);
	}

	////////////////////////////////////////////
	//(REF_SKILL)
	if(m_RefSettings[REF_SKILL] && m_RefSettings[REF_SKILL]->m_Parser)
	{
		sVSKILLINFO_BASE* pRefInfo;
		pParser	= m_RefSettings[REF_SKILL]->m_Parser;

		//怪物普通技能
		for(INT n=0; n<ATTACK_SEQUENCE_MAX; n++)
		{
			pRefInfo = theSkillInfoParser.GetVSkillInfo((SLOTCODE)pVInfo->m_SkillCode[n]);
			if(pRefInfo)
				pParser->CreateRecordBy(pVInfo->m_SkillCode[n],pRefInfo);
		}
	}

	////////////////////////////////////////////
	//(REF_SOUNDCHAR)
	if(m_RefSettings[REF_SOUNDCHAR] && m_RefSettings[REF_SOUNDCHAR]->m_Parser)
	{
		sCHAR_SOUND* pRefInfo;
		pParser	= m_RefSettings[REF_SOUNDCHAR]->m_Parser;

		//怪物普通技能
		pRefInfo = theCharSoundInfoParser.GetCharSoundInfo(pVInfo->m_wSoundTempl);
		if(pRefInfo)
			pParser->CreateRecordBy(pVInfo->m_wSoundTempl,pRefInfo);
	}

	//////////////////////////////////////////
	//Script
	//if(m_RefSettings[REF_SCRIPT] && m_RefSettings[REF_SCRIPT]->m_Parser)
	//{
	//	RESOURCE_INFO* pRefInfo;
	//	pParser	= m_RefSettings[REF_SCRIPT]->m_Parser;

	//	//装备音效
	//	pRefInfo = theScriptManager.GetScript(pVInfo->m_sDeathScript);
	//	if(pRefInfo)
	//		pParser->CreateRecordBy(pVInfo->m_sDeathScript,pRefInfo);
	//}

	return TRUE;
}


BOOL		ResPublishVNpcInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sVNPCINFO_BASE*	pInfo;

	pInfo = (sVNPCINFO_BASE*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	file << "";
	/////////////////////////////////////////////////

	file << pInfo->m_vMonsterCode ;
	file << pInfo->m_sMonsterName;
	file << pInfo->m_resModelID;
	//file << pInfo->m_MonsterGrade;	//eNPC_GRADE
	file << pInfo->m_wSoundTempl;
	file << pInfo->m_MonsterType;	//见 eNPC_GRADE
	//file << pInfo->m_fMoveSpeed;	

			file(ATTACK_SEQUENCE_MAX);
	file << pInfo->m_SkillCode;
	//file << pInfo->m_SkillLevel;
			file(0);

	file << pInfo->m_fBodySize;	
	file << pInfo->m_sSkin;
	file << pInfo->m_fAlpha;		
				file(ELEMENTKIND_MAX);
	file << pInfo->m_ElementSizes	;
	file << pInfo->m_ElementSkins	;
	file << pInfo->m_ElementAlphas;		
				file(0);
	//file << pInfo->m_sDeathScript;
	//file << pInfo->m_sAIScript;	
	file << pInfo->m_byWeaponSound;
	file << pInfo->m_byArmourTexture;
	//file << pInfo->m_fRun;			
	//file << pInfo->m_fWalk;			
	file << pInfo->m_nFootNum;		
	file << pInfo->m_dwIdleSoundPeriod;		
	file << pInfo->m_byPlayBattleVoiceRate;		
	file << pInfo->m_byPlayIdleVoiceRate;	
	file << pInfo->m_byPlayHurtVoiceRate;	
	file << pInfo->m_byPlayDieVoiceRate;	

	/////////////////////////////////////////////////
	file << "\n";


	return TRUE;
}






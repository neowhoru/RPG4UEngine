/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishStyleInfo.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "SkillInfoParser.h"
#include "ItemInfoParser.h"
#include "StateInfoParser.h"
#include "Archive.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CODETYPE		ResPublishStyleInfo::GetRefCode(VRSTR sName)	
{
	sSKILLINFO_COMMON* pInfo;
	pInfo = theSkillInfoParser.GetInfo((LPCSTR)sName);
	if(pInfo)
		return pInfo->m_SkillCode;
	return INVALID_CODETYPE;
}

void*		ResPublishStyleInfo::GetRefInfo(VRSTR sName)	
{
	return theSkillInfoParser.GetInfo((LPCSTR)sName);
}

void*		ResPublishStyleInfo::GetRefInfo(CODETYPE code)
{
	return theSkillInfoParser.GetInfo((SLOTCODE)code);
}

BOOL		ResPublishStyleInfo::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum
	{
		 REF_VSKILL
		,REF_VITEM
		//,REF_STATE
	};

	BaseInfoParser*	pParser;
	sSTYLEINFO_BASE*	pInfo;

	pInfo = (sSTYLEINFO_BASE*)(sSKILLINFO_COMMON*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);

	/////////////////////////////////////////////////
	//REF_VSKILL
	if(m_RefSettings[REF_VSKILL] && m_RefSettings[REF_VSKILL]->m_Parser)
	{
		sVSKILLINFO_BASE*	pRefInfo;
		pRefInfo = theSkillInfoParser.GetVSkillInfo((SLOTCODE)pInfo->m_VSkillCode);
		if(pRefInfo)
		{
			pParser = m_RefSettings[REF_VSKILL]->m_Parser;
			pParser->CreateRecordBy(pInfo->m_VSkillCode,pRefInfo);
		}
	}

	/////////////////////////////////////////////////
	//REF_VITEM
	if(m_RefSettings[REF_VITEM] && m_RefSettings[REF_VITEM]->m_Parser)
	{
		sVITEMINFO_BASE*	pRefInfo;
		pRefInfo = theItemInfoParser.GetVItemInfo(pInfo->m_VItemCode);
		if(pRefInfo)
		{
			pParser = m_RefSettings[REF_VITEM]->m_Parser;
			pParser->CreateRecordBy(pInfo->m_VItemCode,pRefInfo);
		}
	}


	///////////////////////////////////////////////////
	////REF_STATE
	//if(m_RefSettings[REF_STATE] && m_RefSettings[REF_STATE]->m_Parser)
	//{
	//	sABILITYINFO_BASE* pAbility;
	//	sSTATEINFO_BASE*	pRefInfo;
	//	pParser	= m_RefSettings[REF_STATE]->m_Parser;

	//	for(INT n=0; n<pInfo->GetAbilityNum(); n++)
	//	{
	//		pAbility	= pInfo->GetAbilityInfoAt(n);
	//		if(!pAbility)
	//			continue;
	//		pRefInfo = theStateInfoParser.GetStateInfo(pAbility->m_wStateID);
	//		if(pRefInfo)
	//			pParser->CreateRecordBy(pAbility->m_wStateID,pRefInfo);
	//	}
	//}

	return TRUE;
}

BOOL		ResPublishStyleInfo::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sSTYLEINFO_BASE	*	pInfoData;
	sSTYLEINFO_BASE	*	pInfo;
	INT					nLVMax;
	INT					nBaseID;

	pInfoData = (sSTYLEINFO_BASE*)(sSKILLINFO_COMMON*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfoData);

	if(pInfoData->m_wSkillLV == 0)
		nBaseID	= pInfoData->m_SkillCode;
	else
		nBaseID	= pInfoData->m_SkillCode - pInfoData->m_wSkillLV + 1;

	if(pInfoData->m_wOverLV)
		nLVMax	= pInfoData->m_wOverLV;
	else
		nLVMax	= 1;

	/// 同时输出技能所有其它等级	
	for(INT n=0; n<nLVMax; n++)
	{
		pInfo = theSkillInfoParser.GetStyleInfo((SLOTCODE)(nBaseID + n));
		if(!pInfo)continue;

		////////////////////////////////////////////////////////////////
		file <<  "";

		file <<  pInfo->m_SkillCode;

		file << pInfo->m_SkillClassCode;
		file	<< pInfo->m_SkillName;
		file << pInfo->m_VSkillCode;
		file << pInfo->m_VItemCode;

		//file << pInfo->m_wSkillattribute;

		file << pInfo->m_wRequireLV;
		file << pInfo->m_wSkillLV;
		file << pInfo->m_wMaxLV;
		file << pInfo->m_wOverLV;

				//file(sSTYLEINFO_BASE::OVERSTAT_NUM);
		//file << pInfo->m_wOverStatclass	;
		//file << pInfo->m_wOverstat;
		//		file(0);

		file << pInfo->m_byRequireSkillPoint		;
		file << pInfo->m_dwClassDefine			;
		file << pInfo->m_WeaponDefines			;
		//file << pInfo->m_byAttType				;
		file << pInfo->m_fAttRange				;
		//file << pInfo->m_byMoveAttack			;
		//file << pInfo->m_byStyleCheck			;



				file(ATTACK_SEQUENCE_MAX);
		file << pInfo->m_arAttackActions;
		file << pInfo->m_dwAttackTime;
		file << pInfo->m_iAddDamage;
		file << pInfo->m_fDamagePercent;
				file(0);

		file << pInfo->m_byAttRangeform			;
		file << pInfo->m_fStyleArea				;
		file << pInfo->m_fThirdDelay				;

		file << pInfo->m_iAttackRate				;
		file << pInfo->m_iAvoidRate				;
		file << pInfo->m_iAttackSpeed			;
		file << pInfo->m_iBonusDefence			;
		file << pInfo->m_wCriticalBonus			;
		file << pInfo->m_fDefenseIgnore			;

		file << pInfo->m_fPierceRate				;
		file << pInfo->m_fPierceRange			;
		file << pInfo->m_fStunRate				;
		file << pInfo->m_wStunTime				;
		file << pInfo->m_fKnockBackRate			;
		file << pInfo->m_fKnockBackRange		;
		file << pInfo->m_fDownRate				;
		//file << pInfo->m_fDelayReduce			;
		//file << pInfo->m_fDelayOccur				;

		file << pInfo->m_wHPAbsorb				;
		file << pInfo->m_fHPAbsorbPer			;
		file << pInfo->m_wMPAbsorb				;
		file << pInfo->m_fMPAbsorbPer			;
		//file << pInfo->m_wMaxTargetNo			;


		////////////////////////////////////////////////////////////////
		file <<  "\n";
	}

	return TRUE;
}




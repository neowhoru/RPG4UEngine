/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishTipAI.cpp
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ResPublishResDeclare.h"
#include "TextResManager.h"
#include "SoundResourceList.h"
#include "AIInfoParser.h"
#include "Archive.h"


using namespace info;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void*		ResPublishTipAI::GetRefInfo(VRSTR /*sName*/)	
{
	LOGINFO("ResPublishTipAI::GetRefInfo not support name getter\n");
	return NULL;
}

void*		ResPublishTipAI::GetRefInfo(CODETYPE code)
{
	return theAIInfoParser.GetInfo(code);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL		ResPublishTipAI::OnScanRef(sPUBLISH_RECORD* pRecord)
{
	if(!pRecord)
		return TRUE;

	enum
	{
		 REF_TEXT
	};
	//$(TEXT)


	BaseInfoParser*		pParser;
	sAI_INFO*				pInfo;

	pInfo = (sAI_INFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);


	//////////////////////////////////
#define		CREATE_TIPAI(NAME)\
			pRefInfo = theTextResManager.GetTextInfo(unit.NAME[n]);\
			if(pRefInfo)\
				pParser->CreateRecordBy(unit.NAME[n],(void*)pRefInfo)


	/////////////////////////////////////////////////
	//REF_TEXT
	if(m_RefSettings[REF_TEXT] && m_RefSettings[REF_TEXT]->m_Parser)
	{
		sTEXTINFO_BASE*	pRefInfo;
		pParser = m_RefSettings[REF_TEXT]->m_Parser;

		for(UINT m=0; m<MAX_AI_TYPE; m++)
		{
			sAI_INFO_UNIT&	unit = pInfo->m_Units[m];
			for(UINT n=0; n<MAX_AI_STATE; n++)
			{
				///////////////////////////////////
				CREATE_TIPAI(m_Wanders	);
				CREATE_TIPAI(m_Idles		);
				CREATE_TIPAI(m_Backs		);
				CREATE_TIPAI(m_Patrols	);
				CREATE_TIPAI(m_Searchs	);

				CREATE_TIPAI(m_Passives	);
				CREATE_TIPAI(m_Nearests	);
				CREATE_TIPAI(m_LowHPs	);
				CREATE_TIPAI(m_LowLVs	);
				CREATE_TIPAI(m_HighMPs	);
				CREATE_TIPAI(m_Singles	);
			}
		}
	}


	return TRUE;
}


BOOL		ResPublishTipAI::OnPublishInfo(Archive& file,sPUBLISH_RECORD* pRecord)
{
	//头注释
	if(pRecord == NULL)
		return _SUPER::OnPublishInfo(file,pRecord);

	sAI_INFO	*	pInfo;

	pInfo = (sAI_INFO*)pRecord->m_InfoDataPtr;
	__CHECK_PTR(pInfo);



	//	序号	AI编号	注释		状态编号	状态		机率	静止型1	静止型2	静止型3	游荡型1	游荡型2	游荡型3	归家型1	归家型2	归家型3	巡逻型1	巡逻型2	巡逻型3	搜索型1	搜索型2	搜索型3	被动1	被动2	被动3	就近攻击1	就近攻击2	就近攻击3	低血攻击1	低血攻击2	低血攻击3	低级攻击1	低级攻击2	低级攻击3	高魔攻击1	高魔攻击2	高魔攻击3	单人攻击1	单人攻击2	单人攻击3
	//1		1			猪AI		0			闲时		10		20200	20201	20202	20203	20204	20205	20206	20207	20208	20209	20210	20211	20212	20213	20214	20215	20216	20217	20218	20219	20220	20221	20222	20223	20224	20225	20226	20227	20228	20229	20230	20231	20232
	//2								1			出生点	10		20200	20201	20202	20203	20204	20205	20206	20207	20208	20209	20210	20211	20212	20213	20214	20215	20216	20217	20218	20219	20220	20221	20222	20223	20224	20225	20226	20227	20228	20229	20230	20231	20232
	
	for(UINT m=0; m<MAX_AI_TYPE; m++)
	{
		////////////////////////////////////////////////////////////////
		file <<  "";
		sAI_INFO_UNIT*	pUnit = &pInfo->m_Units[m];

		file << pUnit->m_Code	;

		if(m == 0)
		{
			file << pInfo->m_AICode	;
			file << pInfo->m_Name	;
		}
		else
		{
			file <<  "";
			file <<  "";
		}

		file << pUnit->m_AIType;
		file << pUnit->m_Name;
		file << pUnit->m_Rate;

		///////////////////////////////////////
			file(MAX_AI_STATE);
		file << pUnit->m_Idles	;
		file << pUnit->m_Wanders;
		file << pUnit->m_Backs	;
		file << pUnit->m_Patrols;
		file << pUnit->m_Searchs;

		file << pUnit->m_Passives	;
		file << pUnit->m_Nearests	;
		file << pUnit->m_LowHPs	;
		file << pUnit->m_LowLVs	;
		file << pUnit->m_HighMPs	;
		file << pUnit->m_Singles	;
			file(0);

		////////////////////////////////////////////////////////////////
		file <<  "\n";
	}




	return TRUE;
}




// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "LogSystem.h"
#include "ApplicationDefine.h"

#include "ResPublishManager.h"
#include "ResPublishImpl.h"
#include "ResPublishResDeclare.h"






#define REG_PUBLISH(Name, Impl)\
			static ResPublish##Name  s_##ResPublish##Name(Impl);\
			theResPublishManager.RegParser(&s_##ResPublish##Name);

/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_RESPUBLISH_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{
	theResPublishImpl.SetPublish(&theResPublishManager);
	theResPublishManager.AddListener(&theResPublishImpl);
	theResPublishManager.SetImpl(&theResPublishImpl);

	REG_PUBLISH(RegenMonster				,&theResPublishImpl);
	REG_PUBLISH(RegenGroup					,&theResPublishImpl);
	REG_PUBLISH(DropEntry					,&theResPublishImpl);
	REG_PUBLISH(DropMonster					,&theResPublishImpl);
	REG_PUBLISH(DropMonsterGroup			,&theResPublishImpl);
	REG_PUBLISH(DropMoney					,&theResPublishImpl);

	REG_PUBLISH(ItemInfo						,&theResPublishImpl);
	//REG_PUBLISH(SkillDefault				,&theResPublishImpl);
	REG_PUBLISH(NpcInfo						,&theResPublishImpl);
	REG_PUBLISH(SkillInfo					,&theResPublishImpl);
	REG_PUBLISH(StateInfo					,&theResPublishImpl);
	REG_PUBLISH(NpcFuncInfo					,&theResPublishImpl);
	REG_PUBLISH(WorldInfo					,&theResPublishImpl);
	REG_PUBLISH(TipHero						,&theResPublishImpl);
	REG_PUBLISH(TipNpc						,&theResPublishImpl);
	REG_PUBLISH(TipMonster					,&theResPublishImpl);
	REG_PUBLISH(TipFreshMan					,&theResPublishImpl);
	REG_PUBLISH(TipAI							,&theResPublishImpl);
	REG_PUBLISH(HorseInfo					,&theResPublishImpl);
	REG_PUBLISH(ShopInfo						,&theResPublishImpl);
	REG_PUBLISH(SkillLib						,&theResPublishImpl);
	REG_PUBLISH(StyleInfo					,&theResPublishImpl);
	REG_PUBLISH(NpcQuestInfo				,&theResPublishImpl);
	REG_PUBLISH(TipLogInfo					,&theResPublishImpl);
	REG_PUBLISH(SoundInfoWeapon			,&theResPublishImpl);
	REG_PUBLISH(SoundInfoChar				,&theResPublishImpl);
	REG_PUBLISH(SoundInfoItem				,&theResPublishImpl);

	REG_PUBLISH(VItemInfo					,&theResPublishImpl);
	REG_PUBLISH(VNpcInfo						,&theResPublishImpl);
	REG_PUBLISH(VSkillInfo					,&theResPublishImpl);
	REG_PUBLISH(VStateInfo					,&theResPublishImpl);

	REG_PUBLISH(SoundRes						,&theResPublishImpl);
	REG_PUBLISH(MeshRes						,&theResPublishImpl);
	REG_PUBLISH(GraphicRes					,&theResPublishImpl);
	REG_PUBLISH(EquipRes						,&theResPublishImpl);
	REG_PUBLISH(DescRes						,&theResPublishImpl);
	REG_PUBLISH(TextRes						,&theResPublishImpl);
	REG_PUBLISH(EffectRes					,&theResPublishImpl);
	REG_PUBLISH(ScriptRes					,&theResPublishImpl);
	REG_PUBLISH(FieldInfo					,&theResPublishImpl);
	REG_PUBLISH(QuestRes						,&theResPublishImpl);
	REG_PUBLISH(QuestDropInfo				,&theResPublishImpl);

	LOGINFO("Plugin ResPublish insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_RESPUBLISH_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	return gamemain::ePluginOK;
}

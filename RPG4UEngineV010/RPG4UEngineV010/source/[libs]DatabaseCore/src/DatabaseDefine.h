/*////////////////////////////////////////////////////////////////////////
文 件 名：DatabaseDefine.h
创建日期：2009年3月30日
最后更新：2009年3月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __DATABASEDEFINE_H__
#define __DATABASEDEFINE_H__
#pragma once


#include <DatabaseConst.h>
#include <sqlext.h>



///////////////////////////////////////////////
class		Query;
//typedef	Query		Query;

///////////////////////////////////////////////
typedef void			(*PROC_DATABASE_LOGTEXT			)( LPCTSTR szMessage );
typedef void			(*PROC_DATABASE_QUERY_RESULT	)( DWORD dwIndex, Query * pData );



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sDATABASE_DESC
{
	eDATABASE_TYPE					databaseType;
	DWORD								dwQueryPoolSize;
	WORD								dwMaxQueryThreadNum;
	LPCTSTR							szDatabaseIP;
	LPCTSTR							szDatabaseName;
	LPCTSTR							szUserName;
	LPCTSTR							szUserPwd;
	PROC_DATABASE_QUERY_RESULT	procQueryResult;
	PROC_DATABASE_LOGTEXT		procErrorText;
};



//////////////////////////////////////////////////////////////
#define _QUERY_PARAM_INIT()		\
						virtual eQUERYTYPE	GetQueryType()\
						{ return QUERYKIND_LARGE; }

//////////////////////////////////////////////////////////////
#define _PARAM_BINDING_BEGIN( TAG_TYPE, _TAGPTR, _PARAMNUM )\
						virtual INT	 GetResult			(){return _TAGPTR[0].m_Result;}\
						virtual BOOL IsResultSucceed	(){return _TAGPTR[0].m_Result == QUERY_RESULT_SUCCEEDED;}\
						virtual BOOL IsResultFailed	(){return _TAGPTR[0].m_Result == QUERY_RESULT_FAILED;}\
						virtual sPARAM_QUERY*	GetQueryParam	(){return _TAGPTR;}\
						virtual ULONG				GetParameterNum()\
							{ return _PARAMNUM; } \
						virtual eQUERYTYPE		GetQueryType()\
							{ return QUERYKIND_LARGE; } \
						virtual VOID GetParameterInfo( ULONG ulParamIndex\
																		, SHORT & ioType\
																		, BYTE *& pParamPtr\
																		, LONG & lSize\
																		, SHORT & sqlType\
																		, SHORT & cType ) \
						{ \
							TAG_TYPE	* pTagPtr = _TAGPTR; \
							switch( ulParamIndex ) \
							{

//////////////////////////////////////////////////////////////
#define _PARAM_PTR_BINDING( _PARAMIDX, _PARAMPTR, _IOTYPE )\
						case _PARAMIDX: \
						{ \
							pParamPtr = (BYTE *)(pTagPtr->_PARAMPTR); \
							lSize		= sizeof(pTagPtr->_PARAMPTR); \
							cType		= GetDataCType		( pTagPtr->_PARAMPTR ); \
							sqlType	= GetType_ODBC_Sql	( pTagPtr->_PARAMPTR ); } \
							ioType	= _IOTYPE; \
							return;

//////////////////////////////////////////////////////////////
#define _PARAM_PTR_BINDING2( _PARAMIDX, _PARAMPTR, _PARAMSIZE, _IOTYPE )\
						case _PARAMIDX: \
						{ \
							pParamPtr = (BYTE *)(pTagPtr->_PARAMPTR); \
							lSize		= _PARAMSIZE; \
							cType		= GetDataCType( pTagPtr->_PARAMPTR ); \
							sqlType	= GetType_ODBC_Sql( pTagPtr->_PARAMPTR ); } \
							ioType	= _IOTYPE; \
							return;

//////////////////////////////////////////////////////////////
#define _PARAM_BINDING( _PARAMIDX, _PARAM, _IOTYPE )\
						case _PARAMIDX: \
						{ \
						pParamPtr	= (BYTE *)&(pTagPtr->_PARAM); \
						lSize			= sizeof(pTagPtr->_PARAM); \
						cType			= GetDataCType( pTagPtr->_PARAM ); \
						sqlType		= GetType_ODBC_Sql( pTagPtr->_PARAM ); \
						ioType		= _IOTYPE; \
						} return;


//////////////////////////////////////////////////////////////
#define _PARAM_BINDING_END()\
						} \
					return ; \
					}


//////////////////////////////////////////////////////////////
#define _PARAM_BINDING_INIT()\
					virtual ULONG			GetMaxRowNum() { return 0; } \
					virtual ULONG			GetMaxColNum() { return 0; } \
					virtual BYTE *			GetBuffer( ULONG ulRow ) { return NULL; }


//////////////////////////////////////////////////////////////
#define _RESULT_BINDING_BEGIN( TAG_TYPE, COL_ARRAY, COL_LENs, ROWNUM, COLNUM )\
					typedef	TAG_TYPE	TagType;	\
					virtual sRESULT_QUERY*	GetQueryResult	(UINT nRowAt){if(nRowAt < ROWNUM)return &COL_ARRAY[nRowAt]; return NULL;}\
					virtual ULONG				GetMaxRowNum() { return ROWNUM; }	\
					virtual ULONG				GetMaxColNum() { return COLNUM; }	\
					virtual BYTE *				GetBuffer( ULONG nRow ) { return (BYTE *)&COL_ARRAY[nRow]; }	\
					virtual ULONG *			GetColumnSizePtr( DWORD dwColAt ) \
					{ \
						return &COL_LENs[dwColAt]; \
					} \
					virtual VOID			GetColumnInfo	( ULONG row\
																			, ULONG col\
																			, ULONG *& pColPtr\
																			, LONG & lSize\
																			, SHORT & sColType ) \
					{ \
						TagType * pTag = COL_ARRAY; \
						switch(col)	\
						{


#ifdef _USING_OLEDB
//////////////////////////////////////////////////////////////
#define _RESULT_BINDING_PTR( _COLIDX, _COL_DATAPTR )\
					case _COLIDX:\
					{ \
						*pColPtr		=  (size_t)&(((TagType *)0)->_COL_DATAPTR); \
						lSize			= sizeof(pTag[row]._COL_DATAPTR); \
						sColType		= GetDataCType( pTag[row]._COL_DATAPTR ); } \
						return ;

//////////////////////////////////////////////////////////////
#define _RESULT_BINDING_COL( _COLIDX, _COL_DATA )	\
					case _COLIDX: \
					{ \
						*pColPtr =  (size_t)&(((TagType *)0)->_COL_DATA); \
						lSize		= sizeof(pTag[row]._COL_DATA); \
						sColType = GetDataCType( pTag[row]._COL_DATA ); } \
						return ;
#else
//////////////////////////////////////////////////////////////
#define _RESULT_BINDING_PTR( _COLIDX, _COL_DATAPTR )\
					case _COLIDX: \
					{ \
					pColPtr	= (ULONG *)(pTag[row]._COL_DATAPTR); \
					lSize		= sizeof(pTag[row]._COL_DATAPTR); \
					sColType = GetDataCType( pTag[row]._COL_DATAPTR ); } \
					return ;

//////////////////////////////////////////////////////////////
#define _RESULT_BINDING_COL( _COLIDX, _COL_DATA )	\
					case _COLIDX: \
					{ \
						pColPtr	= (ULONG *)&(pTag[row]._COL_DATA); \
						lSize		= sizeof(pTag[row]._COL_DATA); \
						sColType = GetDataCType( pTag[row]._COL_DATA ); } \
						return ;
#endif


//////////////////////////////////////////////////////////////
#define _RESULT_BINDING_END()\
							} \
						return ; \
					}


//////////////////////////////////////////////////////////////
enum
{
	 _TINYBIND			= 1
	,_SMALLBIND			= 2
	,_INTBIND			= 3
	,_CHARBIND			= 4
	,_BITBIND			= 6
	,_FLT8BIND			= 9
	,_NTBSTRINGBIND	= 11
	,_FLT4BIND			= 14
};


////////////////////////////////////////////////////////
#ifndef _DATABASECORE

#if defined( _USING_ODBC )
	#define GetDataCType( cType )					_GetODBCType( cType )
#elif defined( _USING_OLEDB )
	#define GetDataCType( cType )					_GetOLEDBType( cType )
#elif defined( _USING_SQLLIB )
	#define GetDataCType( cType )					_GetSQLLIBType( cType )
#else
	#error Database Library not defined!! ( _USING_ODBC or _USING_OLEDB or _USING_SQLLIB )
#endif 

#endif



////////////////////////////////////////////////////////
#define _PARAM_TYPE_ODBC( cType, odbcType )\
						inline SQLSMALLINT _GetODBCType(cType) \
						{ \
							return odbcType; \
						}

		_PARAM_TYPE_ODBC(CHAR,					SQL_C_CHAR)
		_PARAM_TYPE_ODBC(WORD,					SQL_C_LONG)
		_PARAM_TYPE_ODBC(DWORD,					SQL_C_LONG)
		_PARAM_TYPE_ODBC(UINT,					SQL_C_LONG)
		_PARAM_TYPE_ODBC(UINT64,				SQL_C_UBIGINT)

		_PARAM_TYPE_ODBC(BYTE,					SQL_C_UTINYINT)
		_PARAM_TYPE_ODBC(SHORT,					SQL_C_SHORT)  
		_PARAM_TYPE_ODBC(LONG,					SQL_C_LONG)
		_PARAM_TYPE_ODBC(INT,					SQL_C_LONG)
		_PARAM_TYPE_ODBC(INT64,					SQL_C_SBIGINT)

		_PARAM_TYPE_ODBC(float,					SQL_C_DOUBLE)
		_PARAM_TYPE_ODBC(double,				SQL_C_DOUBLE)

		_PARAM_TYPE_ODBC(BYTE*,					SQL_C_BINARY);		// binary or varbinary
		_PARAM_TYPE_ODBC(CHAR*,					SQL_C_CHAR)
		_PARAM_TYPE_ODBC(LPCTSTR,				SQL_C_CHAR)

		_PARAM_TYPE_ODBC(TIMESTAMP_STRUCT,	SQL_C_TIMESTAMP)



////////////////////////////////////////////////////////
#define _PARAM_TYPE_OLEDB(cType, oledbtype)\
					inline DBTYPE _GetOLEDBType(cType) \
					{ \
						return oledbtype; \
					}



		_PARAM_TYPE_OLEDB(char					,DBTYPE_I1)
		_PARAM_TYPE_OLEDB(SHORT					,DBTYPE_I2)     // DBTYPE_BOOL
		_PARAM_TYPE_OLEDB(LONG					,DBTYPE_I4)     // DBTYPE_ERROR (SCODE)
		_PARAM_TYPE_OLEDB(int					,DBTYPE_I4)
		_PARAM_TYPE_OLEDB(INT64					,DBTYPE_I8)
		_PARAM_TYPE_OLEDB(LARGE_INTEGER		,DBTYPE_I8)     // DBTYPE_CY

		_PARAM_TYPE_OLEDB(BYTE					,DBTYPE_UI1)
		_PARAM_TYPE_OLEDB(WORD					,DBTYPE_UI2)
		_PARAM_TYPE_OLEDB(DWORD					,DBTYPE_UI4)
		_PARAM_TYPE_OLEDB(UINT					,DBTYPE_UI4)
		_PARAM_TYPE_OLEDB(UINT64				,DBTYPE_UI8)
		_PARAM_TYPE_OLEDB(ULARGE_INTEGER		,DBTYPE_UI8)

		_PARAM_TYPE_OLEDB(float					,DBTYPE_R4)
		_PARAM_TYPE_OLEDB(double				,DBTYPE_R8)     // DBTYPE_DATE
		_PARAM_TYPE_OLEDB(DECIMAL				,DBTYPE_DECIMAL)

		_PARAM_TYPE_OLEDB(BYTE*					,DBTYPE_BYTES)						// binary or varbinary
		_PARAM_TYPE_OLEDB(CHAR*					,DBTYPE_STR)
		_PARAM_TYPE_OLEDB(LPCTSTR				,DBTYPE_STR | DBTYPE_BYREF)




////////////////////////////////////////////////////////
#define _PARAM_TYPE_SQLLIB( cType, sqlType )\
						inline INT _GetSQLLIBType(cType) \
						{ \
							return sqlType; \
						}

		_PARAM_TYPE_SQLLIB(char,					_CHARBIND)
		_PARAM_TYPE_SQLLIB(SHORT,					_SMALLBIND)  
		_PARAM_TYPE_SQLLIB(LONG,					_INTBIND)
		_PARAM_TYPE_SQLLIB(int,						_INTBIND)
		_PARAM_TYPE_SQLLIB(INT64,					_INTBIND)

		_PARAM_TYPE_SQLLIB(BYTE,					_TINYBIND)
		_PARAM_TYPE_SQLLIB(WORD,					_SMALLBIND)
		_PARAM_TYPE_SQLLIB(DWORD,					_INTBIND)
		_PARAM_TYPE_SQLLIB(UINT,					_INTBIND)
		_PARAM_TYPE_SQLLIB(UINT64,					_INTBIND)

		_PARAM_TYPE_SQLLIB(LPCTSTR,				_NTBSTRINGBIND)
		_PARAM_TYPE_SQLLIB(float,					_FLT4BIND)
		_PARAM_TYPE_SQLLIB(double,					_FLT8BIND)



////////////////////////////////////////////////////////
#define _PARAM_TYPE_ODBC_C(cType, odbcType)\
				inline int GetType_ODBC_Sql(cType) \
				{ \
					return odbcType; \
				}

		_PARAM_TYPE_ODBC_C(BYTE*,				SQL_VARBINARY)			// binary or varbinary
		_PARAM_TYPE_ODBC_C(LPCTSTR,			SQL_VARCHAR)

		_PARAM_TYPE_ODBC_C(INT64,				SQL_BIGINT)
		_PARAM_TYPE_ODBC_C(UINT64,				SQL_BIGINT)

		_PARAM_TYPE_ODBC_C(char,				SQL_CHAR)
		_PARAM_TYPE_ODBC_C(SHORT,				SQL_SMALLINT)  
		_PARAM_TYPE_ODBC_C(LONG,				SQL_INTEGER)
		_PARAM_TYPE_ODBC_C(int,					SQL_INTEGER)

		_PARAM_TYPE_ODBC_C(BYTE,				SQL_CHAR)
		_PARAM_TYPE_ODBC_C(WORD,				SQL_SMALLINT)
		_PARAM_TYPE_ODBC_C(DWORD,				SQL_INTEGER)
		_PARAM_TYPE_ODBC_C(UINT,				SQL_INTEGER)

		_PARAM_TYPE_ODBC_C(float,				SQL_FLOAT)
		_PARAM_TYPE_ODBC_C(double,				SQL_DOUBLE)


#endif //__DATABASEDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：	StringUtil.h
创建日期：	2007.10.17
最后更新：	2007.10.17
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：
	字符串基本操作封装
		字符串截取
		路径操作



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#pragma once
#ifndef _STRINGUTIL_H_
#define _STRINGUTIL_H_

#include "StringVector.h"

////////////////////////////////////////////////////////////////////////
VRU_BEGIN


////////////////////////////////////////////////////////////////////////
/** 
	字符串基本操作封装
*/
class _BASE_API StringUtil
{
public:
	static char*  CharNext(const char* p );
	static LPCSTR FindChar(LPCSTR pszBlock,char chMatch );


	///////////////////////////////////////////////////
	static void StripEXT		( char *szFile	);
	static void StripPath	( char *name	);
	static void StripName	( char *name	);

	static char* SafeNCpy( char *strDest,LPCSTR strSource,size_t count );


	static char* StrCpyF(char *dst, size_t dstSize, ...);

	static BOOL Atob(LPCSTR szText);

	/** 返回pText格式化过的文本内容
	*/
	static LPCSTR FormatText(char *pText, ...);

	/** 把str中所有 '\\' 变为 '/'
	*/
	static void ForwardSlash(char *str);

	/** 把str中所有 '/' 变为 '\\'
	*/
	static void BackSlash(char *str);

	/** 去删除任何空白字符，包括：空格、TAB等。
	TABs and so on.
	@remarks
		本函数只清除头和尾的空白字符，默认下头和尾一并清除。
	*/
	static void Trim( String& str, BOOL left = true, BOOL right = true );

	/** 把字符串中按<code>分隔符</code>作为分隔标志，然后把各类隔开的内容，放到StringVector，
	并返回。
	@param
	delims 指定字符串中的分隔符
	@param
	maxSplits 指定返回分隔的内容数量，如：
		>0 时，将从头到尾截取指定数量即完成
		否则全部处理
	*/
	static BOOL Split	(const String&                str
                     ,StringVector&                out
							,const String&					   delims = "\t\n "
							,UINT									maxSplits =             0);

	/**  把所有alpha字符转换为小写
	*/
	static void ToLowerCase( String& str );

	/** 把所有alpha字符转换为大写
	*/
	static void ToUpperCase( String& str );


	/** 判断str是否前缀pattern字符块
	@param pattern 待比较的前缀字符块
	@param lowerCase 为true时，如果str前缀pattern的话，则转换为小写，再和pattern比较。
	*/
	static BOOL StartsWith(const String& str, const String& pattern, BOOL lowerCase = true);

	/** 判断str是否后缀pattern字符块
	@param pattern 待比较的后缀字符块
	@param lowerCase 为true时，如果str后缀pattern的话，则转换为小写，再和pattern比较。
	*/
	static BOOL EndsWith(const String& str, const String& pattern, BOOL lowerCase = true);

	/** 把路径标准化，格式为：  c:/dir/dir/
		 把路径中所有'\\'转换为'/'，并且确保后缀'/'。
	 @param Init 为代标准化的路径；
	 @returns String返回格式化过的路径。
	*/
	static void StandardisePath( String &Init, BOOL bWithTail=TRUE);

	/** 把qualifiedName分解为文件名称和路径名称
	@remarks
		outPath为格式化过的路径，@see StringUtil::StandardisePath
	@param qualifiedName待分解的全路径
	@param outBasename待输出的基本名称
	@param outPath待输出的格式化路径
	*/
	static void SplitFilename(const String& qualifiedName, String& outBasename, String& outPath);

	/** Simple pattern-matching routine allowing a wildcard pattern.
	@param str String to test
	@param pattern Pattern to match against; can include simple '*' wildcards
	@param caseSensitive Whether the match is case sensitive or not
	*/
	static BOOL Match(const String& str, const String& pattern, BOOL caseSensitive = true);

	/** 按format格式把buffer数据扫描到后序变量中
	@param buffer 数据内容
	@param format 数据内容格式
	*/
	static INT Scanf(const char *buffer, const char *format, ...);

	/** 将字母变为大写字母
	@param c 待转换的字母
	*/
	static char ToUpper(const char c);

	/** 将字母变为小写字母
	@param c 待转换的字母
	*/
	static char ToLower(const char c);

	/// Constant blank string, useful for returning by ref where local does not exist
	static const String s_strBlank;
};//StringUtil

////////////////////////////////////////////////////////////////////////
VRU_END

#include "StringUtil.inl"


#endif // _STRINGUTIL_H_

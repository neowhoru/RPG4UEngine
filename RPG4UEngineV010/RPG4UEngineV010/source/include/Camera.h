/*////////////////////////////////////////////////////////////////////////
文 件 名：Camera.h
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：
	= 相机原理介绍：
		- 相机创建，须指定，参阅SetProjection
			.Init
			.Release
			.CameraListener		相机监听句柄
		- 相机参数设置，参数变更后，将通知CameraListener
			SetProjection
			.NearClip
			.FarClip
			.FOV
			.宽高比(fAspect)	

		-相机构成
			.相机位置，其实就是LookAt位置
				GetLookAt
				通常，在FrameMove时，这样调用
					theCamera.SetCamera( theHero.GetPosition(), TRUE );	
					theCamera.UpdateListeners();
				这样，可以保持相机位置，永远在Hero身上

			.相机几何位置 
				GetLookFrom
				几何位置，就是在做相机几何逻辑，相机视线的起点


			.相机距离拉伸
				MoveFrontBack
				注意，参数为0时，将保持拉伸的淡出


	= CameraListener虚函数:
		SetProjectionParams	(m_fFOV
									,m_fAspect
									,m_fNearClip
									,m_fFarClip);
		SetCameraParams		(m_vCameraFrom
									,m_vCameraTo
									,m_vUp);


	= 例子：
		///相机旋转
		if( theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_KEY) )
		{
			theCamera.Rotate( pMouseState->lX, pMouseState->lY );
		}

		///人物转身时，保持角度
		else if( theHero.GetCurState() == STATE_KEYBOARDMOVE) 
		{
			theCamera.KeyboardModeInterpolateAngle(theHero.GetAngle());
		}
		if( theHero.IsMoving() )
		{
			if( theGeneralGameParam.GetCameraRotation() )
			{
				theCamera.InterpolateAngle();
			}
		}


		///不断更新相机参数，保持锁定在 Hero位置上
		theCamera.MoveFrontBack( pMouseState->lZ );
		theCamera.SetCamera( theHero.GetVisiblePos(), TRUE );	
		theCamera.UpdateListeners();	///参数更新到CameraListener中

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CAMERA_H__
#define __CAMERA_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "SceneStruct.h"
#include "CameraListener.h"

#define INIT_SWITCH                 0
#define INIT_POWER                  0.05f

#define	CAMERA_YEW_RANGE_MIN				0.00f
#define	CAMERA_YEW_RANGE_MAX				( MATH_PI * 2.0f )
#define	CAMERA_YEW_RANGE_CENTER			MATH_PI

#define CAMERA_DEFAULT_FOV             ( MATH_PI / 6.0f ) // 30.
#define CAMERA_DEFAULT_NEAR_CLIP       1.0f
#define CAMERA_DEFAULT_FAR_CLIP        150.0f

#define CAMERA_DEFAULT_MOVE_SPEED		1.0f


//_NAMESPACEU(Object,			object);
_NAMESPACEU(PathHandler,	tile);

//class CameraListener;
using namespace scene;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _INPUTLIB_API Camera 
{
public:
	Camera( void );
	~Camera( void );

	enum CAMERA_STATUS 
	{ 
		CAMERA_STATUS_NONE				= 0, 
		CAMERA_STATUS_VIBE				= 1,        
		CAMERA_STATUS_EVENT         = (2 << 1), 
		CAMERA_STATUS_SPRING        = (2 << 2), 
	};

	enum VIBE_TYPE
	{  
		VIBE_TYPE_ALL			=0
		,VIBE_TYPE_VERTICAL	
		,VIBE_TYPE_HORISONTAL
	};

	enum
	{
		 CAMERA_ROTATE_NONE
		,CAMERA_ROTATE_LMOUSE
		,CAMERA_ROTATE_RMOUSE
	};


	enum
	{
		CAMERA_NOT_TURN		= 0
		,CAMERA_LEFT_TURN		= 1
		,CAMERA_RIGHT_TURN	= 2
	};

public:
	BOOL            Init			(float				fNearClip
										,float				fFarClip
										,float				fFOV
										,float				fAspect
										,CameraListener*	pHandler);	
	virtual void    Release		( void );

	virtual BOOL    FrameMove	( DWORD dwTick );

	void            StartVibration	(float time,float power=INIT_POWER); 
	void            EndVibration		();
	void            SetVibeType		(VIBE_TYPE type);

	Vector3D			GetSceernToCameraVector(const VECTOR3D& vPos,BOOL bChangeZ=FALSE);

	Vector3C& GetLookAt		( void ) { return m_vCameraTo; }
	Vector3C& GetLookFrom	( void ) { return m_vCameraFrom; }




	void            SetProjection			( float fMin, float fMax ,float fFOV, float fAspect);
	void            RestoreProjection	();

	void				 UnlockedTarget		(  );
	void				 LockedTarget			( const VECTOR3D& vPos );
	void            SetCameraFromTo		(Vector3D	vFrom
                                       ,Vector3D	vTo
													,BOOL			bHeightCheck= FALSE)		;
	void            SetCamera				( const VECTOR3D& pvPos, BOOL bHeightCheck = FALSE );	
	void            UpdateCamera			( const VECTOR3D* pvPos, BOOL bHeightCheck = FALSE );	

	void            UpdateListeners		();

	void            StartMove				( void );
	void            InterpolateAngle		();	
	void            KeyboardModeInterpolateAngle	(float fAngle);	

	//////////////////////////////////////////
	//
	void            MoveFrontBack			( int iDist );
	void            OffsetCamera			( const VECTOR3D& vPos , BOOL bChangeZ=FALSE , BOOL bHeightCheck = FALSE );	///相对当前位置，移动相机LookAt位置
	void            Rotate					( int iAngleX, int iAngleZ = 0.0f, int iState = CAMERA_ROTATE_NONE );
	void            RotateYaw				( int iAngle );
	void            SetCameraPosition	( const VECTOR3D& vPos );
	void            SetYaw					(float fYaw);
	void            SetPitch				(float fPitch);
	float           GetYaw					();

	void            MoveUpDown			( float fDist );
	void            SetFocus			(Vector3D wzFocus);

	void            SetAngle	( float fAngle );
	void            SetAngle	( Vector3D *wvDir );
	float				 GetAngle	() { return m_fAngleYaw; }
	void            RotateAroundFocus(float fAngle);

	Ray           GenerateCameraRay	();

	void               SetAspectRatio	(eSCREEN_RATIO eRatio);
	eSCREEN_RATIO  GetAspectRatio	();

	//void				AttachToBone		(Object *pObject,int boneindex);
	void				SetRotateState		(BOOL bState ) ;
	BOOL				IsKeepRotating		(float fMultiple=1.0f);
	BOOL				IsRotate				();
	int				RotateMouseState	() ;
	void				RotateAngleCheck	( int iCheck ) ;
	int				GetStartRotateCheck	() ;
	void				SetStartRotateCheck	( BOOL bStartRotate );

	void           FreeCameraMoveFrontBack( int iDist );
	int				GetCameraTurnState();

public:
	// getting
	float          GetSpringConst();
	float          GetDampConst();
	float          GetSpringLen();
	BOOL				IsCloseToLookAt(float t=1.0f);
	BOOL				IsReachToTarget(float t=0.1f);

	// setting
	void            SetSpringConst(float springconst);
	void            SetDampConst	(float dampconst);
	void            SetSpringLen	(float springlen);

	Vector3D         SpringDamp ( Vector3D currPos,    // Current Position
											Vector3D trgPos,     // Target Position
											Vector3D prevTrgPos, // Previous Target Position
											float deltaTime,    // Change in Time
											float springConst,  // Hooke's Constant
											float dampConst,    // Damp Constant
											float springLen);

protected:

	BOOL			_UpdateCameraPosition	();
	BOOL			_UpdateCameraInfo			(BOOL bHeightCheck);


protected:
	VG_BOOL_PROPERTY		(TargetTracking);
	VG_FLOAT_PROPERTY		(TargetTrackSlotDownStep);
	VG_TYPE_GET_PROPERTY	(CameraDirectionToTarget, Vector3D);

	Vector3D	   m_CameraTargetPosition;
	Vector3D	   m_CameraPosition;
	VG_VECTOR_PROPERTY	(Direction,		Vector3D);
	VG_VECTOR_PROPERTY	(CameraFrom,	Vector3D);
	VG_VECTOR_PROPERTY	(CameraTo,		Vector3D);

	Vector3D	   m_vY;
	VG_VECTOR_PROPERTY	(Up, Vector3D);

	BOOL			m_bIsRotate;
	BOOL			m_bIsRotateX;
	BOOL			m_bIsRotateZ;

	float			m_fMoveSpeed;

	VG_BOOL_PROPERTY	(LockDistance);
	VG_FLOAT_PROPERTY	(DistanceMin);
	VG_FLOAT_PROPERTY	(DistanceMax);
	VG_FLOAT_PROPERTY	(AngleCorrect);	//校正角度...
	VG_FLOAT_PROPERTY	(Distance);
	VG_FLOAT_PROPERTY	(PitchMin);
	VG_FLOAT_PROPERTY	(PitchMax);
	VG_FLOAT_PROPERTY	(AnglePitch);
	VG_FLOAT_PROPERTY	(YawMin);
	VG_FLOAT_PROPERTY	(YawMax);
	VG_FLOAT_PROPERTY	(AngleYaw);
	VG_FLOAT_PROPERTY	(YawCenter);
	VG_FLOAT_PROPERTY	(RotationStep);
	VG_FLOAT_PROPERTY	(RotationSlowDownLevel);
	VG_FLOAT_PROPERTY	(RotationSlowDown);
	VG_FLOAT_PROPERTY	(RotationMin);         

	VG_FLOAT_PROPERTY(UpCorrect);   

	float		   m_fPrevAngleYaw;
	float		   m_fAngleYawStep;
	float		   m_fAngleYawDirection;
	float		   m_fPrevAnglePitch;
	int			m_iPreYaw;
	int			m_iPrePitch;
	int			m_iRotateState;

	Vector3D	    m_CameraTargetDirection;
	float		    m_fTargetDirectionAngle;

	BOOL		    m_bStartMove;
	float		    m_fAngle;
	float        m_fTurnAccelerate;
	int			 m_iTurnState;

	VG_FLOAT_GET_PROPERTY	(Aspect);
	VG_FLOAT_GET_PROPERTY	(FOV);
	VG_FLOAT_GET_PROPERTY	(NearClip);
	VG_FLOAT_GET_PROPERTY	(FarClip);

	eSCREEN_RATIO m_eRatio;


protected:
	Vector3D        m_targetOldPos;
	Vector3D        m_chasePos;
	/// spiring constant
	float           m_springConst;
	/// damping constant
	float           m_dampConst;
	/// spring length constatnt
	float           m_springLen;

	BOOL				m_bStartRotate;
	BOOL				m_bStartRotateX;
	BOOL				m_bStartRotateZ;


	VG_FLOAT_GET_PROPERTY	(RotateLevelX);
	VG_FLOAT_GET_PROPERTY	(RotateSlowDownX);
	VG_FLOAT_GET_PROPERTY	(RotateLevelZ);
	VG_FLOAT_GET_PROPERTY	(RotateSlowDownZ);
	float				m_fDistanceLevel;
	float				m_fDistanceSlowDown;

	VG_PTR_PROPERTY			(Listener, CameraListener);
	VG_BOOL_PROPERTY			(UseTilePos);	///是否使用Tile系统，默认：使用
	VG_VECTOR_GET_PROPERTY	(LockedTarget, Vector3D);		///	锁定目标位置
	VG_BOOL_GET_PROPERTY		(UseLockedTarget);	///	使用锁定目标

protected:

	DWORD				m_status;				
	float				m_twVibe;			
	float				m_vibePower;
	INT				m_twSwitch;
	PathHandler   *m_pPathExplorer;
	WORD				m_vibeType;
	int				m_iAngleCheckX;
	int				m_iAngleCheckZ;

};//Camera


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(Camera , _INPUTLIB_API);
//extern _INPUTLIB_API Camera& singleton::GetCamera();
#define theCamera  singleton::GetCamera()




#include "Camera.inl"

#endif //__CAMERA_H__
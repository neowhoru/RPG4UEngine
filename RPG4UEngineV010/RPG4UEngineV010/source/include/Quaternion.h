/*////////////////////////////////////////////////////////////////////////
文 件 名：Quaternion.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __QUATERNION_H__
#define __QUATERNION_H__
#pragma once

//#include "Quaternion.h"
//
//namespace math
//{ 
//	typedef Quaternion<float>	Quaternion;
//
//};//namespace math

#include "MathStructDecl.h"
#include "MathFunction.h"
#include "Vector3D.h"

class Vector3D;
class Matrix3;
class Matrix4;
//namespace math
//{ 

class _MATHLIB_API Quaternion : public QUATERNION
{
public:
    Quaternion();
    Quaternion( CONST REAL * );
    Quaternion( CONST QUATERNION& q );
    Quaternion( const Matrix4& mIn );
    Quaternion( REAL x, REAL y, REAL z, REAL w );

public:
    Quaternion& Set( CONST REAL * );
    Quaternion& Set( CONST QUATERNION& q );
    Quaternion& Set( const Matrix4& mIn );
    Quaternion& Set( REAL x, REAL y, REAL z, REAL w );

    Quaternion& operator()( CONST REAL * );
    Quaternion& operator()( CONST QUATERNION& q );
    Quaternion& operator()( const Matrix4& mIn );
    Quaternion& operator()( REAL x, REAL y, REAL z, REAL w );

public:
    // casting
    operator REAL* ();
    operator CONST REAL* () const;

    // assignment operators
    Quaternion& operator += ( CONST Quaternion& );
    Quaternion& operator -= ( CONST Quaternion& );
    Quaternion& operator *= ( CONST Quaternion& );
    Quaternion& operator *= ( REAL );
    Quaternion& operator /= ( REAL );

    // unary operators
    Quaternion  operator + () const;
    Quaternion  operator - () const;

    // binary operators
    Quaternion operator + ( CONST Quaternion& ) const;
    Quaternion operator - ( CONST Quaternion& ) const;
    Quaternion operator * ( CONST Quaternion& ) const;
    Quaternion operator * ( REAL ) const;
    Quaternion operator / ( REAL ) const;

    Vector3D	operator * ( const VECTOR3D& );

    friend Quaternion operator * (REAL, CONST Quaternion& );

    BOOL operator == ( CONST Quaternion& ) const;
    BOOL operator != ( CONST Quaternion& ) const;

public:
	/*////////////////////////////////////////////////////////////////////////
	工具类
	/*////////////////////////////////////////////////////////////////////////
	Quaternion& Identity					();
	Quaternion& SetRotation				(const VECTOR3D& vAxis, float fAngle);
	Quaternion& SetRotationX			(float fAngle) ;
	Quaternion& SetRotationY			(float fAngle) ;
	Quaternion& SetRotationZ			(float fAngle) ;
	Quaternion& SetRotationXYZ			(float fAngleX, float fAngleY, float fAngleZ) ;
	Quaternion& SetRotationViaMatrix	(const VECTOR3D& vAxis, float fAngle);

	Quaternion& ToRotation			(VECTOR3D& vAxis, float& fAngle);

	Vector3D		TramsformRotation	(const VECTOR3D& vDir);
	Quaternion& MakeByCrossVectors(const Vector3D&  vFrom, const Vector3D& vTo);

	Quaternion& FromRotation		(const VECTOR3D&  vRotate);
	//Quaternion& ToRotation		(VECTOR3D&  vRotate);

	Quaternion& FromRotationMatrix(const Matrix3&  mIn);
	Quaternion& ToRotationMatrix	(Matrix3&  mOut);

	void			Blend					(const Quaternion& q, float d);

public:
	///与3D坐标轴转换
	Quaternion& FromAxes (const VECTOR3D* akAxis);
	Quaternion& FromAxes (const VECTOR3D& xAxis, const VECTOR3D& yAxis, const VECTOR3D& zAxis);
	Quaternion& ToAxes	(VECTOR3D* akAxis) ;
	Quaternion& ToAxes	(VECTOR3D& xAxis, VECTOR3D& yAxis, VECTOR3D& zAxis) ;

	///获取本地轴
	Vector3D AxisX(void) const;
	Vector3D AxisY(void) const;
	Vector3D AxisZ(void) const;


	/** 计算本地roll值
	@param reprojectAxis 在y轴投向到xy平面时，将直接返回
	*/
	REAL GetRoll	(BOOL bReprojectAxis = TRUE) const;
	REAL GetPitch	(BOOL bReprojectAxis = TRUE) const;
	REAL GetYaw		(BOOL bReprojectAxis = TRUE) const;		


public:
	Quaternion&	Slerp					(const Quaternion& q1, const Quaternion& q2, float t);
	Quaternion	SlerpNew				(const Quaternion& q1, const Quaternion& q2, float t, BOOL bShortestPath = FALSE);

	/** 合成四元数
	@result   q = q1 * q2
	*/
	Quaternion&	Multiply				(const Quaternion& q1, const Quaternion& q2);
	Vector3D		Multiply				(const VECTOR3D& v);

	Quaternion&	ToMatrix				(Matrix4& mOut);
	Quaternion&	FromMatrix			(const Matrix4& mIn);
	//void			ToAxisAngle	( Vector3D* pwvOutAxis, float* pfOutAngle); 
	//Quaternion*	Conjugate	( ); 

	///创建from到to矢量旋转角度
	///@param bUnit指定 from to矢量是否为单位矢量
	Quaternion&	Generate				(const Vector3D& from, const Vector3D& to, BOOL bUnit);

   Quaternion& Conjugate	(void);
	Quaternion& Scaling		(float s);

   REAL			DotProduct	(const Quaternion& q) const;
   REAL			Norm			(const Quaternion& q);
   REAL			Magnitude	(void);
	Quaternion& Normalize	();
	Quaternion& Inverse		();

	BOOL IsEqual(const Quaternion& q, float tol) const;

public:
	inline friend Quaternion operator * (float f, CONST Quaternion& q )
	{
		 return Quaternion(f * q.x, f * q.y, f * q.z, f * q.w);
	}

};

typedef Quaternion			QuaternionX;
typedef const Quaternion	QuaternionC;

//};//namespace math

#include "Quaternion.inl"


#endif //__QUATERNION_H__
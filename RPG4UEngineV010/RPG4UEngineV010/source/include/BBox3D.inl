/*////////////////////////////////////////////////////////////////////////
文 件 名：BBox3D.inl
创建日期：2008年5月20日
最后更新：2008年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BBOX3D_INL__
#define __BBOX3D_INL__
#pragma once


//------------------------------------------------------------------------------
inline BBox3D::BBox3D()
{
}

//------------------------------------------------------------------------------
inline BBox3D::BBox3D(const Vector3D& center, const Vector3D& extents)
{
    m_Min = center - extents;
    m_Max = center + extents;
}

inline BBox3D::BBox3D(const BBOX3D&  bbox)
{
   Set(bbox);
}

//------------------------------------------------------------------------------
inline BBox3D::BBox3D(const Matrix4& m)
{
    Set(m);
}

inline BBox3D& BBox3D::operator()(const Vector3D&	center, const Vector3D& extents)
{
    return Set(center, extents);
}
inline BBox3D& BBox3D::operator()(const BBOX3D&		bbox)
{
    return Set(bbox);
}

inline BBox3D& BBox3D::operator()(const Matrix4&	m)
{
    return Set(m);
}


//------------------------------------------------------------------------------
inline Vector3D BBox3D::Center() const
{
    return Vector3D((m_Max + m_Min) * 0.5f);
}

//------------------------------------------------------------------------------
inline Vector3D BBox3D::Extents() const
{
    return Vector3D((m_Max - m_Min) * 0.5f);
}

//------------------------------------------------------------------------------
inline Vector3D BBox3D::Size() const
{
    return Vector3D(m_Max - m_Min);
}

//------------------------------------------------------------------------------
inline BBox3D& BBox3D::Set(const Vector3D& center, const Vector3D& extents)
{
    m_Min = center - extents;
    m_Max = center + extents;
	 return *this;
}

inline BBox3D& BBox3D::Set(const BBOX3D&	bbox)
{
    m_Min = bbox.m_Min;
    m_Max = bbox.m_Max;
	 return *this;
}


//------------------------------------------------------------------------------
inline void BBox3D::BeginExtend()
{
	m_Min.Set( MATH_INFINITE, MATH_INFINITE, MATH_INFINITE);
	m_Max.Set( -MATH_INFINITE, -MATH_INFINITE, -MATH_INFINITE);
    //m_Min.set(+1000000.0f,+1000000.0f,+1000000.0f);
    //m_Max.set(-1000000.0f,-1000000.0f,-1000000.0f);
}


inline BOOL BBox3D::IsIntersect		( const BoundingVolume& bv)
{
	DWORD dwStatus = Intersect(bv);
	return dwStatus == NTCT_INTERSECT || dwStatus == NTCT_INSIDE;
}

inline BOOL BBox3D::IsIntersect		( const BBox3D& aabb2)
{
	DWORD dwStatus = Intersect(aabb2);
	return dwStatus == NTCT_INTERSECT || dwStatus == NTCT_INSIDE;
}


inline BOOL BBox3D::IsIntersect	( const Sphere& sphere)
{
	DWORD dwStatus = Intersect(sphere);
	return dwStatus == NTCT_INTERSECT || dwStatus == NTCT_INSIDE;
}


// point in polygon check for sides with constant x,y and z
inline BOOL BBox3D::CheckPointInPolygonX(const Vector3D& p) const
{
	if ((p.y>=m_Min.y)&&(p.y<=m_Max.y)&&(p.z>=m_Min.z)&&(p.z<=m_Max.z)) return TRUE;
	return FALSE;
}
inline BOOL BBox3D::CheckPointInPolygonY(const Vector3D& p) const
{
	if ((p.x>=m_Min.x)&&(p.x<=m_Max.x)&&(p.z>=m_Min.z)&&(p.z<=m_Max.z)) return TRUE;
	return FALSE;
}
inline BOOL BBox3D::CheckPointInPolygonZ(const Vector3D& p) const
{
	if ((p.x>=m_Min.x)&&(p.x<=m_Max.x)&&(p.y>=m_Min.y)&&(p.y<=m_Max.y)) return TRUE;
	return FALSE;
}

#endif //__BBOX3D_INL__
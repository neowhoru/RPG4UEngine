/*////////////////////////////////////////////////////////////////////////
文 件 名：TileLand.h
创建日期：2006年6月23日
最后更新：2006年6月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TILELAND_H__
#define __TILELAND_H__
#pragma once

#include "TileLandDefine.h"
#include "TileMapDefine.h"
#include "TileOctreeDefine.h"
#include "PathHandler.h"
#include "SpecialAreaIterator.h"

class DDrawRender;
class Archive;
class UnitBase;
class V3DGameMap;

namespace tile
{
class		PathHandler;
class		TilePathFinder;
class		TileMapCombination;

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
class _TILEWORLDLIB_API TileLand 
{
public:
	TileLand();
	~TileLand();

public:
	//Factory类型
	virtual TileLandType GetFactoryType() = 0;

public:
	virtual BOOL Init		(LPCSTR szLandName,INT width, INT height,int nMapTiles,float fTileSize)=0 ;
	virtual void Release	() = 0;

	virtual BOOL Load		(LPCSTR	szLandName
								,INT		width
								,INT		height)=0;

public:
	//virtual BOOL Serialize	( Archive *pArchive)=0;
	//virtual BOOL Load			( LPCSTR szFileNoExt)=0;

public:
	virtual const BBox3D&		GetTerrainSize	() = 0;
	virtual DWORD					GetCheckSum		() = 0;

	virtual BOOL	SetActiveRoom( int iRoomIndex)=0;
	virtual int		DuplicateRoomInfo( int iSourceIndex = 0)=0;
	virtual BOOL	DestroyRoomInfo( int iRoomIndex)=0;	

	virtual TilePathFinder* GetPathFinder() = 0;

	virtual OCT_INDEX	GetTileOctIndex( int iTile) = 0;	///< Tile 
	//virtual int				GetTileForTerrainMesh( int iTerrainMesh) = 0;	///< 

	virtual void PathHandleReset(PathHandler *ppe,
										const Vector3D& wvPos, 
										int iTile = -1) = 0;	///< 3D 

	virtual ePATH_RESULT PathHandleFindPath(PathHandler *ppe, 
										const Vector3D& wvDest,
										int iDestTile,
										WORD wAttribute = PTA_CANNT_MOVE,
										float				 fSeekRange = PATHFIND_SEEK_RANGE)= 0;	///< 

	virtual BOOL PathHandleSetPath(PathHandler *ppe,
										Vector3D *pwvDest,
										int *pTileRoute,
										int iNumTileRoute) = 0;	///< 

	virtual BOOL PathHandleFrameMove( PathHandler *ppe,
										float fMove,
										sPATHFIND_EVENT *pEvent = NULL) = 0;	///< 
	virtual int PathHandleGetMeshTileStand( Vector3D wvPos,
										float *pfT,
										float fMeshTileDist = -1.0f,
										DWORD dwTerrainGroupOptionCheck = 0) = 0;	///< 3D 

	//virtual void PathExp_ApplyMoveByAnimation( PathHandler *ppe, 
	//									DDrawRenderObject *pUnitDraw) = 0;	///< 

	virtual ePATH_RESULT PathHandleThrustPath( PathHandler *ppe, 
										Vector3D *pwvMoveDistance,
										WORD wAttribute = PTA_CANNT_MOVE) = 0;	///< 


public:
	virtual int			PickPathTile(Ray *	pwr 
											,float *	pfT		= NULL
											,BOOL		bCulling = TRUE) = 0;	///< 

	virtual Vector3D	GetRandomPosInTile	( int iTile) = 0;	///< 
	virtual BOOL		CorrectHeightByTile	( int iTile, Vector3D *pwvPos) = 0;

public:
	virtual OCT_INDEX	GetOctIndexToAddObject	( BoundingVolume *	pwbv
																, OCT_INDEX			woiFrom = 0)= 0;
	virtual OCT_INDEX	GetOctIndexToSearch		( OCT_INDEX woi)						= 0;


	virtual OCT_INDEX	AddObject					( E_OCTNODE_DATTYPE	dto
																, DWORD					dwObject
																, OCT_INDEX			woiIndex)	= 0;
	virtual OCT_INDEX	AddObject					( E_OCTNODE_DATTYPE	dto
																, DWORD					dwObject
																, BoundingVolume *	pwbv)			= 0;


	virtual BOOL			RemoveObject				( E_OCTNODE_DATTYPE	dto
																, DWORD					dwObject
																, OCT_INDEX			woiIndex)	= 0;
	virtual BOOL			RemoveObject				( E_OCTNODE_DATTYPE	dto
																, DWORD					dwObject
																, BoundingVolume *	pwbv)			= 0;


	virtual BOOL			IsContainable				( OCT_INDEX			woiIndex
																, BoundingVolume *	pwbv)			= 0;

public:
	virtual TILEINDEX GetTileToStand	(const VECTOR3D&	wvPos
										,float *				pfT = NULL
										,float				fJumpLimit		= -1.0f
										,float				fAdjustLimit	= -1.0f
										,WORD *				pwAttribute		= NULL) = 0;	

	virtual void SetAttribute	( int iTile, WORD wAttribute) = 0;
	virtual void UnsetAttribute( int iTile, WORD wAttribute) = 0;
	virtual BOOL CheckAttribute( int iTile, WORD wAttribute) = 0;
	virtual WORD GetAttribute	( int iTile) = 0;


public:
	virtual void	ForEachSpecialArea			(SpecialAreaIterator&)	= 0;
	virtual BOOL	AddSpecialArea					(SpecialArea* pSpecialArea
                                             ,int            nMapX=-1
															,int            nMapY=-1)= 0;

	virtual BOOL	RemoveSpecialArea				(IDTYPE wiIdentity
                                             ,int  nMapX
															,int  nMapY
															,BOOL bLocal)= 0;

	//virtual BOOL	RemoveSpecialAreaByIndex	( int iIndex) = 0;
	virtual void	RemoveAllSpecialArea			() = 0;
	virtual int		GetNumberOfSpecialArea		() = 0;

	virtual SpecialArea* GetSpecialArea			(IDTYPE wiIdentity
                                                ,int  nMapX=-1
																,int  nMapY=-1)= 0;

	virtual SpecialArea* GetSpecialAreaByIndex	(int iIndex
                                                ,int nMapX
																,int nMapY)= 0;

};
};//namespace tile



#endif //__TILELAND_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS.h
创建日期：2008年9月11日
最后更新：2008年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
// *  前缀
//		C : Client
//		G : Game Server
//		M : Master Server
//		D : DBP Server
//
// * 后缀
//		SYN - Server同步协议，ACK协议接受处理，NAK协议拒绝处理
//		ACK - SYN协议接受处理
//		NAK - SYN协议拒绝处理
//		CMD - Server指令，无须返回响应
//		BRD - Server向外广播消息
//		DBR - DBProxy数据库代理向外广播消息
//
// * 协议命名规则：
//		前缀_分类_协议_后缀
//		譬如) CONNECTION_REGISTER_SYN
//
版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_H__
#define __PACKETSTRUCT_CLIENTGAMES_H__
#pragma once


#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "ConstItem.h"
#include "ConstItemComposite.h"
#include "PacketStruct_ClientGameS_Define.h"


#include "PacketStruct_ClientGameS_Connection.h"
#include "PacketStruct_ClientGameS_CharInfo.h"
#include "PacketStruct_ClientGameS_Sync.h"
#include "PacketStruct_ClientGameS_SKill.h"
#include "PacketStruct_ClientGameS_Map.h"
#include "PacketStruct_ClientGameS_Item.h"
#include "PacketStruct_ClientGameS_Player.h"



#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
_CG_GM
/*////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_MONSTER_CREATE_SYN)
//{
    MONSTERCODE	m_dwMonsterCode;
    BYTE				m_byMonsterCnt;
    float			m_fPos[3];	
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_MONSTER_DESTROYALL_SYN)
//{
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_MONSTER_DESTROYNEAR_SYN)
//{
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_OBSERVER_SYN )
//{
    BYTE				m_byObserverOn;		// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_OBSERVER_BRD )
//{
	DWORD				m_dwObjectKey;
    BYTE				m_byObserverOn;		// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_OBSERVER_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


// 公利惑怕肺 傈券
//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_UNDEAD_MODE_SYN )
//{
	bool				m_bUndeadOn;		// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_UNDEAD_MODE_ACK )
//{
	bool				m_bUndeadOn;		// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_UNDEAD_MODE_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_CHARINFO_SYN)
//{
    DWORD			m_dwUserID;	
    char				m_szCharName[MAX_CHARNAME_LENGTH];
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_CHARINFO_ACK)
//{
	ePLAYER_TYPE		m_eCharType;// 
    WORD					m_wLevel;			// 
    MONEY				m_Money;			// 

	KEYTYPE				m_RoomKey;		// 
	CODETYPE				m_MapCode;			// 
	VECTOR3D				m_vCurPos;			// 
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_CHARINFO_NAK)
//{
	DWORD				m_dwErrorCode;		// eGM_RESULT
//};
MSGPACKET_CG_OBJECT_END;

// 老馆 酒捞袍 积己
//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_CREATE_ITEM_SYN)
//{
    SLOTCODE			m_ItemCode;				// 
    DWORD				m_dwItemCount;			// 
//};
MSGPACKET_CG_OBJECT_END;


// 牢镁飘 酒捞袍 积己
//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_CREATE_ENCHANT_ITEM_SYN)
//{
    SLOTCODE			m_ItemCode;					
    DWORD				m_dwItemCount;				
    BYTE				m_byEnchant;					
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_DESTROY_ITEM_AROUND_SYN)
//{
//};
MSGPACKET_CG_OBJECT_END;


// 鞘靛俊 捣 积己
//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_CREATE_MONEY_SYN)
//{
    MONEY				m_Money;			// (1 ~ GM_MAX_CREATE_MONEY)
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_LEVEL_UP_SYN)
//{
    WORD				m_wBonusLevel;	
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_STAT_UP_SYN)
//{
    DWORD				m_dwBonusStat;		// -GM_MAX_STAT_UP_VALUE ~ GM_MAX_STAT_UP_VALUE
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_STAT_UP_ACK)
//{
    DWORD				m_dwRemainStat;
//};
MSGPACKET_CG_OBJECT_END;

// 胶懦器牢飘 诀
//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_SKILLPOINT_UP_SYN)
//{
    DWORD				m_dwBonusSkill;		// -GM_MAX_SKILLPOINT_UP_VALUE ~ GM_MAX_SKILLPOINT_UP_VALUE
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN(GM_SKILLPOINT_UP_ACK)
//{

    DWORD				m_dwRemainSkill;
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RESURRECTION_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RESURRECTION_NAK )
//{
DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RESURRECTION_BRD )
//{
	DWORD						m_dwObjectKey;				
	DWORD						m_dwHP;						
	DWORD						m_dwMP;						
	VECTOR3D					m_vCurPos;					
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RECOVERY_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RECOVERY_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RECOVERY_BRD )
//{
	DWORD						m_dwObjectKey;		
	DWORD						m_dwHP;				
	DWORD						m_dwMP;				
//}
MSGPACKET_CG_OBJECT_END;

								 


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_SET_SPEED_SYN )
//{
	BYTE						m_byMoveLevel;			
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_SET_SPEED_BRD )
//{
	DWORD						m_dwObjectKey;				
	BYTE						m_byMoveLevel;				
	FLOAT						m_fSpeedRatio;								
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_SET_SPEED_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_ENTRANCE_SYN )
//{			
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_ENTRANCE_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_FORCE_DISCONNECT_SYN )
//{	
	DWORD						m_dwUserKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_FORCE_DISCONNECT_ACK )
//{	
	DWORD						m_dwUserKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_FORCE_DISCONNECT_NAK )
//{			
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//漂沥 阁胶磐 力芭(酒捞袍, 版氰摹 绝澜)

MSGOBJECT_CG_GM_BEGIN( GM_MONSTER_DESTROYONE_SYN )
//{
	DWORD						m_dwMonsterKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_MONSTER_DESTROYONE_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_MONSTER_KILLONE_SYN )
//{
	DWORD						m_dwMonsterKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_MONSTER_KILLONE_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_RELOAD_DATA_CMD )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_ROOMINFO_SYN )
//{
	KEYTYPE				m_RoomKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_ROOMINFO_ACK )
//{
	enum CONST_VAL { _MAX_PLAYER_INFO_SIZE = 10 //};
MSGPACKET_CG_OBJECT_END;
	TCHAR				m_szMasterName[MAX_CHARNAME_LENGTH];
	BYTE				m_byCount;
	sCHARINFO_GM	m_pPlayerInfo[_MAX_PLAYER_INFO_SIZE];
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_ROOMINFO_NAK )
//{
	DWORD				m_dwErrorCode;			// eGM_RESULT
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_CHANNELINFO_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_CHANNELINFO_ACK )
//{
	DWORD				m_dwChannelUserNum;	
	WORD				m_wHuntingRoomCount;	
	WORD				m_wMissionRoomCount;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_CHANNELINFO_NAK )
//{
	DWORD				m_dwErrorCode;			// eGM_RESULT
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_WARP_SYN )
//{
	TCHAR				m_szCharName[MAX_CHARNAME_LENGTH];
	bool				m_bWatch;						
	KEYTYPE				m_RoomKey;				
	CODETYPE			m_MapCode;
	VECTOR3D			m_vDestPos;					
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_WARP_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_WARP_NAK )
//{
	BYTE				m_byErrorCode;						// eGM_RESULT
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_STRING_CMD_SYN )
//{
	enum CONST_VAL { MAX_STRING_CMD_LENGTH = 100 //};
MSGPACKET_CG_OBJECT_END;
	TCHAR				m_szStringCmd[MAX_STRING_CMD_LENGTH];	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GM_BEGIN( GM_STRING_CMD_NAK )
//{
	BYTE				m_byErrorCode;								// eGM_RESULT
//}
MSGPACKET_CG_OBJECT_END;



/*////////////////////////////////////////////////////////////////////////
// SUMMON
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_SUMMON_BEGIN( SUMMON_COMMAND_SYN )
//{
	BYTE				m_byCommand;		
	DWORD				m_dwTargetKey;		
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SUMMON_BEGIN( SUMMON_COMMAND_ACK )
//{
	BYTE				m_byCommand;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SUMMON_BEGIN( SUMMON_COMMAND_NAK )
//{
	BYTE				m_byCommand;
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SUMMON_BEGIN( SUMMON_SKILL_ACTION_SYN )
//{
	DWORD				m_dwSummonedObjKey;		
	SLOTCODE			m_SkillCode;				
	DWORD				m_dwClientSerial;			
	VECTOR3D			m_vCurPos;					
	VECTOR3D			m_vDestPos;					
	DWORD				m_dwMainTargetKey;		
	VECTOR3D			m_MainTargetPos;	

	int GetSize() { return SIZE_CG_MSG(SUMMON_SKILL_ACTION_SYN); };
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SUMMON_BEGIN( SUMMON_SKILL_RESULT_BRD )
//{
	BYTE				m_byCurCommand;								
	DWORD				m_dwSummonerObjKey;							
	BYTE				m_bySummonedNum;								
	DWORD				m_dwSummonedObjKey[MAX_SUMMONED_NUM];	

	int	GetSize()
	{
		return SIZE_CG_MSG(SUMMON_SKILL_RESULT_BRD) - sizeof(DWORD) * (MAX_SUMMONED_NUM - m_bySummonedNum);
	}
//}
MSGPACKET_CG_OBJECT_END;







/*////////////////////////////////////////////////////////////////////////
// GUILD
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_SELECT_SYN )
//{	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_SELECT_ACK )
//{
	sGUILD_INFO_PACKET	m_GuildInfo;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_SELECT_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_CREATE_SYN )
//{	
	TCHAR	m_szGuildName[MAX_GUILDNAME_LENGTH];
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_CREATE_ACK )
//{
	GUILDGUID m_GuildGuid;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_CREATE_BRD )
//{
	PLAYERKEY	m_MasterPlayerKey;		
	TCHAR		m_szGuildName[MAX_GUILDNAME_LENGTH];
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_CREATE_NAK )
//{
	BYTE	m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DESTROY_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DESTROY_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DESTROY_BRD )
//{
	PLAYERKEY	m_MasterPlayerKey;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DESTROY_NAK )
//{
	BYTE	m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_SYN )
//{
	PLAYERKEY			m_PlayerKey;							
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_ACCEP_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_REJECT_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_REQUEST_CMD )
//{
	PLAYERKEY			m_PlayerKey;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_ACCEPT_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_REJECT_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_ANSWER_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_INVITE_ANSWER_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_JOIN_SUCCESS_CMD )
//{
	GUILDGUID			m_GuildGuid;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_JOIN_SUCCESS_BRD )
//{
	PLAYERKEY			m_PlayerKey;
	TCHAR				m_szGuildName[MAX_GUILDNAME_LENGTH];
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_JOIN_FAILED_CMD )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_WITHDRAW_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_WITHDRAW_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_WITHDRAW_BRD )
//{
	PLAYERKEY			m_PlayerKey;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_WITHDRAW_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_GRANT_LOW_POSITION_SYN )
//{
	DWORD				m_dwLowPlayerKey;		
	BYTE				m_GuildPosition;		
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_GRANT_LOW_POSITION_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_GRANT_LOW_POSITION_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DROP_LOW_POSITION_SYN )
//{		
	DWORD				m_dwLowPlayerKey;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DROP_LOW_POSITION_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_GUILD_BEGIN( GUILD_DROP_LOW_POSITION_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;






#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_H__
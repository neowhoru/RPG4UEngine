/*////////////////////////////////////////////////////////////////////////
文 件 名：MathAngle.h
创建日期：2008年4月10日
最后更新：2008年4月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHANGLE_H__
#define __MATHANGLE_H__
#pragma once

#include "MathDefine.h"
#include "MathFunction.h"
#include "MathSinFast.h"

//namespace math
//{ 
	class Degree;
	///////////////////////////////////////////////
	//弧度
	class Radian
	{
		REAL m_rad;
	public:

		explicit Radian ( REAL r=0 ) 
			: m_rad(r) {}
		Radian ( const Degree& d );

		Radian& operator = ( const REAL& f )	{ m_rad = f;			return *this; }
		Radian& operator = ( const Radian& r ) { m_rad = r.m_rad;	return *this; }
		Radian& operator = ( const Degree& d );

		REAL DegreeValue() const; // see bottom of this file
		REAL RadianValue() const { return m_rad; }
		REAL AngleUnitValue() const;

		//单元运算
      const Radian& operator + () const	{ return *this; }
		Radian        operator - () const	{ return Radian(-m_rad); }

		Radian  operator + ( const Radian& r ) const { return Radian ( m_rad + r.m_rad ); }
		Radian  operator + ( const Degree& d ) const;

		Radian& operator += ( const Radian& r ) { m_rad += r.m_rad; return *this; }
		Radian& operator -= ( const Radian& r ) { m_rad -= r.m_rad; return *this; }
      Radian  operator *  ( const Radian& f ) const { return Radian ( m_rad * f.m_rad ); }
		Radian  operator -  ( const Radian& r ) const { return Radian ( m_rad - r.m_rad ); }


		Radian& operator += ( const Degree& d );
		Radian& operator -= ( const Degree& d );
		Radian  operator -  ( const Degree& d ) const;

		Radian& operator *= ( REAL f )			{ m_rad *= f; return *this; }
		Radian& operator /= ( REAL f )			{ m_rad /= f; return *this; }
		Radian  operator *  ( REAL f ) const	{ return Radian ( m_rad * f ); }
		Radian  operator /  ( REAL f ) const	{ return Radian ( m_rad / f ); }

		BOOL operator <  ( const Radian& r ) const { return m_rad <  r.m_rad; }
		BOOL operator <= ( const Radian& r ) const { return m_rad <= r.m_rad; }
		BOOL operator == ( const Radian& r ) const { return m_rad == r.m_rad; }
		BOOL operator != ( const Radian& r ) const { return m_rad != r.m_rad; }
		BOOL operator >= ( const Radian& r ) const { return m_rad >= r.m_rad; }
		BOOL operator >  ( const Radian& r ) const { return m_rad >  r.m_rad; }

		//工具类
		Radian	Abs ();
		Radian	Sign();
		Radian	Sqrt();
		REAL		Sin (BOOL bFast=FALSE);
		REAL		Cos (BOOL bFast=FALSE);
		REAL		Tan (BOOL bFast=FALSE);

		static			Radian ACos (REAL fValue);
		static			Radian ASin (REAL fValue);
		static inline	Radian ATan (REAL fValue);
		static inline	Radian ATan2(REAL fY, REAL fX);

	};


	/////////////////////////////////////////////////////////////
	//角度
	class Degree
	{
		REAL m_degree; // if you get an error here - make sure to define/typedef 'REAL' first

	public:
		explicit Degree ( REAL d=0 ) 
			: m_degree(d) {}
		Degree ( const Radian& r ) 
			: m_degree(r.DegreeValue()) {}

		Degree& operator = ( const REAL& f )	{ m_degree = f; return *this; }
		Degree& operator = ( const Degree& d ) { m_degree = d.m_degree; return *this; }
		Degree& operator = ( const Radian& r ) { m_degree = r.DegreeValue(); return *this; }


		REAL DegreeValue() const { return m_degree; }
		REAL RadianValue() const;
		REAL AngleUnitValue() const;


		const Degree&	operator + () const { return *this; }
		Degree			operator - () const { return Degree(-m_degree); }

		Degree& operator += ( const Degree& d )			{ m_degree += d.m_degree; return *this; }
		Degree& operator -= ( const Degree& d )			{ m_degree -= d.m_degree; return *this; }
		Degree  operator	+ ( const Degree& d ) const { return Degree ( m_degree + d.m_degree ); }
		Degree  operator	- ( const Degree& d ) const { return Degree ( m_degree - d.m_degree ); }
      Degree  operator  * ( const Degree& f ) const { return Degree ( m_degree * f.m_degree ); }

		Degree  operator +  ( const Radian& r ) const { return Degree ( m_degree + r.DegreeValue() ); }
		Degree  operator -  ( const Radian& r ) const { return Degree ( m_degree - r.DegreeValue() ); }
		Degree& operator += ( const Radian& r ) { m_degree += r.DegreeValue(); return *this; }
		Degree& operator -= ( const Radian& r ) { m_degree -= r.DegreeValue(); return *this; }

		Degree  operator *  ( REAL f ) const { return Degree ( m_degree * f ); }
		Degree  operator /  ( REAL f ) const { return Degree ( m_degree / f ); }
		Degree& operator *= ( REAL f ) { m_degree *= f; return *this; }
		Degree& operator /= ( REAL f ) { m_degree /= f; return *this; }

		BOOL operator <  ( const Degree& d ) const { return m_degree <  d.m_degree; }
		BOOL operator <= ( const Degree& d ) const { return m_degree <= d.m_degree; }
		BOOL operator == ( const Degree& d ) const { return m_degree == d.m_degree; }
		BOOL operator != ( const Degree& d ) const { return m_degree != d.m_degree; }
		BOOL operator >= ( const Degree& d ) const { return m_degree >= d.m_degree; }
		BOOL operator >  ( const Degree& d ) const { return m_degree >  d.m_degree; }

		//工具类
		Degree	Abs  ();
		Degree	Sign ();
		Degree	Sqrt () ;
	};


	////////////////////////////////////////////////////
	//夹角
	class Angle
	{
		REAL m_angle;

	public:
		explicit Angle ( REAL angle ) : m_angle(angle) {}
		operator Radian() const;
		operator Degree() const;

	public:
	 static inline void SetDegree()	{ms_angleUnit = AU_DEGREE;}
 	 static inline void SetRadian()	{ms_angleUnit = AU_RADIAN;}
	 static inline BOOL IsDegree()	{return ms_angleUnit == AU_DEGREE;}
 	 static inline BOOL IsRadian()	{return ms_angleUnit == AU_RADIAN;}

    static REAL AngleUnit2Radian(REAL units);
    static REAL Radian2AngleUnit(REAL radians);
    static REAL AngleUnit2Degree(REAL units);
    static REAL Degree2AngleUnit(REAL degrees);

	protected:
       enum AngleUnit
       {
           AU_DEGREE,
           AU_RADIAN
       };

    protected:
       static AngleUnit ms_angleUnit;
	};
	



	////////////////////////////////////////////////////
	//全局操作符定义
	Radian operator * ( REAL a, const Radian& b );
	Radian operator / ( REAL a, const Radian& b );
	Degree operator * ( REAL a, const Degree& b );
	Degree operator / ( REAL a, const Degree& b );


//};//namespace math

#include "MathAngle.inl"

#endif //__MATHANGLE_H__
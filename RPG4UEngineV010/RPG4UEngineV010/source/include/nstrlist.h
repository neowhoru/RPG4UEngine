#ifndef N_STRLIST_H
#define N_STRLIST_H
//------------------------------------------------------------------------------
/**
    @brief A doubly linked list for named nodes with slow linear search.

    @author
    - RadonLabs GmbH 

    @since
    - 2005.6.30
    @remarks
    - ���� �߰� 
*/

#include <string.h>
#include "CommonDefine.h"
#include "nlist.h"
#include "nstrnode.h"

//------------------------------------------------------------------------------
class _BASE_API nStrList: public ListBase 
{
public:    
    /// return first element of list
    nStrNode* GetHead() const;
    /// return last element of list
    nStrNode* GetTail() const;
    /// remove first element of list
    nStrNode* RemoveHeader();
    /// remove last element of list
    nStrNode* RemoveTail();
    /// search for named element (slow)
    nStrNode* Find(const char* str) const;
};

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrList::GetHead() const
{
    return (nStrNode *) this->ListBase::GetHead();
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrList::GetTail() const
{
    return (nStrNode *) this->ListBase::GetTail();
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrList::RemoveHeader()
{
    return (nStrNode *) this->ListBase::RemoveHeader();
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrList::RemoveTail()
{
    return (nStrNode *) this->ListBase::RemoveTail();
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrList::Find(const char* str) const
{
    nStrNode* n;
    for (n = this->GetHead(); n; n = n->GetNext()) 
    {
        const char* nodeName = n->GetName();
        ASSERT(nodeName);
        if (strcmp(str, nodeName) == 0) 
        {
            return n;
        }
    }
    return 0;
};

//------------------------------------------------------------------------------
#endif

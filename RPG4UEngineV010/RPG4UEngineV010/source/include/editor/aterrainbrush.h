/*////////////////////////////////////////////////////////////////////////
文 件 名：ATerrainBrush.h
创建日期：2007年10月24日
最后更新：2007年10月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ATERRAINBRUSH_H__
#define __ATERRAINBRUSH_H__
#pragma once



////////////////////////////////////////////
struct sBRUSH_INFO
{
	enum eOPR
	{
		 OPR_ADD
		,OPR_EREASE
		,OPR_SET
		,OPR_PAINTMAX	//取消最大值，做地面刷时用到
		,OPR_PAINTMIN	//取消最小值
		,OPR_CLEAR
		,OPR_FADECLEAR
	};

	INT		nType;		//笔刷类型
	INT		nSize;		//笔刷大小
	INT		nShape;		//形状
	INT		nRadius;	//半径
	eOPR		oprType;		//增加，减小 

	Vector3D vPos;	//点击的坐标

	sBRUSH_INFO()
		:nType(0)
		,nSize(0)
		,nShape(0)
		,nRadius(0)
		,oprType(OPR_ADD)
	{
	}
};

enum eEXCLUSIVE_TYPE
{
	 EXCLUSIVE_NONE
	,EXCLUSIVE_CLEAR			//清除它层
	,EXCLUSIVE_ROUND_EDGE	//用它层做圆边界
	,EXCLUSIVE_BLEND			//多层混合
	,EXCLUSIVE_BLEND2			//多层混合
	,EXCLUSIVE_EDGE			//它层清除余边界
	,EXCLUSIVE_DIRTY			//它层随机脏块
	,EXCLUSIVE_RANDOM			//随机块
	,EXCLUSIVE_HALF			//双层平均
};

enum ePAINT_TYPE
{
	 PAINT_NONE
	,PAINT_ALPHA
	,PAINT_SHADOW
	,PAINT_DIFFUSE
	,PAINT_TILEMASK
	,PAINT_LAYOUT
	,PAINT_DAYNIGHT
	,PAINT_MAX
};



struct sBRUSH_TERRAIN : public sBRUSH_INFO
{
	FLOAT 				fAlphaWeight; 
	UINT					uiTextureIdx;		//
	UINT					nAlphaLayerIdx;	//
	FLOAT					fAlphaValue;
	eEXCLUSIVE_TYPE	m_Exclusive;		//互斥  改变时，其它通道的alpha值共为255
	ePAINT_TYPE			m_PaintType;
	DWORD					dwDiffuse;			//颜色
	DWORD					dwUpdateFlags;		
	UINT					uTileMaskType;		//TileMask类型

	VOID*					Buffer;

	sBRUSH_TERRAIN()
		:fAlphaWeight(0.f)
      ,uiTextureIdx(0)
		,m_PaintType(PAINT_ALPHA)
		,nAlphaLayerIdx(0)
		,fAlphaValue(0)
		,m_Exclusive(EXCLUSIVE_NONE)
		,dwDiffuse(0xFFFFFFFF)
		,Buffer(0)
	{}
};


#endif //__ATERRAINBRUSH_H__
#pragma once
#include "assert.h"
//
#define UNDOSIZE 100	//uodo级别

#include <deque>


template <class T>
class TUndoHistory
{
	std::deque<int>	m_dequeStepOpCount;//每一步包含的op的个数
	std::deque<T>		m_dequeOp;//OP纪录
	int					m_iStepOpCount;
	UINT					m_iUndoSize;

public:
	TUndoHistory(void)
	{
		//
		m_iStepOpCount = 0;
		m_iUndoSize = 256;
	}
	~TUndoHistory(void)
	{
		Reset();
	}

	void SetUndoSize( int iSize )
	{
		m_iUndoSize = iSize;
	}
	void Reset(void)
	{
		m_dequeOp.clear();//.erase( m_dequeOp.begin(),m_dequeOp.end() );
		m_dequeStepOpCount.clear();//.erase( m_dequeStepOpCount.begin(),m_dequeStepOpCount.end() );
		m_iStepOpCount = 0;
	}

	void BeginRecord()
	{
		if( 0 != m_iStepOpCount )
		{
			//
			EndRecord();			
		}
		m_iStepOpCount = 0;
	}


	void EndRecord()
	{
		if( 0 == m_iStepOpCount )
			return;
		m_dequeStepOpCount.push_back( m_iStepOpCount );
		m_iStepOpCount = 0;
	}

	void AddOp( T* t )
	{
		if( m_dequeStepOpCount.size() > m_iUndoSize )
		{
			//undo队列满
			int iStepOpCount = m_dequeStepOpCount[0];
			m_dequeStepOpCount.pop_front();

			std::deque<T>::iterator it;
			it = m_dequeOp.begin();
			while( iStepOpCount > 0 )
			{
				it ++;
				iStepOpCount --;
			}
			m_dequeOp.erase( m_dequeOp.begin(),it );
		}
		m_iStepOpCount ++;		
		m_dequeOp.push_back( *t );
	}

	//先进后出
	T* GetOpOfStep( int iIndex )
	{
		if( m_iStepOpCount != 0 )
		{
			//纪录的过程中调用
			assert( false );
			return 0;
		}
		if( 0 == m_dequeStepOpCount.size() )
			return 0;
		int iOpCount = m_dequeOp.size();		
		int iStepOpCount = m_dequeStepOpCount[ m_dequeStepOpCount.size() - 1 ];
		if( iIndex >= iStepOpCount )
		{
			assert( false );
			return 0;
		}
		return &m_dequeOp[ iOpCount - iIndex - 1 ];
	}

	void DeleteStep()
	{
		//删去一步
		if( m_iStepOpCount != 0 )
		{
			//纪录的过程中调用
			assert( false );
			return;
		}
		int iStepCount = m_dequeStepOpCount.size();
		if( 0 == iStepCount )
			return;
		int iOpCount = m_dequeOp.size();
		int iStepOpCount = m_dequeStepOpCount[iStepCount - 1 ];
		if( iOpCount < iStepOpCount )
		{
			assert( false );
			return;
		}
		m_dequeStepOpCount.pop_back();
		std::deque<T>::iterator it;
		it = m_dequeOp.end();
		while( iStepOpCount > 0 )
		{
			it --;
			iStepOpCount --;
		}
		m_dequeOp.erase( it,m_dequeOp.end() );
	}

	int GetOpCountOfStep()
	{
		if( 0 == m_dequeStepOpCount.size() )
			return 0;
		return m_dequeStepOpCount[ m_dequeStepOpCount.size() - 1 ];
	}
};

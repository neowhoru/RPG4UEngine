/*////////////////////////////////////////////////////////////////////////
文 件 名：MapEditorInfo.h
创建日期：2007年10月5日
最后更新：2007年10月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPEDITORINFO_H__
#define __MAPEDITORINFO_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "MapEditorInfoDefine.h"
#include "TUndoHistory.h"
#include "ATerrainBrush.h"

using namespace v3d;
using namespace v3d::editor;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _MAPEDITORLIB_API MapEditorInfo
{
public:
	MapEditorInfo(void);
	~MapEditorInfo(void);	

public:
	//地表纹理
	void	Map_SetTextureID		( UINT uiTexture, UINT nLayer );
	void	Map_SetBlendTexture	();

public:
	void UndoAddOp				(editor::op* pOp)	;
	void UndoReset				()						;
	void UndoEndRecord		()						;
	void UndoBeginRecord		()						;

	void	AddOp					(HISTORY_TYPE type,editor::op* pOp)	;
	void	Reset					(HISTORY_TYPE type)						;
	void	EndRecord			(HISTORY_TYPE type)						;
	void	BeginRecord			(HISTORY_TYPE type)						;
	int	GetOpCountOfStep	(HISTORY_TYPE type)						;
	void	DeleteStep			(HISTORY_TYPE type)						;
	editor::op*  GetOpOfStep(HISTORY_TYPE type,INT nAt)			;

private:
	void ResetCfg			();
	int LoadCliffSetFile	( const char *sFile );

public:	
	BOOL 	bShowWalkMask;
	BOOL 	bShowGrid;			//是否显示Grid
	BOOL 	bModified;			//地图是否被改动过
	int  	iGridSize;			//1:一个大格子只显示一个边框2：显示4个小格子
	UINT 	uiOp;					//操作
	UINT 	uiRange;				//工具范围1-5
	UINT 	uiRangeType;		//0--方形 1--圆形
	int  	uiCurTexture;		//当前材质
	int	uiCurSubTexture;	//当前子纹理
	int  	uiCurTextureType;
	int  	iCurCliffType;		//当前
	BOOL 	bBlendTexReverse;
	REAL 	fRaiseHeight;

	int  	iCurIDInMPQ;			//当前物体id
	int	iMouseFollowObjID;	//鼠标悬挂的物体
	int	stObjectZAngle;		//当前ZAngle
	BOOL	bRandRotateZ;			//随机旋转开关

	BOOL	bRandScale;				//随机放大

										//如果不是随机Scale,使用这个Scale
	float	fScale;					//当前Scale

	BOOL	bModifyZ;				//是否改变Z

	int	iCurWorldDataID;		//当前世界数据ID
	GUID	guidCurrWorldID;
	int	iGrassType;


	int  iCurWater;			//当前水类型
	int  iCurWaterHeight;	//当前水面高度
	BOOL  bCurWaterBank;	//当前水面高度

	int  iSnippetRadius;		
	//	int  iSnippetID;//小片高度值ID
	int  iHeight;

	int				iCurSceneObjectType;
	ePAINT_TYPE		paintType;
	DWORD				dwDiffuse;	//颜色
	DWORD				dwUpdateFlags;
	//UINT				uTileMaskType;

protected:
	TUndoHistory<editor::op>		m_arUndoHistory[WS_HISTORY_MAX];//undo,redo;//undo和redo信息


public:
	VG_UINT_PROPERTY	(BrushRadius);
	VG_UINT_PROPERTY	(BrushType);	//笔刷类型
	VG_UINT_PROPERTY	(BrushBlend);	//混合值

public:
	short *m_pBrushBuffer;
	UINT	m_nEditerMode;
	FLOAT	fAlphaModulus;

	FLOAT	fAlphaValues;

	UINT	m_nLayer;//地形
	//	FLOAT	fAlpha;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(MapEditorInfo , _MAPEDITORLIB_API);
//extern _MAPEDITORLIB_API MapEditorInfo& singleton::GetMapEditorInfo();
#define  theMapEditorInfo singleton::GetMapEditorInfo()

#include "MapEditorInfo.inl"

#endif //__MAPEDITORINFO_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：EditorCamera.h
创建日期：2007年10月25日
最后更新：2007年10月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	以前的camera追随，除了x,y跟随外，Z也跟着变化
	感觉画面比较晃
	现在考虑减缓z的变化


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __EDITORCAMERA_H__
#define __EDITORCAMERA_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "V3DFrustum.h"

/*////////////////////////////////////////////////////////////////////////
//先用角度决定 Camera的vEye,eAt,然后加上偏移
/*////////////////////////////////////////////////////////////////////////
class _MAPEDITORLIB_API EditorCamera
{
public:
	enum eMODE 	{	XYZ_X	,XYZ_Y,XYZ_Z	};

public:
	EditorCamera(void);
	~EditorCamera(void);

public:
	BOOL SaveInfo(LPCSTR szFile);
	BOOL LoadInfo(LPCSTR szFile);

	//根据角度计算camera
	void CaluCamera	(const Vector3X* pvEye=NULL,const Vector3X* pvLookAt=NULL);
	void SetWindowSize( int iWidth,int iHeight );
	void Pick			( Vector3X& vPos,Vector3X& vDir,int x,int y);
	void Pick			( Ray& ray,int x,int y);
	void GetStdVector	( eMODE nMode, Vector3X &vDir );
	void EnableLimit	( BOOL bEnable = TRUE );

public:
	Vector3X GetCameraPos();
	void		SetCameraPos				( Vector3X vPos,BOOL bCalu = TRUE );
	void		TestCameraLookat			(Vector3X vEye,Vector3X vLookat);
	void		CheckTestCameraLookat	();

public:
	
	void ResetCamera		(Vector3X* pvPos=NULL);//重新将摄象机归位
	void ResetToTopView	( void );//设置成顶视角
	void ResetToGameView	( void );//设置成游戏视角

	void IncAngle( float fXY,float fYZ );

	//X:camera的x轴 Y:世界Z X camera's X Z:camera's Z
	void OffsetCamera			( float fX,float fY,float fZ, float d  );	
	void ReportCameraInfo	(void);


	const Vector3X&	GetLookAt(void);
	void					SetLookAt( Vector3X vLookAt );


protected:
	void _CheckCameraInfo( Vector3X vIn,Vector3X &vOut );
	void _CalcCameraInfo	( Vector3X &vEye,Vector3X &vLookat );


protected:
	struct sCAMERA_DATA
	{
		float fRadius;
		float fAngleXY,fAngleYZ;
		Vector3X vOff,vViewOff;//偏移,和View偏移
	};
	enum
	{
		ZF_NORMAL = 0			//普通模式
		,ZF_SHIFTTO				//切换Z值
	};


protected:
	float m_fMaxRadius,	m_fMinRadius;
	float m_fMaxAngleYZ,	m_fMinAngleYZ;
	BOOL  m_bEnableLimit;

	sCAMERA_DATA	m_data,
						m_dataBack,
						m_dataTestBak;
	BOOL				m_bTestCamera;

	VG_TYPE_PROPERTY	(ViewMatrix,		Matrix4X);
	VG_TYPE_PROPERTY	(ProjectMatrix,	Matrix4X);
	VG_TYPE_PROPERTY	(BillboardMatrix,	Matrix4X);
	VG_TYPE_PROPERTY	(StandboardMatrix,Matrix4X);

	VG_TYPE_PROPERTY	(Frustum,			V3DFrustum);
	VG_VECTOR_PROPERTY(Eye,					Vector3X);
	VG_VECTOR_PROPERTY(Dir,					Vector3X);

	//需要外部更新view矩阵
	VG_BOOL_PROPERTY	(NeedUpdateViewMatrix);

	int					m_iZFollowStatus;
	float					m_fNextZLGate,
							m_fNextZHGate;			//Z变化的区间


	int					m_iWidth,
							m_iHeight;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(EditorCamera , _MAPEDITORLIB_API);
//extern _MAPEDITORLIB_API EditorCamera& singleton::GetEditorCamera();
#define theEditorCamera  singleton::GetEditorCamera()



#include "EditorCamera.inl"

#endif //__EDITORCAMERA_H__
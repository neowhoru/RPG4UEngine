/*////////////////////////////////////////////////////////////////////////
文 件 名：InputDefine.h
创建日期：2008年3月19日
最后更新：2008年3月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	输入控制定义

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __INPUTDEFINE_H__
#define __INPUTDEFINE_H__
#pragma once

#include "ConstInput.h"


namespace input
{ 


#define		MAX_MOUSE_BUFFER			64

enum
{
	IKS_NONE = 0x01,
	IKS_DOWN = 0x02,
	IKS_KEY  = 0x04,
	IKS_UP   = 0x08,

	KS_NONE  = 0x10,
	KS_DOWN  = 0x20,
	KS_KEY	 = 0x40,
	KS_UP	 = 0x80,
};

enum
{
	//IKS_MOUSE_NONE = 0,
	//IKS_MOUSE_DRAG = 0x0100,
	IKS_MOUSE_NONE = 0x0100,
	IKS_MOUSE_DOWN = 0x0200,
	IKS_MOUSE_KEY  = 0x0400,	
	IKS_MOUSE_UP   = 0x0800,

	//KS_MOUSE_NONE  = 0,
	//KS_MOUSE_DRAG  = 0x1000,
	KS_MOUSE_NONE  = 0x1000,
	KS_MOUSE_DOWN  = 0x2000,
	KS_MOUSE_KEY   = 0x4000,	
	KS_MOUSE_UP	   = 0x8000,
};



enum
{
	EXT_MOUSE_BUTTON_NONE			= 0,
	EXT_MOUSE_BUTTON_CLICK_SHORT	= 1,
	EXT_MOUSE_BUTTON_CLICK_MID		= 2,
	EXT_MOUSE_BUTTON_CLICK_LONG		= 3,
};

#define		MOUSE_CLICK_TIME_SHORT		250
#define		MOUSE_CLICK_TIME_MID		500
#define		MOUSE_CLICK_TIME_LONG		1000






enum
{
	INPUT_SUCCESS_ALL = 0,
	INPUT_ERROR_KEY = 1,
	INPUT_ERROR_MOUSE = 2,
};


enum KEYBOARD_LOKC_BIT                       
{ 
	KEYBOARD_LOKC_BIT_NONE			= 0, 
	KEYBOARD_LOKC_BIT_1				= 1,        
	KEYBOARD_LOKC_BIT_2				= (2 << 1), 
	KEYBOARD_LOKC_BIT_3				= (2 << 2), 
	KEYBOARD_LOKC_BIT_4				= (2 << 3), 
	KEYBOARD_LOKC_BIT_5				= (2 << 4), 
	KEYBOARD_LOKC_BIT_6				= (2 << 5), 
	KEYBOARD_LOKC_BIT_7				= (2 << 6), 
	KEYBOARD_LOKC_BIT_8				= (2 << 7), 
	KEYBOARD_LOKC_BIT_9				= (2 << 8), 
	KEYBOARD_LOKC_BIT_10			= (2 << 9), 
	KEYBOARD_LOKC_BIT_11			= (2 << 10), 
	KEYBOARD_LOKC_BIT_12			= (2 << 11), 
};




struct sMOUSEINFO
{
	int nX;
	int nY;
	int nEvent;
	int nKey;
};

struct sMOUSE_INFO
{
    LONG    lX;
    LONG    lY;
    LONG    lZ;
    BYTE    rgbButtons[10];
};


struct sMOUSEDATA
{
    DWORD   dwCode;
    DWORD   dwState;	//按下 true  否则为false
};


#define MAX_QUESIZE 16

enum eMouseEvent
{
	eME_LDOWN,
	eME_RDOWN,
	eME_MDOWN,
	eME_LUP,
	eME_RUP,
	eME_MUP,
	eME_LDBL,
	eME_RDBL,
	eME_MDBL,
	eME_COUNT,
	eME_NONE,
};

};//namespace input

#endif //__INPUTDEFINE_H__


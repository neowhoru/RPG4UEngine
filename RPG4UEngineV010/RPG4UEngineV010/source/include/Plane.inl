/*////////////////////////////////////////////////////////////////////////
文 件 名：Plane.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PLANE_INL__
#define __PLANE_INL__
#pragma once

//namespace math
//{ 

inline Plane::Plane( )
{
}

inline Plane::Plane( CONST REAL* pf )
{
	Set(pf);
}

inline Plane::Plane(const PLANE& plane )
{
	Set(  plane);
}

inline Plane::Plane( REAL fa, REAL fb, REAL fc, REAL fd )
{
	Set(  fa,  fb,  fc, fd );
}

inline Plane::Plane(const Vector3D& v1, const Vector3D& v2, const Vector3D& v3)
{
	Set( v1,  v2,  v3);
}

inline Plane::Plane( const Vector3D& vNormal, const Vector3D& vPoint )
{
	Set( vNormal,  vPoint);
}


inline Plane& Plane::Set(CONST REAL* pf)
{
	assert(pf);
   a = pf[0];
   b = pf[1];
   c = pf[2];
   d = pf[3];
	return *this;
}

inline Plane& Plane::Set(const PLANE& plane )
{
	Set(  plane.a,  plane.b,  plane.c, plane.d );
	return *this;
}

inline Plane& Plane::Set( REAL fa, REAL fb, REAL fc, REAL fd )
{
    a = fa;
    b = fb;
    c = fc;
    d = fd;
	return *this;
}

inline Plane& Plane::Set( const Vector3D& vNormal, const Vector3D& vPoint )
{
	m_Normal = vNormal;
	m_fD		= -vNormal.DotProduct(vPoint);
	return *this;
}

inline Plane& Plane::Set(const Vector3D& v1, const Vector3D& v2, const Vector3D& v3)
{
	Vector3D normal;
	Vector3D v(v1);

	normal	= v.CalculateNormal(v2, v3);
	m_fD		= - normal.DotProduct(v1);

	m_Normal = normal;

    //Vector3D cross((v3 - v1) * (v2 - v1));
    //cross.Normalize();
    //this->a = cross.x;
    //this->b = cross.y;
    //this->c = cross.z;
    //this->d = -(a * v1.x + b * v1.y + c * v1.z);

	return *this;
}

inline Plane& Plane::operator ()( CONST REAL* pf)
{
	return Set(pf);
}

inline Plane& Plane::operator ()( CONST PLANE& plane )
{
	return Set(plane);
}

inline Plane& Plane::operator ()( const Vector3D& vNormal, const Vector3D& vPoint )
{
	return Set(vNormal, vPoint);
}

inline Plane& Plane::operator ()( REAL a, REAL b, REAL c, REAL d )
{
	return Set(a,b,c,d);
}

inline Plane& Plane::operator ()(const Vector3D& v1, const Vector3D& v2, const Vector3D& v3)
{
	return Set(v1,v2,v3);
}


// casting
inline Plane::operator REAL* ()
{
    return (REAL *) &a;
}

inline Plane::operator CONST REAL* () const
{
    return (CONST REAL *) &a;
}


// unary operators
inline Plane Plane::operator + () const
{
    return *this;
}

inline Plane Plane::operator - () const
{
    return Plane(-a, -b, -c, -d);
}


// binary operators
inline BOOL Plane::operator == ( CONST Plane& p ) const
{
    return a == p.a && b == p.b && c == p.c && d == p.d;
}

inline BOOL Plane::operator != ( CONST Plane& p ) const
{
    return a != p.a || b != p.b || c != p.c || d != p.d;
}

inline float Plane::DistanceFrom(const Vector3D& vVertex)
{
	return ( a * vVertex.x + b * vVertex.y + c * vVertex.z + d);
}

inline eSIDE_TYPE Plane::SideFrom (const BBox3D&   box) 
{
	return SideFrom(box.Center(), box.Extents());
}



//};//namespace math


#endif //__TPLANE_INL__
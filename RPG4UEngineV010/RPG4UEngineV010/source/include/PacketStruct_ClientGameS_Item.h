/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Item.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_ITEM_H__
#define __PACKETSTRUCT_CLIENTGAMES_ITEM_H__
#pragma once


#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"

#pragma pack(push,1)


/*////////////////////////////////////////////////////////////////////////
// _CG_ITEM
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_MOVE_SYN  )
//{
	SLOTINDEX					m_fromIndex;
	SLOTINDEX					m_toIndex;
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_MOVE_ACK  )
//{
	SLOTINDEX					m_fromIndex;
	SLOTINDEX					m_toIndex;
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_MOVE_NAK  )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_LINKITEM_SYN  )
//{
	//SLOTINDEX					m_OrgSlotIdx;
	SLOTPOS						m_OrgPos;
	SLOTPOS						m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_LINKITEM_ACK  )
//{
	//SLOTINDEX					m_OrgSlotIdx;
	SLOTPOS						m_OrgPos;
	SLOTPOS						m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_LINKITEM_NAK  )
//{
	DWORD						m_dwErrorCode;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_LINKSKILL_SYN  )
//{
	SLOTCODE					m_SkillCode;
	SLOTPOS						m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_LINKSKILL_ACK  )
//{
	SLOTCODE					m_SkillCode;
	SLOTPOS						m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_LINKSKILL_NAK  )
//{
	DWORD						m_dwErrorCode;	
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_UNLINK_SYN  )
//{
	SLOTPOS						m_atPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_UNLINK_ACK  )
//{
	SLOTPOS						m_atPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_UNLINK_NAK  )
//{
	DWORD						m_dwErrorCode;	
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_MOVE_SYN  )
//{
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_MOVE_ACK  )
//{
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_QUICK_MOVE_NAK  )
//{
	DWORD						m_dwErrorCode;	
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_PICK_SYN  )
//{
	DWORD						m_dwFieldItemObjectKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_PICK_ACK  )
//{
	sTOTALINFO_INVENTORY m_ItemInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(ITEM_PICK_ACK) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_PICK_NAK  )
//{
	DWORD						m_dwErrorCode;
	DWORD						m_dwFieldItemObjectKey;	
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_DROP_SYN  )
//{
	SLOTINDEX					m_atIndex;
	SLOTPOS						m_atPos;
//};
MSGPACKET_CG_OBJECT_END;


//struct MSG_CG_ITEM_DROP_ACK : public MSG_CG_ITEM_DROP_SYN{};
//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(ITEM_DROP_ACK, ITEM_DROP_SYN)
//{
//};
MSGPACKET_CG_SUPER_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_DROP_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_COMBINE_SYN  )
//{
	SLOTINDEX					m_fromIndex;
	SLOTINDEX					m_toIndex;	
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(ITEM_COMBINE_ACK, ITEM_COMBINE_SYN)
//{
//};
MSGPACKET_CG_SUPER_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_COMBINE_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_USE_SYN  )
//{
	SLOTINDEX						m_atIndex;
	SLOTPOS						m_atPos;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(ITEM_USE_ACK, ITEM_USE_SYN)
//{
//};
MSGPACKET_CG_SUPER_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_USE_BRD  )
//{
	CODETYPE		m_PlayerKey;
	CODETYPE		m_ItemCode;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_USE_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SELL_SYN )
//{
	SLOTINDEX					m_atIndex;
	SLOTPOS						m_atPos;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(ITEM_SELL_ACK, ITEM_SELL_SYN)
//{
	MONEY						m_Money;
//};
MSGPACKET_CG_SUPER_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_SELL_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_BUY_SYN )
//{
	DWORD						m_dwShopListID;
	BYTE						m_ShopTabIndex;
	BYTE						m_ShopItemIndex; //< ShopDetailInfo::MAX_SELL_ITEM甫 逞瘤 臼酒具 窃 
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_BUY_ACK )
//{
	MONEY						m_Money;
	sTOTALINFO_INVENTORY		m_TotalInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(ITEM_BUY_ACK) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_TotalInfo.m_InvenCount-m_TotalInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX));
	}
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_BUY_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_REPAIR_SYN )
//{
	SLOTINDEX					m_atIndex;
	SLOTPOS						m_atPos;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(ITEM_REPAIR_ACK, ITEM_REPAIR_SYN)
//{
	MONEY						m_Money;
//};
MSGPACKET_CG_SUPER_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_REPAIR_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_REPAIR_EQUIPS_SYN )
//{
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(ITEM_REPAIR_EQUIPS_ACK, ITEM_REPAIR_EQUIPS_SYN)
//{
	MONEY						m_Money;
//};
MSGPACKET_CG_SUPER_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_REPAIR_EQUIPS_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_OBTAIN_ACK  )
//{
	SLOTPOS						m_ItemPos;
	ITEM_STREAM					m_ItemStream;
//};	
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_OBTAIN_NAK  )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_LOSE_ACK )
//{
	SLOTPOS						m_ItemPos;	
	SLOTPOS						m_ItemNum;	
//}
MSGPACKET_CG_OBJECT_END;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_MAKE_SYN )
//{
	enum CONST_VAL{MAX_NUM_COL=10};
	SLOTCODE			m_DestItemCode;				/// 目标物品，通过目标物品类型、层级确定打造公式
	BYTE				m_byMaterialRow;	///与原料槽列数一样
	char				m_szItemName	[MAX_ITEMNAME2_LENGTH+1];
	union
	{
		DWORD			m_dwMaterialNums;
		struct
		{
			DWORD		m_MaterialNum0	:MATERIAL_ROW_SHIFT;	//< 描述UI槽中摆放的原料数量，每原料容器占3Bit，数量范围[0,8)
			DWORD		m_MaterialNum1	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum2	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum3	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum4	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum5	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum6	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum7	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum8	:MATERIAL_ROW_SHIFT;	//< 
			DWORD		m_MaterialNum9	:MATERIAL_ROW_SHIFT;	//< 
		};
	};

	SLOTPOS			m_arMaterialPos[eITEMENCHANT_MAX_SLOT];	///与原料槽容量一样

	int GetSize()
	{
		INT nBlanks = eITEMENCHANT_MAX_SLOT -	( m_MaterialNum0 
														+ m_MaterialNum1 
														+ m_MaterialNum2
														+ m_MaterialNum3
														+ m_MaterialNum4 
														+ m_MaterialNum5
														+ m_MaterialNum6
														+ m_MaterialNum7
														+ m_MaterialNum8
														+ m_MaterialNum9
														);
		return ( SIZE_CG_MSG(ITEM_MAKE_SYN) - (nBlanks)*sizeof(SLOTPOS) );
	}
	
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_MAKE_ACK )
//{
	SLOTCODE					m_DestItemCode;			/// 目标物品，通过目标物品类型、层级确定打造公式
	MONEY						m_Money;
	sTOTALINFO_INVENTORY	m_ItemInfo;
	int GetSize()
	{
		return (		SIZE_CG_MSG(ITEM_MAKE_ACK)
					-	(sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_MAKE_NAK )
//{
	BYTE					m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_ENCHANT_SYN )
//{
	SLOTPOS				m_TargetPos;					//< 
	BYTE				m_SuccessRateIndex;				//< eSUCCEEDRATE
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_ENCHANT_SUCCESS_ACK )
//{
	BYTE				m_TargetItemEnchant;
	//SLOTPOS				m_TargetPos;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN( ITEM_ENCHANT_FAILED_ACK,ITEM_ENCHANT_SUCCESS_ACK )
//{
MSGPACKET_CG_SUPER_END

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_ENCHANT_NAK )
//{
	BYTE					m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;







//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_ENCHANTUP_SYN )
//{
	SLOTPOS				m_TargetPos;					//<
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_ENCHANTUP_ACK )
//{
	SLOTPOS			m_TargetPos;
	BYTE				m_TargetItemEnchant;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_ENCHANTUP_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_RANKUP_SYN )
//{
	SLOTPOS				m_TargetPos[2];
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_RANKUP_ACK )
//{
	SLOTPOS				m_TargetPos[2];
	ITEMOPT_STREAM		m_TargetItemStream; 
	MONEY				m_Money;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_RANKUP_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;






//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SOCKET_FILL_SYN )
//{
	MATERIALTYPE		m_type;				
	SLOTPOS				m_arSocketItemPos[SOCKET_MAX];
	SLOTPOS				m_TargetPos;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SOCKET_FILL_ACK )
//{
	MATERIALTYPE		m_type;				
	MONEY					m_Money;
	SLOTPOS				m_arSocketItemPos[SOCKET_MAX];
	SLOTPOS				m_TargetPos;
	ITEMOPT_STREAM		m_TargetItemStream;
//}
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SOCKET_FILL_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SOCKET_EXTRACT_SYN )
//{
	MATERIALTYPE		m_type;				
	SLOTPOS				m_TargetPos; 
	DWORD					dwSocketState;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SOCKET_EXTRACT_SUCCESS_ACK )
//{
	MATERIALTYPE		m_type;				
	MONEY					m_Money;
	SLOTPOS				m_TargetPos; 
	DWORD					dwSocketState;
	DWORD					dwSocketResult;


	sTOTALINFO_INVENTORY	m_ItemInfo;
	int GetSize()
	{
		return (		SIZE_CG_MSG(ITEM_SOCKET_EXTRACT_SUCCESS_ACK)
					-	(sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}

//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGSUPER_CG_ITEM_BEGIN(  ITEM_SOCKET_EXTRACT_FAILED_ACK , ITEM_SOCKET_EXTRACT_SUCCESS_ACK)
//{
	//MATERIALTYPE		m_type;				
	//MONEY					m_Money;
	//SLOTPOS				m_TargetPos;
MSGPACKET_CG_SUPER_END




//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_SOCKET_EXTRACT_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_ACCESSORY_CREATE_SYN )
//{
	enum CONST_VAL { LOW_RING = 0, HIGH_RING, LOW_NECKLACE, HIGH_NECKLACE, };

	MATERIALTYPE		m_type;				
	SLOTPOS				m_SocketItemPos; 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_ACCESSORY_CREATE_ACK )
//{
	enum CONST_VAL { LOW_RING = 0, HIGH_RING, LOW_NECKLACE, HIGH_NECKLACE, };
	//enum CONST_VAL { RESULT_MAIN = 0, RESULT_SUBBASE = 1 }; 参考 sITEMCOMPOSITERESULT

	MATERIALTYPE		m_type;				
	SLOTPOS				m_SocketItemPos;
	MONEY					m_Money;
	BYTE					m_byResultPos; /// 参考 sITEMCOMPOSITERESULT
	sTOTALINFO_INVENTORY	m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(ITEM_ACCESSORY_CREATE_ACK) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(  ITEM_ACCESSORY_CREATE_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;





//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_PICK_MONEY_SYN )
//{
	DWORD m_dwFieldItemObjectKey;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_PICK_MONEY_ACK )
//{
	MONEY m_Money;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN( ITEM_PICK_MONEY_NAK )
//{
	DWORD m_dwErrorCode;
	DWORD m_dwFieldItemObjectKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(ITEM_DROP_MONEY_SYN)
//{
	DWORD						m_dwDropMoney;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_ITEM_BEGIN(ITEM_DROP_MONEY_NAK)
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;





#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_ITEM_H__
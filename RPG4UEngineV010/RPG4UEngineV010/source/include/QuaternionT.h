/*////////////////////////////////////////////////////////////////////////
文 件 名：Quaternion.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __QUATERNIONT_H__
#define __QUATERNIONT_H__
#pragma once

#include "TQuaternion.h"

namespace math
{ 
	typedef TQuaternion<float>	QuaternionT;

};//namespace math


#endif //__QUATERNION_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ApplicationBase.h
创建日期：2007年11月8日
最后更新：2007年11月8日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __APPLICATIONBASE_H__
#define __APPLICATIONBASE_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "ApplicationBaseListener.h"
#include "ListenerBaseManager.h"

enum
{
	 APPLICATION_INIT
	,APPLICATION_RUNNING
	,APPLICATION_READYEXIT
	,APPLICATION_EXITNOW
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API ApplicationBase : public ListenerBaseManager
{
public:
	ApplicationBase();
	virtual ~ApplicationBase();

	// Functions to create, run, pause, and clean up the application
	/// StartUp与Create不可同时调用
	/// StartUp不创建任何窗口，通常与AttachMainWnd配合使用
	/// StartUp按Create逻辑触发所有事件
	/// StartUp与shutdown配对使用
	virtual BOOL	 StartUp			( HINSTANCE hInstance );
	virtual BOOL	 Shutdown		(  );
	virtual BOOL	 AttachMainWnd	( HWND hWnd );

	/// Create与Destroy配对使用
	virtual BOOL	 Create	( HINSTANCE hInstance );
	virtual void	 Destroy	(  );
	virtual INT     Run		();
	virtual VOID    Pause	( BOOL bPause ){bPause;}

	LRESULT ProcessMsg( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

protected:
	virtual LRESULT MsgProc( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam );

	virtual BOOL OnRun()							{ return TRUE; }
	virtual BOOL PreCreateApp()				{ return TRUE; }
	virtual BOOL OnCreateApp()					{ return TRUE; }
	virtual BOOL PreCreateMainWnd()			{ return TRUE; }
	virtual BOOL OnCreateMainWnd()			{ return TRUE; }
	virtual BOOL OnDestroyApp()				{ return TRUE; }

	virtual BOOL OnResizeMainWnd()			{return TRUE;}

	virtual LPCSTR OnCreateWndClass(WNDPROC ){ return "_RPG4U_MAIN_"; }
public:
	virtual BOOL FrameMove(DWORD /*dwTick*/) { return TRUE; }
	virtual BOOL Render(DWORD /*dwTick*/)  { return TRUE; }

public:
	BOOL CreateMainWnd	(BOOL bCallEvent=TRUE);
	void Exit();

public:
	inline void DoAppFocus(BOOL bFocus)		{ProcessListeners(ApplicationBaseListener::funcOnAppFocus,	(LPARAM)bFocus);}
	inline void DoAppActive(BOOL bActive)	{ProcessListeners(ApplicationBaseListener::funcOnAppActive,	(LPARAM)bActive);}
	inline void DoWndMsg()						{ProcessListeners(ApplicationBaseListener::funcOnWndMsg,		(LPARAM)this);}
	inline void DoMainWndCreated(HWND hWnd){ProcessListeners(ApplicationBaseListener::funcOnMainWndCreated,	(LPARAM)hWnd);}


public:
	VG_BOOL_PROPERTY	(HasFocus);
	VG_BOOL_PROPERTY	(Active);
	VG_DWORD_PROPERTY	(Tick);
	VG_HWND_PROPERTY	(MainWnd);
	VG_HTYPE_PROPERTY	(Instance, HINSTANCE);

	VG_TYPE_PROPERTY	(WndMessage,	SWndMsg);
protected:	
	BOOL              m_bWindowed;
	BOOL              m_bReady;
	BOOL              m_bFrameMoving;
	BOOL              m_bSingleStep;
	DWORD					m_RunState;
protected:
	//HINSTANCE			m_hInstance;
	//HWND              m_hWnd;              // The main app window
	HWND              m_hWndFocus;         // The D3D focus window (usually same as m_hWnd)
	DWORD             m_dwWindowStyle;     // Saved window style for mode switches
	RECT              m_rcWindowBounds;    // Saved window bounds for mode switches
	RECT              m_rcWindowClient;    // Saved client area size for mode switches

	// Variables for timing
	FLOAT             m_fTime;             // Current time in seconds
	FLOAT             m_fElapsedTime;      // Time elapsed since last frame
	FLOAT             m_fFPS;              // Instanteous frame rate

	// Overridable variables for the app
	CHAR*				 m_strWindowTitle;    // Title for the app's window
	DWORD             m_dwCreationWidth;   // Width used to create window
	DWORD             m_dwCreationHeight;  // Height used to create window
	BOOL              m_bShowCursorWhenFullscreen; // Whether to show cursor when fullscreen
	BOOL              m_bClipCursorWhenFullscreen; // Whether to limit cursor pos when fullscreen
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ApplicationBase , _BASE_API);
//extern _BASE_API ApplicationBase& singleton::GetApplicationBase();
#define theApplication	singleton::GetApplicationBase()

#endif //__CLIENTAPPLICATIONBASE_H__
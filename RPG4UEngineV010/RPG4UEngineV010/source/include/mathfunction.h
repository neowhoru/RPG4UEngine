/*////////////////////////////////////////////////////////////////////////
文 件 名：MathDefine.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	数学库基本定义

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHFUNCTION_H__
#define __MATHFUNCTION_H__
#pragma once

#include <math.h>
#include "MathDefine.h"
#include "MathConst.h"

namespace math
{ 
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define BEGIN_MATHFUNC(Name,Ret, Type, Ps)	\
		template<typename Type> _FUNC\
		Ret Name Ps\
		{
#define END_MATHFUNC()	\
		}

//////////////////////////////////////
//数值基本判断
BEGIN_MATHFUNC(Max, T,T, (const T& a, const T& b))
	return (a > b) ? (a) : (b);
END_MATHFUNC()

BEGIN_MATHFUNC(Min, T,T, (const T& a, const T& b))
	return (a < b) ? (a) : (b);
END_MATHFUNC()

BEGIN_MATHFUNC(AbsI, T,T, (const T& a))
	return (a < 0.f) ? -(a) : (a);
END_MATHFUNC()


BEGIN_MATHFUNC(Sign, INT,T, (const T& a))
	if(a == 0)
		return 0;
	return (a < 0.f) ? -1 : 1;
END_MATHFUNC()

BEGIN_MATHFUNC(Sign, INT,T, (const T& a, const T& b))
	if(a == b)
		return 0;
	return (a < b) ? -1 : 1  ;
END_MATHFUNC()

//abs绝对值...
BEGIN_MATHFUNC(Abs, T,T, (const T& a))
	return (a < 0.f) ? -(a) : (a);
END_MATHFUNC()

BEGIN_MATHFUNC(Swap, void,T, (T& a,T& b))
	T c = a;
	a = b;
	b = c;
END_MATHFUNC()

BEGIN_MATHFUNC(AlignNumber, T,T, ( T nSize, int nAlign=sizeof(T)) )
	return ( (nSize + (T)nAlign-1) & ~((T)nAlign - 1));
END_MATHFUNC()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
_FUNC REAL		Abs	(REAL fValue)		{ return REAL(fabs(fValue)); }
_FUNC REAL		Ceil	(REAL fValue)		{ return REAL(ceil(fValue)); }
_FUNC REAL		Exp	(REAL fValue)		{ return REAL(exp(fValue)); }
_FUNC REAL		Floor (REAL fValue)		{ return REAL(floor(fValue)); }
_FUNC REAL		Log	(REAL fValue)		{ return REAL(log(fValue)); }
_FUNC REAL		Pow	(REAL fBase, REAL fExponent) { return REAL(pow(fBase,fExponent)); }
_FUNC REAL    Decimal(REAL fValue)		{ return fValue - floor(fValue);}//获取小数

////////////////////////////////////
//角度转换
BEGIN_MATHFUNC( Deg2Rad, T,T, (const T& d))
	return (T)(d * cPI_V180);
END_MATHFUNC()

BEGIN_MATHFUNC( Rad2Deg, T,T, (const T& r))
	return (T)(r * cPI_INV180);
END_MATHFUNC()


////////////////////////////////////
//数值截取，函数有：
//		Clamp
//		Clamp 0-1
//		Clamp 0-255
//		Clamp 0-360
BEGIN_MATHFUNC( Clamp, T,T, (const T& val,const T& minVal,const T& maxVal))
	if (val < minVal)		return minVal;
	if (val > maxVal)		return maxVal;
	return val;
END_MATHFUNC()

_FUNC REAL	Clamp0_1		(REAL val)	{	return Clamp(val,0.f,1.0f);}
_FUNC REAL	Saturate		(REAL val)	{	return Clamp(val,0.f,1.0f);}
_FUNC int	Clamp0_255	(int val)	{	return Clamp(val,0,	255);}
BEGIN_MATHFUNC(Clamp0_360, T,T, (const T& val))
		return Clamp(val,(T)0,(T)360);
END_MATHFUNC()


/////////////////////////////////////
_FUNC REAL	Sqrt			(REAL x)	{
														 if (x < 0.0f)	 x = (REAL) 0.0f;
														 return (REAL) sqrt(x);
													}

_FUNC REAL Sqr (REAL fValue) { return fValue*fValue; }

//四舍五入...
_FUNC int 		Round			(REAL f)	{ return (int)(f + 0.5f);}


//随机数
_FUNC REAL   Rand						()				{ return REAL( rand() );}
_FUNC REAL   RandomMax				()				{ return REAL( RAND_MAX);}
_FUNC REAL   Random					()				{ return Rand()/RandomMax();}
_FUNC REAL   RandomUnit				()				{ return Random();}
_FUNC REAL   RandomAngleRadian	()				{ return RandomUnit()* cPI_TWO;}
_FUNC REAL   RandomAngleDegree	()				{ return RandomUnit()* cDEGREE_ROUND;}

//_FUNC REAL   Random				(REAL f)			{ return f*Random();}
//_FUNC REAL   Random				(REAL f,REAL f2){ return f+Random( f2-f );}

BEGIN_MATHFUNC(Random, T,T, (T tMax))
	return (T)(RandomUnit()*(REAL)tMax) ;
END_MATHFUNC()

BEGIN_MATHFUNC(Random, T,T, (T tMin,T tMax))
	return tMin +  (T)(RandomUnit()*(REAL)(tMax - tMin));
END_MATHFUNC()

//_FUNC REAL   RandomRange			(REAL fMin,REAL fMax)		{return fMin +  RandomUnit()*(fMax - fMin); }
//_FUNC REAL   RandomRange			(REAL fMax)					{return RandomUnit()*fMax ; }
BEGIN_MATHFUNC(RandomRange, T,T, (T tMax))
	return (T)(RandomUnit()*(REAL)tMax) ;
END_MATHFUNC()

BEGIN_MATHFUNC(RandomRange, T,T, (T tMin,T tMax))
	return tMin +  (T)(RandomUnit()*(REAL)(tMax - tMin));
END_MATHFUNC()


//BEGIN_MATHFUNC(RandomRange , T,T, (T tMin,T tMax))
//	return tMin + (T)(RandomUnit()*(tMax - tMin));
//END_MATHFUNC()

//BEGIN_MATHFUNC(RandomRange , T,T, (T tMax))
//	return (T)( RandomUnit() * (REAL)tMax);
//END_MATHFUNC()

//线性插值
//Linearly interpolate between 2 values: ret = x + l * (y - x)
BEGIN_MATHFUNC( Lerp, T,T, (const T& x, const T& y, const T& l))
  return x + l * (y - x);
END_MATHFUNC()


//容差比较    
_FUNC BOOL EqualReal( REAL a, REAL b, REAL tolerance )
{
	return (fabs(b-a) <= tolerance);
}

_FUNC int CmpRealSign( REAL a, REAL b, REAL e = 0.0001f )
{
	if( fabsf( a-b ) < e )		return 0;
	if( a < b )						return -1;
	return 1;
}

#define  FloatCmp		CmpRealSign


/*////////////////////////////////////////////////////////////////////////
    Smooth a new value towards an old value using a change value.
/*////////////////////////////////////////////////////////////////////////
_MATHLIB_API REAL Smooth(REAL newVal, REAL curVal, REAL maxChange);



// returns 1 / a for a * a = r
// -- Use this for Vector normalisation!!!
//_FUNC REAL InvSqrtAsm( float r )
//{
//    __asm 
//	 {
//        fld1 // r0 = 1.f
//        fld r // r1 = r0, r0 = r
//        fsqrt // r0 = sqrtf( r0 )
//        fdiv // r0 = r1 / r0
//    } // returns r0
//	return 1. / sqrt( r );
//}

_FUNC REAL InvSqrt( float r ){return 1.f / sqrt( r );}


/*////////////////////////////////////////////////////////////////////////
///2的n次方方面处理
/*////////////////////////////////////////////////////////////////////////
_MATHLIB_API	ULONGLONG	GetNext2Pow(ULONGLONG value);
_MATHLIB_API	DWORD			GetBinLog2(ULONGLONG value);

_FUNC			DWORD			GetNext2Pow(DWORD value)
{
	return (DWORD)GetNext2Pow((ULONGLONG)value);
}

template<typename Type>
_FUNC BOOL Is2Pow(const Type in_num)
{
   return (ULONGLONG)in_num == GetNext2Pow((ULONGLONG)in_num);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template< typename Type >
inline Type GCD( Type Value1, Type Value2 )
{
	Type tmp = 0;
	while( Value1 )
	{
		tmp = Value2%Value1;
		Value2 = Value1;
		Value1 = tmp;
	}
	return Value2;
}

template< typename Type >
inline Type LCM( Type Value1, Type Value2 )
{
	return Value1/GCD(Value1,Value2)*Value2;
}




};//namespace math

#endif //__MATHFUNCTION_H__


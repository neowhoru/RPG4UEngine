/*////////////////////////////////////////////////////////////////////////
文 件 名：GameClientSocket.h
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMECLIENTSOCKET_H__
#define __GAMECLIENTSOCKET_H__
#pragma once


#include "CommonDefine.h"
#include "GameClientSocketDefine.h"


typedef struct tagPACKET_HEADER
{
	WORD	wSize;	
} PACKET_HEADER;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTNETLIB_API GameClientSocket
{
public:
	GameClientSocket();
	virtual ~GameClientSocket();

public:
	static BOOL	SocketStartup		();
	static BOOL	SocketShutdown		();

public:
	virtual BOOL Connect			(char*		ip_addr
										,PORTTYPE	port
										,HWND			hWnd
										,UINT			message
										,long			lSelectEvent=FD_READ|FD_WRITE|FD_CLOSE|FD_CONNECT);

	virtual BOOL	Create			(SOCKETFUNC_NOTIFY fnRecieve=NULL);
	virtual BOOL	Disconnect		();
	virtual BOOL	Send				(BYTE*	pBuffer,DWORD dwLen);
	virtual BOOL	Send_FDWrite	();
	virtual BOOL	Receive			();
	virtual BOOL	SetPacketSize	(DWORD dwPacketSize, DWORD dwPacketMax);

public:
	virtual SOCKET GetSocket			()	;
	virtual BYTE*	GetFirstPacket		();
	virtual VOID	RemoveFirstPacket	( WORD wSize );

	virtual BOOL	IsConneted();
	virtual DWORD	GetLastError();


public : 
	void		UpdateTimer			( void );
	int		GetBackDataCount	() ;

protected:
	static BOOL		_StartUp			();

protected:
	BOOL		_GracefulClose	();
	BOOL		_CreateSocket	();
	BOOL		_CloseSocket	();
	void		_SetLastError	(eCLIENT_SOCKET_MSG	eMsg);
	void		_SafeNotify		(DWORD dwParam,void* pParam);

public:
#ifdef ENABLE_CLIENTSOCKET_LOG
protected:
	FILE*				m_fpLog;
	void _InitLog();
	void _LogPrint( char *szlog, ...);
	void _LogEnd(){if(m_fpLog)fclose(m_fpLog);}

#else
protected:
	void _InitLog(){}
	// ENABLE_CLIENTSOCKET_LOG 
	void _LogPrint( char * /*szlog*/, ...){ _SafeNotify(CSM_ERROR_LOG_PRINT_NOT_DEBUGMODE,NULL); throw CSM_ERROR_LOG_PRINT_NOT_DEBUGMODE;}
	void _LogEnd(){}
#endif


private:
	DWORD		m_dwTimer;
	DWORD		m_dwSendByte;
	DWORD		m_dwRecvByte;

	VG_DWORD_GET_PROPERTY	(SendBytePerSec);
	VG_DWORD_GET_PROPERTY	(RecvBytePerSec);
protected:
	DWORD						m_dwInitialTick;
	long						m_lTotalSendSize;
	SOCKET					m_socket;
	void*						m_pRecvBuffer;

	BYTE						m_SendBuf[MAX_SOCK_SENDBUF];	
	int						m_nCurrSend;			

	DWORD						m_dwLastError;
	SOCKETFUNC_NOTIFY		m_ProcSendParent;

	VG_DWORD_GET_PROPERTY(PacketSize		);
	BYTE*						m_pSendBuffer	;
	VG_DWORD_GET_PROPERTY(PacketMaxSize	);	///数据包允许最大值，超过此大小，则发送失败

	static BOOL				s_bStartUpDone;

};//GameClientSocket

#include "GameClientSocket.inl"

#endif //__GAMECLIENTSOCKET_H__
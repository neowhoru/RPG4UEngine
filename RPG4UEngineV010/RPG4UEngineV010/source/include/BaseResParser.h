/*////////////////////////////////////////////////////////////////////////
文 件 名：BaseResParser.h
创建日期：2009年7月11日
最后更新：2009年7月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BASERESPARSER_H__
#define __BASERESPARSER_H__
#pragma once

#include "CommonDefine.h"
#include "BaseResParserDefine.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API BaseResParser
{
public:
	BaseResParser();
	virtual ~BaseResParser();

public:
	virtual BOOL	Load		(LPCTSTR szFileName, BOOL bReload = FALSE );
	virtual VOID	Reload	();
	virtual VOID	Unload	();

	virtual void*  AllocDataUnit	(BOOL bReset=TRUE)=0;
	virtual void   FreeDataUnit	(void*)=0;
	virtual void*	GetDataUnit		(DWORD dwKey)=0;
	virtual void	ForEachUnit		(IBaseResParserOpr& opr)=0;

protected:
	VG_TYPE_GET_PROPERTY	(FileName,StringHandle);

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define BASERES_UNIT_DECL()\
			virtual void*  AllocDataUnit	(BOOL bReset);\
			virtual void   FreeDataUnit	(void*);\
			virtual void*	GetDataUnit		(DWORD dwKey);\
			virtual void	ForEachUnit		(IBaseResParserOpr& opr);\



#define BASERES_UNIT_FOREACH0(TYPE,MEMBER,UNIT)\
			template< class OPR >\
			VOID ForEach##UNIT( OPR & opr )\
			{\
				TYPE *pInfo = NULL;\
				MEMBER->SetFirst();\
				for(;;)\
				{\
					pInfo = MEMBER->GetNext();\
					if(!pInfo)\
						break;\
					opr(pInfo);\
				}\
			}

#define BASERES_UNIT_FOREACH02(TYPE,MEMBER,UNIT)\
			template< class OPR >\
			VOID ForEach##UNIT( OPR & opr )\
			{\
				TYPE *pInfo = NULL;\
				MEMBER->SetFirst();\
				for(;;)\
				{\
					pInfo = MEMBER->GetNext();\
					if(!pInfo)\
						break;\
					if(!opr(pInfo))\
						break;\
				}\
			}


#define BASERES_UNIT_FOREACH01(TYPE,MEMBER,UNIT,MAP)\
			template< class OPR >\
			VOID ForEach##UNIT( OPR & opr )\
			{\
				MAP##It it;\
				for(it=MEMBER.begin();it!=MEMBER.end();it++)\
				{\
					opr(it->second);\
				}\
			}

#define BASERES_UNIT_FOREACH03(TYPE,MEMBER,UNIT,MAP)\
			template< class OPR >\
			VOID ForEach##UNIT( OPR & opr )\
			{\
				MAP##It it;\
				for(it=MEMBER.begin();it!=MEMBER.end();it++)\
				{\
					opr(&it->second);\
				}\
			}


#define BASERES_UNIT_FORARRAY(MEMBER,UNIT,MAX)\
			template< class OPR >\
			VOID ForEach##UNIT( OPR & opr )\
			{\
				for(UINT n=0;n<MAX;n++)\
				{\
					opr(&MEMBER[n]);\
				}\
			}

#define BASERES_UNIT_FORARRAY2(MEMBER,UNIT,MAX)\
			template< class OPR >\
			VOID ForEach##UNIT( OPR & opr )\
			{\
				for(INT n=0;n<MAX;n++)\
				{\
					opr(MEMBER[n]);\
				}\
			}


#define BASERES_UNIT_FOREACH(TYPE,MEMBER)\
				BASERES_UNIT_FOREACH0(TYPE,MEMBER,)


#define BASERES_UNIT_FOREACH2(TYPE,MEMBER,FIELD)\
			inline TYPE*	Alloc##Unit(BOOL bReset=TRUE){return (TYPE*)AllocDataUnit(bReset);}\
			inline void		Free##Unit(TYPE* pPtr){FreeDataUnit(pPtr);}\
			void	_Add(TYPE*	pInfo){MEMBER->Add( pInfo,pInfo->FIELD );}\
			template<class F>void	_Add(TYPE*	pInfo,F type){MEMBER->Add( pInfo,type );}\
			BASERES_UNIT_FOREACH(TYPE,MEMBER)

#define BASERES_UNIT_FOREACH3(TYPE,MEMBER,FIELD)\
			inline TYPE*	Alloc##Unit(BOOL bReset=TRUE){return (TYPE*)AllocDataUnit(bReset);}\
			inline void		Free##Unit(TYPE* pPtr){FreeDataUnit(pPtr);}\
			void	_Add(TYPE*	pInfo){MEMBER->Add( pInfo,pInfo->FIELD );}\
			template<class F>void	_Add(TYPE*	pInfo,F type){MEMBER->Add( pInfo,type );}\
			BASERES_UNIT_FOREACH02(TYPE,MEMBER,)


#define BASERES_UNIT_ALLOC(TYPE,MEMBER,FIELD,NAME)\
			virtual TYPE*  Alloc##NAME	(BOOL bReset);\
			virtual void   Free##NAME	(TYPE*);\
			void	_Add(TYPE*	pInfo){MEMBER->Add( pInfo,pInfo->FIELD );}\
			template<class F>void	_Add(TYPE*	pInfo,F type){MEMBER->Add( pInfo,type );}\
			BASERES_UNIT_FOREACH0(TYPE,MEMBER,NAME)

#define BASERES_UNIT_ALLOC2(TYPE,MEMBER,FIELD,NAME,MAP)\
			virtual TYPE*  Alloc##NAME	(BOOL bReset);\
			virtual void   Free##NAME	(TYPE*);\
			void	_Add(TYPE*	pInfo){MEMBER.insert(MAP##Pair( pInfo->FIELD,pInfo) );}\
			template<class F>void	_Add(TYPE*	pInfo,F type){MEMBER->Add( pInfo,type );}\
			BASERES_UNIT_FOREACH01(TYPE,MEMBER,NAME,MAP)

#define BASERES_UNIT_ALLOC3(TYPE,MEMBER,FIELD,NAME,MAP)\
			virtual TYPE*  Alloc##NAME	(BOOL bReset);\
			virtual void   Free##NAME	(TYPE*);\
			void	_Add(TYPE*	pInfo){MEMBER.insert(MAP##Pair( pInfo->FIELD,*pInfo) );}\
			template<class F>void	_Add(TYPE*	pInfo,F type){MEMBER.insert(MAP##Pair( type,*pInfo) );}\
			BASERES_UNIT_FOREACH03(TYPE,MEMBER,NAME,MAP)


#define BASERES_OBJECT_ALLOC(TYPE,NAME)\
	virtual TYPE*  Alloc##NAME	(BOOL bReset=TRUE)\
													{\
														TYPE* pRet = new TYPE;\
														if(bReset){__ZERO_PTR(pRet);}\
														return pRet;\
													};\
	virtual void   Free##NAME	(TYPE* p){SAFE_DELETE(p);}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define	BASERES_UNIT_TABLE0(TYPE,MEMBER,NAME)\
	public:\
		TYPE*	Get##NAME	(DWORD dwCode){return MEMBER->GetData(dwCode);}\
	private:\
		THashTable<TYPE*>*	MEMBER;



#define	BASERES_UNIT_TABLE_INIT0(TYPE,MEMBER,MAX)\
				MEMBER = new THashTable<TYPE*>;\
				MEMBER->Initialize(MAX)

#define	BASERES_UNIT_TABLE_CLEAR0(TYPE,MEMBER,UNIT)\
			{\
				TYPE *pInfo = NULL;\
				MEMBER->SetFirst();\
				for(;;)\
				{\
					pInfo = MEMBER->GetNext();\
					if(!pInfo)\
						break;\
						Free##UNIT(pInfo);\
				}\
				MEMBER->clear();\
			}

#define	BASERES_UNIT_TABLE_CLEAR(TYPE)\
			BASERES_UNIT_TABLE_CLEAR0(TYPE,m_pDataInfos,Unit)

#define	BASERES_UNIT_TABLE_FREE0(MEMBER)\
			SAFE_DELETE(MEMBER)

//////////////////////////////////////
#define	BASERES_UNIT_TABLE(TYPE,NAME)\
				BASERES_UNIT_TABLE0(TYPE,m_pDataInfos,NAME)

#define	BASERES_UNIT_TABLE_INIT(TYPE,MAX)\
				BASERES_UNIT_TABLE_INIT0(TYPE,m_pDataInfos,MAX)

#define	BASERES_UNIT_TABLE_FREE()\
				BASERES_UNIT_TABLE_FREE0(m_pDataInfos)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define BASERES_UNIT_ALLOC_IMPL(TYPE,NAME,CLASS)\
				TYPE*  CLASS::Alloc##NAME	(BOOL bReset)\
				{\
					TYPE* pInfo =  new TYPE;\
					if(bReset)\
						__ZERO_PTR(pInfo);\
					return pInfo;\
				}\
				void   CLASS::Free##NAME	(TYPE* pData)\
				{\
					TYPE* pInfo = (TYPE*)pData;\
					SAFE_DELETE(pInfo);\
				}

#define BASERES_UNIT_IMPL(TYPE,CLASS,GET,FOREACH)\
				void*  CLASS::AllocDataUnit	(BOOL bReset)\
				{\
					TYPE* pInfo =  new TYPE;\
					if(bReset)\
						__ZERO_PTR(pInfo);\
					return pInfo;\
				}\
				void   CLASS::FreeDataUnit	(void* pData)\
				{\
					TYPE* pInfo = (TYPE*)pData;\
					SAFE_DELETE(pInfo);\
				}\
				void*		CLASS::GetDataUnit	(DWORD dwKey)\
				{return GET(dwKey);}\
				void		CLASS::ForEachUnit	(IBaseResParserOpr& opr)\
				{FOREACH(opr);}


#define BASERES_UNIT_NULL_IMPL0(TYPE,CLASS,GET,FOREACH,CONV)\
				void*  CLASS::AllocDataUnit	(BOOL /*bReset*/)\
				{\
					return NULL;\
				}\
				void   CLASS::FreeDataUnit	(void* /*pData*/)\
				{\
				}\
				void*		CLASS::GetDataUnit	(DWORD dwKey)\
				{return GET(CONV dwKey);}\
				void		CLASS::ForEachUnit	(IBaseResParserOpr& opr)\
				{FOREACH(opr);}

#define BASERES_UNIT_NULL_IMPL(TYPE,CLASS,GET,FOREACH)\
			BASERES_UNIT_NULL_IMPL0(TYPE,CLASS,GET,FOREACH,)





#endif //__BASERESPARSER_H__
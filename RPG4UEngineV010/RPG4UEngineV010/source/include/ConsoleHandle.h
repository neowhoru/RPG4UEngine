/*////////////////////////////////////////////////////////////////////////
文 件 名：ConsoleHandle.h
创建日期：2009年3月28日
最后更新：2009年3月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSOLEHANDLE_H__
#define __CONSOLEHANDLE_H__
#pragma once




/////////////////////////////////////////////////////////////////////////////
// ConsoleHandle - wrapper around Win32 Console API
//
// Written by Bjarke Viksoe (bjarke@viksoe.dk)
//
// This code may be used in compiled form in any way you desire. This
// file may be redistributed by any means PROVIDING it is 
// not sold for profit without the authors written consent, and 
// providing that this notice and the authors name is included. 
//
// This file is provided "as is" with no expressed or implied warranty.
// The author accepts no liability if it causes any damage to you or your
// computer whatsoever. It's free, so don't hassle me about it.
//
// Beware of bugs.
//

extern _BASE_API void lprintf(LPCTSTR szFormat, ...);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API ConsoleHandle
{
public:
   HANDLE m_hConsole;

public:
   ConsoleHandle(HANDLE hConsole = INVALID_HANDLE_VALUE) ;
   ~ConsoleHandle();

public:
   HANDLE Detach();
   void Release();

   BOOL Create(BOOL bWriteOnly = TRUE);
   BOOL SetTitle(LPCTSTR pstrTitle) const;
   DWORD GetConsoleMode() const;
   BOOL SetConsoleMode(DWORD dwMode) const;
   BOOL ModifyConsoleMode(DWORD dwRemove, DWORD dwAdd) const;
   WORD GetTextAttribute() const;
   BOOL SetTextAttribute(WORD wAttrib) const;
   BOOL SetTextColor(WORD wColor);
   BOOL SetBkColor(WORD wColor);
   COORD GetLargestWindowSize() const;
   COORD GetCursorPos() const;
   BOOL GetScreenBufferInfo(PCONSOLE_SCREEN_BUFFER_INFO lpConsoleScreenBufferInfo) const;
   BOOL GetCursorPos(SHORT& x, SHORT& y) const;
   BOOL SetCursorPos(COORD Coord);
   BOOL SetCursorPos(SHORT x, SHORT y);
   BOOL GetCursorInfo(PCONSOLE_CURSOR_INFO pInfo) const;
   BOOL SetCursorInfo(const PCONSOLE_CURSOR_INFO pInfo) const;
   BOOL ShowCursor(BOOL bShow);
   BOOL Read(LPTSTR lpBuffer, DWORD nNumberOfCharsToRead, LPDWORD lpNumberOfCharsRead) const;
   BOOL Write(LPCTSTR pstrText, int nNumberOfCharsToWrite = -1, LPDWORD lpNumberOfCharsWritten = NULL) const;
   BOOL Write(CONST CHAR_INFO *lpBuffer, COORD dwBufferSize, COORD Coord, PSMALL_RECT lpWriteRegion) const;
   BOOL Write(LPCTSTR lpCharacter, int nLength, COORD Coord, LPDWORD lpNumberOfCharsWritten = NULL) const;
   BOOL Write(LPCTSTR lpCharacter, int nLength, SHORT x, SHORT y, LPDWORD lpNumberOfCharsWritten = NULL) const;
   BOOL Write(TCHAR cCharacter, int nLength, COORD Coord, LPDWORD lpNumberOfCharsWritten = NULL) const;
   BOOL Write(TCHAR cCharacter, int nLength, SHORT x, SHORT y, LPDWORD lpNumberOfCharsWritten = NULL) const;
   BOOL ReadInput(PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsRead = NULL) const;
   BOOL PeekInput(PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsRead = NULL) const;
   BOOL WriteInput(const PINPUT_RECORD lpBuffer, DWORD nLength, LPDWORD lpNumberOfEventsWritten = NULL) const;
   BOOL FlushInputBuffer() const;
   BOOL FillAttribute(WORD attribute, int nLength, COORD Coord, LPDWORD lpNumberOfAttrsWritten = NULL) const;
   BOOL FillAttributes(CONST WORD* lpAttribute, int nLength, COORD Coord, LPDWORD lpNumberOfAttrsWritten = NULL) const;;
   BOOL SetCtrlHandler(PHANDLER_ROUTINE pHandlerProc, BOOL bAdd = TRUE) const;
   static BOOL GenerateCtrlEvent(DWORD dwCtrlEvent, DWORD dwProcessGroupId = 0);

	operator HANDLE() const { return m_hConsole; }
};

//extern ConsoleHandle	g_Console;



#endif //__CONSOLEHANDLE_H__
#ifndef N_STRNODE_H
#define N_STRNODE_H

//------------------------------------------------------------------------------
/**
    @brief A node in a nStrList.

    @author
    - RadonLabs GmbH 

    @since
    - 2005.6.30
    @remarks
    - ���� �߰� 
*/

#include <string.h>
#include "CommonDefine.h"
#include "BaseNode.h"
#include "nstring.h"

//------------------------------------------------------------------------------
class _BASE_API nStrNode: public BaseNode 
{
public:
    /// default constructor
    nStrNode();
    /// constructor providing custom data pointer
    nStrNode(void* ptr);
    /// constructor providing node name
    nStrNode(const char* str);
    /// constructor providing custom data pointer and node name
    nStrNode(const char* str, void* ptr);
    /// set the name of this node
    void SetName(const char* str);
    /// get the name of this node
    const char* GetName() const;
    /// get next node in list
    nStrNode* GetNext() const;
    /// get previous node in list
    nStrNode* GetPrev() const;

private:
    StringHandle name;
};

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode::nStrNode()
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode::nStrNode(void* ptr) :
    BaseNode(ptr)
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode::nStrNode(const char* str) :
    name(str)
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode::nStrNode(const char* str, void* ptr) :
    BaseNode(ptr),
    name(str)
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
inline
void
nStrNode::SetName(const char* str)
{
    this->name.Set(str);
}

//------------------------------------------------------------------------------
/**
*/
inline
const char*
nStrNode::GetName() const
{
    return this->name.IsEmpty() ? 0 : this->name.Get();
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrNode::GetNext() const
{
    return (nStrNode *) BaseNode::GetNext();
}

//------------------------------------------------------------------------------
/**
*/
inline
nStrNode*
nStrNode::GetPrev() const
{
    return (nStrNode *) BaseNode::GetPrev();
}

//------------------------------------------------------------------------------
#endif

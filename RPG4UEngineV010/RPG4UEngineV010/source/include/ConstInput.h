/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstInput.h
创建日期：2008年3月19日
最后更新：2008年3月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTINPUT_H__
#define __CONSTINPUT_H__
#pragma once


namespace input
{ 

enum
{
	ID_MOUSE_LBUTTON	= 0,
	ID_MOUSE_RBUTTON	= 1,
	ID_MOUSE_MBUTTON	= 2,

	MAX_MOUSE_BUTTON,
};


/****************************************************************************
 * 
 *      keyboard scan codes
 *
 ****************************************************************************/


enum eKEY_CODE
{
	  KEYC_NULL				  = -1	
	, KEYC_EMPTY	        = 0

	, KEYC_ESCAPE          = 0x01
	, KEYC_1               = 0x02
	, KEYC_2               = 0x03
	, KEYC_3               = 0x04
	, KEYC_4               = 0x05
	, KEYC_5               = 0x06
	, KEYC_6               = 0x07
	, KEYC_7               = 0x08
	, KEYC_8               = 0x09
	, KEYC_9               = 0x0A
	, KEYC_0               = 0x0B
	, KEYC_MINUS           = 0x0C    /* - on main keyboard */
	, KEYC_EQUALS          = 0x0D
	, KEYC_BACK            = 0x0E    /* backspace */
	, KEYC_TAB             = 0x0F
	, KEYC_Q               = 0x10
	, KEYC_W               = 0x11
	, KEYC_E               = 0x12
	, KEYC_R               = 0x13
	, KEYC_T               = 0x14
	, KEYC_Y               = 0x15
	, KEYC_U               = 0x16
	, KEYC_I               = 0x17
	, KEYC_O               = 0x18
	, KEYC_P               = 0x19
	, KEYC_LBRACKET        = 0x1A
	, KEYC_RBRACKET        = 0x1B
	, KEYC_RETURN          = 0x1C    /* Enter on main keyboard */
	, KEYC_LCONTROL        = 0x1D
	, KEYC_A               = 0x1E
	, KEYC_S               = 0x1F
	, KEYC_D               = 0x20
	, KEYC_F               = 0x21
	, KEYC_G               = 0x22
	, KEYC_H               = 0x23
	, KEYC_J               = 0x24
	, KEYC_K               = 0x25
	, KEYC_L               = 0x26
	, KEYC_SEMICOLON       = 0x27
	, KEYC_APOSTROPHE      = 0x28
	, KEYC_GRAVE           = 0x29    /* accent grave */
	, KEYC_LSHIFT          = 0x2A
	, KEYC_BACKSLASH       = 0x2B
	, KEYC_Z               = 0x2C
	, KEYC_X               = 0x2D
	, KEYC_C               = 0x2E
	, KEYC_V               = 0x2F
	, KEYC_B               = 0x30
	, KEYC_N               = 0x31
	, KEYC_M               = 0x32
	, KEYC_COMMA           = 0x33
	, KEYC_PERIOD          = 0x34    /* . on main keyboard */
	, KEYC_SLASH           = 0x35    /* / on main keyboard */
	, KEYC_RSHIFT          = 0x36
	, KEYC_MULTIPLY        = 0x37    /* * on numeric keypad */
	, KEYC_LMENU           = 0x38    /* left Alt */
	, KEYC_SPACE           = 0x39
	, KEYC_CAPITAL         = 0x3A
	, KEYC_F1              = 0x3B
	, KEYC_F2              = 0x3C
	, KEYC_F3              = 0x3D
	, KEYC_F4              = 0x3E
	, KEYC_F5              = 0x3F
	, KEYC_F6              = 0x40
	, KEYC_F7              = 0x41
	, KEYC_F8              = 0x42
	, KEYC_F9              = 0x43
	, KEYC_F10             = 0x44
	, KEYC_NUMLOCK         = 0x45
	, KEYC_SCROLL          = 0x46    /* Scroll Lock */
	, KEYC_NUMPAD7         = 0x47
	, KEYC_NUMPAD8         = 0x48
	, KEYC_NUMPAD9         = 0x49
	, KEYC_SUBTRACT        = 0x4A    /* - on numeric keypad */
	, KEYC_NUMPAD4         = 0x4B
	, KEYC_NUMPAD5         = 0x4C
	, KEYC_NUMPAD6         = 0x4D
	, KEYC_ADD             = 0x4E    /* + on numeric keypad */
	, KEYC_NUMPAD1         = 0x4F
	, KEYC_NUMPAD2         = 0x50
	, KEYC_NUMPAD3         = 0x51
	, KEYC_NUMPAD0         = 0x52
	, KEYC_DECIMAL         = 0x53    /* . on numeric keypad */
	, KEYC_OEM_102         = 0x56    /* <> or \| on RT 102-key keyboard (Non-U.S.) */
	, KEYC_F11             = 0x57
	, KEYC_F12             = 0x58
	, KEYC_F13             = 0x64    /*                     (NEC PC98) */
	, KEYC_F14             = 0x65    /*                     (NEC PC98) */
	, KEYC_F15             = 0x66    /*                     (NEC PC98) */
	, KEYC_KANA            = 0x70    /* (Japanese keyboard)            */
	, KEYC_ABNT_C1         = 0x73    /* /? on Brazilian keyboard */
	, KEYC_CONVERT         = 0x79    /* (Japanese keyboard)            */
	, KEYC_NOCONVERT       = 0x7B    /* (Japanese keyboard)            */
	, KEYC_YEN             = 0x7D    /* (Japanese keyboard)            */
	, KEYC_ABNT_C2         = 0x7E    /* Numpad . on Brazilian keyboard */
	, KEYC_NUMPADEQUALS    = 0x8D    /* = on numeric keypad (NEC PC98) */
	, KEYC_PREVTRACK       = 0x90    /* Previous Track (KEYC_CIRCUMFLEX on Japanese keyboard) */
	, KEYC_AT              = 0x91    /*                     (NEC PC98) */
	, KEYC_COLON           = 0x92    /*                     (NEC PC98) */
	, KEYC_UNDERLINE       = 0x93    /*                     (NEC PC98) */
	, KEYC_KANJI           = 0x94    /* (Japanese keyboard)            */
	, KEYC_STOP            = 0x95    /*                     (NEC PC98) */
	, KEYC_AX              = 0x96    /*                     (Japan AX) */
	, KEYC_UNLABELED       = 0x97    /*                        (J3100) */
	, KEYC_NEXTTRACK       = 0x99    /* Next Track */
	, KEYC_NUMPADENTER     = 0x9C    /* Enter on numeric keypad */
	, KEYC_RCONTROL        = 0x9D
	, KEYC_MUTE            = 0xA0    /* Mute */
	, KEYC_CALCULATOR      = 0xA1    /* Calculator */
	, KEYC_PLAYPAUSE       = 0xA2    /* Play / Pause */
	, KEYC_MEDIASTOP       = 0xA4    /* Media Stop */
	, KEYC_VOLUMEDOWN      = 0xAE    /* Volume - */
	, KEYC_VOLUMEUP        = 0xB0    /* Volume + */
	, KEYC_WEBHOME         = 0xB2    /* Web home */
	, KEYC_NUMPADCOMMA     = 0xB3    /* , on numeric keypad (NEC PC98) */
	, KEYC_DIVIDE          = 0xB5    /* / on numeric keypad */
	, KEYC_SYSRQ           = 0xB7
	, KEYC_RMENU           = 0xB8    /* right Alt */
	, KEYC_PAUSE           = 0xC5    /* Pause */
	, KEYC_HOME            = 0xC7    /* Home on arrow keypad */
	, KEYC_UP              = 0xC8    /* UpArrow on arrow keypad */
	, KEYC_PRIOR           = 0xC9    /* PgUp on arrow keypad */
	, KEYC_LEFT            = 0xCB    /* LeftArrow on arrow keypad */
	, KEYC_RIGHT           = 0xCD    /* RightArrow on arrow keypad */
	, KEYC_END             = 0xCF    /* End on arrow keypad */
	, KEYC_DOWN            = 0xD0    /* DownArrow on arrow keypad */
	, KEYC_NEXT            = 0xD1    /* PgDn on arrow keypad */
	, KEYC_INSERT          = 0xD2    /* Insert on arrow keypad */
	, KEYC_DELETE          = 0xD3    /* Delete on arrow keypad */
	, KEYC_LWIN            = 0xDB    /* Left Windows key */
	, KEYC_RWIN            = 0xDC    /* Right Windows key */
	, KEYC_APPS            = 0xDD    /* AppMenu key */
	, KEYC_POWER           = 0xDE    /* System Power */
	, KEYC_SLEEP           = 0xDF    /* System Sleep */
	, KEYC_WAKE            = 0xE3    /* System Wake */
	, KEYC_WEBSEARCH       = 0xE5    /* Web Search */
	, KEYC_WEBFAVORITES    = 0xE6    /* Web Favorites */
	, KEYC_WEBREFRESH      = 0xE7    /* Web Refresh */
	, KEYC_WEBSTOP         = 0xE8    /* Web Stop */
	, KEYC_WEBFORWARD      = 0xE9    /* Web Forward */
	, KEYC_WEBBACK         = 0xEA    /* Web Back */
	, KEYC_MYCOMPUTER      = 0xEB    /* My Computer */
	, KEYC_MAIL            = 0xEC    /* Mail */
	, KEYC_MEDIASELECT     = 0xED    /* Media Select */

	, MAX_KEY_BUFFER		  = 0x100	// 256	
	, MOUSE_LBUTTON		  = ID_MOUSE_LBUTTON + MAX_KEY_BUFFER
	, MOUSE_RBUTTON		  = ID_MOUSE_RBUTTON + MAX_KEY_BUFFER
	, MOUSE_MBUTTON		  = ID_MOUSE_MBUTTON + MAX_KEY_BUFFER
};


/*
 *  Alternate names for keys, to facilitate transition from DOS.
 */
#define KEYC_BACKSPACE       KEYC_BACK            /* backspace */
#define KEYC_NUMPADSTAR      KEYC_MULTIPLY        /* * on numeric keypad */
#define KEYC_LALT            KEYC_LMENU           /* left Alt */
#define KEYC_CAPSLOCK        KEYC_CAPITAL         /* CapsLock */
#define KEYC_NUMPADMINUS     KEYC_SUBTRACT        /* - on numeric keypad */
#define KEYC_NUMPADPLUS      KEYC_ADD             /* + on numeric keypad */
#define KEYC_NUMPADPERIOD    KEYC_DECIMAL         /* . on numeric keypad */
#define KEYC_NUMPADSLASH     KEYC_DIVIDE          /* / on numeric keypad */
#define KEYC_RALT            KEYC_RMENU           /* right Alt */
#define KEYC_UPARROW         KEYC_UP              /* UpArrow on arrow keypad */
#define KEYC_PGUP            KEYC_PRIOR           /* PgUp on arrow keypad */
#define KEYC_LEFTARROW       KEYC_LEFT            /* LeftArrow on arrow keypad */
#define KEYC_RIGHTARROW      KEYC_RIGHT           /* RightArrow on arrow keypad */
#define KEYC_DOWNARROW       KEYC_DOWN            /* DownArrow on arrow keypad */
#define KEYC_PGDN            KEYC_NEXT            /* PgDn on arrow keypad */

/*
 *  Alternate names for keys originally not used on US keyboards.
 */
#define KEYC_CIRCUMFLEX      KEYC_PREVTRACK       /* Japanese keyboard */



};//namespace input

#endif //__CONSTINPUT_H__




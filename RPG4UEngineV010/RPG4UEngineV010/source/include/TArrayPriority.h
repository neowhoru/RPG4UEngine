/*////////////////////////////////////////////////////////////////////////
文 件 名：TArrayPriority.h
创建日期：2008年1月15日
最后更新：2008年1月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TARRAYPRIORITY_H__
#define __TARRAYPRIORITY_H__
#pragma once

#include "CommonDefine.h"


//------------------------------------------------------------------------------
template<class TYPE> 
class TArrayPriority
{
public:
    TArrayPriority(int size);
    TArrayPriority(const TArrayPriority<TYPE>& rhs);
    ~TArrayPriority();

public:
    /// assignment operator
    TArrayPriority<TYPE>& operator=(const TArrayPriority<TYPE>& rhs);
    /// [] operator
    TYPE& operator[](int index) const;

public:
    /// 增加元素时，并指定优先级
    void Add(const TYPE& elm, float pri);
    /// get number of m_ElementList in array
    int Size() const;
    /// return n'th array element
    TYPE& At(int index);

private:
    /// update the min pri element index
    void UpdateMinPriElementIndex();

    void Copy(const TArrayPriority<TYPE>& src);
    void Delete();

private:
    /// an element class
    struct sELEMENT
    {
        TYPE	element;
        float	priority;
    };

    int			m_ElementNum;
    int			m_ElementMax;
    int			m_MinPrioIndex;
    sELEMENT*	m_ElementList;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
TArrayPriority<TYPE>::TArrayPriority(int size) :
    m_ElementNum(0),
    m_ElementMax(size),
    m_MinPrioIndex(0)
{
    ASSERT(size > 0);
    m_ElementList = new sELEMENT[size];
}

//------------------------------------------------------------------------------
template<class TYPE>
void
TArrayPriority<TYPE>::Copy(const TArrayPriority<TYPE>& src) :
    m_ElementNum(src.m_ElementNum),
    m_ElementMax(src.m_ElementMax),
    m_MinPrioIndex(src.m_MinPrioIndex)
{
    ASSERT(0 == m_ElementList);
    m_ElementList = new sELEMENT[m_ElementMax];
    int i;
    for (i = 0; i < m_ElementNum; i++)
    {
        m_ElementList[i] = src.m_ElementList[i];
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void
TArrayPriority<TYPE>::Delete()
{
    m_ElementNum = 0;
    m_ElementMax = 0;
    m_MinPrioIndex = 0;
    if (m_ElementList)
    {
        delete[] m_ElementList;
        m_ElementList = 0;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
TArrayPriority<TYPE>::TArrayPriority(const TArrayPriority<TYPE>& rhs) :
    m_ElementNum(0),
    m_ElementMax(0),
    m_MinPrioIndex(0),
    m_ElementList(0)
{
    Copy(rhs);
}

//------------------------------------------------------------------------------
template<class TYPE>
TArrayPriority<TYPE>::~TArrayPriority()
{
    Delete();
}

//------------------------------------------------------------------------------
template<class TYPE>
void
TArrayPriority<TYPE>::UpdateMinPriElementIndex()
{
    int i;
    m_MinPrioIndex = 0;
    float minPri = m_ElementList[0].priority;
    for (i = 1; i < m_ElementNum; i++)
    {
        if (m_ElementList[i].priority < minPri)
        {
            minPri = m_ElementList[i].priority;
            m_MinPrioIndex = i;
        }
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void
TArrayPriority<TYPE>::Add(const TYPE& elm, float pri)
{
    if (m_ElementNum < m_ElementMax)
    {
        m_ElementList[m_ElementNum].element = elm;
        m_ElementList[m_ElementNum].priority = pri;
        m_ElementNum++;
        if (m_ElementNum == m_ElementMax)
        {
            UpdateMinPriElementIndex();
        }
    }
    else
    {
        if (pri > m_ElementList[m_MinPrioIndex].priority)
        {
            m_ElementList[m_MinPrioIndex].element = elm;
            m_ElementList[m_MinPrioIndex].priority = pri;
            UpdateMinPriElementIndex();
        }
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
int
TArrayPriority<TYPE>::Size() const
{
    return m_ElementNum;
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE&
TArrayPriority<TYPE>::At(int index)
{
    ASSERT((index >= 0) && (index < m_ElementNum));
    return m_ElementList[index].element;
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE&
TArrayPriority<TYPE>::operator[](int index) const
{
    ASSERT((index >= 0) && (index < m_ElementNum));
    return m_ElementList[index].element;
}

//------------------------------------------------------------------------------
template<class TYPE>
TArrayPriority<TYPE>& 
TArrayPriority<TYPE>::operator=(const TArrayPriority<TYPE>& rhs)
{
    Delete();
    Copy(rhs);
    return *this;
}



   
    

#endif //__TARRAYPRIORITY_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：MonsterConfig.h
创建日期：2007年12月10日
最后更新：2007年12月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MONSTERCONFIG_H__
#define __MONSTERCONFIG_H__
#pragma once

#include "XmlIOItem.h"
#include "GameStageLoader.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _TILEWORLDLIB_API CMonsterStruct
{
	friend class MonsterConfig;
public:
	const std::string& GetIntro(){return m_strIntro;}
	const std::string& GetName(){return m_strName;}
	const std::string& GetScriptName(){return m_strScriptName;}
	const std::string& GetLuaScript(){return m_strLuaScript;}
	const std::string& GetMonsterName(){return m_strMonsterName;}
	const std::string& GetCreatureSound(){return m_strCreatureSound;}

	void SetIntro				(LPCSTR szText){ m_strIntro = szText;}
	void SetName				(LPCSTR szText){ m_strName = szText;}
	void SetScriptName		(LPCSTR szText){ m_strScriptName = szText;}
	void SetLuaScript			(LPCSTR szText){ m_strLuaScript = szText;}
	void SetMonsterName		(LPCSTR szText){ m_strMonsterName = szText;}
	void SetCreatureSound	(LPCSTR szText){ m_strCreatureSound = szText;}
private:
	std::string m_strIntro;
	std::string m_strName;			// 怪物模板里的名字

public:
	int m_iIndex;					// 怪物模板里的索引
	//std::string m_strZoneName;		// 怪物所在区块的名字
	int m_iZone;
	float m_nX;						// 怪物在地图上的X坐标
	float m_nY;						// 怪物在地图上的Y坐标
	int m_nDir;						// 怪物在地图上的朝向
	int m_iNumber;
	int m_iRelife;
	int m_iCountry;
	bool m_bEnable;
	bool m_bHide;
    bool m_bRandRange;              // 怪物是否随机范围刷出 为真则为随机，为否则定点刷
private:
//	std::vector<CItemStruct> m_Items;
	std::string m_strScriptName;
	std::string m_strLuaScript;
public:
	int m_iAiValue;
	int m_iMoveAiType;
	int m_iAttackAiType;
	int m_iIdleMoveAiType;
	int m_iSpecialAiType;

private:
	std::string m_strMonsterName;

public:
	int m_iProfessionCorrect;
	int m_iModelIDCorrect;

private:
	std::string m_strCreatureSound;

public:
	int m_iMonsterTypeCorrect;
	int m_iBodySizeCorrect;
	int m_iLevelCorrect;
	int m_iRewardExpCorrect;
	int m_iStrengthCorrect;
	int m_iConstitutionCorrect;
	int m_iAgilityCorrect;
	int m_iMagicCorrect;
	int m_iIntelligenceCorrect;
	int m_iCharmCorrect;
	int m_iFrostAttCorrect;
	int m_iBlazeAttCorrect;
	int m_iThunderAttCorrect;
	int m_iPoisonAttCorrect;
	int m_iFrostDefCorrect;
	int m_iBlazeDefCorrect;
	int m_iThunderDefCorrect;
	int m_iPoisonDefCorrect;
	int m_iHpMaxCorrect;
	int m_iMpMaxCorrect;
	int m_iHpRestoreCorrect;
	int m_iMpRestoreCorrect;
	int m_iExactCorrect;
	int m_iDodgeCorrect;
	int m_iViewCorrect;
	int m_iCatchTimeCorrect;
	float m_fMoveSpeedCorrect;
	int m_iPhysicsMinCorrect;
	int m_iPhysicsMaxCorrect;
	int m_iMagicMinCorrect;
	int m_iMagicMaxCorrect;
	int m_iPhysicsDefCorrect;
	int m_iMagicDefCorrect;
	int m_iCriticalRateCorrect;
	int m_iAttackRateCorrect;
	int m_iSkill1RateCorrect;
	int m_iSkill2RateCorrect;
	int m_iSkill3RateCorrect;
	int m_iItem1RateCorrect;
	int m_iItem2RateCorrect;
	int m_iItem3RateCorrect;
	int m_iItem4RateCorrect;
	int m_iItem5RateCorrect;
	int m_iItem6RateCorrect;
	int m_iDropMoneyMinCorrect;
	int m_iDropMoneyMaxCorrect;
	float m_fDropMultipleRateCorrect;
	int m_iSkill1Correct;
	int m_iSkill2Correct;
	int m_iSkill3Correct;
	int m_iSkill1LevelCorrect;
	int m_iSkill2LevelCorrect;
	int m_iSkill3LevelCorrect;
	int m_iItem1Correct;
	int m_iItem2Correct;
	int m_iItem3Correct;
	int m_iItem4Correct;
	int m_iItem5Correct;
	int m_iItem6Correct;



public:
	CMonsterStruct();
	~CMonsterStruct();
};//class _TILEWORLDLIB_API CMonsterStruct


///////////////////////////////////////////////////////////////
class _TILEWORLDLIB_API MonsterConfig
{
public:
	MonsterConfig();
	virtual ~MonsterConfig();

public:
	void ProcessLoadZoneInfo		( XMLElement* element, int nZoneNumber );
	void ProcessLoadMonsterGroup	( XMLElement* element, int nZoneNumber );
	void loadXML						(GameStageLoader* pStageDataLoader
                                 ,XMLElement*      element
											,int              nZoneNumber);
	void exportXML						( GameStageLoader* pStageDataLoader, int i );

public:
	std::vector<CMonsterStruct>	m_vecMonster;
	int GetMonsterCount();
	void Clear();

};//class _TILEWORLDLIB_API MonsterConfig


#endif //__MONSTERCONFIG_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：StdCharSkeleton.h
创建日期：2006年9月24日
最后更新：2006年9月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
 *	纸娃娃系统输出流程
 *	注意：纸娃娃是不输出动画信息的
 *	启动
 *	过滤数据，滤掉不必要的网格，动画，材质等等，
 *	弹出菜单：是否生成纸娃娃
 *		是：
 *			弹出骨架预览框，初始化界面，统计错误
 *			弹出菜单：是否可以生成纸娃娃
 *				是：
 *					对骨骼进行排序，生成标准骨骼顺序的骨架
 *					对网格进行排序和分组，生成换装数据
 *				否：返回
 *		否：
 *			生成非纸娃娃角色动画

	[Hair]		
		Head
	[Helmet]	
		Head
	[Face]		
		Head
	[Armour]	
		Bip01	
		Neck	
		Spine	
		Pelvis	
		L_Clavicle	
		R_Clavicle	
		L_UpperArm	
		R_UpperArm	
		L_Thigh	
		R_Thigh	
		Tail Tail1
	[Glove]		
		L_ForeArm	
		R_ForeArm	
		L_Hand	
		R_Hand
	[Shoe]
		L_Calf
		R_Calf
		L_Foot
		R_Foot
		L_Toe0
		R_Toe0

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STDCHARSKELETON_H__
#define __STDCHARSKELETON_H__
#pragma once


#include "SkeletonDefine.h"
#include "StdCharSkeletonDefine.h"
#include "TextPointerMap.h"

#define ADD_BONE(a) { m_mapStdCharBoneName[a] = #a; }


namespace skeleton
{
struct Part
{
	BOOL	bEnable;
	int		nPartId;
	std::vector<Bone*> vectorBone;
	Part()
	{
		nPartId = -1;
		bEnable = FALSE;
	}
};
};

using namespace skeleton;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API StdCharSkeleton
{
public:
	StdCharSkeleton();
	~StdCharSkeleton();

public:
	LPCSTR GetBoneNameById	( int nBoneId );
	int	GetBoneIdByName	( LPCSTR pszBonename );


	int GetPartIdByBoneName	( LPCSTR pszBoneName );
	int GetPartIdByPartName	( LPCSTR pszPartName );

	BOOL	CanCreate		( char* pszBonenames[], int nBoneCount );
	BOOL	LoadCfg			( LPCSTR szFileName );
	int	GetBoneCount	();
	Bone*	GetBone			( int i );
	Bone*	GetBoneByName	( LPCSTR pszBoneName );

	Part*	GetPart( int nPartId );
	Part*	GetPart( LPCSTR pszPartName );
protected:
	
	Part						m_parts[eMaxPart];
	std::vector<Bone*>	m_vectorBone;
	TextPointerMap			m_BoneFinder;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(StdCharSkeleton , _RESOURCELIB_API);
//extern _RESOURCELIB_API StdCharSkeleton& GetStdCharSkeleton();
#define theStdCharSkeleton  GetStdCharSkeleton()




inline int StdCharSkeleton::GetPartIdByPartName	( LPCSTR pszPartName )
{
	INT nIndex = GetEquipNameValue(pszPartName);
	if(nIndex == eMaxPart)
		return -1;
	return nIndex;
}


#endif //__STDCHARSKELETON_H__
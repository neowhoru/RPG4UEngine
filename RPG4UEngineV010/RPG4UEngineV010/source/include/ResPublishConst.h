/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishConst.h
创建日期：2009年7月8日
最后更新：2009年7月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __RESPUBLISHCONST_H__
#define __RESPUBLISHCONST_H__
#pragma once

#define RES_PUBLISH_TYPE(NAME,VALUE)\
			RESTYPE_##NAME	= TAG(VALUE)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum
{
	 RES_PUBLISH_TYPE		(ITEMINFO			,'ITEM') 
	,RES_PUBLISH_TYPE		(SKILLINFO			,'SKIL') 
	,RES_PUBLISH_TYPE		(STYLEINFO			,'STYL') 
	,RES_PUBLISH_TYPE		(SKILLDEFAULT		,'SKID') 

	,RES_PUBLISH_TYPE		(REGENMONSTER		,'REGN') 
	,RES_PUBLISH_TYPE		(REGENGROUP			,'REGP') 
	,RES_PUBLISH_TYPE		(DROPENTRY			,'DROP') 
	,RES_PUBLISH_TYPE		(DROPMONSTER		,'DRPM') 
	,RES_PUBLISH_TYPE		(DROPMONSTERGROUP	,'DRPG') 
	,RES_PUBLISH_TYPE		(DROPMONEY			,'DRPN') 

	,RES_PUBLISH_TYPE		(SKILLLIB			,'SKIS') 
	,RES_PUBLISH_TYPE		(NPCINFO				,'NPC_') 
	,RES_PUBLISH_TYPE		(STATEINFO			,'STAT') 
	,RES_PUBLISH_TYPE		(NPCFUNCINFO		,'NPCF') 
	,RES_PUBLISH_TYPE		(WORLDINFO			,'LAND') 
	,RES_PUBLISH_TYPE		(TIPHERO				,'TIPH') 
	,RES_PUBLISH_TYPE		(TIPNPC				,'TIPN') 
	,RES_PUBLISH_TYPE		(TIPMONSTER			,'TIPM') 
	,RES_PUBLISH_TYPE		(TIPFRESHMAN		,'TIPF') 
	,RES_PUBLISH_TYPE		(TIPAI				,'TIPA') 
	,RES_PUBLISH_TYPE		(HORSEINFO			,'HORS') 
	,RES_PUBLISH_TYPE		(SHOPINFO			,'SHOP') 
	,RES_PUBLISH_TYPE		(NPCQUESTINFO		,'NPCQ') 
	,RES_PUBLISH_TYPE		(TIPLOGINFO			,'TIPL') 

	,RES_PUBLISH_TYPE		(SOUNDINFOWEAPON	,'SOUW') 
	,RES_PUBLISH_TYPE		(SOUNDINFOITEM		,'SOUI') 
	,RES_PUBLISH_TYPE		(SOUNDINFOCHAR		,'SOUC') 


	////////////////////////////////////////////////////////
	,RES_PUBLISH_TYPE		(VITEMINFO			,'VITM') 
	,RES_PUBLISH_TYPE		(VNPCINFO			,'VNPC') 
	,RES_PUBLISH_TYPE		(VSKILLINFO			,'VSKI') 
	,RES_PUBLISH_TYPE		(VSTATEINFO			,'VSTA') 


	////////////////////////////////////////////////////////
	,RES_PUBLISH_TYPE		(DESCRES				,'DESC') 
	,RES_PUBLISH_TYPE		(TEXTRES				,'TEXT') 
	,RES_PUBLISH_TYPE		(QUESTDROPINFO		,'DRPQ') 
													
													
	///////////////////////////////////////////////////////////
	,RES_PUBLISH_TYPE		(SOUNDRES			,'SOUD') 
	,RES_PUBLISH_TYPE		(MESHRES				,'MESH') 
	,RES_PUBLISH_TYPE		(GRAPHICRES			,'GRAP') 
	,RES_PUBLISH_TYPE		(EQUIPRES			,'EQIP') 
	,RES_PUBLISH_TYPE		(EFFECTRES			,'EFFE') 
	,RES_PUBLISH_TYPE		(SCRIPTRES			,'SCRI') 
	,RES_PUBLISH_TYPE		(FIELDINFO			,'FIED') 
	,RES_PUBLISH_TYPE		(QUESTRES			,'QUES') 
};

#endif //__RESPUBLISHCONST_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ImportLibDefine.h
创建日期：2009年3月26日
最后更新：2009年3月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
//#ifndef __IMPORTLIBDEFINE_H__
//#define __IMPORTLIBDEFINE_H__
//#pragma once
//本文件可能要多次包含



#ifndef _IMPORT_ID_
#error _IMPORT_ID_ not defined, please define it as the lib name, such as: #define _IMPORT_ID_	"RPG4U"
#endif



/////////////////////////////////////////
#ifdef _IMPORT_RESULT_
#undef _IMPORT_RESULT_
#endif

#ifdef _RPG4U_LIBNAME2_
#undef _RPG4U_LIBNAME2_
#endif

#ifdef _RPG4U_LIBNAME3_
#undef _RPG4U_LIBNAME3_
#endif

#ifdef _RPG4U_LIBNAME4_
#undef _RPG4U_LIBNAME4_
#endif

#ifdef _RPG4U_LIBSTATIC_
#undef _RPG4U_LIBSTATIC_
#endif



/////////////////////////////////////////
//命名规则
//		ID Static U D
#if defined _IMPORT_STATIC_
	#define _RPG4U_LIBSTATIC_	"(Static)"
	#define _RPG4U_LIBNAME2_	_IMPORT_ID_##"Static"
#else
	#define _RPG4U_LIBNAME2_	_IMPORT_ID_
	#define _RPG4U_LIBSTATIC_	""
#endif


/////////////////////////////////////////
//Unicode
#ifdef _UNICODE
	#define _RPG4U_LIBNAME3_	_RPG4U_LIBNAME2_##"U"
#else
	#define _RPG4U_LIBNAME3_	_RPG4U_LIBNAME2_
#endif


/////////////////////////////////////////
//Debug
#ifdef _DEBUG
	#define _RPG4U_LIBNAME4_	_RPG4U_LIBNAME3_##"D"
#elif _PRERELEASE
	#define _RPG4U_LIBNAME4_	_RPG4U_LIBNAME3_##"P"
#else
	#define _RPG4U_LIBNAME4_	_RPG4U_LIBNAME3_
#endif


/////////////////////////////////////////
#if _MSC_VER < 1300
	#define _IMPORT_RESULT_		_RPG4U_LIBNAME4_##"60.lib"
#elif _MSC_VER == 1300
	#define _IMPORT_RESULT_		_RPG4U_LIBNAME4_##"70.lib"
#elif _MSC_VER < 1400
	#define _IMPORT_RESULT_		_RPG4U_LIBNAME4_##"71.lib"
#elif _MSC_VER < 1500
	#define _IMPORT_RESULT_		_RPG4U_LIBNAME4_##"80.lib"
#elif _MSC_VER == 1500
	#define _IMPORT_RESULT_		_RPG4U_LIBNAME4_##"90.lib"
#else
	#error "Unsupported VC++ version"
#endif


/////////////////////////////////////////
#ifdef _IMPORT_TIP_
#undef _IMPORT_TIP_
#endif

/////////////////////////////////////////
#ifdef _EXPORT_LIB_
	#define	_IMPORT_TIP_		" Exporting " ## _RPG4U_LIBSTATIC_ ## " Lib = "##_IMPORT_RESULT_
#else
	#pragma comment(lib, _IMPORT_RESULT_)
	#define	_IMPORT_TIP_		" Importing " ## _RPG4U_LIBSTATIC_ ## " Lib = "##_IMPORT_RESULT_
#endif


/////////////////////////////////////////

#ifdef _EXPORT_LIB_
	#undef _EXPORT_LIB_
#endif

#ifdef _IMPORT_STATIC_
#undef _IMPORT_STATIC_
#endif


//#pragma pop_macro("_IMPORT_ID_")





//#endif //__IMPORTLIBDEFINE_H__
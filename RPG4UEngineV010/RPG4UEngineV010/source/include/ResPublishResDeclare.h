/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishResDeclare.h
创建日期：2006年4月22日
最后更新：2006年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __RESPUBLISHRESDECLARE_H__
#define __RESPUBLISHRESDECLARE_H__
#pragma once

#include "ResPublishDefine.h"
#include "BaseInfoParser.h"


////////////////////////////////////////////////////////
RES_PUBLISH_DECLARE_NAME(ItemInfo		,TAG('ITEM') );
RES_PUBLISH_DECLARE_NAME(SkillInfo		,TAG('SKIL') );
RES_PUBLISH_DECLARE_NAME(StyleInfo		,TAG('STYL') );

RES_PUBLISH_DECLARE		(SkillLib		,TAG('SKIS') );
RES_PUBLISH_DECLARE		(NpcInfo			,TAG('NPC_') );
RES_PUBLISH_DECLARE		(StateInfo		,TAG('STAT') );
RES_PUBLISH_DECLARE		(NpcFuncInfo	,TAG('NPCF') );
RES_PUBLISH_DECLARE		(WorldInfo		,TAG('LAND') );
RES_PUBLISH_DECLARE		(TipHero			,TAG('TIPH') );
RES_PUBLISH_DECLARE		(TipNpc			,TAG('TIPN') );
RES_PUBLISH_DECLARE		(TipMonster		,TAG('TIPM') );
RES_PUBLISH_DECLARE		(TipFreshMan	,TAG('TIPF') );
RES_PUBLISH_DECLARE		(HorseInfo		,TAG('HORS') );
RES_PUBLISH_DECLARE		(ShopInfo		,TAG('SHOP') );
RES_PUBLISH_DECLARE		(NpcQuestInfo	,TAG('NPCQ') );
RES_PUBLISH_DECLARE		(TipLogInfo		,TAG('TIPL') );

RES_PUBLISH_DECLARE		(SoundInfoWeapon	,TAG('SOUW') );
RES_PUBLISH_DECLARE		(SoundInfoItem		,TAG('SOUI') );
RES_PUBLISH_DECLARE		(SoundInfoChar		,TAG('SOUC') );


////////////////////////////////////////////////////////
RES_PUBLISH_DECLARE		(VItemInfo		,TAG('VITM') );
RES_PUBLISH_DECLARE		(VNpcInfo		,TAG('VNPC') );
RES_PUBLISH_DECLARE		(VSkillInfo		,TAG('VSKI') );
RES_PUBLISH_DECLARE		(VStateInfo		,TAG('VSTA') );


////////////////////////////////////////////////////////
RES_PUBLISH_DECLARE		(DescRes			,TAG('DESC') );
RES_PUBLISH_DECLARE		(TextRes			,TAG('TEXT') );
RES_PUBLISH_DECLARE		(QuestDropInfo	,TAG('DRPQ') );


////////////////////////////////////////////////////////
RES_PUBLISH_DECLARE_RES	(SoundRes		,TAG('SOUD') );
RES_PUBLISH_DECLARE_RES	(MeshRes			,TAG('MESH') );
RES_PUBLISH_DECLARE_RES	(GraphicRes		,TAG('GRAP') );
RES_PUBLISH_DECLARE_RES	(EquipRes		,TAG('EQIP') );
RES_PUBLISH_DECLARE_RES	(EffectRes		,TAG('EFFE') );
RES_PUBLISH_DECLARE_RES	(ScriptRes		,TAG('SCRI') );
RES_PUBLISH_DECLARE_RES	(FieldInfo		,TAG('FIED') );
RES_PUBLISH_DECLARE_RES	(QuestRes		,TAG('QUES') );
//RES_PUBLISH_DECLARE_RES	(ScriptLevelUp	,TAG('SCRL') );


#endif //__RESPUBLISHRESDECLARE_H__
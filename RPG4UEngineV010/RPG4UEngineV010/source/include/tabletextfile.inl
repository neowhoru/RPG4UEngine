/*////////////////////////////////////////////////////////////////////////
文 件 名：TableTextFile.inl
创建日期：2008年5月25日
最后更新：2008年5月25日
编 写 者：亦哥(Leo/李亦)
       liease@163.com
		 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TABLETEXTFILE_INL__
#define __TABLETEXTFILE_INL__
#pragma once

namespace util
{ 
inline BOOL TableTextFile::IsBlankLine()	
{
	char c = _GetCurChar(); 
	return _IsNewLine(c);
}

inline TableTextFile& TableTextFile::operator >> ( float &Data)
{
	GetTokenType(TOKEN_NUMBER);
	Data = (float)m_dTokenNumber;
	return ( *this);
}


inline TableTextFile& TableTextFile::operator >> ( double &Data)
{
	GetTokenType(TOKEN_NUMBER);
	Data = m_dTokenNumber;
	return ( *this);
}


template <class T>
inline TableTextFile& TableTextFile::operator >> ( T &Data)
{
	GetTokenType(TOKEN_NUMBER);
	if(m_dwValueType == VALUE_NUMBER)
		Data = (T)m_dwNumber;
	else
		Data = (T)(INT)m_dTokenNumber;
	return ( *this);
}

template <class T>
inline TableTextFile& TableTextFile::operator >= ( T &Data)
{
	GetTokenType(TOKEN_ANYTYPE);
	if(m_dwValueType == VALUE_NUMBER)
		Data = (T)m_dwNumber;
	else
		Data = (T)(INT)m_dTokenNumber;
	return ( *this);
}


inline TableTextFile& TableTextFile::operator >> ( LPCSTR &Data)
{
	GetTokenType(TOKEN_STRING);
	Data = (LPCSTR)GetTokenString();
	return ( *this);
}

inline TableTextFile& TableTextFile::operator >> ( StringHandle &Data)
{
	GetTokenType(TOKEN_STRING);
	Data = (LPCSTR)GetTokenString();
	return ( *this);
}


template <class T> 	
inline 	TableTextFile& TableTextFile::operator >> ( T Data[])
	{
		if(m_dwSerializeNum != 0)
		{
			for(DWORD n=0; n<m_dwSerializeNum; n++)
				(*this) >> Data[n];
			//m_dwSerializeNum = 0;
		}
		else
		{
			GetTokenType(TOKEN_STRING);
			strcpy((char*)Data,(LPCSTR)GetTokenString());
		}
		return ( *this);
	}

///> 以下使用方法，参考 operator()的说明
template <class T>	
inline 	TableTextFile& TableTextFile::operator >= ( T Data[])
	{
		if(m_dwSerializeNum != 0)
		{
			for(DWORD n=0; n<m_dwSerializeNum; n++)
				(*this) >> Data[n];
			//m_dwSerializeNum = 0;
		}
		else
		{
			GetTokenType(TOKEN_ANYTYPE);
			strcpy((char*)Data,(LPCSTR)GetTokenString());
		}
		return ( *this);
	}

///> 设定序列数组数量，与 operator >> dat[]  配合使用
///> 方法 (*this)(num) >> dat[]
inline TableTextFile& TableTextFile::operator() (DWORD dwNum)
{
	m_dwSerializeNum = dwNum;
	return *this;
}


inline TableTextFile& TableTextFile::operator() ()
{
	GetTokenType(TOKEN_ANYTYPE);
	return *this;
}


inline LPCSTR	TableTextFile::GetString()				
{
	GetTokenType(TOKEN_STRING);
	return (LPCSTR)GetTokenString();
}

inline LPCSTR	TableTextFile::GetStringInQuote()	
{
	eSEPERATOR_MODE mode = m_Mode;
	m_Mode = SEP_QUOT_TEXT;
	GetTokenType(TOKEN_STRING);
	m_Mode = mode;
	return (LPCSTR)GetTokenString();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline BOOL		TableTextFile::_IsSep( char c )		
{
	return c == TOKEN_TAB || _IsNewLine(c);
}

inline BOOL		TableTextFile::_IsBlank( char c )	
{
	return _IsSpace(c) || _IsNewLine(c);
}

inline BOOL		TableTextFile::_IsSpace( char c )
{
	return c == TOKEN_SPACE  || c == TOKEN_TAB ;
}

inline BOOL		TableTextFile::_IsNewLine( char c )
{
	return c == TOKEN_NEWLINE || c == TOKEN_CARRIAGERETURN ;
}

inline BOOL		TableTextFile::_IsNumber( char c )	
{
	return	c >= '0' && c <= '9' 
			|| c == '.' 
			|| c == '-' 
			|| c == '+';
};

};//namespace util

#endif //__TABLETEXTFILE_INL__


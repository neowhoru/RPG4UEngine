/*////////////////////////////////////////////////////////////////////////
文 件 名：ZipWrapper.h
创建日期：2006年1月9日
最后更新：2006年1月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
    虚拟文件系统ZIP包装类
    提供基本ZIP文件操作


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ZIPWRAPPER_H__
#define __ZIPWRAPPER_H__
#pragma once


#include "StringEx.h"

extern "C"
{
    #include "zlib.h"
    #include "contrib/minizip/zip.h"
    #include "contrib/minizip/unzip.h"
}
#ifdef _DEBUG
#pragma comment(lib, "zlibwapiD.lib")
#else
#pragma comment(lib, "zlibwapi.lib")
#endif


using Common::StringEx;
using std::vector;
using std::fstream;
using stdext::hash_map;
using std::pair;
using std::string;

namespace FileIO
{

const int WARP_APPEND_STATUS_CREATE = 0;            //创建新文件
const int WARP_APPEND_STATUS_CREATEAFTER = 1;       //覆盖现有文件
const int WARP_APPEND_STATUS_ADDINZIP = 2;          //附加至现有文件

const int TMPBUFFSIZE = 1024*100;                  //临时操作数据的BUFF


class _BASE_API CZipWrapper
{
public:
    typedef struct tagUnitInfo
    {
        StringEx strFileName;
        unz_file_info info;
    }UNITINFO;

    typedef std::vector<UNITINFO> UnitInfoList;

public:
    CZipWrapper(void);
    virtual ~CZipWrapper(void);

public:
    CZipWrapper(const TCHAR* szZipFile, int nOpenMode = WARP_APPEND_STATUS_ADDINZIP);

public:
    bool OpenZipFile(const TCHAR* szFile, int nOpenMode = WARP_APPEND_STATUS_ADDINZIP);
    void CloseZipFile();

    bool AddFileFromLocalFile(const TCHAR* szLocalFile, const TCHAR* szUnitName = NULL);
    bool AddFileFromMemory(const TCHAR* szUnitName, const void* pBuf, size_t len, uLong crc = 0, uLong time = 0);
    bool AddFileFromAnotherZipWrapper(CZipWrapper& zip, const TCHAR* szUnitNameInAnoterZip, const TCHAR* szNewUnitName = NULL);

public:
    bool IsOpen() const;
    bool IsEmpty() const;
    size_t GetCount() const;

    void SetCompressLevel(int CompressLevel);

    bool GetUnitSize(const TCHAR* szUnitName, size_t& size);
    UnitInfoList GetUnitList();
    bool UnitIsExist(const TCHAR* szUnitName);

    bool GetUnitToMemory(const TCHAR* szUnitName, void* pDst, size_t& dstSize);
    bool SaveUnitToFile(const TCHAR* szUnitName, const TCHAR* szFilePath);

    StringEx GetZipFileName() const;
private:
    unsigned long GetFileTime(StringEx strFileName, uLong& time);   //获取文件时间
    unsigned long GetFileCrc(StringEx strFileName);                 //获取文件CRC值
    bool SetFileChangeTime(StringEx strFileName, uLong time);       //修改文件日期时间
    bool MakeDir(StringEx strFolder);                               //创建深层目录
    StringEx GetFolder(StringEx strFullFilePath);                   //获取一个完整路径的文件夹
    void MakeIndex();                                               //创建文件位置索引表

private:

    friend CZipWrapper;                              //为方便CZipWrapper之间数据传递，设置自身类为友元

    unzFile m_pUnZipFile;                           //zip解压文件句柄
    zipFile m_pZipFile;                             //zip压缩文件句柄
    size_t m_count;                                 //zip文件中的文件个数

    StringEx m_strZipFile;                          //Zip文件名
    char m_sztmpbuff[TMPBUFFSIZE];                  //用于临时操作文件的BUFFER大小
    int m_compresslevel;                            //压缩等级
    hash_map<string, unz_file_pos> m_index;         //文件位置索引
    unz_file_pos m_endpos;                          //zip文件当前最后位置
    bool m_bUseIndexPos;                            //是否使用哈尔表查找文件位置

private:
    CZipWrapper(const CZipWrapper&);
    CZipWrapper& operator = (const CZipWrapper&);
};

};  //--namespace FileIO

#endif //__ZIPWRAPPER_H__
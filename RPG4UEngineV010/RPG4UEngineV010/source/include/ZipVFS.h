/*////////////////////////////////////////////////////////////////////////
文 件 名：ZipVFS.h
创建日期：2006年1月7日
最后更新：2006年1月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
    虚拟文件系统实现类
    目前暂时使用Zip作为文件载体
    Zip库由Zlib提供
    使用了内存池为ZipVFSUnit数据提供存储空间
    内存池由Common::TObject_pool及Common::TAllocator提供


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ZIPVFS_H__
#define __ZIPVFS_H__
#pragma once


#include "IVFSystem.h"
#include "StringEx.h"
#include "VFSUnitCache.h"
#include "ZipWrapper.h"
#include "ThreadMutex.h"


using Common::StringEx;
using std::map;
using std::vector;

namespace FileIO
{

//将普通文件名转换成为特定的FileIO中的类所使用的文件名
/*_BASELIB_API*/ StringEx ConvFileName(StringEx strFileName);

template <unsigned int CacheSize>           //Unit类型，缓冲大小
class VFSUnitCache;
class ZipVFSystem;



/////////////////////////////////////////////////////////
class _BASE_API ZipVFSUnit : public IVFSUnit, threadutil::SemiAutoCounterLock
{
    typedef struct tagFileInfo
    {
        StringEx strUnitFileName;
        StringEx strVFSFileName;
        size_t nFileSize;
    }FileInfo;

public:
    ZipVFSUnit();
    virtual ~ZipVFSUnit();

public:
    //载入VFS文件系统中的文件个体
    bool LoadUnit(const TCHAR* szUnitName, IVFSystem& vfs);
    /*++

    Routine Description:

        载入虚拟文件系统中的个体文件
        但是如果相同路径下的本地磁盘文件存在的话，将会忽略虚拟文件系统而直接从本地磁盘中载入文件

    Arguments:

        szUnitName - 虚拟文件系统中个体文件路径及文件名
        vfs - 虚拟文件系统

    Return Value:

        true - 载入成功
        false - 载入失败

    --*/


    bool LoadUnit(const TCHAR* szUnitName);
    /*++

    Routine Description:

        载入本地磁盘中存在的文件

    Arguments:

        szUnitName - 本地磁盘文件路径及文件名

    Return Value:

        true - 载入成功
        false - 载入失败

    --*/

    void ReleaseUnit();
    /*++

    Routine Description:

        释放已经载入的文件个体
        允许被释放多次，不会造成影响。
        忽略是由本地磁盘载入或者是由虚拟文件系统载入

    Arguments:

        None

    Return Value:

        None

    --*/

public:

    unsigned char* GetData() const;
    /*++

    Routine Description:

        获取数据块起始地址

    Arguments:

        None

    Return Value:

        unsigned char* - 数据块启始地址。如果未载入文件，则返回NULL指针

    --*/

    unsigned char* operator ()() const;
    /*++

    Routine Description:

        获取数据块起始地址

    Arguments:

        None

    Return Value:

        unsigned char* - 数据块启始地址。如果未载入文件，则返回NULL指针

    --*/


    //获取文件长度
    size_t GetFileLength() const;
    /*++

    Routine Description:

        获取载入的个体文件长度

    Arguments:

        None

    Return Value:
    
        size_t - 文件长度。如果未载入文件，则返回0

    --*/

    StringEx GetUnitFileName() const;
    /*++

    Routine Description:

        获取个体文件名

    Arguments:

        None

    Return Value:
    
        StringEx - 返回的个体文件名。如果未载入文件，则返回 StringEx(_T("")) 空字串

    --*/


    StringEx GetVFSFileName() const;


    bool IsOpen() const;
    /*++

    Routine Description:

        判断是否已载入文件资源

    Arguments:

        None

    Return Value:
    
        true - 已载入
        false - 未载入

    --*/

private:

    unsigned char* m_bData;     //指向文件内存块位置
    //FileInfo m_info;            //用于存储文件信息
    FileInfo* m_pInfo;            //用于存储文件信息
    

private:

    //设置Cache的缓冲大小为50M  
    //注意：50M只是文件所占用的实际大小，为了提高效率，内存池申请的实际内存可能会比这个值大
    //实际内存池所申请的大小为：设置内存大小 < 实际内存大小 < 设置内存大小*2
    static VFSUnitCache<10> s_Cache;

    friend ZipVFSystem;

private:
    ZipVFSUnit(const ZipVFSUnit&);
    ZipVFSUnit& operator = (const ZipVFSUnit&);
};



/////////////////////////////////////////////////////////////
class _BASE_API ZipVFSystem : public IVFSystem, threadutil::SemiAutoCounterLock
{
    typedef struct tagAddUnitSave
    {
        StringEx strUnitName;           //虚拟文件系统中的名字
        StringEx strDiskFileName;       //存储在磁盘上的临时文件
    }ADDUNITSAVE;

public:

    typedef struct tagCallBackArgument
    {
        StringEx strCurrFileName;
        size_t nCurrFileCount;
        size_t nAllFileCount;
    }CALLBACKARGUMENT;
    //在将对VFS的操作改变至磁盘文件时过程回调函数，strcurrfilename为当前更改的文件名 changedcount为已更改的文件数，allchangcount为所有更改的文件数
    typedef void (*ChangeProcCallBack)(const CALLBACKARGUMENT*);

public:
    //返回列表
    typedef struct tagUnitInfo
    {
        StringEx strFile;
        size_t nLength;
        unsigned long nCrc;
    }UNITINFO;
    typedef map<StringEx, UNITINFO> UNITLIST;

public:
    ZipVFSystem();
    virtual ~ZipVFSystem();

public:

    bool LoadVFS(const TCHAR* szFileName);
    /*++

    Routine Description:

        载入资源包

    Arguments:

        szFileName - VFS的资源包（以目前的实现方式，资源包为ZIP文档）

    Return Value:
    
        true - 获取成功
        false - 获取失败

    --*/

    bool AppendVFS(const TCHAR* szFileName);
    /*++

    Routine Description:

        附加资源包 越被后面附加上去的资源包，其访问优先级越高。
        比如后面附加进去的资源包里存在一个与现有的资源包相同名字的文件，
        那么将只访问附加进去的资源包的文件

    Arguments:

        szFileName - VFS的资源包（以目前的实现方式，资源包为ZIP文档）

    Return Value:
    
        true - 获取成功
        false - 获取失败

    --*/


    void Release();
    /*++

    Routine Description:

        释放VFS文件系统资源包
        即使资源包没有载入，调用这个函数也不会产生任何负面影响
        同时，已经载入数据的VFSUnit也不会因此受到影响

    Arguments:

        None

    Return Value:
    
        None

    --*/

    bool GetUnit(const TCHAR* szUnitName, void* pDst, size_t& dstSize);
    /*++

    Routine Description:

        将虚拟文件系统中的某个文件载入

    Arguments:

        szUnitName - 虚拟文件系统中的文件路径及文件名
        pDst - 存放文件的内存地址
        dstSize - 用于通知GetUnit函数pDst为首地址的内存块大小，以及函数处理完毕后返回pDst实际载入数据的内存大小

    Return Value:
    
        true - 载入成功
        false - 载入失败

    --*/

    bool GetUnitLength(const TCHAR* szUnitName, size_t& size);
    /*++

    Routine Description:

        获取虚拟文件系统中某个文件的实际大小（解压后）

    Arguments:

        szUnitName - 虚拟文件系统中的文件路径及文件名
        size - 函数处理完毕后返回文件的大小

    Return Value:
    
        true - 获取成功
        false - 获取失败

    --*/

    bool IsOpen() const;
    /*++

    Routine Description:

        判断虚拟文件系统是否已经打开资源包

    Arguments:

        None

    Return Value:
    
        true - 已打开
        false - 未打开

    --*/

    bool IsEmpty() const;
    /*++

    Routine Description:

        判断虚拟文件系统是否为空

    Arguments:

        None

    Return Value:
    
        true - 已打开
        false - 未打开

    --*/

    bool UnitIsExist(const TCHAR* szUnitName);
    /*++

    Routine Description:

        判断虚拟文件系统中是否存在某个文件

    Arguments:

        szUnitName - 需要判断是否存在的文件

    Return Value:
    
        true - 存在
        false - 不存在

    --*/
    UNITLIST GetUnitList();

    bool SaveUnitToFile(const TCHAR* szUnitName, const TCHAR* szLocalPath);

//VFS文件操作
public:
    //将Unit添加至文件系统中
    bool AddUnit(const TCHAR* szUnitName, const char* pSrc, size_t len);
    bool AddUnit(const TCHAR* szUnitName, const TCHAR* szLocalFilePath);
    bool AddUnit(const ZipVFSUnit& unit);
    
    //将VFS文件中某个UNIT删除
    bool DelUnit(const TCHAR* szUnitName);

    //将添加/删除操作保存（AddUnit及DelUnit只是在内存中进行操作，必须调用Save将之保存在磁盘文件实体）
    bool Save(ChangeProcCallBack proc = NULL);

public:
    //新建一个VFS文件
    bool NewVFSFile(const TCHAR* szSaveName);

public:
    //获取VFS中的Unit个数
    unsigned int GetUnitCount();

    StringEx GetFileName() const;
    /*++

    Routine Description:

        获取虚拟文件系统资源包在本地磁盘中的文件名

    Arguments:

        None

    Return Value:

        StringEx - 返回文件名 如果未载入则返回空字串 StringEx(_T(""))

    --*/

private:
    StringEx m_strFileName;                         //zip文件名
    vector<CZipWrapper*> m_ziplist;                  //zip操作接口
    map<StringEx, StringEx> m_addlist;              //添加的Unit列表
    map<StringEx, StringEx> m_dellist;              //删除Unit列表

//禁止拷贝构造函数
private:
    ZipVFSystem(const ZipVFSystem&);
    ZipVFSystem& operator = (const ZipVFSystem&);
};

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL2(FileIO::ZipVFSystem, VFSDlk , _BASE_API);
#define g_SlkVFS      GetVFSDlk()
#else
extern _BASE_API FileIO::ZipVFSystem& GetSlkVFS();
#define g_SlkVFS      GetSlkVFS()
#endif



#endif //__ZIPVFS_H__
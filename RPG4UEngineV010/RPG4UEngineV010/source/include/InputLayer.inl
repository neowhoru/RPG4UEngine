/*////////////////////////////////////////////////////////////////////////
文 件 名：InputLayer.inl
创建日期：2008年4月7日
最后更新：2008年4月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __INPUTLAYER_INL__
#define __INPUTLAYER_INL__
#pragma once


namespace input
{ 
//----------------------------------------------------------------------------
inline BOOL InputLayer::IsPressShiftKey()
{
	return GetState( KEYC_LSHIFT, KS_KEY ) || GetState( KEYC_RSHIFT, KS_KEY );
}

//----------------------------------------------------------------------------
inline BOOL InputLayer::IsPressCtrlKey()
{
	return GetState( KEYC_LCONTROL, KS_KEY ) || GetState( KEYC_RCONTROL, KS_KEY );
}

//----------------------------------------------------------------------------
inline BOOL InputLayer::IsPressAltKey()
{
	return GetState( KEYC_LALT, KS_KEY ) || GetState( KEYC_RALT, KS_KEY );
}

//----------------------------------------------------------------------------
inline BOOL InputLayer::IsPressAnyExtKey()
{
	BOOL	bPress = (	GetState( KEYC_LSHIFT, KS_KEY )
						|| GetState( KEYC_RSHIFT, KS_KEY ) 
						|| GetState( KEYC_LCONTROL, KS_KEY ) 
						|| GetState( KEYC_RCONTROL, KS_KEY ) 
						|| GetState( KEYC_LALT, KS_KEY ) 
						|| GetState( KEYC_RALT, KS_KEY )
					);
	return bPress;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////


#ifdef USE_PAUSE
inline DWORD InputLayer::GetFocusInEditBit		()
{
	return m_dwLockKeyboard;
}
#endif

inline  BOOL InputLayer::IsPressKey	(eKEY_CODE Key)
{
	return GetState(Key , KS_KEY ) ;
}
inline  BOOL InputLayer::IsKeyPress	(eKEY_CODE Key)
{
	return GetState(Key , KS_KEY ) ;
}
inline  BOOL InputLayer::IsKeyDown	(eKEY_CODE Key)
{
	return GetState(Key , KS_DOWN ) ;
}
inline  BOOL InputLayer::IsKeyUp		(eKEY_CODE Key)
{
	return GetState(Key , KS_UP ) ;
}

inline  BOOL InputLayer::IsPressKey	(eKEY_CODE Key,BOOL bAnyExtKey)
{
	return GetState(Key , KS_KEY ) && IsPressAnyExtKey() == bAnyExtKey;
}
inline  BOOL InputLayer::IsKeyPress	(eKEY_CODE Key,BOOL bAnyExtKey)
{
	return GetState(Key , KS_KEY ) && IsPressAnyExtKey() == bAnyExtKey;
}
inline  BOOL InputLayer::IsKeyDown	(eKEY_CODE Key,BOOL bAnyExtKey)
{
	return GetState(Key , KS_DOWN )&& IsPressAnyExtKey() == bAnyExtKey;
}
inline  BOOL InputLayer::IsKeyUp		(eKEY_CODE Key,BOOL bAnyExtKey)
{
	return GetState(Key , KS_UP )	&& IsPressAnyExtKey() == bAnyExtKey;
}

inline  BOOL InputLayer::IsMousePress		(eKEY_CODE Key)
{
	return GetState(Key , KS_MOUSE_KEY ) ;
}
inline  BOOL InputLayer::IsMouseDown		(eKEY_CODE Key)
{
	return GetState(Key , KS_MOUSE_DOWN ) ;
}
inline  BOOL InputLayer::IsMouseUp			(eKEY_CODE Key)
{
	return GetState(Key , KS_MOUSE_UP ) ;
}
inline  BOOL InputLayer::IsMouseDragging	(eKEY_CODE Key)
{
	return GetState(Key , KS_MOUSE_KEY )	&& GetMouseDragState(Key);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline void InputLayer::AddKeyboarLockBit(DWORD dwBit)
{
	//DWORD dwBit=GetKeyboardLockBit(dwDlgId);

	BIT_ADD(m_dwLockKeyboard,dwBit);
}

inline void InputLayer::RemoveKeyboarLockBit(DWORD dwBit)
{
	//DWORD dwBit=GetKeyboardLockBit(dwDlgId);

	BIT_REMOVE(m_dwLockKeyboard,dwBit);
}

inline void InputLayer::UpdateMouseOffset(INT x, INT y)
{
	m_nMouseX += x;
	m_nMouseY += y;

	if( m_nMouseX < 0 )
		m_nMouseX = 0;
	if( m_nMouseY < 0 )
		m_nMouseY = 0;
}


};//namespace input



#endif //__INPUTLAYER_INL__
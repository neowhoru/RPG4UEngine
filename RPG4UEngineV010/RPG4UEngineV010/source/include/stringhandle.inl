/*////////////////////////////////////////////////////////////////////////
文 件 名：StringHandle.inl
创建日期：2008年1月12日
最后更新：2008年1月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRINGHANDLE_INL__
#define __STRINGHANDLE_INL__
#pragma once


//------------------------------------------------------------------------------
inline void StringHandle::SetInt(int val)
{
    char buf[128];
    _snprintf(buf, sizeof(buf), "%d", val);
    Set(buf);
}

//------------------------------------------------------------------------------
inline void StringHandle::SetFloat(float val)
{
    char buf[128];
    _snprintf(buf, sizeof(buf), "%.6f", val);
    Set(buf);
}

//------------------------------------------------------------------------------
inline StringHandle& StringHandle::operator=(const StringHandle& rhs)
{
    if (&rhs != this)
    {
        Delete();
        Copy(rhs);
    }
    return *this;
}

//------------------------------------------------------------------------------
inline StringHandle& StringHandle::operator=(LPCSTR rhs)
{
    Set(rhs);
    return *this;
}

//------------------------------------------------------------------------------
inline void StringHandle::Append(LPCSTR str)
{
    ASSERT(str);
    DWORD rlen = (DWORD)lstrlen(str);
    AppendRange(str, rlen);
}

//------------------------------------------------------------------------------
inline void StringHandle::Append(const StringHandle& str)
{
    Append(str.Get());
}

//------------------------------------------------------------------------------
inline void StringHandle::AppendInt(int val)
{
    StringHandle str;
    str.SetInt(val);
    Append(str);
}

//------------------------------------------------------------------------------
inline void StringHandle::AppendFloat(float val)
{
    StringHandle str;
    str.SetFloat(val);
    Append(str);
}

//------------------------------------------------------------------------------
inline StringHandle& StringHandle::operator+=(LPCSTR rhs)
{
    Append(rhs);
    return *this;
}

//------------------------------------------------------------------------------
inline StringHandle& StringHandle::operator+=(const StringHandle& rhs)
{
    Append(rhs.Get());
    return *this;
}

//------------------------------------------------------------------------------
inline BOOL StringHandle::operator == ( LPCTSTR szText) const
{
    return lstrcmp(Get(), szText) == 0;
}

//------------------------------------------------------------------------------
inline BOOL StringHandle::operator != (LPCTSTR szText) const
{
    return lstrcmp(Get(), szText) != 0;
}

inline BOOL StringHandle::IsEqualTo	(LPCSTR szText,BOOL bCase) const
{
	if(bCase)
	    return lstrcmp(Get(), szText) == 0;
    return lstrcmpi(Get(), szText) == 0;
}


//------------------------------------------------------------------------------
inline const TCHAR StringHandle::operator[](INT	i) const
{
      return operator[]((UINT)i);
}

//------------------------------------------------------------------------------
inline TCHAR& StringHandle::operator[](INT	i)
{
      return operator[]((UINT)i);
}

inline const TCHAR StringHandle::operator[](UINT	i) const
{
    ASSERT((0 <= i) && (i <= (m_Length - 1)));
    if (m_pText != 0)
        return m_pText[i];
      return m_szTextBuf[i];
}

//------------------------------------------------------------------------------
inline TCHAR& StringHandle::operator[](UINT		i)
{
    ASSERT((0 <= i) && (i <= (m_Length - 1)));
    if (m_pText != 0)
        return m_pText[i];
    return m_szTextBuf[i];
}

//------------------------------------------------------------------------------
inline int StringHandle::Length() const
{
    return m_Length;
}

//------------------------------------------------------------------------------
inline void StringHandle::Clear()
{
    Delete();
}

//------------------------------------------------------------------------------
inline BOOL StringHandle::IsEmpty() const
{
    if (m_pText && (m_pText[0] != 0))
        return FALSE;
    return m_szTextBuf[0] == 0;
}

//------------------------------------------------------------------------------
inline int StringHandle::ToInt() const
{
    return (INT)ToDword();
}


//------------------------------------------------------------------------------
inline float StringHandle::ToFloat() const
{
    LPCSTR ptr = Get();
    return float(atof(ptr));
}


inline BOOL	StringHandle::IsLetterChar	(BYTE c) const
{
	return	( c >= 'a' && c <= 'z' )
			|| ( c >= 'A' && c <= 'Z' );
}

inline BOOL	StringHandle::IsAlhaChar	(BYTE c) const
{
	return	IsLetterChar(c)
			|| ( c == '_');
}

inline BOOL	StringHandle::IsDigitChar	(BYTE c) const
{
	return ( c >= '0' && c <= '9' );
}

inline BOOL	StringHandle::IsHexChar	(BYTE c) const
{
	return	IsDigitChar(c)
			||	( c >= 'a' && c <= 'f' )
			|| ( c >= 'A' && c <= 'F' );
}

inline BOOL	StringHandle::IsHexHeader() const
{
	LPCSTR szStr = Get();
	 if(m_Length >= 2)
	 {
		 if(	szStr[0] == '0'
			 &&(szStr[1] == 'x' || szStr[1] == 'X') )
			 return TRUE;
	 }
	 return FALSE;
}


#endif //__STRINGHANDLE_INL__
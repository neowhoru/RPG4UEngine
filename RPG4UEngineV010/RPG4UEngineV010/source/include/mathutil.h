/*////////////////////////////////////////////////////////////////////////
文 件 名：MathUtil.h
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHUTIL_H__
#define __MATHUTIL_H__
#pragma once


#include "MathDefine.h"
#include "MathVector.h"

namespace math
{
REAL	GetRandomFloatBetween( REAL fMin, REAL fMax);
short FloatToShort			( REAL fFrom);
REAL	FloatFromShort			( short nFrom);
void	ClampRadianAngle		( Vector3D& vRotation); //
void	ClampRadianAngle		( REAL *pAngle); //
void	ClampRadianAngle		( REAL& fAngle); //
void	ClampDegreeAngle		( REAL& fAngle); //
REAL  GetFloatFixed			( REAL f,REAL fUnit );

_MATHLIB_API REAL		DegreeInRange		(REAL degree, REAL range);

_MATHLIB_API REAL		GetProportionalValueBetween	(REAL fParam1
                                                   ,REAL fParam2
																	,REAL fParamToGet
																	,REAL fResult1
																	,REAL fResult2);


_MATHLIB_API BOOL ClipHorizon	( V3DVector2D p[2], V3DVector2D horizon[2], V3DVector2D* out, float* s, float* u );
_MATHLIB_API BOOL ClipVertical( V3DVector2D p[2], V3DVector2D vertical[2], V3DVector2D* out, float* s, float* v );
_MATHLIB_API BOOL OutLine		( V3DVector2D& v0, V3DVector2D& v1, V3DVector2D& p );
_MATHLIB_API BOOL InsideTri	( V3DVector2D tri[3], V3DVector2D vt, float *u, float* v );

_MATHLIB_API Vector3D	ProjPoint2Screen	(const Vector3D&  p
                                          ,const V3DMatrix& matView
														,const V3DMatrix& matProj
														,int              nScreenWidth
														,int              nScreenHeight);
_MATHLIB_API RECT			MakeRect				( const POINT& pt0, const POINT& pt1 );
_MATHLIB_API Vector3D	RandomDirection	();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
_MATHLIB_API float SinTable( int nDegree );
_MATHLIB_API float CosTable( int nDegree );
_MATHLIB_API float TanTable( int nDegree );

};

#include "MathUtil.inl"

#endif //__MATHUTIL_H__
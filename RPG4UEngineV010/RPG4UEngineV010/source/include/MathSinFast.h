/*////////////////////////////////////////////////////////////////////////
文 件 名：MathSinFast.h
创建日期：2007年6月19日
最后更新：2007年6月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHSINFUNC_H__
#define __MATHSINFUNC_H__
#pragma once

#include "MathConst.h"

//namespace math
//{

//#define		NUM_SINTABLE			360
//#define		NUM_ACOSTABLE			180

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _MATHLIB_API MathSinFast
{
	//enum
	//{
	////	SINTABLE_SIZE	= NUM_SINTABLE + NUM_SINTABLE / 4+1,
	//	ACOSTABLE_SIZE = NUM_ACOSTABLE+1,
	//};
public:
	MathSinFast();
	~MathSinFast();


	/** Private function to build trig tables.
	*/
	BOOL Init(UINT uSize);
	void Release();

	REAL GetSin(REAL fValue);
	REAL GetTan(REAL fValue);


	void Generate();

	REAL Sin(REAL degree);
	REAL Tan(REAL degree);
	REAL ACos(REAL x);

public:
	/// Size of the trig tables as determined by constructor.
	UINT		m_uTableSize;

	/// Radian -> index factor value ( m_uTableSize / 2 * PI )
	REAL		m_fScaleFactor;
	REAL		m_fAScaleFactor;
	REAL*		m_pSinTable;
	REAL*		m_pTanTable;
	REAL*		m_pACosTable;

	//REAL		m_szSinTable[SINTABLE_SIZE];
	//REAL*		m_pCosTable;
	//REAL		m_szTanTable[SINTABLE_SIZE];
};
extern _MATHLIB_API MathSinFast theSinFast;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//inline REAL MathSinFast::Sin(REAL degree)  
//{
//	INT nDeg			= (int)Floor(degree);
//	REAL fDecimal	= Decimal(degree);
//	if(fDecimal == 0.f)
//		return m_szSinTable[nDeg];
//
//	return Lerp(m_szSinTable[nDeg], m_szSinTable[nDeg+1], fDecimal);
//}
//
//inline REAL MathSinFast::Cos(REAL degree)  
//{
//	return Sin(degree + cPI_HALF);
//}
//
//inline int MathSinFast::ACos(REAL x) 
//	{
//		return ( (90*x) > 0 ?  (m_pACosTable[(INT)(90*x)]) : (m_pACosTable[(INT)(90 - 90*x)]) );
//	}


//};//namespace math



#endif //__MATHSINFUNC_H__
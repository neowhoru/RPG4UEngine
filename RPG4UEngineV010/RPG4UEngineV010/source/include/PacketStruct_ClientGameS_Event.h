/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Event.h
创建日期：2009年5月12日
最后更新：2009年5月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_EVENT_H__
#define __PACKETSTRUCT_CLIENTGAMES_EVENT_H__
#pragma once

#include <DataTypeDefine.h>
#include <ConstDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "Protocol_ClientGameS_Event.h"
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"


#pragma pack(push,1)
/*////////////////////////////////////////////////////////////////////////
_CG_EVENT
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_SELECT_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_SELECT_ACK )
//{
	sTOTALINFO_EVENT_ITEM m_TotalInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(EVENT_SELECT_ACK) - (sTOTALINFO_EVENT_ITEM::MAX_SLOT_NUM-m_TotalInfo.m_Count)*sizeof(sEVENT_SLOT));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_SELECT_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_MOVE_ITEM_TO_INVEN_SYN )
//{
	SLOTPOS				m_ItemPos;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_MOVE_ITEM_TO_INVEN_ACK )
//{
	sTOTALINFO_INVENTORY m_TotalInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(EVENT_MOVE_ITEM_TO_INVEN_ACK) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_TotalInfo.m_InvenCount-m_TotalInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX));
	}
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_MOVE_ITEM_TO_INVEN_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_RUNSCRIPT_SYN )
//{
	DWORD	m_dwNpcID;
	DWORD m_dwParam;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_RUNQUESTSCRIPT_SYN )
//{
	DWORD	m_dwQuestID;
	DWORD m_dwParam;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_OPENSHOP_CMD )
//{
	DWORD	m_dwNpcID;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_QUESTDATA_CHANGE_SYN )
//{
	CODETYPE	m_QuestID;
	DWORD		m_Data	;
	DWORD		m_Action	;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGSUPER_CG_EVENT_BEGIN( EVENT_QUESTDATA_CHANGE_CMD, EVENT_QUESTDATA_CHANGE_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_QUESTDATA_CHANGE_NAK )
//{
	BYTE		m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_QUEST_DROP_CMD )
//{
	MONSTERCODE	m_MonsterCode	;
	DWORD			m_VarValue		;
	INT			m_nUnitIndex	;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_SHOWTEXT_CMD )
//{
	enum{	 SHOW_TIP_MAX = eTIP_MAX
			,SHOW_ASK
			,SHOW_TELL
			,SHOW_QUEST
			,SHOW_DIALOG
			,END_DIALOG
	};

	BYTE	m_byType;
	DWORD	m_dwTargetNpcID;
	WORD	m_wLength;
	TCHAR	m_szInfo	[MAX_NOTICE_LENGTH];	//ask\0ans\0ans\0
	WORD GetSize(){return sizeof(*this) - (MAX_NOTICE_LENGTH - m_wLength)*sizeof(TCHAR);}
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_EVENT_BEGIN( EVENT_PLAYMUSIC_CMD )
//{
	DWORD	m_dwParam;
	WORD	m_wLength;
	TCHAR	m_szFilePath[MAX_PATH];
	WORD GetSize(){return sizeof(*this) - (MAX_PATH - m_wLength)*sizeof(TCHAR);}
//}
MSGPACKET_CG_OBJECT_END;


#pragma pack(pop)


#endif //__PACKETSTRUCT_CLIENTGAMES_EVENT_H__
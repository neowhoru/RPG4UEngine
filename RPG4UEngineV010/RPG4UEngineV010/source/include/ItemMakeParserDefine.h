/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemMakeParserDefine.h
创建日期：2008年6月15日
最后更新：2008年6月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMMAKEPARSERDEFINE_H__
#define __ITEMMAKEPARSERDEFINE_H__
#pragma once

#include <THashTable.h>
#include "ConstItemComposite.h"
#include "ConstItem.h"


//const DWORD MAX_ITEM_LEVEL		= 18;

/*////////////////////////////////////////////////////////////////////////
Item类型	类型名称	打造级别	群组	目标 金钱耗量	主材	矿石	木材	皮料	骨材	筋材	玉石	素类	肉类	液料	辅助物
Code	Name		Group	money	Main	stone	wood	skin	bone	tendor	gem	vegetable	meat	liquid	accessory

Rank级别	Rank值段率	相生值段率	相克值段率	等级增量	等级值段率	物理小攻	物理大攻	物理攻值段率
五行小攻	五行大攻	五行攻值段率	潜隐属性1	潜隐属性1值段率	潜隐属性2	潜隐属性2值段率
/*////////////////////////////////////////////////////////////////////////
struct sITEMMAKELEVEL
{
	enum{POTENTIAL_NUM=2};
	DWORD			m_Code;
	VRSTR			m_sName;
	VRSTR			m_sGroup;
	BYTE			m_byItemLV;		
	CODETYPE		m_ItemCode;								/// 打造目标物品编号
	MONEY			m_Money;
	BYTE			m_byMainIndex;							/// 主材类型 eMATERIAL_TYPE
	BYTE			m_arMaterialNum[MATERIALTYPE_MAX ];	/// 各类原料数量

	////////////////////////////////
	//以下为打造属性设置

	char			m_Sequenes[SERIES_RELATION_MAX+1];	///追回顺序

	BYTE			m_byRank;				///< Rank级别
	INT			m_RankRateID;			///< Rank值段率
	INT			m_RegenRateID;			///< 相生值段率
	INT			m_OppressRateID;		///< 相克值段率
	BYTE			m_byLevel;				///< 等级增量
	INT			m_LevelRateID;			///< 等级值段率

	WORD			m_wPhyAttackMin;		///< 物理小攻/防
	WORD			m_wPhyAttackMax;		///< 物理大攻/防
	INT			m_PhyAttackRateID;	///< 物理攻/防值段率
	WORD			m_wMagicAttackMin;	///< 物理小攻/防
	WORD			m_wMagicAttackMax;	///< 物理大攻/防
	INT			m_MagicAttackRateID;///< 物理攻/防值段率

	SHORT			m_AttackSpeed;				///< 攻/移速增量
	INT			m_AttackSpeedRateID;		///< 攻/移速值段率
	//SHORT			m_MoveSpeed;				///< 移速增量
	//INT			m_MoveSpeedRateID;		///< 移速值段率
	SHORT			m_AttSuccessRatio;		///< 命中/闪避增量
	INT			m_AttSuccessRatioRateID;///< 命中/闪避值段率
	//SHORT			m_AvoidRatio;				///< 闪避增量
	//INT			m_AvoidRatioRateID;		///< 闪避值段率


	INT			m_Potential			[POTENTIAL_NUM];			///< 潜隐属性 参考随机属性表
	INT			m_PotentialRateID[POTENTIAL_NUM];	///< 
};

struct sITEMMAKE
{
	//CODETYPE				m_MakeCode;
	DWORD					m_ItemType;		//eITEM_TYPE
	VRSTR					m_sName;			// 名称
	VRSTR					m_sGroup;		// 群组名称
	sITEMMAKELEVEL		m_arMakeLVs[MAX_ITEM_LEVEL];
};

typedef util::THashTable<sITEMMAKE *>				ItemMakeTable;
typedef util::THashTable<sITEMMAKELEVEL*>			ItemMakeLevelTable;



struct sITEMMAKE_GROUP
{
	VRSTR				sName;
	ItemMakeTable	itemMakeTable;
};
typedef util::THashTable<sITEMMAKE_GROUP*>	ItemMakeGroupTable;




#endif //__ITEMMAKEPARSERDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Vector3D.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TVECTOR3D_INL__
#define __TVECTOR3D_INL__
#pragma once

//namespace math
//{ 
inline Vector3D::Vector3D( const REAL *pf )
{
#ifdef _DEBUG
	if(!pf)
		return;
#endif

	x = pf[0];
	y = pf[1];
	z = pf[2];
}


inline Vector3D::Vector3D( const VECTOR3D& v )
{
	x = v.x;
	y = v.y;
	z = v.z;
}


inline Vector3D::Vector3D( REAL fx, REAL fy, REAL fz )
{
	x = fx;
	y = fy;
	z = fz;
}


inline Vector3D::Vector3D(const REAL scale)
{
	x = scale;
	y = scale;
	z = scale;
}


inline Vector3D&	Vector3D::Set( REAL fx, REAL fy, REAL fz )
{
	x = fx;
	y = fy;
	z = fz;
	return *this;
}

inline Vector3D&	Vector3D::Set( const REAL * ar )
{
	x = ar[0];
	y = ar[1];
	z = ar[2];
	return *this;
}


inline Vector3D&	Vector3D::SetByAngle	( REAL fRadian )
{
	x = cos(fRadian);
	y = sin(fRadian);
	z = 0.f;
	return *this;
}



// casting
inline REAL& Vector3D::operator [] (INT nIndex)
{
	return m[nIndex];
}
inline REAL  Vector3D::operator [] (INT nIndex)const
{
	return m[nIndex];
}

inline Vector3D::operator REAL* ()
{
	return (REAL *) m;
}


inline Vector3D::operator const REAL* () const
{
	return (const REAL *) m;
}

inline Vector3D::operator Vector3D* ()
{
	return this;
}

inline Vector3D::operator const Vector3D* () const
{
	return this;
}

inline Vector3D::operator VECTOR3D& ()
{
	return *this;
}

inline Vector3D::operator const VECTOR3D& () const
{
	return *this;
}


inline Vector3D& Vector3D::operator = ( const VECTOR3D& v )
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}


inline Vector3D& Vector3D::operator = (const REAL scale )
{
	x = scale;
	y = scale;
	z = scale;
	return *this;
}


inline Vector3D& Vector3D::operator = (const REAL* ar )
{
	x = ar[0];
	y = ar[1];
	z = ar[2];
	return *this;
}

// assignment operators

inline Vector3D& Vector3D::operator += ( const VECTOR3D& v )
{
	x += v.x;
	y += v.y;
	z += v.z;
	return *this;
}


inline Vector3D& Vector3D::operator -= ( const VECTOR3D& v )
{
	x -= v.x;
	y -= v.y;
	z -= v.z;
	return *this;
}


inline Vector3D& Vector3D::operator *= ( const VECTOR3D& v )
{
	Vector3D v0(*this);
	*this = v0.CrossProduct(v);
	//x *= v.x;
	//y *= v.y;
	//z *= v.z;
	return *this;
}


inline Vector3D& Vector3D::operator /= ( const VECTOR3D& v )
{
	x /= v.x;
	y /= v.y;
	z /= v.z;
	return *this;
}

inline Vector3D& Vector3D::operator *= ( const Matrix4& m )
{
	*this = Multiply(m);
	return *this;
}


inline Vector3D& Vector3D::operator += ( REAL f )
{
	x += f;
	y += f;
	z += f;
	return *this;
}


inline Vector3D& Vector3D::operator -= ( REAL f )
{
	x -= f;
	y -= f;
	z -= f;
	return *this;
}


inline Vector3D& Vector3D::operator *= ( REAL f )
{
	x *= f;
	y *= f;
	z *= f;
	return *this;
}


inline Vector3D& Vector3D::operator /= ( REAL f )
{
	REAL fInv;
	if(f != 0.f)   fInv = 1.0f / (REAL)f;
	else				fInv = 0;

	x = (REAL)(x *fInv);
	y = (REAL)(y *fInv);
	z = (REAL)(z *fInv);
	return *this;
}


inline Vector3D& Vector3D::operator() (REAL x0, REAL y0, REAL z0)
{
	x = x0;
	y = y0;
	z = z0;
	return *this;
}

inline Vector3D& Vector3D::operator () ( const Vector3D& v )
{
	x = v.x;
	y = v.y;
	z = v.z;
	return *this;
}


inline Vector3D& Vector3D::operator () (const REAL scale )
{
	x = scale;
	y = scale;
	z = scale;
	return *this;
}


inline Vector3D& Vector3D::operator () (const REAL* ar )
{
	x = ar[0];
	y = ar[1];
	z = ar[2];
	return *this;
}


// unary operators

inline Vector3D Vector3D::operator + () const
{
	return *this;
}


inline Vector3D Vector3D::operator - () const
{
	return Vector3D(-x, -y, -z);
}


// binary operators

inline Vector3D Vector3D::operator + ( const VECTOR3D& v ) const
{
	return Vector3D(x + v.x, y + v.y, z + v.z);
}


inline Vector3D Vector3D::operator - ( const VECTOR3D& v ) const
{
	return Vector3D(x - v.x, y - v.y, z - v.z);
}


inline Vector3D Vector3D::operator * ( const VECTOR3D& v ) const
{
	return CrossProduct(v);
	//return Vector3D(x * v.x, y * v.y, z * v.z);
}


inline Vector3D Vector3D::operator / ( const VECTOR3D& v ) const
{
	return Vector3D(x / v.x, y / v.y, z / v.z);
}

inline Vector3D Vector3D::operator * ( const Matrix4& m )
{
	return Multiply(m);
}

inline REAL Vector3D::operator % ( const VECTOR3D& v ) const
{
	return DotProduct(v);
}


inline Vector3D Vector3D::operator + ( REAL f ) const
{
	return Vector3D(x + f, y + f, z + f);
}

inline Vector3D Vector3D::operator - ( REAL f ) const
{
	return Vector3D(x - f, y - f, z - f);
}

inline Vector3D Vector3D::operator * ( REAL f ) const
{
	return Vector3D(x * f, y * f, z * f);
}


inline Vector3D Vector3D::operator / ( REAL f ) const
{
	REAL fInv;
	if(f != 0.f)   fInv = 1.0f / (REAL)f;
	else				fInv = 0;

	return Vector3D(x * fInv, y * fInv, z * fInv);
}

/////////////////////////////////////////////////

inline BOOL Vector3D::operator == ( const VECTOR3D& v ) const
{
	return x == v.x && y == v.y && z == v.z;
}


inline BOOL Vector3D::operator != ( const VECTOR3D& v ) const
{
	return x != v.x || y != v.y || z != v.z;
}


inline BOOL Vector3D::operator < ( const VECTOR3D& v ) const
{
	return x < v.x && y < v.y && z < v.z;
}


inline BOOL Vector3D::operator > ( const VECTOR3D& v ) const
{
	return x > v.x && y > v.y && z > v.z;
}


//////////////////////////////////////////

inline REAL Vector3D::Length () const
{
	return math::Sqrt( x * x + y * y + z * z );
}


inline REAL Vector3D::LengthSquared () const
{
	return x * x + y * y + z * z;
}


inline REAL Vector3D::Length2 () const
{
	return LengthSquared ();
}


inline REAL Vector3D::Distance(const VECTOR3D& v) const
{
	return (*this - v).Length();
}


inline REAL Vector3D::DistanceSquared(const VECTOR3D& v) const
{
	return (*this - v).LengthSquared();
}


inline REAL Vector3D::DotProduct(const VECTOR3D& vec) const
{
	return x * vec.x + y * vec.y + z * vec.z;
}


inline REAL Vector3D::AbsDotProduct(const VECTOR3D& vec) const
{
	return math::Abs(x * vec.x) + math::Abs(y * vec.y)+ math::Abs(z * vec.z);
}



inline Vector3D Vector3D::CrossProduct( const VECTOR3D& vec ) const
{
	return Vector3D(
		y * vec.z - z * vec.y,
		z * vec.x - x * vec.z,
		x * vec.y - y * vec.x);
}



inline Vector3D Vector3D::NormalizeNew(void) const
{
	Vector3D ret = *this;
	ret.Normalize();
	return ret;
}


inline Vector3D Vector3D::MidPoint( const VECTOR3D& vec ) const
{
	return Vector3D(
		( x + vec.x ) * 0.5f,
		( y + vec.y ) * 0.5f,
		( z + vec.z ) * 0.5f );
}

inline REAL Vector3D::LineDotProduct( const VECTOR3D& vec)
{
	return x * vec.x + y * vec.y /*+ z * vec.z*/;
}

inline REAL Vector3D::LineDistanceFrom( const VECTOR3D& vVertex)
{
	return ( x * vVertex.x + y * vVertex.y + z);
}

inline BOOL Vector3D::LineTestParallel( const VECTOR3D& vLine2)
{
	REAL fTest = ( REAL)fabs( x * vLine2.y - y * vLine2.x);
	return ( fTest < MATH_EPSILON);
}


inline Vector3D&	Vector3D::LineMakeLine( const VECTOR3D& v1, const VECTOR3D& v2)
{
	LineMakeNormal(v1,v2);
	z = - LineDotProduct(v1);
	return *this;
}



inline Vector3D Vector3D::Interpolate( const VECTOR3D& vec2, REAL t)
{
	REAL fInverseTime = 1.0f - t;
	return Vector3D(
		x * t + vec2.x * fInverseTime
		,y * t + vec2.y * fInverseTime
		,z * t + vec2.z * fInverseTime
		);
}



inline void Vector3D::MakeFloor( const VECTOR3D& cmp )
{
	if( cmp.x < x )  x = cmp.x;
	if( cmp.y < y )  y = cmp.y;
	if( cmp.z < z )  z = cmp.z;
}


inline void Vector3D::MakeCeil( const VECTOR3D& cmp )
{
	if( cmp.x > x )  x = cmp.x;
	if( cmp.y > y )  y = cmp.y;
	if( cmp.z > z )  z = cmp.z;
}



inline Vector3D Vector3D::PerpendicularNew(void) const
{
	Vector3D perp = this->CrossProduct( Vector3D::UNIT_X );

	// Check length
	if( perp.IsZeroLength())
	{
		/* This vector is the Y axis multiplied by a scalar, so we have
		to use another axis.
		*/
		perp = this->CrossProduct( Vector3D::UNIT_Y );
	}
	return perp;
}


inline Vector3D Vector3D::ReflectNew(const VECTOR3D& normal) const
{
	return Vector3D( *this - ( 2 * this->DotProduct(normal) * normal ) );
}






inline BOOL Vector3D::IsZeroLength(void) const
{
	return (LengthSquared() <= math::cZERO_T2);
}


inline BOOL Vector3D::IsPositionEquals(const VECTOR3D& pos, REAL tolerance) const
{
	return	math::EqualReal(x, pos.x, tolerance) &&
		math::EqualReal(y, pos.y, tolerance) &&
		math::EqualReal(z, pos.z, tolerance);
}


inline BOOL Vector3D::IsPositionClose(const VECTOR3D& pos, REAL tolerance) const
{
	return DistanceSquared(pos) <=
		(LengthSquared() + pos.LengthSquared()) * tolerance;
}


inline BOOL Vector3D::IsDirectionEqual(const VECTOR3D& vec, const Radian& tolerance) const
{
	REAL dot			= DotProduct(vec);
	Radian angle	= Radian::ACos(dot);

	return angle.Abs() <= tolerance;//.RadianValue();

}

//------------------------------------------------------------------------------
inline void Vector3D::Lerp(const VECTOR3D& v0, REAL fLerp)
{
	x = v0.x + ((x - v0.x) * fLerp);
	y = v0.y + ((y - v0.y) * fLerp);
	z = v0.z + ((z - v0.z) * fLerp);
}

//------------------------------------------------------------------------------
inline void Vector3D::Lerp(const VECTOR3D& v0, const VECTOR3D& v1, REAL fLerp)
{
	x = v0.x + ((v1.x - v0.x) * fLerp);
	y = v0.y + ((v1.y - v0.y) * fLerp);
	z = v0.z + ((v1.z - v0.z) * fLerp);
}

inline Vector3D& Vector3D::Scale(Vector3C& vScale)
{
	x *= vScale.x;
	y *= vScale.y;
	z *= vScale.z;
	return *this;
}



//};//namespace math


#endif //__TVECTOR3D_INL__
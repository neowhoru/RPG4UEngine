/*////////////////////////////////////////////////////////////////////////
文 件 名：MathConvertion.inl
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATHCONVERTION_INL__
#define __MATHCONVERTION_INL__
#pragma once

#ifndef _SERVER




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline Vector3D* D3DVECTORToWzVector3( Vector3D *pwvOut, D3DVECTOR *pd3vIn)
{
	pwvOut->x = pd3vIn->x;
	pwvOut->y = pd3vIn->z;
	pwvOut->z = pd3vIn->y;
	return ( pwvOut);
}
inline D3DVECTOR* Vector3ToD3DVECTOR( D3DVECTOR *pd3vOut, Vector3D *pwvIn)
{
	pd3vOut->x = pwvIn->x;
	pd3vOut->y = pwvIn->z;
	pd3vOut->z = pwvIn->y;
	return ( pd3vOut);
}

inline Vector3D* OpenGLVectorToVector3( float *pfIn)
{
	return ( ( Vector3D*)pfIn);
}

inline float* Vector3ToOpenGLVector( Vector3D *pwvIn)
{
	return ( ( float*)pwvIn);
}


inline Matrix4* D3DMATRIXToMatrix4( Matrix4 *pwmOut, D3DMATRIX *pd3mIn)
{
/*
	v1.x = v1.x;	 v1.y = v1.z;	 v1.z = v1.y;	 v1.w = v1.w;
	v2.x = v3.x;	 v2.y = v3.z;	 v2.z = v3.y;	 v2.w = v3.w;
	v3.x = v2.x;	 v3.y = v2.z;	 v3.z = v2.y;	 v3.w = v2.w;
	v4.x = v4.x;	 v4.y = v4.z;	 v4.z = v4.y;	 v4.w = v4.w;
*/
	pwmOut->_11 = pd3mIn->_11; pwmOut->_12 = pd3mIn->_13; pwmOut->_13 = pd3mIn->_12; pwmOut->_14 = pd3mIn->_14;
	pwmOut->_21 = pd3mIn->_31; pwmOut->_22 = pd3mIn->_33; pwmOut->_23 = pd3mIn->_32; pwmOut->_24 = pd3mIn->_34;
	pwmOut->_31 = pd3mIn->_21; pwmOut->_32 = pd3mIn->_23; pwmOut->_33 = pd3mIn->_22; pwmOut->_34 = pd3mIn->_24;
	pwmOut->_41 = pd3mIn->_41; pwmOut->_42 = pd3mIn->_43; pwmOut->_43 = pd3mIn->_42; pwmOut->_44 = pd3mIn->_44;
	return ( pwmOut);
}
inline D3DMATRIX* Matrix4ToD3DMATRIX( D3DMATRIX *pd3mOut, Matrix4 *pwmIn)
{
	pd3mOut->_11 = pwmIn->_11; pd3mOut->_12 = pwmIn->_13; pd3mOut->_13 = pwmIn->_12; pd3mOut->_14 = pwmIn->_14;
	pd3mOut->_21 = pwmIn->_31; pd3mOut->_22 = pwmIn->_33; pd3mOut->_23 = pwmIn->_32; pd3mOut->_24 = pwmIn->_34;
	pd3mOut->_31 = pwmIn->_21; pd3mOut->_32 = pwmIn->_23; pd3mOut->_33 = pwmIn->_22; pd3mOut->_34 = pwmIn->_24;
	pd3mOut->_41 = pwmIn->_41; pd3mOut->_42 = pwmIn->_43; pd3mOut->_43 = pwmIn->_42; pd3mOut->_44 = pwmIn->_44;
	return ( pd3mOut);
}

inline Matrix4* OpenGLMatrixToMatrix4( float *pfIn)
{
	return ( ( Matrix4*)pfIn);
}
inline float* Matrix4ToOpenGLMatrix( Matrix4 *pwmIn)
{
	return ( ( float*)pwmIn);
}

inline D3DMATRIX* Matrix4ToD3DMATRIXTranspose( D3DMATRIX *pd3mOut, Matrix4 *pwmIn)
{
	pd3mOut->_11 = pwmIn->_11; pd3mOut->_12 = pwmIn->_31; pd3mOut->_13 = pwmIn->_21; pd3mOut->_14 = pwmIn->_41;
	pd3mOut->_21 = pwmIn->_13; pd3mOut->_22 = pwmIn->_33; pd3mOut->_23 = pwmIn->_23; pd3mOut->_24 = pwmIn->_43;
	pd3mOut->_31 = pwmIn->_12; pd3mOut->_32 = pwmIn->_32; pd3mOut->_33 = pwmIn->_22; pd3mOut->_34 = pwmIn->_42;
	pd3mOut->_41 = pwmIn->_14; pd3mOut->_42 = pwmIn->_34; pd3mOut->_43 = pwmIn->_24; pd3mOut->_44 = pwmIn->_44;
	return ( pd3mOut);
}


#endif//#ifndef _SERVER


#endif //__MATHCONVERTION_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_CharInfo.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_CHARINFO_H__
#define __PACKETSTRUCT_CLIENTGAMES_CHARINFO_H__
#pragma once

#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include <Protocol_ClientGameS_Player.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"


#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
CHARINFO
/*////////////////////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN(CHARINFO_APPREARANCE_BRD)
//{
	DWORD						m_dwObjectKey;
	SLOTPOS					m_EquipPos;
	SLOTCODE					m_ItemCode;
//}
MSGPACKET_CG_END;;

////////////////////////////////////////////////////////
MSGPACKET_CG_CHARINFO_BEGIN( CHARINFO_CREATE_SYN )
//{
	BYTE						m_byClass;
	TCHAR						m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE						m_byHeight;
	BYTE						m_byFace;
	BYTE						m_byHair;
	BYTE						m_bySex;	//leo sex
	BYTE						m_byHairColor;	//leo
//}
MSGPACKET_CG_END;

////////////////////////////////////////////////////////
MSGPACKET_CG_CHARINFO_BEGIN( CHARINFO_CREATE_ACK )
//{
	sCHARACTER_CLIENTPART		m_CharInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(CHARINFO_CREATE_ACK) - (MAX_EQUIPMENT_SLOT_NUM - m_CharInfo.m_EquipItemInfo.m_Count)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_END;

////////////////////////////////////////////////////////
MSGPACKET_CG_CHARINFO_BEGIN( CHARINFO_CREATE_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_END;

////////////////////////////////////////////////////////
MSGPACKET_CG_CHARINFO_BEGIN( CHARINFO_DESTROY_SYN )
//{
	BYTE						m_SelectedSlotIndex;
	TCHAR						szPasswd[MAX_PASSWORD_LENGTH];

//}
MSGPACKET_CG_END;

////////////////////////////////////////////////////////
MSGPACKET_CG_CHARINFO_BEGIN( CHARINFO_DESTROY_ACK )
//{
//}
MSGPACKET_CG_END;

////////////////////////////////////////////////////////
MSGPACKET_CG_CHARINFO_BEGIN( CHARINFO_DESTROY_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_END;

////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_CHAR_ITEM_CMD )
//{
	sPLAYERINFO_PACKET	m_CharacterInfo;
	//sTOTALINFO_INVENTORY m_ItemInfo;
	BYTE						m_InvenCount;
	BYTE						m_TmpInvenCount;

	int GetSize()
	{
		return  SIZE_CG_MSG(CHARINFO_CHAR_ITEM_CMD);
		//return ( SIZE_CG_MSG(CHARINFO_CHAR_ITEM_CMD) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END

////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_CHAR_ITEM2_CMD )
//{
	BYTE						m_PageIndex;
	sTOTALINFO_INVENTORY m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(CHARINFO_CHAR_ITEM2_CMD) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_ItemInfo.m_InvenCount-m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END


////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_SKILL_CMD )
//{
	sTOTALINFO_SKILL m_SkillInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(CHARINFO_SKILL_CMD) - (sTOTALINFO_SKILL::MAX_SLOT_NUM-m_SkillInfo.m_Count)*sizeof(sSKILL_SLOT) );
	}
//}
MSGPACKET_CG_OBJECT_END

////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_QUICK_CMD )
//{
	sTOTALINFO_QUICK m_QuickInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(CHARINFO_QUICK_CMD) - (sTOTALINFO_QUICK::MAX_SLOT_NUM-m_QuickInfo.m_Count)*sizeof(sQUICK_SLOT) );
	}
//}
MSGPACKET_CG_OBJECT_END

////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_STYLE_CMD )
//{
	sTOTALINFO_STYLE m_StyleInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(CHARINFO_STYLE_CMD) - (sTOTALINFO_STYLE::MAX_SLOT_NUM-m_StyleInfo.m_Count)*sizeof(sSTYLE_SLOT) );
	}
//}
MSGPACKET_CG_OBJECT_END

////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_QUEST_CMD )
//{
	sTOTALINFO_QUEST m_QuestInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(CHARINFO_QUEST_CMD) - (sTOTALINFO_QUEST::MAX_SLOT_NUM-m_QuestInfo.m_Count)*sizeof(sQUEST_SLOT) );
	}
//}
MSGPACKET_CG_OBJECT_END


////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_SELECT_INFO_SYN )
//{
	DWORD						m_dwObjectKey;
//}
MSGPACKET_CG_OBJECT_END

////////////////////////////////////////////////////////
MSGOBJECT_CG_CHARINFO_BEGIN( CHARINFO_SELECT_INFO_ACK )
//{
	DWORD						m_dwObjectKey;
	HPTYPE					m_HP;
	HPTYPE					m_MaxHP;
	LEVELTYPE					m_LV;
//}
MSGPACKET_CG_OBJECT_END


#pragma pack(pop)


#endif //__PACKETSTRUCT_CLIENTGAMES_CHARINFO_H__
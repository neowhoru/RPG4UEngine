/*////////////////////////////////////////////////////////////////////////
文 件 名：VendorDialog.h
创建日期：2009年5月21日
最后更新：2009年5月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VENDORDIALOG_H__
#define __VENDORDIALOG_H__
#pragma once

#include "VendorDialogDefine.h"
#include "ContainerDialogBase.h"

_NAMESPACEU(Player, object);
class BaseContainer;
class BaseSlot;


class _CLIENTGAMELIB_API VendorDialog : public ContainerDialogBase
{
public:
	VendorDialog();
	virtual ~VendorDialog();

public:
	virtual BOOL	Init			()=0;
	virtual VOID	Release		()=0;
	virtual VOID	ResetTrade	();

public:

	virtual VOID	Update				()=0;
	virtual BOOL	InitContainer		()=0;
	virtual VOID	ReleaseContainer	()=0;
	virtual VOID	FlushSlotListener	()=0;

public:
	virtual BOOL	InsertSlot(SLOTPOS    posIndex
                            ,BaseSlot & slotDat)	=0;
	virtual BOOL	InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat, MONEY money, SLOTPOS fromPos=INVALID_POSTYPE)=0;
	virtual VOID	DeleteSlot(SLOTPOS    posIndex
                            ,BaseSlot * pSlotOut)		=0;

public:
	virtual VOID	NetworkProc	( MSG_BASE * pMsg )=0;
	virtual VOID	ShowWindow(BOOL val)=0;
	virtual VOID	CloseWindow()=0;

public:
   virtual  void  VendingRename		(LPCTSTR szTitle)=0;
   virtual  void  OpenVendor			(BOOL bView=FALSE)=0;
   virtual  void  VendingStart		()=0;
   virtual  void  VendingEnd			()=0;
   virtual  void  VendorDelete		(POSTYPE pos)=0;
   virtual  void  VendingInsert		(SLOTPOS toPos, SLOTPOS fromPos)=0;
   virtual  void  VendingSwap			(SLOTPOS toPos, SLOTPOS fromPos)=0;
   virtual  void	VendingBuy			(SLOTPOS pos)=0;

   virtual  void  OnVendorRename		()=0;
	virtual VOID	OnVendorStart		(BOOL bFailed=FALSE)=0;
	virtual VOID	OnVendorViewStart	(DWORD dwPlayerKey, LPCTSTR szTitle)=0;
	virtual VOID	OnVendorEnd			(BOOL bFailed=FALSE)=0;
	virtual VOID	OnVendorViewEnd	(DWORD dwPlayerKey)=0;
	virtual VOID	OnPlayerBuyGoods	(DWORD dwPlayer,SLOTPOS pos, MONEY money)=0;
	virtual VOID	OnVendorModifyReq	(SLOTPOS pos,BOOL bAck )=0;
	virtual VOID	OnVendorModify		( )=0;
	virtual VOID	OnVendorViewModify(SLOTPOS pos,sVENDOR_ITEM_SLOTEX& item )=0;
	virtual VOID	OnVendorInsert		( )=0;
	virtual VOID	OnVendorViewInsert(SLOTPOS pos,sVENDOR_ITEM_SLOTEX& item )=0;

	virtual VOID	OnVendorDelete		(SLOTPOS pos )=0;
	virtual VOID	OnVendorViewDelete(SLOTPOS pos )=0;
	virtual VOID	OnVendorSwap		(SLOTPOS fromPos, SLOTPOS  toPos)=0;
	virtual VOID	OnVendorSwapView	(SLOTPOS fromPos, SLOTPOS  toPos)=0;

	virtual VOID	OnViewVendorStart	(LPCTSTR szTitle,sTOTALINFO_VENDOR& items)=0;
	virtual VOID	OnViewVendorEnd	()=0;
	virtual VOID	OnViewVendorBuy		(MONEY money,sTOTALINFO_INVENTORY& items)=0;
	virtual VOID	OnViewVendorOtherBuy	(SLOTPOS pos)=0;
	virtual  void   OpenModifyDialog		(POSTYPE pos)=0;


public:
	virtual VOID	ProcessError		(DWORD dwError	, BOOL bReset=TRUE)=0;
	virtual VOID	ProcessErrorTip	(DWORD dwErrorTip)=0;

public:
	VG_PTR_GET_PROPERTY		(TradeContainer,	BaseContainer);	///目标槽容器
	VG_BOOL_GET_PROPERTY		(Started);
	VG_BOOL_PROPERTY			(VendorView);
	VG_TYPE_PROPERTY			(VendorMessage	, StringHandle);
	VG_TYPE_PROPERTY			(TotalMoneySell, MONEY);
	VG_TYPE_PROPERTY			(TotalMoneyBuy	, MONEY);
	VG_DWORD_PROPERTY			(SalerKey);
	VG_BOOL_GET_PROPERTY		(VendorWorking);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(VendorDialog , _CLIENTGAMELIB_API);
//extern API_NULL VendorDialog& singleton::GetVendorDialog();
#define theVendorDialog  singleton::GetVendorDialog()



#endif //__VENDORDIALOG_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_ClientWorldS.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
 Client <-> World Server

 *  前缀
		C : Client
		G : Game Server
		W : World Server

 * 后缀
		SYN - Server同步协议，ACK协议接受处理，NAK协议拒绝处理
		ACK - SYN协议接受处理
		NAK - SYN协议拒绝处理
		CMD - Server指令，无须返回响应
		BRD - Server向外广播消息
		DBR - DBProxy数据库代理向外广播消息

 * 协议命名规则：
		前缀_分类_协议_后缀
		譬如) CG_CONNECTION_REGISTER_SYN


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PROTOCOL_CLIENTWORLDS_H__
#define __PROTOCOL_CLIENTWORLDS_H__
#pragma once


#include "Protocol_Define.h"
#include "Protocol.h"


#define _CW_CAT(NAME)		_PROTOCOL_CATEGORY(CW,NAME)
#define _CW_NIL(NAME)		_PROTOCOL_NIL		(CW,NAME)
#define _CW_SYN(NAME)		_PROTOCOL_SYN		(CW,NAME)
#define _CW_ACK(NAME)		_PROTOCOL_ACK		(CW,NAME)
#define _CW_BRD(NAME)		_PROTOCOL_BRD		(CW,NAME)
#define _CW_SYN_AN(NAME)	_PROTOCOL_SYN_AN	(CW,NAME)
#define _CW_SYN_N(NAME)		_PROTOCOL_SYN_N	(CW,NAME)
#define _CW_SYN_B(NAME)		_PROTOCOL_SYN_B	(CW,NAME)
#define _CW_SYN_BN(NAME)	_PROTOCOL_SYN_BN	(CW,NAME)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_CATEGORY
{
	 //_CW_CAT(NULL		)
	 _CW_CAT(CONNECTION)	 = PROTOCOL_RANGE(CLIENTWORLD)
	,_CW_CAT(CHAT		)					
	,_CW_CAT(GM			)				
	,_CW_CAT(VIEWPORT	)				
	,_CW_CAT(FRIEND	)				
	,_CW_CAT(GUILD		)				
	,_CW_CAT(ARMORKIT	)				
	,_CW_CAT(MAX	)				
};
PROTOCOL_RANGE_END(ClientWorld, _CW_CAT(MAX) , CLIENTWORLD);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_CONNECTION
{
	 _CW_NIL(HEARTBEAT)
	,_CW_SYN(ENTER_SERVER)
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_CHAT
{
	 _CW_SYN_AN(WHISPER		)
	,_CW_SYN_AN(WHISPER_SET	)
	,_CW_SYN_B (NOTICE	)
	,_CW_SYN_B (CHAT_VILLAGE	)	// 村落范围聊天请求
	,_CW_SYN_B (CHAT_BATTLE	)		// 战斗地域聊天请求
	,_CW_SYN_B (CHAT_SHOUT	)		// 场景内呼喊的请求


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_GM_PROTOCOL
{
	 _CW_SYN_N(GM_NOTICE		)	// 公告
	,_CW_SYN_N(GM_STRING_CMD)	// 在指令字发送请求

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_VIEWPORT
{
	_CW_NIL(VIEWPORT_CHARSTATE)			// 登录人物位置信息
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_ARMORKIT
{
	_CW_SYN(ARMORKIT_FINDHACK)
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_FRIEND
{
	 _CW_SYN_N	(FRIEND_NEWLIST		)		// 第一次连接时好友/黑名单目录请求
	,_CW_ACK		(FRIEND_FRIENDLIST	)		// 好友目录
	,_CW_ACK		(FRIEND_BLOCKLIST		)		// 黑名单目录

	,_CW_BRD		(FRIEND_LOGIN	)				// 向好友发送登录通知
	,_CW_BRD		(FRIEND_LOGOUT	)

	,_CW_SYN_AN	(	FRIEND_ADD)
	,_CW_SYN_AN	(	FRIEND_DEL)					// 好友删掉请求

	,_CW_SYN_AN	(	FRIEND_BLOCK)				// 加入黑名单请求
	,_CW_SYN_AN	(	FRIEND_BLOCK_FREE)		// 撤消黑名单某好友请求
	,_CW_SYN_BN	(	FRIEND_CHAT)				// 好友聊天(聊天频道)
	,_CW_SYN_AN	(	FRIEND_CHATBLOCK)


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCW_GUILD
{

	 _CW_SYN_N	(GUILD_MEMBER			)		// 行会成员信息请求(第一次连接的时候只一次发送)

	,_CW_BRD		(GUILD_LOGIN_CHAR		)
	,_CW_BRD		(GUILD_LOGOUT_CHAR	)
	,_CW_BRD		(GUILD_MEMBER_JOIN		)
	,_CW_BRD		(GUILD_MEMBER_CHG			)
	,_CW_BRD		(GUILD_MEMBER				)
	,_CW_BRD		(GUILD_MEMBER_WITHDRAW	)

	,_CW_SYN_BN	(GUILD_CHAT	)					// 行会聊天（频道）请求



};


#endif //__PROTOCOL_CLIENTWORLDS_H__
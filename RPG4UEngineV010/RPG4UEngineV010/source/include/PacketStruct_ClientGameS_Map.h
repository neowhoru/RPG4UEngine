/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Map.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_MAP_H__
#define __PACKETSTRUCT_CLIENTGAMES_MAP_H__
#pragma once


#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"

#pragma pack(push,1)




/*////////////////////////////////////////////////////////////////////////
MAP
/*////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////
MSGOBJECT_CG_MAP_BEGIN( MAP_TELEPORT_BRD )
//{
	DWORD				m_dwObjectKey;
	VECTOR3D			vPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_MAP_BEGIN( MAP_TELEPORT_SYN )
//{
	VECTOR3D			vPos;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_MAP_BEGIN( MAP_MEETING_NPC_SYN )
//{
	DWORD				m_dwNPCKey;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_MAP_BEGIN( MAP_MEETING_NPC_ACK)
//{
	DWORD				m_dwNPCKey;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_MAP_BEGIN( MAP_MEETING_NPC_BRD )
//{
	DWORD				m_dwObjectKey	;
	DWORD				m_dwNPCKey		;
//}
MSGPACKET_CG_OBJECT_END;


/*////////////////////////////////////////////////////////////////////////
ZONE
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_MISSIONLOBBY_SYN )
//{
	BYTE				m_Count;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_MISSIONLOBBY_ACK )
//{
	enum CONST_VAL { _MAX_ROOM_NUM = 40, };
	BYTE				m_Count;
	sROOMINFO_MISSION	m_RoomInfo[_MAX_ROOM_NUM];	
	int GetSize()
	{
		return SIZE_CG_MSG(ZONE_LIST_MISSIONLOBBY_ACK) - (_MAX_ROOM_NUM-m_Count)*sizeof(sROOMINFO_MISSION);
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_MISSIONLOBBY_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_HUNTINGLOBBY_SYN )
//{
	BYTE				m_Count;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_HUNTINGLOBBY_ACK )
//{
	enum CONST_VAL { _MAX_ROOM_NUM = 40, };
	BYTE				m_Count;
	sROOMINFO_HUNTING	m_RoomInfo[_MAX_ROOM_NUM];	
	int GetSize()
	{
		return SIZE_CG_MSG(ZONE_LIST_HUNTINGLOBBY_ACK) - (_MAX_ROOM_NUM-m_Count)*sizeof(sROOMINFO_HUNTING);
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_HUNTINGLOBBY_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_PVPLOBBY_SYN )
//{
	BYTE				m_Count;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_PVPLOBBY_ACK )
//{
	enum CONST_VAL { _MAX_ROOM_NUM = 40, };
	BYTE				m_Count;
	sROOMINFO_PVP		m_RoomInfo[_MAX_ROOM_NUM];	
	int GetSize()
	{
		return SIZE_CG_MSG(ZONE_LIST_PVPLOBBY_ACK) - (_MAX_ROOM_NUM-m_Count)*sizeof(sROOMINFO_PVP);
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_PVPLOBBY_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_HUNTING_SYN )
//{
	BYTE				m_Count;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_HUNTING_ACK )
//{
	enum CONST_VAL { _MAX_ROOM_NUM = 40, };
	BYTE				m_Count;
	sROOMINFO_HUNTING	m_RoomInfo[_MAX_ROOM_NUM];	
	int GetSize()
	{
		return SIZE_CG_MSG(ZONE_LIST_HUNTING_ACK) - (_MAX_ROOM_NUM-m_Count)*sizeof(sROOMINFO_HUNTING);
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_HUNTING_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_SYN )
//{
	KEYTYPE				m_RoomKey;							//< 
	BYTE				m_byRoomType;							//< eZONE_TYPE
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_HUNTINGLOBBY_ACK )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_MISSIONLOBBY_ACK )
//{
	sROOMINFO_MISSION	m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_PVPLOBBY_ACK )
//{
	sROOMINFO_PVP		m_RoomInfo;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_HUNTING_ACK )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_NONE_ACK )
//{
	KEYTYPE				m_RoomKey;	
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LIST_REFRESH_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_SHORTCUT_SYN )
//{
	BYTE				m_byRoomType;		//< eZONE_TYPE
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_SHORTCUT_HUNTINGLOBBY_ACK )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_SHORTCUT_MISSIONLOBBY_ACK )
//{
	sROOMINFO_MISSION	m_RoomInfo;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_SHORTCUT_PVPLOBBY_ACK )
//{
	sROOMINFO_PVP	m_RoomInfo;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_SHORTCUT_HUNTING_ACK )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_SHORTCUT_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_FIND_ROOM_FROM_CHARID_SYN )
//{
	TCHAR				m_szCharName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSIONLOBBY_ROOM_FROM_CHARID_ACK )
//{
	sROOMINFO_MISSION	m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTINGLOBBY_ROOM_FROM_CHARID_ACK )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVPLOBBY_ROOM_FROM_CHARID_ACK )
//{
	sROOMINFO_PVP		m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_ROOM_FROM_CHARID_ACK )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_FIND_ROOM_FROM_CHARID_NAK )
//{
	DWORD				m_dwErrorCode;		
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CREATE_SYN )
//{
	CODETYPE		m_MapCode;
	BYTE			m_byRoomType;								//< eZONE_TYPE
	BYTE			m_byRoomPublic;							//< eZONE_BEGIN_STATE
	TCHAR			m_szRoomTitle[MAX_ROOMTITLE_LENGTH];
	sROOMINFO_BASE	m_RoomInfo;								//< 
	TCHAR			m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int				GetSize()
	{
		return (m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_LOBBY_CREATE_SYN)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_LOBBY_CREATE_SYN));
	}
//}
MSGPACKET_CG_END;




//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CREATE_ACK )
//{
	KEYTYPE			m_LobbyKey;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CREATE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;


// 肺厚 曼啊
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_JOIN_SYN )
//{
	KEYTYPE			m_LobbyKey;
	BYTE			m_byRoomPublic;		//< eZONE_BEGIN_STATE
	TCHAR			m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int				GetSize()
	{// m_byRoomPublic眉农甫窍扼(努扼捞攫飘脚汾且荐绝促.)
		return (m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_LOBBY_JOIN_SYN)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_LOBBY_JOIN_SYN));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_JOIN_ACK )
//{
	DWORD			m_dwMasterKey;					//< 规厘 敲饭捞绢 虐
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_JOIN_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MASTER_CHANGED_BRD )
//{
	DWORD			m_dwMasterPlayerKey;
//}
MSGPACKET_CG_END;


// 饭叼 滚瓢阑 穿抚
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_BRD )
//{
	DWORD			m_PlayerKey;							//< 饭叼甫 穿弗 敲饭捞绢
//}
MSGPACKET_CG_END;


// 饭叼 滚瓢阑 穿抚
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_CANCEL_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_CANCEL_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_CANCEL_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_READY_CANCEL_BRD )
//{
	DWORD			m_PlayerKey;							//< 饭叼秒家甫 穿弗 敲饭捞绢
//}
MSGPACKET_CG_END;


// 规曼啊茄 荤恩捞 扁粮狼 规狼 沥焊 夸没
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_REQUEST_INFO_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_HUNTING_REQUEST_INFO_ACK )
//{
	SLOTCODE			m_MapCode;
	sROOMINFO_ADDITIONAL m_AdditionalInfo;
	BYTE				m_NonBlockNum	:4;
	BYTE				m_Count			:4;
	DWORD				m_dwReadyPlayerKey[MAX_PARTYMEMBER_NUM];	//< 磊扁磊脚 阂 器窃 + 规厘 阂 器窃
	int					GetSize()
	{
		return (SIZE_CG_MSG(ZONE_LOBBY_HUNTING_REQUEST_INFO_ACK) - sizeof(DWORD)*(MAX_PARTYMEMBER_NUM-m_Count));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_MISSION_REQUEST_INFO_ACK )
//{
	SLOTCODE			m_MapCode;
	BYTE				m_NonBlockNum	:4;
	BYTE				m_Count			:4;
	DWORD				m_dwReadyPlayerKey[MAX_PARTYMEMBER_NUM];	//< 磊扁磊脚 阂 器窃 + 规厘 阂 器窃
	int					GetSize()
	{
		return (SIZE_CG_MSG(ZONE_LOBBY_MISSION_REQUEST_INFO_ACK) - sizeof(DWORD)*(MAX_PARTYMEMBER_NUM-m_Count));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_PVP_REQUEST_INFO_ACK )
//{
	SLOTCODE			m_MapCode;
	sPVPINFO_ADDITIONAL	m_AdditionalPVPInfo;
	BYTE				m_NonBlockNum	:4;
	BYTE				m_Count			:4;
	sPVPINFO_LOBBY		m_PlayerInfo[MAX_PARTYMEMBER_NUM];			//< 磊扁磊脚 器窃 + 规厘器窃
	int					GetSize()
	{
		return (SIZE_CG_MSG(ZONE_LOBBY_PVP_REQUEST_INFO_ACK) - sizeof(sPVPINFO_LOBBY)*(MAX_PARTYMEMBER_NUM-m_Count));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_REQUEST_INFO_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



// 牢盔荐 函版
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NUMBEROFPLAYER_SYN )
//{
	BYTE m_NumberOfPlayers;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NUMBEROFPLAYER_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NUMBEROFPLAYER_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NUMBEROFPLAYER_BRD )
//{
	BYTE m_NumberOfPlayers;
//}
MSGPACKET_CG_END;



// 牢盔荐 函版
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NONBLOCKSLOTNUM_SYN )
//{
	BYTE m_NonBlockSlotNum;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NONBLOCKSLOTNUM_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NONBLOCKSLOTNUM_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_NONBLOCKSLOTNUM_BRD )
//{
	BYTE m_NonBlockSlotNum;
//}
MSGPACKET_CG_END;


// 敲饭捞绢 碍硼
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_KICKPLAYER_SYN )
//{
	DWORD			m_PlayerKey;							//< 碍硼且 敲饭捞绢 虐
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_KICKPLAYER_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_KICKPLAYER_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;


// 敲饭捞绢 评 函版
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_TEAM_SYN )
//{
	BYTE			m_Team;									//< 函版且 评 (1or2)
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_TEAM_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_TEAM_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_TEAM_BRD )
//{
	DWORD			m_dwPlayerKey;							//< 函版茄 敲饭捞绢 虐
	BYTE			m_Team;									//< 函版茄 敲饭捞绢狼 评
//}
MSGPACKET_CG_END;


// 甘 函版
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_MAP_SYN )
//{
	CODETYPE		m_MapCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_MAP_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_MAP_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_CHANGE_MAP_BRD )
//{
	CODETYPE		m_MapCode;
//}
MSGPACKET_CG_END;


// 肺厚狼 清泼 力茄 汲沥 官厕( 抄捞档, 焊呈胶)
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_HUNTING_CONFIG_SYN )
//{
	sROOMINFO_ADDITIONAL m_AdditionalInfo;			//< 函版且 汲沥蔼
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_HUNTING_CONFIG_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_HUNTING_CONFIG_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_HUNTING_CONFIG_BRD )
//{
	sROOMINFO_ADDITIONAL	m_AdditionalInfo;			//< 函版等 汲沥蔼
//}
MSGPACKET_CG_END;


// 肺厚狼 PVP 力茄 汲沥 官厕( 逢, 葛靛)
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_PVP_CONFIG_SYN )
//{
	sPVPINFO_ADDITIONAL	m_AdditionalPVPInfo;			//< 函版且 汲沥蔼
//}
MSGPACKET_CG_END;

////////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_PVP_CONFIG_ACK )
//{
////}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_PVP_CONFIG_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_PVP_CONFIG_BRD )
//{
	sPVPINFO_ADDITIONAL	m_AdditionalPVPInfo;
	BYTE				m_Count;
	sPVPINFO_LOBBY		m_PlayerInfo[MAX_PARTYMEMBER_NUM];			//< 磊扁磊脚 器窃 + 规厘器窃
	int					GetSize()
	{
		return (SIZE_CG_MSG(ZONE_LOBBY_PVP_CONFIG_BRD) - sizeof(sPVPINFO_LOBBY)*(MAX_PARTYMEMBER_NUM-m_Count));
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_PVP_INFO_BRD )
//{
	// 甸绢柯 荤恩狼 沥焊
	DWORD			m_dwPlayerKey;
	BYTE			m_Team;
//}
MSGPACKET_CG_END;




// 清泼 积己 ( START甫 穿抚 )
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_CREATE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_CREATE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



// 固记 积己 ( START甫 穿抚 )
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_CREATE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_CREATE_NAK )
//{
DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



// PVP 积己 ( START甫 穿抚 )
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_CREATE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_CREATE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;


// 颇萍 曼啊 荐遏饶 啊瓷 疙飞
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTINGLOBBY_JOIN_CMD )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
	TCHAR				m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int					GetSize()
	{
		return (m_RoomInfo.m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_HUNTINGLOBBY_JOIN_CMD)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_HUNTINGLOBBY_JOIN_CMD));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSIONLOBBY_JOIN_CMD )
//{
	sROOMINFO_MISSION m_RoomInfo;
	TCHAR			m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int				GetSize()
	{
		return (m_RoomInfo.m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_MISSIONLOBBY_JOIN_CMD)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_MISSIONLOBBY_JOIN_CMD));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVPLOBBY_JOIN_CMD )
//{
	sROOMINFO_PVP	m_RoomInfo;
	TCHAR			m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int				GetSize()
	{
		return (m_RoomInfo.m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_PVPLOBBY_JOIN_CMD)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_PVPLOBBY_JOIN_CMD));
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_JOIN_CMD )
//{
	sROOMINFO_HUNTING	m_RoomInfo;
	TCHAR				m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int					GetSize()
	{
		return (m_RoomInfo.m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_HUNTING_JOIN_CMD)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_HUNTING_JOIN_CMD));
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_JOIN_CMD )
//{
	sROOMINFO_MISSION	m_RoomInfo;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_JOIN_SYN )
//{
	KEYTYPE			m_HuntingKey;							//< 
	BYTE			m_byRoomPublic;							//< eZONE_BEGIN_STATE
	TCHAR			m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int				GetSize()
	{
		return (m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_HUNTING_JOIN_SYN)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_HUNTING_JOIN_SYN));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_JOIN_ACK )
//{
	KEYTYPE			m_HuntingKey;							//<
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_JOIN_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;


// 固记 曼啊
//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_JOIN_ACK )
//{
	KEYTYPE			m_MissionKey;							//< 
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_JOIN_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_JOIN_ACK )
//{
	KEYTYPE			m_PVPKey;							//< 
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_JOIN_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_LEAVE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_LEAVE_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_LOBBY_LEAVE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_LEAVE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_LEAVE_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_MISSION_LEAVE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_LEAVE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_LEAVE_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_LEAVE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_LEAVE_SYN )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_LEAVE_ACK )
//{
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_PVP_LEAVE_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_RELAY_CREATE_SYN )
//{
	CODETYPE					m_MapCode;			//< 
	sROOMINFO_BASE			m_RoomInfo;			//< 
	sROOMINFO_ADDITIONAL	m_AdditionalInfo;	//< 
	BYTE						m_byRoomPublic;							//< eZONE_BEGIN_STATE
	TCHAR						m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int					GetSize()
	{
		return (m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_HUNTING_RELAY_CREATE_SYN)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_HUNTING_RELAY_CREATE_SYN));
	}
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_RELAY_CREATE_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_RELAY_CREATE_BRD )
//{
	KEYTYPE					m_HuntingKey;
	CODETYPE					m_MapCode;			//< 
	sROOMINFO_BASE			m_RoomInfo;			//< 
	sROOMINFO_ADDITIONAL	m_AdditionalInfo;	//< 
	BYTE						m_byRoomPublic;							//< eZONE_BEGIN_STATE
	TCHAR						m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int					GetSize()
	{
		return (m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_HUNTING_RELAY_CREATE_BRD)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_HUNTING_RELAY_CREATE_BRD));
	}
//}
MSGPACKET_CG_END;



//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_RELAY_JOIN_SYN )
//{
	KEYTYPE			m_HuntingKey;							//< 
	BYTE			m_byRoomPublic;							//< eZONE_BEGIN_STATE
	TCHAR			m_szRoomPwd[MAX_ROOMPASSWORD_LENGTH];
	int				GetSize()
	{
		return (m_byRoomPublic==ZONEOPEN_PUBLIC?SIZE_CG_MSG(ZONE_HUNTING_RELAY_JOIN_SYN)-sizeof(TCHAR)*MAX_ROOMPASSWORD_LENGTH:SIZE_CG_MSG(ZONE_HUNTING_RELAY_JOIN_SYN));
	}
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_RELAY_JOIN_ACK )
//{
//	KEYTYPE			m_HuntingKey;	
//}
MSGPACKET_CG_END;

//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_HUNTING_RELAY_JOIN_NAK )
//{
	DWORD			m_dwErrorCode;
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_VILLAGE_MOVE_SYN )
//{
	MAPCODE			m_VillageMapCode;	
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_VILLAGE_MOVE_ACK )
//{
//}
MSGPACKET_CG_END;


//////////////////////////////////////////
MSGPACKET_CG_ZONE_BEGIN( ZONE_VILLAGE_MOVE_NAK )
//{
	BYTE			m_byErrorCode;
//}
MSGPACKET_CG_END;





/*////////////////////////////////////////////////////////////////////////
TRIGGER
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_CLICK_OBJECT_SYN )
//{
	DWORD						m_dwClickedObjectKey;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_ENTER_AREA_SYN )
//{
	int							m_iAreaID;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_LEAVE_AREA_SYN )
//{
	int							m_iAreaID;
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_DO_ACTION_BRD )
//{
	DWORD						m_dwTriggerID;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_ACTION_NAK )
//{
	DWORD						m_dwErrorCode;
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_REFLECTDAMAGE_BRD )
//{
	DWORD						m_dwObjectKey;
	DAMAGETYPE					m_wDamage;
	DWORD						m_dwHP;
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_AREADAMAGE_BRD )
//{
	DWORD						m_dwObjectKey;
	DAMAGETYPE					m_wDamage;
	DWORD						m_dwHP;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_PORTAL_ACK )
//{
	CODETYPE m_MapCode;
	CODETYPE m_FieldCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_REWARD_WINDOW_CMD )
//{
	BYTE						m_Num;		//< 
	BYTE						m_Type;		//< 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_REWARD_SYN )
//{
	BYTE						m_Num;		//<
	BYTE						m_Type;		//<
	enum CONST_VAL
	{
		ITEM1_SELECTED = (1<<0),
		ITEM2_SELECTED = (1<<1),
		ITEM3_SELECTED = (1<<2),
		ITEM4_SELECTED = (1<<3),
	};
	BYTE						m_SelectedItem;		//< 急琶茄 酒捞袍甸
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_REWARD_ACK )
//{
	MONEY						m_Money;
	sTOTALINFO_INVENTORY		m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(TRIGGER_REWARD_ACK) - (sTOTALINFO_INVENTORY::MAX_SLOT_NUM - m_ItemInfo.m_InvenCount - m_ItemInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_REWARD_NAK )
//{
	DWORD					m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_DONE_ACTION_INFO_CMD )
//{
	enum CONST_VAL { MAX_DONE_TRIGGER_ID_NUM = 100, };
	BYTE			m_byCount;
	DWORD			m_dwDoneTriggerID[MAX_DONE_TRIGGER_ID_NUM];

	int	GetSize()
	{
		return (SIZE_CG_MSG(TRIGGER_DONE_ACTION_INFO_CMD) - (MAX_DONE_TRIGGER_ID_NUM-m_byCount)*sizeof(DWORD));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRIGGER_BEGIN( TRIGGER_PLAY_EVENT_CMD )
//{
	int						m_nEventIndex;	
//}
MSGPACKET_CG_OBJECT_END;





/*////////////////////////////////////////////////////////////////////////
PVP
/*////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_INFO_CMD )
//{	
	struct 
	{
		BYTE				m_Team  :3;	
		BYTE				m_Count :5;		
	};
	struct sTEAM_INFO
	{
		DWORD				m_dwPlayerKey;
		BYTE				m_Team;
	} m_Info[MAX_PARTYMEMBER_NUM];

	int GetSize()
	{
		return SIZE_CG_MSG(PVP_INFO_CMD) - sizeof(sTEAM_INFO)*(MAX_PARTYMEMBER_NUM-m_Count);
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_INFO_BRD )
//{
	DWORD				m_dwPlayerKey;
	BYTE				m_Team;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_PRESS_KEY_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_PRESS_KEY_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_PRESS_KEY_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_MATCHLESS_MODE_START_CMD )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_MATCH_MODE_FIRST_START_CMD )
//{	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_MATCH_MODE_START_CMD )
//{	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_MATCH_MODE_START_BRD )
//{	
	DWORD				m_dwPlayerKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_SCORE_CMD )
//{
	DWORD				m_Score;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_RESULT_BRD )
//{
	DWORD				m_KillerKey;
	DWORD				m_DierKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PVP_BEGIN( PVP_END_BRD )
//{
//}
MSGPACKET_CG_OBJECT_END;




#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_MAP_H__
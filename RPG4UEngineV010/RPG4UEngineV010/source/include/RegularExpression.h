/*////////////////////////////////////////////////////////////////////////
文 件 名：RegularExpression.h
创建日期：2009年3月16日
最后更新：2009年3月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


正则语法
	替换语法：
		&		代表当前串
		\0		代表当前串
		\1~10	代表10组之内的子串，由()指定

	语法	目标	成功	替换	结果
	abc	abc	y	&	abc
	abc	xbc	n	-	-
	abc	axc	n	-	-
	abc	abx	n	-	-
	abc	xabcy	y	&	abc
	abc	ababc	y	&	abc
	ab*c	abc	y	&	abc
	ab*bc	abc	y	&	abc
	ab*bc	abbc	y	&	abbc
	ab*bc	abbbbc	y	&	abbbbc
	ab+bc	abbc	y	&	abbc
	ab+bc	abc	n	-	-
	ab+bc	abq	n	-	-
	ab+bc	abbbbc	y	&	abbbbc
	ab?bc	abbc	y	&	abbc
	ab?bc	abc	y	&	abc
	ab?bc	abbbbc	n	-	-
	ab?c	abc	y	&	abc
	^abc$	abc	y	&	abc
	^abc$	abcc	n	-	-
	^abc	abcc	y	&	abc
	^abc$	aabc	n	-	-
	abc$	aabc	y	&	abc
	^	abc	y	&	
	$	abc	y	&	
	a.c	abc	y	&	abc
	a.c	axc	y	&	axc
	a.*c	axyzc	y	&	axyzc
	a.*c	axyzd	n	-	-
	a[bc]d	abc	n	-	-
	a[bc]d	abd	y	&	abd
	a[b-d]e	abd	n	-	-
	a[b-d]e	ace	y	&	ace
	a[b-d]	aac	y	&	ac
	a[-b]	a-	y	&	a-
	a[b-]	a-	y	&	a-
	[k]	ab	n	-	-
	a[b-a]	-	c	-	-
	a[]b	-	c	-	-
	a[	-	c	-	-
	a]	a]	y	&	a]
	a[]]b	a]b	y	&	a]b
	a[^bc]d	aed	y	&	aed
	a[^bc]d	abd	n	-	-
	a[^-b]c	adc	y	&	adc
	a[^-b]c	a-c	n	-	-
	a[^]b]c	a]c	n	-	-
	a[^]b]c	adc	y	&	adc
	ab|cd	abc	y	&	ab
	ab|cd	abcd	y	&	ab
	()ef	def	y	&-\1	ef-
	()*	-	c	-	-
	*a	-	c	-	-
	^*	-	c	-	-
	$*	-	c	-	-
	(*)b	-	c	-	-
	$b	b	n	-	-
	a\	-	c	-	-
	a\(b	a(b	y	&-\1	a(b-
	a\(*b	ab	y	&	ab
	a\(*b	a((b	y	&	a((b
	a\\b	a\b	y	&	a\b
	abc)	-	c	-	-
	(abc	-	c	-	-
	((a))	abc	y	&-\1-\2	a-a-a
	(a)b(c)	abc	y	&-\1-\2	abc-a-c
	a+b+c	aabbabc	y	&	abc
	a**	-	c	-	-
	a*?	-	c	-	-
	(a*)*	-	c	-	-
	(a*)+	-	c	-	-
	(a|)*	-	c	-	-
	(a*|b)*	-	c	-	-
	(a+|b)*	ab	y	&-\1	ab-b
	(a+|b)+	ab	y	&-\1	ab-b
	(a+|b)?	ab	y	&-\1	a-a
	[^ab]*	cde	y	&	cde
	(^)*	-	c	-	-
	(ab|)*	-	c	-	-
	)(	-	c	-	-
		abc	y	&	
	abc		n	-	-
	a*		y	&	
	abcd	abcd	y	&-\&-\\&	abcd-&-\abcd
	a(bc)d	abcd	y	\1-\\1-\\\1	bc-\1-\bc
	([abc])*d	abbbcd	y	&-\1	abbbcd-c
	([abc])*bcd	abcd	y	&-\1	abcd-a
	a|b|c|d|e	e	y	&	e
	(a|b|c|d|e)f	ef	y	&-\1	ef-e
	((a*|b))*	-	c	-	-
	abcd*efg	abcdefg	y	&	abcdefg
	ab*	xabyabbbz	y	&	ab
	ab*	xayabbbz	y	&	a
	(ab|cd)e	abcde	y	&-\1	cde-cd
	[abhgefdc]ij	hij	y	&	hij
	^(ab|cd)e	abcde	n	x\1y	xy
	(abc|)ef	abcdef	y	&-\1	ef-
	(a|b)c*d	abcd	y	&-\1	bcd-b
	(ab|ab*)bc	abc	y	&-\1	abc-a
	a([bc]*)c*	abc	y	&-\1	abc-bc
	a([bc]*)(c*d)	abcd	y	&-\1-\2	abcd-bc-d
	a([bc]+)(c*d)	abcd	y	&-\1-\2	abcd-bc-d
	a([bc]*)(c+d)	abcd	y	&-\1-\2	abcd-b-cd
	a[bcd]*dcdcde	adcdcde	y	&	adcdcde
	a[bcd]+dcdcde	adcdcde	n	-	-
	(ab|a)b*c	abc	y	&-\1	abc-ab
	((a)(b)c)(d)	abcd	y	\1-\2-\3-\4	abc-a-b-d
	[ -~]*	abc	y	&	abc
	[ -~ -~]*	abc	y	&	abc
	[ -~ -~ -~]*	abc	y	&	abc
	[ -~ -~ -~ -~]*	abc	y	&	abc
	[ -~ -~ -~ -~ -~]*	abc	y	&	abc
	[ -~ -~ -~ -~ -~ -~]*	abc	y	&	abc
	[ -~ -~ -~ -~ -~ -~ -~]*	abc	y	&	abc
	[a-zA-Z_][a-zA-Z0-9_]*	alpha	y	&	alpha
	^a(bc+|b[eh])g|.h$	abh	y	&-\1	bh-
	(bc+d$|ef*g.|h?i(j|k))	effgz	y	&-\1-\2	effgz-effgz-
	(bc+d$|ef*g.|h?i(j|k))	ij	y	&-\1-\2	ij-ij-j
	(bc+d$|ef*g.|h?i(j|k))	effg	n	-	-
	(bc+d$|ef*g.|h?i(j|k))	bcdd	n	-	-
	(bc+d$|ef*g.|h?i(j|k))	reffgz	y	&-\1-\2	effgz-effgz-
	((((((((((a))))))))))	-	c	-	-
	(((((((((a)))))))))	a	y	&	a
	multiple words of text	uh-uh	n	-	-
	multiple words	multiple words, yeah	y	&	multiple words
	(.*)c(.*)	abcde	y	&-\1-\2	abcde-ab-de
	\((.*), (.*)\)	(a, b)	y	(\2, \1)	(b, a)
	\<abc\>	d abc f	y	&	abc
	\<abc\>	dabc f	n	-	-
	\<abc\>	d abcf	n	-	-
	\<abc\>	d abc_f	n	-	-


使用范例
	void main()
	{
		StringHandle Header =
			_T( "reTest.GetReplaceString\n" )
			_T( "@@测试中文Subject: reTest.GetReplaceString\n\n" )
			_T( "for(INT n=0; n<=nNum; n++)\n" )
			_T( "Cout << reTest[n] << endl;\n" )
			_T( "@@测试中文Cout << reTest[n] << endl;\n\n" )
			_T( "@@测试中文Cout << reTest[n] << endl;\n\n" )
			_T( "Cout << reTest[n] << endl;" );
		

		RegularExpression reTest = _T( "(@@[^:\n]*:)([^\n]*)" );


		int		nNum;
		BOOL		bResult;
		LPCSTR	szMath = Header;

		bResult = reTest.BeginMatch( szMath );
		for(;bResult;bResult = reTest.NextMatch())
		{
			CString sRet = reTest.GetReplaceString("[&][\\0][\\1][\\2]");
			nNum= reTest.GetSubTextCount();
			if(nNum <= 0)	break;
			for(INT n=0; n<=nNum; n++)
				Cout << reTest[n] << endl;
			szMath = (LPCSTR)reTest.SubTextAt(0) + reTest.SubLengthAt(0);
		}
	}


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGULAREXPRESSION_H__
#define __REGULAREXPRESSION_H__
#pragma once


#include "RegularExpressionDefine.h"
#include "StringHandle.h"

//class StringHandle;
class RegExpProgram;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _REGEXPLIB_API RegularExpression
{
public:
	RegularExpression();
	RegularExpression( LPCTSTR exp, BOOL iCase = 0 );
	RegularExpression( const RegularExpression &r );
	virtual ~RegularExpression();

public:
	const RegularExpression & operator=( const RegularExpression & r );
	const StringHandle operator[]( UINT nIndex ) const;

public:
	bool	BeginMatch				(LPCSTR szText, BOOL bNextMath=FALSE);
	bool	NextMatch				();
	bool	IsCompiledOK			() const;

public:
	int		GetSubTextCount	() const;
	int		SubTextStartAt		( UINT nIndex ) const;	///< nIndex子串开始位置
	int		SubLengthAt			( UINT nIndex ) const;	///< nIndex子串长度
	LPCSTR	SubTextAt			( UINT nIndex ) const;	///< nIndex子串
	LPCSTR	SubTextEndAt		( UINT nIndex ) const;	///< nIndex子串尾


	///<	source遵循正则表达式替换语法
	///<	& 或\0 	代表目标串
	///<	\1 ~ \10	代表1至10号子串
	StringHandle GetReplaceString	( LPCTSTR source ) const;

	StringHandle GetErrorString	() const;

#if defined( _RE_DEBUG )
	void Dump();
#endif

private:
	void _ClearErrorInfo() const;
	int safeIndex( UINT nIndex ) const;

private:
	LPCSTR						m_szText;	/* used to return substring offsets only */
	RegExpProgram *			m_pRegExp;
	mutable StringHandle		m_szError;
	LPCSTR						m_sz;
	VG_TYPE_PROPERTY			(TextOrigin, StringHandle);
};

#include "RegularExpression.inl"



#endif //__REGULAREXPRESSION_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：IScriptInterface.h
创建日期：2007年3月11日
最后更新：2007年3月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ISCRIPTINTERFACE_H__
#define __ISCRIPTINTERFACE_H__
#pragma once


#include "ScriptDefine.h"

enum eTIP_TYPE;

//class IGameObject;
class _SCRIPTLIB_API IScriptInterface
{
public:
	IScriptInterface();
	virtual ~IScriptInterface();

	virtual BOOL	HasVarTable				() = 0;
	virtual void	HighLightSourceLine	( int nLine ) = 0;
	virtual void	MarkSourceLine			( int nLine ) = 0;
public:
	virtual void	SetVar				(int               varIndex
                                    ,int               oprType
												,void*             pParams
												,int					 nParamCount = 1)= 0;

	virtual void	SetTeamVar			(int               varIndex
                                    ,int               oprType
												,void*             pParams
												,int					 nParamCount = 1)= 0;

	virtual int 	GetVar				(int               varIndex
                                    ,int					 nPlayerId = -1)		= 0;

	virtual void	SetTempValue		(DWORD dwValue)= 0;
	virtual DWORD	GetTempValue		()					= 0;

	virtual LPCSTR GetLabelOfVar		( int varIndex ) = 0;

	virtual void	SetCharacterID		( DWORD charID ) = 0;
	virtual DWORD	GetCharacterID		() = 0;
	virtual void	SetTargetNpcID		( DWORD destID ) = 0;
	virtual DWORD	GetTargetNpcID		() = 0;



	// function
	virtual void	 ShowDialog	( LPCSTR pszText , BOOL bEndDialog ) = 0;

	virtual void    Tell			( LPCSTR pszText ) = 0;
	virtual void    Talk			( DWORD time, DWORD color, LPCSTR pszText ) = 0;
	virtual void    TalkInShout	( DWORD time, DWORD color, LPCSTR pszText ) = 0;
	virtual void    TalkInSector	(int         nMapId
                              ,int         nX
										,int         nY
										,DWORD       time
										,DWORD       color
										,LPCSTR pszText)= 0;

	virtual void    Broadcast	( LPCSTR pszText ) = 0;

	virtual void	JumpToMap	( int nMapId, int x, int y ) = 0;

	virtual void	OpenShop ( DWORD npcID ) = 0;
	virtual void	DoCommand( LPCSTR command ) = 0;
	virtual void	ShowAsk	( KEYWORD varID, LPCSTR pszText ) = 0;
	virtual void	Show		( short stImageId, int x, int y, DWORD dwLife, DWORD dwFlag, DWORD dwFlagTime ) = 0;

	virtual	void	RemoveAllEquip	( ) = 0;
	virtual	void	RemoveAllItem	( ) = 0;
	virtual void	SetBornPoint	( DWORD dwMapID, int iPosX, int iPosY ) = 0;

	virtual void	AddItem					( LPCSTR szItemName, int nItemCount ) = 0;
	virtual void	DeleteItem				( LPCSTR szItemName, int nItemCount ) = 0;
	virtual BOOL	ExistItem				( LPCSTR szItemName, int nItemCount)=0;

	virtual BOOL	IsItemInEquip			( LPCSTR pItemName  ) = 0;
	virtual BOOL	IsSkillActived			( WORD ustSkillItemID,WORD ustSkillLevel ) = 0;

	virtual int		GetMapCodeByName	( LPCSTR pszMapName ) = 0;
	virtual void	TeashSkill			( const std::vector<int>&vectorSkill ) = 0;
	virtual void	AddSkill				( int nSkillId ) = 0;

	// 任务相关
	virtual DWORD	Quest_CancelQuest		( int nQuestID ) = 0;
	virtual DWORD	Quest_DoneQuest		( int nQuestID ) = 0;
	virtual void	Quest_SetQuestState	( int nQuestID, int nState ) = 0;
	virtual void	Quest_GetQuestState	( int nQuestID, int nVarID ) = 0;
	virtual void	Quest_RelateQuest		( int nQuestID, int nState, int nEnter ) = 0;
	virtual void	Quest_CanGetNewQuest	( int nVarID ) = 0;
	virtual void	Printf					( LPCSTR string, ... ) = 0;

	//
	virtual void	RefreshMonster	( int nMapId, int x, int y, LPCSTR szArea ) = 0;
	virtual void	PlayBGMusic		( LPCSTR szFileName, DWORD dwParam ) = 0;
	virtual void	MakeItem			(BYTE byCreatNewOrOld
                                 ,int  nRate
											,int  nIdBegin[]
											,int  nIdEnd[]
											,int  nCount)= 0;

	virtual int		GetCharCount	( int nMapId ) = 0;
	virtual void	ShowQuestDialog( KEYWORD varID, LPCSTR szInfo ) = 0;
	virtual void	UseSkill			( int nSkillId, int nSkillLevel ) = 0;
	virtual void	PopNpcList		() = 0;
	virtual void	ShowBank			() = 0;
	virtual int		GetWorldTime	( int nTimeType ) = 0;
	virtual int		GetBirthday		( int nTimeType ) = 0;

	virtual void	NpcMoveNext		() = 0;
	virtual void	NpcStopMove		() = 0;
	virtual void	NpcStartMove	() = 0;
	virtual void	NpcPlayAnim		(LPCSTR szName
                                 ,int    nTimes
											,LPCSTR szEndName
											,BOOL   bForView)= 0;
	virtual BOOL	CanLearnSkill	( int nSkillId, int nSkillLevel ) = 0;

	virtual void	DoRide			( int dwScriptID ) = 0;
	virtual int		GetDromeID		() = 0;
    virtual void	SetDialogName	( LPCSTR szName ) = 0;
	virtual void	SetHotkey		( int nIndex, int nID ) = 0;
    virtual BOOL	DumpInfoToDB		( LPCSTR szInfo ) = 0;

	virtual int		GetTeamPlayerCount	() = 0;
	virtual int		GetTeamPlayerLevel	(int nType ) = 0;
	virtual	int	GetTeamPlayerID		(UINT nIndex ) = 0;
	virtual void	SetTargetPlayer		(DWORD dwID ) = 0;
	virtual void	CreateMonster			(DWORD dwMapId
                                       ,float fPosX
													,float fPosY
													,float fRadius
													,float fBodySize
													,int   nMonsterIndex
													,int   nMonsterNum)= 0;
	virtual int		GetEmptySlotCount		() = 0;
   virtual void   CreateGuild				() = 0;
	virtual void	LogTip					(LPCTSTR   szTip
													,LPCTSTR   szTip2
													,eTIP_TYPE type)=0;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(IScriptInterface , _SCRIPTLIB_API);
//extern API_NULL ScriptInterface& singleton::GetScriptInterface();
#define theScriptInterface  singleton::GetIScriptInterface()

#endif //__ISCRIPTINTERFACE_H__


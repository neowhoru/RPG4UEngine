/*////////////////////////////////////////////////////////////////////////
文 件 名：RegenParser.h
创建日期：2008年4月29日
最后更新：2008年4月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	怪物生成点配置信息

工作原理：
	-1.	每个生成点，最多生成7 (MAX_REGEN_MONSTER_KIND)种怪物
			其中指定的每种怪物都可设定自己的生成最大量
	-2.	生成点可指定所属怪物群（AI）
	-3.	同一生成点拥有自己的生成频率，同一点的任何怪物生成共同使用


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __REGENPARSER_H__
#define __REGENPARSER_H__
#pragma once


#include <RegenParserDefine.h>
#include <THashTable.h>
#include <BaseResParser.h>

using namespace util;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API RegenParser : public BaseResParser
{
public:
	RegenParser();
	virtual ~RegenParser();

public:
	BASERES_UNIT_DECL();
	BASERES_UNIT_FOREACH2(sREGEN_INFO, m_pRegenInfos, dwID);

public:
	virtual BOOL		Init		( DWORD dwPoolSize );
	virtual VOID		Release	();
	virtual BOOL		Load		(LPCTSTR szFileName, BOOL bReload = FALSE );
	virtual VOID		Reload	();
	virtual VOID		Unload	();

	virtual sREGEN_INFO*		GetRegenInfo	(DWORD         dwMapCode
                                          ,DWORD         dwFieldIndex
														,DWORD         dwAreaID
														,WORD				wMonType );
	virtual sREGEN_INFO*		GetRegenInfo	(DWORD         dwRegenID);

	virtual BOOL		GetNearRegenPos	(LPCTSTR szMonsterName
													,MAPCODE landCode
													,FIELDID fieldCode
													,Vector3D& IN OUT vResult);
	virtual BOOL		GetNearRegenPos	(DWORD	dwMonsterCode
													,MAPCODE landCode
													,FIELDID fieldCode
													,Vector3D& IN OUT  vResult)=0;

public:
	template< class OPR >	VOID ForEachRandRegen(OPR & Operator );

	void _AddRandom(sREGEN_INFO *	pInfo){m_pRandRegenInfos->Add( pInfo, pInfo->dwID );}
private:
	THashTable<sREGEN_INFO*, ULONG64>	*m_pRegenInfos;
	THashTable<sREGEN_INFO*, ULONG64>	*m_pRandRegenInfos;

};//class RegenParser : public TSingleton<RegenParser>


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(RegenParser , _RESOURCELIB_API);
//extern API_NULL RegenParser& singleton::GetRegenParser();
#define theRegenParser  singleton::GetRegenParser()


#include "RegenParser.inl"


#endif //__REGENPARSER_H__
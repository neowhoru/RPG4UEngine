/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DTerrainOprDefine.h
创建日期：2008年12月19日
最后更新：2008年12月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DTERRAINOPRDEFINE_H__
#define __V3DTERRAINOPRDEFINE_H__
#pragma once



#include "WorldConst.h"


///////////////////////////////////////////////
const DWORD MAX_MAP_CHUNK_AMOUNT			= 16;
const DWORD MAX_RENDER_CHUNK_SIZE		= 41;
const DWORD MAX_VEGETATION_AMOUNT		= 3;
const DWORD  MAX_TERRAIN_LAYER_EFFECT	= 3;


enum eTERRAIN_TYPE
{
	 TERRAINTYPE_BASE
	,TERRAINTYPE_MULTIPLY
};


enum eTERRAIN_FLIP_OPR
{
	 TERRAIN_FLIP_V
	,TERRAIN_FLIP_H
	,TERRAIN_FLIP_SWAP
	,TERRAIN_FLIP_CW90
	,TERRAIN_FLIP_CCW90
	,TERRAIN_FLIP_ROT180
	,TERRAIN_FLIP_MAX
};

enum eTERRAIN_COPY_STATE
{
	 TERRAIN_COPY_TERRAIN		= _BIT(0)
	,TERRAIN_COPY_SCENE			= _BIT(1)
	,TERRAIN_COPY_SHADOW			= _BIT(2)
	,TERRAIN_COPY_TILEMASK		= _BIT(3)
	,TERRAIN_COPY_WATER			= _BIT(4)
	,TERRAIN_COPY_GRASS			= _BIT(5)
	,TERRAIN_COPY_ENVIRONMENT	= _BIT(6)
	,TERRAIN_COPY_ALL				= 0xffffffff
};

enum eUPDATELIGHT_FLAGS
{
	 UPDATELIGHT_GRASS	= _BIT(0)
	,UPDATELIGHT_WATER	= _BIT(1)
	,UPDATELIGHT_TREE		= _BIT(2)
	,UPDATELIGHT_LIGHT	= _BIT(3)
	,UPDATELIGHT_OBJECT	= _BIT(4)
	,UPDATELIGHT_LAYOUT	= _BIT(5)
	,UPDATELIGHT_GENERATE= _BIT(6)

	,UPDATELIGHT_NOCOLOR		= _BIT(30)	//不处理颜色
	,UPDATELIGHT_NOALPHA		= _BIT(31)	//不处理Alpha值
	,UPDATELIGHT_NODIFFUSE	= UPDATELIGHT_NOALPHA | UPDATELIGHT_NOCOLOR

	,UPDATELIGHT_ALL	= 0xFFFFFFFF
};

//////////////////////////////////////////////////
enum SIDES
{
	 SIDE_TOP=0
	,SIDE_LEFT
	,SIDE_RIGHT
	,SIDE_BOTTOM
	,SIDE_MAX
};



///////////////////////////////////////////////
enum eTERRAIN_LEVEL
{
	 TERRAIN_LEVEL_0 
	,TERRAIN_LEVEL_1
	,TERRAIN_LEVEL_2
	,TERRAIN_LEVEL_3
	,TERRAIN_LEVEL_MAX
};

///////////////////////////////////////////////
enum eTERRAIN_BLENDLAYER
{
	 TERRAIN_BLENDLAYER0		=	0
	,TERRAIN_BLENDLAYER1		=	1
	,TERRAIN_BLENDLAYER2		=	2
	,TERRAIN_BLENDLAYER_MAX	=	3
};

enum eTERRAIN_LAYER
{
	 TERRAIN_LAYER_BASE	=	0
	,TERRAIN_LAYER_1		=	1
	,TERRAIN_LAYER_2		=	2
	,TERRAIN_LAYER_3		=	3
	,TERRAIN_LAYER_MAX	=	4
};



#endif //__V3DTERRAINOPRDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PathInfoParser.h
创建日期：2007年11月28日
最后更新：2007年11月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MODELPATHINFOPARSER_H__
#define __MODELPATHINFOPARSER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "PathInfoParserDefine.h"
#include "PathInfoToken.h"

using namespace std;
typedef map<DWORD, sTypeLabelInfo>	TypeLabelInfoMap;
typedef TypeLabelInfoMap::iterator	TypeLabelInfoMapIt;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API PathInfoParser
{
public:
	PathInfoParser(void);
	~PathInfoParser(void);

public:
	BOOL				Load				(LPCSTR szFileName,BOOL bRegLabelInfo);


	LPCSTR			GetAvatarModelFile	(DWORD        nEquipId
                                 ,int          nType
											,int          nSex
											,int          nProfession
											,int nColor = -1);

	LPCSTR			GetPlayerActionModel	(int nProfession, int nSex);
	LPCSTR			GetPlayerHeader		(int nProfession, int nSex);


	PATHINFO_DECL(DataInfo)		;
	PATHINFO_DECL(UIDoc)			;
	PATHINFO_DECL(UIIcon)		;
	PATHINFO_DECL(UIItem)		;
	PATHINFO_DECL(Log)			;
	PATHINFO_DECL(Editor)		;
	PATHINFO_DECL(Texture)		;
	PATHINFO_DECL(Effect)		;
	PATHINFO_DECL(EffectModel)	;
	PATHINFO_DECL(EffectSound)	;
	PATHINFO_DECL(Sound)			;
	PATHINFO_DECL(Setting)		;
	PATHINFO_DECL(TerrainTexture);
	PATHINFO_DECL(Grass)			;
	PATHINFO_DECL(MonsterModel);
	PATHINFO_DECL(NpcModel)		;
	PATHINFO_DECL(SceneModel)	;
	PATHINFO_DECL(ItemModel)	;
	PATHINFO_DECL(Publish)	;
	PATHINFO_DECL(Root)	;

public:
	// 格式：%s%s\%s%s  (maps\  land  land  extra)
	// 范例：maps\Land\LandExtra
	LPCSTR			GetLandData				(LPCSTR szLandName,  LPCSTR szExtraInfo);

	//	格式：%s%s\%s    (maps\  land  extra)
	//	范例：maps\Land\Extra
	LPCSTR			GetLandPath				(LPCSTR szLandName,  LPCSTR szExtraInfo);

	//	%s
	LPCSTR			GetMapName				(LPCSTR szLandName,  INT nPlotX, INT nPlotY, LPCSTR szExtra="");

	// 格式：%s%s\\Plot[%02d,%02d]\\%s    (maps\  land x y extra)
	// 范例：maps\Land\Plot[0,0]\extra
	LPCSTR			GetMapPath				(LPCSTR szLandName,  INT nPlotX, INT nPlotY, LPCSTR szExtra="");

	// 格式：%s%s\\Plot[%02d,%02d]\\%s	    (maps\  land x y extra.dat)
	//	范例：maps\Land\Plot[0,0]\extra.dat
	LPCSTR			GetMapFilePath			(LPCSTR szLandName,  INT nPlotX, INT nPlotY, LPCSTR szExtra="");

	// 格式：%s%s\%s     (MapView\  land extra)
	//	范例：MapView\Land\extra
	LPCSTR			GetMapViewPath			(LPCSTR szLandName,  LPCSTR szExtra="");

	// 格式：%s%s\\PlotLevel[%02d]\\MapView[%d,%d]%s	   (maps\  land level x y extra)
	// 范例：mapview\EastLake\PlotLevel[00]\MapView[0,0].extra
	LPCSTR			GetMapViewFile			(LPCSTR szLandName,  INT nLevel, INT x, INT y, LPCSTR szExtra="");

public:
	BOOL				Register	(INT					nCategory
                           ,LPCSTR*				labelList
									,INT					nNum);

	BOOL				Register	(ePATHINFO_TYPE	pathInfo
									,PathInfoToken*	pPathInfoToken);

	LPCSTR			ParsePathInfo	(ePATHINFO_TYPE pathInfo,const sPATHINFO_TOKEN& tokenDat);	
protected:
	TypeLabelInfoMap		m_TypeLabelInfos;
	PathInfoToken*			m_PathInfoTokens[ePATHINFO_MAX];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(PathInfoParser , _BASE_API);
//extern _BASE_API PathInfoParser& singleton::GetPathInfoParser();
#define thePathInfoParser  singleton::GetPathInfoParser()




#endif //__MODELPATHINFOPARSER_H__
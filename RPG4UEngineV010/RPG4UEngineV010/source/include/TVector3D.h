/*////////////////////////////////////////////////////////////////////////
文 件 名：TVector3D.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TVECTOR3D_H__
#define __TVECTOR3D_H__
#pragma once

#include "MathStructDecl.h"
#include "MathAngle.h"

namespace math
{ 
template<typename Type>
class TVector3D : public TPoint3D<Type>
{
public:
    TVector3D() {};
    TVector3D( CONST Type * );
    TVector3D( CONST TPoint3D<Type>& );
    TVector3D( Type x, Type y, Type z );
    explicit TVector3D(Type scale);			//需要显式调用

    TVector3D&	Set	( Type x, Type y, Type z );

    // casting
    operator Type* ();
    operator CONST Type* () const;

    TVector3D& operator = ( CONST TVector3D& );
    TVector3D& operator = ( CONST Type fScalar );

    // assignment operators
    TVector3D& operator += ( CONST TVector3D& );
    TVector3D& operator -= ( CONST TVector3D& );
    TVector3D& operator *= ( CONST TVector3D& );
    TVector3D& operator /= ( CONST TVector3D& );
    TVector3D& operator += ( Type );
    TVector3D& operator -= ( Type );
    TVector3D& operator *= ( Type );
    TVector3D& operator /= ( Type );

    // unary operators
    TVector3D operator + () const;
    TVector3D operator - () const;

    // binary operators
    TVector3D operator + ( CONST TVector3D& ) const;
    TVector3D operator - ( CONST TVector3D& ) const;
    TVector3D operator * ( CONST TVector3D& ) const;
    TVector3D operator / ( CONST TVector3D& ) const;
    TVector3D operator * ( Type ) const;
    TVector3D operator / ( Type ) const;


    BOOL operator == ( CONST TVector3D& ) const;
    BOOL operator != ( CONST TVector3D& ) const;
    BOOL operator <  ( CONST TVector3D& ) const;
    BOOL operator >  ( CONST TVector3D& ) const;

public:
	//工具类
   Type Length () const;			///> 调用了sqrt，CPU消耗非常大，调用之前，最好
											///> 事先通过LengthSquared检测下是否相等

	Type LengthSquared () const;	///> 获取长度的平方，本函数，因为运算简单，效率相对高出许多
	Type Length2 () const;	

   Type Distance(const TVector3D& vec) const;
   Type DistanceSquared(const TVector3D& vec) const;

	Type DotProduct(const TVector3D& vec) const;	///> 返回其它矢量计算点积
																///>  点积可用来计算两矢量间的夹角，如果
																///>  a.都是单元矢量，则点积则是夹角的cos值
																///>  b.否则，cos值 = 点积值/ (v.len*v2.len)
																///>  c.另，还可计算点(Point)到面(Plane)的距离

   Type AbsDotProduct(const TVector3D& vec) const;	///> 计算绝对值点积，原理上与点积类似，只是每轴值都使用了abs值


	///> 与另一矢量计算叉积
	///> 结果返回两矢量共同的正交矢量
	///>  通常情况下，叉积矢量用来计算平面（由此两变量组成的平面）单位化法线
	///>  为同提高效率，叉积结果并没有做Normalize，故需手工调用
	///>  叉积矢量方向逆时针：由本矢量->Dest构造的扇形
	///>  譬如：UNIT_Y.crossProduct(UNIT_Z) = UNIT_X 而 UNIT_Z.crossProduct(UNIT_Y) = -UNIT_X
	///>  目前，这是使用右手坐标系，简单地说显示器左端为Y，底端为Z，则结果指向屏幕里（即人看屏幕的方向）
   TVector3D CrossProduct(const TVector3D& vec ) const;	


   Type Normalize();					///> 单位化矢量，即 Lenght/Magnitude = 1，即LenghSequard=1
											///> 结果本身也是单元矢量
											///> 返回单位化之前的长度
   TVector3D NormalizeNew(void) const;	//依据本矢量，生成新的单位矢量

   TVector3D MidPoint	( const TVector3D& vec ) const;
	TVector3D Interpolate( const TVector3D& vec2, float t);

   void MakeFloor	( const TVector3D& cmp );
   void MakeCeil	( const TVector3D& cmp );

   TVector3D PerpendicularNew(void) const;		///> 获取本矢量的正交矢量
															///> 可参考 Quaternion
   TVector3D ReflectNew(const TVector3D& normal) const;	///> 依据本矢量，参考normal生成新的镜射矢量

   TVector3D RandomNew(const Radian& angle,const TVector3D& up = TVector3D::ZERO) const;	///> 从给出的角度（弧度），生成随机的矢量

	///> 获取从本矢量到Dest矢量，最小旋转矩阵 arc quaternion
   //Quaternion GetRotationTo(const TVector3D& dest,
			//				const TVector3D& fallbackAxis = TVector3D::ZERO) const;


   BOOL IsZeroLength		(void) const;
	BOOL IsPositionEquals(const TVector3D& pos,	Type tolerance = eZERO) const;
	BOOL IsPositionClose (const TVector3D& pos,	Type tolerance = eZERO) const;
	BOOL IsDirectionEqual(const TVector3D& vec,	const Radian& tolerance) const;

public:
	//其它全局函数 
	inline friend TVector3D operator * ( float f, CONST TVector3D& v )
	{	
		return TVector3D(f * v.x, f * v.y, f * v.z);
	}
	inline friend TVector3D operator / ( const Type fScalar, const TVector3D& rkVector )
	{
		return TVector3D(	 fScalar / rkVector.x,	fScalar / rkVector.y, fScalar / rkVector.z);
	}

	inline friend TVector3D operator + (const TVector3D& v, const Type f)
	{
		return TVector3D(		v.x + f,		v.y + f,		v.z + f);
	}
	inline friend TVector3D operator + (const Type f , const TVector3D& v)
	{
		return TVector3D(		f + v.x ,		f + v.y ,		f + v.z);
	}
	inline friend TVector3D operator - (const TVector3D& v, const Type f)
	{
		return TVector3D(		v.x - f,		v.y - f,		v.z - f);
	}
	inline friend TVector3D operator - (const Type f , const TVector3D& v)
	{
		return TVector3D(		f - v.x ,		f - v.y,		f - v.z);
	}


public:
     // 特殊点
     static const TVector3D ZERO;
     static const TVector3D UNIT_X;
     static const TVector3D UNIT_Y;
     static const TVector3D UNIT_Z;
     static const TVector3D UNIT_X_NEG;
     static const TVector3D UNIT_Y_NEG;
     static const TVector3D UNIT_Z_NEG;
     static const TVector3D UNIT_SCALE;
};


};//namespace math

#include "TVector3D.inl"


#endif //__TVECTOR3D_H__
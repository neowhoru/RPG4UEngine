/*////////////////////////////////////////////////////////////////////////
文 件 名：TRingBuffer.h
创建日期：2008年1月26日
最后更新：2008年1月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TRINGBUFFER_H__
#define __TRINGBUFFER_H__
#pragma once


#include <memory.h>
#include "CommonDefine.h"

//------------------------------------------------------------------------------
template<class TYPE>
class TRingBuffer
{
public:
    TRingBuffer(int capacity);
    TRingBuffer();
    ~TRingBuffer();

public:
    /// assignment operator
    TRingBuffer<TYPE>& operator=(const TRingBuffer<TYPE>& src);

    /// initialize, only use when default constructor has been used
    void Initialize(int capacity);
    BOOL IsValid() const;
    BOOL IsEmpty() const;
    BOOL IsFull() const;

public:
    TYPE* Add();
    /// deletes the oldest element
    void DeleteTail();
    /// return pointer to Header element
    TYPE* GetHead() const;
    /// return pointer to pTail element
    TYPE* GetTail() const;

    /// return pointer to next element
    TYPE* GetNext(TYPE* e) const;
    /// return pointer to previous element
    TYPE* GetPrev(TYPE* e) const;

    /// return pointer to Start of ringbuffer array
    TYPE* GetStart() const;
    /// return pointer to End of ringbuffer array
    TYPE *GetEnd() const;

private:
    /// copy content
    void Copy(const TRingBuffer<TYPE>& src);
    /// delete all content
    void Delete();

private:
    TYPE *m_pStart;                        // m_pStart of ring buffer array
    TYPE *m_pEnd;                          // last element of ring buffer array
    TYPE *m_pTail;                         // oldest valid element
    TYPE *m_pHeader;                       // youngest element+1
};


//------------------------------------------------------------------------------
template<class TYPE>
TRingBuffer<TYPE>::TRingBuffer<TYPE>(int capacity)
{
    //_num++; // there is always 1 empty element in buffer
    m_pStart = new TYPE[capacity+1];
    m_pEnd   = m_pStart + capacity;
    m_pTail  = m_pStart;
    m_pHeader  = m_pStart;
}


//------------------------------------------------------------------------------
template<class TYPE>
TRingBuffer<TYPE>::TRingBuffer<TYPE>() :
    m_pStart(0),
    m_pEnd(0),
    m_pTail(0),
    m_pHeader(0)
{
    // empty
}

//---------------------------------------------------------------
template<class TYPE>
TRingBuffer<TYPE>::~TRingBuffer<TYPE>()
{
    Delete();
}

//---------------------------------------------------------------
template<class TYPE>
void TRingBuffer<TYPE>::Delete()
{
    if (m_pStart)
    {
        delete[] m_pStart;
    }
    m_pStart = 0;
    m_pEnd = 0;
    m_pTail = 0;
    m_pHeader = 0;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TRingBuffer<TYPE>::Copy(const TRingBuffer<TYPE>& src)
{
    if(0 != m_pStart)
    {
        int capacity = src.m_pEnd - src.m_pStart;
        m_pStart = new TYPE[capacity+1];
        m_pEnd   = m_pStart + capacity;
        m_pTail  = m_pStart;
        m_pHeader  = m_pStart;

        memcpy(src.m_pStart, m_pStart, capacity+1);
    }
}

/*////////////////////////////////////////////////////////////////////////
    Initialize with n elements, may only be called when
    default constructor has been used.
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
void TRingBuffer<TYPE>::Initialize(int capacity)
{
    ASSERT(!m_pStart);
    //_num++; // there is always 1 empty element in buffer
    m_pStart = new TYPE[capacity+1];
    m_pEnd   = m_pStart + capacity;
    m_pTail  = m_pStart;
    m_pHeader  = m_pStart;
}

//------------------------------------------------------------------------------
template<class TYPE>
TRingBuffer<TYPE>& TRingBuffer<TYPE>::operator=(const TRingBuffer<TYPE>& src)
{
    Delete();
    Copy(src);
    return *this;
}

//------------------------------------------------------------------------------
template<class TYPE>
BOOL TRingBuffer<TYPE>::IsValid() const
{
    return (0 != m_pStart);
}

/*////////////////////////////////////////////////////////////////////////
    Checks if ring buffer is empty

    Returns true if Header and Tail are in the same position otherwise
    false.
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
BOOL TRingBuffer<TYPE>::IsEmpty() const
{
    return (m_pHeader == m_pTail);
}

/*////////////////////////////////////////////////////////////////////////
    Checks if ring buffer is full
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
BOOL TRingBuffer<TYPE>::IsFull() const
{
    TYPE* e = m_pHeader;
    if (e == m_pEnd)
    {
        e = m_pStart;
    }
    else
    {
        e++;
    }

    return (e == m_pTail);
}

/*////////////////////////////////////////////////////////////////////////
    Add new unitialized m_pHeader element to ringbuffer.
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
TYPE* TRingBuffer<TYPE>::Add()
{
    ASSERT(m_pStart);
    ASSERT(!IsFull());
    TYPE *e = m_pHeader;
    if (m_pHeader == m_pEnd)
    {
        m_pHeader = m_pStart;
    }
    else
    {
        m_pHeader++;
    }
    return e;
}

/*////////////////////////////////////////////////////////////////////////
    Delete the oldest element
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
void TRingBuffer<TYPE>::DeleteTail()
{
    ASSERT(m_pStart);
    ASSERT(!IsEmpty());
    if (m_pTail == m_pEnd)
    {
        m_pTail = m_pStart;
    } else
    {
        m_pTail++;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE* TRingBuffer<TYPE>::GetHead() const
{
    if (m_pHeader == m_pTail)
    {
        // empty ringbuffer
        return 0;
    }
    TYPE *e = m_pHeader - 1;
    if (e < m_pStart)
    {
        e = m_pEnd;
    }
    return e;
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE* TRingBuffer<TYPE>::GetTail() const
{
    if (m_pHeader == m_pTail)
    {
        // empty ringbuffer
        return 0;
    }
    return m_pTail;
};

//------------------------------------------------------------------------------
template<class TYPE>
TYPE* TRingBuffer<TYPE>::GetNext(TYPE* e) const
{
    ASSERT(e);
    ASSERT(m_pStart);
    if (e == m_pEnd)
    {
        e = m_pStart;
    } else
    {
        e++;
    }
    if (e == m_pHeader)
    {
        return 0;
    } else
    {
        return e;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE* TRingBuffer<TYPE>::GetPrev(TYPE* e) const
{
    ASSERT(e);
    if (e == m_pTail)
    {
        return 0;
    }

    if (e == m_pStart)
    {
        return m_pEnd;
    }
    else
    {
        return e-1;
    }
}

/*////////////////////////////////////////////////////////////////////////
    Return physical m_pStart of ringbuffer array.
    Only useful for accessing the ringbuffer array elements directly.
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
TYPE* TRingBuffer<TYPE>::GetStart() const
{
    return m_pStart;
}

/*////////////////////////////////////////////////////////////////////////
    Return physical m_pEnd of ringbuffer array.
    Only useful for accessing the ringbuffer array elements directly.
/*////////////////////////////////////////////////////////////////////////
template<class TYPE>
TYPE* TRingBuffer<TYPE>::GetEnd() const
{
    return m_pEnd;
}



#endif //__TRINGBUFFER_H__
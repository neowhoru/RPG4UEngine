/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillInfoParser.h
创建日期：2008年3月19日
最后更新：2008年3月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SKILLINFOPARSER_H__
#define __SKILLINFOPARSER_H__
#pragma once


#include <GlobalInstanceSingletonDefine.h>
#include "StructBase.h"
#include "SkillInfoParserDefine.h"
#include <BaseResParser.h>
#include <ConstArray.h>


CONSTARRAY_DECLARE	(SkillAbilityRange
                     ,DWORD
							,0
							,SKILL_ABILITY_MAX
							,_RESOURCELIB_API);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API SkillInfoParser : public BaseResParser
{
public:
	SkillInfoParser();
	virtual ~SkillInfoParser();

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sSKILLINFO_COMMON, m_pSkillInfoHashTable	, m_SkillCode);
	BASERES_UNIT_ALLOC	(sVSKILLINFO_BASE	, m_pVSkillInfoHashTable, m_Code, VSkill);

	BASERES_OBJECT_ALLOC	(sSTYLEINFO_BASE, Style);
	BASERES_OBJECT_ALLOC	(SkillDetailInfo, Detail);
public:
	virtual BOOL		Init		( DWORD dwPoolSize );
	virtual VOID		Release	();
	virtual BOOL		Load		(LPCSTR         pszSkillFileName
										,LPCSTR         pszStyleFileName
										,LPCSTR         pszVSkillFileName
										,BOOL				 bReload=FALSE);

	virtual BOOL		LoadOptions	(LPCSTR pszFileName
											,BOOL   bReload=FALSE)		;

	virtual VOID		Reload();

	virtual VOID						SetFirst		() ;
	virtual sSKILLINFO_COMMON *	GetNext		() ;
	virtual SkillDetailInfo *		GetNextSkill() ;
	virtual sSTYLEINFO_BASE *		GetNextStyle() ;


	virtual sSKILLINFO_COMMON *		GetInfo					( DWORD SkillCode ) ;
	virtual sSKILLINFO_COMMON *		GetInfo					( LPCSTR	  szName )	  ;
	virtual SkillDetailInfo *			GetSkillInfo			( DWORD SkillCode ) ;
	virtual sSTYLEINFO_BASE *			GetStyleInfo			( DWORD StyleCode ) ;
	virtual sVSKILLINFO_BASE *			GetVSkillInfo			( DWORD SkillCode ) ;
	virtual sVSKILLINFO_BASE *			GetVSkillInfoBySkill	( DWORD SkillCode );

	virtual SkillCoolTimeTable*		GetSkillClassIDMap	()	;
	virtual LPCTSTR						GetAbilityName			(DWORD dwAbilityID)	;
	virtual sABILITY_OPTION_INFO*		GetAbilityOption		(DWORD dwAbilityID)	;

public:
	template<class OPR> VOID ForEachSkill(OPR &Opr );
	template<class OPR> VOID ForEachStyle(OPR &Opr );
	template<class OPR> VOID ForEachAbilityOption(OPR &Opr );


	void _AddNameSkill(sSKILLINFO_COMMON* pInfo){m_SkillNameMap[pInfo->m_SkillName] = pInfo;}
	void _AddCoolTime	(sSKILLINFO_BASE* pInfo);

public:
	virtual eATTRIBUTE_TYPE GetAttrType							( sABILITYINFO_BASE& ability)=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForAttackpower		( eATTACK_KIND attackType )=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForDefence			( eATTACK_KIND attackType )=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForBonusDefence		( eATTACK_KIND attackType )=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForStat				( eBASE_ATTRIBTUE	statType )=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForBonusDamage		( eARMOR_TYPE	armorType, int iValueType )=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForReduceDamage		( eATTACK_KIND attackType )=0;
	virtual eATTRIBUTE_TYPE GetAttrTypeForCriticalDamage	( int				ValueType )=0;

	virtual void				GetDamageText						(StringHandle&        sText
																			,sABILITYINFO_BASE* pAbility
																			,LPCTSTR            szTipFormat
																			,DWORD					dwAttackKind=INVALID_DWORD_ID)=0;
	virtual void				GetAbilityDesc						(StringHandle&       sResult
																			,sABILITYINFO_BASE*	pAbility
																			,LPCTSTR					szContentFormat=NULL)=0;


protected:
	virtual BOOL						_Load			( LPCSTR pszSkillFileName,	BOOL bReload = FALSE );
	virtual BOOL						_LoadStyle	( LPCSTR pszStyleFileName,	BOOL bReload = FALSE );
	virtual BOOL						_LoadVSkill	( LPCSTR pszStyleFileName,	BOOL bReload = FALSE );
	virtual VOID						Unload();

protected:
	VRSTR						m_sStyleFileName	;
	VRSTR						m_sVSkillFileName	;
	VRSTR						m_pszOptionInfoFileName			;

private:
	THashTable<sSKILLINFO_COMMON *>		*m_pSkillInfoHashTable;
	THashTable<sVSKILLINFO_BASE *>		*m_pVSkillInfoHashTable;

	SkillCoolTimeTable						m_mapSkillClassID;
	SkillNameMap								m_SkillNameMap;
	AbilityOptionMap							m_AbilityOptions;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(SkillInfoParser , _RESOURCELIB_API);
//extern _RESOURCELIB_API SkillInfoParser& singleton::GetSkillInfoParser();
#define theSkillInfoParser  singleton::GetSkillInfoParser()

#include "SkillInfoParser.inl"

#endif //__SKILLINFOPARSER_H__
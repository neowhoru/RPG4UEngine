/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemCompoundDialog.h
创建日期：2008年6月15日
最后更新：2008年6月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UIITEMCOMPOUNDDIALOG_H__
#define __UIITEMCOMPOUNDDIALOG_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "ConstSlot.h"
#include "ContainerDialogBase.h"
#include "ItemCompoundDialogDefine.h"
#include "GameUIManagerDefine.h"
#include "ItemMakeParserDefine.h"
//#include "ItemHandleSlotContainer.h"

class SlotUIListener;
class BaseSlot;
class BaseContainer;
struct MSG_CG_ITEM_ENCHANT_SUCCESS_ACK;
struct MSG_CG_ITEM_SOCKET_EXTRACT_SUCCESS_ACK;

using namespace gameui;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API ItemCompoundDialog	: public ContainerDialogBase
														//, public ItemHandleSlotContainer
{
public:

public:
	ItemCompoundDialog(void);
	virtual ~ItemCompoundDialog(void);
	

public:
	virtual BOOL	Init(  )=0;
	virtual VOID	Release()=0;

public:

	virtual VOID	Update				()=0;
	virtual BOOL	InitContainer		()=0;
	virtual VOID	ReleaseContainer	()=0;
	virtual VOID	FlushSlotListener	()=0;

public:
	virtual BOOL	InsertSlot( SLOTPOS posIndex, BaseSlot & slotDat )=0;
	virtual VOID	DeleteSlot( SLOTPOS posIndex, BaseSlot * pSlotOut )=0;

public:
	virtual VOID	NetworkProc	( MSG_BASE * pMsg )=0;
	virtual VOID	ShowWindow(BOOL val)=0;
	virtual VOID	CloseWindow()=0;

public:
	//////////////////////////////////
	virtual BOOL	OpenByNpc(DWORD dwNpcType)=0;

	////////////////////////////////
	//开始合成
	virtual BOOL			Enchant	()=0;
	virtual VOID			ReTry		(MATERIALTYPE eCompositeType)=0;
	virtual VOID			Exit		()=0;

public:
	///////////////////////////////
	//设置手续费
	virtual VOID			SetRequiredMoney	(MONEY Money)=0;
	virtual MONEY			GetRequiredMoney	()=0;
	virtual void			SetSucceedType		(eSUCCEEDRATE rateType)=0;
	virtual eSUCCEEDRATE	GetMinSucceedRate	()=0;
	virtual VOID			SetTargetInfo		(SLOTINDEX fromSlotIdx, SLOTPOS fromPos)=0;
	virtual VOID			ClearTarget			()=0;	

	virtual BOOL			ExistSocketItem	(BOOL bCheckNew)=0;
	virtual BOOL			ExistEmptySocket	()=0;
	virtual BOOL			FillSocketItems	(SLOTPOS arSocketItemPos[])=0;
	virtual BOOL			FillSocketState	(DWORD& dwSocketState)=0;
	virtual BOOL			ExistTargetItem	()=0;
	virtual BOOL			ExistSubResultItem()=0;
	virtual BOOL			CheckValidMaterails(eCOMPOSITE_CHECK check=eCOMPOSITE_CHECK_ALL)=0;
							
	virtual void			RemoveSocketItem	(SLOTPOS atPos);
	virtual void			RestoreSocketItem	(SLOTPOS atPos);
	virtual void			UpdateTargetCopy	();
	virtual void			ProcessPacketResult	(MSG_CG_ITEM_ENCHANT_SUCCESS_ACK* pMsg);
	virtual void			ProcessPacketResult	(MSG_CG_ITEM_SOCKET_EXTRACT_SUCCESS_ACK* pMsg);

public:
	//////////////////////////////////////////////////
	//以下为物品强化处理
	virtual VOID	RefreshEnchantInfo				()=0;
	virtual BOOL	ExistEnchantMaterials			(eCOMPOSITE_CHECK check=eCOMPOSITE_CHECK_ALL);
	virtual VOID	AutoRegisterEnchantMaterials	();
	virtual VOID	SpendEnchantMaterials			();


public:
	//////////////////////////////////////////////////
	//以下为普通Composite处理
	virtual BOOL	ExistCompositeMaterials			(MATERIALTYPE		  eItemCompositeType
																,SLOTINDEX            fromSlotIndex
																,eCOMPOSITE_CHECK check=eCOMPOSITE_CHECK_ALL);
	virtual BOOL	SpendCompositeMaterials			(MATERIALTYPE       eItemCompositeType
																,SLOTINDEX            fromSlotIndex)		;

	virtual BOOL	AutoRegisterCompositeMaterials(MATERIALTYPE       eItemCompositeType
																,SLOTINDEX            fromSlotIndex		=SI_INVENTORY);	

	virtual BOOL	AutoRegisterCompositeSockets	(MATERIALTYPE       eItemCompositeType
																,SLOTPOS				  fromPos
																,SLOTINDEX            fromSlotIndex		=SI_INVENTORY);	


	//////////////////////////////////////////////////
	//以下为ItemMake处理
	virtual INT*	RefreshMakeMaterialContainer	(sITEMMAKELEVEL*	pMakeLevel
                                                ,INT					nMaterialCol
																,INT					 nMaterialRow)=0;
	virtual BOOL	CheckMakeMaterialAt				(eMATERIAL_TYPE	type
                                                ,SLOTPOS				toPos)=0;
	virtual BOOL	FillMakeMaterialInfo				(DWORD&				dwNums
                                                ,SLOTPOS*			pMaterialPos)		;
	virtual BOOL	ExistMakeMaterialInfo			(eCOMPOSITE_CHECK check=eCOMPOSITE_CHECK_ALL);

protected:
	virtual void				InitEnchantInfo		()=0;

	virtual void				ActivateMaterials		(BOOL bActivate)=0;

protected:

	//设置合成类型
	VG_INT_GET_PROPERTY		(MaterialPosBase);
	VG_TYPE_PROPERTY			(CurrentCompositeType, MATERIALTYPE);
	BOOL							m_bValidEnchant;
	SLOTINDEX					m_FromTargetSlotIdx;
	SLOTPOS						m_FromTargetPos;
	VG_TYPE_GET_PROPERTY		(GameUIID, EGameUIType);
	VG_BOOL_PROPERTY			(Started);
	VG_TYPE_GET_PROPERTY		(SucceedType, eSUCCEEDRATE);
	VG_TYPE_PROPERTY			(EnchantLevel, eENCHANTLEVEL);

	eSUCCEEDRATE				m_PendingSucceedType;
	VG_TYPE_PROPERTY			(PendingTargetPos, SLOTPOS);

	VG_PTR_GET_PROPERTY		(TargetContainer,		BaseContainer);	///目标槽容器
	VG_PTR_GET_PROPERTY		(SubResultContainer,	BaseContainer);	///副物品容器
	VG_PTR_GET_PROPERTY		(SocketContainer,		BaseContainer);	///插槽容器
	VG_PTR_GET_PROPERTY		(MainContainer,		BaseContainer);	///插槽容器

	VG_TYPE_GET_PROPERTY		(EnchantType,			eITEMENCHANT_TYPE);	///副物品容器

	INT							m_arMakeMaterialStates[MATERIALTYPE_MAX];
	sITEMMAKELEVEL*			m_pItemMakeLevel;
	INT							m_nMaterailContainerCol;
	INT							m_nMaterailContainerRow;
	VG_CSTR_PROPERTY			(ItemName,	MAX_ITEMNAME2_LENGTH+1);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ItemCompoundDialog , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API ItemCompoundDialog& singleton::GetItemCompoundDialog();
#define theItemCompoundDialog  singleton::GetItemCompoundDialog()




#endif //__UIITEMCOMPOUNDDIALOG_H__
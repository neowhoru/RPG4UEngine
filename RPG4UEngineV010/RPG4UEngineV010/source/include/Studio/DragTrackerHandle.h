/*////////////////////////////////////////////////////////////////////////
文 件 名：DragTrackerHandle.h
创建日期：2008年11月2日
最后更新：2008年11月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __DRAGTRACKERHANDLE_H__
#define __DRAGTRACKERHANDLE_H__
#pragma once


class _STUDIOMAIN_API DragTrackerHandle : public CRectTracker
{
public:
// Constructors
	DragTrackerHandle();
	DragTrackerHandle(LPCRECT lpSrcRect, UINT nStyle);
	virtual ~DragTrackerHandle();
public:
	typedef BOOL (*PROC_CHECKMAGNET)		(LONG& x, LONG& y,LONG nOffsetX, LONG nOffsetY);
	typedef BOOL (*PROC_CLIENTTOCANVAS)		(CPoint& pt,CWnd* pWnd);
	typedef BOOL (*PROC_CLIENTTOCANVAS2)	(CRect&  rect,CWnd* pWnd);

public:
// Style Flags
	//enum StyleFlags
	//{
	//	solidLine = 1, dottedLine = 2, hatchedBorder = 4,
	//	resizeInside = 8, resizeOutside = 16, hatchInside = 32,
	//};

// Hit-Test codes
	//enum TrackerHit
	//{
	//	hitNothing = -1,
	//	hitTopLeft = 0, hitTopRight = 1, hitBottomRight = 2, hitBottomLeft = 3,
	//	hitTop = 4, hitRight = 5, hitBottom = 6, hitLeft = 7, hitMiddle = 8
	//};

// Attributes
	//UINT m_nStyle;      // current state
	//CRect m_rect;       // current position (always in pixels)
	//CSize m_sizeMin;    // minimum X and Y size during track operation
	//int m_nHandleSize;  // size of resize handles (default from WIN.INI)
	PROC_CHECKMAGNET		m_MagnetProc;
	PROC_CLIENTTOCANVAS	m_ClientToCanvasProc;
	PROC_CLIENTTOCANVAS2	m_ClientToCanvasProc2;
	CPoint					m_MagnetOffset;
public:
// Operations
	void Draw(CDC* pDC) const;
	void GetTrueRect(LPRECT lpTrueRect) const;
	BOOL SetCursor(CWnd* pWnd, UINT nHitTest,const CPoint* pPoint=NULL) const;
	BOOL Track(CWnd* pWnd, CPoint point, BOOL bAllowInvert = FALSE,
		CWnd* pWndClipTo = NULL);
	BOOL TrackRubberBand(CWnd* pWnd, CPoint point, BOOL bAllowInvert = TRUE);
	int HitTest(CPoint point) const;
	int NormalizeHit(int nHandle) const;

// Overridables
	virtual void DrawTrackerRect(LPCRECT lpRect, CWnd* pWndClipTo,
		CDC* pDC, CWnd* pWnd);
	virtual void AdjustRect(int nHandle, LPRECT lpRect);
	virtual void OnChangedRect(const CRect& rectOld);
	virtual UINT GetHandleMask() const;

// Implementation
public:

protected:
	//BOOL m_bAllowInvert;    // flag passed to Track or TrackRubberBand
	//CRect m_rectLast;
	//CSize m_sizeLast;
	//BOOL m_bErase;          // TRUE if DrawTrackerRect is called for erasing
	//BOOL m_bFinalErase;     // TRUE if DragTrackerRect called for final erase

	// implementation helpers
	int HitTestHandles(CPoint point) const;
	void GetHandleRect(int nHandle, CRect* pHandleRect) const;
	void GetModifyPointers(int nHandle, int**ppx, int**ppy, int* px, int*py);
	virtual int GetHandleSize(LPCRECT lpRect = NULL) const;
	BOOL TrackHandle(int nHandle, CWnd* pWnd, CPoint point, CWnd* pWndClipTo);
	void Construct();
};


#endif //__DRAGTRACKERHANDLE_H__
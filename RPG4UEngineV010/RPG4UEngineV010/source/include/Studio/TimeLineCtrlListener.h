/*////////////////////////////////////////////////////////////////////////
文 件 名：TimeLineCtrlListener.h
创建日期：2009年1月3日
最后更新：2009年1月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TIMELINECTRLLISTENER_H__
#define __TIMELINECTRLLISTENER_H__
#pragma once


////////////////////////////////////////////////////////////////////////////
enum eFRAME_HITTEST
{
	 HITTEST_OUTSIDE			= 0
	,HITTEST_IN_KEYPOINT		= 1
	,HITTEST_IN_FRAMEBODY	= 2
	,HITTEST_IN_EDGE_LEFT	= 3
	,HITTEST_IN_EDGE_RIGHT	= 4
	,HITTEST_IN_KEYRANGE		= 5	//在帧段
	,HITTEST_IN_FLAGKEY		= 6	//击中标志

};

////////////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////////
using namespace std;
typedef vector<DWORD>		FlagKeyArray;
struct sKEY_POINT
{
	DWORD					dwKeyIndex;
	string				sKeyLabel;
	FlagKeyArray		arFlagIndices;
	LPARAM				lpParam;
	BOOL					bUsed;

	sKEY_POINT()
	:dwKeyIndex(0)
	,lpParam(0)
	,bUsed(0)
	{
	}
};

typedef  const sKEY_POINT sKEY_POINTC;

////////////////////////////////////////////////////////////////////////////
class TimeLineKeyFrameIterator
{
public:
	virtual BOOL OnIterator(sKEY_POINTC& keyInfo)=0;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class TimeLineCtrlListener
{
public:
#ifdef NONE_V
	virtual HRESULT OnLineSelected(INT nSelectLine)
	{
		return S_OK;
	}
	virtual HRESULT OnLineMoved(INT nLineIndex, INT nMoveAdd)
	{
		return S_OK;
	}
	virtual HRESULT OnKeyFrameAdded(INT nKeyIndex, INT nFrameAt, INT nFrameLength, INT nAddFromKeyIndex)
	{
		return S_OK;
	}
	virtual HRESULT OnKeyFrameMoved(INT nKeyIndex, INT nKeyIndex2, INT nMoveAdd)
	{
		return S_OK;
	}
	virtual HRESULT OnFlagFrameMoved		(INT nKeyIndex, INT nFlagIndex, INT nMoveAdd)
	{
		return S_OK;
	}
	virtual HRESULT OnKeyFrameRemoved(LPARAM lpParam, INT nFrameAt, INT nFrameLength, INT nEffectKeyIndex)
	{
		return S_OK;
	}
	virtual HRESULT OnFlagFrameAdded		(INT nKeyIndex, INT nFrameAt, INT nFlagIndex)
	{
	}
	virtual HRESULT OnFlagFrameRemoved	(INT nKeyIndex, INT nFrameAt, INT nFlagIndex)
	{
	};

	virtual HRESULT OnKeyFrameSelected(INT nKeyIndex, INT /*nHitCode*/)
	{
		return S_OK;
	}
	virtual HRESULT OnLineRangeChanged(INT nLineIndex)
	{
		return S_OK;
	}
	virtual HRESULT OnFrameLocateTo(INT nLineIndex,INT nFrameIndex, INT nKeyIndex)
	{
		return S_OK;
	}
#else
	virtual HRESULT OnLineSelected(INT /*nSelectLine*/)=0;
	virtual HRESULT OnLineMoved(INT nLineIndex, INT nMoveAdd)=0;
	virtual HRESULT OnKeyFrameAdded(INT nKeyIndex, INT nFrameAt, INT nFrameLength, INT nAddFromKeyIndex)=0;
	virtual HRESULT OnKeyFrameMoved(INT nKeyIndex, INT nKeyIndex2, INT nMoveAdd)=0;
	virtual HRESULT OnKeyFrameRemoved(LPARAM lpParam, INT nFrameAt, INT nFrameLength, INT nEffectKeyIndex)=0;
	virtual HRESULT OnKeyFrameSelected(INT nKeyIndex, INT /*nHitCode*/)=0;	//HitCode参看eFRAME_HITTEST
	virtual HRESULT OnLineRangeChanged(INT nLineIndex)=0;
	virtual HRESULT OnFrameLocateTo(INT nLineIndex,INT nFrameIndex, INT nKeyIndex=-1)=0;

	virtual HRESULT OnFlagFrameAdded		(INT nKeyIndex, INT nFrameAt, INT nFlagIndex)=0;
	virtual HRESULT OnFlagFrameRemoved	(INT nKeyIndex, INT nFrameAt, INT nFlagIndex)=0;
	virtual HRESULT OnFlagFrameMoved		(INT nKeyIndex, INT nFlagIndex, INT nMoveAdd)=0;
	virtual BOOL	 CanModifyData			()=0;
	virtual BOOL	 CanSelectData			()=0;
#endif

};



#endif //__TIMELINECTRLLISTENER_H__
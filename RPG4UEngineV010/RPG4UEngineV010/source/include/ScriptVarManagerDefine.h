/*////////////////////////////////////////////////////////////////////////
文 件 名：ScriptVarManagerDefine.h
创建日期：2008年7月2日
最后更新：2008年7月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPTVARMANAGERDEFINE_H__
#define __SCRIPTVARMANAGERDEFINE_H__
#pragma once

#include "ScriptDefine.h"

using namespace std;

/*////////////////////////////////////////////////////////////////////////
	变量ID分布范围
	0        CHARACTER   5999   TEMPVAR    7999      SYSVAR    12999
	||________--_________||_____-------_____||______-------_______||
/*////////////////////////////////////////////////////////////////////////

#define VAR_CHAR_MAX		MAX_PLAYERVAR_SIZE
#define VAR_TEMP_MAX		(VAR_CHAR_MAX + MAX_TEMPVAR_SIZE)
#define VAR_SYS_MAX		(VAR_TEMP_MAX + MAX_SYSVAR_SIZE)

/*////////////////////////////////////////////////////////////////////////
	变量类型
/*////////////////////////////////////////////////////////////////////////
enum eVAR_TYPE
{
	 eVARTYPE_NONE = 0
	,eVARTYPE_CHAR
	,eVARTYPE_TEMP
	,eVARTYPE_SYS
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct LABEL_VAR
{
	enum	{	NAME_LENGTH = 32};

	LABEL_VAR()
	{
		m_Label[0]	= 0;
		m_Value		= 0;
	}

	char	m_Label[NAME_LENGTH];
	DWORD m_Value;
};


//typedef map<string, DWORD>		SysVarMap;
//typedef SysVarMap::iterator	SysVarMapIt;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct SYS_VAR 
{
	SYS_VAR()	{	m_Sequenes = VAR_TEMP_MAX;	}

	KEYWORD		GetKeyByName( const char* sz );
	LABEL_VAR *	GetByKey		( int key );

public:
	LABEL_VAR	m_Vars	[MAX_SYSVAR_SIZE];
	DWORD			m_Sequenes;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

inline KEYWORD SYS_VAR::GetKeyByName( const char* sz )
{
	for( UINT n=0; n<m_Sequenes - VAR_TEMP_MAX; n++ )
	{
		if( stricmp(m_Vars[n].m_Label, sz) == 0)
		{
			return VAR_TEMP_MAX+n;
		}
	}
	return INVALID_KEYWORD;
}

inline LABEL_VAR *SYS_VAR::GetByKey( int key )
{
	__CHECK2 (	key >= VAR_TEMP_MAX 
				&& key <  VAR_SYS_MAX
				, NULL );

	int index = key - VAR_TEMP_MAX;
	return &m_Vars[index];
}


#endif //__SCRIPTVARMANAGERDEFINE_H__
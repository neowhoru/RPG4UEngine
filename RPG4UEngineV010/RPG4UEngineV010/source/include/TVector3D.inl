/*////////////////////////////////////////////////////////////////////////
文 件 名：TVector3D<Type>.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TVECTOR3D_INL__
#define __TVECTOR3D_INL__
#pragma once

namespace math
{ 
template<typename Type> const TVector3D<Type> TVector3D<Type>::ZERO	( 0, 0, 0);
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_X( 1, 0, 0);
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_Y( 0, 1, 0);
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_Z( 0, 0, 1);
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_X_NEG(-1,  0,  0 );
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_Y_NEG( 0, -1,  0 );
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_Z_NEG( 0,  0, -1 );
template<typename Type> const TVector3D<Type> TVector3D<Type>::UNIT_SCALE(1, 1, 1);

template<typename Type>
inline TVector3D<Type>::TVector3D( CONST Type *pf )
{
#ifdef _DEBUG
    if(!pf)
        return;
#endif

    x = pf[0];
    y = pf[1];
    z = pf[2];
}

template<typename Type>
inline TVector3D<Type>::TVector3D( CONST TPoint3D<Type>& v )
{
    x = v.x;
    y = v.y;
    z = v.z;
}

template<typename Type>
inline TVector3D<Type>::TVector3D( Type fx, Type fy, Type fz )
{
    x = fx;
    y = fy;
    z = fz;
}

template<typename Type>
inline TVector3D<Type>::TVector3D(CONST Type scale)
{
    x = scale;
    y = scale;
    z = scale;
}

template<typename Type>
inline TVector3D<Type>&	TVector3D<Type>::Set( Type x, Type y, Type z )
{
    x = fx;
    y = fy;
    z = fz;
	 return *this;
}


// casting
template<typename Type>
inline TVector3D<Type>::operator Type* ()
{
    return (Type *) &x;
}

template<typename Type>
inline TVector3D<Type>::operator CONST Type* () const
{
    return (CONST Type *) &x;
}


template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator = ( CONST TVector3D<Type>& v )
{
    x = v.x;
    y = v.y;
    z = v.z;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator = (CONST Type scale )
{
    x = scale;
    y = scale;
    z = scale;
    return *this;
}

// assignment operators
template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator += ( CONST TVector3D<Type>& v )
{
    x += v.x;
    y += v.y;
    z += v.z;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator -= ( CONST TVector3D<Type>& v )
{
    x -= v.x;
    y -= v.y;
    z -= v.z;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator *= ( CONST TVector3D<Type>& v )
{
    x *= v.x;
    y *= v.y;
    z *= v.z;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator /= ( CONST TVector3D<Type>& v )
{
    x /= v.x;
    y /= v.y;
    z /= v.z;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator += ( Type f )
{
    x += f;
    y += f;
    z += f;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator -= ( Type f )
{
    x -= f;
    y -= f;
    z -= f;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator *= ( Type f )
{
    x *= f;
    y *= f;
    z *= f;
    return *this;
}

template<typename Type>
inline TVector3D<Type>& TVector3D<Type>::operator /= ( Type f )
{
    float fInv = 1.0f / (float)f;
    x = (Type)(x *fInv);
    y = (Type)(y *fInv);
    z = (Type)(z *fInv);
    //x *= fInv;
    //y *= fInv;
    //z *= fInv;
    return *this;
}


// unary operators
template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator + () const
{
    return *this;
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator - () const
{
    return TVector3D<Type>(-x, -y, -z);
}


// binary operators
template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator + ( CONST TVector3D<Type>& v ) const
{
    return TVector3D<Type>(x + v.x, y + v.y, z + v.z);
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator - ( CONST TVector3D<Type>& v ) const
{
    return TVector3D<Type>(x - v.x, y - v.y, z - v.z);
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator * ( CONST TVector3D<Type>& v ) const
{
    return TVector3D<Type>(x * v.x, y * v.y, z * v.z);
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator / ( CONST TVector3D<Type>& v ) const
{
    return TVector3D<Type>(x / v.x, y / v.y, z / v.z);
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator * ( Type f ) const
{
    return TVector3D<Type>(x * f, y * f, z * f);
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::operator / ( Type f ) const
{
    float fInv = 1.0f / (float)f;
    return TVector3D<Type>(x * fInv, y * fInv, z * fInv);
}

/////////////////////////////////////////////////
template<typename Type>
inline BOOL TVector3D<Type>::operator == ( CONST TVector3D<Type>& v ) const
{
    return x == v.x && y == v.y && z == v.z;
}

template<typename Type>
inline BOOL TVector3D<Type>::operator != ( CONST TVector3D<Type>& v ) const
{
    return x != v.x || y != v.y || z != v.z;
}

template<typename Type>
inline BOOL TVector3D<Type>::operator < ( CONST TVector3D<Type>& v ) const
{
    return x < v.x && y < v.y && z < v.z;
}

template<typename Type>
inline BOOL TVector3D<Type>::operator > ( CONST TVector3D<Type>& v ) const
{
    return x > v.x && y > v.y && z > v.z;
}


//////////////////////////////////////////
template<typename Type>
inline Type TVector3D<Type>::Length () const
{
   return Sqrt( x * x + y * y + z * z );
}

template<typename Type>
inline Type TVector3D<Type>::LengthSquared () const
{
   return x * x + y * y + z * z;
}

template<typename Type>
inline Type TVector3D<Type>::Length2 () const
{
   return LengthSquared ();
}

template<typename Type>
inline Type TVector3D<Type>::Distance(const TVector3D<Type>& v) const
{
   return (*this - v).Length();
}

template<typename Type>
inline Type TVector3D<Type>::DistanceSquared(const TVector3D<Type>& v) const
{
   return (*this - v).LengthSquared();
}

template<typename Type>
inline Type TVector3D<Type>::DotProduct(const TVector3D<Type>& vec) const
{
   return x * vec.x + y * vec.y + z * vec.z;
}

template<typename Type>
inline Type TVector3D<Type>::AbsDotProduct(const TVector3D<Type>& vec) const
{
   return Abs(x * vec.x) + Abs(y * vec.y)+ Abs(z * vec.z);
}


template<typename Type>
inline TVector3D<Type> TVector3D<Type>::CrossProduct( const TVector3D<Type>& vec ) const
{
   return TVector3D<Type>(
       y * rkVector.z - z * rkVector.y,
       z * rkVector.x - x * rkVector.z,
       x * rkVector.y - y * rkVector.x);
}

template<typename Type>
inline Type TVector3D<Type>::Normalize()
{
   Type fLength = Length();

   // Will also work for zero-sized vectors, but will change nothing
   if ( fLength > cZERO )
   {
       float fInvLength = 1.0f / (float)fLength;
       x = (Type)(x *fInvLength);
       y = (Type)(y *fInvLength);
       z = (Type)(z *fInvLength);
   }

   return fLength;
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::NormalizeNew(void) const
{
	TVector3D<Type> ret = *this;
	ret.Normalize();
	return ret;
}


template<typename Type>
inline TVector3D<Type> TVector3D<Type>::MidPoint( const TVector3D& vec ) const
{
   return TVector3D<Type>(
       ( x + vec.x ) * 0.5,
       ( y + vec.y ) * 0.5,
       ( z + vec.z ) * 0.5 );
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::Interpolate( const TVector3D& vec2, float t)
{
	float fInverseTime = 1.0f - t;
	return TVector3D(
	    x * t + vec2.x * fInverseTime
		,y * t + vec2.y * fInverseTime
		,z * t + vec2.z * fInverseTime
	);
}


template<typename Type>
void TVector3D<Type>::MakeFloor( const TVector3D<Type>& cmp )
{
	if( cmp.x < x )  x = cmp.x;
	if( cmp.y < y )  y = cmp.y;
	if( cmp.z < z )  z = cmp.z;
}

template<typename Type>
void TVector3D<Type>::MakeCeil( const TVector3D<Type>& cmp )
{
	if( cmp.x > x )  x = cmp.x;
	if( cmp.y > y )  y = cmp.y;
	if( cmp.z > z )  z = cmp.z;
}


template<typename Type>
inline TVector3D<Type> TVector3D<Type>::PerpendicularNew(void) const
{
	TVector3D<Type> perp = this->CrossProduct( TVector3D<Type>::UNIT_X );

	// Check length
	if( perp.IsZeroLength())
	{
		/* This vector is the Y axis multiplied by a scalar, so we have
		to use another axis.
		*/
		perp = this->CrossProduct( TVector3D<Type>::UNIT_Y );
	}
	return perp;
}

template<typename Type>
inline TVector3D<Type> TVector3D<Type>::ReflectNew(const TVector3D<Type>& normal) const
{
	return TVector3D<Type>( *this - ( 2 * this->DotProduct(normal) * normal ) );
}


template<typename Type>
inline TVector3D<Type> TVector3D<Type>::RandomNew(const Radian& angle,const TVector3D<Type>& up) const
{
	TVector3D<Type> newUp;

	if (up == TVector3D<Type>::ZERO)
	{
		// Generate an up vector
		newUp = this->PerpendicularNew();
	}
	else
	{
		newUp = up;
	}

	// Rotate up vector by random amount around this
	Quaternion q;
	q.FromAngleAxis( Radian(RandomUnit() * cPI_X2), *this );
	newUp = q * newUp;

	// Finally rotate this by given angle around randomised up
	q.FromAngleAxis( angle, newUp );
	return q * (*this);
}


//template<typename Type>
//Quaternion TVector3D<Type>::GetRotationTo(const TVector3D<Type>& dest, const TVector3D<Type>& fallbackAxis) const
//{
//   // Based on Stan Melax's article in Game Programming Gems
//   Quaternion q;
//   // Copy, since cannot modify local
//   TVector3D<Type> v0 = *this;
//   TVector3D<Type> v1 = dest;
//   v0.Normalize();
//   v1.Normalize();
//
//   REAL d = v0.dotProduct(v1);
//   // If dot == 1, vectors are the same
//   if (d >= 1.0f)
//   {
//       return Quaternion::IDENTITY;
//   }
//	if (d < (cZERO - 1.0f))
//	{
//		if (fallbackAxis != TVector3D<Type>::ZERO)
//		{
//			// rotate 180 degrees about the fallback axis
//			q.FromAngleAxis(Radian(cPI), fallbackAxis);
//		}
//		else
//		{
//			// Generate an axis
//			TVector3D<Type> axis = TVector3D<Type>::UNIT_X.crossProduct(*this);
//			if (axis.isZeroLength()) // pick another if colinear
//				axis = TVector3D<Type>::UNIT_Y.crossProduct(*this);
//			axis.Normalize();
//			q.FromAngleAxis(Radian(cPI), axis);
//		}
//	}
//	else
//	{
//			REAL s		= Sqrt( (1+d)*2 );
//			REAL invs	= 1 / s;
//
//		TVector3D<Type> c = v0.CrossProduct(v1);
//
//		   q.x = c.x * invs;
//		   q.y = c.y * invs;
//   		q.z = c.z * invs;
//   		q.w = s * 0.5;
//		q.Normalize();
//	}
//   return q;
//}


template<typename Type>
inline BOOL TVector3D<Type>::IsZeroLength(void) const
{
	return (LengthSquared() <= cZERO_T2);
}

template<typename Type>
inline BOOL TVector3D<Type>::IsPositionEquals(const TVector3D<Type>& pos, Type tolerance) const
{
	return	EqualReal(x, pos.x, tolerance) &&
				EqualReal(y, pos.y, tolerance) &&
				EqualReal(z, pos.z, tolerance);
}

template<typename Type>
inline BOOL TVector3D<Type>::IsPositionClose(const TVector3D<Type>& pos, Type tolerance) const
{
	return DistanceSquared(pos) <=
		(LengthSquared() + pos.LengthSquared()) * tolerance;
}

template<typename Type>
inline BOOL TVector3D<Type>::IsDirectionEqual(const TVector3D<Type>& vec, const Radian& tolerance) const
{
	Type dot			= DotProduct(vec);
	Radian angle	= Radian::ACos(dot);

	return angle.Abs() <= tolerance.RadianValue();

}

};//namespace math


#endif //__TVECTOR3D_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_ClientGameS.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

 Client <-> GameShell 互传协议

 *  前缀
		C : Client
		G : Game Server
		M : Master Server
		D : DBP Server

 * 后缀
		SYN - Server同步协议，ACK协议接受处理，NAK协议拒绝处理
		ACK - SYN协议接受处理
		NAK - SYN协议拒绝处理
		CMD - Server指令，无须返回响应
		BRD - Server向外广播消息
		DBR - DBProxy数据库代理向外广播消息

 * 协议命名规则：
		前缀_分类_协议_后缀
		譬如) _CG_CONNECTION_REGISTER_SYN


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PROTOCOL_CLIENTGAMES_H__
#define __PROTOCOL_CLIENTGAMES_H__
#pragma once



#include "Protocol_Define.h"
#include "Protocol_ClientGameS_Define.h"
#include "Protocol.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_CATEGORY
{
	 //_CG_CAT(NULL)
	 _CG_CAT(CONNECTION)		= PROTOCOL_RANGE(CLIENTGAME)
	,_CG_CAT(CHARINFO)							// 登录人物选择、登录人物信息
	,_CG_CAT(SYNC)									// 客户间同步化
	,_CG_CAT(MAP)									// 地图初始化及地图移动
	,_CG_CAT(ZONE)
	,_CG_CAT(CONVERSATION)						// 交流
	,_CG_CAT(ITEM)									// 物品
	,_CG_CAT(WAREHOUSE)							// 为了安装仓库:功能分离追加
	,_CG_CAT(VENDOR)								// 个人商店(店)
	,_CG_CAT(TRADE)								// 交易
	,_CG_CAT(PARTY)								// 组队
	,_CG_CAT(TRIGGER)								// 触发器
	,_CG_CAT(SKILL)								// 技能
	,_CG_CAT(STYLE)								// 姿势
	,_CG_CAT(STATUS)								// 人物状态变化
	,_CG_CAT(BATTLE)								// 战争
	,_CG_CAT(PVP)
	,_CG_CAT(GM)									// GM命令处理
	,_CG_CAT(ETC)									// 其他等等
	,_CG_CAT(SUMMON)								// 召唤
	,_CG_CAT(GUILD)
	,_CG_CAT(EVENT)								// 事件 及script
	,_CG_CAT(MAX)
};

PROTOCOL_RANGE_END(ClientGame, _CG_CAT(MAX), CLIENTGAME);



#include "Protocol_ClientGameS_Skill.h"
#include "Protocol_ClientGameS_Player.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_CONNECTION_PROTOCOL
{
	 _CG_SYN		(CONNECTION_HEARTBEAT)			//Ping操作
	,_CG_CMD		(CONNECTION_DISCONNECT)
	,_CG_CMD		(CONNECTION_RESTART)

	,_CG_SYN_AN	(CONNECTION_BACKTOCHARSELECT)	//退出人物选择界面
	,_CG_SYN_AN	(CONNECTION_ENTERSERVER)		//进入服务器
	,_CG_SYN		(CONNECTION_REENTERSERVER)	
	,_CG_SYN_AN	(CONNECTION_ENTERVILLAGEREQ)	//进入村庄地图请求
	,_CG_SYN_AN	(PREPARE_WORLD_CONNECT)			//进入游戏世界之前的连接


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_MAP_PROTOCOL
{
	 _CG_SYN_B (MAP_TELEPORT)		//地图瞬移处理
	,_CG_SYN_AB(MAP_MEETING_NPC	)	


};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_ZONE_PROTOCOL
{
	 _CG_SYN_AN	(ZONE_LIST_MISSIONLOBBY)	
	,_CG_SYN_AN	(ZONE_LIST_HUNTINGLOBBY)	
	,_CG_SYN_AN	(ZONE_LIST_PVPLOBBY)	
	,_CG_SYN_AN	(ZONE_LIST_HUNTING)	



	
	,_CG_SYN_N	(ZONE_LIST_REFRESH)	
	,_CG_ACK		(ZONE_LIST_REFRESH_HUNTINGLOBBY)	
	,_CG_ACK		(ZONE_LIST_REFRESH_MISSIONLOBBY)	
	,_CG_ACK		(ZONE_LIST_REFRESH_PVPLOBBY)	
	,_CG_ACK		(ZONE_LIST_REFRESH_HUNTING)	
	,_CG_ACK		(ZONE_LIST_REFRESH_NONE)	



	,_CG_SYN_N	(ZONE_SHORTCUT)	
	,_CG_ACK		(ZONE_SHORTCUT_HUNTINGLOBBY)	
	,_CG_ACK		(ZONE_SHORTCUT_MISSIONLOBBY)	
	,_CG_ACK		(ZONE_SHORTCUT_PVPLOBBY)	
	,_CG_ACK		(ZONE_SHORTCUT_HUNTING)	




	,_CG_SYN_N	(ZONE_FIND_ROOM_FROM_CHARID)	
	,_CG_ACK		(ZONE_MISSIONLOBBY_ROOM_FROM_CHARID)	
	,_CG_ACK		(ZONE_HUNTINGLOBBY_ROOM_FROM_CHARID)	
	,_CG_ACK		(ZONE_PVPLOBBY_ROOM_FROM_CHARID)	
	,_CG_ACK		(ZONE_HUNTING_ROOM_FROM_CHARID)	




	,_CG_SYN_AN	(ZONE_LOBBY_CREATE)	
	,_CG_SYN_AN	(ZONE_LOBBY_JOIN)	
	,_CG_BRD		(ZONE_MASTER_CHANGED)	
	,_CG_SYN_ABN(ZONE_LOBBY_READY)	
	,_CG_SYN_ABN(ZONE_LOBBY_READY_CANCEL)	



	,_CG_SYN_N	(ZONE_LOBBY_REQUEST_INFO)	
	,_CG_ACK		(ZONE_LOBBY_HUNTING_REQUEST_INFO)	
	,_CG_ACK		(ZONE_LOBBY_MISSION_REQUEST_INFO)	
	,_CG_ACK		(ZONE_LOBBY_PVP_REQUEST_INFO)	

	,_CG_SYN_ABN(ZONE_LOBBY_NUMBEROFPLAYER)	
	,_CG_SYN_ABN(ZONE_LOBBY_NONBLOCKSLOTNUM)	
	,_CG_SYN_AN	(ZONE_LOBBY_KICKPLAYER)	
	,_CG_SYN_ABN(ZONE_LOBBY_CHANGE_TEAM)	



	,_CG_SYN_ABN(ZONE_LOBBY_CHANGE_MAP)	
	,_CG_SYN_ABN(ZONE_LOBBY_HUNTING_CONFIG)	
	,_CG_SYN_ABN(ZONE_LOBBY_PVP_CONFIG)	
	,_CG_BRD		(ZONE_LOBBY_PVP_INFO)	

	

	,_CG_SYN_N	(ZONE_HUNTING_CREATE)	
	,_CG_SYN_N	(ZONE_MISSION_CREATE)	
	,_CG_SYN_N	(ZONE_PVP_CREATE)	
	,_CG_CMD		(ZONE_HUNTINGLOBBY_JOIN)	
	,_CG_CMD		(ZONE_MISSIONLOBBY_JOIN)	
	,_CG_CMD		(ZONE_PVPLOBBY_JOIN)	
	,_CG_CMD		(ZONE_HUNTING_JOIN)	
	,_CG_CMD		(ZONE_MISSION_JOIN)	
	,_CG_CMD		(ZONE_PVP_JOIN)	
	,_CG_SYN_AN	(ZONE_HUNTING_JOIN)	



	,_CG_SYN_AN	(ZONE_MISSION_JOIN)	
	,_CG_SYN_AN	(ZONE_PVP_JOIN)	
	,_CG_SYN_AN	(ZONE_LOBBY_LEAVE)	
	,_CG_SYN_AN	(ZONE_MISSION_LEAVE)	
	,_CG_SYN_AN	(ZONE_HUNTING_LEAVE)	
	,_CG_SYN_AN	(ZONE_PVP_LEAVE)	




	,_CG_SYN_BN		(ZONE_HUNTING_RELAY_CREATE)	
	,_CG_SYN_AN		(ZONE_HUNTING_RELAY_JOIN)	
	,_CG_SYN_AN		(ZONE_VILLAGE_MOVE)	

};


/*////////////////////////////////////////////////////////////////////////
// TRIGGER


/*////////////////////////////////////////////////////////////////////////
enum eCG_TRIGGER_PROTOCOL
{
	 _CG_SYN			(TRIGGER_CLICK_OBJECT	)	
	,_CG_SYN			(TRIGGER_ENTER_AREA		)	
	,_CG_SYN			(TRIGGER_LEAVE_AREA		)	
	,_CG_BRD			(TRIGGER_DO_ACTION		)	
	,_CG_NAK			(TRIGGER_ACTION			)	
	,_CG_BRD			(TRIGGER_REFLECTDAMAGE)	
	,_CG_ACK			(TRIGGER_PORTAL			)	
	,_CG_CMD			(TRIGGER_REWARD_WINDOW)	

	,_CG_SYN_AN		(TRIGGER_REWARD)	
	,_CG_CMD			(TRIGGER_DONE_ACTION_INFO)			//< action trigger结束
	,_CG_BRD			(TRIGGER_AREADAMAGE)	
	,_CG_CMD			(TRIGGER_PLAY_EVENT)	


};


/*////////////////////////////////////////////////////////////////////////
// GM
/*////////////////////////////////////////////////////////////////////////
enum eCG_GM_PROTOCOL
{
	 _CG_SYN			(GM_MONSTER_CREATE			)	
	,_CG_SYN			(GM_MONSTER_DESTROYALL		)	
	,_CG_SYN			(GM_MONSTER_DESTROYNEAR		)	

	,_CG_SYN_BN		(GM_OBSERVER		)	
	,_CG_SYN_AN		(GM_CHARINFO		)	
	,_CG_SYN			(GM_CREATE_ITEM		)	



	,_CG_SYN			(GM_CREATE_ENCHANT_ITEM		)	
	,_CG_SYN			(GM_DESTROY_ITEM_AROUND		)	
	,_CG_SYN			(GM_CREATE_MONEY		)	
	,_CG_SYN			(GM_LEVEL_UP		)	
	,_CG_SYN_A		(GM_STAT_UP		)	
	,_CG_SYN_A		(GM_SKILLPOINT_UP		)	
	,_CG_CMD			(GM_RELOAD_DATA		)	


	,_CG_SYN_AN		(GM_UNDEAD_MODE	)	
	,_CG_SYN_AN		(GM_ROOMINFO	)	
	,_CG_SYN_AN		(GM_CHANNELINFO	)	
	,_CG_SYN_BN		(GM_RESURRECTION	)	
	,_CG_SYN_BN		(GM_RECOVERY	)	
	,_CG_SYN_AN		(GM_ENTRANCE	)	
	,_CG_SYN_BN		(GM_SET_SPEED	)	
	,_CG_SYN_AN		(GM_FORCE_DISCONNECT	)	


	,_CG_SYN_AN		(GM_WARP	)	
	,_CG_SYN_N		(GM_MONSTER_DESTROYONE	)	
	,_CG_SYN_N		(GM_MONSTER_KILLONE	)	
	,_CG_SYN_N		(GM_STRING_CMD	)	


};



/*////////////////////////////////////////////////////////////////////////
// ETC 
/*////////////////////////////////////////////////////////////////////////
enum eCG_ETC_PROTOCOL
{
	 CG_ETC_CMD				
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_PVP_PROTOCOL
{
	 _CG_CMD_B		(PVP_INFO	)	
	,_CG_SYN_AN		(PVP_PRESS_KEY	)	
	,_CG_CMD			(PVP_MATCHLESS_MODE_START	)	
	,_CG_CMD			(PVP_MATCH_MODE_FIRST_START	)	
	,_CG_CMD_B		(PVP_MATCH_MODE_START	)	
	,_CG_CMD			(PVP_SCORE	)	
	,_CG_BRD			(PVP_RESULT	)	
	,_CG_BRD			(PVP_END	)	


};




#endif //__PROTOCOL_CLIENTGAMES_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishManager.h
创建日期：2006年1月1日
最后更新：2006年1月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

	1> 依据发布资源表做资源发布
		1	[测]TableText增加$(Tag)格式，为Tag数据
		2	增加Publish媒体目录
		3	建立BaseParser接口
			3.1	逻辑信息引用
			3.2	V资源引用
			3.3	素材引用
		4	建立ResPublishManager
			4.1	资源表TAG标识
			4.2	[测]Publish信息表（外来信息表关联  资源解析器TAG关联 发布路径）
				4.2.1	增加字段：V引用Tag
				4.2.2	生成逻辑信息表时，同时处理V引用
			4.3	增加ItemInfo发布 VItemInfo发布
			4.4	Archive增加 << 自动操作封装
			4.5	增加逻辑信息模板文件
			4.6	逻辑信息表 发布处理
				4.6.1	依据外来信息表处理各大资源引用
				4.6.2	逻辑信息引用（由信息表指定）
				4.6.3	依据模板，生成发布信息文件
				4.6.4	V资源引用 （由逻辑信息表指定）
				4.6.5	素材资源引用
				4.6.6	VItem武器模型ID须手工创建，方可导出信息  Equp模型表
			4.7	素材资源 批量发布
				4.7.1	单文件发布
				4.7.2	模型目录发布（同目录已用纹理、待用纹理、mesh、cfg等）
				4.7.3	素材引用（由V资源指定）
			4.8	Publish增加配置，按性别、人物类发布 （EquipInfo）
			4.9	发布系统插件化
			4.10	发布进度监听器
			4.11	发布素材操作监听器
			4.12	发布文件操作监听器

		5	资源发布业务流水线
			5.1	读取Publish信息表
			5.2	依据Publish表，处理各类资源表引用信息（标识为引用）
				5.2.1	处理逻辑层引用
				5.2.2	处理V资源层引用
				5.2.3	处理素材层引用
			5.3	依据素材层引用信息，将素材信息注册到Pushblish中
			5.4	发布所有Pushblish中素材



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __RESPUBLISHMANAGER_H__
#define __RESPUBLISHMANAGER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "ResPublishListener.h"
#include "ResPublishDefine.h"
#include "ISystem.h"
#include "ListenerBaseManager.h"
#include "BaseInfoParserHelper.h"

using namespace std;
typedef map<CODETYPE, sPUBLISH_INFO>		PublishInfoMap;
typedef pair<CODETYPE, sPUBLISH_INFO>		PublishInfoValue;
typedef PublishInfoMap::iterator				PublishInfoMapIt;

typedef multimap<DWORD, sPUBLISH_INFO*>	SortInfoMap;
typedef pair<DWORD, sPUBLISH_INFO*>			SortInfoValue;
typedef SortInfoMap::iterator					SortInfoMapIt;

typedef map<CODETYPE, sPUBLISH_DIR>			PublishDirMap;
typedef pair<CODETYPE, sPUBLISH_DIR>		PublishDirValue;
typedef PublishDirMap::iterator				PublishDirMapIt;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API ResPublishManager	: public ISystem
														, public ListenerBaseManager
{
public:
	ResPublishManager();
	~ResPublishManager();

public:
	//标准System函数
	virtual BOOL Init						()	;
	virtual void Release					()	;
	virtual BOOL RequestWorkingData	()	;
	virtual void ReleaseWorkingData	()	;
	virtual BOOL FrameMove				(DWORD dwTick);

public:
	BOOL					LoadSetting		(LPCSTR		szSettingFile);
	BOOL					Load				(LPCSTR		szInfoFile, LPCSTR	szDirFile);
	sPUBLISH_INFO*		GetInfo			(CODETYPE	code);
	BOOL					RegParser		(BaseInfoParser* pParser, DWORD dwPriority=0);

protected:
	BOOL					_LoadInfo		(LPCSTR		szInfoFile);
	BOOL					_LoadDirInfo	(LPCSTR		szInfoDir);


	/////////////////////////////////////////
	///资源目录发布处理
public:
	BOOL					PublishDirectorys	();
	BOOL					PublishDirInfo		(sPUBLISH_DIR& infoDir);
	BOOL					PublishDirectory	(LPCSTR szFilePath, BOOL bWithSubDir,sPUBLISH_DIR& infoDir);


	/////////////////////////////////////////
	///资源信息发布处理
public:
	BOOL					BeginPublish	(BOOL bLoadInfo);
	BOOL					PublishParser	(CODETYPE	code,BOOL bLoadInfo);

public:
	BOOL					ScanParser			(sPUBLISH_INFO& info,BOOL bLoadInfo);
	BOOL					PublishParser		(sPUBLISH_INFO& info,BOOL bJustSave=FALSE);
	BOOL					PublishParserRes	(sPUBLISH_INFO& info);
	DWORD					GetResFileCount	(ePUBLISH_TYPE type = ePUBLISH_MAX);

	BOOL					ScanParsers		(ePUBLISH_TYPE type,BOOL bLoadInfo);
	BOOL					PublishParsers	(ePUBLISH_TYPE type);
	BOOL					PublishResFiles(ePUBLISH_TYPE type);

	template<class ParserOperator>
	BOOL					ForEach( ParserOperator & Opr );
	template<class ParserOperator>
	BOOL					ForEachByPriority( ParserOperator & Opr );


public:
	/////////////////////////////////
	//资源信息发布监听处理
	void 					DoPublishReady		(DWORD dwAmount)		;
	void 					DoPublishStep		(INT   nStep =1)		;
	void 					DoPublishFinished	()		;
	void 					DoPublishFile		(LPCSTR szResFile, LPCSTR szResTo)		;


public:
	///素材拷贝处理
	BOOL					PublishFileTo		(LPCSTR szResFile, LPCSTR szResTo, BOOL bFilesInDir=FALSE);

protected:
	PublishInfoMap		m_PublishInfos;
	SortInfoMap			m_SortInfos;
	PublishDirMap		m_PublishDirs;

	VG_TYPE_PROPERTY	(DestPath,	VRSTR);
	VG_ARRAY_PROPERTY	(Sex,			BYTE, SEX_MAX);
	VG_ARRAY_PROPERTY	(CharType,	BYTE, PLAYERTYPE_MAX);
	VG_DWORD_PROPERTY	(StepIndex);
	VG_DWORD_PROPERTY	(StepAmount);
	VG_PTR_PROPERTY	(Impl, BaseInfoParserHelper);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ResPublishManager , _RESOURCELIB_API);
//extern _RESOURCELIB_API ResPublishManager& singleton::GetResPublishManager();
#define theResPublishManager  singleton::GetResPublishManager()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "ResPublishManager.inl"

#endif //__RESPUBLISHMANAGER_H__
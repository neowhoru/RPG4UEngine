/*////////////////////////////////////////////////////////////////////////
文 件 名：Matrix.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATRIX4_INL__
#define __MATRIX4_INL__
#pragma once

//namespace math
//{ 

//--------------------------
// Matrix
//--------------------------
inline Matrix4::Matrix4( bool  bIdentity )
{
	if(bIdentity)
		Identity();
}

inline Matrix4::Matrix4( CONST REAL* pf )
{
#ifdef _DEBUG
    if(!pf)
        return;
#endif

    memcpy(&_11, pf, sizeof(Matrix4));
}


inline Matrix4::Matrix4( Euler3C& vEuler )
{
	Set(vEuler);
}

inline Matrix4::Matrix4( CONST MATRIX4& mat )
{
    memcpy(&_11, &mat, sizeof(MATRIX4));
}


inline Matrix4::Matrix4	(REAL f11, REAL f12, REAL f13, REAL f14
								,REAL f21, REAL f22, REAL f23, REAL f24
								,REAL f31, REAL f32, REAL f33, REAL f34
								,REAL f41, REAL f42, REAL f43, REAL f44 )
{
	Set(	f11, f12, f13, f14,
			f21, f22, f23, f24,
			f31, f32, f33, f34,
			f41, f42, f43, f44 );
}


inline Matrix4& Matrix4::Set	(REAL f11, REAL f12, REAL f13, REAL f14
										,REAL f21, REAL f22, REAL f23, REAL f24
										,REAL f31, REAL f32, REAL f33, REAL f34
										,REAL f41, REAL f42, REAL f43, REAL f44 )
{
    _11 = f11; _12 = f12; _13 = f13; _14 = f14;
    _21 = f21; _22 = f22; _23 = f23; _24 = f24;
    _31 = f31; _32 = f32; _33 = f33; _34 = f34;
    _41 = f41; _42 = f42; _43 = f43; _44 = f44;
	 return *this;
}




// access grants

inline REAL& Matrix4::operator () ( UINT iRow, UINT iCol )
{
    return m[iRow][iCol];
}


inline REAL Matrix4::operator () ( UINT iRow, UINT iCol ) const
{
    return m[iRow][iCol];
}


// casting operators

inline Matrix4::operator REAL* ()
{
    return (REAL *) &_11;
}


inline Matrix4::operator CONST REAL* () const
{
    return (CONST REAL *) &_11;
}

inline REAL* Matrix4::operator[] (UINT iRow) const
{
	return (REAL*)m[iRow];
}

// assignment operators

inline Matrix4& Matrix4::operator *= ( CONST Matrix4& mat )
{
	//Matrix4 matT(*this);
	return Multiply(*this,mat);
}

inline Matrix4& Matrix4::Multiply ( CONST Matrix4& mat )
{
	return Multiply(*this,mat);
}

inline Matrix4& Matrix4::operator += ( CONST Matrix4& mat )
{
    _11 += mat._11; _12 += mat._12; _13 += mat._13; _14 += mat._14;
    _21 += mat._21; _22 += mat._22; _23 += mat._23; _24 += mat._24;
    _31 += mat._31; _32 += mat._32; _33 += mat._33; _34 += mat._34;
    _41 += mat._41; _42 += mat._42; _43 += mat._43; _44 += mat._44;
    return *this;
}


inline Matrix4& Matrix4::operator -= ( CONST Matrix4& mat )
{
    _11 -= mat._11; _12 -= mat._12; _13 -= mat._13; _14 -= mat._14;
    _21 -= mat._21; _22 -= mat._22; _23 -= mat._23; _24 -= mat._24;
    _31 -= mat._31; _32 -= mat._32; _33 -= mat._33; _34 -= mat._34;
    _41 -= mat._41; _42 -= mat._42; _43 -= mat._43; _44 -= mat._44;
    return *this;
}


inline Matrix4& Matrix4::operator *= ( REAL f )
{
    _11 *= f; _12 *= f; _13 *= f; _14 *= f;
    _21 *= f; _22 *= f; _23 *= f; _24 *= f;
    _31 *= f; _32 *= f; _33 *= f; _34 *= f;
    _41 *= f; _42 *= f; _43 *= f; _44 *= f;
    return *this;
}


inline Matrix4& Matrix4::operator /= ( REAL f )
{
    float fInv = 1.0f / (float)f;
    _11 *= fInv; _12 *= fInv; _13 *= fInv; _14 *= fInv;
    _21 *= fInv; _22 *= fInv; _23 *= fInv; _24 *= fInv;
    _31 *= fInv; _32 *= fInv; _33 *= fInv; _34 *= fInv;
    _41 *= fInv; _42 *= fInv; _43 *= fInv; _44 *= fInv;
    return *this;
}


// unary operators

inline Matrix4 Matrix4::operator + () const
{
    return *this;
}


inline Matrix4 Matrix4::operator - () const
{
    return Matrix4(-_11, -_12, -_13, -_14,
                      -_21, -_22, -_23, -_24,
                      -_31, -_32, -_33, -_34,
                      -_41, -_42, -_43, -_44);
}


// binary operators

inline Matrix4 Matrix4::operator * ( CONST Matrix4& mat ) const
{
	Matrix4 matT;
	return matT.Multiply(*this,mat);
}


inline Matrix4 Matrix4::operator + ( CONST Matrix4& mat ) const
{
    return Matrix4(_11 + mat._11, _12 + mat._12, _13 + mat._13, _14 + mat._14,
                      _21 + mat._21, _22 + mat._22, _23 + mat._23, _24 + mat._24,
                      _31 + mat._31, _32 + mat._32, _33 + mat._33, _34 + mat._34,
                      _41 + mat._41, _42 + mat._42, _43 + mat._43, _44 + mat._44);
}


inline Matrix4 Matrix4::operator - ( CONST Matrix4& mat ) const
{
    return Matrix4(_11 - mat._11, _12 - mat._12, _13 - mat._13, _14 - mat._14,
                      _21 - mat._21, _22 - mat._22, _23 - mat._23, _24 - mat._24,
                      _31 - mat._31, _32 - mat._32, _33 - mat._33, _34 - mat._34,
                      _41 - mat._41, _42 - mat._42, _43 - mat._43, _44 - mat._44);
}


inline Matrix4 Matrix4::operator * ( REAL f ) const
{
    return Matrix4(_11 * f, _12 * f, _13 * f, _14 * f,
                      _21 * f, _22 * f, _23 * f, _24 * f,
                      _31 * f, _32 * f, _33 * f, _34 * f,
                      _41 * f, _42 * f, _43 * f, _44 * f);
}


inline Matrix4 Matrix4::operator / ( REAL f ) const
{
    float fInv = 1.0f / (float)f;
    return Matrix4(_11 * fInv, _12 * fInv, _13 * fInv, _14 * fInv,
                      _21 * fInv, _22 * fInv, _23 * fInv, _24 * fInv,
                      _31 * fInv, _32 * fInv, _33 * fInv, _34 * fInv,
                      _41 * fInv, _42 * fInv, _43 * fInv, _44 * fInv);
}


inline Vector4D Matrix4::operator * ( const Vector4D& src ) /*const*/
{
	//Vector4D ret(src);
	return Multiply(src);
}



inline BOOL Matrix4::operator == ( CONST Matrix4& mat ) const
{
    return 0 == memcmp(this, &mat, sizeof(Matrix4));
}


inline BOOL Matrix4::operator != ( CONST Matrix4& mat ) const
{
    return 0 != memcmp(this, &mat, sizeof(Matrix4));
}

inline Matrix4& Matrix4::Identity()
{
	*this = IDENTITY;
	 return *this;
}

inline Matrix4& Matrix4::SetZero()
{
	*this = ZERO;
	return *this;
}



inline Matrix4& Matrix4::TranslationTo	(const Vector3D& tran)
{
	_41 = tran.x;
	_42 = tran.y;
	_43 = tran.z;
	return *this;
}

inline Vector3D Matrix4::Multiply(const Vector3D& v) const
{
	Vector3D ret(v);
	return ret.Multiply(*this);
}


inline REAL Matrix4::Det()  const
{
    return
        (_11 * _22 - _12 * _21) * (_33 * _44 - _34 * _43)
       -(_11 * _23 - _13 * _21) * (_32 * _44 - _34 * _42)
       +(_11 * _24 - _14 * _21) * (_32 * _43 - _33 * _42)
       +(_12 * _23 - _13 * _22) * (_31 * _44 - _34 * _41)
       -(_12 * _24 - _14 * _22) * (_31 * _43 - _33 * _41)
       +(_13 * _24 - _14 * _23) * (_31 * _42 - _32 * _41);
}


inline Matrix4&	Matrix4::Transpose()
{
    math::Swap(_12, _21);
    math::Swap(_13, _31);
    math::Swap(_14, _41);
    math::Swap(_23, _32);
    math::Swap(_24, _42);
    math::Swap(_34, _43);
	 return *this;
}

inline Vector3D Matrix4::ComponentX() const
{
    return Vector3D (_11,_12,_13);
}

inline Vector3D Matrix4::ComponentY() const
{
    return Vector3D (_21,_22,_23);
}
inline Vector3D Matrix4::ComponentZ() const
{
    return Vector3D (_31,_32,_33);
}
inline Vector3D Matrix4::GetPosition() const
{
    return Vector3D (_41,_42,_43);
}
inline Vector3D Matrix4::ComponentPos() const
{
    return Vector3D (_41,_42,_43);
}

inline Matrix4& Matrix4::Scaling(REAL scale, BOOL bIdentity)
{
	return Scaling(Vector3D(scale,scale,scale),  bIdentity);
}

inline Matrix4& Matrix4::Scaling(REAL x,	REAL y,REAL z, BOOL bIdentity)
{
	return Scaling(Vector3D(x,y,z),  bIdentity);
}

inline Matrix4& Matrix4::SetPosition(const Vector3D& vPos)
{
	return TranslationTo(vPos);
}

inline Matrix4& Matrix4::SetComponentX(const Vector3D& vPos)
{
	*(Vector3D*)m_Element[0] = vPos;
	return *this;
}
inline Matrix4& Matrix4::SetComponentY(const Vector3D& vPos)
{
	*(Vector3D*)m_Element[1] = vPos;
	return *this;
}
inline Matrix4& Matrix4::SetComponentZ(const Vector3D& vPos)
{
	*(Vector3D*)m_Element[2] = vPos;
	return *this;
}
inline Matrix4& Matrix4::SetComponentPos(const Vector3D& vPos)
{
	*(Vector3D*)m_Element[3] = vPos;
	return *this;
}


inline Matrix4& Matrix4::Translation(REAL t, BOOL bIdentity)
{
	return Translation(Vector3D(t,t,t),  bIdentity);
}

inline Matrix4& Matrix4::Translation(REAL x,	REAL y,REAL z, BOOL bIdentity)
{
	return Translation(Vector3D(x,y,z),  bIdentity);
}


//};//namespace math


#endif //__TMATRIX_INL__
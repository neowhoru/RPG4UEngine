/*////////////////////////////////////////////////////////////////////////
文 件 名：TextResManager.h
创建日期：2007年3月18日
最后更新：2007年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	多国语言支持 

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __LANGUAGETEXTMANAGER_H__
#define __LANGUAGETEXTMANAGER_H__
#pragma once

#include <THashTable.h>
#include <BaseResParser.h>

using namespace std;
using namespace util;

////////////////////////////////
struct sTEXTINFO_BASE
{
	DWORD		m_Code;
	VRSTR		m_Text;
	DWORD		m_VoiceID;
};

struct sDESCINFO_BASE : public sTEXTINFO_BASE
{
};


typedef DWORD								TEXT_ID;
typedef THashTable<sTEXTINFO_BASE*>	TextInfoTable;

////////////////////////////////
typedef map<TEXT_ID, VRSTR>	TextIDMap;
typedef TextIDMap::iterator	TextIDMapIt;
typedef pair<TEXT_ID, VRSTR>	TextIDPair;


////////////////////////////////
struct sTEXT_INFO
{
	VRSTR		m_Text;
	DWORD		m_VoiceID;
};

////////////////////////////////
typedef map<TEXT_ID, sTEXT_INFO>		TextDetailMap;
typedef TextDetailMap::iterator		TextDetailMapIt;
typedef pair<TEXT_ID, sTEXT_INFO>	TextDetailPair;



class StringManager;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API TextResManager : public BaseResParser
{
public:
	TextResManager(void);
	virtual ~TextResManager(void);

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sTEXTINFO_BASE, m_pDataInfos, m_Code);
	BASERES_UNIT_ALLOC	(sDESCINFO_BASE, m_pDescInfos, m_Code, Desc);
	BASERES_UNIT_TABLE	(sTEXTINFO_BASE, TextInfo);
	BASERES_UNIT_TABLE0	(sDESCINFO_BASE, m_pDescInfos, DescInfo);

public:
	virtual BOOL		Init			(LPCSTR szFileName, LPCSTR szDescFile, DWORD dwPoolSize);
	virtual void		Release		();


public:
	virtual LPCSTR	GetString	( TEXT_ID	nIndex );
	virtual LPCSTR	GetDesc		( TEXT_ID	nIndex );
	//virtual LPCSTR	GetPath	( TEXT_ID	nIndex );

	virtual sTEXT_INFO*	GetDetail	( TEXT_ID	nIndex );
	virtual DWORD			GetVoiceID	( TEXT_ID	nIndex );

	virtual BOOL		Load			( LPCSTR		szFileName );
	virtual BOOL		LoadDesc		( LPCSTR		szFileName );

protected:
	virtual BOOL		_Load			( LPCSTR		szFileName, BOOL bDesc );

	void _AddDesc(DWORD dwID,VRSTR sText)	{	m_mpDescs.insert(TextIDPair(dwID,sText));	}
	void _AddText(DWORD dwID,VRSTR sText)	{	m_mpTexts.insert(TextIDPair(dwID,sText));	}
	void _AddDetail(DWORD dwID,sTEXT_INFO&	detail){	m_mpDetails.insert(TextDetailPair(dwID,detail));	}
private:
	TextIDMap				m_mpTexts;
	TextIDMap				m_mpDescs;
	TextDetailMap			m_mpDetails;
	VG_PTR_GET_PROPERTY	(TextBuffer,	StringManager);
};


#define _STRING(x)	theTextResManager.GetString(x)
#define _DESC(x)		theTextResManager.GetDesc(x)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(TextResManager , _RESOURCELIB_API);
//extern API_NULL TextResManager& singleton::GetTextResManager();
#define theTextResManager  singleton::GetTextResManager()



#include "TextResManager.inl"


#endif //__LANGUAGETEXTMANAGER_H__
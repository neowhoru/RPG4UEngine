/*////////////////////////////////////////////////////////////////////////
文 件 名：PluginManager.h
创建日期：2006年12月2日
最后更新：2006年12月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	TLinkerManager			自动把PluginScanListener 增加到 ListenerBaseManager中
	ListenerBaseManager	用来管理 PluginScanListener

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PLUGINMANAGER_H__
#define __PLUGINMANAGER_H__
#pragma once

#include "ApplicationDefine.h"
#include "PluginScanListener.h"
#include "ListenerBaseManager.h"
#include "TLinkerManager.h"
#include "PluginManagerDefine.h"

class IniFile;
namespace gamemain
{

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API PluginManager 
									: public ListenerBaseManager
									, public TLinkerManager<PluginScanListenerLinker>
{
	typedef ListenerBaseManager _Parent;
public:
	struct _BASE_API SPlugin
	{
		HINSTANCE			hInstance;
		FNPluginDetech		fnDetech;
		FNPluginMainEntry	fnEntry;
		FNPluginEnd			fnEnd;
		BOOL					bLoaded;
		SPlugin()
		{
			bLoaded		= FALSE;
			hInstance	= NULL;
			fnDetech		= NULL;;
			fnEntry		= NULL;;
			fnEnd			= NULL;;
		}
	};

	typedef std::map<std::string,SPlugin>		PluginMap;
	typedef PluginMap::iterator					PluginMapIt;
	typedef std::pair<std::string,SPlugin>		PluginMapPair;

public:
	PluginManager();
	virtual ~PluginManager();

public:
	// LPCSTR szPath	插件路径或配置文件
	//BOOL bBySetting	TRUE时，从szPath配置文件中处理插件；
	//						FALSE时，直接从szPath搜索模式中，扫描可用插件
	void BeginScanAllPlugins(LPCSTR szPath,BOOL bBySetting);

public:
	//初始化插件
	void InitPlugins();
	void ReleasePlugins();

	//扫描插件监听链接器
	virtual void PrevScanLinkers();
	virtual void OnLinkerScan(PluginScanListenerLinker* pLinker);
	virtual void AfterScanLinkers();

	//增加可用插件，配置型管理
	void AddPluginFile(LPCSTR szPlugin);
protected:
	void AddPluginFilesBySesstion(IniFile* pFile,LPCSTR szSession,BOOL bCommon);

	void LoadPluginSetting(LPCSTR szSetting);
	void LoadPluginsInSetting();

	BOOL LoadPlugin(LPCSTR szPath, SPlugin& plu);
	void ScanAllPlugins(LPCSTR szPath=NULL);
	void PrevScanPlugins()
	{
		_Parent::ProcessListeners(PluginScanListener::funcPrevScanPlugin);
	}
	void AfterScanPlugins()
	{
		_Parent::ProcessListeners(PluginScanListener::funcAfterScanPlugin);
	}
	void OnBeginNewLibrary()
	{
		_Parent::ProcessListeners(PluginScanListener::funcBeginNewLibrary);
	}

	void OnEndNewLibrary()
	{
		_Parent::ProcessListeners(PluginScanListener::funcEndNewLibrary);
	}


	LPCTSTR GetPluginPath(){return m_sPluginPath;}

protected:
	VG_TYPE_PROPERTY	(ExtraSession, StringHandle);

	StringHandle		m_sPluginPath;
	PluginMap			m_mapPlugins;
	//PluginScanListenerMap	m_mapPluginsScanListeners;
};

GLOBALINST_DECL(PluginManager, _BASE_API);
};


#endif //__PLUGINMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PathInfoParserDefine.h
创建日期：2008年7月14日
最后更新：2008年7月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PATHINFOPARSERDEFINE_H__
#define __PATHINFOPARSERDEFINE_H__
#pragma once

//#include "MediaPathManagerDefine.h"

enum  ePATHINFO_TYPE
{
	 ePATHINFO_LANDDATA			= 0
	,ePATHINFO_LANDPATH			= 1
	,ePATHINFO_MAPPATH			= 2
	,ePATHINFO_MAPFILE			= 3
	,ePATHINFO_MAPNAME			= 4
	,ePATHINFO_PLAYERHUD			= 5
	,ePATHINFO_CHARSKELETON		= 6
	,ePATHINFO_EQUIPMODEL		= 7
	,ePATHINFO_EQUIPMODEL2		= 8
	,ePATHINFO_MAPVIEWPATH		= 9
	,ePATHINFO_MAPVIEWFILE		= 10
	,ePATHINFO_EQUIPMODEL_DIR	= 11
	,ePATHINFO_MAX	
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PATHINFO_DECL(Name)\
	LPCSTR			Get##Name##Path		(LPCSTR szInfoFile);

#define PATHINFO_IMPL(Name,Index)\
										LPCSTR PathInfoParser::Get##Name##Path(LPCSTR szInfoFile)\
										{\
											LPCSTR szPath;\
											szPath = PATH_FMT	("%s%s" \
																	,_MEDIAPATH(Index)\
																	,szInfoFile);\
											return szPath;\
										}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _PATH_LANDDATA				thePathInfoParser.GetLandData
#define _PATH_LANDDIR				thePathInfoParser.GetLandPath
#define _PATH_MAPNAME				thePathInfoParser.GetMapName
#define _PATH_MAPDIR					thePathInfoParser.GetMapPath
#define _PATH_MAPFILE				thePathInfoParser.GetMapFilePath
#define _PATH_MAPVIEWDIR			thePathInfoParser.GetMapViewPath
#define _PATH_MAPVIEWFILE			thePathInfoParser.GetMapViewFile

#define _PATH_DATAINFO				thePathInfoParser.GetDataInfoPath
#define _PATH_SETTING				thePathInfoParser.GetSettingPath
#define _PATH_PLAYERHEADER			thePathInfoParser.GetPlayerHeader
#define _PATH_PLAYERACTION			thePathInfoParser.GetPlayerActionModel
#define _PATH_CHARMODEL				thePathInfoParser.GetAvatarModelFile
#define _PATH_TERRAINTEXTURE		thePathInfoParser.GetTerrainTexturePath
#define _PATH_GRASS					thePathInfoParser.GetGrassPath
#define _PATH_UIICON					thePathInfoParser.GetUIIconPath
#define _PATH_UIIITEM				thePathInfoParser.GetUIItemPath
#define _PATH_UIDOC					thePathInfoParser.GetUIDocPath
#define _PATH_LOG						thePathInfoParser.GetLogPath
#define _PATH_SOUND					thePathInfoParser.GetSoundPath
#define _PATH_EDITOR					thePathInfoParser.GetEditorPath
#define _PATH_TEXTURE				thePathInfoParser.GetTexturePath
#define _PATH_EFFECT					thePathInfoParser.GetEffectPath
#define _PATH_EFFECTMODEL			thePathInfoParser.GetEffectModelPath
#define _PATH_EFFECTSOUND			thePathInfoParser.GetEffectSoundPath
#define _PATH_DEPENDENCE(x)		("Dependence\\" x )
#define _PATH_MONSTERMODEL			thePathInfoParser.GetMonsterModelPath
#define _PATH_NPCMODEL				thePathInfoParser.GetNpcModelPath
#define _PATH_SCENEMODEL			thePathInfoParser.GetSceneModelPath
#define _PATH_ITEMMODEL				thePathInfoParser.GetItemModelPath
#define _PATH_PUBLISH				thePathInfoParser.GetPublishPath
#define _PATH_ROOT					thePathInfoParser.GetRootPath


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sTypeLabelInfo
{
	INT		m_Category;
	LPCSTR*	m_LabelList;
	INT		m_LabelNum;
};


#endif //__PATHINFOPARSERDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Player.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_PLAYER_H__
#define __PACKETSTRUCT_CLIENTGAMES_PLAYER_H__
#pragma once

#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"

#pragma pack(push,1)


/*////////////////////////////////////////////////////////////////////////
// BATTLE
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_PLAYER_ATTACK_SYN )
//{
	BYTE			byAttackType;			// 攻击流水动作 eATTACK_SEQUENCE
	SLOTCODE		StyleCode;				// 姿态技能编号
	DWORD			dwClientSerial;		// 玩家攻击流水号
	VECTOR3D		vCurPos;					// 攻击时所在位置
	VECTOR3D		vDestPos;
	DWORD			dwTargetKey;			// 目标对象编号
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_PLAYER_ATTACK_BRD )
//{
	DWORD			dwAttackerKey;			// 
	BYTE			byAttackType;			// 
	SLOTCODE		StyleCode;				// 
	DWORD			dwClientSerial;		// 
	VECTOR3D		vCurPos;					// 
	VECTOR3D		vDestPos;				// 
	DWORD			dwTargetKey	;			// 
	DAMAGETYPE	wDamage;					// 
	DWORD			dwTargetHP;				// 
	BYTE			byEffect;				// 
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_PLAYER_ATTACK_NAK )
//{
	BYTE			m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_VKR_ATTACK_SYN )
//{
	DWORD			dwTargetKey;			// 
	DWORD			dwClientSerial;		// 
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_VKR_ATTACK_BRD )
//{
	DWORD			dwAttackerKey;			// 
	DWORD			dwClientSerial;		// 
	DWORD			dwTargetKey	;			// 
	DAMAGETYPE	wDamage;				// 
	DWORD			dwTargetHP;				// 
	BYTE			byEffect;				// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_VKR_RELOAD_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_VKR_RELOAD_BRD )
//{
	DWORD			m_dwObjKey;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_MONSTER_ATTACK_CMD )
//{
	DWORD			m_dwObjectKey;
	DWORD			m_dwTargetObjectKey;
	BYTE			m_AttackType;
	VECTOR3D		m_vCurPos;
	DAMAGETYPE	m_wDamage;
	WORD			m_wStandDelay;			/// 站立时间
	WORD			m_wTargetHP;
	BYTE			m_byEffect;				//
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_P2O_ATTACK_SYN )
//{
	DWORD			dwTargetKey;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_P2O_ATTACK_BRD )
//{
	DAMAGETYPE		m_wDamage;
	DWORD			m_dwTargetHP;			// HP
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_BATTLE_BEGIN( BATTLE_P2O_ATTACK_NAK )
//{
//}
MSGPACKET_CG_OBJECT_END;


/*////////////////////////////////////////////////////////////////////////
// CONVERSATION
/*////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
MSGOBJECT_CG_CONVERSATION_BEGIN( CONVERSATION_FIELDCHAT_SYN )
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = 255 };

	DWORD		m_dwObjectKey;
	BYTE		m_byMsgLength;
	char		m_pszChatMsg[ _MAX_CHATMSG_SIZE ];

	int		GetSize()
	{
		return SIZE_CG_MSG(CONVERSATION_FIELDCHAT_SYN) - (_MAX_CHATMSG_SIZE - m_byMsgLength);
	}
//};
MSGPACKET_CG_OBJECT_END;


////////////////////////////////////////////////////////////////////////
MSGSUPER_CG_CONVERSATION_BEGIN(CONVERSATION_FIELDCHAT_BRD,CONVERSATION_FIELDCHAT_SYN)
//{
//};
MSGPACKET_CG_SUPER_END;






/*////////////////////////////////////////////////////////////////////////
TRADE
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_REQ_SYN )
//{
	DWORD		m_dwTargetPlayerKey;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_REQ_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_REQ_CMD )
//{
	DWORD		m_dwPlayerKey;		
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_RES_SYN )
//{
	enum CONST_VAL{ ACCEPT, REFUSE };
	DWORD		m_dwPlayerKey;		
	BYTE		m_Type;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_RES_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_RES_CMD )
//{
	enum CONST_VAL{ ACCEPT, REFUSE };
	BYTE		m_Type;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_RES_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_SYN )
//{
	SLOTPOS		m_OrgPos;
	SLOTPOS		m_TradePos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_ACK )
//{	
	SLOTPOS		m_OrgPos;	
	SLOTPOS		m_TradePos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_CMD )
//{
	SLOTPOS			m_TradePos;	
	ITEMOPT_STREAM	m_ItemStream;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_MONEY_SYN )
//{
	MONEY			m_money;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_MONEY_ACK )
//{
	MONEY			m_money;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_MONEY_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PUT_MONEY_CMD )
//{
	MONEY		m_money;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_SYN )
//{
	SLOTPOS		m_TradePos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_ACK )
//{
	SLOTPOS		m_TradePos;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_CMD )
//{
	SLOTPOS		m_TradePos;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_SWAP_SYN )
//{
	SLOTPOS		m_FromPos;
	SLOTPOS		m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGSUPER_CG_TRADE_BEGIN( TRADE_SWAP_ACK, TRADE_SWAP_SYN)
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_SWAP_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGSUPER_CG_TRADE_BEGIN( TRADE_SWAP_CMD , TRADE_SWAP_SYN)
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_MONEY_SYN )
//{
	MONEY			m_money;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_MONEY_ACK )
//{
	MONEY			m_money;		
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_MONEY_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_GET_MONEY_CMD )
//{
	MONEY			m_money;		
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PROPOSAL_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PROPOSAL_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PROPOSAL_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_PROPOSAL_CMD )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_MODIFY_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_MODIFY_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_MODIFY_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_MODIFY_CMD )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_ACCEPT_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_ACCEPT_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_ACCEPT_NAK )
//{
	DWORD		m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_CANCEL_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_CANCEL_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_CANCEL_NAK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_CANCEL_CMD )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_ACCEPT_CMD )
//{
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_TRADE_BEGIN( TRADE_TRADE_BRD )
//{
	MONEY				m_Money;
	sTOTALINFO_TRADE	m_TradeInfo;	
	int GetSize()
	{
		return (SIZE_CG_MSG(TRADE_TRADE_BRD) - (sTOTALINFO_TRADE::MAX_SLOT_NUM - m_TradeInfo.m_InvenCount-m_TradeInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END;








/*////////////////////////////////////////////////////////////////////////
VENDOR
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_START_SYN )
//{
	TCHAR m_pszTitle[MAX_VENDOR_TITLE_LENGTH];

	sTOTALINFO_ESTABLISHER_VENDOR m_ItemInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(VENDOR_START_SYN)-(sTOTALINFO_ESTABLISHER_VENDOR::MAX_SLOT_NUM - m_ItemInfo.m_Count)*sizeof(sVENDOR_ITEM_SLOT));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_START_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_START_BRD )
//{
	DWORD m_dwPlayerKey;
	TCHAR m_pszTitle[MAX_VENDOR_TITLE_LENGTH+1];
	int GetSize()
	{
		return (int)(SIZE_CG_MSG(VENDOR_START_BRD)- ((MAX_VENDOR_TITLE_LENGTH+1)-(_tcslen(m_pszTitle)+1))*sizeof(TCHAR));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_START_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_RENAME_SYN )
//{
	TCHAR m_pszTitle[MAX_VENDOR_TITLE_LENGTH+1];
	int GetSize()
	{
		return (int)(SIZE_CG_MSG(VENDOR_RENAME_SYN)- ((MAX_VENDOR_TITLE_LENGTH+1)-(_tcslen(m_pszTitle)+1))*sizeof(TCHAR));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_RENAME_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_RENAME_BRD )
//{
	DWORD m_dwPlayerKey;
	TCHAR m_pszTitle[MAX_VENDOR_TITLE_LENGTH+1];
	int GetSize()
	{
		return (int)(SIZE_CG_MSG(VENDOR_RENAME_BRD)- ((MAX_VENDOR_TITLE_LENGTH+1)-(_tcslen(m_pszTitle)+1))*sizeof(TCHAR));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_RENAME_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_END_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_END_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_END_BRD )
//{
	DWORD m_dwPlayerKey;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_END_NAK)
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_REQ_SYN )
//{
	SLOTPOS m_VendorPos;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_REQ_ACK )
//{
	SLOTPOS m_VendorPos;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_REQ_BRD )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_REQ_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_CANCEL_SYN )
//{
	SLOTPOS m_VendorPos;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_CANCEL_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_CANCEL_BRD )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_CANCEL_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_SYN )
//{
	sVENDOR_ITEM_SLOT m_ItemSlot;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_BRD )
//{
	SLOTPOS m_VendorPos;
	sVENDOR_ITEM_SLOTEX m_ItemSlot;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_MODIFY_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_SWAP_SYN )
//{
	SLOTPOS	m_FromPos;
	SLOTPOS	m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGSUPER_CG_VENDOR_BEGIN( VENDOR_SWAP_ACK, VENDOR_SWAP_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGSUPER_CG_VENDOR_BEGIN( VENDOR_SWAP_BRD,  VENDOR_SWAP_SYN)
//{
	DWORD m_dwPlayerKey	;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_SWAP_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_INSERT_SYN )
//{
	sVENDOR_ITEM_SLOT m_ItemSlot;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_INSERT_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_INSERT_BRD )
//{
	SLOTPOS m_VendorPos;
	sVENDOR_ITEM_SLOTEX m_ItemSlot;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_INSERT_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_DELETE_SYN )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_DELETE_ACK )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_DELETE_BRD )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_DELETE_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_BUY_SYN )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_BUY_ACK )
//{
	MONEY m_Money;			
	sTOTALINFO_INVENTORY m_InventoryTotalInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(VENDOR_BUY_ACK)-(sTOTALINFO_INVENTORY::MAX_SLOT_NUM-m_InventoryTotalInfo.m_InvenCount-m_InventoryTotalInfo.m_TmpInvenCount)*sizeof(sITEM_SLOTEX));
	}
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_BUY_CMD )
//{
	DWORD		m_dwPlayerKey;
	SLOTPOS	m_VendorPos;		
	MONEY		m_Money;				
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_BUY_BRD )
//{
	SLOTPOS m_VendorPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_BUY_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_START_SYN )
//{
	DWORD m_dwPlayerKey;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_START_ACK )
//{
	TCHAR m_pszTitle[MAX_VENDOR_TITLE_LENGTH];
	sTOTALINFO_VENDOR m_ItemInfo;
	int GetSize()
	{
		return (SIZE_CG_MSG(VENDOR_VIEW_START_ACK)-(sTOTALINFO_VENDOR::MAX_SLOT_NUM - m_ItemInfo.m_Count)*sizeof(sVENDOR_ITEM_SLOTEX));
	}
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_START_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_START_BRD )
//{
	DWORD m_dwVendorKey;
	DWORD m_dwPlayerKey;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_END_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_END_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_VENDOR_BEGIN( VENDOR_VIEW_END_NAK )
//{
	DWORD m_ErrorCode;
//}
MSGPACKET_CG_OBJECT_END;










/*////////////////////////////////////////////////////////////////////////
WAREHOUSE
/*////////////////////////////////////////////////////////////////////////




//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_START_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_START_ACK )
//{
	MONEY						m_WarehouseMoney;
	sTOTALINFO_WAREHOUSE m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(WAREHOUSE_START_ACK) - (sTOTALINFO_WAREHOUSE::MAX_SLOT_NUM-m_ItemInfo.m_Count)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_START_CMD )
//{
	BYTE						m_PageIndex;
	sTOTALINFO_WAREHOUSE m_ItemInfo;
	int GetSize()
	{
		return ( SIZE_CG_MSG(WAREHOUSE_START_CMD) - (sTOTALINFO_WAREHOUSE::MAX_SLOT_NUM-m_ItemInfo.m_Count)*sizeof(sITEM_SLOTEX) );
	}
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_START_NAK )
//{
	DWORD m_dwErrorCode;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_MONEY_SYN )
//{
	enum CONST_VAL { PUTMONEY = 0, GETMONEY = 1, };
	BYTE				m_byType;
	MONEY				m_Money;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_MONEY_ACK )
//{
	MONEY				m_InventoryMoney;
	MONEY				m_WarehouseMoney;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_MONEY_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_END_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_END_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_WAREHOUSE_BEGIN( WAREHOUSE_END_NAK )
//{
	DWORD				m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;





/*////////////////////////////////////////////////////////////////////////
PARTY
/*////////////////////////////////////////////////////////////////////////

struct PARTY_MEMBER_STREAM
{
	bool			m_bMaster;								
	DWORD			m_dwObjKey;								
	char			m_szCharName[MAX_CHARNAME_LENGTH];		
	LEVELTYPE	m_wLevel;								
	DWORD			m_dwHP;									
	DWORD			m_dwHPMax;
	DWORD			m_dwMP;
	DWORD			m_dwMPMax;

	PARTY_MEMBER_STREAM()
	{
		m_dwObjKey = 0;
		memset( m_szCharName, 0, sizeof(char)*MAX_CHARNAME_LENGTH );
		m_wLevel		= 0;
		m_dwHP		= 0;
	}
} ;



//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_MEMBER_INFO_CMD)
//{
    BYTE						m_byDistributionType;	
	 DWORD					m_dwTargetKeyOfParty;	
    BYTE						m_byMemberNum;			
    PARTY_MEMBER_STREAM	m_MemberInfo[MAX_PARTYMEMBER_NUM];
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_MEMBER_LEVEL_UP_BRD)
//{
    DWORD			m_dwObjKey;				
    WORD				m_wLevel;				
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_MEMBER_HP_CHANGE_BRD)
//{
    DWORD				m_dwObjKey;				
    DWORD				m_dwHP;					
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_MEMBER_STATUS_CHANGE_BRD)
//{
    DWORD			m_dwObjKey;				
    BYTE				m_byStatus;				
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_MEMBER_JOIN_BRD)
//{
    PARTY_MEMBER_STREAM	m_NewMemberInfo;		
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_FORCED_EXPULSION_SYN)
//{
    DWORD				m_dwObjKey;				
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_FORCED_EXPULSION_NAK)
//{
    BYTE				m_byErrorCode;			
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_LEAVE_REQUEST_SYN)
//{
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_LEAVE_REQUEST_NAK)
//{
    BYTE				m_byErrorCode;						
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_LEAVE_START_BRD)
//{
    DWORD				m_dwObjKey;							
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_LEAVE_COMPLETE_BRD)
//{
	DWORD				m_dwObjKey;							
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_INVITE_REQUEST_SYN)
//{
    TCHAR	m_szCharName[MAX_CHARNAME_LENGTH];	
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_INVITE_REQUEST_SERVER_ACK)
//{
    
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_INVITE_REQUEST_SERVER_NAK)
//{
    BYTE				m_byErrorCode;						
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_INVITE_RESPONSE_SERVER_SYN)
//{
   char				m_szMasterName[MAX_CHARNAME_LENGTH];
	DWORD				m_dwMasterUserKey;					
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_INVITE_RESPONSE_ACK)
//{
	DWORD				m_dwMasterUserKey;					
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_INVITE_RESPONSE_NAK)
//{
	DWORD				m_dwMasterUserKey;					
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_CHANGE_MASTER_SYN)
//{
    DWORD				m_dwObjKey;							
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_CHANGE_MASTER_NAK)
//{
    BYTE				m_byErrorCode;						
    
    
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_CHANGE_MASTER_BRD)
//{
    DWORD				m_dwObjKey;							
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_DESTROY_PARTY_SYN)
//{
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_DESTROY_PARTY_NAK)
//{
    BYTE				m_byErrorCode;						
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_DESTROY_PARTY_BRD)
//{
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_CHANGE_ITEM_DISTRIBUTION_TYPE_SYN)
//{
    BYTE				m_byType;							
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_CHANGE_ITEM_DISTRIBUTION_TYPE_NAK)
//{
    BYTE				m_byErrorCode;						
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_CHANGE_ITEM_DISTRIBUTION_TYPE_BRD)
//{
    BYTE				m_byType;							
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_ITEM_DISTRIBUTION_HAPPEN_BRD)
//{
    DWORD				m_dwItemCode;						
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_ITEM_DISTRIBUTION_START_ROULETTE_SYN)
//{
    
    
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_ITEM_DISTRIBUTION_RESULT_ACK)
//{
    SLOTPOS				m_ItemPos;							
    ITEM_STREAM			m_ItemStream;						
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_ITEM_DISTRIBUTION_RESULT_BRD)
//{
    DWORD				m_dwItemCode;						
    char				m_szCharName[MAX_CHARNAME_LENGTH];	
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_REQUEST_JOIN_RELAYROOM_BRD)
//{
    DWORD				m_dwRoomKey;						
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN(PARTY_REQUEST_JOIN_RELAYROOM_ACK)
//{
    DWORD				m_dwRoomKey;						
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN( PARTY_SELECT_TARGET_SYN )
//{
	DWORD				m_dwObjectKey;						
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN( PARTY_SELECT_TARGET_BRD )
//{
	DWORD				m_dwObjectKey;						
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_PARTY_BEGIN( PARTY_SELECT_TARGET_NAK )
//{
	BYTE				m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;





#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_PLAYER_H__
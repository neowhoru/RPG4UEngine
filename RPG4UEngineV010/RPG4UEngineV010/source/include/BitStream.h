/*////////////////////////////////////////////////////////////////////////
文 件 名：BitStream.h
创建日期：2008年8月22日
最后更新：2008年8月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BITSTREAM_H__
#define __BITSTREAM_H__
#pragma once


#include "CommonDefine.h"
#include "BaseNode.h"

//------------------------------------------------------------------------------
class _BASE_API BitStream //: public BaseNode
{
public:
    BitStream();
    BitStream(int size);
    virtual ~BitStream();

public:
    void				Set(const BYTE* s, int size);
    const BYTE*	Get() const;

    void SetSize(int size);
    int GetSize() const;

    /// set the current bit position in the m_pStream
    void SetPos(int pos);
    /// return the current bit position in the m_pStream
    int GetPos() const;
    /// return the number of bits left in the m_pStream
    int BitsLeft() const;


public:
    /// write a BOOL to the m_pStream
    void WriteBool(BOOL flag);
    /// read a BOOL from the m_pStream
    BOOL ReadBool();

    /// write a compressed integer to the m_pStream
    void WriteInt(int value, int numBits);
    /// read a compressed integer from the m_pStream
    int ReadInt(int numBits);

    /// write a float to the m_pStream
    void WriteFloat(float value);
    /// read a float from the m_pStream
    float ReadFloat();

    /// write a custom byte array to the m_pStream
    void WriteCustom(const void* ptr, int size);
    /// read a custom byte array from the m_pStream
    void ReadCustom(void* ptr, int size);

    /// write bitstream
    void WriteBitStream(BitStream& m_pStream);
    /// read bitstream
    void ReadBitStream(BitStream& m_pStream);


    /// copy data from source m_pStream into this m_pStream
    void Copy(const BitStream& source);

public:
    /// Lock for writing.
    void BeginWrite();
    /// Unlock for writing.
    void EndWrite();

    /// Lock for reading.
    void BeginRead();
    /// Unlock for reading.
    void EndRead();


    /// Writeable?
    BOOL IsWriteable() const;
    /// Readable?
    BOOL IsReadable() const;

public:
    /// Assignment operator.
    BitStream& operator = (const BitStream& s);
    /// Equality operator.
    BOOL operator == (const BitStream& s) const;
    /// Inequality operator.
    BOOL operator != (const BitStream& s) const;

    // output
    void Print(int lines);

protected:
    /// clear m_pStream contents, do not rewind bit pointer
    void Clear();
    /// Write bit at current position to m_pStream.
    void WriteBit(BOOL bit);
    /// Read bit at currernt position from m_pStream.
    BOOL ReadBit();

private:
    BYTE*	m_pStream;
    int		m_StreamSize;
    int		m_CurrentByte;    /// Current index in the m_pStream buffer.
    int		m_CurrentBit;     /// Current bit offset int m_CurrentByte.
    BOOL		m_bWritable;
    BOOL		m_bReadable;
};

#include "BitStream.inl"




#endif //__BITSTREAM_H__
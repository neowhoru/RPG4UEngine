/*////////////////////////////////////////////////////////////////////////
文 件 名：	BaseLibExport.h
创建日期：	2007.12.27
最后更新：	2007.12.27
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#pragma once

// disable: "<type> needs to have dll-interface to be used by clients'
// Happens on STL member variables which are not public therefore is ok
#pragma warning (disable : 4251)

// disable: "non dll-interface class used as base for dll-interface class"
// Happens when deriving from Singleton because bug in compiler ignores
// template export
#pragma warning (disable : 4275)
#pragma warning (disable : 4819) //unicode 




#ifdef _BASELIB

	///////////////////////////////////
	//动态库输出
	#ifdef _USRDLL
		#define _BASELIB_API __declspec( dllexport )
		#pragma message("   _BASELIB_API __declspec( dllexport ) ")
	///////////////////////////////////
	//非动态输出
	#else
		#define _BASELIB_API
		#define _BASELIB_STATIC
		#pragma message("   _BASELIB_API STATIC")
	#endif

#else

	#define USE_BASELIB_STATIC	//使用静态库
	///////////////////////////////////
	//静态库
	#ifdef USE_BASELIB_STATIC
		#define _BASELIB_API
		#pragma message("   USING _BASELIB_STATIC")
		#ifdef _DEBUG
			#pragma comment(lib,"BaseLibStaticD.lib")
		#else
			#pragma comment(lib,"BaseLibStatic.lib")
		#endif

	///////////////////////////////////
	//动态库
	#else
		#define _BASELIB_API __declspec( dllimport )
		#pragma message("   _BASELIB_API __declspec( dllimport )")
		#ifdef _DEBUG
			#pragma comment(lib,"BaseLibD.lib")
		#else
			#pragma comment(lib,"BaseLib.lib")
		#endif
	#endif

#endif





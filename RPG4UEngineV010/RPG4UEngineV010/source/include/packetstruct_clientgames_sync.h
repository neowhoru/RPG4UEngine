/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Sync.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_SYNC_H__
#define __PACKETSTRUCT_CLIENTGAMES_SYNC_H__
#pragma once


#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"

#pragma pack(push,1)


/*////////////////////////////////////////////////////////////////////////
// _CG_SYNC
/*////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_PLAYER_ENTER_SYN )
//{
	DWORD						m_dwCheckSum;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_PLAYER_ENTER_ACK )
//{
	float						m_fPos[3];
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_PLAYER_ENTER_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
// sPLAYERINFO_RENDER,sGUILDINFO_RENDER  m_byCount
MSGOBJECT_CG_SYNC_BEGIN( SYNC_ALLPLAYERS_CMD )
//{
	enum CONST_VAL { _MAX_PLAYER_INFO_SIZE = 10 };
	BYTE						m_byCount;
	int GetSize() { return SIZE_CG_MSG(SYNC_ALLPLAYERS_CMD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
// sPLAYERINFO_VILLAGE,sGUILDINFO_RENDER  m_byCount 
MSGOBJECT_CG_SYNC_BEGIN( SYNC_VILLAGE_ALLPLAYERS_CMD )
//{
	enum CONST_VAL { _MAX_PLAYER_INFO_SIZE = 70 };
	BYTE						m_byCount;

	int GetSize() { return SIZE_CG_MSG(SYNC_VILLAGE_ALLPLAYERS_CMD); }
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
//  sEQUIP_ITEM_INFO_BASE  m_byCount
MSGOBJECT_CG_SYNC_BEGIN( SYNC_ALLPLAYERS_EQUIPINFO_CMD )
//{
	enum CONST_VAL { _MAX_PLAYER_INFO_SIZE = 10, };
	BYTE						m_byCount;
	int GetSize() { return SIZE_CG_MSG(SYNC_ALLPLAYERS_EQUIPINFO_CMD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
//  sMONSTERINFO_RENDER  m_byCount 
MSGOBJECT_CG_SYNC_BEGIN( SYNC_ALLMONSTERS_CMD )
//{
	enum CONST_VAL { _MAX_MONSTER_INFO_SIZE = 20, };
	BYTE						m_byCount;
	int GetSize() { return SIZE_CG_MSG(SYNC_ALLMONSTERS_CMD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_ALLFIELDITEMS_CMD )
//{
	enum CONST_VAL { _MAX_FIELDITEM_INFO_SIZE = 20, };
	BYTE						m_byCount;
	sITEMINFO_RENDER			m_pFieldItemRenderInfo[_MAX_FIELDITEM_INFO_SIZE];
	int							GetSize()
	{
		return (SIZE_CG_MSG(SYNC_ALLFIELDITEMS_CMD) - (_MAX_FIELDITEM_INFO_SIZE - m_byCount) * sizeof(sITEMINFO_RENDER) );
	}
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
//  sPLAYERINFO_RENDER, sGUILDINFO_RENDER, sEQUIP_ITEM_INFO_BASE
MSGOBJECT_CG_SYNC_BEGIN( SYNC_PLAYER_ENTER_BRD )
//{
	int GetSize() { return SIZE_CG_MSG(SYNC_PLAYER_ENTER_BRD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
//  sPLAYERINFO_VILLAGE, sGUILDINFO_RENDER, sEQUIP_ITEM_INFO_BASE
MSGOBJECT_CG_SYNC_BEGIN( SYNC_VILLAGE_PLAYER_ENTER_BRD )
//{
	int GetSize() { return SIZE_CG_MSG(SYNC_VILLAGE_PLAYER_ENTER_BRD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_PLAYER_LEAVE_BRD )
//{
	enum CONST_VAL { _MAX_PLAYER_SIZE = 50 };
	BYTE						m_byCount;
	DWORD						m_dwObjectKey[_MAX_PLAYER_SIZE];
	int							GetSize()
	{
		return SIZE_CG_MSG(SYNC_PLAYER_LEAVE_BRD) 
				- (_MAX_PLAYER_SIZE - m_byCount) * sizeof(DWORD) ;
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
// sMONSTERINFO_RENDER
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MONSTER_ENTER_BRD )
//{
	int GetSize() { return SIZE_CG_MSG(SYNC_MONSTER_ENTER_BRD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MONSTER_DIR_ENTER_BRD )
//{
	WORD wAngle;
	int GetSize() { return SIZE_CG_MSG(SYNC_MONSTER_DIR_ENTER_BRD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
//  Boss 
MSGOBJECT_CG_SYNC_BEGIN( SYNC_BOSS_MONSTER_ENTER_BRD )
//{
	DWORD						m_dwBossCode;	
	VECTOR3D					m_vCurPos;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MONSTER_LEAVE_BRD )
//{
	enum CONST_VAL { _MAX_MONSTER_SIZE = 50 };
	BYTE						m_byCount;
	DWORD						m_dwObjectKey[_MAX_MONSTER_SIZE];
	int							GetSize()
	{
		return (SIZE_CG_MSG(SYNC_MONSTER_LEAVE_BRD) - (_MAX_MONSTER_SIZE - m_byCount) * sizeof(DWORD) );
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_FIELDITEM_ENTER_BRD )
//{
	DWORD						m_dwFromMonsterKey;
	sITEMINFO_RENDER			m_ItemRenderInfo;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_FIELDITEM_LEAVE_BRD )
//{
	enum CONST_VAL { _MAX_FIELDITEM_SIZE = 50 };
	BYTE		m_byCount;
	DWORD		m_dwObjectKey[_MAX_FIELDITEM_SIZE];

	int		GetSize()
	{
		return (SIZE_CG_MSG(SYNC_FIELDITEM_LEAVE_BRD) - (_MAX_FIELDITEM_SIZE - m_byCount) * sizeof(DWORD) );
	}
//}
MSGPACKET_CG_OBJECT_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum CONST_VAL { _MAX_PATHROUTE_SIZE = MAX_PATH_ROUTE };

//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MOVE_SYN )
//{	
	VECTOR3D					vCurPos;
	VECTOR3D					vDestPos;
	BYTE						m_byNumRoute;							
	TILEINDEX				m_iRoute[ _MAX_PATHROUTE_SIZE ];	

	int						GetPacketSize()
	{
								return SIZE_CG_MSG(SYNC_MOVE_SYN) - sizeof(TILEINDEX) * (_MAX_PATHROUTE_SIZE - m_byNumRoute);
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MOVE_NAK )
//{
	VECTOR3D		m_vCurPos;
	BYTE			m_byErrorCode;					// eSYNC_RESULT
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MOVE_BRD )
//{
	DWORD			m_dwObjectKey;
	BYTE			m_byState;
	VECTOR3D		vCurPos;
	VECTOR3D		vDestPos;

	int	GetPacketSize() { return SIZE_CG_MSG(SYNC_MOVE_BRD); }
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_MOVE_THRUST_BRD )
//{
	DWORD			m_dwObjectKey;
	BYTE			m_byState;
	VECTOR3D		vCurPos;
	VECTOR3D		vDestPos;
	TILEINDEX	m_TileIndex;

	int			GetPacketSize() { return SIZE_CG_MSG(SYNC_MOVE_THRUST_BRD); }
//};
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_TARGET_MOVE_SYN )
//{
	DWORD			m_dwTargetKey;	
	VECTOR3D		vCurPos;
	VECTOR3D		vDestPos;
	BYTE			m_byNumRoute;
	TILEINDEX	m_iRoute[ _MAX_PATHROUTE_SIZE ];


	int GetPacketSize()
	{
		return SIZE_CG_MSG(SYNC_TARGET_MOVE_SYN) - sizeof(TILEINDEX) * (_MAX_PATHROUTE_SIZE - m_byNumRoute);
	}
//};
MSGPACKET_CG_OBJECT_END;




//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_TARGET_MOVE_BRD )
//{
	DWORD		m_dwTargetKey;
	WORD		m_wPlayerKey;
	VECTOR3D	vCurPos;
	VECTOR3D	vDestPos;

	int		GetPacketSize() { return SIZE_CG_MSG(SYNC_TARGET_MOVE_BRD); }
//};
MSGPACKET_CG_OBJECT_END;





//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_KBMOVE_SYN )
//{
	VECTOR3D		vCurPos;
	WORD			m_wAngle;
	TILEINDEX	m_TileIndex;
	BYTE			m_byMoveState;

	int GetSize() { return SIZE_CG_MSG(SYNC_KBMOVE_SYN); }
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_KBMOVE_BRD )
//{
	WORD			m_wPlayerKey;
	VECTOR3D		vCurPos;
	TILEINDEX	m_TileIndex;
	WORD        m_wAngle;
	BYTE			m_byMoveState;

	int GetSize() { return SIZE_CG_MSG(SYNC_KBMOVE_BRD); }
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_FAST_KBMOVE_SYN )
//{
	VECTOR3D		m_vCurPos;
	WORD			m_wAngle;
	TILEINDEX	m_TileIndex;
	BYTE			m_byCmsMoveState;		// eMOVE_TYPE
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_FAST_KBMOVE_BRD )
//{
	WORD			m_wPlayerKey;
	VECTOR3D		m_vCurPos;
	WORD        m_wAngle;
	TILEINDEX	m_TileIndex;
	BYTE			m_byCmsMoveState;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_NPC_JUMP_BRD )
//{
	DWORD			dwObjectKey;
	VECTOR3D		wvStartPos;
	VECTOR3D		wvEndPos;
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_STOP_SYN )
//{
	VECTOR3D			vCurPos;

	int GetSize() { return SIZE_CG_MSG(SYNC_STOP_SYN); }
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_STOP_BRD )
//{
	DWORD				dwObjectKey;
	VECTOR3D			vCurPos;
	int GetSize() { return SIZE_CG_MSG(SYNC_STOP_BRD); }
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_STOP_DIR_BRD )
//{
	DWORD				dwObjectKey;
	VECTOR3D			vCurPos;
	WORD				wAngle;
	int GetSize() { return SIZE_CG_MSG(SYNC_STOP_BRD); }
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_NPC_STOP_BRD )
//{
	DWORD						dwObjectKey;
	VECTOR3D					vCurPos;
	int GetSize() { return SIZE_CG_MSG(SYNC_NPC_STOP_BRD); }
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN( SYNC_ACTION_EXPIRED_CMD )
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN(SYNC_FORCED_WARP_BRD  )
//{
	DWORD			dwObjectKey;
	VECTOR3D		wvWarpPos;

	int GetSize() { return SIZE_CG_MSG(SYNC_FORCED_WARP_BRD); }
//};
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SYNC_BEGIN(SYNC_NPC_AIINFO_BRD  )
//{
	DWORD			dwObjectKey;
	BYTE			byAIInfoType;
	BYTE			byIndex;
	BYTE			byIndex2;

	int GetSize() { return SIZE_CG_MSG(SYNC_NPC_AIINFO_BRD); }
//};
MSGPACKET_CG_OBJECT_END;


#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_SYNC_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ChatDefine.h
创建日期：2008年7月8日
最后更新：2008年7月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHATDEFINE_H__
#define __CHATDEFINE_H__
#pragma once



namespace gameui
{

	enum eCHAT_TYPE
	{
		 CHAT_TYPE_NORMAL = 0		//普通聊天
		,CHAT_TYPE_GUILD				//公会聊天
		,CHAT_TYPE_TERM				//小队聊天
		,CHAT_TYPE_PRIVATE			//私聊
		,CHANNEL_SYSTEM				//系统公告
		,CHAT_TYPE_BULL				//全服公告--目前只用于gm
		,CHAT_TYPE_SHOUT				//喊话 区域服务器九宫格
		,CHAT_TYPE_HIGHSHOUT			//高喊 区域同全服-- 其实用于玩家
      ,CHAT_TYPE_GM              //GM 玩家向GM发送求助信息
		,AREA_BROADCAST
		,CHAT_TYPE_GM_WHISPER
		,CHAT_TYPE_HELP
		,CHAT_TYPE_SYSTEM
		,CHAT_TYPE_WHISPER	= CHAT_TYPE_PRIVATE
		,PRIVATE_CHAT			= CHAT_TYPE_PRIVATE
	};

enum eCHAT_FILTER_TYPE
{
	//CHAT_FILTER_TYPE_GENERAL = 0x01,
	//CHAT_FILTER_TYPE_PARTY	 = 0x02,
	//CHAT_FILTER_TYPE_GUILD	 = 0x04,
	//CHAT_FILTER_TYPE_TRADE	 = 0x08,
	//CHAT_FILTER_TYPE_SYSTEM	 = 0x10,

	CHAT_FILTER_TYPE_GUILD	 = 0x01,
	CHAT_FILTER_TYPE_GENERAL = 0x02,
	CHAT_FILTER_TYPE_TRADE	 = 0x08,
	CHAT_FILTER_TYPE_SYSTEM	 = 0x10,

	CHAT_FILTER_TYPE_MAX	 = 4,
};


struct ChatMsg
{
	
	eCHAT_TYPE	m_eType;
	TCHAR 		m_pszMessage[MAX_CHATBUFFER_LENGTH];
	TCHAR *		m_szCharName;
	TCHAR			m_pszCharNameRef[MAX_CHARNAME_LENGTH];
	TCHAR			m_pszRecvName[MAX_CHARNAME_LENGTH];		
	DWORD			m_dwSenderKey;
	DWORD			m_dwDelayAppearTick;
	DWORD			m_dwAccTick;

	ChatMsg()
	{
		m_pszMessage[0]		= 0;		
		m_szCharName			= NULL;
		m_pszCharNameRef[0]	= 0;
		m_pszRecvName[0]		= 0;
		m_dwSenderKey			= 0;
		m_dwDelayAppearTick	= 0;
		m_dwAccTick				= 0;
	}

	~ChatMsg()
	{
	}
};


};


#endif //__CHATDEFINE_H__
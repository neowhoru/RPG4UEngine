/*////////////////////////////////////////////////////////////////////////
文 件 名：TVector2D.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TVECTOR2D_H__
#define __TVECTOR2D_H__
#pragma once

#include "MathStructDecl.h"
#include "MathFunction.h"
#include "MathDefine.h"

namespace math
{ 
template<typename Type>
class TVector2D : public TPoint2D<Type>
{
public:
    TVector2D() {};
    TVector2D( CONST Type * );
    TVector2D( Type x, Type y );
    TVector2D( CONST TVector2D& v );
    explicit TVector2D( Type scale);			//需要显式调用

    // casting
    operator Type* ();
    operator CONST Type* () const;

    TVector2D&	Set	( CONST Type * );
    TVector2D&	Set	( Type scale );
    TVector2D&	Set	( Type x, Type y );
    TVector2D&	Set	( CONST TVector2D& v );


    TVector2D& operator = ( const TVector2D& rkVector );
	 TVector2D& operator = ( const Type fScalar);

	//Type  operator [] ( const size_t i ) const;
	//Type& operator [] ( const size_t i );

    // assignment operators
    TVector2D& operator += ( CONST TVector2D& );
    TVector2D& operator -= ( CONST TVector2D& );
    TVector2D& operator *= ( CONST TVector2D& );
    TVector2D& operator /= ( CONST TVector2D& );
    TVector2D& operator += ( Type );
    TVector2D& operator -= ( Type );
    TVector2D& operator *= ( Type );
    TVector2D& operator /= ( Type );

    // unary operators
    TVector2D operator + () const;
    TVector2D operator - () const;

    // binary operators
    TVector2D operator + ( CONST TVector2D& ) const;
    TVector2D operator - ( CONST TVector2D& ) const;
    TVector2D operator * ( CONST TVector2D& ) const;
    TVector2D operator / ( CONST TVector2D& ) const;
    TVector2D operator * ( Type ) const;
    TVector2D operator / ( Type ) const;

    BOOL operator == ( CONST TVector2D& ) const;
    BOOL operator != ( CONST TVector2D& ) const;
    BOOL operator <  ( CONST TVector2D& ) const;
    BOOL operator >  ( CONST TVector2D& ) const;


public:
	//工具类

   Type Length () const;			///> 调用了sqrt，CPU消耗非常大，调用之前，最好
											///> 事先通过LengthSquared检测下是否相等

	Type LengthSquared () const;	///> 获取长度的平方，本函数，因为运算简单，效率相对高出许多

   Type Distance(const TVector2D& vec) const;
   Type DistanceSquared(const TVector2D& vec) const;

	Type DotProduct(const TVector2D& vec) const;	///> 返回其它矢量计算点积
																///>  点积可用来计算两矢量间的夹角，如果
																///>  a.都是单元矢量，则点积则是夹角的cos值
																///>  b.否则，cos值 = 点积值/ (v.len*v2.len)
																///>  c.另，还可计算点(Point)到面(Plane)的距离

   Type AbsDotProduct(const TVector2D& vec) const;	///> 计算绝对值点积，原理上与点积类似，只是每轴值都使用了abs值

   Type CrossProduct(const TVector2D& vec ) const;	///> 与另一矢量计算叉积
																	///> 结果相当矢量面积的平方

   Type Normalize();					///> 单位化矢量，即 Lenght/Magnitude = 1，即LenghSequard=1
											///> 结果本身也是单元矢量
											///> 返回单位化之前的长度
   TVector2D NormalizeNew(void) const;	//依据本矢量，生成新的单位矢量

   TVector2D& GetNormal(const TVector2D& v1, const TVector2D& v2) const;	

   TVector2D MidPoint( const TVector2D& vec ) const;

   void MakeFloor	( const TVector2D& cmp );
   void MakeCeil	( const TVector2D& cmp );

   TVector2D PerpendicularNew(void) const;		///> 获取本矢量的正交矢量
															///> 可参考 Quaternion
   TVector2D ReflectNew(const TVector2D& normal) const;	///> 依据本矢量，参考normal生成新的镜射矢量

   TVector2D RandomNew(Type angle) const;	///> 从给出的角度（弧度），生成随机的矢量

   BOOL IsZeroLength(void) const;

public:
	BOOL InsideRect	(TVector2D rect[4] );



public:
	//其它全局函数...
	inline friend TVector2D operator / ( const Type fScalar, const TVector2D& rkVector )
	{
		return TVector2D(	 fScalar / rkVector.x,		 fScalar / rkVector.y);
	}
	inline friend TVector2D operator * ( Type f, CONST TVector2D& v )
	{	
		return TVector2D(f * v.x, f * v.y);
	}
	inline friend TVector2D operator + (const TVector2D& v, const Type f)
	{
		return TVector2D(		v.x + f,		v.y + f);
	}
	inline friend TVector2D operator + (const Type f , const TVector2D& v)
	{
		return TVector2D(		f + v.x ,		f + v.y);
	}
	inline friend TVector2D operator - (const TVector2D& v, const Type f)
	{
		return TVector2D(		v.x - f,		v.y - f);
	}
	inline friend TVector2D operator - (const Type f , const TVector2D& v)
	{
		return TVector2D(		f - v.x ,		f - v.y);
	}


public:
     // 特殊点
     static const TVector2D ZERO;
     static const TVector2D UNIT_X;
     static const TVector2D UNIT_Y;
     static const TVector2D UNIT_X_NEG;
     static const TVector2D UNIT_Y_NEG;
     static const TVector2D UNIT_SCALE;
};



};//namespace math


#include "TVector2D.inl"


#endif //__VECTOR2D_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include <THashTable.h>
#include "PlayerCommonDefine.h"


typedef union {
	struct {
		BYTE byClass;
		BYTE byPart;
		WORD wPartID;
	};

	DWORD dwID;

} HERO_VARIATION_PART;

const int MAX_VARIATION_RESOURCE = 3;

struct BASE_HeroVariationInfo {

	HERO_VARIATION_PART partID;
	DWORD    m_dResourceID[MAX_VARIATION_RESOURCE];
	

};


class _RESOURCELIB_API HeroVariationInfoParser	
{
public:
	HeroVariationInfoParser();
	~HeroVariationInfoParser();


	BOOL           Init(DWORD dwPoolSize);
	VOID           Release();
	BOOL           Load(LPCSTR szFileName);

	BASE_HeroVariationInfo*            GetHeroVariationInfo(DWORD dwKey);
	int            GetVariationCount(BYTE byClass,int iVariation);

	
protected:
	int m_iVariationCount[PLAYERTYPE_MAX][object::PLAYER_VARIATION_MAX + 1];
	

private:
	VOID		   Unload();
	util::THashTable<BASE_HeroVariationInfo *>		*m_pDataInfos;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(HeroVariationInfoParser , _RESOURCELIB_API );
//extern _RESOURCELIB_API  HeroVariationInfoParser& singleton::GetHeroVariationInfoParser();
#define theHeroVariationInfoParser  singleton::GetHeroVariationInfoParser()

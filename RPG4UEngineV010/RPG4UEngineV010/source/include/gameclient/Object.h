/*////////////////////////////////////////////////////////////////////////
文 件 名：Object.h
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
liease@163.com
qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __OBJECT_H__
#define __OBJECT_H__
#pragma once



#include "CommonDefine.h"
#include "ObjectDefine.h"
#include "ObjectCookie.h"
#include "ConstMesh.h"
#include "StateBaseDefine.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct	GAMEEVENT;
enum		eWEAPONSOUNDKIND;
struct	sATTACK_INFO;

_NAMESPACEU	(StateBase,		state);
_NAMESPACEU	(VObject,		vobject);
_NAMESPACEU	(PathHandler,	tile);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace object
{
struct APPEARANCEINFO;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API Object : public ObjectCookie
{
public:
	Object( void );
	virtual ~Object( void );

public:



public:
	virtual DWORD		GetFactoryType();
	virtual BOOL		Create			( DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void		Destroy			( void );
	virtual void		Reuse				();

	virtual void		RefreshRender	(APPEARANCEINFO* pAppearInfo,BOOL bRefreshBone = FALSE);
	virtual void		UpdateToVObject();
	virtual BOOL		Process			(DWORD dwTick);

	virtual void		SetName		(LPCSTR pszName);
	virtual TCHAR *	GetName		() const;


	virtual float     GetAlphaOrgin();
	virtual void		SetAlpha		(float fAlpha,BOOL bNoFadeIn=FALSE);
	virtual void		SetVisible	(BOOL bSet,BOOL bNoFadeIn=FALSE);
	virtual BOOL		IsVisible	(BOOL bNoFadeIn=FALSE);

	virtual Vector3C&	GetPosition	( void );
	virtual VOID		SetPosition ( Vector3C& vPos);
	virtual VOID		SetPosition	( const VECTOR3D&		vPos
                                 , int						iTileIndex// = INVALID_TILEINDEX
											, BOOL					bResetBackOff=TRUE)		;
	virtual VOID		SetPosition	( const sTILE_POS&	vPos, BOOL bResetBackOff=TRUE);
	virtual void		JumpTo		( const VECTOR3D&		vPos );


	virtual void		SetDirection(const VECTOR3D& vTarget
                                 ,VECTOR3D*       pwvPos=NULL
											,BOOL            bVisibleAngle=FALSE); 
	virtual Vector3D	GetDirection( void );

	virtual void		SetColor		(int r,int g,int b,int a);
	virtual void      SetScale		(float fScale);
	virtual float     GetAngle		();
	virtual void		SetAngle		(float fAngle,BOOL bSetVisibleAngle = FALSE);
	virtual void		Rotate		(float fAngle,BOOL bSetVisibleAngle = FALSE);
	virtual void		SetVisibleAngleModifer(float fAngle);


	virtual void		PlayAnimation			(LPCSTR	szName
                                    ,int		nTimes			=MAX_ANIMATION_LOOP
												,LPCSTR	szEndName		=NULL);
	virtual void		PlayAnimation			(DWORD	dwActionID
                                    ,int		nTimes			=MAX_ANIMATION_LOOP
												,DWORD	dwEndActionID	=-1);

	virtual BOOL		OnTriggerPoint		(BOOL bLastTriggerPoint, int nTriggerPointType,sATTACK_INFO* pAttackInfo );
	virtual BOOL		OnAnimationEnd	();

	virtual void		SetAnimation	( LPCSTR	pAnimation, BOOL bLoop = FALSE);
	virtual void		SetAnimation	( IDTYPE idAnimation, BOOL bLoop = FALSE);
	virtual void		SetCurrentBlendAnimation( char *pAnimation, int boneindex ,float fSpeed = 1.0f,int iBlendFrame = -1);
	virtual void		SetCurrentBlendAnimation( IDTYPE idAnimation, int boneindex ,float fSpeed = 1.0f,int iBlendFrame = -1);
	virtual void		StopBlendAnimation		();

	virtual float		ProcessAnimation		( DWORD dwTick ,GAMEEVENT *pEvent = NULL);	
	virtual void		SetLookAtAnimation	(BOOL bUse,Vector3D *pPosition);
	IDTYPE				GetCurrentAnimation	();


	///////////////////////////////////

	virtual void        OnFootStep();



public:
	virtual void		  TellClient				(LPCSTR szText, ... );
	virtual void		  ShowScreenText			(BOOL        bTitle
                                             ,DWORD       time
															,DWORD       color
															,LPCSTR		 szText
															,...);

	virtual void        DisplayChatMessage		(LPCSTR		szChat);
	virtual void        DisplayChatMessage		(CODETYPE	textID);

	virtual  float      GetMeshHeight(float fExtra=0.f);


public:
	virtual void		ProcessRemove		(DWORD dwTick);
	virtual void		SetVObject			(VObject* pVObject);

public:
	virtual BOOL		InitializeStates	();
	virtual void		ReleaseStates		();
	virtual void		OnInitializeState	();

	virtual void		InitState		(STATE_TYPE initState,DWORD CurTime);
	virtual void		StateProcess	(DWORD dwTick,DWORD CurTime);

	BOOL					IsCurrentState			(STATE_TYPE state);
	STATE_TYPE			GetPreState				();
	STATE_TYPE			GetNextState			();
	StateBase*			GetCurStateInstance	();
	DWORD					GetCurStateStartTime	();
	void					ForceStateChange		(DWORD CurTime);
	BOOL					SetNextState			(STATE_TYPE nextState, DWORD CurTime);
	BOOL					SetNextSkillState		(StateBase *pSkillState);

protected:
	STATECHANGE_TYPE	CommitChangeState(DWORD CurTime);

public:
	BOOL		IsKindOfObject	( eOBJECT_TYPE eObjectType );



private:
	///ObjectCookie，不推荐使用
	void			GetPos(Vector3D *pPos) { *pPos = GetPosition();};
	void			SetPos(Vector3D *pPos) { SetPosition(*pPos);};

protected:
	/////////////////////////////////
	//States
	StateBase*				m_StateArray[STATE_MAX];
	StateBase*				m_pNextSkillState;

	VG_TYPE_GET_PROPERTY	(CurrentState,	STATE_TYPE);
	STATE_TYPE				m_eNextState;
	STATE_TYPE				m_ePreState;

	DWORD						m_CurStateStartTime;	

protected:
	IDTYPE						m_CurrentAnimID;
	VRSTR							m_strObjName;
	//ObjectCookie&				m_ObjectCookie;
	//VG_TYPE_PROPERTY			(ObjectCookie, ObjectCookie);

	VG_TYPE_PROPERTY			(ChatColor, COLOR);
	VG_PTR_GET_PROPERTY		(VObject,	VObject);		//VOBject指针
	VG_INT_PROPERTY			(RemoveLeftTime);

	VG_VECTOR_PROPERTY		(VisiblePos, Vector3D);

	VG_PTR_GET_PROPERTY		(PathExplorer,PathHandler);
	OCT_INDEX					m_woiOctIndex;

	VG_FLOAT_GET_PROPERTY   (AngleZ);      
	float                   m_fVisibleAngleModifier;
	float                   m_fVisibleAngleZ;     

	VG_FLOAT_PROPERTY			(Height); 
	VG_BOOL_PROPERTY			(HideAttachment); 
	VG_TYPE_PROPERTY			(Color, COLOR); 
	VG_BOOL_PROPERTY			(Show); 
	VG_BOOL_PROPERTY			(ShowName); 
	VG_BOOL_PROPERTY			(ForceRemove); 
};

#define	m_ObjectCookie		(*this)

};//object

#include "Object.inl"

#endif //__OBJECT_H__
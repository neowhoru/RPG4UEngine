/*////////////////////////////////////////////////////////////////////////
文 件 名：VCharacterMoveQueue.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	玩家移动信息队列
	移动类型有：
		持续移动
		保持移动状态移动到目标点
		移动到目标点停下来
		移动到锁定目标

		玩家移动到某点
		玩家移动方式的变化
		玩家方向和移动方式的变化

		玩家持续移动通过某点
		玩家以某朝向持续移动通过某点
		玩家原地不动的所在点
		玩家原地不动的面朝方向
		玩家原地不动的所在点和面朝方向


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTERMOVEQUEUE_H__
#define __VCHARACTERMOVEQUEUE_H__
#pragma once

#include "VCharacterMoveQueueDefine.h"


class WalkPath;
using namespace std;

namespace vobject
{ 

class VCharacter;
class VCharacterAnimationCtrl;

class _CLIENTGAMELIB_API VCharacterMoveQueue
{
public:
	VCharacterMoveQueue(void);
	~VCharacterMoveQueue(void);

public:
	int Size();
	void Clear();

	BOOL PopNextMove( sMOVEQUEUE_INFO &move );
	BOOL Push_KeepMovingToPos( float x, float y, float dx, float dy );
	BOOL Push_ContinueMoving( float x, float y, float z, float DirX, float DirY, float fTurnSpeed, short shMovingType );
	BOOL Push_MovingToPos( float x, float y, float dx, float dy );
	BOOL Push_MovingToTarget( int nTargetID, float fKeepDistance );

	BOOL Push_PlayerMovingToPos( const VECTOR3D& vPos, const VECTOR3D& vDir );
	BOOL Push_PlayerMovingType( short shMovingType);
	BOOL Push_PlayerMovingDirAndType( const VECTOR3D& vDir, short shMovingType );

	// 玩家保持移动状态通过某点
	BOOL PushPlayerKeepMovingPassPos( const VECTOR3D& vPos );
	// 玩家以某朝向保持移动状态通过某点
	// WOW只所以掉线了会看见别的玩家在原地打转，并不是因为它在移动中互发了什么：正在朝什么方向转的消息。
	// 而是因为它移动通过了一个点，而通过这个点的路径是用插值算出来的一条弧线，当它收不到下个要通过的点后，
	// 仍然以这个路径线移动，就出现了原地打转的情况
	BOOL PushPlayerKeepMovingPassPos( const VECTOR3D& vPos, const VECTOR3D& vDir );

	// 玩家原地不动的所在点
	BOOL PushPlayerPos( const VECTOR3D& vPos );
	// 玩家原地不动的面朝方向
	BOOL PushPlayerDir( const VECTOR3D& vDir );
	// 玩家原地不动的所在点和面朝方向
	BOOL PushPlayerPosAndDir( const VECTOR3D& vPos, const VECTOR3D& vDir );

private:
	BOOL			Push_Move( sMOVEQUEUE_INFO &move );

protected:
	deque<sMOVEQUEUE_INFO>	m_veMove;
};

};//namespace vobject

#endif //__VCHARACTERMOVEQUEUE_H__


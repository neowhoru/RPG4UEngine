/*////////////////////////////////////////////////////////////////////////
文 件 名：DemoSystemDefault.h
创建日期：2008年7月5日
最后更新：2008年7月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __DEMOSYSTEMDEFAULT_H__
#define __DEMOSYSTEMDEFAULT_H__
#pragma once


#include "IDemoSystem.h"

class _CLIENTGAMELIB_API	DemoSystemDefault : public IDemoSystem
{
public:
	virtual DWORD GetSystemType()	{return TAG('NULL');};
	virtual BOOL OnRegSystem()		{return TRUE;};
	virtual BOOL OnUnregSystem()	{return TRUE;};
	virtual void OnActiveSysem(BOOL bActivated) {__UNUSED(bActivated);};

public:
	virtual BOOL Init						()	;
	virtual void Release					()	;
	virtual BOOL RequestWorkingData	()	;
	virtual void ReleaseWorkingData	()	;
	virtual BOOL FrameMove				(DWORD dwTick);
	virtual BOOL Render					(DWORD dwTick)	;

public:
	virtual BOOL CreateAllPlayers	();

	virtual BOOL LoadRegenInfo(LPCSTR szFile);
	virtual BOOL ProcessRegen	();
	virtual BOOL ProcessSkillEffects	();
	virtual BOOL ProcessStatus	();
	virtual void OnAddExp	(Character* pChar, DWORD dwExp);
	virtual void OnLevelUp	(Character* pChar);

	//virtual BOOL MonsterDoAttack	(SLocationInfo& info,Character* pTarget);
	//virtual BOOL ProcessMonster	(SLocationInfo& info);
	//virtual BOOL CreatePlayer		(SRegenInfo& info);
	//virtual BOOL CreateItem			(SLocationInfo& info);
	//virtual BOOL CreateMonster		(SLocationInfo& info,DWORD dwMonsterID);

public:
	virtual BOOL ParsePacket			(MSG_BASE*			 pMsgBase
                         				,DWORD				 dwSize
												,Character*			 pChar);

	virtual void ProcessStatusPacket		(MSG_OBJECT_BASE* pMsgBase
													,Character*        pChar);

	virtual BOOL ProcessWareHousePacket	(MSG_OBJECT_BASE* pMsgBase
													,Character*        pChar);

	virtual BOOL ProcessSkillPacket	(MSG_OBJECT_BASE* pMsgBase
												,Character*        pChar);
	virtual BOOL ProcessItemPacket	(MSG_OBJECT_BASE* pMsgBase
												,Character*        pChar);
	virtual BOOL ProcessStylePacket	(MSG_OBJECT_BASE* pMsgBase
												,Character*        pChar);
	virtual BOOL ProcessBattlePacket(MSG_OBJECT_BASE* pMsgBase
												,Character*        pChar);
	virtual BOOL ProcessSyncPacket	(MSG_OBJECT_BASE* pMsgBase
												,Character*        pChar);
protected:
	virtual BOOL OnRecover(Character* pChar,int recoverHP, int recoverMP);

};

#include "DemoSystemDefault.inl"

#endif //__DEMOSYSTEMDEFAULT_H__
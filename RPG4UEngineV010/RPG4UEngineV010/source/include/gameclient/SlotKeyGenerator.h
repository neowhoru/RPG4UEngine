#pragma once

#include "GlobalInstanceSingletonDefine.h"

namespace util
{
class KeyGenerator;
};

class _CLIENTGAMELIB_API SlotKeyGenerator
{
public:
	SlotKeyGenerator(void);
	virtual ~SlotKeyGenerator(void);

public:
	BOOL Init();
	void Release();

	DWORD GetKey();
	void Restore(DWORD Key);

private:
	util::KeyGenerator *	m_pItemKeyGenerator;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(SlotKeyGenerator , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API SlotKeyGenerator& singleton::GetSlotKeyGenerator();
#define theSlotKeyGenerator	singleton::GetSlotKeyGenerator()


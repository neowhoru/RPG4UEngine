/*////////////////////////////////////////////////////////////////////////
文 件 名：VCharacterAnimation.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	玩家模型控制类


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTERANIMATIONCTRL_H__
#define __VCHARACTERANIMATIONCTRL_H__
#pragma once


#include "GlobalTexture.h"
#include "MeshAnimationCtrl.h"
#include "VCharacterActionQueue.h"

class AnimationTimeLine;
class CPlayerAnimCtrlMgr;
struct VCharacterEquipment;


namespace vobject
{ 

class VObject;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API VCharacterAnimationCtrl : public MeshAnimationCtrl
{
public:
	VCharacterAnimationCtrl();
	~VCharacterAnimationCtrl();

public:
	BOOL	LoadFromFile( const char* szFileName );
	void	Destroy		();

	BOOL	ChangeArmourModel	( int nModelId, int nProfession, int nSex, int nType );
	BOOL	ChangeEquip			( int nModelId, int nProfession, int nSex, int nType, int nColor = -1 );
	//void	Update				( DWORD dwTime, const Matrix4& pMatrix );
	void	Render				( float fTransparent = 1.0f );
	void	OnTriggerPoint			( sTIMELINE_KEYINFO* pKey, DWORD dwFlag = 0 );

	DWORD	ConvertAnimType	( DWORD dwOldType );


	// 在指定的时间内，播放完动画
	void	PlayAnim2			(DWORD dwAnimType
                           ,DWORD dwAnimPlayMode
									,DWORD dwTimePerLoop
									,DWORD dwLoopCount
									,DWORD dwTimeOut
									,DWORD dwTargetAnimType
									,DWORD dwTargetAnimPlayMode);

	void	PlayAnim3			(DWORD dwAnimType
                           ,DWORD dwAnimPlayMode
									,DWORD dwTimePerLoop
									,DWORD dwLoopCount
									,DWORD dwTimeOut
									,DWORD dwTargetAnimType
									,DWORD dwTargetAnimPlayMode);

	void	PlayAnimByActionName	(const char* AnimName
                              ,DWORD       AnimPlayMode
										,DWORD       OneLoopTime
										,DWORD       LoopTimes
										,DWORD       TimeOut
										,const char* TargetAnimName
										,DWORD       TargetAnimPlayMode);

	//// 播放上半身的动作
	//void	PlayUpperAnim( const char* szAnimName, DWORD dwAnimPlayMode,
	//	DWORD dwTimePerLoop, DWORD dwLoopCount, DWORD dwTimeOut, DWORD dwBlendingTime,
	//	char* szTargetAnimName, DWORD dwTargetAnimPlayMode,
	//	DWORD dwTargetOneLoopTime = 0, DWORD dwTargetLoopTimes = MAX_ANIMATION_LOOP, DWORD dwTargetTimeOut = ANIMATION_TIMEOUT, DWORD dwTargetBlendingTime = ANIMATION_BLENDING_TIME );

	//void	PlayUpperAnim( DWORD dwAnimType, DWORD dwAnimPlayMode,
	//	DWORD dwTimePerLoop, DWORD dwLoopCount, DWORD dwTimeOut, DWORD dwBlendingTime,
	//	DWORD dwTargetAnimType, DWORD dwTargetAnimPlayMode,
	//	DWORD dwTargetOneLoopTime = 0, DWORD dwTargetLoopTimes = MAX_ANIMATION_LOOP, DWORD dwTargetTimeOut = ANIMATION_TIMEOUT, DWORD dwTargetBlendingTime = ANIMATION_BLENDING_TIME );

	// 播放上半身的动作
	void	PlayUpperAnim		(const char* szAnimName
                           ,DWORD       dwAnimPlayMode
									,DWORD       dwTimePerLoop
									,DWORD       dwLoopCount
									,DWORD       dwTimeOut
									,DWORD       dwBlendingTime);

	void	PlayUpperAnim		(DWORD dwAnimType
                           ,DWORD dwAnimPlayMode
									,DWORD dwTimePerLoop
									,DWORD dwLoopCount
									,DWORD dwTimeOut
									,DWORD dwBlendingTime);

	// 播放下半身的动作
	void	PlayLowerAnim		(const char* szAnimName
                           ,DWORD       dwAnimPlayMode
									,DWORD       dwTimePerLoop
									,DWORD       dwLoopCount
									,DWORD       dwTimeOut
									,DWORD       dwBlendingTime
									,const char* szTargetAnimName
									,DWORD       dwTargetAnimPlayMode
									,DWORD       dwTargetOneLoopTime=0
									,DWORD       dwTargetLoopTimes=MAX_ANIMATION_LOOP
									,DWORD       dwTargetTimeOut=ANIMATION_TIMEOUT
									,DWORD       dwTargetBlendingTime=ANIMATION_BLENDING_TIME);

	void	PlayLowerAnim		(DWORD dwAnimType
                           ,DWORD dwAnimPlayMode
									,DWORD dwTimePerLoop
									,DWORD dwLoopCount
									,DWORD dwTimeOut
									,DWORD dwBlendingTime
									,DWORD dwTargetAnimType
									,DWORD dwTargetAnimPlayMode
									,DWORD dwTargetOneLoopTime=0
									,DWORD dwTargetLoopTimes=MAX_ANIMATION_LOOP
									,DWORD dwTargetTimeOut=ANIMATION_TIMEOUT
									,DWORD dwTargetBlendingTime=ANIMATION_BLENDING_TIME);

	//插入一个动画并播放,结束时播放先前的动画
	void	PlayInsertAnim		(const char* AnimName
                           ,DWORD       AnimPlayMode=PLAYMODE_NORMAL
									,DWORD       OneLoopTime=0
									,DWORD       LoopTimes=1
									,DWORD       TimeOut=ANIMATION_TIMEOUT);

	int	GetActionModelId	(int nProfession
                           ,int nSex
									,int nWeaponType=-1);

	BOOL	CanReEnter			( DWORD dwOldAction, DWORD dwNewAction );

	DWORD	GetCurAnimType			()							{ return m_dwAnimType; }
	void	SetCurAnimType			( DWORD dwAnimType )	{ m_dwAnimType = dwAnimType; }
	DWORD	GetCurTargetAnimType	()							{ return m_dwTargetAnimType; }


	//创建身体效果，中毒，石化，冰冻和昏睡
	void	CreateSelfEffect		( int nEffectId );
	void	EndSelfEffect			();

	void	EquipAll				(BOOL  wizard_flag
                           ,BOOL  fight_flag
									,void* pEquips
									,int   iDefEquip
									,int   iDefHair
									,int   iDefFace
									,int   iSex
									,int   nCountry=0
									,int   nColor=-1);

	void	UpdateHandItem			( BOOL fight_state,	void* pEquips0 );
	void	UpdateRightHandItem	( BOOL fight_state );

	void	SetFightState			( BOOL fight_flag );

	//void	FightStateEnd();
	//void	SetHairColor( DWORD r, DWORD g, DWORD b )
	//{ 
	//	m_dwHairColor[0] = r;
	//	m_dwHairColor[1] = g;
	//	m_dwHairColor[2] = b;
	//}

	void	SetAttackInfo( sATTACK_INFO * pAttackInfo )
	{
		m_pAttackInfo = pAttackInfo;
	}

	void	ClearAttackInfo		( void );
	bool	HasAttackInfo			( void );


	BOOL	IsPlayerDeath			();
	void	SetPlayerDeath			( BOOL bDeath );
	void	SetSpecialEquipEffect( int iPart,VCharacterEquipment* pEquip );


protected:
	VG_DWORD_PROPERTY	(TaskSequence);

	DWORD			m_dwAnimType;
	DWORD			m_dwTargetAnimType;
	DWORD			m_dwAnimPlayMode;

	int			m_nSelfEffectId;
	//DWORD		m_dwHairColor[3];


	BOOL			m_bPlayerDeath;

	//攻击信息
	sATTACK_INFO* m_pAttackInfo;

	VG_PTR_PROPERTY(OwnerObject,  VObject);
	//设置某个部位的特殊装备效果
};

};//namespace vobject


#endif //__VCHARACTERANIMATIONCTRL_H__


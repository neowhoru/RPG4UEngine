/*////////////////////////////////////////////////////////////////////////
文 件 名：CharacterDefine.h
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTERDEFINE_H__
#define __CHARACTERDEFINE_H__
#pragma once


#include <queue>
#include <list>
#include "ConstArray.h"

enum eWEAPONSOUNDKIND;
enum eARMOUR_TEX_TYPE;

using namespace std;

namespace object
{ 
	class		Character;
	class		SkillEffect;
	typedef	SkillEffect	SKILL_EFFECT;

	enum
	{
		 KEEP_SCRIPT		= _BIT(0)
		,KEEP_QUEST			= _BIT(1)
		,KEEP_SCRIPT_ALL	= 0xffffffff
	};

	#define		RenderPartBone			0

	const int	MAX_PATH_SEEK_NODE			= 5000;	//寻路节点最大量

	const DWORD CHAR_UPDATE_SKILLEFFECT	= _BIT(0);
	const DWORD CHAR_REMOVE_SKILLEFFECT	= _BIT(1);
	const DWORD CHAR_UPDATE_ATTRIB		= _BIT(2);
	const DWORD CHAR_UPDATE_HPMPINFO		= _BIT(3);
	const DWORD CHAR_UPDATE_EXPINFO		= _BIT(4);
	const DWORD CHAR_UPDATE_PACKINFO		= _BIT(5);
	const DWORD CHAR_UPDATE_SKILLINFO	= _BIT(6);
	const DWORD CHAR_UPDATE_LEVELUP		= _BIT(7);
	const DWORD CHAR_UPDATE_EQUIPMENTS	= _BIT(8);
	const DWORD CHAR_UPDATE_DEATH			= _BIT(9);
	const DWORD CHAR_UPDATE_SPAWN			= _BIT(10);
	const DWORD CHAR_UPDATE_QUEST			= _BIT(11);
	const DWORD CHAR_UPDATE_STYLE			= _BIT(12);
	const DWORD CHAR_UPDATE_PARTY			= _BIT(13);
	const DWORD CHAR_UPDATE_VENDOR		= _BIT(14);
	const DWORD CHAR_UPDATE_TRADE			= _BIT(15);


	const int	MAX_ATTACK_RESULT_TARGET				= 16;
	const DWORD ATTACK_ADDITIONAL_EFFECT_STUN			= _BIT(0);
	const DWORD ATTACK_ADDITIONAL_EFFECT_KNOCKBACK	= _BIT(1);
	const DWORD ATTACK_ADDITIONAL_EFFECT_FREEZE		= _BIT(2);
	const DWORD ATTACK_ADDITIONAL_EFFECT_DOWN			= _BIT(3);
	const DWORD ATTACK_ADDITIONAL_EFFECT_AIR			= _BIT(4);
	const DWORD ATTACK_ADDITIONAL_EFFECT_TOGROUND	= _BIT(5);
	const DWORD ATTACK_ADDITIONAL_EFFECT_REBIRTH		= _BIT(6);
	const DWORD ATTACK_ADDITIONAL_EFFECT_CRITICAL	= _BIT(7);

	/// AttackResult处理标志
	const DWORD ATTACK_RESULT_NEW					= 0;	
	const DWORD ATTACK_RESULT_ACTIONED			= _BIT(0);	
	const DWORD ATTACK_RESULT_PROCESSED			= _BIT(1);	
	//const DWORD ATTACK_RESULT_FLUSHED			= _BIT(2);	
	const DWORD ATTACK_RESULT_END					= ATTACK_RESULT_ACTIONED
															| ATTACK_RESULT_PROCESSED;	
	const DWORD ATTACK_RESULT_TIMEOUT			= 120*1000;	

	const DWORD MAX_TARGERTER = 8;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
	struct ATTACK_RESULT_TARGET_INFO
	{
		DWORD			dwTargetID;
		BOOL			bHeal;
		BOOL			bMiss;
		BOOL        bNoDamage;
		BOOL        bForceSetLatestAttack;
		DAMAGETYPE	wDamage;	
		DWORD			dwTargetHP;
		DWORD			dwAdditionalEffect;
		DWORD			dwAirTime;                // 漂浮时间
		DWORD			dwGetupTime;              // 起身时间
		sTILE_POS		vCurPosition;
		sTILE_POS		vDestPosition;
		VECTOR3D		vOffset;
		float			fForce;
		BYTE			byOrder;                  //
	} ;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
	struct ATTACK_RESULT
	{
		DWORD								dwFlag;	///处理标志
		DWORD								dwTimeStart;
		DWORD                      dwGroupId;  
		DWORD								dwAttackSerial;
		DWORD								dwTimeSerial;
		eWEAPONSOUNDKIND           dwWeaponSoundKind;
		eARMOUR_TEX_TYPE       dwArmourTexture;
		DWORD                      dwSkillID;
		ATTACK_RESULT_TARGET_INFO  TargetInfo;

		void SetProcessed	(){_BIT_ADD(dwFlag, ATTACK_RESULT_PROCESSED);}
		void SetActioned	(){_BIT_ADD(dwFlag, ATTACK_RESULT_ACTIONED);}
		BOOL IsEnd			(){return _BIT_TEST(dwFlag, ATTACK_RESULT_END);}
		BOOL IsProcessed	(){return _BIT_TEST(dwFlag, ATTACK_RESULT_PROCESSED);}
		BOOL IsActioned	(){return _BIT_TEST(dwFlag, ATTACK_RESULT_ACTIONED );}
	};


	typedef struct _HPINFO
	{
		HPTYPE m_HP;
		HPTYPE m_MaxHP;
		DWORD    m_LV;

	} HPINFO;


	typedef list<ATTACK_RESULT>				ATTACK_RESULT_LIST;
	typedef ATTACK_RESULT_LIST::iterator	ATTACK_RESULT_LIST_IT;




	enum eActionID
	{
		 ACTION_INVALID			= -1
		,ACTION_IDLE				= 0
		,ACTION_ATTACK				= 1
		,ACTION_STYLE_ATTACK		= 2
		,ACTION_SKILL				= 3
		,ACTION_MOVE				= 4
		,ACTION_KEYBOARDMOVE		= 5
		,ACTION_KNOCKBACK			= 6
		,ACTION_TOAIR				= 7
		,ACTION_DOWN				= 8
		,ACTION_GETUP				= 9
		,ACTION_NPCMEET			= 10
		,ACTION_GETITEM			= 11
		,ACTION_JUMP				= 12
		,ACTION_EVENTJUMP			= 13
		,ACTION_STOP				= 14
		,ACTION_STABBERSHOT		= 15
		,ACTION_VENDORMEET		= 16
		,ACTION_PLAYERMEET		= 17
		,ACTION_SIT					= 18	
		,ACTION_SPECIAL_MOVE		= 19	
		,ACTION_USE_OBJECT		= 20
		,ACTION_PLAYERFOLLOW		= 21
		,ACTION_MAX

	};


CONSTARRAY_DECLARE(ActionType
                  ,eActionID
						,ACTION_IDLE
						,ACTION_MAX
						,_CLIENTGAMELIB_API);



/*////////////////////////////////////////////////////////////////////////
//人物动作
/*////////////////////////////////////////////////////////////////////////
	typedef struct _PLAYER_ACTION
	{
		eActionID ActionID;	
		DWORD dwTimeSerial;
		
		union
		{
			///////////////////////////////////
			struct 
			{
				VECTOR3D	vCurPos;
			} IDLE;

			///////////////////////////////////
			struct 
			{			
				BOOL		bThrust;
				BYTE		byState;
				sTILE_POS		vCurPos;
				sTILE_POS		vDestPos;
			} MOVE;

			///////////////////////////////////
			struct 
			{
				sTILE_POS		vKeyboardCurPos;
				float		fKeyboardAngle;	
				BYTE		byKeyboardState;
			} KEYBOARDMOVE;

			///////////////////////////////////
			struct 
			{
				DWORD				dwAttackSerial;	
				DWORD				dwAttackStyle;
				BYTE				byAttackType;
				DWORD				dwTargetID;
				BOOL				bAttackMove;
				//ATTACK_RESULT*	pAttackResult;	/// 相关的攻击结果
				sTILE_POS			vCurPos;
				sTILE_POS			vDestPos;
				WORD				wStandDelay;		/// 站立时间
			} ATTACK;

			///////////////////////////////////
			struct
			{
				DWORD		dwEmoteID;
				VECTOR3D	vEmoteCurPos;
			} EMOTE;

			///////////////////////////////////
			struct
			{
				DWORD		dwAttackSerial;
				DWORD		dwSkillID;
				DWORD		dwTargetID;			
				sTILE_POS		vCurPos;
				sTILE_POS		vDestPos;
				VECTOR3D    vTargePos;//惯荤眉 鸥百 器胶
			} SKILL;

			///////////////////////////////////
			struct
			{
				float		fInitialSpeed;
				sTILE_POS		vCurPos;
				sTILE_POS		vDestPos;
				DWORD		dwEndTime; // 馆靛矫 捞矫埃救俊 悼累捞 场唱具窃
			} KNOCKBACK;

			///////////////////////////////////
			struct
			{			
				float		fInitialSpeed;
				DWORD		dwDelay;
				sTILE_POS		vCurPos;
				sTILE_POS		vDestPos;
				BOOL		bFly;
				DWORD		dwEndTime;   // 馆靛矫 捞矫埃救俊 悼累捞 场唱具窃
				DWORD		dwGetupTime; // 捞矫埃俊 老绢巢.
			} DOWN;

			///////////////////////////////////
			struct
			{
				float		fDownForceLimit;
				float		fGravity;
				float		fHeightLimit;
					float		fUpperForce;
				DWORD		dwAirTime;     // 栋乐阑矫埃
				DWORD		dwGetupTime;   // 老绢朝矫埃
				BOOL        bForceDown;   // 弥措 臭捞 鳖瘤 啊搁 碍力肺 郴副巴牢啊.
			} TOAIR;

			///////////////////////////////////
			struct
			{
				DWORD		dwEndTime;
			} GETUP;

			///////////////////////////////////
			struct
			{
				DWORD		dwEndTime;
			} SPAWN;

			///////////////////////////////////
			struct
			{
				DWORD		dwNPCKey;
			} NPCMEET;

			///////////////////////////////////
			struct 
			{
				DWORD		dwItemKey;
			} GETITEM;

			///////////////////////////////////
			struct 
			{
				sTILE_POS		vCurPos;
				sTILE_POS		vDestPos;
				int			iDestTileNum;
				DWORD		dwStartTime;
				DWORD		dwEndTime;
			} EVENT_JUMP;

			///////////////////////////////////
			struct 
			{
				sTILE_POS		vCurPos;
			} STOP;

			///////////////////////////////////
			struct 
			{
				DWORD    dwTargetID;
			} STABBERSHOT;

			///////////////////////////////////
			  struct 
			  {
					DWORD    dwTargetID;
			  } VENDORMEET;

			///////////////////////////////////
			struct 
			  {
					DWORD    dwTargetID;
			  } PLAYERMEET;

			///////////////////////////////////
			struct 
			  {
					DWORD    dwTargetID;
			  } USEOBJECT;

			///////////////////////////////////
			struct 
			{
				sTILE_POS		vCurPos;
				float		fAngle;	
				BYTE		bType;
			} SPECIALMOVE;
		};
	} PLAYER_ACTION;

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	typedef struct _SKILL_EFFECT_DATA
	{
		int        iEffectID;
		DWORD      dwSkillID;
		DWORD      dwAbilityID;
		DWORD      dwStatusID;	
		DWORD      dwDuration;
		int        iRemainTime;
		HANDLE     hEffect;
		float      fEffectHeight;
		BOOL       bAutoDestroy;//iRemainTime 
		BYTE       byEffectPos;
		sABILITYINFO_BASE AbilityInfo;
	} SKILL_EFFECTDAT;


	typedef list<SKILL_EFFECT*>			SKILL_EFFECT_LIST;
	typedef SKILL_EFFECT_LIST::iterator	SKILL_EFFECT_LIST_IT;

	typedef struct _TOAIR_INFO
	{
		float		fGravity;
		float		fDownForceLimit; // 
		float		fHeightLimit;
		float    fUpperForce;
		float    bGroundHit;		//
		int      iBoundCount;
		BOOL     bForceDown;
	} TOAIR_INFO;


	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	struct sNEXTMOVEINFO
	{
		VECTOR3D		m_vNextMoveCurPos;
		VECTOR3D		m_vNextMoveDestPos;
		BYTE			m_bNextMoveState;
		BOOL			m_bNextMoveThrust;
		BOOL			m_bNextMove;
		int			m_iNextMoveCurTile;
		int			m_iNextMoveDestTile;
	};

};//namespace object


#endif //__CHARACTERDEFINE_H__


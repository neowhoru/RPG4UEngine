/*////////////////////////////////////////////////////////////////////////
文 件 名：VCharacterDefine.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTERDEFINE_H__
#define __VCHARACTERDEFINE_H__
#pragma once


////////////////////////////////////////////////////
#ifdef USE_LIB

#endif
#include "SkeletonDefine.h"

using namespace std;
class TextLogger;
class ISkillSpecialEffect;



namespace vobject
{ 

////////////////////////////////////////////////////
#define MAX_TARGET_PLAYER				16
#define MAX_WAIT_TIME					0x7fffffff
#define MAX_STATUS_TIME					0x7fffffff
#define ROTATECORRECT					0.02f
#define THEROLEANDTERRAINOFFZLIMIT	-0.0618f
#define CHAT_INFO_WIDTH_MAX			32


enum
{
	 eNotMoving = 0						// 不在移动
	,eMovingForward						// 直线向前移动
	,eMovingForwardLeft					// 左侧向前移动
	,eMovingForwardRight					// 右侧向前移动
	,eMovingLeft							// 向左平移
	,eMovingRight							// 向右平移
	,eMovingCountermarchLeft			// 左侧后退移动
	,eMovingCountermarchRight			// 右侧后退移动
	,eMovingCountermarch					// 后退移动
	,eMovingTypeMax						// 最大的移动消息类型数
};

////////////////////////////////////////////////////
class VCharacter;
struct VCharacterEquipment;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum FIGHTCHANGE
{
	eNoChangeFight,
	eChangeFightAll,
};


//关于状态信息
struct sCHAR_STATEINFO
{
	DWORD			dwSerialNo;		//状态流水号
	CODETYPE		StatusID;		//该状态的id编号	
	LEVELTYPE	StateLevel;		//附加该状态的等级
	INT			nRemainTime;	//附加该状态的余下时间
	DWORD			dwEffectID;		//在特效管理器内ID
};
typedef map<DWORD,sCHAR_STATEINFO>	StateInfoMap;
typedef StateInfoMap::iterator		StateInfoMapIt;
typedef pair<DWORD,sCHAR_STATEINFO>	StateInfoPair;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _CLIENTGAMELIB_API VCharacterEquipment
{

	VCharacterEquipment()
	{
		iModelID		= INVALID_CODETYPE;
		iModelID2	= INVALID_CODETYPE;
		iModelID3	= INVALID_CODETYPE;
	}

	CODETYPE					vitemID;

	///< 装备的模型ID
	int						iModelID;
	int						iModelID2; //双手武器，和头上饰物
	int						iModelID3; //收刀后的模型
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _CLIENTGAMELIB_API sOPERATION
{
	enum
	{
		 eOpNothing
		,eOpIdle					//待机
		,eOpUseSkill			//使用技能
		,eOpTalk					//对话
		,eOpGetItem				//取得道具
		,eOpDropItem			//丢弃道具
		,eOpMoveTo				//移动到目标地点
		,eOpTrade				//交易
		,eOpChangeDir			//转身
		,eOpMoveSkill			//移动过去使用技能
		,eOpMoveTalk			//移动过去对话
		,eOpMoveGetItem		//移动过去取得道具
		,eOpMoveGetRangeItem //取得周围的道具
		,eOpMoveDropItem		//移动过去丢弃道具
		,eOpMoveTrade			//移动过去交易
		,eOpMoveFollow			//跟随
		,eOpDoAction			//作莫个行为的Action
		,eOpVerifyPos			// 位置校正
		,eOpMax
	};


	enum
	{
		 eTimeOut		= _BIT(0)		// 超时了
		,eNoTryTimes	= _BIT(1)		// 尝试次数完了
		,eUpdate			= _BIT(2)		// 更新了一次

	};

	DWORD dwType;				// (当前的行动)操作类型
	struct Target
	{
		int stDst;				//目标npc
		int x, y;				// 坐标(保留移动的目标坐标) 
	} target;					// 目标


	/////////////////////////////////////
	struct Skill
	{
		int	iSkillID;			//使用的技能编号
		int	iSkillLevel;		//使用的技能等级
	}skill;

	//
	struct Action
	{
		int iActionID;			//需要做的动作的编号
	}action;


	//呆在原地攻击
	bool		bHoldAttack;
	bool		bAttackToDeadFlag;	//不过是攻击,技能判断能否攻击到死
	DWORD		dwFlag;				// 标志位
	//
	DWORD		dwOperBeginTime;	//该操作开始的时间

	sOPERATION(){ Clear(); }
	void	Clear();
};


using namespace std;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _CLIENTGAMELIB_API sSKILL_RUNINFO
{
	enum
	{
		 eNone
		,eIntonate
		,eHold
	};

	DWORD		dwStartTime;			// 技能开始时间
	DWORD		dwIntonateTime;		// 吟唱时间
	DWORD		dwHoldTime;				// 持续时间
	DWORD		dwCancelTime;			// 取消时间
	BOOL		bCancelHasSent;		// 取消是否已发送
	BOOL		bMovePermit;			// 是否允许移动
	DWORD		dwSkillStatus;			// 当前技能状态，吟唱或者持续
	vector<ISkillSpecialEffect*> vectorEffect;		// 当前使用的特效
	//IntonateInfo intonateInfo;

	sSKILL_RUNINFO	(){ Reset() ; }
	void	Reset				();
	BOOL	Cancel			( DWORD dwTime );

	void	Start				( DWORD dwStatus ){ dwSkillStatus = dwStatus; }
	void	AddEffect		( ISkillSpecialEffect* p ){ vectorEffect.push_back( p ); }

	DWORD	GetStatus()		{ return dwSkillStatus; }
	void	CancelHasSent	( BOOL b )	{ bCancelHasSent = b; }
	BOOL	IsCancelHasSent()				{ return bCancelHasSent; }
	void	PermitMove		( BOOL b )	{ bMovePermit = b; }
	BOOL	IsPermitMove	()				{ return bMovePermit; }


};

struct sHOLD_SKILL
{
	int iSkillID;
	int iSkillLevel;
};

enum enumStatus
{
	//普通状态
	Player_Status_Normal			= 0
	//吟唱状态
	,Player_Status_Intonate
	//维持技能状态
	,Player_Status_HoldSkill
	//死亡状态
	,Player_Status_Death
};


struct sMOVE_INFO
{
	BOOL				bHalfStep;					//是否走到一半，如果为fase，则认为新的一步开始
	DWORD				dwTimeLeft;					//上一步的多余时间
	DWORD				dwStartTime;				//开始走的时间
	DWORD				dwStepTime;					//一步的时间
	Vector3X		vStartPos,vEndPos;		//起始位置，结束位置,(象素坐标)
   POINT				tEndPoint;              //移动的目标位置(图素级别)
	float				fVelocity;					//速度，每秒n格

	sMOVE_INFO():bHalfStep(FALSE){}
};




};//namespace vobject


#endif //__VCHARACTERDEFINE_H__
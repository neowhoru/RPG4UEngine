/*////////////////////////////////////////////////////////////////////////
文 件 名：GameParameter.h
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEPARAMETER_H__
#define __GAMEPARAMETER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "ImplBaseManager.h"
#include "GameStruct.h"



using namespace std;
struct sLOGINIP_INFO
{
	TCHAR			szIP[MAX_IP_LENGTH];
	PORTTYPE		port;
};

typedef vector<sLOGINIP_INFO>		LoginIPArray;

enum eOptionDamageOutput
{
	 ODO_SHOWALL		= 0
	,ODO_SHOWMINE		= 1
	,ODO_NONE		   = 2
	,ODO_MAX
};


class IGameParameter;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API GameParameter : public ImplBaseManager
{
public:
	GameParameter(void);
	GameParameter(char* filename);
	virtual ~GameParameter(void);


public:
	void			SetSettingFileName( LPCSTR  szSettingFileName );
	BOOL			WriteInt		(TCHAR* KeyName, int DefaultValue);
	BOOL			WriteFloat	(TCHAR* KeyName, float DefaultValue);

	//BOOL		InstallImpl		(IGameParameter* pImpl);
	//BOOL		UninstallImpl	(IGameParameter* pImpl);

protected:
	//IGameParameter*	m_pImpl;
	char					m_szFullPath[MAX_PATH];

};



//////////////////////////////////////////////////////////////////////////
// GameParameter
class _CLIENTGAMELIB_API GeneralGameParam : public GameParameter
{
	friend class GameParameterImpl;
public:
	GeneralGameParam();


public:
	VG_BOOL_GET_PROPERTY		(EnableNetwork);
	VG_BOOL_GET_PROPERTY		(NetSimple);
	VG_BOOL_GET_PROPERTY		(UITesting);
	VG_BOOL_GET_PROPERTY		(NetCheck);

	VG_BOOL_GET_PROPERTY		(LocalHeroData);

	VG_DWORD_GET_PROPERTY	(NetDialogDelay);
	VG_DWORD_GET_PROPERTY	(NetDialogResID);

	VG_DWORD_PROPERTY			(AutoDragReadyDelay);//以下为自动点击鼠标右键设置  AutoDragReadyDelay非0时，
	VG_DWORD_PROPERTY			(AutoDragTimePeriod);//启动此功能;每隔 AutoDragTimePeriod时，是发送一次鼠标左键处理

	VG_BOOL_PROPERTY			(HeroActionAtServerSync);	//Hero须与服务端同步任何同步，即发送成功后，方可行走等

	//以下只对Village有效
	//StartMapX = -1时，StartTileX取绝对Land坐标
	//否则，StartTileX只取Map内坐标，范围[0,128)
	VG_TYPE_GET_PROPERTY		(StartMapID,	MAPCODE);
	VG_TYPE_GET_PROPERTY		(StartFieldID,	MAPCODE);
	VG_INT_GET_PROPERTY		(StartMapX);
	VG_INT_GET_PROPERTY		(StartMapY);
	VG_INT_GET_PROPERTY		(StartTileX);
	VG_INT_GET_PROPERTY		(StartTileY);

	VG_INT2_GET_PROPERTY		(AuthServerVersion);



	VG_CSTR_PROPERTY			(GameServerIP,		MAX_IP_LENGTH);
	VG_TYPE_PROPERTY			(GameServerPort,	PORTTYPE);

	VG_CSTR_PROPERTY			(ChatServerIP,MAX_IP_LENGTH);
	VG_TYPE_PROPERTY			(ChatServerPort,	PORTTYPE);

	VG_CSTR_PROPERTY			(LoginServerIP,MAX_IP_LENGTH);
	VG_TYPE_PROPERTY			(LoginServerPort,	PORTTYPE);

	VG_DWORD_PROPERTY			(UserID);


	VG_DWORD_PROPERTY			(LoginReconnectDelay);

	VG_DWORD_PROPERTY			(SendPacketSize		);
	VG_DWORD_PROPERTY			(SendPacketMaxSize	);



	VG_INT2_GET_PROPERTY		(WorldServerReconnectDelay);


	VG_BOOL_PROPERTY			(ShowSpecialArea);
	VG_BOOL_PROPERTY			(AutoAttackAfterUseSkill);
	VG_FLOAT_PROPERTY			(AttackReadyRange);
	VG_DWORD_PROPERTY			(AttackReadyPeriod);
	VG_DWORD_PROPERTY			(KBDMoveSendDuration);
	VG_DWORD_PROPERTY			(TickHeartBeat);

	VG_FLOAT_PROPERTY			(CameraNearClip);
	VG_FLOAT_PROPERTY			(CameraFarClip);
	VG_FLOAT_PROPERTY			(CameraFOV);
	VG_FLOAT_PROPERTY			(CameraDistanceMin);
	VG_FLOAT_PROPERTY			(CameraDistanceMax);
	VG_FLOAT_PROPERTY			(CameraDistance);
	VG_FLOAT_PROPERTY			(CameraAlphaDistance);
	VG_FLOAT_PROPERTY			(CameraPitchMin);
	VG_FLOAT_PROPERTY			(CameraPitchMax);
	VG_FLOAT_PROPERTY			(CameraAnglePitch);
	VG_FLOAT_PROPERTY			(CameraYawMin);
	VG_FLOAT_PROPERTY			(CameraYawMax);
	VG_FLOAT_PROPERTY			(CameraAngleYaw);
	VG_FLOAT_PROPERTY			(CameraYawCenter);
	VG_FLOAT_PROPERTY			(CameraRotationStep);
	VG_FLOAT_PROPERTY			(CameraRotationSlowDownLevel);
	VG_FLOAT_PROPERTY			(CameraRotationSlowDown);
	VG_FLOAT_PROPERTY			(CameraRotationMin);
	VG_FLOAT_PROPERTY			(CameraCtrlRotationSpeed);
	VG_FLOAT_PROPERTY			(CameraUpCorrect);
	VG_FLOAT_PROPERTY			(CameraTargetTrackSlotDownStep);

	VG_INT2_GET_PROPERTY		(CameraRotation);
	VG_CSTR_GET_PROPERTY		(StartMovieFile, MAX_PATH);

	VG_FLOAT_GET_PROPERTY	(CameraBlockAlpha);

	VG_BOOL_GET_PROPERTY		(ServerDebug);
	VG_INT2_GET_PROPERTY		(SpecialMode);


	VG_BOOL_PROPERTY			(FreeCameraState);
	VG_FLOAT_GET_PROPERTY	(FreeCameraSpeed);
	VG_INT2_GET_PROPERTY		(FreeCameraTurnSpeed);

	//////////////////////////////////////////////////////////////////////////
public:
	VG_FLOAT_PROPERTY			(DistanceVoiceRange		);
	VG_FLOAT_PROPERTY			(DistanceNpcMeet			);
	VG_FLOAT_PROPERTY			(PercentDistanceNpcMeet	);
	VG_FLOAT_PROPERTY			(DistanceGetItem			);
	VG_FLOAT_PROPERTY			(DistanceTabTarget		);
	VG_FLOAT_PROPERTY			(DistanceTrading			);
	VG_FLOAT_PROPERTY			(DistanceFollowPlayer	);
	VG_FLOAT_PROPERTY			(DistanceSeekRange		);

	VG_DWORD_PROPERTY			(NpcWaitSoundDelayVariable);

	VG_DWORD_PROPERTY			(ScriptRunPeriod);
	VG_DWORD_PROPERTY			(ItemLogShowDelay);

	VG_DWORD_PROPERTY			(QuestStateUpdatePeriod);
	VG_DWORD_PROPERTY			(ObjectMouseTipReady);
	VG_DWORD_PROPERTY			(ObjectMouseTipDelay);
	VG_DWORD_PROPERTY			(FreshManTipReadyTime);
	VG_DWORD_PROPERTY			(ToolTipDelay);
	VG_DWORD_PROPERTY			(ChatDialogBoxLifeTime);
	VG_DWORD_PROPERTY			(ObjectRemoveTime);
	VG_DWORD_PROPERTY			(DeadDisappearTime);

	VG_DWORD_PROPERTY			(PlayDamageSoundRate);
	VG_DWORD_PROPERTY			(PlayDamageAnimRate);

	VG_DWORD_PROPERTY			(SpecialIdleIntervalMin);
	VG_DWORD_PROPERTY			(SpecialIdleIntervalMax);
	VG_INT2_GET_PROPERTY		(MaxActionQueue);

	///////////////////////////////////////////
	VG_DWORD_PROPERTY			(PopupTextCharDelay	);
	VG_DWORD_PROPERTY			(PopupTextCharLife	);
	VG_DWORD_PROPERTY			(PopupTextCharLifeRange);
	VG_INT_PROPERTY			(PopupTextCharSpaceH	);
	VG_INT_PROPERTY			(PopupTextCharSpaceV	);

	///////////////////////////////////////////
	//帧速控制
	VG_INT2_GET_PROPERTY	(MaxFramePerSecond);
	VG_INT2_GET_PROPERTY	(MinFramePerSecond);
	VG_INT2_GET_PROPERTY	(NonFocusMaxFramePerSecond);
	VG_INT2_GET_PROPERTY	(NonFocusMinFramePerSecond);
	VG_INT2_GET_PROPERTY	(NonActiveMaxFramePerSecond);
	VG_INT2_GET_PROPERTY	(NonActiveMinFramePerSecond);
	VG_BOOL_GET_PROPERTY	(EnableRenderOnly);
	VG_BOOL_GET_PROPERTY	(FixedFrame);
	VG_DWORD_GET_PROPERTY(IdleDelay);

	VG_DWORD_GET_PROPERTY(UpdateSvrStatusInterval);	//在登录列表时，刷新服务线状态的时间间隔

	VG_CSTR_GET_PROPERTY	(LoginSceneName,			MAX_WORLD_NAME_LEN);
	VG_CSTR_GET_PROPERTY	(CharSelectSceneName,	MAX_WORLD_NAME_LEN);
	VG_CSTR_GET_PROPERTY	(FirstSceneType,			MAX_WORLD_NAME_LEN);
	VG_BOOL_GET_PROPERTY	(MustLoadMapData);

public:
	BOOL			Load( LPCSTR szFile );
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(GeneralGameParam , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API GeneralGameParam& GetCGeneralGameParam();
#define theGeneralGameParam	singleton::GetGeneralGameParam()



//////////////////////////////////////////////////////////////////////////
// GameParameter 甸...
class _CLIENTGAMELIB_API LoginGameParam : public GameParameter
{
	friend class GameParameterImpl;
public:
    LoginGameParam();

public:
	BOOL	Load( LPCSTR szFile );

public:
	sLOGINIP_INFO*	GetLoginIP(INT nAt);
	void				PushLoginIP			(LPCTSTR szIP,PORTTYPE port);
	void				ClearLoginIPs		(){m_arLoginIPs.clear();}
	UINT				GetLoginIPNum		(){return m_arLoginIPs.size();}

private:
	LoginIPArray		m_arLoginIPs;

private:


	VG_INT2_PROPERTY		(ServiceLoginTryCounts);

	VG_TYPE_PROPERTY		(SelectedServiceLoginServerIP,	StringHandle);
	VG_TYPE_PROPERTY		(SelectedServiceLoginServerPort, PORTTYPE);
	VG_DWORD_PROPERTY		(ServerZoneID);




    //////////////////////////////////////////////////////////////////////////
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(LoginGameParam , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API LoginGameParam& singleton::GetLoginGameParam();
#define theLoginGameParam  singleton::GetLoginGameParam()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API GameOption : public GameParameter
{
	friend class GameParameterImpl;
	//////////////////////////////////////////////////////////////////////////
public:
	GameOption();
	~GameOption();

public:
	BOOL			Init();
	void			Release();

public:
	BOOL			Load(LPCSTR szFile);
	BOOL			Save();


public:
	sOPTIONPARAM	GetGameOption()			;
	void			SetGameOption( sOPTIONPARAM OptionParameter )			;


	/////////////
	BOOL		GetTextureQuality()		;
	BOOL		GetLOD()					;
	BOOL		GetNormalTextureQuality()	;
	BOOL		GetShadowQuality()			;


	///////////////
	BOOL		GetBlurState()					;
	BOOL		GetFogState()				;
	BOOL		GetMapSpecular()			;
	BOOL		GetSystemVoice()			;
	BOOL		GetNPCVoice()				;


	////////////////////
	float		GeBGMVolume()				;
	float		GetAmbientEffectVolume()	;
	float		GetEffectVolume()			;

	int     GetDamageOutput() ;
	BOOL    GetAutoMove()		;
	BOOL    GetShowPlayerName() ;
	BOOL    GetShowNPCName() ;
	void    SetShowHelm( BOOL bShow ) ;
	BOOL    GetShowHelm() ;
	void    SetShowHelp( BOOL bShow ) ;
	BOOL    GetShowHelp() ;

private:
	sOPTIONPARAM		m_OptionParam;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(GameOption , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API GameOption& singleton::GetGameOption();
#define theGameOption	singleton::GetGameOption()


#include "GameParameter.inl"


#endif //__GAMEPARAMETER_H__
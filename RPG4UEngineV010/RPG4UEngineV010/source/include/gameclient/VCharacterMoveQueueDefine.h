/*////////////////////////////////////////////////////////////////////////
文 件 名：VCharacterMoveQueueDefine.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTERMOVEQUEUEDEFINE_H__
#define __VCHARACTERMOVEQUEUEDEFINE_H__
#pragma once

namespace vobject
{ 
	enum enumMove
	{
		 M_CONTINUEMOVING					// 持续移动
		,M_KEEPMOVINGTOPOS				// 保持移动状态移动到目标点
		,M_MOVINGTOPOS						// 移动到目标点停下来
		,M_MOVINGTOLOCKTARGET			// 移动到锁定目标

		,M_PLAYERMOVINGTOPOS				// 玩家移动到某点
		,M_PLAYERMOVINGTYPE				// 玩家移动方式的变化
		,M_PLAYERMOVINGDIRANDTYPE		// 玩家方向和移动方式的变化

		,M_PLAYERKEEPMOVINGPOS			// 玩家持续移动通过某点
		,M_PLAYERKEEPMOVINGPOSANDDIR	// 玩家以某朝向持续移动通过某点
		,M_PLAYERPOS						// 玩家原地不动的所在点
		,M_PLAYERDIR						// 玩家原地不动的面朝方向
		,M_PLAYERPOSANDDIR				// 玩家原地不动的所在点和面朝方向
	};

	struct _CLIENTGAMELIB_API sMOVEQUEUE_INFO
	{
		// 类型
		short			stType;
		bool			bSkip;			// 是否已经失效,,当bSkip = TRUE表示该行为无效，或该行为为空的
		
		DWORD			dwSerialNo;		// 指令序列编号，现在简单的用时间来标示
											// 后来的指令一定在数值上大于先前的指令
		DWORD			dwPushTime;		//动作入列时间

		int			nTargetID;		// 目标的ID
		float			fKeepDistance; // 和目标保持的距离

		short			shX;				// 目标点X
		short			shY;				// 目标点Y

		VECTOR3D		vTargetPos;		// 目标点
		VECTOR3D		vDir;				// 当前的朝向
		short			shMovingType;	// 移动类型

		sMOVEQUEUE_INFO()
			:bSkip(false){}
	};

};//namespace vobject

#endif //__VCHARACTERMOVEQUEUEDEFINE_H__


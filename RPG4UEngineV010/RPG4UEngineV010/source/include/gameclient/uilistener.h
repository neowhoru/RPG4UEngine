
/*////////////////////////////////////////////////////////////////////////
文 件 名：GameUIManagerDefine.h
创建日期：2007年4月5日
最后更新：2007年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __UILISTENER_H__
#define __UILISTENER_H__
#pragma once

#include "GameDefine.h"
#include "GameUIManagerDefine.h"

struct MSG_BASE;



class PlayerRender;
class PathGameMap;

_NAMESPACEU(VObject, vobject);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace gameui
{

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API UIListener
{
public:
	void RegisterMe();
	void UnregisterMe();

	virtual LPCSTR GetName() = 0;
	virtual EGameUIType GetType() = 0;

public:
	virtual void Refresh		(BOOL bExtra=FALSE){__UNUSED(bExtra);}

	virtual void ShowByTime	(DWORD               /*dwStartTime*/
                           ,DWORD               /*dwPeriod*/
									,BOOL bUp =          true
									,int nFlashCount =   0
									,BOOL bAutoClose =   false
									,BOOL bFadeIn =      false
									,const char* pName = NULL
									,BOOL bIsInterrupt = false)	
									{
										__UNUSED(bUp);
										__UNUSED(nFlashCount);
										__UNUSED(bAutoClose);
										__UNUSED(bFadeIn);
										__UNUSED(pName);
										__UNUSED(bIsInterrupt);
									}

	virtual void StartReadytoLaunch	(DWORD /*dwStartTime*/
                                    ,DWORD /*dwPeriod*/
												,short /*shStartPermillage*/)	{}
	virtual void Cancel					( DWORD dwStopTime=0 ){__UNUSED(dwStopTime);}
	virtual void CountDown				(){}
	virtual void ChangeShopState		( BOOL /*bOpen*/ ){}


	virtual void SetData	(DWORD  /*dwType*/
                        ,LPARAM /*dwData*/)	{}
	virtual BOOL GetData	(DWORD /*dwType*/
                        ,void* /*pRet*/)	{return FALSE;}


	virtual void OnMessage		(MSG_BASE* /*pMsg*/){}
	virtual void OnWorldMessage(MSG_BASE* /*pMsg*/){}
	virtual void OnMouseRButton(VObject* /*pPlayer*/
                              ,int      /*x*/
										,int      /*y*/
										,BOOL     /*bPress*/)	{}
	virtual void OnMouseLButton(VObject* /*pPlayer*/
                              ,int      /*x*/
										,int      /*y*/
										,BOOL     /*bPress*/)	{}



	virtual void DisplayInfo		(LPCSTR               /*sText*/
                                 ,const COLOR col = 0xffffffff
											,const UINT nType =   gameui::CHANNEL_SYSTEM
											,char *szToName =     NULL)	
	{__UNUSED(col);__UNUSED(nType);__UNUSED(szToName);}

	virtual void DisplayTip			(LPCSTR /*sText*/
                                 ,DWORD  /*dwData*/)	{}

	virtual void ShowMessageBox	(LPCSTR  /*szText*/
                                 ,LPCSTR	szCaption = ""
											,int		nType			=  MB_OK
											,BOOL		bModal		=  true
											,void*	pCallback	=  NULL
											,void *	pData			=  NULL
											,int		nDataLength =  0)
	{
		__UNUSED(szCaption);
		__UNUSED(nType			);
		__UNUSED(bModal		);
		__UNUSED(pCallback	);
		__UNUSED(pData			);
		__UNUSED(nDataLength);
	}

	virtual void ShowInputBox	(LPCSTR            /*szCaption*/
									,BOOL              /*bNumber=TRUE*/
									,BOOL              /*bModal=TRUE*/
									,void*				 /*procCallback=NULL*/
									,void*             /*pData=NULL*/
									,int               /*nDataLength=0*/
									,BOOL              /*bPassword=FALSE*/	){};


	virtual void ShowSelectBox		(LPCSTR                 /*szText*/
                                 ,void*                  /*pItemList*/
											,int                    /*nItemCount*/
											,LPCSTR	szCaption		= ""
											,BOOL		bModal			= true
											,void*	funCallbackFun = NULL
											,void *	pData				= NULL
											,int		nDataLength		= 0)
	{
		__UNUSED(szCaption		);
		__UNUSED(bModal			);
		__UNUSED(funCallbackFun );
		__UNUSED(pData				);
		__UNUSED(nDataLength		);
	}

	virtual void SetMoveText(LPCSTR /*szText*/, int /*nCount*/ ){}


	virtual void LoadUI			(EGameUIType	nType=eUINull){__UNUSED(nType);}
	virtual void UnloadUI		(EGameUIType	nType=eUINull){__UNUSED(nType);}
	virtual void OnStartUp		(EGameUIType	nType=eUINull){__UNUSED(nType);}
	virtual void OnProcess		(EGameUIType	nType=eUINull){__UNUSED(nType);}
	virtual void FrameMove		(EGameUIType	/*nType*/, DWORD /*dwTick*/){}
	virtual void OnClose			(EGameUIType	nType=eUINull){__UNUSED(nType);}
	virtual void OnEnterWorld	(){}

	virtual void OnDataInput		(void* /*pData*/){}
	virtual void TriggerFunc		(ETriggerData /*eData*/,LPARAM lpData=0){__UNUSED(lpData);}
	virtual void UpdateData			(BOOL /*bSet*/){}
	virtual BOOL IsEditInputFocus	(){return FALSE;}


};

};




#endif //__UILISTENER_H__
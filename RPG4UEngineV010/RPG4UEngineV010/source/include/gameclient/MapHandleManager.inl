/*////////////////////////////////////////////////////////////////////////
文 件 名：MapHandleManager.inl
创建日期：2006年9月14日
最后更新：2006年9月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPHANDLEMANAGER_INL__
#define __MAPHANDLEMANAGER_INL__
#pragma once



	inline MapHandle* MapHandleManager::FindMapHandle		(WH_TYPE type)
	{
		return (MapHandle*)_SUPER::FindSystem(type);
	}
	inline MapHandle* MapHandleManager::GetActiveMapHandle	()
	{
		assert(_SUPER::GetActiveSystem());
		return (MapHandle*)_SUPER::GetActiveSystem();
	}
	inline BOOL MapHandleManager::RegMapHandle		(MapHandle* pHandler)
	{
		return _SUPER::RegSystem(pHandler);
	}
	inline BOOL MapHandleManager::UnregMapHandle		(MapHandle* pHandler)
	{
		return _SUPER::UnregSystem(pHandler);
	}


inline BOOL MapHandleManager::FrameMove(DWORD dwTick)
{
	__BOOL_CALL(GetActiveMapHandle()->FrameMove(dwTick));
	return TRUE;
}

inline BOOL MapHandleManager::ReadyRender(DWORD dwTick)
{
	__BOOL_CALL(GetActiveMapHandle()->ReadyRender(dwTick));
	return TRUE;
}

inline BOOL MapHandleManager::AfterRender(DWORD dwTick)
{
	__BOOL_CALL(GetActiveMapHandle()->AfterRender(dwTick));
	return TRUE;
}

inline BOOL MapHandleManager::Render(DWORD dwTick)
{
	__BOOL_CALL(GetActiveMapHandle()->Render(dwTick));
	return TRUE;
}


//BOOL MapHandleManager::ActivateSystem(MapHandle* pHandler)
//{
//	__BOOL_SUPER(ActivateSystem(pHandler));
//
//	return TRUE;
//}

inline V3DFrustum* MapHandleManager::GetSceneFrustum()
{
	return GetActiveMapHandle()->GetSceneFrustum();
}

inline V3DCamera* MapHandleManager::GetCamera()
{
	return GetActiveMapHandle()->GetCamera();
}

inline void MapHandleManager::ResetViewport()
{
	GetActiveMapHandle()->ResetViewport();
}

inline void MapHandleManager::ResetCamera()
{
	GetActiveMapHandle()->ResetCamera();
}

inline BOOL MapHandleManager::PickCamera(const RAY& ray,float& t)
{
	return GetActiveMapHandle()->PickCamera(ray,t);
}

inline BOOL MapHandleManager::PickRayAtScreen	(Ray& ray,const POINT& ptScreen)
{
	return GetActiveMapHandle()->PickRayAtScreen(ray,ptScreen);
}

inline void MapHandleManager::RebuildCamera()
{
	GetActiveMapHandle()->BuildSceneFrustum();
}



#endif //__MAPHANDLEMANAGER_INL__
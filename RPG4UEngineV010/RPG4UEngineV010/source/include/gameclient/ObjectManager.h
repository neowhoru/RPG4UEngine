/*////////////////////////////////////////////////////////////////////////
文 件 名：ObjectManager.h
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __OBJECTMANAGER_H__
#define __OBJECTMANAGER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "Object.h"
#include "ObjectManagerDefine.h"
#include "KeyGenerator.h"
#include "ImplBaseManager.h"


namespace vobject
{
	class VObject;
	class VObjectManager;
};
namespace object
{
	class Object;
	class Character;
	class ObjectSystem;
};

using namespace object;
using namespace vobject;
using namespace util;
//class util::KeyGenerator;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API ObjectManager : public ImplBaseManager
{
	friend class object::ObjectSystem;
public:
	ObjectManager( void );
	~ObjectManager( void );

protected:
	void			InitKeyGenerator		();
	void			RestoreKey				(Object * pObject);
	void			ReleaseKeyGenerator	();
	
public:
	BOOL			Create	( VObjectManager* pManager );
	void			Destroy	( void );
   void        Destroy	( const std::vector<DWORD> &vdwKeys);

public:
	Character*	CreateHero	(DWORD dwKey/*-1表示本地创建*/, BYTE byCharType);
	void			DestroyHero	();


public:
	Object *		Add			(DWORD dwObjectKey, eOBJECT_TYPE objType, DWORD dwParam);
	Object *		AddMapObject(DWORD dwObjectKey, eOBJECT_TYPE objType, APPEARANCEINFO * pAppearInfo);
	BOOL			Delete		(DWORD dwObjectKey );
	Object *		GetObject	(DWORD dwKey);
	Object *		GetFuncNpc	(DWORD dwFuncID);
	Object *		GetObject	(LPCTSTR szName
									,DWORD dwObjectType=PLAYER_OBJECT
									,BOOL  bJustPrefix=FALSE
									,BOOL	 bNearHero=FALSE);
	void			AddFuncNpc		(DWORD dwFuncID,DWORD dwObjectKey);
	void			RemoveFuncNpc	(DWORD dwFuncID);
	void			ClearFuncNpcs	();

	BOOL			AddToDeadList	(DWORD dwObjectKey);
	void			PurgeDeadList	(DWORD dwTick);

	void			DeleteObjectsByType		( eOBJECT_TYPE eObjectType );
	void			DeletePlayersWithoutHero( DWORD dwHeroKey );

	DWORD			GetIdleObjectKey	(eOBJECT_TYPE objType);

	void			RestoreItemKey		(SERIALTYPE Serial);       

	int         GetObjectCount		(eOBJECT_TYPE objType);

protected:
	Character*	_AllocHero		();
	void			_FreeHero		(Character* pHero);

	VObject*		_CreateVObject	(DWORD dwObjectKey, eOBJECT_TYPE objType, Object* pObj );
	void			_DestroyObject	(Object* pObj );

public:
	template <class OPR>	BOOL ForEach(OPR& opr, DWORD dwType=INVALID_DWORD_ID);

	ObjectMapIt GetBegin();
	ObjectMapIt GetEnd();

	DWORD			GenerateKeyAtSingleMode(eOBJECT_TYPE objType);


public:
	VG_PTR_GET_PROPERTY	(Hero,				Character);
	VG_PTR_GET_PROPERTY	(VObjectManager,	VObjectManager);

	ObjectMap				m_hashmapObject;
	FuncNpcMap				m_hashmapNpcFunc;
	ObjectMap				m_hashmapDead;
	//ObjectFactory*			m_pObjectFactory;

	KeyGenerator* 			m_pPlayerKeyGenerator;
	KeyGenerator* 			m_pMonsterKeyGenerator;
	KeyGenerator* 			m_pNpcKeyGenerator;
	KeyGenerator* 			m_pItemKeyGenerator;
	KeyGenerator* 			m_pMapObjectKeyGenerator;
	KeyGenerator* 			m_pNonCharacterKeyGenerator;

	VG_INT2_GET_PROPERTY	(PlayerCount);
	VG_INT2_GET_PROPERTY	(MonsterCount);
	VG_INT2_GET_PROPERTY	(ItemCount);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ObjectManager , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API ObjectManager& singleton::GetObjectManager();
#define theObjectManager	singleton::GetObjectManager()

////////////////////////////
//Hero实例创建前，指向默认对象，故无须做指针验证
#define theHeroBasePtr		theObjectManager.GetHero()
#define theHeroBase			(*theObjectManager.GetHero())

#include "ObjectManager.inl"

#endif //__OBJECTMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ClientSetting.h
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CLIENTSETTING_H__
#define __CLIENTSETTING_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "BaseApplicationSetting.h"

#define MAX_FONTCOUNT	3
class IniFile;
//class Archive;

class _CLIENTGAMELIB_API ClientSetting : public gamemain::BaseApplicationSetting
{
public:
	ClientSetting(void);
	~ClientSetting(void);

public:
	BOOL LoadClientSetting	(IniFile& ini);
	BOOL LoadScreen			(IniFile& ini);
	BOOL SaveScreen			(IniFile& ini);

public:

	/*////////////////////////////////////////////////////////////////////////
		UI配置信息
	/*////////////////////////////////////////////////////////////////////////
	BOOL	m_bFullScreen;
	INT	m_nLoadmapBackPicNum;
	DWORD	m_AutoSaveDataDelay;
	INT	m_MiniMapSight ;
	INT	m_MiniMapWinSize ;
	INT	m_TipLogMax;
	VRSTR	m_LogFile;

	float	m_UICreatorScale;
	float	m_UIModelScale;
	float	m_UISelectorScale;
	float	m_CharacterFadeStep;	//淡入淡出速度
	float	m_FadeOutAtBlocked;	//人物遮挡时，淡出


	/*////////////////////////////////////////////////////////////////////////
		相机配置信息
	/*////////////////////////////////////////////////////////////////////////
	float	m_NearPlane ;
	float	m_FarPlane ;
	float m_CamerFOV;
	float m_FarFog	;
	float	m_fBspRenderFactorToHeroDist;


	/*////////////////////////////////////////////////////////////////////////
		游戏相关配置信息
	/*////////////////////////////////////////////////////////////////////////
	 float m_Gravitation ; //游戏世界中的重力加速度(按豪秒记)
	 float m_NotRenderRange ;
	 float m_HeroSlipJumpingAngle ; //主角下滑的角度
	 BOOL  m_ShowAttackRange ; //显示主角攻击距离
	 float m_AttackRangeInc ;

	 float m_CharRotateSpeed;
	 float m_CharGrowSpeed;
	 float m_CharGrowStep;
	 float m_CharCollisionHeight;

	float	m_KnockBackInitSpeed;
	float	m_KnockBackAccelerate;
	float	m_KnockBackLowSpeed;
	float	m_KnockBackAnimMultiplier;

	BOOL	m_HandleCameraLButton;
	BOOL	m_HandleCameraRButton;

	/*////////////////////////////////////////////////////////////////////////
		功能开关配置信息
	/*////////////////////////////////////////////////////////////////////////
	BOOL		m_ShowCollision ; //显示阻挡
	BOOL		m_EnableRenderSkyBox ;
	float		m_SkyBoxScale ;
	BOOL		m_ShowAdvancedMiniMap ;

	float		m_CharRenderRadius ;
	BOOL		m_ShowMouseTile ;
	BOOL		m_ShowPlayer ;
	BOOL		m_RenderHero;
	BOOL		m_ShowMonsterInfo;

	/*////////////////////////////////////////////////////////////////////////
		模型特效信息
	/*////////////////////////////////////////////////////////////////////////
	VRSTR	m_sLoginSceneMusic		;
	VRSTR	m_sCharSelectSceneMusic	;
	BOOL	m_bEnableSoundEffect;
	BOOL	m_bEnableBGMusic;

	DWORD	m_TargetLockEffectPassivity;//锁定被动目标时的特效路径
	DWORD	m_TargetLockEffectInitiative;//锁定主动目标时的特效路径

	//任务提示特效
	EFFECTINDEX	m_szHaveQuestEffect			;
	EFFECTINDEX	m_szHaveQuestNotNowEffect	;
	EFFECTINDEX	m_szQuestDoneEffect			;
	EFFECTINDEX	m_szQuestNotDoneEffect		;

	//////////////////////////////////
	EFFECTINDEX	m_szRecoverHPEffect;
	EFFECTINDEX	m_szRecoverMPEffect;

	EFFECTINDEX	m_szWarriorCriticalEffectSelf;
	EFFECTINDEX	m_szRobberCriticalEffectSelf;
	EFFECTINDEX	m_szTaoistCriticalEffectSelf;
	EFFECTINDEX	m_szWarriorCriticalEffectTarget;
	EFFECTINDEX	m_szRobberCriticalEffectTarget;
	EFFECTINDEX	m_szTaoistCriticalEffectTarget;

	//背击特效
	EFFECTINDEX	m_szWarriorBackStrikeEffectSelf;
	EFFECTINDEX	m_szRobberBackStrikeEffectSelf;
	EFFECTINDEX	m_szTaoistBackStrikeEffectSelf;
	EFFECTINDEX	m_szWarriorBackStrikeEffectTarget;
	EFFECTINDEX	m_szRobberBackStrikeEffectTarget;
	EFFECTINDEX	m_szTaoistBackStrikeEffectTarget;

	//致命攻击特效
	EFFECTINDEX	m_szWarriorDeadlyEffectSelf;
	EFFECTINDEX	m_szRobberDeadlyEffectSelf;
	EFFECTINDEX	m_szTaoistDeadlyEffectSelf;
	EFFECTINDEX	m_szWarriorDeadlyEffectTarget;
	EFFECTINDEX	m_szRobberDeadlyEffectTarget;
	EFFECTINDEX	m_szTaoistDeadlyEffectTarget;


	BOOL		m_bUseObjectShadow;



	/*////////////////////////////////////////////////////////////////////////
	字体配置信息
	/*////////////////////////////////////////////////////////////////////////
	int		m_nTitleFontSize;
	int		m_nFontSize;
	int		m_nNameFontSize;
	DWORD		m_ScreenTextFadeDelay;
	DWORD		m_ScreenTextDelay;

	VRSTR		m_szFontName;
	VRSTR		m_szFontFile[MAX_FONTCOUNT];


	/*////////////////////////////////////////////////////////////////////////
	渲染配置信息
	/*////////////////////////////////////////////////////////////////////////
	BOOL		m_bUseSigleModel;

	DWORD		m_dwMultiSample;


	/*////////////////////////////////////////////////////////////////////////
	MapHandle辅助信息
	/*////////////////////////////////////////////////////////////////////////
	VG_VECTOR_PROPERTY	(CameraFrom,	Vector3D);
	VG_VECTOR_PROPERTY	(CameraTo,		Vector3D);
	VG_BOOL_PROPERTY		(LockCameraDistance);

	VG_BOOL_PROPERTY		(LoginAuto);
	VG_CSTR_PROPERTY		(LoginID,		MAX_ID_LENGTH);
	VG_CSTR_PROPERTY		(LoginPassword,MAX_PASSWORD_LENGTH);
	VG_INT2_PROPERTY		(LoginServerGroup);
	VG_INT2_PROPERTY		(LoginServerChannel);
	VG_INT2_PROPERTY		(CharSelectSlot);

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ClientSetting , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API ClientSetting& singleton::GetClientSetting();
#define theClientSetting  singleton::GetClientSetting()


#endif //__CLIENTSETTING_H__
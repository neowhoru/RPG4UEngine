/*////////////////////////////////////////////////////////////////////////
文 件 名：VObjectManager.h
创建日期：2007年1月10日
最后更新：2007年1月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTERMANAGER_H__
#define __VCHARACTERMANAGER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "QuestDefine.h"
#include "VObjectManagerDefine.h"
#include "ImplBaseManager.h"

class PathGameMap;

namespace vobject
{
class VObject;
class VCharacter;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API VObjectManager : public ImplBaseManager
{
	friend class VObjectSystem;
public:
	VObjectManager();
	virtual ~VObjectManager();

public:
	static bool SortByHeroDist(VObject*	pVObj1, VObject*	pVObj2);

public:
	BOOL Init();
	void Release();



public:
	BOOL		FrameMove			( DWORD dwTick);
	void		UpdateMouseInfo	( DWORD dwTick );
	void		Update				( DWORD dwTick);
	void		UpdateRemove		( DWORD dwTick);
	void		UpdateRendering	( DWORD dwTick);
	void		UpdateDeaths		( DWORD dwTick);
	void		UpdateBlockObjects( DWORD dwTick);

	BOOL		Render				( DWORD dwTick );
	BOOL		RenderInfo			( DWORD dwTick );

	BOOL		CheckViewPlayerBBox	( V3DVector &vDir );
	void		CheckHPInfo				( VOBJID stIDWho );


	BOOL		ProcessBlockObject	(VObject*	pVObject);
public:
	//VCharacter* CreateHero();
	//void DestroyHero();
	VObject*	AddObject				(VOBJID objID,int iCharType );
	void RemoveObject					(VOBJID objID );
	BOOL ObjectExit					(VOBJID objID );

protected:
	void _DeleteAllExceptHero			();
	BOOL _DeleteDeathPlayer				( VObject* pObject );
	BOOL _RemoveFromActiveList			( VOBJID	  objID );
	BOOL _AddToKeepList					( VObject* pObject );
	void _DestroyObject					( VObject* pObject );

	VCharacter*	_AllocVHero				();
	void			_FreeVHero				(VCharacter* pVHero);

public:
	BOOL			GetObjectsInRange	(VObjectVector& vecObjects,float fRange );
	BOOL			GetObjectsBlocked	(VObjectVector&   vecObjects
											,DWORD dwExceptType = 0);
	BOOL			GetObjectsByRay	(VObjectVector&   vecObjects
                                 ,const V3DVector& pos
											,const V3DVector& dir
											,DWORD dwExceptType = 0);
	BOOL			GetObjectsByRay	(VObjectVector&   vecObjects
                                 ,const Ray&			ray
											,DWORD dwExceptType = 0);
	// 根据服务器的Id查找VObject
	VObject*		FindByID						(VOBJID	charID );
	VObject*		FindPlayerByCharacterID	(VOBJID dwID );// 根据数据库中的角色ID查找VObject
	VObject*		FindByClientID				(VOBJID nID );	// 根据客户端的Id查找VObject
	VObject*		FindByServerIDInDeath	(VOBJID shID );// 根据服务端的Id在死亡队列中查找VObject
	VObject*		FindByClientIDInDeath	(VOBJID nID );	// 根据客户端的Id在死亡队列中查找VObject
	VObject*		FindByPt		( int x, int y );
	VObject*		FindAvatarInfo	( const char*szName );


public:
	VObject*		FindItemNpc		(void);
	VObject*		FindNearItemNpc(int x, int y, int range);
	VObject*		FindNearItemNpc(void);

	// 检查是否看玩家的个人商店
	BOOL			CheckPrivateShop( int x, int y );

	BOOL			IsHaveItemHere(int x, int y);
	BOOL			CheckViewPlayerBBox(VObject* pHero, V3DVector &vDir );
	void			SetFarFromHero	(float fDist);

public:
	VG_FLOAT_GET_PROPERTY(FarFromHero);
	VG_DWORD_PROPERTY		(GamePing);
	VG_PTR_PROPERTY		(Map,								PathGameMap);	//用来寻路的PathGameMap
	VG_TYPE_PROPERTY		(MouseTargetPlayerSeverID, VCHARID);
	VG_PTR_PROPERTY		(MouseTargetObject,			VObject);	//当前鼠标下对象

	VObjectIDMap			m_mapObjects;
	VObjectVector			m_arDeathObjects;		// 当前正在死亡的玩家
															// 当see attack得知该玩家会被杀死那一霎那
	
	VG_PTR_GET_PROPERTY	(Hero,		VCharacter);
	BlockObjectMap			m_BlockObjects;

};
};//namespace vobject

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(vobject::VObjectManager ,VObjectManager , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API VObjectManager& singleton::GetVCharacterManager();
#define theVObjectManager  singleton::GetVObjectManager()
#define theVHeroBasePtr		singleton::GetVObjectManager().GetHero()


#endif //__VCHARACTERMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：VObjectSystem.h
创建日期：2007年11月28日
最后更新：2007年11月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	为了提高性能，theVObjectSystem
		Init Release
		都皆在GameStartup中调用，全局级初始化与释放资源

	为了方便使用 VObject的特性，
		FrameMove Render 由SceneBase调用，作为局部场景渲染
		通过UserGameWorld使用触发VObject的步进

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VOBJECTSYSTEM_H__
#define __VOBJECTSYSTEM_H__
#pragma once

#include "IRenderSystem.h"
#include "VObjectManagerDefine.h"
#include <KeyGenerator.h>

namespace vobject
{
class VObject;
class VObjectRender;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API VObjectSystem : public IRenderSystem
{
public:
	VObjectSystem();
	virtual ~VObjectSystem();

public:
	virtual BOOL Init()=0;
	virtual void Release()=0;

	virtual BOOL RequestWorkingData()=0;
	virtual void ReleaseWorkingData()=0;

	virtual BOOL FrameMove(DWORD dwTick)=0;
	virtual BOOL Render(DWORD dwTick)=0;
	virtual BOOL RenderInfo(DWORD dwTick)=0;

public:
	virtual VObjectRender *	AllocRender	(DWORD dwType=VRENDER_NORMAL);
	virtual VOID				FreeRender	(VObjectRender * pRender );

	virtual VObject *			AllocObject	(eOBJECT_TYPE eObjecType );
	virtual VOID				FreeObject	( VObject * pVObject );
	virtual eOBJECT_TYPE		GetAllocType(eOBJECT_TYPE typeReq);

protected:
	util::KeyGenerator *			m_pKeyGenerator;		//统一ID生成
};
};//namespace vobject

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(vobject::VObjectSystem,VObjectSystem , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API VObjectSystem& singleton::GetVObjectSystem();
#define theVObjectSystem  singleton::GetVObjectSystem()


#endif //__VOBJECTSYSTEM_H__
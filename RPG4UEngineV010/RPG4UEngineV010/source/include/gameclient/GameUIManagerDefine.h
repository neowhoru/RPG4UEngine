/*////////////////////////////////////////////////////////////////////////
文 件 名：GameUIManagerDefine.h
创建日期：2007年4月5日
最后更新：2007年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEUIMANAGERDEFINE_H__
#define __GAMEUIMANAGERDEFINE_H__
#pragma once

#include "GameDefine.h"
#include "ConstArray.h"

//class VCharacter;
class PathGameMap;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace gameui
{

enum EGameUIType
{
	 eUINull = -1
	,eUIBlank
	,eUIRoot

	,eUISceneMOVIEPLAY
	,eUISceneSTART	
   ,eUISceneCHARSELECT
	,eUISceneLOGIN
	,eUISceneLOAD
	,eUISceneVILLAGE	
	,eUISceneLOBBY
	,eUISceneMISSION
	,eUISceneHUNTING
	,eUIScenePVP
	,eUISceneEVENT
	,eUISceneUITEST
	,eUIGameInPlaying

	,eUIActionUI
	,eUIAddFriendDlg
	,eUIBank
	,eUIBargaining
	,eUIBaseProperty
	,eUIChatInfoBox
	,eUIChatListDlg
	,eUIChatSystem
	,eUICompound
	,eUICreate
	,eUICurrState
	,eUITipLog

	,eUIESCDlg
	,eUIExitGame
	,eUIExMain
	,eUIFaceDlg
	,eUIFaceDlg1
	,eUIFriendDlg
	,eUIFriRBMeun
	,eUIGameSet
	,eUIGroup
	,eUIGroupDeBuffUI
	,eUITeamHero
	,eUIGroupPlayer1
	,eUIGroupPlayer2
	,eUIGroupPlayer3
	,eUIGroupPlayer4
	,eUIGroupPlayer5
	,eUIGroupPlayer6
	,eUIGroupPlayer7
	,eUIGroupStateUI

	,eUIGuild
	,eUIGuildAdd
	,eUIGuildAnnounce
	,eUIGuildCreate
	,eUIGuildInfo
	,eUIGuildMember
	,eUIInputBox
	,eUIIntensify
	,eUIKeyboard
	,eUILoadMap
	,eUILogin

	,eUIMain
	,eUIMap
	,eUIMessage
	,eUIMessageBox
	,eUIMiniMap
	,eUIMouseOnInfo
	,eUINpcChatDialogBox

	,eUIItemCompound
	,eUIItemMake
	,eUIItemInlay
	,eUIItemEnchant
	,eUIItemLog

	,eUINPCList
	,eUIOtheEquip
	,eUIInventory
	,eUIWareHouse
	,eUIPlayerPrivateShop
	,eUIPrivateShop
	,eUIprocess
	,eUIProgress
	,eUIProperty
	,eUIRbtnMenu

	,eUISelect
	,eUISelectBox
	,eUISelectRoleMsg
	,eUISelectServer
	,eUISelfRbtnMenu
	,eUIShop
	,eUIShortcut
	,eUISkill
	,eUISpecialSkillProgress
	,eUIStorage
	,eUITarget
	,eUITaskTrack
	,eUIQuest
	,eUIQuestTrack
	,eUITips
	,eUIFrameTop
	,eUIHeroRelive

	//未实现UI
	,eUIBattleLobby
	,eUIBattleZoneList
	,eUIBattleZoneCreate
	,eUIInputConfirm
	,eUIMax

	,eUITopMax	= 1000	//全部最多1000个控件listener
	};

	enum ESetData
	{
		 eKilledNum
		,eActionFunction
		,eGetRealRect
		,eGetLoginTryConnect
		,eSetMapLoadingProgress
		,eSetMapLoadHideAtEnd
		,eSetLoginInfo
		,eSetLoginWaitInfo
		,eSetCheckMouse
		,eSetCheckAngle
		,eSetText
		,eGetPrivatenName
		,eGetCurrentChannel
		,eGetZoomScale
		,eGetFontIndex
		,eSetFontIndex
		,eGetNpcId
		,eSetNpcID
		,eSetVisible
		,eGetVisible
		,eSetEnable
		,eGetEnable
		,eGetDefaultServer
		,eSetDefaultServer
		,eSetPlayerID
		,eGetPlayerID
		,eGetGameMapID
		,eSetGameMapID
		,eSetMoney
		,eGetScreenTitlePos
		,eGetScreenTextPos
	};

	enum ETriggerData
	{
		 eShowConnectedDlg
		,eAutoSwitchChat
		,eScriptUISetVisible
		,eShowAccountInput
		,eShowServerList
		,eLockUI					//锁定界面，不能操作
		,eClearServerGroupInfo
		,eAddServerGroupInfo
		,eSetServerGroupSelectAt
		,eCharSelectAddPlayer
		,eCharSelectSelectPlayer
		,eCharSelectRemovePlayer
		,eOpenCharacterCreator
		,eCloseCharacterCreator
		,eToggleSystemMenu
		,eToggleVisible
		,eOnPackInfoChange
		,eFillNeedMaterials
		,eOnItemCompoundFinished
		,eOnClearTarget
		,eOnReTry
		,eToggleDialogText
		,eShowDialogNext
		,eShowQuestionText
		,eShowQuestList
		,eAddTipLog
		,eChangePage
	};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define UILISTERN_DECLARE(Name, ret)\
				ret UI##Name(gameui::EGameUIType nType )
#define UILISTERN_DECLARE1(Name, ret, p1)\
				ret UI##Name(gameui::EGameUIType nType, p1)
#define UILISTERN_DECLARE2(Name, ret, p1,p2)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2)
#define UILISTERN_DECLARE3(Name, ret, p1,p2,p3)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2,p3)
#define UILISTERN_DECLARE4(Name, ret, p1,p2,p3,p4)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2,p3,p4)
#define UILISTERN_DECLARE5(Name, ret, p1,p2,p3,p4,p5)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2,p3,p4,p5)
#define UILISTERN_DECLARE6(Name, ret, p1,p2,p3,p4,p5,p6)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2,p3,p4,p5,p6)
#define UILISTERN_DECLARE7(Name, ret, p1,p2,p3,p4,p5,p6,p7)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2,p3,p4,p5,p6,p7)
#define UILISTERN_DECLARE8(Name, ret, p1,p2,p3,p4,p5,p6,p7,p8)\
				ret UI##Name(gameui::EGameUIType nType, p1,p2,p3,p4,p5,p6,p7,p8)
	

};

#endif //__GAMEUIMANAGERDEFINE_H__
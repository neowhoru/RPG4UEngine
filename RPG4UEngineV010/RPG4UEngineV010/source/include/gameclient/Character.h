/*////////////////////////////////////////////////////////////////////////
文 件 名：Character.h
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CHARACTER_H__
#define __CHARACTER_H__
#pragma once


#include "CommonDefine.h"
#include "Object.h"
#include "CharacterDefine.h"
#include "CharacterCookie.h"
#include "SkillStateFactoryDefine.h"
#include "TRingQueue.h"
#include "ScriptDefine.h"


_NAMESPACEU	(PathHandler,			tile);
_NAMESPACEU	(VCharacter,			vobject);
_NAMESPACE2U(sQUEST_RELATEINFO,	quest);
_NAMESPACE2U(sHERO_QUESTINFO,		quest);
using state::SKILL_USE_PARAMETER;

struct	MSG_CG_SYNC_MOVE_SYN;		
struct	MSG_CG_SYNC_TARGET_MOVE_SYN;
enum		ePOPUPTIP_KIND;

namespace state
{
 class StateHeroAttack;
 class StateAttack;
 class StateAir;
 class StateJump;
 class StateEventJump;
 class StateDamage;
 class StateDown;
 class StateSpecialMove;
 class StateSit;
 class StateEmotion;
};



namespace object
{ 
typedef TRingQueue<PLAYER_ACTION>		PLAYER_ACTION_QUEUE;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API Character : public Object, public CharacterCookie
{
	friend class state::StateHeroAttack;
	friend class state::StateAttack;
	friend class state::StateAir;
	friend class state::StateJump;
	friend class state::StateEventJump;
	friend class state::StateDamage;
	friend class state::StateDown;
	friend class state::StateSpecialMove;
	friend class state::StateSit;
	friend class state::StateEmotion;

public:
	Character( void );
	~Character( void );


public:
	virtual BOOL		Create( DWORD dwObjectKey, DWORD dwParam = 0 );
	virtual void		Destroy( void );

	//////////////////////////////////////////////////////////
	//
	virtual BOOL		InitializeStates	();

	virtual BOOL		Process					( DWORD dwTick );
	virtual BOOL		ProcessInterpolation	(DWORD dwTick
                                          ,float fInterpolateRatio=0.93f)		;		
	virtual void		UpdateToVObject		();

	/////////////////////////////////////////////////
	//脚本操作
public:
	virtual void					SetQuestChar	(IQuestCharacter*);
	virtual IQuestCharacter*	GetQuestChar	();
	virtual void					SetScriptChar	(IScriptCharacter*);
	virtual IScriptCharacter*	GetScriptChar	();

	virtual void					SetScriptTargetID	( DWORD dwNpcID );
	virtual void					SetVar				( KEYWORD keyVar, int nVar, int oprType = KEYW_SET );
	virtual int						GetVar				( KEYWORD keyVar );
	virtual BOOL					RegisterVar			( KEYWORD keyVar );



	//////////////////////////////////////////////
	// 任务状态操作
	virtual sHERO_QUESTINFO*		FindQuestInfoById			(CODETYPE questID );
	virtual void						UpdateActiveQuestState	();
	virtual void						ReqQuestInfo				(CODETYPE questID );

	virtual BOOL						KeepQuestScript			(DWORD dwScriptType,DWORD dwQuestType);


	/////////////////////////////////////////////////
	//模型操作
public:
	virtual void      ShowPopupTip	(int               iNumber
                                    ,ePOPUPTIP_KIND eKind
												,COLOR             color
												,BOOL              bCritical
												,float             fMultiplier=1.0f
												,int               iSecondary=-1
												,COLOR             secondarycol=0
												,float             fHeight=0.0f);
	virtual void      ShowPopupTip	(int               iNumber
                                    ,ePOPUPTIP_KIND eKind)		;

	virtual void      CreateEffectAtHead	(IDTYPE effectID,float fHeight);
	virtual float		ProcessAnimation		(DWORD dwTick ,GAMEEVENT *pEvent = NULL);	
	virtual float     GetMeshHeight			(float fExtra=0.f);
	virtual void      SetMeshHeight			(float fMeshHeight);
	virtual void      SetFadeIn				(int   iDelay);
	

	//////////////////////////////////
	//位置信息
	virtual Vector3D  GetArrowBonePos	(int iAttachIndex);
	virtual Vector3D	GetWeaponBonePos	(int index);

	virtual Vector3D	GetHandBonePos		(int index);
	virtual Vector3D	GetSkillEffectPos	(int iIndex,BYTE byType);

	virtual void		CreateProjectile(const VECTOR3D& vTarget
                                     ,float           fHeight
												 ,int             iArrowBone=0);


	virtual BOOL		OnTriggerPoint		(BOOL          bLastTriggerPoint
                                    ,int           nTriggerPointType
												,sATTACK_INFO* pAttackInfo);

public:
	//////////////////////////////////////////////////////////
	//移动处理...
	// 
	virtual BOOL		MakePathPacket	( MSG_CG_SYNC_MOVE_SYN *			pMsg );
	virtual BOOL		MakePathPacket	( MSG_CG_SYNC_TARGET_MOVE_SYN *	pMsg );

	virtual void		StopMove			( void );
	virtual BOOL		IsMoving			( void );

	virtual void		InterpolatePos	(DWORD dwInterval);	
	virtual BOOL		MoveTo			(const Vector3D& vDestPos);

	virtual BOOL		Move				(Vector3D           vCurPos
                                    ,Vector3D           vDestPos
												,BYTE               byState
												,BOOL               bThrust
												,int                iCurTileNum =  -1
												,int                iDestTileNum = -1);

	virtual void		SetNextMove		(Vector3D           vCurPos
                                    ,Vector3D           vDestPos
												,BYTE               byState
												,BOOL               bThrust
												,int                iCurTileNum =  -1
												,int                iDestTileNum = -1);
	void					StartNextMove	();

	virtual void		SetMoveFlag			(int /*flag*/) {}
	virtual void      SetSpecialMoveFlag(BYTE /*type*/){}
	

public:
	//////////////////////////////////////////////////////
	// Action queue
	int					ClearActionQueue				();
	int					GetActionQueueCount			();
	PLAYER_ACTION*		PutAction						(PLAYER_ACTION *pAction);
	virtual BOOL		CheckQueueActionCondition	();
	virtual int			DoNextAction					(BOOL bSameContinue=FALSE);
	virtual int			DoAction							(PLAYER_ACTION *pAction,BOOL bSameContinue=FALSE);
	virtual BOOL		DoNextSameAction				(eActionID peekType=ACTION_INVALID);


	// 状态及动作处理
	virtual BOOL		CommonStateProcess();
	virtual BOOL		ProcessNextAction	();
	virtual float		GetProcessFactor	();		///计算Process缩放扰值



	/////////////////////////////////////////////////////
	//jump
	//void				SetJumpFlag	(BOOL bFlag);
	void				SetJumpInfo	(const VECTOR3D&	vJumpFrom
                              ,const VECTOR3D&	vJumpTo)		;
	void				GetJumpInfo	(VECTOR3D&			vJumpFrom
                              ,VECTOR3D&			vJumpTo
										,float*				pDistance);

public:
	///////////////////////////////////////////////
	//人物动作播放操作
	virtual IDTYPE		GetCurrentIdleAnimation		() {return 0;}
	virtual IDTYPE		GetCurrentMoveAnimation		() {return 0;}
	virtual IDTYPE		GetCurrentAttackAnimation	() {return 0;}
	virtual IDTYPE		GetCurrentDeathAnimation	() {return 0;}
	virtual IDTYPE		GetCurrentDamageAnimation	(BOOL /*bRight*/) {return 0;}
	virtual IDTYPE		GetCurrentBlockAnimation	();

	virtual void		SetIdleAnimation				();
	virtual void      SetMoveAnimation				();
	virtual void      SetStandAnimationDelay		();

	virtual void		PlayDamageAnimation			(BOOL /*bRight*/) {}
	virtual void		PlayEvadeAnimation			() {}
	virtual void		PlayBlockAnimation			() {}

public:
	/*////////////////////////////////////////////////////////////////////////
	//人物普通攻击信息
	/*////////////////////////////////////////////////////////////////////////
	virtual BOOL				CanBeAttackTarget();

	/////////////////////////
	///> GetNextAttackSerial 初始化为1 
	///> 每次处理攻击时，AddNextAttackSerial	()
	///>  攻击动作流水号 SetNextActionAttackSerial(攻击流水号 )
	///>  进入State之后， 取 SetCurrentAttackSerial（攻击动作流水号 GetNextActionAttackSerial() ）
	///> 每次进入攻击过时比较时，取  GetNextAttackSerial
	virtual void				SetLatestAttack			(DWORD dwTimeSerial);
	virtual void				SetLatestQueueAttack		(DWORD dwTimeSerial);
	virtual void				AddNextAttackSerial		();

	virtual void				SetNextActionAttackSerial(DWORD dwAttackSerial);
	virtual DWORD				GetNextActionAttackSerial();


	virtual INT					AddAttackResult			(ATTACK_RESULT *pAttResult);
	virtual int					GetAttackResultListSize	();
	virtual BOOL				GetAttackTargetList		(DWORD dwAttackSerial,int *pNum,DWORD *pTarget);
	virtual ATTACK_RESULT*	GetAttackResultBy			(DWORD dwAttackSerial, DWORD dwFlag=0xFFFFFFFF);

	virtual void				EmptyAttackResultList	();
	virtual void				ProcessRemainActionResults();
	virtual BOOL				ProcessAttackResult		(DWORD   dwAttackSerial
																	,BOOL		bAll					= FALSE
																	,BOOL    bGroup				= FALSE
																	,BOOL		bRight				= FALSE
																	,DWORD   dwObjKey				= 0xFFFFFFFF
																	,BOOL    bAddtionalEffect	= TRUE);
	///< 把攻击结果更新到VObject中
	virtual BOOL				_ShowVAttackResult			(ATTACK_RESULT *	pResult
																	,sATTACK_INFO*		pAttackInfo);

	virtual BOOL				PrepareAttackAnimation				(DWORD         dwAttackSerial
																	,BOOL				bNormalAttack
																	,BOOL				bAll		= FALSE
																	,BOOL          bGroup	=FALSE
																	,DWORD         dwObjKey	=0xFFFFFFFF);
	virtual BOOL				_PrepareVAttackAnimation				(Character *		pTarget
																	,ATTACK_RESULT *	pResult
																	,sATTACK_INFO*&		pAttackInfo);

	virtual void				SetAttackMovePath			(sTILE_POS vCurPos
                                                   ,sTILE_POS vDestPos)		;

	virtual void	FlyToAir					(TOAIR_INFO *	pInfo);
	virtual void	DownToGround			();

	virtual int		GetShotCount			();
	virtual void	AddCurrentDelayCount	(DWORD dwTime);
	
public:
	////////////////////////////////////////////////////////
	// 音效处理
	virtual void		PlayAttackingSound	(eWEAPONSOUNDKIND    eSoundKind
                                          ,eARMOUR_TEX_TYPE		eArmourTex)		;
	virtual void		PlayDamageSound		(eWEAPONSOUNDKIND    eSoundKind
                                          ,eARMOUR_TEX_TYPE		eArmourTex)		;
	virtual void		PlayDeadSound			()	;
	virtual void		PlayIdleSound			()	;
	virtual void		CreateDamageEffect	(BOOL bCritical,BOOL bRight);

	virtual DWORD					GetWeaponKind			();
	virtual eWEAPONSOUNDKIND	GetWeaponSoundKind	();
	virtual eARMOUR_TEX_TYPE	GetArmourTexture		();
	virtual DWORD					GetCharSoundCode		();


public:
	////////////////////////////////////////////////////////
	// 基本属性
	virtual void		SetMoney		(MONEY Money);
	virtual void		SetLevel		(LEVELTYPE /*LV*/) {};
	virtual void		SetHP			(DWORD iHP );
	virtual void		SetMP			(DWORD iMP );
	virtual void		SetExp		(DWORD /*dwExp*/)  {};
	virtual void		SetMaxHP		( DWORD iHP );
	virtual void		SetMaxMP		( DWORD iMP );

	virtual void      SetAttackSpeedRatio	(int iSpeed);
	virtual void      SetMoveSpeedRatio		(int iSpeed);
	virtual void      SetHPInfo				(HPINFO *pInfo);

	virtual MONEY			GetMoney		()const;
	virtual LEVELTYPE		GetLevel		()const	{return 0;}
	virtual DWORD			GetHP			()const;
	virtual DWORD			GetMP			()const;
	virtual DWORD			GetExp		()const	{return 0;}
	virtual DWORD			GetMaxHP		()const	{return 0;}
	virtual DWORD			GetMaxMP		()const	{return 0;}
	virtual DWORD			GetNextExp	()const {return 0;}
	virtual DWORD			GetSex		()const;
	virtual ePLAYER_TYPE	GetClass		()const;
	virtual DWORD			GetInfoID	()const;
	
	virtual eOBJECT_TYPE			GetCharacterType() const {return GetObjectType();}
	virtual Attributes&			GetAttr();
	virtual const Attributes&	GetAttr() const;
	virtual eARMOR_TYPE		GetArmorType() const {return eARMOR_HARD;}
	virtual eMELEE_TYPE		GetMeleeType() const {return MELEETYPE_SLASH;}
	virtual DWORD				GetPhysicalAvoidValue() const {return 0;}
	virtual float				GetPhysicalAttackSpeed() const {return GetAttackSpeedModifier();};


	///////////////////////////////////////////////////////
	//信息获取 
	virtual CODETYPE	GetCurrentVSkill			();
	virtual DWORD     GetAttackDelay				()const;
	virtual DWORD     GetAttackSpeed				()const;
	virtual float     GetAttackSpeedModifier	()const;
	virtual float     GetMoveSpeedModifier		()const;

	virtual Object *	GetTargetObject			( void );
	virtual BOOL      IsNormalRangedAttack		();

public:
	///////////////////////////////////////////////////////
	//状态检测
   virtual BOOL      IsStun			();
	virtual BOOL      IsSleep			();
	virtual BOOL      IsFrozen			();
	virtual BOOL      IsBlind			();
	virtual BOOL      IsFainting		();

	virtual BOOL      CannotAct		();      // 
	virtual BOOL      CannotMove		();     // 
	virtual BOOL      CannotAir		();      // 
	virtual BOOL      CannotUseSkill	(); // 

	virtual BOOL		IsDead	();  
	virtual BOOL		IsAlive	() { return GetHP() > 0; }

public:
	//////////////////////////////////////////////////
	///作用于人物上的技能状态处理
	virtual BOOL					ProcessSkillEffect	(DWORD dwTick);
	virtual int						AddSkillEffect			(SKILL_EFFECT *pEffect);
	virtual SKILL_EFFECT *     GetSkillEffect			(int iEffectID);
	virtual SKILL_EFFECTDAT*	GetSkillEffectData	(int iEffectID);
	virtual void					DeleteSkillEffect		(int iEffectID);
	virtual void					DeleteAllSkillEffect	();
	virtual void					RemoveSkillEffectBy	(DWORD dwStatus);	//依据状态删除技能状态
	virtual void               UpdateSkillEffectPos	();
	virtual SKILL_EFFECT *     GetFirstSkillEffect	();
	virtual SKILL_EFFECT *     GetNextSkillEffect	();

	//////////////////////////////////////////////////
	///持续伤害处理
	virtual void	DoPeriodicDamage(DAMAGETYPE wDamage
                                  ,DWORD      dwTargetKey
											 ,DWORD      dwTargetHP
											 ,DWORD      timeSerial
											 ,DWORD      dwSkillCode);

public:
	/////////////////////////////////////////////////
	///状态事件
	virtual void    OnParalyzed	();	///麻痹
	virtual BOOL	 OnResurrection(float fRecoverExp		///复活计算
                                 ,float fRecoverHP
											,float fRecoverMP);
	virtual void	 OnRevive		(const Vector3D& vCurPos	///复活后处理
                                 ,DWORD           dwHP
											,DWORD           dwMP
											,DWORD           dwLevel=-1);
	virtual  void   OnStartMove	();
	virtual  void   OnStartRun		();
	virtual  void   OnDamage		(DWORD                dwAddtionalEffect
                                 ,BOOL                 bRight
											,eWEAPONSOUNDKIND     eSoundKind
											,eARMOUR_TEX_TYPE eArmourTex
											,DWORD                dwSkillCode);
	virtual  void   OnMiss			(BOOL bCreateDamageMesh);
	virtual  void   OnFootStep		();
	virtual  void   OnShotStart	(); 
	virtual  void   OnShotFire		(int iShotCount); 
	virtual  void   OnShotEnd		(); 
	virtual  void   OnDead			();
	virtual  void   OnUseSkill		();



public:
	/////////////////////////////////////////////////
	///辅助信息
	virtual void    SetCondition				(BYTE byCondition);
	virtual float   GetRangeWithHero			();
	virtual float   GetArrowTargetHeight	();

	virtual void	Vibrate		(const VECTOR3D&	vDirection,	float fForce);
	virtual void	HitSpin		(float				fForce,		BOOL bRight);

	virtual BOOL	SendPacket	(void* pBuf
										,int   iLength
										,BOOL  bLocalPacket=FALSE);

public:
	/////////////////////////////////////////////////
	//目标对象操作
	virtual int     GetTargetingCount		();
	virtual void    AddTargetingCount		(DWORD dwTargeter);
	virtual void    RemoveTargetingCount	(DWORD dwTargeter);

	///取消移动目标锁定
	virtual void    ReleaseMoveTarget();
	virtual void	 LockOn				(DWORD dwObjectKey);
	virtual void	 LockOnPosition	(BOOL bFlag,Vector3D wzPos);
	virtual void	 LookLockedTarget	();

	//表情符号控制
	virtual void    ShowEmoteIcon(int iEmote,int iDelay);
	virtual void    ProcessEmoteDelay(DWORD dwTick);
	virtual void    FreeEmoteIcon();

	//人声停止
	virtual void    StopVoice();
	

public:
	///////////////////////////////////////////////////
	//技能相关
	virtual BOOL	 UseSkill					(SKILL_USE_PARAMETER *pSkill);
	virtual void    UpdateSkillAttributes			();


	///////////////////////////////////////////////////
	//技能限制检测
	virtual BOOL   CanUseSkill					(DWORD skillID
                                          ,BOOL  bShowFailMessage=FALSE)		;
	virtual BOOL	CanUseSkillLimitMP		(DWORD skillID
                                          ,BOOL  bShowFailMessage=FALSE)		;
	virtual BOOL	CanUseSkillLimitHP		(DWORD skillID
                                          ,BOOL  bShowFailMessage=FALSE)		;
	virtual BOOL	CanUseSkillLimitCoolTime(DWORD skillID
                                          ,BOOL  bShowFailMessage=FALSE)		;



public:
	virtual void SetVObject			(VObject* pVObject);
	
	////////////////////////////////////////////
	//更新信息
	virtual void AddUpdateFlag		(DWORD dwTypes);
	virtual void RemoveUpdateFlag	(DWORD dwTypes);
	virtual BOOL ProcessUpdateFlag(DWORD dwTick,DWORD dwFlags); 

protected:
	virtual BOOL _ShowAttackResult(ATTACK_RESULT*	pResult
											   ,BOOL					bRight =  FALSE
												,BOOL					bAddtionalEffect=TRUE);


	/*////////////////////////////////////////////////////////////////////////
	数据信息
	/*////////////////////////////////////////////////////////////////////////

protected:
	///////////////////////////////////////
	//脚本信息



	IQuestCharacter*			m_pQuestChar	;
	IScriptCharacter*			m_pScriptChar	;

	///////////////////////////////////////////////
	//辅助信息
	VG_DWORD_PROPERTY			(StartMoveTime);
	VG_INT_GET_PROPERTY		(Condition);
	DWORD							m_dwUpdateFlags;			/// 人物数据更新标志
	VG_BOOL_PROPERTY			(EnterWorld);				/// 是否进入游戏

	///////////////////////////////////////////////
	//基本信息
	VG_PTR_GET_PROPERTY		(VCharacter,	VCharacter);		//VObject指针
	VG_DWORD_PROPERTY			(TargetID);
	DWORD							m_dwHP;
	DWORD							m_dwMP;


	///////////////////////////////////////////////
	//目标信息
	VG_DWORD_PROPERTY			(MoveTarget);					///移动目标
	list<DWORD>					m_TargeterList;				///目标列表

	VG_DWORD_GET_PROPERTY	(LockedTargetID);			///锁定信息
	BOOL							m_bLockedPosition;
	Vector3D						m_vLockedPosition;	
	BOOL							m_bLocked;


	///////////////////////////////////////////////
	//跳跃信息
	VG_BOOL_PROPERTY			(Jumping);	
	VECTOR3D						m_vJumpFrom;
	VECTOR3D						m_vJumpTo;
	float							m_fJumpDistance;

	///////////////////////////////////////////////
	//人物普通攻击信息
	VG_FLOAT_PROPERTY			(AttackRange);	
	VG_TYPE_PROPERTY			(AttackSequence,eATTACK_SEQUENCE);	
	sTILE_POS						m_vAttackStartPos;
	sTILE_POS						m_vAttackEndPos;
	VG_BOOL_PROPERTY			(AttackMove);        
	VG_DWORD_GET_PROPERTY	(LatestAttack);  
	VG_DWORD_GET_PROPERTY	(LatestQueueAttack); 
	VG_DWORD_PROPERTY			(CurrentAttackSerial);		/// 适用Hero
	VG_DWORD_GET_PROPERTY	(NextAttackSerial); 
	DWORD                   m_dwNextActionAttackSerial;


	///////////////////////////////////////////////
	//人物连击信息
	VG_INT2_PROPERTY			(HitCount); 
	int							m_iHitEvent;
	BOOL							m_bRightHit;
	ATTACK_RESULT_LIST		m_AttackResultList;   
	VG_INT2_PROPERTY			(AttackStandTime);


	///////////////////////////////////////////////
	//人物状态
	VG_BOOL_PROPERTY			(KnockBack);				// 推倒信息
	
	VG_BOOL_PROPERTY			(ToGround);					// 倒地信息
	VG_BOOL_GET_PROPERTY		(ToAir);						// 漂浮信息
	TOAIR_INFO					m_AirInfo;

	Vector3D						m_vVibrateDirection;		//摇摆
	float							m_fVibrateForce;
	float							m_fSpinForce;				//旋转

	///////////////////////////////////////////////
	//人物移动
	VG_FLOAT_PROPERTY			(MoveSpeed);	
	VG_BYTE_PROPERTY			(MoveState);	
	sNEXTMOVEINFO				m_NextMoveInfo;		//寻路移动信息

	VG_DWORD_PROPERTY			(CurrentDelayCount);
	VG_DWORD_PROPERTY			(DelayTime);			//延迟动作时间

	PLAYER_ACTION_QUEUE		m_ActionQueue;			//动作队列
	VG_TYPE_PROPERTY			(CurrentAction	,PLAYER_ACTION);
	
	SKILL_EFFECT_LIST_IT		m_SkillEffectIter;	//人物状态
	SKILL_EFFECT_LIST       m_SkillEffectList;


	VG_INT2_PROPERTY			(FightingEnergy);		///能量存储
	float							m_fMeshHeight;

	int							m_iEmoteIcon;			// 表情符号
	int							m_iEmoteIconDelay;

	VG_INT2_PROPERTY			(VoiceHandle);		//人声信息
	int							m_iVoiceKind;
	VG_VECTOR_PROPERTY		(DestWarp,	Vector3D);
	VG_TYPE_PROPERTY			(Emotion,	IDTYPE);

public:
	float							m_fTurnAccelerate;


};//Character


#include "Character.inl"


};//namespace object


#endif //__CHARACTER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：VCharacterActionQueueDefine.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	动作队列定义


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTERACTIONQUEUEDEFINE_H__
#define __VCHARACTERACTIONQUEUEDEFINE_H__
#pragma once


#include "GameDefine.h"
#include "ActionQueueDefine.h"

//class WalkPath;
namespace vobject
{ 

class VCharacter;

	enum enumConst
	{
		ACTIONQUEUE_BUFFER_SIZE = 1024
	};
	enum enumAction
	{
		A_EXPRESSION,
		A_INTONATE,
		A_ATTACKEX,
		A_WALK,
		A_FLASHMOVE,
		A_TALKTONPC,
		A_DEATH,
		
		A_STARTHOLDSKILL,		// 开始持续技能				A_INTINATE_END
		//A_HOLDSKILLONEHIT,		// 持续技能的一次命中
		A_ENDHOLDSKILL,			// 结束持续技能
		A_CANCELSKILL,

		A_SEEHOLDSKILL			//看到持续技能

		,A_BEATTACKED			//被击动作
	};	

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	//struct SAttackInfoRecord
	//{
	//	DWORD dwState;		//技能附加的状态
	//	int	iBalanceType;	//结算参数
	//	int iTargetNumberHUD;     	//被攻击者显示的数字
	//	int	iTargetHP;			//被攻击者的HP
	//	int nTargetID;		//目标的ID
	//	//VCharacter* pDst;
	//};


	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	//struct _CLIENTGAMELIB_API sATTACK_INFO
	//{
	//	short stSrcX, stSrcY;//攻击者的位置
	//	int iSkill;			// 技能编号
	//	int	iSkillLevel;	//使用的技能的等级
	//	DWORD dwHoldTime;
	//	DWORD dwActionTime;
	//
	//	int	iAttackerHP;			//攻击者的HP
	//	int	iAttackerNumHUD;		//攻击者显示的数字
	//	int	attacketID;
	//	DWORD dwSerialNo;
	//
	//	bool bIsCharTarget;			//是否角色目标
	//
	//	int nTargetId;					//目标坐标
	//
	//	short stTargetX,stTargetY;
	//
	//	bool bNew;
	//	int nRecordCount;
	//	SAttackInfoRecord records[MAX_ATTACKINFO_RECORD];
	//
	//	enum eAtkHand
	//	{
	//		e_rhand = 0,
	//		e_lhand,
	//		e_max
	//	};
	//	
	//	sATTACK_INFO(){ Clear(); }
	//	~sATTACK_INFO(){  }
	//
	//	BOOL AddRecord( SAttackInfoRecord* pRecord )
	//	{
	//		if( nRecordCount >= MAX_ATTACKINFO_RECORD )
	//		{
	//			assert( false && "attack info add record failed, buffer is full" );
	//			return FALSE;
	//		}
	//		records[nRecordCount++] = *pRecord;
	//		return TRUE;
	//	}
	//	void Clear()
	//	{
	//		stSrcX = 0;
	//		stSrcY = 0;
	//
	//		iSkill = -1;
	//		iSkillLevel = 0;
	//
	//		dwHoldTime = 0;
	//		dwActionTime = 0;
	//
	//		iAttackerHP = 0;
	//		iAttackerNumHUD = 0;
	//
	//		attacketID = -1;
	//		dwSerialNo = 0;
	//
	//		bIsCharTarget = true;
	//		nTargetId = -1;
	//		stTargetX = -1;
	//		stTargetY = -1;
	//
	//		nRecordCount = 0;
	//		memset( records, 0x00, sizeof( records ) );
	//	}
	//};


	/*////////////////////////////////////////////////////////////////////////
	//吟唱结束后的施展法动作
	/*////////////////////////////////////////////////////////////////////////
	struct _CLIENTGAMELIB_API IntonateEndInfo
	{
		WORD			ustSkillId;
		WORD			ustSkillLevel;
		DWORD			dwEmissionFlyTime;

		GameCharID	ustTargetID;
		BOOL			bUseTargetDummy;
		Vector3D		vTargetPos;

	};

	/*////////////////////////////////////////////////////////////////////////
	//看到表情动作
	/*////////////////////////////////////////////////////////////////////////
	struct ExpressionInfo
	{
		DWORD dwSerialNo;
		//
		int iActionID;    //行为动作的时间
	};

	/*////////////////////////////////////////////////////////////////////////
	//吟唱需要的数据
	/*////////////////////////////////////////////////////////////////////////
	struct IntonateInfo
	{
		int			iSkillNo;
		int			iSkillLevel;
		DWORD			dwDelayTime;	//持续的时间
		VCharacter* pSrc;				//攻击者
		VCharacter* pDst;				//攻击目标
		DWORD			dwSerialNo;
		int			iPercent;		//当前吟唱进度
	};


	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	struct SSeeHoldSkill
	{
		VCharacter* pSrc;
		int			iSkillID;
		int			iSkillLevel;
		int			iPercent;
	};


	struct SAction
	{	
        //类型
		short stType;			
		BOOL bSkip;				// 是否已经失效,,当bSkip = TRUE表示该行为无效，或该行为为空的
									
		DWORD dwSerialNo;		// 指令序列编号，现在简单的用时间来标示
									// 后来的指令一定在数值上大于先前的指令
		DWORD dwPushTime;		//动作入列时间
		char	sData[ ACTIONQUEUE_BUFFER_SIZE ];
		SAction():	bSkip(FALSE){}
	};

	struct SAttack
	{
		short stSkill;
		short stDstID;
		short xDst,yDst;
		short stDamage;
	};

	struct SWalk
	{
		BYTE ucDir;
	};
	struct SFlashMove
	{
		int a;
	};
	struct STalkToNpc
	{
		short stNpcId;
	};
	struct SDeath
	{
		DWORD dwStartTime;
		BOOL bDeadOver;
	};
	struct SHoldSkillStart
	{
		DWORD dwTmp;
	};
	struct SHoldSkillEnd
	{
		DWORD dwTmp;
	};
	struct SHoldSkillOneHit
	{
		DWORD dwTmp;
	};

	struct SCancelSkill
	{
		DWORD dwTmp;
	};



};//namespace vobject





#endif //__VCHARACTERACTIONQUEUEDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：GameUIManager.h
创建日期：2007年4月3日
最后更新：2007年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEUIMANAGER_H__
#define __GAMEUIMANAGER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "GameUIManagerDefine.h"
#include "UIListener.h"
#include "ApplicationBaseListener.h"
#include "ConstArray.h"

class ToolTipManager;

class UIListener;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API GameUIManager: public ApplicationBaseListener
{
public:
	GameUIManager	();
	~GameUIManager	();

public:

public:
	virtual BOOL Init			()=0;
	virtual void Release		()=0;
	virtual BOOL FrameMove	(DWORD dwTick)=0;
	virtual BOOL Render		(DWORD dwTick)=0;

public:
	//从Listen中得来
	virtual void OnWinMsg		( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)=0;

public:
	///点击处理
	virtual void		SetMovePointer		(BOOL					bFlag)=0;
	virtual void		SetMovePointer		(BOOL					bFlag
                                       ,const VECTOR3D&	vPos)=0;
	virtual void		SetAreaPointer		(BOOL					bFlag)=0;
	virtual void		SetAreaPointer		(BOOL					bFlag
                                       ,const VECTOR3D&	vPos
													,WORD					wArea
													,COLOR				color=0xffffffff)=0;
	virtual void		SetAttackPointer	(BOOL					bFlag
                                       ,const VECTOR3D&	vPos)	=0;
	virtual void		ToggleShowName		()=0;

public:
	virtual void		AddUIListener				(gameui::UIListener*);
	virtual void		RemoveUIListener			(gameui::EGameUIType);

	virtual BOOL		InstallToolTipManager	(ToolTipManager* pManager);
	virtual BOOL		UninstallToolTipManager	(ToolTipManager* pManager);

public:
	//////////////////////////////////////
	//UIListener监听处理

	UILISTERN_DECLARE1(Refresh,				void		,BOOL				bExtra	= FALSE);
	UILISTERN_DECLARE4(DisplayInfo,			void		,LPCSTR			sText
																   ,const COLOR	col		= 0xffffffff 
																	,const UINT		infoType = gameui::CHANNEL_SYSTEM
																	,char *			szToName = NULL );

	UILISTERN_DECLARE2(DisplayTip,			void,		LPCSTR sText,DWORD dwData=0 );
	UILISTERN_DECLARE8(ShowByTime,			void,		DWORD dwStartTime, DWORD dwPeriod, BOOL bUp = true,
																	int nFlashCount = 0, BOOL bAutoClose = false,
																	BOOL bFadeIn = false, const char* pName = NULL,
																	BOOL bIsInterrupt = false);

	UILISTERN_DECLARE3(StartReadytoLaunch,	void,		DWORD dwStartTime, DWORD dwPeriod, short shStartPermillage);
	UILISTERN_DECLARE1(Cancel,					void,		DWORD dwStopTime=0 	);
	UILISTERN_DECLARE (CountDown,				void);
	UILISTERN_DECLARE1(ChangeShopState,		void,		BOOL bOpen 	);

	UILISTERN_DECLARE1(OnMessage,				void,		MSG_BASE* pMsg);
	UILISTERN_DECLARE1(OnWorldMessage,		void,		MSG_BASE* pMsg);
	UILISTERN_DECLARE4(OnMouseRButton,		void,		vobject::VObject* pPlayer, int x, int y, BOOL bPress);
	UILISTERN_DECLARE4(OnMouseLButton,		void,		vobject::VObject* pPlayer, int x, int y, BOOL bPress);
	UILISTERN_DECLARE7(ShowMessageBox,		void		,LPCSTR	szText
																	,LPCSTR	szCaption = ""
																	,int		nBoxType = MB_OK
																	,BOOL		bModal = true
																	,void*	pCallback = NULL
																	,void *	pData = NULL
																	,int		nDataLength = 0 );
	UILISTERN_DECLARE7(ShowInputBox,			void		,LPCSTR            szCaption
																	,BOOL              bNumber=TRUE
																	,BOOL              bModal=TRUE
																	,void*				 procCallback=NULL
																	,void*             pData=NULL
																	,int               nDataLength=0
																	,BOOL              bPassword=FALSE);
																	
	UILISTERN_DECLARE2(SetMoveText,			void,		LPCSTR szText, int nCount );
	UILISTERN_DECLARE2(SetData,				void,		DWORD dwType,LPARAM lpData);
	UILISTERN_DECLARE2(GetData,				BOOL,		DWORD dwType,void* pRet);
	UILISTERN_DECLARE1(LoadUI,					void,		gameui::EGameUIType	nSubType=gameui::eUINull);
	UILISTERN_DECLARE1(UnloadUI,				void,		gameui::EGameUIType	nSubType=gameui::eUINull);
	UILISTERN_DECLARE1(OnStartUp,				void,		gameui::EGameUIType	nSubType=gameui::eUINull);
	UILISTERN_DECLARE1(OnProcess,				void,		gameui::EGameUIType	nSubType=gameui::eUINull);
	UILISTERN_DECLARE2(FrameMove,				void,		gameui::EGameUIType	nSubType,DWORD dwTick);
	UILISTERN_DECLARE1(OnClose,				void,		gameui::EGameUIType	nSubType=gameui::eUINull);
	UILISTERN_DECLARE (OnEnterWorld,			void);
	UILISTERN_DECLARE2(TriggerFunc,			void,		gameui::ETriggerData eData,LPARAM lpData=0);
	UILISTERN_DECLARE1(OnDataInput,			void,		void* pData);
	UILISTERN_DECLARE8(ShowSelectBox,		void,		LPCSTR szText, void* pItemList, int nItemCount,
		 															LPCSTR szCaption = "", BOOL bModal = true,
																	void* funCallbackFun = NULL, void *pData = NULL, 
																	int nDataLength = 0 );
	UILISTERN_DECLARE1(UpdateData,			void,		BOOL bSet);
	UILISTERN_DECLARE (IsEditInputFocus,	BOOL);

public:
	////////////////////////////////////////////////////////////
	//
	void UIShowMessageBox	(DWORD      dwTextID
                           ,DWORD      dwCaptionID
									,const int  nType			=MB_OK
									,const BOOL bModal		=true
									,void*      pFun			=NULL
									,void*      pData			=NULL
									,const int  nDataLength	=0);
	void UIShowMessageBox	(LPCTSTR		szText
                           ,LPCTSTR    szCaption
									,const int  nType			=MB_OK
									,const BOOL bModal		=true
									,void*      pFun			=NULL
									,void*      pData			=NULL
									,const int  nDataLength	=0);

	void UIMessageBox			(LPCSTR	sText, DWORD dwShowTime = INVALID_DWORD_ID);
	void UIMessageBox			(DWORD	dwID,  DWORD dwShowTime = INVALID_DWORD_ID);
	void UIMessageBoxF		(DWORD	dwID,...);

	void UIShowInputBox		(LPCSTR            szCaption
									,BOOL              bNumber=TRUE
									,BOOL              bModal=TRUE
									,void*				 procCallback=NULL
									,void*             pData=NULL
									,int               nDataLength=0
									,BOOL              bPassword=FALSE);
	void UIShowInputBox		(DWORD				 dwCaptionID
									,BOOL              bNumber=TRUE
									,BOOL              bModal=TRUE
									,void*				 procCallback=NULL
									,void*             pData=NULL
									,int               nDataLength=0
									,BOOL              bPassword=FALSE);


	void UIDisplayChatInfo	(DWORD				dwID
                           ,const COLOR		col		=0xffffffff
									,const UINT			infoType	=gameui::CHANNEL_SYSTEM
									,char*				szToName	=NULL);
	void UIDisplayChatInfo	(LPCSTR				sText
                           ,const COLOR		col		=0xffffffff
									,const UINT			infoType	=gameui::CHANNEL_SYSTEM
									,char*				szToName	=NULL);

	void UISetVisible			(gameui::EGameUIType nType,	BOOL bSet);
	BOOL UIIsVisible			(gameui::EGameUIType nType);
	void UISetEnable			(gameui::EGameUIType nType,	BOOL bSet);
	BOOL UIIsEnable			(gameui::EGameUIType nType);

public:
	BOOL    Sys_VerifyLockF(DWORD dwID, BOOL bShutdown,...);


	//////////////////////////////////////////////////////////
	//基本信息
protected:
	gameui::UIListener*		m_arListeners[gameui::eUITopMax];
	VG_PTR_GET_PROPERTY		(ToolTipManager, ToolTipManager);

#ifdef USE_LIB
	Vector3D						m_vMovePointerPos;
	BOOL							m_bMovePointerFlag;

	BOOL							m_bAreaPointerFlag;
	Vector3D						m_vAreaPointerPos;

	BOOL							m_bAttackPointerFlag;
	Vector3D						m_vAttackPointerPos;
#endif

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(GameUIManager , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API GameUIManager& singleton::theGameUIManager;
#define theGameUIManager  singleton::GetGameUIManager()



////////////////////////////////////////////////////////////////////////
CONSTARRAY_DECLARE	(GameUIType
                     ,DWORD
							,0
							,gameui::eUIMax
							,_CLIENTGAMELIB_API);

////////////////////////////////////////////////////////////////////////
#include "GameUIManager.inl"



#endif //__GAMEUIMANAGER_H__
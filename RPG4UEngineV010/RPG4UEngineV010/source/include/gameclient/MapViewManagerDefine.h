/*////////////////////////////////////////////////////////////////////////
文 件 名：MapViewManagerDefine.h
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPVIEWMANAGERDEFINE_H__
#define __MAPVIEWMANAGERDEFINE_H__
#pragma once


#define	MAX_MAP_COUNTS	18
#define	MINIMAP_WIDTH	128
#define	MINIMAP_HEIGHT 128

namespace mapinfo
{ 
	const		DWORD			ICON_RENDER_SIZE	= 16;
	const		DWORD			ICON_REAL_SIZE		= 6;
	const		DWORD			MAPVIEW_SIZE		= 512;
	const		DWORD			OBJECT_MAX			= 100;

	enum	{		MAPMODE_MINI,	MAPMODE_FULL	};

	///////////////////////////////////////////////
	enum
	{
		 MAPICON_PLAYER
		,MAPICON_NPC
		,MAPICON_NPCLOCK
		,MAPICON_MONSTER
		,MAPICON_TEAMMATE
		
		,MAPICON_RESTORE				// 药品商
		,MAPICON_WEAPON				// 武器店
		,MAPICON_SHIELD				// 盾牌店
		,MAPICON_DRESS					// 服装店
		,MAPICON_MATERIAL				// 材料商
		,MAPICON_TRANSPORT			// 传送点
		,MAPICON_RECORD				// 记录点
		,MAPICON_STORAGE				// 仓库
		,MAPICON_GUIDEPOST			// 路牌
		,MAPICON_ITEMMAKE				// 合成精炼强化NPC
		,MAPICON_NPCDIRECT			// Npc 指引

		,MAPICON_YELLOWGANTANHAO
		,MAPICON_WHITEGANTANHAO
		,MAPICON_YELLOWWENHAO
		,MAPICON_WHITEWENHAO


		,MAPICON_HEROARROW
		,MAPICON_NPCPOSMARK

		,MAPICON_HERO
		,MAPICON_MAX            
	};


	enum	{		MAX_MAP_MARK = 32,	MAX_MAP_MARKTEXT = 32	};

	///////////////////////////////////
	struct sMAP_ICONINFO
	{
		short nTextureID;
		int	nWidth;
		int	nHeight;

		sMAP_ICONINFO()
		{
			nTextureID = -1;
			nWidth = 0;
			nHeight = 0;
		}
	};


	///////////////////////////////////
	enum eMAP_MARK_TYPE
	{
		 MAPMARK_NONE			// 无用
		,MAPMARK_NPCPOS		// 提示npc的位置
		,MAPMARK_HELPER
	};

	///////////////////////////////////
	// 地图标记
	struct sMAP_MARK
	{
		int		nType;				// 类型
		DWORD		dwBornTime;			// 出生时间，单位毫秒
		DWORD		dwLife;				// 生命，单位毫秒
		BOOL		bFlicker;			// 是否闪烁
		int		x, y;					// 在地图上的位置，单位网格，不是像素
		char		szText[MAX_MAP_MARKTEXT];

		sMAP_MARK()
		{
			nType			= MAPMARK_NONE;
			dwBornTime	= 0;
			dwLife		= 0;
			bFlicker		= FALSE;
			x				= 0;
			y				= 0;
		}
	};


	///////////////////////////////////
	enum
	{
		 MAPBTN_CLOSE
		,MAPBTN_BACK
		,MAPBTN_MAX
	};

	///////////////////////////////////
	struct sMAPBTN_INFO
	{
		INT	m_Normal;
		INT	m_MouseOver;
		RECT	m_HotArear;
	};

	///////////////////////////////////
	struct sMAPBTN_SYS : public sMAPBTN_INFO
	{
		INT m_MousePress;
	};


};//namespace mapinfo


struct sNPC_COORDINFO
{
	VRSTR			m_Name;
	DWORD			m_NpcID;
	float			m_MapX;
	float			m_MapY;
	RECT			m_rcIcon;
	DWORD			m_NpcFuncID;
};




#endif //__MAPVIEWMANAGERDEFINE_H__
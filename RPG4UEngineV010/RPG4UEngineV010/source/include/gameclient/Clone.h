/*////////////////////////////////////////////////////////////////////////
文 件 名：Clone.h
创建日期：2008年3月28日
最后更新：2008年3月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CLONE_H__
#define __CLONE_H__
#pragma once

#include "Character.h"
#include "sceneStruct.h"
#include "ObjectFactoryManager.h"

_NAMESPACEU(VClone, vobject);
namespace object
{ 

class _CLIENTGAMELIB_API Clone :public Character
{
public:
	Clone(void);
	~Clone(void);

public:
	virtual void	Init();
	virtual void	Destroy			();
	virtual void	CopyAppearance	(Character *pChar);
	virtual BOOL	Process			(DWORD dwTick);


public:
	virtual void	ResetCurDelay	();
	virtual BOOL	OffsetPosition		(const Vector3D &wvPos);
	virtual BOOL	InitPosition		(const Vector3D &wvPos);

	virtual BOOL	Move					(Vector3D vCurPos,Vector3D vDestPos);
	virtual void	SetDisappear		(DWORD dwTime);
	virtual void	SetAppear			(DWORD dwTime,BYTE byStartAlpha = 0);
	virtual void	SetScaleAnimation	(DWORD dwTime,float fDestScale);
	virtual void	SetScale				(float fScale);
	virtual void	SetPlayAni			(BOOL isAni);
	virtual void	SetRefraction		(DWORD dwTime);
	virtual void	Vibrate				(const VECTOR3D& vDirection,float fForce);
	virtual void	AttachToObject		(DWORD dwObjectKey);


public:
	virtual void	SetVObject(VObject* pVObject);
	virtual IDTYPE	GetCurrentIdleAnimation(){return 0;};
	virtual IDTYPE	GetCurrentMoveAnimation(){return 0;};
	virtual IDTYPE	GetCurrentAttackAnimation(){return 0;};
	virtual IDTYPE	GetCurrentDeathAnimation(){return 0;};
	virtual IDTYPE	GetCurrentDamageAnimation(BOOL /*bRight*/) {return 0;};

	virtual void		SetExp(DWORD /*dwExp*/) {};
	virtual void		SetLevel(LEVELTYPE /*LV*/)	{};

	virtual LEVELTYPE	GetLevel()	const{return 0;}
	virtual DWORD		GetExp()		const{return 0;}
	virtual DWORD		GetMaxHP()	const{return 0;}
	virtual DWORD		GetMaxMP()	const{return 0;}
	virtual DWORD		GetNextExp()const{return 0;}

protected:
	VClone*		m_pVClone;

	VG_VECTOR_PROPERTY(CurPos, Vector3D);


	Vector3D		m_vStart;
	Vector3D		m_vEnd;
	Vector3D		m_vDir;
	Vector3D    m_vVibStartPos;
	BOOL			m_bMove;
	BOOL        m_IsAni;
	BOOL			m_bDisappear;
	DWORD       m_dwAttachObject;

	Vector3D		m_vVibrateDirection;
	float			m_fVibrateForce;

	BOOL        m_bRefraction;
	DWORD			m_dwRefractionCount;
	DWORD			m_dwRefractionTime;
	BYTE			m_byRefractionAlpha;

	BOOL			m_bAppear;
	DWORD			m_dwAppearCount;
	DWORD			m_dwAppearTime;
	BYTE			m_byAppearAlpha;
	BYTE			m_byAppearStartAlpha;



	float					m_fDistance;
	VG_FLOAT_PROPERTY	(MoveSpeed);
	float					m_fMoved;
	float					m_fRefractionRate;
	float					m_fRefractionRateAlpha;
	VG_DWORD_PROPERTY	(Delay);
	DWORD					m_dwCurDelay;
	DWORD					m_dwDelayDisappear;

	BOOL        m_bScaleAnim;
	DWORD       m_dwScaleTime;
	DWORD       m_dwScaleCount;
	float       m_fDestScale;
	float       m_fCurScale;
	float       m_fStartScale;

	//VG_TYPE_PROPERTY(AlphaBlend, scene::ENUM_ALPHABLEND);
	VG_TYPE_PROPERTY(Color,			COLOR);

	VG_BOOL_PROPERTY	(Permanant);
};//Clone

OBJECT_FACTORY_DECLARE(Clone);

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

inline void Clone::SetPlayAni(BOOL isAni)
{
    m_IsAni=isAni;
}

};//namespace object



#endif //__CLONE_H__
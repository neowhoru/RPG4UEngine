/*////////////////////////////////////////////////////////////////////////
文 件 名：AttackInfoManager.cpp
创建日期：2008年1月15日
最后更新：2008年1月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "AttackInfoManager.h"
#include "VObject.h"
#include "VObjectManager.h"
#include "GlobalTexture.h"



using namespace vobject;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(AttackInfoManager, ()  , gamemain::ePriorityNormal/*eInstPrioAttackInfoManager*/);




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
AttackInfoManager::AttackInfoManager(void)
:m_map(TRUE,CBRemoveAttackInfo)
{
	m_pAttackInfoPool = new AttackInfoPool;
}

AttackInfoManager::~AttackInfoManager( void )
{
	SAFE_DELETE(m_pAttackInfoPool);
}

BOOL AttackInfoManager::Init(UINT uPoolSize)
{
	__BOOL_CALL( m_pAttackInfoPool->Initialize(uPoolSize) );
	return TRUE;
}

void AttackInfoManager::Release()
{
	m_map.GetObjects( NULL );
	ClearAll();
	m_pAttackInfoPool->Release();
}

void AttackInfoManager::CBRemoveAttackInfo(sATTACK_INFO** pAttackInfo)
{
	theAttackInfoManager.FreeAttackInfo	(*pAttackInfo);
}

sATTACK_INFO* AttackInfoManager::AllocAttackInfo( )
{
	return m_pAttackInfoPool->Alloc();
}

void AttackInfoManager::FreeAttackInfo	(sATTACK_INFO* pAttackInfo)
{
	if(!pAttackInfo)
		return;
	m_pAttackInfoPool->Free(pAttackInfo);
}


sATTACK_INFO* AttackInfoManager::AddAttackInfo( sATTACK_INFO &attackinfo )
{
	VObject* pSrcObject	= theVObjectManager.FindByID( attackinfo.attacketID );
	VObject* pTarget		= theVObjectManager.FindByID( attackinfo.nTargetId );

	if( !pSrcObject || !pTarget )
		return NULL;

	sATTACK_INFO *pAttackInfo = AllocAttackInfo();
	if(pAttackInfo == NULL)
		return NULL;

	*pAttackInfo		= attackinfo;
	pAttackInfo->bNew = true;


	m_map.Add( pAttackInfo, pAttackInfo );
	return pAttackInfo;
}

void AttackInfoManager::PushAttackRecord( sATTACK_INFO*  pAttackInfo, SAttackInfoRecord*	rec)
{
	pAttackInfo->AddRecord(rec);
}


//传入的是AddAttackInfo返回的
void AttackInfoManager::DeleteAttackInfo( sATTACK_INFO *pAttackInfo )
{
	sATTACK_INFO* found = m_map.Find( pAttackInfo );
	if( !found )
	{
		assert( 0 );
		return;
	}

	//VObject* pSrcObject	= theVObjectManager.FindByID( pAttackInfo->attacketID );
	//VObject* pTarget		= theVObjectManager.FindByID( pAttackInfo->nTargetId);


	assert( pAttackInfo->bNew );
	pAttackInfo->bNew = false;

	m_map.Remove( pAttackInfo );

}

//玩家切换地图时调用执行保存的所有AttackInfo
void AttackInfoManager::ExcuteAllAttackInfo( void )
{
	std::vector<sATTACK_INFO*> vectorInfo;
	m_map.GetObjects( &vectorInfo );

	for( UINT i = 0; i < vectorInfo.size(); i++ )
	{
		sATTACK_INFO *pAttackInfo = vectorInfo[i];
		if( pAttackInfo )
		{
			VObject* pSrcObject = theVObjectManager.FindByID( pAttackInfo->attacketID );
			if( pSrcObject )
				pSrcObject->OnTriggerPoint(TRUE, eTRIGGERPT_NORMAL,pAttackInfo );
		}
	}
}

void AttackInfoManager::ClearAll( void )
{
	m_map.Destroy();
}
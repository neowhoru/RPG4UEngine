/*////////////////////////////////////////////////////////////////////////
文 件 名：MapViewSetting.h
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPVIEWSETTING_H__
#define __MAPVIEWSETTING_H__
#pragma once


#include "ImageTGA.h"
#include "GameDefine.h"
//#include "NpcCoordinateManager.h"
#include "XmlIOItem.h"

#define	MAX_MAP_COUNTS	18


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API MapViewSetting :public IXmlIOItem
{
public:
	MapViewSetting()
		:IXmlIOItem("MiniMapSetting")
	{
		m_nRealWidth	=	0;
		m_nRealHeight	=	0;
		m_nMaxMapWidth	=	0;
		m_nMaxMapHeight	=	0;
		m_nBeignPixelX	=	0;
		m_nBeignPixelY	=	0;
		m_nEndPixelX	=	0;
		m_nEndPixelY	=	0;
		m_nMaxMapCounts		=	0;

		m_SysBtnPos.x	= 926;
		m_SysBtnPos.y	= 22;
		m_SysBtnSize.x	= 64;
		m_SysBtnSize.y	= 64;
		m_SysBtnSpace	= 30;

		::ZeroMemory(m_ptBtnPos,sizeof(POINT)*MAX_MAP_COUNTS);
	}
	~MapViewSetting()
	{

	}
public:
	virtual bool loadXMLSettings(const char *path  );
	virtual bool loadXMLSettings(XMLElement *element);

	virtual bool exportXMLSettings(std::ofstream &xmlFile  );
	virtual bool exportXMLSettings(const char* xmlPath);
public:
	int		m_nRealWidth;
	int		m_nRealHeight;
	int		m_nMaxMapWidth;
	int		m_nMaxMapHeight;

	int		m_nMaxMapWidthLvl1;
	int		m_nMaxMapHeightLvl1;

	int		m_nBeignPixelX;
	int		m_nBeignPixelY;
	int		m_nEndPixelX;
	int		m_nEndPixelY;
//sbw_add
	int		m_nMaxMapCounts;
	POINT		m_ptBtnPos[MAX_MAP_COUNTS];
	POINT		m_SysBtnPos;
	POINT		m_SysBtnSize;
	INT		m_SysBtnSpace;
private:
};



#endif //__MAPVIEWSETTING_H__
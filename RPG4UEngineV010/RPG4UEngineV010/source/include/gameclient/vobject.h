/*////////////////////////////////////////////////////////////////////////
文 件 名：VObject.h
创建日期：2007年4月15日
最后更新：2007年4月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	基本对象数据：
		ID、类型
		位置信息
		对象构建
		外形参数
		血量提示
		模型


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VOBJECT_H__
#define __VOBJECT_H__
#pragma once

#include "CommonDefine.h"
#include "ISkillSpecialObject.h"
#include "VObjectFactoryManager.h"
#include "VObjectDefine.h"
#include "ObjectUtil.h"
#include "ConstMesh.h"
#include "GameDefine.h"
//#include "PathHandlerListener.h"

class PathGameMap;
class V3DSpaceBox;
class VUCtrlText;
class VUCtrlPicture;
struct sATTACK_INFO;

_NAMESPACE2(SItemWeapon,	giteminfo);
_NAMESPACEU(Object,			object);
_NAMESPACEU(PathHandler,	tile);


namespace vobject
{
class VCharacterAnimationCtrl;
class VObjectPathHandlerListener;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API VObject :  public ISkillSpecialObject
										//,  public tile::PathHandlerListener
{
public:
	friend class VObjectRenderInstance;
	VObject();
	~VObject();

public:
	///////////////////////////////////////////
	//VObject类型
	#define VOBJ_CHECKTYPE(Name,Type)	inline BOOL	Is##Name(){ return IsKindOf(Type##_OBJECT); }

	inline BOOL IsKindOf(eOBJECT_TYPE type){return BOOL((DWORD)(m_dwObjectType & type) == (DWORD)type);}

	VOBJ_CHECKTYPE(Character,	CHARACTER); 
	VOBJ_CHECKTYPE(Npc,			NPC); 
	VOBJ_CHECKTYPE(Item,			ITEM); 
	VOBJ_CHECKTYPE(Money,		MONEY); 
	VOBJ_CHECKTYPE(Player,		PLAYER); 
	VOBJ_CHECKTYPE(Hero,			HERO); 
	VOBJ_CHECKTYPE(MapNpc,		MAPNPC); 
	VOBJ_CHECKTYPE(Monster,		MONSTER); 
	VOBJ_CHECKTYPE(Effect,		EFFECT); 
	VOBJ_CHECKTYPE(Building,	BUILDING); 
	VOBJ_CHECKTYPE(MapObject,	MAP); 
	VOBJ_CHECKTYPE(Collection,	COLLECTION); 


public:
	///////////////////////////////////////////
	//基本操作
	DWORD GetFactoryType();

	virtual void	Reset();
	virtual BOOL	LoadMesh();
	virtual void	UnloadMesh();

	virtual BOOL	OnMeshLoading();
	virtual void	OnMeshUnloading();

	virtual BOOL	RequestWorkingData();
	virtual void	ReleaseWorkingData();
	virtual VOID	Reuse		();				///主要由对象工厂在对象撤消时调用
	virtual void	Remove	( DWORD dwStartRemoveTime );

public:
	virtual void	Update			( DWORD dwTick );
	virtual void   UpdateManual	( DWORD dwTime );
	virtual BOOL	RenderInfo		( DWORD dwTick, DWORD dwColor = 0xffffffff, float fTransparent = 1.0f, float fPlayerTransparent = 1.0f );
	virtual BOOL	RenderMesh		( DWORD dwTick );

	virtual BOOL	CanRotateAnim			();
	virtual DWORD	GetTriggerPointType	();
	virtual void	GetShowTextInfo		(StringHandle&	sText);
	virtual float  GetGrowSize				();
	virtual BOOL	IsItemDropDelayOver	(void);

public:
	//PathHandlerListener 虚函数
	virtual void OnStop			();
	virtual void OnReset			();
	virtual void OnReadyToMove	(BOOL bThrust);

protected:
	virtual void	_PrevRenderMesh	( DWORD /*dwTick*/,LIGHT_INFO& /*light*/ ){}
	virtual void	_OnRenderMesh		( DWORD dwTick,LIGHT_INFO& light );
	virtual void	_OnDrawName			(DWORD dwTick
                                    ,INT&    x
												,INT&    y
												,LPCTSTR szText
												,COLOR   color);
	virtual void   _UpdateHUD			( DWORD dwTick);
	virtual void   _UpdateAlpha		( DWORD dwTick);

	virtual void   _UpdateAnim			(DWORD dwTime);
	virtual void   _UpdateRotation	(DWORD dwTick);
	virtual void   _DoRotation			(float fRot);
	virtual void   _UpdateGrowSize	(DWORD dwTick);
	virtual void   _UpdatePosZ			(DWORD dwTick);
	virtual float  _GetUpdatePosZ		(FLOAT fRoleHeight);

public:
	virtual V3DVector			GetBBoxMin();
	virtual V3DVector			GetBBoxMax();
	virtual BoundingVolume	GetBoundingVolume();

	virtual V3DSpaceBox*			GetBBox			();
	virtual float				GetMeshHeight	(float fExtra=0.f);

	//外观
	virtual BOOL	ShowPopupText( int nBalanceType, int nNum );
	virtual BOOL	ShowPopupText( VObject *pTagObject, VObject *pSrcObject, int iBalanceType, int iTargetNumberHUD, int iScNum );

	virtual BOOL	CanShowPopupText	(){return TRUE;}
	virtual BOOL   CanCollection		(){return FALSE;}
	virtual BOOL   IsInHeroRange		(float fRadius);
	virtual BOOL	IsShowName			();
	virtual COLOR	GetNameColor		();

public:
	//特效控制虚函数 
	virtual float			GetEffectDir		(){return m_fCurrentDir;}
	virtual VECTOR3D		GetEffectPos		(){return GetPosition();}
	virtual BOOL			OnTriggerPoint		(BOOL          bLastTriggerPoint
														,int           nTriggerPointType
														,sATTACK_INFO* pAttackInfo);
	virtual BOOL			OnAnimationEnd		();

	virtual VECTOR3D		GetBonePosAtWorld	(INT nBone);

public:
	virtual 	void	PlayAnimation	(LPCSTR szName
									,int    nTimes			=MAX_ANIMATION_LOOP
									,LPCSTR szEndName		=NULL);

	virtual 	void	PlayAnimation	(DWORD dwActionID
									,int   nTimes			=MAX_ANIMATION_LOOP
									,DWORD dwEndActionID	=-1);

public:
	/////////////////////////////////////////////////////
	//位置信息
	virtual BOOL ChangeMapMask	( short x,short y );
	virtual void ClearMapMask	(void);

   virtual void	ChangeTilePos	(int x, int y);
	virtual void	UpdatePos		(const VECTOR3D& vPos );

	virtual void	SetTilePos		( int x, int y, PathGameMap* pMap );
	BOOL				EqualTilePos	( int x, int y );


	virtual void	SyncTilePos		(int x
                                 ,int y)		;
	virtual void	JumpToTilePos	(int      x
                                 ,int      y
											,PathGameMap* pMap);
	virtual void	JumpToTilePos	(const VECTOR3D& vDest
                                 ,PathGameMap*        pMap)		;

	///////////////////////////////////////////
	//方向信息操作
	virtual void	SetDirection	( float		dir ,BOOL bSetTarget = TRUE);
	virtual void	SetDirection	( PATH_DIR	dir);

	virtual void	SetDirectionUpper	( float dir , BOOL bSetTarget = TRUE );
	virtual void	SetDirectionLower	( float dir , BOOL bSetTarget = TRUE );
	virtual void	RotateTo				( float dir );


	//转身操作
	virtual void		SetDirection	(const VECTOR3D& vTarget
												,VECTOR3D*       pwvPos=NULL
												,BOOL            bVisibleAngle=FALSE); 
	virtual void		SetDirection	(VObject* pFrom, VObject* pTo,BOOL bVisibleAngle = FALSE); 
	virtual Vector3D	GetDirection	( void );

	virtual void		SetAngle			(float fAngle,BOOL bSetVisibleAngle = FALSE);
	virtual void		Rotate			(float fAngle,BOOL bSetVisibleAngle = FALSE);
	virtual void		SetAlpha			(float fAlpha,BOOL bNoFadeIn=FALSE);
	virtual void		SetVisible		(BOOL bSet,BOOL bNoFadeIn=FALSE);
	virtual BOOL		IsVisible		(BOOL bNoFadeIn=FALSE);

public:
	virtual void		DisplayChat		(LPCSTR     szText
                                    ,DWORD		dwExpressionID =0)		;

	virtual BOOL		InitSericeIcon	(DWORD attackKind);

public:
	virtual void		ModShowFlag		(DWORD dwAdd, DWORD dwRemove);

	///////////////////////////////////////////
	//从属关系
	virtual void	SetMasterID( GameCharID MasterID, BOOL bMasterIsMe );
	virtual void	ReleaseMaster();
	BOOL				HaveMaster()	;
	BOOL				IsMaster()		;

	virtual void	UpdateDistanceToHero	(const VECTOR3D& vPos );
	virtual void	OnStepHeroRange		(BOOL bIn);
	virtual BOOL	CanHideOutofHeroRange(float fAlphaGap=0.01f);

	virtual BOOL	CanCastRayTo			(Vector3C& vTarget);


	virtual void	OnCreate			(Object*			pObject);
	virtual void	SetObject		(Object*			pObject);
	virtual void	SetPathHandler	(PathHandler*	pHandler);

protected:
	/////////////////////////////////////////////////
	// 头顶上的对话框
	VG_PTR_GET_PROPERTY	(SeriesIcon,	VUCtrlPicture	);	
	VG_PTR_GET_PROPERTY	(ChatDialog,	VUCtrlText		);	
	VG_DWORD_PROPERTY		(ChatDialogStartTime	);	
	VG_DWORD_PROPERTY		(ExpressionID	);	

	/////////////////////////////////////////////////
	// 寻路信息
	VObjectPathHandlerListener*			m_pListener;

	VG_PTR_GET_PROPERTY	(Object,			Object);			//逻辑对象
	VG_PTR_GET_PROPERTY	(PathHandler,	PathHandler);	//寻路句柄...

	VG_TYPE_PROPERTY	(ID,			VOBJID);	//对象编号，服务端同步
	VG_TYPE_PROPERTY	(ClientID,	VOBJID);	//对象编号，客户端方面
	VG_CSTR_PROPERTY	(Name,		MAX_NAME_LENGTH);			//名称 
	VG_INT_PROPERTY	(Level);

	VG_FLOAT_PROPERTY	(BodySize);	//体型修正
	VG_INT_PROPERTY	(ModelId);
	VG_INT_PROPERTY	(ModelColor); //ColorID

	VG_DWORD_PROPERTY	(ObjectType);		//对象类型
	VG_DWORD_PROPERTY	(ShowFlag);			//显示类型
	VG_BOOL_PROPERTY	(Mask);				//对象位置阻挡

	VG_BOOL_GET_PROPERTY		(NeedToDelete);	//需要删除
	VG_BOOL_PROPERTY			(NeedToRemove);		//是否要移除
	VG_DWORD_GET_PROPERTY	(StartRemoveTime);	//开始准备移除的时间

	//辅助
	float							m_fGrowSize;;
	VG_BOOL_PROPERTY			(Dead);				//死亡标志
	VG_BOOL_PROPERTY			(DeadOver);			//死亡标志
	VG_DWORD_PROPERTY			(DeadOverTime);
	VG_BOOL_PROPERTY			(Active);	
	VG_BOOL_PROPERTY			(WillDie);				//将死亡标志


	VG_BOOL_PROPERTY			(NeedRender);
	VG_DWORD_PROPERTY			(DisplayAfterTime);

	VG_FLOAT_GET_PROPERTY	(DistanceToRole);	//和主角的距离
	VG_BOOL_GET_PROPERTY		(InWater);			//是否在水中
	VG_BOOL_PROPERTY			(HideProwl);		//是否在潜行状态
	BOOL							m_bStepInHeroRange;


	sMAP_MASK					m_MapMask;

	//模型数据
	VG_PTR_PROPERTY			(Anim,		VCharacterAnimationCtrl);	//人物模型
	VG_FLOAT_PROPERTY			(CurRenderAlpha);					//当前渲染透明度
	VG_FLOAT_PROPERTY			(FinalRenderAlpha);				//最终渲染透明度
	VG_FLOAT_PROPERTY 		(DefaultFinalRenderAlpha);		//默认渲染透明度
	VG_FLOAT_GET_PROPERTY 	(BakFinalRenderAlpha);
	VG_FLOAT_PROPERTY 		(AlphaOrgin);						//初始Alpha值，默认为1.0 通用由appearanceManager设定



	VG_BOOL_PROPERTY		(Invisible);			//隐形
	VG_BOOL_PROPERTY		(TwoHalfBody);			//是否上下半身怪物
	VG_BOOL_PROPERTY		(FourFoot);				//是否四足怪物
	VG_BOOL_GET_PROPERTY	(Jumping);				// 开始起跳

	//从属关系
	VG_TYPE_GET_PROPERTY		(MasterID,	VOBJID);
	BOOL							m_bMaster;


	///////////////////////////////////////////////////
	//位置	
	VG_TYPE_PROPERTY			(Transform,	Matrix4);			//动作

	VG_TYPE_PROPERTY			(TilePos,	POINT);
	VG_VECTOR_PROPERTY		(Position,	VECTOR3D);				//动作
	VG_BOOL_GET_PROPERTY		(NeedUpdateZ);	
	FLOAT							m_fZBackup;

	VG_FLOAT_GET_PROPERTY	(TargetDir);			///
	VG_FLOAT_GET_PROPERTY	(CurrentDir);
	VG_TYPE_GET_PROPERTY		(PathDir, PATH_DIR);

	///////////////////////////////////////
	BOOL							m_bRotateLowerBody;
	BOOL							m_bRotateLowerBodyStart;
	DWORD							m_dwRotateLowerBodyStartTime;
									
	float							m_fUpperBodyDir,
									m_fLowerBodyDir,
									m_fTargetUpperBodyDir,
									m_fTargetLowerBodyDir;
	float							m_fDirFinal;	//最终转向弧度
	BOOL							m_bRotating;
									
	float							m_fTerrainZ;	// VObject所在的地面高度
	VG_BOOL_PROPERTY			(InFloor);
	PathGameMap*				m_pPathMap;
	VG_FLOAT_GET_PROPERTY	(Angle);				//Z轴角度
	float							m_fVisibleAngleZ;	//Z轴角度
	float							m_fScale;			//
	DWORD							m_dwShowNameFlag;
									
protected:						
	static DWORD				ms_dwNullModelID;

};


};//namespace vobject

#include "VObject.inl"

#endif //__VOBJECT_H__
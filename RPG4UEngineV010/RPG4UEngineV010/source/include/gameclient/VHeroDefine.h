/*////////////////////////////////////////////////////////////////////////
文 件 名：VHeroDefine.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __VHERODEFINE_H__
#define __VHERODEFINE_H__
#pragma once

namespace vobject
{ 
	class VHero;
	typedef BOOL (VHero::*FuncOperation)(void);
	struct VHeroFuncOperation
	{
		FuncOperation	pFunc;
		DWORD				oprType;
	};


};//namespace vobject


#endif //__VHERODEFINE_H__
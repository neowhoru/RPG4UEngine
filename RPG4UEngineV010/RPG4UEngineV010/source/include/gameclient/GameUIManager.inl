/*////////////////////////////////////////////////////////////////////////
文 件 名：GameUIManager.inl
创建日期：2007年4月3日
最后更新：2007年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEUIMANAGER_INL__
#define __GAMEUIMANAGER_INL__
#pragma once

using gameui::EGameUIType;

inline void GameUIManager::UIMessageBox(LPCSTR sText,  DWORD /*dwShowTime*/)
{
//#pragma message(__FILE__  "(21) GameUIManager..UIMessageBox 增加ShowTime处理 " )
	UIShowMessageBox(gameui::eUIMessageBox, sText);
}

inline void GameUIManager::UIDisplayChatInfo	(LPCSTR         sText
                                             ,const COLOR col
															,const UINT     infoType
															,char*          szToName)
{
	UIDisplayInfo(gameui::eUIChatInfoBox, sText, col, infoType, szToName); 
}

inline void GameUIManager::UISetVisible(gameui::EGameUIType nType,	BOOL bSet)
{
	UISetData(nType,gameui::eSetVisible, bSet);
}

inline BOOL GameUIManager::UIIsVisible(gameui::EGameUIType nType)
{
	BOOL bVisible(FALSE);
	UIGetData(nType,gameui::eGetVisible, &bVisible);
	return bVisible;
}

inline void GameUIManager::UISetEnable(gameui::EGameUIType nType,	BOOL bSet)
{
	UISetData(nType,gameui::eSetEnable, bSet);
}

inline BOOL GameUIManager::UIIsEnable(gameui::EGameUIType nType)
{
	BOOL bEnable(FALSE);
	UIGetData(nType,gameui::eGetEnable, &bEnable);
	return bEnable;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define UILISTENER_IMPL_BODY(Name, params,ret,ret2)\
{\
	if(_nUIType >= 0 && _nUIType < gameui::eUITopMax)\
	{\
		if(m_arListeners[_nUIType])\
		{\
			ret m_arListeners[_nUIType]->##Name  params ;\
		}\
	}\
	ret2;\
}

#define RET_VOID

#define UILISTENER_IMPL(Name,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType)\
	UILISTENER_IMPL_BODY(Name, () ,ret,ret2)

#define UILISTENER_IMPL1(Name,p1,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL2(Name,p1,p2,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL3(Name,p1,p2,p3,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2,p3)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL4(Name,p1,p2,p3,p4,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2,p3,p4)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL5(Name,p1,p2,p3,p4,p5,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2,p3,p4,p5)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL6(Name,p1,p2,p3,p4,p5,p6,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2,p3,p4,p5,p6)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL7(Name,p1,p2,p3,p4,p5,p6,p7,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2,p3,p4,p5,p6,p7)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)

#define UILISTENER_IMPL8(Name,p1,p2,p3,p4,p5,p6,p7,p8,d,res,ret,ret2)\
	inline res GameUIManager::UI##Name(gameui::EGameUIType _nUIType, p1,p2,p3,p4,p5,p6,p7,p8)\
	UILISTENER_IMPL_BODY(Name, d,ret,ret2)



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
UILISTENER_IMPL1(Refresh,		BOOL bExtra,
										(bExtra),
										void, RET_VOID, RET_VOID)

UILISTENER_IMPL4(DisplayInfo	,LPCSTR			sText
										,const COLOR	col
										,const UINT		infoType
										,char *			szToName
										,(sText,  col, infoType, szToName)
										,void,RET_VOID, RET_VOID);
UILISTENER_IMPL2(DisplayTip	,LPCSTR sText
										,DWORD dwData
										,(sText, dwData)
										,void,RET_VOID, RET_VOID);


UILISTENER_IMPL8(ShowByTime	,DWORD			dwStartTime
										,DWORD			dwPeriod 
										,BOOL				bUp
										,int				nFlashCount
										,BOOL				bAutoClose
										,BOOL				bFadeIn 
										,const char*	pName
										,BOOL				bIsInterrupt 
										,(dwStartTime,dwPeriod,bUp,nFlashCount,bAutoClose,bFadeIn,pName,bIsInterrupt)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL3(StartReadytoLaunch	
										,DWORD	dwStartTime
										,DWORD	dwPeriod
										,short	shStartPermillage
										,(dwStartTime,  dwPeriod, shStartPermillage)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL1(Cancel			,DWORD dwStopTime
										,(dwStopTime)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL(CountDown		,void, RET_VOID, RET_VOID)

UILISTENER_IMPL1(ChangeShopState	
										,BOOL bOpen
										,(bOpen)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL1(OnMessage		,MSG_BASE* pMsg
										,(pMsg)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL1(OnWorldMessage
										,MSG_BASE* pMsg
										,(pMsg)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL4(OnMouseRButton 
										,vobject::VObject* pPlayer
										,int	x
										,int	y
										,BOOL bPress
										,(pPlayer,  x,  y, bPress)
										,void,RET_VOID, RET_VOID);
UILISTENER_IMPL4(OnMouseLButton 
										,vobject::VObject* pPlayer
										,int		x
										,int		y
										,BOOL		bPress
										,(pPlayer,  x,  y, bPress)
										,void,RET_VOID, RET_VOID);


UILISTENER_IMPL7(ShowMessageBox 
										,LPCSTR	szText
										,LPCSTR	szCaption
										,int		nBoxType
										,BOOL		bModal
										,void*	pCallback
										,void *	pData
										,int		nDataLength
										,(szText,szCaption,nBoxType,bModal,pCallback,pData,nDataLength)
										,void,RET_VOID, RET_VOID);

UILISTENER_IMPL7(ShowInputBox 
										,LPCSTR            szCaption
										,BOOL              bNumber
										,BOOL              bModal
										,void*				 procCallback
										,void*             pData
										,int               nDataLength
										,BOOL              bPassword		
										,	(szCaption
											,bNumber
											,bModal
											,procCallback
											,pData
											,nDataLength
											,bPassword)
										,void,RET_VOID, RET_VOID);


UILISTENER_IMPL2(SetMoveText	,LPCSTR szText
										,int nCount
										,(szText, nCount)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL2(SetData		,DWORD dwType
										,LPARAM lpData
										,(dwType, lpData )
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL2(GetData		,DWORD dwType
										,void* pRet
										,(dwType ,pRet)
										,BOOL, return, return FALSE);

UILISTENER_IMPL1(LoadUI			,EGameUIType	nSubType
										,(nSubType)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL1(UnloadUI		,EGameUIType	nSubType
										,(nSubType)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL1(OnStartUp		,EGameUIType	nSubType
										,(nSubType)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL1(OnProcess		,EGameUIType	nSubType
										,(nSubType)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL2(FrameMove		,EGameUIType	nSubType,DWORD dwTick
										,(nSubType, dwTick)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL1(OnClose		,EGameUIType	nSubType
										,(nSubType)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL2(TriggerFunc	,gameui::ETriggerData eData,LPARAM lpData
										,(eData,lpData)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL(OnEnterWorld	,void, RET_VOID, RET_VOID);
UILISTENER_IMPL1(OnDataInput	,void* pData
										,(pData)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL8(ShowSelectBox
										,LPCSTR	szText
										,void*	pItemList
										,int		nItemCount
										,LPCSTR	szCaption
										,BOOL		bModal
										,void*	funCallbackFun
										, void *	pData
										,int		nDataLength 
										,(szText,pItemList,nItemCount,szCaption,bModal,funCallbackFun,pData,nDataLength)
										,void, RET_VOID, RET_VOID);
UILISTENER_IMPL1(UpdateData	,BOOL bSet
										,(bSet)
										,void, RET_VOID, RET_VOID);

UILISTENER_IMPL(IsEditInputFocus,
										BOOL, return, return FALSE);




#endif //__GAMEUIMANAGER_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：VCharacter.h
创建日期：2008年3月29日
最后更新：2008年3月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	可视人物控制与封装

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VCHARACTER_H__
#define __VCHARACTER_H__
#pragma once


#include "CommonDefine.h"
#include "VObject.h"
#include "VCharacterDefine.h"
#include "VCharacterActionQueue.h"
#include "VCharacterMoveQueue.h"
#include "VCharacterStatusCtrl.h"
#include "VCharacterStatusDefine.h"
#include "TStaticArray.h"
#include "walkpath.h"
#include "MoveNodePath.h"
#include "MeshAnimationCtrlListener.h"
#include "GameEffectDefine.h"
#include "ConstSkeleton.h"


using namespace std;
using namespace gameeffect;

class PlayerEffectByMdl;
class V3DSceneObject;
class VUCtrlEdit;
class IGameEffectContainer;
class PathGameMap;
class ISkillSpecialEffect;
class IGameEffect;
class V3DSpaceBox;
class IniFile;
class IGameEffectHelper;
struct sVSKILLINFO_BASE;

//_NAMESPACE2	(SItemSkill, giteminfo);
_NAMESPACE2U(APPEARANCEINFO,	object);
_NAMESPACEU(Character,			object);

namespace vobject
{ 
class VCharacterAnimationCtrl;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API VCharacter : public VObject 
											, public MeshAnimationCtrlListener
{
	friend class VClone;
public:
	VCharacter();
	~VCharacter();

public:
	virtual BOOL	OnMeshLoading();
	virtual void	OnMeshUnloading();
	
	virtual BOOL	RequestWorkingData();
	virtual void	ReleaseWorkingData();
	virtual void	Reset();

public:
	virtual void	Update		( DWORD dwTick );
	virtual void   UpdateManual( DWORD dwTime );
	virtual BOOL	RenderInfo	( DWORD dwTick
										, DWORD dwColor = 0xffffffff
										, float fTransparent = 1.0f
										, float fPlayerTransparent = 1.0f );

	virtual DWORD	GetTriggerPointType	();
	virtual void	GetShowTextInfo		(StringHandle&	sText);
	virtual BOOL   CheckFightingStatus	();

public:
	/////////////////////////////////////////////////////
	//MeshAnimationCtrlListener虚函数
	virtual void OnAnimating		(const SAnimationInfo& info);

	/////////////////////////////////////////////////////
	//PathHandlerListener 虚函数
	virtual void OnStop			();
	virtual void OnReset			();
	virtual void OnReadyToMove	(BOOL bThrust);
	virtual void OnLoad			(BOOL bLoaded );


protected:
	virtual void	_PrevRenderMesh( DWORD dwTick,LIGHT_INFO& light );
	virtual void	_OnRenderMesh	( DWORD dwTick,LIGHT_INFO& light );

public:
	//update
	virtual void	UpdateEquips();

	virtual void	RenderPath	();
	//virtual void	Render		( DWORD dwTick );
	virtual void	RenderDebugInfo(DWORD dwTick, int x, int y);

	//工具类函数
	virtual void	ChangeDrome	( VOBJID nID );
	virtual void	DisDrome		();
	virtual void	MountMonster( int nModelId );
	virtual void	DisMount		();

protected:
	virtual void _OnMountMonster(BOOL bMountUp);

public:
	virtual void Jump();

	virtual BOOL	Relive			();
	virtual void	Dead				();
	virtual void	ClearDeath		();

	virtual void		ChangeEquip	(eEQUIP_POS_INDEX		equipPos
                                 ,CODETYPE					itemID)		;	//设定 某个部位的装备
	virtual void		ChangeEquip	(const APPEARANCEINFO & appearInfo );					//设定 所有的装备
	virtual void		UpdateEquips(BOOL bFightFlag );						//穿戴所有的物品

protected:
	virtual void		_ChangeEquip(ePLAYER_TYPE					charType
											,eSEX_TYPE			iSex
											,VCharacterEquipment*	pEquip
											,const APPEARANCEINFO&	appearInfo);

public:
	virtual void		AddDeathTask();
	virtual void		OnUseItem	(DWORD dwWasteType);

public:
	///////////////////////////////////////////////////
	//特效控制
	virtual int   PlayEffect				(EFFECTINDEX   effectIndex
													,VCharacter*	pPlayer=NULL
													,float			fScale=0
													,DWORD			dwFlyTime=0
													,VCharacter*	pDestPlayer=NULL);

	virtual int   PlayEffect				(LPCSTR			pEffectName
													,VCharacter*	pPlayer=NULL
													,float			fScale=0
													,DWORD			dwFlyTime=0
													,VCharacter*	pDestPlayer=NULL);

	virtual BOOL	LoadMtlEffect			( LPCSTR		 pszFileName );
	virtual void	ClearIntonateEffect	( void );

	virtual void	SetCastTargetPos		(Vector3D* pTargetPos);

	virtual void	CastModelEffect		(EFFECTINDEX  effectIndex
													,VCharacter*  pTargetPlayer=NULL
													,int          nSlot=0);

	virtual void	CastModelEffect		(IGameEffect* pEffect
													,VCharacter*  pTargetPlayer=NULL
													,int          nSlot=0);

	virtual void	CastHitFlashEffect	(IGameEffect* pEffect );

	virtual IGameEffectHelper*	GetTargetDummy(INT nTargetBindWhere=eEffectBindPart_Body);


protected:
	virtual BOOL	IsHitFlashEffect( LPCSTR pEffectName );


public:
	/////////////////////////////////////////////////
	//位置及移动相关
	virtual void	SetTilePos		( int x, int y, PathGameMap* pMap );
	virtual void	SyncTilePos		( int x, int y);
	virtual void	JumpToTilePos	( int x, int y, PathGameMap* pMap );
	virtual void	JumpToTilePos	( const VECTOR3D& vDest, PathGameMap* pMap );

	virtual BOOL   MoveToTilePos	( int nTargetX, int nTargetY, PathGameMap* pMap,int iLag = 0 );
	virtual void	ResetMove		( DWORD dwSerialNo );


	virtual void	HangMoving		( DWORD dwCurTime, DWORD dwTimeLenght ); //挂起移动
	virtual BOOL	IsHangMoving	( DWORD dwCurTime );
	virtual void	AssaultMoving	( VOBJID targetID ); //冲锋移动

	///////////////////////////////////////////////////////
	//设置路径后使用
	virtual POINT	GetTargetMovePos	( POINT ptNowPos, POINT ptTargetPos, int iAttackRange );
	virtual void	CaluMoveVelocity	( int iLag );
	virtual void	DirToPath			(POINT ptStart, int nStep, BYTE byDir[], POINT ptPath[] );
	virtual void   StopMovePath		(); //  定止走动
	virtual void   ClearMovePath		();//  清空已经保存的路径，数据

	void				SetMoveVelocity	( float fVelocity );
	int				GetPathSize			();

	virtual void	SetMoving			( BOOL bMoving, BOOL bBackOff=FALSE );



protected:
	virtual void	_CalcStepCost	(float& fCost);
	virtual void	_OnNextStep	();

	virtual BOOL   GetNextStep();
	virtual SHORT	GetMovingType( float fCurDir, float fMovingDir );
	

public:
	////////////////////////////////////////////////////
	//动作控制操作
   virtual void  SetActionStatus		(STATUS_KEY		CurKey
                                    ,DWORD			Delay
												,STATUS_KEY		NextKey = esk_Idle);

	virtual void  _SetActionStatus	(STATUS_KEY		CurKey
                                    ,DWORD			dwAnimPlayTime
												,DWORD			dwStatusTime
												,STATUS_KEY		NextKey=esk_Idle);

	virtual BOOL  CanSwitchAnim				();
	virtual BOOL  CanCountermarchAnim		();


public:
	#define	VCHARACTER_ANIM(Name)		virtual void	Switch##Name##Anim(DWORD dwTimeOut=ANIMATION_TIMEOUT)
	#define	VCHARACTER_ANIM2(Name,p)	virtual void	Switch##Name##Anim p
	
	VCHARACTER_ANIM	(Sit);					//坐的动作		
	VCHARACTER_ANIM	(Action);				//切换到，莫个动作
	VCHARACTER_ANIM	(Die);					//切换到死亡的动画

	VCHARACTER_ANIM	(Walk);					//切换到行走的动作
	VCHARACTER_ANIM	(WalkBackWards);		//切换到倒退动作
	VCHARACTER_ANIM	(Shuffle);				//切换到转身动作
	VCHARACTER_ANIM	(ShuffleLeft);			//切换到向左转身的动作
	VCHARACTER_ANIM	(ShuffleRight);		//切换到向右转身的动作
	VCHARACTER_ANIM	(Run);					//切换到跑步的动作

	VCHARACTER_ANIM	(Swim);					//切换到游泳的动作
	VCHARACTER_ANIM	(SwimIdle);				//切换到水中待机的动作
	VCHARACTER_ANIM	(Jump);					//切换到起跳动作		
	VCHARACTER_ANIM	(Drop);					//切换到下落动作
	VCHARACTER_ANIM	(UrgencyDrop);			//切换的紧急下落动作
	VCHARACTER_ANIM	(ReadyToSpecial);		//切换到准备角色的特有动作
	VCHARACTER_ANIM	(Special);				//切换角色特有动作
   VCHARACTER_ANIM	(Idle);					//切换到待机动作
	VCHARACTER_ANIM	(SpecialIdle);			//切换到特殊待机动作
   VCHARACTER_ANIM	(Attack);				//切换到战斗动作
	VCHARACTER_ANIM	(Pick);					//Pick
	VCHARACTER_ANIM	(HoldSkill);			//切换到持续技能
	VCHARACTER_ANIM	(Dodge);					//切换到躲避动作             
	VCHARACTER_ANIM	(Hurt);					//切换到被击动作	
	VCHARACTER_ANIM	(Scabbard);				//切换到拔刀
	VCHARACTER_ANIM	(Assault);				//冲锋动作      
	VCHARACTER_ANIM	(KnockDown);			//倒下动作      
	VCHARACTER_ANIM	(Stand);					//倒下动作      

	VCHARACTER_ANIM2	(Attack,	(DWORD ustSkillId, DWORD ustSkillLevel ));
																						
	VCHARACTER_ANIM2	(Spell,	(IntonateEndInfo* pInfo 
										,DWORD				dwTimeOut=ANIMATION_TIMEOUT));
																											
	VCHARACTER_ANIM	(Intoate);			//切换到吟唱动作
	VCHARACTER_ANIM2	(Intoate,(DWORD dwTimeOut, sVSKILLINFO_BASE* pSkill ));

public:


protected:
	///////////////////////////////////////////////////
	//Action相关
   virtual void		ClearActionQueue			(); // 清除当前的动作序列
	virtual void		HandleActionQueue			( BOOL bSkipStatus = false );
	virtual void		HandleMoveQueue			( BOOL bHang = FALSE );

	///< 切换到需要状态的动画
   virtual void		SwitchAnim					(STATUS_KEY PreStatus
															,DWORD		dwTimeOut = ANIMATION_TIMEOUT);  

	///< VCharacter对象在销毁前执行所有的动作队列
	virtual void		ExcuteClearActionQueue	( void );


	virtual int		 GetPlayerActionID		(int ActionID, int iFightType = -1);//取得对应动作的编号
	virtual int		 GetPlayerIdleActionID	(int iFightType = -1);//取得对应动作的编号
	virtual LPCSTR	 GetPlayerIdleActionName(int iFightType = -1);//取得对应动作的编号
	virtual LPCSTR	 GetPlayerHurtActionName(int iFightType= -1);

	//状态控制函数
	virtual void    _DoStatusIdle			();    //待机
	virtual void    _DoStatusPickItem	();	 //捡道具
	virtual void    _DoStatusDropItem	();	 //丢弃道具
	virtual void    _DoStatusTalk			();    //对话
	virtual void    _DoStatusTrade		();    //交易
	virtual BOOL    _DoStatusMove			();    //移动
	virtual void    _DoStatusChangeDir	();	 //转身
	virtual void    _DoStatusPreAttack	();    // 预备攻击状态
	//
	virtual void	 _DoStatusPreIntonate();
	virtual void	 _DoStatusHoldSkill	();

	virtual void    _DoStatusAttack		();     // 攻击状态
	virtual void    _DoStatusIntonate	();	  // 吟唱
	virtual void    _DoStatusBeAttack	();     // 受击状态
	virtual void    _DoStatusMiss			();     // 闪避状态
	virtual void    _DoStatusSleep		();     // 昏倒状态
	virtual void    _DoStatusDie			();     // 死亡状态
	virtual void    _DoStatusBeatBack	();	  // 击退
	virtual void    _DoStatusActionTime	();	  //在一段时间内作莫个动作

public:
	virtual float	GetGrowSize		();

protected:
	virtual BOOL	_On_DoStatusMove();
	virtual void   _UpdateMountAnim	(DWORD dwTime);
	virtual void   _UpdateAnim			(DWORD dwTime);
	virtual void   _UpdateEffect		(DWORD dwTime);

	virtual void   _UpdateAnimStatus	(DWORD dwTick);
	virtual void   _UpdateJumping		(DWORD dwTick);
	virtual void   _UpdateWaterStatus(DWORD dwTick);
	virtual BOOL   _PrevIntoWater			();
	virtual void   _OnIntoWater			();
	virtual void   _OnOutofWater			();
	virtual void   _UpdateFootStep		(DWORD dwTick);
	virtual void   _UpdateWalkingOnLand	(DWORD dwTick);
	virtual void   _UpdateWalkingDir		(DWORD dwTick);



	virtual void _UpdateMovingGoal		(Vector3& vNextFramePos);
	virtual void _ApplyMovingData			(BOOL bCollision, const Vector3& vNextFramePos);
	virtual void _SendMovingState			();
	virtual void _UpdateMovingState		();

	virtual void   _OnAttack				(VCharacter* pSrcPlayer
                                       ,VCharacter* pTargetPlayer)		;

public:
	//判断是否有武器
	virtual BOOL	IsHaveWeapon		();
	virtual BOOL	IsHaveLHardWeapon	( );
	virtual BOOL	IsDoubleHandWeapon();
	virtual BOOL	IsHaveShield		();				//判断是否有盾
	virtual BOOL	IsIdle				();


public:
	///////////////////////////////////////////////
	//技能相关
	virtual void	ReadySpecialSkill		( SHORT shStartPermillage );//准备发动技能
	virtual void	CancelSpecialSkill	();//暂停准备
	virtual void	CountDownSpecialSkill();//开始技能倒计记时

public:
	//////////////////////////////////////////////////
	//堆一些服务器发过来的基本行为队列的执行
	virtual void				DoExpression		(ExpressionInfo* pInfo);
	virtual void				DoEndExpression	(ExpressionInfo* /*pInfo*/){};

	virtual void				DoAttack				( sATTACK_INFO* pAttackInfo ,BOOL bJustTriggerPoint=FALSE);
	virtual void				DoEndAttack			( sATTACK_INFO* pAttackInfo );

	virtual void				DoMoveAStep			( BYTE byDir );
	virtual void				DoEndMoveAStep		( BYTE byDir );

	virtual void				DoIntonate			(IntonateInfo* pIntonageInfo);
	virtual void				DoEndIntonate		(IntonateEndInfo* pInfo);

	virtual void				DoHoldSkillStart	( int iPercent = 0);
	virtual void				DoHoldSkillEnd		();
	virtual void				DoHoldSkillCancel	();
	virtual void				DoHoldSkillOneHit	();
	virtual void				DoSeeHoldSkill		( SSeeHoldSkill*pSee );
	virtual void				DoBeAttacked		();

public:
	//////////////////////////////////////////////////
	//人物身体上附加的战斗状态
	virtual	ISkillSpecialEffect*	
									CreateSkillEffect		(DWORD				dwEffectID
                                                ,const VECTOR3D&	vSrc
																,const VECTOR3D&	vDst);

	virtual	void				ClearEffectState		(DWORD dwKey); //清除状态特效
	virtual	void				ClearEffectStates		(); //清除状态特效
	virtual	void 				ProcessEffectStates	(DWORD dwTick);//设置人物状态的上图效果
	virtual	void				UpdateEffectStates	();//	设置角色身上的状态



	//////////////////////////////////////////////////////
	//攻击触发点
	virtual	BOOL	OnTriggerPoint	(BOOL				bLastTriggerPoint
											,int				nTriggerPointType
											,sATTACK_INFO* pAttackInfo			=0);
	virtual  BOOL  CanRemoveAttackInfo	(sATTACK_INFO* pAttackInfo);

protected:
	virtual void	PushTriggerPoint		(int   nStartFrameID
                                       ,int   nEndFrameID
													,int   nTriggerPointFrameID
													,DWORD dwTimePerLoop);
	virtual BOOL	CanPlayTriggerPoint	();
				

public:
	virtual void	SetObject		(Object* pObject);


public:
	virtual void	ReleasePet	();
	virtual BOOL	HavePet		();

	///////////////////////////////////////
	//hp信息，主要用来驱动各类动作的变化
public:
	virtual void	SetHPInfo		( int nHp, int nHpMax );
	virtual void	CaluHPPercent	();
	virtual void	SetHPInfo_HP	( int nHp );			//主角升级后会用到
	virtual int		GetHp()			;
	virtual int		GetHpMax()		;

public:
	virtual void	SetName				( char* pszName );
	virtual void	SetHairColorIndex	( int nIndex );

	void				SetCharType		(DWORD  dwType );
	DWORD				GetCharType		()					;
	void				ResetRenderAlpha()				;


	///////////////////////////////////////////////////////////
	//基本信息
protected:
	VG_PTR_GET_PROPERTY		(Character,	Character);

	///////////////////////////////////////////////////////////
	//数据信息
	BOOL							m_bCastEffectNoTarget;	///无目标投射处理
	BOOL							m_bAssaultMoving; //是否在冲锋移动
	VOBJID						m_shAssaultTargetID; //冲锋目标的ID

	//属性
	VG_INT2_GET_PROPERTY	(PlayerStatus);
	VG_BYTE_PROPERTY		(SitStatus);		//坐在地上的标志
	VG_BYTE_PROPERTY		(ToFightStatus);	//需要切换到的战斗状态
	VG_BYTE_PROPERTY		(FightStatus);		//战斗模式
	VG_BYTE_PROPERTY		(Sex);				//性别
	VG_BYTE_PROPERTY		(Profession);		//职业
	VG_INT_GET_PROPERTY	(HairColorIndex);	//发色

	VG_INT2_PROPERTY		(Country);		//角色所在国家
	VG_INT2_PROPERTY		(WeaponType);
	VG_FLOAT_PROPERTY		(WeaponAttackRange);
	VG_INT2_PROPERTY		(ShotEffectID);
	VG_BOOL_PROPERTY		(LookAtTarget);	//移动时是否锁定目标


	int						m_nSpineBoneID;		//人物的上下身的骨骼位置
	int						m_nRightHandBoneID;	//人物的右手位置
	int						m_nLeftHandBoneID;	//人物的左手位置

	VG_ARRAY_PROPERTY		(Equip	, VCharacterEquipment, skeleton::eMaxPart);		//装备



	//////////////////////////////////////////
	//移动
	BOOL								m_bHangMoving; //挂起移动
	DWORD								m_dwStartHangMovingTime; //开始挂起移动的时间
	DWORD								m_dwHangMovingTimeLength; //挂多少时间

	VG_FLOAT_GET_PROPERTY		(MoveRate);		//移动速度
	float								 m_fCurMoveRate;
	VG_FLOAT_PROPERTY				(MovingStep);
	VG_FLOAT_PROPERTY				(RunStep);
	VG_FLOAT_PROPERTY				(SwimStep);
	VG_FLOAT_PROPERTY				(NormalAttackRate);
	VG_BOOL_GET_PROPERTY			(Moving);
	VG_BOOL_PROPERTY				(Countermarch); //是否在后退
	BOOL								m_bRunning;
	BOOL 								m_bPlayedCoutermarchAnim;
	BOOL 								m_bLastFrameMoving;
	DWORD 							m_dwStartStopMovingTime;
	float 							m_fSpeed;

	WalkPath							m_PathInfo;
	sMOVE_INFO						m_MoveInfo;
	MoveNodePath					m_PathFinder;

	////////
	sMOVEQUEUE_INFO				m_NowMove;			//	当前的移动

	VG_TYPE_PROPERTY				(ActionQueue,	VCharacterActionQueue);    //	npc的行为堆栈...保存服务器发过来的行为...
	VG_TYPE_PROPERTY				(MoveQueue,		VCharacterMoveQueue);			//	移动队列
	VG_TYPE_GET_PROPERTY			(Operation,		sOPERATION);	//动作
   SAction							m_NowAction;					//	npc但前正在执行的行为，pop处理后会纪录
	DWORD								m_dwSpecialIdleInterval;	//特殊待机的播放间隔
	short								m_shCurMovingType;			//当前的移动状态


	sHP_INFO							m_HPInfo;

	VG_PTR_PROPERTY				(CharEffectContainer, IGameEffectContainer);	//特效容器

	////////////////////////////////////////////////////////
	//技能
	VG_BOOL_PROPERTY				(Intonating);//是否在吟唱
	VG_DWORD_PROPERTY				(IntonatingStartTime);//吟唱开始的时间
	sHOLD_SKILL						m_HoldSkill;
	VG_PTR_PROPERTY				(AttackInfo, sATTACK_INFO);							//当前攻击信息


	sSKILL_RUNINFO					m_skillRuntimeDesc;
	VG_TYPE_PROPERTY				(SkillRunInfo,	sSKILL_RUNINFO);

	////////////////////////////////////////////////////////////////
	//人物身体上附加的战斗状态
	StateInfoMap						m_mpStateInfo;
	VCharacterStatusCtrl				m_status;

	DWORD									m_LastEffectLight;


	/////////////////////////////////////////////////////
	//TriggerPoint触发点
	VG_BOOL_GET_PROPERTY	(TriggerPointPlayOver);	
	DWORD						m_dwPushTriggerPointTime;
	DWORD						m_dwTriggerPointPlayTime;
	float						m_fAnimationFactor;		///> 动画播放进度

	VECTOR3D					m_vDir; //角色的朝向

	VG_FLOAT_PROPERTY		(StartJumpingSpeed);	// 角色向上跳的初速度
	short						m_shJumpingType;			// 跳跃的类型
	DWORD 					m_dwStartJumpingTime;	// 开始起跳时间
	float 					m_fJumpingHeight;			// 角色的跳跃高度
	float 					m_fStartJumpingTerrainZ;// 角色的起跳的地表高度
	float 					m_fJumpSpeed;				// 角色向前跳的速度
	float 					m_fMaxStartJumpingSpeed;// 角色向上跳的最大初速度
	BOOL						m_bNeedDropBuf;			// 角色是否需要下落缓冲
	BOOL						m_bPlayedDropAnim;		// 角色播放了下落动作

	//Mounting
	VG_PTR_PROPERTY		(MountAnim, VCharacterAnimationCtrl);	//坐骑模型
	VG_BOOL_GET_PROPERTY	(Mounting);			// 是否在骑乘	
	int						m_nMountModelID;
	int						m_nMountBoneID;

	//模型Rotate
	VECTOR3D					m_vCurNormal;		//当前相对地表的法线偏移
	VECTOR3D					m_vTargetNormal;	//将要偏移到的目标法线

	VG_TYPE_PROPERTY			(PetID,		VOBJID);

};//VCharacter


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////


};//namespace vobject

#include "VCharacter.inl"

#endif //__VCHARACTER_H__


/*////////////////////////////////////////////////////////////////////////
文 件 名：AttackInfoMan.h
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	
	攻击数值的延迟处理（扣血等到攻击命中才扣，显示很合理）	
	接收到攻击消息
	加入AttackInfo到AttackInfo管理器
	加入攻击到动作队列
	角色死亡
	简单的进入死亡循环
	只到被复活
	角色退出（MSG_EXIT_CMDStruct,MSG_EXITMYSIGHT_TIPStruct）
	角色切换到保留队列，逻辑对象已经消失
	清除运行动作队列（保证AttackInfo被处理或移交给效果）
	切换地图			
	运行所有保存的AttackInfo
	清除所有的角色（主角除外）
	主角清除动作队列


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ATTACKINFOMAN_H__
#define __ATTACKINFOMAN_H__
#pragma  once

#include "TMapContainer.h"
#include "ActionQueueDefine.h"
#include "TMemoryPoolFactory.h"



typedef util::TMemoryPoolFactory<sATTACK_INFO>	AttackInfoPool;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API AttackInfoManager
{
public:
	AttackInfoManager(void);
	~AttackInfoManager( void );


public:
	BOOL Init(UINT uPoolSize);
	void Release();

public:
	static	void CBRemoveAttackInfo(sATTACK_INFO** pAttackInfo);
public:
	sATTACK_INFO*	AllocAttackInfo( );
	void				FreeAttackInfo	(sATTACK_INFO* pAttackInfo);

	sATTACK_INFO* AddAttackInfo		( sATTACK_INFO & attackinfo );
	void			 PushAttackRecord	( sATTACK_INFO*  pAttackInfo, SAttackInfoRecord*	rec);

	///> 传入的是AddAttackInfo返回的
	void DeleteAttackInfo		( sATTACK_INFO *pAttackInfo);

	///> 玩家切换地图时调用执行保存的所有AttackInfo
	void ExcuteAllAttackInfo( void );

	//退出游戏时调用
	void ClearAll( void );

protected:
	TMapContainer<sATTACK_INFO*,sATTACK_INFO*>	m_map;
	AttackInfoPool*									m_pAttackInfoPool;	
};

//Global
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(AttackInfoManager , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API AttackInfoManager& GetAttackInfoManager();
#define theAttackInfoManager	GetAttackInfoManager()

#endif //__ATTACKINFOMAN_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：MapViewManager.h
创建日期：2008年1月14日
最后更新：2008年1月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

	MapView与MapSize比例为： 
		水平比 150：128
		竖起比 160：128
	MapView偏移量：	（100,80）
	MapView内容：
		区域	：（100,80）-(699,559)
		宽		：150*4
		高		：160*3
					即容纳：4x3张地图

	小地图有两类：
		1.雷达图
		2.MapView俯瞰图

	MapView按照以上比例转换到MapView之前，须指定相应Map在land偏移量（Tile坐标）


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAPVIEWMANAGER_H__
#define __MAPVIEWMANAGER_H__
#pragma once

#include "CommonDefine.h"
#include "MapViewManagerDefine.h"
#include "MapViewSetting.h"

using namespace mapinfo;
using namespace std;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API MapViewManager 
{

public:
	MapViewManager();
	~MapViewManager();

public:
	virtual BOOL		Create	(int              nMapOriginW
										,int              nMapOriginH
										,sFIELDINFO_BASE* pField)=0;
	virtual void		Destroy	()=0;

	virtual BOOL		FrameMove	(DWORD dwTick)=0;
	virtual BOOL		Render		(DWORD dwTick)=0;

	virtual BOOL		ReadyRender	(int          nMode
                        ,const RECT&  rcWindow
								,const POINT& ptHero
								,int          nSight)=0;

public:
	virtual BOOL		MsgProc	(HWND   hWnd
                     ,UINT   msg
							,WPARAM wParam
							,LPARAM lParam)=0;

	virtual BOOL		LoadSettings	(LPCSTR szFile)=0;

	
	virtual void		Transfrom	( int &nCovX, int &nCovY )=0;
	virtual BOOL		AddMark		( sMAP_MARK* pMark )=0;
	virtual BOOL		LoadFullMap	( sFIELDINFO_BASE*	m_pFieldInfo);
	virtual BOOL		LoadImage	(int    nImage
											,LPCSTR szFileName)		=0;


	virtual void		Update				()=0;
	virtual BOOL		DrawTextUnderImage(RECT   rcImage
                              ,LPCSTR pszText
										,DWORD  dwColor)=0;

	virtual BOOL		DrawIconImg			(RECT  rcRenderWindow
										,RECT  rcSight
										,int   nImageType
										,float  fImageX
										,float  fImageY
										,RECT* prcDst=NULL
										,int   nHeroPosX=0
										,int   nHeroPosY=0
										,float fScale=1.0f)=0;

	virtual void		GetImageWindowRect(RECT   rcSight
                              ,sMAP_ICONINFO* pImage
										,int    nImageX
										,int    nImageY
										,RECT&  rcDst);

	virtual BOOL		MarkNpcPosition	(int		nNpcID );
	virtual BOOL		MarkNpcPositionBy	(LPCSTR  pszName );


	virtual void		ClearNpcInfo		();
	virtual void		ClearViewNpc		();
	virtual void		SetNpcInfo			(LPCSTR szName
													,float mapX
													,float mapY)=0;

	virtual POINT	GetPlayerPos		(int mapIdx,POINT MaxMapPos);
	virtual void SetOffPoint			(int idx,POINT pnt)=0;
	virtual void CreateMapButton		(LPCSTR pszFilename1
												,LPCSTR pszFilename2
												,RECT   rtArea)=0;
	virtual void _RenderMapButtons		(void);

	virtual void RealeaseSecondLV	()=0;	//用于释放当前显示的２级大地图

	virtual int		CheckWherePlayerIn(POINT pPlayerPos)=0;
	virtual void	SetMaxMapLevel		(BOOL bIsSec){ m_bIsSecondMap = bIsSec; };
	virtual POINT	GetMouseGlobalPos	();
	virtual void	SecondMapBtnDown	()=0;

	virtual BOOL	DrawTipInfo			(sNPC_COORDINFO* pInfo)=0;

	virtual sNPC_COORDINFO*	GetViewNpcAt			(POINT&	pt);
	virtual sNPC_COORDINFO*	GetViewNpcAtMouse		();
	virtual BOOL				GetTargetPosAt			(POINT&	pt,Vector3D& vPos)=0;
	virtual BOOL				GetTargetPosAtMouse	(Vector3D& vPos);

protected:
	virtual float  _GetHeroTargetDir		()=0;
	virtual void	_RenderFullMap			();
	virtual void	_RenderNpcDirect		(const RECT&	rcSight);
	virtual void	_RenderMarkLabels		(const RECT&	rcSight);
	virtual void	_DrawMiniMapPicture	();
	virtual void	_DrawNormapNpc			();
	virtual BOOL	_CalcMiniMapInfo		(DWORD dwTick);
	void				_PushButtonInfo		(const sMAPBTN_INFO&		info);
	void				_PushNpcInfo			(const sNPC_COORDINFO&	info);
	void				_PushViewNpc			(const sNPC_COORDINFO&	info);


protected:
	//////////////////////////////////////////////////////
	MapViewSetting		m_MapViewSetting;

	BOOL					m_bInSecondMap;
	vector<SHORT>		m_MapLevels[MAX_MAP_COUNTS];
	vector<SHORT>		m_MapLevelRoot;

	VG_INT_PROPERTY	(RenderMode);
	VG_INT_PROPERTY	(SightRange);
	VG_TYPE_PROPERTY	(TargetRect,	RECT);
	VG_TYPE_PROPERTY	(HeroTilePos,	POINT);

	sFIELDINFO_BASE*	m_pFieldInfo;
	int					m_nMapImageWidth;	// 地图图片的大小
	int					m_nMapImageHeight;
	
	int					m_nMapOriginW;	// 地图的实际大小
	int					m_nMapOriginH;

	sMAP_ICONINFO		m_IconImgs		[MAPICON_MAX];
	sMAP_MARK			m_MarkLabels	[MAX_MAP_MARK];
	short					m_nHeroArrowTexture;
	short					m_nMaskTextureID;
	short					m_arBGTextureIDs[3][3];



	BOOL							m_bMouseDown;
	BOOL							m_bIsSecondMap;
	int							m_nMaxMapIndex;

	sMAPBTN_SYS						m_SysBtns		[MAPBTN_MAX];
	POINT								m_OffsetInfos	[MAX_MAP_COUNTS];

	RECT								m_rcMiniImage		[3][3];		//取得源图片的矩形 clip
	RECT								m_rcRenderWindow	[3][3];		//渲染的矩形	src

	INT								m_nHeroMiniX;
	INT	 							m_nHeroMiniY;
	INT 								m_nHeroMiniTargetX;
	INT	 							m_nHeroMiniTargetY;
	float								m_fZoomScale;
	DWORD								m_MoveTimer;

private:
	vector<sMAPBTN_INFO>			m_ButtonInfos;
	vector<sNPC_COORDINFO>		m_NpcInfos;
	vector<sNPC_COORDINFO>		m_ViewNpcs;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(MapViewManager , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API MapViewManager& singleton::GetMapViewManager();
#define theMapViewManager  singleton::GetMapViewManager()


#endif //__MAPVIEWMANAGER_H__
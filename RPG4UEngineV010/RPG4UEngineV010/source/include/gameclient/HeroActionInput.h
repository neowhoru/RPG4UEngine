/*////////////////////////////////////////////////////////////////////////
文 件 名：HeroActionInput.h
创建日期：2008年5月21日
最后更新：2008年5月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __HEROACTIONINPUT_H__
#define __HEROACTIONINPUT_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "CharacterDefine.h"
#include "RPGGameDefine.h"
#include "HeroActionInputDefine.h"

_NAMESPACEU(MapObject,	object);
_NAMESPACEU(Character,	object);
_NAMESPACEU(Object,		object);
_NAMESPACEU(Item,			object);

class HeroSoundLayer;
class HeroMouseInputLayer;
class HeroKeyboardInputLayer;
class HeroAssistantLayer;
class HeroQueueActionLayer;
using object::PLAYER_ACTION;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API HeroActionInput
{
	friend class HeroSoundLayer;
	friend class HeroMouseInputLayer;
	friend class HeroKeyboardInputLayer;
	friend class HeroAssistantLayer;
	friend class HeroQueueActionLayer;
public:
	HeroActionInput(void);
	~HeroActionInput(void);

public:
	virtual BOOL	Init();
	virtual void	Release();
	virtual BOOL	FrameMove	(DWORD dwTick)=0;

public:
	virtual void	ProcessNpcMeet			(DWORD npcID);
	virtual void	OnProcessNpcMeet		(DWORD dwPlayerID,DWORD npcID);

	virtual void	SetCurrentTarget		(DWORD dwID,BOOL bCheck=TRUE)=0;
	virtual DWORD	GetCurrentTarget		()=0;
	virtual void	RefreshCurrentTarget	()=0;

	virtual BOOL	ProcessCamera			()=0;
	virtual void	EndCamera				()=0;
	virtual BOOL	ProcessQueueAction	();
	virtual BOOL	ProcessQueueGetItem	();


	virtual float	GetActionDistance			(PLAYER_ACTION action)=0;
	virtual float	GetAttackActionDistance	()=0;
	float				GetQueueActionDistance	();


	virtual BOOL  CanBeSkillTarget	(DWORD dwSkillID,Character *pChr)=0;
	virtual BOOL  IsAttackSkillArea	()=0;
	
	virtual void    SetComboReset()	=0; 

	virtual Object    *	GetPickedObject		()=0;
	virtual Character *	GetPickedCharacter	()=0;
	virtual Item      *	GetPickedItem			()=0;
	virtual MapObject *	GetPickedMapObject	()=0;

	virtual void			StabberShot		()=0;
	virtual void			StabberReload	()=0;

	virtual void	 ToggleAdvancedMap	()=0;
	virtual void	 ToggleAttackState	()=0;
	virtual void    SwitchAutoAttack		(BOOL bFlag,BOOL bCheck=TRUE)=0;
	virtual void    SwitchAutoMove		(BOOL bFlag,BOOL bCheck=TRUE)=0;
	virtual void    SwitchAreaSkill		(BOOL bFlag,BOOL bCheck=TRUE)=0;
	virtual void	 SwitchCanntAttack	(BOOL bFlag)=0;

	virtual DWORD		IncreaseComboCount()=0;
	virtual DWORD		IncComboCount		();
	virtual void		ClearPickingTarget();

	virtual BOOL		PushSkill			(DWORD dwSkillid)=0;
	virtual BOOL		DoSkillProcessing	(DWORD dwSkillid)=0;
	virtual BOOL		UseSummonSkill		(DWORD dwSkillid)=0;
	virtual BOOL		UseStyle				(DWORD dwSkillid)=0;
	virtual int			GetSkillQueueSize	()=0;

	virtual BOOL		IsIdleState			( void )=0;;

public:
	virtual void    PlayLimitUseSound			();
	virtual void    PlayNoTargetSound			();
	virtual void    PlayTargetTooFarSound		();
	virtual void    PlayNotEnoughMoneySound	();
	virtual void    PlayNotEnoughManaSound		();
	virtual void    PlayNotEnoughPointSound	();
	virtual void    PlayRepairableSound			(BOOL bCanntRepair);


public:	
	virtual BOOL    UseSkill			(DWORD skillid)=0;
	virtual BOOL    UseAreaSkill		()=0;

	virtual BOOL	OnSpacePushItem	( void );
	virtual BOOL	UpdateTargetList	( float fDistance, BOOL bCheckCameraAngle = TRUE );
	virtual BOOL	OnTabPushTarget	( void );
	virtual BOOL	OnNextTarget		( void );
	virtual BOOL	OnPrevTarget		( void );

	virtual BOOL	MoveToAction		(const VECTOR3D& vDest,int iTileNum)=0;
	virtual VOID	InitAction				()=0;
	


	virtual void	ProcessComboDelay		(DWORD dwTick)=0;
	virtual void	ProcessTargeting		()=0;
   virtual void	ProcessPicking			()=0;
	virtual void	ProcessCursor			()=0;
   virtual void	ProcessAssistTarget	()=0;
	
	virtual BOOL	ProcessTargetClick	(Character* pTarget
													,BOOL       bAutoAttack=TRUE)	;
	virtual BOOL	ProcessClickPosition	(Vector3C&	vTarget)	;

	virtual BOOL    AttackNearestTarget	()=0;
	virtual DWORD   FindNearestTarget	()=0;


	void	  PushQueueAction(const PLAYER_ACTION& action);

public:
	Vector3D				GetPickingPos		(); 
	void					SetMoveDelay		(DWORD dwDelay);

	virtual BOOL		SetFollowState	( BOOL bFlag, DWORD dwPlayerKey = 0 );
	virtual BOOL		GetFollowState	( void );
	virtual DWORD		GetFollowPlayer( void );


	virtual BOOL		UseSpecialMoveSkill	( DWORD dwSkillid)=0;
	virtual BOOL		IsShootingMode			()=0;


//protected:
	virtual DWORD		GetSkillInfo			(DWORD dwClassId,BOOL & bActive,float &dwDistance)=0;

	virtual Vector3D	GetCanMovePos			(const VECTOR3D& vDest)=0;
	virtual BOOL		CanUseSpecialMove		()=0;

public:
	BOOL		InstallKeyboardLayer		(HeroKeyboardInputLayer* pKeyboard, ePLAYER_TYPE charType);
	BOOL		InstallSoundLayer			(HeroSoundLayer*				pLayer);
	BOOL		InstallMouseLayer			(HeroMouseInputLayer*		pLayer);
	BOOL		InstallKeyboardLayer		(HeroKeyboardInputLayer*	pLayer);
	BOOL		InstallAssistantLayer	(HeroAssistantLayer*			pLayer);
	BOOL		InstallQueueActionLayer	(HeroQueueActionLayer*		pLayer);

protected:

	VG_VECTOR_PROPERTY	(Dest, Vector3D);
	VG_BOOL_PROPERTY		(Picking);
	VG_DWORD_PROPERTY		(CurrentSkillID);
	VG_DWORD_PROPERTY		(SelectedTarget);
	VG_DWORD_PROPERTY		(PickedTarget);
	VG_BOOL_GET_PROPERTY	(AutoAttack);
	VG_BOOL_GET_PROPERTY	(AutoMove);
	VG_BOOL_PROPERTY		(KeyBoardMoveCheck);
	VG_BOOL_GET_PROPERTY	(AreaSkill);

	VG_INT2_PROPERTY		(ComboDelay);
	VG_INT2_PROPERTY		(ComboCount);
	VG_TYPE_PROPERTY		(QueueAction, PLAYER_ACTION);
	VG_BOOL_PROPERTY		(CanDoNextAttack);
	VG_BOOL_PROPERTY		(IgnorePlayer);
	VG_BOOL_PROPERTY		(EnableInput);
	VG_BOOL_PROPERTY		(HeroKeyboardMove);
	BOOL						m_bFirstClick;
	//DWORD               m_TargetId;

	VG_BOOL_GET_PROPERTY	(KeyboardQueueAttack);
	int						m_iKeyboardQueueAttackDelay;

	VG_VECTOR_PROPERTY	(PickedPos, Vector3D);
	VG_INT2_PROPERTY		(PickedTile);
	VG_BOOL_PROPERTY		(CanMoveToPickedPos);
	VG_BOOL_PROPERTY		(SpecialMove);
	VG_INT2_PROPERTY		(MoveDelay);




	VG_BOOL_GET_PROPERTY	(CannotAttack);
	VG_BOOL_GET_PROPERTY	(MouseInRange);

	VG_TYPE_GET_PROPERTY	(TargetType,		eRELATION_TYPE);

protected:
	VG_BOOL_PROPERTY		(CameraDetech);
	VG_VECTOR_PROPERTY	(FreeCameraPos,	Vector3D);

	VG_PTR_GET_PROPERTY	(SoundLayer,		HeroSoundLayer);
	VG_PTR_GET_PROPERTY	(MouseLayer,		HeroMouseInputLayer);
	VG_PTR_GET_PROPERTY	(KeyboardLayer,	HeroKeyboardInputLayer);
	VG_PTR_GET_PROPERTY	(AssistantLayer,	HeroAssistantLayer);
	VG_PTR_GET_PROPERTY	(QueueActionLayer,HeroQueueActionLayer);


	HeroKeyboardInputLayer*		m_arKeyboardLayer[PLAYERTYPE_MAX];

};//class HeroActionInput

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(HeroActionInput , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API HeroActionInput& singleton::GetHeroActionInput();
#define theHeroActionInput		singleton::GetHeroActionInput()
#define theHeroActionInput		singleton::GetHeroActionInput()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "HeroActionInput.inl"



#endif //__HEROACTIONINPUT_H__
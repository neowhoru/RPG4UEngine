/*////////////////////////////////////////////////////////////////////////
文 件 名：GameInPlaying.h
创建日期：2007年4月26日
最后更新：2007年4月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	游戏Playing生命周期管理类，即从人物选择器进入游戏->退出游戏阶段
	这个阶段，主要：
	1.创建人物
	2.各类游戏规则
	3.各类场景变化
	4.退出游戏规则活动
		1.退出游戏
		2.退到人物选择
		3.退到服务器选择

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEINPLAYING_H__
#define __GAMEINPLAYING_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "GameInPlayingDefine.h"
#include "IRenderSystem.h"
#include "ApplicationBaseListener.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _CLIENTGAMELIB_API GameInPlaying : public IRenderSystem
												, public ApplicationBaseListener
{
public:
	GameInPlaying();
	~GameInPlaying();

public:
	virtual BOOL Init()		=0;
	virtual void Release()	=0;
	virtual BOOL RequestWorkingData()	=0;
	virtual void ReleaseWorkingData()	=0;
	virtual BOOL FrameMove(DWORD dwTick)	=0;
	virtual BOOL Render(DWORD dwTick)	=0;
	virtual BOOL RenderHUD(DWORD dwTick)=0	;

protected:
	virtual BOOL _RenderHeroStatus(DWORD dwTick)	=0;

public:
	//Listener虚函数
	virtual void OnWinMsg( HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)=0;
	virtual void OnAppFocus(BOOL bFocus)=0;
	virtual void OnAppActive(BOOL bActive)=0;

	virtual void OnEnterWorld()=0;

protected:
	virtual void InitLandData()=0;

public:
	//运行状态处理

	GAMEINPLAYING_DECLARE_STATUS(Disable,DISABLE);
	GAMEINPLAYING_DECLARE_STATUS(Init	,INIT  );
	GAMEINPLAYING_DECLARE_STATUS(Working,WORKING );
	GAMEINPLAYING_DECLARE_STATUS(End		,END );

	GAMEINPLAYING_DECLARE_RUN(Playing,	PLAY);
	GAMEINPLAYING_DECLARE_RUN(Pause,		PAUSE);
	GAMEINPLAYING_DECLARE_RUN(Stop,		STOP);

//protected:
	virtual BOOL CreateHero(BOOL bLocal)=0; 
	virtual void DestroyHero(BOOL bLocal)=0; 

	virtual BOOL CreateHeroData()=0; 

protected:
	INT		m_nRefCounter;	//引用计数器，为0时会销毁对象
	VG_FLOAT_PROPERTY(JumpToX);//szLandName时，为宽度数量，否则为me坐标
	VG_FLOAT_PROPERTY(JumpToY);
	VG_FLOAT_PROPERTY(JumpToZ);
	DWORD		m_dwWorkingStatus;	//GameInPlaying是否在运作中...
	DWORD		m_dwRunStatus;			//run pause...

	DWORD		m_dwSectorXNum;
	DWORD		m_dwSectorYNum;
};
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(GameInPlaying , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API GameInPlaying& singleton::GetGameInPlaying();
#define theGameInPlaying  singleton::GetGameInPlaying()


#endif //__GAMEINPLAYING_H__
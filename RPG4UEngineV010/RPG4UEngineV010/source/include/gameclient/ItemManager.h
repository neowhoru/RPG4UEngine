/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemManager.h
创建日期：2008年5月31日
最后更新：2008年5月31日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMMANAGER_H__
#define __ITEMMANAGER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "ItemManagerDeclare.h"


class _CLIENTGAMELIB_API ItemManagerClient : public  ItemManager
{
public:
	ItemManagerClient();
	~ItemManagerClient();

public:
	virtual BOOL					Init()=0;
	virtual VOID					Release()=0;
	//virtual VOID					Render()=0;
	//virtual VOID					Update()=0;

public:


public:
	virtual BaseContainer *				GetContainer				(SLOTINDEX ContainerIndex )=0;
   virtual SkillSlot *					GetSkillSlot				(CODETYPE code)=0;
	virtual BOOL							GetItemDesc					(SERIALTYPE serial, SLOTINDEX & slotIdxOut, SLOTPOS & posOut )=0;
	virtual BOOL							GetEmptySlotPos			(SLOTINDEX AtSlotIdx, SLOTPOS & OUT rPosOut)=0;
	virtual BOOL							GetEquipPosition			(BaseSlot & rSlot, SLOTPOS & OUT rEqPosOut)=0;

	// sound	
	virtual DWORD							GetItemSound	(eSLOT_SOUND_TYPE eSoundType, ItemSlot * pItemSlot)=0;
	virtual DWORD							GetItemSound	(eSLOT_SOUND_TYPE eSoundType, DWORD dwItemType)=0;
	virtual DWORD							GetSkillSound	(eSLOT_SOUND_TYPE eSoundType, SkillSlot * pSkillSlot)=0;
	virtual VOID							PlaySlotSound	(eSLOT_SOUND_TYPE eSoundType, BaseSlot * pSlot )=0;

	// 
	virtual BOOL							InsertInventoryItems	( eTOTALINFO_INSERT_TYPE eType, sTOTALINFO_INVENTORY & rTotalInfo)=0;
	virtual BOOL							InsertEventItems		( sTOTALINFO_EVENT_ITEM & rTotalInfo)=0;
	virtual BOOL							ExistEmptySlotAllInventory()=0;

public:
	// 网络消息处理
	virtual VOID							ParsePacket				(MSG_BASE * pMsg )=0;


public:
	/*////////////////////////////////////////////////////////////////////////
	@name Item移动处理
		上装、下装本身亦看作为物品移动处理的其中一类
	/*////////////////////////////////////////////////////////////////////////
	virtual VOID							MoveItem( SLOTINDEX fromIdx, SLOTINDEX toIdx, SLOTPOS fromPos, SLOTPOS toPos )=0;								


public:
	/*////////////////////////////////////////////////////////////////////////
	@name 快速栏物品链接处理
	/*////////////////////////////////////////////////////////////////////////
	virtual VOID							LinkItemToQuick(SLOTINDEX  atIndex
																	,SLOTPOS  OrgPos
																	,SLOTPOS  toQuickPos
																	,SLOTCODE OrgCode=0)=0;

	/*////////////////////////////////////////////////////////////////////////
	@name 快速栏技能链接处理
	/*////////////////////////////////////////////////////////////////////////
	virtual VOID							LinkSkillToQuick	(SLOTINDEX  atIndex
																		,SLOTCODE OrgCode
																		,SLOTPOS  toQuickPos)=0; 

	/*////////////////////////////////////////////////////////////////////////
	@name 快速栏心法链接处理
	/*////////////////////////////////////////////////////////////////////////
	virtual VOID							LinkStyle		(SLOTCODE OrgCode, SLOTPOS toQuickPos )=0; 


	// Quick/Style处理
	virtual VOID							MoveLinkItem	(SLOTINDEX AtIndex
																	,SLOTPOS fromPos
																	,SLOTPOS toQuickPos)=0;

	// Dummy Item 处理
	virtual VOID							LinkDummyItem	(SLOTINDEX fromIdx
                                                   ,SLOTINDEX toIdx
																	,SLOTPOS fromPos
																	,SLOTPOS toPos)=0;								


public:
	//////////////////////////////////////////////////////////////
	///@name 商店物品处理
	virtual VOID						TradeBuyItem	(SLOTINDEX fromIdx
																,SLOTINDEX toIdx
																,SLOTPOS fromPos
																,SLOTPOS toPos)=0;

	virtual VOID						TradeSellItem	(SLOTINDEX fromIdx
																,SLOTINDEX toIdx
																,SLOTPOS fromPos
																,SLOTPOS toPos)=0;


public:
	//////////////////////////////////////////////////////////////
	///@name 物品使用处理
	virtual VOID						UseInventoryItem( SLOTINDEX AtIndex, SLOTPOS posIndex )=0;
	virtual VOID						UseEquipmentItem( SLOTINDEX AtIndex, SLOTPOS posIndex )=0;

	virtual VOID						EquipItem(SLOTINDEX AtIndex
                                          ,SLOTPOS posIndex)		=0;

public:
	//////////////////////////////////////////////////////////////
	virtual VOID						CopyItem	(SLOTINDEX fromIdx
                                          ,SLOTINDEX toIdx
														,SLOTPOS fromPos
														,SLOTPOS toPos)=0;

public:
	//////////////////////////////////////////////////////////////
	// Item属性存取
	virtual BOOL					CanEquip			(SLOTINDEX AtIndex
                                             ,SLOTPOS posIndex
															,SLOTPOS ToPos)=0;
	virtual BOOL					CanEquipClass	( sITEMINFO_BASE * pInfo ) const=0;
	virtual BOOL					CanEquipLevel	( sITEMINFO_BASE * pInfo ) const=0;


	///物品属性限制检验


	virtual BOOL					CanLinkQuick		(SLOTINDEX           AtIndex
                                                ,SLOTPOS           ToPos
																,const BaseSlot * IN pSlot)=0;
	virtual BOOL					CanLinkStyleQuick	(SLOTINDEX           AtIndex
                                                ,SLOTPOS           ToPos
																,const BaseSlot * IN pSlot)=0;

	//BOOL								CanUse( SLOTINDEX AtIndex, SLOTPOS posIndex)=0;
	


public:
	/////////////////////////////////////////////////////////
	virtual BOOL					IsExistItem						(SLOTINDEX  FromSlotIdx
                                                         ,CODETYPE ItemCode
																			,int      iItemNum)=0;
	virtual BOOL					SpendItem						(SLOTINDEX  FromSlotIdx
                                                         ,CODETYPE ItemCode
																			,int      iItemNum)=0;

	
	//物品重叠处理
	virtual int						GetItemToltalAmount	(CODETYPE          code
                                                   ,SLOTINDEX AtIndex = SI_INVENTORY)		=0;
	virtual BOOL					LocateItemAtFirst			(CODETYPE          code
																	,SLOTPOS & OUT     rOutPos
																	,SLOTINDEX AtIndex = SI_INVENTORY)=0;
	virtual VOID					UpdateQuickInfo		()=0;


	//
	virtual VOID					UpdateQuickForEquips	()=0;
	virtual VOID					UpdateQuickItems			()=0;
	//--

public:
	///物品掉落
	virtual BOOL					CreateFieldItem( const sITEMINFO_RENDER * pItemInfo )=0;




	//////////////////////////////////////////////////////////////////////////
	// NAK 物品信息出错处理
	virtual VOID					ProcessItemError(DWORD dwErrCode)=0;

protected:
	virtual VOID					_MoveLinkItem	(SLOTINDEX AtIndex
                                             ,SLOTPOS fromQuickPos
															,SLOTPOS toQuickPos)=0;
	virtual VOID					_MoveLinkStyle	(SLOTINDEX AtIndex
                                             ,SLOTPOS fromQuickPos
															,SLOTPOS toQuickPos)=0;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(ItemManagerClient,ItemManager , _CLIENTGAMELIB_API);
//extern _CLIENTGAMELIB_API ItemManager& singleton::GetItemManager();
#define theItemManager  singleton::GetItemManager()


#endif //__ITEMMANAGER_H__
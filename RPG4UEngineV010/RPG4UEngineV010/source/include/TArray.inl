/*////////////////////////////////////////////////////////////////////////
文 件 名：TArray.inl
创建日期：2008年1月25日
最后更新：2008年1月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TARRAY_INL__
#define __TARRAY_INL__
#pragma once


//------------------------------------------------------------------------------
template<class TYPE>
TArray<TYPE>::TArray() 
:m_GrowSize(16)
,m_AllocSize(0)
,m_ElementNum(0)
,m_Flags(0)
{
    m_pElements = 0;
}

//------------------------------------------------------------------------------
template<class TYPE>
TArray<TYPE>::TArray(int initialSize, int grow) 
: m_GrowSize(grow)
, m_AllocSize(initialSize)
, m_ElementNum(0)
, m_Flags(0)
{
    ASSERT(initialSize >= 0);
    if (initialSize > 0)
    {
        m_pElements = new TYPE[m_AllocSize];
    }
    else
    {
        m_pElements = 0;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
TArray<TYPE>::TArray(int initialSize, int grow, TYPE initialValue) 
:m_GrowSize(grow)
,m_AllocSize(initialSize)
,m_ElementNum(initialSize)
,m_Flags(0)
{
    ASSERT(initialSize >= 0);
    if (initialSize > 0)
    {
        m_pElements = new TYPE[m_AllocSize];
        int i;
        for (i = 0; i < initialSize; i++)
        {
            m_pElements[i] = initialValue;
        }
    }
    else
    {
        m_pElements = 0;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Copy(const TArray<TYPE>& src)
{
    ASSERT(0 == m_pElements);

    m_GrowSize    = src.m_GrowSize;
    m_AllocSize   = src.m_AllocSize;
    m_ElementNum = src.m_ElementNum;
    m_Flags       = src.m_Flags;
    if (m_AllocSize > 0)
    {
        m_pElements = new TYPE[m_AllocSize];
        int i;
        for (i = 0; i < m_ElementNum; i++)
        {
            m_pElements[i] = src.m_pElements[i];
        }
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Delete()
{
    m_GrowSize = 0;
    m_AllocSize = 0;
    m_ElementNum = 0;
    m_Flags = 0;
    if (m_pElements)
    {
        delete[] m_pElements;
        m_pElements = 0;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Destroy(TYPE* elm)
{
	elm;
   elm->~TYPE();
}

//------------------------------------------------------------------------------
template<class TYPE>
TArray<TYPE>::TArray(const TArray<TYPE>& rhs) 
:m_GrowSize(0)
,m_AllocSize(0)
,m_ElementNum(0)
,m_pElements(0)
,m_Flags(0)
{
    Copy(rhs);
}

//------------------------------------------------------------------------------
template<class TYPE>
TArray<TYPE>::~TArray()
{
    Delete();
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::SetFlags(int f)
{
    m_Flags = f;
}

//------------------------------------------------------------------------------
template<class TYPE>
int TArray<TYPE>::GetFlags() const
{
    return m_Flags;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Reallocate(int initialSize, int grow)
{
    Delete();
    m_GrowSize    = grow;
    m_AllocSize   = initialSize;
    m_ElementNum = 0;
    if (initialSize > 0)
    {
        m_pElements = new TYPE[initialSize];
    }
    else
    {
        m_pElements = 0;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::SetFixedSize(int size)
{
    Reallocate(size, 0);
    m_ElementNum = size;
}

//------------------------------------------------------------------------------
template<class TYPE>
TArray<TYPE>& TArray<TYPE>::operator=(const TArray<TYPE>& rhs)
{
    Delete();
    Copy(rhs);
    return *this;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::GrowTo(int newAllocSize)
{
    TYPE* newArray = new TYPE[newAllocSize];

    if (m_pElements)
    {
        // copy over contents
        int i;
        for (i = 0; i < m_ElementNum; i++)
        {
            newArray[i] = m_pElements[i];
        }

        // discard old array and update contents
        delete[] m_pElements;
    }
    m_pElements  = newArray;
    m_AllocSize = newAllocSize;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Grow()
{
    ASSERT(m_GrowSize > 0);
    int growToSize;
    if ((DoubleGrowSize & m_Flags) != 0)
    {
        // double growth behaviour
        if (0 == m_AllocSize)
        {
            growToSize = m_GrowSize;
        }
        else
        {
            growToSize = 2 * m_AllocSize;
        }
    }
    else
    {
        // classic linear growth behaviour
        growToSize = m_AllocSize + m_GrowSize;
    }
    GrowTo(growToSize);
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Move(int fromIndex, int toIndex)
{
    ASSERT(m_pElements);
    ASSERT(fromIndex < m_ElementNum);

    // nothing to move?
    if (fromIndex == toIndex)
    {
        return;
    }

    // compute number of m_pElements to move
    int num = m_ElementNum - fromIndex;

    // check if array needs to grow
    int neededSize = toIndex + num;
    while (neededSize >= m_AllocSize)
    {
        Grow();
    }

    if (fromIndex > toIndex)
    {
        // this is a backward move
        int i;
        for (i = 0; i < num; i++)
        {
            m_pElements[toIndex + i] = m_pElements[fromIndex + i];
        }

        // destroy remaining m_pElements
        for (i = (fromIndex + i) - 1; i < m_ElementNum; i++)
        {
            Destroy(&(m_pElements[i]));
        }
    }
    else
    {
        // this is a forward move
        int i;
        for (i = num - 1; i >= 0; --i)
        {
            m_pElements[toIndex + i] = m_pElements[fromIndex + i];
        }

        // destroy freed m_pElements
        for (i = fromIndex; i < toIndex; i++)
        {
            Destroy(&(m_pElements[i]));
        }
    }

    // adjust array size
    m_ElementNum = toIndex + num;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::MoveQuick(int fromIndex, int toIndex)
{
    ASSERT(m_pElements);
    ASSERT(fromIndex < m_ElementNum);

    // compute number of m_pElements to move
    int num = m_ElementNum - fromIndex;

    // nothing to move?
    if (fromIndex == toIndex)
    {
        return;
    }

    // do a direct memory move
    memmove(&(m_pElements[toIndex]), &(m_pElements[fromIndex]), num * sizeof(TYPE));

    // adjust array size
    m_ElementNum = toIndex + num;
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE& TArray<TYPE>::PushBack(const TYPE& elm)
{
    // grow allocated space if exhausted
    if (m_ElementNum == m_AllocSize)
    {
        Grow();
    }
    ASSERT(m_pElements);
    m_pElements[m_ElementNum] = elm;
    return m_pElements[m_ElementNum++];
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Append(const TYPE& elm)
{
    // grow allocated space if exhausted
    if (m_ElementNum == m_AllocSize)
    {
        Grow();
    }
    ASSERT(m_pElements);
    m_pElements[m_ElementNum++] = elm;
}

//------------------------------------------------------------------------------
template<class TYPE>
typename TArray<TYPE>::iterator TArray<TYPE>::Reserve(int num)
{
    ASSERT(num > 0);
    int maxElement = m_ElementNum + num;
    while (maxElement >= m_AllocSize)
    {
        Grow();
    }
    ASSERT(m_pElements);
    iterator iter = m_pElements + m_ElementNum;
    m_ElementNum += num;
    return iter;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::CheckIndex(int index)
{
    if (index >= m_ElementNum)
    {
        // grow array if necessary
        if (index >= m_AllocSize)
        {
            ASSERT(m_GrowSize > 0);
            GrowTo(index + m_GrowSize);
        }
        // update number of contained m_pElements
        m_ElementNum = index + 1;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE& TArray<TYPE>::Set(int index, const TYPE& elm)
{
    ASSERT(index < m_ElementNum);
    CheckIndex(index);
    m_pElements[index] = elm;
    return m_pElements[index];
}

//------------------------------------------------------------------------------
template<class TYPE>
int TArray<TYPE>::Size() const
{
    return m_ElementNum;
}

//------------------------------------------------------------------------------
template<class TYPE>
int TArray<TYPE>::AllocSize() const
{
    return m_AllocSize;
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE& TArray<TYPE>::At(int index)
{
    CheckIndex(index);
    return m_pElements[index];
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE& TArray<TYPE>::operator[](int index) const
{
    ASSERT(m_pElements && (index >= 0) && (index < m_ElementNum));
    return m_pElements[index];
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE& TArray<TYPE>::Front() const
{
    ASSERT(m_pElements && (m_ElementNum > 0));
    return m_pElements[0];
}

//------------------------------------------------------------------------------
template<class TYPE>
TYPE& TArray<TYPE>::Back() const
{
    ASSERT(m_pElements && (m_ElementNum > 0));
    return m_pElements[m_ElementNum - 1];
}

//------------------------------------------------------------------------------
template<class TYPE>
bool TArray<TYPE>::Empty() const
{
    return (m_ElementNum == 0);
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Erase(int index)
{
    ASSERT(m_pElements && (index >= 0) && (index < m_ElementNum));
    if (index == (m_ElementNum - 1))
    {
        // special case: last element
        Destroy(&(m_pElements[index]));
        m_ElementNum--;
    }
    else
    {
        Move(index + 1, index);
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::EraseQuick(int index)
{
    ASSERT(m_pElements && (index >= 0) && (index < m_ElementNum));
    if (index == (m_ElementNum - 1))
    {
        // special case: last element
        m_ElementNum--;
    }
    else
    {
        MoveQuick(index + 1, index);
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
typename TArray<TYPE>::iterator TArray<TYPE>::Erase(typename TArray<TYPE>::iterator iter)
{
    ASSERT(m_pElements && (iter >= m_pElements) && (iter < (m_pElements + m_ElementNum)));
    Erase(iter - m_pElements);
    return iter;
}

//------------------------------------------------------------------------------
template<class TYPE>
typename TArray<TYPE>::iterator TArray<TYPE>::EraseQuick(typename TArray<TYPE>::iterator iter)
{
    ASSERT(m_pElements && (iter >= m_pElements) && (iter < (m_pElements + m_ElementNum)));
    EraseQuick(iter - m_pElements);
    return iter;
}


//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Insert(int index, const TYPE& elm)
{
    ASSERT((index >= 0) && (index <= m_ElementNum));
    if (index == m_ElementNum)
    {
        // special case: append element to back
        PushBack(elm);
    }
    else
    {
        Move(index, index + 1);
        m_pElements[index] = elm;
    }
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Clear()
{
    int i;
    for (i = 0; i < m_ElementNum; i++)
    {
        Destroy(&(m_pElements[i]));
    }
    m_ElementNum = 0;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Reset()
{
    m_ElementNum = 0;
}

//------------------------------------------------------------------------------
template<class TYPE>
typename TArray<TYPE>::iterator TArray<TYPE>::Begin() const
{
    return m_pElements;
}

//------------------------------------------------------------------------------
template<class TYPE>
typename TArray<TYPE>::iterator TArray<TYPE>::End() const
{
    return m_pElements + m_ElementNum;
}

//------------------------------------------------------------------------------
template<class TYPE>
typename TArray<TYPE>::iterator TArray<TYPE>::Find(const TYPE& elm) const
{
    int index;
    for (index = 0; index < m_ElementNum; index++)
    {
        if (m_pElements[index] == elm)
        {
            return &(m_pElements[index]);
        }
    }
    return 0;
}

//------------------------------------------------------------------------------
template<class TYPE>
int TArray<TYPE>::FindIndex(const TYPE& elm) const
{
    int index;
    for (index = 0; index < m_ElementNum; index++)
    {
        if (m_pElements[index] == elm)
        {
            return index;
        }
    }
    return -1;
}

//------------------------------------------------------------------------------
template<class TYPE>
void TArray<TYPE>::Fill(int first, int num, const TYPE& elm)
{
    if ((first + num) > m_ElementNum)
    {
        GrowTo(first + num);
    }
    int i;
    for (i = first; i < (first + num); i++)
    {
        m_pElements[i] = elm;
    }
}



#endif //__TARRAY_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：Quaternion.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __QUATERNION_INL__
#define __QUATERNION_INL__
#pragma once

//namespace math
//{ 

inline Quaternion::Quaternion( )
{
	Identity();
}

inline Quaternion::Quaternion( CONST REAL* pf )
{
	Set(pf);
}

inline Quaternion::Quaternion( CONST QUATERNION& q )
{
	Set(q);
}
inline Quaternion::Quaternion( const Matrix4& mIn )
{
	FromMatrix(mIn);
}


inline Quaternion::Quaternion( REAL fx, REAL fy, REAL fz, REAL fw )
{
	Set(fx,fy,fz,fw);
}

///////////////////////////////////////////////////
inline Quaternion& Quaternion::Set( REAL fx, REAL fy, REAL fz, REAL fw )
{
    x = fx;
    y = fy;
    z = fz;
    w = fw;
    return *this;
}

inline Quaternion& Quaternion::Set( CONST REAL * pf)
{
    x = pf[0];
    y = pf[1];
    z = pf[2];
    w = pf[3];
	 return *this;
}

inline Quaternion& Quaternion::Set( CONST QUATERNION& q )
{
    x = q.x;
    y = q.y;
    z = q.z;
    w = q.w;
	 return *this;
}

inline Quaternion& Quaternion::Set( const Matrix4& mIn )
{
	return FromMatrix(mIn);
}

///////////////////////////////////////////////////
inline Quaternion& Quaternion::operator()( CONST REAL * pf )
{
	return Set(pf);
}

inline Quaternion& Quaternion::operator()( CONST QUATERNION& q )
{
	return Set(q);
}

inline Quaternion& Quaternion::operator()( const Matrix4& mIn )
{
	return FromMatrix(mIn);
}

inline Quaternion& Quaternion::operator()( REAL fx, REAL fy, REAL fz, REAL fw )
{
	return Set(fx,fy,fz,fw);
}

// casting
inline Quaternion::operator REAL* ()
{
    return (REAL *) &x;
}

inline Quaternion::operator CONST REAL* () const
{
    return (CONST REAL *) &x;
}


// assignment operators
inline Quaternion& Quaternion::operator += ( CONST Quaternion& q )
{
    x += q.x;
    y += q.y;
    z += q.z;
    w += q.w;
    return *this;
}

inline Quaternion& Quaternion::operator -= ( CONST Quaternion& q )
{
    x -= q.x;
    y -= q.y;
    z -= q.z;
    w -= q.w;
    return *this;
}

inline Quaternion& Quaternion::operator *= ( CONST Quaternion& q )
{
    Quaternion qT = *this;
	 Multiply(qT,q);
    return *this;
}

inline Quaternion& Quaternion::operator *= ( REAL f )
{
    x *= f;
    y *= f;
    z *= f;
    w *= f;
    return *this;
}

inline Quaternion& Quaternion::operator /= ( REAL f )
{
    float fInv = 1.0f / (float)f;
    x = (REAL)(x *fInv);
    y = (REAL)(y *fInv);
    z = (REAL)(z *fInv);
    w = (REAL)(w *fInv);
    //x *= fInv;
    //y *= fInv;
    //z *= fInv;
    //w *= fInv;
    return *this;
}


// unary operators
inline Quaternion Quaternion::operator + () const
{
    return *this;
}

inline Quaternion Quaternion::operator - () const
{
    return Quaternion(-x, -y, -z, -w);
}


// binary operators
inline Quaternion Quaternion::operator + ( CONST Quaternion& q ) const
{
    return Quaternion(x + q.x, y + q.y, z + q.z, w + q.w);
}

inline Quaternion Quaternion::operator - ( CONST Quaternion& q ) const
{
    return Quaternion(x - q.x, y - q.y, z - q.z, w - q.w);
}

inline Quaternion Quaternion::operator * ( CONST Quaternion& q ) const
{
    Quaternion qT;
	 qT.Multiply(*this,q);
    return qT;
}

inline Vector3D	Quaternion::operator * ( const VECTOR3D& vDir) 
{
	return Multiply(vDir);
}

inline Quaternion Quaternion::operator * ( REAL f ) const
{
    return Quaternion(x * f, y * f, z * f, w * f);
}

inline Quaternion Quaternion::operator / ( REAL f ) const
{
    float fInv = 1.0f / (float)f;
    return Quaternion(x * fInv, y * fInv, z * fInv, w * fInv);
}



inline BOOL Quaternion::operator == ( CONST Quaternion& q ) const
{
    return x == q.x && y == q.y && z == q.z && w == q.w;
}

inline BOOL Quaternion::operator != ( CONST Quaternion& q ) const
{
    return x != q.x || y != q.y || z != q.z || w != q.w;
}

inline Quaternion& Quaternion::Identity()
{
    x = 0.f;
    y = 0.f;
    z = 0.f;
    w = 1.f;
    return *this;
}

inline Quaternion& Quaternion::Conjugate	(void)
{
	x = -x;
	y = -y;
	z = -z;
	return *this;
}
inline Quaternion& Quaternion::Scaling		(float s)
{
	x *= s;
	y *= s;
	z *= s;
	w *= s;
	return *this;
}

inline REAL Quaternion::DotProduct	(const Quaternion& q) const
{
	return w*q.w + x*q.x + y*q.y + z*q.z;
}

inline REAL Quaternion::Norm(const Quaternion& q)
{
	return x * q.x + y * q.y + z * q.z + w * q.w;
}

inline float Quaternion::Magnitude(void) 
{
	float n = Norm(*this);
	if (n > 0.0f) return math::Sqrt(n);
	else          return 0.0f;
};

inline Quaternion& Quaternion::Normalize()
{
	float l = Magnitude();
	if (l > 0.0f) Scaling(1.0f / l);
	else          Set(0.0f,0.0f,0.0f,1.0f);
	return *this;
}

inline Quaternion& Quaternion::Inverse		()
{
	float n = Norm(*this);
	if (n > 0.0f) Scaling(1.0f / n);
	Conjugate();
	return *this;
}


inline BOOL Quaternion::IsEqual(const Quaternion& q, float tol) const
{
	if (fabs(q.x-x) > tol)      return FALSE;
	else if (fabs(q.y-y) > tol) return FALSE;
	else if (fabs(q.z-z) > tol) return FALSE;
	else if (fabs(q.w-w) > tol) return FALSE;
	return TRUE;
}



//};//namespace math


#endif //__QUATERNION_INL__
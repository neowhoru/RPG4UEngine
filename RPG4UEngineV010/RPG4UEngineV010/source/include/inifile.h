/*////////////////////////////////////////////////////////////////////////
文 件 名：IniFile.h
创建日期：2006年9月18日
最后更新：2006年9月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __INIFILE_H__
#define __INIFILE_H__
#pragma once

#include "IniFileDefine.h"
#include "ScriptWord.h"

////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////
class _BASE_API IniFile
{
public:
	IniFile(void);
	~IniFile(void);

public:
	BOOL	LoadFromFile	( LPCSTR szFileName );
	BOOL	LoadFrom			( BYTE* pbyBuffer, int nBufferSize );
	BOOL	SaveToFile		( LPCSTR szFileName );



	int	ReadLine				(char* pchLine
                           ,char  szBuffer[]
									,int   nBufferSize);
	ScriptWord* GetWords		( LPCSTR pszName );
	
public:
	BOOL	OpenSession			( LPCSTR pszSessionName, BOOL bCreateIfNull=FALSE );
	BOOL	OpenSession			( int i );

	TextPointerMap*			GetSessionList();

public:
	LPCSTR GetStringValue( LPCSTR pszName );
	BOOL	GetStringValue	( LPCSTR pszName, LPCSTR pszDefault, char szBuffer[], int nBufferSize );
	int	GetIntValue		( LPCSTR pszName );
	float	GetFloatValue	( LPCSTR pszName );

	BOOL  SetValue			(LPCSTR  pszName , LPCTSTR szText);
	BOOL  SetIntValue		(LPCSTR  pszName , INT		nValue);
	BOOL  SetFloatValue	(LPCSTR  pszName , float	fValue);

	BOOL	HasKey			( LPCSTR pszName );
public:
	LPCSTR GetStringValueAt	( int nLine );
	LPCSTR GetKeyNameAt		( int nLine );

	int		GetSessionCount				();
	int		GetWordCount					();
	int		GetLineCountOfCurSession	();
	LPCSTR	GetNameOfCurSession			();
	
public:
	template<typename Type>
	IniFile&	operator()	(LPCSTR	szName, Type tDefault);
	IniFile&	operator()	(LPCSTR	szName, float fDefault);
	IniFile&	operator()	(LPCSTR	szName, LPCSTR szDefault);
	IniFile&	operator()	(DWORD	dwBuffLen);

	////////////////////////////////////////////
	template<typename Type> 
	IniFile&	operator	>>	(Type&	buf);
	IniFile&	operator	>>	(float&	buf);
	IniFile&	operator	>>	(LPCSTR& buf);
	IniFile&	operator	>>	(TCHAR	buf[]);
	IniFile&	operator	>>	(VRSTR&	sBuf);
	IniFile&	operator	>>	(StringHandle&	sBuf);

	////////////////////////////////////////////
	template<typename Type> 
	IniFile&	operator	<<	(Type				buf);
	IniFile&	operator	<<	(float			buf);
	IniFile&	operator	<<	(LPCSTR			buf);
	IniFile&	operator	<<	(TCHAR			buf[]);
	IniFile&	operator	<<	(VRSTR			sBuf);
	IniFile&	operator	<<	(StringHandle&	sBuf);

private:
	sINI_SESSION*	_RegSession	(LPCTSTR szSession);
	BOOL				_RegWord		(ScriptWord*	pWords, BOOL bReplace);

protected:
	vector<ScriptWord*>			m_WordList;
	sINI_SESSION*					m_pCurrentSession;
	TextPointerMap					m_SessionList;
	vector<sINI_SESSION*>		m_Sessions;

	LPCSTR							m_pCurrentName;
	DWORD								m_dwBufLen;
	union
	{
		LPCSTR						m_pszDefault;
		float							m_fDefault;
		DWORD							m_dwDefault;
	};
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "IniFile.inl"

#endif //__INIFILE_H__
#ifndef _FORMULARATIOPARSER_H_
#define _FORMULARATIOPARSER_H_


#pragma once

#include <THashTable.h>
#include <BaseResParser.h>

enum RATIO_TYPE
{
	RATIO_TYPE_EXP			= 1,		
	RATIO_TYPE_ADDEXP		= 2,					
	RATIO_TYPE_DROP			= 101,
};


class _RESOURCELIB_API FormulaRatioParser : public BaseResParser
{
public:
	FormulaRatioParser();
	virtual ~FormulaRatioParser();

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sFORMULA_RATIOINFO_BASE, m_pRatioHashTable, m_InfoCode);

public:
	virtual VOID					Init	( DWORD dwPoolSize );
	virtual VOID					Release();

	virtual BOOL					Load	( LPCTSTR pszFileName, BOOL bReload = FALSE );
	virtual VOID					Reload();
	virtual VOID					Unload();

public:
	 sFORMULA_RATIOINFO_BASE*	GetFormulaRatio( DWORD wCode ) { return m_pRatioHashTable->GetData( wCode ); }

private:
	util::THashTable<sFORMULA_RATIOINFO_BASE *> *	m_pRatioHashTable;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(FormulaRatioParser , _RESOURCELIB_API);
//extern API_NULL FormulaRatioParser& singleton::GetFormulaRatioParser();
#define theFormulaRatioParser  singleton::GetFormulaRatioParser()


#endif // _FORMULARATIOPARSER_H_
/*////////////////////////////////////////////////////////////////////////
文 件 名：SkillEffectInfoManager.h
创建日期：2007年3月1日
最后更新：2007年3月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	效果结构
	技能表中的效果ID

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SKILLEFFECTRESMANAGER_H__
#define __SKILLEFFECTRESMANAGER_H__
#pragma once

using namespace std;

#include "ConstArray.h"
#include "THashTable.h"
#include <BaseResParser.h>

using namespace util;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API SkillEffectInfoManager : public BaseResParser
{
public:
	enum AttachPart
	{
		 AttachNone			= 0
		,AttachBody			= 1	// 附着在身体
		,AttachRightHand	= 2	// 附着在右手
		,AttachLeftHand	= 3	// 附着在左手
		,AttachTwoHand		= 4	// 附着在双手
		,AttachFoot			= 5	// 附着在脚下，魔法阵
		,AttachMax	
	};
	enum CastType			
	{
		 CastTypeUnknown		= 0
		,CastFollow				= 1// 跟随
		,CastExplode			= 2// 爆炸
		,CastLaser				= 3// 激光
		,CastMissile			= 4// 导弹，有弹道
		,CastAttach				= 5// 绑定效果，
		,CastMax
	};
	enum PlayMode
	{
		 PlayModeUnknown		= 0
		,PlayOneTime			= 1// 播放一次
		,PlayLoop				= 2// 循环播放，显式停止
		,PlayModeMax
	};

	enum CastTargetType
	{
		 casttarget_onlyone		= 0//只产生一个特效
		,casttarget_oneforone	= 1//每个目标一个特效
		,casttarget_max
	};

	struct sSKILLEFFECT_INFO
	{
		DWORD		dwId;
		DWORD		dwModelId;
		DWORD		dwAttachPart;
		DWORD		dwCastType;
		DWORD		dwPlayMode;		
		DWORD		dwCastTargetType;
		int 		iHitEffect;//发射效果，命中时在目标位置创建一个特效
		float		fFlySpeed;// 发射型特效飞行速度， 
		VRSTR		strCastSound;	//发射音效
		VRSTR		strHitSound;	//命中音效

		sSKILLEFFECT_INFO()
			:dwId(0xffffffff)
			,dwModelId(0xffffffff)
			,dwAttachPart(AttachNone)
			,dwCastType(CastTypeUnknown)
			,dwPlayMode(PlayModeUnknown)
			,dwCastTargetType(casttarget_onlyone )
			,fFlySpeed(0.0f)
		{
		}


		LPCSTR GetCastSound	(){return (LPCSTR)strCastSound;}
		LPCSTR GetHitSound	(){return (LPCSTR)strHitSound;}
	};

public:
	SkillEffectInfoManager(void);
	virtual ~SkillEffectInfoManager(void);


public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sSKILLEFFECT_INFO, m_pDataInfos, dwId);
	BASERES_UNIT_TABLE	(sSKILLEFFECT_INFO, EffectInfo);

public:
	virtual BOOL		Load( const char* szFileName );
	virtual void		Clear( void );
	

protected:
	sSKILLEFFECT_INFO		m_recordDefault;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
CONSTARRAY_DECLARE(AttachPart
						,SkillEffectInfoManager::AttachPart
						,SkillEffectInfoManager::AttachNone
						,SkillEffectInfoManager::AttachMax
						,_RESOURCELIB_API);

///////////////////////////////////////////////////////////
CONSTARRAY_DECLARE(CastType
						,SkillEffectInfoManager::CastType
						,SkillEffectInfoManager::CastTypeUnknown
						,SkillEffectInfoManager::CastMax
						,_RESOURCELIB_API);

///////////////////////////////////////////////////////////
CONSTARRAY_DECLARE(PlayMode
						,SkillEffectInfoManager::PlayMode
						,SkillEffectInfoManager::PlayModeUnknown
						,SkillEffectInfoManager::PlayModeMax
						,_RESOURCELIB_API);

///////////////////////////////////////////////////////////
CONSTARRAY_DECLARE(CastTargetType
						,SkillEffectInfoManager::CastTargetType
						,SkillEffectInfoManager::casttarget_onlyone
						,SkillEffectInfoManager::casttarget_max
						,_RESOURCELIB_API);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(SkillEffectInfoManager , _RESOURCELIB_API);
//extern _RESOURCELIB_API SkillEffectInfoManager& GetSkillEffectInfoManager();
#define theSkillEffectInfoManager GetSkillEffectInfoManager()

#endif //__SKILLEFFECTRESMANAGER_H__
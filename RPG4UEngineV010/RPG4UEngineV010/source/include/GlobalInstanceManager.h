/*////////////////////////////////////////////////////////////////////////
文 件 名：GlobalInstanceManager.h
创建日期：2007年8月23日
最后更新：2007年8月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	负责批量全局实例，主要有以下特点：
	1.连续块形式，批量生成实例对象
	2.有序地进行实例构造
		可通过指定全局对象优先级
	3.反序地进行析构
		参考2点
	4.全局实例声明与实例都很方便
		a.实例型
			声明 GLOBALINST_DECL
			实现 GLOBALINST_IMPL
		b.指针型
			这要求须手工实现对象实例
			声明 GLOBALINST_DECL
			指针实现 GLOBALINSTPTR_IMPL  GLOBALINSTPTR_INIT
		只要通过以下宏，便能实现全局对象，安全有序；同时宏里面可以指定优先级
	5.GlobalInst与PluginManager紧密结合在一起，保证外部插件也能正常使用GlobalInst
		详情可参阅：ApplicationStartUp
	6.主程序中，只需要建立ApplicationStartUp实例即可，譬如ServerMain中代码如下：
		ApplicationSetting					appSetting;
		gamemain::ApplicationStartUp		appStartup;

		appSetting.LoadSetting();
		appStartup.StartUp(&appSetting);

		nRet = gamemain::ServerRun(hInstance,hPrevInstance,lpCmdLine,nCmdShow);
		appStartup.Shutdown();


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GLOBALINSTANCEMANAGER_H__
#define __GLOBALINSTANCEMANAGER_H__
#pragma once

#include "GlobalInstanceDefine.h"
#include "GlobalInstanceLinker.h"
#include "PluginScanListener.h"
#include "TLinkerManager.h"


namespace gamemain
{
typedef TLinkerListenerManager<GlobalInstanceLinker,PluginScanListener>  GlobalInstanceManagerBase;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API GlobalInstanceManager : public GlobalInstanceManagerBase
{
public:
	GlobalInstanceManager();
	virtual ~GlobalInstanceManager();

	void Initialize();
	void Release();

	static bool SortLinkerList(LinkerBase* s1, LinkerBase* s2);
	void ProcessLinker(ELinkerProcess ePro);

	//virtual void OnBeginNewLibrary();
	//virtual void OnEndNewLibrary();
	//virtual void PrevScanPlugin(){};
	//virtual void AfterScanPlugin(){};

protected:

	virtual void PrevScanLinkers();
	virtual void OnLinkerScan(GlobalInstanceLinker* pLinker);
	virtual void AfterScanLinkers();
	void IntegrateLinker(){ScanLinkers(TRUE);}

protected:
	//typedef std::list<GlobalInstanceLinker*>				GlobalInstanceLinkerList;
	typedef LinkerBaseList										GlobalInstanceLinkerList;
	typedef GlobalInstanceLinkerList::iterator			GlobalInstanceLinkerIt;
	typedef GlobalInstanceLinkerList::reverse_iterator	GlobalInstanceLinkerItR;
	
	GlobalInstanceLinkerList	m_ltLinkers;
	DWORD								m_dwTotalSize;
	GLOBALINST_INT*				m_pDataPtr;
	//GlobalInstanceLinkerList	m_ltHeaderLinkers;
};


GLOBALINST_DECL(GlobalInstanceManager , _BASE_API);
};


#endif //__GLOBALINSTANCEMANAGER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishDefine.h
创建日期：2008年8月6日
最后更新：2008年8月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __RESPUBLISHDEFINE_H__
#define __RESPUBLISHDEFINE_H__
#pragma once


class BaseInfoParser;

enum ePUBLISH_TYPE
{
	 ePUBLISH_LOGIC	= 0///< 逻辑信息
	,ePUBLISH_VDATA	= 1///< V资源
	,ePUBLISH_RES		= 2///< 素材
	,ePUBLISH_MAX
};
enum ePUBLISH_FLAG
{
	 ePUBLISH_RESFILE	= 1	///< 发布资源文件
	,ePUBLISH_ALLINFO	= 2	///< 发布全部信息
	,ePUBLISH_ALL		= -1
};

struct sPUBLISH_INFO
{
	enum{REF_NUM	= 5};
	CODETYPE				m_Code;					///< 资源表代码  
	BYTE					m_Type;					///< ePUBLISH_TYPE
	DWORD					m_Priority;				///< 处理优先级
	VRSTR					m_InfoPath;				///< 发布信息文件
	VRSTR					m_DestPath;				///< 目标路径
	VRSTR					m_DestFile;				///< 目标文件名
	VRSTR					m_TemplFile;			///< 模板文件名
	DWORD					m_PublishResFile;		///< 发布资源文件
	BYTE					m_RefNum;				///< 引用数量
	CODETYPE				m_RefCode[REF_NUM];	///< 资源表代码  
	BaseInfoParser*	m_Parser;	
};


struct sPUBLISH_RECORD
{
	enum{REF_CODE=0,REF_NAME=1};

	CODETYPE		m_Code;		///< 代码
	BYTE			m_RefType;	///< 引用类型
	CODETYPE		m_RefCode;	///< 引用代码
	VRSTR			m_RefName;	///< 引用名称
	void*			m_InfoDataPtr;	
};



//				0目录	
//				1文件	
//				2模式匹配	
//					目录时，才处理
//	代号	媒质	发布路径	类型	发布子目录

struct sPUBLISH_DIR
{
	enum{EXT_NUM = 5};
	enum{
		 MODE_DIR	=0	///< 目录
		,MODE_FILE	=1	///< 文件
		,MODE_RULE	=2 ///< 规则模式
	};

	CODETYPE		m_Code;				///< 代码
	BYTE			m_MediaType;		///< 媒质类型
	VRSTR			m_Path;				///< 发布目录
	BYTE			m_Mode;				///< 路径类型  
	BYTE			m_SubDir;			///< 发布目录时，是否处理子目录
	BYTE			m_ExtNum;			///< 发布指定扩展名数量
	VRSTR			m_Exts[EXT_NUM];	///< 只发布指定扩展名
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _RES_PUBLISH_DECLARE(Name,Code)\
			class ResPublish##Name : public BaseInfoParser\
			{\
			public:\
				ResPublish##Name(BaseInfoParserHelper* pImpl)\
				:BaseInfoParser(pImpl){}\
				virtual CODETYPE GetCode(){return Code;}\
				virtual void*		GetRefInfo			(VRSTR sName);\
				virtual void*		GetRefInfo			(CODETYPE code);\
				virtual BOOL		OnScanRef			(sPUBLISH_RECORD* pRecord);\
				virtual BOOL		OnPublishInfo		(Archive& file, sPUBLISH_RECORD* pRecord);

#define RES_PUBLISH_DECLARE_RES(Name,Code)\
				_RES_PUBLISH_DECLARE(Name,RESTYPE_##Code)\
				virtual BOOL		OnPublishResFile	(sPUBLISH_RECORD* pRecord);\
			};

#define RES_PUBLISH_DECLARE_NAME(Name,Code)\
				_RES_PUBLISH_DECLARE(Name,RESTYPE_##Code)\
				virtual CODETYPE	GetRefCode		(VRSTR sName);\
			};

#define RES_PUBLISH_DECLARE(Name,Code)\
				_RES_PUBLISH_DECLARE(Name,RESTYPE_##Code)\
			};

#define RES_PUBLISH_DECLARE_PARAM(Name,Code)\
				_RES_PUBLISH_DECLARE(Name,RESTYPE_##Code)\
				DWORD m_dwParam;\
			};



#endif //__RESPUBLISHDEFINE_H__
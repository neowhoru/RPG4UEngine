/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientLogin.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTLOGIN_H__
#define __PACKETSTRUCT_CLIENTLOGIN_H__
#pragma once



#include "StructBase.h"
#include "PacketStruct_Base.h"
#include <Protocol_ClientLogin.h>
#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include "PacketStruct_ClientLogin_Define.h"

#pragma once

#pragma pack(push,1)



////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(USER_READY_CMD)//,CL_LOGIN_USER_READY_CMD)
//{
	char			szServerInfo[MAX_INFO_LENGTH];	
   DWORD			dwEncKey;						// 密码编码Key，Leo 暂用连接序列号
//};
MSGPACKET_CL_END;


const int KEYLEN = 1;
////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(VERSION_CHECK_SYN)//, CL_LOGIN_VERSION_CHECK_SYN)
//{
	DWORD		dwEncKey;						// 密码编码Key，Leo 暂用连接序列号
	BYTE		byHighVersion;					// 努扼捞攫飘 滚傈
	BYTE		byMiddleVersion;
	BYTE		byLowVersion;
	char        szLocalIP[MAX_IP_LENGTH]; 
//};
MSGPACKET_CL_END;


////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(VERSION_CHECK_ACK)//,CL_LOGIN_VERSION_CHECK_ACK)
//{
	enum
	{
		 ERR_OK			= 0
		,ERR_UNKNOWN
		,ERR_VERSION_NOTMATCH
		,ERR_IP_NOTMATCH
		,ERR_SEQUENCEINVALID
		,ERR_AUTH_REPEAT
		,ERR_PROTOCOLINVALID
	};
	
	BYTE		byResult;		//OK (0) ; ERROR (Code) 
//};
MSGPACKET_CL_END;


////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(ACCOUNT_LOGIN_SYN)//,CL_LOGIN_ACCOUNT_LOGIN_SYN)
//{
   DWORD    dwAuthUserID;          
	char		szID		[MAX_ID_LENGTH];				
	char		szPasswd	[MAX_PASSWORD_LENGTH];	
//};
MSGPACKET_CL_END;


////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(ACCOUNT_LOGIN_ACK)//,CL_LOGIN_ACCOUNT_LOGIN_ACK)
//{
	enum
	{
		ERR_OK,
		ERR_UNKNOWN,
		ERR_DUPLICATED_ID,
		ERR_WRONG_ID,
		ERR_WRONG_PW,
		ERR_WRONG_LOGINING,
		ERR_WRONG_DB,
		ERR_NOTEXIST,
		ERR_PWDNOTMATCH,
	};
	
	BYTE		byResult;				
	char		szInfo[MAX_INFO_LENGTH];	
//};
MSGPACKET_CL_END;



////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(GATESTATUS_LIST_SYN)//,CL_LOGIN_GATESTATUS_LIST_SYN)
//{
//};
MSGPACKET_CL_END;


////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(GATESTATUS_LIST_ACK)//,CL_LOGIN_GATESTATUS_LIST_ACK)
//{
	//memset(szServerStatusList, 0, sizeof(szServerStatusList));
	enum
	{
		STATUS_VALUE_AMOUNT	= 255
	};

	DWORD GetDataSize() { return sizeof(MSG_BASE) + sizeof(bySvrCnt) + bySvrCnt*sizeof(BYTE);}

   BYTE        bySvrCnt;										// 服务线数量
	BYTE        szServerStatusList[MAX_SERVER_AMOUNT]; // 服务线状态数据缓冲区，每条线对应1个字节，
//};
MSGPACKET_CL_END;


////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(GATE_SELECT_SYN)//,CL_LOGIN_GATE_SELECT_SYN)
//{
	BYTE		bySelectIndex;	//选择了的世界服务器的组内目录
//};
MSGPACKET_CL_END;



////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(GATE_SELECT_ACK)//,CL_LOGIN_GATE_SELECT_ACK)
//{
	enum
	{
		ERR_OK,
		ERR_CANNT_CONNECT_SELECTED_SERVER,
		ERR_EXIST_CONNECT_INFO_RETRY
	};

	DWORD		dwAuthUserID;								// AuthServer识别码
	BYTE		szSerialKey[MAX_AUTH_SERIAL_LENGTH];	// 序列号信息
	char		szSvrIP[MAX_IP_LENGTH];						// Agent ClientSide IP
	DWORD		dwSvrPort;									// Agent ClientSide Port
   BYTE     byResult;								// OK(0) 无法连接(1) 尝试再次连接(2)
//};
MSGPACKET_CL_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
const int MAX_GROUP_CNT     = 15;	// 服务器组数量
const int MAX_SVR_CNT       = 100;	// 服务器线总数
//const int MAX_GROUP_CNT     = 32;	// 服务器组数量
//const int MAX_SVR_CNT       = 254;	// 服务器线总数
struct AUTH_SERVER_NAME
{
	char		szServerName[MAX_CHANNEL_NAME_LEN];	// 组名称
	BYTE		byGroupIdx;									// 所在位置序号

};

////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(GATENAME_LIST_SYN)//,CL_LOGIN_GATENAME_LIST_SYN)
//{
//};
MSGPACKET_CL_END;


////////////////////////////////////////
MSGPACKET_CL_LOGIN_BEGIN(GATENAME_LIST_ACK)//,CL_LOGIN_GATENAME_LIST_ACK)
//{
	enum 
	{
		S2C_BUFFER_SIZE = MAX_GROUP_CNT*MAX_WORLD_NAME_LEN + MAX_SVR_CNT*(MAX_CHANNEL_NAME_LEN+1)
	};

	inline DWORD GetDataSize(DWORD dwBufSize){return sizeof(MSG_BASE) + sizeof(BYTE)*2 + dwBufSize;}

	BYTE		byGroupCnt;							// 组数量
	BYTE		byServerCnt;						// 服务线数量
	BYTE		szBuffer[S2C_BUFFER_SIZE];		// 消息最大缓冲区 格式（空格不算）：
														//  group\0 group\0 n svr\0 n svr\0
														//  group数量由byGroupCnt决定
														//  svr数量由byServerCnt决定
														//  n为BYTE，即byGroupIdx
//};
MSGPACKET_CL_END;


#pragma pack(pop)




#endif //__PACKETSTRUCT_CLIENTLOGIN_H__
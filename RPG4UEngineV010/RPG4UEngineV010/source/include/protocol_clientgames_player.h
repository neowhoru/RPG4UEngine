/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_ClientGameS_Player.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PROTOCOL_CLIENTGAMES_PLAYER_H__
#define __PROTOCOL_CLIENTGAMES_PLAYER_H__
#pragma once

#include "Protocol_Define.h"
#include "Protocol_ClientGameS_Define.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_CHARINFO_PROTOCOL
{
	 _CG_SYN_AN	(CHARINFO_CREATE)				//创建人物
	,_CG_SYN_AN	(CHARINFO_DESTROY)			//删除人物	
	,_CG_BRD		(CHARINFO_APPREARANCE)	//人物窗变化的信息，登录人物容貌信息broadcast

													//人物信息分块传输到客户端
	,_CG_CMD		(CHARINFO_CHAR_ITEM)		//	1.物品
	,_CG_CMD		(CHARINFO_CHAR_ITEM2)	//	1.分块物品
	,_CG_CMD		(CHARINFO_SKILL)			//	2.技能信息
	,_CG_CMD		(CHARINFO_QUICK)			// 3.快速键
	,_CG_CMD		(CHARINFO_STYLE)			// 4.姿态信息
	,_CG_CMD		(CHARINFO_QUEST)			// 5.当前任务状态

	,_CG_SYN_A	(CHARINFO_SELECT_INFO)	//鼠标略过人物时，提取人物基本信息


};

/*////////////////////////////////////////////////////////////////////////
// SYNC	双端同步处理
/*////////////////////////////////////////////////////////////////////////
enum eCG_SYNC_PROTOCOL
{
	 _CG_SYN_AN	(SYNC_PLAYER_ENTER)			// 进入场景的同步请求

	,_CG_CMD		(SYNC_ALLPLAYERS		)		// 扇区内全部的玩家信息命令
	,_CG_CMD		(SYNC_VILLAGE_ALLPLAYERS)	// 村庄场景中扇区内全部玩家信息命令
	,_CG_CMD		(SYNC_ALLPLAYERS_EQUIPINFO)// 扇区的全部玩家的装备信息				
	,_CG_CMD		(SYNC_ALLMONSTERS		)	
	,_CG_CMD		(SYNC_ALLFIELDITEMS		)	



	,_CG_BRD		(SYNC_PLAYER_ENTER)			// 进入场景的后的广播信息
	,_CG_BRD		(SYNC_VILLAGE_PLAYER_ENTER)
	,_CG_BRD		(SYNC_PLAYER_LEAVE)
	,_CG_BRD		(SYNC_MONSTER_ENTER)
	,_CG_BRD		(SYNC_MONSTER_DIR_ENTER)
	,_CG_BRD		(SYNC_MONSTER_LEAVE)

	,_CG_BRD		(SYNC_FIELDITEM_ENTER)
	,_CG_BRD		(SYNC_FIELDITEM_LEAVE)



	,_CG_SYN_BN	(SYNC_MOVE)					// 在场景内移动同步请求
	,_CG_BRD		(SYNC_MOVE_THRUST)		// 被推的信息broadcasting
	,_CG_SYN_B	(SYNC_TARGET_MOVE)		// 移动目标设定请求
	,_CG_SYN_B	(SYNC_KBMOVE)				// 键盘移动请求
	,_CG_BRD		(SYNC_NPC_JUMP)			// NPC转移指令广播信息
	,_CG_SYN_B	(SYNC_STOP)					// 玩家停止移动请求
	,_CG_BRD		(SYNC_NPC_STOP)
	,_CG_BRD		(SYNC_STOP_DIR)			// 带方向停止广播


	,_CG_CMD		(SYNC_ACTION_EXPIRED)		// 玩家动作结束信息通知
	,_CG_BRD		(SYNC_FORCED_WARP)			// 玩家强制移动
	,_CG_BRD		(SYNC_BOSS_MONSTER_ENTER)
	,_CG_SYN_B	(SYNC_FAST_KBMOVE)					// 
	,_CG_BRD		(SYNC_NPC_AIINFO)


};

/*////////////////////////////////////////////////////////////////////////
// BATTLE
/*////////////////////////////////////////////////////////////////////////
enum eCG_BATTLE_PROTOCOL
{

	 _CG_SYN_BN	(BATTLE_PLAYER_ATTACK)			// 玩家攻击(NPCPLAYER...)	
	,_CG_SYN_B	(BATTLE_VKR_ATTACK)		
	,_CG_SYN_B	(BATTLE_VKR_RELOAD)		
	,_CG_CMD		(BATTLE_MONSTER_ATTACK)			// 怪物攻击

	,_CG_SYN_BN	(BATTLE_P2O_ATTACK)				// PlayerRender 攻击 MapObject
	,_CG_BRD		(BATTLE_O2P_ATTACK)				// MapObject 攻击 PlayerRender


};


/*////////////////////////////////////////////////////////////////////////
// CONVERSATION
/*////////////////////////////////////////////////////////////////////////
enum eCG_CONVERSATION_PROTOCOL
{
	_CG_SYN_AB(CONVERSATION_FIELDCHAT)			// 场景内玩家聊天请求


};




/*////////////////////////////////////////////////////////////////////////
// ITEM
/*////////////////////////////////////////////////////////////////////////
enum eCG_ITEM_PROTOCOL
{
	 _CG_ACK		(ITEM_INITINFO)
	,_CG_SYN_AN	(ITEM_MOVE)
	,_CG_BRD 	(ITEM_APPREARANCE)
	,_CG_SYN_AN	(ITEM_QUICK_LINKITEM)
	,_CG_SYN_AN	(ITEM_QUICK_LINKSKILL)
	,_CG_SYN_AN	(ITEM_QUICK_UNLINK)
	,_CG_SYN_AN	(ITEM_QUICK_MOVE)


	,_CG_SYN_AN	(ITEM_PICK)
	,_CG_SYN_AN	(ITEM_DROP)
	,_CG_SYN_AN	(ITEM_COMBINE)
	,_CG_SYN_ABN(ITEM_USE)
	,_CG_SYN_AN	(ITEM_SELL)
	,_CG_SYN_AN	(ITEM_BUY)
	,_CG_SYN_AN	(ITEM_REPAIR)
	,_CG_SYN_AN	(ITEM_REPAIR_EQUIPS)


	,_CG_ACK	(ITEM_OBTAIN)
	,_CG_NAK	(ITEM_OBTAIN)
	,_CG_ACK	(ITEM_LOSE)
	,_CG_SYN_AN	(ITEM_PICK_MONEY)
	,_CG_SYN_N	(ITEM_DROP_MONEY)
	,_CG_SYN_AN	(ITEM_ENCHANTUP)
	,_CG_SYN_AN	(ITEM_RANKUP)
	,_CG_SYN_AN	(ITEM_SOCKET_FILL)




	,_CG_SYN_N	(ITEM_SOCKET_EXTRACT)
	,_CG_ACK		(ITEM_SOCKET_EXTRACT_SUCCESS)
	,_CG_ACK		(ITEM_SOCKET_EXTRACT_FAILED)

	,_CG_SYN_AN	(ITEM_ACCESSORY_CREATE)
	,_CG_SYN_N	(ITEM_ENCHANT)
	,_CG_ACK		(ITEM_ENCHANT_SUCCESS)
	,_CG_ACK		(ITEM_ENCHANT_FAILED)

	,_CG_SYN_AN	(ITEM_MAKE)



};


/*////////////////////////////////////////////////////////////////////////
// TRADE
/*////////////////////////////////////////////////////////////////////////
enum eCG_TRADE_PROTOCOL
{
	 _CG_SYN_NC	(TRADE_REQ)	// 交易请求
	,_CG_SYN_ANC(TRADE_RES)	// 交易接受/拒绝的时候，服务器交易箱生成
	,_CG_SYN_ANC(TRADE_PUT)
	,_CG_SYN_ANC(TRADE_PUT_MONEY)



	,_CG_SYN_ANC(TRADE_GET)
	,_CG_SYN_ANC(TRADE_GET_MONEY)
	,_CG_SYN_ANC(TRADE_PROPOSAL)		// 交易提示完成
	,_CG_SYN_ANC(TRADE_MODIFY)			// 交易提示有交易变动


	,_CG_SYN_ANC(TRADE_ACCEPT)
	,_CG_SYN_ANC(TRADE_CANCEL)
	,_CG_BRD		(TRADE_TRADE)		//交易完成
	,_CG_SYN_ANC(TRADE_SWAP)		//交换交易栏中物品


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_VENDOR_PROTOCOL
{
	 _CG_SYN_ABN	(VENDOR_START)
	,_CG_SYN_ABN	(VENDOR_END)
	,_CG_SYN_ABN	(VENDOR_MODIFY_REQ)
	,_CG_SYN_ABN	(VENDOR_MODIFY_CANCEL)
	,_CG_SYN_ABN	(VENDOR_MODIFY)
	,_CG_SYN_ABN	(VENDOR_RENAME)
	,_CG_SYN_ABN	(VENDOR_SWAP)


	,_CG_SYN_ABN	(VENDOR_INSERT)
	,_CG_SYN_ABN	(VENDOR_DELETE)
	,_CG_SYN_ABCN	(VENDOR_BUY)
	,_CG_SYN_ABN	(VENDOR_VIEW_START)
	,_CG_SYN_AN		(VENDOR_VIEW_END)

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_WAREHOUSE_PROTOCOL
{
	 _CG_SYN_AN	(WAREHOUSE_START)
	,_CG_CMD		(WAREHOUSE_START)
	,_CG_SYN_AN	(WAREHOUSE_MONEY)
	,_CG_SYN_AN	(WAREHOUSE_END)


};


/*////////////////////////////////////////////////////////////////////////
// PARTY
/*////////////////////////////////////////////////////////////////////////
enum eCG_PARTY_PROTOCOL
{
	 _CG_CMD		(PARTY_MEMBER_INFO		)
	,_CG_BRD		(PARTY_MEMBER_LEVEL_UP)
	,_CG_BRD		(PARTY_MEMBER_HP_CHANGE)
	,_CG_BRD		(PARTY_MEMBER_STATUS_CHANGE)
	,_CG_BRD		(PARTY_MEMBER_JOIN)
	,_CG_SYN_N	(PARTY_FORCED_EXPULSION)
	,_CG_SYN_N	(PARTY_LEAVE_REQUEST)
	,_CG_BRD		(PARTY_LEAVE_START)
	,_CG_BRD		(PARTY_LEAVE_COMPLETE)


	,_CG_SYN		(PARTY_INVITE_REQUEST		)
	,_CG_ACK		(PARTY_INVITE_REQUEST_SERVER)
	,_CG_NAK		(PARTY_INVITE_REQUEST_SERVER)
	,_CG_SYN		(PARTY_INVITE_RESPONSE_SERVER)
	,_CG_ACK		(PARTY_INVITE_RESPONSE)
	,_CG_NAK		(PARTY_INVITE_RESPONSE)

	,_CG_SYN_BN	(PARTY_CHANGE_MASTER)
	,_CG_SYN_BN	(PARTY_DESTROY_PARTY)
	,_CG_SYN_BN	(PARTY_CHANGE_ITEM_DISTRIBUTION_TYPE)



	,_CG_BRD		(PARTY_ITEM_DISTRIBUTION_HAPPEN)
	,_CG_SYN		(PARTY_ITEM_DISTRIBUTION_START_ROULETTE)
	,_CG_ACK		(PARTY_ITEM_DISTRIBUTION_RESULT)
	,_CG_BRD		(PARTY_ITEM_DISTRIBUTION_RESULT)
	,_CG_BRD		(PARTY_REQUEST_JOIN_RELAYROOM)
	,_CG_ACK		(PARTY_REQUEST_JOIN_RELAYROOM)
	,_CG_SYN_BN	(PARTY_SELECT_TARGET)


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_SUMMON_PROTOCOL
{
	 _CG_SYN_AN	(SUMMON_COMMAND)
	,_CG_SYN		(SUMMON_SKILL_ACTION)
	,_CG_BRD		(SUMMON_SKILL_RESULT)

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCG_GUILD_PROTOCOL
{
	 _CG_SYN_AN		(GUILD_SELECT)
	,_CG_SYN_ABN	(GUILD_CREATE)
	,_CG_SYN_ABN	(GUILD_DESTROY)
	,_CG_SYN_N		(GUILD_INVITE)
	,_CG_ACK			(GUILD_INVITE_ACCEP)
	,_CG_ACK			(GUILD_INVITE_REJECT)


	,_CG_CMD	(GUILD_INVITE_REQUEST)
	,_CG_SYN	(GUILD_INVITE_ACCEPT)
	,_CG_SYN	(GUILD_INVITE_REJECT)
	,_CG_ACK	(GUILD_INVITE_ANSWER)
	,_CG_NAK	(GUILD_INVITE_ANSWER)
	,_CG_CMD	(GUILD_JOIN_SUCCESS)
	,_CG_BRD	(GUILD_JOIN_SUCCESS)
	,_CG_CMD	(GUILD_JOIN_FAILED)



	,_CG_SYN_ABN	(GUILD_WITHDRAW)
	,_CG_SYN_AN		(GUILD_TRANSFER_POSITION)
	,_CG_SYN_AN		(GUILD_GRANT_LOW_POSITION)
	,_CG_SYN_AN		(GUILD_DROP_LOW_POSITION)
	,_CG_SYN_AN		(GUILD_BUY_ITEM)
	,_CG_SYN_AN		(GUILD_USE_ITEM)
	,_CG_SYN_AN		(GUILD_USE_GP)
	,_CG_SYN_AN		(GUILD_DONATE_GP)


};



#endif //__PROTOCOL_CLIENTGAMES_PLAYER_H__
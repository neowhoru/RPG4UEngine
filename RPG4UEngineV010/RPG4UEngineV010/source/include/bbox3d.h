/*////////////////////////////////////////////////////////////////////////
文 件 名：BBox3D.h
创建日期：2008年5月20日
最后更新：2008年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BBOX3D_H__
#define __BBOX3D_H__
#pragma once


#include "BBox3DDefine.h"

class Sphere;
class Vector3D;
class BoundingVolume;
class Matrix4;
class Line3D;

class _MATHLIB_API BBox3D : public BBOX3D
{
public:
	BBox3D();
	BBox3D(const Vector3D&	center, const Vector3D& extents);
	BBox3D(const BBOX3D&		bbox);
	BBox3D(const Matrix4&	m);

public:
	BBox3D& operator()(const Vector3D&	center, const Vector3D& extents);
	BBox3D& operator()(const BBOX3D&		bbox);
	BBox3D& operator()(const Matrix4&	m);

public:
	Vector3D Center()		const;
	Vector3D Extents()	const;
	Vector3D Size()		const;

	BBox3D& Set(const Matrix4& m);
	BBox3D& Set(const Vector3D& center, const Vector3D& extents);
	BBox3D& Set(const BBOX3D&	bbox);


	void BeginExtend();
	void Extend	(const Vector3D& vVertex);
	void Extend	(float x, float y, float z);
	void Extend	(const BBox3D& box);

	/// transform axis aligned bounding box
	void Transform(const Matrix4& m);

   Matrix4 ToMatrix4() const;

	/*////////////////////////////////////////////////////////////////////////
        @brief Gets closest intersection with BBox3D.
        If the line starts inside the box,  start point is returned in ipos.
        @param line the pick ray
        @param ipos closest point of intersection if successful, trash otherwise
        @return true if an intersection occurs
	/*////////////////////////////////////////////////////////////////////////
	BOOL	Intersect(const Line3D& line, Vector3D& ipos) const;

public:
	DWORD Intersect			( const BoundingVolume& bv);
	DWORD Intersect			( const BBox3D& aabb2);
	DWORD Intersect			( const Sphere& sphere);
	DWORD Intersect			( const Matrix4& viewProjection);

	BOOL  IsIntersect			( const BoundingVolume& bv);
	BOOL	IsIntersect			( const BBox3D& aabb2);
	BOOL	IsIntersect			( const Sphere& sphere);

	BOOL  IsContain			( const BoundingVolume& bv);
	BOOL  IsContain			( const Vector3D&			vVertex);
	BOOL	IsContain			( const BBox3D&			aabb2);
	BOOL	IsContain			( const Sphere&			sphere);

	void	GenerateFrom		(Vector3D*        pvVertices
                           ,int              iCountVertices
									,int					iSize = sizeof ( Vector3D));
	void	GenerateFrom		(Vector3D*			pvVertices
                           ,int*					piIndexMap
									,int					iCountVertices
									,int					iSize=sizeof(Vector3D));


protected:
	BOOL CheckPointInPolygonX(const Vector3D& p) const;
	BOOL CheckPointInPolygonY(const Vector3D& p) const;
	BOOL CheckPointInPolygonZ(const Vector3D& p) const;

};

typedef const BBox3D  BBox3C;

#include "BBox3D.inl"


#endif //__BBOX3D_H__
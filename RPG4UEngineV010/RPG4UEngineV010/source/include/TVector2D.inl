/*////////////////////////////////////////////////////////////////////////
文 件 名：TVector2D<Type>.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TTVECTOR2D_INL__
#define __TTVECTOR2D_INL__
#pragma once

namespace math
{ 
template<typename Type> const TVector2D<Type> TVector2D<Type>::ZERO( 0, 0);
template<typename Type> const TVector2D<Type> TVector2D<Type>::UNIT_X( 1, 0);
template<typename Type> const TVector2D<Type> TVector2D<Type>::UNIT_Y( 0, 1);
template<typename Type> const TVector2D<Type> TVector2D<Type>::UNIT_X_NEG( -1,  0);
template<typename Type> const TVector2D<Type> TVector2D<Type>::UNIT_Y_NEG(  0, -1);
template<typename Type> const TVector2D<Type> TVector2D<Type>::UNIT_SCALE(1, 1);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<typename Type>
inline TVector2D<Type>::TVector2D( CONST Type *pf )
{
#ifdef _DEBUG
    if(!pf)
        return;
#endif
    x = pf[0];
    y = pf[1];
}

template<typename Type>
inline TVector2D<Type>::TVector2D( Type fx, Type fy )
{
    x = fx;
    y = fy;
}

template<typename Type>
inline TVector2D<Type>::TVector2D( CONST TVector2D& v )
{
    x = v.x;
    y = v.y;
}

template<typename Type>
inline /*explicit*/ TVector2D<Type>::TVector2D( Type scaler )
{
   x = scaler;
	y = scaler;
}


template<typename Type>
inline TVector2D<Type>&	TVector2D<Type>::Set	( CONST Type * dat)
{
	x = dat[0];
	y = dat[1];
	return *this;
}
template<typename Type>
inline TVector2D<Type>&	TVector2D<Type>::Set	( Type scale )
{
	x = scale;
	y = scale;
	return *this;
}
template<typename Type>
inline TVector2D<Type>&	TVector2D<Type>::Set	( Type x0, Type y0 )
{
	x = x0;
	y = y0;
	return *this;
}
template<typename Type>
inline TVector2D<Type>&	TVector2D<Type>::Set	( CONST TVector2D& v )
{
	x = v.x;
	y = v.y;
	return *this;
}

// casting
template<typename Type>
inline TVector2D<Type>::operator Type* ()
{
    return (Type *) &x;
}

template<typename Type>
inline TVector2D<Type>::operator CONST Type* () const
{
    return (CONST Type *) &x;
}


//template<typename Type>
//inline Type TVector2D<Type>::operator [] ( const size_t i ) const
//{
//   assert( i < 2 );
//   return m[i];
//}
//
//template<typename Type>
//inline Type& TVector2D<Type>::operator [] ( const size_t i )
//{
//   assert( i < 2 );
//   return m[i];
//}


template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator = ( const TVector2D<Type>& rkVector )
{
	x = rkVector.x;
	y = rkVector.y;

	return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator = ( const Type fScalar)
{
	x = fScalar;
	y = fScalar;

	return *this;
}


// assignment operators
template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator += ( CONST TVector2D<Type>& v )
{
    x += v.x;
    y += v.y;
    return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator -= ( CONST TVector2D<Type>& v )
{
    x -= v.x;
    y -= v.y;
    return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator *= ( CONST TVector2D<Type>& v )
{
    x *= v.x;
    y *= v.y;
    return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator /= ( CONST TVector2D<Type>& v )
{
    x /= v.x;
    y /= v.y;
    return *this;
}


template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator += ( Type f )
{
    x += f;
    y += f;
    return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator -= ( Type f )
{
    x -= f;
    y -= f;
    return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator *= ( Type f )
{
    x *= f;
    y *= f;
    return *this;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::operator /= ( Type f )
{
    assert( (float)f != 0.0f );
    float fInv = 1.0f / (float)f;
    x = (Type)(x *fInv);
    y = (Type)(y *fInv);
    //x *= fInv;
    //y *= fInv;
    return *this;
}

// unary operators
template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator + () const
{
    return *this;
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator - () const
{
    return TVector2D<Type>(-x, -y);
}

// binary operators
template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator + ( CONST TVector2D<Type>& v ) const
{
    return TVector2D<Type>(x + v.x, y + v.y);
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator - ( CONST TVector2D<Type>& v ) const
{
    return TVector2D<Type>(x - v.x, y - v.y);
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator * ( CONST TVector2D<Type>& v ) const
{
    return TVector2D<Type>(x * v.x, y * v.y);
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator * ( Type f ) const
{
    return TVector2D<Type>(x * f, y * f);
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator / ( CONST TVector2D<Type>& v ) const
{
    return TVector2D<Type>(x / v.x, y / v.y);
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::operator / ( Type f ) const
{
    float fInv = 1.0f / (float)f;
    return TVector2D<Type>(x * fInv, y * fInv);
}

template<typename Type>
inline BOOL TVector2D<Type>::operator == ( CONST TVector2D<Type>& v ) const
{
    return x == v.x && y == v.y;
}

template<typename Type>
inline BOOL TVector2D<Type>::operator != ( CONST TVector2D<Type>& v ) const
{
    return x != v.x || y != v.y;
}

template<typename Type>
inline BOOL TVector2D<Type>::operator < ( CONST TVector2D<Type>& v ) const
{
    return x < v.x && y < v.y;
}

template<typename Type>
inline BOOL TVector2D<Type>::operator > ( CONST TVector2D<Type>& v ) const
{
    return x > v.x && y > v.y;
}


//////////////////////////////////////////
template<typename Type>
inline Type TVector2D<Type>::Length () const
{
   return (Type)Sqrt( (REAL)(x * x + y * y) );
}

template<typename Type>
inline Type TVector2D<Type>::LengthSquared () const
{
   return x * x + y * y;
}

template<typename Type>
inline Type TVector2D<Type>::Distance(const TVector2D<Type>& v) const
{
   return (*this - v).Length();
}

template<typename Type>
inline Type TVector2D<Type>::DistanceSquared(const TVector2D<Type>& v) const
{
   return (*this - v).LengthSquared();
}

template<typename Type>
inline Type TVector2D<Type>::DotProduct(const TVector2D<Type>& vec) const
{
   return x * vec.x + y * vec.y;
}

template<typename Type>
inline Type TVector2D<Type>::AbsDotProduct(const TVector2D<Type>& vec) const
{
   return Abs(x * vec.x) + Abs(y * vec.y);
}

template<typename Type>
inline Type TVector2D<Type>::CrossProduct( const TVector2D& vec ) const
{
	return x * vec.y - y * vec.x;
}


template<typename Type>
inline Type TVector2D<Type>::Normalize()
{
   Type fLength = Length();

   // Will also work for zero-sized vectors, but will change nothing
   if ( fLength > cZERO )
   {
       float fInvLength = 1.0f / (float)fLength;
       x = (Type)(x *fInvLength);
       y = (Type)(y *fInvLength);
   }

   return fLength;
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::NormalizeNew(void) const
{
	TVector2D<Type> ret = *this;
	ret.Normalize();
	return ret;
}

template<typename Type>
inline TVector2D<Type>& TVector2D<Type>::GetNormal(const TVector2D& v1, const TVector2D& v2) const
{
	x = - (v2.y - v1.y);
	y = v2.x - v1.x;

	float fScale = Length();
	if(fScale > 0.f)
		fScale = 1.0f / fScale;

	x *= fScale;
	y *= fScale;
	return *this;
}



template<typename Type>
inline TVector2D<Type> TVector2D<Type>::MidPoint( const TVector2D& vec ) const
{
   return TVector2D<Type>(
       ( x + vec.x ) * 0.5,
       ( y + vec.y ) * 0.5 );
}

template<typename Type>
void TVector2D<Type>::MakeFloor( const TVector2D<Type>& cmp )
{
	if( cmp.x < x )  x = cmp.x;
	if( cmp.y < y )  y = cmp.y;
}

template<typename Type>
void TVector2D<Type>::MakeCeil( const TVector2D<Type>& cmp )
{
	if( cmp.x > x )  x = cmp.x;
	if( cmp.y > y )  y = cmp.y;
}


template<typename Type>
inline TVector2D<Type> TVector2D<Type>::PerpendicularNew(void) const
{
	return  TVector2D(-y, x);
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::ReflectNew(const TVector2D<Type>& normal) const
{
	return TVector2D<Type>( *this - ( 2 * this->DotProduct(normal) * normal ) );
}

template<typename Type>
inline TVector2D<Type> TVector2D<Type>::RandomNew(Type angle) const
{

	angle *=  RandomUnit() * cPI_X2;
	Type cosa = cos(angle);
	Type sina = sin(angle);
	return  TVector2D(cosa * x - sina * y,		sina * x + cosa * y);
}


template<typename Type>
inline BOOL TVector2D<Type>::IsZeroLength(void) const
{
	return (LengthSquared() <= cZERO_T2);

}

template<typename Type>
BOOL TVector2D<Type>::InsideRect(TVector2D<Type> rect[4] )
{
	if( FloatCmp( x, rect[RC_LEFT].x )		< 0 )return FALSE;
	if( FloatCmp( x, rect[RC_RIGHT].x )		> 0 )return FALSE;
	if( FloatCmp( y, rect[RC_TOP].y )		< 0 )return FALSE;
	if( FloatCmp( y, rect[RC_BOTTOM].y )	> 0 )return FALSE;
	return TRUE;
}


};//namespace math


#endif //__TVector2D<Type>D_INL__
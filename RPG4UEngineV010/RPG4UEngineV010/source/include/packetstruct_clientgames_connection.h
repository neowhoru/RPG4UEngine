/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Connection.h
创建日期：2008年9月11日
最后更新：2008年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_CONNECTION_H__
#define __PACKETSTRUCT_CLIENTGAMES_CONNECTION_H__
#pragma once


#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"


#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
_CG_CONNECTION
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_HEARTBEAT_SYN )
//{
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGOBJECT_CG_CONNECTION_BEGIN( CONNECTION_BACKTOCHARSELECT_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;

/////////////////////////////////////
MSGOBJECT_CG_CONNECTION_BEGIN( CONNECTION_BACKTOCHARSELECT_ACK )
//{
//}
MSGPACKET_CG_OBJECT_END;

/////////////////////////////////////
MSGOBJECT_CG_CONNECTION_BEGIN( CONNECTION_BACKTOCHARSELECT_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_DISCONNECT_CMD )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_RESTART_CMD )
//{
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_REENTERSERVER_SYN )
//{
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_ENTERSERVER_SYN )
//{
	DWORD						m_dwAuthID;	
	char						m_szID[MAX_ID_LENGTH];
	BYTE						m_byHighVerNo;	
	BYTE						m_byMiddleVerNo;
	BYTE						m_byLowVerNo;
	BYTE						m_szSerialKey[MAX_AUTH_SERIAL_LENGTH];
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_ENTERSERVER_ACK )
//{
	BYTE							m_byCount;
	sCHARACTER_CLIENTPART	m_CharacterList[MAX_CHARACTER_LIST_NUM];

	//////////////////////////////
	int GetSize()
	{
		return ( SIZE_CG_MSG(CONNECTION_ENTERSERVER_ACK) - (MAX_CHARACTER_LIST_NUM - m_byCount)*sizeof(sCHARACTER_CLIENTPART) );
	}
	int GetSize(INT nCharCount)
	{
		return ( SIZE_CG_MSG(CONNECTION_ENTERSERVER_ACK) - (MAX_CHARACTER_LIST_NUM - nCharCount)*sizeof(sCHARACTER_CLIENTPART) );
	}
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_ENTERSERVER_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_END;



/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_ENTERVILLAGEREQ_SYN )
//{
	BYTE						m_bySelectedCharIndex;
//}
MSGPACKET_CG_END

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_ENTERVILLAGEREQ_ACK )
//{
	DWORD						m_dwPlayerKey;
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( CONNECTION_ENTERVILLAGEREQ_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( PREPARE_WORLD_CONNECT_SYN )
//{
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( PREPARE_WORLD_CONNECT_ACK )
//{
	TCHAR						szChatServerIP[MAX_IP_LENGTH];
	WORD						wChatServerPort;
//}
MSGPACKET_CG_END;

/////////////////////////////////////
MSGPACKET_CG_CONNECTION_BEGIN( PREPARE_WORLD_CONNECT_NAK )
//{
//}
MSGPACKET_CG_END;



#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_CONNECTION_H__
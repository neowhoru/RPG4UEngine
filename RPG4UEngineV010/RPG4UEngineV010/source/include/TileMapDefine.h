/*////////////////////////////////////////////////////////////////////////
文 件 名：TileMapDefine.h
创建日期：2008年4月16日
最后更新：2008年4月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TILEMAPDEFINE_H__
#define __TILEMAPDEFINE_H__
#pragma once

namespace tile
{ 
	const DWORD MAX_JUMP_IN_JUMPEVENT	=  16;
	//typedef std::string	TileMapKeyType;		//Text类型的Factory
	//typedef LPCSTR			TileMapType;
	//#define TileMap_NULL      NULL
	typedef DWORD				TileMapKeyType;	//ID类型的Factory
	typedef DWORD			    TileMapType;
	#define TileMap_NULL    ((TileMapType)-1)

	const	DWORD		START_AREA_FLAG	= (DWORD)_BIT(31);

	enum
	{
		 TILEMAP_NULL	= TileMap_NULL
		,TILEMAP_2D		= TAG('TM2D')
		,TILEMAP_3D		= TAG('TM3D')
	};
	#define TILEMAP_2D_EXT		"tm2d"
	#define TILEMAP_3D_EXT		"tm3d"

	struct SpecialArea
	{
		BYTE					m_byGroup;
		IDTYPE				m_AreaID;
		BoundingVolume		m_bvData;
	};


	enum ENUM_JUMP_STEP
	{
		EJS_NONE		= 0,
		EJS_JUMPSTART,
		EJS_JUMPEND,
	};

	typedef struct
	{
		ENUM_JUMP_STEP m_eJumpStep;// Jump 
		Vector3D			m_wvJumpFrom;		// 
		Vector3D			m_wvJumpTo;			//  
	} WzPathFindJump;

	typedef struct
	{
		int				m_iCountJump;	
		WzPathFindJump m_Jump[MAX_JUMP_IN_JUMPEVENT];
	} sPATHFIND_EVENT;


	#define TMGO_NO_Z_WRITE		( 0x00000001)	// 
	#define TMGO_NO_COLLISION	( 0x00000002)	// 
	#define TMGO_WATER			( 0x00000004)	// 
	#define TMGO_NO_DECAL		( 0x00000008)	// 
	#define TMGO_SPECULAR		( 0x00000010)	// 
	#define TMGO_DETAIL			( 0x00000020)	// 



};//namespace tile


#endif //__TILEMAPDEFINE_H__
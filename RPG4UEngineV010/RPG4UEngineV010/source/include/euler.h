/*////////////////////////////////////////////////////////////////////////
文 件 名：Euler.h
创建日期：2008年8月21日
最后更新：2008年8月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __EULER_H__
#define __EULER_H__
#pragma once


/*** Definitions ***/
/* Quaternion */
struct QuatX
{
	float x, y, z, w;
} ;
enum	QuatPart 
{
	 EULER_X
	,EULER_Y
	,EULER_Z
	,EULER_W
};

typedef float HMatrix[4][4]; /* Right-handed, for column vectors */
typedef QuatX EulerAngles;    /* (x,y,z)=ang 1,2,3, w=order code  */

/*** Order type constants, constructors, extractors ***/
    /* There are 24 possible conventions, designated by:    */
    /*    o EulAxI = axis used initially                    */
    /*    o EulPar = parity of axis permutation             */
    /*    o EulRep = repetition of initial axis as last     */
    /*    o EulFrm = frame from which axes are taken        */
    /* Axes I,J,K will be a permutation of EULER_X,EULER_Y,EULER_Z.           */
    /* Axis H will be either I or K, depending on EulRep.   */
    /* Frame S takes axes from initial static frame.        */
    /* If ord = (AxI=EULER_X, Par=Even, Rep=No, Frm=S), then      */
    /* {a,b,c,ord} means Rz(c)Ry(b)Rx(a), where Rz(c)v      */
    /* rotates v around EULER_Z by c radians.                     */

static LPCSTR  EulSafe =  "\000\001\002\000";
static LPCSTR  EulNext =  "\001\002\000\001";

#define EulFrmS      0
#define EulFrmR      1
#define EulFrm(ord)  ((unsigned)(ord)&1)
#define EulRepNo     0
#define EulRepYes    1
#define EulRep(ord)  (((unsigned)(ord)>>1)&1)
#define EulParEven   0
#define EulParOdd    1
#define EulPar(ord)  (((unsigned)(ord)>>2)&1)

#define EulAxI(ord)  ((int)(EulSafe[(((unsigned)(ord)>>3)&3)]))
#define EulAxJ(ord)  ((int)(EulNext[EulAxI(ord)+(EulPar(ord)==EulParOdd)]))
#define EulAxK(ord)  ((int)(EulNext[EulAxI(ord)+(EulPar(ord)!=EulParOdd)]))
#define EulAxH(ord)  ((EulRep(ord)==EulRepNo)?EulAxK(ord):EulAxI(ord))

    /* EulGetOrd unpacks all useful information about order simultaneously. */
#define EulGetOrd(ord,i,j,k,h,n,s,f)\
							{\
								unsigned o=ord;\
								f=o&1;o>>=1;\
								s=o&1;o>>=1;\
								n=o&1;o>>=1;\
								i=EulSafe[o&3];\
								j=EulNext[i+n];\
								k=EulNext[i+1-n];\
								h=s?k:i;\
							}

    /* EulOrd creates an order value between 0 and 23 from 4-tuple choices. */
#define EulOrd(i,p,r,f)    (((((((i)<<1)+(p))<<1)+(r))<<1)+(f))

/*////////////////////////////////////////////////////////////////////////
Static axes
/*////////////////////////////////////////////////////////////////////////
#define EulOrdXYZs    EulOrd(EULER_X,EulParEven,EulRepNo,EulFrmS)
#define EulOrdXYXs    EulOrd(EULER_X,EulParEven,EulRepYes,EulFrmS)
#define EulOrdXZYs    EulOrd(EULER_X,EulParOdd,EulRepNo,EulFrmS)
#define EulOrdXZXs    EulOrd(EULER_X,EulParOdd,EulRepYes,EulFrmS)
#define EulOrdYZXs    EulOrd(EULER_Y,EulParEven,EulRepNo,EulFrmS)
#define EulOrdYZYs    EulOrd(EULER_Y,EulParEven,EulRepYes,EulFrmS)
#define EulOrdYXZs    EulOrd(EULER_Y,EulParOdd,EulRepNo,EulFrmS)
#define EulOrdYXYs    EulOrd(EULER_Y,EulParOdd,EulRepYes,EulFrmS)
#define EulOrdZXYs    EulOrd(EULER_Z,EulParEven,EulRepNo,EulFrmS)
#define EulOrdZXZs    EulOrd(EULER_Z,EulParEven,EulRepYes,EulFrmS)
#define EulOrdZYXs    EulOrd(EULER_Z,EulParOdd,EulRepNo,EulFrmS)
#define EulOrdZYZs    EulOrd(EULER_Z,EulParOdd,EulRepYes,EulFrmS)

/*////////////////////////////////////////////////////////////////////////
Rotating axes
/*////////////////////////////////////////////////////////////////////////
#define EulOrdZYXr    EulOrd(EULER_X,EulParEven,EulRepNo,EulFrmR)
#define EulOrdXYXr    EulOrd(EULER_X,EulParEven,EulRepYes,EulFrmR)
#define EulOrdYZXr    EulOrd(EULER_X,EulParOdd,EulRepNo,EulFrmR)
#define EulOrdXZXr    EulOrd(EULER_X,EulParOdd,EulRepYes,EulFrmR)
#define EulOrdXZYr    EulOrd(EULER_Y,EulParEven,EulRepNo,EulFrmR)
#define EulOrdYZYr    EulOrd(EULER_Y,EulParEven,EulRepYes,EulFrmR)
#define EulOrdZXYr    EulOrd(EULER_Y,EulParOdd,EulRepNo,EulFrmR)
#define EulOrdYXYr    EulOrd(EULER_Y,EulParOdd,EulRepYes,EulFrmR)
#define EulOrdYXZr    EulOrd(EULER_Z,EulParEven,EulRepNo,EulFrmR)
#define EulOrdZXZr    EulOrd(EULER_Z,EulParEven,EulRepYes,EulFrmR)
#define EulOrdXYZr    EulOrd(EULER_Z,EulParOdd,EulRepNo,EulFrmR)
#define EulOrdZYZr    EulOrd(EULER_Z,EulParOdd,EulRepYes,EulFrmR)





#endif //__EULER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：NPCInfoParser.h
创建日期：2008年4月14日
最后更新：2008年4月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NPCINFOPARSER_H__
#define __NPCINFOPARSER_H__
#pragma once




#include "GlobalInstanceSingletonDefine.h"
#include "NPCInfoParserDefine.h"
#include <THashTable.h>
#include <BaseResParser.h>

using namespace util;
using namespace quest;
struct sNPCINFO_BASE;
struct sNPCINFO_FUNC;

using namespace std;
typedef map<VRSTR, sNPCINFO_BASE*>		NpcInfoNameMap;
typedef NpcInfoNameMap::iterator			NpcInfoNameMapIt;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API NPCInfoParser : public BaseResParser
{
public:
	NPCInfoParser();
	virtual ~NPCInfoParser();

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sNPCINFO_BASE, m_pNPCInfoHashTable, m_MonsterCode);
	BASERES_UNIT_ALLOC	(sVNPCINFO_BASE,m_pVNPCInfoHashTable		 ,m_vMonsterCode  ,VNpc			);
	BASERES_UNIT_ALLOC	(sNPCINFO_FUNC ,m_pExtraNPCInfoHashTable	 ,m_ExtraCode		,Extra		);
	BASERES_UNIT_ALLOC	(NPC_QUESTINFO ,m_pQuestInfoHashTable		 ,m_Code				,NpcQuest	);

public:
	virtual BOOL			Init	(DWORD dwMonsterInfoPoolSize
										,DWORD dwExtraNPCInfoPoolSize
										,DWORD dwQuestInfoPoolSize);

	virtual VOID			Release();

	virtual BOOL			Load				( LPCSTR pszFileName, BOOL bReload = FALSE );
	virtual BOOL			LoadVNpc			( LPCSTR pszFileName, BOOL bReload = FALSE );
	virtual BOOL			LoadExtra		( LPCSTR pszFileName, BOOL bReload = FALSE );
	virtual BOOL			LoadQuestInfo	( LPCSTR pszFileName, BOOL bReload = FALSE );

	virtual VOID						Reload			();
	virtual VOID						ReloadVNpc		();
	virtual VOID						ReloadExtra		();
	virtual sNPCINFO_BASE*			GetMonsterInfo		( CODETYPE MonsterCode ) { return m_pNPCInfoHashTable->GetData( MonsterCode ); }
	virtual sNPCINFO_BASE*			GetMonsterInfo		( LPCSTR		szName );
	virtual sVNPCINFO_BASE*			GetVMonsterInfo	( CODETYPE MonsterCode ) { return m_pVNPCInfoHashTable->GetData( MonsterCode ); }
	virtual sNPCINFO_FUNC*			GetExtraInfo		( CODETYPE npcFuncCode ) { return m_pExtraNPCInfoHashTable->GetData( npcFuncCode ); }
	virtual sVNPCINFO_BASE*			GetVMonsterInfoBy	( CODETYPE MonsterCode );

	virtual sNPCINFO_EXTRA_GROUP* 	GetExtraInfoSetFromFieldID(CODETYPE MapCode,FIELDID FieldID);
	virtual NPC_QUESTINFO*			GetQuestInfo	( CODETYPE MonsterCode ) { return m_pQuestInfoHashTable->GetData( MonsterCode ); }

	virtual VOID						SetFirst()			{ m_pNPCInfoHashTable->SetFirst();	}
	virtual sNPCINFO_BASE *			GetNext()			{ return m_pNPCInfoHashTable->GetNext();	}

	virtual VOID						SetExtraFirst()	{ m_pExtraNPCInfoHashTable->SetFirst();	}
	virtual sNPCINFO_FUNC *			GetExtraNext()		{ return m_pExtraNPCInfoHashTable->GetNext();	}

	template<class OPR>
		void ForEachFuncNPC(OPR& opr,DWORD dwLandID=0,DWORD dwFieldID=0);

	void _AddNameNpc(sNPCINFO_BASE* pInfo){m_NpcNameMap[pInfo->m_NpcName] = pInfo;}
	void _Push(NPC_QUESTINFO*		pQuestInfo,sQUEST_RELATEINFO&	questInfo){pQuestInfo->m_QuestInfos.push_back(questInfo);}
	void _Push(NPC_QUESTINFO*		pQuestInfo,CODETYPE	questID){pQuestInfo->m_QuestIDs.push_back(questID);}
protected:
	virtual VOID		Unload();
	virtual VOID		UnLoadExtra();

protected:
	VRSTR										m_pszVNpcFileName		;
	VRSTR										m_pszExtraFileName	;
	VRSTR										m_pszQuestFileName	;

private:
	THashTable<sNPCINFO_BASE *>		*m_pNPCInfoHashTable			;
	THashTable<sVNPCINFO_BASE*>		*m_pVNPCInfoHashTable		;
	THashTable<sNPCINFO_FUNC *>		*m_pExtraNPCInfoHashTable	;
	THashTable<NPC_QUESTINFO *>		*m_pQuestInfoHashTable		;

	sNPCINFO_EXTRA_GROUP						m_Extra_NPCInfoSet;
	NpcInfoNameMap							m_NpcNameMap;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(NPCInfoParser , _RESOURCELIB_API);
//extern _RESOURCELIB_API NPCInfoParser& singleton::GetNPCInfoParser();
#define theNPCInfoParser  singleton::GetNPCInfoParser()


#include "NPCInfoParser.inl"


#endif //__NPCINFOPARSER_H__
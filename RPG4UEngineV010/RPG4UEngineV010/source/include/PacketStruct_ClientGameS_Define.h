/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Define.h
创建日期：2008年9月11日
最后更新：2008年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_DEFINE_H__
#define __PACKETSTRUCT_CLIENTGAMES_DEFINE_H__
#pragma once


#include "PacketStruct_Define.h"

#define MSGPACKET_CG_BEGIN( c, p )			MSGPACKET_BEGIN(CG_##c,	CG_##p)
#define MSGPACKET_CG_END						MSGPACKET_END

#define MSGPACKET_CG_OBJECT_BEGIN(c,p)		MSGPACKET_OBJECT_BEGIN(CG_##c,CG_##p)
#define MSGPACKET_CG_OBJECT_END				MSGPACKET_OBJECT_END

#define MSGPACKET_CG_SUPER_BEGIN(c, p,s)	MSGPACKET_SUPER_BEGIN(CG_##c,CG_##p,CG_##s)
#define MSGPACKET_CG_SUPER_END				MSGPACKET_SUPER_END

#define SIZE_CG_MSG(p)							SIZE_MSG(CG_##p)



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define MSGPACKET_CG_SKILLACTION_BEGIN( c, p )\
					struct MSG_CG_##p : public MSG_SKILL_ACTION_BASE \
					{\
						MSG_CG_##p()\
						{\
							m_byCategory = CG_##c;\
							m_byProtocol = CG_##p;\
						}

#define MSGPACKET_CG_SKILLACTION_END\
					};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define MSGPACKET_CG_CONNECTION_BEGIN(p)	MSGPACKET_CG_BEGIN			( CONNECTION, p )
#define MSGOBJECT_CG_CONNECTION_BEGIN(p)	MSGPACKET_CG_OBJECT_BEGIN	( CONNECTION, p )

#define MSGPACKET_CG_CHARINFO_BEGIN(p)		MSGPACKET_CG_BEGIN			( CHARINFO, p )
#define MSGOBJECT_CG_CHARINFO_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( CHARINFO , p )

#define MSGOBJECT_CG_MAP_BEGIN(p)			MSGPACKET_CG_OBJECT_BEGIN	( MAP , p )

#define MSGPACKET_CG_SYNC_BEGIN(p)			MSGPACKET_CG_BEGIN			( SYNC, p )
#define MSGOBJECT_CG_SYNC_BEGIN(p)			MSGPACKET_CG_OBJECT_BEGIN	( SYNC , p )


#define MSGPACKET_CG_BATTLE_BEGIN(p)		MSGPACKET_CG_BEGIN			( BATTLE, p )
#define MSGOBJECT_CG_BATTLE_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( BATTLE , p )

#define MSGPACKET_CG_STYLE_BEGIN(p)			MSGPACKET_CG_BEGIN			( STYLE, p )
#define MSGOBJECT_CG_STYLE_BEGIN(p)			MSGPACKET_CG_OBJECT_BEGIN	( STYLE , p )

#define MSGPACKET_CG_SKILL_BEGIN(p)			MSGPACKET_CG_BEGIN				( SKILL, p )
#define MSGOBJECT_CG_SKILL_BEGIN(p)			MSGPACKET_CG_OBJECT_BEGIN		( SKILL , p )
#define MSGSKILLACTION_CG_SKILL_BEGIN(p)	MSGPACKET_CG_SKILLACTION_BEGIN( SKILL , p )


#define MSGOBJECT_CG_CONVERSATION_BEGIN(p)	MSGPACKET_CG_OBJECT_BEGIN	( CONVERSATION , p )
#define MSGSUPER_CG_CONVERSATION_BEGIN(p,s)	MSGPACKET_CG_SUPER_BEGIN	( CONVERSATION,  p , s)

#define MSGPACKET_CG_ZONE_BEGIN(p)		MSGPACKET_CG_BEGIN			( ZONE, p )
#define MSGOBJECT_CG_ZONE_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( ZONE , p )

#define MSGPACKET_CG_ITEM_BEGIN(p)		MSGPACKET_CG_BEGIN			( ITEM, p )
#define MSGOBJECT_CG_ITEM_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( ITEM , p )
#define MSGSUPER_CG_ITEM_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( ITEM,  p , s)

#define MSGPACKET_CG_EVENT_BEGIN(p)		MSGPACKET_CG_BEGIN			( EVENT, p )
#define MSGOBJECT_CG_EVENT_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( EVENT , p )
#define MSGSUPER_CG_EVENT_BEGIN(p,s)	MSGPACKET_CG_SUPER_BEGIN	( EVENT,  p , s)

#define MSGPACKET_CG_GUILD_BEGIN(p)		MSGPACKET_CG_BEGIN			( GUILD, p )
#define MSGOBJECT_CG_GUILD_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( GUILD , p )
#define MSGSUPER_CG_GUILD_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN( GUILD,  p , s)

#define MSGPACKET_CG_SUMMON_BEGIN(p)		MSGPACKET_CG_BEGIN			( SUMMON, p )
#define MSGOBJECT_CG_SUMMON_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( SUMMON , p )
#define MSGSUPER_CG_SUMMON_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( SUMMON,  p , s)

#define MSGPACKET_CG_PVP_BEGIN(p)		MSGPACKET_CG_BEGIN			( PVP, p )
#define MSGOBJECT_CG_PVP_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( PVP , p )
#define MSGSUPER_CG_PVP_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( PVP,  p , s)

#define MSGPACKET_CG_GM_BEGIN(p)			MSGPACKET_CG_BEGIN			( GM, p )
#define MSGOBJECT_CG_GM_BEGIN(p)			MSGPACKET_CG_OBJECT_BEGIN	( GM , p )
#define MSGSUPER_CG_GM_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( GM,  p , s)

#define MSGPACKET_CG_PARTY_BEGIN(p)		MSGPACKET_CG_BEGIN			( PARTY, p )
#define MSGOBJECT_CG_PARTY_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( PARTY , p )
#define MSGSUPER_CG_PARTY_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN( PARTY,  p , s)

#define MSGPACKET_CG_STATUS_BEGIN(p)		MSGPACKET_CG_BEGIN			( STATUS, p )
#define MSGOBJECT_CG_STATUS_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( STATUS , p )
#define MSGSUPER_CG_STATUS_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( STATUS,  p , s)

#define MSGPACKET_CG_TRIGGER_BEGIN(p)		MSGPACKET_CG_BEGIN			( TRIGGER, p )
#define MSGOBJECT_CG_TRIGGER_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( TRIGGER , p )
#define MSGSUPER_CG_TRIGGER_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( TRIGGER,  p , s)

#define MSGPACKET_CG_TRADE_BEGIN(p)		MSGPACKET_CG_BEGIN				( TRADE, p )
#define MSGOBJECT_CG_TRADE_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN		( TRADE , p )
#define MSGSUPER_CG_TRADE_BEGIN(p,s)	MSGPACKET_CG_SUPER_BEGIN		( TRADE,  p , s)

#define MSGPACKET_CG_VENDOR_BEGIN(p)		MSGPACKET_CG_BEGIN			( VENDOR, p )
#define MSGOBJECT_CG_VENDOR_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( VENDOR , p )
#define MSGSUPER_CG_VENDOR_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( VENDOR,  p , s)

#define MSGPACKET_CG_WAREHOUSE_BEGIN(p)		MSGPACKET_CG_BEGIN			( WAREHOUSE, p )
#define MSGOBJECT_CG_WAREHOUSE_BEGIN(p)		MSGPACKET_CG_OBJECT_BEGIN	( WAREHOUSE , p )
#define MSGSUPER_CG_WAREHOUSE_BEGIN(p,s)		MSGPACKET_CG_SUPER_BEGIN	( WAREHOUSE,  p , s)

#endif //__PACKETSTRUCT_CLIENTGAMES_DEFINE_H__


/*////////////////////////////////////////////////////////////////////////
文 件 名：StringHandle.h
创建日期：2008年1月12日
最后更新：2008年1月12日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __STRINGHANDLE_H__
#define __STRINGHANDLE_H__
#pragma once


#include "CommonDefine.h"
#include <ctype.h>
#include <String.h>
#include "TArray.h"


class _BASE_API StringHandle
{
public:
    StringHandle();
    StringHandle(LPCSTR str, BOOL bAttach=FALSE);
    StringHandle(const StringHandle& rhs);

    /// set to int val
    StringHandle(int intVal);
    /// set to float val
    StringHandle(float floatVal);
    /// destructor
    ~StringHandle();

public:
    /// = operator
    StringHandle& operator=(const StringHandle& rhs);
    /// = operator with string
    StringHandle& operator=(LPCSTR rhs);
    /// += operator with TCHAR*
    StringHandle& operator+=(LPCSTR rhs);
    /// += operator with string
    StringHandle& operator+=(const StringHandle& rhs);

    /// Is a equal to b?
    BOOL operator == (LPCTSTR szText) const;
    /// Is a inequal to b?
    BOOL operator != (LPCTSTR szText) const;

	 BOOL IsEqualTo	(LPCSTR szText,BOOL bCase=FALSE) const;

public:
    const TCHAR operator[](INT	i) const;
    const TCHAR operator[](UINT	i) const;
    TCHAR&		operator[](INT		i);
    TCHAR&		operator[](UINT	i);
	 bool	operator <		 (const StringHandle& src) const;
	 bool	operator >		 (const StringHandle& src) const;


	 void SetLength	(int length){Set(NULL,length);}

	 //校正单字节与单字混合位置
	 void CorrectWordPos	(LPCTSTR szText, int& nPos);

    /// set as TCHAR ptr, with explicit length
    void Set		(LPCSTR ptr, int length,BOOL bCheckWord=FALSE);	//ptr为NULL时，将设定保留length空间
    void Set		(LPCSTR str);
    void Attach	(LPCSTR str);
    void SetInt	(int val);
    void SetFloat	(float val);

    /// get string as TCHAR ptr
    LPCSTR	Get		() const;
    LPSTR	GetBuffer();
    int		ToInt		() const;
    float	ToFloat	() const;
    DWORD	ToDword	() const;

	 operator TCHAR* (){return GetBuffer();}
	 operator LPCSTR ()const{return Get();}

	 BOOL	LoadString	(UINT uResID, HINSTANCE	hInst=NULL);

public:
    int	Length	() const;
    void	Clear		();
    BOOL	IsEmpty	() const;


	 void Insert		(int iIndex, TCHAR  ch );
	 void Insert		(int iIndex, LPCSTR psz );
    void Append		(LPCSTR str);
    void Append		(const StringHandle& str);
    void AppendRange	(LPCSTR str, UINT numChars);
    void AppendInt	(int val);
    void AppendFloat	(float val);

public:
	void ToLower();
	void ToUpper();

    /// get first token (this will destroy the string)
    LPCSTR GetFirstToken(LPCSTR whiteSpace);
    /// get next token (this will destroy the string)
    LPCSTR GetNextToken(LPCSTR whiteSpace);

    /// tokenize string into a provided StringHandle array
    //int Tokenize(LPCSTR whiteSpace, TArray<StringHandle>& tokens) const;

    /// extract substring
    StringHandle  Mid			(UINT from, UINT numChars,BOOL bCheckWord=FALSE) const;
    StringHandle	Right			(UINT nCount,BOOL bCheckWord=FALSE) const;
    StringHandle	Left			(UINT nCount,BOOL bCheckWord=FALSE) const;

    /// terminate string at first occurence of character in set
    StringHandle&	Strip			(LPCSTR charSet);
    StringHandle& StripPrefix	(LPCSTR sPrefix);


    /// Index of first appearance of `v' starting from index `startIndex'.
    int Find			(LPCSTR szMatch, int startIndex=0) const;

    /// return index of character in string
    int FindChar		(TCHAR c, int startIndex=0) const;
    int ReverseFind	(TCHAR ch);


    /// terminate string at given index
    void TerminateAtIndex(int index);

    /// strip slash at end of path, if exists
    void StripTailSlash();

    /// delete characters from charset at left side of string
    StringHandle& TrimLeft		(LPCSTR charSet);
    StringHandle	TrimLeftNew	(LPCSTR charSet) const;

    /// delete characters from charset at right side of string
    StringHandle& TrimRight		(LPCSTR charSet);
    StringHandle	TrimRightNew	(LPCSTR charSet) const;

    /// trim characters from charset at both sides of string
    StringHandle& Trim		(LPCSTR charSet);
    StringHandle	TrimNew	(LPCSTR charSet) const;

    /// substitute every occurance of a string with another string
    UINT	Replace	(LPCSTR szMatch, LPCSTR szNew);
    UINT Replace	(TCHAR chOld, TCHAR chNew);

    /// convert string inplace from UTF-8 to 8-bit ANSI
    void UTF8toANSI();
    /// convert ANSI to UTF-8 in place
    void ANSItoUTF8();

public:
	/// get pointer to extension (without the dot)
	LPCSTR GetExtension(BOOL bWithoutDot=TRUE) const;
	/// check if extension matches (no dot in the extension!)
	BOOL CheckExtension(LPCSTR ext,BOOL bCase=FALSE) const;

   /// 删除 c:\\dir\\FileTitle.txt 结尾的 .txt扩展名，
	//		结果为：c:\\dir\\FileTitle
   BOOL StripExtension	(BOOL bRemoveDot=TRUE);

   /// 删除 c:\\dir\\FileTitle.txt 结尾的 FileTitle.txt
	//		结果为：c:\\dir\\ 
	//@param bRemoveLastSlash TRUE：去除最后的\符
   BOOL StripFileName	(BOOL bRemoveTailSlash=FALSE);


    void ReplaceExtension	(LPCSTR ext, BOOL bAddWhileNoExt=TRUE);
	 void	ConvertFullPath	();
	 BOOL	EqualPath			(LPCSTR szPath);
	 BOOL	IsSubDirectory		(LPCSTR szSubDir, BOOL bConvertFullPath=TRUE);

	void ConvertSlashes	(BOOL bToBackSlash=FALSE,BOOL bWithTail=TRUE);
	void StandardisePath	(BOOL bToBackSlash=FALSE,BOOL bWithTail=TRUE);



	/// 返回 c:\\root\\dir\\FileTitle.txt	中的目录 FileTitle.txt
	/// bWithExt == FALSE 时，	只返回	FileTitle
    StringHandle ExtractFileName		(BOOL bWithExt=TRUE) const;

	 /// 返回 c:\\root\\dir\\file.txt	中的目录 dir
	 //  或   c:\\root\\dir\\dir2\\		中的目录 dir
    StringHandle ExtractPathDirName	() const;

	 /// 返回 c:\\root\\dir\\file.txt	中的目录 c:\\root\\dir\\
	 //  或   c:\\root\\dir\\dir2\\		中的目录 c:\\root\\dir\\ 
    StringHandle ExtractPathDir		() const;


	 /// 返回 c:\\root\\dir\\file.txt	中的目录 c:\\root\\dir\\
	 //  或   c:\\root\\dir\\dir2\\		中的目录 c:\\root\\dir\\dir2\\ 
    StringHandle ExtractToLastSlash() const;


	 ///////////////////////////////////////
	 BOOL	HexToBytes	(BYTE* pBuf,DWORD dwSize=0);

	 ///////////////////////////////////////
	 BOOL IsNumberic	() const;
	 BOOL IsBlank		(BOOL bWithNL=FALSE) const;
	 BOOL IsHex			() const;
	 BOOL IsHexHeader	() const;

    void Format(const TCHAR* pszFormat, ...);

 public:
	 BOOL	IsLetterChar(BYTE c) const;
	 BOOL	IsAlhaChar	(BYTE c) const;
	 BOOL	IsDigitChar	(BYTE c) const;
	 BOOL	IsHexChar	(BYTE c) const;

protected:
    void		Copy(const StringHandle& src);
    void		Delete();

    /// get pointer to last directory separator
    TCHAR*	GetLastSlash() const;

protected:
    enum    {       LOCALSTRINGSIZE = 32,   };

    TCHAR*						m_pText;
    TCHAR						m_szTextBuf[LOCALSTRINGSIZE];
    DWORD						m_Length;

	 VG_BOOL_GET_PROPERTY	(Attach);
};


//------------------------------------------------------------------------------
static inline StringHandle operator+(const StringHandle& s0, const StringHandle& s1)
{
    StringHandle newString = s0;
    newString.Append(s1.Get());
    return newString;
}


#include "StringHandle.inl"



#endif //__STRINGHANDLE_H__
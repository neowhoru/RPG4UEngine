/*////////////////////////////////////////////////////////////////////////
文 件 名：Matrix3.h
创建日期：2008年5月21日
最后更新：2008年5月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATRIX3_H__
#define __MATRIX3_H__
#pragma once


#include "MathStructDecl.h"
#include "Vector2D.h"
#include "Vector3D.h"
#include "Quaternion.h"


//------------------------------------------------------------------------------
class _MATHLIB_API Matrix3 : public MATRIX3
{
public:
    Matrix3();
    Matrix3(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2);
    Matrix3(const Matrix3& mx);
    Matrix3(REAL _m11
           ,REAL _m12
			  ,REAL _m13
			  ,REAL _m21
			  ,REAL _m22
			  ,REAL _m23
			  ,REAL _m31
			  ,REAL _m32
			  ,REAL _m33);
    Matrix3(const Quaternion& q);

public:
    /// get as Quaternion
    Quaternion ToQuaternion() const;
    /// get as euler representation
    Vector3D ToEuler() const;
    /// Set as euler
    void FromEuler(const Vector3D& ea);

public:
    /// unrestricted LookAt
    void LookAt	(const Vector3D& from, const Vector3D& to, const Vector3D& up);
    /// restricted LookAt (Billboard)
    void Billboard(const Vector3D& from, const Vector3D& to, const Vector3D& up);

    void Set	(REAL m11
               ,REAL m12
					,REAL m13
					,REAL m21
					,REAL m22
					,REAL m23
					,REAL m31
					,REAL m32
					,REAL m33);
    void Set	(const Vector3D& v0, const Vector3D& v1, const Vector3D& v2);
    void Set	(const Matrix3& m1);


public:
    void Identity();
    void Transpose();
    /// is orthonormal?
    BOOL Orthonorm(REAL limit);

    void Scaling		(const Vector3D& s);
    void RotateX		(const REAL a);
    void RotateY		(const REAL a);
    void RotateZ		(const REAL a);
    void RotateLocalX(const REAL a);
    void RotateLocalY(const REAL a);
    void RotateLocalZ(const REAL a);
    void Rotate(		const Vector3D& vec, REAL a);

    Vector3D ComponentX(void) const;
    Vector3D ComponentY(void) const;
    Vector3D ComponentZ(void) const;

public:
    // inplace matrix multiply
   Matrix3&	operator *= (const Matrix3& m1);

	Matrix3	operator * (const Matrix3& m1);
	Vector3D	operator * (const Vector3D& v);

   REAL* operator[] (UINT iRow) const;

public:
    Vector3D Multiply	(const Vector3D& src) const;
    Matrix3& Multiply	(const Matrix3& m0, const Matrix3& m1);
    void		 Translate	(const Vector2D& t);

public:
	static const Matrix3 IDENTITY;
	static const Matrix3 ZERO;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "Matrix3.inl"

#endif //__MATRIX3_H__
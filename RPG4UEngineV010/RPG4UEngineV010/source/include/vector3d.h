/*////////////////////////////////////////////////////////////////////////
文 件 名：Vector3D.h
创建日期：3008年4月9日
最后更新：3008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VECTOR3D_H__
#define __VECTOR3D_H__
#pragma once


#include "MathDefine.h"
#include "MathConst.h"
#include "MathStructDecl.h"
#include "MathAngle.h"

class Matrix4;
//class Radian;
//namespace math
//{ 
	
class _MATHLIB_API Vector3D : public VECTOR3D
{
public:
    Vector3D() {};
    Vector3D( const REAL * );
    Vector3D( const VECTOR3D& );
    Vector3D( REAL x, REAL y, REAL z );
    explicit Vector3D(REAL scale);			//需要显式调用

    Vector3D&	Set			( REAL x, REAL y, REAL z );
    Vector3D&	Set			( const REAL * );
    Vector3D&	SetByAngle	( REAL fRadian );

	 //
	Vector3D& operator ()	(REAL x0, REAL y0, REAL z0);
	Vector3D& operator ()	(const Vector3D&		);
	Vector3D& operator ()	(const REAL  fScalar);
	Vector3D& operator ()	(const REAL* fDat	);

    // casting
   REAL& operator [] (INT nIndex);
   REAL  operator [] (INT nIndex) const;

    operator REAL* ();
    operator const REAL* () const;
    operator Vector3D*	 ();
    operator const Vector3D*	 () const;
    operator VECTOR3D&	 ();
    operator const VECTOR3D&	 () const;


    Vector3D& operator = ( const VECTOR3D& );
    Vector3D& operator = ( const REAL  fScalar );
    Vector3D& operator = ( const REAL* fDat );

    // assignment operators
    Vector3D& operator += ( const VECTOR3D& );
    Vector3D& operator -= ( const VECTOR3D& );
    Vector3D& operator *= ( const VECTOR3D& );
    Vector3D& operator /= ( const VECTOR3D& );
    Vector3D& operator += ( REAL );
    Vector3D& operator -= ( REAL );
    Vector3D& operator *= ( REAL );
    Vector3D& operator /= ( REAL );

    Vector3D& operator *= ( const Matrix4& );

    // unary operators
    Vector3D operator + () const;
    Vector3D operator - () const;

    // binary operators
    Vector3D operator + ( const VECTOR3D& ) const;
    Vector3D operator - ( const VECTOR3D& ) const;
    Vector3D operator * ( const VECTOR3D& ) const;
    Vector3D operator / ( const VECTOR3D& ) const;
    Vector3D operator + ( REAL ) const;
    Vector3D operator - ( REAL ) const;
    Vector3D operator * ( REAL ) const;
    Vector3D operator / ( REAL ) const;

    REAL		 operator % ( const VECTOR3D& ) const;

    Vector3D operator * ( const Matrix4& );


    BOOL operator == ( const VECTOR3D& ) const;
    BOOL operator != ( const VECTOR3D& ) const;
    BOOL operator <  ( const VECTOR3D& ) const;
    BOOL operator >  ( const VECTOR3D& ) const;


public:
	/*////////////////////////////////////////////////////////////////////////
	//工具类
	/*////////////////////////////////////////////////////////////////////////
	Vector3D& Scale(const Vector3D& vScale);

   REAL Length () const;			///> 调用了sqrt，CPU消耗非常大，调用之前，最好
											///> 事先通过LengthSquared检测下是否相等

	REAL LengthSquared () const;	///> 获取长度的平方，本函数，因为运算简单，效率相对高出许多
	REAL Length2 () const;	

   REAL Distance(const VECTOR3D& vec) const;
   REAL DistanceSquared(const VECTOR3D& vec) const;

	/// inplace linear interpolation
	void Lerp(const VECTOR3D& v0, float lerpVal);

	/// linear interpolation between v0 and v1
	void Lerp(const VECTOR3D& v0, const VECTOR3D& v1, float lerpVal);


	REAL DotProduct(const VECTOR3D& vec) const;	///> 返回其它矢量计算点积
																///>  点积可用来计算两矢量间的夹角，如果
																///>  a.都是单元矢量，则点积则是夹角的cos值
																///>  b.否则，cos值 = 点积值/ (v.len*v2.len)
																///>  c.另，还可计算点(Point)到面(Plane)的距离

   REAL AbsDotProduct(const VECTOR3D& vec) const;	///> 计算绝对值点积，原理上与点积类似，只是每轴值都使用了abs值


	Vector3D CalculateNormal(const VECTOR3D& v2, const VECTOR3D& v3) const;

	Vector3D Multiply			(const Matrix4& mTransform) const;
	Vector3D MultiplyNormal	(const Matrix4& mTransform) const;

	//Vector3D& MergeRotation	( const Vector3D& vRot1, const Vector3D& vRot2);
	Vector3D& ChaosRotation	( DWORD dwTick, float fSpeedRate);

	Vector3D& Rotate			(const VECTOR3D& axis, float angle);

	Vector3D						GetOrtho() const;	///< 获得垂直矢量

public:
	/*////////////////////////////////////////////////////////////////////////
	2D线条工具类
	/*////////////////////////////////////////////////////////////////////////

	float			LineDotProduct					( const VECTOR3D& v2);
	Vector3D&	LineMakeNormal					( const VECTOR3D& v1, const VECTOR3D& v2);
	Vector3D&	LineMakeLine					( const VECTOR3D& v1, const VECTOR3D& v2);
	float			LineDistanceFrom				( const VECTOR3D& vVertex);
	BOOL			LineTestParallel				( const VECTOR3D& vLine2);
	Vector3D&	LineMakeByCrossPoint			( const VECTOR3D& vLine1, const VECTOR3D& vLine2);
	BOOL			LineTestCrossTwoSegment		( const VECTOR3D& v2, const VECTOR3D& v3, const VECTOR3D& v4);

public:
	float GetAngleByACos	( );
	float GetAngleByAtan2( );

public:
	///> 与另一矢量计算叉积
	///> 结果返回两矢量共同的正交矢量
	///>  通常情况下，叉积矢量用来计算平面（由此两变量组成的平面）单位化法线
	///>  为同提高效率，叉积结果并没有做Normalize，故需手工调用
	///>  叉积矢量方向逆时针：由本矢量->Dest构造的扇形
	///>  譬如：UNIT_Y.crossProduct(UNIT_Z) = UNIT_X 而 UNIT_Z.crossProduct(UNIT_Y) = -UNIT_X
	///>  目前，这是使用右手坐标系，简单地说显示器左端为Y，底端为Z，则结果指向屏幕里（即人看屏幕的方向）
   Vector3D CrossProduct(const VECTOR3D& vec ) const;	


   REAL Normalize();					///> 单位化矢量，即 Lenght/Magnitude = 1，即LenghSequard=1
											///> 结果本身也是单元矢量
											///> 返回单位化之前的长度
   Vector3D NormalizeNew(void) const;	//依据本矢量，生成新的单位矢量

   Vector3D MidPoint	( const VECTOR3D& vec ) const;
	Vector3D Interpolate( const VECTOR3D& vec2, float t);

   void MakeFloor	( const VECTOR3D& cmp );
   void MakeCeil	( const VECTOR3D& cmp );

   Vector3D PerpendicularNew(void) const;		///> 获取本矢量的正交矢量
															///> 可参考 Quaternion
	///获取正交矢量
	void		GetOrthogonalVectors	( Vector3D& vOut1, Vector3D& vOut2);

   Vector3D ReflectNew(const VECTOR3D& normal) const;	///> 依据本矢量，参考normal生成新的镜射矢量

   Vector3D RandomNew(const Radian& angle,const VECTOR3D& up = Vector3D::ZERO) const;	///> 从给出的角度（弧度），生成随机的矢量

	///> 获取从本矢量到Dest矢量，最小旋转矩阵 arc quaternion
   //Quaternion GetRotationTo(const Vector3D& dest,
			//				const Vector3D& fallbackAxis = Vector3D::ZERO) const;


   BOOL IsZeroLength		(void) const;
	BOOL IsPositionEquals(const VECTOR3D& pos,	REAL tolerance = math::cZERO) const;
	BOOL IsPositionClose (const VECTOR3D& pos,	REAL tolerance = math::cZERO) const;
	BOOL IsDirectionEqual(const VECTOR3D& vec,	const Radian& tolerance) const;


public:
	//其它全局函数 
	//inline friend Vector3D operator * ( float f, const VECTOR3D& v )
	//{	
	//	return Vector3D(f * v.x, f * v.y, f * v.z);
	//}
	inline friend Vector3D operator / ( const REAL fScalar, const Vector3D& rkVector )
	{
		return Vector3D(	 fScalar / rkVector.x,	fScalar / rkVector.y, fScalar / rkVector.z);
	}

	inline friend Vector3D operator + (const REAL f , const Vector3D& v)
	{
		return Vector3D(		f + v.x ,		f + v.y ,		f + v.z);
	}
	inline friend Vector3D operator - (const REAL f , const Vector3D& v)
	{
		return Vector3D(		f - v.x ,		f - v.y,		f - v.z);
	}


public:
     // 特殊点
     static const Vector3D ZERO;
     static const Vector3D UNIT_X;
     static const Vector3D UNIT_Y;
     static const Vector3D UNIT_Z;
     static const Vector3D UNIT_X_NEG;
     static const Vector3D UNIT_Y_NEG;
     static const Vector3D UNIT_Z_NEG;
     static const Vector3D UNIT_SCALE;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef Vector3D			Vector3;
typedef Vector3D			Vector3X;
typedef Vector3D			Euler3D;
typedef Vector3D			VectorLine;		///< x, y  normal, z  k  ( a*x + b*y + k = 0)
typedef const Vector3D	Vector3C;
typedef const Euler3D	Euler3C;

//};//namespace math

#include "Vector3D.inl"


#endif //__VECTOR3D_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemAttrInfoParser.h
创建日期：2008年6月25日
最后更新：2008年6月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMATTRINFOPARSER_H__
#define __ITEMATTRINFOPARSER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include <ItemAttrInfoParserDefine.h>
#include <BaseResParser.h>


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _RESOURCELIB_API ItemAttrInfoParser : public BaseResParser
{
public:
	ItemAttrInfoParser(void);
	virtual ~ItemAttrInfoParser(void);

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sITEM_RANDATTR_INFO		,m_pRandAttrInfoHashTable	, m_nAttrCode);
	BASERES_UNIT_ALLOC	(sITEM_DOUBLE_ATTR_INFO	,m_pDoubleAttrInfoHashTable, m_nAttrCode , DoubleAttrInfo);
	BASERES_UNIT_ALLOC	(sITEM_RANDATTR_UNIT		,m_pRandAttrUnitHashTable	, m_nAttrCode , RandAttrUnit);

	BASERES_OBJECT_ALLOC	(sITEM_RANDATTR_TYPE		,AttrType	);
	BASERES_OBJECT_ALLOC	(sITEM_RANDATTR_GROUP	,AttrGroup	);

public:
	virtual BOOL				Init	(DWORD dwRandAttrInfoSize
											,DWORD dwDoubleAttrInfoSize
											,DWORD dwRandAttrTableSize);

	virtual VOID				Release();

	virtual BOOL				Load	(LPCSTR pszRandAttrInfoFileName
											,LPCSTR pszDoubleAttrInfoFileName
											,LPCSTR pszRandAttrTableFileName
											,BOOL   bReload=FALSE);

	virtual BOOL				LoadOptions	(LPCSTR pszFileName,BOOL   bReload=FALSE);

	virtual VOID				Reload();


public:
	virtual sITEM_RANDATTR_UNIT *			GetRandAttrUnit		(INT nCode ) ;
	virtual sITEM_RANDATTR_TYPE *			GetRandAttrType		(INT nCode ) ;
	virtual sITEM_RANDATTR_INFO *			GetRandAttrInfo		(INT nCode ) ;
	virtual sITEM_DOUBLE_ATTR_INFO *		GetDoubleAttrInfo		(INT nCode ) ;

	virtual BOOL								GetRandAttrUnit		(const sITEM_RANDATTR_INFO& randAttr
																				,sITEM_RANDATTR_UNIT**      pRandUnit
																				,sITEM_RANDATTR_UNIT**      pRandUnit2);
	virtual sITEM_RANDATTR_UNIT *			GetRandAttrUnitByType(INT nType);
	//float										GetSectorRateByRate	(BYTE RandSectors[RAND_SECTION_MAX]);

public:
	virtual sITEM_OPTION_INFO*				GetOptionInfo			(INT nOptionIndex);
	virtual LPCSTR								GetOptionName			(INT nOptionIndex);

protected:
	virtual BOOL								LoadRandAttrTable	(LPCSTR szFileName, BOOL bReload);
	virtual BOOL								LoadDoubleAttrInfo(LPCSTR szFileName, BOOL bReload);
	virtual VOID								Unload				();


	void	_AddType(sITEM_RANDATTR_TYPE* pInfo,INT nType)
	{
		m_pRandAttrTypeHashTable->Add(pInfo, nType);
		pInfo->m_mpGroups.Initialize(m_dwRandAttrGroupSize);
	}
	void _Push(sITEM_RANDATTR_TYPE* pInfo,sITEM_RANDATTR_GROUP*	pGroup,VRSTR sGroup)
	{
		pInfo->m_mpGroups.Add( pGroup, (DWORD)sGroup);//nGroup );
		pInfo->m_arGroups.push_back( pGroup);
	}
	void _Push(sITEM_RANDATTR_TYPE* pInfo,INT nGroupRate)
	{
		pInfo->m_arRates.push_back(nGroupRate);
	}
	void _Push(sITEM_RANDATTR_GROUP*	pGroup,sITEM_RANDATTR_UNIT*		pUnit)
	{
			pGroup->m_arUnits.push_back(pUnit);
	}


protected:
	DWORD									m_dwRandAttrGroupSize;
	sITEM_OPTION_INFO					m_arOptionInfos[MAX_ITEM_OPTIONS];
	VRSTR									m_pszRandAttrTableFileName		;
	VRSTR									m_pszRandAttrInfoFileName		;
	VRSTR									m_pszDoubleAttrInfoFileName	;
	VRSTR									m_pszOptionInfoFileName			;

private:
	ItemRandAttrInfoTable *			m_pRandAttrInfoHashTable;
	ItemDoubleAttrInfoTable *		m_pDoubleAttrInfoHashTable;
	ItemRandAttrTypeTable *			m_pRandAttrTypeHashTable;
	ItemRandAttrUnitMap *			m_pRandAttrUnitHashTable;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(ItemAttrInfoParser , _RESOURCELIB_API);
//extern _RESOURCELIB_API ItemAttrInfoParser& singleton::GetItemAttrInfoParser();
#define theItemAttrInfoParser  singleton::GetItemAttrInfoParser()

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "ItemAttrInfoParser.inl"


#endif //__ITEMATTRINFOPARSER_H__
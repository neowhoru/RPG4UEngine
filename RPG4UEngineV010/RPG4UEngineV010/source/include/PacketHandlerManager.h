/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerManager.h
创建日期：2007年11月24日
最后更新：2007年11月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	可以考虑把 协议类别与协议进入组合
		1.限定最大组合值  0x0f80|0x7f 即，类别数量：128  协议最大值：128
		2.建立协议组合值，查找表，索引注册的协议信息

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETHANDLERMANAGER_H__
#define __PACKETHANDLERMANAGER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "PacketHandlerDefine.h"
#include "FunctionMap.h"
#include "TLinkerManager.h"
#include "PluginScanListener.h"

#define NONE_INDEXPROC
#ifndef NONE_INDEXPROC
const DWORD MSG_AMOUNT = 1000;
#endif
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct MSG_BASE;
using namespace util;
using namespace gamemain;

typedef TLinkerListenerManager<PacketHandlerLinker,PluginScanListener>  PacketHandlerBase;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API PacketHandlerManager : public PacketHandlerBase
{
public:
	PacketHandlerManager();
	~PacketHandlerManager();

protected:
	typedef BOOL (*FnProcHandler)(LPARAM lpParam,MSG_BASE * pMsg , DWORD dwSize  );

#ifndef NONE_INDEXPROC
	struct FUNC_PacketHandler
	{
#ifdef _DEBUG
		LPCSTR	_szFile;
		INT		_nLineNo;
		LPCSTR	_szMsgName;
#endif
		FnProcHandler	m_fnHandler;
	};
#else
	struct FUNC_PacketHandler : public util::BASE_FUNC
	{
		FnProcHandler	m_fnHandler;

		virtual void Release();
	};
#endif


public:
	///< @param byCategoryExtra 用于Category（低字节）组合，在高字节
	///< @param byProtocolExtra 用于Protocol（低字节）组合，在高字节
	BOOL	ParsePacket	(LPARAM			lpParam
							,MSG_BASE*		pMsg
							,DWORD			dwSize = INVALID_DWORD_ID
							,BYTE				byProtocolExtra=0
							,BYTE				byCategoryExtra=0);

#ifdef _DEBUG
	BOOL	AddHandler	(BYTE          byProtocol
                     ,BYTE          byCategory
							,FnProcHandler FnProcHandler
							,LPCSTR        szMsgName
							,LPCSTR        szFile
							,INT           nLineNo
							,BYTE				byProtocolExtra=0
							,BYTE				byCategoryExtra=0);
#endif
	BOOL	AddHandler	(BYTE          byProtocol
                     ,BYTE          byCategory
							,FnProcHandler FnProcHandler
							,BYTE				byProtocolExtra=0
							,BYTE				byCategoryExtra=0);


	void IntegratePacketHandlers(){ScanLinkers(TRUE);}

protected:
	virtual void PrevScanLinkers();
	virtual void OnLinkerScan(PacketHandlerLinker* pLinker);
	virtual void AfterScanLinkers();

protected:

#ifndef NONE_INDEXPROC
	FUNC_PacketHandler		m_arProcHandlers[MSG_AMOUNT];
#else
	FunctionMap			m_FunctionMap;
#endif
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(PacketHandlerManager , _BASE_API);
//extern _BASE_API PacketHandlerManager& singleton::GetPacketHandlerManager();
#define thePacketHandlerManager  singleton::GetPacketHandlerManager()



#endif //__PACKETHANDLERMANAGER_H__
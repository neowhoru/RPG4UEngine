/*////////////////////////////////////////////////////////////////////////
文 件 名：KeyQueueManager.h
创建日期：2008年3月27日
最后更新：2008年3月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
		用来管理按键优先顺序，譬如，处理Esc键一样，很多功能在取消或关闭时，
	都在使用Esc；举个例子来说吧，
		1.锁了怪物，需要按Esc键取消
		2.属性窗口打开了，需要Esc键取消
		3.系统菜单，需要按Esc触发
		通过KeyQueueManager，就可以把它们的触发函数压到队列中来处理
		系统菜单的，作为默认处理即可

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __KEYQUEUEMANAGER_H__
#define __KEYQUEUEMANAGER_H__
#pragma once


#include "TArray.h"
#include "InputDefine.h"


namespace input
{ 
typedef void (*fn_CallBack)(DWORD,DWORD);

struct keyMsg
{
	fn_CallBack DoSomething;
	BYTE        bPriority;   
	DWORD       dwType;		
	DWORD       wParam;
	DWORD       lParam;
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _INPUTLIB_API KeyQueueManager
{
public:
	KeyQueueManager	();
	~KeyQueueManager	();

	virtual BOOL Init			();
	virtual void Release		();
	virtual void Reset		();
	virtual BOOL FrameMove	(DWORD dwTick);

public:
	BOOL PushBack		(DWORD			dwType, fn_CallBack,DWORD dwParam=0,DWORD lParam=0);
	BOOL PushBack		(keyMsg &		msg);
	BOOL DeleteMsg		(DWORD			dwType);
	void SetKey			(eKEY_CODE		key);
	void SetDefaultFun(fn_CallBack	func);
	int  Size			();

protected:	
	TArray<keyMsg>		m_MsgArray;
	fn_CallBack			m_defulatFunction;  
	eKEY_CODE			m_key;
};
};//namespace input

#include "KeyQueueManager.inl"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(input::KeyQueueManager,KeyQueueManager , _INPUTLIB_API);
//extern _INPUTLIB_API KeyQueueManager& singleton::GetKeyQueueManager();
#define theKeyQueueManager  singleton::GetKeyQueueManager()




#endif //__KEYQUEUEMANAGER_H__
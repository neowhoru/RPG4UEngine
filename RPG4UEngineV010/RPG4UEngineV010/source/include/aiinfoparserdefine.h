/*////////////////////////////////////////////////////////////////////////
文 件 名：AIInfoParserDefine.h
创建日期：2008年6月10日
最后更新：2008年6月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __AIINFOPARSERDEFINE_H__
#define __AIINFOPARSERDEFINE_H__
#pragma once


enum{MAX_AI_STATE=3};

enum eAI_INFOTYPE
{
	 AI_TYPE_IDLE			=	0	//	闲时
	,AI_TYPE_SPAWN_IDLE	=	1	//	出生点
	,AI_TYPE_STOP_IDLE	=	2	//	静止
	,AI_TYPE_WANDER		=	3	//	游荡
	,AI_TYPE_PATROL		=	4	//	巡逻
	,AI_TYPE_TRACK			=	5	//	跟踪
	,AI_TYPE_ATTACK		=	6	//	攻击
	,AI_TYPE_HELPREQUEST	=	7	//	求救
	,AI_TYPE_DEAD			=	8	//	死亡
	,AI_TYPE_THRUST		=	9	//	推动
	,AI_TYPE_FLYING		=	10	//	漂浮
	,AI_TYPE_KNOCKDOWN 	=	11	//	倒下
	,AI_TYPE_JUMP			=	12	//	跳跃
	,AI_TYPE_FALL_APART	=	13	//	粉碎
	,AI_TYPE_RETURN		=	14	//	返归
	,AI_TYPE_RETREAT		=	15	//	撤退
	,AI_TYPE_RUNAWAY		=	16	//	逃跑
	,AI_TYPE_CHAOS			=	17	//	混乱
	,AI_TYPE_TRACK_AREA	=	18	//	搜索区域
	,AI_TYPE_FOUND_ENEMY	=	19	//	发现敌情
	,AI_TYPE_FOLLOW		=	20	//	跟随
	,AI_TYPE_SKILLING		=	21	//	发技能 
	,AI_TYPE_ATTACK_FORCE=	22	//	强攻
	,AI_TYPE_ATTACKED		=	23	//	受攻
	,AI_TYPE_LEAVE_FIELD	=	24	//	离战区
	,AI_TYPE_STUN			=	25	//	眩晕
	,AI_TYPE_LETS_GO		=	26	//	一起走
	,AI_TYPE_FEAR			=	27	//	混乱
	,AI_TYPE_STATE_CHANGE=	28	//	换状态
	,AI_TYPE_HELPIT		=	29	//	救助
	,MAX_AI_TYPE
};

//	AI编号	状态编号	静止型1	静止型2	静止型3	游荡型1	游荡型2	游荡型3	归家型1	归家型2	归家型3	巡逻型1	巡逻型2	巡逻型3	搜索型1	搜索型2	搜索型3	
//						   被动1	被动2	被动3	就近攻击1	就近攻击2	就近攻击3	低血攻击1	低血攻击2	低血攻击3	低级攻击1	低级攻击2	低级攻击3	高魔攻击1	高魔攻击2	高魔攻击3	单人攻击1	单人攻击2	单人攻击3
struct sAI_INFO_UNIT
{
	DWORD		m_Code;
	WORD		m_AIType;
	VRSTR		m_Name;
	BYTE		m_Rate;									//机率 100满
	DWORD		m_Wanders[MAX_AI_STATE];			//eMOVE_ATTITUDE_WANDER			游荡型
	DWORD		m_Idles	[MAX_AI_STATE];			//eMOVE_ATTITUDE_ETERNAL_STOP	静止型
	DWORD		m_Backs	[MAX_AI_STATE];			//eMOVE_ATTITUDE_BACK_SPAWN	归家型
	DWORD		m_Patrols[MAX_AI_STATE];			//eMOVE_ATTITUDE_PATROL			巡逻型
	DWORD		m_Searchs[MAX_AI_STATE];			//eMOVE_ATTITUDE_SEARCH_AREA	搜索型

	DWORD		m_Passives	[MAX_AI_STATE];		//ATTACK_ATTITUDE_PASSIVE				 被动攻击
	DWORD		m_Nearests	[MAX_AI_STATE];		//ATTACK_ATTITUDE_NEAREST_FIRST		 就近攻击
	DWORD		m_LowHPs		[MAX_AI_STATE];		//ATTACK_ATTITUDE_LOW_HP_FIRST		 低血攻击
	DWORD		m_LowLVs		[MAX_AI_STATE];		//ATTACK_ATTITUDE_LOW_LEVEL_FIRST	 低级攻击
	DWORD		m_HighMPs	[MAX_AI_STATE];		//ATTACK_ATTITUDE_HIGH_MP_FIRST		 高魔攻击
	DWORD		m_Singles	[MAX_AI_STATE];		//ATTACK_ATTITUDE_ONE_TARGET			 单目标攻击
};


struct sAI_INFO
{
	DWORD					m_AICode;
	VRSTR					m_Name;
	sAI_INFO_UNIT		m_Units[MAX_AI_TYPE];
};

using namespace std;
typedef map<DWORD, sAI_INFO>		AIInfoMap;
typedef pair<DWORD, sAI_INFO>		AIInfoMapPair;
typedef AIInfoMap::iterator		AIInfoMapIt;




#endif //__AIINFOPARSERDEFINE_H__
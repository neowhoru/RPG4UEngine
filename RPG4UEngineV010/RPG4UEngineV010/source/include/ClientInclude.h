/*////////////////////////////////////////////////////////////////////////
文 件 名：ClientMainInclude.h
创建日期：2008年3月18日
最后更新：2008年3月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CLIENTMAININCLUDE_H__
#define __CLIENTMAININCLUDE_H__
#pragma once


//#include "TextResDefine.h"
#include "FormatString.h"


#include "UIConst.h"
#include "ConstDefine.h"
#include "CommonDefine.h"
#include "RPGGameDefine.h"
#include "GameConst.h"
#include "ResultCode.h"
//#include "ConstTextRes.h"
#include "ConstSlot.h"


//#include "StructBase.h"
//#include "PacketStruct_Base.h"
//#include "Protocol_ClientGameS.h"
//#include "Protocol_ClientWorldS.h"
//#include "Protocol_ClientAgent.h"
//#include "PacketStruct_ClientGameS.h"
//#include "PacketStruct_ClientWorldS.h"
//#include "PacketStruct_ClientAgent.h"


#include "MathLib.h"

#include "BaseSetting.h"
#include "VUIBaseData.h"
#include "GameDefine.h"
#include "GameUtil.h"
#include "GlobalUtil.h"
#include "GlobalData.h"
#include "GameParameter.h"
#include "ApplicationSetting.h"
#include "GameClientApplication.h"

#include "LogSystem.h"
#include "MediaPathManager.h"
#include "PathInfoParser.h"

#include "Camera.h"
#include "InputLayer.h"
#include "Mouse.h"
#include "MouseHandler.h"
#include "GameRunning.h"


#include "SkillInfoParser.h"
#include "ItemInfoParser.h"
#include <NPCInfoParser.h>

#include "SlotKeyGenerator.h"
#include "SkillQueueManager.h"
#include "KeyQueueManager.h"

#include "FreshmanTipManager.h"
#include "TextResManager.h"
#include "GameUIManager.h"
#include "VUICursorManager.h"
#include "CursorHandler.h"
#include "VUCtrlManager.h"
#include "TipLogManager.h"
#include "ScreenTipManager.h"
#include "ToolTipManager.h"
#include "ColorManager.h"

#include "IV3DEngine.h"
#include "IV3DTexture.h"
#include "IV3DTextureManager.h"
#include "IV3DRenderer.h"
#include "V3DGraphicDDraw.h"
#include "GameEffectManager.h"
#include "SkillSpecialEffectManager.h"
#include "V3DGameWorld.h"
#include "V3DGraphicDDraw.h"


#include <ItemSlot.h>
#include "StyleSlot.h"
#include "StyleContainer.h"
#include "BaseContainer.h"
#include <PlayerAttributes.h>
#include <AttributeAffectorItemHandle.h>
#include <AttributeAffectorSkillHandle.h>
#include "SlotHandle.h"


#include "Character.h"
#include "Player.h"
#include "Item.h"
#include "Monster.h"
//#include "HeroData.h"
#include "Hero.h"
#include "Clone.h"
#include "ObjectManager.h"
#include "ObjectSystem.h"
#include "AppearanceManager.h"
#include "ItemSystem.h"
#include "ItemManager.h"
#include "ItemManagerNet.h"
#include "ItemLogManager.h"
#include "HeroTipLayer.h"
#include "HeroActionInput.h"
#include "Map.h"
#include "CloneManager.h"

#include "VCharacter.h"
#include "VObjectManager.h"
#include "VObjectSystem.h"
#include "VObject.h"
#include "VHeroInputData.h"
#include "VItem.h"
#include "VHero.h"
#include "VHeroActionInput.h"


#ifdef _USE_NET
#include "GameClientSocket.h"
#include "NetworkLayer.h"
#endif

#include "SoundEffect.h"
#include "SoundLayer.h"
#include "SoundBGM.h"

#include "FrameworkSystem.h"
#include "LoadScene.h"
#include "SceneLogicFlow.h"
#include "FormularManager.h"
#include "ScriptManager.h"
#include "QuestManager.h"

#include "PathHandler.h"
#include "TileWorldClient.h"

#include "GameMainUtil.h"
#include "FilePathUtil.h"
#include "RenderUtil.h"
#include "ObjectUtil.h"
#include "BattleUtil.h"
#include "StringUtil.h"


#endif //__CLIENTMAININCLUDE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TradeDialog.h
创建日期：2009年5月21日
最后更新：2009年5月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TRADEDIALOG_H__
#define __TRADEDIALOG_H__
#pragma once

#include "TradeDialogDefine.h"
#include "ContainerDialogBase.h"

_NAMESPACEU(Player, object);
class BaseContainer;
class BaseSlot;


class _CLIENTGAMELIB_API TradeDialog : public ContainerDialogBase
{
public:
	TradeDialog();
	virtual ~TradeDialog();

public:
	virtual BOOL	Init			()=0;
	virtual VOID	Release		()=0;
	virtual VOID	ResetTrade	();

public:

	virtual VOID	Update				()=0;
	virtual BOOL	InitContainer		()=0;
	virtual VOID	ReleaseContainer	()=0;
	virtual VOID	FlushSlotListener	()=0;

public:
	virtual BOOL	InsertSlot(SLOTPOS    posIndex
                            ,BaseSlot & slotDat
									 ,BOOL		 bViewContainer)	=0;
	virtual VOID	DeleteSlot(SLOTPOS    posIndex
                            ,BaseSlot * pSlotOut
									 ,BOOL		 bViewContainer)		=0;

public:
	virtual VOID	NetworkProc	( MSG_BASE * pMsg )=0;
	virtual VOID	ShowWindow(BOOL val)=0;
	virtual VOID	CloseWindow()=0;

public:
	virtual VOID	RequestTrade	(DWORD dwObjectKey	)=0;
	virtual VOID	OnRequestTrade	(DWORD dwReqPlayerKey)=0;
	virtual VOID	OnResponseTrade()=0;
	virtual VOID	CancelTrade		()=0;
	virtual VOID	OnCancelTrade	()=0;

	virtual VOID	UnlockTrade		(BOOL bRedraw=TRUE)=0;
	virtual VOID	LockTrade		(BOOL bRedraw=TRUE)=0;

	virtual BOOL	PutItem			(POSTYPE tradePos, POSTYPE				invenPos)=0;
	virtual VOID	OnTradePut1		(POSTYPE tradePos, POSTYPE				invenPos)=0;
	virtual VOID	OnTradePut2		(POSTYPE tradePos, ITEMOPT_STREAM&	item)=0;
	virtual BOOL	GetItem			(POSTYPE tradePos)=0;
	virtual VOID	OnTradeGet1		(POSTYPE tradePos)=0;
	virtual VOID	OnTradeGet2		(POSTYPE tradePos)=0;

	virtual BOOL	SwapItem			(POSTYPE fromPos, POSTYPE				toPos)=0;
	virtual VOID	OnTradeSwap1	(POSTYPE fromPos, POSTYPE				toPos)=0;
	virtual VOID	OnTradeSwap2	(POSTYPE fromPos, POSTYPE				toPos)=0;

	virtual BOOL	PutMoney			(MONEY money)=0;
	virtual VOID	PutMoneySending(MONEY money)=0;
	virtual VOID	OnMoneyPut1		(MONEY money)=0;
	virtual VOID	OnMoneyPut2		(MONEY money)=0;
	virtual VOID	OnMoneyGet1		(MONEY money)=0;
	virtual VOID	OnMoneyGet2		(MONEY money)=0;

	virtual VOID	Proposal		()=0;
	virtual VOID	OnProposal1	()=0;
	virtual VOID	OnProposal2	()=0;

	virtual VOID	ModifyTrade	()=0;
	virtual VOID	OnModify1	()=0;
	virtual VOID	OnModify2	()=0;

	virtual VOID	Accept		()=0;
	virtual VOID	OnAccept1	()=0;
	virtual VOID	OnAccept2	()=0;

	virtual VOID	OnTradeFinished	(MONEY money, sTOTALINFO_TRADE& tradeInfo)=0;

	virtual VOID	ProcessError		(DWORD dwError	, BOOL bReset=TRUE)=0;
	virtual VOID	ProcessErrorTip	(DWORD dwErrorTip)=0;

public:
	VG_PTR_GET_PROPERTY		(TradeContainer,	BaseContainer);	///目标槽容器
	VG_PTR_GET_PROPERTY		(ViewContainer,	BaseContainer);	///副物品容器
	VG_BOOL_GET_PROPERTY		(Started);
	VG_PTR_GET_PROPERTY		(PlayerTrading, Player);
	VG_BOOL_GET_PROPERTY		(LockTrade1);
	VG_BOOL_GET_PROPERTY		(LockTrade2);
	VG_DWORD_GET_PROPERTY	(Status1	);
	VG_DWORD_GET_PROPERTY	(Status2	);
	VG_TYPE_GET_PROPERTY		(Money1  , MONEY	);
	VG_TYPE_GET_PROPERTY		(Money2	, MONEY);

	BOOL							m_bModifyPending		;
	UINT							m_uiFunctionIndex		;
   sTRADE_PENDING_PUT        m_modpendingPut		;
   sTRADE_PENDING_GET        m_modpendingGet		;	
   sTRADE_PENDING_PUTMONEY   m_modpendingPutMoney	;
   sTRADE_PENDING_SWAP       m_modpendingSwap		;

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(TradeDialog , _CLIENTGAMELIB_API);
//extern API_NULL TradeDialog& singleton::GetTradeDialog();
#define theTradeDialog  singleton::GetTradeDialog()



#endif //__TRADEDIALOG_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ResPublishImpl.h
创建日期：2007年4月5日
最后更新：2007年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __RESPUBLISHIMPL_H__
#define __RESPUBLISHIMPL_H__
#pragma once

#include "CommonDefine.h"
#include "GlobalInstanceSingletonDefine.h"
#include "ResPublishListener.h"
#include "BaseInfoParserHelper.h"

class ResPublishManager;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class API_NULL ResPublishImpl : public ResPublishListener	
										, public BaseInfoParserHelper
{
public:
	ResPublishImpl();
	~ResPublishImpl();

public:
	virtual BOOL	PublishNormalRes	(LPCSTR szResFile);
	virtual BOOL	PublishTexture		(LPCSTR szResFile);
	virtual BOOL	PublishDir			(LPCSTR szPath, INT nExtNum=0,LPCSTR* ppExts=NULL);
	virtual BOOL	PublishMesh			(LPCSTR szPath);

public:
	virtual void OnPublishReady	(DWORD dwAmount);
	virtual void OnPublishStep		(DWORD dwStep)	;
	virtual void OnPublishFinished();
	virtual void OnPublishFile		(LPCSTR szResFile, LPCSTR szResTo);

public:
	VG_PTR_PROPERTY	(Publish, ResPublishManager);

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ResPublishImpl , API_NULL);
//extern API_NULL ResPublishImpl& singleton::GetResPublishImpl();
#define theResPublishImpl  singleton::GetResPublishImpl()

#endif //__RESPUBLISHIMPL_H__
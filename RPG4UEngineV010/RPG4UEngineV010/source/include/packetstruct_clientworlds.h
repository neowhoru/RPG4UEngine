/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientWorldS.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

 *  Prefix
		C : Client
		G : Game Server
		M : Master Server
		D : DBP Server
		W : World Server

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTWORLDS_H__
#define __PACKETSTRUCT_CLIENTWORLDS_H__
#pragma once


#include "StructBase.h"
#include "PacketStruct_Base.h"
#include <Protocol_ClientWorldS.h>
#include "PacketStruct_ClientWorldS_Define.h"

#pragma pack(push,1)



/*////////////////////////////////////////////////////////////////////////
// CW_CONNECTION
/*////////////////////////////////////////////////////////////////////////

//------------------------------------------
MSGPACKET_CW_CONNECTION_BEGIN(HEARTBEAT)
//{
	int GetSize()		{ return SIZE_CW_MSG(HEARTBEAT); }
//};
MSGPACKET_CW_END;


//-----------------------------------------
MSGPACKET_CW_CONNECTION_BEGIN(ENTER_SERVER_SYN)
//{
	DWORD				dwAuthUserID;
//};
MSGPACKET_CW_END;



/*////////////////////////////////////////////////////////////////////////
// CW_CHAT
/*////////////////////////////////////////////////////////////////////////


//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(WHISPER_SYN)
//{
	TCHAR		szCharNameTo[MAX_CHARNAME_LENGTH];
	BYTE		byMsgLen;
	TCHAR		szWhisperMsg[MAX_CHAT_LENGTH];

	int GetSize() { return ( SIZE_CW_MSG(WHISPER_SYN) - ( sizeof( TCHAR) * ( MAX_CHAT_LENGTH - byMsgLen ) ) ); }
//};
MSGPACKET_CW_END;


//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(WHISPER_ACK)
//{
	BYTE		byMsgLen;
	TCHAR		szCharNameFrom	[MAX_CHARNAME_LENGTH];
	TCHAR		szWhisperMsg	[MAX_CHAT_LENGTH];

	int GetSize() { return ( SIZE_CW_MSG(WHISPER_ACK) - ( sizeof( TCHAR) * ( MAX_CHAT_LENGTH - byMsgLen ) ) ); }
//};
MSGPACKET_CW_END;


//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(WHISPER_NAK)
//{
	enum CONST_VAL
	{
		USER_NOT_FOUND,
		USER_WHISPER_OFF,
	};

	BYTE		byReason;
	int GetSize() { return SIZE_CW_MSG(WHISPER_NAK); }
//};
MSGPACKET_CW_END;



//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(NOTICE_SYN)
//{
	BYTE		byLen;
	char		szMsg[MAX_NOTICE_LENGTH];

	int GetSize() { return ( SIZE_CW_MSG(NOTICE_SYN) - ( sizeof(char) * ( MAX_NOTICE_LENGTH - byLen ) ) ); }
//};
MSGPACKET_CW_END;


//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(NOTICE_BRD)
//{
	WORD		wLen;
	char		szMsg[MAX_NOTICE_LENGTH];

	int GetSize() { return ( SIZE_CW_MSG(NOTICE_BRD) - ( sizeof(char) * ( MAX_NOTICE_LENGTH - wLen ) ) ); }
//};
MSGPACKET_CW_END;



//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(WHISPER_SET_SYN)
//{

	BOOL	bWhisper;

	int GetSize() { return SIZE_CW_MSG(WHISPER_SET_SYN); }
//};
MSGPACKET_CW_END;



//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(WHISPER_SET_ACK)
//{
	BOOL	bWhisper;
	int GetSize()		{ return SIZE_CW_MSG(WHISPER_SET_ACK); }
//};
MSGPACKET_CW_END;


//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(WHISPER_SET_NAK)
//{
	int GetSize()		{ return SIZE_CW_MSG(WHISPER_SET_NAK); }
//};
MSGPACKET_CW_END;


//-----------------------------------------
//MSGPACKET_CW_CHAT_BEGIN()
////{
////};
//MSGPACKET_CW_END;


//-----------------------------------------
MSGOBJECT_CW_CHAT_BEGIN(CHAT_VILLAGE_SYN)
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH };

	PLAYERKEY	m_ChatPlayer;
	BYTE			m_byMsgLength;
	TCHAR			m_pszChatMsg[ _MAX_CHATMSG_SIZE ];

	int		GetSize()
	{
		return ( SIZE_CW_MSG(CHAT_VILLAGE_SYN) - ( sizeof(TCHAR) * (_MAX_CHATMSG_SIZE - m_byMsgLength) ) );
	}
//};
MSGPACKET_CW_OBJECT_END;


//-----------------------------------------
MSGSUPER_CW_CHAT_BEGIN(CHAT_VILLAGE_BRD,CHAT_VILLAGE_SYN)
//{
//};
MSGPACKET_CW_SUPER_END;




//-----------------------------------------
MSGOBJECT_CW_CHAT_BEGIN(CHAT_BATTLE_SYN)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH };
	BYTE						m_byMsgLength;
	char						m_pszChatMsg[ _MAX_CHATMSG_SIZE ];

	int	GetSize()
	{
		return ( SIZE_CW_MSG(CHAT_BATTLE_SYN) - ( sizeof(char) * (_MAX_CHATMSG_SIZE - m_byMsgLength) ) );
	}
//};
MSGPACKET_CW_OBJECT_END;


//-----------------------------------------
MSGOBJECT_CW_CHAT_BEGIN(CHAT_BATTLE_BRD)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH };

	TCHAR						m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE						m_byMsgLength;
	char						m_pszChatMsg[ _MAX_CHATMSG_SIZE ];
	int	GetSize()
	{
		return ( SIZE_CW_MSG(CHAT_BATTLE_BRD) - ( sizeof(char) * (_MAX_CHATMSG_SIZE - m_byMsgLength) ) );
	}
//};
MSGPACKET_CW_OBJECT_END;

//-----------------------------------------
MSGOBJECT_CW_CHAT_BEGIN(CHAT_SHOUT_SYN)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH };

	BYTE		m_byMsgLength;
	char		m_pszChatMsg[ _MAX_CHATMSG_SIZE ];
	int	GetSize()
	{
		return ( SIZE_CW_MSG(CHAT_SHOUT_SYN) - ( sizeof(char) * (_MAX_CHATMSG_SIZE - m_byMsgLength) ) );
	}
//};
MSGPACKET_CW_OBJECT_END;



//-----------------------------------------
MSGPACKET_CW_CHAT_BEGIN(CHAT_SHOUT_BRD)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH };

	char	m_szCharName[MAX_CHARNAME_LENGTH];
	BYTE	m_byMsgLength;
	char	m_pszChatMsg[ _MAX_CHATMSG_SIZE ];

	int GetSize()
	{
		return ( SIZE_CW_MSG(CHAT_SHOUT_BRD) - ( sizeof(char) * (_MAX_CHATMSG_SIZE - m_byMsgLength) ) );
	}
//};
MSGPACKET_CW_END;



/*////////////////////////////////////////////////////////////////////////
GM
/*////////////////////////////////////////////////////////////////////////

//-----------------------------------------
MSGPACKET_CW_GM_BEGIN(GM_NOTICE_SYN)
//{
	enum eNOTICE
	{
		NOTICE_WORLD,		
		NOTICE_CHANNEL,	
		NOTICE_ZONE,		
	};
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH };


	BYTE		byNoticeType;					
	DWORD		dwNoticeLength;					
	char		szNotice[_MAX_CHATMSG_SIZE];	

	WORD		GetSize() { return SIZE_CW_MSG(GM_NOTICE_SYN); }
//};
MSGPACKET_CW_END;



//-----------------------------------------
MSGPACKET_CW_GM_BEGIN(GM_NOTICE_NAK)
//{
	enum CONST_VAL
	{
		ERR_NOTICE_DEFAULT,
	};

	DWORD		dwResultCode;
//};
MSGPACKET_CW_END;



//-----------------------------------------
MSGPACKET_CW_GM_BEGIN( GM_STRING_CMD_SYN )
//{
	enum CONST_VAL { MAX_STRING_CMD_LENGTH = MAX_STR_CMD_LENGTH };
	TCHAR				m_szStringCmd[MAX_STRING_CMD_LENGTH];	
//};
MSGPACKET_CW_END;


//-----------------------------------------
MSGPACKET_CW_GM_BEGIN( GM_STRING_CMD_NAK )
//{
	BYTE				m_byErrorCode;								// eGM_RESULT
//};
MSGPACKET_CW_END;


/*////////////////////////////////////////////////////////////////////////
CW_VIEWPORT
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////
MSGPACKET_CW_OBJECT_BEGIN(VIEWPORT, VIEWPORT_CHARSTATE)
//{
	DWORD			dwSectorIndex;
	DWORD			dwFieldCode;

	int GetSize()		{ return SIZE_CW_MSG(VIEWPORT_CHARSTATE); }
//};
MSGPACKET_CW_OBJECT_END;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MSGPACKET_CW_BEGIN(ARMORKIT,ARMORKIT_FINDHACK_SYN)
//{
	int		HackType;
	int		Size;	
	TCHAR		Buf[1024];

	int GetSize()		{ return SIZE_CW_MSG(ARMORKIT_FINDHACK_SYN); }
//};
MSGPACKET_CW_END;





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct STRUCT_FRIEND_STAT
{
	TCHAR			szFriendName[MAX_CHARNAME_LENGTH];	
	BOOL			bOnline;
};


//////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_NEWLIST_SYN)
//{
	int GetSize()		{ return SIZE_CW_MSG(FRIEND_NEWLIST_SYN); }
//};
MSGPACKET_CW_END;



//////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_FRIENDLIST_ACK)
//{
	BYTE					byFriendStatNum;				
	sFRIEND_INFO_BASE	sFriendStat[MAX_FRIENDBLOCK_STAT_NUM];

	int GetSize()
	{ 
		return ( SIZE_CW_MSG(FRIEND_FRIENDLIST_ACK) - ( (MAX_FRIENDBLOCK_STAT_NUM - byFriendStatNum) * sizeof(sFRIEND_INFO_BASE) ) );
	}
//};
MSGPACKET_CW_END;


//////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCKLIST_ACK)
//{
	USHORT				byBlockStatNum;					
	sBLOCK_INFO_BASE	sBlockStat[MAX_FRIEND_STAT_NUM];

	int GetSize()
	{ 
		return ( SIZE_CW_MSG(FRIEND_BLOCKLIST_ACK) - ( (MAX_FRIEND_STAT_NUM - byBlockStatNum) * sizeof(sBLOCK_INFO_BASE) ) );
	}
//};
MSGPACKET_CW_END;




//////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_NEWLIST_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,
		NO_FRIEND_DATA,		// 单捞磐 绝促
	};

	DWORD				dwResult;
//};
MSGPACKET_CW_END;



//////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_LOGIN_BRD)
//{
	TCHAR				szFriendName[MAX_CHARNAME_LENGTH];
//};
MSGPACKET_CW_END;


//////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_LOGOUT_BRD)
//{
	TCHAR		szFriendName[MAX_CHARNAME_LENGTH];
//};
MSGPACKET_CW_END;




/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_ADD_SYN)
//{
	TCHAR				ptszCharNameTo[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_ADD_ACK)
//{
	sFRIEND_INFO_BASE	sFriendStat;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_ADD_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,	
		NO_USER,			
		ALREADY_FRIEND,
		FRIEND_IS_ME,	
	};
	BYTE		m_byResult;
//}
MSGPACKET_CW_END;



/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_DEL_SYN)
//{
	TCHAR				szFriendName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_DEL_ACK)
//{
	TCHAR				ptszDeletedName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_DEL_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,	
		NOT_FRIEND_USER,
	};

	BYTE			m_byResult;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCK_SYN)
//{
	TCHAR				szBlockName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCK_ACK)
//{
	TCHAR				szBlockName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCK_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,	
		NO_USER,			
		BLOCKED_ME,		
	};
	DWORD				m_byResult;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCK_FREE_SYN)
//{
	TCHAR				szBlockName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCK_FREE_ACK)
//{
	TCHAR				szBlockName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_BLOCK_FREE_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,	
		NO_USER,		
		NOT_BLOCKED,	
	};

	DWORD				m_byResult;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_CHAT_SYN)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH 	};

	TCHAR				ptszFriendNameTo[MAX_CHARNAME_LENGTH];
	BYTE				byMsgLength;
	TCHAR				ptszChatMsg[ _MAX_CHATMSG_SIZE ];

	WORD GetSize()	{ return (WORD)( SIZE_CW_MSG(FRIEND_CHAT_SYN) - ( sizeof(TCHAR) * (_MAX_CHATMSG_SIZE - byMsgLength) ) ); }
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_CHAT_BRD)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH 	};

	TCHAR				ptszFriendNameFrom[MAX_CHARNAME_LENGTH];
	BYTE				byMsgLength;
	TCHAR				ptszChatMsg[ _MAX_CHATMSG_SIZE ];

	WORD GetSize()	{ return (WORD)( SIZE_CW_MSG(FRIEND_CHAT_BRD) - ( sizeof(TCHAR) * (_MAX_CHATMSG_SIZE - byMsgLength) ) ); }
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_CHAT_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,	
		NOT_FRIEND,		
		OFFLINE_FRIEND,
		BLOCKED_FRIEND,
		BLOCK_CHAT,	
	};


	TCHAR				ptszFriendNameTo[MAX_CHARNAME_LENGTH];	
	DWORD				m_byResult;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_CHATBLOCK_SYN)
//{
	BOOL				m_bBlock;	
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_CHATBLOCK_ACK)
//{
	BOOL		m_bNowBlock;
//}
MSGPACKET_CW_END;



/////////////////////////////////////////////
MSGPACKET_CW_FRIEND_BEGIN(FRIEND_CHATBLOCK_NAK)
//{
	enum CONST_VAL { ERR_DEFAULT = 0	};

	DWORD		dwResult;
//}
MSGPACKET_CW_END;



/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_MEMBER_SYN)
//{
	GUILDGUID	m_GuildGuid;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_MEMBER_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT = 1,
		ERR_NOT_GUILD,			
		ERR_ALREADY_RECVED,	
		ERR_WAIT_INFO,			
	};

	BYTE	m_byErrorCode;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_CHAT_SYN)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH 	};

	GUILDGUID	m_GuildGuid;
	BYTE			m_byMsgLen;
	TCHAR			m_ptszCharMsg[_MAX_CHATMSG_SIZE];

	int			GetSize() { return ( SIZE_CW_MSG(GUILD_CHAT_SYN) - ( sizeof(TCHAR) * ( _MAX_CHATMSG_SIZE - m_byMsgLen ) ) ); }
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_CHAT_BRD)
//{
	enum CONST_VAL { _MAX_CHATMSG_SIZE = MAX_CHATBUFFER_LENGTH	};

	TCHAR		ptszCharName[MAX_CHARNAME_LENGTH];
	BYTE		byMsgLen;
	TCHAR		ptszChatMsg[_MAX_CHATMSG_SIZE];

	int			GetSize() { return ( SIZE_CW_MSG(GUILD_CHAT_BRD) - ( sizeof(TCHAR) * ( _MAX_CHATMSG_SIZE - byMsgLen ) ) ); }
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_CHAT_NAK)
//{
	enum CONST_VAL
	{
		ERR_DEFAULT,
		ERR_NOT_GUILD,	
		ERR_NOT_MEMBER,
		ERR_WAIT_INFO,	
	};

	BYTE		byResult;
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_LOGIN_CHAR_BRD)
//{
	TCHAR	m_szCharName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_LOGOUT_CHAR_BRD)
//{
	TCHAR	m_szCharName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;



/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN(GUILD_MEMBER_BRD)
//{
	enum CONST_VAL { _MAX_GUILD_MEMBER_NUM = 50 	};

	BYTE					m_Count;
	sGUILD_MEMBER_INFO_PACKET	m_Member[_MAX_GUILD_MEMBER_NUM];

	int GetSize()
	{
		return ( SIZE_CW_MSG(GUILD_MEMBER_BRD) - (  sizeof(sGUILD_MEMBER_INFO_PACKET) * ( _MAX_GUILD_MEMBER_NUM - m_Count ) ) );
	}
//}
MSGPACKET_CW_END;


/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN( GUILD_MEMBER_JOIN_BRD )
//{
	sGUILD_MEMBER_INFO_PACKET	m_Member;
//}
MSGPACKET_CW_END;

/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN( GUILD_MEMBER_CHG_BRD )
//{
	sGUILD_MEMBER_INFO_PACKET	m_Member;
//}
MSGPACKET_CW_END;

/////////////////////////////////////////////
MSGPACKET_CW_GUILD_BEGIN( GUILD_MEMBER_WITHDRAW_BRD )
//{
	TCHAR	m_szCharName[MAX_CHARNAME_LENGTH];
//}
MSGPACKET_CW_END;

#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTWORLDS_H__
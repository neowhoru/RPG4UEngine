/*////////////////////////////////////////////////////////////////////////
文 件 名：VFSUnitCache.h
创建日期：2006年1月6日
最后更新：2006年1月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
    虚拟文件系统文件缓存模板类
    用于提高短时间内频繁载入/释放小块文件速度。
    缓存清理规则：
        当缓冲区大于所设定的值时，则将最早加入的至少占用一半缓存的文件清除


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VFSUNITCACHE_H__
#define __VFSUNITCACHE_H__
#pragma once

#include "IVFSystem.h"
#include "MemPool/Memory_Pool.h"
#include "StringEx.h"


using Common::StringEx;
using FileIO::IVFSystem;
using FileIO::IVFSUnit;
using std::map;
using std::list;


typedef Common::TAllocator<unsigned char, 1024*1024, 50>		VFSUnitCacheAllocator;
GLOBALINST_DECL(VFSUnitCacheAllocator , _BASE_API);


namespace FileIO
{
class ZipVFSystem;
class ZipVFSUnit;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template <unsigned int CacheSize>           //Unit类型，缓冲大小
class VFSUnitCache
{
    //文件块
    typedef struct tagFileBlock
    {
        size_t nSize;                       //长度
        unsigned int nRefCount;             //引用计数
        unsigned char* pData;               //内存数据块地址
    }FILEBLOCK;

    //文件信息(用于缓存释放)
    typedef struct tagFileInfo
    {
        StringEx strVFSFileName;
        StringEx strUnitFileName;
        size_t nSize;
    }FILEINFO;

public:
    VFSUnitCache() : m_UseCacheSize(0) {}

    virtual ~VFSUnitCache()
    {
        //释放全部内存块
        while (!m_VFSCacheIndex.empty())
        {
            map<StringEx, FILEBLOCK>& secondindex = m_VFSCacheIndex.begin()->second;
            while (!secondindex.empty())
            {
                GetVFSUnitCacheAllocator().deallocate(secondindex.begin()->second.pData);
                secondindex.erase(secondindex.begin());
            }
            m_VFSCacheIndex.erase(m_VFSCacheIndex.begin());
        }
    }

public:
    //通过文件名获取文件缓存，返回内存地址指针以及通过引用返回nUnitSize
    //如果文件不存在则返回NULL
    unsigned char* GetFileBlockFromCache(StringEx strFileName, size_t& nUnitSize)
    {
        //对本地的cache中进行查找
        StringEx strUnitFileName = ConvFileName(strFileName);
        map<StringEx, FILEBLOCK>::iterator itr = m_LocalFileCacheIndex.find(strUnitFileName);
        if (itr != m_LocalFileCacheIndex.end())
        {
            nUnitSize = itr->second.nSize;
            //缓存大小改变
            if (itr->second.nRefCount == 0)
            {
                m_UseCacheSize-=nUnitSize;
                FILEINFO info;
                info.strUnitFileName = strUnitFileName;
                info.nSize = nUnitSize;
                RemoveCacheList(info);
            }
            itr->second.nRefCount++;
            return itr->second.pData;
        }
        else
        {
            //当cache中未找到此文件，则进行载入
            std::fstream file(_tUNICODECHAR(strUnitFileName), std::ios::in | std::ios::binary);
            if (!file.is_open())
            {
                nUnitSize = 0;
                return NULL;
            }

            file.seekg(0, std::ios::end);
            size_t filesize = file.tellg();

            FILEBLOCK block = {0};
            block.pData = GetVFSUnitCacheAllocator().allocate(filesize+1);
            if (block.pData == NULL)
            {
                file.close();
                nUnitSize = 0;
                return false;
            }

            file.seekg(0, std::ios.beg);
            file.read((char*)block.pData, static_cast<std::streamsize>(filesize));

            block.pData[filesize] = '\0';       //为了方便纯文本文件使用，最后多申请一个字节并且置为空字符。
            block.nRefCount = 1;
            block.nSize = filesize;

            file.close();

            m_LocalFileCacheIndex.insert(pair<StringEx, FILEBLOCK>(strUnitFileName, block));

            nUnitSize = filesize;
            return block.pData;
        }
    }

    //通过VFS对象以及文件名获取文件缓存。返回值同上
    unsigned char* GetFileBlockFromCache(StringEx strFileName, size_t& nUnitSize, ZipVFSystem& vfs)
    {
        StringEx strVFSFileName = ConvFileName(vfs.GetFileName());
        StringEx strUnitFileName = ConvFileName(strFileName);
        map<StringEx, map<StringEx, FILEBLOCK> >::iterator itr = m_VFSCacheIndex.find(strVFSFileName);
        if (itr != m_VFSCacheIndex.end())
        {
            //查找到第二级索引
            map<StringEx, FILEBLOCK>& cache = itr->second;
            map<StringEx, FILEBLOCK>::iterator unititr = cache.find(strUnitFileName);

            if (unititr != cache.end())
            {
                //如果文件已存在，则返回地址
                nUnitSize = unititr->second.nSize;
                if (unititr->second.nRefCount == 0)
                {
                    m_UseCacheSize-=nUnitSize;
                    FILEINFO info;
                    info.strUnitFileName = strUnitFileName;
                    info.strVFSFileName = strVFSFileName;
                    info.nSize = nUnitSize;
                    RemoveCacheList(info);
                }
                unititr->second.nRefCount++;
                return unititr->second.pData;
            }
        }

        size_t size = 0;
        if (!vfs.GetUnitLength(strFileName.c_str(), size))
        {
            return NULL;
        }

        unsigned char* pData = GetVFSUnitCacheAllocator().allocate(size+1);
        if (vfs.GetUnit(strFileName.c_str(), pData, size))
        {
            pData[size] = '\0';                 //为了方便纯文本文件使用，最后多申请一个字节并且置为空字符。
            nUnitSize = size;
            
            FILEBLOCK block = {0};
            block.nRefCount = 1;
            block.nSize = size;
            block.pData = pData;

            //在索引中加入
            if (itr != m_VFSCacheIndex.end())
            {
                map<StringEx, FILEBLOCK>& cache = itr->second;
                cache.insert(pair<StringEx, FILEBLOCK>(strUnitFileName, block));
            }
            else
            {
                m_VFSCacheIndex.insert(pair<StringEx, map<StringEx, FILEBLOCK> >(strVFSFileName, map<StringEx, FILEBLOCK>()));
                map<StringEx, map<StringEx, FILEBLOCK> >::iterator itrsecond = m_VFSCacheIndex.find(strVFSFileName);
                if (itrsecond != m_VFSCacheIndex.end())
                {
                    map<StringEx, FILEBLOCK>& secondindex = itrsecond->second;
                    secondindex.insert(pair<StringEx, FILEBLOCK>(strUnitFileName, block));
                }
            }
            return pData;
        }
        else
        {
            nUnitSize = 0;
            GetVFSUnitCacheAllocator().deallocate(pData);
            return NULL;
        }
    }
    
    //释放文件
    bool ReleaseFileFromCache(const ZipVFSUnit& unit)
    {
        RefreshCache();
        StringEx strUnitFileName = ConvFileName(unit.GetUnitFileName());
        StringEx strVFSFileName = ConvFileName(unit.GetVFSFileName());

        if (strUnitFileName == _T(""))
        {
            return false;
        }

        if (strVFSFileName == _T(""))
        {
            map<StringEx, FILEBLOCK>::iterator unititr = m_LocalFileCacheIndex.find(strUnitFileName);
            if (unititr != m_LocalFileCacheIndex.end())
            {
                unititr->second.nRefCount--;
                if (unititr->second.nRefCount == 0)
                {
                    m_UseCacheSize+=unititr->second.nSize;
                    FILEINFO info;
                    info.nSize = unititr->second.nSize;
                    info.strUnitFileName = strUnitFileName;
                    info.strVFSFileName = strVFSFileName;
                    AddCacheList(info);
                }
                return true;
            }
        }
        else
        {
            map<StringEx, map<StringEx, FILEBLOCK> >::iterator itr = m_VFSCacheIndex.find(strVFSFileName);
            if (itr != m_VFSCacheIndex.end())
            {
                //查找到第二级索引
                map<StringEx, FILEBLOCK>& cache = itr->second;
                map<StringEx, FILEBLOCK>::iterator unititr = cache.find(strUnitFileName);

                if (unititr != cache.end())
                {
                    unititr->second.nRefCount--;
                    if (unititr->second.nRefCount == 0)
                    {
                        m_UseCacheSize+=unititr->second.nSize;
                        FILEINFO info;
                        info.nSize = unititr->second.nSize;
                        info.strUnitFileName = strUnitFileName;
                        info.strVFSFileName = strVFSFileName;
                        AddCacheList(info);
                    }
                    return true;
                }
            }
        }
        return false;
    }

protected:
    //刷新缓存大小，决定释放哪些
    void RefreshCache()
    {
        if (m_UseCacheSize < CacheSize*1024*1024)           //如果使用内存大于CacheSize(CacheSize单位为Mbyte)则清理Cache
        {
            return;
        }
	//	return;
        size_t ReleaseCacheSize = m_UseCacheSize;
        while (m_UseCacheSize > ReleaseCacheSize/2)
        {
            FILEINFO& info = m_CacheList.front();
            if (info.strVFSFileName == _T(""))
            {
                map<StringEx, FILEBLOCK>::iterator itr = m_LocalFileCacheIndex.find(info.strUnitFileName);
                unsigned char* pData = itr->second.pData;
                GetVFSUnitCacheAllocator().deallocate(pData);
                m_LocalFileCacheIndex.erase(itr);
                m_UseCacheSize-=info.nSize;
            }
            else
            {
                map<StringEx, FILEBLOCK>& secondindex = m_VFSCacheIndex.find(info.strVFSFileName)->second;
                map<StringEx, FILEBLOCK>::iterator itr = secondindex.find(info.strUnitFileName);
                unsigned char* pData = itr->second.pData;
                GetVFSUnitCacheAllocator().deallocate(pData);
                secondindex.erase(itr);
                m_UseCacheSize-=info.nSize;
            }
            m_CacheList.pop_front();
        }

    }

    //添加到缓冲列表中
    void AddCacheList(FILEINFO info)
    {
        m_CacheList.push_back(info);
    }

    //从缓存列表中移除
    void RemoveCacheList(FILEINFO info)
    {
        for (list<FILEINFO>::iterator itr = m_CacheList.begin(); itr != m_CacheList.end(); itr++)
        {
            if (itr->strUnitFileName == info.strUnitFileName &&
                itr->strVFSFileName == info.strVFSFileName &&
                itr->nSize == info.nSize)
            {
                m_CacheList.erase(itr);
                break;
            }
        }
    }

private:
    //static Common::TAllocator<unsigned char, 1024*1024, 50> s_Mempool;      //内存池
    map<StringEx, map<StringEx, FILEBLOCK> > m_VFSCacheIndex;               //二级map索引(存储所有ZipVFSystem管理的文件)
    map<StringEx, FILEBLOCK> m_LocalFileCacheIndex;                         //本地文件缓存索引
    list<FILEINFO> m_CacheList;                                             //引用计数为0(已被删除)队列
    size_t m_UseCacheSize;                                                  //当前Cache所占用的大小

private:
    VFSUnitCache(const VFSUnitCache&) {}
    VFSUnitCache& operator = (const VFSUnitCache&) {}

};

//template <unsigned int CacheSize>
//Common::TAllocator<unsigned char, 1024*1024, 50> VFSUnitCache<CacheSize>::s_Mempool;


};


#endif //__VFSUNITCACHE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerDefine.h
创建日期：2007年12月10日
最后更新：2007年12月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETHANDLERDEFINE_H__
#define __PACKETHANDLERDEFINE_H__
#pragma once

#include "TLinker.h"

struct MSG_BASE;

#ifdef NO_DISABLE
enum EMsgCategory
{
	eMsgLogin=0,
	eMsgGameIn=0,
	eMsgObject=0,
	eMsgGuild=0,
	eMsgAI=0,
	eMsgTask=0,
	eMsgPlayer=0,
	eMsgGameSystem=0,
};

enum EMsgHandleErrorCode
{
	eMsgHandleError = -1,
	eMsgHandleOK,
	eMsgHandleFinished,
	eMsgHandleIgore,
	eMsgHandleDBDefault,
	eMsgHandleDBInterfaceDefault,
	eMsgHandleGateDefault,
	eMsgHandlePassNext,
	eMsgHandlePlayerNotFound,
	eMsgHandleObjectNotFound,
	eMsgHandleStatusError,
	eMsgHandleDataNull,
	eMsgHandleDataError,
	eMsgHandleTimeNotEnough,
};


#define	MSG_RETURN_NEXT		return eMsgHandlePassNext
#define	MSG_RETURN_FINISHED	return eMsgHandleFinished
#define	MSG_RETURN_OK			return eMsgHandleOK
#endif

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PacketHandlerManager;
class _BASE_API PacketHandlerLinker : public TLinker<PacketHandlerLinker>
{
public:
	PacketHandlerLinker()
	{
	}
	~PacketHandlerLinker()
	{
	}

	virtual void RegisterHandler(PacketHandlerManager& packetHandler)=0;
};


#ifdef _DEBUG
#define	PACKETHANDLER_LINKER_DEBUGINFO(name,file,line)			,name,file,line
#else
#define	PACKETHANDLER_LINKER_DEBUGINFO(name,file,line)
#endif

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_LINKER2(NAME,protocol,category, FnProcHandler,protocolExtra,categoryExtra)\
namespace packethandler\
{\
	class PacketHandlerLinker##NAME##Linker : public PacketHandlerLinker\
	{\
	public:\
		virtual void RegisterHandler(PacketHandlerManager& packetHandler)\
		{\
			packetHandler.AddHandler(protocol\
											,category\
											,FnProcHandler\
											 PACKETHANDLER_LINKER_DEBUGINFO(#protocol,__FILE__,__LINE__)\
											,protocolExtra\
											,categoryExtra);\
		}\
	};\
	static PacketHandlerLinker##NAME##Linker	sInstPacketHandlerLinker##NAME##Linker;\
};

#define PACKETHANDLER_LINKER(NAME,protocol,category, FnProcHandler)\
				PACKETHANDLER_LINKER2(NAME,protocol,category, FnProcHandler,0,0)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_METHOD_DECL( Protocal , category )\
	static BOOL OnHandle_##Protocal( LPARAM lpParam,MSG_BASE * pMsg, DWORD dwSize )



/////////////////////////////////
#define PACKETHANDLER_METHOD_IMPL( Class, Protocal, category)\
						PACKETHANDLER_LINKER	(Class##OnHandle_##Protocal\
													,Protocal,category\
													,Class::OnHandle_##Protocal);\
						BOOL Class::OnHandle_##Protocal( LPARAM lpParam,MSG_BASE * pMsg, DWORD dwSize )

/////////////////////////////////
#define PACKETHANDLER_METHOD_IMPL2( Class, Protocal, category,ProtocalExtra, categoryExtra)\
						PACKETHANDLER_LINKER2(Class##OnHandle_##Protocal\
													,Protocal,category\
													,Class::OnHandle_##Protocal\
													,ProtocalExtra, categoryExtra);\
						BOOL Class::OnHandle_##Protocal( LPARAM lpParam,MSG_BASE * pMsg, DWORD dwSize )



#define PACKETHANDLER_METHOD_CALL( Protocal , category ,pMsg)\
						return OnHandle_##Protocal(  lpParam,   pMsg, 0 )

////////////////////////////////////////////
#define PACKETHANDLER_DECL( Protocal )\
	static BOOL OnHandle_##Protocal( LPARAM lpParam,MSG_BASE * pMsg, DWORD dwSize )

#define PACKETHANDLER_IMPL( Class, Protocal, category)	PACKETHANDLER_METHOD_IMPL(Class, Protocal, category)



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_CLIENT_BEGIN( Class, Protocal, category)\
			PACKETHANDLER_METHOD_IMPL(Class,Protocal , category)\
			{\
				dwSize;	lpParam;\
				__CHECK_PTR(pMsg);

#define PACKETHANDLER_CLIENT_END( )\
				return TRUE;\
				;\
			}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_SERVER_BEGIN( Class, Protocal, category,sessionType)\
			PACKETHANDLER_METHOD_IMPL2(Class,Protocal , category,0,sessionType)\
			{\
				dwSize;	lpParam;\
				__CHECK_PTR(pMsg);

#define PACKETHANDLER_SERVER_END( )\
				return TRUE;\
				;\
			}

#ifdef NO_DISABLE
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_METHOD_CHARACTER( Class, Protocal, category)\
			PACKETHANDLER_METHOD_IMPL(Class,Protocal , eMsgPlayer)\
			{\
				;\
				dwSize;	lpParam;\
				GameCharacter* pChar = (GameCharacter*)GetGameObjectManager().FindObject(pMsg->GetCharID());\
				if (pChar == NULL || !pChar->IsCharacter())\
					return eMsgHandlePlayerNotFound;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_METHOD_PLAYER( Class, Protocal, category)\
			PACKETHANDLER_METHOD_IMPL(Class,Protocal , eMsgPlayer)\
			{\
				;\
				dwSize;	lpParam;\
				GamePlayer* pPlayer = (GamePlayer*)GetGamePlayerManager().FindPlayer(pMsg->GetCharID());\
				if (pPlayer == NULL || !pPlayer->IsPlayer())\
					return eMsgHandlePlayerNotFound;


#define PACKETHANDLER_METHOD_PLAYER_GAMEIN( Class, Protocal, category)\
				PACKETHANDLER_METHOD_PLAYER( Class, Protocal, category)\
				if( !pPlayer->IsStatusGameIn() ) \
					return eMsgHandleStatusError;\

#define PACKETHANDLER_METHOD_PLAYER_SKILL( Class, Protocal, category)\
				PACKETHANDLER_METHOD_PLAYER( Class, Protocal, category)\
				if( !pPlayer->IsStatusSkillNotHold() ) \
					return eMsgHandleStatusError;\

#endif

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define PACKETHANDLER_METHOD_END( )\
				return TRUE;\
				;\
			}


#endif //__PACKETHANDLERDEFINE_H__
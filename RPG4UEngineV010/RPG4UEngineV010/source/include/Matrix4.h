/*////////////////////////////////////////////////////////////////////////
文 件 名：Matrix.h
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MATRIX_H__
#define __MATRIX_H__
#pragma once



#include "MathStructDecl.h"
#include "Vector3D.h"
#include "Vector4D.h"

//namespace math
//{ 

class _MATHLIB_API Matrix4 : public MATRIX4
{
public:
    Matrix4() {};
    explicit Matrix4( bool  bIdentity );
    Matrix4( CONST REAL * );
    explicit Matrix4(Euler3C& );
    Matrix4( CONST MATRIX4& );
    Matrix4( REAL _11, REAL _12, REAL _13, REAL _14,
             REAL _21, REAL _22, REAL _23, REAL _24,
             REAL _31, REAL _32, REAL _33, REAL _34,
             REAL _41, REAL _42, REAL _43, REAL _44 );


	 Matrix4& Set( Euler3C& vEuler);
    Matrix4& Set(REAL _11, REAL _12, REAL _13, REAL _14
					 ,REAL _21, REAL _22, REAL _23, REAL _24
					 ,REAL _31, REAL _32, REAL _33, REAL _34
					 ,REAL _41, REAL _42, REAL _43, REAL _44 );


    // access grants
    REAL& operator () ( UINT Row, UINT Col );
    REAL  operator () ( UINT Row, UINT Col ) const;

    // casting operators
    operator REAL* ();
    operator CONST REAL* () const;
	 REAL* operator[] (UINT iRow) const;

    // assignment operators
    Matrix4& operator *= ( CONST Matrix4& );
    Matrix4& operator += ( CONST Matrix4& );
    Matrix4& operator -= ( CONST Matrix4& );
    Matrix4& operator *= ( REAL );
    Matrix4& operator /= ( REAL );

    // unary operators
    Matrix4 operator + () const;
    Matrix4 operator - () const;

    // binary operators
    Matrix4 operator * ( CONST Matrix4& ) const;
    Matrix4 operator + ( CONST Matrix4& ) const;
    Matrix4 operator - ( CONST Matrix4& ) const;
    Matrix4 operator * ( REAL ) const;
    Matrix4 operator / ( REAL ) const;

    Vector4D operator * ( const Vector4D& ) /*const*/;

    friend Matrix4 operator * ( REAL, CONST Matrix4& );

    BOOL operator == ( CONST Matrix4& ) const;
    BOOL operator != ( CONST Matrix4& ) const;

public:
	/*////////////////////////////////////////////////////////////////////////
	工具类
	/*////////////////////////////////////////////////////////////////////////
   static inline INT ArrayIdx(INT row, INT col) { return (col + row*4); }

	Matrix4& Identity			();
	Matrix4& SetZero			();
	Matrix4&	AffineInverse	();	
	Matrix4& Inverse			();	///< 矩阵倒数
	Matrix4& Inverse2			();	///< 矩阵倒数
	Matrix4& Inverse4x3		();	///< 右列为 [0,0,0,1]型矩阵
	REAL		Det				() const;	///< Det值
	Matrix4&	Transpose		();	///< 矩阵转置

	Matrix4&	Normalize		();	///< 

	Matrix4& Scaling		(const Vector3D& scale,	BOOL bIdentity=FALSE);
	Matrix4& Scaling		(REAL scale,				BOOL bIdentity=FALSE);
	Matrix4& Scaling		(REAL x,	REAL y,REAL z,	BOOL bIdentity=FALSE);
	//Matrix4& SetScaling	(REAL sx, REAL sy, REAL sz);

	Vector3D GetScale		() const;

	Matrix4& Translation		(const Vector3D& tran,	BOOL bIdentity=FALSE);	///< 相对移动量
	Matrix4& Translation		(REAL  t,					BOOL bIdentity=FALSE);
	Matrix4& Translation		(REAL x,	REAL y,REAL z,	BOOL bIdentity=FALSE);
	Matrix4& TranslationTo	(const Vector3D& tran);	///< 绝对移动量

	/////////////////////////////////////////////////////
	///< 绕X轴转fAngle, Identity为True，则设置为单位旋转矩阵，否则将当前矩阵绕X轴转fAngle
	Matrix4& Rotation					(const Vector3D& vAxis, REAL fAngle); ///< 使用Quaternion
	Matrix4& RotationX				(REAL fAngle, BOOL bIdentity=FALSE);
	Matrix4& RotationY				(REAL fAngle, BOOL bIdentity=FALSE);
	Matrix4& RotationZ				(REAL fAngle, BOOL bIdentity=FALSE);
	Matrix4& RotationZYX				(Vector3C&	vAngle);
	Matrix4& RotationViaQuaternion(const Vector3D& vAxis, REAL fAngle);
	Matrix4& RotationEuler			(Euler3C&	vEuler, BOOL bIdentity=FALSE);	///Euler方式旋转，与RotationZYX结果不一样，角度范围：[-pi , pi]
	Vector3D ExtractEuler			() const;	//获得Euler角，与RotationEuler相应

	Matrix4& Mirror					(INT nAxis);

	Matrix4& Multiply		(const Matrix4& m);	///<结果保存到本矩阵
	Matrix4& Multiply		(const Matrix4& m1, const Matrix4& m2);	///<结果保存到本矩阵，m1或m2可为自身矩阵
	Matrix4& Multiply4x3	(const Matrix4& m2);	///< 右列为 [0,0,0,1]型矩阵
	Vector3D Multiply		(const Vector3D& v) const;	///< 
	Vector4D Multiply		(const Vector4D& v) const;	///< 
	//Matrix4& Multiply4x3	(Matrix4 *pwm1, Matrix4 *pwm2);
	//Matrix4& Transpose	(Matrix4 *pwmIn);


    Vector2D TransformCoord(const Vector2D& v) const;
    Vector3D TransformCoord(const Vector3D& v) const;
	 Matrix4& SetPosition	(const Vector3D& vPos);	///< 相对移动量
    Vector3D GetPosition	()const;
    Matrix4& SetComponentX		(const Vector3D& vPos);
    Matrix4& SetComponentY		(const Vector3D& vPos);
    Matrix4& SetComponentZ		(const Vector3D& vPos);
    Matrix4& SetComponentPos	(const Vector3D& vPos);
    Vector3D ComponentX()	const;
    Vector3D ComponentY()	const;
    Vector3D ComponentZ()	const;
    Vector3D ComponentPos()const;

	/*////////////////////////////////////////////////////////////////////////
	以下函数 
			Eye点为眼睛点
			at为目标点
			up为向上矢量
	/*////////////////////////////////////////////////////////////////////////
    Matrix4& LookAtLH	(const Vector3D& eye,const Vector3D& at, const Vector3D& up);
    Matrix4& LookAtRH	(const Vector3D& eye,const Vector3D& at, const Vector3D& up);

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
    /// create left-handed field-of-view perspective projection matrix
    void PerspFovLH(REAL fovY, REAL aspect, REAL zn, REAL zf);
    /// create right-handed field-of-view perspective projection matrix
    void PerspFovRH(REAL fovY, REAL aspect, REAL zn, REAL zf);

	/*////////////////////////////////////////////////////////////////////////
		创建正交矩阵
		OrthoLH函数是OrthoOffCenterLH 函数的特例。
		用OrthoOffCenterLH函数同样可以创建这种矩阵，
		只要让参数：l = -w/2, r = w/2, b = -h/2, 和 t = h/2就行了。
	/*////////////////////////////////////////////////////////////////////////
    void OrthoLH(REAL w, REAL h, REAL zn, REAL zf);
    /// create right-handed orthogonal projection matrix
    void OrthoRH(REAL w, REAL h, REAL zn, REAL zf);

    void OrthoOffCenterLH(REAL l, REAL r,REAL t, REAL b, REAL zn, REAL zf);
    void OrthoOffCenterRH(REAL l, REAL r,REAL t, REAL b, REAL zn, REAL zf);


    /// restricted lookat
    void Billboard(const Vector3D& to, const Vector3D& up);



public:
	inline friend Matrix4 operator * ( REAL f, CONST Matrix4& mat )
	{
		 return Matrix4(f * mat._11, f * mat._12, f * mat._13, f * mat._14,
							f * mat._21, f * mat._22, f * mat._23, f * mat._24,
							f * mat._31, f * mat._32, f * mat._33, f * mat._34,
							f * mat._41, f * mat._42, f * mat._43, f * mat._44);
	}

public:
	static	const Matrix4	IDENTITY;
	static	const Matrix4	ZERO;
};

typedef Matrix4			Matrix4X;
typedef const Matrix4	Matrix4C;

//};//namespace math

#include "Matrix4.inl"




#endif //__MATRIX_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemAttrInfoParserDefine.h
创建日期：2008年6月15日
最后更新：2008年6月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMATTRINFOPARSERDEFINE_H__
#define __ITEMATTRINFOPARSERDEFINE_H__
#pragma once

#include <THashTable.h>
//#include "ConstItemAttrInfo.h"
#include "ConstItem.h"

using namespace std;

enum{RAND_SECTION_0=0,RAND_SECTION_MAX=6};



/*////////////////////////////////////////////////////////////////////////
	以下为ItemOption信息
/*////////////////////////////////////////////////////////////////////////
struct sITEM_OPTION_INFO
{
	INT		m_OptionIndex;
	VRSTR		m_OptionName;
};


/*////////////////////////////////////////////////////////////////////////
	以下为随机属性随机表

	code	Type	序号	注释	等级	值类型 随机属性	值	范围	机率	组	[0]	(0,20)	[20,40)	[40,60)	[60,80)	[80,100)
/*////////////////////////////////////////////////////////////////////////
struct sITEM_RANDATTR_UNIT
{

	INT			m_nAttrCode;		///< 编号
	//INT			m_nRandKind;		///< 组类
	BYTE			m_nRandIndex;		///< 组内序号
											///< comment
	LEVELTYPE	m_Level;				///< 要求等级
	BYTE			m_byValueType;		///< 属性值类型
	INT			m_nOptionIndex;	///< 属性索引
	INT			m_nOptionValue;	///< 属性值
	INT			m_nIncValueMax;	///< 属性值最大增量

	INT			m_nRateInfoID;		///< 机率段编号
	//BYTE			m_arRandSectors[RAND_SECTION_MAX];	///< 机率段
};

typedef vector<sITEM_RANDATTR_UNIT*>				ItemRandAttrUnitVector;
typedef util::THashTable<sITEM_RANDATTR_UNIT *>	ItemRandAttrUnitMap;

struct sITEM_RANDATTR_GROUP
{
	VRSTR							m_sGroupName;
	//INT							m_nGroupIndex;
	BYTE							m_byRate;
	ItemRandAttrUnitVector	m_arUnits;
};

//typedef map<sITEM_RANDATTR_GROUP*>	ItemRandAttrGroupMap;
typedef vector<sITEM_RANDATTR_GROUP*>						ItemRandAttrGroupVector;
typedef util::THashTable<sITEM_RANDATTR_GROUP*>	ItemRandAttrGroupMap;


struct sITEM_RANDATTR_TYPE
{
	INT							m_nType;
	ItemRandAttrGroupVector	m_arGroups;
	vector<int>					m_arRates;
	ItemRandAttrGroupMap		m_mpGroups;
};


typedef util::THashTable<sITEM_RANDATTR_TYPE *>	ItemRandAttrTypeTable;



struct sITEM_ATTR_INFO
{
	eSOCKET_TYPE	m_SocketType;
	INT				m_nAttrCode;		///< 编号
												///< comment
	SLOTCODE			m_SocketItemCode;	///< 物品代码
	LEVELTYPE		m_Level;				///< 要求等级
	BYTE				m_byInLevel;		///< 等级最大增量
};

/*////////////////////////////////////////////////////////////////////////
	以下为随机属性资源表

	Code	注释	Level	浮动范围	type	主属性	主属性值	浮动范围	随机1机率	随机2机率	随机属性类1	随机属性类2	[0]	(0,20)	[20,40)	[40,60)	[60,80)	[80,100)
/*////////////////////////////////////////////////////////////////////////

struct sITEM_RANDATTR_INFO : public sITEM_ATTR_INFO
{
	BYTE			m_byMainValueType;///< 主属性值类型
	INT			m_nMainOption;		///< 主属性索引
	INT			m_nMainValue;		///< 主属性值
	INT			m_nMainIncValue;	///< 主属性值最大增量

	BYTE			m_nRandRate;		///< 随机属性1机率
	BYTE			m_nRandRate2;		///< 随机属性2机率
	INT			m_nRandIndex;		///< 随机属性1  参考sITEM_RANDATTR_TYPE
	INT			m_nRandIndex2;		///< 随机属性2  参考sITEM_RANDATTR_TYPE

	INT			m_nRateInfoID;		///< 机率段编号
	//BYTE			m_arRandSectors[RAND_SECTION_MAX];	///< 机率段
};

typedef util::THashTable<sITEM_RANDATTR_INFO *>	ItemRandAttrInfoTable;

/*////////////////////////////////////////////////////////////////////////
	以下为攻防孖属性资源表
	Code	注释	Level	浮动范围	type	攻属性	攻属性值	浮动范围	攻属性2值	浮动范围	type	防属性	防属性值	浮动范围	[0]	(0,20)	[20,40)	[40,60)	[60,80)	[80,100)

/*////////////////////////////////////////////////////////////////////////

struct sITEM_DOUBLE_ATTR_INFO : public sITEM_ATTR_INFO
{
	BYTE			m_byAttValueType;	///< 攻属性值类型
	INT			m_nAttOption;		///< 攻属性索引
	INT			m_nAttValue;		///< 攻属性值
	INT			m_nAttIncValue;	///< 攻属性值最大增量

	INT			m_nAttOption2;		///< 攻属性2索引
	INT			m_nAttValue2;		///< 攻属性2值
	INT			m_nAttIncValue2;	///< 攻属性2值最大增量

	BYTE			m_byDefValueType;	///< 攻属性值类型
	INT			m_nDefOption;		///< 攻属性索引
	INT			m_nDefValue;		///< 攻属性值
	INT			m_nDefIncValue;	///< 攻属性值最大增量

	INT			m_nRateInfoID;		///< 机率段编号
	//BYTE			m_arRandSectors[RAND_SECTION_MAX];	///< 机率段
};

typedef util::THashTable<sITEM_DOUBLE_ATTR_INFO *>	ItemDoubleAttrInfoTable;


#endif //__ITEMATTRINFOPARSERDEFINE_H__
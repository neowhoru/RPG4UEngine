/*////////////////////////////////////////////////////////////////////////
文 件 名：TileOctreeDefine.h
创建日期：2008年4月18日
最后更新：2008年4月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TILEOCTREEDEFINE_H__
#define __TILEOCTREEDEFINE_H__
#pragma once


namespace tile
{ 


#define	OCT_DEPTHSHIRT		(28 + 31)
#define	OCT_DEPTHMASK		0xF
#define	OCT_INDEXMASK		0x0FFFFFFFFFFFFFFF
#define	OCT_CHILDMASK		0x7
#define	OCT_CHILDSHIFT		3
#define MAX_DEPTH				15


	#define GetDepth_WzOctIndex( woi)			( ( (OCT_INDEX)(woi) >> OCT_DEPTHSHIRT) & OCT_DEPTHMASK)
	#define GetIndex_WzOctIndex( woi)			( (OCT_INDEX)(woi) & OCT_INDEXMASK)
	#define Make_WzOctIndex( iDepth, iIndex)	( (( (OCT_INDEX)(iDepth) & OCT_DEPTHMASK) << OCT_DEPTHSHIRT)   |   ( (OCT_INDEX)(iIndex) & OCT_INDEXMASK)   )


	#define Child_WzOctIndex( woiParent, iChildIndex)	\
												Make_WzOctIndex(	GetDepth_WzOctIndex( woiParent) + 1, \
																		( GetIndex_WzOctIndex( woiParent) << OCT_CHILDSHIFT) | ( iChildIndex))

	#define Parent_WzOctIndex( woi)		\
												Make_WzOctIndex( GetDepth_WzOctIndex( woi) - 1, GetIndex_WzOctIndex( woi) >> OCT_CHILDSHIFT)

	#define GetChild_ToReach( woiCurrent, woiDest)	\
												( OCT_CHILDMASK & \
													( GetIndex_WzOctIndex( woiDest) >> \
														( 3 * ( GetDepth_WzOctIndex( woiDest) - GetDepth_WzOctIndex( woiCurrent) - 1))))




	enum E_OCTNODE_DATTYPE
	{
		DTO_TO_GET_OCTNODES = -1,	///< 
		DTO_TERRAIN_MESH = 0,		///< 
		DTO_PATH_MESH,					///< 
		DTO_OBJECT,						///< 
		DTO_UNIT,						///< 
		DTO_LIGHT,						///< 
		DTO_EFFECT,						///< 
		DTO_SOUND,						///< 
		DTO_SIMPLEOCCLUSION,			///< 

	#ifdef  NEW_OCTNODE
		DTO_USER = 1000,				///< 
	#else
		NUM_DTO,							///< 
	#endif
	};

};//namespace tile


#endif //__TILEOCTREEDEFINE_H__
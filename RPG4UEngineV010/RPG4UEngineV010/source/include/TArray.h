/*////////////////////////////////////////////////////////////////////////
文 件 名：TArray.h
创建日期：2008年1月25日
最后更新：2008年1月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TARRAY_H__
#define __TARRAY_H__
#pragma once

#include "CommonDefine.h"

//------------------------------------------------------------------------------
template<class TYPE> 
class TArray
{
public:
    typedef TYPE* iterator;
    enum    
	 {  
		 DoubleGrowSize = (1<<0),    // when set, grow size doubles each turn
    };

public:
    TArray	();
    TArray	(int initialSize, int initialGrow);
    TArray	(int initialSize, int initialGrow, TYPE initialValue);
    TArray	(const TArray<TYPE>& rhs);

	 ~TArray();

public:
    TArray<TYPE>& operator	=	(const TArray<TYPE>& rhs);
	 TYPE&			operator	[]	(int index) const;

public:
    void SetFlags(int f);
    int	GetFlags() const;

    /// clear contents and set a fixed size
    void SetFixedSize(int size);

    /// push element to back of array
    TYPE&	PushBack	(const TYPE& elm);
    void		Append	(const TYPE& elm);

    iterator Reserve(int num);

    int Size		() const;
    int AllocSize	() const;	///获取已分配空间大小

    /// set element at index, grow array if necessary
    TYPE& Set(int index, const TYPE& elm);
    /// return reference to nth element in array
    TYPE& At(int index);
    /// return reference to first element
    TYPE& Front() const;
    /// return reference to last element
    TYPE& Back() const;
    /// return true if array empty
    bool Empty() const;


    /// erase element at index
    void Erase(int index);
    /// quick erase, does not call operator= or destructor
    void EraseQuick(int index);
    /// erase element pointed to by iterator
    iterator Erase(iterator iter);
    /// quick erase, does not call operator= or destructor
    iterator EraseQuick(iterator iter);


    /// insert element at index
    void Insert(int index, const TYPE& elm);
    /// clear array (calls destructors)
    void Clear();
    /// reset array (does NOT call destructors)
    void Reset();


    /// return iterator to beginning of array
    iterator Begin() const;
    /// return iterator to end of array
    iterator End() const;

    /// find identical element in array, return iterator
    iterator Find(const TYPE& elm) const;
    /// find identical element in array, return index
    int FindIndex(const TYPE& elm) const;


    /// find array range with element
    void Fill(int first, int num, const TYPE& elm);
    /// clear contents and preallocate with new attributes
    void Reallocate(int initialSize, int grow);

private:
    /// check if index is in valid range, and grow array if necessary
    void CheckIndex(int);

    /// destroy an element (call destructor without freeing memory)
    void Destroy(TYPE* elm);
    /// copy content
    void Copy(const TArray<TYPE>& src);
    /// delete content
    void Delete();

    /// grow array
    void Grow();
    /// grow array to target size
    void GrowTo(int newAllocSize);


    /// move Elements, grows array if needed
    void Move(int fromIndex, int toIndex);
    /// unsafe quick move, does not call operator= or destructor
    void MoveQuick(int fromIndex, int toIndex);

private:
    int		m_GrowSize;          // grow by this number of m_pElements if array exhausted
    int		m_AllocSize;         // number of m_pElements allocated
    int		m_ElementNum;        // number of m_pElements in array
    int		m_Flags;
    TYPE*	m_pElements;         // pointer to element array
};

#include "TArray.inl"


#endif //__TARRAY_H__
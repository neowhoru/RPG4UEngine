/*////////////////////////////////////////////////////////////////////////
文 件 名：Protocol_ClientLogin.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PROTOCOL_CLIENTLOGIN_H__
#define __PROTOCOL_CLIENTLOGIN_H__
#pragma once

/*////////////////////////////////////////////////////////////////////////
// *  前缀
//		C : Client
//		G : Game Server
//		M : Master Server
//		D : DBP Server
//	   L : LoginFront
//		W : World Server
//
// * 后缀
//		SYN - Server同步协议，ACK协议接受处理，NAK协议拒绝处理
//		ACK - SYN协议接受处理
//		NAK - SYN协议拒绝处理
//		CMD - Server指令，无须返回响应
//		BRD - Server向外广播消息
//		DBR - DBProxy数据库代理向外广播消息
//
// * 协议命名规则：
//		前缀_分类_协议_后缀
//		譬如) CG_CONNECTION_REGISTER_SYN
//
/*////////////////////////////////////////////////////////////////////////
#ifndef __PROTOCOL_CL_H__
#define __PROTOCOL_CL_H__

#include "Protocol_Define.h"
#include "Protocol.h"


#define _CL_CAT(NAME)			_PROTOCOL_CATEGORY(CL,NAME)
#define _CLLOGIN_SYN_A(NAME)	_PROTOCOL_SYN_A	(CL_LOGIN,NAME)
#define _CLLOGIN_CMD(NAME)		_PROTOCOL_CMD		(CL_LOGIN,NAME)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
// Client <-> LoginFront Protocol
enum eCL_CATEGORY
{
	 _CL_CAT(LOGIN) = PROTOCOL_RANGE(CLIENTLOGIN)
	,_CL_CAT(MAX)
};

PROTOCOL_RANGE_END(ClientLogin, CL_MAX, CLIENTLOGIN);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eCL_AUTH
{
	 _CLLOGIN_CMD		(USER_READY			)	//	<-- 在连接后LoginShell最先送回客户的协议
	,_CLLOGIN_SYN_A	(VERSION_CHECK		)	//	--> 客户连接请求 (版本检查) 
	,_CLLOGIN_SYN_A	(ACCOUNT_LOGIN		)
	,_CLLOGIN_SYN_A	(GATESTATUS_LIST	)
	,_CLLOGIN_SYN_A	(GATE_SELECT		)
	,_CLLOGIN_SYN_A	(GATENAME_LIST		)


};



#endif // __PROTOCOL_CL_H__




#endif //__PROTOCOL_CLIENTLOGIN_H__
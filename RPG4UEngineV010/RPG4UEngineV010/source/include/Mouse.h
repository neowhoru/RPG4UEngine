/*////////////////////////////////////////////////////////////////////////
文 件 名：Mouse.h
创建日期：2008年3月19日
最后更新：2008年3月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	鼠标输入系统

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MOUSE_H__
#define __MOUSE_H__
#pragma once


#include "InputDefine.h"

namespace input
{ 


class Mouse 
{
public:
	Mouse();
	~Mouse();

public:
	BOOL			Init( HWND hWnd );
	VOID			Release();

public:

	VOID			MouseMessageProc( HWND hWnd, DWORD wmsg, WPARAM wParam, LPARAM lParam );

	VOID			GetNextMouseEvent(  HWND hWnd, sMOUSEINFO * pMouseInfo );
	BOOL			IsEmptyMouseEvent() { return m_nTail == m_nHead ? TRUE : FALSE; }

	VOID			SetPoint(POINT & pt);
	VOID			GetPoint();

	POINT           ConvertPoint(POINT pt);

	VOID			CheckMousePos();


	void			SetScreenRes( float fWidth, float fHeight ) { m_fScreenWidthRes = fWidth; m_fScreenHeightRes = fHeight; }

	inline int		GetMouseX()				{ return m_nMouseX;					}
	inline int		GetMouseY()				{ return m_nMouseY;					}
	inline BOOL		LButtonDown()			{ return m_bMouseEvent[eME_LDOWN];	}
	inline BOOL		RButtonDown()			{ return m_bMouseEvent[eME_RDOWN];	}
	inline BOOL		MButtonDown()			{ return m_bMouseEvent[eME_MDOWN];	}
	inline BOOL		LButtonUp()				{ return m_bMouseEvent[eME_LUP];	}
	inline BOOL		RButtonUp()				{ return m_bMouseEvent[eME_RUP];	}
	inline BOOL		MButtonUp()				{ return m_bMouseEvent[eME_MUP];	}
	inline BOOL		LButtonDoubleClick()	{ return m_bMouseEvent[eME_LDBL];	}
	inline BOOL		RButtonDoubleClick()	{ return m_bMouseEvent[eME_RDBL];	}
	inline BOOL		MButtonDoubleClick()	{ return m_bMouseEvent[eME_MDBL];	}
	inline int		GetMouseEventX()		{ return m_nMouseEventX;			}
	inline int		GetMouseEventY()		{ return m_nMouseEventY;			}
	inline int		GetMouseAxisX()			{ return m_nMouseAxisX;				}
	inline int		GetMouseAxisY()			{ return m_nMouseAxisY;				}
	inline BOOL		LButtonPressed()		{ return m_bLBtnPress;				}
	inline BOOL		RButtonPressed()		{ return m_bRBtnPress;				}
	inline BOOL		MButtonPressed()		{ return m_bMBtnPress;				}
	inline BOOL		LButtonDrag()			{ return m_bLBtnDrag;				}
	inline BOOL		RButtonDrag()			{ return m_bRBtnDrag;				}
	inline BOOL		MButtonDrag()			{ return m_bMBtnDrag;				}
	inline int		GetWheel()				{ return m_nWheel;					}

	inline BOOL		CtrlPressed()			{ return m_nCombineKey & MK_CONTROL;}
	inline BOOL		ShiftPressed()			{ return m_nCombineKey & MK_SHIFT;	}

protected:

	VOID			RecordMouseEvent( HWND hWnd, int nEvent, WPARAM wParam, LPARAM lParam );
	VOID			ParseCurMouseEvent( sMOUSEINFO* pMouseInfo );
	BOOL			PushMouseInfo( sMOUSEINFO* pIn );
	BOOL			PopMouseInfo( sMOUSEINFO* pOut );

private:

	float							m_fScreenWidthRes;
	float							m_fScreenHeightRes;

	sMOUSEINFO*						m_pMouseInfo;
	sMOUSEINFO						m_CurMouseInfo;
	int								m_nHead;
	int								m_nTail;
	int								m_nQueSize;



	BOOL							m_bMouseEvent[eME_COUNT];

	BOOL							m_bLBtnPress;
	BOOL							m_bRBtnPress;
	BOOL							m_bMBtnPress;
	BOOL							m_bLBtnDrag;
	BOOL							m_bRBtnDrag;
	BOOL							m_bMBtnDrag;

	int								m_nMouseX;
	int								m_nMouseY;
	int								m_nMouseOldX;
	int								m_nMouseOldY;
	int								m_nMouseEventX;
	int								m_nMouseEventY;

	int								m_nMouseAxisX;
	int								m_nMouseAxisY;

	int								m_nWheelMsg;
	int								m_nWheel;

	int								m_nCombineKey;
	BOOL							m_bInited;
	POINT							m_Point;
};
};//namespace input

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(input::Mouse,Mouse , _INPUTLIB_API);
//extern _INPUTLIB_API Mouse& singleton::GetMouseSystem();
#define theMouse	singleton::GetMouse()


#endif //__MOUSESYSTEM_H__


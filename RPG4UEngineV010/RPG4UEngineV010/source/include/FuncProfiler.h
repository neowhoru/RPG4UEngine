/*////////////////////////////////////////////////////////////////////////
文 件 名：FuncProfiler.h
创建日期：2006年11月29日
最后更新：2006年11月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
		统计运行中，某个区段代码的执行时间


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __FUNCPROFILER_H__
#define __FUNCPROFILER_H__
#pragma once


#include "StringHelper.h"
#include "FuncProfilerDefine.h"
#include "StringHandle.h"

using namespace std;

/////////////////////////////////////
struct sPROFILE_INFO
{
	DWORD				dwSequence;	//流水号
	VRSTR				sKeyName;
	BOOL				bBeginPeriod;
	DWORD				dwCallTimes;

	float				fAbsTime;
	float				fAllPeriod;
	sPROFILE_INFO* pParent;

	sPROFILE_INFO():	bBeginPeriod(FALSE),
				dwCallTimes(0),
				sKeyName(NULL),
				fAbsTime(0.0f),
				fAllPeriod(0.0f),
				dwSequence(0),
				pParent(NULL){}
};

typedef map<DWORD,sPROFILE_INFO* >	ProfileKeyMap;
typedef pair<DWORD,sPROFILE_INFO* >	ProfileKeyMapPair;
typedef ProfileKeyMap::iterator		ProfileKeyMapIt;

typedef map<VRSTR,sPROFILE_INFO >	ProfileInfoMap;
typedef pair<VRSTR,sPROFILE_INFO >	ProfileInfoMapPair;
typedef ProfileInfoMap::iterator		ProfileInfoMapIt;

typedef stack<sPROFILE_INFO*>			ProfileInfoStack;


//////////////////////////////////////////
class _BASE_API FuncProfiler
{
public:
	enum eTIMER_COMMAND 
	{ 
		TIMER_RESET, 
		TIMER_START, 
		TIMER_STOP, 
		TIMER_ADVANCE,
		TIMER_GETABSOLUTETIME, 
		TIMER_GETAPPTIME, 
		TIMER_GETELAPSEDTIME 
	};





public:
	FuncProfiler(void);
	~FuncProfiler(void);

public:
	void WriteLog		();
	void BeginPeriod	(LPCTSTR szKeyName);
	void EndPeriod		( LPCTSTR szKeyName );

	float __stdcall HQ_Timer( eTIMER_COMMAND command );

public:
	static void LogProfilerMsg	( LPCTSTR szText, ... );
	static void LogProfilerMsg2( LPCTSTR szText, ... );

protected:
	VG_TYPE_PROPERTY		(File, StringHandle);
	DWORD						m_dwStartLogTick;
private:
	ProfileInfoMap			m_mpProfiles;
	ProfileKeyMap			m_mpSorts;
	ProfileInfoStack		m_StackInfo;
};



//////////////////////////////////////////////////
class _BASE_API FuncTimeProfiler
{
public:
	FuncTimeProfiler(LPCTSTR	szKeyName);
	virtual ~FuncTimeProfiler();

public:
	VRSTR		m_sKeyName;
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(FuncProfiler , _BASE_API);
#define gTrueTime		GetFuncProfiler()


//////////////////////////////////////////////////
#define USETRUETIME
#ifdef USETRUETIME
#define BEGINFUNCPERLOG(x) gTrueTime.BeginPeriod(x);
#define ENDFUNCPERLOG(x)	gTrueTime.EndPeriod(x);

#define TRUETIMEBEGIN		BEGINFUNCPERLOG
#define TRUETIMEEND			ENDFUNCPERLOG
#define TRUETIMEFUNC()		FuncTimeProfiler _funcInst(__FUNCTION__);
#define TRUETIMEBLOCK(a)	FuncTimeProfiler _blockInst(a);

#else
#define BEGINFUNCPERLOG(x)
#define ENDFUNCPERLOG(x)

#define TRUETIMEFUNC()
#define TRUETIMEBLOCK(a)
#endif

#define	OUTPUT_TRY()			FuncProfiler::LogProfilerMsg("%s",__FUNC_NAME__)
#define	OUTPUT_ERR(MSG)		FuncProfiler::LogProfilerMsg("%s:%s",__FUNC_NAME__, MSG)

#define	OUTPUT_TRY2()			FuncProfiler::LogProfilerMsg2("%s",__FUNC_NAME__)
#define	OUTPUT_ERR2(MSG)		FuncProfiler::LogProfilerMsg2("%s:%s",__FUNC_NAME__, MSG)

////////////////////////////////////////////////////////////////
#define _USE_FUNCPROFILER

///////////////////////////////////////////////////
#ifndef _USE_FUNCPROFILER
#define BEGIN_FUNC_PROFILER			{
#define ON_FUNC_PROFILER(func)		{
#define END_FUNC_PROFILER				}
#define END_FUNC_PROFILER2(msg)		}
#else
#define BEGIN_FUNC_PROFILER()			{static const char __FUNC_NAME__[]=__FUNCTION__; try{
#define ON_FUNC_PROFILER(func)		{static const char __FUNC_NAME__[]=#func; try{
#define END_FUNC_PROFILER()			}catch(char* Err){OUTPUT_ERR(Err);}		catch(...){OUTPUT_TRY(); }}
#define END_FUNC_PROFILER2()			}catch(char* Err){OUTPUT_ERR(Err);}		catch(...){OUTPUT_TRY(); }}
#define END_FUNC_FINALLY()				}catch(char* Err){OUTPUT_ERR2(Err);}	catch(...){OUTPUT_TRY2(); }}
#define END_FUNC_FINALLY2()			}catch(char* Err){OUTPUT_ERR2(Err);}	catch(...){OUTPUT_TRY2(); }}
#endif



#endif //__FUNCPROFILER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TileMapDefault.h
创建日期：2006年6月29日
最后更新：2006年6月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TILEMAPDEFAULT_H__
#define __TILEMAPDEFAULT_H__
#pragma once

#include "TileMap.h"

namespace tile
{ 
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _TILEWORLDLIB_API TileMapDefault : public TileMap
{
public:
	//Factory类型
	virtual TileMapType GetFactoryType() {return TAG('TDEF');};

public:
	virtual BOOL Init	() {return 0;}
	virtual void Release() {}

	virtual BOOL Create	( TileMapCombination* pCombination=NULL){__UNUSED(pCombination); return 0;}
	virtual void Destroy	( ){}

	virtual DWORD GetCheckSum	( void) {return 0;}
//#	ifndef _SERVER
//	virtual DWORD GenerateCheckSum( void){return 0;}
//#	endif

public:
	virtual BOOL Serialize	( Archive * /*pArchive*/){return 0;}
	virtual BOOL SerializeInfo	( Archive * /*pArchive*/){return 0;}
	virtual BOOL Load			( LPCSTR /*szFileNoExt*/, BOOL bWithExt=FALSE){__UNUSED(bWithExt);return 0;}
#	ifndef _SERVER
	virtual BOOL Save			( LPCSTR /*szFileNoExt*/,BOOL /*bCheckInfo*/){return 0;}
	virtual void				BindLand( V3DGameWorld * /*pLand*/,V3DGameMap* /*pMap*/ )			 {}	
	virtual V3DGameWorld*	GetBindLand	() {return NULL;}	
	virtual V3DGameMap*		GetBindMap	() {return NULL;}
#	endif

protected:
	//virtual BOOL JumpToChunk( Archive *pArchive, unsigned short nChunkID){return 0;}

public:
	virtual BOOL	Flip			( eTERRAIN_FLIP_OPR /*oprType*/){return 0;}
	virtual BOOL	Copy			( TileMap *, DWORD ){return 0;}
	virtual void	SetWorldMapXY(int /*x*/, int /*y*/){}
	virtual void	GetWorldMapXY(int& /*x*/, int& /*y*/){}

	virtual const BBox3D&	GetTerrainSize(){static BBox3D box;return box;}
#	ifndef _SERVER
	virtual BOOL	GenerateFrom(V3DGameMap* /*p3DMap*/,LPCSTR /*szFileNotExt*/,TileMapGeneratorListener* /*pListener*/) {return 0;}
	virtual BOOL	GenerateTileMasks(V3DGameMap* /*p3DMap*/){return 0;};
	virtual BOOL	Analyze3DMap(V3DGameMap* /*p3DMap*/, BBox3D* /*pBvSize*/,TileMapGeneratorListener* /*pListener*/ ) {return 0;}
	//virtual BOOL	Analyze3DMap( UnitBase *pWzd) {return 0;}
#	endif

	virtual BOOL GetMappingInfo	(SRemappingInfo& OUT /*info*/){return 0;}
	virtual BOOL RemappingTo		(const SRemappingInfo&,BOOL /*bResetInfo=TRUE*/){return 0;}	///< 
	virtual BOOL GetRemappingInfo	(SRemappingInfo& ){return 0;}
	virtual BOOL UpdateMappingInfo(const SRemappingInfo& /*info*/){return 0;}
	virtual BOOL Unremapping		(BOOL /*bClearInfo=TRUE*/){return 0;}	///< 
	virtual BOOL RemappingSimple	(const SRemappingInfo&){return 0;}	///< 
	//virtual BOOL OversewEdgeWith(TileMap* pTileMap){return 0;}	///< 
public:
	virtual BOOL SetActiveRoom( int /*iRoomIndex*/){return 0;}
	virtual int DuplicateRoomInfo( int /*iSourceIndex*/ = 0){return 0;}
	virtual BOOL DestroyRoomInfo( int /*iRoomIndex*/){return 0;}	

	virtual TilePathFinder* GetPathFinder() {return 0;}
	virtual int GetPathTileCount	(void) {return 0;}

	virtual int GetNearNodes	(int /*iTile*/
										,int * /*piTile*/
										,int  /*iMaxCount*/) {return 0;}
	virtual PathNodeTile* GetPathTile	(int /*iTile*/) {return 0;}
#	ifndef _SERVER
	virtual BOOL	RenderPathTile		(int /*iTile*/, DWORD /*dwTick*/, DWORD /*dwColor*/) {return FALSE;}
#	endif

	virtual OCT_INDEX			GetTileOctIndex( int /*iTile*/) {return 0;}	///< Tile 
	//virtual int				GetTileForTerrainMesh( int iTerrainMesh) {return 0;}	///< 

	virtual void PathHandleReset(PathHandler * /*ppe*/
										,const Vector3D& /*wvPos*/ 
										,TILEINDEX /*iTile*/
										,BOOL /*bResetBackOff*/) {}	///< 3D 

	virtual ePATH_RESULT PathHandleFindPath(PathHandler *   /*ppe*/
                                             ,const Vector3D& /*wvDest*/
															,int             /*iDestTile*/
															,WORD            /*wAttribute*/
															,float           /*fSeekRange*/)
										{return (ePATH_RESULT)0;}	///< 

	virtual BOOL PathHandleSetPath(PathHandler * /*ppe*/,
										Vector3D * /*pwvDest*/,
										int * /*pTileRoute*/,
										int /*iNumTileRoute*/) {return 0;}	///< 

	virtual BOOL PathHandleFrameMove( PathHandler * /*ppe*/,
										float /*fMove*/,
										sPATHFIND_EVENT *pEvent = NULL) {__UNUSED(pEvent);return 0;}	///< 
	virtual int PathHandleGetMeshTileStand( Vector3D /*wvPos*/,
										float * /*pfT*/,
										float fMeshTileDist = -1.0f,
										DWORD dwTerrainGroupOptionCheck = 0) {__UNUSED(fMeshTileDist);__UNUSED(dwTerrainGroupOptionCheck);return 0;}	///< 3D 

	//virtual void PathExp_ApplyMoveByAnimation( PathHandler *ppe, 
	//									DDrawRenderObject *pUnitDraw) {return 0;}	///< 

	virtual ePATH_RESULT PathHandleThrustPath( PathHandler * /*ppe*/, 
										Vector3D * /*pwvMoveDistance*/,
										WORD wAttribute = PTA_CANNT_MOVE) {__UNUSED(wAttribute);return (ePATH_RESULT)0;}	///< 


public:
#	ifndef _SERVER
	virtual void		RenderPathTiles	(DWORD /*dwTick*/,DWORD /*maskType*/, COLOR /*color*/) {}	///< 
	virtual void		RenderPathRoute	(DWORD        /*dwTick*/
                                       ,PathHandler* /*pPath*/
													,COLOR        /*color*/
													,BOOL			  /*bShowFullTile*/)	{};
#	endif

	virtual int			PickPathTile(Ray *	/*pwr*/ 
											,float *	/*pfT*/		= NULL
											,BOOL		bCulling = TRUE) {__UNUSED(bCulling);return 0;}	///< 

	virtual Vector3D	GetRandomPosInTile	( int /*iTile*/) {return 0;}	///< 
	virtual BOOL		CorrectHeightByTile	( int /*iTile*/, Vector3D * /*pwvPos*/) {return 0;}

#	ifndef _SERVER
#	ifdef USE_CONVEX
	virtual void		RenderConvex(DDrawRender * /*pDraw*/
											,int *		/*piConvexes*/ 
											,int			/*iNumConvex*/
											,COLOR		/*wcColor*/) {}
#	endif
#	endif


public:
	virtual OCT_INDEX	GetOctIndexToAddObject	( BoundingVolume *	/*pwbv*/
															, OCT_INDEX			woiFrom = 0){__UNUSED(woiFrom);return 0;}
	virtual OCT_INDEX	GetOctIndexToSearch		( OCT_INDEX /*woi*/)						{return 0;}


	virtual OCT_INDEX	AddObject					( E_OCTNODE_DATTYPE	/*dto*/
																, DWORD				/*dwObject*/
																, OCT_INDEX			/*woiIndex*/)	{return 0;}
	virtual OCT_INDEX	AddObject					( E_OCTNODE_DATTYPE	/*dto*/
																, DWORD					/*dwObject*/
																, BoundingVolume *	/*pwbv*/)			{return 0;}


	virtual BOOL			RemoveObject				( E_OCTNODE_DATTYPE	/*dto*/
																, DWORD					/*dwObject*/
																, OCT_INDEX			/*woiIndex*/)	{return 0;}
	virtual BOOL			RemoveObject				( E_OCTNODE_DATTYPE	/*dto*/
																, DWORD					/*dwObject*/
																, BoundingVolume *	/*pwbv*/)			{return 0;}


	virtual BOOL			IsContainable				( OCT_INDEX			/*woiIndex*/
																, BoundingVolume *	/*pwbv*/)			{return 0;}

public:
	virtual TILEINDEX GetTileToStand	(const VECTOR3D&	/*wvPos*/
										,float *				pfT = NULL
										,float				fJumpLimit		= -1.0f
										,float				fAdjustLimit	= -1.0f
										,WORD *				pwAttribute		= NULL) {__UNUSED(pfT);__UNUSED(fJumpLimit);__UNUSED(fAdjustLimit);__UNUSED(pwAttribute);return 0;}	

	virtual void SetAttribute	( int /*iTile*/, WORD /*wAttribute*/) {}
	virtual void UnsetAttribute( int /*iTile*/, WORD /*wAttribute*/) {}
	virtual BOOL CheckAttribute( int /*iTile*/, WORD /*wAttribute*/) {return 0;}
	virtual WORD GetAttribute	( int /*iTile*/) {return 0;}


public:
	virtual void	ForEachSpecialArea			(SpecialAreaIterator& /*opr*/){};
	virtual BOOL	AddSpecialArea					( SpecialArea * /*pSpecialArea*/) {return 0;}
	virtual BOOL	RemoveSpecialArea				( IDTYPE /*wiIdentity*/,BOOL /*bLocal*/) {return 0;}
	virtual BOOL	RemoveSpecialAreaByIndex	( int /*iIndex*/,BOOL /*bLocal*/) {return 0;}
	virtual void	RemoveAllSpecialArea			( BOOL /*bLocal*/) {}
	virtual int		GetNumberOfSpecialArea		( BOOL /*bLocal*/) {return 0;}

	virtual SpecialArea* GetSpecialArea			( IDTYPE /*wiIdentity*/, BOOL /*bLocal*/) {return 0;}
	virtual SpecialArea* GetSpecialAreaByIndex	( int /*iIndex*/, BOOL /*bLocal*/) {return 0;}

#	ifndef _SERVER
	virtual void		RenderSpecialAreas(DWORD			/*dwTick*/,COLOR /*color*/){};	///< 
	virtual void		RenderSpecialArea	(DWORD			/*dwTick*/
													,int				/*iIndex*/
													,COLOR			/*color*/){};
#	endif

};//class _TILEWORLDLIB_API TileMap


};//namespace tile

#endif //__TILEMAPDEFAULT_H__


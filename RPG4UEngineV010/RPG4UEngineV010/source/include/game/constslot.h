/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstSlot.h
创建日期：2008年2月16日
最后更新：2008年2月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTSLOT_H__
#define __CONSTSLOT_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD				   BASESLOT_KEY;	//ID类型的Factory
typedef DWORD			      BASESLOT_TYPE;
#define BASESLOT_NULL		((BASESLOT_TYPE)-1)


//预定义slot之FactoryType
enum eBASESLOT_TYPE
{
	//gamelib
	 SLOTT_ITEM			= TAG('ITEM')
	,SLOTT_QUICK		= TAG('QICK')
	,SLOTT_SKILL		= TAG('SKIL')
	,SLOTT_STYLEQUICK	= TAG('STYQ')

	///client
	,SLOTT_ITEMDUMMY	= TAG('ITMD')
	,SLOTT_ITEMHANDLE	= TAG('ITMH')
	,SLOTT_EQUIP		= TAG('EQIP')
	,SLOTT_SKILLPANE	= TAG('SKLP')
	,SLOTT_QUICKPANE	= TAG('QIKP')
	,SLOTT_STYLE		= TAG('STYL')
	,SLOTT_SUMMON		= TAG('SUMM')

	//server
	,SLOTT_TRADE		= TAG('TRAD')
	,SLOTT_VENDOR		= TAG('VEND')
	,SLOTT_EVENT		= TAG('EVEN')
	,SLOTT_INVENTORY	= TAG('INVE')
	,SLOTT_INVENTORY2	= TAG('TEMP')
	,SLOTT_WAREHOUSE	= TAG('WARE')
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSLOTUPDATE
{
	 SU_TYPE			= _BIT(0)
	,SU_SERIAL		= _BIT(1)
	,SU_CODE			= _BIT(2)
	,SU_DURA			= _BIT(3)

	,SU_ALL			= _MASK(4)
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSLOTINDEX
{
	 SI_INVENTORY2			= 0 //第二背包，即临时背包
	,SI_INVENTORY			= 1
	,SI_EQUIPMENT			= 2
	,SI_WAREHOUSE			= 3
	,SI_SKILL				= 4
	,SI_QUICK				= 5
	,SI_STYLE				= 6
	,SI_MAX					= 7			///<  SI_MAX

	,SI_EVENT_INVENTORY	= 8
   ,SI_TRADE				= 9
	,SI_NPCSHOP			= 10		
	,SI_ENCHANT				= 11		///物品制造时，原料容器
	,SI_RANKUP				= 12		///物品升品时
   ,SI_VENDOR_SELL		= 13
   ,SI_VENDOR_BUY			= 14

	,SI_INLAY				= SI_ENCHANT	/// leo 装备镶嵌时，插槽清单容器
	,SI_ITEMMAKE			= 15				/// leo 物品打造时，原料容器
	,SI_ENCHANT_TARGET	= 16				/// 合成系统目标槽/结果槽
	,SI_ENCHANT_SUBRESULT= 17				/// 合成系统副物品槽
	,SI_ENCHANT_SOCKET	= 18				/// 合成系统插槽
	,SI_ITEMLOG				= 19				/// 物品日志
   ,SI_TRADE2				= 20
	,SI_MAXNUM

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSLOTTYPE
{
	 ST_NONE
	,ST_SKILL			///<技能槽
	,ST_QUICK			///<快速栏槽
	,ST_STYLE_QUICK	///<心法快速栏槽
	,ST_ITEM				///<物品槽
	,ST_ITEMHANDLE		///<辅助物品槽
};



#endif //__CONSTSLOT_H__
#ifndef __SCMONSTER_H__
#define __SCMONSTER_H__

#pragma once

#include "StructBase.h"

class SCMonster
{
public:
	SCMonster();
	~SCMonster();

	inline BASE_MONSTERINFO*	GetMonsterInfo() { return m_pBaseInfo; }
	inline VOID					SetMonsterInfo( BASE_MONSTERINFO* pMonsterInfo) { m_pBaseInfo = pMonsterInfo; }

protected:
	BASE_MONSTERINFO*			m_pBaseInfo;	// SCMonsterInfoList에 있는 정보를 공유하는 포인터이므로 삭제하지 말것
};

#endif // __SCMONSTER_H__
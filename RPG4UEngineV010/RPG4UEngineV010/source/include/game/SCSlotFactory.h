#ifndef __SC_SLOT_FACTORY_H__
#define __SC_SLOT_FACTORY_H__

//=============================================================================================================================
/// BaseSlot 풀을 관리하는 SCSlotFactory class
/**
	@author	Kim Min Wook < taiyo@webzen.co.kr >
	@since	2004. 12. 29
	@note
		- 기본적인 ItemSlot, SkillSlot 그대로 사용하는 곳에서 풀관리를 편하게 하기 위해 만들어진 helper class
		- 사용하는 곳에서 SCSlotFactory::Instance()->Init(), SCSlotFactory::Instance()->Release()를 반드시 호출해야 한다.	
*/
//=============================================================================================================================

#pragma once


#include <Singleton.h>
#include <TMemoryPoolFactory.h>
#include "ConstSlot.h"

using namespace util;

class BaseSlot;
class ItemSlot;
class SkillSlot;

class SCSlotFactory : public Singleton<SCSlotFactory>
{
public:
	SCSlotFactory();
	~SCSlotFactory();

	VOID								Init( DWORD dwItemSlotPoolSize, DWORD dwSkillSlotPoolSize );
	VOID								Release();
	BaseSlot *							AllocSlot( eSLOTTYPE type );
	VOID								FreeSlot( BaseSlot * pSlot );

private:
	TMemoryPoolFactory<ItemSlot> *	m_pItemSlotPool;
	TMemoryPoolFactory<SkillSlot> *	m_pSkillSlotPool;
};

#endif // __SC_SLOT_FACTORY_H__
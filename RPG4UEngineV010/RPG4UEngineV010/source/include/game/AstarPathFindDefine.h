/*////////////////////////////////////////////////////////////////////////
文 件 名：AstarPathFindDefine.h
创建日期：2007年3月17日
最后更新：2007年3月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ASTARPATHFINDDEFINE_H__
#define __ASTARPATHFINDDEFINE_H__
#pragma once



typedef DWORD	ADWORD;


///////////////////////////////////////////////////////////////
const DWORD		MAX_PATH_FIND_COST			= 30000;
const DWORD		PATH_FIND_SHORT_LENGTH		= 100*2;
const DWORD		CONST_SHORTFINDINTERVAL		=	1;

///////////////////////////////////////////////////////////////
#define PATH_TILE_POS(x,y)			(((ADWORD)y<<16)|x)
#define PATH_TILE_X(pos)			(pos&0xffff)
#define PATH_TILE_Y(pos)			(pos>>16)

enum
{
	 DIR_UP			= _BIT(0)
	,DIR_RIGHTUP	= _BIT(1)
	,DIR_RIGHT		= _BIT(2)
	,DIR_RIGHT_DOWN= _BIT(3)
	,DIR_DOWN		= _BIT(4)
	,DIR_LEFT_DOWN	= _BIT(5)
	,DIR_LEFT		= _BIT(6)
	,DIR_LEFT_UP	= _BIT(7)
};


///////////////////////////////////////////////////////////////
typedef BOOL (*PROC_PATHMOVE_CHECK)(TILEPOS x, TILEPOS y);

struct sPATH_FIND_NODE
{
	ADWORD				m_TilePos;			// 该点的坐标(16,16)的模式保存(y,x)
	TILEPOS				m_Cost;				// 保存从起点到该节点的实际开销
	TILEPOS				m_CostEstimate;	// 保存此点的估价开销
	TILEPOS				m_CostSum;			// 上面两者之和
	sPATH_FIND_NODE *	m_pFather;			// 此点的父节点
	sPATH_FIND_NODE *	m_pPrev;				// 在m_OpenNodes或者m_pNext中的上一个节点
	sPATH_FIND_NODE *	m_pNext;				// 在m_OpenNodes或者m_pNext链表中的下一个节点
	char					m_bModified;      // 该节点是否被修改过，记录而备清除1空,2 m_OpenNodes,4 m_CloseNodes
	TILEPOS				m_DirWalked;		// 有字节点的方向(未用)
	TILEPOS				m_DirFrom;			// 从哪个方向来的(未用)
};



#endif //__ASTARPATHFINDDEFINE_H__
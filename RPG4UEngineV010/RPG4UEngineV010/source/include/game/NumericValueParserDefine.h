/*////////////////////////////////////////////////////////////////////////
文 件 名：NumericValueParserDefine.h
创建日期：2008年4月22日
最后更新：2008年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NUMERICVALUEPARSERDEFINE_H__
#define __NUMERICVALUEPARSERDEFINE_H__
#pragma once


//////////////////////////////////////////////////////////////
#define	DEFAULT_NUM		2
#define	DEFAULT_INDEX	0


//////////////////////////////////////////////////////////////
#define NUMERIC_FUNC_DECL( ret, name, param_type )\
								ret Get##name ( param_type va );

//////////////////////////////////////////////////////////////
#define NUMERIC_FUNC_DECL2( ret, name, param_type,param_type2 )\
								ret Get##name ( param_type va,param_type2 type );

//////////////////////////////////////////////////////////////
#define NUMERIC_FUNC_IMPL( ret, name, param_type )\
								inline ret Get##name ( param_type va )\
								{\
									return theNumericValueParser.Get##name (va );\
								}\
								inline ret NumericValueParser::Get##name ( param_type va )

//////////////////////////////////////////////////////////////
#define NUMERIC_FUNC_IMPL2( ret, name, param_type,param_type2 )\
								inline ret Get##name ( param_type va ,param_type2 type)\
								{\
									return theNumericValueParser.Get##name (va,type );\
								}\
								inline ret NumericValueParser::Get##name ( param_type va,param_type2 type )

//////////////////////////////////////////////////////////////
#define NUMERIC_FUNC_MINMAX( ret, name, param_type, off,min,max,mem )\
								inline ret Get##name ( param_type va )\
								{\
									return theNumericValueParser.Get##name (va );\
								}\
								inline ret NumericValueParser::Get##name ( param_type va )\
								{\
									if(va >= min && va <= max)\
										return (ret)mem.m_##name[va+DEFAULT_NUM + off];\
									return (ret)mem.m_##name[DEFAULT_INDEX];\
								}


/*////////////////////////////////////////////////////////////////////////
			PriceWeightForItemType			DamageForDivineWeapon																
	编号	描述	价格权重				力量附加限制					敏捷附加限制					智力附加限制					备注
	Code	Desc	Hard类价格	Medium类价格	Soft类价格	附加伤害	力士	游侠	术士	刺客	巫师	力士	游侠	术士	刺客	巫师	力士	游侠	术士	刺客	巫师	Comment
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_ITEMTYPE
{
	enum{PRICE_HARD,PRICE_MEDIUM,PRICE_SOFT,PRICE_NUM};
	DWORD		m_ItemType;
	float		m_Prices		[PRICE_NUM];
	INT		m_ExtraDamage;
	INT		m_LimitSTR	[PLAYERTYPE_NUM];
	INT		m_LimitDEX	[PLAYERTYPE_NUM];
	INT		m_LimitINT	[PLAYERTYPE_NUM];
	INT		m_LimitVIT	[PLAYERTYPE_NUM];
};


/*////////////////////////////////////////////////////////////////////////
	PriceWeightForEnchant	强化价格级权重
	WeaponEnchantWeightForEnchant	武器强化级权重
	ArmorEnchantWeightForEnchant	防具强化级权重
	DuraEnchantWeightForEnchant	耐久度强化级权重
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_EHCHANT
{
	enum{DATAUNIT_NUM=13+ DEFAULT_NUM};
	float	m_PriceWeightForEnchant				[DATAUNIT_NUM];
	SHORT	m_WeaponEnchantWeightForEnchant	[DATAUNIT_NUM];
	SHORT	m_ArmorEnchantWeightForEnchant	[DATAUNIT_NUM];
	SHORT	m_DuraEnchantWeightForEnchant		[DATAUNIT_NUM];
};


/*////////////////////////////////////////////////////////////////////////
PriceWeightForRank	品质价格级权重
PriceWeightForRareRank	
LimitValueForRank	品质限制
/*////////////////////////////////////////////////////////////////////////

struct sNUMERICINFO_RANK
{
	enum{DATAUNIT_NUM=RANK_MAX+1+ DEFAULT_NUM};
	float	m_PriceWeightForRank			[DATAUNIT_NUM];
	float	m_PriceWeightForRareRank	[DATAUNIT_NUM];
	SHORT	m_LimitValueForRank			[DATAUNIT_NUM];
};


/*////////////////////////////////////////////////////////////////////////
WeaponEnchantWeightForLV	武器对人物级别强化权重
ArmorEnchantWeightForLV	防具对人物级别强化权重
ExpLevelRatio	级别差距影响经验获取率
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_LEVEL
{
	enum{DATAUNIT_NUM=20 + DEFAULT_NUM};
	BYTE	m_WeaponEnchantWeightForLV		[DATAUNIT_NUM];
	BYTE	m_ArmorEnchantWeightForLV		[DATAUNIT_NUM];
	float	m_ExpLevelRatio					[DATAUNIT_NUM];
};

/*////////////////////////////////////////////////////////////////////////
AdditionalExpRatio	队员数量经验加成
BonusAttackPower	队员数量攻击加成
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_PARTY
{
	enum{DATAUNIT_NUM=10 + DEFAULT_NUM};
	float	m_AdditionalExpRatio		[DATAUNIT_NUM];
	SHORT	m_BonusAttackPower		[DATAUNIT_NUM];
};

/*////////////////////////////////////////////////////////////////////////
ExpRankRatio	Npc级别影响经验加成
StatusRatioAsNPCGrade	Npc级别受状态机率
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_NPC
{
	enum{DATAUNIT_NUM=5 + DEFAULT_NUM};
	float	m_ExpRankRatio				[DATAUNIT_NUM];
	float	m_StatusRatioAsNPCGrade	[DATAUNIT_NUM];
};

/*////////////////////////////////////////////////////////////////////////
RoomBonusTypeRatio	区域奖励加成
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_MAP
{
	enum{DATAUNIT_NUM=2 + DEFAULT_NUM};
	float	m_RoomBonusTypeRatio			[DATAUNIT_NUM];
};

/*////////////////////////////////////////////////////////////////////////
BaseMoveSpeedAsState	各类动作移动速度
MoveTimeAsState		各类动作移动时间
/*////////////////////////////////////////////////////////////////////////
struct sNUMERICINFO_SPEED
{
	enum{DATAUNIT_NUM=MOVETYPE_MAX + DEFAULT_NUM};
	float	m_BaseMoveSpeedAsState			[DATAUNIT_NUM];
	int	m_MoveTimeAsState					[DATAUNIT_NUM];
};


#endif //__NUMERICVALUEPARSERDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemSlot.inl
创建日期：2008年6月10日
最后更新：2008年6月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMSLOT_INL__
#define __ITEMSLOT_INL__
#pragma once


inline eSLOTTYPE		ItemSlot::GetSlotType() const
{
	return ST_ITEM;
}
inline SERIALTYPE		ItemSlot::GetSerial() const	
{
	return m_ItemStream.ItemPart.dwSerial; 
}
inline SLOTCODE		ItemSlot::GetCode() const
{
	return m_ItemStream.ItemPart.wCode;
}
//inline DURATYPE	ItemSlot::GetDura() const				{ return m_ItemStream.ItemPart.byDura;	}


inline BOOL ItemSlot::IsLocked()	
{
	if( IsBlocked() || m_bLocked )
		return TRUE;
	return FALSE;	
}
inline VOID				ItemSlot::SetLock( BOOL val )
{
	m_bLocked = val;	
}

inline DURATYPE	ItemSlot::GetMaxNum()		
{
	return GetItemInfo()->m_byDupNum; 
}

inline float		ItemSlot::GetfDura()
{
	ASSERT(WithDura());
	return m_Modify.m_Dura; 
}

inline VOID ItemSlot::_InitfDura()
{
	m_Modify.m_Dura = GetMaxDura(); 
}




inline BOOL ItemSlot::IsDivine()			
{ return (m_Modify.m_Set == 1);	}

inline BOOL ItemSlot::IsBlocked()			
{ return (m_Modify.m_Set == 15);	}	//< 4bit


inline eRANK ItemSlot::GetRank()		
{
	return m_RankInfo.GetRank();
}
inline VOID ItemSlot::SetRank( eRANK rank )	
{
	m_RankInfo.SetRank(rank);	
}

inline VOID				ItemSlot::SetRankAttr(eRANK                rank
                                            ,const sRANK_OPTION * pRankOption
														  ,INT						nModify
														  ,BOOL						bCreateInc)
{ 
	m_RankInfo.SetRankAttr(rank,pRankOption,nModify,bCreateInc);
}

inline eATTRIBUTE_TYPE		ItemSlot::GetRankAttr( eRANK rank )	
{ 
	return m_RankInfo.GetRankAttr(rank);
}

inline INT				ItemSlot::GetRankAttrValue( eRANK rank )	
{
	return m_RankInfo.GetRankAttrValue(rank);
}

inline const sRANK_OPTION * ItemSlot::GetRankAttrDesc( eRANK rank )
{
	return m_RankInfo.GetRankAttrDesc(rank);
}

inline eRANK_LEVEL ItemSlot::GetRankLevel() 
{ 
	return Rank2Level(m_RankInfo.GetRank());
}

inline const ItemRank& ItemSlot::GetRankInfo()
{
	return	m_RankInfo;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline ePOTENTIAL ItemSlot::GetPotential()		
{
	return m_RankInfo.GetPotential();
}
inline VOID ItemSlot::SetPotential( ePOTENTIAL poten )	
{
	m_RankInfo.SetPotential(poten);	
}

inline VOID ItemSlot::SetPotentialAttr	(ePOTENTIAL           poten
                                          ,const sRANK_OPTION * pPotentialOption
														,INT						 nModify
														,BOOL						 bCreateInc)
{ 
	m_RankInfo.SetPotentialAttr(poten,pPotentialOption,nModify,bCreateInc);
}

inline void ItemSlot::SetPotentialAttrValue	( ePOTENTIAL poten,   INT nValue )
{
	m_RankInfo.SetPotentialAttrValue(poten,nValue);
}

inline eATTRIBUTE_TYPE		ItemSlot::GetPotentialAttr( ePOTENTIAL poten )	
{ 
	return m_RankInfo.GetPotentialAttr(poten);
}

inline INT ItemSlot::GetPotentialAttrValue( ePOTENTIAL poten )	
{
	return m_RankInfo.GetPotentialAttrValue(poten);
}

inline const sRANK_OPTION * ItemSlot::GetPotentialAttrDesc( ePOTENTIAL poten )
{
	return m_RankInfo.GetPotentialAttrDesc(poten);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline BYTE				ItemSlot::GetSocketNum()			
{ 
	if(GetItemInfo()->m_wType == ITEMTYPE_SOCKET)
		return 1;
	return m_SocketInfo.GetSocketNum();	
}
inline VOID				ItemSlot::SetSocketNum( BYTE num )	
{ 
	m_SocketInfo.SetSocketNum(num);
}
inline const sITEM_ATTR_INFO * ItemSlot::GetSocketAttrDesc( eSOCKET sock )
{ 
	return m_SocketInfo.GetSocketAttrDesc(sock);
}

inline VOID ItemSlot::SetSocketAttr	(eSOCKET							sock
													,const sITEM_ATTR_INFO *	pSocketOption)
{ 
	m_SocketInfo.SetSocketAttr(sock,pSocketOption);	
	UpdatePotentialUsed();
}

inline eATTRIBUTE_TYPE		ItemSlot::GetSocketAttr( eSOCKET sock ,eSOCKET_VALUE valueType)	
{
	return m_SocketInfo.GetSocketAttr(sock,valueType);	
}

inline INT				ItemSlot::GetSocketAttrValue( eSOCKET sock,eSOCKET_VALUE valueType )	
{ 
	return m_SocketInfo.GetSocketAttrValue(sock,valueType);	
}

inline INT				ItemSlot::GetSocketValueType( eSOCKET sock,eSOCKET_VALUE valueType )	
{ 
	return m_SocketInfo.GetSocketValueType(sock,valueType);	
}

inline INT				ItemSlot::GetSocketAttrIndex( eSOCKET sock,eSOCKET_VALUE valueType )	
{
	return m_SocketInfo.GetSocketAttrIndex(sock,valueType);	
}

inline INT ItemSlot::GetSocketLimitLV	( eSOCKET sock)
{
	return m_SocketInfo.GetSocketLimitLV( sock);	
}

inline BOOL ItemSlot::IsDoubleAttr( eSOCKET sock)
{
	return m_SocketInfo.IsDoubleAttr(sock);	
}


inline BOOL ItemSlot::IsMinMaxAttr( eSOCKET sock)
{
	return m_SocketInfo.IsMinMaxAttr(sock);	
}

inline BOOL ItemSlot::IsRandAttr( eSOCKET sock)
{
	return m_SocketInfo.IsRandAttr(sock);	
}

inline INT ItemSlot::GetSocketAttrNum( eSOCKET sock)
{
	return m_SocketInfo.GetSocketAttrNum(sock);	
}

inline BOOL ItemSlot::CopySocketInfo		(eSOCKET           sockTo
                                          ,const ItemSlot& itemInfo
														,eSOCKET           sockFrom)
{
	BOOL bRet;
	bRet = m_SocketInfo.CopySocketInfo	(sockTo
													,itemInfo.m_SocketInfo
													,sockFrom);
	if(bRet)
		UpdatePotentialUsed();
	return bRet;
}

inline const ItemSocket& ItemSlot::GetSocketInfo()
{
	return m_SocketInfo;
}



inline ITEM_STREAM *			ItemSlot::_GetItemStream()	
{
	return &m_ItemStream;	
}

inline eENCHANTLEVEL ItemSlot::GetEnchantLV()
{
	return GetCompEnchantLV(GetEnchant());
}




#endif //__ITEMSLOT_INL__
/*////////////////////////////////////////////////////////////////////////
文 件 名：BaseBattleFormular.h
创建日期：2009年5月8日
最后更新：2009年5月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __BASEBATTLEFORMULAR_H__
#define __BASEBATTLEFORMULAR_H__
#pragma once

#include "IBattleFormula.h"

class CharacterCookie;


class BaseBattleFormular : public IBattleFormula
{
public:
	DWORD CalcPVPPoint(DWORD dwMaxKillSeries
                     ,DWORD dwTotalKill
						   ,DWORD dwTotalDie);
	DWORD CalcPVPScore(LEVELTYPE charLV
                             ,INT       nLVInterval);

	////////////////////////////////////////////
	//战斗经验
	virtual float CalcExpRatioNum		( DWORD partymemberNum );
	virtual DWORD GetKillExpOfParty	(BYTE       byRoomBonusType
                                    ,DWORD      partymemberNum
												,LEVELTYPE  partyTotalLV
												,LEVELTYPE  attackerLV
												,LEVELTYPE  targetLV
												,BYTE       NPCGrade
												,DAMAGETYPE attackDamage
												,DWORD      targetMaxHP);

	///////////////////////////////////
	//装备质地带来的伤害
	virtual BYTE GetArmorDamageRatio	(eARMOR_TYPE armorType
                                    ,eMELEE_TYPE attackType)		;	// 酒赣 鸥涝喊 单固瘤 利侩啦

	///////////////////////////////////
	// HP,MP恢复处理
	virtual int		CalcHPRecover				(ePLAYER_TYPE eCharType
                                          ,DWORD        maxHP
														,BYTE         byCharState
														,LEVELTYPE    Level);
	virtual int		CalcMPRecover				(ePLAYER_TYPE eCharType
                                          ,DWORD        spirit
														,BYTE         byCharState);
	virtual DWORD	CalcMPRecoverByAttack	(ePLAYER_TYPE eCharType
                                          ,DWORD        spirit
														,DWORD        skillRecoverPercent
														,DWORD        skillRequireMP);
	virtual DWORD	CalcMPRecoverByAttacked	(ePLAYER_TYPE eCharType
                                          ,DWORD        spirit
														,DAMAGETYPE   damage);									// 鸥拜 罐疽阑嫐 雀汗樊


	///////////////////////////////////
	// NPC恢复HP, MP 
	virtual int CalcNPCHPRecover			( DWORD maxHP, DWORD recoverRatio = 1 );
	virtual int CalcNPCMPRecover			( DWORD maxMP, DWORD recoverRatio = 1 );

	///////////////////////////////////
	virtual BOOL IsPhysicalAttackSuccess(LEVELTYPE targetLV
                                       ,DWORD     attackRate
													,DWORD     avoidRate);
	virtual BOOL IsMagicAttackSuccess	();

	///////////////////////////////////
	virtual DAMAGETYPE	CalcNormalDamage	(CharacterCookie*   pAttacker
                                          ,CharacterCookie*   pTarget
														,eATTACK_KIND eAttackType
														,WORD         criticalRatioBonus	=0
														,BOOL*        pCritical				=NULL
														,float        fDefenseIgnore		=0);

	///////////////////////////////////
	virtual DAMAGETYPE CalcSkillDamage		(CharacterCookie*       pAttacker
                                          ,CharacterCookie*       pTarget
														,eATTACK_KIND           eAttackType
														,int                    iSkillAttackPower
														,float                  fSkillPercentDamage
														,WORD                   criticalRatioBonus=0
														,BOOL*                  pCritical			=NULL
														,BYTE							bySkillStatType	= 0);


protected:
	DWORD FastRecoveryTime	( DWORD delayTime, SHORT Dexterity, DWORD dwItemFastRecovery, DWORD dwSkillFastRecovery );
	BOOL	IsAttSuccess		( ePLAYER_TYPE eCharType, LEVELTYPE LV, SHORT Dexterity, DWORD dwItemAttSuccessInc, DWORD dwSKillAttSuccessInc );

	int	CalcFirstDamage(int          iBaseAttackPower
                        ,int          iSkillAttackPower
								,SHORT        sSkillStat
								,float        fSkillPercentDamage
								,eATTACK_KIND attackType);
	int CalcSecondDamage_Physical	(int   iFirstMinDamage
											,int   iFirstMaxDamage
											,int   iBonusDamage
											,int   iTargetDefense
											,float fArmorDamageRatio);
	int CalcSecondDamage_Magic	(int iFirstMinDamage
										,int iFirstMaxDamage
										,int iBonusDamage
										,int iTargetDefense);

	BOOL IsCriticalHit		(eOBJECT_TYPE eObjectType
                           ,int          criticalRatio
									,LEVELTYPE    LV);
	int CalcCriticalDamage	(eOBJECT_TYPE eObjectType
									,LEVELTYPE    TargetLV
									,int          iSecondDamage
									,int          iCriticalRatio
									,int          iCriticalDamageBonus
									,float        fCriticalDamagePercentBonus);

	DAMAGETYPE CalcPhysicalDamage_Skill(CharacterCookie*   pAttacker
												  ,CharacterCookie*   pTarget
												  ,eATTACK_KIND attackType
												  ,int          iSkillAttackPower
												  ,float        fSkillPercentDamage
												  ,WORD         criticalRatioBonus
												  ,BOOL*        pCritical
												  ,BYTE         bySkillStatType
												  ,float        fDefenseIgnore);

	DAMAGETYPE CalcMagicDamage_Skill	(CharacterCookie*   pAttacker
												,CharacterCookie*   pTarget
												,eATTACK_KIND attackType
												,int          iSkillAttackPower
												,float        fSkillPercentDamage
												,WORD         criticalRatioBonus
												,BOOL*        pCritical
												,BYTE         bySkillStatType);

	BOOL		 IsStatusHit				( CharacterCookie* pAttacker, CharacterCookie* pTarget );
};

#endif //__BASEBATTLEFORMULAR_H__
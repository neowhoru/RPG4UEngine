/*////////////////////////////////////////////////////////////////////////
文 件 名：NumericValueInfo.h
创建日期：2008年4月22日
最后更新：2008年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NUMERICVALUEINFO_H__
#define __NUMERICVALUEINFO_H__
#pragma once


#include "NumericValueParser.h"

#ifdef USE_PAUSE
#include "ConstItem.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

//---------------------------------------------------------------------------------------------------------
#define NUMERIC_FUNC_START_PARAM( ret, name, param_type, param_type2 )\
								inline ret Get##name ( param_type va, param_type2 type )\
								{\
									switch( va )\
									{


//---------------------------------------------------------------------------------------------------------
#define NUMERIC_FUNC_START( ret, name, param_type )\
								inline ret Get##name ( param_type va )\
								{\
									switch( va )\
									{


//---------------------------------------------------------------------------------------------------------
#define NUMERIC_FUNC_END( retvalue )\
									}\
									LOGINFO( "Invalid value(%u,%d).\n", va,retvalue ); \
									return retvalue;\
								}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( FLOAT, PriceWeightForEnchant, BYTE )
	case 0:		return 0.30f;
	case 1:		return 0.31f;
	case 2:		return 0.33f;
	case 3:		return 0.37f;
	case 4:		return 0.45f;
	case 5:		return 0.61f;
	case 6:		return 0.93f;
	case 7:		return 1.57f;
	case 8:		return 2.85f;
	case 9:		return 5.41f;
	case 10:	return 10.53f;
	case 11:	return 20.77f;
	case 12:	return 41.25f;
NUMERIC_FUNC_END( 0.3f )


NUMERIC_FUNC_START( FLOAT, PriceWeightForRank, eRANK )
	case RANK_9:	return 1.0f;
	case RANK_8:	return 1.1f;
	case RANK_7:	return 1.3f;
	case RANK_6:	return 1.7f;
	case RANK_5:	return 2.5f;
	case RANK_4:	return 4.1f;
	case RANK_3:	return 7.3f;
	case RANK_2:	return 13.7f;
	case RANK_1:	return 26.5f;
	case RANK_0:	return 52.1f;
NUMERIC_FUNC_END(1.0f)


NUMERIC_FUNC_START( FLOAT, PriceWeightForRareRank, eRANK )
	case RANK_NIL:	return 1.0f;
	case RANK_9:	return 1.2f;
	case RANK_8:	return 1.4f;
	case RANK_7:	return 1.6f;
	case RANK_6:	return 1.8f;
	case RANK_4:	return 2.0f;
NUMERIC_FUNC_END(1.0f)


NUMERIC_FUNC_START_PARAM( FLOAT, PriceWeightForItemType, eITEM_TYPE, eARMOR_TYPE )
	case ITEMTYPE_TWOHANDAXE:			return 1.15f;
	case ITEMTYPE_TWOHANDSWORD:		return 1.13f;
	case ITEMTYPE_ONEHANDSWORD:		return 1.05f;
	case ITEMTYPE_SPEAR:				return 0.96f;
	case ITEMTYPE_WHIP:					return 0.92f;
	case ITEMTYPE_ETHERCLAW:			return 0.93f;
	case ITEMTYPE_CROSSBOW:	return 0.97f;
	case ITEMTYPE_ETHERWEAPON:		return 0.99f;
	case ITEMTYPE_SCIMITAR:
	case ITEMTYPE_STAFF:				return 1.26f;
	case ITEMTYPE_ORB:					return 1.23f;
	case ITEMTYPE_BOW:		
	//case ITEMTYPE_COMBAT:		
	case ITEMTYPE_DAGGER:		
	case ITEMTYPE_SHIELD:		
	case ITEMTYPE_ONEHANDAXE:		
	case ITEMTYPE_BOXING:				return 0.92f;

	case ITEMTYPE_ARMOR:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_PROTECTOR:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_PANTS:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_HELMET:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_BOOTS:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_GLOVE:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_BELT:
		{
			switch( type )
			{
			case eARMOR_HARD:				return 0.6f;
			case eARMOR_MEDIUM:			return 0.5f;
			case eARMOR_SOFT:				return 0.4f;
			}
		}
		break;
	case ITEMTYPE_SHIRTS:				return 0.5f;
	case ITEMTYPE_RING:
	case ITEMTYPE_NECKLACE:			return 2.0f;
		/*
	case ITEMTYPE_CAPE:				return 1.6f;
	case ITEMTYPE_WING:				return 1.7f;
	case ITEMTYPE_STONE:				return 1.8f;
	case ITEMTYPE_SPRITE:				return 1.8f;
	case ITEMTYPE_CREST:				return 1.2f;
		*/
	case ITEMTYPE_WARRIOR_SACCESSORY:	return 1.6f;
	case ITEMTYPE_PALADIN_SACCESSORY:		return 1.7f;
	case ITEMTYPE_STABBER_SACCESSORY:		return 1.8f;
	case ITEMTYPE_POWWOW_SACCESSORY:		return 1.8f;
	case ITEMTYPE_ELEMENTALIST_SACCESSORY:return 1.2f;
	case ITEMTYPE_WASTE:						return 0.023f;
	case ITEMTYPE_ENCHANT:						return 0.2f;
	case ITEMTYPE_CRYSTAL:						return 0.1f;
	case ITEMTYPE_SOCKET:						return 0.4f;
	case ITEMTYPE_SOCKETMATERIAL:			return 0.3f;
	case ITEMTYPE_ACCESSORYMATERIAL:		return 0.6f;
	case ITEMTYPE_SACCESSORYMATERIAL:		return 0.5f;
	case ITEMTYPE_ARROW:						return 0.0005f;
	case ITEMTYPE_TOPMAX:		ASSERT(NULL);	return 1.0f;

NUMERIC_FUNC_END(1.0f)


NUMERIC_FUNC_START( INT, LimitValueForRank, BYTE )
	case RANK_9:		return 0;
	case RANK_8:		return 1;
	case RANK_7:		return 2;
	case RANK_6:		return 3;
	case RANK_5:		return 6;
	case RANK_4:		return 9;
	case RANK_3:		return 12;
	case RANK_2:		return 17;
	case RANK_1:		return 22;
	case RANK_0:		return 27;
NUMERIC_FUNC_END( 0 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( INT, WeaponEnchantWeightForEnchant, BYTE )
	case 0:		return 0;
	case 1:		return 0;
	case 2:		return 0;
	case 3:		return 1;
	case 4:		return 2;
	case 5:		return 3;
	case 6:		return 5;
	case 7:		return 7;
	case 8:		return 9;
	case 9:		return 12;
	case 10:		return 15;
	case 11:		return 18;
	case 12:		return 22;
NUMERIC_FUNC_END( 0 )

NUMERIC_FUNC_START( INT, WeaponEnchantWeightForLV, LEVELTYPE )
	case 1:		return 2;
	case 2:		return 2;
	case 3:		return 3;
	case 4:		return 3;
	case 5:		return 3;
	case 6:		return 4;
	case 7:		return 4;
	case 8:		return 4;
	case 9:		return 4;
	case 10:		return 5;
	case 11:		return 5;
	case 12:		return 5;
	case 13:		return 5;
	case 14:		return 5;
	case 15:		return 6;
NUMERIC_FUNC_END( 2 )

NUMERIC_FUNC_START( INT, ArmorEnchantWeightForEnchant, BYTE )
	case 0:		return 0;
	case 1:		return 0;
	case 2:		return 0;
	case 3:		return 1;
	case 4:		return 2;
	case 5:		return 3;
	case 6:		return 5;
	case 7:		return 7;
	case 8:		return 9;
	case 9:		return 12;
	case 10:	return 15;
	case 11:	return 18;
	case 12:	return 22;
NUMERIC_FUNC_END( 0 )

NUMERIC_FUNC_START( INT, ArmorEnchantWeightForLV, LEVELTYPE )
	case 1:		return 1;
	case 2:		return 1;
	case 3:		return 1;
	case 4:		return 1;
	case 5:		return 2;
	case 6:		return 2;
	case 7:		return 2;
	case 8:		return 2;
	case 9:		return 3;
	case 10:	return 3;
NUMERIC_FUNC_END( 1 )

//---------------------------------------------------------------------------------------------------------
// 耐久度
NUMERIC_FUNC_START( BYTE, DuraEnchantWeightForEnchant, BYTE )
	case 0:		return 0;
	case 1:		return 0;
	case 2:		return 0;
	case 3:		return 1;
	case 4:		return 2;
	case 5:		return 3;
	case 6:		return 5;
	case 7:		return 7;
	case 8:		return 9;
	case 9:		return 12;
	case 10:		return 15;
	case 11:		return 18;
	case 12:		return 22;
NUMERIC_FUNC_END( 0 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START_PARAM( INT, LimitStrForWeapon, ePLAYER_TYPE, eITEM_TYPE )
	case PLAYERTYPE_WARRIOR:
		{
			switch(type)
			{
			case ITEMTYPE_TWOHANDAXE:		return 13;
			case ITEMTYPE_TWOHANDSWORD:	return 9;
			}
		}break;
	case PLAYERTYPE_PALADIN:
		{
			switch(type)
			{
			case ITEMTYPE_ONEHANDSWORD:	return 7;
			case ITEMTYPE_SPEAR:			return 12;
			}
		}break;
	case PLAYERTYPE_POWWOW:
		{
			switch(type)
			{
			case ITEMTYPE_WHIP:			return 11;
			case ITEMTYPE_ETHERCLAW:		return 8;
			}
		}break;
	case PLAYERTYPE_STABBER:
		{
			switch(type)
			{
			case ITEMTYPE_CROSSBOW:	return 4;
			case ITEMTYPE_ETHERWEAPON:		return 6;
			}
		}break;
	case PLAYERTYPE_NECROMANCER:
		{
			switch(type)
			{
			case ITEMTYPE_STAFF:			return 2;
			case ITEMTYPE_ORB:				return 0;
			}
		}break;
NUMERIC_FUNC_END( 0 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START_PARAM( INT, LimitDexForWeapon, ePLAYER_TYPE, eITEM_TYPE )
	case PLAYERTYPE_WARRIOR:
		{
			switch(type)
			{
			case ITEMTYPE_TWOHANDAXE:		return 0;
			case ITEMTYPE_TWOHANDSWORD:	return 4;
			}
		}break;
	case PLAYERTYPE_PALADIN:
		{
			switch(type)
			{
			case ITEMTYPE_ONEHANDSWORD:	return 5;
			case ITEMTYPE_SPEAR:			return 0;
			}
		}break;
	case PLAYERTYPE_POWWOW:
		{
			switch(type)
			{
			case ITEMTYPE_WHIP:			return 0;
			case ITEMTYPE_ETHERCLAW:		return 3;
			}
		}break;
	case PLAYERTYPE_STABBER:
		{
			switch(type)
			{
			case ITEMTYPE_CROSSBOW:	return 7;
			case ITEMTYPE_ETHERWEAPON:		return 5;
			}
		}break;

/*
	// 郡府膏呕府胶飘绰 刮酶荤侩救窃
	case PLAYERTYPE_NECROMANCER:
		{
			switch(type)
			{
			case ITEMTYPE_STAFF:			return 0;
			case ITEMTYPE_ORB:				return 2;
			}
		}break;
*/
NUMERIC_FUNC_END( 0 )


NUMERIC_FUNC_START_PARAM( INT, LimitIntForWeapon, ePLAYER_TYPE, eITEM_TYPE )
	case PLAYERTYPE_NECROMANCER:
		{
			switch(type)
			{
			case ITEMTYPE_STAFF:			return 8;
			case ITEMTYPE_ORB:				return 10;
			}
		}break;
NUMERIC_FUNC_END( 0 )


//////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( INT, LimitStrForArmor, ePLAYER_TYPE )
	case PLAYERTYPE_WARRIOR:		return 11;
	case PLAYERTYPE_PALADIN:			return 9;
	case PLAYERTYPE_POWWOW:			return 10;
	case PLAYERTYPE_STABBER:		return 5;
	case PLAYERTYPE_NECROMANCER:		return 1;
NUMERIC_FUNC_END( 0 )

NUMERIC_FUNC_START( INT, LimitDexForArmor, ePLAYER_TYPE )
	case PLAYERTYPE_WARRIOR:		return 0;
	case PLAYERTYPE_PALADIN:			return 2;
	case PLAYERTYPE_POWWOW:			return 0;
	case PLAYERTYPE_STABBER:		return 6;
	//case PLAYERTYPE_NECROMANCER:		return 1;
NUMERIC_FUNC_END( 0 )

NUMERIC_FUNC_START( INT, LimitIntForArmor, ePLAYER_TYPE )
	case PLAYERTYPE_NECROMANCER:		return 9;
NUMERIC_FUNC_END( 0 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( float, ExpLevelRatio, int )
	case 1:			return 0.95f;
	case 2:			return 0.9f;
	case 3:			return 0.85f;
	case 4:			return 0.8f;
	case 5:			return 0.75f;
	case 6:			return 0.7f;
	case 7:			return 0.65f;
	case 8:			return 0.6f;
	case 9:			return 0.55f;
	case 10:			return 0.5f;
	case 11:			return 0.45f;
	case 12:			return 0.4f;
	case 13:			return 0.35f;
	case 14:			return 0.3f;
	case 15:			return 0.25f;
	case 16:			return 0.2f;
	case 17:			return 0.15f;
	case 18:			return 0.1f;
	case 19:			return 0.05f;
	default:
		if( va <= 0 )	return 1.0f;
		else			return 0;
NUMERIC_FUNC_END( 0 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( float, AdditionalExpRatio, DWORD )
	case 1:		return 1.0f;
	case 2:		return 1.15f;
	case 3:		return 1.3f;
	case 4:		return 1.45f;
	case 5:		return 1.55f;
	case 6:		return 1.65f;
	case 7:		return 1.75f;
	case 8:		return 1.85f;
	case 9:		return 1.9f;
	case 10:		return 1.95f;
NUMERIC_FUNC_END( 1.0f )

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( float, ExpRankRatio, BYTE )
	case 1:		return 1.0f;		// 
	case 2:		return 1.3f;		// 
	case 3:		return 1.5f;		// 
	case 4:		return 10.0f;		// 
	case 5:		return 15.0f;		// 
NUMERIC_FUNC_END( 1.0f )


NUMERIC_FUNC_START( float, RoomBonusTypeRatio, BYTE )
	case 0:		return 1.0f;		// 
	case 1:		return 1.1f;		// 
NUMERIC_FUNC_END( 1.0f )


NUMERIC_FUNC_START( float, BonusAttackPower, int )
	case 1:		return 0;
	case 2:		return 2.0f;
	case 3:		return 4.0f;
	case 4:		return 6.0f;
	case 5:		return 8.0f;
	case 6:		return 10.0f;
	case 7:		return 12.0f;
	case 8:		return 14.0f;
	case 9:		return 16.0f;
	case 10:		return 18.0f;
NUMERIC_FUNC_END( 0 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( INT, DamageForDivineWeapon, eITEM_TYPE )
	case ITEMTYPE_TWOHANDAXE:			return 25;
	case ITEMTYPE_TWOHANDSWORD:		return 22;	
	case ITEMTYPE_ONEHANDSWORD:		return 18;
	case ITEMTYPE_SPEAR:				return 23;
	case ITEMTYPE_WHIP:					return 15;
	case ITEMTYPE_ETHERCLAW:			return 13;		
	case ITEMTYPE_CROSSBOW:	return 17;
	case ITEMTYPE_ETHERWEAPON:		return 24;
	case ITEMTYPE_STAFF:				return 11;	
	case ITEMTYPE_ORB:					return 9	;
	//case ITEMTYPE_STAFF:				return 22;	
	//case ITEMTYPE_ORB:				return 20;
NUMERIC_FUNC_END( 1 )


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( float, StatusRatioAsNPCGrade, int )
	case 1:		return 1.0f;
	case 2:		return 0.80f;
	case 3:		return 0.70f;
	case 4:		return 0.50f;
	case 5:		return 0.20f;
	case 6:		return 1.0f;	
NUMERIC_FUNC_END( 1.0f )


// 基本速度值
NUMERIC_FUNC_START( float, BaseMoveSpeedAsState, int )
	case MOVETYPE_WALK:					return 0.8f  * SCALE_SPEED_FACTOR;
	case MOVETYPE_RUN:					return 2.4f  * SCALE_SPEED_FACTOR;
	case MOVETYPE_SWIPE:				return 8.0f  * SCALE_SPEED_FACTOR;
	case MOVETYPE_KNOCKBACK:			return 4.5f  * SCALE_SPEED_FACTOR;
	case MOVETYPE_KNOCKBACK_DOWN:	return 9.0f  * SCALE_SPEED_FACTOR;
	case MOVETYPE_SIDESTEP:			return 1.8f  * SCALE_SPEED_FACTOR;
	case MOVETYPE_BACKSTEP:			return 1.2f  * SCALE_SPEED_FACTOR;
	//case MOVETYPE_WALK:				return 1.2f  * SCALE_SPEED_FACTOR;
	//case MOVETYPE_RUN:				return 5.0f  * SCALE_SPEED_FACTOR;
	//case MOVETYPE_SWIPE:				return 10.0f * SCALE_SPEED_FACTOR;
	//case MOVETYPE_KNOCKBACK:		return 6.0f  * SCALE_SPEED_FACTOR;
	//case MOVETYPE_KNOCKBACK_DOWN:	return 12.0f * SCALE_SPEED_FACTOR;
	//case MOVETYPE_SIDESTEP:			return 3.5f  * SCALE_SPEED_FACTOR;
	//case MOVETYPE_BACKSTEP:			return 2.5f  * SCALE_SPEED_FACTOR;

	case MOVETYPE_TUMBLING_FRONT:	return 10.0f * SCALE_SPEED_FACTOR;
	case MOVETYPE_TUMBLING_LEFT:		return 10.0f * SCALE_SPEED_FACTOR;
	case MOVETYPE_TUMBLING_RIGHT:	return 10.0f * SCALE_SPEED_FACTOR;
	case MOVETYPE_TUMBLING_BACK:		return 10.0f * SCALE_SPEED_FACTOR;
	case MOVETYPE_SHOULDER_CHARGE:	return 10.0f * SCALE_SPEED_FACTOR;
	case MOVETYPE_SLIDING:				return 10.0f * SCALE_SPEED_FACTOR;
	case MOVETYPE_TELEPORT:			return 10.0f * SCALE_SPEED_FACTOR;
NUMERIC_FUNC_END( 5.0f * SCALE_SPEED_FACTOR )



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_START( WORD, MoveTimeAsState, int )
	case MOVETYPE_TUMBLING_FRONT:	return 300;
	case MOVETYPE_TUMBLING_LEFT:		return 300;
	case MOVETYPE_TUMBLING_RIGHT:	return 300;
	case MOVETYPE_SHOULDER_CHARGE:	return 300;
	case MOVETYPE_SLIDING:			return 300;
	case MOVETYPE_TELEPORT:			return 300;
	case MOVETYPE_TUMBLING_BACK:		return 300;
NUMERIC_FUNC_END( 433 )



#endif

#endif //__NUMERICVALUEINFO_H__
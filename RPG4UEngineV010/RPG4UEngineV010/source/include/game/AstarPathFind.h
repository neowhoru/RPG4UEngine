/*////////////////////////////////////////////////////////////////////////
文 件 名：AstarPathFind.h
创建日期：2007年3月17日
最后更新：2007年3月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

	Exampla:

	BOOL ProcPossible(TILEPOS x,TILEPOS y) 
	{ 
		if (x<0||y<0||x>=mapw||y>=maph)
			return false;
		return map[y][x].m_ProcMoveCheck;
	}
	--begin
		AstarPathFind PathFind(mapw,maph,ProcPossible);
		PathFind.FindPath(startx,starty,endx,endy);
	--end

	Point PosXY[500];
	char PosR[500];
	PathFind.GetPosPath(&PosXY,500);
	PathFind.GetDirPath(&PosR,500);

	//清在moveable函数中判断 坐标范围

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ASTARPATHFIND_H__
#define __ASTARPATHFIND_H__
#pragma once


#include "CommonDefine.h"
#include "AstarPathFindDefine.h"

///////////////////////////////////////////////////////////////
class _GAMELIB_API AstarPathFind
{
public:
	AstarPathFind	(int                 nMapWidth
                  ,int                 nMapHeight
						,PROC_PATHMOVE_CHECK proc);
	AstarPathFind	();
	~AstarPathFind	();

public:
	int Create	(int                 nMapWidth
               ,int                 nMapHeight
					,PROC_PATHMOVE_CHECK proc);
	int Release	();

	virtual int FindPath			(TILEPOS startx
                              ,TILEPOS starty
										,TILEPOS endx
										,TILEPOS endy);

   virtual int FindPathShort	(TILEPOS startx
                              ,TILEPOS starty
										,TILEPOS endx
										,TILEPOS endy);


	int GetPosPath	(TILEPOS *	pstPosXY,int nMaxPos);
	int GetDirPath	(char *		Dirs,		int nMaxPos);

	int	GetPathLengh();


protected:
	void Init();

	char _PushToOpenQueue	(sPATH_FIND_NODE *node);// 将节点排序加入m_OpenNodes队列
	char _GetFromOpenQueue();                       // 从m_OpenNodes队列取出最小的并放入m_CloseNodes队列
	char _PopCloseStack();                          // 取出m_CloseNodes队列中的节点
	void _ClearModified();                          // 清除所有搜索记录

	char _FindTile	(TILEPOS          x
                  ,TILEPOS          y
						,sPATH_FIND_NODE* m_pFather
						,char             FromDir);

	TILEPOS _CalcCost	(TILEPOS
                     ,TILEPOS
							,TILEPOS
							,TILEPOS);


public:
    static DWORD ms_ShortFindStartTime;
protected:

	sPATH_FIND_NODE **	m_PathNodes;			// 对应地图中每个节点
	sPATH_FIND_NODE *		m_OpenNodes;			// 保存没有处理的按估计值排序的节点
	sPATH_FIND_NODE *		m_CloseNodes;			// 保存处理过的节点
	sPATH_FIND_NODE **	m_ModifiedNodes;		// 保存修改过的节点
	int						m_MapHeight;			// 地图的高度
	int						m_MapWidth;				// 地图的宽度
	ADWORD					m_MapSize;				// 最大面积即m_MapHeight*m_MapWidth
	ADWORD 					m_ModifyIndex;			// m_ModifiedNodes数组的指针
	ADWORD 					m_OpenCount;			// m_OpenNodes队列中的节点数目
	ADWORD 					m_CloseCount;			// m_CloseNodes队列里面的节点数目
	ADWORD 					m_OpenMax;				// m_OpenNodes队列最多节点数目
	ADWORD 					m_CloseMax;				// m_CloseNodes队列最多节点数目
	TILEPOS 					m_Direction;             // 要搜索方向的标志，0-7位为从上开始顺时针七个方向
	ADWORD 					m_NearPos;             // 终点或最接近终点的节点

	TILEPOS 					m_TargetX;             // 终点坐标
	TILEPOS 					m_TargetY;            

	VG_TYPE_PROPERTY		(ProcMoveCheck, PROC_PATHMOVE_CHECK); // 检查地图是否可以移动函数指针
};

#include "AstarPathFind.inl"

#endif //__ASTARPATHFIND_H__
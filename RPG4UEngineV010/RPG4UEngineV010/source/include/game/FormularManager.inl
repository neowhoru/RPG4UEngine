/*////////////////////////////////////////////////////////////////////////
文 件 名：FormularManager.inl
创建日期：2008年4月25日
最后更新：2008年4月25日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __FORMULARMANAGER_INL__
#define __FORMULARMANAGER_INL__
#pragma once

//#ifdef USE_OLD

inline DWORD FormularManager::GetExpAccumlate(LEVELTYPE LV)
{
	return m_pFormular->GetExpAccumlate(LV);
}


inline DWORD FormularManager::GetStatPointPerLevel()
{
	return m_pFormular->GetStatPointPerLevel();
}

inline DWORD FormularManager::GetSkillPoint(LEVELTYPE LV)
{
	return m_pFormular->GetSkillPoint(LV);
}

inline DWORD FormularManager::CalcHP( ePLAYER_TYPE eCharType, LEVELTYPE LV, SHORT Vitality )
{
	return m_pFormular->CalcHP(eCharType,  LV,  Vitality);
}

inline DWORD FormularManager::CalcMP( ePLAYER_TYPE eCharType, LEVELTYPE LV, SHORT Spirit )
{
	return m_pFormular->CalcMP(  eCharType,  LV,  Spirit);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcMinMeleeAttackPower( ePLAYER_TYPE eCharType, SHORT Strength, SHORT Dexterity )
{
	return m_pFormular->CalcMinMeleeAttackPower(  eCharType,  Strength,  Dexterity );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcMinRangeAttackPower(	ePLAYER_TYPE eCharType, SHORT Strength, SHORT Dexterity )
{
	return m_pFormular->CalcMinRangeAttackPower(	 eCharType,  Strength,  Dexterity );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcMaxMeleeAttackPower( ePLAYER_TYPE eCharType, SHORT Strength, SHORT Dexterity )
{
	return m_pFormular->CalcMaxMeleeAttackPower(  eCharType,  Strength,  Dexterity );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcMaxRangeAttackPower( ePLAYER_TYPE eCharType, SHORT Strength, SHORT Dexterity )
{
	return m_pFormular->CalcMaxRangeAttackPower(  eCharType,  Strength,  Dexterity );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcMagicAttackPower( BOOL bMin, SHORT Intelligence )
{
	return m_pFormular->CalcMagicAttackPower(  bMin,  Intelligence );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcPhyBaseDef( ePLAYER_TYPE eCharType, SHORT Vitality )
{
	return m_pFormular->CalcPhyBaseDef(  eCharType,  Vitality );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcMagicBaseDef( ePLAYER_TYPE eCharType, SHORT Spirit )
{
	return m_pFormular->CalcMagicBaseDef(  eCharType,  Spirit );
}

inline int FormularManager::CalcMoveSpeedRatio( SHORT Dexterity, int iItemMoveSpeed, int iSkillMoveSpeed)
{
	return m_pFormular->CalcMoveSpeedRatio(  Dexterity,  iItemMoveSpeed,  iSkillMoveSpeed);
}


inline DWORD FormularManager::CalcPhyCriticalBaseRatio( ePLAYER_TYPE eCharType, SHORT Dexterity )
{
	return m_pFormular->CalcPhyCriticalBaseRatio(  eCharType,  Dexterity );
}

inline DWORD FormularManager::CalcMagicCriticalBaseRatio( ePLAYER_TYPE eCharType, SHORT Spirit )
{
	return m_pFormular->CalcMagicCriticalBaseRatio(  eCharType,  Spirit );
}


inline DWORD FormularManager::CalcPhysicalAttackRateBase(ePLAYER_TYPE eCharType, LEVELTYPE LV, SHORT Dex)
{
	return m_pFormular->CalcPhysicalAttackRateBase( eCharType,  LV,  Dex);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline DWORD FormularManager::CalcPhysicalAvoidRateBase(ePLAYER_TYPE eCharType, SHORT Dex)
{
	return m_pFormular->CalcPhysicalAvoidRateBase( eCharType,  Dex);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline int FormularManager::CalcAttackSpeedRatio( ePLAYER_TYPE eCharType, SHORT Dexterity, int iItemAttSpeed, int iSkillAttSpeed )
{
	return m_pFormular->CalcAttackSpeedRatio(  eCharType,  Dexterity,  iItemAttSpeed,  iSkillAttSpeed );
}


/*////////////////////////////////////////////////////////////////////////
// HP
/*////////////////////////////////////////////////////////////////////////
//inline float FormularManager::CalcHPRatioNum( DWORD partymemberNum )
//{
//	return m_pFormular->CalcHPRatioNum(  partymemberNum );
//}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//inline float FormularManager::CalcAdditionalHPRatio( DWORD partymemberNum )
//{
//	return m_pFormular->CalcAdditionalHPRatio(  partymemberNum );
//}


//  NPC HP 
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline float FormularManager::CalcHPIncreaseRatioAsParty( DWORD partymemberNum )
{
	return m_pFormular->CalcHPIncreaseRatioAsParty(  partymemberNum );
}

//inline float FormularManager::CalcAttPowerRatio( DWORD partymemberNum )
//{
//	return m_pFormular->CalcAttPowerRatio(  partymemberNum );
//}

inline int FormularManager::CalcAttackPowerIncreaseAsParty( DWORD partymemberNum, int iAttackPower )
{
	return m_pFormular->CalcAttackPowerIncreaseAsParty(  partymemberNum, iAttackPower );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////


inline float FormularManager::CalcItemPriceForBuy(LEVELTYPE   DropLV
                                                  ,eRANK       Rank
																  ,BYTE        Enchant
																  ,eITEM_TYPE   Type
																  ,eARMOR_TYPE ArmorType
																  ,eRANK       RareRank)
{
	return m_pItemFormular->CalcItemPriceForBuy(  DropLV,  Rank,  Enchant,  Type,  ArmorType,  RareRank );
}

inline float FormularManager::CalcItemPriceForSellWithDura(LEVELTYPE   ItemLV
                                                           ,LEVELTYPE   DropLV
																			  ,eRANK       Rank
																			  ,BYTE        Enchant
																			  ,eITEM_TYPE   Type
																			  ,eARMOR_TYPE ArmorType
																			  ,eRANK       RareRank
																			  ,MONEY       RepairFare)
{
	return m_pItemFormular->CalcItemPriceForSellWithDura(  ItemLV,  DropLV,  Rank,  Enchant,  Type,  ArmorType,  RareRank,  RepairFare );
}

inline float FormularManager::CalcItemPriceForSellWithoutDura(LEVELTYPE   DropLV
                                                              ,eRANK       Rank
																				  ,BYTE        Enchant
																				  ,eITEM_TYPE   Type
																				  ,eARMOR_TYPE ArmorType
																				  ,eRANK       RareRank)
{
	return m_pItemFormular->CalcItemPriceForSellWithoutDura(  DropLV,  Rank,  Enchant,  Type,  ArmorType,  RareRank );
}



inline DWORD FormularManager::CalcAttackPower( DWORD Damage, BYTE Enchant, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcAttackPower(  Damage,  Enchant,  ItemLV );
}

inline DWORD FormularManager::CalcPhyDef( DWORD PhyDef, BYTE Enchant, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcPhyDef(  PhyDef,  Enchant,  ItemLV );
}

inline float FormularManager::CalcWeaponDura( eITEM_TYPE ItemType, DWORD MonsterDef, DWORD WeaponMinDamage, DWORD WeaponMaxDamage )
{
	return m_pItemFormular->CalcWeaponDura(  ItemType,  MonsterDef,  WeaponMinDamage,  WeaponMaxDamage );
}

inline float FormularManager::CalcArmorDura( eARMOR_TYPE ArmorType, DWORD MonMinAttackPower, DWORD MonMaxAttackPower, DWORD ItemDef )
{
	return m_pItemFormular->CalcArmorDura(  ArmorType,  MonMinAttackPower,  MonMaxAttackPower,  ItemDef );
	
}
	
inline DWORD FormularManager::CalcPriceForDura( DWORD ItemPrice, DURATYPE ItemDura )
{
	return m_pItemFormular->CalcPriceForDura(  ItemPrice,  ItemDura );
}

inline WORD FormularManager::CalcLimitStatWeapon( WORD ItemLimitStr, BYTE Enchant, eRANK Rank, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcLimitStatWeapon(  ItemLimitStr,  Enchant,  Rank,  ItemLV );
}
inline WORD FormularManager::CalcLimitStatArmor( WORD ItemLimitStr, BYTE Enchant, eRANK Rank, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcLimitStatArmor(  ItemLimitStr,  Enchant,  Rank,  ItemLV );
}


inline WORD FormularManager::CalcLimitStrWeapon( WORD ItemLimitStr, BYTE Enchant, eRANK Rank, ePLAYER_TYPE CharType, eITEM_TYPE ItemType )
{
	return m_pItemFormular->CalcLimitStrWeapon(  ItemLimitStr,  Enchant,  Rank,  CharType,  ItemType );
}

inline WORD FormularManager::CalcLimitDexWeapon( WORD ItemLimitDex, BYTE Enchant, eRANK Rank, ePLAYER_TYPE CharType, eITEM_TYPE ItemType )
{
	return m_pItemFormular->CalcLimitDexWeapon(  ItemLimitDex,  Enchant,  Rank,  CharType,  ItemType );
}

inline WORD FormularManager::CalcLimitIntWeapon( WORD ItemLimitInt, BYTE Enchant, eRANK Rank, ePLAYER_TYPE CharType, eITEM_TYPE ItemType )
{
	return m_pItemFormular->CalcLimitIntWeapon(  ItemLimitInt,  Enchant,  Rank,  CharType,  ItemType );
}

inline WORD FormularManager::CalcLimitStrArmor( WORD ItemLimitStr, BYTE Enchant, eRANK Rank, ePLAYER_TYPE CharType )
{
	return m_pItemFormular->CalcLimitStrArmor(  ItemLimitStr,  Enchant,  Rank,  CharType );
}

inline WORD FormularManager::CalcLimitDexArmor( WORD ItemLimitDex, BYTE Enchant, eRANK Rank, ePLAYER_TYPE CharType )
{
	return m_pItemFormular->CalcLimitDexArmor(  ItemLimitDex,  Enchant,  Rank,  CharType );
}


inline WORD FormularManager::CalcLimitIntArmor( WORD ItemLimitInt, BYTE Enchant, eRANK Rank, ePLAYER_TYPE CharType )
{
	return m_pItemFormular->CalcLimitIntArmor(  ItemLimitInt,  Enchant,  Rank,  CharType );
}





inline WORD FormularManager::CalcLimitLVAccessory( WORD ItemLimitEqLevel, eRANK Rank )
{
	return m_pItemFormular->CalcLimitLVAccessory(  ItemLimitEqLevel,  Rank );
}

inline DWORD FormularManager::CalcMinAttackPowerForDivineWeapon( DWORD dwMinDamage, eITEM_TYPE ItemType, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcMinAttackPowerForDivineWeapon(  dwMinDamage,  ItemType,  ItemLV );
}

inline DWORD FormularManager::CalcMaxAttackPowerForDivineWeapon( DWORD dwMaxDamage, eITEM_TYPE ItemType, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcMaxAttackPowerForDivineWeapon(  dwMaxDamage,  ItemType,  ItemLV );
}

inline WORD FormularManager::CalcLimitStrForDivineWeapon( WORD LimitStr, ePLAYER_TYPE CharType, eITEM_TYPE ItemType )
{	
	return m_pItemFormular->CalcLimitStrForDivineWeapon(  LimitStr,  CharType,  ItemType );
}

inline WORD FormularManager::CalcLimitDexForDivineWeapon( WORD LimitDex, ePLAYER_TYPE CharType, eITEM_TYPE ItemType )
{
	return m_pItemFormular->CalcLimitDexForDivineWeapon(  LimitDex,  CharType,  ItemType );
}

inline WORD FormularManager::CalcLimitIntForDivineWeapon( WORD LimitInt, ePLAYER_TYPE CharType, eITEM_TYPE ItemType )
{
	return m_pItemFormular->CalcLimitIntForDivineWeapon(  LimitInt,  CharType,  ItemType );
}

inline WORD FormularManager::CalcPhyDefForDivineArmor( WORD wPhyDef, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcPhyDefForDivineArmor(  wPhyDef,  ItemLV );
}

inline WORD FormularManager::CalcLimitStrForDivineArmor( WORD LimitStr, ePLAYER_TYPE CharType, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcLimitStrForDivineArmor(  LimitStr,  CharType,  ItemLV );
}

inline WORD FormularManager::CalcLimitDexForDivineArmor( WORD LimitDex, ePLAYER_TYPE CharType, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcLimitDexForDivineArmor(  LimitDex,  CharType,  ItemLV );
}

inline WORD FormularManager::CalcLimitIntForDivineArmor( WORD LimitInt, ePLAYER_TYPE CharType, LEVELTYPE ItemLV )
{
	return m_pItemFormular->CalcLimitIntForDivineArmor(  LimitInt,  CharType,  ItemLV );
}


inline LEVELTYPE FormularManager::CalcDropLVForDivine( LEVELTYPE ItemDropLV )
{
	return m_pItemFormular->CalcDropLVForDivine(  ItemDropLV );
}

//#endif

#endif //__FORMULARMANAGER_INL__
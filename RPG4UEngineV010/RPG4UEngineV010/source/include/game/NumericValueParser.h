/*////////////////////////////////////////////////////////////////////////
文 件 名：NumericValueParser.h
创建日期：2008年4月22日
最后更新：2008年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NUMERICVALUEPARSER_H__
#define __NUMERICVALUEPARSER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "NumericValueParserDefine.h"

class _GAMELIB_API NumericValueParser
{
public:
	NumericValueParser();
	~NumericValueParser();

public:
	BOOL	Init			();
	void  Release		();

	void  Unload		();

	BOOL	LoadNormal	(LPCSTR szInfoFile);
	BOOL	LoadItem		(LPCSTR szInfoFile);

public:
	NUMERIC_FUNC_DECL	( FLOAT, PriceWeightForEnchant,	BYTE );
	NUMERIC_FUNC_DECL	( FLOAT, PriceWeightForRank,		eRANK );
	NUMERIC_FUNC_DECL	( FLOAT, PriceWeightForRareRank, eRANK );
	NUMERIC_FUNC_DECL2( FLOAT, PriceWeightForItemType, eITEM_TYPE, eARMOR_TYPE );
	NUMERIC_FUNC_DECL	( INT,	LimitValueForRank,		BYTE );
	NUMERIC_FUNC_DECL	( INT,	WeaponEnchantWeightForEnchant,	BYTE );
	NUMERIC_FUNC_DECL	( INT,	WeaponEnchantWeightForLV,			LEVELTYPE );
	NUMERIC_FUNC_DECL	( INT,	ArmorEnchantWeightForEnchant,		BYTE );
	NUMERIC_FUNC_DECL	( INT,	ArmorEnchantWeightForLV,			LEVELTYPE );
	NUMERIC_FUNC_DECL	( BYTE,	DuraEnchantWeightForEnchant,		BYTE );
	NUMERIC_FUNC_DECL2( INT,	LimitStrForWeapon,		ePLAYER_TYPE,	eITEM_TYPE );
	NUMERIC_FUNC_DECL2( INT,	LimitDexForWeapon,		ePLAYER_TYPE,	eITEM_TYPE );
	NUMERIC_FUNC_DECL2( INT,	LimitIntForWeapon,		ePLAYER_TYPE,	eITEM_TYPE );
	NUMERIC_FUNC_DECL2( INT,	LimitStrForArmor,			ePLAYER_TYPE,	eITEM_TYPE  );
	NUMERIC_FUNC_DECL2( INT,	LimitDexForArmor,			ePLAYER_TYPE,	eITEM_TYPE  );
	NUMERIC_FUNC_DECL2( INT,	LimitIntForArmor,			ePLAYER_TYPE,	eITEM_TYPE  );
	NUMERIC_FUNC_DECL	( float, ExpLevelRatio,				int );
	NUMERIC_FUNC_DECL	( float, AdditionalExpRatio,		DWORD );
	NUMERIC_FUNC_DECL	( float, ExpRankRatio,				BYTE );
	NUMERIC_FUNC_DECL	( float, RoomBonusTypeRatio,		BYTE );
	NUMERIC_FUNC_DECL	( float, BonusAttackPower,			int );
	NUMERIC_FUNC_DECL	( INT,	DamageForDivineWeapon,	eITEM_TYPE );
	NUMERIC_FUNC_DECL	( float, StatusRatioAsNPCGrade,	int );
	NUMERIC_FUNC_DECL	( float, BaseMoveSpeedAsState,	int );
	NUMERIC_FUNC_DECL	( WORD,	MoveTimeAsState,			int );


public:
	sNUMERICINFO_ITEMTYPE*	m_ItemTypes[ITEMTYPE_TOPMAX];

	sNUMERICINFO_EHCHANT		m_EnchantValue;
	sNUMERICINFO_RANK			m_RankValue	;
	sNUMERICINFO_LEVEL		m_LevelValue	;
	sNUMERICINFO_PARTY		m_PartyValue	;
	sNUMERICINFO_NPC			m_NpcValue		;
	sNUMERICINFO_MAP			m_MapValue		;
	sNUMERICINFO_SPEED		m_SpeedValue	;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(NumericValueParser , _GAMELIB_API);
//extern _GAMELIB_API NumericValueParser& singleton::GetNumericValueParser();
#define theNumericValueParser  singleton::GetNumericValueParser()


#include "NumericValueParser.inl"

#endif //__NUMERICVALUEPARSER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemSlot.h
创建日期：2008年6月10日
最后更新：2008年6月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMSLOT_H__
#define __ITEMSLOT_H__
#pragma once


#include "ItemSlotDefine.h"
#include <ConstSlot.h>
#include <ConstItem.h>
#include <BaseSlot.h>
#include <ItemRank.h>
#include <ItemSocket.h>
#include "BaseSlotFactoryManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMELIB_API ItemSlot : public BaseSlot
{
	BASESLOT_DECLARE(ItemSlot);
public:
	ItemSlot(void);
	virtual ~ItemSlot(void);

public:
	ItemSlot	(const ITEM_STREAM & ItemStream);		//< 
	ItemSlot	(const ITEMOPT_STREAM & ItemStream);	//< 

	VOID Copy	(const ITEMOPT_STREAM &IN 	ItemStream);
	VOID Copy	(const ITEM_STREAM &IN 		ItemStream);
	VOID CopyOut(ITEMOPT_STREAM &OUT 			ItemStream);
	VOID CopyOut(ITEM_STREAM &OUT 				ItemStream);			// 可记 力寇

public:
	virtual VOID			Copy( BaseSlot & IN slot );
	virtual VOID			Clear();
	
	virtual eSLOTTYPE		GetSlotType	() const			;
	virtual SERIALTYPE	GetSerial	() const				;
	virtual SLOTCODE		GetCode		() const				;
	//virtual DURATYPE	GetDura		() const				;
	virtual BOOL			IsOverlap	();
	

	virtual VOID			SetSlotType	( eSLOTTYPE type )	{ __UNUSED(type);	}
	virtual VOID			SetSerial	( SERIALTYPE serial );
	//virtual VOID			SetDura		( DURATYPE dura );
	virtual VOID			SetCode		( SLOTCODE code );

public:
	BOOL						IsLocked		();
	VOID						SetLock		( BOOL val );
	sITEMINFO_BASE *		GetItemInfo	();
	DURATYPE					GetMaxNum	();
	
	
	BOOL						WithDura				();
	float						GetfDura				()	;
	VOID						SetfDura				(float fv );
	BYTE						GetMaxDura			();
	MONEY						GetPriceForRepair	();

protected:
	VOID						_InitfDura();

public:	
	static BOOL				IsOverlap( SLOTCODE code );

	BOOL						AppendItem			(ItemSlot& itemFrom, DWORD dwFlag);

	LPCSTR					GetName				();
	void						SetName				(LPCSTR szName);
	eATTACK_KIND			GetSeries			();
	void						SetSeries			(eATTACK_KIND type);
	DWORD						GetLimitSTR			(ePLAYER_TYPE CharType);
	DWORD						GetLimitDEX			(ePLAYER_TYPE CharType);
	DWORD						GetLimitVIT			(ePLAYER_TYPE CharType);
	DWORD						GetLimitINT			(ePLAYER_TYPE CharType);
	LEVELTYPE				GetLimitLevel		();
	MONEY						GetPriceForBuy		();
	MONEY						GetPriceForSell	();
	INT						GetAttackSpeed		();
	INT						GetMoveSpeed		();
	INT						GetAttSuccessRatio();
	INT						GetAttBlockRatio	();
	INT						GetPhyDef			();
	INT						GetRangeDef			();
	INT						GetMagicDef			();
	INT						GetMeleeAttack		(BOOL bMax);
	INT						GetRangeAttack		(BOOL bMax);
	INT						GetMagicAttack		(BOOL bMax);
	LEVELTYPE				GetDropLevel		();

	NORMAL_DECL		(Enchant,	BYTE);

	BOOL				IsDivine		();
	BOOL				IsBlocked	();

	NORMAL_DECL		(Set,				BYTE);
	NORMAL_DECL		(DamageLV,		BYTE);
	NORMAL_DECL		(PotentialUsed,BYTE);
	//NORMAL_DECL		(Name,			VRSTR);

	MODIFY_DECL		(Series,				BYTE);
	MODIFY_DECL		(LimitPTY,			SHORT);
	MODIFY_DECL		(LimitDEX,			SHORT);
	MODIFY_DECL		(LimitSTR,			SHORT);
	MODIFY_DECL		(LimitVIT,			SHORT);
	MODIFY_DECL		(LimitINT,			SHORT);
	MODIFY_DECL		(LimitLevel,		SHORT);
	MODIFY_DECL		(Speed,				SHORT);
	MODIFY_DECL		(AttackSpeed,		SHORT);
	MODIFY_DECL		(MoveSpeed		,	SHORT);
	MODIFY_DECL		(AttackRatio,		INT);
	MODIFY_DECL		(AttSuccessRatio,	INT);
	MODIFY_DECL		(AttBlockRatio	,	INT);
	MODIFY_DECL		(AttackMin,			INT);
	MODIFY_DECL		(AttackMax,			INT);
	MODIFY_DECL		(PhyAttackMin,		INT);
	MODIFY_DECL		(PhyAttackMax,		INT);
	MODIFY_DECL		(RangeAttackMin,	INT);
	MODIFY_DECL		(RangeAttackMax,	INT);
	MODIFY_DECL		(MagicAttackMin,	INT);
	MODIFY_DECL		(MagicAttackMax,	INT);
	MODIFY_DECL		(Defense,			INT);
	MODIFY_DECL		(PhyDef,				INT);
	MODIFY_DECL		(RangeDef,			INT);
	MODIFY_DECL		(MagicDef,			INT);


public:	
	/////////////////////////////////////////////////////////
	//以下为Rank信息存取
	eRANK							GetRank				();
	VOID							SetRank				( eRANK rank );
	VOID							SetRankAttr			( eRANK                rank
                                             , const sRANK_OPTION * pRankOption
															, INT						  nModify
															, BOOL					  bCreateInc=FALSE)		;
	eATTRIBUTE_TYPE					GetRankAttr			( eRANK rank );
	INT							GetRankAttrValue	( eRANK rank );
	const sRANK_OPTION *		GetRankAttrDesc	( eRANK rank );
	eRANK_LEVEL					GetRankLevel		();
	eRANK_OPTION_ITEM_TYPE	GetRankOptionItemType();

	const ItemRank&			GetRankInfo();
	eENCHANTLEVEL				GetEnchantLV		();

public:	
	/////////////////////////////////////////////////////////
	//以下为Potential信息存取
	ePOTENTIAL							GetPotential				();
	VOID									SetPotential				( ePOTENTIAL poten );
	VOID									SetPotentialAttr			(ePOTENTIAL           poten
                                                         ,const sRANK_OPTION * pPotentialOption
																			,INT						 nModify
																			,BOOL						 bCreateInc)		;
	void									SetPotentialAttrValue	( ePOTENTIAL poten,   INT nValue=FALSE );
	eATTRIBUTE_TYPE					GetPotentialAttr			( ePOTENTIAL poten );
	INT									GetPotentialAttrValue	( ePOTENTIAL poten );
	const sRANK_OPTION *				GetPotentialAttrDesc		( ePOTENTIAL poten );

	void									UpdatePotentialUsed		();		///更新潜隐激活信息


public:	
	/////////////////////////////////////////////////////////
	//以下为Socket信息存取
	BYTE							GetSocketNum		();
	VOID							SetSocketNum		( BYTE num ); 
	const sITEM_ATTR_INFO *	GetSocketAttrDesc	( eSOCKET sock );
	BOOL							ExistEmptySocket	( eSOCKET & OUT EmptySocket );
	VOID							SetSocketAttr		(eSOCKET							sock
															,const sITEM_ATTR_INFO *	pSocketOption);
	eATTRIBUTE_TYPE					GetSocketAttr		( eSOCKET sock ,eSOCKET_VALUE valueType);
	INT							GetSocketAttrValue( eSOCKET sock ,eSOCKET_VALUE valueType);
	INT							GetSocketValueType( eSOCKET sock ,eSOCKET_VALUE valueType);
	INT							GetSocketAttrIndex( eSOCKET sock ,eSOCKET_VALUE valueType);

	INT							GetSocketLimitLV	( eSOCKET sock);
	//BOOL						IsDoubleAttAttr	( eSOCKET sock, eSOCKET_VALUE valueType );
	//BOOL						IsDoubleDefAttr	( eSOCKET sock, eSOCKET_VALUE valueType);
	BOOL							CanUseSocketAttr	( eSOCKET sock, eSOCKET_VALUE valueType );
	BOOL							IsDoubleAttr		( eSOCKET sock);
	BOOL							IsMinMaxAttr		( eSOCKET sock);
	BOOL							IsRandAttr			( eSOCKET sock);
	INT							GetSocketAttrNum	( eSOCKET sock);
	BOOL							CopySocketInfo		( eSOCKET           sockTo
                                             , const ItemSlot& itemInfo
															, eSOCKET           sockFrom);

	const ItemSocket&			GetSocketInfo		();

	////////////////////////////////////

public:
	static eRANK_LEVEL		Rank2Level				(eRANK rank);
	static eRANK_LEVEL		GetCompRankLevel		(eRANK rank );
	static eENCHANTLEVEL		GetCompEnchantLV		(INT	 nLevel);

	/////////////////////////////////////////////////////////
private:	
	ITEM_STREAM *			_GetItemStream();
	VOID						_SetItemInfo();


protected:	
	sITEMINFO_MODIFY		m_Modify;
	ITEM_STREAM				m_ItemStream;
	sITEMINFO_BASE *		m_pItemInfo;
	BOOL						m_bLocked;

	ItemRank					m_RankInfo;
	ItemSocket				m_SocketInfo;
	VG_BOOL_PROPERTY		(ShopItem);
};

BASESLOT_FACTORY_DECLARE(ItemSlot, _GAMELIB_API);

#include "ItemSlot.inl"


#endif //__ITEMSLOT_H__
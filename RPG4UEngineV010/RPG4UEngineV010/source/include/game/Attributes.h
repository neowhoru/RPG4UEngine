/*////////////////////////////////////////////////////////////////////////
文 件 名：Attribute.h
创建日期：2008年6月28日
最后更新：2008年6月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ATTRIBUTE_H__
#define __ATTRIBUTE_H__
#pragma once

#include "AttributeDefine.h"


using std::vector;
typedef vector<AttributeValue*>		AttributeValueArray;

class AttributesImpl;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMELIB_API Attributes
{
public :
#ifdef USE_PAUSE
	typedef MinMaxBoundPolicy<MINUS_100, PULUS_100>					BoundPolicyMinusPlus100;
	typedef MinMaxBoundPolicy<MINUS_1000, PULUS_1000>				BoundPolicyMinusPlus1000;
	typedef MinMaxBoundPolicy<MINUS_10000, PULUS_10000>			BoundPolicyMinusPlus10000;
	typedef MinMaxBoundPolicy<MINUS_10000000, PULUS_10000000>	BoundPolicyMinusPlus10000000;

	typedef MinMaxBoundPolicy<ZERO_0, PULUS_100>						BoundPolicyZeroPlus100;
	typedef MinMaxBoundPolicy<ZERO_0, PULUS_1000>					BoundPolicyZeroPlus1000;
	typedef MinMaxBoundPolicy<ZERO_0, PULUS_10000>					BoundPolicyZeroPlus10000;
	typedef MinMaxBoundPolicy<ZERO_0, PULUS_10000000>				BoundPolicyZeroPlus10000000;


	typedef 	AttributeValueImpl<int, BoundPolicyZeroPlus10000000>	AttrValueINT_ZP10000000;
	typedef 	AttributeValueImpl<int, BoundPolicyMinusPlus10000000>	AttrValueINT_MP10000000;
	typedef 	AttributeValueImpl<int, BoundPolicyMinusPlus10000>		AttrValueINT_MP10000;
	typedef 	AttributeValueImpl<int, BoundPolicyMinusPlus1000>		AttrValueINT_MP1000;
	typedef 	AttributeValueImpl<int, BoundPolicyZeroPlus1000>		AttrValueINT_ZP1000;
#endif

public :
	Attributes();
	virtual ~Attributes();

public :
	VOID				Clear();
	
	AttributeValue&			operator [] (eATTRIBUTE_TYPE eAttrType)			;
	const AttributeValue&	operator [] (eATTRIBUTE_TYPE eAttrType) const	;

	BOOL					IsNULL(eATTRIBUTE_TYPE eAttrType) ;

public :
			  DWORD		GetFactoryType	() {return m_dwFactoryType;};
	virtual VOID		Update			() = 0;
	virtual VOID		UpdateEx			() = 0;

protected :
	BOOL				InitImpl		(DWORD dwType);
	void				ReleaseImpl	();
	VOID				RegisterAll	();
	VOID				RegisterAttr(eATTRIBUTE_TYPE eAttrType, AttributeValue* pAttrValue);

protected:

	AttributesImpl*		m_pImpl;
	AttributeValueArray	m_AttrValues;
	DWORD						m_dwFactoryType;


protected :
public :
//基本属性
	__DECLARE_ATTR_VALUE( STR,								SHORT, 	ATTRIBUTE_STR ); // 力量
	__DECLARE_ATTR_VALUE( DEX,  							SHORT, 	ATTRIBUTE_DEX ); // 敏捷
	__DECLARE_ATTR_VALUE( VIT,  							SHORT, 	ATTRIBUTE_VIT ); // 体质
	__DECLARE_ATTR_VALUE( INT,  							SHORT, 	ATTRIBUTE_INT ); // 智慧
	__DECLARE_ATTR_VALUE( SPR,  							SHORT, 	ATTRIBUTE_SPR ); // 精神
	__DECLARE_ATTR_VALUE( LRN,  							SHORT, 	ATTRIBUTE_LRN ); // 悟力
	__DECLARE_ATTR_VALUE( CRE,  							SHORT, 	ATTRIBUTE_CRE ); // 信用

	__DECLARE_ATTR_VALUE(Experty1,						SHORT,	ATTRIBUTE_EXPERTY1 ); // 
	__DECLARE_ATTR_VALUE(Experty2,						SHORT,	ATTRIBUTE_EXPERTY2 ); // 

	// HP, MP 
	__DECLARE_ATTR_VALUE( MaxHP,							int,		ATTRIBUTE_MAX_HP				); // 
	__DECLARE_ATTR_VALUE( MaxMP,							int,		ATTRIBUTE_MAX_MP				); // 
	__DECLARE_ATTR_VALUE( CurHP,							int,		ATTRIBUTE_CUR_HP				); // 
	__DECLARE_ATTR_VALUE( CurMP,							int,		ATTRIBUTE_CUR_MP				); // 
	__DECLARE_ATTR_VALUE( MaxHPPer,						int,		ATTRIBUTE_MAX_HP_PER			); // 
	__DECLARE_ATTR_VALUE( MaxMPPer,						int,		ATTRIBUTE_MAX_MP_PER			); // 
	__DECLARE_ATTR_VALUE( RecoverHP,						int,		ATTRIBUTE_RECOVERY_HP		); // 
	__DECLARE_ATTR_VALUE( RecoverMP,						int,		ATTRIBUTE_RECOVERY_MP		); // 
	__DECLARE_ATTR_VALUE( RecoverHPPer,					int,		ATTRIBUTE_RECOVERY_HP_PER	); // 
	__DECLARE_ATTR_VALUE( RecoverMPPer,					int,		ATTRIBUTE_RECOVERY_MP_PER	); // 

	// 命中 闪避
	__DECLARE_ATTR_VALUE(PhysicalAttackRate,			SHORT,	ATTRIBUTE_PHYSICAL_ATTACK_SUCCESS_RATIO	  ); // 
	__DECLARE_ATTR_VALUE(PhysicalAvoidRate,			SHORT,	ATTRIBUTE_PHYSICAL_ATTACK_BLOCK_RATIO	  ); // 
	__DECLARE_ATTR_VALUE(PhysicalAttackRatePer,		SHORT,	ATTRIBUTE_PHYSICAL_ATTACK_SUCCESS_PERCENT ); // 
	__DECLARE_ATTR_VALUE(PhysicalAvoidRatePer,		SHORT,	ATTRIBUTE_PHYSICAL_ATTACK_BLOCK_PERCENT	  ); // 

	// 攻速移速
	__DECLARE_ATTR_VALUE(MoveSpeedRatio,				int, 		ATTRIBUTE_MOVE_SPEED	  ); // 
	__DECLARE_ATTR_VALUE(AttSpeedRatio,					int,		ATTRIBUTE_ATTACK_SPEED  ); // 

	// 技能攻距
	__DECLARE_ATTR_VALUE(SkillRangeBonus,				int,		ATTRIBUTE_SKILL_ATTACK_RANGE  ); // 

	// 爆击 
	__DECLARE_ATTR_VALUE(PhysicalCriticalRatio,		int,		ATTRIBUTE_ADD_PHYSICAL_CRITICAL_RATIO  ); // 
	__DECLARE_ATTR_VALUE(MagicCriticalRatio,			int,		ATTRIBUTE_ADD_MAGICAL_CRITICAL_RATIO	  ); // 
	__DECLARE_ATTR_VALUE(CriticalRatioBonus,			int,		ATTRIBUTE_ADD_ALL_CRITICAL_RATIO		  ); // 
	__DECLARE_ATTR_VALUE(CriticalDamageBonus,			int,		ATTRIBUTE_ADD_CRITICAL_DAMAGE			  ); // 
	__DECLARE_ATTR_VALUE(CriticalDamagePercentBonus,int,		ATTRIBUTE_ADD_CRITICAL_DAMAGE_RATIO	  ); // 

	// 基本攻击
	__DECLARE_ATTR_VALUE(BaseMeleeMinAttPower,		int,		ATTRIBUTE_BASE_MELEE_MIN_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseMeleeMaxAttPower,		int,		ATTRIBUTE_BASE_MELEE_MAX_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseRangeMinAttPower,		int,		ATTRIBUTE_BASE_RANGE_MIN_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseRangeMaxAttPower,		int,		ATTRIBUTE_BASE_RANGE_MAX_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseTrapMinAttPower,			int,		ATTRIBUTE_BASE_TRAP_MIN_ATTACK_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BaseTrapMaxAttPower,			int,		ATTRIBUTE_BASE_TRAP_MAX_ATTACK_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BasePhy4MinAttPower,			int,		ATTRIBUTE_BASE_PHY4_MIN_ATTACK_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BasePhy4MaxAttPower,			int,		ATTRIBUTE_BASE_PHY4_MAX_ATTACK_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BasePhy5MinAttPower,			int,		ATTRIBUTE_BASE_PHY5_MIN_ATTACK_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BasePhy5MaxAttPower,			int,		ATTRIBUTE_BASE_PHY5_MAX_ATTACK_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BaseMagicMinAttPower,		int,		ATTRIBUTE_BASE_MAGICAL_MIN_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseMagicMaxAttPower,		int,		ATTRIBUTE_BASE_MAGICAL_MAX_ATTACK_POWER   ); // 

	// 攻击附加
	__DECLARE_ATTR_VALUE(OptionPhysicalAttPower,		int,		ATTRIBUTE_OPTION_PHYSICAL_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(OptionMagicAttPower,			int,		ATTRIBUTE_OPTION_MAGICAL_ATTACK_POWER	  ); // 
	__DECLARE_ATTR_VALUE(OptionAllAttPower,			int,		ATTRIBUTE_OPTION_ALL_ATTACK_POWER			  ); // 
																				
	__DECLARE_ATTR_VALUE(MagicalAllAttPower,			int,		ATTRIBUTE_MAGICAL_ALL_ATTACK_POWER		  ); // 
																
	// 基本防御												
	__DECLARE_ATTR_VALUE(BaseMeleeDefPower,			int,		ATTRIBUTE_BASE_MELEE_DEFENSE_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseRangeDefPower,			int,		ATTRIBUTE_BASE_RANGE_DEFENSE_POWER	  ); // 
	__DECLARE_ATTR_VALUE(BaseTrapDefPower,				int,		ATTRIBUTE_BASE_TRAP_DEFENSE_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BasePhy4DefPower,				int,		ATTRIBUTE_BASE_PHY4_DEFENSE_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BasePhy5DefPower,				int,		ATTRIBUTE_BASE_PHY5_DEFENSE_POWER		  ); // 
	__DECLARE_ATTR_VALUE(BaseMagicDefPower,			int,		ATTRIBUTE_BASE_MAGICAL_DEFENSE_POWER   ); // 

	__DECLARE_ATTR_VALUE(OptionPhysicalDefPower,		int,		ATTRIBUTE_OPTION_PHYSICAL_DEFENSE_POWER   ); // 
	__DECLARE_ATTR_VALUE(OptionMagicDefPower,			int,		ATTRIBUTE_OPTION_MAGICAL_DEFENSE_POWER	  ); // 
	__DECLARE_ATTR_VALUE(OptionAllDefPower,			int,		ATTRIBUTE_OPTION_ALL_DEFENSE_POWER		  ); // 
																
	__DECLARE_ATTR_VALUE(MagicalAllDefPower,			int,		ATTRIBUTE_MAGICAL_ALL_DEFENSE_POWER		  ); // 
	// 视野																
	__DECLARE_ATTR_VALUE(SightRange,						int,		ATTRIBUTE_SIGHT_RANGE  ); // 
																			
	__DECLARE_ATTR_VALUE(SkillAttackPower,				int,		ATTRIBUTE_ADD_SKILL_ATTACK_POWER  ); // 
	__DECLARE_ATTR_VALUE(SkillPercentDamage,			int,		ATTRIBUTE_ADD_SKILL_DAMAGE_RATIO  ); // 
																			
																			
	__DECLARE_ATTR_VALUE( WeaponMaster,					SHORT,	ATTRIBUTE_WEAPON_MASTER); // 
	__DECLARE_ATTR_VALUE( AttackIncRate,				SHORT,	ATTRIBUTE_ADD_ATTACK_INC_RATIO); // 
	__DECLARE_ATTR_VALUE( DefenseIncRate,				SHORT,	ATTRIBUTE_ADD_DEFENSE_INC_RATIO	); // 
	__DECLARE_ATTR_VALUE( SkillLevelAll,				SHORT,	ATTRIBUTE_INCREASE_SKILL_LEVEL); // 
	__DECLARE_ATTR_VALUE( AbsorbHP,						SHORT,	ATTRIBUTE_ABSORB_HP); // 
	__DECLARE_ATTR_VALUE( AbsorbMP,						SHORT,	ATTRIBUTE_ABSORB_MP); // 



#ifdef USE_PAUSE

	//  Att ( AttrName,   DataType, MinValue, MaxValue, UpdatePolicy )
	__DECLARE_ATTR_VALUE( STR, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 力量 
	__DECLARE_ATTR_VALUE( DEX, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 敏捷
	__DECLARE_ATTR_VALUE( VIT, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 体质
	__DECLARE_ATTR_VALUE( INT, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 智慧
	__DECLARE_ATTR_VALUE( SPR, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 精神
	__DECLARE_ATTR_VALUE( LRN, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 悟力
	__DECLARE_ATTR_VALUE( CRE, 						SHORT, 	BoundPolicyZeroPlus1000 );		// 信用

	// 
	__DECLARE_ATTR_VALUE( Experty1,					SHORT, 	BoundPolicyZeroPlus1000 );		// 职业点1
	__DECLARE_ATTR_VALUE( Experty2,					SHORT, 	BoundPolicyZeroPlus1000 );		// 职业点2

	// HP, MP
	__DECLARE_ATTR_VALUE( MaxHP,					int, 	BoundPolicyZeroPlus10000000 );	// 生命
	__DECLARE_ATTR_VALUE( MaxMP,					int, 	BoundPolicyZeroPlus10000000 );	// 精气
	__DECLARE_ATTR_VALUE( CurHP,					int, 	BoundPolicyZeroPlus10000000 );	// 当前生命
	__DECLARE_ATTR_VALUE( CurMP,					int, 	BoundPolicyZeroPlus10000000 );	// 当前精气
	__DECLARE_ATTR_VALUE( MaxHPPer,				int, 	BoundPolicyMinusPlus10000	);		// 生命
	__DECLARE_ATTR_VALUE( MaxMPPer,				int, 	BoundPolicyMinusPlus10000	);		// 精气
	__DECLARE_ATTR_VALUE( RecoverHP,				int, 	BoundPolicyMinusPlus10000	);		// 生命回
	__DECLARE_ATTR_VALUE( RecoverMP,				int, 	BoundPolicyMinusPlus10000	);		// 精气回
	__DECLARE_ATTR_VALUE( RecoverHPPer,			int, 	BoundPolicyMinusPlus10000	);		// 生命回%
	__DECLARE_ATTR_VALUE( RecoverMPPer,			int, 	BoundPolicyMinusPlus10000	);		// 精气回%

	// 命中闪避
	__DECLARE_ATTR_VALUE( PhysicalAttackRate,				SHORT,	BoundPolicyZeroPlus10000 );		// 命中率
	__DECLARE_ATTR_VALUE( PhysicalAvoidRate,				SHORT,	BoundPolicyZeroPlus10000 );		// 闪避率
	__DECLARE_ATTR_VALUE( PhysicalAttackRatePer,			SHORT,	BoundPolicyZeroPlus10000 );		// 命中率
	__DECLARE_ATTR_VALUE( PhysicalAvoidRatePer,			SHORT,	BoundPolicyZeroPlus10000 );		// 闪避率

	// 攻速移速
	__DECLARE_ATTR_VALUE_EX( MoveSpeedRatio,				int, 	BoundPolicyZeroPlus1000, DoNothingAttrPolicy );	// 100  %
	__DECLARE_ATTR_VALUE_EX( AttSpeedRatio,				int,	BoundPolicyZeroPlus1000, DoNothingAttrPolicy );	// 100  %

	////////
	__DECLARE_ATTR_VALUE( SkillRangeBonus,					int,	BoundPolicyZeroPlus1000 );

	////////
	__DECLARE_ATTR_VALUE( PhysicalCriticalRatio,			int,	BoundPolicyZeroPlus1000 );		// 
	__DECLARE_ATTR_VALUE( MagicCriticalRatio,				int,	BoundPolicyZeroPlus1000 );		// 
	__DECLARE_ATTR_VALUE( CriticalRatioBonus,				int,	BoundPolicyMinusPlus1000 );	// 
	__DECLARE_ATTR_VALUE( CriticalDamageBonus,			int,	BoundPolicyMinusPlus10000 );	// 
	__DECLARE_ATTR_VALUE( CriticalDamagePercentBonus,	int,	BoundPolicyMinusPlus1000 );	// 

	// 基本攻击
	__DECLARE_ATTR_VALUE( BaseMeleeMinAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseMeleeMaxAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseRangeMinAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseRangeMaxAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseTrapMinAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseTrapMaxAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BasePhy4MinAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BasePhy4MaxAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BasePhy5MinAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BasePhy5MaxAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseMagicMinAttPower,			int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseMagicMaxAttPower,			int,	BoundPolicyZeroPlus10000000 );

	// 攻击附加
	__DECLARE_ATTR_VALUE( OptionPhysicalAttPower,		int,	BoundPolicyMinusPlus10000 );
	__DECLARE_ATTR_VALUE( OptionMagicAttPower,			int,	BoundPolicyMinusPlus10000 );
	__DECLARE_ATTR_VALUE( OptionAllAttPower,				int,	BoundPolicyMinusPlus10000 );

	__DECLARE_ATTR_VALUE( MagicalAllAttPower,				int,	BoundPolicyZeroPlus10000000 );

	// 基本防御
	__DECLARE_ATTR_VALUE( BaseMeleeDefPower,				int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseRangeDefPower,				int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseTrapDefPower,				int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BasePhy4DefPower,				int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BasePhy5DefPower,				int,	BoundPolicyZeroPlus10000000 );
	__DECLARE_ATTR_VALUE( BaseMagicDefPower,				int,	BoundPolicyZeroPlus10000000 );

	////////
	__DECLARE_ATTR_VALUE( OptionPhysicalDefPower,		int,	BoundPolicyMinusPlus10000 );
	__DECLARE_ATTR_VALUE( OptionMagicDefPower,			int,	BoundPolicyMinusPlus10000 );
	__DECLARE_ATTR_VALUE( OptionAllDefPower,				int,	BoundPolicyMinusPlus10000 );

	__DECLARE_ATTR_VALUE( MagicalAllDefPower,				int,	BoundPolicyZeroPlus10000000 );

	////////
	__DECLARE_ATTR_VALUE( SightRange,						int,	BoundPolicyZeroPlus1000 );

	////////
	__DECLARE_ATTR_VALUE( SkillAttackPower,				int,	BoundPolicyZeroPlus10000 );
	__DECLARE_ATTR_VALUE( SkillPercentDamage,				int,	BoundPolicyZeroPlus1000 );

	////////
	__DECLARE_ATTR_VALUE( WeaponMaster,					SHORT,	BoundPolicyZeroPlus1000 );
	__DECLARE_ATTR_VALUE( AttackIncRate,				SHORT,	BoundPolicyZeroPlus1000 );
	__DECLARE_ATTR_VALUE( DefenseIncRate,				SHORT,	BoundPolicyZeroPlus1000 );
	__DECLARE_ATTR_VALUE( SkillLevelAll,				SHORT,	BoundPolicyZeroPlus1000 );
	__DECLARE_ATTR_VALUE( AbsorbHP,						SHORT,	BoundPolicyZeroPlus1000 );
	__DECLARE_ATTR_VALUE( AbsorbMP,						SHORT,	BoundPolicyZeroPlus1000 );
#endif

public :
	__DECLARE_ATTR_GROUP(MagicalAttackPower,			INT,		ATTRIBUTE_MAGICAL_GOLD_ATTACK_POWER,		ATTACKKIND_MAX);

	__DECLARE_ATTR_GROUP(BonusMinAttackPower,			INT,		ATTRIBUTE_ADD_MELEE_MIN_ATTACK_POWER,	ATTACKKIND_MAX);
	__DECLARE_ATTR_GROUP(BonusMaxAttackPower,			INT,		ATTRIBUTE_ADD_MELEE_MAX_ATTACK_POWER,	ATTACKKIND_MAX);

	__DECLARE_ATTR_GROUP(MagicalDefense,				INT,		ATTRIBUTE_MAGICAL_GOLD_DEFENSE_POWER,	ATTACKKIND_MAX);
	__DECLARE_ATTR_GROUP(BonusDefense,					INT,		ATTRIBUTE_ADD_ALL_DEFENSE_POWER,			ATTACKKIND_MAX);
	__DECLARE_ATTR_GROUP(ReduceDefenseRate,			INT,		ATTRIBUTE_DEL_ALL_TARGET_DEFENSE_RATIO,	ATTACKKIND_MAX);

	__DECLARE_ATTR_GROUP(BonusDamage,					INT,		ATTRIBUTE_ADD_ARMOR_HARD_DAMAGE,			eARMOR_TYPE_MAX);
	__DECLARE_ATTR_GROUP(BonusPercentDamage,			INT,		ATTRIBUTE_ADD_RATIO_ARMOR_HARD_DAMAGE,	eARMOR_TYPE_MAX);
	__DECLARE_ATTR_GROUP(ReduceDamage,					INT,		ATTRIBUTE_DEL_ALL_DAMAGE,					ATTACKKIND_MAX);

#ifdef USE_PAUSE
	int GetMagicalAttackPower(eATTACK_KIND attackType)		const	;

	int GetBonusMinAttackPower(eATTACK_KIND attackType)	const	;
	int GetBonusMaxAttackPower(eATTACK_KIND attackType)	const	;

	int GetMagicalDefense(eATTACK_KIND attackType)			const	;
	int GetBonusDefense(eATTACK_KIND attackType)				const	;
	int GetReduceDefenseRate(eATTACK_KIND attackType)		const	;

	int GetBonusDamage(eARMOR_TYPE armorType)					const	;
	int GetBonusPercentDamage(eARMOR_TYPE armorType)		const	;
	int GetReduceDamage(eATTACK_KIND attackType)				const	;

protected :
	AttrValueINT_ZP10000000		m_MagicalAttackPower		[ATTACKKIND_MAX];		// 魔法各系攻击

	AttrValueINT_MP10000000		m_BonusMinAttackPower[ATTACKKIND_MAX];	// 各系附加攻击Min
	AttrValueINT_MP10000000		m_BonusMaxAttackPower[ATTACKKIND_MAX];	// 各系附加攻击Max

	AttrValueINT_ZP10000000		m_MagicalDefense		[ATTACKKIND_MAX];		// 魔法防御
	AttrValueINT_MP10000			m_BonusDefense			[ATTACKKIND_MAX];			// 各系附加防御
	AttrValueINT_MP1000			m_ReduceDefenseRate	[ATTACKKIND_MAX];		// 各系防御减弱

	AttrValueINT_MP10000			m_BonusDamage			[eARMOR_TYPE_MAX];				// 
	AttrValueINT_ZP1000			m_BonusPercentDamage	[eARMOR_TYPE_MAX];		// 
	AttrValueINT_MP10000			m_ReduceDamage			[ATTACKKIND_MAX];			// 
#endif
};


#include "Attributes.inl"







#endif //__ATTRIBUTE_H__
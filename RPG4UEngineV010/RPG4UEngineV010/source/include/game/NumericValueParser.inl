/*////////////////////////////////////////////////////////////////////////
文 件 名：NumericValueParser.inl
创建日期：2008年4月22日
最后更新：2008年4月22日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NUMERICVALUEPARSER_INL__
#define __NUMERICVALUEPARSER_INL__
#pragma once


NUMERIC_FUNC_MINMAX	( FLOAT, PriceWeightForEnchant,	BYTE,	0, 0,		12,			m_EnchantValue );

NUMERIC_FUNC_MINMAX	( FLOAT, PriceWeightForRank,		eRANK,1, RANK_9,RANK_0,	m_RankValue );
NUMERIC_FUNC_MINMAX	( FLOAT, PriceWeightForRareRank,	eRANK,1, RANK_9,RANK_0,	m_RankValue  );

NUMERIC_FUNC_IMPL2	( FLOAT, PriceWeightForItemType, eITEM_TYPE, eARMOR_TYPE )
{
	if(	va > ITEMTYPE_INVALID && va < ITEMTYPE_TOPMAX
		&& m_ItemTypes[va])
	{
		if(type > eARMOR_SOFT || type < eARMOR_HARD)
			return 1.0f;
		INT nIndex = (INT)(type - eARMOR_HARD);
		if(m_ItemTypes[va]->m_Prices[nIndex] == 0.f)
			nIndex = 0;
		return m_ItemTypes[va]->m_Prices[nIndex];
	}
	return 1.0f;
}

NUMERIC_FUNC_MINMAX	( INT,	LimitValueForRank,				BYTE,			1, RANK_9,	RANK_0,	m_RankValue );
NUMERIC_FUNC_MINMAX	( INT,	WeaponEnchantWeightForEnchant,BYTE,			0, 0,			12,		m_EnchantValue );
NUMERIC_FUNC_MINMAX	( INT,	WeaponEnchantWeightForLV,		LEVELTYPE,	0, 0,			15,		m_LevelValue);
NUMERIC_FUNC_MINMAX	( INT,	ArmorEnchantWeightForEnchant,	BYTE,			0, 0,			12,		m_EnchantValue  );
NUMERIC_FUNC_MINMAX	( INT,	ArmorEnchantWeightForLV,		LEVELTYPE,	0, 0,			10,		m_LevelValue );
NUMERIC_FUNC_MINMAX	( BYTE,	DuraEnchantWeightForEnchant,	BYTE,			0, 0,			12,		m_EnchantValue );


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_IMPL2( INT,	LimitStrForWeapon,		ePLAYER_TYPE,	eITEM_TYPE )
{
	if(	type > ITEMTYPE_INVALID && type < ITEMTYPE_TOPMAX
		&& m_ItemTypes[type])
	{
		return m_ItemTypes[type]->m_LimitSTR[va];
	}
	return 0;
}

NUMERIC_FUNC_IMPL2( INT,	LimitDexForWeapon,		ePLAYER_TYPE,	eITEM_TYPE )
{
	if(	type > ITEMTYPE_INVALID && type < ITEMTYPE_TOPMAX
		&& m_ItemTypes[type])
	{
		return m_ItemTypes[type]->m_LimitDEX[va];
	}
	return 0;
}

NUMERIC_FUNC_IMPL2( INT,	LimitIntForWeapon,		ePLAYER_TYPE,	eITEM_TYPE )
{
	if(	type > ITEMTYPE_INVALID && type < ITEMTYPE_TOPMAX
		&& m_ItemTypes[type])
	{
		return m_ItemTypes[type]->m_LimitINT[va];
	}
	return 0;
}

//NUMERIC_FUNC_IMPL	( INT,	LimitStrForArmor,			ePLAYER_TYPE )
NUMERIC_FUNC_IMPL2( INT,	LimitStrForArmor,		ePLAYER_TYPE,	eITEM_TYPE )
{
	return GetLimitStrForWeapon(va,type);
}
NUMERIC_FUNC_IMPL2	( INT,	LimitDexForArmor,			ePLAYER_TYPE,	eITEM_TYPE )
{
	return GetLimitDexForWeapon(va,type);
}
NUMERIC_FUNC_IMPL2	( INT,	LimitIntForArmor,			ePLAYER_TYPE,	eITEM_TYPE )
{
	return GetLimitIntForWeapon(va,type);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NUMERIC_FUNC_MINMAX	( float, ExpLevelRatio,				int,		0, 0,			19,		m_LevelValue );
NUMERIC_FUNC_MINMAX	( float, AdditionalExpRatio,		DWORD,	0, 0,			10,		m_PartyValue  );
NUMERIC_FUNC_MINMAX	( float, ExpRankRatio,				BYTE,		0, 0,			5,			m_NpcValue );
NUMERIC_FUNC_MINMAX	( float, RoomBonusTypeRatio,		BYTE,		0, 0,			2,			m_MapValue );
NUMERIC_FUNC_MINMAX	( float, BonusAttackPower,			int,		0, 0,			10,		m_PartyValue   );

NUMERIC_FUNC_IMPL	( INT,	DamageForDivineWeapon,	eITEM_TYPE )
{
	if(	va > ITEMTYPE_INVALID && va < ITEMTYPE_TOPMAX
		&& m_ItemTypes[va+ DEFAULT_NUM])
	{
		return m_ItemTypes[va]->m_ExtraDamage;
	}
	return 1;
}

NUMERIC_FUNC_MINMAX	( float, StatusRatioAsNPCGrade,	int,		0, 0,			5,			m_NpcValue );
NUMERIC_FUNC_IMPL	( float, BaseMoveSpeedAsState,	int  )
{
	if(va >= 0 && va < MOVETYPE_MAX)
		return m_SpeedValue.m_BaseMoveSpeedAsState[va + DEFAULT_NUM] * SCALE_SPEED_FACTOR;
	return m_SpeedValue.m_BaseMoveSpeedAsState[DEFAULT_INDEX] * SCALE_SPEED_FACTOR;
}
NUMERIC_FUNC_MINMAX	( WORD,	MoveTimeAsState,			int,		0, 0,			MOVETYPE_MAX,	m_SpeedValue );



#endif //__NUMERICVALUEPARSER_INL__
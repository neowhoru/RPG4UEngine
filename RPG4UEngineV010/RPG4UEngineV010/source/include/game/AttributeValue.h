/*////////////////////////////////////////////////////////////////////////
文 件 名：AttributeValue.h
创建日期：2008年6月28日
最后更新：2008年6月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ATTRIBUTEVALUE_H__
#define __ATTRIBUTEVALUE_H__
#pragma once

#include "CommonDefine.h"
#include "AttributeValueDefine.h"


#define ATTRIBUTEVALUE_V(Name)\
	virtual IValue& Get##Name##Value()	= 0;\
	virtual const IValue& Get##Name##Value()const	= 0;

#define ATTRIBUTEVALUE_IMPL(Name,Check)\
	public:\
		IValue& Get##Name##Value()	{return m_##Name##ValueImpl;}\
		const IValue& Get##Name##Value()const		{return m_##Name##ValueImpl;};\
	protected:\
		NumValueImpl<ATTR,Check>		m_##Name##ValueImpl	;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMELIB_API AttributeValue 
{
public:
	//AttributeValue(){}
	//~AttributeValue(){}	

public:
	VOID	Init();
	VOID	Update();			

	IValue&			operator [] (ATTRIBUTE_KIND eAttr);
	const IValue&	operator [] (ATTRIBUTE_KIND eAttr) const;
public:
	virtual UINT	GetAttrIndex() = 0;
	virtual UINT	GetAttrNum	(){return 1;};

protected:
	ATTRIBUTEVALUE_V(Base);
	ATTRIBUTEVALUE_V(Calc);
	ATTRIBUTEVALUE_V(Item);
	ATTRIBUTEVALUE_V(Skill);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "AttributeValue.inl"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<UINT ATTR>
class AttributeValueImpl : public AttributeValue
{
public:
	//AttributeValueImpl();
	virtual UINT	GetAttrIndex(){return ATTR;};
	virtual UINT	GetAttrNum	(){return 1;};

protected:
	ATTRIBUTEVALUE_IMPL(Base,	TRUE);
	ATTRIBUTEVALUE_IMPL(Calc,	TRUE);
	ATTRIBUTEVALUE_IMPL(Item,	FALSE);
	ATTRIBUTEVALUE_IMPL(Skill,	FALSE);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
template<UINT ATTR,UINT ATTRNUM>
class AttributeValueArrayImpl : public AttributeValueImpl<ATTR>
{
public:
	AttributeValueArrayImpl():m_ValueIndex(0){}

	virtual UINT	GetAttrIndex(){return ATTR + m_ValueIndex;}
	virtual UINT	GetAttrNum	(){return ATTRNUM;}

	UINT	m_ValueIndex;
};



#ifdef USE_PAUSE
//----------------------------------------------------------------------------------------------------
template<typename Type, class BoundCheckPolicy, class UpdatePolicy=AttrSumPolicy >
class AttributeValueImpl : public AttributeValue
{
public:
	AttributeValueImpl()		{ Init(); }
	~AttributeValueImpl()	{}	

	virtual VOID	Init()
	{
		m_CalcValue		= 0;
		m_BaseValue		= 0;
		m_ItemValue		= 0;
		m_SkillValue	= 0;		
	}

	virtual IValue&	operator [] (ATTRIBUTE_KIND eAttr)
	{
		assert( eAttr < ATTRIBUTE_KIND_MAX );
		switch (eAttr)
		{
			case ATTRIBUTE_KIND_BASE :	return m_BaseValue;
			case ATTRIBUTE_KIND_ITEM :	return m_ItemValue;
			case ATTRIBUTE_KIND_SKILL :	return m_SkillValue;
		}		
		return m_CalcValue;			
	}
	
	virtual const IValue&	operator [] (ATTRIBUTE_KIND eAttr) const
	{
		assert( eAttr < ATTRIBUTE_KIND_MAX );
		switch (eAttr)
		{
			case ATTRIBUTE_KIND_BASE :	return m_BaseValue;
			case ATTRIBUTE_KIND_ITEM :	return m_ItemValue;
			case ATTRIBUTE_KIND_SKILL :	return m_SkillValue;
		}		
		return m_CalcValue;	
	}

	virtual VOID Update()			
	{
		UpdatePolicy up;
		up.Update( *this );		
	}

private:
	NumValue<Type,BoundCheckPolicy>		m_CalcValue;		// 
	NumValue<Type,BoundCheckPolicy>		m_BaseValue;		// 
	NumValue<Type>								m_ItemValue;		// 
	NumValue<Type>								m_SkillValue;		// 
};

#endif



#endif //__ATTRIBUTEVALUE_H__
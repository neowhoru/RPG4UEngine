/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemSocketDefine.h
创建日期：2008年6月26日
最后更新：2008年6月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMSOCKETDEFINE_H__
#define __ITEMSOCKETDEFINE_H__
#pragma once


#include "ConstItem.h"
#include <ItemAttrInfoParserDefine.h>

struct sSOCKET_ATTR_VALUE
{
	eSOCKET_TYPE	m_SocketType;
	INT				m_nAttrInfoIndex;
	LEVELTYPE		m_LimitLevel;
	sITEMINFO_BASE*	m_pItemInfo;

	union
	{
		struct
		{
			INT							m_nAttAttrIndex;	///攻属性
			INT							m_nAttAttrType;	///攻属性值类型
			INT							m_nAttAttrValue;	///攻属性值

			INT							m_nAttAttrIndex2;	///攻属性
			INT							m_nAttAttrType2;	///攻属性值类型
			INT							m_nAttAttrValue2;	///攻属性值2

			INT							m_nDefAttrIndex;	///防属性
			INT							m_nDefAttrType;	///防属性值类型
			INT							m_nDefAttrValue;	///防属性值

		}DOUBLE;
		struct
		{
			INT							m_nMainAttrIndex;	///主属性
			INT							m_nMainAttrType;	///主属性值类型
			INT							m_nMainAttrValue;	///主属性值

			INT							m_nRandAttrIndex;	///随机属性
			INT							m_nRandAttrType;	///随机属性值类型
			INT							m_nRandAttrValue;	///随机属性值

			INT							m_nRandAttr2Index;///随机属性2
			INT							m_nRandAttr2Type;	///随机属性2值类型
			INT							m_nRandAttr2Value;///随机属性2值

			sITEM_RANDATTR_UNIT*		m_pRandAttr;
			sITEM_RANDATTR_UNIT*		m_pRandAttr2;
		}RANDOM;
	};

	union
	{
		sITEM_DOUBLE_ATTR_INFO*	m_pDoubleAttr;
		sITEM_RANDATTR_INFO*		m_pRandAttr;
		sITEM_ATTR_INFO*			m_pAttrInfo;
	};

#ifdef USE_SUNITEM//x
	eSOCKET_LEVEL	m_SocketLevel;
	INT				m_AttrIndex;
	eATTRIBUTE_TYPE		m_AttrType;
	INT				m_AttrValue;
	const sSOCKET_OPTION * m_pSocketOption;
#endif

};



#endif //__ITEMSOCKETDEFINE_H__
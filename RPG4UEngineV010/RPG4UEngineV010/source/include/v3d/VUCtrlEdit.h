/*////////////////////////////////////////////////////////////////////////
文 件 名：VUCtrlEdit.h
创建日期：2006年10月30日
最后更新：2006年10月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUCTRLEDIT_H__
#define __VUCTRLEDIT_H__
#pragma once


#include "CommonDefine.h"
#include "VUCtrlObject.h"
#include "VUCtrlScrollBar.h"

const DWORD EDIT_ROW_MAX =	100;

using namespace std;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API VUCtrlEdit : public VUCtrlObject
{
public:
	VUCtrlEdit(void);
	~VUCtrlEdit(void);

public:
	typedef void (*PROC_ONENTER)	( VUCtrlObject* pSender, LPCSTR szData );
	typedef void (*PROC_ONTAB)		( VUCtrlObject* pSender, LPCSTR szData);
	typedef void (*PROC_ONUPDATE)	( VUCtrlObject* pSender, LPCSTR szData);

public:
	virtual BOOL GetObjectType		(){return Type_Edit;}
	////////////////////////////////////////////////////////////////////////
	BOOL InitUIData(VUCtrlObject* pParent, sUIINFO_BASE* pData );



	////////////////////////////////////////////////////////////////////////
	BOOL Render			(DWORD dwTick);

	////////////////////////////////////////////////////////////////////////
	BOOL OnChar				( UINT nChar );
	BOOL OnKeyDown			( UINT nChar );

	////////////////////////////////////////////////////////////////////////
	BOOL OnMouseMove		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDown	( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonUp		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDBClick( DWORD dwFlags, INT nMouseX, INT nMouseY );


public:
	////////////////////////////////////////////////////////////////////////
	void		SetGlobalRect	(const RECT* prc );
	BOOL		IsEnableFocus		();

	void		SetText		( LPCSTR strText );
	void		SetText		( int		nValue );
	LPCSTR	GetText		();
	void		SetTextAlign( E_AlignMode		nAlign );

	void		SetRTFText	(LPCSTR  szModeText
								,COLOR	colText = COLOR_ARGB(200,255,0,255));
	LPCSTR	GetRTFText	()  ;

	void		SetIsPassword	( BOOL b );
	void		SetIsNumber		( BOOL b ) ;
	void		SetReadOnly		( BOOL b );
	BOOL		IsReadOnly		();
	void		SetMultiline	( BOOL b );
	BOOL		IsMultiline		();

public:
	const string&	operator=( const string& stText );
	const int&		operator=( const int& nNum );

private:
	void _OnEnterEvent	( LPCSTR szData );
	BOOL _OnTabEvent		( LPCSTR szData );	//表示回掉函数调用 return消息
	void _OnUpdateEvent	( LPCSTR szData );
	int  _GetRowLength	();
	int  _GetRowCNCount	( const unsigned int nDstIndex );
	void _AddChar			( BYTE byChar );
	void _DelChar			( BOOL bBack  );

private:
	///////////////////////////////////////////////
	VG_TYPE_PROPERTY		(ProcOnEnter,	PROC_ONENTER);
	VG_TYPE_PROPERTY		(ProcOnTab,		PROC_ONTAB);
	VG_TYPE_PROPERTY		(ProcOnUpdate,	PROC_ONUPDATE);
	VG_INT_PROPERTY		(MaxLength);
	VG_INT_PROPERTY		(MaxCount);		//允许输入的最大字符数量
	VG_TYPE_PROPERTY		(DefaultColor,	COLOR);

	char*		m_szText;
	int		m_nBufferSize;
	int		m_nTextLength;
	int		m_nColCount,
				m_nLineCount;

	int		m_RowIndices[EDIT_ROW_MAX];
	int		m_nRowAmount;
	DWORD		m_dwDrawFlags;
	int		m_nStartAt;
	BOOL		m_bSelectAll;
	COLOR		m_colorSelection;

	int		m_nCursorIndex;

	int		m_nRTFCurrentIndex;	//如果有模式text　光标最小处为RTFText
	COLOR		m_colorRTFText;		//模式text颜色
	string	m_strRTFText;			//模式text例如　 "x说:你好！" 中　"x说:" 为模式文字不可编辑
	int		m_nTextHideSize;		//记录下 m_szText 前隐藏字符个数 (当m_szText超出输入框宽度时)

	VUCtrlScrollBar	m_UIScroll;

	string		m_strTextWhole;	//包括RTFText部分 和 m_szText部分


};


#include "VUCtrlEdit.inl"


#endif //__VUCTRLEDIT_H__


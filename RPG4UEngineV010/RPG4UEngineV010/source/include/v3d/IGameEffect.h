/*////////////////////////////////////////////////////////////////////////
文 件 名：IGameEffect.h
创建日期：2007年2月17日
最后更新：2007年2月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __IGAMEEFFECT_H__
#define __IGAMEEFFECT_H__
#pragma once

#include "GameEffectDefine.h"

class IGameEffectHelper;
class IGameEffectEmitter;
class IGameEffectQuoter;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VR3DDLL_API IGameEffect
{
public:
	virtual DWORD	 GetFactoryType() = 0;

	///////////////////////////////////////
	virtual void	SetID		( int nID ) = 0;
	virtual int		GetID		() = 0;
	virtual void	Update	( DWORD dwTime ) = 0;
	virtual void	Render	() = 0;
	virtual void	Destroy	() = 0;
	virtual BOOL	IsDead	( DWORD dwTime ) = 0;

	///////////////////////////////////////
	virtual void SetPos		( float /*x*/, float /*y*/, float /*z*/ ) {}
	virtual BOOL IsAction	(int /*nAction*/){return FALSE;}
	virtual void SetMaster	(IGameEffectHelper*){}
	virtual void Start		(){}

	///////////////////////////////////////
	virtual void	SetFlyTime			(DWORD /*dwTime*/){}
	virtual void	SetScale				(float /*fScale*/){}
	virtual void	SetDead				(){}
	virtual void	SetStartTime		(DWORD /*dwTime*/){}
	virtual void	SetLife				(DWORD dwLifeTime){__UNUSED(dwLifeTime);}

	virtual DWORD	GetDelay				(){return 0;}
	virtual float	GetRadius			(){return 0.f;}
	virtual int		GetInstanceNum		(){return 0;}
	virtual int		GetTerminateMode	(){return 0;}
	virtual DWORD	GetLife				(){return 0;}

	///////////////////////////////////////
	virtual gameeffect::Instance* AllocInstance	(){return NULL;}
	virtual gameeffect::Instance* GetInstance		( int /*nInst*/ ){return NULL;}

	///////////////////////////////////////
	virtual BOOL	LoadFromMemory	( BYTE* /*pbyBuffer*/,		int /*nBufferSize*/ ){return FALSE;}
	virtual BOOL	LoadSetting		( LPCSTR /*szFileName*/,	BOOL bCreate= TRUE ){bCreate;return FALSE;}
	virtual BOOL	Compile			( LPCSTR /*szFileName*/,	BYTE** /*ppbyBuffer*/, int* /*pnBufferSize*/ ){return FALSE;}
	virtual BOOL	ConvertToBin	( LPCSTR /*szFileName*/,	BYTE** /*ppbyBuffer*/, int* /*pnBufferSize*/ ){return FALSE;}

	virtual char*	GetSoundPath	(){return NULL;}
	virtual char*	GetModelPath	(){return NULL;}


	///////////////////////////////////////
	virtual IGameEffectQuoter* GetEffectQuoter() = 0;
	virtual void					SetEffectQuoter( IGameEffectQuoter* /*pEffectQuoter*/ ) = 0;
};





#endif //__IGAMEEFFECT_H__
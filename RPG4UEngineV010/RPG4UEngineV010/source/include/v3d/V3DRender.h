/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DRender.h
创建日期：2008年11月10日
最后更新：2008年11月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DRENDER_H__
#define __V3DRENDER_H__
#pragma once

#include "V3DVertexDefine.h"
#include "V3DRendererDefine.h"
#include "LightDefine.h"

using namespace std;
typedef vector<VERTEX3D_UV>	V3DVerticesBuffer;

const DWORD WORLD_STACK_MAX  = 32;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _V3DGRAPHIC_API V3DRender
{
public:
	V3DRender();
	~V3DRender();

public:
   //void beginToBuffer( GFXPrimitiveType type, DWORD maxVerts );
   //GFXVertexBuffer *endToBuffer( DWORD &outNumPrims );

   void Begin	(ePRIMITIVE_TYPE type
               ,DWORD           maxVerts
					,DWORD           dwFlag=RENDER_LIGHT_DISABLE|RENDER_BOTH_SIDE
					,INT             nTextureID=-1);
   void End		();

   void Vertex2F( REAL x, REAL y );
   void Vertex3F( REAL x, REAL y, REAL z );

   void Vertex2FV( const REAL *data );
   void Vertex2FV( const Vector2D &vPos ) ;;
   void Vertex2FV( const Vector2D *vPos ) ;;

   void Vertex3FV( const REAL *data );
   void Vertex3FV( const Vector3D &vPos ) ;;
   void Vertex3FV( const Vector3D *vPos ) ;;

   void Vertex2I	( INT x, INT y ) ;
   void Vertex3I	( INT x, INT y, INT z ) ;

   void Color( const COLOR & );
   void Color( const COLOR4F & );
   void Color3I( BYTE red, BYTE green, BYTE blue );
   void Color4I( BYTE red, BYTE green, BYTE blue, BYTE alpha );
   void Color3F( REAL red, REAL green, REAL blue );
   void Color4F( REAL red, REAL green, REAL blue, REAL alpha );

   void TexCoord2F( REAL x, REAL y );

   //void shutdown();

public:
	void UpdateStates(bool bForceSet=FALSE);

public:
   /// @name Matrix interface
   /// @{

   void SetWorldMatrix	( Matrix4C &newWorld );

   Matrix4C &GetWorldMatrix() const;

   void PushWorldMatrix	();
   void PopWorldMatrix	();
   void MulWorld			( Matrix4C &mat );

   //void setProjectionMatrix( Matrix4C &newProj );
   //Matrix4C &getProjectionMatrix() const;

   //void setViewMatrix( Matrix4C &newView );
   //Matrix4C &getViewMatrix() const;

   /// @}


protected:
	V3DVerticesBuffer		m_VertTempBuf;
	ePRIMITIVE_TYPE		m_PolyType;
	DWORD						m_VertexIndex;
	COLOR						m_VertexColor;
	Vector2D					m_TexCoord;
	DWORD						m_dwFlag;
	INT						m_nTextureID;


   BOOL	m_bStateDirty;     

   /// @name Matrix managing variables
   /// @{
   ///
   Matrix4 m_matWorlds[WORLD_STACK_MAX];
   BOOL    m_bWorldMatrixDirty;
   INT     m_nWorldStackSize;

   Matrix4 m_matProjection;
   BOOL    m_bMatrixProjDirty;

   Matrix4 m_matView;
   BOOL    m_bViewMatrixDirty;

   //Matrix4 mTextureMatrix[TEXTURE_STAGE_COUNT];
   //bool    mTextureMatrixDirty[TEXTURE_STAGE_COUNT];
   //bool    mTextureMatrixCheckDirty;
   /// @}

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(V3DRender , _V3DGRAPHIC_API);
//extern _V3DGRAPHIC_API V3DRender& singleton::GetV3DRender();
#define theV3DRender  singleton::GetV3DRender()


#include "V3DRender.inl"

#endif //__V3DRENDER_H__
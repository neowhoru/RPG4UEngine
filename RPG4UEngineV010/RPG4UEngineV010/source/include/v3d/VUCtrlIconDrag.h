/*////////////////////////////////////////////////////////////////////////
文 件 名：VUCtrlIconDrag.h
创建日期：2006年11月2日
最后更新：2006年11月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUCTRLICONDRAG_H__
#define __VUCTRLICONDRAG_H__
#pragma once


#include "CommonDefine.h"
#include "IVUIIconInfo.h"
#include "VUCtrlObject.h"
#include "VUCtrlscrollbar.h"
#include "VUIconDragListImg.h"

_NAMESPACE(Timer,util);

class IVUIIconInfo;
class BaseSlot;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API VUCtrlIconDrag : public VUCtrlObject
{
public:
	VUCtrlIconDrag(void);
	virtual ~VUCtrlIconDrag(void);


public:
	///////////////////////////////////////////
	typedef BOOL (*PROC_ONBUTTON_CLICK)	(VUCtrlObject*		pSender
													,IconDragListImg*	pItem );

	typedef BOOL (*PROC_ONDRAGFROM)	(VUCtrlObject*    pSender
                                    ,VUCtrlObject*    pThis
												,IconDragListImg* pItemDrag
												,IconDragListImg* pItemFrom);

	typedef BOOL (*PROC_ONDROPTO)		(VUCtrlObject*    pSender
                                    ,VUCtrlObject*    pThis
												,IconDragListImg* pItemDrag
												,IconDragListImg* pItemDest);

	typedef BOOL (*PROC_ONLBUTTON_DBCLICK)	(VUCtrlObject*    pSender
														,IconDragListImg* pItem)		;

	typedef BOOL (*PROC_ONRBUTTON_UP)		(VUCtrlObject*    pSender
														,IconDragListImg* pItem)		;

public:	
	///////////////////////////////////////////
	BOOL FrameMove				(DWORD dwTick);

	///////////////////////////////////////////
	BOOL OnMouseWheel			( BOOL  bUp,	  INT nMouseX, INT nMouseY );
	BOOL OnMouseMove			( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDown		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonUp			( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnRButtonUp			( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDBClick	( DWORD dwFlags, INT nMouseX, INT nMouseY );

public:
	///////////////////////////////////////////
	virtual int	 GetIconSize	() = 0;
	virtual void GetIconMargin	(INT& x,INT& y) = 0;
	virtual BOOL GetTilePt		( int nPosX, int nPosY, POINT *ptTile )	= 0;
	virtual BOOL GetTileRect	( int nPosX, int nPosY, RECT *ptRect)		= 0;
	virtual BOOL CanPicking		() = 0;
	virtual void SetCanPicking	(BOOL b ) = 0;

	///////////////////////////////////////////
	BOOL GetItemRect	( int nIndex,  RECT *ptRect);

public:
	///////////////////////////////////////////
	BOOL OnDragFrom	(int              nPosX
                     ,int              nPosY
							,IconDragListImg* pItem
							,VUCtrlObject*    pSender);

	BOOL OnDropTo		(int              nPosX
                     ,int              nPosY
							,IconDragListImg* pItem
							,VUCtrlObject*    pSender);

	BOOL OnEnterItem	(int              nPosX
                     ,int              nPosY
							,IconDragListImg* pItem);

	BOOL OnLeaveItem	(int              nPosX
                     ,int              nPosY
							,IconDragListImg* pItem);


	void ForceDragFrom	(INT nIndex);
	void ForceDropTo		(INT nIndex);
	void ShowToolTip		(INT nIndex, DWORD dwLockKey=0);


public:
	///////////////////////////////////////////
	void	Clear				( BOOL bReset = FALSE );
	// 移除指定坐标项
	BOOL RemoveItemBy		( int nItemID,  int nCount );
	BOOL RemoveItemAt		( int nIndex );

	BOOL SetItem			(IconDragListImg* pItem
                        ,int					nIndex = -1
								,BOOL					bReverse =    FALSE);

	INT InsertItem			(IN IconDragListImg*	pItem
                        ,INT						nIndex	= -1
								,BOOL						bReverse	=  FALSE);

	// 取得项
	int					GetEmptyItem	( BOOL bReverse = FALSE );
	IconDragListImg*	GetItem			( UINT nPosX, UINT nPosY );
	IconDragListImg*	GetItemAt		( UINT nIndex );
	IconDragListImg*	GetItemBy		( int nID );
	IconDragListImg*	GetCurSelItem	();
	void					SetCurSelItem	( UINT nPosX, UINT nPosY=0);
	int					GetItemIndex	( UINT nPosX, UINT nPosY );
	int					GetItemIndex	(const  IconDragListImg *pItem );
	int					GetItemCount	()	;


	/////////////////////////////////////////////////////////////
	void		SetSpareTime		( int nIndex, int nTime);
	void		SetScrollVisable	( BOOL bShow = TRUE );
	void		SetScrollBarEnable( BOOL bEnable = TRUE)		;
	
protected:
	/////////////////////////////////////////////////////////////
	void _InitItemData			();
	int  _Refresh					();
	BOOL _ForceBeginDrag			(INT nIndex, BOOL bClear);
	BOOL _OnLButtonDBClickEvent(int              nPosX
                              ,int              nPosY
										,IconDragListImg* pItem
										,VUCtrlObject*    pSender);

public:
	/////////////////////////////////////////////////////////////
	static void		SetToolTipDelay(DWORD dwDelay);;
	static DWORD	ms_dwToolTipDelay;

protected:
	IconListImgArray		m_IconItems;

	VG_TYPE_PROPERTY		(ProcOnDragFrom,			PROC_ONDRAGFROM);
	VG_TYPE_PROPERTY		(ProcOnDropTo,				PROC_ONDROPTO);
	VG_TYPE_PROPERTY		(ProcOnLButtonDBClick,	PROC_ONLBUTTON_DBCLICK);
	VG_TYPE_PROPERTY		(ProcOnRButtonUp,			PROC_ONRBUTTON_UP);
	VG_TYPE_PROPERTY		(ProcOnButtonClick,		PROC_ONBUTTON_CLICK);

	VG_BOOL_PROPERTY		(ShopType);
	VG_BOOL_PROPERTY		(ShowAllInfo);
	VG_INT_PROPERTY		(StartIndex);
	VG_BOOL_PROPERTY		(PickingDisabled);

	int						m_nColShowMaxCount;
	int						m_nRowShowMaxCount;
	int						m_nMaxHeightCount;
	POINT						m_ptCurrentSelect;
	VG_TYPE_PROPERTY		(MouseOverPoint, POINT);

	VUCtrlScrollBar		m_UIScroll;
	COLOR						m_colorSelection;
	COLOR						m_colorMouseOver;

	DWORD						m_dwToolTipTimer;
	BOOL						m_bCanShowToolTip;

	IconDragListImg*		m_pListImgMouseOver;
	VG_PTR_PROPERTY		(SlotContainer, void);
	VG_UINT_PROPERTY		(SlotStartIndex);
	VG_BOOL_PROPERTY		(GroupContainer);		///通过群组方式，将多个独立的ListImg关联同1个Conatiner


};//class _VRUIDLL_API VUCtrlIconDrag : public VUCtrlObject


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "VUCtrlIconDrag.inl"

#endif //__VUCTRLICONDRAG_H__
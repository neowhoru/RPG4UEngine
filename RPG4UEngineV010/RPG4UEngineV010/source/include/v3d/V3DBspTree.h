/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DBspTree.h
创建日期：2007年5月20日
最后更新：2007年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DBSPTREE_H__
#define __V3DBSPTREE_H__
#pragma once


#include "V3DBspTreeDefine.h"
#include "V3DPrimitive.h"
#include "TMemoryPoolFactory.h"


class IMeshModel;

class IV3DRenderable;
class V3DHugeModel;

using namespace std;
using namespace util;


typedef vector<VERTEX3D_PT>				VertexArray;
typedef vector<sBSP_FACE>					FaceArray;
typedef TMemoryPoolFactory<sBSP_NODE>	BspNodePoolFactory;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DBspTree
{
public:
	V3DBspTree();
	virtual ~V3DBspTree();

public:
	BOOL Create	(DWORD dwPoolSize = 100);
	void Destroy();

	BOOL	Render			( DWORD dwTick);
	INT	Render			( sBSP_NODE* node );
	void	RenderBBox		( sBSP_NODE* node, DWORD dwColor );

public:
	BOOL CreateByMesh		( LPCSTR				pszFilename );
	BOOL CreateByMesh		( IMeshModel*		pMeshModel );
	BOOL CreateByHugeMesh( V3DHugeModel*	pHugeModel );
	
public:
	/////////////////////////////////////////////
	BOOL CreateBspTree	();

	BOOL AddMesh			(float* pVertices
                        ,int    nVertexCount
								,short* pFaces
								,int    nFaceCount);
	BOOL AddRenderable	(IV3DRenderable* pRenderable );



protected:
	void _RemoveNode	( sBSP_NODE**	ppNode );
	void _ResetNode	( sBSP_NODE*	pNode );

	void _GetAABB		(vector<int>&	arFaceIDs
                     ,V3DVector&		vMin
							,V3DVector&		vMax);

	void _SplitFace	(int		nFaceID
                     ,float	fSplit
							,int		nAxisType
							,int*		pLeftCount
							,int*		pRightCount
							,int*		pBothCount);

	void _SplitSpace	(sBSP_NODE* pRootNode
                     ,int        nDepth
							,int        nAxisType);

	void CullFaceEx	(const V3DFrustum*   pFrustum
                     ,sBSP_NODE*          pNode
							,const VECTOR3D& 		vPos
							,float           		fOutOfRangeDistance
							,int             		nFaceIds[]
							,int*            		pnFaceCount);			

	//////////////////////////////////////
	int _GetFace		(sBSP_NODE* pNode
                     ,short*     pIndexBuffer)		;

	void _GetMaxDepth	(sBSP_NODE* pNode
                     ,int*       pDepth)		;

	void	_CameraCorrect	( Ray&	ray );
	int	_CameraCorrect	( sBSP_NODE* node, Ray&	ray );

public:
	//////////////////////////////////////
	int BuildVisibleList	(const V3DFrustum*   pFrustum
                        ,const VECTOR3D&		vPos
								,float					fOutOfRangeDistance);


	/////////////////////////////////////////////
	BOOL RayIntersect		(sBSP_NODE*       node
                        ,const V3DVector& pos
								,const V3DVector& dir);

	BOOL RayIntersect		(sBSP_NODE*			node
                        ,const Ray&			ray)		;



	
	///////////////////////////////////////////////////////////
	int					GetIntersects			(sBSP_NODE*			node
                                          ,const VECTOR3D&	pos
														,const VECTOR3D&	dir
														,sBSP_INTERSECT*	pIntersects
														,BOOL					bNormal =  FALSE);

	int					GetIntersects			(sBSP_NODE*			node
                                          ,const Ray&			ray
														,sBSP_INTERSECT*	pIntersects
														,BOOL					bNormal =  FALSE);

	///////////////////////////////////////////////////////////
	sBSP_INTERSECT*	GetNearestIntersect	(const VECTOR3D&	vPos
                                          ,const VECTOR3D&	vDir
														,BOOL					bNormal =  FALSE);

	sBSP_INTERSECT*	GetNearestIntersect	(const Ray&			ray
														,BOOL					bNormal =  FALSE);


	///////////////////////////////////////////////////////////
	int					GetCameraIntersects		(sBSP_NODE*       node
                                             ,const V3DVector& pos
															,const V3DVector& dir
															,sBSP_INTERSECT*  pIntersects);

	int					GetCameraIntersects		(sBSP_NODE*      node
                                             ,const Ray&      ray
															,sBSP_INTERSECT* pIntersects);

	///////////////////////////////////////////////////////////
	sBSP_INTERSECT*	GetCameraNearestIntersect	(const V3DVector& vPos
                                                ,const V3DVector& vDir)		;

	sBSP_INTERSECT*	GetCameraNearestIntersect	(const Ray&	ray)		;

	///////////////////////////////////////////////////////////
	sBSP_INTERSECT*	GetNearestIntersectCmpZ	(const V3DVector& vPos
                                             ,const V3DVector& vDir
															,BOOL             bNormal
															,float            fCurrZ);
	sBSP_INTERSECT*	GetNearestIntersectCmpZ	(const Ray&	ray
															,BOOL       bNormal
															,float      fCurrZ);


	///////////////////////////////////////////
	BOOL Transform		(float				x
                     ,float				y
							,float				z
							,float				fScale
							,sBSP_NODE*			pNode);

	BOOL Transform		(const VECTOR3D&	vPos
							,float				fScale
							,sBSP_NODE*			pNode);

public:
	///////////////////////////////////////////////
	vector<BOOL>			m_FaceStates;

	int						m_nMaxDepth;
	int						m_nMinNodeLeafCount;
	IMeshModel*				m_pMeshModel;

	int*						m_pRenderFaceIDs;

	int						m_nNumRenderFace;
	BspNodePoolFactory*	m_pNodePool;

	VG_TYPE_GET_PROPERTY	(Vertices,	VertexArray);
	VG_TYPE_GET_PROPERTY	(Faces,		FaceArray);

	VG_PTR_GET_PROPERTY	(RootNode, sBSP_NODE);
	VG_FLOAT_PROPERTY		(MinLeafSize);
};




#endif //__V3DBSPTREE_H__
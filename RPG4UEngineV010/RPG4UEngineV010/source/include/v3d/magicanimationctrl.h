/*////////////////////////////////////////////////////////////////////////
文 件 名：MagicAnimationCtrl.h
创建日期：2008年1月15日
最后更新：2008年1月15日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：

	魔法特效播放规则，分为
		a.开始动画	结束动画只播放一次
		b.中间动画	循环播放
		c.结束动画

	三段动画在Period的时限内播放完成
	三段动画要匀速足帧播放

	播放时间：Period-开始动画播放时间-结束动画播放时间
				 如果剩余时间不够中间动画播放一次
				 则增长Period到dwPeriod2
				 最后把Period2缩放到dwPeriod


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MAGICANIMATIONCTRL_H__
#define __MAGICANIMATIONCTRL_H__

#pragma once

#include "CommonDefine.h"
#include "MagicAnimationCtrlDefine.h"
#include "MeshConfig.h"
#include "StdCharSkeleton.h"
#include "AnimationTimeLine.h"
#include "MeshAnimationCtrl.h"

class AnimationTimeLine;
class MeshAnimationCtrl;
class PlayerRender;
class MeshConfig;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _MESHLIB_API MagicAnimationCtrl:public MeshAnimationCtrl
{
public:
	MagicAnimationCtrl();

public:
	BOOL	Create	(IMeshModel* pMeshModel
                  ,MeshConfig* pMeshConfig
						,DWORD       dwPeriod
						,BOOL			 bLoadInStudio=FALSE);
	void	Destroy	();

public:
	////////////////////////////////////////////
	void	Render();
	void	Update			(DWORD          dwTime
                        ,const Matrix4& matrix)		;

	void	OnTriggerPoint	(sTIMELINE_KEYINFO*	pKey
                        ,DWORD					dwFlag =     0)		;

	////////////////////////////////////////////
	DWORD	PlayAnimation	(DWORD dwStartType			// 开始动画类型
								,DWORD dwMiddleType			// 中间动画类型
								,DWORD dwMiddleLoopCount	// 中间动画类型循环播放次数
								,DWORD dwEndType				// 结束动画类型
								,DWORD dwPeriod);				// 整段动画播放时间			

	void	ScaleFrameTo		( DWORD dwPeriod );
	DWORD	GetEndTime			();
	BOOL	IsFinishedInLoop	( DWORD dwTime );

protected:
	sMAGIC_ANIM_SEGMENT					m_Segments[3];
	VG_INT_GET_PROPERTY					(LastFrameID);
	VG_DWORD_GET_PROPERTY				(Period);

	vector<MeshConfigTriggerPoint*> m_TriggerPoints;
};


#endif //__MAGICANIMATIONCTRL_H__
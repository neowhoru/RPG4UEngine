/*////////////////////////////////////////////////////////////////////////
文 件 名：VUICursorDefine.h
创建日期：2006年12月21日
最后更新：2006年12月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUICURSORDEFINE_H__
#define __VUICURSORDEFINE_H__
#pragma once

namespace cursor
{

	enum eCURSOR_TYPE
	{
		//....... 保留
		 CURSORTYPE_DEFAULT					= 0 // = CURSORTYPE_DEFAULT 
		,CURSORTYPE_CANCLE					= 1 // = CURSORTYPE_DEFAULT 	
		,CURSORTYPE_DEFAULT_HL				= 2 // = CURSORTYPE_DEFAULT	
		,CURSORTYPE_DEFAULT_CLICK			= 3 // = CURSORTYPE_DEFAULT		
		,CURSORTYPE_OPEN_BOX					= 4 // = Type_Pick		
		,CURSORTYPE_OPEN_BOX_HL				= 5 // = Type_Pick 	
		,CURSORTYPE_ACTION_SWITCH			= 6 // = Type_Pick 	
		,CURSORTYPE_ACTION_SWITCH_HL		= 7 // = Type_Pick 
		,CURSORTYPE_ATTACK_TARGET			= 8 // = Type_Attack
		,CURSORTYPE_ATTACK_TARGET_HL		= 9 // = Type_Attack	
		,CURSORTYPE_USE						= 10 // = Type_Pick			
		,CURSORTYPE_USE_HL					= 11 // = Type_Pick		
		,CURSORTYPE_ENTER_ZONE				= 12 // = CURSORTYPE_DEFAULT		
		,CURSORTYPE_ENTER_ZONE_HL			= 13 // = CURSORTYPE_DEFAULT	
		,CURSORTYPE_WAITING					= 14 // = Type_Wait			
		,CURSORTYPE_CONVERSATION			= 15 // = Type_Dialog 		
		,CURSORTYPE_REPAIR					= 16 // = CURSORTYPE_DEFAULT 		
		,CURSORTYPE_REPAIR_HL				= 17 // = CURSORTYPE_DEFAULT		
		,CURSORTYPE_BUY						= 18 // = Type_Shop			
		,CURSORTYPE_BUY_HL					= 19 // = Type_Shop		
		,CURSORTYPE_SELL						= 20 // = Type_Shop			
		,CURSORTYPE_SELL_HL					= 21 // = Type_Shop	
		,CURSORTYPE_ITEMMAKE					= 22 // = Type_Shop		
		,CURSORTYPE_ITEMMAKE_HL				= 23 // = Type_Shop		
		,CURSORTYPE_MAGIC_READY				= 24 // = Type_AreaSkill		
		,CURSORTYPE_MAGIC_READY_HL			= 25 // = Type_AreaSkill	
		,CURSORTYPE_MAGIC_INVALID			= 26 // = Type_AreaSkill	
		,CURSORTYPE_CANT_GOTO_TARGET		= 27 // = CURSORTYPE_DEFAULT 	
		,CURSORTYPE_REMOVE_GEM				= 28 // = 拆除宝石 		
		,CURSORTYPE_REMOVE_GEM_HL			= 29 // = 拆除宝石	
																	
		,CURSORTYPE_MAX										

		,CURSORTYPE_TEAM			= CURSORTYPE_DEFAULT			// 组队
		,CURSORTYPE_LOCKITEM    = CURSORTYPE_OPEN_BOX			// 物品加锁
		,CURSORTYPE_UNLOCKITEM	= CURSORTYPE_OPEN_BOX			// 物品解锁
								 
	};

};


#endif //__VUICURSORDEFINE_H__
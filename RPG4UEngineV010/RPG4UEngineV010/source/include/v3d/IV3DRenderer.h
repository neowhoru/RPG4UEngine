/*////////////////////////////////////////////////////////////////////////
文 件 名：IV3DRenderer.h
创建日期：2006年8月30日
最后更新：2006年8月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __IV3DRENDERER_H__
#define __IV3DRENDERER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "V3DRendererDefine.h"
#include "IV3DIndexBuffer.h"
#include "IV3DVertexBuffer.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _V3DGRAPHIC_API IV3DRenderer
{
public:
	IV3DRenderer();
	virtual ~IV3DRenderer();

public:
	virtual void	SetDevice		( LPVOID pDevice ) = 0;
	virtual LPVOID	GetDevice		() = 0;
	virtual LPVOID	GetDeviceOrigin() = 0;

public:
	virtual void SetTexture				( DWORD dwStage, LPVOID pTexture ) = 0;
	virtual void SetRenderState		( DWORD dwState, DWORD dwValue ) = 0;
	virtual void SetVertexShader		( DWORD dwShader ) = 0;
	virtual void SetTextureStageState( DWORD dwStage, DWORD dwType, DWORD dwValue ) = 0;

	virtual void SetIndices				( LPVOID pIndexData, DWORD pVertexIndex ) = 0;
	virtual void SetStreamSource		( DWORD  dwStreamNumber, LPVOID pStreamData, DWORD StreamStride ) = 0;
	
	virtual void SetVertices			( LPVOID pVertices) = 0;			// 设置顶点
	virtual void SetNormals				( LPVOID pNormals ) = 0;			// 设置法线
	virtual void SetIndices				( LPVOID pIndices ) = 0;			// 设置面索引
	virtual void SetUVs					( LPVOID pUVs, int nLayer ) = 0;	// 设置贴图坐标
	virtual void SetDiffuse				( LPVOID pDiffuse ) = 0;			// 设置顶点色


public:
	virtual void SetLayerCount			( int nLayerCount ) = 0;
	virtual void SetShaderId			( short shShaderId ) = 0;

	
	virtual void SetTextureID			( short shTextureId, int nLayer ) = 0;// 设置贴图
	virtual void SetFVF					( DWORD dwFVF ) = 0;		// 设置顶点格式
	virtual void SetVertexStride		( int nStride ) = 0;		// 设置顶点跨踞

	virtual void SetMaterial			( BYTE* pMaterial ) = 0;// 设置材质
	virtual void GetDefaultMaterial	( BYTE* pMaterial ) = 0;
	virtual void SetDefaultMaterial	( BYTE* pMaterial ) = 0;

public:
	virtual void EnableSpecular		( BOOL bEnable ) = 0;
	virtual void SetMaterialSpecular	(float power
                                    ,float a
												,float r
												,float g
												,float b)= 0;

	virtual void SetLight				( int nLightID, LPVOID pLight ) = 0;
	virtual void LightEnable			( int nLightID, BOOL bEnable ) = 0;

	virtual void  SetGlobalTexture	( short shTextureID ) = 0;
	virtual short GetGlobalTexture	() = 0;


public:
	///////////////////////////////////////////////////////////
	virtual DWORD BeginScene	() = 0;
	virtual VOID  EndScene		() = 0;
	//绘图刷新
	virtual void Flush			( DWORD dwFlag ) = 0;
	virtual void Commit			() = 0;

	virtual DWORD Clear			(DWORD Count
                              ,DWORD Flags
										,DWORD Color
										,float Z
										,DWORD Stencil)= 0;

	///////////////////////////////////////////////////////////
	virtual void PushRenderTask(int   nVertexCount
                              ,int   nFaceCount
										,DWORD dwFlag)= 0;

	virtual void PushSurface	(short  nMainMapID
                              ,short  nAddMapID
										,short  nShaderID		// 贴图/Shader
										,DWORD  dwFvf
										,UINT   nStride
										,BYTE*  pVertices
										,WORD   wVertCount
										,BYTE * pIndeices
										,WORD   wTriangleCount
										,DWORD  dwFlag
										,DWORD  dwPolyType=V3DPT_TRIANGLELIST)= 0;

	//绘画表面
	virtual void DrawSurface	(short  nMainMapID
                              ,short  nAddMapID
										,short  nShaderID
										,DWORD  dwFvf
										,UINT   nStride
										,BYTE*  pVertices
										,WORD   nVertCount
										,BYTE * pIndeices
										,WORD   wTriangleCount
										,DWORD  dwFlag
										,DWORD  dwPolyType=V3DPT_TRIANGLELIST
										,INT    nIBOffset=0)= 0;

public:
	virtual DWORD SetViewport	( LPVOID pViewport) = 0;
	virtual void SetTransform	(DWORD State,CONST FLOAT* pMatrix) = 0;
	virtual void SetViewMatrix	( float* pMatrix ) = 0;
	virtual void SetEyePoint	( float* pEyePoint ) = 0;


public:
	////////////////////////////////////////////
	virtual void DrawPrimitive		(ePRIMITIVE_TYPE PrimitiveType
                                 ,INT            FirstIndex
											,INT            NumPrimitives
											,INT            MinIndex
											,INT            MaxIndex)= 0;

	virtual void DrawPrimitiveUP	(ePRIMITIVE_TYPE  PrimitiveType
                                 ,UINT             PrimitiveCount
											,CONST LPVOID     pVertexStreamZeroData
											,UINT             VertexStreamZeroStride)= 0;

public:
	////////////////////////////////////////////
	// 工具类函数

	// 设置全局透明度
	virtual void SetGlobalTransparent( float t ) = 0;
	// 设置全局色
	virtual void SetGlobalDiffuse		( float r, float g, float b ) = 0;
	// 设置全局uv偏移
	virtual void SetGlobalUOffset		( int nLayer, float u ) = 0;
	virtual void SetGlobalVOffset		( int nLayer, float v ) = 0;
	

	/////////////////////////////////////////////////////
	virtual IV3DIndexBuffer*	AllocIndexBuffer	(int nCount, BOOL bDynamic = false)= 0;
	virtual void					FreeIndexBuffer	(IV3DIndexBuffer*)= 0;

	virtual IV3DVertexBuffer*	AllocVertexBuffer	(DWORD          dwFVF
																,int            nVertexSize
																,int            nVertexCount
																,BOOL				 bDynamic = false)= 0;
	virtual void					FreeVertexBuffer	(IV3DVertexBuffer*)= 0;

	virtual BOOL CreateVertexBuffer	(UINT    nLength
                                    ,DWORD   dwUsage
												,DWORD   dwFVF
												,DWORD   dwPool
												,LPVOID* ppVertexBuffer)= 0;

	virtual BOOL CreateIndexBuffer	(UINT    nLength
                                    ,DWORD   dwUsage
												,DWORD   dwFormat
												,DWORD   dwPool
												,LPVOID* ppIndexBuffer)= 0;


	virtual BOOL DestroyVertexBuffer	(void* pVB) = 0;
	virtual BOOL DestroyIndexBuffer	(void* pIB) = 0;

	virtual BOOL LockVertexBuffer		(void*  pVB
                                    ,UINT   OffsetToLock
												,UINT   SizeToLock
												,BYTE** ppbData
												,DWORD  Flags)= 0;
	virtual BOOL UnlockVertexBuffer	(void*  pVB) = 0;

	virtual BOOL LockIndexBuffer		(void*  pIB
                                    ,UINT   OffsetToLock
												,UINT   SizeToLock
												,BYTE** ppbData
												,DWORD  Flags)= 0;
	virtual BOOL UnlockIndexBuffer	(void*  pIB)= 0;

};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(IV3DRenderer , _V3DGRAPHIC_API);
//extern _V3DGRAPHIC_API IV3DRenderer& singleton::GetIV3DRenderer();
//inline void SetIV3DRenderer(IV3DEngine* Ptr);
#define the3DRenderer  singleton::GetIV3DRenderer()



#endif //__IV3DRENDERER_H__
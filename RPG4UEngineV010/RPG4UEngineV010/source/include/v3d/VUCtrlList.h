/*////////////////////////////////////////////////////////////////////////
文 件 名：VUCtrlList.h
创建日期：2006年11月4日
最后更新：2006年11月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUCTRLLIST_H__
#define __VUCTRLLIST_H__
#pragma once


#include "CommonDefine.h"
#include "VUCtrlListDefine.h"
#include "VUCtrlobject.h"
#include "VUCtrlscrollbar.h"

using namespace std;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API VUCtrlList : public VUCtrlObject
{
public:
	VUCtrlList(void);
	~VUCtrlList(void);

public:
	typedef void (*PROC_ONSELECTCHANGE)		(VUCtrlObject* pSender,sUILIST_ITEM* pItem );
	typedef void (*PROC_ONLBUTTON_DBCLICK)	(VUCtrlObject* pSender,sUILIST_ITEM* pItem );
	typedef void (*PROC_ONRBUTTON_DBCLICK)	(VUCtrlObject* pSender,sUILIST_ITEM* pItem );	//右键按下 好友用

public:
	virtual BOOL GetObjectType		(){return Type_List;}
	//////////////////////////////////////


	BOOL InitUIData		( IN VUCtrlObject* pParent, IN sUIINFO_BASE* pData );

	//////////////////////////////////////
	BOOL Render				(DWORD dwTick);

	//////////////////////////////////////
	BOOL OnMouseWheel		( BOOL  bUp,	  INT nMouseX, INT nMouseY );
	BOOL OnMouseMove		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDown	( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonUp		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDBClick( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnRButtonDown	( DWORD dwFlags, INT nMouseX, INT nMouseY);
	BOOL OnKeyDown			( UINT nChar );

	//////////////////////////////////////
	BOOL PtInObject		( int nPosX, int nPosY,INT		nMinAlpha=PTCHECK_SETTING);

public:
	////////////////////////////////////////////////
	BOOL	AddItem			(const sUILIST_ITEM* pItem
                        ,BOOL                bMoveDown=TRUE)		;
	BOOL	AddItems			(const sUILIST_ITEM* pItem
                        ,int                 nItemCount
								,BOOL                bMoveDown=TRUE);
	BOOL	SetItemAt		(int                 nAt
                        ,const sUILIST_ITEM* pItem)		;

	BOOL	ResetItem		(const sUILIST_ITEM* pItem
                        ,int                 nItemCount)		;

	////////////////////////////////////////////////
	BOOL						GetItemRect		(int nIndex, RECT& rc);
	sUILIST_ITEM*			GetCurSelItem	();		// 取得当前选择项
	sUILIST_ITEM*			FindItemBy		(int nID );// 查找项
	void						SelectItemAt	(INT nAt, BOOL bChangeEvent=TRUE );
	
	BOOL						SetCurSelIndex	( const int nIndex );
	void						Clear				();


	/////////////////////////////////////////////////
	void	SetSelectionInfo	(BOOL bHaveSelBar
                           ,BOOL bShow=LISTCTRL_SHOWBAR)		;
	void	SetScrollPos		(int nPosX
                           ,int nPosY)		;
	int   GetListItemHeight	();
	void  SetUIScrollBlend	( int nBlend ) ;
	void	SetScrollHeight	( int nHeight );

	void	SetScrollValue		(int nValue);

	void	SetScrollVisable	( BOOL bShow = TRUE )		;
	void	SetScrollBarEnable( BOOL bEnable = TRUE )		;

	sUILIST_ITEM*				GetListItemAt		(int n) ;
	int							GetListItemCnt		()		   ;
	VUCtrlScrollBar*			GetScrollBarCtrl	()	;

private:
	/////////////////////////////////////////////////////////
	int	_Refresh				();
	BOOL	_DoSelectChange	(int nPosX
                           ,int nPosY)		;

	void	_OnSelectChangeEvent		(sUILIST_ITEM* pItem );
	void	_OnLButtonDBClickEvent	(sUILIST_ITEM* pItem );
	void	_OnRButtonDBClickEvent	(sUILIST_ITEM* pItem );

private:
	/////////////////////////////////////////////////////////
	VG_INT_GET_PROPERTY		(CurSelIndex);		// 取得当前选项的索引

	COLOR							m_colorNormal;
	COLOR							m_colorFrame;


	VG_TYPE_GET_PROPERTY		(ListItems, vector<sUILIST_ITEM*>);
	int							m_nMouseOverSelection;
	BOOL							m_bSelectedBar;
	BOOL							m_bSelectionShow;

	VG_INT_PROPERTY			(StartIndex);
	VG_DWORD_PROPERTY			(ItemMaxCount);
	VG_DWORD_PROPERTY			(ShowMaxCount);

	VG_TYPE_PROPERTY			(ProcOnSelectChange,		PROC_ONSELECTCHANGE);
	VG_TYPE_PROPERTY			(ProcOnLButtonDBClick,	PROC_ONLBUTTON_DBCLICK	);
	VG_TYPE_PROPERTY			(ProcOnRButtonDBClick,	PROC_ONRBUTTON_DBCLICK	);

	VG_BOOL_PROPERTY		(SelectChangeOnButtonUp);
	VUCtrlScrollBar			m_UIScroll;
};

#include "VUCtrlList.inl"


#endif //__VUCTRLLIST_H__
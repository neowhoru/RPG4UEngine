/*////////////////////////////////////////////////////////////////////////
文 件 名：VUIControlStruct.h
创建日期：2007年11月1日
最后更新：2007年11月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUICONTROLSTRUCT0_H__
#define __VUICONTROLSTRUCT0_H__
#pragma once


#include "VUIControlDefine.h"
#include "VUIControlStruct.h"

namespace vui0
{

struct  sUIINFO_PIC
{
	void operator >> (::sUIINFO_PIC& dest)
	{
		dest.m_rcFrame		=m_rcFrame	;
		dest.m_rcClient	=m_rcClient;
		strcpy(dest.m_szPicName,m_szPicName);
	}
	RECT	m_rcFrame;
	RECT	m_rcClient;
	char	m_szPicName[128];
};

// 控制基类
struct  sUIINFO_BASE
{
	enum
	{
		Mode_Left = 0
		,Mode_Top
		,Mode_Right
		,Mode_Bottom
		,Mode_Middle

		,Mode_Max
	};
	void operator >> (::sUIINFO_BASE& dest)
	{
		dest.m_nType					 = m_nType;						
		dest.m_nAlignMode				 = m_nAlignMode;				
		dest.m_rcRealSize				 = m_rcRealSize;				
		dest.m_nFontSize				 = m_nFontSize;				
		dest.m_clrFontText			 = m_clrFontText;					

		m_MainPic				 >> dest.m_MainPic;				

		memcpy(dest.m_nDrawMode,m_nDrawMode,sizeof(m_nDrawMode));	
		memcpy(&dest.m_szID,			  &m_szID, sizeof(m_szID));					
		memcpy(&dest.m_szCaption,	  &m_szCaption, sizeof(m_szCaption));			
		memcpy(&dest.m_szFont,		  &m_szFont, sizeof(m_szFont));				
	}



	int		m_nType;						// 控件类型
	int		m_nAlignMode;				// 对齐模式
	RECT		m_rcRealSize;				// 框架的RECT
	int		m_nDrawMode[Mode_Max];	// 绘制模式
	char		m_szID[64];					// 控件的ID
	sUIINFO_PIC		m_MainPic;				// 控件背景图片
	char		m_szCaption[128];			// 标题
	char		m_szFont[32];				// 字体
	int		m_nFontSize;				// 字体大小
	COLORREF m_clrFontText;					// 字体颜色
};

//////////////////////////////////////////////////////////////////////////
// 框架
struct  sUIINFO_FRAME : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_FRAME& data = (::sUIINFO_FRAME&)dest;
		data.m_nArrayMode	= m_nArrayMode	;
		data.m_bCanMoving	= m_bCanMoving;
		_SUPER::operator >>(dest);
	}

	int	m_nArrayMode;			// 排列模式
	bool	m_bCanMoving;			// 是否可被移动
};

//////////////////////////////////////////////////////////////////////////
// 按钮
struct  sUIINFO_BUTTON : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_BUTTON& data = (::sUIINFO_BUTTON&)dest;
		m_PicMouseOver		>>data.m_PicMouseOver	 	;
		m_PicMousePress		>>data.m_PicMousePress		 	;
		m_PicDisabled	>>data.m_PicDisabled 	;
		_SUPER::operator >>(dest);
	}

	sUIINFO_PIC	m_PicMouseOver;
	sUIINFO_PIC	m_PicMousePress;
	sUIINFO_PIC	m_PicDisabled;
};

//////////////////////////////////////////////////////////////////////////
// 选择框
struct  sUIINFO_CHECKBOX : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_CHECKBOX& data = (::sUIINFO_CHECKBOX&)dest;
		data.m_bChecked	= m_bChecked;
		m_BtnChecked			>>data.m_BtnChecked		 	;
		_SUPER::operator >>(dest);
	}

	bool			m_bChecked;
	sUIINFO_BUTTON	m_BtnChecked;
};

//////////////////////////////////////////////////////////////////////////
// 滚动条
struct  sUIINFO_SCROLLBAR : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_SCROLLBAR& data = (::sUIINFO_SCROLLBAR&)dest;
		m_BtnUp				>>data.m_BtnUp;		
		m_BtnDown			>>data.m_BtnDown;		
		m_BtnDragBar		>>data.m_BtnDragBar;		
		_SUPER::operator >>(dest);
	}
	sUIINFO_BUTTON	m_BtnUp;
	sUIINFO_BUTTON	m_BtnDown;
	sUIINFO_BUTTON	m_BtnDragBar;
};

// 滚动条
struct  sUIINFO_SCROLLBAR_EX : public sUIINFO_SCROLLBAR
{
};

//////////////////////////////////////////////////////////////////////////
// 编辑框
struct  sUIINFO_EDIT : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_EDIT& data = (::sUIINFO_EDIT&)dest;
		data.m_bMultiLine	= m_bMultiLine;;
		data.m_bReadOnly	= m_bReadOnly;;
		data.m_bPassword	= m_bPassword;;
		data.m_bIsNumber	= m_bIsNumber;;
		data.m_nTextAlign	= m_nTextAlign;;
		m_ScrollBar			>>data.m_ScrollBar		 	;
		_SUPER::operator >>(dest);
	}

	bool	m_bMultiLine;
	bool	m_bReadOnly;
	bool	m_bPassword;
	bool	m_bIsNumber;
	int	m_nTextAlign;
	sUIINFO_SCROLLBAR	m_ScrollBar;
};

//////////////////////////////////////////////////////////////////////////
// 文本框
struct  sUIINFO_TEXT : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_TEXT& data = (::sUIINFO_TEXT&)dest;
		data.m_nTextAlign	= m_nTextAlign;
		_SUPER::operator >>(dest);
	}

	int		m_nTextAlign;
};

//////////////////////////////////////////////////////////////////////////
// 文本列表
struct  sUIINFO_LIST : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_LIST& data = (::sUIINFO_LIST&)dest;
		data.m_clrSelected		= m_clrSelected;;
		data.m_nItemHeight	= m_nItemHeight;
		m_ScrollBar			>>data.m_ScrollBar		 	;
		_SUPER::operator >>(dest);
	}

	COLORREF m_clrSelected;
	int		m_nItemHeight;
	sUIINFO_SCROLLBAR	m_ScrollBar;
};

//////////////////////////////////////////////////////////////////////////
// 图像列表
struct  sUIINFO_IMGLIST : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_IMGLIST& data = (::sUIINFO_IMGLIST&)dest;
		data.m_nIconSize			= m_nIconSize;;
		data.m_nOffsetWidth		= m_nOffsetWidth;;
		data.m_nOffsetHeight		= m_nOffsetHeight;;
		data.m_nMaxHeightCount	= m_nMaxHeightCount;;
		data.m_bCanPick			= m_bCanPick;;
		data.m_bShowFrame  	= m_bShowFrame;  ;
		data.m_clrShowFrame    	= m_clrShowFrame;    ;
		m_ScrollBar			>>data.m_ScrollBar		 	;
		_SUPER::operator >>(dest);
	}

	int		m_nIconSize;
	int		m_nOffsetWidth;
	int		m_nOffsetHeight;
	int		m_nMaxHeightCount;
	bool		m_bCanPick;
	bool		m_bShowFrame;  
	COLORREF m_clrShowFrame;    
	sUIINFO_SCROLLBAR	m_ScrollBar;
};

//////////////////////////////////////////////////////////////////////////
// 下拉框
struct  sUIINFO_COMBOBOX : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_COMBOBOX& data = (::sUIINFO_COMBOBOX&)dest;
		m_EditBox				>>data.m_EditBox;		 	;
		m_ListCtrl				>>data.m_ListCtrl;		 	;
		m_ButtonCtrl			>>data.m_ButtonCtrl		 	;
		_SUPER::operator >>(dest);
	}
	sUIINFO_EDIT		m_EditBox;
	sUIINFO_LIST		m_ListCtrl;
	sUIINFO_BUTTON	m_ButtonCtrl;
};

//////////////////////////////////////////////////////////////////////////
// 图片
struct  sUIINFO_IMAGE : public sUIINFO_BASE 
{
};

//////////////////////////////////////////////////////////////////////////
// 进度条
struct  sUIINFO_PROGRESS : public sUIINFO_BASE
{
};

//////////////////////////////////////////////////////////////////////////
// 属性页
struct  sUIINFO_TAB : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_TAB& data = (::sUIINFO_TAB&)dest;
		for(INT n=0; n<TAB_CONTROL_MAX; n++)
			m_TabButtons[n]		>>data.m_TabButtons[n]		 	;
		m_FrameCtrl		>>data.m_FrameCtrl		 	;
		memcpy(data.m_FrameFiles,m_FrameFiles,64*TAB_CONTROL_MAX ) ;
		data.m_nTabCount	= m_nTabCount;
		_SUPER::operator >>(dest);
	}

	sUIINFO_BUTTON	m_TabButtons[TAB_CONTROL_MAX];
	sUIINFO_FRAME		m_FrameCtrl;
	char			m_FrameFiles[TAB_CONTROL_MAX][64];
	int				m_nTabCount;
};

//////////////////////////////////////////////////////////////////////////
// 列表框扩展
struct  sUIINFO_LIST_EX : public sUIINFO_BASE
{
	void operator >> (::sUIINFO_BASE& dest)
	{
		::sUIINFO_LIST_EX& data = (::sUIINFO_LIST_EX&)dest;
		data.m_clrSelected			= m_clrSelected;;
		data.m_nItemHeight		= m_nItemHeight;;
		data.m_nItemWidth			= m_nItemWidth;;
		data.m_nOffsetWidth		= m_nOffsetWidth;;
		data.m_nOffsetHeight		= m_nOffsetHeight;;
		data.m_nMaxHeightCount	= m_nMaxHeightCount;
		data.m_bCanPick    	= m_bCanPick;    ;
		m_ScrollBar			>>data.m_ScrollBar		 	;
		_SUPER::operator >>(dest);
	}

	COLORREF		m_clrSelected;
	int			m_nItemHeight;
	int			m_nItemWidth;
	int			m_nOffsetWidth;
	int			m_nOffsetHeight;
	int			m_nMaxHeightCount;
	bool			m_bCanPick;    
	sUIINFO_SCROLLBAR	m_ScrollBar;
};


//////////////////////////////////////////////////////////////////////////
//////////////////////////////////////////////////////////////////////////


	struct sUIDATA_HEADER
	{

		void operator >> (::sUIDATA_HEADER& dest)
	{
		memcpy(dest.m_szVersion,m_szVersion,10) ;
		dest.m_dwCreatedDate = m_dwCreatedDate;
		m_FrameData			>>dest.m_FrameData		 	;
		dest.m_nControlCount = m_nControlCount;
		memcpy(dest.m_ControlTypes,m_ControlTypes,CONTROLS_COUNT_MAX*sizeof(int)) ;
	}

		char		m_szVersion[10];
		DWORD		m_dwCreatedDate;
		sUIINFO_FRAME	m_FrameData;
		int			m_nControlCount;
		int			m_ControlTypes[CONTROLS_COUNT_MAX];
	};

};


#endif //__VUICONTROLSTRUCT_H__
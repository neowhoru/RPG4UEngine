/*////////////////////////////////////////////////////////////////////////
文 件 名：VUIControlDefine.h
创建日期：2007年11月21日
最后更新：2007年11月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUICONTROLDEFINE_H__
#define __VUICONTROLDEFINE_H__
#pragma once

//#define DECLARE_DLG_INST(cname)					\
//	public:										\
//		static cname* instance()				\
//		{										\
//			static cname* pDlg = NULL;			\
//			if( NULL == pDlg )					\
//			{									\
//				pDlg = new cname();				\
//				assert(pDlg);					\
//				pDlg->LoadUI();					\
//				pDlg->SetVisible(FALSE);		\
//			}									\
//			return pDlg;						\
//		}


#define UI_VER				"20080707a"		// 版本号
#define UI_VER0			"20040625a"		// 版本号

#define CONTROLS_COUNT_MAX	64				// 控件最大数
#define TAB_CONTROL_MAX		5				// 属性页最大页数

//////////////////////////////////////////////////////////////////////////
// 控件类型
enum E_ControlType
{
	 Type_Canvas = -1
	,Type_Normal = 0
	,Type_Frame				// 框架
	,Type_Button			// 按钮
	,Type_CheckBox			// 多选框
	,Type_Edit				// 编辑框
	,Type_Text				// 文本框
	,Type_List				// 文本列表
	,Type_ListImg			// 图像列表
	,Type_ScrollBar		// 滚动条
	,Type_ComboBox			// 下拉框
	,Type_Picture			// 图像
	,Type_Progress			// 进条条
	,Type_Tab				// 属性页
	,Type_ListEx			// 列表框扩展
	,Type_ScrollBarEx		// 滚动条		
	,Type_MAX
};

// 绘制模式
enum E_DrawMode
{
	 DrawModeNormal = 0
	,DrawModeTile
	,DrawModeStrectch
	,DrawMode_MAX
};

// 对齐模式
enum E_AlignMode
{
	AlignMode_LeftTop = 0
	,AlignMode_RightTop
	,AlignMode_LeftBottom
	,AlignMode_RightBottom
	,AlignMode_Left
	,AlignMode_Top
	,AlignMode_Right
	,AlignMode_Bottom
	,AlignMode_Middle
	,AlignMode_TopMiddle
	,AlignMode_LeftMiddle
	,AlignMode_BottomMiddle
	,AlignMode_RightMiddle
	,AlignMode_MAX
};

// 排列模式
enum E_ArrayMode
{
	ArrayMode_Normal = 0
	,ArrayMode_Top
	,ArrayMode_Bottom
	,ArrayMode_MAX
};

enum eUIOBJ_TYPE
{
	 UIOBJ_BASE					= 0
	,UIOBJ_NPCDIALOG			= 1
	,UIOBJ_INVENTORY			= 2
	,UIOBJ_PROPERTY			= 3
	,UIOBJ_CHARSELECT			= 4
	,UIOBJ_SELECTSERVER		= 5
	,UIOBJ_SHOP					= 6
	,UIOBJ_SKILL				= 7
	,UIOBJ_TARGET				= 8
	,UIOBJ_QUEST				= 9
	,UIOBJ_ACTION				= 10
	,UIOBJ_TRADE				= 11
	,UIOBJ_CHATINPUT			= 12
	,UIOBJ_CHATLOG				= 13
	,UIOBJ_CHARCREATE			= 14
	,UIOBJ_HERORELIVE			= 15
	,UIOBJ_SYSTEMMENU			= 16
	,UIOBJ_LOADMAP				= 17
	,UIOBJ_LOGIN				= 18
	,UIOBJ_GAMEIN				= 19
	,UIOBJ_MAP					= 20
	,UIOBJ_MINIMAP				= 21
	,UIOBJ_BASEPROPERTY		= 22
	,UIOBJ_FRIEND				= 23
	,UIOBJ_HELP					= 24

	,UIOBJ_MAX
};


#endif //__VUICONTROLDEFINE_H__
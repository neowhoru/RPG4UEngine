/*////////////////////////////////////////////////////////////////////////
文 件 名：VUCtrlFrame.h
创建日期：2006年11月1日
最后更新：2006年11月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUCTRLFRAME_H__
#define __VUCTRLFRAME_H__
#pragma once


#include "CommonDefine.h"
#include "VUCtrlObject.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API VUCtrlFrame : public VUCtrlObject
{
public:
	VUCtrlFrame(void);
	~VUCtrlFrame(void);

public:
	///////////////////////////////////////////////////////
	typedef BOOL (*PROC_ONCLICK)			(VUCtrlObject* pSender );
	typedef BOOL (*PROC_ONBUTTONDOWN)	(VUCtrlObject* pSender );
	typedef BOOL (*PROC_ONFRAMEMOVE)		(DWORD dwTick);
	typedef BOOL (*PROC_ONFRAMEMOVE2)	(DWORD dwTick, VUCtrlObject* pSender);
	typedef BOOL (*PROC_ONRENDER)			(DWORD dwTick);
	typedef BOOL (*PROC_ONRENDER2)		(DWORD dwTick, VUCtrlObject* pSender);


public:
	virtual BOOL GetObjectType		(){return Type_Frame;}
	///////////////////////////////////////////////////////
	virtual BOOL Create		(const RECT*   pRect=NULL
                           ,VUCtrlObject* pParent=NULL
									,sUIINFO_BASE* pData=NULL);


	void				ReleaseUI			();
	DWORD				LoadFromFile		(IN LPCSTR				szFileName
                                    ,IN VUCtrlObject*		pParent = NULL
												,LPCSTR					szNewName=NULL);
	VUCtrlFrame*	LoadChildByFrame	(IN  LPCSTR				szFileName
							               ,LPCSTR					szNewName = NULL)		;

	///////////////////////////////////////////////////////
	BOOL Render				(DWORD dwTick);
	BOOL FrameMove			(DWORD dwTick);

	///////////////////////////////////////////////////////
	void StartFlash		(int  nFlashCount
                        ,BOOL bAutoClose = FALSE
								,BOOL bFadeIn =    FALSE);

	///////////////////////////////////////////////////////
	VUCtrlObject*	FindFrame		(LPCSTR szFrameID );
	VUCtrlObject*	FindControl		(LPCSTR szControlID
											,LPCSTR szFrameID=NULL)		;
	void				AppendControl	(VUCtrlObject* pChlid);
	VUCtrlObject*	GetControl		(UINT nIndex);
	UINT				GetControlCount();


	///////////////////////////////////////////////////////
	BOOL OnMouseWheel			( BOOL  bUp,	  INT nMouseX, INT nMouseY );
	BOOL OnMouseMove			( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDown		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonUp			( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnLButtonDBClick	( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnRButtonDown		( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnRButtonUp			( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnRButtonDBClick	( DWORD dwFlags, INT nMouseX, INT nMouseY );
	BOOL OnKeyDown				( UINT  nChar );
	BOOL OnKeyUp				( UINT  nChar );
	BOOL OnChar					( UINT  nChar );

	BOOL IsLButtonDragging	();

public:

	/////////////////////////////////////////////////////
	BOOL	PtInObject			(int nPosX, int nPosY,INT		nMinAlpha=PTCHECK_SETTING ); //-1不使用Alpha检测，-2使用g_V3DConfig.GetUIMinAlpha()配置
	BOOL	IsChild				(const VUCtrlObject* pChild );
	BOOL	IsEnableFocus		()	;

	int	GetArrangeMode		();
	void	SetArrangeMode		( int nMode );
	void	SetProcOnRender	(PROC_ONRENDER		pFun,BOOL bBeforeRender );
	void	SetProcOnRender2	(PROC_ONRENDER2	pFun,BOOL bBeforeRender );

	void	SetFadeWhole		(int	nFadeMax
                           ,int	nFadeMin)		;
	void	SetBlendColorWhole(int	nColor );

	void	SetVisibleWhole	(BOOL bVisible
                           ,BOOL bFadeInorFadeOut = FALSE
									,int	nColor =            0xff);


public:
	///////////////////////////////////////
	VG_DWORD_PROPERTY			(CreatedDate);	//创建日期
	VG_TYPE_PROPERTY			(ProcOnFrameMove,		PROC_ONFRAMEMOVE);
	VG_TYPE_PROPERTY			(ProcOnFrameMove2,	PROC_ONFRAMEMOVE2);
	VG_TYPE_PROPERTY			(ProcOnRender,			PROC_ONRENDER);
	VG_TYPE_PROPERTY			(ProcOnRender2,		PROC_ONRENDER2);
	VG_BOOL_PROPERTY			(BeforeRender);
	VG_TYPE_PROPERTY			(ProcOnClick,			PROC_ONCLICK);
	VG_TYPE_PROPERTY			(ProcOnButtonDown,	PROC_ONBUTTONDOWN);

	vector<VUCtrlObject*>	m_Controls;
	BOOL							m_bLButtonDrag;
	POINT							m_ptOffset;

	BOOL							m_bAutoClose;
	RECT							m_rcAutoClose;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "VUCtrlFrame.inl"


#endif //__VUCTRLFRAME_H__
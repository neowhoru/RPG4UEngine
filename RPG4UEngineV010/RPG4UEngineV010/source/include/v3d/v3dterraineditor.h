/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DTerrainEditor.h
创建日期：2006年10月19日
最后更新：2006年10月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DTERRAINEDITOR_H__
#define __V3DTERRAINEDITOR_H__
#pragma once


#include "WorldConst.h"
#include "V3DTerrainEditorDefine.h"
#include "V3DTerrainVertex.h"
#include "V3DTerrain.h"

class V3DSceneGrass;
class V3DSceneObject;
using namespace std;
typedef map<sVEGETATION_GRASS*,V3DSceneGrass*>	SceneGrassMap;
typedef pair<sVEGETATION_GRASS*,V3DSceneGrass*>		SceneGrassValue;
typedef SceneGrassMap::iterator							SceneGrassMapIt;

class ImageTGA;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API  V3DTerrainEditor : public V3DTerrain
{
public:
	V3DTerrainEditor();
	virtual void	CloseMap();

public:
	virtual BOOL	Create	( DWORD dwCol, DWORD dwRow );
	virtual void	Release	();

public:
	BOOL				BuildingSceneGrass	();
	V3DSceneGrass*	GetSceneGrass			(DWORD dwType,sVEGETATION_GRASS* pGrass);
	BOOL				UpdateSceneGrass		(DWORD dwType,sVEGETATION_GRASS* pGrass);
	BOOL				InsertSceneGrassMap	(DWORD dwType,V3DSceneGrass*			pGrassObj);

	////////////////////////////////////////////////////////////////////////
	void				SaveAlphaMap	(LPCSTR szPath );
	void				LoadAlphaMap	(LPCSTR szPath );

	////////////////////////////////////////////////////////////////////////
	BOOL	LoadHeightMapBy	(FLOAT* pfBuffer
                           ,int    iBaseHeight
									,int    iHeightStep);

	BOOL	SaveHeightMapTo	( LPCSTR szFileName);
	BOOL	LoadHeightMapBy	( LPCSTR szFileName);
	BOOL	LoadHeightMapBy	( FLOAT* pFloatData);

	BOOL	SaveHeightMapPic	(LPCSTR pszFilename
                           ,int    iBaseHeight
									,int    iHeightStep);
	BOOL	LoadHeightMapPic	(LPCSTR pszFilename
                           ,int    iBaseHeight
									,int    iHeightStep);
	

	//////////////////////////////////////////
	BOOL	LoadTileMaskMapPic(LPCSTR	szPath );
	void	SaveTileMaskMapPic(LPCSTR	szPath );

	//////////////////////////////////////////
	BOOL	LoadDiffuseMapPic	(LPCSTR	szPath );
	void	SaveDiffuseMapPic	(LPCSTR	szPath );

	//////////////////////////////////////////
	//阴影
	BOOL	GenerateShadow		(V3DScene* pScene
                           ,LPCSTR    szFile
									,BOOL      bOnlyTerrainShadow);
	BOOL	LoadShadowMapPic	(LPCSTR		szFileName );
	void	SaveShadowMapPic	(LPCSTR	   szPath );

	
	///////////////////////////////////////
	//BOOL	LoadShadowMapPic	(LPCSTR		szFileName);

	BOOL	GenerateShadowBySnapshort	(INT			nWidth
												,INT			nHeight
												,BYTE *		pBuffer)		;


	///////////////////////////////////////
	//Render Info
	BOOL	RenderAll					(DWORD dwTick);
	BOOL	RenderGrid					(DWORD         dwTick
                                 ,COLOR			clrGrid
                                 ,COLOR			clrChunk
											,Vector3C*     pvLockPos=NULL
											,INT				nShrinkNum = 0);
	BOOL	RenderPathTileInfo		(DWORD dwTick,INT nMaskType, COLOR color);
	BOOL	RenderMaskInfo				(DWORD dwTick);
	BOOL	RenderSelectedVertices	(DWORD dwTick);

	BOOL	RenderArea			(int   nBeginX
                           ,int   nBeginY
									,int   nEndX
									,int   nEndY
									,DWORD dwColor);

	BOOL	RenderZone			(int    nBeginX
                           ,int    nBeginY
									,int    nEndX
									,int    nEndY
									,DWORD  dwColor
									,int    nStep
									,LPCSTR pZoneName);

	BOOL	RenderZoneEx		(int    nBeginX
                           ,int    nBeginY
									,int    nEndX
									,int    nEndY
									,DWORD  dwColor
									,int    nStep
									,LPCSTR pZoneName);

public:
	///////////////////////////////////////
	int	StartPointBlender	(int   nX
                           ,int   nY
									,short stTexture
									,int   nBlend
									,int   nLayer
									,int   bBrushType
									,float fAlphaModulus);
	
	int	AttachMemoryBlender	(RECT   &rt
                              ,short  stTexture
										,int    nRadius
										,short* pBuffer
										,int    nLayer
										,float  fAlphaModulus
										,INT    nDirType);
	int		StartPointBlender	(int    nX
                              ,int    nY
										,short  stTexture
										,int    nLayer
										,int    nRadius
										,short* pBuffer
										,float  fAlphaModulus);

	//材质
	int		StartRectBlender	(RECT  &rt
                              ,short stTexture
										,int   nBlend
										,int   nLayer);

public:
	BOOL	LoadLayoutTexture	(LPCSTR		pszFilename );

	///////////////////////////////////////
	BOOL	ImportTerrain		( LPCSTR		pszFilename );
	BOOL	ApplyImportTerrain( RECT		rect, DWORD dwID );

	///////////////////////////////////////
	BOOL	ResetGrass			();
	BOOL	ClearGrass			();
	BOOL	RemoveGrass			(int nMapX, int nMapY);
	V3DSceneObject*	AddGrass		(int                      nType
											,int                      x
											,int                      y
											,sVEGETATION_GEN_SETTING* pSetting
											,float                    fRandomSize);

	V3DSceneObject*	GhostGrass	(int                nType
											,sVEGETATION_GRASS* pGrass
											,sVEGETATION_GRASS* pGrass2);
	V3DSceneObject*	GhostGrass			(V3DSceneObject* pObject);
	BOOL					RemoveSceneGrass	(V3DSceneObject* pObject);

public:
	///////////////////////////////////////
	void	BeginSelection	(int nTileX
                        ,int nTileY
								,int nRadius
								,int nType);
	void	EndSelection	();

	///////////////////////////////////////
	BOOL	Raise				(int fRaiseVal);
	BOOL	RaisePlot		(int fRaiseVal);

	//顶点
	BOOL	RaiseCliffPoint(int  x
                        ,int  y
								,int  iCliffType
								,int  fRaiseVal
								,BOOL bCheckRaiseLimit);
	BOOL	RaiseGroudEx	(int  x
                        ,int  y
								,REAL fRaiseVal
								,BOOL bSet=false
								,BOOL bUseMask=false);
	BOOL	RaiseGroud		(int  x
                        ,int  y
								,REAL fRaiseVal
								,BOOL bSet=false
								,BOOL bUseMask=false);

	BOOL	UpdateGroud		(int  x
                        ,int  y
								,int  nWidth
								,int  nHeight);
	BOOL	UpdateGroudEx	(int  x
                        ,int  y
								,int  nWidth
								,int  nHeight);

	///////////////////////////////////////
	BOOL	SetWaterChunkHeight	(int  x
										,int  y
										,float fWaterHeight
										,BOOL bAutoFitToAround=FALSE);	///自动与周围连续水块
	BOOL	SetWaterChunkHeight	(V3DTerrainChunk* pChunk
										,float fWaterHeight
										,BOOL bAutoFitToAround=FALSE);	///自动与周围连续水块

	BOOL	SetWaterChunk	(int  x
                        ,int  y
								,BOOL bSet
								,int  iWaterType
								,float fWaterHeight
								,BOOL  bTraceDiffuse		=FALSE
								,BOOL bShore				=FALSE
								,BOOL bAutoFitToAround	=FALSE);	///自动与周围水块结合

	BOOL	SetWaterTextureChunk	(V3DTerrainChunk* pChunk
										,int  iWaterType
										,BOOL bAutoFitToAround=FALSE);	///自动与周围水块结合
	BOOL	SuckTerrainDiffuse	(V3DTerrainChunk* pChunk
										,DWORD dwUpdateFlag=0
										,BOOL bAutoFitToAround=FALSE);	///自动与周围水块结合
	BOOL	UpdateTerrainWaterColor	(COLOR color,DWORD dwUpdateFlag=0);
	BOOL	UpdateChunkWaterColors	(V3DTerrainChunk* pChunk
											,COLOR arColors[]
											,DWORD dwUpdateFlag = 0
											,BOOL  bPreview=FALSE
											,BOOL bAutoFitToAround=FALSE);	///自动与周围水块结合

	//在指定Tile下Chunk九宫格内查找有水面的地块
	V3DTerrainChunk* FindWaterChunkAround	(int nTileX,int nTileY);

	/////////////////////////////////////////
	//阻挡
	void	SetMoveMaskInfo(int  x
                        ,int  y
								,int  nRadius
								,BOOL bWalkMak
								,BOOL	bSystem = FALSE);

	BOOL	SmoothGround	(INT	nX
                        ,INT	nY
								,INT	nRange);


	/////////////////////////////////////////
	BOOL	SelectEditorVertex	( int x, int y, float fPlusValues );




	BOOL  SelectChunk			(V3DTerrainChunk* pChunk);
	BOOL  UnselectChunk		(V3DTerrainChunk* pChunk);
	BOOL  SelectChunk			(UINT nChunkX, UINT nChunkY);
	BOOL  UnselectChunk		(UINT nChunkX, UINT nChunkY);
	BOOL  ClearChunkSelect	();

public:

protected:
	BOOL						m_SelectionStates	[MAP_SIZE_BYTILE]
														[MAP_SIZE_BYTILE];
	COLOR						m_clrSelection;

#ifdef USE_TERRAIN_VERTEX
	V3DTerrainVertex		m_VerticesSelection;#ifdef USE_TERRAIN_VERTEX

#else
#endif
	VG_BOOL_PROPERTY				(CtrlState);
	VG_BOOL_PROPERTY				(AltState);
	VG_VECTOR_PROPERTY			(BrushSize, Vector3D);
	SceneGrassMap					m_GrassObjects[MAX_VEGETATION_AMOUNT];
	VG_BOOL_PROPERTY				(RenderDiffuse);
	
};



#endif //__V3DTERRAINEDITOR_H__
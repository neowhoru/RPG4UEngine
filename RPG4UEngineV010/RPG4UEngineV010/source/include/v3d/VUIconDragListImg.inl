/*////////////////////////////////////////////////////////////////////////
文 件 名：VUIconDragListImg.inl
创建日期：2008年6月4日
最后更新：2008年6月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUICONDRAGLISTIMG_INL__
#define __VUICONDRAGLISTIMG_INL__
#pragma once

////////////////////////////////////////////
inline BOOL IconDragListImg::IsNull()		
{
	return m_pIconInfo == NULL || m_pIconInfo->IsNull();		
}

inline void IconDragListImg::SetFlags( DWORD Flags )	
{
	m_dwFlagData = Flags;	
}

inline void IconDragListImg::SetTime( DWORD dwStartTime, DWORD dwPeriod )
{
	m_dwStartTime	= dwStartTime;
	m_dwPeriod		= dwPeriod;
	m_pItemTimer	= NULL;
}

inline void IconDragListImg::SetTimer(util::Timer* pItemTimer )
{
	m_pItemTimer = pItemTimer;
}

inline void IconDragListImg::SetExtraInfo( const char* szInfo )
{
	if(m_pIconInfo)
		m_pIconInfo->SetExtraInfo( szInfo );
}

inline void IconDragListImg::SetColor(COLOR col )
{
	m_Color		= col;
	m_bUseCol	= TRUE;
}
inline void IconDragListImg::SetEnable( BOOL bEnable , BOOL bStatic)
{
	if(bEnable)		_BIT_ADD		(m_dwFlagData, DRAGICON_ENABLE);
	else				_BIT_REMOVE	(m_dwFlagData, DRAGICON_ENABLE);

	if(bStatic)
	{
		if(bEnable)		_BIT_ADD		(m_dwFlagStatic, DRAGICON_ENABLE);
		else				_BIT_REMOVE	(m_dwFlagStatic, DRAGICON_ENABLE);
	}
}

inline BOOL IconDragListImg::IsEnable()
{
	return _BIT_TEST(m_dwFlagData, DRAGICON_ENABLE) && _BIT_TEST(m_dwFlagStatic, DRAGICON_ENABLE);
}

inline void IconDragListImg::SetDeactivate( BOOL bDeactivate , BOOL bStatic)
{
	if(bDeactivate)	_BIT_ADD(m_dwFlagData, DRAGICON_DEACTIVATE);
	else					_BIT_REMOVE(m_dwFlagData, DRAGICON_DEACTIVATE);

	if(bStatic)
	{
		if(bDeactivate)	_BIT_ADD			(m_dwFlagStatic, DRAGICON_DEACTIVATE);
		else					_BIT_REMOVE		(m_dwFlagStatic, DRAGICON_DEACTIVATE);
	}
}

inline BOOL IconDragListImg::IsDeactivate()
{
	return _BIT_TEST(m_dwFlagData, DRAGICON_DEACTIVATE) || _BIT_TEST(m_dwFlagStatic, DRAGICON_DEACTIVATE);
}

inline void IconDragListImg::SetNoPick( BOOL bSet, BOOL bStatic )
{
	if(bSet)		_BIT_ADD		(m_dwFlagData, DRAGICON_NOPICK);
	else			_BIT_REMOVE	(m_dwFlagData, DRAGICON_NOPICK);
	if(bStatic)
	{
		if(bSet)		_BIT_ADD		(m_dwFlagStatic, DRAGICON_NOPICK);
		else			_BIT_REMOVE	(m_dwFlagStatic, DRAGICON_NOPICK);
	}
}

inline BOOL IconDragListImg::IsNoPick()
{
	return _BIT_TEST(m_dwFlagData, DRAGICON_NOPICK) || _BIT_TEST(m_dwFlagStatic, DRAGICON_NOPICK);
}

inline void IconDragListImg::SetDamaged( BOOL bSet , BOOL bStatic)
{
	if(bSet)		_BIT_ADD		(m_dwFlagData, DRAGICON_DAMAGED);
	else			_BIT_REMOVE	(m_dwFlagData, DRAGICON_DAMAGED);

	if(bStatic)
	{
		if(bSet)		_BIT_ADD		(m_dwFlagStatic, DRAGICON_DAMAGED);
		else			_BIT_REMOVE	(m_dwFlagStatic, DRAGICON_DAMAGED);
	}
}

inline BOOL IconDragListImg::IsDamaged()
{
	return _BIT_TEST(m_dwFlagData, DRAGICON_DAMAGED) || _BIT_TEST(m_dwFlagStatic, DRAGICON_DAMAGED);
}

inline void IconDragListImg::SetFocus( BOOL bSet , BOOL bStatic)
{
	if(bSet)		_BIT_ADD		(m_dwFlagData, DRAGICON_FOCUS);
	else			_BIT_REMOVE	(m_dwFlagData, DRAGICON_FOCUS);
	if(bStatic)
	{
		if(bSet)		_BIT_ADD		(m_dwFlagStatic, DRAGICON_FOCUS);
		else			_BIT_REMOVE	(m_dwFlagStatic, DRAGICON_FOCUS);
	}
}

inline BOOL IconDragListImg::IsFocus()
{
	return _BIT_TEST(m_dwFlagData, DRAGICON_FOCUS) || _BIT_TEST(m_dwFlagStatic, DRAGICON_FOCUS);
}


inline void IconDragListImg::SetInvisible( BOOL bSet , BOOL bStatic)
{
	if(bSet)		_BIT_ADD		(m_dwFlagData, DRAGICON_INVISIBLE);
	else			_BIT_REMOVE	(m_dwFlagData, DRAGICON_INVISIBLE);

	if(bStatic)
	{
		if(bSet)		_BIT_ADD		(m_dwFlagStatic, DRAGICON_INVISIBLE);
		else			_BIT_REMOVE	(m_dwFlagStatic, DRAGICON_INVISIBLE);
	}
}

inline BOOL IconDragListImg::IsInvisible()
{
	return _BIT_TEST(m_dwFlagData, DRAGICON_INVISIBLE) || _BIT_TEST(m_dwFlagStatic, DRAGICON_INVISIBLE);
}




#endif //__VUICONDRAGLISTIMG_INL__
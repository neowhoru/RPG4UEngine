/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DLight.h
创建日期：2007年7月28日
最后更新：2007年7月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DLIGHT_H__
#define __V3DLIGHT_H__
#pragma once

#include "LightDefine.h"
#include "V3DSceneObject.h"


class V3DSceneAnimation;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DLight:public V3DSceneObject
{
public:
	V3DLight(void);
	virtual ~V3DLight(void);

public:
	//virtual
	virtual BOOL	Update				(void);
	virtual BOOL	Render				(void);

	///////////////////////////////////////
	void	StartDirectionLight	(const VECTOR3D& vPos
                              ,const VECTOR3D& vDir)		;

	void	StartPointLight		(const VECTOR3D& vPos
                              ,float           fRange
										,float           fAttenuation1);

	void	StartSpotLight			(const VECTOR3D& vPos
                              ,float           fRange)		;

public:
	///////////////////////////////////////
	//设置灯光颜色
	void				SetDiffuseColor	( const COLOR4F& color );	

	LIGHT_INFO*		GetLightData		(void);
	LIGHT_INFO*		GetLightRenderData(void);
	float				GetLightRange		(void);
	LIGHTTYPE		GetlightType		(void);
	void				UpdateLightData	(void );


public:
	void				FadeOut				( void );		//color-->0
	void				FadeIn				( void );		//0-->color

	//过渡
	void				ProcessFading		( void );

	void				SetMoveData			(const VECTOR3D& vCenter
                                    ,float           fRadius
												,float           fAngleV);

	void			BlendRenderData( void );

public:
	static V3DSceneAnimation* GetLightObject( void );

protected:
	
	VG_TYPE_GET_PROPERTY	(LightInfo,		LIGHT_INFO);	//灯光数据
	VG_TYPE_GET_PROPERTY	(LightRender,	LIGHT_INFO);	//实际用于显示的灯光
	
	//绕着中心旋转
	struct sMOVE_INFO
	{
		
		Vector3X vCenter;							//中心点
		float		fRadius;
		float		fAngle;
		float		fAngleV;
	}m_MoveInfo;							

	VG_BOOL_PROPERTY	(ShowedBefore);		//上一帧是否显示
	VG_BOOL_PROPERTY	(Showed);				//是否正在显示中


	VG_BOOL_GET_PROPERTY	(FadeIn);			//淡入
	VG_BOOL_GET_PROPERTY	(Fading);			//正在淡入或淡出

	DWORD						m_dwLastFading;	//上次衰减的时间
	float						m_fFadingRate;		//每毫秒衰减多少
	float						m_fFadingPercent;

	
	VG_BOOL_PROPERTY		(NotSaveToScene);	//保存场景时，是否保存
	VG_BOOL_PROPERTY		(SystemLight);
	
	VG_INT2_PROPERTY		(LightPriority);	//灯光优先级，越高越优先

};

#include "V3DLight.inl"

#endif //__V3DLIGHT_H__


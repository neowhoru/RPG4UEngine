/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DTerrain.h
创建日期：2007年12月7日
最后更新：2007年12月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DTERRAIN_H__
#define __V3DTERRAIN_H__
#pragma once


#include "CommonDefine.h"
#include "V3DTerrainDefine.h"
#include "V3DTerrainVertex.h"
#include "V3DTerrainBlender.h"
#include "V3DTextureGroupSet.h"

class ImageTGA;
class V3DGameWorld;
class V3DGameMap;
class V3DTerrainVertex;
class V3DCompositeTexture;
using namespace threadutil;
struct sCHUNK_INFO;

typedef vector<V3DTerrainChunk*>				TerrainChunkArray;
typedef vector<V3DCompositeTexture*>		CompositeTextureArray;

#define TERR_COL_MAX		(m_dwCol*TILE_RES+1)
#define TERR_ROW_MAX		(m_dwRow*TILE_RES+1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DTerrain : SemiAutoCounterLock
{
	friend class V3DTerrainChunk;
	friend class V3DScene;
	friend class V3DGameWorld;
public:
	V3DTerrain();
	~V3DTerrain();

public:
	virtual BOOL	Create	( DWORD dwCol, DWORD dwRow );
	virtual void	Release	();

	virtual BOOL	_CreateChunkShadow	( );

	virtual UINT	UpdateTerrain	( const VECTOR3D& vHeroTilePos );
	virtual BOOL	NewMap			( int nWidth, int nHeight, BOOL bGen = TRUE );
	virtual void	CloseMap			();
	virtual void	ReLoadAll		();

	virtual void	UpdateChunk		( int nX, int nY );
	virtual void	UpdateLight		(BOOL bGenerateChunks=TRUE, DWORD dwUpdateLightFlags=UPDATELIGHT_ALL);
	virtual void	UpdateLightEx	();



	virtual BOOL   BuildingSceneGrass	();

public:
	BOOL				InsertAlphaLayer	(BYTE*  buffer
                                    ,size_t size
												,INT    nX
												,INT    nY
												,INT    nLvl
												,DWORD  dwFormat
												,INT    &nTextureID
												,FLOAT  &fOffsetU
												,FLOAT  &fOffsetV);

public:


	void				AddMesh				( V3DTerrainChunk *pChunk );
	void				GenerateChunk		();
	void				GenerateChunks		();

	///////////////////////////////////////////////
	void				DisposeRenderPipe	(TerrainChunkArray& pTerrainChunk
                                    ,BOOL               bUseTexture=TRUE
												,BOOL					  bLayerLayout=FALSE)		;

	void				PushRenderTask		(TerrainChunkIdxArray& pChunkNotTextureBase
												,TerrainChunkIdxArray& pBaseChunks
												,TerrainChunkIdxArray& pLayer0Chunks
												,TerrainChunkIdxArray& pLayer1Chunks
												,TerrainChunkIdxArray& pLayer2Chunks);
	void				PopRender			();

	BOOL				Render				(DWORD dwTick, BOOL bRenderTask);
	BOOL				RenderWater			(DWORD dwTick);
	BOOL				RenderLOD			(DWORD dwTick, BOOL bRenderTask);
public:
	virtual BOOL	SetPackName			(LPCSTR  szName );

public:
	virtual BOOL		RegTerrainTextrues	();
	virtual void		UnregTerrainTextrues	();

	virtual BOOL		ResisterShadowInfo(V3DTerrainBlender *pBlender);
	virtual void		BindLand				( V3DGameWorld *pLand , V3DGameMap* pMap);
	virtual BOOL		LoadFast				( LPCSTR  pszFilename );
	virtual BOOL		Flip					( eTERRAIN_FLIP_OPR oprType);
	virtual BOOL		Copy					( V3DTerrain *pTerrain, DWORD dwTerrainCopyState = TERRAIN_COPY_ALL );
	virtual void		ClearShadowMap		( void );

	virtual Vector3X	CalcNormal			( int x, int y );


protected:

	void		_CheckReadOnly			( LPCSTR szPath );
	Vector3X _GetMapCoord			( const VECTOR3D& vPos );

	virtual BOOL		Write			(DataChunkWriter *w, BOOL IsTitle = TRUE );
	virtual BOOL		ReadMap		(uDataChunkCursor cursorPointer
                                 ,int              InSize
											,int              nWidth
											,int              iHeight
											,char*            szTerrainSet
											,BOOL             bNeed=FALSE);//
	virtual BOOL		_ReadVertices	(uDataChunkCursor& cursorPointer
                                    ,Vector3D*         pVertexPoses		=NULL
												,Vector3D*         pVertexNormals	=NULL
												,COLOR*            pVertexDiffuses	=NULL
												,BOOL              bReadToExternal	=FALSE);//

	BOOL		_WriteTextureGroup	( DataChunkWriter *w );
	BOOL		_ReadTextureGroup		( uDataChunkCursor cursorPointer, int InSize );
	BOOL		_ReadGrass				( uDataChunkCursor cursorPointer, int InSize );
	BOOL		_WriteShadow			( DataChunkWriter *w );
	BOOL		_ReadShadow				( uDataChunkCursor InP, int size );

	BOOL		_ReadChunks				(uDataChunkCursor cursorPointer
                                 ,int              nSize
											,BOOL             bNeed);


	int		_ProjectIndices		(int   nCount
                                 ,WORD* pUsedVert
											,WORD* pSrcIndices
											,WORD* pProjIndices);

	BOOL			_UpdateVertexNormals();

public:
	int		SetLayerFloorTexture		(int              x
												,int              y
												,short            stTextureID
												,int					nEditLayer = 0);

	VOID		SetWorldMapXY		( INT nX, INT nY );
	VOID		GetWorldMapXY		( INT &nX, INT &nY );
	FLOAT		GetBaseX				( );
	FLOAT		GetBaseY				( );
	INT		GetBaseTileX		( );
	INT		GetBaseTileY		( );
	Vector3X	GetBasePos			( );
	FLOAT		GetMapSizeWidth	( );
	FLOAT		GetMapSizeHeight	( );

	void		TilePosToLocal		( int &x, int &y );
	void		PosToLocal			( float &x, float &y );
	BOOL		PointInMap			( const VECTOR3D& vPos );
	BOOL		IsVisible			( const VECTOR3D& vPos );

	Vector3X	GetNormal			( float x, float y );
	BBOX		GetFarRenderBox	( VOID );
	INT		GetRenderChunkHalf() ;

	V3DTerrainChunk*		GetChunk				( int col, int row );
	V3DTerrainBlender*	GetTextureBlender	( int x, int y, int nLayer );

	BOOL		LoadFromShadowMap		( LPCSTR  szFile );
	BOOL		SaveShadow				( LPCSTR  szFile );

public:
	BOOL		Save		( LPCSTR  pszFilename );
	BOOL		Load		( LPCSTR  pszFilename, BOOL bNeedLoad = FALSE );



public:




public:
	///////////////////////////////////
	void	SetCamera				(V3DCamera *pCamera);
	void	SetTileByMeter			( float fMeter );
	float	GetTileSize				();
	float	GetTileSizeOrigin		();

	DWORD GetTileCol				();
	DWORD GetTileRow				();
#ifdef USE_TERRAIN_VERTEX
#else
#endif

#ifdef USE_TERRAIN_VERTEX
	V3DTerrainVertex*			GetVerticesAt	(int x, int y );
	void							SetVertexAt		(int      x
                                          ,int      y
														,Vector3X vPos);
#else
	UINT							GetVertexAmount	();
	INT							GetVertexIndex		(int nTileX, int nTileY );
	Vector3D*					GetVertexPosAt		(int nTileX, int nTileY );
	void							SetVertexPosAt		(int			nTileX
															,int			nTileY
															,Vector3C&	vPos
															,BOOL			bOffset=FALSE);
	float							GetVertexPosZAt	(int			nTileX,int			nTileY);
	void							SetVertexPosZAt	(int			nTileX
															,int			nTileY
															,float		fHeight
															,BOOL			bOffset=FALSE);
	Vector3D*					GetVertexNormalAt	(int nTileX, int nTileY );
	void							SetVertexNormalAt	(int			nTileX
															,int			nTileY
															,Vector3C&	vNormal
															,BOOL			bOffset=FALSE);
	COLOR*						GetVertexDiffuseAt(int nTileX, int nTileY );
	void							SetVertexDiffuseAt(int			nTileX
															,int			nTileY
															,COLOR		clrDiffuse);
#endif

	///////////////////////////////////
	//tools
	BOOL	SplitAlphaMap			( LPCSTR  pszFilename );


	///////////////////////////////////
	BOOL	GetRayIntersectPixel	(const Vector3X* vSrc
                              ,const Vector3X* vDir
										,int             &x
										,int             &y
										,BOOL bBinDirect=TRUE);
	BOOL	GetRayIntersectPixel	(const Ray& ray
                              ,int        &x
										,int        &y
										,BOOL bBinDirect=TRUE);

	BOOL	GetRayIntersectPoint	(const Ray& ray
                              ,int        &x
										,int        &y
										,BOOL bBinDirect=TRUE);
	BOOL	GetRayIntersectPoint	(const Ray& ray
                              ,float      &x
										,float      &y
										,BOOL bBinDirect=TRUE);

	//取交点
	BOOL	GetRayCrossPoint		(const VECTOR3D& vSrc
										,const VECTOR3D& vDir
										,VECTOR3D        &vCross
										,BOOL bBinDirect=TRUE);		//Edit
	BOOL	GetRayCrossPoint		(const Ray&		  ray
										,VECTOR3D        &vCross
										,BOOL bBinDirect=TRUE);		//Edit


	//////////////////////////////////////////
	void	Pick	(V3DCamera*              pCamera
               ,Vector3X&               vFrom
					,Vector3X&               vDir
					,int                     nMouseX
					,int                     nMouseY);
	BOOL	Pick	(V3DCamera*              pCamera
               ,int                     nMouseX
					,int                     nMouseY
					,V3DTerrainIntersection* pvNearestIntersection);

	BOOL	Pick	(Vector3C&               vFrom
               ,Vector3C&               vDir
					,V3DTerrainIntersection* pvNearestIntersection
					,BOOL bBinDirect);
	BOOL	Pick	(const Ray&              ray
               ,V3DTerrainIntersection* pvNearestIntersection
					,BOOL bBinDirect)		;

	//////////////////////////////////////////
	BOOL	GetIntersection	(Vector3C*         pvFrom
                           ,Vector3C*         pvDir
									,V3DTerrainIntersection* pvNearestIntersection
									,BOOL  bBinDirect=TRUE);//双方向检测

	BOOL	GetIntersection	(const Ray&              ray
                           ,V3DTerrainIntersection* pvNearestIntersection
									,BOOL  bBinDirect=TRUE);//双方向检测



	//////////////////////////////////////////////////////
	float	GetHeightAt		(float     x
                        ,float     y
								,BOOL      bOverWater=false
								,BOOL      bLocal=false
								,DWORD*    pdwReturn=NULL
								,Vector3X* pvNormal=NULL);


	virtual void	CullTerrain	(const Vector3X *   pvEye
                              ,const Vector3X *   pvDir
										,const V3DFrustum * lpFrustum);
	virtual void	CullTerrain	(const Ray& ray
										,const V3DFrustum * lpFrustum);


	//////////////////////////////////////
	virtual Vector3X* GetDecal	(const Vector3X &vLeftBottom
                              ,const Vector3X &vLeftTop
										,const Vector3X &vRightTop
										,const Vector3X &vRightBottom
										,int            &iTriCount);







public:

	void			SetLayerFloorTexture			( int nIdx, int nID );
	int			GetLayerFloorTexture			( int nIdx );
	void			RemoveLayerFloorTexture		( int nID );

	void			SetLayerLayoutTexture		( int nID );
	int			GetLayerLayoutTexture		( );
	void			RemoveLayerLayoutTexture	( int nID );

	BOOL			ClearTerrainTextures			();
	int			GetLayerEffectTexture		( int nLayer, int nIdx );
	void			RemoveLayerEffectTexture	( int nLayer,int nIdx );
	void			SetLayerEffectTexture		( int nLayer, int nIdx, int nID );


	//取地图信息
	int			GetMapWidth				(void);
	int			GetMapHeight			(void);

	DWORD			GetDiffuse				( int x, int y );
	void			SetDiffuse				( int x, int y, DWORD dwColor );
	BOOL			BuildLightList			();

public:
	void			ProcssShadowMarker	(INT			nVerNum=0,INT nHorzNum=0);	//处理水印 rpg4u.com
	BOOL			_SaveShadowMapTo		(ImageTGA&  tga );
	void			_ProcssShadowMarker	(ImageTGA&  tga, INT			nVerNum=0,INT nHorzNum=0);	//处理水印 rpg4u.com
	BOOL			_GenerateShadow		(ImageTGA&  tga, BOOL bCheckColor)		;

  static void FilterHeightMapInBox	(DWORD   width
												,DWORD   height
												,float*& heightMap
												,BOOL    smoothEdges);

protected:
	VG_PTR_GET_PROPERTY		(BindLand,	V3DGameWorld);
	VG_PTR_GET_PROPERTY		(BindMap,	V3DGameMap);
	VG_DWORD_PROPERTY			(WaterColor);
	VG_SHORT_PROPERTY			(WaterID);
	VG_BOOL_GET_PROPERTY		(Loaded);
	VG_BOOL_GET_PROPERTY		(HeroInTerrain);
	VG_TYPE_GET_PROPERTY		(TextureSet,	V3DTextureGroupSet);
	VG_VECTOR_GET_PROPERTY	(HeroTilePos,	Vector3X);
	VG_BOOL_GET_PROPERTY		(ReadOnly);
	VG_INT_PROPERTY			(RenderChunk);
	VG_BOOL_PROPERTY			(Render);

	sTERRAIN_RAY				m_RayInfo;
	V3DModelBuilder			m_ModelBuilder;
	Vector2X						m_vOffset;
	Vector2X						m_ChunkMapStates	[MAX_RENDER_CHUNK_SIZE]
															[MAX_RENDER_CHUNK_SIZE];	

	BBOX							m_BBox;
	Vector3X						m_vCenter;
	VG_INT_GET_PROPERTY		(WorldMapX);
	VG_INT_GET_PROPERTY		(WorldMapY);

	BOOL							m_bUseFullTex;

	CompositeTextureArray	m_CompositeTextures;

	VG_DWORD_GET_PROPERTY	(Col);
	VG_DWORD_GET_PROPERTY	(Row);
	VG_INT_PROPERTY			(MaxLayer);
	VG_FLOAT_PROPERTY			(TerrainMaterial);
	VG_ARRAY_PROPERTY			(Vegetation,	V3DVegetation,  MAX_VEGETATION_AMOUNT);

	float							m_fTileSize;
	V3DTerrainChunk**			m_ppTerrainChunks;

#ifdef USE_TERRAIN_VERTEX
#else
#endif

#ifdef USE_TERRAIN_VERTEX
	VG_PTR_GET_PROPERTY		(Vertices, V3DTerrainVertex);
#else
	VG_PTR_GET_PROPERTY		(VertexPoses	,	Vector3D);
	VG_PTR_GET_PROPERTY		(VertexNormals	,	Vector3D);
	VG_PTR_GET_PROPERTY		(VertexDiffuses,	COLOR);
#endif
	VG_INT_GET_PROPERTY		(ShadowBaseId);

	POINT							m_ptSaveVerticesIndex;		//Edit
	V3DTerrainBlender *		m_pShadowBlender;
	V3DTerrainBlender			m_TerrainShadowBlender;

	VG_PTR_GET_PROPERTY		(SnippetVertices, INT);

	INT							m_LayerLayoutTextureID;
	int							m_LayerFloorTextureIDs	[TERRAIN_BLENDLAYER_MAX];
	int							m_LayerEffectTextureIDs	[TERRAIN_BLENDLAYER_MAX]
														[TERRAIN_BLENDLAYER_MAX];

	INT							m_AlphaTextureIDs			[TERRAIN_BLENDLAYER_MAX];
	INT							m_nShadowTextureID;

	VG_PTR_GET_PROPERTY		(Camera,		V3DCamera);
	VG_PTR_PROPERTY			(WalkMask,	V3DWalkMask);

	VG_INT_GET_PROPERTY		(Type);
	VG_CSTR_PROPERTY			(MiniMapFile, MAX_PATH);


protected:
	//地图的长宽
	int							m_iMapWidth,
									m_iMapHeight;

	sCHUNK_INFO*				m_pChunkInfos;
	VG_BOOL_PROPERTY			(RenderChunkState);



};//V3DTerrain



#include "V3DTerrain.inl"




#endif //__V3DTERRAIN_H__
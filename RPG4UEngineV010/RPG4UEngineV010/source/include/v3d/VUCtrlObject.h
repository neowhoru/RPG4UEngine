/*////////////////////////////////////////////////////////////////////////
文 件 名：VUCtrlObject.h
创建日期：2006年11月11日
最后更新：2006年11月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUCTRLOBJECT_H__
#define __VUCTRLOBJECT_H__
#pragma once


#include "CommonDefine.h"
#include "VUIObjectDefine.h"
#include "VUIControlStruct.h"
//#include "VUIFontDDraw.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API VUCtrlObject
{
public:
	VUCtrlObject(void);
	virtual ~VUCtrlObject(void);

public:
	typedef BOOL (*PROC_ONMSG_PROC)	(UINT   msg
                                    ,WPARAM wParam
												,LPARAM lParam
												,BOOL   bMsg); 


public:	
	virtual BOOL GetObjectType		(){return Type_Normal;}

	//////////////////////////////////////////////
	virtual BOOL Create		(const RECT*   pRect=NULL
                           ,VUCtrlObject* pParent=NULL
									,sUIINFO_BASE* pData=NULL);

	virtual void ReleaseUI	();
	virtual BOOL InitUIData	(VUCtrlObject* pParent, sUIINFO_BASE* pData );

	//////////////////////////////////////////////
	virtual BOOL GhostFrom	(VUCtrlObject* pParent
                           ,VUCtrlObject* pObject)		;

	virtual void GhostData	(sUIINFO_BASE* pDataTo
                           ,sUIINFO_BASE* pDataFrom)		;

	//////////////////////////////////////////////
	virtual BOOL	Render		(DWORD dwTick);
	virtual BOOL	FrameMove	(DWORD dwTick);

	virtual void	OnHide		(){}
	virtual void	OnUpdate		();
	virtual LPCSTR GetAlias		(){ return ""; }

	//////////////////////////////////////////////
	virtual BOOL IsEnableFocus	();
	virtual BOOL IsNeedRender	();	// 是否需要Render

	//////////////////////////////////////////
	//@param nMinAlpha  -1不使用Alpha检测，-2使用g_V3DConfig.GetUIMinAlpha()配置
	virtual BOOL PtInObject	(int			nPosX
                           ,int			nPosY
									,INT		nMinAlpha=PTCHECK_SETTING);

	virtual BOOL PtInObject	(const RECT& rc
                           ,int         nPosX
									,int         nPosY);

	virtual DWORD GetAlphaAt(int			nPosX
                           ,int			nPosY);

	// 设置是否可见/可用
	virtual void SetVisible	(BOOL bVisible
                           ,BOOL bFadeIn = FALSE
									,int	bColor  = 0xff);

	// 闪烁
	virtual void StartFlash	(int  nFlashCount
                           ,BOOL bAutoClose = FALSE
									,BOOL bFadeIn =    FALSE);


	/////////////////////////////////////////////////
	// 消息
	virtual BOOL MsgProc		(UINT					msg
                           ,WPARAM				wParam
									,LPARAM				lParam
									,BOOL					bMsg = FALSE);

	/////////////////////////////////////////////////
	virtual VUCtrlObject*	FindControl	(IN LPCSTR /*szControlID*/
                                       ,IN LPCSTR szFrameID=NULL)	{__UNUSED(szFrameID); return NULL;  }
	virtual VUCtrlObject*	FindFrame	(IN LPCSTR /*szFrameID*/ )	{ return NULL;  }

	virtual int	 GetArrangeMode			()	{ return 0;}
	virtual BOOL IsChild						(const VUCtrlObject* )	{ return FALSE; }

	/*////////////////////////////////////////////////////////////////////////
	// 鼠标移动消息
	// 鼠标滚轮消息
	// 鼠标左键按下消息
	// 鼠标右键按下消息
	// 鼠标左键松开消息
	// 鼠标右键松开消息
	// 鼠标左键双击消息
	// 鼠标右键双击消息
	/*////////////////////////////////////////////////////////////////////////
	virtual BOOL OnMouseMove		( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }
	virtual BOOL OnMouseWheel		( BOOL /*bUp*/,		INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }

	virtual BOOL OnLButtonDown		( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }
	virtual BOOL OnLButtonUp		( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }
	virtual BOOL OnLButtonDBClick	( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }

	virtual BOOL OnRButtonUp		( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }
	virtual BOOL OnRButtonDown		( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }
	virtual BOOL OnRButtonDBClick	( DWORD /*dwFlags*/, INT /*nMouseX*/, INT /*nMouseY*/ )		{ return FALSE; }

	/*////////////////////////////////////////////////////////////////////////
	// 键盘按下消息
	// 键盘松开消息
	// 字符消息
	/*////////////////////////////////////////////////////////////////////////
	virtual BOOL OnChar		( UINT /*nChar*/ )	{ return FALSE; }
	virtual BOOL OnKeyDown	( UINT /*nChar*/ )	{ return FALSE; }
	virtual BOOL OnKeyUp		( UINT /*nChar*/ )	{ return FALSE; }


	// 窗口是否被拖拽
	virtual BOOL IsLButtonDragging	()	{ return FALSE; }


public:
	
	virtual void	GetParentRect	(RECT* prc );			//得父区域

	virtual void	SetGlobalRect	(const RECT* prc );
	virtual void	GetGlobalRect	(RECT*      prc		//得绝对矩形
									      ,BOOL			bCalcMargin = FALSE)		;

	////////////////////////////////////////////////////////
	int	GetFontSize		();
	int	GetFontIndex	();
	void	SetFontIndex	(int nIndex)	;
	COLOR GetFontColor	();
	void  SetFontColor	(COLOR color);
	
	
	char*	GetControlID	();					//得到控件ID
	int	GetControlType	();					// 得到控件类型


	/////////////////////////////////////////////////
	sUIINFO_PIC*		GetPicInfo	();
	
	/////////////////////////////////////////////////
	// 设置当前显示图片
	void	SetPicInfo		(LPCSTR				szPicPath );
	BOOL	SetPicInfo		(sUIINFO_PIC*     pPic
                        ,BOOL					bRewriteData = FALSE)		;


	void	GetPos			(int& nPosX, int& nPosY, BOOL bGlobal = TRUE );
	void	SetPos			(int  nPosX, int  nPosY, BOOL bGlobal = TRUE );
	void	SetPosX			(int nPosX );// 设置X坐标
	void	SetPosY			(int nPosY );// 设置Y坐标


	// 设置宽度
	// 设置高度
	void	SetWidth		(int nW );
	void	SetHeight	(int nH );
	void  SetMargin	(INT left
                     ,INT right
							,INT top
							,INT bottom);

	void	SetBackColor(COLOR col
                     ,BOOL  bFrame=FALSE)		;
	
	BOOL	IsBeCovered	(VUCtrlObject* pCover );		// 是否被覆盖

	// 激活
	void	SetActivate	(BOOL bActivate = TRUE );
	BOOL	IsActivated	();
	BOOL  IsInGroup	();

	///////////////////////////////////////////////////////
	// 移动
	void	MoveTo			( int nPosX, int nPosY );
	BOOL	PlaySound		();
	void	SetupFadeInfo	(int nMaxFade, int nMinFade );


protected:
	// 画图
	void 	BitBlt	(int*   pPicID
                  ,LPCSTR szPicName
						,int    nDestX
						,int    nDestY
						,int    nDestW
						,int    nDestH
						,int    nSrcX
						,int    nSrcY
						,int    nSrcW
						,int    nSrcH
						,int    nDrawMode
						,COLOR  color=0xffffffff);

	BOOL	_GetAlphaAt	(int    nDestX
							,int    nDestY
							,int    nDestW
							,int    nDestH
							,int    nSrcX
							,int    nSrcY
							,int    nSrcW
							,int    nSrcH
							,int    nDrawMode
							,INT	  nPosX
							,INT	  nPosY
							,DWORD& dwAlphaRet);

	BOOL	_GetAlphaAt	(const RECT*	pDstRc
							,const RECT*	pSrcRc
							,const POINT&	ptPos
							,DWORD&			dwAlphaRet);

public:
	//////////////////////////////////////////////////
	BOOL					m_bNeedRemove;			// 是否需要释放资源
	SOUNDINDEX			m_SoundDefault;

protected:
	//////////////////////////////////////////////////
	VG_TYPE_PROPERTY		(ProcOnMsgProc, PROC_ONMSG_PROC);

	VG_PTR_GET_PROPERTY	(Data,	sUIINFO_BASE);	// 控件数据
	VG_PTR_GET_PROPERTY	(Parent,	VUCtrlObject);	// 父控件
	VG_TYPE_PROPERTY		(BackColor,	COLOR);		// 背景颜色(当没有图片时显示)
	VG_FLOAT_PROPERTY		(BackPicZ);		
	VG_BOOL_PROPERTY		(MsgHangUp);		
	VG_BOOL_PROPERTY		(Covered);					// 是否被覆盖
	VG_BOOL_GET_PROPERTY	(Visible);					// 是否可见
	int						m_nBackPicID;				// 当前背景图片ID
	string					m_strBackPicName;			// 当前背景图片名
	int						m_nFontIndex;				// 字体索引值
	BOOL						m_bDrawFrame;				// 是否画边框

	VG_TYPE_PROPERTY		(StrokeColor,	COLOR);
	VG_DWORD_PROPERTY		(StrokeType);

	sUIINFO_PIC*			m_pPicInfo;				// 当前图片


	VG_INT_GET_PROPERTY	(Width);		// 宽高
	VG_INT_GET_PROPERTY	(Height);
	
	sUIOBJECT_LAYOUT		m_Layout;
	sUIOBJECT_FLASH		m_Flash;	// 闪动

	////////////////////////////////////////
	// 淡入淡出
	VG_INT_PROPERTY	(BlendColor);
	VG_BOOL_PROPERTY	(FadeChildren);//设置淡入淡出效果对次对象是否有效
	BOOL					m_bFadeInOut;
	DWORD					m_dwFadeStartTime;
	int					m_nFadeMax;
	int					m_nFadeMin;

	////////////////////////////////////////
	// 移动
	BOOL					m_bMoving;
	int					m_nMoveTargetX;
	int					m_nMoveTargetY;
	DWORD					m_dwMoveFirstTime;
	BYTE					m_byAlphaNoBackPic;	///没背景图时，Alpha设为此值


	VG_BOOL_PROPERTY	(AllVisible);
	VG_INT_PROPERTY	(MarginLeft);
	VG_INT_PROPERTY	(MarginRight);
	VG_INT_PROPERTY	(MarginTop);
	VG_INT_PROPERTY	(MarginBottom);

	VG_DWORD_PROPERTY		(GroupID);
	VG_INT_PROPERTY		(IndexAtGroup);
	VG_BOOL_PROPERTY		(ForceFocus);
	VG_BOOL_PROPERTY		(CanFocus);

public:
	VG_LPARAM_PROPERTY(UserData);		//用户附加数据信息

#ifdef _DEBUG
public:
	BOOL m_debug_bHasWarning;		// 是否有警告信息
#endif
};


#include "VUCtrlObject.inl"


#endif //__VUCTRLOBJECT_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：AvatarDefine.h
创建日期：2006年9月27日
最后更新：2006年9月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __AVATARDEFINE_H__
#define __AVATARDEFINE_H__
#pragma once

#include "MeshDefine.h"
#include "AvatarReplacableTexture.h"


class IMeshParticleEmitter;
class IMeshRibbonEmitter;
class IV3DRenderable;
class IMeshModel;
class IV3DRenderable;
class IMeshAttachment;
class V3DMaterialAnimation;
class Avatar;
using namespace std;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace avatar
{
////////////////////////////////////////////////////////////////////////
enum
{
	 AVATAR_RIGHT_BACK			= _BIT(0)
	,AVATAR_LEFT_HAND				= _BIT(1)
	,AVATAR_RIGHT_HAND			= _BIT(2)
	,AVATAR_GROUND					= _BIT(3)
	,AVATAR_LEFT_HAND_SHIELD	= _BIT(4)
	,AVATAR_LEFT_BACK				= _BIT(5)
	,AVATAR_LEFT_BACK_SHIELD	= _BIT(6)
};



	///////////////////////////////////////////////
	struct _MESHLIB_API sAVATAR_EFFECT
	{
		IMeshParticleEmitter*	m_pParticle;
		IMeshRibbonEmitter*		m_pRibbon;
		IV3DRenderable*			m_pBillBoard;
		int							m_nType;
		DWORD							m_dwFlag;			// 装备位置,lh,rh,lback,rback

		sAVATAR_EFFECT();
	};


	///////////////////////////////////////////////
	struct _MESHLIB_API sEFFECT_CONTAINER
	{
		vector<sAVATAR_EFFECT> effects;

		void Destroy();
	};


	///////////////////////////////////////////////
	struct _MESHLIB_API sAVATAR_COMPONENT
	{
	public:
		//////////////////////////////////////////////
		sAVATAR_COMPONENT();
		void Clear();

		//////////////////////////////////////////////
		friend class Avatar;
	private:
		string								m_strName;

		//////////////////////////////////////////////
	public:
		int									m_nModelID;
		IMeshModel*							m_pMeshModel;
		V3DMaterialAnimation*			m_pMaterialEffect;
		vector<IV3DRenderable*>			m_Chunks;
		vector<DWORD>						m_Flags;
		int									m_nNumAttachBone;
		AvatarReplacableTexture			m_ReplacableTexture;
		IMeshAttachment*					m_pAttachments[MAX_ATTACH_BONE];
		BOOL									m_bIsAttachment;
		BOOL									m_bLeftHand;
		sEFFECT_CONTAINER					m_EffectContainer;
	};


	////////////////////////////////////////////////
	struct _MESHLIB_API sCREATE_PARAM
	{
		int		m_nComponentCount;
		char**	m_ppComponentNames;

		sCREATE_PARAM()
			:m_nComponentCount(0)
			,m_ppComponentNames(NULL)
		{
		}
	};

};

#endif //__AVATARDEFINE_H__
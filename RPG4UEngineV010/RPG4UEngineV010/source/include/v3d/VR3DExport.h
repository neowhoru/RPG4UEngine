/*////////////////////////////////////////////////////////////////////////
文 件 名：	BaseLibExport.h
创建日期：	2007.12.27
最后更新：	2007.12.27
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#pragma once

// disable: "<type> needs to have dll-interface to be used by clients'
// Happens on STL member variables which are not public therefore is ok
#pragma warning (disable : 4251)

// disable: "non dll-interface class used as base for dll-interface class"
// Happens when deriving from Singleton because bug in compiler ignores
// template export
#pragma warning (disable : 4275)




#ifdef _VR3D

	///////////////////////////////////
	//动态库输出
	#ifdef _USRDLL
		#define _VR3D_API __declspec( dllexport )
		#pragma message("   _VR3D_API __declspec( dllexport )")
	///////////////////////////////////
	//非动态输出
	#else
		#define _VR3D_API
		#define _VR3D_STATIC
		#pragma message("   _VR3D_API _LIB")
	#endif

#else

	#define USE_VR3D_STATIC

	///////////////////////////////////
	//静态库
	#ifdef USE_VR3D_STATIC
		#define _VR3D_API
		#pragma message("   USE_VR3D_STATIC")
		#ifdef _DEBUG
			#pragma comment(lib,"VR3DStaticD.lib")
		#else
			#pragma comment(lib,"VR3DStatic.lib")
		#endif

	///////////////////////////////////
	//动态库
	#else
		#define _VR3D_API __declspec( dllimport )
		#pragma message("   _VR3D_API __declspec( dllimport )")
		#ifdef _DEBUG
			#pragma comment(lib,"VR3DD.lib")
		#else
			#pragma comment(lib,"VR3D.lib")
		#endif
	#endif

#endif



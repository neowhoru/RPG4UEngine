/*////////////////////////////////////////////////////////////////////////
文 件 名：MeshAnimationCtrl.h
创建日期：2006年9月8日
最后更新：2006年9月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MESHANIMATIONCTRL_H__
#define __MESHANIMATIONCTRL_H__
#pragma once



#include "CommonDefine.h"
#include "ConstMesh.h"
#include "MeshAnimationCtrlDefine.h"
#include "MeshDefine.h"
#include "AnimationTimeLine.h"
#include "IMeshInc.h"
#include "MeshAnimationCtrlListener.h"
#include "AvatarDefine.h"
#include "V3DSpaceBox.h"
#include "MeshConfig.h"


class AnimationTimeLine;
class MeshConfig;
class MeshConfigSequence;
class V3DMaterialAnimation;
class MeshAnimationCtrl;
class MeshConfigAvatarSetting;
class Avatar;

using namespace avatar;
using namespace std;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _MESHLIB_API MeshAnimationCtrl
{
public:
	MeshAnimationCtrl();
	~MeshAnimationCtrl();

public:


public:
	void	Destroy();

public:
	virtual void	Render			(float fTransparent );
	virtual void	RenderSubset	(int           nSubset
                                 ,IV3DRenderer* pRenderer)		;
	virtual void	RenderShadow	(void );
	virtual void	RenderSkeleton	( );


public:
	virtual		void	Update				(DWORD            dwTime
                                       ,const Matrix4&   matrix
													,BOOL					bUpdateComponent = TRUE);

	virtual		void  UpdateManual		( DWORD dwTime );
	virtual		void	UpdateSubsets		(DWORD            dwTime
                                       ,const Matrix4&   matrix)		;

	virtual		void	UpdateMeshBody		(const VECTOR3D&		vPos
													,Vector3C&           vScale
													,float               fUpperBodyRotation
													,float               fLowerBodyRotation
													,MeshConfigSequence* pSeqUpperBody
													,float               fUpperBodyFactor
													,BOOL                bUpperBodyBlending
													,MeshConfigSequence* pSeqLowerBody
													,float               fLowerBodyFactor
													,BOOL                bLowerBodyBlending);

public:
	virtual		void	OnNextPlayTask		(){}
	virtual		void	OnTriggerPoint		( sTIMELINE_KEYINFO* , DWORD dwFlag = 0 ){__UNUSED(dwFlag);}
	virtual		BOOL	LoadFromFile		( LPCSTR  ){ return TRUE; }


public:
	BOOL InstallListener		(MeshAnimationCtrlListener* pListener
									,eANIMATION_CHECK           eCheckType=ANIM_CHECK_ALL);

protected:
	void _InitMesh				(INT nNum);
	void _DoAnimatingReady	(sMESH_ANIM_PLAYTASK& task, DWORD dwTime);
	void _DoAnimating			(sMESH_ANIM_PLAYTASK& task, DWORD dwTime);
	void _DoAnimatingEnd		(sMESH_ANIM_PLAYTASK& task, DWORD dwTime);


public:
	VECTOR3D		GetBonePosAtWorld	( int nBoneId );
	Matrix4X		GetBoneMatrices	( int nBoneId );

	void			UpdateMeshModel	(DWORD			dwBindType);
	void			BindMeshModel		(IMeshModel*	pMeshModel
                                 ,MeshConfig*	pMeshConfig
											,BOOL				bLoadToStudio
											,DWORD			dwBindType= MESH_BIND_ALL)		;

public:
	BOOL		PlayAnimation	(LPCSTR pszActionname
                           ,DWORD  dwTimePerLoop
									,float  fPercentStartAt=0.f)		;

	BOOL		PlayAnimation	(int   nStartFrameID
                           ,int   nEndFrameID
									,DWORD dwTimePerLoop
									,float  fPercentStartAt=0.f);


	DWORD		GetAnimationLength( LPCSTR  pszActionname );
protected:
	BOOL		_PlayAnimation			(sMESH_ANIM_PLAYTASK* pTask );

	void		_PushAnimation			(sMESH_ANIM_PLAYTASK* pTask );
	void		_PushUpperAnimation	( sMESH_ANIM_PLAYTASK* pTask );
	void		_PushLowerAnimation	( sMESH_ANIM_PLAYTASK* pTask );

	void		_ClearAnimations		();
	void		_ClearUpperAnimations();
	void		_ClearLowerAnimations();

	void		_ResetPlayTask		( sMESH_ANIM_PLAYTASK* pTask );

public:
	///> 更换装备模型，包括头发，脸，盔甲，武器等全部可以更换的部位
	BOOL	MountEquip					( int nPartID, IMeshModel* pMeshModel );
	BOOL	MountEquip					( int nPartID, int nModelId );

	///> 应用装备材质特效
	BOOL	ApplyEquipMaterialEffect( int nPartID, int nEffectID );


public:
	///> @name 更换模型
	BOOL	ReloadMesh			( IMeshModel* pMeshModel
									, MeshConfig* pMeshConfig
									, BOOL		  bLoadToStudio=FALSE);
	BOOL	ReloadMesh			( int nModelId , BOOL bLoadToStudio=FALSE);



	void	ReleaseMesh			();
	void	ReleaseAllEquips	();
	void	ReleaseEquip		( int nPartID );


public:


	void SetPosition		( float x, float y, float z );
	void SetPosition		( const VECTOR3D&  vPos);

	void ActivateEffects	( BOOL bEnable );


public:
	///> @name 光线检测类操作
	BOOL	IntersectVRay	(const V3DVector& vStart
                        ,V3DVector&			vHit);

	BOOL	IntersectRay	(const Ray&	ray,V3DVector& vHit);
	BOOL	IntersectRay	(const Ray&	ray,float&     t);
	BOOL	IntersectRay	(const Ray&	ray,sRAY_INFO& rayInfo);


public:
	///> @name 变换方面操作


	void CalcTransformMatrix( Matrix4& matTransform );


	/////////////////////////////////////////////
	///> @name 包围盒方面操作

	void		GetBoundingInfo	(INT& nWidth
                              ,INT& nLength
										,INT& nHeight
										,INT& nLift);
	void		UpdateLocalBBox	();
	void		UpdateWorldBBox	( const Matrix4& matrix );

	void		LimitLocalBBox		(float fMinRadius
                              ,float fMaxRadius
										,float fMinHeight
										,float fMaxHeight);


	///////////////////////////////////////////////
	///> @name Avatar方面操作
protected:
	void	_AvatarUpdate			(DWORD   dwTime
                              ,float*  pfModel);

	void	_AvatarRender			(IV3DRenderer* pRenderer
										,float         fAlpha);

	void	_AvatarRenderSubset	(sAVATAR_COMPONENT*	pComponent
										,IV3DRenderer*			pRenderer
										,float					fAlpha );

public:
	BOOL	ReleaseAvatarSetting	(MeshConfigAvatarSetting*	pSetting );
	BOOL	SetAvatarSetting		(LPCSTR							pszName
										,V3DFORMAT forceFormat = V3DFMT_UNKNOWN);
	BOOL	SetAvatarStudio		(LPCSTR							pszName);

public:
	///////////////////////////////////////
	///> @name 其它杂项处理
	
	sMESH_ANIM_PLAYTASK*		GetPlayTask( int nTask );

	BOOL			InitUpperBodyAndLowerBody		();
	void			CancelUpperBodyAndLowerBodyMode();


	///> 获取当前上半身正在播放的动画
	LPCSTR 		GetCurUpperAnim();
	int			GetCurUpperAnimType();

	///> 获取当前下半身正在播放的动画
	LPCSTR 		GetCurLowerAnim();
	int			GetCurLowerAnimType();

	///> 动画播放进度
	float			GetAnimationFactor();

	BOOL			IsIdleSequence		( MeshConfigSequence* pSequence );
	BOOL			IsCommonSequence	( MeshConfigSequence* pSequence );


	void			SetScale		(float fScale);
	void			SetRotationZ(float fAngle);
	void			SetRotationX(float fAngle);

	int			GetMaxLightCount();
	IMeshLight*	GetMeshLight( int  );



protected:
	///////////////////////////////////////////////////
	///> @name 动画播放监听器
	VG_PTR_GET_PROPERTY	(AnimatingListener,	MeshAnimationCtrlListener);
	SAnimationInfo			m_animatingInfo;

	VG_BOOL_PROPERTY		(Animated);				//
	VG_BOOL_PROPERTY		(NotUseTexture);				//
	VG_DWORD_PROPERTY		(RenderFlag);

	/////////////////////////////////////////////
	//AnimationTimeLine	m_timeLine;
	//上半身的动画时间
	VG_TYPE_GET_PROPERTY	(UpperBodyAnimTimeLine, AnimationTimeLine);
	VG_TYPE_GET_PROPERTY	(LowerBodyAnimTimeLine, AnimationTimeLine);

	VG_INT_GET_PROPERTY	(StartFrameId);
	VG_INT_GET_PROPERTY	(EndFrameId);
	VG_INT_GET_PROPERTY	(TriggerPointFrameID);
	int						m_nLastFrameID;
	DWORD						m_dwOneLoopTime;

	VG_INT_PROPERTY		(FrameId);
	VG_INT_GET_PROPERTY	(FrameCount);
	
	int						m_nLoopTimes;		///> 实际循环的次数
	int						m_nTriggerPointCount;

	int						m_nNumPlayTask;

	int						m_nNumUpperBodyPlayTask;
	int						m_nNumLowerBodyPlayTask;

	
	VG_FLOAT_PROPERTY		(UpperBodyFactor);///> 上半身的动画播放进度
	VG_FLOAT_PROPERTY		(LowerBodyFactor);///> 下半身的动画播放进度

	///>	上半身的动画任务
	sMESH_ANIM_PLAYTASK		m_UpperBodyPlayTask[MAX_ANIMATION_PLAY_TASK+1];

	///>	下半身的动画任务
	sMESH_ANIM_PLAYTASK		m_LowerBodyPlayTask[MAX_ANIMATION_PLAY_TASK+1];

	//以前全身的动画任务
	sMESH_ANIM_PLAYTASK		m_PlayTasks[MAX_ANIMATION_PLAY_TASK+1];


	///> 动画数据
	VG_PTR_GET_PROPERTY	(MeshModel,				IMeshModel);
	VG_PTR_PROPERTY		(MeshConfig,			MeshConfig);
	VG_PTR_GET_PROPERTY	(AvatarSetting,		MeshConfigAvatarSetting	);

	VG_TYPE_GET_PROPERTY	(AvatarTexture,		AvatarReplacableTexture);


	// 上半身的动作
	MeshConfigSequence*		m_pSeqUpperBody;
	int							m_nUpperBodyAnimType;

	// 下半身的动作
	MeshConfigSequence*		m_pSeqLowerBody;
	int							m_nLowerBodyAnimType;

	VG_BOOL_PROPERTY			(NeedLimitLocalBBox);
	VG_TYPE_GET_PROPERTY		(TmpSequence,	MeshConfigSequence);
	VG_VECTOR_GET_PROPERTY	(Min,				V3DVector);
	VG_VECTOR_GET_PROPERTY	(Max,				V3DVector);
	VG_TYPE_GET_PROPERTY		(WorldBBox,		V3DSpaceBox);



	IMeshParticleEmitter*		m_pParticleEmitters[MAX_PARTICLE_EMITTER];
	IMeshRibbonEmitter*			m_pRibbonEmitters[MAX_RIBBON_EMITTER];

	VG_PTR_GET_PROPERTY	(Matrices,			V3DMatrix);
	VG_PTR_GET_PROPERTY	(BoneMatrices,		V3DMatrix);
	VG_PTR_GET_PROPERTY	(SrcBoneMatrices,	V3DMatrix);

	/////////////////////////////////////////////////////
	V3DMatrix				m_matModel;
	V3DMatrix				m_matModelInv;

	//VG_TYPE_PROPERTY		(LocalBox, BBox3D);


	VG_VECTOR_PROPERTY	(Position, V3DVector);
	VG_VECTOR_PROPERTY	(Scale,    Vector3D);
	VG_VECTOR_PROPERTY	(Rotation, Vector3D);

	float						m_fFigureScale;	// 表格中填写的模型自身缩放比


	BOOL						m_bEnableRibbonEmitter;
	BOOL						m_bEnableParticleEmitter;

	VG_BOOL_GET_PROPERTY	(ExistEmitter);
	VG_INT_GET_PROPERTY	(ModelID);

	///> Avatar数据
	VG_PTR_PROPERTY		(Avatar, Avatar);
	int						m_nUpperBodyRootBoneID;	// 上半身根节点
	int						m_nBipBoneID;			// 模型的中心点


	vector<bool>			m_BodyBoneFlags;



	VG_VECTOR_GET_PROPERTY	(UpperBodyRootBonePos,	V3DVector);
	VG_VECTOR_GET_PROPERTY	(LowerBodyRootBonePos,	V3DVector);
	VG_VECTOR_GET_PROPERTY	(SpineBonePosOffset,		V3DVector);	//获取脊椎骨骼的偏移位置

	float							m_fCurUpperBodyFrame;
	float							m_fCurLowerBodyFrame;

	// 上下半身的旋转朝向
	VG_FLOAT_PROPERTY			(UpperRotation);
	VG_FLOAT_PROPERTY			(LowerRotation);

	//
	BOOL			m_bUpperBodyPlayOver;
	BOOL			m_bLowerBodyIdle;
	BOOL			m_bUpperBodyIdle;

	///> 上半身做完动作后的Blending
	BOOL						m_bUpperBodyBackBlending;
	DWORD						m_dwUpperBodyStartBackBlendingTime;

	VG_BOOL_GET_PROPERTY	(UpperBodyBlending);
	DWORD						m_dwUpperBodyStartBlendingTime;
	DWORD						m_dwUpperBodyBlendingStep;

	///> 下半身做完动作后的Blending
	BOOL						m_bLowerBodyBackBlending;
	DWORD						m_dwLowerBodyStartBackBlendingTime;

	VG_BOOL_GET_PROPERTY	(LowerBodyBlending);
	DWORD						m_dwLowerBodyStartBlendingTime;
	DWORD						m_dwLowerBodyBlendingStep;

	VG_BOOL_GET_PROPERTY	(UpperBodyAndLowerBodyMode);		///> 是否上下身类型的动画
	VG_BOOL_PROPERTY		(NeedInitUpperBodyAndLowerBody);	///> 是否要初始化上下半身

	VG_BOOL_GET_PROPERTY	(UpdateComponents);
	VG_BOOL_PROPERTY		(EffectAnim);

	float						m_fCurBodyFrame;

	DWORD						m_dwLightTrackDeadTime;
	FLOAT						m_fLightTrackAlpha;
	Vector3D*				m_pSkeletonVertices;

private:
	BOOL						m_bCanBlending;



};//class _MESHLIB_API MeshAnimationCtrl

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "MeshAnimationCtrl.inl"


#endif //__MESHANIMATIONCTRL_H__
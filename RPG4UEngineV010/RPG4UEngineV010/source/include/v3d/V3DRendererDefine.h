/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DRendererDefine.h
创建日期：2006年8月29日
最后更新：2006年8月29日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DRENDERERDEFINE_H__
#define __V3DRENDERERDEFINE_H__
#pragma once



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eRENDER_FLAG
{
	 RENDER_BOTH_SIDE				= _BIT(0)
	,RENDER_ALPHA_BLEND			= _BIT(1)
	,RENDER_ALPHA_TEST			= _BIT(2)
	
	,RENDER_DEST_BLEND_ONE		= _BIT(3)
	,RENDER_USE_TEXTURE_COLOR	= _BIT(4)
	,RENDER_SORT_FAR_Z			= _BIT(5)
	,RENDER_Z_FUNC_LESS			= _BIT(6)
	
	,RENDER_OP_MODULATE1X		= _BIT(8)
	,RENDER_OP_MODULATE2X		= _BIT(9)
	,RENDER_OP_MODULATE4X		= _BIT(10)
	,RENDER_OP_ADD					= _BIT(11)
	
	,RENDER_GLOBAL_TRANSPARENT	= _BIT(12)

	,RENDER_NOTNEED_DRAW			= _BIT(13)
	,RENDER_NOTNEED_SETIB		= _BIT(14)
	,RENDER_NOTNEED_SETVB		= _BIT(15)
	,RENDER_USING_VB				= _BIT(16)
	,RENDER_USING_IB				= _BIT(17)
	,RENDER_NOTNEED_SETTEXURE	= _BIT(18)
	,RENDER_ENABLE_SPECULAR		= _BIT(19)
	,RENDER_BE_TERRAIN			= _BIT(20)
	
   ,RENDER_BLUR_MIPMAP_SHIFT	= _BIT(22)
	,RENDER_Z_DISABLE				= _BIT(23)
	,RENDER_ZWRITE_DISABLE		= _BIT(24)

	,RENDER_FILTER_POINT			= _BIT(25)
	,RENDER_FILTER_LINEAR		= _BIT(26)

	,RENDER_CULL_CCW				= _BIT(27)
	,RENDER_WIREFRAME				= _BIT(28)
	,RENDER_LIGHT_DISABLE		= _BIT(29)

	,RENDER_MIPMAP_LINEAR		= _BIT(30)
	,RENDER_MIPMAP_NONE			= _BIT(31)
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
enum eSHADER_TYPE
{
	 SHADER_TERRAIN_NORMAL
	,SHADER_TERRAIN_BLEND
	,SHADER_TERRAIN_GRID
	,SHADER_TERRAIN_LIGHTMAP

	,SHADER_TERRAIN_GROUND
	,SHADER_TERRAIN_SPLATTING

	,SHADER_UI_DRAW
	,SHADER_2D_OBJECT

	,SHADER_MESH
	,SHADER_WALKMASK

	,SHADER_TERRAIN_NO_TEXTURE

	,SHADER_MAX
};

// Primitive types for DrawPrimitive.
enum ePRIMITIVE_TYPE
{
     V3DPT_POINTLIST             = 1
    ,V3DPT_LINELIST              = 2
    ,V3DPT_LINESTRIP             = 3
    ,V3DPT_TRIANGLELIST          = 4
    ,V3DPT_TRIANGLESTRIP         = 5
    ,V3DPT_TRIANGLEFAN           = 6
    ,V3DPT_FORCE_DWORD           = 0x7fffffff /* force 32-bit size enum */
};






#endif //__V3DRENDERERDEFINE_H__
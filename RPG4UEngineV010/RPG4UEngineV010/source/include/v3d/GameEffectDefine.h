/*////////////////////////////////////////////////////////////////////////
文 件 名：GameEffectDefine.h
创建日期：2008年1月17日
最后更新：2008年1月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEEFFECTDEFINE_H__
#define __GAMEEFFECTDEFINE_H__
#pragma once


class IGameEffectHelper;
class MdlEffectCtrl;


enum
{
	 GAMEEFFECT_BASE		= TAG('base')
	,GAMEEFFECT_MESH		= TAG('mesh')
	,GAMEEFFECT_SHOOT		= TAG('shot')
	,GAMEEFFECT_HITFLASH	= TAG('flsh')
	,GAMEEFFECT_CHAR		= TAG('char')
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace gameeffect
{

#define ONE_FRAME_TIME 33
	enum
	{
		eEffectBindPart_Body,
		eEffectBindPart_Head,
		eEffectBindPart_LeftHand,
		eEffectBindPart_RightHand,
		eEffectBindPart_Foot,
		eEffectBindPart_CastDummy,	///投射辅助点，调用者可改变
		eEffectBindPartMax,
	};

	enum
	{
		eTerminateMode_Life,
		eTerminateMode_WaitAllInstanceDead,
		eTerminateMode_WaitParentDelete,
		eTerminateMode_Loop,
		eTerminateModeMax
	};


	enum
	{
		eEmpty,
		ePoison,			// 中毒效果
		eStun,				// 眩晕
		eNpcTip,			// npc头顶的符号
		eEffectMax,
	};

	enum
	{
		eLight_Hit,
		eLight_State,
		eLightMax,
	};
	enum
	{
		eSkillCastMax = 32,
	};

	enum
	{
		eActionStand,			// 原地不动
		eActionFollow,			// 跟随
		eActionFollowPos,		// 仅位置跟随
		eActionTrace,			// 追踪
		eActionFall,			// 坠落
		eActionSpray,			// 原地喷射
		eActionMax,
	};

	enum
	{
		eTrackTypeLine,			// 直线轨迹
		eTrackTypeParabola,		// 抛物线轨迹
		eTrackTypeGravity,		// 重力轨迹
		eMaxTrackType,
	};

	struct _VR3DDLL_API Instance
	{
		int			nMode;
		//int			nStartLoc;
		int			nFrameID;				// 动画帧编号
		float		fFrameFactor;
		//
		Vector3X vBornPos;				// 诞生时的位置
		Vector3X vTargetPos;				// 目标位置
		Vector3X vPos;					// 当前位置
		BOOL		bUpdateBornPos;			// 更新出生位置
		float		fRot;					// 当前旋转
		float		fRotCorrect;			// 当前角度的修正
		//float		fBindRot;				// 绑定物的角度

		//
		DWORD		dwBornTime;				// 出生时间
		DWORD		dwLife;					// 生命
		BOOL		bVisible;				// 是否可见
		BOOL		bDead;					// 是否已经死亡
		//
		DWORD		dwDeadTime;				// 死亡时间
		DWORD		dwDeadRenderTime;		// 死亡渲染时间

		int			nNumLoop;				// 循环次数
		BOOL		bLoopForever;			// 一直循环
		BOOL		bRotateTogether;		// 一起旋转
		float		fScale;					// 当前缩放
		int			nBindWhere;				// 特效出发点绑定位置
		int			nTargetBindWhere;		// 特效目标点绑定位置
		//
		int			nAction;				// 动作类型，比如跟随，追踪等
		int			nActionTime;			// 动作执行时间
		BOOL		bDisapearAfterAction;	// action完成后是否消失
		int			nTrackType;				// 轨迹类型
		float		fParabolaHeight;		// 抛物线轨迹高度
		
		float		fFallHeight;			// 坠落的高度
		float		fGroundZ;				// 地面的z
		//

		IGameEffectHelper* pMaster;				// 出发的位置
		IGameEffectHelper* pTarget;				// 目标的位置

		//MeshAnimationCtrl* pAnimCtrl;
		MdlEffectCtrl* pEffectCtrl;

		int			nCarryNumber;			// 携带的数字，命中时出现

	public:
		Instance():
			nMode(eTerminateMode_Loop),
			dwBornTime(0),
			dwLife(0),
			bVisible(TRUE),
			bDead(FALSE),
			nNumLoop(1),
			bLoopForever(FALSE),
			fScale(1.0),
			fRotCorrect(D3DX_PI*0.5f),
			dwDeadTime(0),
			dwDeadRenderTime(0),
			fRot(0.0f),
			bUpdateBornPos(FALSE),
			//fBindRot(0.0f),
			nBindWhere(eEffectBindPart_Body),
			nTargetBindWhere(eEffectBindPart_Body),
			nAction(eActionFollow),
			nActionTime(0),
			bRotateTogether(FALSE),
			bDisapearAfterAction(FALSE),
			fParabolaHeight(0.0f),
			fFallHeight(0.0f),
			fGroundZ(0.0f),
			nFrameID(0),
			fFrameFactor(0),
			pMaster(NULL),
			pTarget(NULL),
			nCarryNumber(0),
			pEffectCtrl(NULL)
			{
				;
				
				;

			}
	};

};

#endif //__GAMEEFFECTDEFINE_H__
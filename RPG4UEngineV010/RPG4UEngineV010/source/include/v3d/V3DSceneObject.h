/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DSceneObject.h
创建日期：2006年8月17日
最后更新：2006年8月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DSCENEOBJECT_H__
#define __V3DSCENEOBJECT_H__
#pragma once

#include "CommonDefine.h"
#include "V3DSceneObjectDefine.h"
#include "V3DOctaTree.h"
#include "V3DGlobal.h"
#include "V3DSpaceBox.h"
#include "StudioObject.h"

using namespace std;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DSceneObject : public StudioObject
{
public:
	V3DSceneObject				(void);
	virtual ~V3DSceneObject	(void);

public:
	SCENEOBJECT_TYPE	GetFactoryType	();

public:
	V3DRenderPass*	GetRenderPass		(INT n);
	INT				GetRenderPassCount();
	VOID				AddRenderPass		(const V3DRenderPass& RenderPass)	;

public:
	virtual BOOL	CanSave		()		{return TRUE;}
	virtual BOOL	Update		(void) = 0;
	virtual BOOL	Render		(void)=0;



	virtual void	ReleaseStaticMesh()					{}

public:
	void	GetSceneXY			( INT &x, INT &y )		;
	void	SetWithScene		( INT nX, INT nY )	;

	virtual BOOL		CanDropToGroup		(void);	//是否可以放在当前位置

	virtual void		RectifyPos	( const Vector3X &vecPos );
	virtual void		SetPos		( const Vector3X &vecPos );
	virtual void		SetScale		( float		fScale );
	virtual void		SetScale		( Vector3C& vScale );
	virtual Vector3C	GetScale		();
	virtual void		SetRotationZ(float     fRot);
	virtual void		SetRotationX(float     fRot);
	virtual void		SetRotation (Vector3C& vRot);
	virtual Vector3C	GetPosition	( );
	virtual void		SetPosition	( Vector3C &vecPos );
	virtual Vector3C	GetRotation	();
	virtual void		ResetNeedRender	(void);														//裁减前调用

	virtual void		SetBBox		(const Vector3X& vecMin
										   ,const Vector3X& vecMax)		;	
	virtual void		GetBBox		( Vector3X& vecMin,Vector3X& vecMax );

	virtual void	UpdateCullObject		(void);
	virtual void	UpdateTraceDiffuse	(void);
	virtual void	UpdateTerrainHeight	(void);
	virtual void	CheckTerrainHeight	(void);


	////////////////////////////////////////////////////////////
	virtual void	GetAABB	( Vector3X& vecMin,Vector3X& vecMax );
	virtual V3DSpaceBoxC&	GetBBox				();
	virtual BBox3C&			GetWorldBBox		();
	virtual Matrix4C&			GetTransform		();
	virtual void				SetTransform		(Matrix4C& matTran);

	virtual BOOL	RayCast	(const Vector3X*  pvRay
                           ,const Vector3X*  pvDir
									,sRAY_INFO&			rayInfo);

	virtual BOOL	RayCast	(const Vector3X*  pvRay
                           ,const Vector3X*  pvDir
									,Vector3X *			pvHit = NULL);

	virtual BOOL	RayCast	(const Vector3X*	pvRay
                           ,const Vector3X*	pvDir
									,float&				t);

	virtual BOOL	RayCast	( const Ray& ray, Vector3X *pvHit = NULL );
	virtual BOOL	RayCast	( const Ray& ray, float& t );
	virtual BOOL	RayCast	( const Ray& ray, sRAY_INFO&			rayInfo );


	////////////////////////////////////////////////////////////
	virtual BOOL	IntersectTriangle	(const Vector3X *pVectex);
	virtual BOOL	IntersectTriangle	(const Vector3X* p0
                                    ,const Vector3X* p1
												,const Vector3X* p2);


public:
	VG_CSTR_PROPERTY			(Name,	MAX_SCENEOBJECT_NAME_LENGTH);	//对象名字
	VG_INT2_PROPERTY			(ObjectType);						//对象类型
	VG_INT2_PROPERTY			(ResType);							//对象类型
	VG_INT2_PROPERTY			(SceneID);							//对象在Scene中的ID
	VG_INT_PROPERTY			(FrameSerialNo);					
	VG_INT2_PROPERTY			(CityID);			//所属城市ID



	V3DSpaceBox					m_SpaceBox;
	BBox3D						m_BBox;
	Vector3D						m_vRotation;
	Vector3D						m_vScale;
	Vector3D						m_Position;

	VG_FLOAT_PROPERTY			(Alpha);
	VG_BOOL_PROPERTY			(NeedRender);			//是否通过了裁减		
	VG_BOOL_PROPERTY			(Animated);				//



	VG_BOOL_PROPERTY			(UpdateTerrainHeight);		//是否Z随着地面起伏改变
	VG_INT2_PROPERTY			(WorldDataID);				//在CWorldData中数据模板ID
	VG_TYPE_PROPERTY			(WorldDataGUID, GUID);	//在GameWorldResManager中数据模板ID
	VG_BOOL_PROPERTY			(OverWater);				//是否是浮在水面上的物体

	VG_BOOL_PROPERTY			(StaticModel);				//
	VG_BOOL_PROPERTY			(SaveStaticModel);				//
	VG_BOOL_PROPERTY			(ExistEmitter);				//

	VG_INT_GET_PROPERTY		(SceneX);				//
	VG_INT_GET_PROPERTY		(SceneY);				//
	VG_BOOL_PROPERTY			(NotTextureRender);	//
	VG_BOOL_PROPERTY			(TraceDiffuse);

protected:	
	SCENEOBJECT_TYPE			m_FactoryType;
	Vector3X						m_vPosUpdateHeight;			//上次更新Z的位置
	VG_BOOL_PROPERTY			(GlobalObject);				//全局物体，永远有效，不用裁减（不需要加入到树中）

	vector<V3DRenderPass>	m_RenderPass;
};


#include "V3DSceneObject.inl"

#endif //__V3DSCENEOBJECT_H__
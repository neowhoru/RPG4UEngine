/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DScene.h
创建日期：2006年8月30日
最后更新：2006年8月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DSCENE_H__
#define __V3DSCENE_H__
#pragma once

#include "CommonDefine.h"
#include "V3DSceneDefine.h"
#include "GameWorldResManager.h"
#include "V3DSceneObject.h"
#include "V3DFrustum.h"
#include "V3DSceneOctaTree.h"
#include "V3DWalkMask.h"
#include "V3DVegetation.h"
#include "ThreadMutex.h"
#include "V3DModelBuilder.h"
#include "V3DBufferChunker.h"
#include "V3DBspTree.h"
#include "LightDefine.h"




class V3DSceneProxyListener;
class V3D_CSceneSound;
class V3DScene;
class V3DSceneBuilding;
class V3DSceneAnimation;
class V3DGameWorld;
class V3DGameMap;
class V3DBspTree;
class V3DTerrainLight;
union uDataChunkCursor;

using namespace threadutil;
using namespace v3d;

typedef map< int,V3DSceneObject*>			SceneObjectMap;
typedef pair< int,V3DSceneObject*>			SceneObjectValue;
typedef SceneObjectMap::iterator				SceneObjectMapIt;
typedef vector<V3DTerrainLight*>				LightContainer;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DScene : public SemiAutoCounterLock
{
	friend class V3DGameWorld;
public:
	V3DScene(void);
	virtual ~V3DScene(void);



	/////////////////////////////////////////////
public:	
	BOOL	PushRenderTask	(SceneObjectArray& vtObject);
	BOOL	PopRender	();
	BOOL	SetPackName( char *szName ) ;

	/////////////////////////////////////////////
	BOOL					ProcessAlphas	( Vector3X &vPos, Vector3X &vDist );

	/////////////////////////////////////////////
public:
	BOOL				Raise					( INT nHeight );

	/////////////////////////////////////////////
	void				UpdateDayLightRender			();


	/////////////////////////////////////////////
public:
	V3DSceneObject*	AllocSceneObject	(SCENEOBJECT_TYPE type);
	void					FreeSceneObject	(V3DSceneObject* pObject);

	void RenderSceneObjects	();
	void RemoveSceneObject	( V3DSceneObject *pObject, BOOL bDestroy=TRUE );
	void RemoveSceneObject	( int iSceneID,	BOOL bDestroy=TRUE );
	BOOL AddSceneObject		( V3DSceneObject *pObject);

	void	GetAllBuildings	( SceneBuildingArray* pBuildingsRet );

	//使用GUID来标识对象
	V3DSceneObject*	CreateSceneObject	(GUID      iWorldDataID
                                       ,int       iSceneID=-1
													,Vector3X* pvHugePos=NULL
													,Vector3C  vRot  =Vector3D::ZERO
													,Vector3C  vScale=Vector3D::UNIT_SCALE
													,BOOL		  bUpdateHeight=TRUE);
	V3DSceneObject*	CreateProxyObject	(V3DSceneProxyListener*	pListener
													,float						fBBoxScale = 1.0f)		;
	V3DSceneObject*	CreateCityObject	(LPCSTR						szName
													,int							iSceneID = -1 );

	void				RemoveCityObject		(LPCSTR	szName );

	////////////////////////////////////////////////////
	V3DSceneObject*	FindSceneObjectByFile( LPCSTR szFileName );
	V3DSceneObject*	FindSceneObject		( LPCSTR szName );
	V3DSceneObject*	FindSceneObject		( int		iSceneID );


	V3DSceneObject*	GetCityObject			( LPCSTR szName );
	V3DSceneObject*	GetCityObject			( int iSceneID );

	////////////////////////////////////////////////////



	//
	BOOL				Render		(DWORD dwTick);
	BOOL				RenderGrass	(DWORD dwTick);
	void				Update		(const VECTOR3D&     vPlayer
										,const VECTOR3D&     vCamera
										,const VECTOR3D&     vCameraOrg
										,const V3DFrustum*	lpFrustum=0);
	void				Update		(const VECTOR3D&     vPlayer
										,const Ray&				ray
										,const V3DFrustum*	lpFrustum=0);
	void				UpdateBlockObjects(DWORD dwTick
                              ,RayC& cameraRay
										,float fBlockAlpha
										,DWORD dwBlockExpire);
protected:



	////////////////////////////////////////////
public:
	float	GetHeightAt	(float     fCurrentZ
                     ,float     d
							,float     x
							,float     y
							,DWORD     dwFlag
							,BOOL*     bInFloor=NULL
							,DWORD*    pdwRet=NULL
							,Vector3X* pvNormal=NULL);

	BOOL	IsPointInMultiFloors	(float z0
                              ,float z1
										,float x
										,float y);


	/////////////////////////////////////////////
	BOOL GetCollisionDistance	(const VECTOR3D&  vStart
										,const VECTOR3D&  vDir
										,float*           fDistToCollision
										,sBSP_INTERSECT** pIntersect=NULL);

	BOOL GetCollisionDistance	(const Ray&       ray
										,float*           fDistToCollision
										,sBSP_INTERSECT** pIntersect=NULL);


	/////////////////////////////////////////////
	//pick	


	V3DSceneObject*	Pick	(Vector3X*			pvOrigin
                           ,Vector3X*			pvDir
									,int					iPickMode = SCENEPICK_NORMAL
									,sRAY_INFO*			pRayInfo = NULL);

	V3DSceneObject*	Pick	(const Ray&			ray
									,int					iPickMode = v3d::SCENEPICK_NORMAL
									,sRAY_INFO*			pRayInfo = NULL)		;


	float				GetMeshHeight		( float x,float y );

	//是否和物件碰撞
	BOOL				IntersectBuilding	(const Ray&			ray);




	void			SetMaterial	(const COLOR4F& clrAmbient
									,const COLOR4F& clrDiffuse
									,const COLOR4F& clrSpecular
									,const COLOR4F& clrEmissive
									,float fPower);



private:
	///////////////////////////
	void				_InitFromTerrain			( void );
	void				_ReleaseScene				(void);
	void				_UpdateDecorateObjects	( const V3DFrustum& frustum );

	///////////////////////////
	BOOL				_SaveLogicInfos	( char *szFile );
	BOOL				_SaveResInfo		( LPCSTR szSceneName );

	///////////////////////////
	BOOL				_SaveLightInfo		( DataChunkWriter *w );
	BOOL				_LoadLightInfo		(uDataChunkCursor cursorPointer
                                    ,int              InSize)		;
	BOOL				_ReadLightInfo		(uDataChunkCursor cursorPointer
                                    ,int              InSize
												,int              nCnt);


	///////////////////////////
	BOOL				_ReadSenceObejct	(uDataChunkCursor pBuffer
                                    ,int              nSize)		;
	BOOL				_ReadNature			( uDataChunkCursor cursorPointer, int nSize );

	////////////////////////////////////////////////
	BOOL				_CreateFloorAndCollision();
	DWORD				_AllocID						(void);


	V3DSceneObject*	_CreateBuilding	(sWORLDINFO_BASE* pResInfo
                                       ,int              iSceneID
													,Vector3X*        pvHugePos
													,Vector3C&  vRot  
													,Vector3C&  vScale
													,BOOL		  bUpdateHeight);

public:
	////////////////////////////////////////////////
	virtual BOOL				LoadScene_V0	( LPCSTR szSceneName );
	virtual BOOL				LoadScene		( LPCSTR szSceneName );

	//保存 地形 物件 地图障碍 小地图
	virtual BOOL				SaveAllData		( LPCSTR szSceneName,BOOL bSaveShadow );	
	virtual BOOL				SaveScene		( LPCSTR szSceneName );

	virtual BOOL				NewScene			( int iWid,int iHei, BOOL bMult = false );
	virtual void				CloseScene		( BOOL bNeedShare = FALSE );


	////////////////////////////////////////////////
	virtual void					ClearSelectFlag	(void);
	virtual BOOL					Flip					( eTERRAIN_FLIP_OPR oprType);
	virtual BOOL					Copy					( V3DScene *pScene , DWORD dwTerrainCopyState = TERRAIN_COPY_ALL);
	virtual V3DVegetation&		GetVegetationAt	(INT nIdx)	;
	virtual void					BindLand				( V3DGameWorld *pLand, V3DGameMap* pMap );



public:
	static void InitDefaultScene();


protected:	
	//////////////////////////////////////////////////////
	SceneObjectArray*			m_ObjectsTreePipe;
	SceneObjectArray			m_Decorates;

	VG_TYPE_PROPERTY			(SunLightInfo,	LIGHT_INFO);
	V3DGameWorld*				m_pBindLand;
	V3DGameMap*					m_pBindMap;

	DWORD							m_dwFileVersion;
	VG_TYPE_GET_PROPERTY		(SceneInfoDat,	sSCENE_INFODATA);
	VG_TYPE_GET_PROPERTY		(SceneObjects, SceneObjectMap);
	SceneObjectMap				m_CityObjects;

	VG_PTR_PROPERTY			(CurSelObject, V3DSceneAnimation);
	VG_BOOL_PROPERTY			(RenderObj);
	VG_BOOL_PROPERTY			(NewTerrain);

	//static DWORD				ms_dwIDCounter;

	////////////////////////////////////////////////////
	//裁减
	V3DSceneOctaTree			m_OctTree;

	// 已接受场景管理，但是还没有加入到octree中对象
	// 当物体新加入到场景，即加入到队列
	// 当队列数量超过一定时， 则会全部转移到octree中，

	// 如果对象受到空间上修改，
	// 或缩放或偏移或旋转，就需从octree中删除
	// 加入到队列
	vector<V3DSceneObject*> m_ObjectsToOctTree;
	SceneObjectMap				m_ObjectGarbage;


	VG_TYPE_PROPERTY		(MeshBuffer,	V3DBufferChunker);
	VG_PTR_GET_PROPERTY	(ModelFloor,	V3DBspTree);
	VG_PTR_GET_PROPERTY	(Collision,		V3DBspTree);
	VG_PTR_PROPERTY		(Terrain,		V3DTerrain);

	VG_TYPE_PROPERTY		(MaskInfos,	V3DWalkMask);

	//摄像机的位置，物件将根据这个顺序简单排序，显示
	Vector3X					m_vCameraPos;
	Vector3X					m_vPlayerPos;
	BOOL						m_IsMult;

	V3DModelBuilder		m_ModelBuilder;
	BlockSceneMap			m_BlockObjects;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(V3DScene , _GAMEMAP_API);
//extern _GAMEMAP_API V3DScene& singleton::GetV3DScene();
#define theV3DScene  singleton::GetV3DScene()



#include "V3DScene.inl"


#endif //__V3DSCENE_H__
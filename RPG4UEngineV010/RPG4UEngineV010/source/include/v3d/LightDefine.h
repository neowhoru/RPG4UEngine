/*////////////////////////////////////////////////////////////////////////
文 件 名：LightDefine.h
创建日期：2006年1月4日
最后更新：2006年1月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __LIGHTDEFINE_H__
#define __LIGHTDEFINE_H__
#pragma once


enum  LIGHTTYPE
{
     LIGHT_POINT          = 1
    ,LIGHT_SPOT           = 2
    ,LIGHT_DIRECTIONAL    = 3
    ,LIGHT_FORCE_DWORD    = 0x7fffffff
} ;

struct COLOR4F
{
    float r;
    float g;
    float b;
    float a;
};

inline COLOR4F operator * (float f, COLOR4F& ref)
{
	COLOR4F clr={f*ref.r, f*ref.g, f*ref.b, f*ref.a};
	return clr;
}

inline COLOR4F operator + (COLOR4F& ref1, COLOR4F& ref2)
{
	COLOR4F clr={ref1.r+ref2.r, ref1.g+ref2.g, ref1.b+ref2.b, ref1.a+ref2.a};
	return clr;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _V3DGRAPHIC_API LIGHT_INFO
{
    LIGHTTYPE		Type;            /* Type of light source */
    COLOR4F			Diffuse;         /* Diffuse color of light */
    COLOR4F			Specular;        /* Specular color of light */
    COLOR4F			Ambient;         /* Ambient color of light */
    Vector3D      Position;         /* Position in world space */
    Vector3D      Direction;        /* Direction in world space */
    float         Range;            /* Cutoff range */
    float         Falloff;          /* Falloff */
    float         Attenuation0;     /* Constant attenuation */
    float         Attenuation1;     /* Linear attenuation */
    float         Attenuation2;     /* Quadratic attenuation */
    float         Theta;            /* Inner angle of spotlight cone */
    float         Phi;              /* Outer angle of spotlight cone */
	
	LIGHT_INFO();
};


struct MATERIAL_INFO 
{
    COLOR4F   Diffuse;        /* Diffuse color RGBA */
    COLOR4F   Ambient;        /* Ambient color RGB */
    COLOR4F   Specular;       /* Specular 'shininess' */
    COLOR4F   Emissive;       /* Emissive color RGB */
    float     Power;          /* Sharpness if specular highlight */
};


#endif //__LIGHTDEFINE_H__
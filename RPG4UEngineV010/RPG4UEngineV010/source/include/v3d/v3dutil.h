/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DUtil.h
创建日期：2008年4月19日
最后更新：2008年4月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DUTIL_H__
#define __V3DUTIL_H__
#pragma once


namespace util
{ 

_GRAPHIC_API DWORD	MakeColor					( float clrARGB[4], BOOL bFullAlpha = TRUE );

//-----------------------------------------------------------------------------
_GRAPHIC_API HRESULT LoadAndCreateShader		(LPDIRECT3DDEVICE8 m_pD3DDev
                                             ,LPCSTR            sPath
															,const DWORD*      pDeclaration
															,DWORD             Usage
															,BOOL              bType
															,DWORD*            pHandle);

_GRAPHIC_API HRESULT LoadSkinMeshVertexShader(LPDIRECT3DDEVICE8 pd3dDevice
                                             ,LPCSTR            szFileName
															,DWORD*            pdwVertexShader);

_GRAPHIC_API void DrawRenderToTexture			(LPDIRECT3DTEXTURE8 pTexture
                                             ,LPRECT             lpDestRect
															,LPRECT             lpSrcRect
															,int                nSrcWidth
															,int                nSrcHeight
															,float              z
															,DWORD              dwColor
															,DWORD              dwFlag);


//-----------------------------------------------------------------------------
_GRAPHIC_API BOOL DrawText							(short   index
                                             ,int     x
															,int     y
															,DWORD   dwColor
															,LPCTSTR strText);

//-----------------------------------------------------------------------------
_GRAPHIC_API void SetWorldMatrix					(float * matrix);

//-----------------------------------------------------------------------------
_GRAPHIC_API long SaveBmpFile						(LPCSTR filename
                                             ,long   sx
															,long   sy
															,long   xl
															,long   yl
															,BYTE*  bitmap);

//-----------------------------------------------------------------------------
_GRAPHIC_API void SaveSurfaceAsBmp				(LPDIRECT3DDEVICE8  pd3dDevice
                                             ,LPDIRECT3DSURFACE8 pBackBuffer
															,LPCSTR             szFileName
															,BYTE * pBuffer =   NULL);



//-----------------------------------------------------------------------------
_GRAPHIC_API LPCSTR			GetDXSDKMediaPath	();

_GRAPHIC_API HRESULT			FindMediaFile		(PCSTR strPath
                                             ,LPCSTR strFilename)																 ;


//-----------------------------------------------------------------------------
enum TIMER_COMMAND
{	
	  TIMER_RESET
	, TIMER_START
	, TIMER_STOP
	, TIMER_ADVANCE
	, TIMER_GETABSOLUTETIME
	, TIMER_GETAPPTIME
	, TIMER_GETELAPSEDTIME 
};

/*////////////////////////////////////////////////////////////////////////
// @param  Performs timer opertations. Use the following commands:
//          TIMER_RESET           - to reset the timer
//          TIMER_START           - to start the timer
//          TIMER_STOP            - to stop (or pause) the timer
//          TIMER_ADVANCE         - to advance the timer by 0.1 seconds
//          TIMER_GETABSOLUTETIME - to get the absolute system time
//          TIMER_GETAPPTIME      - to get the current time
//          TIMER_GETELAPSEDTIME  - to get the time that elapsed between 
//                                  TIMER_GETELAPSEDTIME calls
/*////////////////////////////////////////////////////////////////////////
_GRAPHIC_API FLOAT __stdcall TimerHandle( TIMER_COMMAND command );





};//namespace util





#endif //__V3DUTIL_H__
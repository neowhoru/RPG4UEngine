/*////////////////////////////////////////////////////////////////////////
文 件 名：VUCtrlManager.h
创建日期：2006年11月9日
最后更新：2006年11月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUCTRLMANAGER_H__
#define __VUCTRLMANAGER_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "VUIPictureManager.h"
#include "VUCtrlFrame.h"
#include "VUCtrlIconDrag.h"
#include "VUCtrlCheckBox.h"
#include "VUCtrlComboBox.h"
#include "VUCtrlScrollBarEx.h"
#include "VUCtrlText.h"
//#include "TextResDefine.h"
#include "VUCtrlManagerDefine.h"
#include "ConstSound.h"
#include "VUCtrlGraphicListener.h"


//using namespace uidraw;
class IV3DTexture;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API VUCtrlManager
{
public:
	VUCtrlManager(void);
	virtual ~VUCtrlManager(void);

public:
	////////////////////////////////
	typedef BOOL (*PROC_PLAYSOUND)	( SOUNDINDEX	sound);
	typedef void (*PROC_LOGINFO)		( LPCSTR			name );


public:
	////////////////////////////////
	void	Destroy		();
	void	ReleaseFonts	();
	void	ReloadFonts		();

	void	FrameMove	(DWORD dwTick);

	void	Render		(DWORD dwTick);
	void	RenderTips	(DWORD dwTick);

	////////////////////////////////
	void	InitProcInfo	(VUCtrlGraphicListener*	pDrawer);



public:
	BOOL	MsgProc	(HWND   hWnd
                  ,UINT   msg
						,WPARAM wParam
						,LPARAM lParam);

	/////////////////////////////
	void	SetPlaySoundFun(PROC_PLAYSOUND	pFun );
	BOOL	PlaySound		(SOUNDINDEX			sound );


protected:
	DWORD	_UpdateTopFrame	();

public:
	/////////////////////////////
	BOOL	AddFrame				(VUCtrlObject* pObject
									,BOOL             bAddToTop=TRUE
									,BOOL             bAllVisable=FALSE);
	DWORD	AddFrame				(LPCSTR	  szFileName
                           ,BOOL      bToTop=TRUE
									,BOOL      bAllVisable=FALSE);

	VUCtrlFrame* LoadFrame	(LPCSTR szFileName
                           ,BOOL      bToTop=TRUE
									,BOOL      bAllVisable=FALSE
									,LPCSTR    szNewName=NULL);

	BOOL	RemoveFrame			(LPCSTR			szFileName );
	BOOL	RemoveFrame			(const VUCtrlObject* pObject );


	/////////////////////////////////////////////////////
	VUCtrlObject* FindControl		(LPCSTR szFrameID
                                 ,LPCSTR szControlID)		;

	VUCtrlObject* FindFrame			(LPCSTR szFrameID );

	VUCtrlObject* CreateWidget		(sUIINFO_BASE*   sBaseData
											,VUCtrlObject*		pParent)		;

	/////////////////////////////
	// 是否有模式窗口
	BOOL	ExistModalFrame	();

public:
	///////////////////////////////////////////////////
	// Frame
	BOOL OnFrameRun		(LPCSTR              szFrameID
								,VUCtrlFrame::PROC_ONFRAMEMOVE pFun)		;

	BOOL OnFrameRender	(LPCSTR						szFrameID
								,VUCtrlFrame::PROC_ONRENDER pFun
								,BOOL                      bBeforeRender=FALSE)		;

	// Button
	BOOL	OnButtonClick	(LPCSTR                         szFrameID
                        ,LPCSTR                         szControlID
								,VUCtrlButton::PROC_ONBUTTON_CLICK pFun);
	// Edit
	BOOL OnEditEnter		(LPCSTR                 szFrameID
                        ,LPCSTR                 szControlID
								,VUCtrlEdit::PROC_ONENTER pFun);
	// CheckBox
	BOOL OnCheckBoxCheck	(LPCSTR                     szFrameID
                        ,LPCSTR                     szControlID
								,VUCtrlCheckBox::PROC_ONCHECK pFun);

	// List
	BOOL OnListSelectChange	(LPCSTR                      szFrameID
                           ,LPCSTR                      szControlID
									,VUCtrlList::PROC_ONSELECTCHANGE pFun);

	BOOL OnListLDBClick			(LPCSTR                  szFrameID
                              ,LPCSTR                  szControlID
										,VUCtrlList::PROC_ONLBUTTON_DBCLICK pFun);

	// ListImg / ListEx
	BOOL OnIconDragFrom	(LPCSTR								szFrameID
								,LPCSTR								szControlID
								,VUCtrlIconDrag::PROC_ONDRAGFROM	pFun);

	BOOL OnIconDropTo	(LPCSTR                    szFrameID
                     ,LPCSTR                    szControlID
							,VUCtrlIconDrag::PROC_ONDROPTO pFun);

	BOOL OnIconLDBClick	(LPCSTR                      szFrameID
                        ,LPCSTR                      szControlID
								,VUCtrlIconDrag::PROC_ONLBUTTON_DBCLICK pFun);

	BOOL OnIconRButtonUp	(LPCSTR                       szFrameID
								,LPCSTR                       szControlID
								,VUCtrlIconDrag::PROC_ONRBUTTON_UP pFun);


	// ScrollBar
	BOOL OnScrollBarUpdatePos		(LPCSTR                        szFrameID
                                 ,LPCSTR                        szControlID
											,VUCtrlScrollBar::PROC_ON_UPDATEPOS pFun);

	// ScrollBar
	BOOL OnScrollBarExUpdatePos	(LPCSTR                        szFrameID
                                 ,LPCSTR                        szControlID
											,VUCtrlScrollBar::PROC_ON_UPDATEPOS pFun);

	// ComboBox
	BOOL OnComboBoxChange			(LPCSTR                      szFrameID
                                 ,LPCSTR                      szControlID
											,VUCtrlComboBox::PROC_ONCHANGE pFun);

	//text hyber click
	BOOL OnTextHyperLinkClick		(LPCSTR                    szFrameID
                                 ,LPCSTR                    szControlID
											,VUCtrlText::PROC_ON_HYPERLINK_CLICK pFun);

public:
	//////////////////////////////////////////////////////
	void				ReloadUI			();
	void				UpdateControls	();		// 排序

	VUCtrlObject*	BringToTop		( const VUCtrlObject* pObject );
	VUCtrlObject*	BringToBottom	( const VUCtrlObject* pObject );

	VUCtrlObject*	GetFrameFocus	();




private:
	void _UpdataFrame	( int nPosX, int nPosY );
	void Cover			();

public:
	VUCtrlObject*			m_pParentFocus;
	POINT						m_MousePos;

public:
	static SOUNDINDEX		m_SoundBtnClick;
	static SOUNDINDEX		m_SoundMouseOver;
	static SOUNDINDEX		m_SoundCheckBox;
	static SOUNDINDEX		m_SoundIconDrag;
	static SOUNDINDEX		m_SoundList;
	static SOUNDINDEX		m_SoundScrollBar;
	static SOUNDINDEX		m_SoundClose;

	static PROC_LOGINFO	m_pLogInfoFun;

protected:
	VG_BOOL_GET_PROPERTY	(MouseInUI);
	VG_PTR_PROPERTY		(Focus,  VUCtrlObject);
	BOOL						m_bDragFrame;
	PROC_PLAYSOUND			m_pPlaySoundFun;
	UIControlArray			m_arUICtrlArray;
	VG_PTR_PROPERTY		(TextureAlphaHelper	, IV3DTexture);
	VG_PTR_GET_PROPERTY	(Drawer					,VUCtrlGraphicListener);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(VUCtrlManager , _VRUIDLL_API);
//extern _VRUIDLL_API VUCtrlManager& singleton::GetVUCtrlManager();
#define theVUCtrlManager  singleton::GetVUCtrlManager()
#define theUICtrlManager  singleton::GetVUCtrlManager()
#define theUICtrlDrawer   (*::theUICtrlManager.GetDrawer())



#endif //__VUCTRLMANAGER_H__
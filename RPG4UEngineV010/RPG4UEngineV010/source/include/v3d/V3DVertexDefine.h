
/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DVertexDefine.h
创建日期：2006年11月16日
最后更新：2006年11月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DVERTEXDEFINE_H__
#define __V3DVERTEXDEFINE_H__
#pragma once


#include "ConstVertex.h"

namespace v3d
{
	template<class TYPE>
	inline WORD GetVertexSize  (const TYPE* dat)
	{
		return sizeof(*dat);
	}
	template<class TYPE>
	inline WORD GetVertexSize  (const TYPE& dat)
	{
		return sizeof(dat);
	}
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _VERTEX_DECLARE(TYPE,FLAG) struct TYPE; \
						namespace v3d{\
							const DWORD VERTEX_FMT_##TYPE =  FLAG ;\
							inline DWORD GetVertexFormat(const TYPE*) { return FLAG; }\
							inline DWORD GetVertexFormat(const TYPE&) { return FLAG; }\
						};\
						struct TYPE 




///////////////////////////////////////////
const DWORD FVF_VERTEX2D_UV	= V3DFVF_XYZRHW|V3DFVF_DIFFUSE|V3DFVF_TEX1;
const DWORD FVF_VERTEX3N_UV2	= V3DFVF_XYZ|V3DFVF_NORMAL|V3DFVF_DIFFUSE|V3DFVF_TEX2;

///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX2D_PT4, V3DFVF_XYZRHW|V3DFVF_DIFFUSE)
{
	Vector4X p;
	union
	{
		DWORD		color;
		DWORD		dwColor;
	};
};

///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX2D_4UV, V3DFVF_XYZRHW|V3DFVF_DIFFUSE|V3DFVF_TEX1)
{
	Vector4X p;
	union
	{
		DWORD		color;
		DWORD		dwColor;
	};
	float		u, v;
};


///////////////////////////////////////////
struct VERTEX2D_ITSUV
{
	Vector2D		p;	// 交点坐标
	int			idx;	// 三角形边编号
	float			t;	// 交点相对三角形边的参数t
	float			s;	// 顶点落在三角形内部时，一个点要靠三角形的两条边来描述，和t配套使用，
	float			u, v;	// 交点相对矩形的uv坐标，贴图使用
};



///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX3D_PT, V3DFVF_XYZ)
{
	Vector3X p;
} ;

///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX3N_PT, V3DFVF_XYZ|V3DFVF_NORMAL)
{
	union
	{
		struct
		{
			Vector3X		p;
			Vector3X		n;
		};
		struct
		{
			Vector3X		point;
			Vector3X		normal;
		};
		struct
		{
			Vector3X		vPos;
			Vector3X		vNormal;
		};
	};
} ;


///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX3D, V3DFVF_XYZ|V3DFVF_DIFFUSE)
{
	union
	{
		struct
		{
			Vector3X p;
			DWORD		color;
		};
		struct
		{
			Vector3X point;
			DWORD		color;
		};
		struct
		{
			Vector3X vPos;
			DWORD		dwColor;
		};
	};
} ;


///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX3D_UV, V3DFVF_XYZ|V3DFVF_DIFFUSE|V3DFVF_TEX1)
{
	union
	{
		struct
		{
			Vector3X p;
			DWORD		color;
			float		u,v;
		};
		struct
		{
			Vector3X p;
			DWORD		color;
			Vector2F	uv;
		};
		struct
		{
			Vector3X point;
			DWORD		color;
			Vector2F	texCood;
		};
		struct
		{
			Vector3X vPos;
			DWORD		dwColor;
			Vector2F	vTexCood;
		};
	};

} ;


///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX3N_UV, V3DFVF_XYZ|V3DFVF_NORMAL|V3DFVF_DIFFUSE|V3DFVF_TEX1)
{
	union
	{
		struct
		{
			Vector3X p;
			Vector3X n;
			DWORD		color;
			float		u,v;
		};
		struct
		{
			Vector3X p;
			Vector3X n;
			DWORD		color;
			Vector2F	uv;
		};
		struct
		{
			Vector3X point;
			Vector3X normal;
			DWORD		color;
			Vector2F	texCood;
		};
		struct
		{
			Vector3X vPos;
			Vector3X vNormal;
			DWORD		dwColor;
			Vector2F	vTexCood;
		};
	};


};

///////////////////////////////////////////
_VERTEX_DECLARE(VERTEX3N_UV2, V3DFVF_XYZ|V3DFVF_NORMAL|V3DFVF_DIFFUSE|V3DFVF_TEX2)
{
	union
	{
		struct
		{
			Vector3X p;
			Vector3X n;
			DWORD		color;
			float		u,v;
			float		u1,v1;
		};
		struct
		{
			Vector3X p;
			Vector3X n;
			DWORD		color;
			Vector2F	uv;
			Vector2F	uv1;
		};
		struct
		{
			Vector3X point;
			Vector3X normal;
			DWORD		color;
			Vector2F	texCood;
			Vector2F	texCood1;
		};
		struct
		{
			Vector3X vPos;
			Vector3X vNormal;
			DWORD		dwColor;
			Vector2F	vTexCood;
			Vector2F	vTexCood1;
		};
	};
};


#endif //__V3DVERTEXDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：GameWorldResManagerDefine.h
创建日期：2007年7月20日
最后更新：2007年7月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMEWORLDRESMANAGERDEFINE_H__
#define __GAMEWORLDRESMANAGERDEFINE_H__
#pragma once


/////////////////////////////////////////////////////////
#include "ConstArray.h"
#include "WorldConst.h"
#include "SceneResDefine.h"

const DWORD MAX_RESDATA_NAME  = 127;
const DWORD MAX_RESDATA_DESC  = 127;

/////////////////////////////////////////////////////////
#define WALK_MASK_SIZE			MAPTILESIZE
#define WALK_MASK_SIZE_HALF	WALK_MASK_SIZE/2


///////////////////////////////////////////
enum eMOVE_FLAG
{
	 MOVEFLAG_NONE				= 0
	,MOVEFLAG_CANWALK			= 1
	,MOVEFLAG_NOWALK			= 2
};

///////////////////////////////////////////
enum ePIC_MOVE_FLAG
{
	 PIC_MOVEFLAG_NONE		= COLOR_RGB( 0,0,0 )
	,PIC_MOVEFLAG_CANWALK	= COLOR_RGB( 0,0,255 )
	,PIC_MOVEFLAG_NOWALK		= COLOR_RGB( 255,0,0 )
};

///////////////////////////////////////////
enum eRES_DATA_TYPE
{
	 RESDATATYPE_BUILDING
	,RESDATATYPE_SOUND
	,RESDATATYPE_HUGEOBJECT
	,RESDATATYPE_TREE
	,RESDATATYPE_DECORATE
	,RESDATATYPE_ANIMATION
	,RESDATATYPE_INTERFACE
	,RESDATATYPE_DUMMY
};


enum
{
	 LIB_ICON_FOLDER
	,LIB_ICON_SUBFOLDER
	,LIB_ICON_DOC
	,LIB_ICON_SOUND
	,LIB_ICON_WINDOW
	,LIB_ICON_TREE
	,LIB_ICON_PAGE
	,LIB_ICON_PROPERTY
	,LIB_ICON_TEXT
	,LIB_ICON_OTHER
	,LIB_ICON_DOC2
	,LIB_ICON_SOUND2
	,LIB_ICON_WINDOW2
	,LIB_ICON_TREE2
	,LIB_ICON_PAGE2
	,LIB_ICON_PROPERTY2
	,LIB_ICON_TEXT2
	,LIB_ICON_OTHER2
	,LIB_ICON_MAX
};



/////////////////////////////////////////////////////////






////////////////////////////////////////////////////////
enum eBUILDING_STYLE
{
     //普通建筑
	 BUILDING_STYLE_NORMAL			= 0
	,BUILDING_STYLE_0		//
	,BUILDING_STYLE_1		//
	,BUILDING_STYLE_2		//
	,BUILDING_STYLE_MAX
};


CONSTARRAY_DECLARE	(BuildingType
                     ,DWORD
							,0
							,BUILDING_STYLE_MAX
							,_GAMEMAP_API);





/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#pragma pack(push,1)

/////////////////////////////////////////////////
struct _GAMEMAP_API sWORLDRES_INFO
{
	sWORLDRES_INFO()
	{
		nResDataType = 0;
		__ZERO(szName);
		__ZERO(szDesc);
		dwReserve1 = 0;
		dwReserve2 = 0;
	};

	GUID		infoGUID;
	char		szName[MAX_RESDATA_NAME];
	char		szDesc[MAX_RESDATA_DESC];
	int		nResDataType;
	DWORD		dwReserve1;
	DWORD		dwReserve2;
};


///////////////////////////////////////////////////
struct _GAMEMAP_API sWORLDRES_MESH
{	
	sWORLDRES_MESH()
	{
		fInitRotation		= 1.5;		//初始旋转Init rot
		bCanRotation		= true;		//是否可以旋转Can rot
		fScaleMin			= 1.0f;	//等比缩放，最小值Scale min
		fScaleMax			= 1.5f;	//等比缩放最大值Scale max
		bShowInMiniMap		= true;	//是否在小地图上显示show in MM
		miniMapColorRed	= 100;	//r
		miniMapColorGreen	= 100; //g
		miniMapColorBlue	= 100;	//b
		iBuildingType		= BUILDING_STYLE_NORMAL;
		bOverWater			= false;//是否是浮在水上的物体bOverWater
		bParticleEmitter				= false;
		bCreateShadow		= false;
		dwReserve1			= 0;
		dwReserve2			= 0;

		__ZERO(szWalkPath);
		__ZERO(szModelPath);
		//i
	};

	char		szModelPath	[MAX_PATH];
	char		szWalkPath	[MAX_PATH];
	float		fInitRotation;			//fInit是物体初始旋转
	BOOL		bCanRotation;
	float		fScaleMin;
	float		fScaleMax;
	BOOL		bShowInMiniMap;
	UINT		miniMapColorRed,
				miniMapColorGreen,
				miniMapColorBlue;
	int		iBuildingType;
	BOOL		bOverWater;
	BOOL		bParticleEmitter;		//是否粒子发生器
	BOOL		bCreateShadow;//是否产生Shadow
	DWORD		dwReserve1;
	DWORD		dwReserve2;
};

////////////////////////////////////////////////
struct _GAMEMAP_API sWORLDRES_SOUND_INFO
{	
	sWORLDRES_SOUND_INFO()
	{
		szWaveFile[0]	= 0;
		fEffectRange	= 1000.0f;
		bCanLoop			= false;
		nPlayPeriod		= 0;
		fDistMin			= 10;
		fDistMax			= 1000;
		bEffectGlobal	= false;			
	}

	char		szWaveFile[MAX_PATH];
	float		fEffectRange;
	BOOL		bCanLoop;
	int		nPlayPeriod;
	float		fDistMin;
	float		fDistMax;
	BOOL		bEffectGlobal;
};

#pragma pack( pop ) 


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct sWORLDINFO_BASE
{
	GUID		infoGUID;
	char *	szName;
	char *	szDesc;
	int		nResDataType;
};


//////////////////////////////////////
struct _GAMEMAP_API sWORLDINFO_WALKMASK
{
	char	*szMaskInfoFile;
	BYTE	*pMaskBuf;				//mask 数据
	int	iMaskWidth;
	INT	iMaskHeight;
	BOOL	bBlankMask;				//不占格
	BOOL	bWalkableMask;			//有可行走的mask
										//这种可能是桥等特殊建筑

	void CheckMaskAt			(Vector3X &vPos );

	void GetMaskStartTile	(Vector3X &vPos
                           ,int      &xBegin
									,int      &yBegin);

	void GetMaskCenterTile	(Vector3X &vPos
                           ,int      &xCenter
									,int      &yCenter);
};

/////////////////////////////////////////////////
struct sWORLDINFO_BUILDING : public sWORLDINFO_BASE
{
	char*							szMeshFile;		
	sWORLDINFO_WALKMASK	*	pMaskInfo;
	
	float							fInitRotation;//物体初始旋转
	BOOL							bCanRotation;
	float							fScaleMin;
	float							fScaleMax;
	BOOL							bShowInMiniMap;
	DWORD							dwColorInMM;
	UINT							miniMapColorRed,
									miniMapColorGreen,
									miniMapColorBlue;
	int							iBuildingType;
	BOOL							bOverWater;
	BOOL							bParticleEmitter;	//是否粒子发生器
	BOOL							bCreateShadow;		//是否产生Shadow
};

///////////////////////////////////////////////////
struct sWORLDINFO_SOUND : public sWORLDINFO_BASE
{
	char		*	szWaveFile;
	float			fEffectRange;
	BOOL			bCanLoop;
	int			nPlayPeriod;
	float			fDistMin;
	float			fDistMax;
	BOOL			bEffectGlobal;
};


#endif //__GAMEWORLDRESMANAGERDEFINE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DMaterialAnimationDefine.h
创建日期：2006年10月5日
最后更新：2006年10月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DMATERIALANIMATIONDEFINE_H__
#define __V3DMATERIALANIMATIONDEFINE_H__
#pragma once

#include "V3DRendererDefine.h"

using namespace std;


struct _MESHLIB_API sMATERIAL_ANIMATION_INFO
{
	float		fUOffset,
				fVOffset;

	int		nTextureID;
	DWORD		dwBlendMode;
	DWORD		dwColor;
	
	BOOL		bRender2Pass;

	float		fAlpha;
	BOOL		bGlobalTransparent;

	sMATERIAL_ANIMATION_INFO();
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _MESHLIB_API sMATERIALANIM_VERSION
{
	sMATERIALANIM_VERSION():	dwVersion(100){}


	DWORD	dwVersion;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _MESHLIB_API sMATERIALANIM_KEYFRAME
{
	DWORD		dwTime;
	float		fValues[4];

	sMATERIALANIM_KEYFRAME()
		:	dwTime(0)
	{
		fValues[0] = 0.0f;
		fValues[1] = 0.0f;
		fValues[2] = 0.0f;
		fValues[3] = 0.0f;
	}

	// 从keyframe1 keyframe2中blend结果
	// @param nElement  blend对象数量
	void	BlendFrame	(sMATERIALANIM_KEYFRAME* pKeyFrame1
                     ,sMATERIALANIM_KEYFRAME* pKeyFrame2
							,float                   t
							,int                     nElementNum);


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _MESHLIB_API sMATERIALANIM_TRACK
{
	//////////////////////////////////////
	vector<sMATERIALANIM_KEYFRAME*>	arKeyFrames;
	DWORD										dwLength;

	//////////////////////////////////////
	sMATERIALANIM_TRACK();
	~sMATERIALANIM_TRACK();

	//////////////////////////////////////
	BOOL	Read		(uDataChunkCursor cursorPointer
                  ,int              nSize)		;
	
	void	SetLength	( DWORD length ){ dwLength = length; }

	BOOL	GetKeyframe	(DWORD                    dwTime
                     ,sMATERIALANIM_KEYFRAME** ppKeyFrame1
							,sMATERIALANIM_KEYFRAME** ppKeyFrame2
							,float*                   t);

	sMATERIALANIM_KEYFRAME*	AddKeyframe	( sMATERIALANIM_KEYFRAME* pKeyFrame );
	sMATERIALANIM_KEYFRAME*	AddKeyframe	(DWORD dwTime
                                       ,float t0=0.0f
													,float t1=0.0f
													,float t2=0.0f
													,float t3=0.0f);

	BOOL	DelKeyframe		( sMATERIALANIM_KEYFRAME* pKeyFrame );


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _MESHLIB_API sMATERIALANIM_BLENDMODE
{
	DWORD		dwFlag;
	DWORD		dwMethod;

	//////////////////////////////////////
	sMATERIALANIM_BLENDMODE():	dwMethod(0),dwFlag(RENDER_BOTH_SIDE|RENDER_ALPHA_BLEND)
	{}


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _MESHLIB_API sMATERIALANIM_PROPERTY
{
	BOOL		bRender2Pass;
	DWORD		dwLength;
	DWORD		dwAffectRange;

	sMATERIALANIM_PROPERTY():	dwLength(1000), dwAffectRange(0),bRender2Pass(FALSE){}


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _MESHLIB_API sMATERIALANIM_TEXTURE
{
	////////////////////////////
	sMATERIALANIM_TEXTURE();
	~sMATERIALANIM_TEXTURE();

	////////////////////////////

	void	Destroy			();
	void	SetFilename		( LPCSTR szFilename );
	int	Register			();
	void	UnRegister		();


	////////////////////////////
	char	szFileName		[MAX_PATH];
	int	nTextureID;


};




#endif //__V3DMATERIALANIMATIONDEFINE_H__
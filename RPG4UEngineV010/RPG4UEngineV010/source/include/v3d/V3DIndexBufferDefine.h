/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DIndexBufferDefine.h
创建日期：2008年9月24日
最后更新：2008年9月24日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DINDEXBUFFERDEFINE_H__
#define __V3DINDEXBUFFERDEFINE_H__
#pragma once

enum V3DFORMAT
{
    V3DFMT_UNKNOWN              =  0,

    V3DFMT_R8G8B8               = 20,
    V3DFMT_A8R8G8B8             = 21,
    V3DFMT_X8R8G8B8             = 22,
    V3DFMT_R5G6B5               = 23,
    V3DFMT_X1R5G5B5             = 24,
    V3DFMT_A1R5G5B5             = 25,
    V3DFMT_A4R4G4B4             = 26,
    V3DFMT_R3G3B2               = 27,
    V3DFMT_A8                   = 28,
    V3DFMT_A8R3G3B2             = 29,
    V3DFMT_X4R4G4B4             = 30,
    V3DFMT_A2B10G10R10          = 31,
    V3DFMT_G16R16               = 34,

    V3DFMT_A8P8                 = 40,
    V3DFMT_P8                   = 41,

    V3DFMT_L8                   = 50,
    V3DFMT_A8L8                 = 51,
    V3DFMT_A4L4                 = 52,

    V3DFMT_V8U8                 = 60,
    V3DFMT_L6V5U5               = 61,
    V3DFMT_X8L8V8U8             = 62,
    V3DFMT_Q8W8V8U8             = 63,
    V3DFMT_V16U16               = 64,
    V3DFMT_W11V11U10            = 65,
    V3DFMT_A2W10V10U10          = 67,

    V3DFMT_UYVY                 = MAKEFOURCC('U', 'Y', 'V', 'Y'),
    V3DFMT_YUY2                 = MAKEFOURCC('Y', 'U', 'Y', '2'),
    V3DFMT_DXT1                 = MAKEFOURCC('D', 'X', 'T', '1'),
    V3DFMT_DXT2                 = MAKEFOURCC('D', 'X', 'T', '2'),
    V3DFMT_DXT3                 = MAKEFOURCC('D', 'X', 'T', '3'),
    V3DFMT_DXT4                 = MAKEFOURCC('D', 'X', 'T', '4'),
    V3DFMT_DXT5                 = MAKEFOURCC('D', 'X', 'T', '5'),

    V3DFMT_D16_LOCKABLE         = 70,
    V3DFMT_D32                  = 71,
    V3DFMT_D15S1                = 73,
    V3DFMT_D24S8                = 75,
    V3DFMT_D16                  = 80,
    V3DFMT_D24X8                = 77,
    V3DFMT_D24X4S4              = 79,


    V3DFMT_VERTEXDATA           =100,
    V3DFMT_INDEX16              =101,
    V3DFMT_INDEX32              =102,

    V3DFMT_FORCE_DWORD          =0x7fffffff
};


enum V3DPOOL 
{
    V3DPOOL_DEFAULT                 = 0,
    V3DPOOL_MANAGED                 = 1,
    V3DPOOL_SYSTEMMEM               = 2,
    V3DPOOL_SCRATCH                 = 3,

    V3DPOOL_FORCE_DWORD             = 0x7fffffff
};


#define V3DUSAGE_WRITEONLY          (0x00000008L)
#define V3DUSAGE_SOFTWAREPROCESSING (0x00000010L)
#define V3DUSAGE_DONOTCLIP          (0x00000020L)
#define V3DUSAGE_POINTS             (0x00000040L)
#define V3DUSAGE_RTPATCHES          (0x00000080L)
#define V3DUSAGE_NPATCHES           (0x00000100L)
#define V3DUSAGE_DYNAMIC            (0x00000200L)


#define V3DLOCK_READONLY           0x00000010L
#define V3DLOCK_DISCARD             0x00002000L
#define V3DLOCK_NOOVERWRITE        0x00001000L
#define V3DLOCK_NOSYSLOCK          0x00000800L

#define V3DLOCK_NO_DIRTY_UPDATE     0x00008000L



enum eMEMORY_LOCK_FLAG
{
	 LOCKFLAG_FLUSH  = V3DLOCK_NOSYSLOCK | V3DLOCK_DISCARD
	,LOCKFLAG_APPEND = V3DLOCK_NOSYSLOCK | V3DLOCK_NOOVERWRITE
};

#endif //__V3DINDEXBUFFERDEFINE_H__
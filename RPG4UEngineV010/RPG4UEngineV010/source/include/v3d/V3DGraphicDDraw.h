/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DGraphicDDraw.h
创建日期：2006年11月16日
最后更新：2006年11月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DGRAPHICDDRAW_H__
#define __V3DGRAPHICDDRAW_H__
#pragma once


#include "GlobalInstanceSingletonDefine.h"
#include "V3DVertexDefine.h"




class V3DSpaceBox;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _V3DGRAPHIC_API V3DGraphicDDraw
{
public:

	/////////////////////////////////////////////
	void	DrawLine2D	(int   x0
                     ,int   y0
							,int   x1
							,int   y1
							,DWORD dwColor);

	void	DrawLine3D	(const VECTOR3D& v0
                     ,const VECTOR3D& v1
							,DWORD           dwColor);


	/////////////////////////////////////////////
	void	DrawRect2D	(const POINT& pt0
                     ,const POINT& pt1
							,DWORD        dwColor);


	void	FillRect2D	(const POINT& pt0
                     ,const POINT& pt1
							,DWORD        dwColor);

	/////////////////////////////////////////////
	void	DrawRect2D	(const RECT& rect
							,DWORD       dwColor)		;

	void	FillRect2D	(const RECT& rect
							,DWORD       dwColor)		;

	/////////////////////////////////////////////
	void	DrawCircle2D(int   x
                     ,int   y
							,float r
							,int   nSegment
							,DWORD dwColor);

	void	FillCircle2D(int   x
                     ,int   y
							,float r
							,int   nSegment
							,DWORD dwColor);	

	/////////////////////////////////////////////
	void	FillCircle3D		(float x
                           ,float y
									,float z
									,float r
									,int   nSegment
									,DWORD dwColor);

	/////////////////////////////////////////////
	void	DrawCircle3D		(float x
                           ,float y
									,float z
									,float r
									,int   nSegment
									,DWORD dwColor);

	/////////////////////////////////////////////
	void	DrawFlatModel		(BYTE* v
                           ,int   nVertexCount
									,int   nStride
									,short face[]
									,int   nFaceCount
									,DWORD dwColor);

	/////////////////////////////////////////////
	void	FillTriangle		(int   x0
									,int   y0
									,int   x1
									,int   y1
									,int   x2
									,int   y2
									,DWORD dwColor);

	void	FillTriangle		(const VECTOR3D& v0
									,const VECTOR3D& v1
									,const VECTOR3D& v2
									,DWORD           dwColor);

	/////////////////////////////////////////////
	void	DrawBox3D	(const VECTOR3D&		vMin
                     ,const VECTOR3D&		vMax
							,DWORD					dwColor);
	void	DrawBox3D	(BBox3C&					bbox
                     ,DWORD               dwColor)		;
	void	DrawBox3D	(V3DSpaceBoxC&			bbox
                     ,DWORD               dwColor)		;

	void	DrawBox3D	(Vector3C*				arVertices
                     ,DWORD               dwColor)		;

	/////////////////////////////////////////////
	 void	DrawProgressRect	(int x
									,int y
									,int size
									,int nDegree);

	/////////////////////////////////////////////
	void	Blt	(int        nTextureID
					,LPRECT     lpDestRect
					,LPRECT     lpSrcRect
					,int        nSrcWidth
					,int        nSrcHeight
					,float      z
					,DWORD      dwColor
					,DWORD      dwFlag);

	BOOL	Blt	(int        nTextureID
               ,LPRECT     lpDestRect
					,LPRECT     lpSrcRect
					,LPRECT     lpClipRect
					,int        nSrcWidth
					,int        nSrcHeight
					,float      z
					,DWORD      dwColor
					,DWORD		dwFlag);

	BOOL BltEx	(int        nTextureID
               ,LPRECT     lpDestRect
					,LPRECT     lpSrcRect
					,LPRECT     lpClipRect
					,int        nSrcWidth
					,int        nSrcHeight
					,float      z
					,DWORD      dwColor
					,DWORD  		dwFlag
					,FLOAT		fDir);
//protected:
//	LPDIRECT3DDEVICE8 m_pDevice;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(V3DGraphicDDraw , _V3DGRAPHIC_API);
//extern _V3DGRAPHIC_API V3DGraphicDDraw& singleton::theV3DGraphicDDraw;
#define theV3DGraphicDDraw  singleton::GetV3DGraphicDDraw()

#include "V3DGraphicDDraw.inl"

#endif //__V3DGRAPHICDDRAW_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DConfig.h
创建日期：2006年11月7日
最后更新：2006年11月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DCONFIG_H__
#define __V3DCONFIG_H__
#pragma once


#include "CommonDefine.h"

#include "V3DPrimitiveDefine.h"
#include "V3DCamera.h"
#include "BaseSetting.h"
#include "V3DConfigDefine.h"

class IniFile;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _V3DGRAPHIC_API V3DConfig
{
public:
	V3DConfig();

public:
	void	LoadFrom(IniFile& cfgApp);

public:
	BOOL					m_bHeroMultiRegTexture;

protected:

	VG_BOOL_PROPERTY	(EditorMode);
	VG_BOOL_PROPERTY	(ShowDummy);
	VG_BOOL_PROPERTY	(ForceSelect);
	VG_BOOL_PROPERTY	(MoveOnGround);
	VG_INT_PROPERTY	(MaxActiveLightNum);	//场景中灯光的个数
	VG_INT_PROPERTY	(MaxActiveSoundNum);	//场景音效的个数
	VG_BOOL_PROPERTY	(ShowSceneID);
	VG_INT_PROPERTY	(BorderSize);			//

	VG_BOOL_PROPERTY	(JustLoadFootMap);	//只读取脚上地图
	VG_BOOL_PROPERTY	(AutoLoadTileMap);	//只读取脚上地图

	////////////////////////////
	VG_BOOL_PROPERTY	(ShowModelFloorTree);
	VG_BOOL_PROPERTY	(ShowObjectBBox);
	VG_BOOL_PROPERTY	(ShowFloatAshes);
	VG_BOOL_PROPERTY	(ShowGrasses	);

public:
	VG_INT_PROPERTY		(RigidHaloTextureId);
	VG_INT_PROPERTY		(LightTrackTextureId);
	VG_TYPE_PROPERTY		(Billboard,	V3DMatrix);
	VG_TYPE_PROPERTY		(Standboard,V3DMatrix);
	VG_INT_GET_PROPERTY	(ScreenWidth);
	VG_INT_GET_PROPERTY	(ScreenHeight);
	VG_BOOL_PROPERTY		(Pause);

	VG_TYPE_PROPERTY		(Camera,	V3DCamera);


	VG_FLOAT_PROPERTY	(ObjectMtlMod);
	VG_BOOL_PROPERTY	(RenderAllObjs);

public:
	VG_BYTE_PROPERTY	(ShadowAlphaBase);
	VG_BOOL_PROPERTY	(EnjoinTwoSide);
	VG_BOOL_PROPERTY	(ToolMode);
	VG_BOOL_PROPERTY	(RenderHelper);

	VG_INT_PROPERTY	(ShowInfoLV);				//名字信息显示开关

	VG_BOOL_PROPERTY	(ShowGrid);					//是否显示线筐图
	VG_BOOL_PROPERTY	(ShowWalk);
	VG_BOOL_PROPERTY	(RenderChunkGrid);
	VG_BOOL_PROPERTY	(RenderShadows);
	VG_BOOL_PROPERTY	(RenderTerrain);
	VG_BOOL_PROPERTY	(RenderObjs);
	VG_BOOL_PROPERTY	(RenderBBox);
	VG_BOOL_PROPERTY	(RenderMapBox);	//是否渲染地图 包围盒
	VG_BOOL_PROPERTY	(RenderTree);
	VG_BOOL_PROPERTY	(RenderGrass);
	VG_BOOL_PROPERTY	(RenderWorld);
	VG_BOOL_PROPERTY	(RenderWater);
	VG_BOOL_PROPERTY	(RenderLayout);

	VG_BOOL_PROPERTY	(MiniMap);
	VG_BOOL_PROPERTY	(MiniWorld);

	VG_BOOL_PROPERTY	(ShowAllAlphaInfo); //Terrain的Aphla层信息
	VG_BOOL_PROPERTY	(ShowFog);
	VG_FLOAT_PROPERTY	(MipLodBais);

	VG_DWORD_PROPERTY	(LoadSleepTime);

	VG_INT_PROPERTY	(UIMinAlpha);		//UI镂空最低Alpha值，-1不镂空

public:
	VG_DWORD_PROPERTY		(SkinMeshVertexShaderID);
	VG_BOOL_PROPERTY		(UseSkinMesh);
	VG_BOOL_PROPERTY		(AlwayseWireFrameRendering);


	VG_BOOL_PROPERTY		(UseDepthBuffer);		///是否使用Z缓冲
	VG_DWORD_PROPERTY		(MinDepthBits);

	VG_BOOL_PROPERTY		(UseVBIB);	
	VG_INT_PROPERTY		(CardType);

	VG_ARRAY_PROPERTY		(LayerIsRender,BOOL , MAX_TERR_LAYER_NUM);
	VG_ARRAY_PROPERTY		(LayerIsLock,	BOOL , MAX_TERR_LAYER_NUM);

	//显卡 Render状态
	VG_FLOAT_PROPERTY		(BspRenderDistance);

///////////////////////////////////////////////////

public:


};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
_V3DGRAPHIC_API extern V3DConfig g_V3DConfig;

#endif //__V3DCONFIG_H__
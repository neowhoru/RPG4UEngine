/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DGameMap.h
创建日期：2007年6月18日
最后更新：2007年6月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	V3DGameMap主要用三大部分组成：
		1.Terrain		地形
		2.Scene			景物
		3.TileMap		寻路图

		表示地图中一个地块 Plot

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DGAMEMAP_H__
#define __V3DGAMEMAP_H__
#pragma once


#include "CommonDefine.h"
#include "v3dGameWorldDefine.h"
#include "V3DGameZoneManager.h"
#include "V3Dscene.h"
#include "ThreadMutex.h"
#include "V3DBspTree.h"


_NAMESPACEU(TileMap, tile);

class V3DGameWorld;
class V3DWaterResParser;
class V3DScene;
class V3DTerrain;
class TileMapGeneratorListener;

using namespace threadutil;
using namespace std;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DGameMap :public SemiAutoCounterLock 
{
public:
	V3DGameMap();

public:
	BOOL		Init(  );
	BOOL		Destory();

public:
	BOOL		Update			(const VECTOR3D&        vPlayer
                           ,const VECTOR3D&        vPos
									,const VECTOR3D&        vCamera
									,const VECTOR3D&        vCameraDir
									,const V3DFrustum *		pFrustum = NULL);

	BOOL		Update			(const VECTOR3D&        vPlayer
                           ,const VECTOR3D&        vPos
									,const Ray&					ray
									,const V3DFrustum *		pFrustum = NULL);
	BOOL		Render			(DWORD dwTick, BOOL bRenderTask);
	BOOL		RenderScene		(DWORD dwTick);
	BOOL		RenderWater		(DWORD dwTick);
	BOOL		RenderGrass		(DWORD dwTick);

public:
	////////////////////////////////////////
	BOOL		Load				(LPCSTR        szFileTitle
                           ,V3DGameWorld* pLand
									,BOOL				bNeed =  FALSE);
	BOOL		Save				(BOOL bJustTerrain = FALSE );

	BOOL		LoadScene		( LPCSTR szFileTitle, V3DGameWorld *pLand );
	BOOL		LoadTerrain		( LPCSTR szFileTitle, V3DGameWorld *pLand );

	BOOL		GenerateTileMap(BOOL bSave,TileMapGeneratorListener* pListener);
	BOOL		Flip				( eTERRAIN_FLIP_OPR oprType);

public:
	////////////////////////////////////////
	VOID		PushRenderTask		();
	VOID		PopRender			();

public:
	VOID		GetWorldMapXY		( INT &nX, INT &nY );
	VOID		SetWorldMapXY		( INT nX, INT nY );
	float		GetTileSize			();

	void		SetRenderChunk		( UINT nRenderChunk );
	BOOL 		IsLoadScene			();
	BOOL 		IsLoadTerrain		();
	BOOL		IsReadOnly			();
	BOOL		IsHeroInTerrain	()	;	

	////////////////////////////////////////
	TileMap*				GetTileMap	(BOOL bCheckLoaded=TRUE);
	V3DTerrain	*		GetTerrain	(BOOL bCheckLoaded=TRUE);
	V3DScene		*		GetScene		(BOOL bCheckLoaded=TRUE);


	BOOL					GetRayCrossPoint	(Vector3C& vSrc
                                       ,Vector3C& vDir
													,VECTOR3D        &vCross
													,BOOL bBinDirect=TRUE);
	BOOL					GetRayCrossPoint	(Vector3C& vSrc
                                       ,Vector3C& vDir
													,float           &t
													,BOOL bBinDirect=TRUE);

	BOOL					GetRayCrossPoint	(const Ray&		  ray
													,VECTOR3D        &vCross
													,BOOL bBinDirect=TRUE);
	BOOL					GetRayCrossPoint	(const Ray&		  ray
													,float           &t
													,BOOL bBinDirect=TRUE);


protected:

	V3DTerrain*			m_pTerrain;
	V3DScene*			m_pScene;
	TileMap*				m_pTileMap;//		用来寻路的TipMap
	V3DGameWorld	  *m_pBindLand;

	BOOL					m_bLoadTerrain;
	BOOL					m_bLoadScene;

	string				m_szLoadingText;

	VG_INT_GET_PROPERTY	(MiniMapTextureID);
	VG_BOOL_PROPERTY		(NoRender);
	VG_BOOL_GET_PROPERTY	(Loaded);

	VG_INT_GET_PROPERTY	(PlotX);
	VG_INT_GET_PROPERTY	(PlotY);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "V3DGameMap.inl"


#endif //__V3DGAMEMAP_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DTerrainChunk.h
创建日期：2007年12月7日
最后更新：2007年12月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DTERRAINCHUNK_H__
#define __V3DTERRAINCHUNK_H__
#pragma once


#include "CommonDefine.h"
#include "V3DTerrainDefine.h"
#include "V3DVertexDefine.h"
#include "V3DVegetationDefine.h"
#include "ThreadMutex.h"
#include "V3DTerrainBlender.h"
#include "V3DWaterSurfaceDefine.h"

////////////////////////////////////////////////////////////
class V3DSceneGrass;
class V3DWaterSurface;
class V3DTerrainBlender;
class V3DTerrain;
class V3DTerrainVertex;
using namespace threadutil;


const DWORD MAX_TERRAINCHUNK_INDEX = 4096*3;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DTerrainChunk : public SemiAutoCounterLock
{
	friend class V3DGameWorld;
	friend class V3DTerrain;
	friend class V3DTerrainEditor;

public:
	V3DTerrainChunk();
	~V3DTerrainChunk();

public:
	/////////////////////////////////////////
	BOOL		ReadyRenderBuffer		();
	BOOL		RenderWater				();
	BOOL		RenderLayerFloor		();
	BOOL		RenderLayerLayout		();
	BOOL		RenderLayerEffect		( int nLayer );
	BOOL		RenderLayerShadow		();



public:
	BOOL		Generate			(DWORD dwWidth				= TILE_RES
									,DWORD dwHeigh				= TILE_RES
									,BOOL  bUseCompTexture	= FALSE);
	BOOL		GenerateLayout	(DWORD dwWidth		=TILE_RES
									,DWORD dwHeight	=TILE_RES);
	BOOL		ReleaseChunk();

	BOOL		Write			( DataChunkWriter *w );
	BOOL		Read			( uDataChunkCursor cursorPointer, int InSize, BOOL bNeed );

protected:
	BOOL		_GenerateUVs	(VERTEX3N_UV2* pVertices
                           ,BOOL          bFullTexture
									,float         fOffsetU
									,float         fOffsetV);
	BOOL		_ReadBlendInfo	( uDataChunkCursor cursorPointer, int InSize );
	BOOL		_ReadVegetation( uDataChunkCursor cursorPointer, int InSize );
	BOOL		_ReadGrass		( uDataChunkCursor cursorPointer, int InSize );
	BOOL		_ReadGassID		( uDataChunkCursor cursorPointer, int InSize );

	int 		_CalcIndices				(WORD* pIndices
                                    ,int   nDetailLevel)		;
	int		_CalcLocalIndices			(WORD* index
                                    ,int   nDetailLevel)		;
	void		_GenerateEffectTexture	(int                nLayer
                                    ,int                nX
												,int                nY
												,DWORD *pdwBuffer = NULL);

public:
	BOOL		Flip			(eTERRAIN_FLIP_OPR oprType);
	BOOL		Copy			(V3DTerrainChunk *pSrcChunk, DWORD dwTerrainCopyState);
	VOID		BuildMesh	();
	BOOL		AddGrass		(int                      nType
                        ,int                      x
								,int                      y
								,V3DSceneGrass*			  pGrassObj
								,sVEGETATION_GEN_SETTING* pSetting=NULL
								,FLOAT                    fRandomSize=0.0f);

	BOOL		ClearGrass	();

	int		FlushTerrainTexture			(int nLayer);
	BOOL		ResisterGroupTexture			(INT nLvl0
													,INT nLvl1
													,INT nLvl2
													,INT nShadow);
	BOOL		UnresisterEfectTextures		();
	BOOL		ResisterShadowTexture		(V3DTerrainBlender *pBlender = NULL);
	BOOL		UnresisterShadowTexture		();


	void		SetTerrainXY			( SHORT x, SHORT y );
	Vector3X	GetNormal				( float x, float y );



	BOOL		UpdateLight					(DWORD dwUpdateLightFlags);		
	
	/////////////////////////////////////////

	BOOL		ResisterEffectTextures	( BOOL bLoad	= false);
	INT		GetLayerFloorTexture		( INT nIdx		= -1);
	void		SetGrassVisible			( BOOL bVisible );

	void		SetAlphaTextureCount		(int nCount ); 	
	int		GetAlphaTextureIndex		(int nIdx) ;
	void		SetAlphaTextureIndex		(int nIdx, int nTexIdx);
	void		SetRenderTextureCount	(INT nCount );
	
	void		SetEffectTexId				( int nIndex, int nLayer ) ;
	INT		GetEffectTextureID		(INT nLayer);
	VOID		SetEffectTextureID		(INT nLayer, INT i)			 ;

	void		SetVisible					( BOOL bVis );
	void		AddGrassID					(UINT nGrassID);
	void		RemoveGrassID				(UINT nGrassID);

	///////////////////////////////////////////
	void				CalcBBox	();
	V3DSpaceBox*	GetBBox	();
	BBOX*				GetMMBox	() ;

	float				GetHeightAt	(float     x
                              ,float     y
										,BOOL      bLocal=false
										,DWORD*    pdwReturn=NULL
										,Vector3X* pvNormal=NULL);


	///////////////////////////////////////////
	void		GetIntersection	(const Vector3X*              pvFrom
                              ,const Vector3X*              pvDir
										,V3DTerrainIntersectionArray* pvectorIntersection
										,int                          nDetailLevel
										,BOOL bBinDirect=TRUE);

	void		GetIntersection	(const Ray&                   ray
                              ,V3DTerrainIntersectionArray* pvectorIntersection
										,int                          nDetailLevel
										,BOOL bBinDirect=TRUE);

	void		GetTerrainIntersection	(const VECTOR3D&              vFrom
                                    ,const VECTOR3D&              vDir
												,V3DTerrainIntersectionArray* pvectorIntersection
												,int                          nDetailLevel
												,BOOL bBinDirect=TRUE);

	void		GetTerrainIntersection	(const Ray&                   ray
												,V3DTerrainIntersectionArray* pvectorIntersection
												,int                          nDetailLevel
												,BOOL bBinDirect=TRUE);

	COLOR*	GetWaterColors				(BOOL bPreivew=FALSE);
	void		SetWaterColors				(COLOR arColors[WATER_COLOR_MAX], DWORD dwUpdateFlag=0, BOOL bPreivew=FALSE );
	void		PreivewWaterColors		(COLOR arColors[WATER_COLOR_MAX], DWORD dwUpdateFlag=0 );
	BOOL		IsEqualWaterColors		(COLOR arColors[WATER_COLOR_MAX], DWORD dwUpdateFlag=0, BOOL bPreivew=FALSE);
	BOOL		IsSuckTerrainDiffuse		(DWORD dwUpdateFlag=0 );
public:
	///////////////////////////////////////////
	int						StartPointBlender		(int nX, int nY, short stTexture, int nBlend, int nLayer, int bBrushType = 0, float fAlphaModulus = 1.0f);
	int						StartRectBlender		(RECT  &rt
                                             ,short stTexture
															,int   nBlend
															,int   nLayer);

	int						AttachMemoryBlender	(RECT   &rt
                                             ,short  stTexture
															,int    nRadius
															,short* pBuffer
															,int    nLayer
															,float  fAlphaModulus=1.0f
															,INT    nDirType=0);
	V3DTerrainBlender *	GetBlender				(int nLayer) ;

	////////////////////////////////////////////
	//set 



public:
	static WORD m_ProjectIndex		[MAX_TERRAINCHUNK_INDEX];
	static WORD m_VerticesUsed		[(TILE_RES+1)*(TILE_RES+1)];
	static WORD m_Index				[MAX_TERRAINCHUNK_INDEX];

protected:
	VG_BOOL_GET_PROPERTY	(BuildMesh);
	VG_FLOAT_PROPERTY		(WaterHeight);
	VG_BOOL_PROPERTY		(Shore);
	VG_BOOL_PROPERTY		(HasWater);
	VG_BOOL_PROPERTY		(RenderAll);
	VG_PTR_PROPERTY		(WaterSurface,	V3DWaterSurface);
	VG_PTR_PROPERTY		(Terrain,		V3DTerrain);
	VG_PTR_PROPERTY		(Camera,			V3DCamera);
	VG_SHORT_PROPERTY		(TerrainX);
	VG_SHORT_PROPERTY		(TerrainY);
	VG_INT_GET_PROPERTY	(AlphaTextureCount);
	VG_INT_PROPERTY		(DetailLevel);		// the current detail level to use
	VG_PTR_PROPERTY		(Vertices,			VERTEX3N_UV2);
	VG_PTR_PROPERTY		(LayoutVertices,	VERTEX3N_UV2);	//规划图顶点
#ifdef USE_TERRAIN_VERTEX
	VG_PTR_PROPERTY		(TerrainVertices,	V3DTerrainVertex*);
#else
	VG_PTR_PROPERTY		(VertexReflect,	INT);
#endif
	VG_INT_PROPERTY		(TextureIndex);	//基本地表idx
	VG_BOOL_GET_PROPERTY	(Visible);
	VG_FLOAT_GET_PROPERTY(DistanceToCamera);
	VG_INT_PROPERTY		(ShadowBaseId);
	VG_INT_GET_PROPERTY	(ShadowTextrueId);
	VG_INT_PROPERTY		(Repeat);
	VG_BOOL_PROPERTY		(Generated);
	VG_BOOL_PROPERTY		(Render);
	VG_BOOL_PROPERTY		(LayoutGenerated);
	VG_BOOL_PROPERTY		(TraceTerrainDiffuse);
	VG_BOOL_PROPERTY		(PreivewWaterColor);

protected:
	int						m_nFaceCount;
	int						m_nUseVert;
	vector<UINT>			m_VegetationIDs;

	short						m_stRenderLayer;
	int						m_EffectTextureIDs			[TERRAIN_BLENDLAYER_MAX];
	int						m_WorkingEffectTextureIDs	[TERRAIN_BLENDLAYER_MAX];

	V3DTerrainBlender		m_TerrainBlender	[TERRAIN_BLENDLAYER_MAX];

	VG_PTR_PROPERTY		(ShadowBlender, V3DTerrainBlender);

	V3DSpaceBox				m_BBox;
	BBOX						m_MMBox;
	COLOR						m_arWaterColors[WATER_COLOR_MAX];
	COLOR						m_arPreviewColors[WATER_COLOR_MAX];
};



#include "V3DTerrainChunk.inl"



#endif //__V3DTERRAINCHUNK_H__
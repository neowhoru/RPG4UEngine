/*////////////////////////////////////////////////////////////////////////
文 件 名：VUIControlStruct.h
创建日期：2007年11月1日
最后更新：2007年11月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __VUICONTROLSTRUCT_H__
#define __VUICONTROLSTRUCT_H__
#pragma once


#include "VUIControlDefine.h"

///////////////////////////////////////////
enum eUIDRAW_MODE
{
	 UIDRAW_LEFT = 0
	,UIDRAW_TOP
	,UIDRAW_RIGHT
	,UIDRAW_BOTTOM
	,UIDRAW_MIDDLE

	,UIDRAW_MAX
};


///////////////////////////////////////////
struct _VRUIDLL_API sUIINFO_PIC
{
	enum eCONST{MAX_PIC_NAME=128};
	sUIINFO_PIC()
	{
		__ZERO(m_rcFrame);
		__ZERO(m_rcClient);
		__ZERO(m_szPicName);
	}

	sUIINFO_PIC& operator = (const sUIINFO_PIC& dat);

	void	SetPicInfo		(LPCSTR      szName
                        ,const RECT* prcFrame
								,const RECT* prcClient);

	RECT	m_rcFrame;
	RECT	m_rcClient;
	char	m_szPicName[MAX_PIC_NAME];
};


///////////////////////////////////////////
struct _VRUIDLL_API sUIINFO_BASE
{

	sUIINFO_BASE();
	sUIINFO_BASE&	operator		= (const sUIINFO_BASE& dat);
	DWORD				GetFactoryType()	{return m_nType;}

	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
	int			m_nType;						// 控件类型
	int			m_nAlignMode;				// 对齐模式
	RECT			m_rcRealSize;				// 框架的RECT
	int			m_nDrawMode[UIDRAW_MAX];// 绘制模式
	char			m_szID[64];					// 控件的ID
	sUIINFO_PIC	m_MainPic;					// 控件背景图片
	char			m_szCaption[128];			// 标题
	char			m_szFont[32];				// 字体
	int			m_nFontSize;				// 字体大小
	COLOR			m_clrFontText;				// 字体颜色
};

//////////////////////////////////////////////////////////////////////////
// 框架
struct _VRUIDLL_API sUIINFO_FRAME : public sUIINFO_BASE
{
	sUIINFO_FRAME();

	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	int	m_nArrayMode;			// 排列模式
	BOOL	m_bCanMoving;			// 是否可被移动
};

//////////////////////////////////////////////////////////////////////////
// 按钮
struct _VRUIDLL_API sUIINFO_BUTTON : public sUIINFO_BASE
{
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);
	sUIINFO_PIC	m_PicMouseOver;
	sUIINFO_PIC	m_PicMousePress;
	sUIINFO_PIC	m_PicDisabled;
};

//////////////////////////////////////////////////////////////////////////
// 选择框
struct _VRUIDLL_API sUIINFO_CHECKBOX : public sUIINFO_BASE
{
	sUIINFO_CHECKBOX();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	BOOL				m_bChecked	;
	sUIINFO_BUTTON	m_BtnChecked;
};

//////////////////////////////////////////////////////////////////////////
// 滚动条
struct _VRUIDLL_API sUIINFO_SCROLLBAR : public sUIINFO_BASE
{
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);
	sUIINFO_BUTTON	m_BtnUp;
	sUIINFO_BUTTON	m_BtnDown;
	sUIINFO_BUTTON	m_BtnDragBar;
};


// 滚动条
struct _VRUIDLL_API sUIINFO_SCROLLBAR_EX : public sUIINFO_SCROLLBAR
{
	//sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);
};

//////////////////////////////////////////////////////////////////////////
// 编辑框
struct _VRUIDLL_API sUIINFO_EDIT : public sUIINFO_BASE
{
	sUIINFO_EDIT();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	BOOL	m_bMultiLine;
	BOOL	m_bReadOnly	;
	BOOL	m_bPassword	;
	BOOL	m_bIsNumber	;
	int	m_nTextAlign;
	sUIINFO_SCROLLBAR		m_ScrollBar;
};

//////////////////////////////////////////////////////////////////////////
// 文本框
struct _VRUIDLL_API sUIINFO_TEXT : public sUIINFO_BASE
{
	sUIINFO_TEXT();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	INT	m_nTextAlign;
};

//////////////////////////////////////////////////////////////////////////
// 文本列表
struct _VRUIDLL_API sUIINFO_LIST : public sUIINFO_BASE
{
	sUIINFO_LIST();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	COLORREF		m_clrSelected		;
	int			m_nItemHeight	;
	sUIINFO_SCROLLBAR		m_ScrollBar;
};

//////////////////////////////////////////////////////////////////////////
// 图像列表
struct _VRUIDLL_API sUIINFO_IMGLIST : public sUIINFO_BASE
{
	sUIINFO_IMGLIST();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	int		m_nIconSize;
	int		m_nOffsetWidth;
	int		m_nOffsetHeight;
	int		m_nMaxHeightCount;
	BOOL		m_bCanPick;

	BOOL		m_bShowFrame;
	COLOR		m_clrShowFrame;

	sUIINFO_SCROLLBAR	m_ScrollBar;
};

//////////////////////////////////////////////////////////////////////////
// 下拉框
struct _VRUIDLL_API sUIINFO_COMBOBOX : public sUIINFO_BASE
{
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);
	sUIINFO_EDIT		m_EditBox		;
	sUIINFO_LIST		m_ListCtrl		;
	sUIINFO_BUTTON		m_ButtonCtrl	;
};

//////////////////////////////////////////////////////////////////////////
// 图片
struct _VRUIDLL_API sUIINFO_IMAGE : public sUIINFO_BASE 
{
	//sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);
};

//////////////////////////////////////////////////////////////////////////
// 进度条
struct _VRUIDLL_API sUIINFO_PROGRESS : public sUIINFO_BASE
{
	//sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);
};

//////////////////////////////////////////////////////////////////////////
// 属性页
struct _VRUIDLL_API sUIINFO_TAB : public sUIINFO_BASE
{
	sUIINFO_TAB();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	sUIINFO_BUTTON		m_TabButtons[TAB_CONTROL_MAX];
	sUIINFO_FRAME		m_FrameCtrl;
	char					m_FrameFiles[TAB_CONTROL_MAX][64];
	INT					m_nTabCount;
};

//////////////////////////////////////////////////////////////////////////
// 列表框扩展
struct _VRUIDLL_API sUIINFO_LIST_EX : public sUIINFO_BASE
{
	sUIINFO_LIST_EX();
	sUIINFO_BASE& operator = (const sUIINFO_BASE& dat);

	COLORREF		m_clrSelected			;
	int			m_nItemHeight		;
	int			m_nItemWidth		;
	int			m_nOffsetWidth		;
	int			m_nOffsetHeight	;
	int			m_nMaxHeightCount	;
	BOOL			m_bCanPick		;
	sUIINFO_SCROLLBAR	m_ScrollBar;
};



//////////////////////////////////////////////////////////////////////////
struct sUIDATA_HEADER
{
	sUIDATA_HEADER()
	{
		__ZERO(m_szVersion);
		__ZERO(m_ControlTypes);
		strcpy( m_szVersion, UI_VER);
		m_nControlCount = 0;
		m_dwCreatedDate = 0;
	}

	char				m_szVersion[10];
	DWORD				m_dwCreatedDate;
	sUIINFO_FRAME	m_FrameData;
	int				m_nControlCount;
	int				m_ControlTypes	[CONTROLS_COUNT_MAX];
};



//////////////////////////////////////////////////////////////////////////
struct _VRUIDLL_API sUIDATA_BODY
{
	sUIDATA_BODY();
	~sUIDATA_BODY();

	void	Release		();
	DWORD LoadFile		( LPCSTR pszFileName );
	DWORD SaveFile		( LPCSTR pszFileName );
	DWORD LoadFile_V0	( LPCSTR pszFileName );

	static sUIINFO_BASE* NewData( int nType );

	sUIDATA_HEADER		m_HeaderData;
	sUIINFO_BASE*		m_ControlDatas[CONTROLS_COUNT_MAX];
};




#endif //__VUICONTROLSTRUCT_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DOctaTree.h
创建日期：2006年8月28日
最后更新：2006年8月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	树使用中，物体不断被加入、删除
	树本身节点有机会被划分越来越细
	占用资源将越来越多，是否需要重新构造整个树？

	新增一个子节点所消耗的源，非常少（50bytes左右）
	
	1.可以考虑使用节点池优化内存使用

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DOCTANODETREE_H__
#define __V3DOCTANODETREE_H__
#pragma once

#include "V3DOctaTreeDefine.h"
#include "V3DFrustum.h"
#include "TMemoryPoolFactory.h"

typedef util::TMemoryPoolFactory<sOCTA_NODE>		OctaNodePoolFactory;
/*////////////////////////////////////////////////////////////////////////
//8叉空间树
/*////////////////////////////////////////////////////////////////////////
class _V3DGRAPHIC_API V3DOctaTree
{
public:
	V3DOctaTree(void);
	~V3DOctaTree(void);

public:
	//初始化
	//bSplitZ:是否分割Z
	BOOL Create				(const BBOX3D&			bbox
								,float					fSplitUnit =   -1.0f
								,int						iInitTreeLayer = 1
								,BOOL						bSplitZ =       true);

	BOOL Create				(const Vector3X&      vecMin
                        ,const Vector3X&      vecMax
								,float					fSplitUnit =   -1.0f
								,int						iInitTreeLayer = 1
								,BOOL						bSplitZ =       true);

	void Destroy();


public:
	void CullFrustum		(const V3DFrustum& frustum
                        ,HITCALLBACK       fun
								,void*             lpData);	

	BOOL InsertObject	( V3DOctaSubObject *pObj );
	void RemoveObject	( V3DOctaSubObject *pObj );


	//destroy tree	
	BOOL CastRay		(const Ray&		ray
							,HITCALLBACK	fun
							,void*			lpData);

protected:
	sOCTA_NODE*	_GetRoot	( void );

	//创建多层次子节点
	void _CreateChilds	( sOCTA_NODE * pParent,int nLayerIndex = 1 );

	//遍历
	sOCTA_NODE*	_WalkTree( sOCTA_NODE*pNode,	BBOX*	pBBox);	
	int			_WalkTree( int iNode,			BBOX *pBBox );


	void			_OnFrustumCull	( sOCTA_NODE *pNode,	const V3DFrustum* pFrustum );
	void			_OnRayCast		( sOCTA_NODE *pNode,	const Ray& ray );


	//规范化物件
	//比如：如果物件的包围盒超出了世界的范围，
	//这个操作可能会改变pObj的包围盒内容
	void _NormalizeObject		( V3DOctaSubObject *pObj );


protected:
	OctaNodeArray			m_TreeNodes;
	HITCALLBACK				m_CallbackProc;	//挑选时，确认调用
	void				*		m_pProcParam;
	float						m_fSplitUnit;	//切分单位
	BOOL						m_bSplitZ;		//是否分割Z轴  如果不分割Z，那就4叉树
	int						m_iChildNum;	//子节点个数
	OctaNodePoolFactory*	m_pNodePool;
};


#endif //__V3DOCTANODETREE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：	BaseLibExport.h
创建日期：	2007.12.27
最后更新：	2007.12.27
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#pragma once

// disable: "<type> needs to have dll-interface to be used by clients'
// Happens on STL member variables which are not public therefore is ok
#pragma warning (disable : 4251)

// disable: "non dll-interface class used as base for dll-interface class"
// Happens when deriving from Singleton because bug in compiler ignores
// template export
#pragma warning (disable : 4275)



#ifdef _VRUI

	///////////////////////////////////
	//动态库输出
	#ifdef _USRDLL
		#define _VRUI_API __declspec( dllexport )
		#pragma message("   _VRUI_API __declspec( dllexport )")
	///////////////////////////////////
	//非动态输出
	#else
		#define _VRUI_API
		#define _VRUI_STATIC
		#pragma message("   _VRUI_API _LIB")
	#endif

#else

	#define USE_VRUI_STATIC
	///////////////////////////////////
	//静态库
	#ifdef USE_VRUI_STATIC
		#pragma message("   USE_VRUI_STATIC")
		#define _VRUI_API
		#ifdef _DEBUG
			#pragma comment(lib,"VRUIStaticD.lib")
		#else
			#pragma comment(lib,"VRUIStatic.lib")
		#endif

	///////////////////////////////////
	//动态库
	#else

		#pragma message("   _VRUI_API __declspec( dllimport )")
		#define _VRUI_API __declspec( dllimport )
		#ifdef _DEBUG
			#pragma comment(lib,"VRUID.lib")
		#else
			#pragma comment(lib,"VRUI.lib")
		#endif
	#endif

#endif




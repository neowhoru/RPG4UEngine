/*////////////////////////////////////////////////////////////////////////
文 件 名：IVUIIconInfo.h
创建日期：2007年11月27日
最后更新：2007年11月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __IVUIICONINFO_H__
#define __IVUIICONINFO_H__
#pragma once

#include "IVUIIconInfoDefine.h"
#include "CommonDefine.h"


class BaseSlot;
using namespace std;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _VRUIDLL_API IVUIIconInfo
{
public:
	IVUIIconInfo();
	virtual ~IVUIIconInfo(){};

public:// Virtual Function
	virtual VUIIconInfoType GetFactoryType() = 0;

	virtual INT		GetIconID()		= 0;
	virtual INT		GetCount()		= 0;
	virtual BOOL	IsLocked()		= 0;
	virtual INT		GetMaxCount()	= 0;
	virtual INT		GetSpareTime()	= 0;
	virtual BOOL	IsNextLevel()	= 0;
	virtual BOOL	IsNull()			= 0;

	virtual BaseSlot*	GetSlot	()		= 0;

	virtual void	SetSlot		(BaseSlot*	pSlot)	= 0;
	virtual void	SetCount		(INT		nCount)		= 0;
	virtual void	SetIconFile	(VRSTR	sFile,BOOL bBigIcon=FALSE)		= 0;
	virtual void	SetMaxCount (INT		nMax)			= 0;
	virtual void   SetSpareTime(int nSpareTime)		= 0;
	virtual void   SetCost		(INT		nCost)		= 0;

	virtual BOOL SetData	(void*		pData
                        ,INT			nCount
                        ,BOOL			bShowCount=FALSE
								,BOOL			bNextLevel=FALSE)= 0;

	virtual BOOL SetData	(INT  nID
                        ,INT  nCount
								,BOOL bShowCount=FALSE
								,BOOL bNextLevel=FALSE)= 0;

	virtual BOOL operator==(const IVUIIconInfo& stIconInfoBase ) = 0;
	virtual BOOL PlaySound() = 0;

//protected:// Virtual Function
	virtual LPCSTR IconFileName(BOOL bBigIcon=FALSE) = 0;
	virtual int		GetCost		() = 0;

public:
	virtual void	Clear			();
	virtual void	ClearTips	();
	virtual void	ReUse			();
	virtual void   GhostFrom	(const IVUIIconInfo& datSrc);

public:
	virtual void DrawIcon	(IN RECT*       prcDst
									,const COLORREF col
									,INT            nFontIndex
									,INT            nFontSize
									,const COLORREF colFont
									,BOOL				 bBigIcon=FALSE);

	virtual void DrawInfo	(IN RECT*       prc
									,BOOL           bIsShop
									,INT            nFontIndex
									,INT            nFontSize
									,const COLORREF col
									,BOOL           bFaveFrame=TRUE
									,BOOL           bShowAllInfo=TRUE);

	virtual void DrawInfoAt	(const RECT&	 rcIcon
									,BOOL           bIsShop
									,INT            nFontIndex
									,INT            nFontSize
									,const COLORREF col
									,BOOL           bFaveFrame=TRUE
									,BOOL           bShowAllInfo=TRUE);


	void SetInfo		( LPCSTR szInfo );
	void SetExtraInfo	( LPCSTR szInfo );

	void AddToolTip	(LPCSTR szText
                     ,DWORD  dwColor=0xFFFFFFFF
							,INT	  nPicID=-1);

	void AddToolTip	(LPCSTR szText
                     ,DWORD  dwAlign
							,BOOL   m_bKeepInLine
							,DWORD  dwColor=0xFFFFFFFF
							,INT	  nPicID=-1);

	void AddExtraTip	(LPCSTR szText
                     ,DWORD  dwColor=0xFFFFFFFF
							,INT	  nPicID=-1);

	void		UpdateTips	(BOOL bExtra=FALSE);
	LPCSTR	GetToolTip	(INT nIndex);

protected:
	///////////////////////////////////////////////////
	int						m_nIconTextureID;
	int						m_nBigTextureID;

	sICON_INFO				m_IconInfos		[MAX_ICON_INFO_LINE_COUNT];
	sICON_INFO				m_ExtraInfos	[MAX_ICON_OTHER_INFO_LINE_COUNT];

	///////////////////////////////////////////////////
	VG_INT_GET_PROPERTY	(IconInfoLine);
	VG_INT_GET_PROPERTY	(IconOtherInfoLine);
	VG_BOOL_GET_PROPERTY	(ShowCount);

	int						m_nByteMaxWidthCount;
	int						m_nByteOtherInfoMaxWidthCount;
	int						m_nIconInfoRenderLine;
};


#include "IVUIIconInfo.inl"

#endif //__IVUIICONINFO_H__
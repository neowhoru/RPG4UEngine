/*////////////////////////////////////////////////////////////////////////
文 件 名：IMeshModel.h
创建日期：2006年11月17日
最后更新：2006年11月17日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __IMESHMODEL_H__
#define __IMESHMODEL_H__
#pragma once

#include "MeshDefine.h"
#include "IMeshLayer.h"
#include "IMeshAttachment.h"
#include "IMeshBone.h"
#include "IMeshBoneGroup.h"
#include "IMeshGeometry.h"
#include "IMeshLayer.h"
#include "IMeshLight.h"
#include "IMeshMaterial.h"
#include "IMeshParticleEmitter.h"
#include "IMeshRibbonEmitter.h"
#include "IMeshSequence.h"
#include "IMeshSkeleton.h"
#include "IMeshTexture.h"

struct sRAY_INFO;
/*////////////////////////////////////////////////////////////////////////

/*////////////////////////////////////////////////////////////////////////
class _V3DGRAPHIC_API IMeshModel
{
public:
	virtual LPCSTR	GetFilename		() = 0;
	virtual DWORD	GetMeshVer		() = 0;
	virtual void	Release			() = 0;
	virtual BOOL	LoadFromFile	( LPCSTR szFileName ) = 0;
	virtual BOOL	SaveToFile		( LPCSTR szFileName, DWORD dwFlag ) = 0;

public:
	virtual IMeshSequence*			GetSequence		() = 0;
	virtual IMeshSkeleton*			GetSkeleton		() = 0;
	virtual IMeshGeometry*			GetGeometry		() = 0;
	virtual IMeshSequenceArray*	GetSequences	() = 0;
	virtual IMeshBoneGroupArray*	GetBoneGroups	() = 0;

	virtual IMeshMaterialArray*			GetMaterials			() = 0;
	virtual IMeshTextureArray*				GetTextures				() = 0;
	virtual IMeshLightArray*				GetLights				() = 0;
	virtual IMeshAttachmentArray*			GetAttachments			() = 0;
	virtual IMeshRibbonEmitterArray*		GetRibbonEmitters		() = 0;
	virtual IMeshParticleEmitterArray*	GetParticleEmitters	() = 0;

	virtual BOOL								CreateRibbonEmitters		()=0;
	virtual BOOL								CreateParticleEmitters	()=0;

	//
public:
	virtual BOOL			SetSkeleton		( IMeshSkeleton* pSkeleton ) = 0;
	virtual BOOL			BindSkeleton	( IMeshSkeleton* pSkeleton ) = 0;
	virtual void			SetTransform	( V3DMatrix* pMatrix ) = 0;
	virtual V3DMatrix*	GetTransform	() = 0;

	virtual void			SetFrameID		( int nFrameId ) = 0;
	virtual int				GetFrameID		() = 0;

	virtual int				GetTextureID	( int nMaterialID, int nLayer ) = 0;
	virtual int				GetVertexCount	() = 0;
	virtual int				GetFaceCount	() = 0;

	virtual BOOL			IsBoneVisible	( int nFrameId, int nBoneId ) = 0;
	virtual BOOL			ExistMaterial	() = 0;

public:
	virtual BOOL	Intersect	(const Ray&	ray
										,sRAY_INFO& rayInfo
										,DWORD		dwFlag)= 0;

	virtual BOOL	Intersect	(const Ray&	ray
										,Vector3D&	vHit
										,DWORD		dwFlag)= 0;
	virtual BOOL	Intersect	(const Ray&	ray
										,float&		t
										,DWORD		dwFlag)= 0;

	virtual BOOL	Intersect	(float* pvStart
                              ,float* pvDir
										,float* pvHit
										,DWORD  dwFlag)= 0;

	virtual BOOL	Intersect	(float* pvStart
                              ,float* pvDir
										,float& t
										,DWORD  dwFlag)= 0;
};

#endif //__IMDLMODEL_H__
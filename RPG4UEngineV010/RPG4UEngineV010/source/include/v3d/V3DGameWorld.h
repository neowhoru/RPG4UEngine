/*////////////////////////////////////////////////////////////////////////
文 件 名：V3DGameWorld.h
创建日期：2007年6月18日
最后更新：2007年6月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __V3DGAMEWORLD_H__
#define __V3DGAMEWORLD_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "CommonDefine.h"
#include "V3dGameWorldDefine.h"
#include "V3DGameZoneManager.h"
#include "V3DTerrain.h"
#include "V3Dscene.h"
#include "ThreadMutex.h"
#include "V3DBspTree.h"
#include "V3DGameMap.h"
#include "V3DMapLoadManager.h"
#include "V3DGameLandConfig.h"
#include "V3DGameWorldListener.h"
#include "ListenerBaseManager.h"
#include "MeshAnimationCtrl.h"
#include "SaveListener.h"



class V3DTerrainChunk;
class V3DDayNightAnimation;
class V3DDayNightDriver;
class Archive;
struct sBRUSH_TERRAIN;
using namespace threadutil;
using namespace std;

struct sPAINTMATERAIL_INFO
{
	V3DTerrainBlender*	pBlender;
	INT						nTextureID;
	DWORD						paintType;
	V3DTerrainChunk*		pChunk;
};

typedef map<V3DTerrainBlender*,sPAINTMATERAIL_INFO>	TerrainBlenderMap;
typedef TerrainBlenderMap::iterator							TerrainBlenderMapIt;
typedef pair<V3DTerrainBlender*,sPAINTMATERAIL_INFO>	TerrainBlenderValue;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _GAMEMAP_API V3DGameWorld	: public SemiAutoCounterLock
											, public ListenerBaseManager
{
public:
	V3DGameWorld();
	~V3DGameWorld();

public:
	/////////////////////////////////////////
	BOOL				StartUp	();
	BOOL				Shutdown	();

	/////////////////////////////////////////
	BOOL				RequestWorkingData	();
	BOOL				ReleaseWorkingData	();


	/////////////////////////////////////////
	BOOL				CreateWorld	(LPCSTR szName
                              ,INT    nWidth
										,INT    nHeight);

	BOOL				LoadWorld	(LPCSTR     szLandName
                              ,INT        nWidth
										,INT        nHeight
										,INT			nHeroTilePosX = 0
										,INT			nHeroTilePosY = 0);

	BOOL				LoadWorldBy	(const  string& szConfigFile );
	BOOL				SaveWorld		();	
	BOOL				LoadLandInfo	(LPCSTR szLandName);	
	BOOL				SaveLandInfo	();	
	BOOL				SaveEnvironment(INT nPlotX,INT nPlotY);	
	BOOL				SerializeLand	(Archive& landDoc);

	BOOL				EnterPlotMap(INT  nX
                              ,INT  nY
										,BOOL bMult
										,BOOL bOnly1x1=FALSE);

protected:
	///////////////////////////////////////
	V3DGameMap*		_LoadPlotMap(INT  nX
                              ,INT  nY
										,BOOL bMult);

	BOOL				_InitData	(LPCSTR szPath
                              ,INT    nX
										,INT    nY);
public:
	///////////////////////////////////////
	//loading listen处理
	void DoReadyLoading		(UINT nMax);
	void DoProcessLoading	(UINT nProgress)	;
	void DoEndLoading			();

	void DoLoadMapPlot		(V3DGameMap* pMapPlot)	;
	void DoUnloadMapPlot		(V3DGameMap* pMapPlot)	;

public:
	BOOL				Update	(DWORD                   dwTime
                           ,const VECTOR3D&         vPos
									,const VECTOR3D&         vMap
									,const VECTOR3D&         vCamera
									,const VECTOR3D&         vCameraDir
									,const V3DFrustum *		 lpFrustum = NULL);
	BOOL				Update	(DWORD                   dwTime
                           ,const VECTOR3D&         vPos
									,const VECTOR3D&         vMap
									,const Ray&					 ray
									,const V3DFrustum *		 lpFrustum = NULL);

	void				UpdateBlockObjects(DWORD dwTick
												,RayC& cameraRay
												,float fBlockAlpha
												,DWORD dwBlockExpire);

	///////////////////////////////////////
	BOOL				RenderSky	(DWORD dwTick, const VECTOR3D &vPos );
	BOOL				RenderAlpha	(DWORD dwTick);

	BOOL				Render		(DWORD dwTick, BOOL bRenderTask, BOOL bWithScene=TRUE);
	BOOL				RenderScene	(DWORD dwTick, BOOL bWithTerrain, BOOL bRenderTask=FALSE);
	BOOL				RenderWater	(DWORD dwTick);
	BOOL				RenderGrass	(DWORD dwTick);

	void				PushAlphaObject	(V3DSceneObject*	pObject);

public:
	///////////////////////////////////////
	VOID		PushRenderTask	();
	VOID		PopRender		();

public:
	////////////////////////////////////////
	void				ResetNatureInfo		( void );
	void				ApplyNatureEffect		(void);
	void				UpateVertexNormals	();

	BOOL				LoadDayNightInfo			( LPCSTR szFile );	
	BOOL				UpdateDayNightAnimation	( void );

	BOOL				UpdateMiniMap				();
	FLOAT				ChangeFarPlane				(INT nFarChunk );

public:
	VOID				ClearRareUsing	( INT nX, INT nY );

	////////////////////////////////////////
	FLOAT					GetSkyBoxScale			(void );
	Vector3X				GetNormal				(float	x,		float y );
	Vector3X				GetVertices				(INT		nX,	INT	nY );
	Vector3D*			GetTerrainVertex		(const Vector3C& vPos);
	COLOR*				GetVertexDiffuse		(const Vector3C& vPos);
	Vector3D*			GetTerrainVertex		(INT		nTileX,	INT	nTileY);
	COLOR*				GetVertexDiffuse		(INT		nTileX,	INT	nTileY);

	LPCSTR 			GetZoneName				(int x, int y );
	const string&	GetName					();
	LPCSTR 			GetMapAlias				( int x, int y );
	INT				GetMiniMapTextureID	(INT x, INT y);
	LPCSTR			GetBGMusicFile			( int x, int y );

	V3DTerrainChunk*	GetChunk				( int col, int row );

	V3DTerrain*			GetTerrainInCache			(int nX, int nY );
	V3DTerrain*			GetTerrainInCacheByTile	(INT nX, INT nY);

	////////////////////////////////////////
	DWORD				GetGameTime			( void );
	void				SetGameTime			( DWORD dwTime );
	void				SetClockFactor		( DWORD dwTime );

	////////////////////////////////////////
	V3DGameMap*		GetHeroPlotMap		(VOID);
	void				SetHeroChunkPos	( INT nX, INT nY );
	VOID				GetHeroChunkXY		(int &nX, int &nY ) ;
	INT				GetRenderChunk		()		 ;

	float				GetHeightAt	(float        fCurrentZ
										,float        d
										,float        x
										,float        y
										,DWORD        dwFlag		=0
										,BOOL*        bInFloor	=NULL
										,DWORD*       pdwRet		=NULL
										,BOOL         bIsRole	=FALSE
										,VECTOR3D*	  pNormal	=NULL);

	Vector3X*		GetDecal		(const Vector3X &vLeftBottom
                              ,const Vector3X &vLeftTop
										,const Vector3X &vRightTop
										,const Vector3X &vRightBottom
										,int            &iTriCount
										,FLOAT          fOffsetZ=0.0f);

	////////////////////////////////////////
	void				SetPlotMap			( INT nIdx, V3DGameMap *pLvl );
	void				DeletePlotMap		( INT nIdx );
	V3DGameMap*		GetPlotMap			( INT nX, INT nY );
	V3DGameMap*		GetPlotMap			( INT nIdx );
	BOOL				CheckPlotBound		( INT nX, INT nY );					//如果是越界 不能显示和加载.
	BOOL				IsPlotLoaded		( INT nX, int nY );					//是否已经加载
	BOOL				IsReplacePlot		( INT nX, int nY, char *szFile );//检测此地图是否是需要替换
	V3DGameMap*		GetPlotMap3X3		( INT nX, INT nY );


	////////////////////////////////////////
	VOID					BeginThreadLoading();

	void					FindObjects			(INT              nID
                                       ,SceneObjectArray &vtObjs)		;

	////////////////////////////////////////////////
	V3DSceneObject*	PickSceneObject	(const Ray&    ray
                                       ,int				iPickMode = 0
													,sRAY_INFO*		pRayInfo  = NULL)		;

	V3DTerrain*			PickTerrain			(const Ray& ray
													,Vector3X &vCross
													,BOOL bBinDirect=TRUE);
	V3DTerrain*			PickTerrain			(const Ray& ray
													,float&		t	
													,BOOL bBinDirect=TRUE);

	BOOL					PickTerrainVT		(const Ray&             ray
													,vector<V3DTerrain*>*	vtTerrain
													,Vector3X               &vCross);


	////////////////////////////////////////////////
	sBSP_INTERSECT*	GetNearestIntersect	(const VECTOR3D&     vPos
                                          ,const VECTOR3D&     vDir
														,BOOL						bNormal = FALSE);
	sBSP_INTERSECT*	GetNearestIntersect	(const Ray&				ray
														,BOOL						bNormal = FALSE);

	BOOL				GetCollisionDistance	(const VECTOR3D&          vStart
                                       ,const VECTOR3D&          vDir
													,float*                   fDistToCollision
													,sBSP_INTERSECT** pIntersect);

	BOOL				GetCollisionDistance	(const Ray&						ray
													,float*							fDistToCollision
													,sBSP_INTERSECT**				pIntersect);

	BOOL				GetRayCrossPoint		(RayC& ray,VECTOR3D& vCross );

	//获取建筑、地面等可能交叉点
	BOOL				GetWorldCastRay		(RayC& ray,VECTOR3D& vCross);
	BOOL				GetWorldCastRay		(RayC& ray,float&		t, BOOL bNeg=FALSE);

	/////////////////////////////////////////////////////
	//取得信息
	UINT				GetPlotIndex	( INT nX, INT nY )	;
	UINT				GetMaxPlotMap	()				;


	/////////////////////////////////////////////////////

	void				DisposeRenderPipe			(TerrainChunkIdxArray& arTerrainChunks
                                          ,BOOL                  bUseTexture
														,BOOL                  bUsedMult=FALSE
														,INT                   nLvl=0
														,BOOL                  bUsedOld=FALSE);
protected:
	void				_DisposeTreeRenderPipe	(SceneObjectArray&		arTrees );
	VOID				_DisposeMultTexturePipe	(TerrainChunkIdxArray&	arTerrainChunks
														,INT							nEffectLayer);
	VOID				_DisposeShadowsPipe		(TerrainChunkIdxArray&	arTerrainChunks );



public:
	//////////////////////////////////////////////////////////////////////////
	BOOL			SetTerrainBrush	(const sBRUSH_TERRAIN& brush );
	BOOL			ReadyPaintMaterial(const sBRUSH_TERRAIN& brush );
	BOOL			PaintMaterial		(const sBRUSH_TERRAIN& brush );
	BOOL			PaintDiffuse		(const sBRUSH_TERRAIN& brush );
	BOOL			PaintTileMask		(const sBRUSH_TERRAIN& brush );
	BOOL			EndPaintMaterial	(const sBRUSH_TERRAIN& brush );
	BOOL			IsLock				();
	BOOL			EditorLock			();
	BOOL			EditorUnLock		();

	BOOL			OversewPlotMap		(LPCSTR szLandName=NULL,INT nPlotX=-1, INT nPlotY=-1);
	BOOL			OversewVertices	(Vector3D& vVertex0,Vector3D& vVertex1,BOOL bAtTail=FALSE);
	BOOL			OversewPlotMapEx	();
	BOOL			OversewChunk		(INT nChunkX, INT nChunkY );
	//顶点（世界绝对坐标）
	BOOL			SmoothGroud			(INT nX, INT nY, INT nRange =2 );
	BOOL			FlatGround			(int nX, INT nY, INT nRange =2);
	//Editor
public:
	BOOL			ConvertToNewMap	( PROC_NEWMAP fn );

	BOOL			ApplyImportTerrain(int   x
                                 ,int   y
											,int   EndX
											,int   EndY
											,DWORD nID);

	BOOL			RaiseGroudEx		(int  x
                                 ,int  y
											,REAL  fRaiseVal
											,BOOL bSet =     false
											,BOOL bUseMask = false);


	//////////////////////////////////////////////////////

public:
	//////////////////////////////////////////////////////
	V3DGameZoneManager			m_ZoneManager;
	V3DGameLandConfig				m_WorldInfo;
	LIGHT_INFO						m_GameLightInfo;
	LIGHT_INFO						m_HeroLightInfo;		//角色自身的灯光信息
	COLOR4F							m_colorRoleAmbient;	//角色自身的环境光信息
	COLOR4F							m_colorAmbient;		//环境光

	V3DDayNightDriver		*		m_pDayNightDriver;

public:
	VG_PTR_PROPERTY				(DayNightAnimation,		V3DDayNightAnimation);
	VG_BOOL_GET_PROPERTY			(Changing);
	VG_TYPE_PROPERTY				(FogData,		sFOG_WORLRD_INFO);
	VG_FLOAT_GET_PROPERTY		(SkyBoxHeight);
	VG_INT_GET_PROPERTY			(SkyBoxMeshID);
	VG_FLOAT_GET_PROPERTY		(FarPlane);


public:

protected:
	MeshAnimationCtrl				m_SkyBoxMesh;
	HANDLE							m_hLoadThread;
	vector<sMAPINFO_REPLACE>	m_ReplaceMaps;
	UINT								m_nRenderChunk;

	V3DGameMap			*			m_pHeroPlotMap;
	POINT								m_3x3PlotMaps			[GAMEMAP_CACHE_COL_SIZE]
																	[GAMEMAP_CACHE_COL_SIZE];
	INT								m_3x3MiniMapTextures	[GAMEMAP_CACHE_COL_SIZE]
																	[GAMEMAP_CACHE_COL_SIZE];

	VG_PTR_PROPERTY				(Camera, V3DCamera);
	V3DFrustum	*					m_pFrustum;
	BOOL								m_bLockFirstMap;

	BOOL			*					m_pMapLoadStates;

	INT								m_nHeroChunkX;
	INT								m_nHeroChunkY;

	
	INT								m_nCurrentPlotMapX;	//当前Terrain坐标的X,Y
	INT								m_nCurrentPlotMapY;
	INT								m_nNextPlotMapX;		//需要加载的坐标
	INT								m_nNextPlotMapY;
	
	VG_INT_GET_PROPERTY			(Width);//World的宽和高
	VG_INT_GET_PROPERTY			(Height);

	//Land名称
	string							m_szLandName;

	VG_TYPE_PROPERTY				(ReplacePath, string);//替换名
	BOOL								m_bLoading;
	Vector3X							m_vHeroPos;
	BOOL								m_bLock;

	TerrainBlenderMap				m_PaintMaterials;
	VG_DWORD_PROPERTY				(CheckSum);

	VG_PTR_PROPERTY				(DocListener, SaveListener);
	VG_DWORD_PROPERTY				(DocListenerStep);


};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(V3DGameWorld , _GAMEMAP_API);
//extern _GAMEMAP_API V3DGameWorld& singleton::GetV3DGameWorld();
#define theGameWorld  singleton::GetV3DGameWorld()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL2(SceneObjectArray,	AlphaObjectArray, _GAMEMAP_API);
GLOBALINST_DECL2(vector<INT>,			EffectObjectArray , _GAMEMAP_API);

#define theAlphaObjects		GetAlphaObjectArray()
#define theEffectObjects	GetEffectObjectArray()


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
namespace v3d
{
GLOBALINST_DECL2(TerrainChunkIdxArray, ChunkNotTextureBase, _GAMEMAP_API);
GLOBALINST_DECL2(TerrainChunkIdxArray, ChunkVectorBase,		_GAMEMAP_API);
GLOBALINST_DECL2(TerrainChunkIdxArray, ChunkVectorLayer0,	_GAMEMAP_API);
GLOBALINST_DECL2(TerrainChunkIdxArray, ChunkVectorLayer1,	_GAMEMAP_API);
GLOBALINST_DECL2(TerrainChunkIdxArray, ChunkVectorLayer2,	_GAMEMAP_API);
GLOBALINST_DECL2(SceneObjectArray,		SceneObjectTreePipe,	_GAMEMAP_API);

#define theNoTextureChunks			GetChunkNotTextureBase()
#define theLayerFloorChunks		GetChunkVectorBase()
#define theLayerEffect0Chunks		GetChunkVectorLayer0()
#define theLayerEffect1Chunks		GetChunkVectorLayer1()
#define theLayerEffect2Chunks		GetChunkVectorLayer2()
#define theSceneObjectTreePipe	GetSceneObjectTreePipe()

};

#include "V3DGameWorld.inl"

#endif //__V3DGAMEWORLD_H__
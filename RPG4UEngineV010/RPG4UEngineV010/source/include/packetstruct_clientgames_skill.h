/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketStruct_ClientGameS_Skill.h
创建日期：2007年9月11日
最后更新：2007年9月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PACKETSTRUCT_CLIENTGAMES_SKILL_H__
#define __PACKETSTRUCT_CLIENTGAMES_SKILL_H__
#pragma once



#include <DataTypeDefine.h>
#include "CommonDefine.h"
#include <Protocol_ClientGameS.h>
#include "StructBase.h"
#include "PacketStruct_Base.h"
#include "PacketStruct_ClientGameS_Define.h"

#pragma pack(push,1)

/*////////////////////////////////////////////////////////////////////////
// STYLE
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
//{
//}

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_PLAYER_ATTACK_BRD )
//{
	DWORD			dwAttackerKey;				// 
	BYTE			byAttackType;				// 
	SLOTCODE		StyleCode;					// 
	DWORD			dwClientSerial;			//
	DWORD			dwPrimaryTargetKey;		// 
	VECTOR3D		vCurPos;
	VECTOR3D		vDestPos;

	int				GetSize() { return SIZE_CG_MSG(STYLE_PLAYER_ATTACK_BRD); }
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
struct STYLE_ATTACK_RESULT_INFO
{
	DWORD			dwTargetKey;				// 
	DAMAGETYPE	wDamage;						// 
	DWORD			dwTargetHP;					// 
	VECTOR3D		vCurPos;
	VECTOR3D		vDestPos;
	BYTE			byEffect;
};

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_PLAYER_ATTACK_RESULT_BRD )
//{
	DWORD			dwClientSerial;				// 
	DWORD			dwAttackerKey;					// 
	BYTE			byNumberOfTargets;			// 

	STYLE_ATTACK_RESULT_INFO AttackInfo[MAX_TARGET_NUM];

	int				GetSize()
	{
		return SIZE_CG_MSG(STYLE_PLAYER_ATTACK_RESULT_BRD)
				- sizeof(STYLE_ATTACK_RESULT_INFO) * (MAX_TARGET_NUM - byNumberOfTargets);
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_SELECT_STYLE_SYN )
//{
	SLOTCODE					m_NewStyleCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_SELECT_STYLE_BRD )
//{
	DWORD						m_dwObjectKey;			// 
	SLOTCODE					m_CurStyleCode;		// 
	SLOTCODE					m_NewStyleCode;		// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_SELECT_STYLE_NAK )
//{
	SLOTCODE					m_CurStyleCode;
	BYTE						m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_LINK_SYN )
//{
	SLOTCODE					m_StyleCode;
	SLOTPOS					m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_LINK_ACK )
//{
	SLOTCODE					m_StyleCode;
	SLOTPOS					m_ToPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_LINK_NAK )
//{
	BYTE						m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_UNLINK_SYN )
//{
	SLOTPOS						m_atPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_UNLINK_ACK )
//{
	SLOTPOS						m_atPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_UNLINK_NAK )
//{
	BYTE						m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_LINKMOVE_SYN )
//{
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_LINKMOVE_ACK )
//{
	SLOTPOS						m_fromPos;
	SLOTPOS						m_toPos;	
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_STYLE_BEGIN( STYLE_LINKMOVE_NAK )
//{
	BYTE						m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



/*////////////////////////////////////////////////////////////////////////
// SKILL
/*////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_ACTION_SYN )
//{
	SLOTCODE		m_SkillCode;					// 
	DWORD			m_dwClientSerial;				// 
	VECTOR3D		m_vCurPos;						// 
	VECTOR3D		m_vDestPos;					// 
	DWORD			m_dwMainTargetKey;			// 
	VECTOR3D		m_MainTargetPos;			// 

	int GetSize() { return SIZE_CG_MSG(SKILL_ACTION_SYN); };
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////

//////////////////////////////////////////
MSGSKILLACTION_CG_SKILL_BEGIN( SKILL_ACTION_INSTANT_RESULT_BRD )
//{
	VECTOR3D		m_vCurPos;								// 
	VECTOR3D		m_vDestPos;							// 
	DWORD			m_dwAttackerHP;						// 
	DWORD			m_dwAttackerMP;						// 
	BYTE			m_byNumberOfTargets			: 6;	// 
	BYTE			m_byNumberOfFieldEffect		: 2;	// 
	
	int GetSize() { return SIZE_CG_MSG(SKILL_ACTION_INSTANT_RESULT_BRD); };
//};
MSGPACKET_CG_SKILLACTION_END;


//////////////////////////////////////////
MSGSKILLACTION_CG_SKILL_BEGIN( SKILL_ACTION_DELAY_START_BRD )
//{
	VECTOR3D		m_vCurPos;							
	VECTOR3D		m_vDestPos;						
	
	int GetSize() { return SIZE_CG_MSG(SKILL_ACTION_DELAY_START_BRD); };
//};
MSGPACKET_CG_SKILLACTION_END;


//////////////////////////////////////////
MSGSKILLACTION_CG_SKILL_BEGIN( SKILL_ACTION_DELAY_RESULT_BRD )
//{
	DWORD			m_dwAttackerHP;						// 
	DWORD			m_dwAttackerMP;						// 
	BYTE			m_byNumberOfTargets			: 6;	// 
	BYTE			m_byNumberOfFieldEffect		: 2;	//
	
	int GetSize() { return SIZE_CG_MSG(SKILL_ACTION_DELAY_RESULT_BRD); };
//};
MSGPACKET_CG_SKILLACTION_END;


//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_ACTION_NAK )
//{
	BYTE			m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_BASE
{
	DWORD			m_dwTargetKey;					// 目标对象编号
	BYTE			m_EffectFlag			: 5;	// NPC技能特效数量
	BYTE			m_byAbilityNum			: 3;	// Ability 数量

	int GetSize() { return sizeof(SKILL_RESULT_BASE); };
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_EFFECT
{
	WORD			m_wAbilityIndex;					// Ability Index
	VECTOR3D		m_vCurPos;							// 捞棋飘 困摹

	int GetSize() { return sizeof(SKILL_RESULT_EFFECT); }
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_CODE
{
	WORD			m_wAbilityIndex;					// Ability Index

	int GetSize() { return sizeof(SKILL_RESULT_CODE); };
};


/*////////////////////////////////////////////////////////////////////////
// ABILITY_DAMAGE
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_DAMAGE : SKILL_RESULT_CODE
{
	DAMAGETYPE	m_wDamage;		// 技能伤害
	DWORD			m_dwTargetHP;	// 目标对象 HP
	BYTE			m_byEffect;		// 技能附加效果 SKILL_EFFECT_CRITICAL ...
	int GetSize() { return sizeof(SKILL_RESULT_DAMAGE); };
};

/*////////////////////////////////////////////////////////////////////////
// ABILITY_ABNORMAL_STATUS
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_STUN : SKILL_RESULT_CODE
{
	VECTOR3D		m_vCurPos;							// 傍拜措惑狼 泅犁 谅钎

	int GetSize() { return sizeof(SKILL_RESULT_STUN); };
};

/*////////////////////////////////////////////////////////////////////////
// ABILITY_KNOCKBACK
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_POSITION : SKILL_RESULT_CODE
{
	VECTOR3D		m_vCurPos;						
	VECTOR3D		m_vDestPos;					

	int GetSize() { return sizeof(SKILL_RESULT_POSITION); };
};


/*////////////////////////////////////////////////////////////////////////
// ABILITY_EXHAUST_HP, ABILITY_EXHAUST_MP
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_EXHAUST : SKILL_RESULT_CODE
{
	DWORD			m_dwTargetHP;				
	DWORD			m_dwTargetMP;				

	int GetSize() { return sizeof(SKILL_RESULT_EXHAUST); };
};


/*////////////////////////////////////////////////////////////////////////
// ABILITY_FIGHTING_ENERGY_NUM_INCREASE, ABILITY_BONUS_DAMAGE_PER_FIGHTING_ENERGY
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_FIGHTING_ENERGY : SKILL_RESULT_CODE
{
	SHORT			m_sFightingEnergyCount;	

	int GetSize() { return sizeof(SKILL_RESULT_FIGHTING_ENERGY); };
};


/*////////////////////////////////////////////////////////////////////////
// ABILITY_RESURRECTION
/*////////////////////////////////////////////////////////////////////////
struct SKILL_RESULT_RESURRECTION : SKILL_RESULT_CODE
{
	VECTOR3D		m_vCurPos;		
	DWORD			m_dwTargetHP;	
	DWORD			m_dwTargetMP;	

	int GetSize() { return sizeof(SKILL_RESULT_RESURRECTION); };
};







/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct DAMAGE_INFO
{
	DWORD			m_dwTargetKey;						// 
	DAMAGETYPE	m_wDamage;							// 
	DWORD			m_dwTargetHP;						// 
};


//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_PERIODIC_DAMAGE_BRD )
//{
	DWORD			m_dwAttackerKey;					// 
	SLOTCODE		m_SkillCode;						// 
	BYTE			m_byNumberOfTargets;				// 
	DAMAGE_INFO	m_DamageInfo[MAX_TARGET_NUM];	

	int				GetSize()
	{
		return SIZE_CG_MSG(SKILL_PERIODIC_DAMAGE_BRD) - sizeof(DAMAGE_INFO) * (MAX_TARGET_NUM - m_byNumberOfTargets);
	}
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_REFLECT_DAMAGE_BRD )
//{
	DWORD			m_dwAttackerKey;					//
	DWORD			m_dwTargetKey;						//
	SLOTCODE		m_SkillCode;						//
	WORD			m_wAbilityCode;					
	DAMAGETYPE	m_wDamage;						
	DWORD			m_dwTargetHP;						//

	int GetSize() { return SIZE_CG_MSG(SKILL_REFLECT_DAMAGE_BRD); };
//}
MSGPACKET_CG_OBJECT_END;


//-------------------------------------------------------------------------------------------------
//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_SELECT_SKILLPOINT_SYN )
//{
	bool				m_bSkill;						// 
	SLOTCODE			m_SkillCode;					// SkillCode, StyleCode
//}
MSGPACKET_CG_OBJECT_END;
		

//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_SELECT_SKILLPOINT_ACK )
//{
	SLOTCODE			m_OldSkillCode;					// 
	sSKILL_SLOT		m_NewSkillSlot;					// 
	DWORD				m_dwRemainSkillPoint;			// 
//}
MSGPACKET_CG_OBJECT_END;

//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_SELECT_SKILLPOINT_NAK )
//{
	SLOTCODE		m_SkillCode;
	BYTE			m_byErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_PASSIVE_SKILL_LIST_CMD )
//{
	DWORD				m_dwObjectKey;	
	BYTE				m_byCount;
	SLOTCODE			m_SkillCode[MAX_SKILL_SLOT_NUM];

	int GetSize() 
	{
		return (SIZE_CG_MSG(SKILL_PASSIVE_SKILL_LIST_CMD)-(MAX_SKILL_SLOT_NUM-m_byCount)*sizeof(SLOTCODE));
	}
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_SKILL_BEGIN( SKILL_PASSIVE_SKILL_ADD_CMD )
//{
	DWORD			m_dwObjectKey;			
	SLOTCODE		m_OldSkillCode;	
	SLOTCODE		m_NewSkillCode;		
//}
MSGPACKET_CG_OBJECT_END;





/*////////////////////////////////////////////////////////////////////////
STATUS
/*////////////////////////////////////////////////////////////////////////


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_LEVEL_UP_BRD )
//{
	DWORD						m_dwObjectKey;					// 
	LEVELTYPE				m_Level;						// 
	WORD						m_wRemainStat;					// 
	WORD						m_wRemainSkill;				// 
	DWORD						m_dwCurHP;						// 
	DWORD						m_dwCurMP;						// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_LEVEL_UP_CMD )
//{
	LEVELTYPE				m_Level;						// 
	WORD						m_wRemainStat;					// 
	WORD						m_wRemainSkill;					// 
	DWORD						m_dwCurHP;						// 
	DWORD						m_dwCurMP;						// 
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN(STATUS_RECOVER_ATTR_BRD)
//{
	DWORD						m_dwObjectKey;					// 
	DWORD						m_dwTargetHP;					// 
	DWORD						m_dwTargetMP;					// 
//};
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_RESURRECTION_SYN )
//{
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_RESURRECTION_NAK )
//{
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_RESURRECTION_BRD )
//{
	DWORD						m_dwObjectKey;					// 
	DWORD						m_dwHP;							// 
	DWORD						m_dwMP;							// 
	VECTOR3D					m_vCurPos;						// 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_TITLE_CHANGED_CMD )
//{
	DWORD						m_dwTitleFlag;					//< ePLAYER_TITLE 
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_TITLE_CHANGED_BRD )
//{
	DWORD						m_dwPlayerKey;
	DWORD						m_dwTitleFlag;					//< ePLAYER_TITLE
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN(STATUS_DEAD_BRD)
//{
	DWORD						m_dwObjectKey;					// 
	VECTOR3D					m_vCurPos;					
	DWORD						m_dwExp;						//
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN(STATUS_EXP_CMD)
//{
	DWORD						m_dwTargetObjKey;			
	DWORD						m_dwExp;						//
//};
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_ATTR_CHANGE_BRD )
//{
	DWORD						m_dwObjKey;
	BYTE						m_AttrType;						// eATTRIBUTE_TYPE
	DWORD						m_dwValue;						// value
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_STAT_SELECT_SYN )
//{
	BYTE						m_AttrType;						// eATTRIBUTE_TYPE
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_STAT_SELECT_ACK )
//{
	DWORD						m_dwObjKey;
	BYTE						m_AttrType;						// eATTRIBUTE_TYPE
	DWORD						m_dwValue;						// value
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_STAT_SELECT_NAK )
//{
	BYTE						m_AttrType;						// eATTRIBUTE_TYPE
	DWORD						m_dwErrorCode;
//}
MSGPACKET_CG_OBJECT_END;

							   
// 某腐磐 惑怕 函版
//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_CONDITION_CHANGE_SYN )
//{
	BYTE						m_byConditionType;		
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_CONDITION_CHANGE_BRD )
//{
	DWORD						m_dwObjectKey;					// 
	BYTE						m_byConditionType;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_CONDITION_CHANGE_NAK )
//{
	BYTE						m_byErrorCode;					// eCONDITION_RESULT
//}
MSGPACKET_CG_OBJECT_END;


// 惑怕 秦力
//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_REMOVE_BRD )
//{
	DWORD						m_dwTargetKey;					
	DWORD						m_dwStatusCode;				
	int GetSize() { return SIZE_CG_MSG(STATUS_REMOVE_BRD); };
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_EMOTION_SYN )
//{
	BYTE						m_byEmotionType;	
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_EMOTION_BRD )
//{
	DWORD						m_dwObjectKey;					// 
	BYTE						m_byEmotionType;		
//}
MSGPACKET_CG_OBJECT_END;


// 状态变更
//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_NPC_STATE_CHANGE_CMD )
//{
	DWORD						m_dwObjectKey;					// 目标
	BYTE						m_byState;						// NPC状态(eNPC_STATE)
//}
MSGPACKET_CG_OBJECT_END;



//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_RENDER_OPTION_SYN )
//{
	enum CONST_VAL {	eRENDER_HELMET_OPTION = 0,};
	BYTE						m_byRenderOptionType;	
	BYTE						m_byOptionLevel;			
//}
MSGPACKET_CG_OBJECT_END;


//////////////////////////////////////////
MSGOBJECT_CG_STATUS_BEGIN( STATUS_RENDER_OPTION_BRD )
//{
	DWORD						m_dwObjectKey;				
	BYTE						m_byRenderOptionType;	
	BYTE						m_byOptionLevel;			
//}
MSGPACKET_CG_OBJECT_END;







#pragma pack(pop)



#endif //__PACKETSTRUCT_CLIENTGAMES_SKILL_H__
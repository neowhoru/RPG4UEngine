/*++
Abstract:

    提供一个自适应TCHAR的StringEx,与TCHAR配合使用
    自适应Unicode与ANSI C
    支持UTF-8编码转换
    支持Base64编码转换
    注：它是为方便使用而设计的，并非为效率而设计，
    所以在效率要求很高的场合应该寻找更合适的方式

Author:


Revision History:

    增加UTF-8编码支持功能                                   17-Nov-2005

    去除所有函数的Virtual声明，使之不允许被继承
    修改format函数，添加忽略大小写比较函数，大小写转换函数
    添加几个宏方便CHAR与STRING的转换
    去除UTF8转换函数中的线程锁定                            11-Apr-2006
    添加Replace函数                                         17-Apr-2006
    添加Right,Left,Mid,Find,ReverseFind                     7-Nov-2006
    添加Base64转换                                          7-Nov-2006

--*/

#ifndef _H_T_STRING
#define _H_T_STRING

#include <string>
#include <TCHAR.h>


namespace Common
{

#define _tANSICHAR(x)       Common::StringEx(x).toNarrowString().c_str()
#define _tUNICODECHAR(x)    Common::StringEx(x).toWideString().c_str()
#define _tUTF8CHAR(x)       Common::StringEx(x).toUTF8().c_str()

#define _tstringA(x)        Common::StringEx(x).toNarrowString()
#define _tstringW(x)        Common::StringEx(x).toWideString()




class /*_BASELIB_API*/ StringEx
		#ifdef _UNICODE
			 : public std::wstring
		#else
			 : public std::string
		#endif
{
public:
	StringEx(void){}
	~StringEx(void){}

public:
    void fromUTF8(const char* pszUTF8);
    std::string toUTF8();

    void fromBASE64(const char* pszBASE64);
    std::string toBASE64();

    std::string toNarrowString();
    std::wstring toWideString();

public:
    StringEx& MakeLower();
    StringEx& MakeUpper();

    int CompareNoCase(StringEx str);

    unsigned int Replace(StringEx strOld, StringEx strNew);
    unsigned int Replace(wchar_t chOld, wchar_t chNew);
    unsigned int Replace(char chOld, char chNew);

    StringEx Right(size_t nCount);
    StringEx Left(size_t nCount);
    StringEx Mid(size_t iFirst, size_t nCount);

    size_t Find(const TCHAR ch, unsigned int nStart = 0);
    size_t Find(const TCHAR* pszch, unsigned int nStart = 0);
    size_t ReverseFind(const TCHAR ch);

public:
    //实现sprintf功能
    void Format(const char* pszFormat, ...);
    void Format(const wchar_t* pwszFormat, ...);


//正则表达式匹配功能:
//public:
//
//    //匹配: strPattern为规则字串, strResult为匹配成功的字串
//    virtual bool match(const StringEx& strPattern, StringEx& strResult);
//
//    //匹配: 不需要返回值只获得是否匹配成功
//    virtual bool match(const StringEx& strPattern);
//
//
//    virtual bool match(const char* pStr, StringEx& strResult);
//    virtual bool match(const wchar_t* pwStr, StringEx& strResult);
//    virtual bool match(const std::string& str, StringEx& strResult);
//    virtual bool match(const std::wstring& str, StringEx& strResult);
//
//    virtual bool match(const char* pStr);
//    virtual bool match(const wchar_t* pwStr);
//    virtual bool match(const std::string& str);
//    virtual bool match(const std::wstring& str);

public:

#ifdef _UNICODE
    using std::wstring::operator =;
#else
    using std::string::operator =;
#endif
    //重载=号函数
    StringEx& operator = (char ch);
    StringEx& operator = (wchar_t ch);
    StringEx& operator = (const char* pStr);
    StringEx& operator = (const wchar_t* pwStr);
    StringEx& operator = (const StringEx& Str);
    StringEx& operator = (const std::string& Str);
    StringEx& operator = (const std::wstring& Str);

public:
    //构造函数
    StringEx(wchar_t ch);
    StringEx(char ch);
    StringEx(const char* pStr);
    StringEx(const wchar_t* pwStr);
    StringEx(const StringEx& str);
    StringEx(const std::string& str);
    StringEx(const std::wstring& str);

protected:

    //ANSI C串与UNICODE串转换功能
    std::wstring toWideString(const char* pStr);
    std::string toNarrowString(const wchar_t* pwStr);

    //UTF-8与UNICODE串转换功能
    std::string wcstoUTF8(const wchar_t* pwStr);
    std::wstring UTF8towcs(const char* pStr);

    std::string cstobase64(const char* pStr, size_t nLength);
    std::string base64tocs(const char* pStr);
};

};

#endif
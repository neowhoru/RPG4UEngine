/*////////////////////////////////////////////////////////////////////////
文 件 名：MacroTextParser.h
创建日期：2007年9月23日
最后更新：2007年9月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	宏文本解释器
		宏代码标准格式： $[Name,param1,param2,...]
			$[		宏代码标志开始
			Name	首单词，为宏代码名称，不区分大小
			,...	每逗号后面为参数，参数数量不限制
					参数将自动识别数值和字符串

	宏文件解释结果：
			a.原文引用
			b.宏代码释义
		以上结果将按照所在原文位置为依据，有序地放到指定的结果列表中
		使用时，只需要遍历结果列表即可

代码范例：
struct MacroTest
{
	DataChunker			buff(NULL);
	MacroTextParser	macro;
	macro.Init(&buff);
	macro.ParseFile("D:\\Macro.txt");
	for(UINT n=0; n<macro.GetTokenCount(); n++)
	{
		SMacroToken& token = macro.GetTokenAt(n);
		OutputDebugString(token.szOriginalText);
		OutputDebugString("\n");
	}

范例内容：
		$[Name,0xff00ff,left] +$[level,0] $[ItemType,] $[Rank]
		$[EquipLimit,0xff00ff,right]
		等级限制：$[LevelLimit,0xff0000ff,0xff0000aa,left]
		职业要求：$[Equips] $[Equip1]
		附加属性：




版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __MACROTEXTPARSER_H__
#define __MACROTEXTPARSER_H__
#pragma once

#include "MacroTextParserDefine.h"

class DataChunker;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API MacroTextParser
{
public:
	MacroTextParser();
	~MacroTextParser();

public:
	BOOL Init(DataChunker* pTextBuffer,BOOL bGlobalString);
	void Clear();

	BOOL ParseFile	(LPCSTR szFile);
	///@desc 调用时，注意szBuffer结尾须有\n结束，否则最后一行无法解释
	BOOL ParseText	(char* szBuffer, DWORD dwLength);

	UINT				GetTokenCount();
	SMacroToken&	GetTokenAt(UINT nAt);

public:
	void		SetNormalMode	(){m_MacroMode = eMACROMODE_NORMAL;}
	void		SetAspMode		(){m_MacroMode = eMACROMODE_ASPCODE;}
	void		SetPhpMode		(){m_MacroMode = eMACROMODE_PHPCODE;}

protected:
	void _ParseParam	(char* szText,SMacroParam& param);
	BOOL		_isnumber( char c )	{return c >= '0' && c <= '9' || c == '.' || c == '-' || c == '+';};

protected:
	VG_PTR_PROPERTY	(TextBuffer, DataChunker);
	MacroTokenArray	m_arTokens;
	VG_BOOL_PROPERTY	(GlobalString);
	VG_BOOL_PROPERTY	(ParseNewLine);
	VG_BOOL_PROPERTY	(ParseComment);
	VG_TYPE_PROPERTY	(MacroMode, eMACRO_MODE);
	
	sMACRO_LABEL		m_arMacroModes[eMACROMODE_MAX];
};


#endif //__MACROTEXTPARSER_H__
#ifndef N_HASHLIST_H
#define N_HASHLIST_H
//------------------------------------------------------------------------------
/**
    @brief  A doubly linked list of named nodes with fast hashtable based search.

    @author
    - RadonLabs GmbH 

    @since
    - 2005.6.30
    @remarks
    - ���� �߰� 
*/

#include "CommonDefine.h"
#include "nhashtable.h"
#include "nhashnode.h"

//------------------------------------------------------------------------------
class _BASE_API nHashList : public ListBase 
{
public:
    // default constructor
    nHashList();
    /// constructor with given hashtable size
    nHashList(int hashsize);
    /// get first node
    nHashNode* GetHead() const;
    /// get last node
    nHashNode* GetTail() const;
    /// add node to beginning of list
    void AddHead(nHashNode* n);
    /// add node to end of list
    void AddTail(nHashNode* n);
    /// remove first node
    nHashNode* RemoveHeader();
    /// remove last node
    nHashNode* RemoveTail();
    /// search node by name
    nHashNode* Find(const char* name) const;

private:
    enum 
    {
        N_DEFAULT_HASHSIZE = 16,
    };
    nHashTable h_table;
};

//------------------------------------------------------------------------------
/**
*/
inline
nHashList::nHashList() :
    h_table(N_DEFAULT_HASHSIZE)
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
inline
nHashList::nHashList(int hashsize) :
    h_table(hashsize)
{
    // empty
}

//------------------------------------------------------------------------------
/**
*/
inline
nHashNode*
nHashList::GetHead() const
{
    return (nHashNode *) ListBase::GetHead();
}

//------------------------------------------------------------------------------
/**
*/
inline
nHashNode*
nHashList::GetTail() const
{
    return (nHashNode *) ListBase::GetTail();
}

//------------------------------------------------------------------------------
/**
*/
inline
void 
nHashList::AddHead(nHashNode* n)
{
    ASSERT(n);
    n->SetHashTable(&(this->h_table));
    this->h_table.Add(&(n->str_node));
    ListBase::AddHead((BaseNode*) n);
}

//------------------------------------------------------------------------------
/**
*/
inline
void
nHashList::AddTail(nHashNode *n)
{
    ASSERT(n);
    n->SetHashTable(&(this->h_table));
    this->h_table.Add(&(n->str_node));
    ListBase::AddTail((BaseNode*) n);
}

//------------------------------------------------------------------------------
/**
*/
inline
nHashNode*
nHashList::RemoveHeader()
{
    nHashNode *n = (nHashNode *) ListBase::RemoveHeader();
    if (n) 
    {
        n->str_node.Remove();
        n->SetHashTable(0);
    }
    return n;
}

//------------------------------------------------------------------------------
/**
*/
inline
nHashNode*
nHashList::RemoveTail()
{
    nHashNode *n = (nHashNode *) ListBase::RemoveTail();
    if (n) 
    {
        n->str_node.Remove();
        n->SetHashTable(0);
    }
    return n;
};

//------------------------------------------------------------------------------
/**
*/
inline
nHashNode*
nHashList::Find(const char *name) const
{
    nStrNode *sn = this->h_table.Find(name);
    if (sn) 
    {
        return (nHashNode *) sn->GetPtr();
    }
    else    
    {
        return 0;
    }
}

//------------------------------------------------------------------------------
#endif    

/*////////////////////////////////////////////////////////////////////////
文 件 名：ScriptWord.h
创建日期：2007年1月18日
最后更新：2007年1月18日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	脚本单词解释器

版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SCRIPTWORD_H__
#define __SCRIPTWORD_H__

#pragma once

#include "CommonDefine.h"

class _BASE_API ScriptWord 
{
	enum
	{
		eMaxWord = 32
	};
	struct _BASE_API OneWord
	{
		int		nStart;
		int		nLen;
		char*		pszStart;
		OneWord():nStart(0),nLen(0),pszStart(NULL){}
	};
public:
	
	ScriptWord();
	ScriptWord	(LPCTSTR		pszScriptLine
					,TCHAR		cSeperator=' '
               ,LPCTSTR     szComment1=NULL
					,LPCTSTR     szComment2=NULL);
	~ScriptWord();

public:
	void		Clear				();
	int		Parse				(LPCTSTR		 pszScriptLine
                           ,LPCTSTR      szComment1=NULL
									,LPCTSTR      szComment2=NULL
									);
	BOOL		IsLineComment	(INT			 nIndex
                           ,LPCTSTR		 szComment1
									,LPCTSTR		 szComment2);



	int		GetWordCount();
	LPCTSTR	GetWord		( int i );
	double	GetFloat		( int i );
	DWORD		GetNumber	( int i );
	BOOL		IsNumber		( int i );
	BOOL		Compare		( int i, LPCTSTR s );
	INT		FindWord			( LPCTSTR szWord, BOOL bCase=FALSE);
	INT		FindWordPrefix	( LPCTSTR szWord, BOOL bCase=FALSE);
	INT		FindWordInText	( LPCTSTR szText, BOOL bCase=FALSE);

	BOOL		IsTextInAnyWord	( LPCTSTR szText, BOOL bCase=FALSE);
	BOOL		IsAnyWordInText	( LPCTSTR szText, BOOL bCase=FALSE);

	LPCTSTR	operator[]	(INT  i);

	void		ConcatWordsTo				(StringHandle& sText,LPCTSTR szSep=_T(" "));		
	BOOL		CombineWordFromXToEnd	(int   x
                                    ,char* pszBuffer
												,int   nBufferSize);

	BOOL		CreateFrom					(int         nStart
                                    ,ScriptWord* pSrcWord)		;

	static BOOL	IsNumber( LPCTSTR s );

protected:
	OneWord	m_words2[eMaxWord];
	int		m_nNumWord;
	int		m_nLength;

	VG_CHAR_PROPERTY		(Seperator);
	VG_PTR_GET_PROPERTY	(OriginText,TCHAR);
};


inline double	ScriptWord::GetFloat		( int i )
{
	return atof(GetWord(i));
}
inline DWORD ScriptWord::GetNumber	( int i )
{
	return (DWORD)atoi(GetWord(i));
}

inline LPCTSTR ScriptWord::operator[]	(INT i)
{
	return GetWord(i);
}


#endif //__SCRIPTWORD_H__
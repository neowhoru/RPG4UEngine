#pragma once

#include "ConstDefine.h"
#include "ItemDropParser.h"
#include <BaseResParser.h>
#include <THashTable.h>


struct sDROP_MONEY_RATIO
{
	DWORD m_Code;
	VRSTR	m_Name;
	DWORD	m_Grade;
	DWORD m_dwIndex;
	DWORD	m_dwMultipleMin;
	DWORD	m_dwMultipleMax;
	float	m_PlusGradePercent;
};
typedef const sDROP_MONEY_RATIO	DROP_MONEY_RATIOC;

using namespace std;
using namespace util;

typedef map<DWORD, sDROP_MONEY_RATIO>	DropMoneyRatioMap;
typedef DropMoneyRatioMap::iterator		DropMoneyRatioMapIt;
typedef pair<DWORD, sDROP_MONEY_RATIO>	DropMoneyRatioMapPair;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API MoneyDropParser: public BaseResParser
{
public:
	MoneyDropParser(void);
	virtual ~MoneyDropParser(void);

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sDROP_MONEY_RATIO, m_pDataInfos, m_Code);
	BASERES_UNIT_TABLE	(sDROP_MONEY_RATIO, DropMoney);

public:
	virtual BOOL				Init		(DWORD dwPoolSize);
	virtual VOID				Release	();
	virtual BOOL				Load	( LPCTSTR pszFileName, BOOL bReload = FALSE );
	virtual VOID				Reload();

	virtual DROP_MONEY_RATIOC&	GetDropRatio( eNPC_GRADE Grade, DWORD dwType );
	void _Add(INT nGrade,sDROP_MONEY_RATIO&	info){m_DropRatios[nGrade].insert(DropMoneyRatioMapPair(info.m_dwIndex,info));}
private:
	///////////////////////////////////
	DropMoneyRatioMap 	m_DropRatios[NPC_GRADE_MAX];
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(MoneyDropParser , _SERVERCORE_API);
//extern API_NULL MoneyDropParser& singleton::GetMoneyDropParser();
#define theMoneyDropParser  singleton::GetMoneyDropParser()



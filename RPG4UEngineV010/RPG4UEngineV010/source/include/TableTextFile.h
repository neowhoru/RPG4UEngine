/*////////////////////////////////////////////////////////////////////////
文 件 名：TabTextFile.h
创建日期：2008年4月3日
最后更新：2008年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	本类主要用来读取 execel导出的Table格式的文本内容
	可操作Bin型、Text型两类文件
	但 本操作类，无法判断行的变化，故在读取 Table型的行记录时，无法验证数据的正确性


	@usage
	- 推荐用法	
	- 用法3 行记录解析，只处理Tab符分隔，像用Excel导出的文本，便能很好处理

		util::TableTextFile file;
		file.OpenFile("F:\\temp\\aa.txt",TRUE);	///第2个参数,须为TRUE

		DWORD		dwID;
		LPCSTR	sz;
		VRSTR		vStr;			///

		for( ;file.GotoNextLine(); )
		{
			dwID	= file.GetDword();
			sz		= file.GetString();
			sz		= file.GetStringInQuote();

			file >> vStr;			/// 将把字符中自动注册到 GlobalString中，不区分大小写
			file >= vStr;			/// 将把字符中自动注册到 GlobalString中，区分大小写
			file >= dwID;			/// >=符对其它非VRSTR变量时，不严格检测数据类型
			file >> dwID;
			file >> sz;
				file(ArrayNum);	///指定数量
			file >> Array;			///连续的字段导到数组	数据自动识别变量类型
				file(0);				///数组清除标志
		}
		file.CloseFile();

		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	- 用法1
		TableTextFile sr;
		sr.OpenFile( pszCalcCharFileName, SEP_QUOT_TEXT );

		while( TOKEN_END != sr.GetTokenType() )
		{		
			int iGetNumber = sr.GetTokenNumber();
			sr.GetTokenType( TOKEN_STRING );
			strncpy( pszCharName, sr.GetTokenString(), MAX_CHARNAME_LENGTH );
		}

		sr.CloseFile();

		@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	- 用法2
		TableTextFile sr;
		sr.OpenFile( pszCalcCharFileName, SEP_QUOT_TEXT );

		while( !sr.IsEOF() )
		{
			int iGetNumber = sr.GetNextNumber();
			strncpy( pszCharName, sr.GetNextString(), MAX_CHARNAME_LENGTH );
		}

		sr.CloseFile();



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TABLETEXTFILE_H__
#define __TABLETEXTFILE_H__
#pragma once

#include "TableTextFileDefine.h"
#include "StringHandle.h"

union TAG_CODE;
namespace util 
{

class _BASE_API TableTextFile
{
	enum 
	{
		 MAX_TOKEN_BUFFER_SIZE			= 1024
		,VALUE_TEXT							= 0
		,VALUE_NUMBER						= 1
		,VALUE_FLOAT						= 2
	};

public:
	TableTextFile();
	~TableTextFile();

public:
	BOOL				OpenFile		(LPCSTR                 pszFileName
										,BOOL                   bInLine	= TRUE
										,eSEPERATOR_MODE			mode		= SEP_WHITESPACE);

	BOOL				OpenBuffer	(BYTE *                 pMemoryBuffer
                              ,DWORD                  dwBufferSize
										,BOOL                   bInLine	= FALSE
										,eSEPERATOR_MODE			mode		= SEP_WHITESPACE);

	VOID				CloseBuffer	();
	VOID				CloseFile	();

public:
	eTOKEN_TYPE		GetTokenType	( eTOKEN_TYPE eTokenType = TOKEN_ANYTYPE );

	char *			GetTokenString	() { return m_pszTokenString;	}
	double			GetTokenFloat	() { return m_dTokenNumber;	}
	DWORD				GetTokenNumber	() { return m_dwNumber;	}

public:
	BOOL				GotoNextLine	(BOOL bForceNextLine=FALSE);
	BOOL				IsEOF				();
	BOOL				IsBlankLine		()	;

	char *			GetNextString	( eTOKEN_TYPE eTokenType = TOKEN_STRING );
	double			GetNextNumber	( eTOKEN_TYPE eTokenType = TOKEN_NUMBER );
	VOID				Reset				();
	void				SkipOneToken	(){GetTokenType(TOKEN_ANYTYPE);}
	
public:
	template <class T> inline
	TableTextFile& operator >> ( T &Data);
	template <class T> inline
	TableTextFile& operator >= ( T &Data);

	TableTextFile& operator >> ( TAG_CODE &Data);
	TableTextFile& operator >> ( float &Data);
	TableTextFile& operator >> ( double &Data);
	TableTextFile& operator >> ( LPCSTR &Data);
	TableTextFile& operator >> ( StringHandle &Data);

	///> >>符在注册VRSTR时，不区分大小
	TableTextFile& operator >> ( VRSTR &Data);
	///> >>符在注册VRSTR时，区分大小
	TableTextFile& operator >= ( VRSTR &Data);

	/// >> 符严格检测数据类型
	///> 以下使用方法，参考 operator()的说明
	template <class T> inline	
	TableTextFile& operator >> ( T Data[]);

	/// >= 符不严格检测数据类型
	/// 以下使用方法，参考 operator()的说明
	template <class T> inline	
	TableTextFile& operator >= ( T Data[]);

	///> 设定序列数组数量，与 operator >> dat[]  配合使用
	///> 方法 (*this)(num) >> dat[]
	inline TableTextFile& operator() (DWORD dwNum);

	inline TableTextFile& operator() ();


public:
	////////////////////////////////////////////
	//字段值获取
	#define FR_DECLARE_VALUE(Name,Type)\
								inline Type	Get##Name()		{GetTokenType(TOKEN_NUMBER); return (Type)GetTokenNumber();}
			
	FR_DECLARE_VALUE(Int,	int);
	FR_DECLARE_VALUE(Dword,	DWORD);
	FR_DECLARE_VALUE(Long,	LONG);
	FR_DECLARE_VALUE(Float,	float);
	FR_DECLARE_VALUE(Word,	WORD);
	FR_DECLARE_VALUE(Byte,	BYTE);
	FR_DECLARE_VALUE(Bool,	BOOL);
	FR_DECLARE_VALUE(Char,  char);

	LPCSTR	GetString()				;
	LPCSTR	GetStringInQuote();	


private:
	//////////////////////////////////
	//内部函数
	VOID				_Clear	();

	eTOKEN_TYPE		_ParseNumber(char	c);	
	eTOKEN_TYPE		_SkipNonContent(DWORD* pPos=NULL);		//跳过非正文内容部分，如comment 空格等

	char		_GetChar		( DWORD * pPos = NULL );
	char		_GetCurChar	( DWORD * pPos = NULL );
	VOID		_Rewind		( int i , DWORD * pPos = NULL );

	BOOL		_IsSep		( char c )		;
	BOOL		_IsBlank		( char c )	;
	BOOL		_IsSpace		( char c )	;
	BOOL		_IsNewLine	( char c )	;
	BOOL		_IsNumber	( char c )	;;


	eTOKEN_TYPE			_GetNextTokenTypeWhiteSpace			( eTOKEN_TYPE eTokenType );
	eTOKEN_TYPE			_GetNextTokenTypeQuotText				( eTOKEN_TYPE eTokenType );
	eTOKEN_TYPE			_GetNextTokenTypeNewLine				( eTOKEN_TYPE eTokenType );
	eTOKEN_TYPE			_GetNextTokenTypeWhiteSpaceNString	( eTOKEN_TYPE eTokenType );

protected:
	DWORD					m_dwSerializeNum;
	DWORD					m_dwValueType;
	double				m_dTokenNumber;
	DWORD					m_dwNumber;
	char					m_pszTokenString[MAX_TOKEN_BUFFER_SIZE];
	DWORD					m_dwBufferPos;
	DWORD					m_dwBufferSize;
	char *				m_pszBuffer;
	eSEPERATOR_MODE	m_Mode;
	BOOL					m_bInLine;	//限于行内数据解释，不自动跳行，须手工换行
};


} /// namespace util

#include "TableTextFile.inl"

#endif //__TABLETEXTFILE_H__


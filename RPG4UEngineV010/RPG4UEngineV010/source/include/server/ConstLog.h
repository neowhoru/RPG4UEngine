/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstLog.h
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTLOG_H__
#define __CONSTLOG_H__
#pragma once

#include "StructCharacter.h"

enum
{
	MAX_CHAR_STAT_SIZE	= sizeof(sPLAYERINFO_BASE)
};


////////////////////////////////////////////////
enum
{
	 LOGEXP_LEVELUP	
	,LOGEXP_DOWN			
	,LOGEXP_DIVIDECNT			
	,LOGEXP_QUEST				
};


////////////////////////////////////////////////
enum
{
	 LOGITEM_PICKUP						
	,LOGITEM_PICKUPPRIORITY	
	,LOGITEM_SELLNPC						
	,LOGITEM_CAST							
	,LOGITEM_DROP							
	,LOGITEM_MONSTER_ITEMDROP	
	,LOGITEM_BUYNPC						
	,LOGITEM_REQUEST						
	,LOGITEM_ACCEPT						
	,LOGITEM_DELPC							
	,LOGITEM_REJECT						
	,LOGITEM_GETSTORAGE					
	,LOGITEM_PUTSTORAGE					
	,LOGITEM_GIVE							
	,LOGITEM_RECEIVE						
	,LOGITEM_SELLPC						
	,LOGITEM_SETPRICE_PC					
	,LOGITEM_NEWPC							
	,LOGITEM_WATCHPC						
	,LOGITEM_MONEY							
	,LOGITEM_BUYPC							
	,LOGITEM_WATCHSTORAGE				

	,LOGITEM_ENCHANTTRY					
	,LOGITEM_ENCHANTSUCCESS				
	,LOGITEM_ENCHANTFAIL					
	,LOGITEM_ENCHANTMONEY				

	,LOGITEM_RANKUPSUCCESS				
	,LOGITEM_RANKUPCANCEL				
	
	,LOGITEM_NEWACC_SUCCESS				
	,LOGITEM_NEWACC_TRY					
	,LOGITEM_NEWACC_INFO					
	,LOGITEM_NEWACC_FAIL					
	,LOGITEM_NEWACC_CANCEL				
	,LOGITEM_NEWACC_MONEY				

	,LOGITEM_SOCKETOPT_ADDTRY			
	,LOGITEM_SOCKETOPT_DELMONEY		
	,LOGITEM_SOCKETOPT_DELFAIL			
	,LOGITEM_SOCKETOPT_DELTRY			
	,LOGITEM_SOCKETOPT_ADDFAIL			
	,LOGITEM_SOCKETOPT_ADDSUCCESS		
	,LOGITEM_SOCKETOPT_ADDINFO			
	,LOGITEM_SOCKETOPT_DELSUCCESS		
	,LOGITEM_SOCKETOPT_DELINFO			

	,LOGITEM_RANKUPTRY		
	,LOGITEM_REPAIR				
	,LOGITEM_QUEST				


};



////////////////////////////////////////////////
enum
{
	 LOGACTION_CREATE_CHAR	
	,LOGACTION_DELETE_CHAR			
	,LOGACTION_REBIRTH_CHAR			
	,LOGACTION_DEAD_CHAR				
	,LOGACTION_ACQUIRE_SKILL		
	,LOGACTION_SKILLLV_UP			
	,LOGACTION_PKKILL				
	,LOGACTION_PKDIE			
	,LOGACTION_TRY_PVP				
	,LOGACTION_USE_STAT				
	,LOGACTION_INVENLOGIN		
	,LOGACTION_INVENLOGOUT		
	,LOGACTION_MOVE_TOWN				
	,LOGACTION_GETTAX				
};


////////////////////////////////////////////////
enum
{
	 LOGCONN_AUTH				
	,LOGCONN_LOGIN_FAIL				
	,LOGCONN_AUTH_FAIL				
	,LOGCONN_LOGIN						
	,LOGCONN_CHAR_CONNECT			
	,LOGCONN_LOGOUT					
	,LOGCONN_FORCE_LOGOUT			
	,LOGCONN_ABNORMAL_LOGOUT		
	,LOGCONN_CANNOT_LOGIN_ACCOUNT	
	,LOGCONN_CHAR_DISCONNECT		
	,LOGCONN_WORLD_LOGOUT			
	,LOGCONN_HACK						
	,LOGCONN_ABNORMAL_LOGIN			
	,LOGCONN_ABNORMAL_CONNECT		
	,LOGCONN_EXPIRE					
	,LOGCONN_ADDEDTYPE					
};

////////////////////////////////////////////////
enum
{
	 LOGCHAT_EYERANGE		
	,LOGCHAT_SHOUT			
	,LOGCHAT_GUILD			
	,LOGCHAT_PRIVATE		
	,LOGCHAT_FRIEND			
	,LOGCHAT_BATTLEZONE	
};





#endif //__CONSTLOG_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：MonsterItemDrop.h
创建日期：2007年8月27日
最后更新：2007年8月27日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __DROPITEMMONSTER_H__
#define __DROPITEMMONSTER_H__
#pragma once



#include "BaseItemDrop.h"
#include "GroupItemDrop.h"
#include "DropItemInfo.h"
#include "RatioRandomizer.h"
#include <BaseResParser.h>
#include <THashTable.h>


#define MAX_DROPGROUP_COUNT	5

class DropFormulaCore;
////////////////////////////////////////////////////////////////////////
struct sGROUPDROP_ARRAY
{
	int		nProbability;		// 掉落机率
	DWORD		arGroupInfos[MAX_DROPGROUP_COUNT];
	sGROUPDROP_ARRAY()
		:nProbability(0)
	{
		__ZERO(arGroupInfos);
	}
};
typedef  vector<sGROUPDROP_ARRAY> GroupDropIndexGroup;


////////////////////////////////////////////////////////////////////////
struct _SERVERCORE_API sMONSTERDROP_INFO : public sGROUPDROP_INFO
{
	CODETYPE					dwMonsterCode;
	GroupDropIndexGroup	arGroupIndices;
	RatioRandomizer		arGroupRatio;

	sMONSTERDROP_INFO()
		:dwMonsterCode(0)
	{
	}
	void ResizeGroupDropArray(UINT nSize)
	{
		if(nSize == 0)
			arGroupIndices.clear();
		else if(nSize != arGroupIndices.size())
			arGroupIndices.resize(nSize);
	}
};

typedef  const sMONSTERDROP_INFO sMONSTERDROP_INFOC;

typedef map<DWORD,sMONSTERDROP_INFO>	MonsterDropInfoMap;
typedef pair<DWORD,sMONSTERDROP_INFO>	MonsterDropInfoMapPair;
typedef MonsterDropInfoMap::iterator	MonsterDropInfoMapIt;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API  MonsterItemDrop : public BaseItemDrop
													, public BaseResParser
{
public:
	MonsterItemDrop();
	virtual ~MonsterItemDrop(void);

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_ALLOC3	(sMONSTERDROP_INFO, m_DropInfos, dwMonsterCode,DropMonster,MonsterDropInfoMap);

public:
	virtual BOOL Init		();
	virtual void Release	();
	virtual BOOL Load		(LPCTSTR szFile);


public:
	virtual BOOL						DropMonsterItem(CODETYPE		 dwMonsterCode);
	virtual sMONSTERDROP_INFO*		GetDropInfo		( DWORD dwMonsterID );
	virtual BOOL						_ProcessDropItem(sGROUPDROP_INFO& groupDrop);

public:
	void _Push(sMONSTERDROP_INFO* pDrop,sITEMDROP_INFO& info){pDrop->arDropInfos.push_back( info );}
	void _Push(sMONSTERDROP_INFO* pDrop,sGROUPDROP_ARRAY& info){pDrop->arGroupIndices.push_back( info );}

private:
	MonsterDropInfoMap	m_DropInfos;
	//GroupItemDrop		m_GroupInfos;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(MonsterItemDrop , _SERVERCORE_API);
//extern API_NULL MonsterItemDrop& singleton::GetMonsterItemDrop();
#define theMonsterItemDrop  singleton::GetMonsterItemDrop()


#endif //__DROPITEMMONSTER_H__
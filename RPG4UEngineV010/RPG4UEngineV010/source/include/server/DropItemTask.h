/*////////////////////////////////////////////////////////////////////////
文 件 名：DropItemTask.h
创建日期：2007年8月26日
最后更新：2007年8月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __DROPITEMTASK_H__
#define __DROPITEMTASK_H__
#pragma once

#include "BaseItemDrop.h"

class  GamePlayer;

class _SERVERCORE_API  DropItemTask : public BaseItemDrop
{
public:
	struct _SERVERCORE_API STask
	{
		int		nTaskId;
		int		nTaskState;
		sITEMDROP_INFO stItem;
		int		nMaxItemCount;
		int		nVarId;
		int		nMaxVar;
		std::string strVarName;
	};
	struct _SERVERCORE_API STaskDrop
	{
		int nMonster;
		std::vector<STask>	vtTask;
	};

public:
	bool Init();
	bool LoadData();
	bool MonsterDeath( GameMonster* pMonster, GameCharacter* pKiller );
	bool IsTaskItemFull( GamePlayer* pPlayer, int dwItemCode );

private:
	struct _SERVERCORE_API SDropCount
	{
		int dwItemCode;
		int nMaxItemCount;
	};

private:
	bool AddDropCount( const SDropCount& stItem );
	bool UpdateTeamTaskVar( const STask* pTask,
						const GameMonster* pMonster,
						GamePlayer *pPlayer );

private:
	std::vector<STaskDrop> m_DropInfos;
	std::vector<SDropCount>	m_vtDropCount;

public:
	DropItemTask(void);
	~DropItemTask(void);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#ifndef NONE_GLOBALINST
GLOBALINST_DECL(DropItemTask , _SERVERCORE_API);
#else
extern DropItemTask& GetDropItemTask();
#endif


#endif //__DROPITEMTASK_H__
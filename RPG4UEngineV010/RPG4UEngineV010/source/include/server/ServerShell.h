/*////////////////////////////////////////////////////////////////////////
文 件 名：ServerShell.h
创建日期：2007年9月1日
最后更新：2007年9月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SERVERSHELL_H__
#define __SERVERSHELL_H__
#pragma once

#include <ServerShellDefine.h>
#include "GlobalInstanceSingletonDefine.h"
#include <Timer.h>
#include <ServerInfoDefine.h>
#include <ServerStruct.h>
#include <NetworkGroupDefine.h>
//#include <IServerSession.h>

class ISessionHandle;
class ServerGroupLog;
class NetworkGroup;
class IServerSession;
class BaseLog;

using namespace util;




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API ServerShell
{
public:
	ServerShell();
	virtual ~ServerShell();

public:
	virtual eSERVER_TYPE	GetServerType()=0;

public:
	virtual BOOL		Init			();
	virtual VOID		Release		();
	virtual VOID		Run			(DWORD dwDelay=0);
	virtual BOOL		FrameMove	(DWORD dwTick);
	virtual void		OnRunEnd		();

	virtual BOOL		StartListen			(DWORD dwListenType=LISTEN_ALL);
	virtual BOOL		StartServerListen	(LPCTSTR szIP, PORTTYPE port );
	virtual BOOL		StartClientListen	(LPCTSTR szIP, PORTTYPE port );
	virtual BOOL		StartListen			(INT nNetType, LPCTSTR szIP, PORTTYPE port );
	virtual BOOL		StartListen			(sSERVER_GROUPC& group, DWORD dwListenType=LISTEN_ALL);

	virtual VOID		Shutdown	();
	virtual VOID		Testing	(LPCTSTR szParam);
public:
	virtual BOOL		ProcessConsole		(LPCTSTR szCmd);
	virtual VOID		UpdateServerTitle	();

	virtual VOID		UpdateFPS				();
	virtual VOID		DisplayServerInfo		();
	virtual VOID		MaintainConnection	();
	virtual BOOL		IsConnectedTo			( eSERVER_TYPE eServerType );

	virtual BOOL		ConnectToServer		(ISessionHandle* pSessionHandle
														,LPCTSTR        pszIP
														,WORD           wPort);
	virtual BOOL		ConnectToServer		(IServerSession* pServer);
	virtual BOOL		ConnectToClient		(ISessionHandle* pSessionHandle
														,LPCTSTR        pszIP
														,WORD           wPort);

	static void			Ini2NetworkInfo	(sIOHANDLER_INFOC& ini,sNETWORK_INFO& ioDesc);

public:
	_SERVER_DECL	(Agent				,GATE_SHELL			);
	_SERVER_DECL	(Field				,FIELD_SHELL		);
	_SERVER_DECL	(Battle				,BATTLE_SHELL		);
	_SERVER_DECL	(GameDBProxy		,PLAYER_DBPROXY	);
						  
	_SERVER_DECL	(World				,CHAT_SHELL			);
	_SERVER_DECL	(Master				,MASTER_SHELL		);
	_SERVER_DECL	(Guild     			,GUILD_SHELL     	);
	_SERVER_DECL	(AccountDBProxy	,USER_DBPROXY		);
	_SERVER_DECL	(AIShell				,AI_SHELL			);
						  
	_SERVER_DECL	(Login				,LOGIN_SHELL		);
	_SERVER_DECL	(Admin				,ADMIN_SHELL		);
	//_SERVER_DECL	(Temp					,TEMP_SHELL			);
	_SERVER_DECL	(Game					,GAME_SHELL			);


#ifdef USE_DISABLE
	BOOL		SendMaster					( MSG_BASE * pMsg, WORD wSize );
	BOOL		IsMasterServerConnected	();
	VOID		SetMasterServerAddr		(LPCTSTR szIP, PORTTYPE port );
	VOID		ConnectToMasterServer	();
#endif

public:
	IServerSession*	GetServerSession	(DWORD dwType);

	BOOL		SendToServer			(MSG_BASE * pMsg, WORD wSize ,DWORD dwServerType );
	BOOL		IsServerConnected		(DWORD dwServerType);
	BOOL		SetServerAddr			(LPCTSTR szIP, PORTTYPE port,DWORD dwServerType );
	BOOL		ConnectToServerAt		(DWORD dwServerType);
	BOOL		UpdateLocalServers	(IServerSession* pSession);

public:
	inline BYTE		GetWorldID()		{ return m_ServerKey.GetWorldID(); }
	inline BYTE		GetChannelID()		{ return m_ServerKey.GetChannelID(); }
	inline BYTE		GetServerSubID()	{ return m_ServerKey.GetServerSubID(); }
	//inline BYTE		GetServerID()		{ return theServerSetting.m_dwServerID; }

protected:
	BOOL					_InitGameLog			(BaseLog&  log, LPCTSTR szLogName);

	virtual BOOL		_InitLog			(sSERVERLOG_INFO*	pSvrLogInfo);
	virtual BOOL		_InitNetwork	();
	virtual BOOL		_InitReconnect	(DWORD dwDelay);
	virtual BOOL		_InitNetworkCS	(sNETWORK_INFO	arDescs[],DWORD dwNetworkType=NETWORK_ALL);

	virtual BOOL		_PrevInit		();
	virtual BOOL		_PrevRelease	();

private:
	BOOL					_InitServerSessions	();

protected:
	VG_DWORD_PROPERTY		(ServerGUID);

#ifdef USE_MULTI_SESSION
	sMULTI_SESSION			m_arServers[SHELL_MAX];
#else
	IServerSession	*		m_arServers[SHELL_MAX];
#endif
	DWORD						m_dwSeverEnableFlags;		// SERVER_TYPE对应位 1:Enable
	INT						m_nServerNet;
	INT						m_nClientNet;

	VG_TYPE_PROPERTY		(RunState,	eRUN_STATE);
	VG_DWORD_PROPERTY		(SleepDelay);	//睡眠间隔，保证程序性能消耗浪费CPU
	VG_BOOL_PROPERTY		(Shutdown);
	VG_TYPE_PROPERTY		(ServerKey, SERVER_KEY);
	DWORD						m_dwFPS;
	util::Timer				m_ReconnectTimer;

	VG_PTR_GET_PROPERTY	(WorldLog,	ServerGroupLog);
	VG_PTR_GET_PROPERTY2	(NetworkGroup);
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ServerShell , _SERVERCORE_API);
//extern API_NULL ServerShell& singleton::GetServerShell();
#define theServerShell  singleton::GetServerShell()


#include "ServerShell.inl"


#endif //__SERVERSHELL_H__
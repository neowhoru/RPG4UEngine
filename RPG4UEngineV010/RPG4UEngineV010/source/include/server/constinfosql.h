/*////////////////////////////////////////////////////////////////////////
文 件 名：ConstInfoSql.h
创建日期：2009年4月10日
最后更新：2009年4月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __CONSTINFOSQL_H__
#define __CONSTINFOSQL_H__
#pragma once


enum
{
	 SQL_UPDATEWAREHOUSE								=	50000	//	{?=call S_UpdateWarehouse (%u, ?, %u) }
	,SQL_SELECTWAREHOUSE								=	50001	//	S_SelectWarehouse %u
										//	0	,//	预留
	,SQL_LOGINUSER										=	50020	//	{?=call S_LoginUser('%s')}
	,SQL_SELECTUSER									=	50021	//	S_SelectUser %u
										//	0	,//	预留
	,SQL_SELECTCHAR									=	50040	//	S_SelectChar %u
	,SQL_UPDATECHAR									=	50041	//	{?=call S_UpdateChar (%u,%u,%d,%d,%d,%d,%d,%d,%d,%d,%d,%d,%u,%u,%u,%u,%u,%I64u,%u,%u,%u,%u,%u,'%s',%u,%d,%d,%d,'%s',%d,%u,?,?,?,?,?,?,?,?,%d,%u,%u,%u,%u,%u,%u,%u,%u,%u,)}
	,SQL_CREATECHAR									=	50042	//	{?=call S_CreateChar(%u, '%s', '%s', %u, %u, %u, %u, %u, %u)}
	,SQL_DESTROYCHAR									=	50043	//	{?=call S_DestroyChar(%u, %u)}
	,SQL_UPDATEITEMS									=  50044 //	{?=call Char_UpdateItems(%u,%u,%d,?,?,?,?)}

	,SQL_CREATEITEMSEQ								=	50080	//	S_CreateItemSeq %u
	,SQL_UPDATEITEMSEQ								=	50081	//	S_UpdateItemSeq %u
										//	0	,//	预留
	,SQL_EVENT_SELECT									=	50100	//	{?=call S_Event_Select(%d)}
	,SQL_EVENT_UPDATE									=	50101	//	{?=call S_Event_Update(%d,%d)}
										//	0	,//	预留
	,SQL_GUILD_SELECT									=	50120	//	{?=call S_Guild_Select(%d)}
	,SQL_GUILD_SELECTMEMBER							=	50121	//	{?=call S_Guild_SelectMember(%d)}
	,SQL_GUILD_CREATE									=	50122	//	{?=call S_Guild_Create('%s',%d,'%s')}
	,SQL_GUILD_DESTROYDEL							=	50123	//	{?=call S_Guild_DestroyDel(%d, %d)}
	,SQL_GUILD_JOINMEMBER							=	50124	//	{?=call S_Guild_JoinMember(%d,%d,%d,'%s')}
	,SQL_GUILD_WITHDRAWMEMBER						=	50125	//	{?=call S_Guild_WithdrawMember(%d, %d)}
	,SQL_GUILD_CHANGEMEMBERPOSITION				=	50126	//	{?=call S_Guild_ChangeMemberPosition(%d, %d, %d)}
										//	0	,//	预留
	,SQL_FRIEND_SELECTLIST							=	50140	//	{?=call S_Friend_SelectList(%d)}
	,SQL_FRIEND_REQUEST								=	50141	//	{?=call S_Friend_Request(%d,'%s')}
	,SQL_FRIEND_DELETE								=	50142	//	{?=call S_Friend_Delete(%d,%d)}
	,SQL_FRIEND_BLOCKCHAR							=	50143	//	{?=call S_Friend_BlockChar(%d,'%s')}
	,SQL_FRIEND_FREECHAR								=	50144	//	{?=call S_Friend_FreeChar(%d,'%s')}	
															//	0	,//	
	,SQL_SERVERINFOLIST								=	50160	//	S_ServerInfoList %u, %u


};


#endif //__CONSTINFOSQL_H__
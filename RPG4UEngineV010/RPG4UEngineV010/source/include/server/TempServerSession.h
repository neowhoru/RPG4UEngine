/*////////////////////////////////////////////////////////////////////////
文 件 名：TempServerSession.h
创建日期：2007年1月1日
最后更新：2007年1月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TEMPSERVERSESSION_H__
#define __TEMPSERVERSESSION_H__
#pragma once

#include "IServerSession.h"
#include "ServerSessionFactoryManager.h"


class _SERVERCORE_API TempServerSession : public IServerSession
{
public:
	TempServerSession();
	virtual ~TempServerSession();

public:
	virtual BOOL			Init		();
	virtual VOID			Release	();
	virtual VOID			Update	();

	eSERVER_TYPE			GetServerType() { return TEMP_SHELL; }

protected:
	virtual VOID			OnAccept	( DWORD dwNetworkIndex );
	virtual	VOID			OnRecv	( BYTE *pMsg, WORD wSize );

private:
	BOOL					m_bFirstPacketReceived;
};

SERVERSESSION_FACTORY_DECLARE(TempServerSession);


#endif //__TEMPSERVERSESSION_H__
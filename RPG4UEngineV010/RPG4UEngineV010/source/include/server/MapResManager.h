
#pragma once

#include <THashTable.h>
using namespace util;


class SectorInfo;
class FieldInfo;
class MapInfo;

class _SERVERCORE_API MapResManager
{
public:
	MapResManager();
	virtual ~MapResManager();

public:
	virtual MapInfo *		GetMapInfo		(CODETYPE MapID );
	virtual VOID			BuildAllMaps	(WORD wVillageSectorSize
                                       ,WORD wRoomSectorSize)		;
	virtual VOID			ReleaseAllMaps	();

public:
	SectorInfo*	AllocSector		();
	void			FreeSector		(SectorInfo*);
	FieldInfo*	AllocField		();
	void			FreeField		(FieldInfo*	);
	MapInfo*		AllocMap			();
	void			FreeMap			(MapInfo*	);


protected:
	VOID						_AddFieldInfo	(CODETYPE    FieldCode
                                       ,FieldInfo * pInfo)		;
	FieldInfo *				_GetFieldInfo	(CODETYPE FieldCode );

private:
	THashTable<MapInfo *> *		m_pMapInfoTable;
	THashTable<FieldInfo *> *	m_pFieldInfoTable;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(MapResManager , _SERVERCORE_API);
//extern _SERVERCORE_API MapResManager& singleton::GetMapResManager();
#define theMapResManager  singleton::GetMapResManager()



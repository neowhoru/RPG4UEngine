/*////////////////////////////////////////////////////////////////////////
文 件 名：ServerSessionManager.h
创建日期：2007年12月1日
最后更新：2007年12月1日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __SERVERSESSIONMANAGER_H__
#define __SERVERSESSIONMANAGER_H__
#pragma once



#pragma once

class IServerSession;

#include <GlobalInstanceSingletonDefine.h>
#include <IServerSession.h>
#include <CriticalSection.h>

using namespace util;
using namespace std;

typedef map<DWORD, IServerSession*>					ServerSessionMap;
typedef map<DWORD, IServerSession*>::iterator	ServerSessionMapIt;
typedef pair<DWORD, IServerSession*>				ServerSessionValue;

typedef map<IServerSession*, IServerSession*>		LocalSessionMap;
typedef LocalSessionMap::iterator						LocalSessionMapIt;
typedef pair<IServerSession*, IServerSession*>		LocalSessionValue;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API ServerSessionManager
{
public:
	ServerSessionManager();
	~ServerSessionManager();

public:
	//实例创建管理
	virtual		IServerSession*   AllocServerSession(SERVERSESSION_TYPE type /* =SERVERSESSION_NULL*/)	;
	virtual		void					FreeServerSession	(IServerSession* pServerSession)	;

public:
	virtual		BOOL					Init				();
	virtual		void					Release			();
	virtual		VOID					Update			();
	virtual		void					ReleaseLocals	();

	virtual		BOOL					AddServer		( IServerSession * pServer, BOOL bLocal=FALSE);
	virtual		BOOL					RemoveServer	( DWORD dwSessionIndex);
	virtual		IServerSession *	FindServer		( DWORD dwSessionIndex);
	virtual		IServerSession *	FindLocalServer	( IServerSession * pSession);
	virtual		BOOL					RemoveLocalServer	( IServerSession * pSession);

public:
	virtual		BOOL		OnAddServer				( IServerSession * ){return TRUE;} //返回TRUE时，方可AddServer成功
	virtual		BOOL		OnRemoveServer			( IServerSession * ){return TRUE;} //返回TRUE时，方可RemoveServer成功
	virtual		BOOL		OnAddLocalServer		( IServerSession * ){return TRUE;} //返回TRUE时，方可AddServer成功
	virtual		BOOL		OnRemoveLocalServer	( IServerSession * ){return TRUE;} //返回TRUE时，方可RemoveServer成功

public:
	template <class ObjectOperator>
	VOID							Foreach( ObjectOperator  & op );
	template <class ObjectOperator>
	VOID							Foreach( ObjectOperator  & op, eSERVER_TYPE type);
	template <class ObjectOperator>
	VOID							ForeachMask( ObjectOperator  & op );
	template <class ObjectOperator>
	VOID							ForeachLocal( ObjectOperator  & op );

private:
	ServerSessionMap			m_mapSessions;
	LocalSessionMap			m_mapLocalSessions;
	util::CriticalSection	m_Locker;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ServerSessionManager , _SERVERCORE_API);
//extern _SERVERCORE_API ServerSessionManager& singleton::GetServerSessionManager();
#define theServerSessionManager  singleton::GetServerSessionManager()


#include "ServerSessionManager.inl"


#define SAFE_FREESESSION(x)		if(x){theServerSessionManager.FreeServerSession(x);(x)=NULL;}

#endif //__SERVERSESSIONMANAGER_H__
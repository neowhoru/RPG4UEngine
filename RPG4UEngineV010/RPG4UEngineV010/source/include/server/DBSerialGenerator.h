#ifndef __DB_SERIAL_GENERATOR_H__
#define __DB_SERIAL_GENERATOR_H__

#pragma once

#include <CommonDefine.h>
#include <CriticalSection.h>

using namespace util;

//=============================================================================================================================
class _DATABASECORE_API DBSerialGenerator
{
public:
	DBSerialGenerator();
	~DBSerialGenerator();

	
public:
	DBSERIAL				AllocSerial		();
	VOID					SetSerial		( DBSERIAL	CurSerial
												, DBSERIAL SerialSize );

	BOOL					NeedQuerySerial	();

public:
	VG_TYPE_GET_PROPERTY		(CurrentSerial, DBSERIAL);

private:
	DBSERIAL						m_Init	;
	DBSERIAL						m_TopMax	;
	util::CriticalSection	m_Locker	;

};

#endif // __DB_SERIAL_GENERATOR_H__
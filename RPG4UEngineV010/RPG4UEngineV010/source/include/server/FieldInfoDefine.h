/*////////////////////////////////////////////////////////////////////////
文 件 名：FieldInfoDefine.h
创建日期：2008年5月2日
最后更新：2008年5月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __FIELDINFODEFINE_H__
#define __FIELDINFODEFINE_H__
#pragma once

#include "CommonDefine.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD				   FIELDINFO_KEY;	//ID类型的Factory
typedef DWORD			      FIELDINFO_TYPE;
#define FIELDINFO_NULL    ((FIELDINFO_TYPE)-1)

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD				   MAPINFO_KEY;	//ID类型的Factory
typedef DWORD			      MAPINFO_TYPE;
#define MAPINFO_NULL    ((MAPINFO_TYPE)-1)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
typedef DWORD				   SECTORINFO_KEY;	//ID类型的Factory
typedef DWORD			      SECTORINFO_TYPE;
#define SECTORINFO_NULL    ((SECTORINFO_TYPE)-1)

enum
{
	 SECTORINFO_DEFAULT	= TAG('SECT')
	,FIELDINFO_DEFAULT	= TAG('FILD')
	,MAPINFO_DEFAULT		= TAG('MAPD')
};

enum 
{
	 MAX_NEIGHBOR_SECTOR_NUM	= 8
	,MAX_FIELD_NUM_IN_MAP		= 10
	,MAX_MAPOBJECT_INFO_NUM		= 100
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
const	IDTYPE	ID_START_PREFIX	= Prefix3CharToTAG('S','T','R');

class sMAPOBJECT_INFO
{
public:
	DWORD				m_dwObjectKey;
	IDTYPE			m_ObjectID;
	DWORD				m_dwAttribute;
	Vector3D			m_vPos;
	Vector3D			m_vRot;
	Vector3D			m_vScale;
};

typedef util::THashTable<sMAPOBJECT_INFO *>		MapObjectInfoTable;


#endif //__FIELDINFODEFINE_H__
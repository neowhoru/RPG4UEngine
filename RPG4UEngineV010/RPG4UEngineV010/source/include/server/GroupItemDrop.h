/*////////////////////////////////////////////////////////////////////////
文 件 名：GroupItemDrop.h
创建日期：2007年8月21日
最后更新：2007年8月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __DROPITEMCOMMON_H__
#define __DROPITEMCOMMON_H__
#pragma once



#include "BaseItemDrop.h"
#include "RatioRandomizer.h"
#include <BaseResParser.h>
#include <THashTable.h>

using namespace std;
using namespace util;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
struct _SERVERCORE_API sGROUPDROP_INFO
{
	DWORD							dwGroupID;
	VRSTR							sName;
	INT							nMaxProbability;
	INT							nMinDropCount;
	INT							nMaxDropCount;
	BOOL							bRepeat;
	ItemDropInfoArray			arDropInfos;
	RatioRandomizer			arItemRatio;

	sGROUPDROP_INFO()
		:dwGroupID(0)
		,sName(NULL)
		,nMaxProbability(0)
		,nMinDropCount(0)
		,nMaxDropCount(0)
		,bRepeat(0)
	{
	}

	void ResizeItemDropArray(UINT nSize)
	{
		if(nSize == 0)
			arDropInfos.clear();
		else if(nSize != arDropInfos.size())
			arDropInfos.resize(nSize);
	}
};
typedef const sGROUPDROP_INFO	sGROUPDROP_INFOC;

typedef map<DWORD,sGROUPDROP_INFO>	GroupDropInfoMap;
typedef pair<DWORD,sGROUPDROP_INFO>	GroupDropInfoMapPair;
typedef GroupDropInfoMap::iterator	GroupDropInfoMapIt;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API  GroupItemDrop : public BaseItemDrop ,public BaseResParser
{
public:
	GroupItemDrop(void);
	virtual ~GroupItemDrop(void);

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_ALLOC3	(sGROUPDROP_INFO, m_DropInfos, dwGroupID,DropGroup,GroupDropInfoMap);

public:
	virtual BOOL Init	();
	virtual void Release();

	virtual BOOL Load		(LPCTSTR szFile);
	virtual void Reload	();

	virtual sGROUPDROP_INFO*	GetDropInfo( DWORD dwGroupID );

	void _Push(sGROUPDROP_INFO* pDrop,sITEMDROP_INFO& info){pDrop->arDropInfos.push_back( info );}

private:
	GroupDropInfoMap m_DropInfos;

};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(GroupItemDrop , _SERVERCORE_API);
//extern API_NULL GroupItemDrop& singleton::GetGroupItemDrop();
#define theGroupItemDrop  singleton::GetGroupItemDrop()


#endif //__DROPITEMCOMMON_H__
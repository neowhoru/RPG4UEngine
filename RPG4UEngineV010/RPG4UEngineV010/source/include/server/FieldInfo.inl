/*////////////////////////////////////////////////////////////////////////
文 件 名：FieldInfo.inl
创建日期：2008年4月30日
最后更新：2008年4月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __FIELDINFO_INL__
#define __FIELDINFO_INL__
#pragma once



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline SpecialArea *		FieldInfo::GetAreaInfo( IDTYPE AreaID,int nMapX,int nMapY )
	{ return m_pTileLand->GetSpecialArea( AreaID,nMapX, nMapY ); }

inline SpecialArea *		FieldInfo::GetAreaInfoByIndex( INT index ,int nMapX,int nMapY )
	{ return m_pTileLand->GetSpecialAreaByIndex(index,nMapX, nMapY); }

inline VOID FieldInfo::ForeachArea( SpecialAreaIterator & op )
{
	m_pTileLand->ForEachSpecialArea(op);
}

inline CODETYPE		FieldInfo::GetFieldCode() { return m_pFieldInfo->FieldCode;	}
inline SectorInfo*	FieldInfo::GetSectorInfo( DWORD dwIndex )	{ return m_ppSectorInfo[dwIndex];	}


template<class OPR>
void	FieldInfo::ForEachMapObject(OPR& opr)
{
	sMAPOBJECT_INFO * pMapInfo = NULL;
	m_pMapObjectInfoTable->SetFirst();
	for(;;)
	{
		pMapInfo = m_pMapObjectInfoTable->GetNext();
		if(!pMapInfo)
			break;
		opr(pMapInfo);
	}
}



#endif //__FIELDINFO_INL__
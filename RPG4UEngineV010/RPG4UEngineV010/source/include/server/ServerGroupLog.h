/*////////////////////////////////////////////////////////////////////////
文 件 名：ServerGroupLog.h
创建日期：2009年3月28日
最后更新：2009年3月28日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __SERVERGROUPLOG_H__
#define __SERVERGROUPLOG_H__
#pragma once



#define  OUTPUT_NONE			0
#define	OUTPUT_FILE			1
#define	OUTPUT_MONITOR		2
#define  OUTPUT_BOTH			3
#define  MAX_TIME_SIZE		100


using namespace std;
typedef pair<BYTE, StringHandle>			LOG_INFO_PAIR;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API ServerGroupLog : public LogListener
{
public:
	ServerGroupLog(void);
	virtual ~ServerGroupLog(void);


public:
	virtual bool Output			( LPCTSTR szError, ... );
	virtual bool OutputWithTime( LPCTSTR szError,... );

public:
	virtual BOOL	Init	(BYTE   byLogOption
								,BYTE   byLogLevel
								,LPCTSTR szFilePath
								,LPCTSTR szFileName);

	virtual BOOL	OutputMsg			(LPCTSTR pszMsg
                                    ,BYTE    byLogLevel=OUTPUT_BOTH)		;

public:
	virtual BOOL	CreateDispayThread();
	virtual BOOL	CreateFileThread	();
	virtual BOOL	DumpLogsToFile		();									
	virtual BOOL	DumpLogsToDisplay	();	

public:
	virtual BOOL	_CheckLogTime	();		
	virtual BOOL	_CreateLogFile	();							

private:
	virtual BOOL	_GetLogInfo		(LPCTSTR pszMsg
                                 ,BYTE    byLogLevel
											,int     nType
											,LOG_INFO_PAIR& msg);

private:
	StringHandle			m_sFilePath;	
	StringHandle			m_sFileName;	

	CRITICAL_SECTION		m_FileLocker;
	HANDLE					m_hFileThread;

	CRITICAL_SECTION		m_DisplayLocker;
	HANDLE					m_hDisplayThread;

	HANDLE					m_hFile;				
	StringHandle			m_sTime;		
	StringHandle			m_sNowTime;

	VG_BOOL_PROPERTY		(DisplayRunning);
	VG_BOOL_PROPERTY		(FileRunning);

	VG_BYTE_PROPERTY		(LogOption);
	VG_BYTE_PROPERTY		(LogLevel);

private:
	list<LOG_INFO_PAIR>		m_DisplayLogs;	
	list<LOG_INFO_PAIR>		m_FileLogs;	

};


//////////////////////////////////////////////////
_SERVERCORE_API BOOL MessageOut(BYTE byLogLevel, LPCTSTR szMsg,...);


//////////////////////////////////////////////////
#define	LOGMSG		MessageOut

#define LOG_OK				LOGMSG( LOG_FULL,  " [OK]" );
#define LOG_FAILED		LOGMSG( LOG_FULL,  " Failed!" );

#endif //__SERVERGROUPLOG_H__
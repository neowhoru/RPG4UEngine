#pragma once

#include <THashTable.h>
#include <CommonDefine.h>
#include <ServerStruct.h>
#include <ServerOptionParserDefine.h>


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API ServerOptionParser 
{
public:
	ServerOptionParser();
	virtual ~ServerOptionParser();

public:
	virtual BOOL	Load		(LPCTSTR             szFileName
									,eSERVER_OPTION_TYPE byServerType
									,BOOL						bReload =      FALSE);
	virtual VOID	Reload	();
	virtual VOID	Release	();

private:
	StringHandle				m_szFileName;
	eSERVER_OPTION_TYPE		m_ServerType;
	VG_TYPE_GET_PROPERTY		(ServerOption, sSERVER_OPTION);
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(ServerOptionParser , _SERVERCORE_API);
//extern API_NULL ServerOptionParser& singleton::GetServerOptionParser();
#define theServerOptionParser  singleton::GetServerOptionParser()


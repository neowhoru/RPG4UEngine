/*////////////////////////////////////////////////////////////////////////
文 件 名：IServiceShell.h
创建日期：2009年3月30日
最后更新：2009年3月30日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


	class TestServiceApp : public IServiceShell
	{
	public :
		TestServiceApp(const char* pServiceName)
		: IServiceShell(pServiceName)
		{
		}

		virtual ~TestServiceApp()
		{
		}

		virtual VOID	Update()
		{
			while (1)
			{
				printf(".");
				Sleep(500);
			}
		}
	};


	__DECLARE_SERVICE( TestServiceApp, "TestService" );

	void main( ) 
	{ 	
		__START_SERVICE( TestServiceApp );
	} 



	>sc create GateShell binPath= d:\server\_bin\AgentServer_d.exe DisplayName= "Agent"
	>sc delete GateShell


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ISERVICESHELL_H__
#define __ISERVICESHELL_H__
#pragma once



class _SERVERCORE_API IServiceShell
{
public :
	IServiceShell(LPCTSTR pServiceName);
	virtual ~IServiceShell();


public :
	virtual VOID		StartService	(LPSERVICE_MAIN_FUNCTION func
                                    ,LPHANDLER_FUNCTION      pCtrlHandler)		;

	virtual VOID		HandleStart		(DWORD argc, LPTSTR *argv);
	virtual VOID		HandleControl	(DWORD opcode);

	virtual VOID		Update() = 0;

protected :
	virtual DWORD		OnInit		(DWORD   /*argc*/
											,LPTSTR* /*argv*/
											,DWORD*  /*specificError*/)	{ return 0; }
	virtual VOID		OnPause			()	{}
	virtual VOID		OnContinue		()	{}
	virtual VOID		OnStop			()	{}
	virtual VOID		OnInterrogate	()	{}

protected :
	virtual VOID		PrinfInfo	(LPSTR String, DWORD Status);
	virtual VOID		SetApplicationDir();	

protected :
	StringHandle				m_ServiceName;
	SERVICE_STATUS          m_ServiceStatus; 
	SERVICE_STATUS_HANDLE   m_ServiceStatusHandle; 
	LPHANDLER_FUNCTION		m_pServiceCtrlHandler;
};


///////////////////////////////////////////////////
#define __DECLARE_SERVICE_FUN( CLASS )	\
		static VOID	NTAPI Start##CLASS##Function (DWORD argc, LPTSTR *argv);	\
		static VOID	NTAPI Control##CLASS##Function (DWORD opcode);

#define __DECLARE_SERVICE0( CLASS,INST )	\
		static VOID	NTAPI Start##CLASS##Function (DWORD argc, LPTSTR *argv)\
						{ INST.HandleStart(argc, argv); }	\
		static VOID	NTAPI Control##CLASS##Function (DWORD opcode)\
						{ INST.HandleControl(opcode); }

#define __DECLARE_SERVICE( CLASS, NAME )	\
		static CLASS INST( NAME);\
		__DECLARE_SERVICE0(CLASS,INST)


#define __START_SERVICE( CLASS )\
		gs_##ServiceInst.StartService	( (LPSERVICE_MAIN_FUNCTION)Start##CLASS##Function\
												, (LPHANDLER_FUNCTION)Control##CLASS##Function );

				


#endif //__ISERVICESHELL_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：tileworldserver.h
创建日期：2006年4月3日
最后更新：2006年4月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __TILEWORLDSERVER_H__
#define __TILEWORLDSERVER_H__
#pragma once

#include "TileWorld.h"

namespace tile
{
	/*////////////////////////////////////////////////////////////////////////
	/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API TileWorldServer : public TileWorld
{
public:
	TileWorldServer();
	~TileWorldServer();
};
};//namespace tile

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL2(tile::TileWorldServer , TileWorldServer , _SERVERCORE_API);
//extern  API_NULLTileWorldServer& singleton::GetTileWorldServer();
#define theTileWorldServer  singleton::GetTileWorldServer()



#endif //__TILEWORLDSERVER_H__
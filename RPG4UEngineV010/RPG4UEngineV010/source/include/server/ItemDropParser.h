/*////////////////////////////////////////////////////////////////////////
文 件 名：ItemDropParser.h
创建日期：2009年7月14日
最后更新：2009年7月14日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __ITEMDROPPARSER_H__
#define __ITEMDROPPARSER_H__
#pragma once

#pragma once

#include <ItemDropDefine.h>
#include "ConstDefine.h"
#include <BaseResParser.h>
#include <THashTable.h>

using namespace util;

struct sDROPITEM_RATIO
{
	DWORD	m_Code;
	VRSTR	m_Name;
	DWORD	m_Grade;
	WORD	m_Index;
	DWORD	m_DropAmount[MAX_PARTYMEMBER_NUM+1];
	DWORD	m_DropStandardAmount;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API ItemDropParser: public BaseResParser
{
public:
	ItemDropParser(void);
	virtual ~ItemDropParser(void);

public:
	BASERES_UNIT_DECL		();
	BASERES_UNIT_FOREACH2(sDROPITEM_RATIO, m_pDataInfos, m_Code);
	BASERES_UNIT_TABLE	(sDROPITEM_RATIO, DropItem);


public:
	virtual BOOL				Init		(DWORD dwPoolSize);
	virtual VOID				Release	();
	virtual BOOL				Load	(LPCTSTR pszFileName, BOOL bReload = FALSE );
	virtual VOID				Reload();

	virtual sDROPITEM_RATIO *	GetDropItemRatio( eNPC_GRADE Grade, INT Step );

	template<class OPR>		void ForEach(OPR& opr, INT nGrade);
protected:
	///////////////////////////////////
	sDROPITEM_RATIO 	m_DropItemRatio[NPC_GRADE_MAX][MAX_DROP_ITEM_STEP_NUM];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(ItemDropParser , _SERVERCORE_API);
//extern API_NULL ItemDropParser& singleton::GetItemDropParser();
#define theItemDropParser  singleton::GetItemDropParser()



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
inline sDROPITEM_RATIO *	ItemDropParser::GetDropItemRatio( eNPC_GRADE Grade, INT Step )
{ 
	__CHECK2_RANGE(Grade,NPC_GRADE_MAX, NULL);
	__CHECK2_RANGE(Step,MAX_DROP_ITEM_STEP_NUM, NULL);

	return &m_DropItemRatio[Grade][Step];
}

template<class OPR>		void ItemDropParser::ForEach(OPR& opr, INT nGrade)
{
	if(nGrade >= NPC_GRADE_MAX)
		return;
	for(INT n=0; n<MAX_DROP_ITEM_STEP_NUM; n++)
	{
		opr(m_DropItemRatio[nGrade][n]);
	}
}


#endif //__ITEMDROPPARSER_H__
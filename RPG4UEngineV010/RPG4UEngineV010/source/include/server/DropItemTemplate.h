/*////////////////////////////////////////////////////////////////////////
文 件 名：DropItemTemplate.h
创建日期：2007年10月9日
最后更新：2007年10月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
	 物品掉落模板类


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __DROPITEMTEMPLATE_H__
#define __DROPITEMTEMPLATE_H__
#pragma once


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API  DropItemTemplate
{
public:
    class _SERVERCORE_API  ItemDropRate
    {
    public:
        std::string strName;
        float fRate;
    };

    DropItemTemplate();
    virtual ~DropItemTemplate();

    bool LoadFromFile( const char *szFileName );

    ItemDropRate *GetItemInfo( int i )
    {
        if( i<0 || i>=Size() )
            return NULL;

        return &m_vecItemDropRate[i];
    }

    int Size() {
        return (int)m_vecItemDropRate.size();
    }

protected:

    std::vector<ItemDropRate> m_vecItemDropRate;

};

DropItemTemplate *GetItemDropTemplate( const char *szTempName );




#endif //__DROPITEMTEMPLATE_H__
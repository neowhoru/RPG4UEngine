/*////////////////////////////////////////////////////////////////////////
文 件 名：DropFormulaCore.h
创建日期：2009年5月2日
最后更新：2009年5月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __DROPFORMULACORE_H__
#define __DROPFORMULACORE_H__
#pragma once

//=======================================================================================================================
#include <ItemDropDefine.h>
#include <RatioRandomizer.h>
#include <RareItemDropParser.h>
#include <ItemDropParser.h>
#include <MoneyDropParser.h>
#include <MonsterItemDrop.h>


class ItemSlot;
class DropItemGradeParser;

struct sDROP_RESULT
{
	sDROP_RESULT():m_DropType(DROP_NONE),m_Money(0){}

	eDROP_TYPE		m_DropType;
	DropItemInfo	m_ItemInfo;
	MONEY				m_Money;
};
typedef const sDROP_RESULT	 sDROP_RESULTC;

using namespace std;
typedef vector<sDROP_RESULT>		DropResultArray;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API DropFormulaCore
{
public:
	enum			{ MAXTOP_ITEM_DROP_RATE = (10000-1), };

public:
	DropFormulaCore(void);
	virtual ~DropFormulaCore(void);

public:
	virtual BOOL Init	();
	virtual void Release	();

	virtual VOID Reload();

	///////////////////////////////////////
	virtual BOOL DispatchDrop	(sNPCINFO_BASE*	pNpcInfo
                              ,DWORD            PlayerNum
										,FLOAT            fBonusPercent
										,LEVELTYPE        LVAverage)=0;

	virtual BOOL	AdditionalItemDrop(BOOL           IsItemDrop
												,INT            MaxArrayNum
												,WORD *         pDropItemRate
												,WORD *         pDropItemCode);

	////////////////////////
	virtual BOOL	ProcessMoneyDrop	(FLOAT       fBonusPercent
												,LEVELTYPE   LVAverage
												,eDROP_TYPE  DropType
												,eNPC_GRADE  MonsterGrade
												,LEVELTYPE   MonsterDisplayLV);
	////////////////////////
	virtual BOOL	ProcessGeneralItemDrop	(eDROP_TYPE     DropType
														,LEVELTYPE      MonsterDisplayLV);

	////////////////////////
	virtual BOOL	ProcessRareItemDrop	(DWORD          PlayerNum
													,eNPC_GRADE     MonsterGrade
													,LEVELTYPE      MonsterDisplayLV);

	virtual BOOL	ProcessMonsterItemDrop	(CODETYPE		 dwMonsterCode);

	////////////////////////
	virtual BOOL	ProcessMonsterPrivateDrop	(INT            MaxArrayNum
															,WORD *         pDropItemRate
															,WORD *         pDropItemCode);


	//////////////////////////////
	virtual void	ClearDropResult();
	virtual void	AddDropResult	(sDROP_RESULTC& result);
	virtual BOOL	AddItemResult	(DWORD      dwItemCode
											,eDROP_TYPE dropType=DROP_ITEM_MONSTER)		;

	//////////////////////////////
	template<class OPR>
	BOOL			ForEachResult	(OPR& opr);


protected:
	VOID			_InitRatio();

protected:
	//DropItemGradeParser *	m_pItemDropRatio;

	FLOAT						m_fPlusScriptPercent;	

	RatioRandomizer		m_DropRatioSelecter	[NPC_GRADE_MAX][MAX_PARTYMEMBER_NUM+1];
	LEVELTYPE				m_Section				[MAX_LEVEL_STEP_NUM];
	RatioRandomizer		m_RareDropRatioSelecter[MAX_LEVEL_STEP_NUM][NPC_GRADE_MAX][MAX_PARTYMEMBER_NUM+1];

	//------------------------------------------------------------------------------------------------------------------------------------
public:
	//ItemDropParser			m_ItemDropParser;
	//RareItemDropParser	m_RareItemDropParser;
	//MoneyDropParser		m_MoneyDropParser;
	//MonsterItemDrop		m_MonsterDropParser;

private:
	DropResultArray		m_arDropResults;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_PTR_DECL(DropFormulaCore , API_NULL);
//extern API_NULL DropFormulaCore& singleton::GetDropFormulaCore();
#define theDropFormulaCore  singleton::GetDropFormulaCore()


#include "DropFormulaCore.inl"


#endif //__DROPFORMULACORE_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：NetworkGroupFactoryManager.h
创建日期：2006年6月23日
最后更新：2006年6月23日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NETWORKGROUPFACTORYMANAGER_H__
#define __NETWORKGROUPFACTORYMANAGER_H__
#pragma once

#include "GlobalInstanceSingletonDefine.h"
#include "NetworkGroupDefine.h"
#include "TObjectFactoryManager.h"

class NetworkGroup;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////

typedef TIObjectFactory<NetworkGroup,NETWORKGROUP_TYPE>		NetworkGroupFactory;

/////////////////////
//使用方法：
//1.在NetworkGroup加
//	virtual NETWORKGROUP_TYPE GetFactoryType()=0;
//2.NetworkGroup在NetworkGroupFactory注册时，指定FactoryType及池数量即可
//		参考以下宏：
//		2.1库注册宏
#define REG_NETWORKGROUPFACTORY(NAME,TYPE)\
	static NAME##NetworkGroupFactory	s##NAME##NetworkGroupFactory(TYPE);\
	theNetworkGroupFactoryManager.RegisterFactory(&s##NAME##NetworkGroupFactory);
//		2.2通过宏，注册NetworkGroup到Factory中，譬如：
//			REG_NetworkGroupFACTORY(NETWORKGROUP		,TAG('NETWORKGROUP')		,	100);			


/////////////////////
//3.到"需要注册到 NetworkGroupFactory的"NetworkGroup子类中，加入以下宏声明：
//   譬如：
//   NETWORKGROUP_DECLARE(NETWORKGROUP_SUB);
#define NETWORKGROUP_DECLARE(NAME)\
	public:\
		virtual NETWORKGROUP_TYPE GetFactoryType();

/////////////////////
//4.到"需要注册到 NetworkGroupFactory的"NetworkGroup子类中，加入以下宏声明：
//   譬如：
//   NETWORKGROUP_FACTORY_DECLARE(NETWORKGROUP_SUB);
#define NETWORKGROUP_FACTORY_DECLARE(NAME)\
	typedef TObjectFactoryNew<NAME,NetworkGroup,NETWORKGROUP_TYPE,NETWORKGROUP_NULL>	NAME##NetworkGroupFactory;

/////////////////////
//5.到"需要注册到 NetworkGroupFactory的"NetworkGroup子类cpp文件中，加入以下宏实现：
//   譬如：
//   NETWORKGROUP_IMPL(NETWORKGROUP_SUB);
#define NETWORKGROUP_IMPL(NAME)\
	NETWORKGROUP_TYPE NAME::GetFactoryType()\
{\
	assert(NAME##NetworkGroupFactory::ms_typeName);\
	return NAME##NetworkGroupFactory::ms_typeName;\
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERCORE_API NetworkGroupFactoryManager : public TObjectFactoryManager<NetworkGroup,NETWORKGROUP_KEY,NETWORKGROUP_TYPE >
{
};
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(NetworkGroupFactoryManager , _SERVERCORE_API);
//extern _SERVERCORE_API NetworkGroupFactoryManager& singleton::GetNetworkGroupFactoryManager();
#define theNetworkGroupFactoryManager  singleton::GetNetworkGroupFactoryManager()


#endif //__NETWORKGROUPFACTORYMANAGER_H__
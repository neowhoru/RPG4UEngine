/*////////////////////////////////////////////////////////////////////////
文 件 名：LogStruct.h
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __LOGSTRUCT_H__
#define __LOGSTRUCT_H__
#pragma once

#include "ConstLog.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#pragma pack(push, 1)

struct sLOG_HEADER
{
	DWORD		dwLogType;									
	TCHAR		szActionTime[MAX_TIMEDATA_SIZE	];	
	TCHAR		szAccountID	[MAX_ACCOUNT_LENGTH	];			
	TCHAR		szCharName	[MAX_CHARNAME_LENGTH	];			
	INT		nDataSize;										
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class BaseLogData : public sLOG_HEADER
{
public:
	BaseLogData()
	{
		Init();
	}
	void Init()
	{
		dwLogType			= 0;
		szActionTime[0]	= 0;
		szAccountID[0]		= 0;
		szCharName[0]		= 0;
		nDataSize			= 0;
	}
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class CharacterLogData : public BaseLogData
{
public:
	CharacterLogData()
	{
		Init();
	}
	void Init()
	{
		__ZERO(szClientIP		);
		__ZERO(arCharStats		);
		__ZERO(arInventory	);
		__ZERO(arTmpInventory);
		__ZERO(arEquipItem	);
		__ZERO(arSkills		);
		__ZERO(arQuests		);
		__ZERO(arMissions		);
	}
	TCHAR szClientIP	[MAX_IP_LENGTH						]	;
	BYTE arCharStats	[MAX_CHAR_STAT_SIZE				];
	BYTE arInventory	[MAX_INVENTORYITEMSTREAM_SIZE	];
	BYTE arTmpInventory[MAX_TEMPINVENTORYITEMSTREAM_SIZE];
	BYTE arEquipItem	[MAX_EQUIPITEMSTREAM_SIZE		];
	BYTE arSkills		[MAX_SKILLSTREAM_SIZE			];
	BYTE arQuests		[MAX_QUESTSTREAM_SIZE			];
	BYTE arMissions	[MAX_MISSIONSTREAM_SIZE			];
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ItemLogData : public BaseLogData
{
public:
	ItemLogData()
	{
		Init();
	}

	void Init()
	{
		dwItemCode = 0;
		dwItemInfoCode = 0;
		dwItemSerial = 0;
		dwItemAmount = 0;
		dwMapCode = 0;
		fMapX = 0;
		fMapY = 0;
		fMapZ = 0;
		szToUserID[0] = '\0';
		szToCharName[0] = '\0';
		szToIpAddr[0] = '\0';
		dwServerCode = 0;
	}

	DWORD   dwItemCode;								
	DWORD   dwItemInfoCode;							
	DWORD   dwItemSerial;							
	DWORD   dwItemAmount;
	DWORD dwMapCode;
	float fMapX;
	float fMapY;
	float fMapZ;
	TCHAR szToUserID[MAX_ACCOUNT_LENGTH];			
	TCHAR szToCharName[MAX_CHARNAME_LENGTH];		
	TCHAR szToIpAddr[MAX_IP_LENGTH];					
	DWORD   dwServerCode;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ExpData : public BaseLogData
{
public:
	ExpData()
	{
		Init();
	}
	void Init()
	{
		dwExpLogCode = 0;
		dwNewLevel = 0;
		dwPrevLevel = 0;
		dwNewExp = 0;
		dwPrevExp = 0;
		dwLevelUpAmount = 0;
		dwServerCode = 0;
	}

	DWORD	  dwExpLogCode;	
	DWORD   dwNewLevel;						
	DWORD   dwPrevLevel;
	DWORD   dwNewExp;
	DWORD   dwPrevExp;
	DWORD   dwLevelUpAmount;
	DWORD   dwServerCode;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ActionLogData : public BaseLogData
{
public:
	ActionLogData()
	{
		Init();
	}

	void Init()
	{
		dwActionLogCode = 0;
		dwMapCode = 0;
		fMapX = 0;
		fMapY = 0;
		fMapZ = 0;
		dwServerCode = 0;
	}

	DWORD   dwActionLogCode;
	DWORD dwMapCode;
	float fMapX;
	float fMapY;
	float fMapZ;
	DWORD   dwServerCode;
};




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class SkillLogData : public BaseLogData
{
public:
	SkillLogData()
	{
		Init();
	}

	void Init()
	{
		dwSkillCode = 0;
		dwSkillLevel = 0;
		dwSkillUseCounter = 0;
	}

	DWORD    dwSkillCode;				
	DWORD		dwSkillLevel;				
	DWORD	   dwSkillUseCounter;			
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChatLogData : public BaseLogData
{
public:
	ChatLogData()
	{
		Init();
	}

	void Init()
	{
		dwChatLogCode	= 0;
		nChannelIndex	= 0;
		dwUserGuid		= 0;
		dwToUserGuid	= 0;
		szToCharName[0] = '\0';
		szChatContext[0] = '\0';
		dwMapCode		= 0;
		dwRoomCode		= 0;
	}
	DWORD	dwChatLogCode;
	DWORD dwMapCode;
	DWORD	dwRoomCode;
	DWORD dwUserGuid;
	DWORD dwToUserGuid;
	INT	nChannelIndex;
	TCHAR szToCharName	[MAX_CHARNAME_LENGTH];
	TCHAR szChatContext	[MAX_CHAT_LENGTH];
};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class SessionLogData : public BaseLogData
{
public:
	SessionLogData() 
	{
		Init();
	}

	void Init()
	{
		nConnectType = 0;
		nLoginType = 0;
		dwServerCode = 0;
		szClientIP[0] = '\0';
		szLoginTime[0] = '\0';
		szLogoutTime[0] = '\0';
		nLevel = 0;
		dwUseTime = 0;
		dwIP = 0;
	}
	
	INT	nConnectType;							
	INT   nLoginType;									
	INT   dwServerCode;								
	TCHAR szClientIP	[MAX_IP_LENGTH];		
	TCHAR szLoginTime	[MAX_TIMEDATA_SIZE];		
	TCHAR szLogoutTime[MAX_TIMEDATA_SIZE];
	INT	nLevel;
	DWORD   dwUseTime;
	DWORD  dwIP;
	DWORD	dwClassType;
};


#pragma pack(pop)



#endif //__LOGSTRUCT_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：XMLParser.h
创建日期：2007年10月2日
最后更新：2007年10月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __XMLPARSER_H__
#define __XMLPARSER_H__
#pragma once

#include "NamedObject.h"

using namespace std;

#define deleteObject(A){ if(A){ delete   A; A = NULL; } }
#define deleteArray(A) { if(A){ delete[] A; A = NULL; } }

#define XML_FILE_NOT_FOUND  0x0001
#define XML_SUCCESS         0x0002   
#define XML_FAILED          0x0004
#define XML_READY           0x0008
#define XML_MATCH           0x0010
#define XML_BUFFER_OVERFLOW 0x0020

class _BASE_API RawData
{
  public:
    bool  skipDelete;
    char *data;
    int   byteCount;

    RawData();
    RawData(const RawData &copy);
    RawData &operator=(const RawData &copy);
   ~RawData();
    void destroy();
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class XMLElement;
class _BASE_API XMLTree
{
  protected:
    vector<XMLElement*> children;

  public:
   ~XMLTree();

  public:
	XMLElement& operator()(LPCSTR szField);

  public:
    void        addChild(XMLElement *child);
    XMLElement *getChildByName(const char *name);
    XMLElement *getChild(size_t index);
    size_t      getChildrenCount();
    void        flush();
    void        print();

};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API XMLElement : public XMLTree, public NamedObject
{
private:
	string  value;
public:
	XMLElement();
	~XMLElement();

public:
	XMLElement(const XMLElement &copy);
	XMLElement &operator =(const XMLElement &copy);


public:
	template <class T> 
	XMLElement& operator >> ( T &Data);
	XMLElement& operator >> ( LPCSTR &Data);
	XMLElement& operator >> ( char Data[]);

	operator char* ();
	operator float  ();
	operator double ();
	template <class T> 
	operator T ();


public:
	void setValue(const string& val);
	void print();
	void flush(); 

	const string  &getValue ();
	const double   getValued();
	const float    getValuef();
	const char*    getValuec();
	const int      getValuei();
	const bool	   getValueb();
	//const string&		   getValues();
	LPCSTR   getValues();


	RawData rawData;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API XMLStack : public XMLTree
{
  public:
    XMLStack(char *xmlFilePath = NULL, bool loggerOn = true);    
    XMLStack(const XMLStack &copy);
   ~XMLStack();
    XMLStack &operator =(const XMLStack &copy);

    void   print();
    void   flush();
    int    loadXMLFile(const char *xmlFilePath);
	int	   loadXMLBuffer( const char *pszBuffer, int nBufferSize );

  private:
    void   writeFatalLogInfoList(const char* format,...);
    void   writeFatalLogInfo(const char* string);


    bool   moveAndCheckProgress(int jump = 1);
    bool   consumeWhiteSpaces(char **stream);
    bool   consumeXMLHeader(char **stream);
    bool   consumeComment(char **stream);
    bool   fillRawData(char **stream, RawData *dataStruct, int count = -1);
    char*  parseXMLStream(char *stream, XMLElement *parent);
    int    getRemainingBytes();

private:
	void   getStreamedValue(char **stream, string&);
	void   getStringValue  (char **stream, string&);
	void   getIdentifier   (char **stream, string&);

private:
    string encoding,
           logFilePath;
    float  XMLVersion;
    bool   loggerOn;
    int    bufferProgress,
           bufferSize,
           state;

public:
	static char nullChar[];
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include "XMLParser.inl"

#endif //__XMLPARSER_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：	BaseLibExport.h
创建日期：	2007.12.27
最后更新：	2007.12.27
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#pragma once

// disable: "<type> needs to have dll-interface to be used by clients'
// Happens on STL member variables which are not public therefore is ok
#pragma warning (disable : 4251)

// disable: "non dll-interface class used as base for dll-interface class"
// Happens when deriving from Singleton because bug in compiler ignores
// template export
#pragma warning (disable : 4275)



#ifdef _SCRIPTLIBSTATIC

	///////////////////////////////////
	//动态库输出
	#ifdef _USRDLL
		#define _SCRIPTLIBSTATIC_API __declspec( dllexport )
		#pragma message("   _SCRIPTLIBSTATIC_API __declspec( dllexport )")

	///////////////////////////////////
	//非动态输出
	#else
		#define _SCRIPTLIBSTATIC_API
		#define _SCRIPTLIBSTATIC_STATIC
		#pragma message("   _SCRIPTLIBSTATIC_STATIC")
	#endif

#else

#define USE_SCRIPTLIBSTATIC_STATIC
	///////////////////////////////////
	//静态库
	#ifdef USE_SCRIPTLIBSTATIC_STATIC
		#define _SCRIPTLIBSTATIC_API
		#pragma message("    USE_SCRIPTLIBSTATIC_STATIC")
		#ifdef _DEBUG
			#pragma comment(lib,"ScriptlibStaticD.lib")
		#else
			#pragma comment(lib,"ScriptlibStatic.lib")
		#endif

	///////////////////////////////////
	//动态库
	#else
		#define _SCRIPTLIBSTATIC_API __declspec( dllimport )
		#pragma message("   _SCRIPTLIBSTATIC_API __declspec( dllimport )")
		#ifdef _DEBUG
			#pragma comment(lib,"ScriptlibD.lib")
		#else
			#pragma comment(lib,"Scriptlib.lib")
		#endif
	#endif

#endif




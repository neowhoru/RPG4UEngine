/*////////////////////////////////////////////////////////////////////////
文 件 名：FilePathUtil.h
创建日期：2007年10月19日
最后更新：2007年10月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __FILEPATHUTIL_H__
#define __FILEPATHUTIL_H__
#pragma once

#include "CommonDefine.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API  FilePathUtil
{
public:
	FilePathUtil();

public:
	static BOOL ExplorerFile	( LPCSTR pszPathName ,HWND hWnd=NULL);
	static BOOL ExplorerSelect	( LPCSTR pszPathName ,HWND hWnd=NULL);

	static BOOL IsExist			( LPCSTR pszPathName );
	static BOOL IsDirectory		( LPCSTR pszPathName );
	static BOOL MakeDirectory	( LPCSTR pszPathName , BOOL bCreateWithFilename=FALSE );
	static DWORD GetFileSize	( LPCSTR pszPathName );
	static BOOL IsSamePath		(LPCSTR szFromPath
                              ,LPCSTR szToPath);
	static BOOL CopyDirectory	(LPCSTR szFromPath
                              ,LPCSTR szToPath
										,BOOL   bFailIfExists
										,BOOL   bWithSubDir);

	//static LPCSTR GetFileName();
	static LPCSTR GetFileNameExt( LPCSTR pszPathName );
	static LPCSTR GetFileDir( LPCSTR pszPathName );
	//static LPCSTR GetDir();
	//static LPCSTR GetPath();
	
	
	BOOL		Split					( LPCSTR pszPathName );
	LPCSTR	ConcatFullPath		( );
	


	BOOL IsExt(LPCSTR szExt);

protected:
	VG_CSTR_PROPERTY(Drive,		_MAX_DRIVE);
	VG_CSTR_PROPERTY(Dir,		_MAX_DIR  );
	VG_CSTR_PROPERTY(FileName,	_MAX_FNAME);
	VG_CSTR_PROPERTY(Ext,		_MAX_EXT  );
	VG_CSTR_PROPERTY(FullPath,	_MAX_PATH  );

};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API SystemFileInfo
{
public:
	SystemFileInfo(void);
	~SystemFileInfo(void);


	int GetByteBitSum( BYTE byte )
	{
		int nCount = 0;
		for( int i = 0; i < 8; i++ )
		{
			if( byte & 0x01 )
				nCount++;
			byte >>= 1;
		}
		return nCount;

	}
	BOOL Create( LPCSTR szFileName, BOOL* pbProcess );

public:
	//std::string m_strFilename;		// 文件名
	char		m_szFilename[MAX_PATH];
	DWORD		m_dwSize;			// 文件大小
	DWORD		m_dwCheckSum;		// 文件所有byte的和
	DWORD		m_dwBitSum;			// 所有的bit的和

};



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _BASE_API SystemFileInfoManager
{
public:
	SystemFileInfoManager(){};
	~SystemFileInfoManager();

	void	Destroy();
	BOOL	CreateFileList( LPCSTR szFileName, std::vector<SystemFileInfo>& files );
protected:
	std::vector<SystemFileInfo*> m_files;
};




#endif //__FILEPATHUTIL_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：TColor4D.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TCOLOR4D_INL__
#define __TCOLOR4D_INL__
#pragma once

namespace math
{ 
//--------------------------
// Color
//--------------------------


template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::TColor4D( DWORD dw )
{
	Set(dw);
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::TColor4D( CONST Type* pf )
{
	Set(pf);
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::TColor4D( CONST TColor4DStruct<Type>& c )
{
	Set(c);
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::TColor4D( Type fr, Type fg, Type fb, Type fa )
{
	Set(fr,  fg,  fb, fa);
}

//////////////////////////
template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>& TColor4D<Type,range,diff>::Set( DWORD dw )
{
    //CONST Type f = 1.0f / 255.0f;
    CONST float f = (float)range / 255.0f;
    r = f * (Type) (BYTE) (dw >> 16);
    g = f * (Type) (BYTE) (dw >>  8);
    b = f * (Type) (BYTE) (dw >>  0);
    a = f * (Type) (BYTE) (dw >> 24);
	 return *this;
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>& TColor4D<Type,range,diff>::Set( CONST Type* pf )
{
#ifdef _DEBUG
    if(!pf)
        return;
#endif
    r = pf[0];
    g = pf[1];
    b = pf[2];
    a = pf[3];
	 return *this;
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>& TColor4D<Type,range,diff>::Set( CONST TColor4DStruct<Type>& c )
{
    r = c.r;
    g = c.g;
    b = c.b;
    a = c.a;
	 return *this;
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>& TColor4D<Type,range,diff>::Set( Type fr, Type fg, Type fb, Type fa )
{
    r = fr;
    g = fg;
    b = fb;
    a = fa;
	 return *this;
}



// casting
template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator DWORD () const
{
    //float f = 255.0f/ (float)range ;
    //DWORD dwR = r >= range ? 0xff : r <= 0.0f ? 0x00 : (DWORD) (r * f + (Type)0.5f);
    //DWORD dwG = g >= range ? 0xff : g <= 0.0f ? 0x00 : (DWORD) (g * f + (Type)0.5f);
    //DWORD dwB = b >= range ? 0xff : b <= 0.0f ? 0x00 : (DWORD) (b * f + (Type)0.5f);
    //DWORD dwA = a >= range ? 0xff : a <= 0.0f ? 0x00 : (DWORD) (a * f + (Type)0.5f);

    //return (dwA << 24) | (dwR << 16) | (dwG << 8) | dwB;
	return GetARGB();
}


template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator Type * ()
{
    return (Type *) &r;
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator CONST Type * () const
{
    return (CONST Type *) &r;
}


template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator TColor4DStruct<Type> * ()
{
    return (TColor4DStruct<Type> *) &r;
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator CONST TColor4DStruct<Type> * () const
{
    return (CONST TColor4DStruct<Type> *) &r;
}


template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator TColor4DStruct<Type>& ()
{
    return *((TColor4DStruct<Type> *) &r);
}

template<typename Type,int range,int diff> inline
TColor4D<Type,range,diff>::operator CONST TColor4DStruct<Type>& () const
{
    return *((CONST TColor4DStruct<Type> *) &r);
}


// assignment operators
template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>&
TColor4D<Type,range,diff>::operator += ( CONST TColor4D<Type,range,diff>& c )
{
    r = r +c.r;
    g = g +c.g;
    b = b +c.b;
    a = a +c.a;
    return *this;
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>&
TColor4D<Type,range,diff>::operator -= ( CONST TColor4D<Type,range,diff>& c )
{
    r -= c.r;
    g -= c.g;
    b -= c.b;
    a -= c.a;
    return *this;
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>&
TColor4D<Type,range,diff>::operator *= ( Type f )
{
    r = r *f;
    g = g *f;
    b = b *f;
    a = a *f;
    return *this;
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>&
TColor4D<Type,range,diff>::operator /= ( Type f )
{
    float fInv = 1.0f / (float)f;
    r = (Type)(r *fInv);
    g = (Type)(g *fInv);
    b = (Type)(b *fInv);
    a = (Type)(a *fInv);
    return *this;
}


// unary operators
template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>
TColor4D<Type,range,diff>::operator + () const
{
    return *this;
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>
TColor4D<Type,range,diff>::operator - () const
{
    return TColor4D<Type,range,diff>(-r, -g, -b, -a);
}


// binary operators
template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>
TColor4D<Type,range,diff>::operator + ( CONST TColor4D<Type,range,diff>& c ) const
{
    return TColor4D<Type,range,diff>(r + c.r, g + c.g, b + c.b, a + c.a);
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>
TColor4D<Type,range,diff>::operator - ( CONST TColor4D<Type,range,diff>& c ) const
{
    return TColor4D<Type,range,diff>(r - c.r, g - c.g, b - c.b, a - c.a);
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>
TColor4D<Type,range,diff>::operator * ( Type f ) const
{
    return TColor4D<Type,range,diff>(r * f, g * f, b * f, a * f);
}

template<typename Type,int range,int diff> inline TColor4D<Type,range,diff>
TColor4D<Type,range,diff>::operator / ( Type f ) const
{
    float fInv = 1.0f / (float)f;
    return TColor4D<Type,range,diff>(r * fInv, g * fInv, b * fInv, a * fInv);
}




template<typename Type,int range,int diff> inline BOOL
TColor4D<Type,range,diff>::operator == ( CONST TColor4D<Type,range,diff>& c ) const
{
    return r == c.r && g == c.g && b == c.b && a == c.a;
}

template<typename Type,int range,int diff> inline BOOL
TColor4D<Type,range,diff>::operator != ( CONST TColor4D<Type,range,diff>& c ) const
{
    return r != c.r || g != c.g || b != c.b || a != c.a;
}


template<typename Type,int range,int diff>
inline void	 TColor4D<Type,range,diff>::Clamp  ()
{
   if (red > range)		red = range;
   else if (red < 0)    red = 0;

   if (green > range)   green = range;
   else if (green < 0)  green = 0;

   if (blue > range)		blue = range;
   else if (blue < 0)   blue = 0;

   if (alpha > range)   alpha = range;
   else if (alpha < 0)  alpha = 0;
}

template<typename Type,int range,int diff>
DWORD TColor4D<Type,range,diff>::GetARGB() const
{
  float fMax = 255.0f/ (float)range ;

   return (DWORD(alpha * fMax + 0.5) << 24) |
          (DWORD(red   * fMax + 0.5) << 16) |
          (DWORD(green * fMax + 0.5) <<  8) |
          (DWORD(blue  * fMax + 0.5) <<  0);
}

template<typename Type,int range,int diff>
DWORD TColor4D<Type,range,diff>::GetRGBA() const
{
  float fMax = 255.0f/ (float)range ;
   return (DWORD(red   * fMax + 0.5) << 24) |
          (DWORD(green * fMax + 0.5) << 16) |
          (DWORD(blue  * fMax + 0.5) <<  8) |
          (DWORD(alpha * fMax + 0.5) <<  0);
}

template<typename Type,int range,int diff>
DWORD TColor4D<Type,range,diff>::GetABGR() const
{
  float fMax = 255.0f/ (float)range ;
   return (DWORD(alpha * fMax + 0.5) << 24) |
          (DWORD(blue  * fMax + 0.5) << 16) |
          (DWORD(green * fMax + 0.5) <<  8) |
          (DWORD(red   * fMax + 0.5) <<  0);
}

template<typename Type,int range,int diff>
void TColor4D<Type,range,diff>::Slerp	 (const TColor4D<Type,range,diff>& clr1,
											 const TColor4D<Type,range,diff>& clr2,
											 REAL     in_factor)
{
   REAL f2 = 1.0f - in_factor;
	REAL d  = (float)diff/range;

   red   = Type(((REAL(clr1.red)   * f2) + (REAL(clr2.red)   * in_factor)) + d);
   green = Type(((REAL(clr1.green) * f2) + (REAL(clr2.green) * in_factor)) + d);
   blue  = Type(((REAL(clr1.blue)  * f2) + (REAL(clr2.blue)  * in_factor)) + d);
   alpha = Type(((REAL(clr1.alpha) * f2) + (REAL(clr2.alpha) * in_factor)) + d);
}



};//namespace math


#endif //__TCOLOR4D_INL__
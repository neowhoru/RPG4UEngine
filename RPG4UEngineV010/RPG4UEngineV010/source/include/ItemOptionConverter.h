
#include "StructBase.h"
#include <ConstItem.h>

class ItemSlot;
class _RESOURCELIB_API ItemOptionConverter
{
public:
	ItemOptionConverter(void);
	~ItemOptionConverter(void);

	static BOOL ExistEmptySocket( ItemSlot & slot, eSOCKET & OUT EmptySocket );
	static BOOL	IsOption( ItemSlot & slot );
};

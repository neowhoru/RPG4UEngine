/*////////////////////////////////////////////////////////////////////////
文 件 名：Matrix.inl
创建日期：2008年4月9日
最后更新：2008年4月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __TMATRIX4_INL__
#define __TMATRIX4_INL__
#pragma once

namespace math
{ 

//--------------------------
// Matrix
//--------------------------
template<typename Type>
inline TMatrix4<Type>::TMatrix4( CONST Type* pf )
{
#ifdef _DEBUG
    if(!pf)
        return;
#endif

    memcpy(&_11, pf, sizeof(TMatrix4<Type>));
}

template<typename Type>
inline TMatrix4<Type>::TMatrix4( CONST TMatrix4Struct<Type>& mat )
{
    memcpy(&_11, &mat, sizeof(TMatrix4<Type>));
}

template<typename Type>
inline TMatrix4<Type>::TMatrix4( Type f11, Type f12, Type f13, Type f14,
                        Type f21, Type f22, Type f23, Type f24,
                        Type f31, Type f32, Type f33, Type f34,
                        Type f41, Type f42, Type f43, Type f44 )
{
    _11 = f11; _12 = f12; _13 = f13; _14 = f14;
    _21 = f21; _22 = f22; _23 = f23; _24 = f24;
    _31 = f31; _32 = f32; _33 = f33; _34 = f34;
    _41 = f41; _42 = f42; _43 = f43; _44 = f44;
}



// access grants
template<typename Type>
inline Type& TMatrix4<Type>::operator () ( UINT iRow, UINT iCol )
{
    return m[iRow][iCol];
}

template<typename Type>
inline Type TMatrix4<Type>::operator () ( UINT iRow, UINT iCol ) const
{
    return m[iRow][iCol];
}


// casting operators
template<typename Type>
inline TMatrix4<Type>::operator Type* ()
{
    return (Type *) &_11;
}

template<typename Type>
inline TMatrix4<Type>::operator CONST Type* () const
{
    return (CONST Type *) &_11;
}


// assignment operators
template<typename Type>
inline TMatrix4<Type>& TMatrix4<Type>::operator *= ( CONST TMatrix4<Type>& mat )
{
	Matrix4 matT(*this);
	return Multiply(matT,mat);
}

template<typename Type>
inline TMatrix4<Type>& TMatrix4<Type>::operator += ( CONST TMatrix4<Type>& mat )
{
    _11 += mat._11; _12 += mat._12; _13 += mat._13; _14 += mat._14;
    _21 += mat._21; _22 += mat._22; _23 += mat._23; _24 += mat._24;
    _31 += mat._31; _32 += mat._32; _33 += mat._33; _34 += mat._34;
    _41 += mat._41; _42 += mat._42; _43 += mat._43; _44 += mat._44;
    return *this;
}

template<typename Type>
inline TMatrix4<Type>& TMatrix4<Type>::operator -= ( CONST TMatrix4<Type>& mat )
{
    _11 -= mat._11; _12 -= mat._12; _13 -= mat._13; _14 -= mat._14;
    _21 -= mat._21; _22 -= mat._22; _23 -= mat._23; _24 -= mat._24;
    _31 -= mat._31; _32 -= mat._32; _33 -= mat._33; _34 -= mat._34;
    _41 -= mat._41; _42 -= mat._42; _43 -= mat._43; _44 -= mat._44;
    return *this;
}

template<typename Type>
inline TMatrix4<Type>& TMatrix4<Type>::operator *= ( Type f )
{
    _11 *= f; _12 *= f; _13 *= f; _14 *= f;
    _21 *= f; _22 *= f; _23 *= f; _24 *= f;
    _31 *= f; _32 *= f; _33 *= f; _34 *= f;
    _41 *= f; _42 *= f; _43 *= f; _44 *= f;
    return *this;
}

template<typename Type>
inline TMatrix4<Type>& TMatrix4<Type>::operator /= ( Type f )
{
    float fInv = 1.0f / (float)f;
    _11 *= fInv; _12 *= fInv; _13 *= fInv; _14 *= fInv;
    _21 *= fInv; _22 *= fInv; _23 *= fInv; _24 *= fInv;
    _31 *= fInv; _32 *= fInv; _33 *= fInv; _34 *= fInv;
    _41 *= fInv; _42 *= fInv; _43 *= fInv; _44 *= fInv;
    return *this;
}


// unary operators
template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator + () const
{
    return *this;
}

template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator - () const
{
    return TMatrix4<Type>(-_11, -_12, -_13, -_14,
                      -_21, -_22, -_23, -_24,
                      -_31, -_32, -_33, -_34,
                      -_41, -_42, -_43, -_44);
}


// binary operators
template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator * ( CONST TMatrix4<Type>& mat ) const
{
	TMatrix4<Type> matT;
	return matT.Multiply(*this,mat);
}

template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator + ( CONST TMatrix4<Type>& mat ) const
{
    return TMatrix4<Type>(_11 + mat._11, _12 + mat._12, _13 + mat._13, _14 + mat._14,
                      _21 + mat._21, _22 + mat._22, _23 + mat._23, _24 + mat._24,
                      _31 + mat._31, _32 + mat._32, _33 + mat._33, _34 + mat._34,
                      _41 + mat._41, _42 + mat._42, _43 + mat._43, _44 + mat._44);
}

template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator - ( CONST TMatrix4<Type>& mat ) const
{
    return TMatrix4<Type>(_11 - mat._11, _12 - mat._12, _13 - mat._13, _14 - mat._14,
                      _21 - mat._21, _22 - mat._22, _23 - mat._23, _24 - mat._24,
                      _31 - mat._31, _32 - mat._32, _33 - mat._33, _34 - mat._34,
                      _41 - mat._41, _42 - mat._42, _43 - mat._43, _44 - mat._44);
}

template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator * ( Type f ) const
{
    return TMatrix4<Type>(_11 * f, _12 * f, _13 * f, _14 * f,
                      _21 * f, _22 * f, _23 * f, _24 * f,
                      _31 * f, _32 * f, _33 * f, _34 * f,
                      _41 * f, _42 * f, _43 * f, _44 * f);
}

template<typename Type>
inline TMatrix4<Type> TMatrix4<Type>::operator / ( Type f ) const
{
    float fInv = 1.0f / (float)f;
    return TMatrix4<Type>(_11 * fInv, _12 * fInv, _13 * fInv, _14 * fInv,
                      _21 * fInv, _22 * fInv, _23 * fInv, _24 * fInv,
                      _31 * fInv, _32 * fInv, _33 * fInv, _34 * fInv,
                      _41 * fInv, _42 * fInv, _43 * fInv, _44 * fInv);
}




template<typename Type>
inline BOOL TMatrix4<Type>::operator == ( CONST TMatrix4<Type>& mat ) const
{
    return 0 == memcmp(this, &mat, sizeof(TMatrix4<Type>));
}

template<typename Type>
inline BOOL TMatrix4<Type>::operator != ( CONST TMatrix4<Type>& mat ) const
{
    return 0 != memcmp(this, &mat, sizeof(TMatrix4<Type>));
}

};//namespace math


#endif //__TMATRIX_INL__
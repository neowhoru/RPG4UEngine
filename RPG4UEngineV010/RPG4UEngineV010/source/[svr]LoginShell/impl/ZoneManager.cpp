#include "StdAfx.h"
#include ".\zonemanager.h"
#include <assert.h>

ZoneManager::ZoneManager(void)
{
}

ZoneManager::~ZoneManager(void)
{
}

VOID ZoneManager::Init()
{
}

VOID ZoneManager::Release()
{
	m_mapZones.clear();
}

VOID ZoneManager::AddZone( DWORD dwZoneKey, Zone *pZone )
{
	ZONE_MAP_ITER it = m_mapZones.find( dwZoneKey );
	assert( it == m_mapZones.end() );
	m_mapZones.insert( ZONE_MAP_PAIR( dwZoneKey, pZone ) );
}

VOID ZoneManager::RemoveZone( DWORD dwZoneKey )
{
	ZONE_MAP_ITER it = m_mapZones.find( dwZoneKey );
	assert( it != m_mapZones.end() );
	m_mapZones.erase( it );
}

Zone* ZoneManager::FindZone( DWORD dwZoneKey )
{
	ZONE_MAP_ITER it = m_mapZones.find( dwZoneKey );
	if( it != m_mapZones.end() )
	{
		return it->second;
	}
	else
	{
		return NULL;
	}
}
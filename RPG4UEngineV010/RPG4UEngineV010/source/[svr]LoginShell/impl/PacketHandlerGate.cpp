/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGate.cpp
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerGate.cpp
创建日期：2007年6月7日
最后更新：2007年6月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "PacketHandlerGate.h"
#include "LoginShell.h"
#include "GateSession.h"
#include "UserManager.h"
#include <PacketStruct_Gate_Login.h>
#include <Protocol_Gate_Login.h>
#include <PacketStruct_ClientLogin.h>
#include <Protocol_ClientLogin.h>

#include "GateChannel.h"
#include "ChannelManager.h"
#include "User.h"
#include "UserManager.h"


/*////////////////////////////////////////////////////////////////////////
GateShell成功连接AuthServer后，提供其服务端信息
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_LOGIN_AU(AU_LOGIN_SERVER_LOGIN_CMD	)
{
	MSG_AU_LOGIN_SERVER_LOGIN_CMD*	pMsgLogin = (MSG_AU_LOGIN_SERVER_LOGIN_CMD*)pMsg;
	assert(pSession->GetChannel() == 0);

	GateChannel* pChannel;
	
	pChannel = theChannelManager.AddChannel(pMsgLogin->wAgentID);
	__VERIFY_PTR(pChannel, "Invalid GateChannel");

	//pSession->SetChannelID(pMsgLogin->wServerID);
	pSession->SetChannel(pChannel);

	pChannel->SetGateSession	(pSession);
	pChannel->SetGroupID					(pMsgLogin->wWorldID);
	pChannel->SetChannelName			(pMsgLogin->szSvrName);
	pChannel->SetMaxUserCount			(pMsgLogin->wMaxUserCount);

	MessageOut( LOG_CRITICAL,   "新增服务线(%s :%d#)\n",  pMsgLogin->szSvrName,pMsgLogin->wAgentID );

	pChannel->SetGateClientSidePort	(pMsgLogin->wPort);
	pChannel->SetGateClientSideIP	(pMsgLogin->szSvrIP);



}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
保持GateShell同步数量
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_LOGIN_AU(AU_LOGIN_GAME2LOGIN_STATUS_CMD)
{
	MSG_AU_LOGIN_GAME2LOGIN_STATUS_CMD*	pMsgStatus = (MSG_AU_LOGIN_GAME2LOGIN_STATUS_CMD*)pMsg;
	GateChannel* pChannel = pSession->GetChannel();
	assert(pChannel);

	pChannel->OnGetGameStatus(pMsgStatus->dwTimeStamp,
							(DWORD)pMsgStatus->wUserCount);

}
PACKETHANDLER_SERVER_END();

								
/*////////////////////////////////////////////////////////////////////////
选择进入GateShell后的响应
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_LOGIN_AU(AU_LOGIN_NEWCLIENT_ACK	)
{
	MSG_AU_LOGIN_NEWCLIENT_ACK* pMsgAns = (MSG_AU_LOGIN_NEWCLIENT_ACK*)pMsg;
	User* pUser = theUserManager.FindUser(pMsgAns->dwAuthUserID);
	GateChannel* pChannel = pSession->GetChannel();
	assert(pChannel);

	MSG_CL_LOGIN_GATE_SELECT_ACK msgResult;
	msgResult.byResult = MSG_CL_LOGIN_GATE_SELECT_ACK::ERR_EXIST_CONNECT_INFO_RETRY;


	if(!pUser)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_NEWCLIENT_ACK]用户[编号%d]不存在\n", pMsgAns->dwAuthUserID);
		goto laFailed;
	}

	if(pMsgAns->byResult != 0)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_NEWCLIENT_ACK]GateShell用户准备出错[编号%d]\n", pMsgAns->dwAuthUserID);
		goto laFailed;
	}

	if(strcmpi(pUser->GetAccount(), pMsgAns->szID) != 0)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_NEWCLIENT_ACK]用户[本地帐号%s != 传来帐号]不匹配\n",pUser->GetAccount() , pMsgAns->szID);
		goto laFailed;
	}

	//传递GateShell面向Client的 IP Port
	msgResult.byResult		= MSG_CL_LOGIN_GATE_SELECT_ACK::ERR_OK;
	msgResult.dwAuthUserID	= pUser->GetAuthID();
	msgResult.dwSvrPort		= pChannel->GetGateClientSidePort();
	
	__ZERO(msgResult.szSerialKey);

	memcpy	(msgResult.szSerialKey, pUser->GetSerialKey(),				MAX_AUTH_SERIAL_LENGTH* sizeof(BYTE));
	lstrcpyn	(msgResult.szSvrIP,		pChannel->GetGateClientSideIP(),	MAX_IP_LENGTH);

	pUser->Send((BYTE*)&msgResult, sizeof(msgResult));
	//登录成功后，通知GateShell准备NewUser


	return TRUE;


laFailed:
	if(pUser)
	{
		pUser->Send((BYTE*)&msgResult, sizeof(msgResult));
		pUser->Disconnect();
	}

}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_LOGIN_AU(AU_LOGIN_PC_LOGIN_CMD)
{
	MSG_AU_LOGIN_PC_LOGIN_CMD*	pMsgLogin = (MSG_AU_LOGIN_PC_LOGIN_CMD*)pMsg;

	User* pUser = theUserManager.FindUser(pMsgLogin->dwAuthUserID);

	if(!pUser)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_PC_LOGIN_CMD]用户[编号%d]不存在\n", pMsgLogin->dwAuthUserID);
		goto laFailed;
	}

	if(lstrcmpi(pUser->GetAccount(), pMsgLogin->szID) != 0)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_PC_LOGIN_CMD]用户帐号[本地%s != 传来%s]不匹配\n",pUser->GetAccount() , pMsgLogin->szID);
		goto laFailed;
	}

	if(pUser->GetAgentServerID() !=  pMsgLogin->wSvrCode)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_PC_LOGIN_CMD]所在Agent服务器编码[本地%d != 传来%d]不匹配\n",pUser->GetAgentServerID() , pMsgLogin->wSvrCode);
		goto laFailed;
	}

	//未做处理

	return TRUE;


laFailed:
	if(pUser)
		pUser->Disconnect();
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_LOGIN_AU(AU_LOGIN_PC_LOGOUT_CMD		)
{
	MSG_AU_LOGIN_PC_LOGOUT_CMD* pMsgLogout = (MSG_AU_LOGIN_PC_LOGOUT_CMD*)pMsg;

	User* pUser = theUserManager.FindUser(pMsgLogout->dwAuthUserID);

	if(!pUser)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_PC_LOGOUT_CMD]用户[编号%d]不存在\n", pMsgLogout->dwAuthUserID);
		goto laFailed;
	}

	/////////////////////////////////////////////
	//不同的服务线，则不处理
	if(pUser->GetAgentServerID() != pMsgLogout->wSvrCode)
		return TRUE;

	/////////////////////////////////////////////
	if(lstrcmpi(pUser->GetAccount(), pMsgLogout->szAccountID) != 0)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_PC_LOGOUT_CMD]用户帐号[本地%s != 传来%s]不匹配\n",pUser->GetAccount() , pMsgLogout->szAccountID);
		goto laFailed;
	}

	if(pUser->GetAgentServerID() !=  pMsgLogout->wSvrCode)
	{
		MessageOut(LOG_CRITICAL," [AU_LOGIN_PC_LOGOUT_CMD]所在Agent服务器编码[本地%d != 传来%d]不匹配\n",pUser->GetAgentServerID() , pMsgLogout->wSvrCode);
		goto laFailed;
	}

	//pMsgLogout->byLogoutType
	//charLV charType szCharName
#pragma message(__FILE__  "(199) 这里应该传 pUser->Disconnect 原因的协议" )

	pUser->Disconnect();

	return TRUE;

laFailed:
	if(pUser)
		pUser->Disconnect();
}
PACKETHANDLER_SERVER_END();



PACKETHANDLER_SERVER_LOGIN_AU(AU_LOGIN_PC_LOGOUT_ACK	)
{
	//未做处理
}
PACKETHANDLER_SERVER_END();



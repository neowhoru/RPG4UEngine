#include "stdAfx.h"
#include ".\gmcmdparser.h"

GMCmdParser::GMCmdParser(void)
{
}

GMCmdParser::~GMCmdParser(void)
{
}

bool GMCmdParser::Parsing( const char* pszCmd, vector<string>& vecCmd )
{
	if( pszCmd == NULL || (strcmp(pszCmd, "") == 0) )
		return false;

	string strCmd = pszCmd;
	string strHead[MAX_ARGUMENT];
	string strTemp = strCmd;
	int nIdx = 0, nCntCmd = 0;

	//1. 이 문장을 파싱한다.
	for(int i = 0; i < MAX_ARGUMENT; i++)
	{
		nIdx = (int)strCmd.find(' ');
		if( nIdx == string::npos )
		{
			//만일 마지막 문자열이면, 구분자가 없기 때문에 여기로 온다.
			if( strCmd.length() > 0 )
			{
				strHead[i] = strCmd;
				vecCmd.push_back( strHead[i] );
				nCntCmd++;
			}

			break;
		}

		strHead[i] = strCmd.substr( 0, nIdx );
		vecCmd.push_back( strHead[i] );
		strCmd.erase(0, strHead[i].length() + 1 );  // +1를 해준것은 빈칸을 포함시켜서 지워야 하기 때문이다.
		nCntCmd++;
	}

	return true;
}
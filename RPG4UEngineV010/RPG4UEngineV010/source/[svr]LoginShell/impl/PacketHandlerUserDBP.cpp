/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerUserDBP.cpp
创建日期：2008年3月11日
最后更新：2008年3月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerUserDBP.h"
#include "UserDBProxySession.h"
#include <Protocol_Login_DBP.h>
#include <PacketStruct_Login_DBP.h>

#include "User.h"
#include "UserManager.h"
#include <Protocol_ClientLogin.h>
#include <PacketStruct_ClientLogin.h>
#include <EncodeMD5.h>

#include <Protocol_Gate_Login.h>
#include <PacketStruct_Gate_Login.h>


/*////////////////////////////////////////////////////////////////////////
	查询帐号信息之后，返回客户端验证信息
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AUTH_TU(TU_USER_LOGIN_ACK)
{
	MSG_TU_USER_LOGIN_ACK*	pMsgAck = (MSG_TU_USER_LOGIN_ACK*) pMsg;

	MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK		msgResult;
	msgResult.byResult = MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_UNKNOWN;

	User*	pUser = theUserManager.FindUser(pMsgAck->m_dwKey);
	if(!pUser)
	{
		MessageOut(LOG_MIDDLE, "User not Found %d", pMsgAck->m_dwKey);
		goto laEnd;
	}

	//用户不存在
	if(pMsgAck->dwUserGUID == 0)
	{
		msgResult.byResult	= MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_NOTEXIST;
		goto laEnd;
	}

	////////////////////////////////////////
	//密码不匹配
	//md5码比较
	if(pUser->GetPassword() != (LPCTSTR)pMsgAck->szPassWD)
	{
		msgResult.byResult	= MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_PWDNOTMATCH;
		goto laEnd;
	}

	pUser->SetStatus( USER_STATUS_AUTH );

	msgResult.byResult = MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_OK;
	pUser->SetGUID				(pMsgAck->dwUserGUID);
	pUser->SetCharPassword	(pMsgAck->szCharPassWD);
	pUser->SetUserStatus		(pMsgAck->bySts);
	msgResult.szInfo[0] = 0;

	pUser->Send((BYTE*)&msgResult, sizeof(msgResult));
	DISPMSG(" User [%s] Login OK... at %s \n", pUser->GetAccount(), pUser->GetIP());
	return TRUE;

laEnd:
	if(pUser)
	{
		pUser->Send((BYTE*)&msgResult, sizeof(msgResult));
		theUserManager.RemoveUser(pUser, TRUE);
	}
	return FALSE;
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_AUTH_TU(TU_USER_LOGIN_NAK)
{

}
PACKETHANDLER_SERVER_END();


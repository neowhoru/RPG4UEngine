/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerClient.cpp
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
文 件 名：PacketHandlerClient.cpp
创建日期：2007年6月6日
最后更新：2007年6月6日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PacketHandlerClient.h"
#include "User.h"
#include "LoginShell.h"
#include "UserManager.h"
#include "PacketCryptManager.h"
#include "GateChannel.h"
#include "ChannelManager.h"
#include "LoginShellInfo.h"
#include <Protocol_ClientLogin.h>
#include <PacketStruct_ClientLogin.h>

#include <Protocol_Login_DBP.h>
#include <PacketStruct_Login_DBP.h>
#include <Protocol_Gate_Login.h>
#include <PacketStruct_Gate_Login.h>
#include <EncodeMD5.h>

//VOID PacketHandlerClient::OnCL_LOGIN_VERSION_CHECK_SYN( User *pUser, MSG_BASE *pMsg, WORD wSize )
//{
//}
//

#pragma message(__FILE__  "(35) CheckUserAccount 此处检查用户登录帐号的合法性 " )
BOOL CheckUserAccount(LPCSTR szAccount)
{
	szAccount;
	return TRUE;
}

BOOL CheckUserPassword(LPCSTR )
{
	return TRUE;
}

#pragma message(__FILE__  "(47) PACKETHANDLER_SERVER_CL_LOGIN CL_LOGIN_GATESTATUS_LIST_SYN 有待重新封装GateChannel Group的管理... " )
INT GroupIP2GroupIndex(DWORD dwID)
{
	return 0;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_CL_LOGIN(CL_LOGIN_ACCOUNT_LOGIN_SYN)
{
	MSG_CL_LOGIN_ACCOUNT_LOGIN_SYN*	pMsgAuth = (MSG_CL_LOGIN_ACCOUNT_LOGIN_SYN*)pMsg;

	MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK			msgResult;
	MSG_TU_USER_LOGIN_SYN	msgLogin;


	//检测帐号合法性
	if(!CheckUserAccount(pMsgAuth->szID))
	{
		msgResult.byResult = MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_ID;
		goto laEnd;
	}

	//在登录中...
	if(theUserManager.FindUser(pMsgAuth->szID))
	{
		msgResult.byResult = MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_LOGINING;
		goto laEnd;
	}

	TCHAR szPassWD[MAX_PASSWORD_LENGTH];// = pUser->GetPassword();
	//检测密码合法性
	thePacketCryptManager.PacketDecode( (BYTE *)pMsgAuth->szPasswd,
							  MAX_PASSWORD_LENGTH,
							  (BYTE *)szPassWD,
							  pUser->GetAuthSequence() );

	if(!CheckUserPassword(szPassWD))
	{
		msgResult.byResult = MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_PW;
		goto laEnd;
	}

	pUser->SetPassword (EncodeMD5::GetMD5(szPassWD));
	
	msgLogin.m_dwKey	= pUser->GetAuthID();
	strcpy(msgLogin.szID, pMsgAuth->szID);
	//到数据库查询帐号信息
	if(!theLoginShell.SendToAccountDBProxy(&msgLogin, sizeof(msgLogin)))
	{
		msgResult.byResult = MSG_CL_LOGIN_ACCOUNT_LOGIN_ACK::ERR_WRONG_DB;
		goto laEnd;
	}

	goto laOK;

laEnd:
	pUser->Send((BYTE*)&msgResult, sizeof(msgResult));
	//pUser->Disconnect();
	return FALSE;

laOK:
	pUser->SetAccountLower(pMsgAuth->szID);
	theUserManager.AddUser(pUser,TRUE);	//把名字增加进去
}
PACKETHANDLER_SERVER_END();




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_CL_LOGIN(CL_LOGIN_GATESTATUS_LIST_SYN)
{
	MSG_CL_LOGIN_GATESTATUS_LIST_ACK			msgSvrList;

	UINT			n;
	GateChannel*		pChannel;

	////////////////////////////////////////
	//处理服务线名称
	DWORD dwSvrCnt = min(MAX_SERVER_AMOUNT, theChannelManager.GetChannelCount());
	msgSvrList.bySvrCnt		= (BYTE)dwSvrCnt;

	memset(msgSvrList.szServerStatusList, 0, sizeof(msgSvrList.szServerStatusList));

	for(n=0; n<dwSvrCnt; n++)
	{
		pChannel = theChannelManager.GetChannelAt(n);
		assert(pChannel);

		//处理服务线状态信息
//#ifdef _DEBUG
//#pragma message(__FILE__  "(185) ...... 测试数据 pChannel->GetServerStatus + 10 " )
//		static BYTE sStatus = 0;
//		sStatus += 5;
//		msgSvrList.szServerStatusList[n] = sStatus;
//#else
		msgSvrList.szServerStatusList[n] = pChannel?(BYTE)pChannel->GetServerStatus() : 0;
//#endif
	}

	pUser->Send((BYTE*)&msgSvrList,	(WORD)msgSvrList.GetDataSize());//sizeof(msgSvrList));
}
PACKETHANDLER_SERVER_END();



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_CL_LOGIN(CL_LOGIN_GATENAME_LIST_SYN)
{
	//MSG_CL_LOGIN_GATESTATUS_LIST_ACK		msgSvrList;
	MSG_CL_LOGIN_GATENAME_LIST_ACK	msgNameList;

	UINT			n,
					nSize;
	GateChannel*		pChannel;
	BYTE*			pBuffer;
	LPCTSTR		szText;

	/////////////////////////////////////////////////////////
	//处理服务端组名称信息，
	DWORD dwGroupCnt = min(MAX_GROUP_CNT, theLoginShellInfo.m_arGroupNames.size());
	memset(msgNameList.szBuffer, 0, sizeof(msgNameList.szBuffer));
	pBuffer		= msgNameList.szBuffer;

	if(dwGroupCnt)
	{

		for(n=0; n<dwGroupCnt; n++)
		{	
			StringHandle	sName = theLoginShellInfo.m_arGroupNames[n];
			nSize		= sName.Length() + 1;
			memcpy(pBuffer, sName.GetBuffer() , nSize * sizeof(TCHAR));

			pBuffer	+= nSize;
		}
	}
	msgNameList.byGroupCnt = (BYTE)dwGroupCnt;

	////////////////////////////////////////
	//处理服务线名称
	DWORD dwSvrCnt				= min(MAX_SERVER_AMOUNT, theChannelManager.GetChannelCount());
	msgNameList.byServerCnt	= (BYTE)dwSvrCnt;

	for(n=0; n<dwSvrCnt; n++)
	{
		pChannel = theChannelManager.GetChannelAt(n);
		assert(pChannel);
		*pBuffer	=  (BYTE)GroupIP2GroupIndex(pChannel->GetGroupID());
		pBuffer++;

		szText = pChannel->GetChannelName();
		nSize = lstrlen(szText) + 1;
		memcpy(pBuffer, szText, nSize*sizeof(TCHAR));
		pBuffer	+= nSize;
	}
	nSize = pBuffer-msgNameList.szBuffer;

	pUser->Send((BYTE*)&msgNameList,	(WORD)msgNameList.GetDataSize(nSize));//sizeof(msgNameList));


	///////////////////////////////////////////
	//再派发状态数据
	MSG_CL_LOGIN_GATESTATUS_LIST_SYN msgStatus;
	PACKETHANDLER_METHOD_CALL	(CL_LOGIN_GATESTATUS_LIST_SYN
										,CL_LOGIN
										,&msgStatus);
	//OnCL_LOGIN_GATESTATUS_LIST_SYN(pUser,&msgStatus,sizeof(msgStatus));
}
PACKETHANDLER_SERVER_END();


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
PACKETHANDLER_SERVER_CL_LOGIN(CL_LOGIN_GATE_SELECT_SYN)
{
	MSG_CL_LOGIN_GATE_SELECT_SYN*	pMsgSelect = (MSG_CL_LOGIN_GATE_SELECT_SYN*) pMsg;

	MSG_AU_LOGIN_NEWCLIENT_SYN	msgNewClient;

	//设置所在Agent服务器编码
	GateChannel* pChannel = theChannelManager.GetChannelAt(pMsgSelect->bySelectIndex);
	if(!pChannel)
	{
		MessageOut(LOG_CRITICAL, "GateChannel[%d#] 不存在...", pMsgSelect->bySelectIndex);
		goto laEnd;
	}

	//////////////////////////////
	//同一服务线，则不处理了
	if(pChannel->GetChannelID() == pUser->GetAgentServerID())
		return TRUE;

	//////////////////////////////
	//退出之前的服务线
	if(!pUser->LogoutAtGate(TRUE))
		goto laEnd;


	////////////////////////////////
	//记录用户的在ChannelID
	pUser->SetAgentServerID( pChannel->GetChannelID() );
	pUser->SetStatus( USER_STATUS_SVRSELECT );


	////////////////////////////////
	//先向GateShell发出NEWCLIENT请求

	msgNewClient.dwAuthUserID		= pUser->GetAuthID();
	msgNewClient.dwUserGuid			= pUser->GetGUID();
	msgNewClient.dwAuthSequence	= pUser->GetAuthSequence();
	msgNewClient.wSvrCode			= (WORD)pUser->GetAgentServerID();
	msgNewClient.byBillingType		= pUser->GetBillingType();
	msgNewClient.dwReservedValue	= 0;

	lstrcpy(msgNewClient.szID,					pUser->GetAccount());
	lstrcpy(msgNewClient.szClientIP,			pUser->GetIP());
	lstrcpyn(msgNewClient.szBillingInfo,	pUser->GetBillingInfo(),	MAX_INFO_LENGTH);
	lstrcpyn(msgNewClient.szCharPassword,	pUser->GetCharPassword(),	MAX_MD5_BUF_LENGTH);
	
	//////////////////////////
	__ZERO(msgNewClient.szSerialKey);
	__ZERO(msgNewClient.szMD5Key);
	memcpy(msgNewClient.szSerialKey,		pUser->GetSerialKey(),	MAX_AUTH_SERIAL_LENGTH	*sizeof(BYTE));
	memcpy(msgNewClient.szMD5Key,			pUser->GetMD5Key(),		MAX_MD5_BUF_LENGTH		*sizeof(BYTE));

	pChannel->SendToGateServer(&msgNewClient, sizeof(msgNewClient));

	return TRUE;

laEnd:
	MSG_CL_LOGIN_GATE_SELECT_ACK msgResult;
	msgResult.byResult = MSG_CL_LOGIN_GATE_SELECT_ACK::ERR_CANNT_CONNECT_SELECTED_SERVER;
	pUser->Send((BYTE*)&msgResult, sizeof(msgResult));
	pUser->Disconnect();
}
PACKETHANDLER_SERVER_END();

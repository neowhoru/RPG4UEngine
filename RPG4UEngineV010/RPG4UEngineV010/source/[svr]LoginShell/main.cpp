
#include "stdafx.h"
#include "LoginShellStartUp.h"
#include "ServerRunning.h"
#include "LoginShellInfo.h"

#ifdef _DEBUG
#define SETTING_FILE		_T("Setting\\LoginShellD.ini")	
#elif _PRERELEASE
#define SETTING_FILE		_T("Setting\\LoginShellP.ini")	
#else
#define SETTING_FILE		_T("Setting\\LoginShell.ini")	
#endif

#pragma message(__FILE__ "(7) _SERVER_MAIN = " _PROJNAME " [LoginShellStartUp,ServerRunning]")
_SERVER_MAIN	(_PROJNAME
					,LoginShellStartUp
					,ServerRunning
					,LoginShellInfo
					,SETTING_FILE);


//__SERVICE_RUNNING(_PROJNAME, gs_ServiceInst);
//
//int APIENTRY WinMain(HINSTANCE hInstance
//						  ,HINSTANCE /*hPrevInstance*/
//						  ,LPSTR     lpCmdLine
//						  ,int       /*nCmdShow*/)
//{
//
//	return __WinMain	<LoginShellStartUp
//							,LoginShellInfo
//							,ServerRunning>(_PROJNAME
//												,_T("Setting\\LoginShell.ini")
//												,hInstance
//												,lpCmdLine);
//}



#pragma once

#include <hash_map>

class Zone;

typedef stdext::hash_map<DWORD, Zone*>		ZONE_MAP;
typedef ZONE_MAP::iterator					ZONE_MAP_ITER;
typedef std::pair<DWORD, Zone*>				ZONE_MAP_PAIR;

class ZoneManager
{
public:
	ZoneManager(void);
	~ZoneManager(void);

	VOID					Init();
	VOID					Release();

	VOID					AddZone( DWORD dwZoneKey, Zone *pZone );
	VOID					RemoveZone( DWORD dwZoneKey );
	Zone*					FindZone( DWORD dwZoneKey );

private:
	ZONE_MAP				m_mapZones;
};

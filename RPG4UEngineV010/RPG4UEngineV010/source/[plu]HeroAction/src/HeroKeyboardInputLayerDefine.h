/*////////////////////////////////////////////////////////////////////////
文 件 名：HeroKeyboardInputLayerDefine.h
创建日期：2008年5月21日
最后更新：2008年5月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __HEROKEYBOARDINPUTLAYERDEFINE_H__
#define __HEROKEYBOARDINPUTLAYERDEFINE_H__
#pragma once

enum SPECIAL_MOVE
{
	SPECIAL_MOVE_FORWORD =0,
	SPECIAL_MOVE_BACKWORD,
	SPECIAL_MOVE_LEFT,
	SPECIAL_MOVE_RIGHT,
	SPECIAL_MOVE_MAX,
};


#endif //__HEROKEYBOARDINPUTLAYERDEFINE_H__
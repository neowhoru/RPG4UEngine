/*////////////////////////////////////////////////////////////////////////
文 件 名：heroarcherkeyboardinputlayer.cpp
创建日期：2007年11月4日
最后更新：2007年11月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "AssassinKeyboardInputHandler.h"
#include "HeroKeyboardInputLayer.h"
#include "HeroMouseInputLayer.h"
#include "HeroActionInput.h"
#include "Item.h"
#include "Hero.h"
#include "MapObject.h"
#include "GameUtil.h"
#include "InputLayer.h"
#include "Camera.h"
#include "MouseHandler.h"
#include "VUICursorManager.h"
#include "TileWorldClient.h"
#include "PathHandler.h"
#include "HeroTipLayer.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "SceneDefine.h"
#include "QuickContainer.h"
#include "SummonSkillContainer.h"

using namespace input;
using namespace scene;


#pragma message(__FILE__  "(42) @@@@@@@ 这里增加宠物控制处理  " )
///暂时放在这
	enum PACKET_STATUS_SUMMONTARGET                        
	{ 
		PACKET_STATUS_SUMMONTARGET_NONE				= 0, 
		PACKET_STATUS_SUMMONTARGET_ATTACK			= 1,        
		PACKET_STATUS_SUMMONTARGET_FOLLOW			= (2 << 1), 
		PACKET_STATUS_SUMMONTARGET_DELEGATE_ATTACK	= (2 << 2), 
		PACKET_STATUS_SUMMONTARGET_DELEGATE_DEFENSE	= (2 << 3), 
		PACKET_STATUS_SUMMONTARGET_DESTROY			= (2 << 4), 
	};

	enum eSUMMON_TARGET_POS
	{
		SUMMON_TARGET_PIC_GAUGE = 0, 
		SUMMON_TARGET_PIC_MP_GAUGE, 
		SUMMON_TARGET_SKILL_EFFECT1, 
		SUMMON_TARGET_SKILL_EFFECT2, 
		SUMMON_TARGET_SKILL_EFFECT3, 
		SUMMON_TARGET_SKILL_EFFECT4, 
		SUMMON_TARGET_SKILL_EFFECT5, 
		SUMMON_TARGET_SKILL_EFFECT6, 
		SUMMON_TARGET_SKILL_EFFECT7, 
		SUMMON_TARGET_SKILL_EFFECT8, 
		SUMMON_TARGET_SKILL_EFFECT9, 
		SUMMON_TARGET_SKILL_EFFECT10, 

		SUMMON_TARGET_TEXT_INFO,
		SUMMON_TARGET_PIC_MON,
	
		SUMMON_TARGET_CHECK_ATTACK,
		SUMMON_TARGET_CHECK_FOLLOW,
		SUMMON_TARGET_CHECK_DELEGATE_ATTACK,
		SUMMON_TARGET_CHECK_DELEGATE_DEFENSE,
		SUMMON_TARGET_CHECK_DESTROY,

		Dialog_Max
	};

#define MAX_SUMMON_COMMAND						5
#define MAX_SUMMON_TARGETDLG_SKILL_EFFECT 10


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(AssassinKeyboardInputHandler, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
AssassinKeyboardInputHandler::AssassinKeyboardInputHandler()
{
}
AssassinKeyboardInputHandler::~AssassinKeyboardInputHandler()
{
}

void AssassinKeyboardInputHandler::_ProcessStyleQuickKey()
{
}

BOOL AssassinKeyboardInputHandler::ProcessOptionKey()
{

	if(_SUPER::ProcessOptionKey())
		return TRUE;

	//if( theHero.GetPlayerCookie().GetCharType() == PLAYERTYPE_STABBER )

	if( theInputLayer.IsPressShiftKey() )
	{
		BOOL	bIsCommand = FALSE;
		int		iCommand = 0;

		for( int Index = 0 ; Index < MAX_SUMMON_COMMAND ; ++Index )
		{
			if( theInputLayer.GetState((eKEY_CODE) (KEYC_1 + Index), KS_DOWN ) )
			{
				iCommand = SUMMON_TARGET_CHECK_ATTACK + Index;
				bIsCommand = TRUE;
				break;
			}
		}

		if( bIsCommand )
		{
	assert(0 && " AssassinKeyboardInputHandler..ProcessOptionKey ");
#pragma message(__FILE__  "(82) AssassinKeyboardInputHandler..ProcessOptionKey  " )


		}
	}


	if( theInputLayer.IsPressCtrlKey() )
	{
		BOOL	bIsCommand = FALSE;
		int		iCommand = 0;

		for( int Index = 0 ; Index < MAX_SUMMON_SLOT_NUM; ++Index )
		{
			if( theInputLayer.GetState((eKEY_CODE) (KEYC_1 + Index), KS_DOWN ) )
			{
				iCommand =   Index;
				bIsCommand = TRUE;
				break;
			}
		}

		if( bIsCommand )
		{

		}
	}

	return FALSE;
}

//------------------------------------------------------------------------------
BOOL AssassinKeyboardInputHandler::ProcessKeyboardMove()
{
	//DWORD dwCurTick=g_CurTime;
	//DWORD dwCurTick=g_CurTime;//g_pRendererPtr->x_Clock.GetCurrentTick();

	BOOL bLeftStepPressed = FALSE;
	BOOL bRightStepPressed = FALSE;
	BOOL bFowardPressed = FALSE;

	BOOL bBackwardPressed = FALSE;
	BOOL bLeftTurnPressed = FALSE;
	BOOL bRightTurnPressed = FALSE;

	//胶其既 捞悼
	BOOL bLeftSpecialMovePressed = FALSE;
	BOOL bRightSpecialMovePressed = FALSE;
	BOOL bFowardSpecialMovePressed = FALSE;
	BOOL bBackwardSpecialMovePressed = FALSE;

	theHero.ResetMoveFlag();


	if (theHero.CannotMove())
		return FALSE;


	PLAYER_ACTION& qAction =m_pHeroActionInput-> GetQueueAction();

	m_pHeroActionInput->SetKeyBoardMoveCheck (FALSE);

	if (theInputLayer.GetState(KEYC_A, KS_KEY))
	{
		bLeftTurnPressed = TRUE;
	}

	else if (theInputLayer.GetState(KEYC_D, KS_KEY))
	{
		bRightTurnPressed = TRUE;			
	}


	Character	*pChar = (Character *)theObjectManager.GetObject( m_pHeroActionInput->GetCurrentTarget() );
	BOOL bRange=FALSE;
	if(pChar)
	{
		Vector3D vPos = theHero.GetPosition();
		Vector3D vDest = pChar->GetPosition();
		Vector3D vDiff = vDest - vPos;

		if(vDiff.Length() <= m_pHeroActionInput->GetAttackActionDistance())
		{
			bRange=TRUE;
		}
	}

	BOOL bUse=TRUE;

	if (theHero.GetWeaponKind() == WEAPONTYPE_ETHERWEAPON) 
	{
		bUse=FALSE;
	}



	if( bUse
		&&	(	theHero.GetCurrentState() == STATE_ATTACK
			||	theHero.GetNextState() == STATE_ATTACK
			||	(	m_pHeroActionInput->IsAutoAttack()
				&&	pChar
				&&	GameUtil::GetRelationOfHero( pChar ) == RELATION_ENEMY
				&&	pChar->IsDead()==FALSE&&bRange)
			))
	{
		if(	m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD]==FALSE
			&&	theInputLayer.GetState(KEYC_W, KS_DOWN))
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD]=TRUE;

			bFowardSpecialMovePressed = TRUE;
		}
		else if (m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD]==FALSE&&theInputLayer.GetState(KEYC_S,KS_DOWN))
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD]=TRUE;

			bBackwardSpecialMovePressed =TRUE;
		}
		else if (m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT]==FALSE&&theInputLayer.GetState(KEYC_Q,KS_DOWN))
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT]=TRUE;

			bLeftSpecialMovePressed = TRUE;

		}
		else if (m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT]==FALSE&&theInputLayer.GetState(KEYC_E,KS_DOWN))
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT]=TRUE;
			bRightSpecialMovePressed = TRUE;
		}

	}
	else if(qAction.ActionID != ACTION_SPECIAL_MOVE)
	{
		if (theInputLayer.GetState(KEYC_W,KS_KEY))
		{
			bFowardPressed = TRUE;

			m_pHeroActionInput->SwitchAutoMove(FALSE);
			m_pHeroActionInput->SetFollowState( FALSE );
		}
		else if (theInputLayer.GetState(KEYC_S,KS_KEY))
		{
			bBackwardPressed = TRUE;

			m_pHeroActionInput->SwitchAutoMove(FALSE);
			m_pHeroActionInput->SetFollowState( FALSE );
		}

		if (theInputLayer.GetState(KEYC_Q,KS_KEY))
		{
			bLeftStepPressed = TRUE;	
		}
		else if (theInputLayer.GetState(KEYC_E,KS_KEY))
		{
			bRightStepPressed = TRUE;
		}

	}

	if (theInputLayer.GetState(KEYC_W,KS_UP))
	{
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD]=FALSE;
	}

	if (theInputLayer.GetState(KEYC_Q,KS_KEY))
	{
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD]=FALSE;
	}

	if (theInputLayer.GetState(KEYC_E,KS_KEY))
	{
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT]=FALSE;
	}

	if (theInputLayer.GetState(KEYC_W,KS_KEY))
	{
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT]=FALSE;
	}


	if (bLeftSpecialMovePressed )
	{
		qAction.ActionID			=ACTION_SPECIAL_MOVE;
		qAction.SPECIALMOVE.bType=MOVETYPE_TUMBLING_LEFT;

		return FALSE;
	}
	else if (bRightSpecialMovePressed )
	{
		qAction.ActionID=ACTION_SPECIAL_MOVE;
		qAction.SPECIALMOVE.bType=MOVETYPE_TUMBLING_RIGHT;
		return FALSE;
	}
	else if (bFowardSpecialMovePressed)
	{
		qAction.ActionID=ACTION_SPECIAL_MOVE;
		qAction.SPECIALMOVE.bType=MOVETYPE_TUMBLING_FRONT;
		return FALSE;
	}	
	else if (bBackwardSpecialMovePressed)
	{
		qAction.ActionID=ACTION_SPECIAL_MOVE;
		qAction.SPECIALMOVE.bType=MOVETYPE_TUMBLING_BACK;
		return FALSE;
	}


	/////////////////////////////////////////
	if (	bRightStepPressed 
		|| bLeftStepPressed 
		|| bFowardPressed
		|| bBackwardPressed 
		|| bLeftTurnPressed 
		|| bRightTurnPressed) 
	{

		m_pHeroActionInput->InitAction();
		theSkillQueueManager.ClearAll();
		m_pHeroActionInput->SwitchAreaSkill(FALSE);
		m_pHeroActionInput->SetKeyBoardMoveCheck (TRUE);

		if (	m_pHeroActionInput->IsIdleState() 
			|| theHero.GetCurrentState() == STATE_MOVE )
		{
			theHero.StopMove();
			theHero.SetNextState(STATE_KEYBOARDMOVE,g_CurTime);
		}

		if (bLeftTurnPressed) 
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_LEFTTURN);
		}
		else if (bRightTurnPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTTURN);
		}

		if (bFowardPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_FOWARD);
		}
		else if (bBackwardPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_BACKWARD);
		}

		if (bRightStepPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTSTEP);
		}
		else if (bLeftStepPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_LEFTSTEP);
		}

		return TRUE;
	}


	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：GameAssistantHandler.cpp
创建日期：2007年12月26日
最后更新：2007年12月26日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameAssistantHandler.h"
#include "HeroActionInput.h"
#include "Item.h"
#include "Hero.h"
#include "MapObject.h"
#include "GameUtil.h"
#include "ConstTextRes.h"

#include "InputLayer.h"
#include "Camera.h"
#include "MouseHandler.h"
#include "VUICursorManager.h"
#include "TileWorldClient.h"
#include "PathHandler.h"
#include "HeroTipLayer.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "SceneDefine.h"
#include "QuickContainer.h"
#include "GameParameter.h"
#include "HeroParty.h"
#include "TradeDialog.h"
#include "VendorDialog.h"
#include "PacketInclude.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameAssistantHandler, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameAssistantHandler::GameAssistantHandler()
{
	m_bFollowPlayer		= FALSE;
	m_dwFollowPlayerKey	= 0;
}

GameAssistantHandler::~GameAssistantHandler()
{
}


//------------------------------------------------------------------------------
void GameAssistantHandler::ProcessAutoAttack()
	{
	//TCHAR	szMessage[MAX_UITEXT_LENGTH];

	
	if( theHero.IsDead() && m_pHeroActionInput->IsAutoAttack() )
	{
		m_pHeroActionInput->SwitchAutoAttack(FALSE);

		m_pHeroActionInput->SetCurrentTarget( 0 );
		return;
	}

	if (m_pHeroActionInput->IsCannotAttack())
	{
		m_pHeroActionInput->SwitchAutoAttack(FALSE);
		return;
	}

	switch (theHero.GetCurrentState())
	{
	case STATE_SKILL:
	case STATE_DAMAGE:
	case STATE_DOWN:
	case STATE_AIR:
	case STATE_DEATH:
	case STATE_SIT:
	case STATE_SPECIALMOVE:
		{
			return;
		}
		break;
	}
	

	if (	m_pHeroActionInput->IsAutoAttack() 
		&& !m_pHeroActionInput->IsAreaSkill())
	{
		PLAYER_ACTION	action;
		Vector3D			vTargetPos;

		ZeroMemory(&action,sizeof(action));

		DWORD			dwTarget = m_pHeroActionInput->GetCurrentTarget();
		Character *	pChr		= (Character*)theObjectManager.GetObject(dwTarget);
		if (pChr)
		{
			if (GameUtil::GetRelationOfHero(pChr) != RELATION_ENEMY)
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
				return;	
			}


			//	 0.5
			if (theGameOption.GetAutoMove())
			{
				if(	theHero.IsMoving() 
					&& theHero.GetCurrentState() == STATE_MOVE )
				{
					if( (g_CurTime - theHero.GetStartMoveTime()) >= UPDATE_MOVE_POSITION_TIME )
					{
						theHero.SetStartMoveTime( g_CurTime );

						DWORD			dwTarget = m_pHeroActionInput->GetCurrentTarget();
						Character *	pChr		= (Character *)theObjectManager.GetObject(dwTarget);
						if (pChr)
						{
							vTargetPos = GameUtil::FindAttackTargetPos(pChr);
							Vector3D vDistance = vTargetPos - theHero.GetPosition();
							float fDistance = vDistance.Length();

							if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
							{
								action.ActionID				= ACTION_ATTACK;
								action.ATTACK.dwTargetID	= m_pHeroActionInput->GetCurrentTarget();
								m_pHeroActionInput->PushQueueAction(action);
								m_pHeroActionInput->MoveToAction	(vTargetPos
                                                         ,pChr->GetPathExplorer()->GetTile());
								return;
							}
						}
					}
				}
			}


			if( m_pHeroActionInput->GetQueueAction().ActionID != ACTION_IDLE ) // 促澜悼累 己傍  
			{
				return;
			}

			if( m_pHeroActionInput->IsShootingMode() == FALSE )
			{
				if( m_pHeroActionInput->GetCurrentTarget() )
				{
					action.ActionID				= ACTION_ATTACK;
					action.ATTACK.dwTargetID	= m_pHeroActionInput->GetCurrentTarget();
					m_pHeroActionInput->PushQueueAction(action);


					if (theHero.IsNormalRangedAttack())
					{
						vTargetPos = pChr->GetPosition();
					}
					else
					{
						vTargetPos = GameUtil::FindAttackTargetPos(pChr);
					}
					Vector3D vDistance = vTargetPos - theHero.GetPosition();


					float fDistance = vDistance.Length();
					if (theGameOption.GetAutoMove())
					{
						if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
						{
							if(theHero.GetCurrentState() == STATE_SPECIALMOVE||theHero.GetPreState() == STATE_SPECIALMOVE)//漂荐 捞悼吝俊 荤芭府 郭绢唱搁 坷配绢琶 秒家 
							{
								m_pHeroActionInput->SwitchAutoAttack( FALSE );
							}
							else
								m_pHeroActionInput->MoveToAction(vTargetPos,pChr->GetPathExplorer()->GetTile());		
							return;
						}
					}
					else
					{
						if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
						{
							m_pHeroActionInput->SwitchAutoAttack( FALSE );
							OUTPUTCHAT(TEXTRES_SKILL_FAIL_NOT_ENOUGH_DISTANCE );
							return;
						}

					}

					return;
				}
			}
			else
			{
				if (!pChr->CanBeAttackTarget())
					return;

				if ( m_pHeroActionInput->GetCurrentTarget() == pChr->GetObjectKey())
				{
					if (theHero.GetStabberShotDelay() > STABBER_SHOT_DELAY_MARGIN)
					{
						return;
					}

					Vector3D vPos = theHero.GetPosition();
					Vector3D vDest = pChr->GetPosition();
					Vector3D vDiff = vDest - vPos;

					if (vDiff.Length() >= theHero.GetAttackRange())
					{
						if( theHero.IsMoving() == FALSE )
						{
							action.ActionID				= ACTION_IDLE;
							action.ATTACK.dwTargetID	= m_pHeroActionInput->GetCurrentTarget();
							m_pHeroActionInput->PushQueueAction( action);

							if (theGameOption.GetAutoMove())
							{
								if (m_pHeroActionInput->MoveToAction(vDest,pChr->GetPathExplorer()->GetTile()) == FALSE)
								{
									;
								}
							}
						}
						else if( m_pHeroActionInput->IsKeyBoardMoveCheck())
							return;

						return;
					}

					if( theHero.IsMoving() )
					{
						// STOP
						theHero.StopMove();
						theHero.StopAtServer();

						//MSG_CG_SYNC_STOP_SYN packet;
						//packet.m_byCategory = CG_SYNC;
						//packet.m_byProtocol = CG_SYNC_STOP_SYN;
						//packet.vCurPos     = theHero.GetPosition();
						//theHero.SendPacket(&packet,sizeof(MSG_CG_SYNC_STOP_SYN));
					}

					m_pHeroActionInput->StabberShot();

					return;
				}
			}
		}
		m_pHeroActionInput->SwitchAutoAttack(FALSE);

	}
}

BOOL GameAssistantHandler::ProcessUserAction( DWORD dwSkillid )
{
	//TCHAR	szMessage[MAX_UITEXT_LENGTH];

	if( singleton::ExistHero() == NULL )
	{
		return FALSE;
	}


	if( GetFollowState() )
	{
		SetFollowState( FALSE );
	}

	DWORD	dwActionCode = dwSkillid -  PLAYERACTION_SKILLCODE_FIRST;
	switch( dwActionCode )
	{
	///自动攻击
	case PLAYERACTION_AUTO_ATTACK:
		{
			Object	*pObj = theObjectManager.GetObject( m_pHeroActionInput->GetCurrentTarget() );
			if( pObj )
			{
				Character	*pChar = (Character *)pObj;
				if( GameUtil::GetRelationOfHero( pChar ) == RELATION_ENEMY )
				{

					if(m_pHeroActionInput->ProcessTargetClick( pChar ) == FALSE )
					{
						return FALSE;
					}
				}
				else
				{
					OUTPUTCHAT(TEXTRES_CANNOT_ATTACK_TARGET);	
				}
			}
			else
			{
				OUTPUTCHAT(TEXTRES_NOT_SELECTED_PLAYER);	
			}
		}

		break;


		///////////////////////////////////////////////
	case PLAYERACTION_PICK_ITEM:
		if( FALSE == OnSpacePushItem() )
		{
			return FALSE;
		}
		break;

		///////////////////////////////////////////////
	case PLAYERACTION_FOLLOW_PLAYER:
		{
			DWORD	dwTarget = m_pHeroActionInput->GetCurrentTarget();
			if( dwTarget )
			{
				SetFollowState( TRUE, dwTarget );
			}
			else
			{
				OUTPUTCHAT(TEXTRES_NOT_SELECTED_PLAYER );
			}

		}
		break;

		///////////////////////////////////////////////
	case PLAYERACTION_SIT_OR_STAND:
		{
			MSG_CG_STATUS_CONDITION_CHANGE_SYN	Packet;
			Packet.m_byCategory = CG_STATUS;
			Packet.m_byProtocol = CG_STATUS_CONDITION_CHANGE_SYN;		

			if( theHero.GetCurrentState() == STATE_SIT )
				Packet.m_byConditionType	= CHAR_CONDITION_STANDUP;
			else
				Packet.m_byConditionType	= CHAR_CONDITION_SITDOWN;

			if( !theHero.SendPacket( &Packet, sizeof(MSG_CG_STATUS_CONDITION_CHANGE_SYN) ) )
			{
				ASSERT(!"Net Failed\n");
				return FALSE;
			}


		}
		break;


	case PLAYERACTION_TRADE:
		{
			theTradeDialog.RequestTrade(m_pHeroActionInput->GetCurrentTarget());
		}
		break;


	case PLAYERACTION_VENDOR:
		{
			theVendorDialog.OpenVendor();
		}

		break;

	case PLAYERACTION_GUILD_INVITE:
		{
		}
		break;


	case PLAYERACTION_PARTY_INVITE:
		{
			theHeroParty.Invite(theHeroActionInput.GetCurrentTarget());
		}
		break;

	case PLAYERACTION_PARTY_LEAVE:
		{
			theHeroParty.LeaveHero();

		}
		break;


	case PLAYERACTION_NORMAL_ATTACK:
		{
			assert(0 && " case PLAYERACTION_NORMAL_ATTACK ");
#pragma message(__FILE__  "(1560) case PLAYERACTION_NORMAL_ATTACK  " )

			Object	*pObj = theObjectManager.GetObject( m_pHeroActionInput->GetCurrentTarget() );
			if( pObj )
			{
				Character	*pChar = (Character *)pObj;
				if( GameUtil::GetRelationOfHero( pChar ) == RELATION_ENEMY )
				{
					if(m_pHeroActionInput->ProcessTargetClick( pChar, FALSE ) == FALSE )
					{
						return FALSE;
					}
				}
				else
				{
					OUTPUTCHAT(TEXTRES_CANNOT_ATTACK_TARGET);
				}
			}
			else
			{
				OUTPUTCHAT(TEXTRES_NOT_SELECTED_PLAYER);
			}
		}

		break;

	case PLAYERACTION_FRIEND_ADD:
		{
/*
			Object	*pObj = theObjectManager.GetObject( m_pHeroActionInput->GetCurrentTarget() );
			if( pObj )
			{
				Character	*pChar = (Character *)pObj;
				if( GameUtil::GetRelationOfHero( pChar ) == RELATION_FRIEND )
				{
					_CallBack_Friend_Add( true, pChar->GetName() );
				}
				else
				{
					//	模备肺 眠啊且 荐 绝绰 措惑涝聪促.
					g_InterfaceManager.GetUIText( TEXTRES_CANNOT_ADD_FRIEND, szMessage, MAX_UITEXT_LENGTH );
					OUTPUTCHAT(OUTPUTMSG_CHAT, szMessage );
				}
			}
			else
			{
				//	急琶等 敲饭捞绢啊 绝嚼聪促.
				g_InterfaceManager.GetUIText( TEXTRES_NOT_SELECTED_PLAYER, szMessage, MAX_UITEXT_LENGTH );
				OUTPUTCHAT(OUTPUTMSG_CHAT, szMessage );
			}
*/
		}
		break;


	default:
		return FALSE;
		break;
	}

	return TRUE;
}


BOOL GameAssistantHandler::ProcessUserEmotion( DWORD dwSkillid )
{
	if( singleton::ExistHero() == NULL )
	{
		return FALSE;
	}

	if( GetFollowState() )
	{
		SetFollowState( FALSE );
	}

	MSG_CG_STATUS_EMOTION_SYN	SendPacket;
	ZeroMemory(&SendPacket,sizeof(SendPacket));
	SendPacket.m_dwKey			= theGeneralGameParam.GetUserID();
	SendPacket.m_byCategory		= CG_STATUS;
	SendPacket.m_byProtocol		= CG_STATUS_EMOTION_SYN;
	SendPacket.m_byEmotionType	= (BYTE)(dwSkillid - EMOTION_START_CODE);
	if( FALSE == theHero.SendPacket( &SendPacket, sizeof(MSG_CG_STATUS_EMOTION_SYN) ) )
	{
		ASSERT(!"Net Failed");
		return FALSE;
	}

	return TRUE;
}


BOOL GameAssistantHandler::SetFollowState( BOOL bFlag, DWORD dwPlayerKey )
{
	BOOL	preState = m_bFollowPlayer;
	m_bFollowPlayer = bFlag;

	OUTPUTCHATIF(TEXTRES_SET_FOLLOWPLAYER_STOP 
					,preState && m_bFollowPlayer == FALSE);

	if( dwPlayerKey )
	{
		Object	*pObj = theObjectManager.GetObject( dwPlayerKey );
		if( pObj )
		{
//			if( pObj->GetObjectCookie().GetObjectType() == PLAYER_OBJECT )
			{
				Character	*pChar = (Character *)pObj;
				if( GameUtil::GetRelationOfHero( pChar ) == RELATION_FRIEND )
				{
					m_dwFollowPlayerKey	= dwPlayerKey;
					m_dwFollowPlayerTime = g_CurTime;
					m_pHeroActionInput->SetCurrentTarget( m_dwFollowPlayerKey );

					OUTPUTTIP(TEXTRES_SET_FOLLOWPLAYER_START );
					//LOGTIP(TEXTRES_SET_FOLLOWPLAYER_START );
				}
				else
				{
					OUTPUTTIP( TEXTRES_CANNOT_FOLLOW_OBJECT);
					m_bFollowPlayer = FALSE;

					return FALSE;
				}
			}
		}
	}

	return TRUE;
}


void GameAssistantHandler::ProcessFollow( void )
{
	if(	singleton::ExistHero() 
		&& GetFollowState() )
	{
		DWORD dwTimeLength = theGeneralGameParam.GetKBDMoveSendDuration();
		if( (g_CurTime - m_dwFollowPlayerTime) >= dwTimeLength)//FOLLOW_DELAY_TIME )
		{
			m_dwFollowPlayerTime = g_CurTime;

			Character	*pChar = (Character *)theObjectManager.GetObject( GetFollowPlayer() );
			if( pChar )
			{
				PLAYER_ACTION	action;
				ZeroMemory(&action,sizeof(action));
				action.ActionID					= ACTION_PLAYERFOLLOW;
				action.PLAYERMEET.dwTargetID	= pChar->GetObjectKey();
				m_pHeroActionInput->PushQueueAction(action);

				m_pHeroActionInput->SwitchAutoAttack(FALSE);
				m_pHeroActionInput->SwitchAutoMove(FALSE);

				Vector3D vTargetPos	= pChar->GetPosition();
				Vector3D vOffset		= vTargetPos - theHero.GetPosition();
				float		fDistance	= vOffset.Length();

				if( fDistance > theGeneralGameParam.GetDistanceFollowPlayer() )
				//if( fDistance > DISTANCE_FOLLOW_PLAYER )
				{
					vOffset.Normalize();
					vTargetPos = vTargetPos - vOffset * theGeneralGameParam.GetDistanceFollowPlayer();
					m_pHeroActionInput->MoveToAction(vTargetPos,pChar->GetPathExplorer()->GetTile());
				}
			}
		}
	}
}



//------------------------------------------------------------------------------
/**
*/
BOOL GameAssistantHandler::OnSpacePushItem( void )
{
	Item					*pObj;
	Object				*pObject;
	ItemObtainVector	vectorItem;
	ObjectMapIt		itr;

	for( itr = theObjectManager.GetBegin() ; itr != theObjectManager.GetEnd() ; ++itr )
	{
		pObject = static_cast<Object *>(itr->second);
		if (0 == pObject)
		{
			ASSERT(!"Object Is NULL!!");
			return FALSE;
		}

		if( pObject->IsItem() )
		{
			pObj = (Item *)pObject;

			if( pObj->IsOpened() == FALSE )
			{
				if (pObj->GetOwnerKey())
				{
					if (theHero.GetObjectKey() != pObj->GetOwnerKey())
						continue;
				}
			}
			else	//	
			{
				continue;
			}

			float fDistance;
			Vector3D vPlayerPos	= theHero.GetPosition();
			Vector3D vTargetPos	= pObj->GetPosition();
			Vector3D vOffset		= vTargetPos - vPlayerPos;

			fDistance = vOffset.Length();
			if( fDistance <= theGeneralGameParam.GetDistanceGetItem() )
			//if( fDistance <= DISTANCE_SPACE_GET_ITEM )
			{
				sITEM_OBTAIN	Getitem;
				Getitem.dwObjectKey	= pObj->GetObjectKey();
				Getitem.fDistance		= fDistance;
				vectorItem.push_back( Getitem );
			}
		}
	}


	sITEM_OBTAIN					itemMin = { 0, 9999.0f };
	ItemObtainVectorIt	it,
								HeroIt;
	//bool	bPass		= false;
	//int	iCount	= (int)theHero.m_vectorGetItem.size();

	for( it = vectorItem.begin() ; it != vectorItem.end() ; ++it )
	{
		//bPass = false;
		//for( HeroIt = theHero.m_vectorGetItem.begin() ; HeroIt != theHero.m_vectorGetItem.end() ; ++HeroIt )
		//{
		//	if( (*it).dwObjectKey == (*HeroIt).dwObjectKey )
		//	{
		//			bPass = true;
		//		break;
		//	}
		//}
		//if( bPass == true )
		//{
		//	continue;
		//}

		pObj = (Item *)theObjectManager.GetObject( (*it).dwObjectKey );
		if( pObj )
		{
			if( pObj->IsSendPickPacket() == FALSE )
			{
				if( itemMin.fDistance > (*it).fDistance )
				{
					itemMin.dwObjectKey = (*it).dwObjectKey;
					itemMin.fDistance = (*it).fDistance;
				}
			}
		}
	}


	//	theHero.m_vectorGetItem.push_back( itemMin );
	pObj = (Item *)theObjectManager.GetObject( itemMin.dwObjectKey );
	if( pObj )
	{
		PLAYER_ACTION	action;
		ZeroMemory( &action, sizeof(action) );

		action.ActionID = ACTION_GETITEM;
		action.GETITEM.dwItemKey = itemMin.dwObjectKey;
		m_pHeroActionInput->PushQueueAction(action);
		if( m_pHeroActionInput->ProcessQueueGetItem() )
		{
			m_pHeroActionInput->GetQueueAction().ActionID = ACTION_IDLE; 
			return TRUE;
		}
	}

	return TRUE;
}


//------------------------------------------------------------------------------
BOOL GameAssistantHandler::UpdateTargetList( float /*fDistance*/, BOOL bCheckCameraAngle )
{
	//TabTargetInfoVectorIt	Check_it;
	theHero.ClearTargetList();


	Object					*pObject;
	Character				*pChar;
	ObjectMapIt			itr;
	
	for( itr = theObjectManager.GetBegin() ; itr != theObjectManager.GetEnd() ; ++itr )
	{
		pObject = static_cast<Object *>(itr->second);
		if (NULL == pObject)
		{
			ASSERT(!"Object Is NULL!!");
			return FALSE;
		}


		pChar = (Character *)pObject;
		if( RELATION_ENEMY == GameUtil::GetRelationOfHero(pChar) )
		{
			if( pChar->IsDead() == TRUE )
				continue;

			float		fDistance;
			float		fMinAngle	= pChar->GetAngle();
			Vector3D vPlayerPos	= theHero.GetPosition();
			Vector3D vTargetPos	= pChar->GetPosition();
			Vector3D vOffset		= vTargetPos - vPlayerPos;

			fDistance = vOffset.Length();
			if( fDistance <= theGeneralGameParam.GetDistanceTabTarget() )
			//if( fDistance <= DISTANCE_TAB_TARGET_MONSTER )
			{

				if( bCheckCameraAngle )
				{
					//	前夹角45( 视野 90 )
					vOffset = vTargetPos - theCamera.GetLookFrom();

					float fTargetAngle	= vOffset.GetAngleByACos();
					float fAngle			= theCamera.GetAngle();
					float fDiff				= abs(fTargetAngle - fAngle);
					float fAnotherDiff	= MATH_PI * 2.0f - fDiff;

					// 大于 45
					fMinAngle = min(fDiff,fAnotherDiff);
					if ( fMinAngle > MATH_PI / 4.0f )
					{
						continue;
					}
				}

				sTAB_TARGET_INFO	TAB_TargetMonster;
				TAB_TargetMonster.dwObjectKey		= pChar->GetObjectKey();
				TAB_TargetMonster.fAngle			= fMinAngle;
				TAB_TargetMonster.fDistance		= fDistance;
				TAB_TargetMonster.wTargetCount	= 0;
				TAB_TargetMonster.wTargetCheck	= 1;
				theHero.PushTarget( TAB_TargetMonster );
			}
		}
	}

	theHero.SortTargetList();

	return TRUE;
}


//------------------------------------------------------------------------------
BOOL GameAssistantHandler::OnTabPushTarget( void )
{
	if( !theHero.ExistTarget() )
		return TRUE;

	DWORD dwObjectKey;
	dwObjectKey = theHero.LockTargetToList();

	m_pHeroActionInput->SetCurrentTarget( dwObjectKey );



	return TRUE;
}


//------------------------------------------------------------------------------
BOOL GameAssistantHandler::OnNextTarget( void )
{
	if( !theHero.ExistTarget() )
		return TRUE;

	if( !theHero.ToggleNextTarget() )
		theHero.LockFirstTarget();

	m_pHeroActionInput->SetCurrentTarget( theHero.m_TabTargetInfo.dwObjectKey );

	return TRUE;


}


//------------------------------------------------------------------------------
BOOL GameAssistantHandler::OnPrevTarget( void )
{
	if(! theHero.ExistTarget())
		return TRUE;

	if( !theHero.TogglePrevTarget() )
		theHero.LockLastTarget();

	m_pHeroActionInput->SetCurrentTarget( theHero.m_TabTargetInfo.dwObjectKey );

	return TRUE;


	//return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：heroactionsoundlayer.cpp
创建日期：2007年7月7日
最后更新：2007年7月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "HeroSoundHandler.h"
#include "SoundEffect.h"
#include "Hero.h"
#include "HeroVoiceInfoParser.h"
#include "GameParameter.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(HeroSoundHandler, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
HeroSoundHandler::HeroSoundHandler()
{
	m_iUIVoiceDelay = 0;
	m_dwUIVoice = 0;
	//m_iMoveDelay=0;
}

HeroSoundHandler::~HeroSoundHandler()
{
}

void HeroSoundHandler::ProcessVoiceDelay(DWORD dwTick)
{
	if (m_iUIVoiceDelay > 0)
	{
		m_iUIVoiceDelay -= dwTick;
		if (m_iUIVoiceDelay <= 0)
		{
			m_iUIVoiceDelay = 0;
			m_dwUIVoice = 0;
		}

	}

}


void    HeroSoundHandler::PlayLimitUseSound()
{
	if( theGameOption.GetSystemVoice() == FALSE )
		return;

	if (m_dwUIVoice == UI_VOICE_LIMIT_USE)
		return;

	m_dwUIVoice			= UI_VOICE_LIMIT_USE;
	m_iUIVoiceDelay	= UI_VOICE_DELAY;

	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(theHero.GetCharSoundCode());

	if (pInfo)
	{
		theSoundEffect.PlayUI(pInfo->m_LimitUse.m_VoiceID);
		theHero.DisplayChatMessage(pInfo->m_LimitUse.m_TextID);
	}

}
void    HeroSoundHandler::PlayNoTargetSound()
{
	if( theGameOption.GetSystemVoice() == FALSE )
	{
		return;
	}


	if (m_dwUIVoice == UI_VOICE_NO_TARGET)
	{
		return;
	}

	m_dwUIVoice = UI_VOICE_NO_TARGET;
	m_iUIVoiceDelay = UI_VOICE_DELAY;
	
	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(theHero.GetCharSoundCode());

	if (pInfo)
	{
		if (rand() & 1)
		{
			theSoundEffect.PlayUI(pInfo->m_NoTarget1.m_VoiceID);
			theHero.DisplayChatMessage(pInfo->m_NoTarget1.m_TextID);
		}
		else
		{
			theSoundEffect.PlayUI(pInfo->m_NoTarget2.m_VoiceID);
			theHero.DisplayChatMessage(pInfo->m_NoTarget2.m_TextID);
		}
	}
}

void    HeroSoundHandler::PlayTargetTooFarSound()
{
	if( theGameOption.GetSystemVoice() == FALSE )
	{
		return;
	}

	if (m_dwUIVoice == UI_VOICE_TARGET_TOO_FAR)
	{
		return;
	}

	m_dwUIVoice = UI_VOICE_TARGET_TOO_FAR;
	m_iUIVoiceDelay = UI_VOICE_DELAY;

	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(dwClass);

	if (pInfo)
	{
		if(rand()&1)
		{
			theSoundEffect.PlayUI(pInfo->m_FarTarget1.m_VoiceID);
			theHero.DisplayChatMessage(pInfo->m_FarTarget1.m_TextID);
		}
		else
		{
			theSoundEffect.PlayUI(pInfo->m_FarTarget2.m_VoiceID);
			theHero.DisplayChatMessage(pInfo->m_FarTarget2.m_TextID);
		}
	}
}


void    HeroSoundHandler::PlayNotEnoughMoneySound()
{
	if( theGameOption.GetSystemVoice() == FALSE )
	{
		return;
	}

	if (m_dwUIVoice == UI_VOICE_NOT_ENOUGH_MONEY)
	{
		return;
	}

	m_dwUIVoice = UI_VOICE_NOT_ENOUGH_MONEY;
	m_iUIVoiceDelay = UI_VOICE_DELAY;

	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(theHero.GetCharSoundCode());

	if (pInfo)
	{
		theSoundEffect.PlayUI(pInfo->m_LackMon.m_VoiceID);
		theHero.DisplayChatMessage(pInfo->m_LackMon.m_TextID);
	};
}
void    HeroSoundHandler::PlayNotEnoughManaSound()
{
	if( theGameOption.GetSystemVoice() == FALSE )
	{
		return;
	}

	if (m_dwUIVoice == UI_VOICE_NOT_ENOUGH_MANA)
	{
		return;
	}

	m_dwUIVoice = UI_VOICE_NOT_ENOUGH_MANA;
	m_iUIVoiceDelay = UI_VOICE_DELAY;

	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(theHero.GetCharSoundCode());

	if (pInfo)
	{
		theSoundEffect.PlayUI(pInfo->m_LackMP.m_VoiceID);
		theHero.DisplayChatMessage(pInfo->m_LackMP.m_TextID);
	}
}
void    HeroSoundHandler::PlayNotEnoughPointSound()
{
	if( theGameOption.GetSystemVoice() == FALSE )
	{
		return;
	}

	if (m_dwUIVoice == UI_VOICE_NOT_ENOUGH_POINT )
	{
		return;
	}

	m_dwUIVoice = UI_VOICE_NOT_ENOUGH_POINT;
	m_iUIVoiceDelay = UI_VOICE_DELAY;

	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(theHero.GetCharSoundCode());

	if (pInfo)
	{
		theSoundEffect.PlayUI(pInfo->m_LackPoint.m_VoiceID);
		theHero.DisplayChatMessage(pInfo->m_LackPoint.m_TextID);
	}
}

void    HeroSoundHandler::PlayRepairableSound(BOOL bCanntRepair)
{
	if( theGameOption.GetSystemVoice() == FALSE )
	{
		return;
	}

	if (m_dwUIVoice == UI_VOICE_NO_REPAIRABLE)
		return;

	m_dwUIVoice			= bCanntRepair?UI_VOICE_NO_REPAIRABLE : 0;
	m_iUIVoiceDelay	= UI_VOICE_DELAY;

	DWORD dwClass = 0;
	if (singleton::ExistHero())
	{
		dwClass = theHero.GetClass();
	}

    
	sHERO_VOICEINFO *pInfo = theHeroVoiceInfoParser.GetVoiceInfo(theHero.GetCharSoundCode());

	if (pInfo)
	{
		theSoundEffect.PlayUI(pInfo->m_NOREPItem.m_VoiceID);
		theHero.DisplayChatMessage(pInfo->m_NOREPItem.m_TextID);
	}
}




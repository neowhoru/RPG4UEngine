/*////////////////////////////////////////////////////////////////////////
文 件 名：MouseInputHandler.cpp
创建日期：2007年9月10日
最后更新：2007年9月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "MouseInputHandler.h"
#include "HeroActionInput.h"
#include "Item.h"
#include "Hero.h"
#include "MapObject.h"
#include "GameUtil.h"
#include "ConstTextRes.h"

#include "InputLayer.h"
#include "Camera.h"
#include "MouseHandler.h"
#include "VUICursorManager.h"
#include "TileWorldClient.h"
#include "PathHandler.h"
#include "HeroTipLayer.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "VHeroActionInput.h"
#include "GameParameter.h"
#include "ApplicationSetting.h"
#include "VObjectManager.h"
#include "PacketInclude.h"
#include "MapViewManager.h"

using namespace input;

const float CAMERADRAG_GAP	= 5.0;	///5倍

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(MouseInputHandler, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
MouseInputHandler::MouseInputHandler()
{
	m_pHeroActionInput	= NULL;
	m_MousePoint.x			= 0;
	m_MousePoint.y			= 0;
	m_bIsMousePointHide	= FALSE;
}

MouseInputHandler::~MouseInputHandler()
{
}



DWORD MouseInputHandler::ProcessPicking()
{
	DWORD dwPickedTarget = 0;

#ifndef _DEBUG
    PROFILE_PROC_BEGIN(_T("CheckMovable"));
	m_pHeroActionInput->SetCanMoveToPickedPos(FALSE);

	INT		tileIndex;
	Vector3D	szVec;

	szVec.Set(0,0,0);
	tileIndex	= -1;


	BOOL bPickTerrain = GameUtil::GetClickedPosition(&szVec);

	if (bPickTerrain)
	{
		tileIndex = theTileMap.GetTileToStand( szVec, NULL, -1.0f, 100.0f );

		if (tileIndex != -1)
		{
			if (!(theTileMap.GetAttribute(tileIndex) & (PTA_ONLY_JUMP | PTA_NO_WALK)))
			{			
				m_pHeroActionInput->SetCanMoveToPickedPos(TRUE);
			}
		}
	}
	m_pHeroActionInput->SetPickedTile(tileIndex);
	m_pHeroActionInput->SetPickedPos(szVec);


	PROFILE_PROC_END( _T("CheckMovable") );
#else
	m_pHeroActionInput->SetPickedTile(-1);
	m_pHeroActionInput->SetCanMoveToPickedPos(TRUE);
#endif


	DWORD dwPickingOption = PICKING_IGNORE_CORPSE;

	if (m_pHeroActionInput->IsIgnorePlayer())
	{
		dwPickingOption |= PICKING_IGNORE_FRIEND;

	}

	if ( !theInputLayer.IsUIMouseInput() )
	{
		Character* pPickedObj;
		pPickedObj = GameUtil::GetPickedCharacter(Ray(g_MouseRay),dwPickingOption);
		if (pPickedObj)
		{
			dwPickedTarget = pPickedObj->GetObjectKey();
			return dwPickedTarget;
		}

		Item* pItem = GameUtil::GetPickedItem(Ray(g_MouseRay));
		if (pItem)
		{
			dwPickedTarget = pItem->GetObjectKey();
			return dwPickedTarget;
		}

		MapObject * pMapObject;
		pMapObject = GameUtil::GetPickedMapUnitObject(Ray(g_MouseRay));
		if (pMapObject)
		{
			
			if (pMapObject->IsTriggerObj()) 
			{
				dwPickedTarget = pMapObject->GetObjectKey();		
			}
			
			return dwPickedTarget;
		}
	}
	return dwPickedTarget;
}


BOOL MouseInputHandler::ProcessTargetClick( Character *pTarget, BOOL bAutoAttack )
{
	if( !m_pHeroActionInput->IsShootingMode() )
	{
		if( FALSE == OnLClickedCharacter(pTarget, GameUtil::GetRelationOfHero(pTarget)) )
			return FALSE;


		theGameUIManager.SetMovePointer( FALSE );
		m_pHeroActionInput->SwitchAutoMove(FALSE);
		m_pHeroActionInput->SetFollowState( FALSE );

		if( m_pHeroActionInput->GetTargetType() == RELATION_ENEMY )
		{
			m_pHeroActionInput->SwitchAutoAttack( bAutoAttack );
		}
	}
	else
	{
		if( theHero.IsMoving() == TRUE )
		{
			//	45
			Vector3D vTarget = pTarget->GetPosition();
			Vector3D vPos = theHero.GetPosition();
			Vector3D vDiff = vTarget - vPos;
			float fTargetAngle = vDiff.GetAngleByACos();
			float fAngle = theHero.GetAngle();

			float fDiff = abs(fTargetAngle - fAngle);
			float fAnotherDiff = MATH_PI * 2.0f - fDiff;

			//  45
			if (min(fDiff,fAnotherDiff) > MATH_PI / 4.0f )
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
				theHero.LockOn( 0 );
			}
			else
			{
				theHero.LockOn( m_pHeroActionInput->GetCurrentTarget() );

				//	籍泵 饭牢瘤 绢琶 鸥涝
				if( FALSE == OnStabberLClickedCharacter(pTarget) )
				{
					//	俊矾
					return FALSE;
				}
			}
		}
		else
		{
			//	籍泵 饭牢瘤 绢琶 鸥涝
			if( FALSE == OnStabberLClickedCharacter(pTarget) )
			{
				//	俊矾
				return FALSE;
			}
		}

		theGameUIManager.SetMovePointer( FALSE );
		m_pHeroActionInput->SwitchAutoMove(FALSE);
		m_pHeroActionInput->SetFollowState( FALSE );
		if( m_pHeroActionInput->GetTargetType() == RELATION_ENEMY )
		{
			m_pHeroActionInput->SwitchAutoAttack( bAutoAttack );
		}
	}

	return TRUE;
}


BOOL	MouseInputHandler::ProcessClickPosition(Vector3C&	vTarget)
{
	OnLClickedPosition(&vTarget);
	return TRUE;
}

BOOL	MouseInputHandler::ProcessCamera(const sMOUSE_INFO& info)
{
	BOOL bUpdate = FALSE;
	if(	!singleton::ExistMouseHandler()
		|| !theMouseHandler.IsExistItemAtHand())
	{
		VObject*	pMouseObject = theVObjectManager.GetMouseTargetObject();

		if(	pMouseObject == NULL
			||	theCamera.IsKeepRotating(CAMERADRAG_GAP)
			|| pMouseObject && theInputLayer.GetMouseDragState(KEYC_NULL))
		{

			if(	theApplicationSetting.m_HandleCameraRButton 
				&& theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_KEY ) )
			{
				if(m_pHeroActionInput->IsAutoMove())
					theCamera.Rotate( 0, info.lY, Camera::CAMERA_ROTATE_RMOUSE );
				else
					theCamera.Rotate( -info.lX, info.lY, Camera::CAMERA_ROTATE_RMOUSE );
				bUpdate = TRUE;
			}

			else if(		theApplicationSetting.m_HandleCameraLButton 
						&& theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_KEY) )
			{
				theCamera.Rotate( -info.lX, info.lY );
				bUpdate = TRUE;
			}

			else if (theInputLayer.GetState( MOUSE_MBUTTON, KS_MOUSE_DOWN ))
			{
				if(singleton::ExistHero())
					theCamera.SetYaw( theHero.GetAngle() );
				theCamera.Rotate(0,0);	
				bUpdate = TRUE;
			}
		}////if(theVHeroActionInput.GetMouseTargetObject() == NULL)

	}



	if(info.lZ != 0)
	{
		theCamera.MoveFrontBack( info.lZ /** SCALE_MESH */);
		//bUpdate = TRUE;
	}

	return bUpdate;
}

BOOL	MouseInputHandler::ProcessCamera(const sMOUSE_INFO& info,BOOL bFreeCamera)
{
	if(!bFreeCamera)
		return FALSE;

	BOOL bUpdate = FALSE;

	if(	theApplicationSetting.m_HandleCameraLButton 
		&& theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_KEY) )
	{
		theCamera.Rotate( -info.lX, info.lY );
		bUpdate = TRUE;
	}

	return bUpdate;
}

//------------------------------------------------------------------------------
BOOL MouseInputHandler::ProcessMouse()
{
	if (!singleton::ExistHero())
		return FALSE;

	//LPDIMOUSESTATE	pMouserState;
	static	BOOL	bIsLButtonDown			= FALSE;
	static	BOOL	bIsRButtonDown			= FALSE;
	static	BOOL	bIsLButtonDragging	= FALSE;
	static	BOOL	bIsRButtonDragging	= FALSE;

	static	BOOL	bIsRotateL = FALSE;
	static	BOOL	bIsRotateR = FALSE;
	static	POINT ptCursor;

	BOOL		bRotate;


	GetCursorPos( &ptCursor );

	//ProcessCamera(info);

	m_pHeroActionInput->SetPicking(FALSE);

	if( theInputLayer.GetState( MOUSE_LBUTTON, IKS_MOUSE_DOWN ))
	{
		m_MousePoint = ptCursor;
		//GetCursorPos( &m_MousePoint );
	}
	if( theInputLayer.GetState( MOUSE_RBUTTON, IKS_MOUSE_DOWN ))
	{
		m_MousePoint = ptCursor;
		//GetCursorPos( &m_MousePoint );
	}


	theHero.SetHeroTurnState( theCamera.GetCameraTurnState() );


	/////////////////////////////////////////////////////
	//左键处理
	///1.1左键按下
	if(	theInputLayer.IsMouseDown( MOUSE_LBUTTON) )
	{
		//LOGINFO("theInputLayer.IsMouseDown( MOUSE_LBUTTON )\n");
		bIsRotateL				= FALSE;
		bIsLButtonDown			= TRUE;
		bIsLButtonDragging	= FALSE;
		m_MousePoint	= ptCursor;
		//GetCursorPos( &m_MousePoint );
		OnMouseLButtonDown(ptCursor);
	}
	else if(theInputLayer.IsMouseDragging(MOUSE_LBUTTON) )
	{//IsKeepRotating

		//LOGINFO("theInputLayer.IsMouseDragging( MOUSE_LBUTTON )\n");
		if(theApplicationSetting.m_HandleCameraLButton)
			bRotate = theCamera.IsKeepRotating(CAMERADRAG_GAP);
		else
			bRotate	= FALSE;

		///1.2左键拖动，导致相机开始转动
		if(bIsLButtonDragging == FALSE)
		{
			bIsLButtonDragging	= TRUE;
			if(bRotate)
			{
				m_bIsMousePointHide = TRUE;
				theCursorManager.OnBeginDragCamera();
			}
			OnMouseLButtonBeginDrag(ptCursor, bRotate);
		}
		/// 1.3左键拖着、相机转动着
		else
		{
			if( m_bIsMousePointHide == FALSE && bRotate)
			{
				theCursorManager.OnBeginDragCamera();
				m_bIsMousePointHide = TRUE;
			}
			OnMouseLButtonDragging(ptCursor, bRotate);
		}
	}
	/// 1.4左键松开
	else if(theInputLayer.IsMouseUp( MOUSE_LBUTTON ) )
	{
		if( bIsLButtonDown == TRUE )
		{
			//LOGINFO("theInputLayer.IsMouseUp( MOUSE_LBUTTON )\n");
			if(theApplicationSetting.m_HandleCameraLButton)
				bRotate		= theCamera.IsKeepRotating(CAMERADRAG_GAP);
			else
				bRotate		= FALSE;

			if( m_bIsMousePointHide )
			{
				theCursorManager.OnEndDragCamera();
				m_bIsMousePointHide = FALSE;
			}

			ClipCursor(NULL);

			if( bIsLButtonDragging )
				OnMouseLButtonEndDrag(ptCursor,bRotate);
			else
				OnMouseLButtonUp(ptCursor);

			bIsLButtonDown		= FALSE;
			bIsLButtonDragging= FALSE;
		}	//	if( bIsLButtonDown == TRUE )
	}


	////////////////////////////////////////
	/// 2.1处理
	if(theInputLayer.IsMouseDown( MOUSE_RBUTTON) )
	{
		bIsRotateR				= FALSE;
		bIsRButtonDown			= TRUE;
		bIsRButtonDragging	= FALSE;
		m_MousePoint	= ptCursor;
		//GetCursorPos( &m_MousePoint );
		OnMouseRButtonDown(ptCursor);
	}
	else if(theInputLayer.IsMouseDragging(MOUSE_RBUTTON) )
	{//IsKeepRotating
		if(theApplicationSetting.m_HandleCameraRButton)
			bRotate	= theCamera.IsKeepRotating(CAMERADRAG_GAP);
		else
			bRotate	= FALSE;

		///1.2右键拖动，导致相机开始转动
		if(bIsRButtonDragging == FALSE)
		{
			bIsRButtonDragging	= TRUE;
			if(bRotate)
			{
				m_bIsMousePointHide = TRUE;
				theCursorManager.OnBeginDragCamera();
			}
			OnMouseRButtonBeginDrag(ptCursor, bRotate);
		}
		/// 1.3右键拖着、相机转动着
		else
		{
			if( m_bIsMousePointHide == FALSE && bRotate)
			{
				theCursorManager.OnBeginDragCamera();
				m_bIsMousePointHide = TRUE;
			}
			OnMouseRButtonDragging(ptCursor, bRotate);
		}
	}
	/// 1.4右键松开
	else if(theInputLayer.IsMouseUp( MOUSE_RBUTTON ) )
	{
		if( bIsRButtonDown == TRUE )
		{
			if(theApplicationSetting.m_HandleCameraRButton)
				bRotate		= theCamera.IsKeepRotating(CAMERADRAG_GAP);
			else
				bRotate		= FALSE;

			if( m_bIsMousePointHide )
			{
				theCursorManager.OnEndDragCamera();
				m_bIsMousePointHide = FALSE;
			}

			if( bIsRButtonDragging )
				OnMouseRButtonEndDrag(ptCursor,bRotate);
			else
				OnMouseRButtonUp(ptCursor);

			bIsRButtonDown		= FALSE;
			bIsRButtonDragging= FALSE;
		}	//	if( bIsRButtonDown == TRUE )
	}




	return FALSE;
}


BOOL MouseInputHandler::OnMouseLButtonDown(const POINT& ptCursor)
{
	if(ProcessViewNpc(TRUE))
		return FALSE;

	theGameUIManager.UISetVisible	(gameui::eUIRbtnMenu		,FALSE);
	theGameUIManager.UISetVisible	(gameui::eUISelfRbtnMenu,FALSE);

	if(!theApplicationSetting.m_HandleCameraLButton)
		return ProcessButtonEvent(ptCursor);
	return FALSE;
}

BOOL MouseInputHandler::OnMouseLButtonUp(const POINT& ptCursor)
{
	theCursorManager.OnEndDragCamera();

	if(ProcessViewNpc())
		return FALSE;

	if(theApplicationSetting.m_HandleCameraLButton)
		return ProcessButtonEvent(ptCursor);
	return FALSE;
}

BOOL	MouseInputHandler::ProcessViewNpc(BOOL bHitTest)
{
	sNPC_COORDINFO*	pCood;
	Object*				pObject;

	////////////////////////////////
	pCood = theMapViewManager.GetViewNpcAtMouse();
	if(pCood)
	{
		////////////////////////////////
		pObject = theObjectManager.GetObject(pCood->m_NpcID);
		if(pObject)
		{
			if(!bHitTest)
				OnLClickedPosition(&pObject->GetPosition());
			return TRUE;
		}
	}

	////////////////////////////////
	Vector3D	vPos;
	if(theMapViewManager.GetTargetPosAtMouse(vPos))
	{
		if(!bHitTest)
			OnLClickedPosition(vPos);
		return TRUE;
	}

	return FALSE;
}


BOOL MouseInputHandler::ProcessButtonEvent(const POINT& /*ptCursor*/)
{
	///////////////////////////////////////////////////
	// 8.右键松开处理
	//		a.前进
	//		b.攻击
	//		c.拾取物品
	//		d.MapNPC触发、商店...

	if (theMouseHandler.IsExistItemAtHand())
	{
		if(theMouseHandler.DoItemDrop())
			return TRUE;
	}
	if(theMouseHandler.GetMouseState() != eMOUSE_NONE)
		return TRUE;

	if(theHero.IsLockInput())
		return TRUE;

	//取消对话
	theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
											,gameui::eToggleDialogText
											,(LPARAM)NULL);


	m_pHeroActionInput->SwitchAutoAttack(FALSE);
	m_pHeroActionInput->SwitchAutoMove(FALSE);
	m_pHeroActionInput->SetFollowState( FALSE );

	Character* pPickedObj = m_pHeroActionInput->GetPickedCharacter();

	//if( m_pHeroActionInput->m_bIsHeroKeyboardMove || theHero.GetCurrentState() == STATE_KEYBOARDMOVE )
	if( m_pHeroActionInput->IsHeroKeyboardMove() )
	{
		if( pPickedObj )
		{
			m_pHeroActionInput->SetCurrentTarget( pPickedObj->GetObjectKey() );
		}

		return FALSE;
	}

	m_pHeroActionInput->InitAction();//m_QueueAction清除

	if(	pPickedObj
		&&	!m_pHeroActionInput->IsAreaSkill()) //
	{
		if (OnLClickedCharacter(pPickedObj, GameUtil::GetRelationOfHero(pPickedObj)))
		{
			theGameUIManager.SetMovePointer( FALSE );
			return TRUE;
		}
	}

	Item * pPickItem = m_pHeroActionInput->GetPickedItem();
	if (pPickItem)
	{
		if (	!pPickItem->IsOpened() 
			&& theMouseHandler.IsExistItemAtHand() == FALSE )
		{
			if (OnLClickedItem(pPickItem))
			{
				return TRUE;
			}
		}
	}

	Vector3D ClickedPosition;
	if( GameUtil::GetClickedPosition(&ClickedPosition) == FALSE )
		return TRUE;

	if (OnLClickedPosition(&ClickedPosition))
	{
		return TRUE;
	}

	return TRUE;
}


BOOL MouseInputHandler::OnMouseLButtonBeginDrag(const POINT& /*ptCursor*/,BOOL bDragCamera)
{
	//if(theHero.IsLockInput())
	//	return TRUE;

	if(bDragCamera)
	{
		if(	!m_pHeroActionInput->IsAutoAttack()
			&&	!m_pHeroActionInput->IsShootingMode()
			&&	theHero.IsDead() == FALSE )
		{
			if(	theHero.GetCurrentState() == STATE_IDLE 
				|| theHero.GetCurrentState() == STATE_KEYBOARDMOVE )
			{
				theHero.SetHeroTurnState( theCamera.GetCameraTurnState() );

				theHero.SetDirection(theCamera.GetLookAt()
										  ,(Vector3D*)&theCamera.GetLookFrom()
										  ,FALSE);
			}
		}
	}//if(bDragCamera)


	return TRUE;
}

BOOL MouseInputHandler::OnMouseLButtonDragging(const POINT& /*ptCursor*/,BOOL bDragCamera)
{
	//if(theHero.IsLockInput())
	//	return TRUE;

	if(bDragCamera)
	{
		if(	!m_pHeroActionInput->IsAutoAttack() 
			&& !m_pHeroActionInput->IsShootingMode()
			&& theHero.IsDead() == FALSE )
		{
			if(	theHero.GetCurrentState() == STATE_IDLE
				|| theHero.GetCurrentState() == STATE_KEYBOARDMOVE )
			{
				theHero.SetHeroTurnState( theCamera.GetCameraTurnState() );

				theHero.SetDirection(theCamera.GetLookAt(), (Vector3D*)&theCamera.GetLookFrom(), FALSE );
			}
		}
	}//if(bDragCamera)
	return TRUE;
}

BOOL MouseInputHandler::OnMouseLButtonEndDrag(const POINT& /*ptCursor*/,BOOL /*bDragCamera*/)
{
	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
	右键事件
/*////////////////////////////////////////////////////////////////////////
BOOL MouseInputHandler::OnMouseRButtonDown(const POINT& /*ptCursor*/)
{

	return FALSE;
}

	///////////////////////////////////////////
//COLOR	sColor = COLOR_RGBA(255,255,255,255);
//float	sScale = 2.0f;
//BOOL		sDispperar = 1;
//BOOL		sV		= 1;

BOOL MouseInputHandler::OnMouseRButtonUp(const POINT& /*ptCursor*/)
{
	theCursorManager.OnEndDragCamera();

	if(theHero.IsLockInput())
		return TRUE;

	//	a.锁定目标

	Character*	pPickedObj;
	BOOL			bCanShowPlayerMenu = FALSE;

	pPickedObj = m_pHeroActionInput->GetPickedCharacter();

	///////////////////////////////////////////
	//testing
	//if(pPickedObj)
	//{
	//	DWORD			dwCloneKey;
	//	Clone *		pClone;

	//	dwCloneKey	= theCloneManager.CreateClone(pPickedObj);
	//	pClone		= theCloneManager.GetClone(dwCloneKey);
	//	if (pClone) 
	//	{
	//		DWORD dwTime = theGeneralGameParam.GetDeadDisappearTime();

	//		pClone->SetColor(sColor);
	//		//pClone->SetAlphaBlend(AB_ALPHA);
	//		//pClone->SetDelay(1100);
	//		//pClone->SetDisappear(1000);
	//		if(sScale != 0)
	//			pClone->SetScaleAnimation(dwTime, sScale);
	//		pClone->SetDelay(dwTime+100);
	//		if(sDispperar)
	//			pClone->SetDisappear(dwTime);
	//		pClone->SetPlayAni(FALSE);
	//		if(sV)
	//			pClone->Vibrate(Vector3D::UNIT_X*0.02f ,3.f);
	//	}
	//}

	///////////////////////////////////////////
	if(pPickedObj == NULL || pPickedObj->IsHero())
	{
		m_pHeroActionInput->SwitchAutoAttack(FALSE);
		m_pHeroActionInput->SetCurrentTarget( 0 );
		if(	theHero.GetCurrentState() != STATE_MOVE 
			&&	theHero.GetCurrentState() != STATE_KEYBOARDMOVE
			&&	theHero.GetCurrentState() != STATE_SPECIALMOVE
			&&	theHero.GetCurrentState() != STATE_SIT 
			&&	theHero.GetCurrentState() != STATE_EMOTICON )
		{
			PLAYER_ACTION	action;
			action.ActionID = ACTION_IDLE;
			m_pHeroActionInput->PushQueueAction(action);
			theHero.SetNextState(STATE_IDLE,g_CurTime);
		}
	}

	//////////////////////////////////////////////
	else
	{
		//////////////////////////////////////////////
		m_pHeroActionInput->SetCurrentTarget( pPickedObj->GetObjectKey() );
		if( theHero.GetCurrentState() == STATE_IDLE )
		{
			theHero.SetDirection(pPickedObj->GetPosition());
		}

		//////////////////////////////////////////////
		if(pPickedObj->IsPlayer())
		{
			theGameUIManager.UIOnMouseRButton	(gameui::eUIRbtnMenu
															,pPickedObj->GetVObject()
															,theVUCtrlManager.m_MousePos.x
															,theVUCtrlManager.m_MousePos.y
															,FALSE);
			bCanShowPlayerMenu = TRUE;
		}
	}
	

	/////////////////////////////////////////////
	if(!bCanShowPlayerMenu)
	{
		theGameUIManager.UISetVisible	(gameui::eUIRbtnMenu,FALSE);
	}


	m_pHeroActionInput->ProcessTargeting();
	return TRUE;
}

BOOL MouseInputHandler::OnMouseRButtonBeginDrag(const POINT& /*ptCursor*/,BOOL /*bDragCamera*/)
{
	return TRUE;
}

BOOL MouseInputHandler::OnMouseRButtonDragging(const POINT& /*ptCursor*/,BOOL /*bDragCamera*/)
{
	return TRUE;
}

BOOL MouseInputHandler::OnMouseRButtonEndDrag(const POINT& /*ptCursor*/,BOOL /*bDragCamera*/)
{
	return TRUE;
}



//------------------------------------------------------------------------------
BOOL MouseInputHandler::ProcessStabberMouse()
{
	if (!singleton::ExistHero())
	{
		return FALSE;
	}

	if( theInputLayer.GetState( MOUSE_LBUTTON, IKS_MOUSE_DOWN ))
	{
		GetCursorPos( &m_MousePoint );
	}
	if( theInputLayer.GetState( MOUSE_RBUTTON, IKS_MOUSE_DOWN ))
	{
		GetCursorPos( &m_MousePoint );
	}

	static	BOOL	bIsLButtonDown = FALSE;
	static	BOOL	bIsRButtonDown = FALSE;

	static	BOOL	bIsRotateL = FALSE;
	static	BOOL	bIsRotateR = FALSE;


	theHero.SetHeroTurnState( theCamera.GetCameraTurnState() );

	if( theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_DOWN ))
	{
		bIsRotateL = FALSE;
		bIsLButtonDown = TRUE;
		GetCursorPos( &m_MousePoint );
	}
	else if ( bIsRotateL == FALSE && 
			  theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_KEY) && 
			  theCamera.IsKeepRotating(CAMERADRAG_GAP) == TRUE )
	{
		bIsRotateL = TRUE;
		if(	!m_pHeroActionInput->IsAutoAttack() 
			&& theHero.IsDead() == FALSE )
		{
			if(	theHero.GetCurrentState() == STATE_IDLE 
				|| theHero.GetCurrentState() == STATE_KEYBOARDMOVE )
			{
				theHero.SetHeroTurnState( theCamera.GetCameraTurnState() );

				theHero.SetDirection(theCamera.GetLookAt()
                                 ,(Vector3D*)&theCamera.GetLookFrom()
											,FALSE);
			}
		}

		if( m_bIsMousePointHide == FALSE )
		{
//			cursor::Instance()->ShowCursor( FALSE );
			m_bIsMousePointHide = TRUE;
		}
	}
	else if (bIsRotateL == TRUE && 
			 theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_KEY) && 
			 theCamera.IsKeepRotating(CAMERADRAG_GAP) == TRUE )
	{
		if(	!m_pHeroActionInput->IsAutoAttack() 
			&& theHero.IsDead() == FALSE )
		{
			if(	theHero.GetCurrentState() == STATE_IDLE 
				|| theHero.GetCurrentState() == STATE_KEYBOARDMOVE )
			{
				theHero.SetHeroTurnState( theCamera.GetCameraTurnState() );

				theHero.SetDirection(theCamera.GetLookAt()
                                 ,(Vector3D*)&theCamera.GetLookFrom()
											,FALSE);
			}
		}
	}


	if( theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_DOWN ))
	{
		bIsRotateR		= FALSE;
		bIsRButtonDown = TRUE;
		GetCursorPos( &m_MousePoint );
	}
	else if (	bIsRotateR == FALSE 
				&& theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_KEY) 
				&& theCamera.IsKeepRotating(CAMERADRAG_GAP) == TRUE )
	{
		bIsRotateR = TRUE;

		if( m_bIsMousePointHide == FALSE )
		{
//			cursor::Instance()->ShowCursor( FALSE );
			m_bIsMousePointHide = TRUE;
		}
	}
	else if (bIsRotateR == TRUE && 
			 theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_KEY) && 
			 theCamera.IsKeepRotating(CAMERADRAG_GAP) == TRUE )
	{
	}


	if( theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_UP ))
	{
		if( bIsLButtonDown == TRUE || m_bIsMousePointHide )
		{
			m_bIsMousePointHide = FALSE;
//			SetCursorPos( m_MousePoint.x, m_MousePoint.y );
//			cursor::Instance()->ShowCursor( TRUE );
		}
	}
	else if( theInputLayer.GetState( MOUSE_LBUTTON, IKS_MOUSE_UP ))
	{
		if( bIsLButtonDown == TRUE || m_bIsMousePointHide )
		{
			m_bIsMousePointHide = FALSE;
//			SetCursorPos( m_MousePoint.x, m_MousePoint.y );
//			cursor::Instance()->ShowCursor( TRUE );
		}
	}

	if( theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_UP ))
	{
		if( bIsRButtonDown == TRUE || m_bIsMousePointHide )
		{
			m_bIsMousePointHide = FALSE;
			SetCursorPos( m_MousePoint.x, m_MousePoint.y );
			//theCursorManager.SetShowCursor(TRUE);
			theCursorManager.OnEndDragCamera();
			//cursor::Instance()->ShowCursor( TRUE );
		}
	}
	else if( theInputLayer.GetState( MOUSE_RBUTTON, IKS_MOUSE_UP ))
	{
		if( bIsRButtonDown == TRUE || m_bIsMousePointHide )
		{
			m_bIsMousePointHide = FALSE;
//			SetCursorPos( m_MousePoint.x, m_MousePoint.y );
//			cursor::Instance()->ShowCursor( TRUE );
		}
	}



	if( theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_UP ))
	{
		if( bIsLButtonDown == TRUE )
		{
			bIsLButtonDown = FALSE;

			if( bIsRotateL == FALSE )
			{
				Vector3D ClickedPosition;
				m_pHeroActionInput->SwitchAutoMove(FALSE);
				m_pHeroActionInput->SetFollowState( FALSE );
				m_pHeroActionInput->SwitchAutoAttack(FALSE);


				m_pHeroActionInput->InitAction();

				if (!theHero.CanStabberAction())
				{
					return FALSE;
				}

				Character* pPickedObj = m_pHeroActionInput->GetPickedCharacter();
				if(pPickedObj)
				{
					if (OnStabberLClickedCharacter(pPickedObj))
					{
						theGameUIManager.SetMovePointer( FALSE );
						return TRUE;
					}
				}

				theHero.LockOn(0);

				Item * pPickItem = m_pHeroActionInput->GetPickedItem();
				if (pPickItem)
				{
					if (	!pPickItem->IsOpened() 
						&& theMouseHandler.IsExistItemAtHand() == FALSE )
					{
						if (OnLClickedItem(pPickItem))
						{
							return TRUE;
						}
					}
				}


				if (GameUtil::GetClickedPosition(&ClickedPosition))
				{
					if (OnLClickedPosition(&ClickedPosition))
					{
						return TRUE;
					}

				}
				else
				{
					return TRUE;
				}

			}
			else
			{
				bIsRotateL = FALSE;
			}
		}	//	if( bIsLButtonDown == TRUE )
	}


	if( theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_UP ))
	{
		if( bIsRotateR == FALSE )
		{
			Character* pPickedObj = m_pHeroActionInput->GetPickedCharacter();
			if(pPickedObj && (pPickedObj->GetObjectKey() != theHero.GetObjectKey()) )
			{
				m_pHeroActionInput->SetCurrentTarget( pPickedObj->GetObjectKey() );
				if (theHero.GetCurrentState() == STATE_IDLE)
				{
					theHero.SetDirection(pPickedObj->GetPosition());
				}
			}
			else
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);

				m_pHeroActionInput->SetCurrentTarget( 0 );
				if(	theHero.GetCurrentState() != STATE_MOVE 
					&& theHero.GetCurrentState() != STATE_KEYBOARDMOVE 
					&& theHero.GetCurrentState() != STATE_SPECIALMOVE)
				{
					PLAYER_ACTION	action;
					action.ActionID	= ACTION_IDLE;
					m_pHeroActionInput->PushQueueAction(action);
					theHero.SetNextState(STATE_IDLE,g_CurTime);
				}
			}

			//	鸥百 促捞倔肺弊 汲沥
			m_pHeroActionInput->ProcessTargeting();
		}
		else
		{
			bIsRotateR = FALSE;
		}
	}

	return FALSE;	
}


//------------------------------------------------------------------------------
/**
*/
BOOL MouseInputHandler::ProcessMouseWhenKeyboardMove()
{
	if(theInputLayer.GetState( MOUSE_LBUTTON, KS_MOUSE_DOWN))
	{
		Character* pPickedObj = m_pHeroActionInput->GetPickedCharacter();
		if(	pPickedObj 
			&& (pPickedObj->GetObjectKey() != theHero.GetObjectKey()) )
		{
			m_pHeroActionInput->SetCurrentTarget(pPickedObj->GetObjectKey());
			return TRUE;
		}
	}

	return FALSE;
}

//------------------------------------------------------------------------------
BOOL MouseInputHandler::OnLClickedCharacter(Character* pObj, eRELATION_TYPE relationKind)
{
	m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());

	if (singleton::ExistHero()) 
	{
		if (theHero.GetShow()) 
		{
			theGameUIManager.UISetData	(gameui::eUITarget
												,gameui::eSetPlayerID
												,pObj->GetObjectKey());
			//g_InterfaceManager.ShowWindow(InterfaceManager::DIALOG_TARGET, TRUE);
		}
	}

	float fDistance;
	PLAYER_ACTION action;
	Vector3D vPlayerPos = theHero.GetPosition();
	Vector3D vOffset;
	Vector3D vTargetPos;

	ZeroMemory(&action,sizeof(action));
	action.ActionID = ACTION_IDLE;


	if (relationKind == RELATION_ENEMY)
	{
		if (!pObj->CanBeAttackTarget())
			return FALSE;

		//if(theHero.GetCurrentAttackStyle() == 0)
		//{
		//	OUTPUTTIP(TEXTRES_BATTLE_STYLECODE_WHERE_DONOT_SELECT);
		//	return FALSE;
		//}

		if (m_pHeroActionInput->IsCannotAttack())
			return FALSE;

		ZeroMemory(&action,sizeof(action));
		action.ActionID = ACTION_ATTACK;		
		action.ATTACK.dwTargetID = pObj->GetObjectKey();

		
		m_pHeroActionInput->SwitchAutoAttack(TRUE);
		m_pHeroActionInput->SetCurrentTarget( pObj->GetObjectKey() );


		vTargetPos = GameUtil::FindAttackTargetPos(pObj);
	}
	else if (relationKind == RELATION_ASSISTANT)
	{	
		if (singleton::ExistHero())
		{
			if ( theHero.GetBehaveState() == PLAYER_BEHAVE_IDLE )
			{
				vTargetPos = pObj->GetPosition();
				ZeroMemory(&action,sizeof(action));
				action.ActionID = ACTION_NPCMEET;						
				action.NPCMEET.dwNPCKey = pObj->GetObjectKey();	
			
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
			}
			else
			{
			}
		}
	}
	else if (relationKind == RELATION_VENDOR)
	{
		vTargetPos = pObj->GetPosition();
		ZeroMemory(&action,sizeof(action));
		action.ActionID = ACTION_VENDORMEET;						
		action.VENDORMEET.dwTargetID = pObj->GetObjectKey();		
		m_pHeroActionInput->SwitchAutoAttack(FALSE);


	}
	else if (relationKind == RELATION_FRIEND)
	{
		vTargetPos = pObj->GetPosition();
		ZeroMemory(&action,sizeof(action));
		action.ActionID					= ACTION_PLAYERMEET;						
		action.PLAYERMEET.dwTargetID	= pObj->GetObjectKey();
		m_pHeroActionInput->SwitchAutoAttack(FALSE);

		m_pHeroActionInput->SetFollowState(TRUE, pObj->GetObjectKey());
	 }


	if (action.ActionID == ACTION_IDLE)
		return FALSE;

	vOffset		= vTargetPos - vPlayerPos;
	fDistance	= vOffset.Length();


	m_pHeroActionInput->PushQueueAction(action);
	

	if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
	{
		if( m_pHeroActionInput->MoveToAction(vTargetPos,pObj->GetPathExplorer()->GetTile()) == FALSE )
		{
			if (relationKind == RELATION_ASSISTANT)
			{
				Vector3D vDir = theHero.GetPosition() - vTargetPos;
				vDir.Normalize();

				float		fDISTANCE_NPC_MEET		= theGeneralGameParam.GetDistanceNpcMeet();
				float		fDISTANCE_NPC_MEET_EXT	= theGeneralGameParam.GetPercentDistanceNpcMeet();
				//	MapNPC ( DISTANCE_NPC_MEET -  )  70%
				//	 30% 
				Vector3D TargetPos = vTargetPos + ( vDir * (fDISTANCE_NPC_MEET * fDISTANCE_NPC_MEET_EXT) );

				PathHandler	*pPE = NULL;

				pPE = ( PathHandler* )theTileWorldClient.CreatePathHandler(PathHandler_NULL);
				if( pPE )
				{
					theTileMap.PathHandleReset( pPE, TargetPos );
					if( m_pHeroActionInput->MoveToAction(TargetPos,pPE->GetTile()) == FALSE )
					{
						if(pPE)
						{
							theTileWorldClient.DestroyPathHandler( pPE );
							pPE = NULL;
						}
						return TRUE;
					}

					m_pHeroActionInput->GetQueueAction().ActionID = ACTION_NPCMEET;

					if(pPE)
					{
						theTileWorldClient.DestroyPathHandler( pPE );
						pPE = NULL;
					}
				}
			}
		}
		return TRUE;		
	}
	return TRUE;
}

//------------------------------------------------------------------------------
BOOL MouseInputHandler::OnShiftLClickedCharacter(Character* pObj, eRELATION_TYPE /*relationKind*/)
{

	m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());

	if (singleton::ExistHero()) 
	{
		if (theHero.GetShow()) 
		{
			theGameUIManager.UISetData	(gameui::eUITarget
												,gameui::eSetPlayerID
												,pObj->GetObjectKey());
			//g_InterfaceManager.ShowWindow(InterfaceManager::DIALOG_TARGET, TRUE);
		}
	}

	return TRUE;
	

}

BOOL MouseInputHandler::OnLDragedCharacter(Character* pObj, eRELATION_TYPE relationKind)
{

	if (relationKind == RELATION_ENEMY)
	{

		float fDistance;
		PLAYER_ACTION action;
		Vector3D vPlayerPos = theHero.GetPosition();
		Vector3D vOffset;
		Vector3D vTargetPos;

		ZeroMemory(&action,sizeof(action));
		action.ActionID = ACTION_IDLE;//咀记 惑怕 檬扁拳 	

		m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());

		if (singleton::ExistHero()) 
		{
			if (theHero.GetShow()) 
			{
				theGameUIManager.UISetData	(gameui::eUITarget
													,gameui::eSetPlayerID
													,pObj->GetObjectKey());
				//g_InterfaceManager.ShowWindow(InterfaceManager::DIALOG_TARGET, TRUE);
			}
		}


		if (!pObj->CanBeAttackTarget())
		{
			return FALSE;
		}

		ZeroMemory(&action,sizeof(action));
		action.ActionID = ACTION_ATTACK;			
		action.ATTACK.dwTargetID = pObj->GetObjectKey();

		
		vTargetPos = pObj->GetPosition();

		vOffset = vTargetPos - vPlayerPos;
		fDistance = vOffset.Length();

		m_pHeroActionInput->PushQueueAction(action);

		if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
		{
			m_pHeroActionInput->MoveToAction(vTargetPos,pObj->GetPathExplorer()->GetTile());		
			return TRUE;		
		}

		return TRUE;
	}

	return FALSE;
	

}




//------------------------------------------------------------------------------
BOOL MouseInputHandler::OnLClickedItem(Item* pObj)
{
	//TCHAR	szMessage[MAX_UITEXT_LENGTH];

	if (pObj->IsOpened())
	{
		return TRUE;
	}

	if (pObj->GetOwnerKey())
	{
		if (theHero.GetObjectKey() != pObj->GetOwnerKey())
		{
			OUTPUTCHAT(TEXTRES_NOT_OWNER );
			return TRUE;
		}

	}
	float fDistance;
	PLAYER_ACTION action;
	Vector3D vPlayerPos = theHero.GetPosition();
	Vector3D vOffset;
	Vector3D vTargetPos;

	ZeroMemory(&action,sizeof(action));
	action.ActionID = ACTION_IDLE;

	action.ActionID = ACTION_GETITEM;
	action.GETITEM.dwItemKey = pObj->GetObjectKey();
	
	if (action.ActionID == ACTION_IDLE)
	{
		return FALSE;
	}

	vTargetPos = pObj->GetPosition();
	vOffset = vTargetPos - vPlayerPos;
	fDistance = vOffset.Length();
	m_pHeroActionInput->PushQueueAction(action);
	
	if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
	{
		m_pHeroActionInput->MoveToAction(vTargetPos,-1);		
		return TRUE;
	}
	
	return TRUE;
}

//------------------------------------------------------------------------------
BOOL MouseInputHandler::OnLClickedPosition(Vector3C* pClickedPosition)
{
 	if( !singleton::ExistHero()  )
	{
		return FALSE;
	}

	if (theHero.CannotMove())
	{
		return FALSE;
	}


	Object	*pObj = theObjectManager.GetObject( m_pHeroActionInput->GetCurrentTarget() );
	if( pObj )
	{
		//ObjectCookie SCObj = pObj->GetObjectCookie();
		//if( SCObj.GetObjectType() == MONSTER_OBJECT )
		{
			theHero.SetStartMoveTime( g_CurTime );
		}
    }


	Vector3D wvTarget = *pClickedPosition;
	if (theTileMap.GetTileToStand( wvTarget, NULL, -1.0f, 100.0f ) == INVALID_TILEINDEX)
		return FALSE;

	///////////////////////
	//手上物品处理 
	if (theMouseHandler.IsExistItemAtHand())
	{
		if(theMouseHandler.DoItemDrop())
			return TRUE;
	}
	else
	{
		///清除技能列表
		if(!m_pHeroActionInput->IsAreaSkill())
			theSkillQueueManager.ClearAll();

		if( m_pHeroActionInput->IsIdleState() || theHero.GetCurrentState() == STATE_MOVE ) 
		{
			// 	CTRL+MOUSE_L_BUTTON
			if (theGeneralGameParam.GetSpecialMode())
			{
				if (theGeneralGameParam.IsEnableNetwork())
				{
					if (theInputLayer.GetState(KEYC_LCONTROL, KS_KEY))
					{
						MSG_CG_MAP_TELEPORT_SYN SendPacket;
						SendPacket.m_byCategory = CG_MAP;
						SendPacket.m_byProtocol = CG_MAP_TELEPORT_SYN;
						SendPacket.vPos			= *pClickedPosition;
						theHero.SendPacket( &SendPacket, sizeof(SendPacket) );
						return TRUE;
					}
				}
				else
				{
					if (theInputLayer.GetState(KEYC_LCONTROL, KS_KEY))
					{
						theHero.SetPosition(*pClickedPosition);
						return TRUE;
					}

				}
			}

			if(m_pHeroActionInput->IsAreaSkill())
			{
				m_pHeroActionInput->SetPicking(TRUE);
				return TRUE;
			}

			if(m_pHeroActionInput->GetMoveDelay() > 0)
			{
				return FALSE;
			}


			if(theHero.SetNextState( STATE_MOVE,g_CurTime ) )
			{
				theHero.SetTargetID( 0 );
				BOOL bResult = theHero.Move(theHero.GetPosition()
                                       ,*pClickedPosition
													,MOVETYPE_RUN
													,theInputLayer.IsKeyPress(KEYC_RCONTROL));
				if (bResult == TRUE)
				{
					MSG_CG_SYNC_MOVE_SYN	SendPacket;
					SendPacket.m_dwKey = theGeneralGameParam.GetUserID();

					// MakePathPacket
					if( theHero.MakePathPacket( &SendPacket ) == FALSE)
					{
						LOGINFO("MouseInputHandler::OnLClickedPosition 网络发送失败");
					}

					bResult = theHero.SendPacket( &SendPacket, sizeof(MSG_CG_SYNC_MOVE_SYN) );
					if( bResult )
					{
						if(theGeneralGameParam.IsHeroActionAtServerSync())
							theHero.StopMove();
						else
							theGameUIManager.SetMovePointer(TRUE,*pClickedPosition);
					}
					
					return bResult;	
				}
				else
				{
					if (theHero.GetCurrentState()==STATE_MOVE)
					{					
						// STOP
						theHero.StopAtServer();
					}

					theHero.SetNextState(STATE_IDLE,g_CurTime);
					return TRUE;
				}
			}
		}
	}

	return FALSE;
}

//------------------------------------------------------------------------------
/**
*/
BOOL MouseInputHandler::OnRClickedCharacter(Character* /*pObj*/, eRELATION_TYPE /*relationKind*/)
{

/*	float fDistance;
	PLAYER_ACTION action;
	Vector3D vPlayerPos = theHero.GetPosition();
	Vector3D vOffset;
	Vector3D vTargetPos;

	ZeroMemory(&action,sizeof(action));
	action.ActionID = ACTION_IDLE;

	// 秦具且 青悼阑 搬沥茄促!
	if (relationKind == RELATION_ENEMY)
	{
		if (!pObj->CanBeAttackTarget())
		{
			return FALSE;
		}				

		// 利措利 胶懦捞搁!!
		if (1)
		{			
			ZeroMemory(&action,sizeof(action));
			action.ActionID = ACTION_SKILL;
			action.SKILL.dwSkillID = m_dwCurSkillID;
			action.SKILL.dwTargetID = pObj->GetObjectKey();
			vTargetPos = pObj->GetPosition();		
		}
		else
		{
			return FALSE;
		}
		
	}*/			
	return FALSE;
}

//------------------------------------------------------------------------------
/**
*/
BOOL MouseInputHandler::OnRClickedPosition(Vector3C* /*pClickedPosition*/)
{
	return FALSE;
}


//------------------------------------------------------------------------------
/**
*/
BOOL MouseInputHandler::OnStabberLClickedCharacter(Character* pObj)
{
	//TCHAR	szMessage[MAX_UITEXT_LENGTH];


	m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());

	if (singleton::ExistHero()) 
	{
		if (theHero.GetShow()) 
		{
			theGameUIManager.UISetData	(gameui::eUITarget
												,gameui::eSetPlayerID
												,pObj->GetObjectKey());
			//g_InterfaceManager.ShowWindow(InterfaceManager::DIALOG_TARGET, TRUE);
		}
	}

	float fDistance = 0.0f;
	PLAYER_ACTION action;
	Vector3D vPlayerPos = theHero.GetPosition();
	Vector3D vOffset;
	Vector3D vTargetPos;

	ZeroMemory(&action,sizeof(action));
	action.ActionID = ACTION_IDLE;//咀记 惑怕 檬扁拳 	



	eRELATION_TYPE relationKind = GameUtil::GetRelationOfHero(pObj);
	//	傍拜 惑怕 汲沥

	if (theHero.GetCurrentState() == STATE_SKILL)
	{
		return TRUE;
	}

	if (relationKind == RELATION_ENEMY)
	{
		if (!pObj->CanBeAttackTarget())
		{
			return FALSE;
		}

		if (m_pHeroActionInput->IsCannotAttack())
		{
			return FALSE;
		}

		// 棱腮仇 肚努腐
//		if ( m_pHeroActionInput->GetCurrentTarget() == pObj->GetObjectKey())
		{
			m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());
//			if( theHero.GetCurrentState() == STATE_IDLE )
			if( m_pHeroActionInput->IsIdleState() ) 
			{
				theHero.SetDirection(pObj->GetPosition(),NULL,TRUE);
			}
			else if (theHero.GetCurrentState() == STATE_MOVE)
			{
				theHero.StopMove();
				theHero.StopAtServer();

				theHero.SetDirection(pObj->GetPosition(),NULL,TRUE);
			}

			// 狐规!
			m_pHeroActionInput->SwitchAutoAttack(TRUE);
			m_pHeroActionInput->SetCurrentTarget( pObj->GetObjectKey() );


			// 掉饭捞啊 巢疽栏搁 给金促. 楷荤仿
			if (theHero.GetStabberShotDelay() > STABBER_SHOT_DELAY_MARGIN)
			{
				return TRUE;
			}
			// 芭府啊 逞 钢绢档 给金促.
			Vector3D vPos = theHero.GetPosition();
			Vector3D vDest = pObj->GetPosition();
			Vector3D vDiff = vDest - vPos;

			if (theGameOption.GetAutoMove())
			{
				if (vDiff.Length() > m_pHeroActionInput->GetAttackActionDistance())
				{
					// 芭府啊 呈公钢搁 捞悼~
					action.ActionID = ACTION_IDLE;
					action.ATTACK.dwTargetID = m_pHeroActionInput->GetCurrentTarget();
					m_pHeroActionInput->PushQueueAction(action);

					m_pHeroActionInput->MoveToAction(vDest,pObj->GetPathExplorer()->GetTile());
					return TRUE;
				}
			}
			else
			{
				if (vDiff.Length() > m_pHeroActionInput->GetAttackActionDistance())
				{
					m_pHeroActionInput->SwitchAutoAttack( FALSE );
					action.ActionID				= ACTION_IDLE;
					action.ATTACK.dwTargetID	= m_pHeroActionInput->GetCurrentTarget();
					m_pHeroActionInput->PushQueueAction(action);


					OUTPUTCHAT(TEXTRES_SKILL_FAIL_NOT_ENOUGH_DISTANCE );
					return TRUE;
				}
			}

			if( theHero.IsMoving() )
			{
				// STOP
				theHero.StopMove();
				theHero.StopAtServer();

			}

			m_pHeroActionInput->StabberShot();
			return TRUE;
		}
/*
		else
		{
			m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());
			if (theHero.GetCurrentState() == STATE_IDLE) 
			{
				theHero.SetDirection(pObj->GetPosition());
			}
			else if (theHero.GetCurrentState() == STATE_MOVE)
			{
				theHero.StopMove();
				MSG_CG_SYNC_STOP_SYN packet;
				packet.m_byCategory = CG_SYNC;
				packet.m_byProtocol = CG_SYNC_STOP_SYN;				
				packet.vCurPos     = theHero.GetPosition();
				theHero.SendPacket(&packet,sizeof(MSG_CG_SYNC_STOP_SYN));	
				theHero.SetDirection(pObj->GetPosition());
			}
			return TRUE;
		}
*/
	}
	else if (relationKind == RELATION_ASSISTANT)
	{		
		vTargetPos = pObj->GetPosition();
		ZeroMemory(&action,sizeof(action));
		action.ActionID = ACTION_NPCMEET;						
		action.NPCMEET.dwNPCKey = pObj->GetObjectKey();	
		Vector3D vDistance = vTargetPos - theHero.GetPosition();
		fDistance = vDistance.Length();

		m_pHeroActionInput->SwitchAutoAttack(FALSE);

//		return TRUE;
	}
    else if (relationKind == RELATION_VENDOR)
    {		
        vTargetPos = pObj->GetPosition();
        ZeroMemory(&action,sizeof(action));
        action.ActionID = ACTION_VENDORMEET;						
        action.VENDORMEET.dwTargetID = pObj->GetObjectKey();	
        Vector3D vDistance = vTargetPos - theHero.GetPosition();
        fDistance = vDistance.Length();

		m_pHeroActionInput->SwitchAutoAttack(FALSE);
    }
	else if (relationKind == RELATION_FRIEND)
	{
		vTargetPos = pObj->GetPosition();
		ZeroMemory(&action,sizeof(action));
		action.ActionID = ACTION_PLAYERMEET;						
		action.PLAYERMEET.dwTargetID = pObj->GetObjectKey();

		// 磊悼 傍拜 掺扁
		m_pHeroActionInput->SwitchAutoAttack(FALSE);
	}


	// 秦具且青悼捞 绝栏搁 嫐妨模促.
	if (action.ActionID == ACTION_IDLE)
	{
		return FALSE;
	}

	vOffset = vTargetPos - vPlayerPos;
	fDistance = vOffset.Length();

	m_pHeroActionInput->PushQueueAction(action);


	// 芭府啊 呈公钢搁 捞悼~
	if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
	{


		if( m_pHeroActionInput->MoveToAction(vTargetPos,pObj->GetPathExplorer()->GetTile()) == FALSE )
		{
			//	鸥百捞 MapNPC老版快父 促矫 茄锅 歹 拌魂
			if (relationKind == RELATION_ASSISTANT)
			{
				//	MapNPC客 唱 荤捞狼 父朝 荐 乐绰 弥措 芭府焊促 距埃 歹 埃 困摹甫 逞败拎辑 促矫 茄锅 歹 拌魂
				Vector3D vDir = theHero.GetPosition() - vTargetPos;
				vDir.Normalize();

				float		fDISTANCE_NPC_MEET	= theGeneralGameParam.GetDistanceNpcMeet();
				float		fDISTANCE_NPC_MEET_EXT	= theGeneralGameParam.GetPercentDistanceNpcMeet();

				Vector3D			TargetPos = vTargetPos + ( vDir * (fDISTANCE_NPC_MEET * fDISTANCE_NPC_MEET_EXT) );
				PathHandler*	pPE = NULL;

				pPE = ( PathHandler* )theTileWorldClient.CreatePathHandler(PathHandler_NULL);
				if( pPE )
				{
					theTileMap.PathHandleReset( pPE, TargetPos );
					if( m_pHeroActionInput->MoveToAction(TargetPos,pPE->GetTile()) == FALSE )
					{
						if(pPE)
						{
							theTileWorldClient.DestroyPathHandler( pPE );
							pPE = NULL;
						}
						return TRUE;
					}

					m_pHeroActionInput->GetQueueAction().ActionID = ACTION_NPCMEET;

					if(pPE)
					{
						theTileWorldClient.DestroyPathHandler( pPE );
						pPE = NULL;
					}
				}
			}
		}
		return TRUE;		
	}


/*
	// 芭府啊 呈公钢搁 捞悼~
	if( fDistance > m_pHeroActionInput->GetQueueActionDistance() )
	{
		m_pHeroActionInput->MoveToAction(vTargetPos,pObj->GetPathExplorer()->GetTile());		
		return TRUE;
	}
*/

	return TRUE;
}

//------------------------------------------------------------------------------
/**
*/
BOOL MouseInputHandler::OnStabberLDragedCharacter(Character* pObj)
{

	eRELATION_TYPE relationKind = GameUtil::GetRelationOfHero(pObj);
	//	傍拜 惑怕 汲沥
	if (relationKind == RELATION_ENEMY)
	{
		if (!pObj->CanBeAttackTarget())
		{
			return FALSE;
		}
		// 棱腮仇 肚努腐
		if ( m_pHeroActionInput->GetCurrentTarget() == pObj->GetObjectKey())
		{
			// 掉饭捞啊 巢疽栏搁 给金促. 楷荤仿
			if (theHero.GetStabberShotDelay() > STABBER_SHOT_DELAY_MARGIN)
			{
				return TRUE;
			}
			// 芭府啊 逞 钢绢档 给金促.
			Vector3D vPos = theHero.GetPosition();
			Vector3D vDest = pObj->GetPosition();
			Vector3D vDiff = vDest - vPos;

			if (vDiff.Length() > m_pHeroActionInput->GetAttackActionDistance())
			{
				return FALSE;
			}



			m_pHeroActionInput->StabberShot();
			return TRUE;
		}
		else
		{
			m_pHeroActionInput->SetCurrentTarget(pObj->GetObjectKey());
			if (theHero.GetCurrentState() == STATE_IDLE) 
			{
				theHero.SetDirection(pObj->GetPosition());
			}
			else if (theHero.GetCurrentState() == STATE_MOVE)
			{
				theHero.StopMove();
				theHero.StopAtServer();

				theHero.SetDirection(pObj->GetPosition());
			}
			return TRUE;
		}
	}
	

	return FALSE;
}

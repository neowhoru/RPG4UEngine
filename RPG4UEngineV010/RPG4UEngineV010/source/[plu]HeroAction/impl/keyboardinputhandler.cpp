/*////////////////////////////////////////////////////////////////////////
文 件 名：herokeyboardinputlayer.cpp
创建日期：2007年10月8日
最后更新：2007年10月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "KeyboardInputHandler.h"
#include "HeroActionInput.h"
#include "Item.h"
#include "Hero.h"
#include "HeroData.h"
#include "MapObject.h"
#include "GameUtil.h"
#include "ConstTextRes.h"

#include "InputLayer.h"
#include "Camera.h"
#include "MouseHandler.h"
#include "VUICursorManager.h"
#include "TileWorldClient.h"
#include "PathHandler.h"
#include "HeroTipLayer.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "SceneDefine.h"
#include "QuickContainer.h"
#include "ApplicationSetting.h"
#include "GameParameter.h"
#include "GameUIManager.h"
#include "GameUtil.h"
#include "VillageScene.h"
#include "VHeroActionInput.h"

using namespace input;
using namespace scene;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(KeyboardInputHandler, ()  , gamemain::eInstPrioGameFunc);

#define		FREE_CAMERA_DEFAULT_SPEED			4.0f
#define		FREE_CAMERA_SPEED_UP					0.2f
#define		FREE_CAMERA_MIN_SPEED_UP			0.2f
#define		FREE_CAMERA_MAX_SPEED_UP			50.0f
static	float		g_fFreeCameraSpeed = FREE_CAMERA_DEFAULT_SPEED;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
KeyboardInputHandler::KeyboardInputHandler()
{
	ResetSpecialMove();
}
KeyboardInputHandler::~KeyboardInputHandler()
{
}

//------------------------------------------------------------------------------
void KeyboardInputHandler::ResetSpecialMove()
{
	memset(m_dwSpecialMovePreTime,0,sizeof(DWORD)*SPECIAL_MOVE_MAX);
	memset(m_bSpecialMoveFirstDown,0,sizeof(BOOL)*SPECIAL_MOVE_MAX);
	if(m_pHeroActionInput)
		m_pHeroActionInput->SetSpecialMove(FALSE);
}


BOOL	KeyboardInputHandler::_ProcessSpecialMove	(const Vector3D& vMoveDir)
{
	BOOL  bActive;
	float fdistance;
	DWORD dwSkillId;
	BOOL	bUse;

	dwSkillId	= m_pHeroActionInput->GetSkillInfo(SKILLCODE_TELEPORT,bActive,fdistance);
	bUse			= m_pHeroActionInput->CanUseSpecialMove();

	if(dwSkillId && bUse)
	{
		if(bActive)
		{
			float		fDistance;
			Vector3D vDest;
			Vector3D vDirection	=  vDirection + vMoveDir;
			Vector3D vOffset		= (vDirection * fdistance*0.85f);

			vOffset	+= theHero.GetPosition();
			vDest		=  m_pHeroActionInput->GetCanMovePos(vOffset);
			vOffset	=  vDest - theHero.GetPosition();

			fDistance = vOffset.Length();

			m_pHeroActionInput->SetDest(vDest);

			if(fDistance==0.0f)
			{
				OUTPUTCHAT(TEXTRES_NOT_USE_TELEPORT_SKILL );
				return FALSE;
			}


			m_pHeroActionInput->SetCurrentSkillID	(dwSkillId);
			m_pHeroActionInput->SetSpecialMove		(TRUE);
			m_pHeroActionInput->UseSpecialMoveSkill(dwSkillId);

			return FALSE;
		}
		else
			LOGINFO("无可用技能.\n");
	}
	else
	{
		LOGINFO("无可用技能.\n");
	}

	return FALSE;
}


void	KeyboardInputHandler::ProcessQuickKey()
{
	if(theApplicationSetting.m_ShowAdvancedMiniMap)
		return;

	if ( theInputLayer.IsPressShiftKey() )
	{
		_ProcessStyleQuickKey();
		return;
	}

	if(theInputLayer.IsKeyDown(KEYC_SPACE))
	{
		//theVHeroActionInput.BeginJumping();
		theVHeroActionInput.SendJumpMsg();
	}
	if(theInputLayer.IsKeyUp(KEYC_SPACE))
	{
		theVHeroActionInput.SetJumpingState(FALSE);
	}


	if ( theInputLayer.IsPressAnyExtKey() == FALSE )
	{
		BOOL	bProcessKey = FALSE;
		// 处理 1234567890-=
		for(DWORD key=KEYC_1; key<=KEYC_8; key++)
		{
			if ( theInputLayer.GetState( (eKEY_CODE)key, KS_UP))
			{
				bProcessKey = TRUE;
				theMouseHandler.DoItemUsing(SI_QUICK, (SLOTPOS)(key-KEYC_1));
				break;
			}
		}//for(DWORD key=KEYC_1; key<=KEYC_EQUALS; key++)

		if(!bProcessKey)
		{
			for(DWORD key=KEYC_F1; key<=KEYC_F8; key++)
			{
				if ( theInputLayer.GetState( (eKEY_CODE)key, KS_UP))
				{
					bProcessKey = TRUE;
					theMouseHandler.DoItemUsing(SI_QUICK, (SLOTPOS)(key-KEYC_F1) + 8);
					break;
				}
			}//for(DWORD key=KEYC_1; key<=KEYC_EQUALS; key++)
		}

	}//if ( theInputLayer.IsPressAnyExtKey() == FALSE )
}



BOOL	KeyboardInputHandler::ProcessCamera()
{
	BOOL bUpdate	= FALSE;
	BOOL bShift		= theInputLayer.IsPressShiftKey();

	int	iKeyRotateSpeed = (INT)theGeneralGameParam.GetCameraCtrlRotationSpeed();
	/// ; 符处理 左转相机
	if(	theInputLayer.IsKeyPress( KEYC_SEMICOLON) 
		||	theInputLayer.IsKeyPress( KEYC_END) )
	{
		theCamera.Rotate(!bShift?0:iKeyRotateSpeed
							,!bShift?iKeyRotateSpeed:0
							,Camera::CAMERA_ROTATE_RMOUSE);
		bUpdate = TRUE;
	}

	/// ' 符处理 右转相机
	else if( theInputLayer.IsKeyPress( KEYC_APOSTROPHE )
			||	theInputLayer.IsKeyPress( KEYC_HOME ))
	{
		theCamera.Rotate(!bShift?0:-iKeyRotateSpeed
                     ,!bShift?-iKeyRotateSpeed:0
							,Camera::CAMERA_ROTATE_RMOUSE);
		bUpdate = TRUE;
	}

	/// 键盘移动时 
	else if( theHero.GetCurrentState() == STATE_KEYBOARDMOVE) 
	{
		theCamera.KeyboardModeInterpolateAngle(theHero.GetAngle());
		bUpdate = TRUE;
	}


	const int		iZoomSpeed = 120;
	/// , 符处理 
	if(  theInputLayer.GetState( KEYC_COMMA, KS_KEY ) )
	{
		theCamera.MoveFrontBack( iZoomSpeed );
		bUpdate = TRUE;
	}
	/// . 符处理 
	else if( theInputLayer.GetState( KEYC_PERIOD, KS_KEY ) )
	{
		theCamera.MoveFrontBack( -iZoomSpeed );
		bUpdate = TRUE;
	}


	return bUpdate;
}

BOOL	KeyboardInputHandler::ProcessCamera(BOOL bFreeCamera)
{
	if(!bFreeCamera)
		return FALSE;

	BOOL bUpdate = FALSE;
	Vector3D&	vCamera = m_pHeroActionInput->GetFreeCameraPos();

	if( theInputLayer.GetState( KEYC_F, KS_DOWN ) )
	{
		theCamera.MoveFrontBack( -9999 );
		m_pHeroActionInput->SetFreeCameraPos(theHero.GetPosition() );
		BOOL	bState = theGeneralGameParam.GetFreeCameraState() ^ TRUE;
		theGeneralGameParam.SetFreeCameraState( bState );

		bUpdate = TRUE;
	}

	if( theInputLayer.GetState( KEYC_RBRACKET, KS_DOWN ) )
	{
		g_fFreeCameraSpeed += FREE_CAMERA_SPEED_UP;
		if( g_fFreeCameraSpeed >= FREE_CAMERA_MAX_SPEED_UP )
		{
			g_fFreeCameraSpeed = FREE_CAMERA_MAX_SPEED_UP;
		}
		bUpdate = TRUE;
	}

	else if( theInputLayer.GetState( KEYC_LBRACKET, KS_DOWN ) )
	{
		g_fFreeCameraSpeed -= FREE_CAMERA_SPEED_UP;
		if( g_fFreeCameraSpeed <= FREE_CAMERA_MIN_SPEED_UP )
		{
			g_fFreeCameraSpeed = FREE_CAMERA_MIN_SPEED_UP;
		}
		bUpdate = TRUE;
	}

	/// w s a k q e 数键的控制
	if( theInputLayer.GetState( KEYC_W, KS_KEY ) )
	{
		vCamera -= (theCamera.GetDirection() * (theGeneralGameParam.GetFreeCameraSpeed() * g_fFreeCameraSpeed) );
		bUpdate = TRUE;
	}
	else if( theInputLayer.GetState( KEYC_S, KS_KEY ) )
	{
		vCamera += (theCamera.GetDirection() * (theGeneralGameParam.GetFreeCameraSpeed() * g_fFreeCameraSpeed) );
		bUpdate = TRUE;
	}
	if(  theInputLayer.GetState( KEYC_A, KS_KEY ) )
	{
		theCamera.RotateYaw( theGeneralGameParam.GetFreeCameraTurnSpeed() );
		bUpdate = TRUE;
	}
	else if( theInputLayer.GetState( KEYC_D, KS_KEY ) )
	{
		theCamera.RotateYaw( -theGeneralGameParam.GetFreeCameraTurnSpeed() );
		bUpdate = TRUE;
	}
	if( theInputLayer.GetState( KEYC_Q, KS_KEY ) )
	{
		Vector3D RightVector;
		Vector3D UpVector;
		UpVector.Set( 0.0f, 0.0f, 1.0f );
		RightVector = theCamera.GetDirection().CrossProduct(UpVector);
		RightVector.Normalize();
		vCamera += (RightVector * (theGeneralGameParam.GetFreeCameraSpeed() * g_fFreeCameraSpeed) );
		bUpdate = TRUE;
	}
	else if( theInputLayer.GetState( KEYC_E, KS_KEY ) )
	{
		Vector3D RightVector;
		Vector3D UpVector;
		UpVector.Set( 0.0f, 0.0f, 1.0f );
		RightVector = theCamera.GetDirection().CrossProduct(UpVector);
		RightVector.Normalize();
		vCamera -= (RightVector * (theGeneralGameParam.GetFreeCameraSpeed() * g_fFreeCameraSpeed) );
		bUpdate = TRUE;
	}
	//if( theInputLayer.GetState( KEYC_LCONTROL, KS_KEY ) || theInputLayer.GetState( KEYC_RCONTROL, KS_KEY ) )
	//{
	//	theCamera.MoveFrontBack( -24000 );
	//}
	//else
	//{
	//	theCamera.MoveFrontBack( 200 );
	//}

	//if(bUpdate)
	//{
	//	theCamera.SetCamera( &vCamera, FALSE );
	//	theCamera.UpdateListeners();
	//}

	return bUpdate;
}


void KeyboardInputHandler::_ProcessStyleQuickKey()
{
	for(DWORD key=KEYC_1; key<=KEYC_4; key++)
	{
		if ( theInputLayer.GetState( (eKEY_CODE)key, KS_UP))
		{
			theMouseHandler.DoItemUsing(SI_STYLE, (SLOTPOS)(key-KEYC_1));
			break;
		}
	}//for(DWORD key=KEYC_1; key<=KEYC_4; key++)
}


BOOL KeyboardInputHandler::ProcessOptionKey()
{
	///M切换地图
	if (	theInputLayer.GetState(KEYC_M, KS_DOWN))
	{
		m_pHeroActionInput->ToggleAdvancedMap();
	}

	if(theApplicationSetting.m_ShowAdvancedMiniMap)
		return TRUE;


	if (	theInputLayer.GetState(KEYC_F11, KS_DOWN) 
		&& theInputLayer.IsPressAnyExtKey() == FALSE)
	{
		m_pHeroActionInput->SetIgnorePlayer(!m_pHeroActionInput->IsIgnorePlayer());
	}

	///R切换自动跑
	if (theInputLayer.GetState(KEYC_R, KS_DOWN))
	{
		BOOL temp = !m_pHeroActionInput->IsAutoMove();
		m_pHeroActionInput->SwitchAutoMove(temp);
		m_pHeroActionInput->SetFollowState( FALSE );
	}

	///X切换攻击状态
	if (	theInputLayer.GetState(KEYC_X, KS_DOWN))
	{
		m_pHeroActionInput->ToggleAttackState();
	}





	///切换名字显示
	if (	theInputLayer.GetState(KEYC_SLASH, KS_DOWN))
	{
		theGameUIManager.ToggleShowName();
	}


	///切换目标 Tab, [, ] 三键
	if(	theInputLayer.GetState(KEYC_TAB, KS_DOWN )	
		|| theInputLayer.GetState(KEYC_LBRACKET, KS_DOWN) 
		|| theInputLayer.GetState(KEYC_RBRACKET, KS_DOWN) )
	{
		if( theInputLayer.IsPressAnyExtKey() == FALSE )
		{
			BOOL	bUpdateTargetList = FALSE;

			float		fDISTANCE_TAB_TARGET	= theGeneralGameParam.GetDistanceTabTarget();
			if( m_pHeroActionInput->UpdateTargetList( fDISTANCE_TAB_TARGET, TRUE ) == TRUE )
			{
				bUpdateTargetList = TRUE;
			}
			else if( m_pHeroActionInput->UpdateTargetList( fDISTANCE_TAB_TARGET, FALSE ) == TRUE )
			{
				bUpdateTargetList = TRUE;
			}
			else if( m_pHeroActionInput->UpdateTargetList( (fDISTANCE_TAB_TARGET * 1.5f), FALSE ) == TRUE )
			{
				bUpdateTargetList = TRUE;
			}
			else
			{
				OUTPUTCHATIF(TEXTRES_NOT_EXIST_TARGET 
								,theFrameworkSystem.GetCurrentScene()->GetType() != SCENE_TYPE_VILLAGE);
			}


			if( bUpdateTargetList )
			{
				if( theInputLayer.GetState(KEYC_TAB, KS_DOWN ))
				{
					if( FALSE == m_pHeroActionInput->OnNextTarget() )
					{
					}
				}
				else if( theInputLayer.GetState(KEYC_LBRACKET, KS_DOWN ))
				{
					if( FALSE == m_pHeroActionInput->OnPrevTarget() )
					{
					}
				}
				else if( theInputLayer.GetState(KEYC_RBRACKET, KS_DOWN ))
				{
					if( FALSE == m_pHeroActionInput->OnNextTarget() )
					{
					}
				}
			}//if( bUpdateTargetList )
		}
	}

	//if (theInputLayer.GetState(KEYC_T, KS_DOWN))
	//{
	//	if( theInputLayer.IsPressAnyExtKey() == FALSE )
	//	{
	//		StabberReload();
	//	}
	//}



	if(	theInputLayer.GetState( KEYC_F1, KS_DOWN ) 
		&& (	theInputLayer.GetState( KEYC_LSHIFT, KS_KEY ) 
			|| theInputLayer.GetState( KEYC_RSHIFT, KS_KEY ) ) )
	{
		m_pHeroActionInput->SetCurrentTarget( theHero.GetSummonId() );
	}

	return FALSE;
}


//------------------------------------------------------------------------------
BOOL KeyboardInputHandler::ProcessKeyboardMove()
{
	if (!singleton::ExistHero())
		return FALSE;

	//DWORD dwCurTick			= g_CurTime;//g_pRendererPtr->x_Clock.GetCurrentTick();

	BOOL bLeftStepPressed	= FALSE;
	BOOL bRightStepPressed	= FALSE;
	BOOL bFowardPressed		= FALSE;

	BOOL bBackwardPressed	= FALSE;
	BOOL bLeftTurnPressed	= FALSE;
	BOOL bRightTurnPressed	= FALSE;

	//BOOL bLeftSpecialMovePressed = FALSE;
	//BOOL bRightSpecialMovePressed = FALSE;
	//BOOL bFowardSpecialMovePressed = FALSE;
	//BOOL bBackwardSpecialMovePressed = FALSE;

	theHero.ResetMoveFlag();

	if (theHero.CannotMove())
	{
		return FALSE;
	}

	if(m_pHeroActionInput->IsAutoMove())
	{
		if(theInputLayer.GetState( MOUSE_RBUTTON, KS_MOUSE_KEY ))
		{
			const		sMOUSE_INFO& info	= theInputLayer.GetMouseBuffer();
			if(info.lX < 0)
				theHero.AddMoveFlag(KEYBOARDMOVE_LEFTTURN);
			else if(info.lX > 0)
				theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTTURN);
		}
	}


	m_pHeroActionInput->SetKeyBoardMoveCheck(FALSE);

	////////////////////////////////////
	/// A Left键处理
	if(	theInputLayer.IsKeyPress(KEYC_A, FALSE) 
		|| theInputLayer.IsKeyPress(KEYC_LEFT) )
	{
		bLeftTurnPressed = TRUE;
	}
	/// D Right键处理
	else if	(	theInputLayer.IsKeyPress(KEYC_D, FALSE) 
				||	theInputLayer.IsKeyPress(KEYC_RIGHT) )
	{
		bRightTurnPressed = TRUE;			
	}

	////////////////////////////////////
	/// W Up键处理
	if(	theInputLayer.IsKeyPress(KEYC_W, FALSE) 
		|| theInputLayer.IsKeyPress(KEYC_UP) )
	{
		//DWORD diff=dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_FORWORD];

		bFowardPressed = TRUE;

		m_pHeroActionInput->SwitchAutoMove(FALSE);
		m_pHeroActionInput->SetFollowState( FALSE );

		if( m_pHeroActionInput->IsShootingMode() == FALSE )
		{
			m_pHeroActionInput->SwitchAutoAttack(FALSE);
		}
		else
		{
			if( theHero.GetCurrentState() == STATE_MOVE )
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
			}
		}
	}
	////////////////////////////////////
	/// S Down键处理
	else if	(	theInputLayer.IsKeyPress(KEYC_S, FALSE) 
				||	theInputLayer.IsKeyPress(KEYC_DOWN) )
	{
		bBackwardPressed = TRUE;
		
		m_pHeroActionInput->SwitchAutoMove(FALSE);
		m_pHeroActionInput->SetFollowState( FALSE );

		if( m_pHeroActionInput->IsShootingMode() == FALSE )
		{
			m_pHeroActionInput->SwitchAutoAttack(FALSE);
		}
		else
		{
			if( theHero.GetCurrentState() == STATE_MOVE )
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
			}
		}
	}

	////////////////////////////////////
	/// Q Del键处理
	if	(	theInputLayer.IsKeyPress(KEYC_Q, FALSE) 
		||	theInputLayer.IsKeyPress(KEYC_DELETE) )
	{
		//DWORD diff = dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_LEFT];

			bLeftStepPressed = TRUE;
	}

	////////////////////////////////////
	/// E PGDN键处理
	else if	(	theInputLayer.IsKeyPress(KEYC_E, FALSE) 
				||	theInputLayer.IsKeyPress(KEYC_PGDN) )
	{
		//DWORD diff=dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_RIGHT];

		bRightStepPressed = TRUE;
	}


	////////////////////////////////////////////////
	//增加控制标志...
	if (	bRightStepPressed 
		|| bLeftStepPressed 
		|| bFowardPressed
		|| bBackwardPressed 
		|| bLeftTurnPressed 
		|| bRightTurnPressed) 
	{
		m_pHeroActionInput->InitAction();
		theSkillQueueManager.ClearAll();
		m_pHeroActionInput->SwitchAreaSkill(FALSE);
		m_pHeroActionInput->SetKeyBoardMoveCheck(TRUE);

		if (	m_pHeroActionInput->IsIdleState()
			|| theHero.GetCurrentState() == STATE_MOVE )
		{
//			theHero.StopMove();
			theHero.SetNextState(STATE_KEYBOARDMOVE,g_CurTime);
		}

		if (bLeftTurnPressed) 
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_LEFTTURN);
		}
		else if (bRightTurnPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTTURN);
		}
	
		if (bFowardPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_FOWARD);
		}
		else if (bBackwardPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_BACKWARD);
		}
		
		if (bRightStepPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTSTEP);
		}
		else if (bLeftStepPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_LEFTSTEP);
		}

		return TRUE;
	}

	return FALSE;
}



void    KeyboardInputHandler::ProcessKeyboardAttack()
{
	if (theGeneralGameParam.GetSpecialMode() == 3)
	{
		if( m_pHeroActionInput->IsShootingMode() == FALSE )
		{
			if( m_pHeroActionInput->IsKeyboardQueueAttack() )
			{
				if (m_pHeroActionInput->AttackNearestTarget())
					return;
			}

			if (theInputLayer.GetState(KEYC_SPACE, KS_DOWN ))
			{
				if (m_pHeroActionInput->AttackNearestTarget())
				{
					return;
				}
			}
		}//if( m_pHeroActionInput->IsShootingMode() == FALSE )
	}
}


//------------------------------------------------------------------------------
BOOL KeyboardInputHandler::_ProcessKeyInput_GM()
{
	__CHECK(theGeneralGameParam.GetSpecialMode());


	//if (theInputLayer.GetState( KEYC_LCONTROL, KS_KEY ))
	//{
	//	if( theInputLayer.GetState( KEYC_F12, KS_DOWN) 
	//		|| theInputLayer.GetState( KEYC_F12, IKS_DOWN ) )
	//	{
	//		theMap.SetUseAutoCamera(m_bUseAutoCamera ^= TRUE);
	//		if (singleton::ExistHero())
	//		{
	//			theMap.SetAutoCameraTargetObjectKey( theHero.GetObjectKey() );
	//		}
	//	}
	//}

	//GM 

	if(	theInputLayer.GetState( KEYC_Z, KS_DOWN ) 
		&& theInputLayer.GetState( KEYC_LCONTROL, KS_DOWN ) )
	{
		static DWORD dwMonsCode = 30;
		GameUtil::SpawnMonster(dwMonsCode);
	}		

	if ( theInputLayer.GetState(KEYC_LCONTROL, KS_KEY))
	{
		if (theInputLayer.GetState(KEYC_1, KS_DOWN))
			GameUtil::SpawnMonster(1);
		else if (theInputLayer.GetState(KEYC_2, KS_DOWN))
			GameUtil::SpawnMonster(3);
		else if (theInputLayer.GetState(KEYC_3, KS_DOWN))
			GameUtil::SpawnMonster(6);
		else if (theInputLayer.GetState(KEYC_4, KS_DOWN))
			GameUtil::SpawnMonster(8);
		else if (theInputLayer.GetState(KEYC_5, KS_DOWN))
			GameUtil::SpawnMonster(10);
		else if (theInputLayer.GetState(KEYC_6, KS_DOWN))
			GameUtil::SpawnMonster(11);
		else if (theInputLayer.GetState(KEYC_7, KS_DOWN))
			GameUtil::SpawnMonster(13);
		else if (theInputLayer.GetState(KEYC_8, KS_DOWN))
			GameUtil::SpawnMonster(18);
		else if (theInputLayer.GetState(KEYC_9, KS_DOWN))
			GameUtil::SpawnMonster(20);
		else if (theInputLayer.GetState(KEYC_0, KS_DOWN))
			GameUtil::SpawnMonster(24);
		else if (theInputLayer.GetState(KEYC_U, KS_DOWN))
			GameUtil::SpawnMonster(5);
		else if (theInputLayer.GetState(KEYC_Y, KS_DOWN))
			GameUtil::SpawnMonster(15);
	}



	//if( theGeneralGameParam.GetSpecialMode() )
	//{
	//	if ( theInputLayer.GetState(KEYC_SLASH, KS_DOWN) )
	//	{
	//		if( theInputLayer.GetState( KEYC_LCONTROL, KS_KEY) )
	//		{
	//			static BOOL bFlag = FALSE;
	//			bFlag = !bFlag;
	//			MSG_CG_GM_OBSERVER_SYN SendPacket;
	//			SendPacket.m_byCategory = CG_GM;
	//			SendPacket.m_byProtocol = CG_GM_OBSERVER_SYN;
	//			SendPacket.m_dwKey =	theGeneralGameParam.GetUserID();
	//			SendPacket.m_byObserverOn = !bFlag;
	//			theNetworkLayer.SendPacket(CK_GAMESERVER, &SendPacket, sizeof(SendPacket));
	//			theHero.SetShow(bFlag);
	//		}
	//	}
	//}


	if( theInputLayer.GetState( KEYC_LCONTROL, KS_KEY ) )
	{
		//if (theInputLayer.GetState( KEYC_N, KS_DOWN ))
		//{
		//	g_bShowMonsterInfo = !g_bShowMonsterInfo;
		//}
		//else if (theInputLayer.GetState( KEYC_G, KS_DOWN))
		//{
		//	//bgm on/off
		//	static BOOL bBGMSound = FALSE;
		//	bBGMSound  = !bBGMSound ;
		//	theSoundBGM.Stop();
		//	theSound.SetUseBGMSound(bBGMSound );
		//}
		//else if (theInputLayer.GetState( KEYC_E, KS_DOWN ))
		//{
		//	//effect on/off
		//	static BOOL bEffectSound = FALSE;
		//	bEffectSound = !bEffectSound;
		//	theSound.SetUseEffectSound(bEffectSound);
		//}
	}




	return TRUE;
}
/*////////////////////////////////////////////////////////////////////////
文 件 名：GameActionHandler.cpp
创建日期：2007年1月21日
最后更新：2007年1月21日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameActionHandler.h"
#include "HeroActionInput.h"
#include "Item.h"
#include "Hero.h"
#include "MapObject.h"
#include "GameUtil.h"
#include "ConstTextRes.h"

#include "InputLayer.h"
#include "Camera.h"
#include "MouseHandler.h"
#include "VUICursorManager.h"
#include "TileWorldClient.h"
#include "PathHandler.h"
#include "HeroTipLayer.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "MapNPC.h"
//#include "SkillInfoParser.h"
#include "WareHouseDialog.h"
#include "ShopDialog.h"
#include "ItemCompoundDialog.h"
#include "ScriptManager.h"
#include "ItemLogManager.h"
#include "GameParameter.h"
#include "PacketInclude.h"
#include "VendorDialog.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameActionHandler, ()  , gamemain::eInstPrioGameFunc);


	typedef BOOL (GameActionHandler::*FuncProcess)(void);
	struct SFuncProcess
	{
		DWORD				oprType;
		FuncProcess		pFunc;
	};

	static SFuncProcess gs_arFuncProcess[ACTION_MAX]=
	{
		{ACTION_IDLE			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_ATTACK			 , &GameActionHandler::ProcessQueueAttack},
		{ACTION_STYLE_ATTACK	 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_SKILL			 , &GameActionHandler::ProcessQueueSkill},
		{ACTION_MOVE			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_KEYBOARDMOVE	 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_KNOCKBACK		 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_TOAIR			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_DOWN			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_GETUP			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_NPCMEET		 , &GameActionHandler::ProcessQueueNPCMeet},
		{ACTION_GETITEM		 , &GameActionHandler::ProcessQueueGetItem},
		{ACTION_JUMP			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_EVENTJUMP		 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_STOP			 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_STABBERSHOT	 , &GameActionHandler::ProcessQueueShot},
		{ACTION_VENDORMEET    , &GameActionHandler::ProcessQueueVendorMeet},
		{ACTION_PLAYERMEET	 , &GameActionHandler::ProcessQueuePlayerMeet},
		{ACTION_SIT				 , &GameActionHandler::ProcessQueueDefault},
		{ACTION_SPECIAL_MOVE	 , &GameActionHandler::ProcessSpecialMove},
		{ACTION_USE_OBJECT	 , &GameActionHandler::ProcessQueueUseObject},
		{ACTION_PLAYERFOLLOW	 , &GameActionHandler::ProcessQueuePlayerFollow},
	};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameActionHandler::GameActionHandler()
{
	m_pPlayerAction = NULL;
}
GameActionHandler::~GameActionHandler()
{
}


//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueAction()
{
	//m_pPlayerAction = &playerAction;
	BOOL bRet = FALSE;

	if(theSkillQueueManager.Size()>0)
	{
		bRet = ProcessQueueSkill();
	}
	else
	{
		assert(	gs_arFuncProcess[m_pPlayerAction->ActionID].oprType == (DWORD)m_pPlayerAction->ActionID 
					&& "Opr类型不一样，请检查的gs_arFuncProcess定义与oprType的定义");
		
		FuncProcess fOpr = gs_arFuncProcess[m_pPlayerAction->ActionID].pFunc;

		bRet = (this->*fOpr)();


	}

	if (bRet)
	{
		m_pPlayerAction->ActionID = ACTION_IDLE;  
		return TRUE;
	}

	///更新状态数据
	if (	( m_pHeroActionInput->IsIdleState() && theHero.GetNextState() == STATE_MOVE) 
		|| theHero.GetCurrentState() == STATE_MOVE )
	{
		return FALSE;
	}

	if (	theHero.GetCurrentState() == STATE_ATTACK 
		&& m_pPlayerAction->ActionID == ACTION_ATTACK)
	{
		return FALSE;
	}

	if (	theHero.GetCurrentState() == STATE_ATTACK 
		&& m_pPlayerAction->ActionID == ACTION_SKILL)
	{
		return FALSE;
	}

	if (	theHero.GetCurrentState() == STATE_ATTACK 
		&& m_pPlayerAction->ActionID == ACTION_SPECIAL_MOVE)
	{
		return FALSE;
	}

	if (	theHero.GetStabberShotDelay() > 30 
		&& m_pPlayerAction->ActionID == ACTION_SKILL)
	{
		return FALSE;
	}


	if (	theHero.GetStabberShotDelay() > 30 
		&& m_pPlayerAction->ActionID == ACTION_SPECIAL_MOVE)
	{
		return FALSE;
	}

	
	m_pPlayerAction->ActionID = ACTION_IDLE;
	return FALSE;
}


BOOL GameActionHandler::ProcessQueueDefault()
{
	return FALSE;
}


//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueNPCMeet()
{
	MapNPC * pNPC = (MapNPC *)theObjectManager.GetObject(m_pPlayerAction->NPCMEET.dwNPCKey); 

	__CHECK_PTR(pNPC);
	__CHECK(singleton::ExistHero());

	Vector3D wzVec1	= pNPC->GetPosition() - theHero.GetPosition();
	float		fLength	= wzVec1.Length();

	__CHECK (fLength <= m_pHeroActionInput->GetQueueActionDistance());

	//////////////////////////////////////
	pNPC->DoAction			(m_pPlayerAction);	

	if(pNPC->IsLocalNPC())
	{
		if (theHero.GetCurrentState() == STATE_MOVE)
		{
			theHero.StopMove();
			pNPC->StopMove();
			//theHero.StopAtServer();
		}
		return OnProcessNpcMeet(theHero.GetObjectKey(),pNPC->GetObjectKey());
	}

	//////////////////////////////////////
	MSG_CG_MAP_MEETING_NPC_SYN	msgMetting;
	//msgMetting.m_dwObjectKey	= theHero.GetObjectKey();
	msgMetting.m_dwNPCKey		= pNPC->GetObjectKey();

	return theHero.SendPacket(&msgMetting,sizeof(msgMetting));
}

BOOL GameActionHandler::OnProcessNpcMeet(DWORD dwPlayerKey,DWORD dwNPCKey)
{
	Player * pPlayer	= (Player *)theObjectManager.GetObject(dwPlayerKey); 
	MapNPC * pNPC		= (MapNPC *)theObjectManager.GetObject(dwNPCKey); 

	__CHECK(pPlayer && pNPC);
	__CHECK(pPlayer->IsPlayer() && pNPC->IsMapNpc());


	//////////////////////////////////////////////////
	//执行客户端脚本
	if(pPlayer->IsHero())
	{
		////////////////////////////////////////////////////
		//未执行脚本的话，就执行正常Npc动作

		//theItemLogManager.EnableLog(TRUE);
		theItemLogManager.ClearLog();

		////////////////////////////////////////////////////
		if(!theGeneralGameParam.IsEnableNetwork())
		{
			if( !theHero.RunScript(0,pNPC->GetObjectKey()) )
				m_pHeroActionInput->ProcessNpcMeet(pNPC->GetObjectKey());
		}
		
		pNPC->ForceStateChange		(g_CurTime);
		pNPC->ProcessPlayerMeeting	(pPlayer);
		//pNPC->PlayMeetSound		();
	}
	else
	{
		pNPC->SetDirection		(pPlayer->GetPosition());
		pPlayer->SetDirection	(pNPC->GetPosition());
	}

	

	return TRUE;
}

//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueVendorMeet()
{
    Player * pPlayer = (Player *)theObjectManager.GetObject(m_pPlayerAction->VENDORMEET.dwTargetID); 

    if (pPlayer && singleton::ExistHero())
    {
        Vector3D wzVec1 = pPlayer->GetPosition() - theHero.GetPosition();
        Vector3D wzVec2;

        wzVec2 = -wzVec1;
        wzVec2.z = 0.0f;
        float fLength = wzVec1.Length();

        if (fLength <= m_pHeroActionInput->GetQueueActionDistance())
        {
            if (theHero.GetCurrentState() == STATE_MOVE)
				{
					theHero.StopMove();
					theHero.StopAtServer();
				}

				theVendorDialog.OpenVendor(TRUE);

            if ( !(wzVec2.x == 0.0f && wzVec2.y == 0.0f) )
            {
                float fAngle = wzVec2.GetAngleByACos();
                pPlayer->SetAngle(fAngle);
            }

            pPlayer->DoAction(m_pPlayerAction);	
            pPlayer->ForceStateChange(g_CurTime);

            return TRUE;
        }
    }

    return FALSE;
}

BOOL GameActionHandler::ProcessQueueUseObject()
{
    MapObject * pObject = (MapObject *)theObjectManager.GetObject(m_pPlayerAction->USEOBJECT.dwTargetID); 

    if (pObject && singleton::ExistHero())
    {
        Vector3D wzVec1 = pObject->GetPosition() - theHero.GetPosition();
        Vector3D wzVec2;
        wzVec2 = -wzVec1;
        wzVec2.z = 0.0f;
        float fLength = wzVec1.Length();

        if (fLength <= m_pHeroActionInput->GetQueueActionDistance())
        {
            if (theHero.GetCurrentState() == STATE_MOVE)
				{
					theHero.StopMove();
					theHero.StopAtServer();

				}
            
            return TRUE;
        }
    }

    return FALSE;
}


//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueuePlayerMeet()
{
    Player * pPlayer = (Player *)theObjectManager.GetObject(m_pPlayerAction->PLAYERMEET.dwTargetID); 

    if (pPlayer && singleton::ExistHero())
    {
        Vector3D wzVec1 = pPlayer->GetPosition() - theHero.GetPosition();
        Vector3D wzVec2;
        wzVec2 = -wzVec1;
        wzVec2.z = 0.0f;
        float fLength = wzVec1.Length();

        if (fLength <= m_pHeroActionInput->GetQueueActionDistance())
        {
            if (theHero.GetCurrentState() == STATE_MOVE)
				{
					theHero.StopMove();
					theHero.StopAtServer();

				}

            if (theMouseHandler.IsExistItemAtHand())
            {
					 assert(0 && " GetUserInterfaceManager[UIMAN_TRADE ");
#pragma message(__FILE__  "(824) GetUserInterfaceManager[UIMAN_TRADE  " )

            }

            return TRUE;
        }
    }

    return FALSE;
}


BOOL GameActionHandler::ProcessQueuePlayerFollow()
{
	Player * pPlayer = (Player *)theObjectManager.GetObject(m_pPlayerAction->PLAYERMEET.dwTargetID); 

	if (pPlayer && singleton::ExistHero())
	{
		Vector3D wzVec1 = pPlayer->GetPosition() - theHero.GetPosition();
		Vector3D wzVec2;

		wzVec2 = -wzVec1;
		wzVec2.z = 0.0f;
		float fLength = wzVec1.Length();

		//if (fLength <= DISTANCE_FOLLOW_PLAYER )
		if (fLength <= theGeneralGameParam.GetDistanceFollowPlayer() )
		{
			if (theHero.GetCurrentState() == STATE_MOVE)
			{
				theHero.StopMove();
				theHero.StopAtServer();
			}

			return TRUE;
		}
	}

	return FALSE;
}


//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueGetItem()
{
	Item * pItem = (Item *)theObjectManager.GetObject(m_pPlayerAction->GETITEM.dwItemKey); 

	if (pItem)
	{
		Vector3D wzVec1 = pItem->GetPosition() - theHero.GetPosition();
		Vector3D wzVec2;
		wzVec2 = -wzVec1;
		wzVec2.z = 0.0f;

		float fLength = wzVec1.Length();

		//if (fLength <= DISTANCE_SPACE_GET_ITEM)
		if (fLength <= theGeneralGameParam.GetDistanceGetItem())
		{
			if (theHero.GetCurrentState() == STATE_MOVE)
			{
				theHero.StopMove();
				theHero.StopAtServer();

			}

			switch(pItem->GetDropType())
			{
			case sITEMINFO_RENDER::eFIELDITEM_ITEM:
				{
					if( false == BIT_CHECK( theHero.m_dwPacketStatus, PACKET_STATUS_PICK_ITEM ) )
					{
						BIT_ADD( theHero.m_dwPacketStatus, PACKET_STATUS_PICK_ITEM );
						theItemManagerNet.SendPickupItem(pItem->GetObjectKey());
						pItem->SetSendPickPacket( TRUE );
					}
				}
				break;

			case sITEMINFO_RENDER::eFIELDITEM_MONEY:
				{
					if( false == BIT_CHECK( theHero.m_dwPacketStatus, PACKET_STATUS_PICK_MONEY ) )
					{
						BIT_ADD( theHero.m_dwPacketStatus, PACKET_STATUS_PICK_MONEY );
						theItemManagerNet.SendPickupMoney(pItem->GetObjectKey());
						pItem->SetSendPickPacket( TRUE );
					}
				}
				break;

			default:
				ASSERT("无效操作!!");
			}
			return TRUE;	
		}
	}
	return FALSE;
}


//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueAttack()
{
	Character * pEnemy = (Character *)theObjectManager.GetObject(m_pPlayerAction->ATTACK.dwTargetID);

	if (pEnemy)	
	{
		if (!pEnemy->CanBeAttackTarget())
		{
			return FALSE;
		}

		Vector3D wzVec1 = pEnemy->GetPosition() - theHero.GetPosition();
		float fLength = wzVec1.Length();

		if (fLength <= theHeroActionInput.GetAttackActionDistance())
		{
			if (theHero.CanDoQueueAction(m_pPlayerAction))
			{
				theHero.StopMove();	
				//theHero.SetComboCount(m_pHeroActionInput->m_iComboCount);
				theHero.SetTargetID(pEnemy->GetObjectKey());
				theHero.SetNextState(STATE_ATTACK,g_CurTime);
				theHero.SetCurrentAction(*m_pPlayerAction);
				
				return TRUE;
			}
		}
		else
		{
		}
	}
	else
	{
		return FALSE;
	}

	return FALSE;
}



//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessSpecialMove()
{
	if (theHero.CanDoQueueAction(m_pPlayerAction)) 
	{
		if (m_pHeroActionInput->IsShootingMode())
		{
			if (theHero.GetStabberShotDelay() > 30)
			{
				return FALSE;
			}
		}

		theHero.DoAction(m_pPlayerAction);
	
		if( m_pHeroActionInput->IsAutoMove())
		{
			if( singleton::ExistHero() )
			{
				m_pHeroActionInput->SwitchAutoMove(FALSE);
				m_pHeroActionInput->SetFollowState( FALSE );
				theHero.StopMove();
				theHero.StopAtServer();
			}
		}


		return TRUE;
	}

	return FALSE;
}

//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueSkill()
{

	DWORD dwSkillID	= theSkillQueueManager.Front();
	m_pHeroActionInput->SetCurrentSkillID(dwSkillID);

	if(theHero.GetCurrentState()==STATE_SKILL)
	{
		return FALSE;
	}

	m_pHeroActionInput->SwitchAutoMove(FALSE);
	m_pHeroActionInput->SetFollowState( FALSE );

	sSKILLINFO_BASE *pInfo = (sSKILLINFO_BASE *)theSkillInfoParser.GetInfo((SLOTCODE)dwSkillID);

	if(!pInfo)
	{
		theSkillQueueManager.PopFront();
		return TRUE;
	}

	if(	pInfo->m_byTarget == SKILL_TARGET_AREA 
		|| pInfo->m_byTarget == SKILL_TARGET_REACHABLE_ME)
	{
		if(!m_pHeroActionInput->IsAreaSkill())
		{
			OUTPUTCHAT(TEXTRES_CLICKAREA);
		}

		m_pHeroActionInput->SwitchAreaSkill(TRUE);
	}

	if(m_pHeroActionInput->IsAreaSkill())
	{
		if(!m_pHeroActionInput->UseAreaSkill())
		{
			m_pHeroActionInput->SwitchAreaSkill(FALSE);
			theSkillQueueManager.PopFront();
			return TRUE;
		}
	}
	else
	{
		if(!m_pHeroActionInput->UseSkill(dwSkillID))
		{
			m_pHeroActionInput->SwitchAreaSkill(FALSE);
			theSkillQueueManager.PopFront();
			return TRUE;
		}
	}

	if(m_pPlayerAction->ActionID != ACTION_SKILL)
		return FALSE;

	if (theHero.CanDoQueueAction(m_pPlayerAction)) 
	{
		sSKILLINFO_BASE *pInfo;
		pInfo = (sSKILLINFO_BASE *)theSkillInfoParser.GetInfo((SLOTCODE)m_pPlayerAction->SKILL.dwSkillID);


		if (	pInfo==NULL
			||	!theHero.CanUseSkill(m_pPlayerAction->SKILL.dwSkillID))
		{
			m_pHeroActionInput->SwitchAreaSkill(FALSE);
			theSkillQueueManager.PopFront();
			return TRUE;
		}

		
		util::Timer *pTimer = theHero.GetCoolTimer(pInfo->m_SkillClassCode);

		if (!pTimer) 
		{
			m_pHeroActionInput->SwitchAreaSkill(FALSE);
			theSkillQueueManager.PopFront();
			return TRUE;
		}


		if (m_pHeroActionInput->IsShootingMode()) 
		{
			if (theHero.GetStabberShotDelay() > 30)
			{
				return FALSE;
			}
		}


		/////////////////////////////////////////////
		if (	pInfo->m_byTarget == SKILL_TARGET_ME
			||	pInfo->m_byTarget == SKILL_TARGET_SUMMON)
		{	
			
			theHero.DoAction(m_pPlayerAction);
			m_pHeroActionInput->SwitchAreaSkill(FALSE);
			theSkillQueueManager.PopFront();
			return TRUE;
		}
		else if(pInfo->m_byTarget ==SKILL_TARGET_AREA)
		{
			PLAYER_ACTION skillaction;
			ZeroMemory(&skillaction,sizeof(skillaction));
			skillaction.ActionID				= ACTION_SKILL;
			skillaction.SKILL.dwSkillID	= m_pHeroActionInput->GetCurrentSkillID();
			skillaction.SKILL.dwTargetID	= m_pHeroActionInput->GetCurrentTarget();
			skillaction.SKILL.vTargePos	= m_pHeroActionInput->GetDest();

			m_pHeroActionInput->PushQueueAction(skillaction);

			float		fLength		= m_pHeroActionInput->GetQueueActionDistance();
			Vector3D vTargetPos	= m_pHeroActionInput->GetDest();
			Vector3D vDistance	= vTargetPos - theHero.GetPosition();
			float		fDistance	= vDistance.Length();

			if (fDistance <= fLength)
			{
				theHero.DoAction(m_pPlayerAction);
				theSkillQueueManager.PopFront();
				m_pHeroActionInput->SwitchAreaSkill(FALSE);
				return TRUE;
			}

			return FALSE;
		}
		else if(pInfo->m_byTarget ==SKILL_TARGET_FRIEND_CORPSE)
		{
			float			fLength	= m_pHeroActionInput->GetQueueActionDistance();
			Character *	pChr		= (Character *)theObjectManager.GetObject(m_pPlayerAction->SKILL.dwTargetID);

			if (pChr)
			{
				Vector3D vTargetPos	= pChr->GetPosition();
				Vector3D vDistance	= vTargetPos - theHero.GetPosition();
				float		fDistance	= vDistance.Length();

				if (fDistance <= fLength)
				{
					theHero.DoAction(m_pPlayerAction);
					theSkillQueueManager.PopFront();
					m_pHeroActionInput->SwitchAreaSkill(FALSE);
					return TRUE;
				}
			}
		}

		else
		{
			float			fLength = m_pHeroActionInput->GetQueueActionDistance();
			Character *	pChr = (Character *)theObjectManager.GetObject(m_pPlayerAction->SKILL.dwTargetID);

			if (pChr&&!pChr->CanBeAttackTarget())
			{
				theSkillQueueManager.ClearAll();
				m_pHeroActionInput->SwitchAreaSkill(FALSE);
				return TRUE;
			}

			if (pChr)
			{
				Vector3D vTargetPos	= pChr->GetPosition();
				Vector3D vDistance	= vTargetPos - theHero.GetPosition();
				float		fDistance	= vDistance.Length();

				//if(fDistance>=MAX_LEN) //辨茫扁 滚弊
				//{
				//	//	芭府啊 呈公 纲聪促.
				//	g_InterfaceManager.GetUIText(TEXTRES_TOO_FAR,szMessage,MAX_UITEXT_LENGTH);
				//	OUTPUTCHAT(OUTPUTMSG_CHAT,szMessage);
				//	theSkillQueueManager.ClearAll();
				//	m_pHeroActionInput->SwitchAreaSkill(FALSE);

				//	return TRUE;
				//}

				if (fDistance <= fLength)
				{
					if (pInfo->m_SkillClassCode == SKILLCODE_ASSAULT )
					{					
						Vector3D vDest;
						
						if (fDistance > 1.0f)
						{
							Vector3D vDirection;

							vDirection = vDistance;
							vDirection.Normalize();
						
							vDest = vTargetPos + (vDirection * -0.9f);

						}
						else 
						{
							vDest = theHero.GetPosition();
						}

						m_pPlayerAction->SKILL.vDestPos.wvPos			= vDest;
						m_pPlayerAction->SKILL.vDestPos.m_TileIndex	= -1;
					}

					if (pInfo->m_SkillClassCode == SKILLCODE_EXPLOZEN_ATTACK )
					{					
						Vector3D vDest;

						if (fDistance > 1.0f)
						{
							Vector3D vDirection;

							vDirection = vDistance;
vDirection.Normalize();
							vDest = vTargetPos + (vDirection * -0.9f);
						}
						else 
						{
							vDest = theHero.GetPosition();
						}

						m_pPlayerAction->SKILL.vDestPos.wvPos			= vDest;
						m_pPlayerAction->SKILL.vDestPos.m_TileIndex	= -1;
					}

					if (pInfo->m_SkillClassCode == SKILLCODE_MACHSLASH)
					{					
						Vector3D vDest;
						Vector3D vDirection;

						vDirection = vDistance;
vDirection.Normalize();
						
						vDest = theHero.GetPosition() + (vDirection * 7.0f);

						m_pPlayerAction->SKILL.vDestPos.wvPos			= vDest;
						m_pPlayerAction->SKILL.vDestPos.m_TileIndex	= -1;
					}

					theHero.DoAction(m_pPlayerAction);
					theSkillQueueManager.PopFront();
					m_pHeroActionInput->SwitchAreaSkill(FALSE);
					return TRUE;
				}
			}
			else
			{
				return TRUE;
			}
		}
	}
	
	return FALSE;
}




//------------------------------------------------------------------------------
BOOL GameActionHandler::ProcessQueueShot()
{
	Character * pEnemy = (Character *)theObjectManager.GetObject(m_pPlayerAction->STABBERSHOT.dwTargetID);

	if (pEnemy)	
	{
		if (!pEnemy->CanBeAttackTarget())
		{
			return FALSE;
		}

		Vector3D wzVec1	= pEnemy->GetPosition() - theHero.GetPosition();
		float		fLength	= wzVec1.Length();

		if (fLength <= theHero.GetAttackRange())
		{
			if (theHero.CanDoQueueAction(m_pPlayerAction))
			{
				theHero.StopMove();
				theHero.SetComboCount(m_pHeroActionInput->GetComboCount());
				theHero.SetTargetID(pEnemy->GetObjectKey());
				theHero.SetNextState(STATE_ATTACK,g_CurTime);
				
				m_pHeroActionInput->IncComboCount();

				DWORD dwCurStyle = theHero.GetCurrentAttackStyle();

				if (dwCurStyle == 9999) 
				{
					//m_pHeroActionInput->m_iComboCount = 0;					
					if (m_pHeroActionInput->GetComboCount() >= 2)
					{
						m_pHeroActionInput->SetComboCount(0);
					}
				}
				else
				{
					if (m_pHeroActionInput->GetComboCount() >= 3)
					{
						m_pHeroActionInput->SetComboCount(0);
//						m_bAutoAttack = FALSE;
					}
				}
				return TRUE;
			}			
		}
	}
	else
	{
		return FALSE;
	}

	return FALSE;
}


void	GameActionHandler::ProcessNpcMeet(DWORD npcID)
{
	MapNPC * pNPC = (MapNPC *)theObjectManager.GetObject(npcID); 
	__CHECK2_PTR(pNPC,);

	eNPC_FUNC_TYPE npcType = pNPC->GetNpcFunc();
	switch (npcType)
	{
	case NPC_FUNC_MAKE_ZONE:
		{
			assert(0 && " NPC_FUNC_MAKE_ZONE ");
#pragma message(__FILE__  "(543) NPC_FUNC_MAKE_ZONE  " )
		}
		break;

	case NPC_FUNC_BANK:
		{
			WareHouseDialog* pDlg;

			pDlg = (WareHouseDialog*)theHero.GetSlotContainer(SI_WAREHOUSE);
			if (pDlg && !pDlg->IsStarted())
			{
				pNPC->SetDialogSlotIdx(pDlg->GetSlotIndex());
				pDlg->SendWareHouseStart();
			}
		}
		break;

	case NPC_FUNC_STORE:
	case NPC_FUNC_WEAPON:
	case NPC_FUNC_SHIELD:
	case NPC_FUNC_DRESS:
	case NPC_FUNC_MATERIAL:
		{
			ShopDialog * pShopDlg;
			pShopDlg = (ShopDialog *)theItemManager.GetContainer(SI_NPCSHOP);
			if ( pShopDlg && !pShopDlg->IsStarted() )
			{
				pNPC->SetDialogSlotIdx(pShopDlg->GetSlotIndex());
				pShopDlg->SetShopID( pNPC->GetFuncInfo()->m_ExtraCode );
				pShopDlg->ShowWindow(TRUE);
			}
		}
		break;


	case NPC_FUNC_GUILD:
		{
			assert(0 && " NPC_FUNC_GUILD ");
#pragma message(__FILE__  "(602) NPC_FUNC_GUILD  " )
		}
		break;


	case NPC_FUNC_ITEMCOMPOUND:
	case NPC_FUNC_ITEMINLAY:
	case NPC_FUNC_ITEMMAKE:
	case NPC_FUNC_ITEMENCHANT:
		{
			//ItemCompoundDialog * pItemCompoundDlg;

			//pItemCompoundDlg = (ItemCompoundDialog *)theItemManager.GetContainer(SI_ENCHANT);
			//if ( pItemCompoundDlg && !pItemCompoundDlg->IsStarted() )
			if(theItemCompoundDialog.IsNotStarted())
			{
				if(theItemCompoundDialog.OpenByNpc(npcType))
				{
					pNPC->SetDialogSlotIdx(theItemCompoundDialog.GetMainContainer()->GetSlotIndex());
					theItemCompoundDialog.ShowWindow(TRUE);
				}
			}
		}
		break;


	case NPC_FUNC_TRANSPORT:
		{
			assert(0 && " NPC_FUNC_TRANSPORT ");
#pragma message(__FILE__  "(647) NPC_FUNC_TRANSPORT  " )
		}
		break;

	default:
		break;
	}
}



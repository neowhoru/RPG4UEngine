/*////////////////////////////////////////////////////////////////////////
文 件 名：NecromancerKeyboardInputHandler.cpp
创建日期：2007年11月3日
最后更新：2007年11月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NecromancerKeyboardInputHandler.h"
#include "HeroKeyboardInputLayer.h"
#include "HeroMouseInputLayer.h"
#include "HeroActionInput.h"
#include "Item.h"
#include "Hero.h"
#include "MapObject.h"
#include "GameUtil.h"

#include "InputLayer.h"
#include "Camera.h"
#include "MouseHandler.h"
#include "VUICursorManager.h"
#include "TileWorldClient.h"
#include "PathHandler.h"
#include "HeroTipLayer.h"
#include "ObjectManager.h"
#include "ItemManager.h"
#include "SceneDefine.h"
#include "QuickContainer.h"

using namespace input;
using namespace scene;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(NecromancerKeyboardInputHandler, ()  , gamemain::eInstPrioGameFunc);


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
NecromancerKeyboardInputHandler::NecromancerKeyboardInputHandler()
{
}
NecromancerKeyboardInputHandler::~NecromancerKeyboardInputHandler()
{
}

void NecromancerKeyboardInputHandler::_ProcessStyleQuickKey()
{
}

//------------------------------------------------------------------------------
BOOL NecromancerKeyboardInputHandler::ProcessKeyboardMove()
{
	//TCHAR	szMessage[MAX_UITEXT_LENGTH];

	Vector3D vDirection;
	vDirection.Set(0.0f,0.0f,0.0f);

	Vector3D vFoward = theHero.GetDirection();

	Vector3D vRight,vLeft,vBack;

	vRight.x = vFoward.y; 
	vRight.y = -vFoward.x; 
	vRight.z = 0.0f;

	vLeft.x = -vFoward.y; 
	vLeft.y = vFoward.x; 
	vLeft.z = 0.0f;

	vBack.x = -vFoward.x; 
	vBack.y = -vFoward.y; 
	vBack.z = 0.0f;


	DWORD dwCurTick=g_CurTime;
	//DWORD dwCurTick=g_CurTime;//g_pRendererPtr->x_Clock.GetCurrentTick();

	BOOL bLeftStepPressed = FALSE;
	BOOL bRightStepPressed = FALSE;
	BOOL bFowardPressed = FALSE;

	BOOL bBackwardPressed = FALSE;
	BOOL bLeftTurnPressed = FALSE;
	BOOL bRightTurnPressed = FALSE;

	BOOL bLeftSpecialMovePressed = FALSE;
	BOOL bRightSpecialMovePressed = FALSE;
	BOOL bFowardSpecialMovePressed = FALSE;
	BOOL bBackwardSpecialMovePressed = FALSE;

	theHero.ResetMoveFlag();

	if (theHero.CannotMove())
	{
		return FALSE;
	}

	m_pHeroActionInput->SetKeyBoardMoveCheck(FALSE);

	///////////////////////////////////////////////////
	if (theInputLayer.GetState(KEYC_A, KS_KEY))
	{
		bLeftTurnPressed = TRUE;
	}

	else if (theInputLayer.GetState(KEYC_D, KS_KEY))
	{
		bRightTurnPressed = TRUE;			
	}

	if (theInputLayer.GetState(KEYC_W, KS_KEY ))
	{
		DWORD diff=dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_FORWORD];

		if(	diff < DOUBLE_DOWN_TIME
		   &&	!m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD])
		{
			bFowardSpecialMovePressed=TRUE; 
		}
		else
			bFowardPressed = TRUE;

		m_pHeroActionInput->SwitchAutoMove(FALSE);
		m_pHeroActionInput->SetFollowState( FALSE );

		if( m_pHeroActionInput->IsShootingMode() == FALSE )
		{
			m_pHeroActionInput->SwitchAutoAttack(FALSE);
		}
		else
		{
			if( theHero.GetCurrentState() == STATE_MOVE )
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
			}
		}
	}

	else if (theInputLayer.GetState(KEYC_S, KS_KEY))
	{
		DWORD diff=dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_BACKWORD];

		if(	diff<DOUBLE_DOWN_TIME
		   &&	!m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD])
		{
			bBackwardSpecialMovePressed = TRUE; 
		}
		else
			bBackwardPressed = TRUE;

		m_pHeroActionInput->SwitchAutoMove(FALSE);
		m_pHeroActionInput->SetFollowState( FALSE );

		if( m_pHeroActionInput->IsShootingMode() == FALSE )
		{
			m_pHeroActionInput->SwitchAutoAttack(FALSE);
		}
		else
		{
			if( theHero.GetCurrentState() == STATE_MOVE )
			{
				m_pHeroActionInput->SwitchAutoAttack(FALSE);
			}
		}
	}

	if (theInputLayer.GetState(KEYC_Q, KS_KEY))
	{
		DWORD diff=dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_LEFT];

		if(	diff < DOUBLE_DOWN_TIME
		   &&	!m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT])
		{
			//theHero.ResetMoveFlag();
			bLeftSpecialMovePressed	= TRUE; 
			m_pHeroActionInput->SwitchAutoMove(FALSE);
			m_pHeroActionInput->SetFollowState( FALSE );
		}
		else if( m_pHeroActionInput->GetQueueAction().ActionID != ACTION_SPECIAL_MOVE )
			bLeftStepPressed = TRUE;

	}
	else if (theInputLayer.GetState(KEYC_E, KS_KEY))
	{
		DWORD diff=dwCurTick-m_dwSpecialMovePreTime[SPECIAL_MOVE_RIGHT];

		if(	diff < DOUBLE_DOWN_TIME
		   &&	!m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT])
		{
			bRightSpecialMovePressed = TRUE;
			m_pHeroActionInput->SwitchAutoMove(FALSE);
			m_pHeroActionInput->SetFollowState( FALSE );
		}
		else if( m_pHeroActionInput->GetQueueAction().ActionID != ACTION_SPECIAL_MOVE)
			bRightStepPressed = TRUE;
	}

	if (theInputLayer.GetState(KEYC_Q, KS_KEY))
	{
		if(!m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT])
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT]= TRUE;
			m_dwSpecialMovePreTime[SPECIAL_MOVE_LEFT]	= g_CurTime;
		}
	}
	else
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_LEFT]=FALSE;

	if (theInputLayer.GetState(KEYC_E, KS_KEY))
	{
		if(!m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT])
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT]=TRUE;
			m_dwSpecialMovePreTime[SPECIAL_MOVE_RIGHT]=g_CurTime;
		}
	}
	else
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_RIGHT]=FALSE;

	if (theInputLayer.GetState(KEYC_W, KS_KEY))
	{
		if(!m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD])
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD]= TRUE;
			m_dwSpecialMovePreTime[SPECIAL_MOVE_FORWORD]	= g_CurTime;
		}
	}
	else
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_FORWORD]=FALSE;


	if (theInputLayer.GetState(KEYC_S, KS_KEY))
	{
		if(!m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD])
		{
			m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD]	= TRUE;
			m_dwSpecialMovePreTime[SPECIAL_MOVE_BACKWORD]	= g_CurTime;
		}
	}
	else
		m_bSpecialMoveFirstDown[SPECIAL_MOVE_BACKWORD]=FALSE;


	///////////////////////////////////////////////////
	//特殊处理
	if (	bLeftSpecialMovePressed 
		&& (!bRightStepPressed && !bFowardPressed && !bBackwardPressed ) )
	{
		_ProcessSpecialMove(vLeft);
		return FALSE;

	}
	else if (	bRightSpecialMovePressed
				&&	(!bLeftStepPressed && !bFowardPressed && !bBackwardPressed ) )
	{
		_ProcessSpecialMove(vRight);
		return FALSE;
	}
	else if (bFowardSpecialMovePressed&&(!bRightStepPressed && !bLeftStepPressed && !bBackwardPressed ))
	{
		_ProcessSpecialMove(vFoward);
		return FALSE;
	}	
	else if (bBackwardSpecialMovePressed&&(!bRightStepPressed && !bLeftStepPressed && !bFowardPressed))
	{
		_ProcessSpecialMove(vBack);
		return FALSE;
	}	



	if (	bRightStepPressed 
		|| bLeftStepPressed 
		|| bFowardPressed
		|| bBackwardPressed 
		|| bLeftTurnPressed 
		|| bRightTurnPressed) 
	{
		m_pHeroActionInput->InitAction();
		theSkillQueueManager.ClearAll();
		m_pHeroActionInput->SwitchAreaSkill(FALSE);
		m_pHeroActionInput->SetKeyBoardMoveCheck(TRUE);

		if (m_pHeroActionInput->IsIdleState() || theHero.GetCurrentState() == STATE_MOVE )
		{
			theHero.StopMove();
			theHero.SetNextState(STATE_KEYBOARDMOVE,g_CurTime);
		}

		if (bLeftTurnPressed) 
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_LEFTTURN);
		}
		else if (bRightTurnPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTTURN);
		}

		if (bFowardPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_FOWARD);
		}
		else if (bBackwardPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_BACKWARD);
		}

		if (bRightStepPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_RIGHTSTEP);
		}
		else if (bLeftStepPressed)
		{
			theHero.AddMoveFlag(KEYBOARDMOVE_LEFTSTEP);
		}

		return TRUE;
	}

	return FALSE;
}





// stdafx.cpp : 只包括标准包含文件的源文件
// stdafx.obj 将包含预编译类型信息

#include "stdafx.h"
#include "ApplicationDefine.h"
#include "KeyboardInputHandler.h"
#include "AssassinKeyboardInputHandler.h"
#include "NecromancerKeyboardInputHandler.h"
#include "HeroActionInput.h"
#include "MouseInputHandler.h"
#include "GameAssistantHandler.h"
#include "GameActionHandler.h"
#include "LogSystem.h"
#include "ApplicationDefine.h"
#include "HeroSoundHandler.h"


//#ifdef _DEBUG
//#pragma comment(lib,"commonlib_d.lib")
//#else
//#pragma comment(lib,"commonlib.lib")
//#endif

#define INSTALL_KBHANDLER(Name,CharType)\
	theHeroActionInput.InstallKeyboardLayer(&the##Name##InputHandler,			CharType		);

/*////////////////////////////////////////////////////////////////////////
插件入口
/*////////////////////////////////////////////////////////////////////////
_HEROACTION_PLUGIN_API DWORD PluginMainEntry(LPARAM)
{
#ifndef DISABLE_HANDLER
	INSTALL_KBHANDLER(Keyboard,				PLAYERTYPE_WARRIOR);		//力士键盘控制
	INSTALL_KBHANDLER(Keyboard,				PLAYERTYPE_PALADIN);		//游侠键盘控制
	INSTALL_KBHANDLER(NecromancerKeyboard, PLAYERTYPE_POWWOW);		//术士键盘控制
	INSTALL_KBHANDLER(AssassinKeyboard,		PLAYERTYPE_STABBER);		//刺客键盘控制
	INSTALL_KBHANDLER(NecromancerKeyboard, PLAYERTYPE_NECROMANCER);//巫师键盘控制

	theHeroActionInput.InstallAssistantLayer	(&theGameAssistantHandler	);
	theHeroActionInput.InstallMouseLayer		(&theMouseInputHandler	);
	theHeroActionInput.InstallQueueActionLayer(&theGameActionHandler	);
	theHeroActionInput.InstallSoundLayer		(&theHeroSoundHandler	);
#endif

	LOGINFO("Plugin HeroAction insall OK!\n");
	return gamemain::ePluginOK;
}

/*////////////////////////////////////////////////////////////////////////
插件结束
/*////////////////////////////////////////////////////////////////////////
_HEROACTION_PLUGIN_API DWORD PluginEnd(LPARAM)
{
	return gamemain::ePluginOK;
}

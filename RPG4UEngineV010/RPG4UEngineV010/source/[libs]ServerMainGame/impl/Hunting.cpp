#include "StdAfx.h"
#include ".\hunting.h"
#include "Player.h"
#include "PartyManager.h"
#include "Map.h"

__IMPL_ZONE_POOL(Hunting);
Hunting::Hunting(void)
{
}

Hunting::~Hunting(void)
{
}

BOOL Hunting::Init( KEYTYPE Key, CODETYPE MapCode, KEYTYPE LeaderPlayerKey, HUNTING_DESC & IN desc )
{
	if( !Room::Init( Key, MapCode, LeaderPlayerKey ) ) return FALSE;

	setType( eZONE_HUNTING );

	memcpy( &m_RoomDesc, &desc, sizeof(HUNTING_DESC) );


	// TEMP : 임시 
	// 코드 수정이 필요한 좃치안은 코드
	// TAIYO
	ASSERT( desc.m_byMonsterType < eBATTLE_MONSTER_DIFFICULT_MAX );
	util::SolarHashTable<Field *> * pHash = GetMap()->GetFieldHashTable();
	Field * pField = NULL;
	pHash->SetFirst();
	while( (pField = pHash->GetNext()) != NULL )
	{
		pField->InitRegenManager( desc.m_byMonsterType+1 );
	}

	return TRUE;
}


BOOL Hunting::readyPlayer( Player * pPlayer, CODETYPE FieldCode )
{
	// 상태 체크
	if( !readyCheck( isLeaderPlayer( pPlayer->GetObjectKey() ) ) ) return FALSE;

	DEBUG_CODE( Zone * pEnterZone		= NULL;
	CODETYPE EnterFieldCode = 0;
	eZONE_STATE eRoomState = pPlayer->GetZone( &pEnterZone, EnterFieldCode );
	if( eRoomState & ePRS_AT_ZONE ) 
		return FALSE;);

	pPlayer->SetZone( ePRS_BEFORE_ENTER_HUNTING, this, FieldCode );
	return TRUE;
}

BOOL Hunting::joinPlayer( Player * pPlayer, CODETYPE FieldCode )
{
	if( !joinCheck( isLeaderPlayer( pPlayer->GetObjectKey() ) ) ) return FALSE;

	Zone * pEnterZone		= NULL;
	CODETYPE EnterFieldCode = 0;
	eZONE_STATE eRoomState = pPlayer->GetZone( &pEnterZone, EnterFieldCode );
	if( !(eRoomState & ePRS_BEFORE_ENTER_HUNTING) ) 
	{
		SASSERT( NULL, "유저의 방상태가 이상합니다.JoinPlayer() : 들어올려는 방이 헌팅이아니다." );
		return FALSE;
	}

	pPlayer->SetZone( ePRS_AT_HUNTING, this, FieldCode );

	addPlayer( pPlayer );


	// 상태 변경
	if( isLeaderPlayer( pPlayer->GetObjectKey() ) )
	{
		setState( eROOM_ACTIVE );
	}

	return TRUE;
}


BOOL Hunting::leavePlayer( Player * pPlayer )
{
	if( !leaveCheck( isLeaderPlayer( pPlayer->GetObjectKey() ) ) ) return FALSE;

	removePlayer( pPlayer->GetObjectKey() );

	// 방장이 나가면 위임 처리
	if( isLeaderPlayer( pPlayer->GetObjectKey() ) )
	{
		if( getNumberOfPlayers() == 0 )
		{
			// 방파괴 상태
			setState( eROOM_DEACTIVE );

		}
		else
		{
			delegateLeaderPlayer();
		}
	}

	pPlayer->SetZone( ePRS_NOT_AT_ZONE, NULL, 0 );

	return TRUE;
}

BOOL Hunting::Update( DWORD dwDeltaTick )
{
	if( !Room::Update( dwDeltaTick ) ) 
		return FALSE;

	if( m_PlayerTimer.IsExpiredManual() )
	{
		if( getState() == eROOM_READY )
			return FALSE;
	}

	return TRUE;
}
// ItemImpl.cpp: implementation of the ItemImpl class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ItemImpl.h"
#include "ServerOptionParser.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

ItemImpl::ItemImpl()
{
}

ItemImpl::~ItemImpl()
{

}

BOOL ItemImpl::Init()
{
	return _SUPER::Init();
}

VOID ItemImpl::Release()
{
	_SUPER::Release();
}

VOID ItemImpl::CreateItem(DWORD         OwnerKey
                         ,DWORD         MonsterKey
								 ,ItemSlot &	 slot)
{
	SetOwnerPlayerKey( OwnerKey );
	SetFromMonsterKey( MonsterKey );
	m_ItemSlot.Copy( slot );
}

VOID ItemImpl::GetItem( ITEMOPT_STREAM & OUT ItemStreamOut )
{
	m_ItemSlot.CopyOut( ItemStreamOut );
}

BOOL ItemImpl::IsDelayDropExpired() 
{
	return m_DelayDropTimer.IsExpired(); 
}

VOID ItemImpl::CreateMoney( DWORD OwnerKey, DWORD MonsterKey, MONEY & money )
{
//		SetLocked(FALSE);
	SetOwnerPlayerKey	( OwnerKey );
	SetFromMonsterKey	( MonsterKey );
	SetMoney				( money );
}

VOID ItemImpl::SetDropDelay(Field *    pField
                           ,Vector3D * pvPos
									,DWORD      dwDelayTime)
{
	m_pDropField	= pField;
	m_DropPos		= *pvPos;
	m_DelayDropTimer.SetTimer( dwDelayTime );
}

VOID ItemImpl::Update( DWORD dwTick )
{
	_SUPER::Update( dwTick );

	if(	GetField() 
		&& m_DestroyTimer.IsExpiredManual() )
	{
		DestroyFromField();
	}
}

VOID ItemImpl::GetRenderInfo( sITEMINFO_RENDER & OUT RenderInfo )
{
	RenderInfo.m_dwObjectKey		= GetObjectKey();
	RenderInfo.m_dwOwnerPlayerKey	= GetOwnerPlayerKey();

	if( GetObjectType() == MONEY_OBJECT )
	{
		RenderInfo.m_byFieldItemType	= sITEMINFO_RENDER::eFIELDITEM_MONEY;
		RenderInfo.m_Money				= GetMoney();
	}
	else
	{
		RenderInfo.m_byFieldItemType = sITEMINFO_RENDER::eFIELDITEM_ITEM;
		GetItem( RenderInfo.m_ItemStream );
	}

	Vector3D vPos = GetPosition();
	RenderInfo.m_fPos[0]		= vPos.x;
	RenderInfo.m_fPos[1]		= vPos.y;
	RenderInfo.m_fPos[2]		= vPos.z;
}


VOID ItemImpl::OnEnterField(Field* pField, Vector3D* pvPos)
{
	sSERVER_OPTION& opt = theServerOptionParser.GetServerOption();

	Init();

	_SUPER::OnEnterField( pField, pvPos );

	m_DestroyTimer.SetTimer(opt.m_wItemDestroyDelay  );
}

VOID ItemImpl::OnLeaveField()
{
	_SUPER::OnLeaveField();
	Release();
}

VOID ItemImpl::DestroyFromField()
{
	GetField()->PushNpcDeadList( this );
}




#include "StdAfx.h"
#include ".\charinfolist.h"
#include <ServerStruct.h>
#include <ScriptRead.h>

CharInfoList::CharInfoList()
	:	m_pInitCharInfoHashTable( NULL ),
		m_pCalcCharInfoHashTable( NULL )
{
}

CharInfoList::~CharInfoList()
{
	ASSERT( m_pInitCharInfoHashTable == NULL );
	ASSERT( m_pCalcCharInfoHashTable == NULL );
}

VOID CharInfoList::Init( DWORD dwCharInfoPoolSize )
{
	ASSERT( m_pInitCharInfoHashTable == NULL );
	ASSERT( m_pCalcCharInfoHashTable == NULL );
	m_pInitCharInfoHashTable = new util::SolarHashTable<INIT_CHARINFO *>;
	m_pCalcCharInfoHashTable = new util::SolarHashTable<CALC_CHARINFO *>;
	m_pInitCharInfoHashTable->Initialize( dwCharInfoPoolSize );
	m_pCalcCharInfoHashTable->Initialize( dwCharInfoPoolSize );
}

VOID CharInfoList::Release()
{
	Unload();

	delete m_pInitCharInfoHashTable;
	delete m_pCalcCharInfoHashTable;
	m_pInitCharInfoHashTable = NULL;
	m_pCalcCharInfoHashTable = NULL;
}

VOID CharInfoList::Unload()
{
	INIT_CHARINFO * pInfo = NULL;
	while( pInfo = m_pInitCharInfoHashTable->GetNext() )
	{
		delete pInfo;
	}
	m_pInitCharInfoHashTable->RemoveAll();

	CALC_CHARINFO * pInfo2 = NULL;
	while( pInfo2 = m_pCalcCharInfoHashTable->GetNext() )
	{
		delete pInfo2;
	}
	m_pCalcCharInfoHashTable->RemoveAll();
}

#pragma warning ( push )
#pragma warning ( disable : 4244)

BOOL CharInfoList::Load( char * pszInitCharFileName, char * pszCalcCharFileName )
{
	ASSERT( m_pInitCharInfoHashTable != NULL );
	ASSERT( m_pCalcCharInfoHashTable != NULL );

	Unload();
	
	// InitCharInfoList.txt 파일 열기
	CScriptRead sr;
	if ( !sr.Create( pszInitCharFileName ) )
	{
		DISPMSG("File Open Error : InitCharInfoList.txt");
		return FALSE;
	}

	INIT_CHARINFO * pInfo = new INIT_CHARINFO;
	sr.GetToken(TT_NUMBER); pInfo->m_ClassCode = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_ClassNameCode = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwLV = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwExp = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwRemainStat = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwRemainSkill = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMaxHP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMaxMP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwRegion = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sLocationX = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sLocationY = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sLocationZ = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sStrength = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sDexterity = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sVitality = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sInteligence = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sSpirit = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sSkillStat1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_sSkillStat2 = sr.GetTokenNumber();

	ASSERT( NULL == m_pInitCharInfoHashTable->GetData( pInfo->m_ClassCode ) );
	m_pInitCharInfoHashTable->Add( pInfo, pInfo->m_ClassCode );

	sr.Destroy();



	// CalcCharInfoList.txt 파일 열기
	if ( !sr.Create( pszCalcCharFileName ) )
	{
		DISPMSG("File Open Error : CalcCharInfoList.txt");
		return FALSE;
	}

	CALC_CHARINFO * pInfo2 = new CALC_CHARINFO;

	sr.GetToken(TT_NUMBER); pInfo2->m_ClassCode = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_dwMeleeDamage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_dwRangeDamage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_dwMagicDamage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byHPPerStat = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byHPPerLevel = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byHPRecover = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMPPerStat = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMPPerLevel = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMPRecover = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byStatPerLevel = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_bySkillPerLevel = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byPhyDef = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMagicDef = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byPhyCritical = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMagicCritical = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo2->m_byPhyAttRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byPhyAvoidRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMagicAttRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMagicAvoidRate = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo2->m_byAttSpeed = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMagicSpeed = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byMoveSpeed = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byLimitCarry = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo2->m_byFastRecovery = sr.GetTokenNumber();

	ASSERT( NULL == m_pCalcCharInfoHashTable->GetData( pInfo2->m_ClassCode ) );
	m_pCalcCharInfoHashTable->Add( pInfo2, pInfo2->m_ClassCode );

	sr.Destroy();


	return TRUE;
}

#pragma warning ( pop )
/*////////////////////////////////////////////////////////////////////////
文 件 名：GameServerScriptInterface.cpp
创建日期：2007年5月20日
最后更新：2007年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////

#include "stdafx.h"
#include "Player.h"
#include "GameServerScriptInterface.h"
#include "ScriptKeywordManager.h"
#include "IScriptCompiler.h"
#include "ScriptWord.h"
#include "GameServerScriptHandler.h"
#include "ScriptStructDefine.h"
#include "MapNPC.h"
#include "GlobalManager.h"
#include "PlayerManager.h"
#include "ItemManager.h"
#include "ItemInfoParser.h"
#include "PacketStruct_ClientGameS_Event.h"

#include "GameServerScriptInterface.inc.h"

using namespace quest;


#define CHECK_PLAYER()		if(!CheckPlayer( _T("[x]"  __FUNCDNAME__ "\n") ) ) 			return 
#define CHECK_PLAYER2()		CHECK_PLAYER() FALSE;

#define SEND_PLAYER(x)		SendPacket(&x,sizeof(x))


BOOL g_bScriptCompilerError = FALSE;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_IMPL(GameServerScriptInterface, ()  , gamemain::eInstPrioGameFunc);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GameServerScriptInterface::GameServerScriptInterface()
{
	m_dwTempValue	= 0;
	m_pPlayer		= 0;
}

void GameServerScriptInterface::SetTeamVar	(int   varIndex
                                    ,int   oprType
												,void* pParams
												,int   nParamCount)
{
	//Player* m_pPlayer = thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::SetTeamVar error" );
	//	return;
	//}
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( m_pPlayer->GetTeamID() );
	//if ( pTeam )
	//{
	//	for ( int n=0; n<pTeam->GetTeamNumber(); n++ )
	//	{
	//		m_dwCharacterID = pTeam->GetTeamMemberID( n );
	//		SetVar( varIndex, oprType, pParams, nParamCount );
	//	}
	//}
	//else
	{
		SetVar( varIndex, oprType, pParams, nParamCount );
	}
	
}

void GameServerScriptInterface::SetVar(int   varIndex
                               ,int   oprType
										 ,void* pvParams
										 ,int   nParamCount)
{
	__UNUSED(nParamCount);
	__CHECK2_PTR(pvParams,);


	SetVarParam*	pParams = (SetVarParam*)pvParams;
	Character*		pChar;

	pChar				= (Character*)theGlobalManager.FindCharacter(m_dwCharacterID);

	__VERIFY2	(pChar && pChar->IsCharacter()
					,FMSTR("SetVar()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,);


	///玩家变量，须是玩家
	if ( varIndex < MAX_PLAYERVAR_SIZE && !pChar->IsKindOf(PLAYER_OBJECT))
		return;


	GameServerScriptHandler script(pChar);

	switch( varIndex )
	{
	case KEYW_LEVEL:
		{
			assert( nParamCount == 1 );
			pChar->SetLevel((LEVELTYPE)(INT) pParams[0] );
		}break;

	case KEYW_POS:
		{
			Vector3D vPos;
			vPos.Set( pParams[0], pParams[1],0.f);
			pChar->SetPosition(vPos);
			if(nParamCount > 2)
			{	
				WORD wAngle(0);
				wAngle = (WORD)pParams[2].GetNumber();
				pChar->StopMoving(TRUE,&wAngle);
			}
			else
			{
				pChar->StopMoving();
			}
		}
		break;

	case KEYW_NAME:
		{
			assert( nParamCount == 1 );
			pChar->SetName( pParams[0] );
		}break;

	//case KEYW_HEADERNAME:
	//	break;
	//case KEYW_TARGETNAME:
	//	break;

	case KEYW_MAP:
		{
			assert(0);
			assert( nParamCount == 1 );
			//pChar->SetMapID( pParams[0] );
		}break;

	case KEYW_MODEL:
		{
			assert(0);
			assert( nParamCount == 1 );
			//pChar->SetModelID((short) pParams[0] );
		}
		break;

	case KEYW_PROFESSION:
		{
			assert( nParamCount == 1 );
			script.SetProfession( pParams[0] );
		}break;

	case KEYW_SEX:
		{
			assert( nParamCount == 1 );
			script.SetSex( pParams[0] );
		}break;

	case KEYW_MONEY:
		{
			assert( nParamCount == 1 );
			script.Money( pParams[0], oprType );
		}break;

	case KEYW_EXP:
		{
			assert( nParamCount == 1 );
			script.SetExp( pParams[0], oprType );
		}break;

	case KEYW_SKILLEXP:
		{
			assert( nParamCount == 1 );
			script.SetSkillExp( pParams[0], oprType );
		}break;

	case KEYW_VIT:
		{
			assert( nParamCount == 1 );
			script.SetVIT( pParams[0], oprType );
		}break;

	case KEYW_STR:
		{
			assert( nParamCount == 1 );
			script.SetSTR( pParams[0], oprType );
		}break;

	case KEYW_DEX:
		{
			assert( nParamCount == 1 );
			script.SetDEX( pParams[0], oprType );
		}break;


	case KEYW_INT:
		{
		assert( nParamCount == 1 );
			script.SetINT( pParams[0], oprType );
		}break;



	case KEYW_SPR:
		{
			assert( nParamCount == 1 );
			script.SetSPR( pParams[0], oprType );
		}break;


	case KEYW_HP:
		{
			assert( nParamCount == 1 );
			script.SetHP( pParams[0], oprType );
		}break;

	case KEYW_MP:
		{
			assert( nParamCount == 1 );
			script.SetMP( pParams[0], oprType );
		}break;


	case KEYW_SKILL:
		{
			//assert( nParamCount == 1 );
			//script.Skill( pParams[0], oprType );
		}break;

	case KEYW_REPUTATION:
		{
			assert( nParamCount == 1 );
			script.SetReputation( pParams[0], oprType );
		}break;


	default:
		{
			pChar->SetVar( varIndex, pParams[0], oprType );
		}break;
	}
}


LPCSTR GameServerScriptInterface::GetLabelOfVar( int varIndex )
{
	Character*			pChar;

	pChar				= (Character*)theGlobalManager.FindCharacter(m_dwCharacterID);

	__VERIFY2	(pChar && pChar->IsCharacter()
					,FMSTR("GetLabelOfVar()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,NULL);

	///玩家变量，须是玩家
	if ( varIndex < MAX_PLAYERVAR_SIZE && !pChar->IsKindOf(PLAYER_OBJECT))
		return NULL;


	switch( varIndex )
	{
	case KEYW_NAME:
		{
			return pChar->GetName();
		}break;


	case KEYW_HEADERNAME:
		{
			//PlayerTeam * pTeam = NULL;
			//pTeam = GetPlayerTeamManager().GetTeam( pChar->GetTeamID() );
			//if ( pTeam )
			//{
			//	int nHeaderId = pTeam->GetTeamHeaderID();
			//	Player* pHeader = thePlayerManager.FindPlayerUser( nHeaderId );
			//	if ( pHeader )
			//		return pHeader->GetPlayerName();
			//}
			return "";
		}
		break;

	case KEYW_TARGETNAME:
		{
			//Player*			pDestPlayer;

			//pDestPlayer			= thePlayerManager.FindPlayerUser(pChar->GetDstPlayerId());
			//__VERIFY2_PTR	(pDestPlayer
			//					,FMSTR("GetLabelOfVar ()脚本错误，char:%ld 无效.\n",pChar->GetDstPlayerId() )
			//					,"");

			//return pDestPlayer->GetPlayerName();
			return "";
		}
		break;
	}

	static char szVar[32];
	itoa( GetVar(varIndex), szVar, 10 );
	return szVar;
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int GameServerScriptInterface::GetVar( int varIndex, int nPlayerId )
{
	int nID;
	
	if ( nPlayerId != -1 )
		nID = nPlayerId;
	else
		nID = m_dwCharacterID;

	
	Character*			pChar;

	pChar				= (Character*)theGlobalManager.FindCharacter(m_dwCharacterID);

	__VERIFY2	(pChar && pChar->IsCharacter()
					,FMSTR("GetLabelOfVar()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,0);

	///玩家变量，须是玩家
	if ( varIndex < MAX_PLAYERVAR_SIZE && !pChar->IsKindOf(PLAYER_OBJECT))
		return 0;



	GameServerScriptHandler script(pChar);

	switch( varIndex )
	{
	case KEYW_MONEY:			return (int)pChar->GetMoney();
	case KEYW_LEVEL:			return pChar->GetLevel();
	case KEYW_PROFESSION:	return script.GetProfession();
	case KEYW_SEX:				return script.GetSex();
	case KEYW_EXP:				return script.GetExp();
	case KEYW_SKILLEXP:		return script.GetSkillExp();
	case KEYW_VIT:				return script.GetVIT();

	case KEYW_STR:				return script.GetSTR();
	case KEYW_DEX:				return script.GetDEX();
	case KEYW_INT:				return script.GetINT();
	case KEYW_SPR:				return script.GetSPR();
	case KEYW_HP:				return script.GetHP();
	case KEYW_MP:				return script.GetMP();
	case KEYW_REPUTATION:		return script.GetReputation();
	default:						return pChar->GetVar( varIndex );
	}
	//return 0;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::ShowDialog( LPCSTR pszText, BOOL bEndDialog )
{
	//////////////////////////////////////////////
	CHECK_PLAYER();

	MakeShowTextPacket(bEndDialog?MSG_CG_EVENT_SHOWTEXT_CMD::END_DIALOG : MSG_CG_EVENT_SHOWTEXT_CMD::SHOW_DIALOG
							,pszText);
	SEND_PLAYER(m_msgShowText);

	//theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
	//										,gameui::eToggleDialogText
	//										,(LPARAM)pszText);

	//theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
	//										,gameui::eShowDialogNext
	//										,(LPARAM)!bEndDialog);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::ShowAsk( KEYWORD varID, LPCSTR pszText )
{
	CHECK_PLAYER();

	if( !m_pPlayer->RegisterVar( varID ) )
	{
		LOGINFO( "ShowAsk脚本错误，CharacterID id:%ld ask failed.\n", m_dwCharacterID );
		return;
	}

	//////////////////////////////////////////////

	MakeShowTextPacket(MSG_CG_EVENT_SHOWTEXT_CMD::SHOW_ASK, pszText);
	SEND_PLAYER(m_msgShowText);

	//theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
	//										,gameui::eShowQuestionText
	//										,(LPARAM)pszText);
}

void	GameServerScriptInterface::MakeShowTextPacket	(BYTE byType, LPCTSTR pszText)
{
	m_msgShowText.m_byType			= byType;
	m_msgShowText.m_dwTargetNpcID	= GetTargetNpcID();
	m_msgShowText.m_wLength			= math::Min((INT)_countof(m_msgShowText.m_szInfo), lstrlen(pszText)+1);
	lstrcpyn(m_msgShowText.m_szInfo, pszText, m_msgShowText.m_wLength);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Tell( LPCSTR pszText )
{
	CHECK_PLAYER();

	MakeShowTextPacket(MSG_CG_EVENT_SHOWTEXT_CMD::SHOW_TELL, pszText);
	SEND_PLAYER(m_msgShowText);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Talk( DWORD /*time*/, DWORD /*color*/, LPCSTR pszText )
{
	__CHECK2(m_dwCharacterID != -1, );

	if ( !pszText || strlen(pszText) <= 0 )
		return;
	//MSG_SHOWTEXT msg;
	//msg.bTile = FALSE;
	//msg.byAlign = MsgShowText::eAlignMiddle;
	//msg.dwTime = time;
	//msg.dwColor = color;
	//strcpy( msg.szText, MsgShowText::STRING_LEGTH, pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::TalkInShout( DWORD /*time*/, DWORD /*color*/, LPCSTR pszText )
{
	__CHECK2(m_dwCharacterID != -1, );
	if ( !pszText || strlen(pszText) <= 0 )
		return;
	//MSG_SHOWTEXT msg;
	//msg.bTile = FALSE;
	//msg.byAlign = MsgShowText::eAlignMiddle;
	//msg.dwTime = time;
	//msg.dwColor = color;
	//strcpy( msg.szText, MsgShowText::STRING_LEGTH, pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::TalkInSector	(int         /*nMapId*/
                                    ,int         /*nX*/
												,int         /*nY*/
												,DWORD       /*time*/
												,DWORD       /*color*/
												,LPCSTR pszText)
{
	__CHECK2(m_dwCharacterID != -1, );
	if ( !pszText || strlen(pszText) <= 0 )
		return;
	//MSG_SHOWTEXT msg;
	//msg.bTile = FALSE;
	//msg.byAlign = MsgShowText::eAlignMiddle;
	//msg.dwTime = time;
	//msg.dwColor = color;
	//strcpys( msg.szText, MsgShowText::STRING_LEGTH, pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Broadcast( LPCSTR /*pszText*/ )
{
  __CHECK2(m_dwCharacterID != -1, );
    
   //MSG_CHAT_REQ Msg(gameui::CHAT_TYPE_BULL);
	//Msg.header.stID = (GameCharID)m_dwCharacterID;
	//Msg.SetString( pszText );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::JumpToMap( int nMapId, int x, int y )
{
	CHECK_PLAYER();
	//GameServerScriptHandler script(m_pPlayer);
	//script.JumpToMap( nMapId, x, y );
	//theSceneLogicFlow.GotoVillageScene((MAPCODE)nMapId ,x	 ,y);
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::OpenShop ( DWORD npcID )
{
	CHECK_PLAYER();

	MSG_CG_EVENT_OPENSHOP_CMD		msgCmd;
	msgCmd.m_dwNpcID	= npcID;

	SEND_PLAYER(msgCmd);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::DoCommand( LPCSTR /*command*/ )
{
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Show	(short /*stImageId*/
                              ,int   /*x*/
										,int   /*y*/
										,DWORD /*dwLife*/
										,DWORD /*dwFlag*/
										,DWORD /*dwFlagTime*/)
{
	
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::RemoveAllEquip( )
{
	CHECK_PLAYER();

	GameServerScriptHandler script(m_pPlayer);
	script.RemoveAllEquip( );
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::RemoveAllItem( )
{
	CHECK_PLAYER();

	GameServerScriptHandler script(m_pPlayer);
	script.RemoveAllItem( );
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::SetBornPoint( DWORD dwMapID, int iPosX, int iPosY )
{
	CHECK_PLAYER();
	
	//SetBornPoint	
	GameServerScriptHandler script(m_pPlayer);
	script.SetBornPoint( dwMapID, iPosX, iPosY );
	
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::AddItem( LPCSTR pszItemName, int nItemCount )
{
	CHECK_PLAYER();
	{
		GameServerScriptHandler script(m_pPlayer);
		script.AddItem( pszItemName, nItemCount );
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::DeleteItem( LPCSTR pszItemName, int nItemCount )
{
	CHECK_PLAYER();
	{
		GameServerScriptHandler script(m_pPlayer);
		script.DeleteItem( pszItemName, nItemCount );
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameServerScriptInterface::ExistItem(LPCSTR  pItemName, int nItemCount )
{
	CHECK_PLAYER2();

	GameServerScriptHandler script(m_pPlayer);
	return script.ExistItem(pItemName, nItemCount);

	
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameServerScriptInterface::IsItemInEquip( LPCSTR pItemName )
{
	CHECK_PLAYER2();

	sITEMINFO_BASE* pItemInfo;

	pItemInfo = theItemInfoParser.GetItemInfo(pItemName);
	__CHECK_PTR(pItemInfo);

	return m_pPlayer->GetItemManager()->IsExistItem(SI_EQUIPMENT
                                                ,pItemInfo->m_Code
																,1
																,NULL
																,NULL);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameServerScriptInterface::IsSkillActived( WORD skillItemID,WORD skillLevel )
{
	CHECK_PLAYER2();

	GameServerScriptHandler script(m_pPlayer);
	return script.IsSkillActived( skillItemID,skillLevel );
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int	GameServerScriptInterface::GetMapCodeByName( LPCSTR /*pszMapName*/ )
{
	return -1;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::TeashSkill( const std::vector<int>& /*vectorSkill*/ )
{
	//msg.nNum = vectorSkill.size();
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::AddSkill( int /*nSkillId*/ )
{

	//__CHECK2(m_dwCharacterID != -1, );
	//
	//int iCharID = GetCharacterID();
	//Player* m_pPlayer = (Player*)thePlayerManager.FindPlayerUser( iCharID );
	//if ( !m_pPlayer )
	//	return;
	//if( m_pPlayer->LearnSkill( nSkillId, 0 ) == FALSE )
	//{
	//	LOGINFO( "脚本学习技能失败 ID=%d", nSkillId );
	//}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD GameServerScriptInterface::Quest_CancelQuest( int nQuestID )
{
	
	CHECK_PLAYER2();

	GameServerScriptHandler script(m_pPlayer);
	return script.CancelQuest( nQuestID );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD GameServerScriptInterface::Quest_DoneQuest( int nQuestID )
{
	
	CHECK_PLAYER2();
	GameServerScriptHandler script(m_pPlayer);
	return script.DoneQuest( nQuestID );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Quest_SetQuestState( int nQuestID, int nState )
{
	
	CHECK_PLAYER();
	{
		GameServerScriptHandler script(m_pPlayer);
		script.SetQuestState( nQuestID, nState );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Quest_GetQuestState( int nQuestID, int nVarID )
{
	
	CHECK_PLAYER();
	{
		GameServerScriptHandler script(m_pPlayer);
		//m_pPlayer->SetVar( varIndex, script.GetQuestState( nQuestId ) );
		script.GetQuestState( nQuestID, nVarID );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Quest_RelateQuest( int nQuestID, int nState, int nEnter )
{
	__CHECK2(m_dwCharacterID != -1, );


	MapNPC*			pNpc;

	pNpc				= (MapNPC*)theGlobalManager.FindCharacter(m_dwCharacterID);

	__VERIFY2	(pNpc && pNpc->IsMapNpc()
					,FMSTR("Quest_RelateQuest()脚本错误，CharacterID id:%ld 无效\n", m_dwCharacterID)
					,);

	sQUEST_RELATEINFO info;
	info.m_QuestID		= nQuestID;
	info.m_State		= nState;
	info.m_Entrance	= nEnter;
	pNpc->AddRelateQuestInfo( &info );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::Quest_CanGetNewQuest( int nVarID )
{
	CHECK_PLAYER();
	{
		GameServerScriptHandler script(m_pPlayer);
		script.CanGetNewQuest( nVarID );
	}
	
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	GameServerScriptInterface::Printf( LPCSTR string, ... )
{
	if( !string )
		return;

	va_list	va;
	static char data[1024];
	va_start( va, string );
	vsnprintf( (char*)data,1024, string, va );
	va_end( va );

	LOGINFO(data);
}


void GameServerScriptInterface::RefreshMonster	(int    /*nMapId*/
                                          ,int    /*x*/
														,int    /*y*/
														,LPCSTR /*szArea*/)
{
}

int GameServerScriptInterface::GetCharCount( int /*nMapId*/ )
{
	//theObjectManager.GetPlayerCount();
	return 0;
	
}

BOOL	GameServerScriptInterface::CheckPlayer(LPCTSTR szErrorText)
{
	m_pPlayer = thePlayerManager.FindPlayer( m_dwCharacterID );
	if( !m_pPlayer )
	{
		LOGINFO( szErrorText );
	}
	return m_pPlayer != NULL;
}

BOOL	GameServerScriptInterface::SendPacket(MSG_OBJECT_BASE * pMsg, WORD wSize, BOOL bCrypt)
{
	if(m_pPlayer)
		m_pPlayer->SendPacket(pMsg, wSize, bCrypt);
	return FALSE;
}




void GameServerScriptInterface::PlayBGMusic( LPCSTR szFileName, DWORD dwParam )
{
	CHECK_PLAYER();

	MSG_CG_EVENT_PLAYMUSIC_CMD		msgCmd;
	msgCmd.m_dwParam	= dwParam;
	msgCmd.m_wLength	= math::Min((INT)_countof(msgCmd.m_szFilePath), lstrlen(szFileName)+1);
	lstrcpyn(msgCmd.m_szFilePath, szFileName, msgCmd.m_wLength);

	SEND_PLAYER(msgCmd);
}

void GameServerScriptInterface::MakeItem	(BYTE byCreatNewOrOld
                                    ,int  nRate
												,int  nIdBegin[]
												,int  nIdEnd[]
												,int  nCount)
{
	__UNUSED(byCreatNewOrOld);
	__UNUSED(nRate);
	__UNUSED(nIdBegin);
	__UNUSED(nIdEnd);
	__UNUSED(nCount);
}

void GameServerScriptInterface::ShowQuestDialog(KEYWORD varID , LPCSTR pszText )
{
	CHECK_PLAYER();

	m_pPlayer->SetScriptTargetID( m_dwTargetNpcID );

	if( !m_pPlayer->RegisterVar( varID ) )
	{
		LOGINFO( "GameServerScriptInterface::ShowQuestDialog(%d) error", varID );
	}


	MakeShowTextPacket(MSG_CG_EVENT_SHOWTEXT_CMD::SHOW_QUEST, pszText);
	SEND_PLAYER(m_msgShowText);

	//theGameUIManager.UITriggerFunc(gameui::eUINpcChatDialogBox
	//										,gameui::eShowQuestList
	//										,(LPARAM)szInfo);
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptInterface::UseSkill( int nSkillId, int nSkillLevel )
{
	//theHeroActionInput.DoSkillProcessing(nSkillId+nSkillLevel);
	//pDst->TryAttack(&target, pSkill, nSkillId, nSkillLevel );
}

void GameServerScriptInterface::PopNpcList()
{
	//MSG_POPMAPNPCLIST_CMDStruct pop;
	//theServer.SendMsgToSingle( &pop, pDst );
}

int GameServerScriptInterface::GetWorldTime( int /*nTimeType*/ )
{
	return 0;
}

int GameServerScriptInterface::GetBirthday( int /*nTimeType*/ )
{
	return 0;
}

void GameServerScriptInterface::ShowBank()
{
	//MSG_UPDATEBANK msg;
	//msg.dwCurMoney = pBank->dwCurMoney;
	//msg.dwMaxMoney = pBank->dwMaxMoney;
	//msg.dwPlayerMoney = m_pPlayer->GetMoney();
}



void GameServerScriptInterface::NpcMoveNext()
{
}

void GameServerScriptInterface::NpcStopMove()
{
}

void GameServerScriptInterface::NpcStartMove()
{
}

void GameServerScriptInterface::NpcPlayAnim	(LPCSTR /*szName*/
                                       ,int    /*nTimes*/
													,LPCSTR /*szEndName*/
													,BOOL   /*bForView*/)
{
	//MSG_NPCPLAYANIM msg;
	//msg.nNpcId = GetDestID();
	//StringUtil::SafeNCpy( msg.szActionName, szName, MsgNpcPlayAnim::eActionNameLength );
	//StringUtil::SafeNCpy( msg.szEndActionName, szEndName, MsgNpcPlayAnim::eActionNameLength );
	//msg.nLoopTimes = nTimes;
}

BOOL GameServerScriptInterface::CanLearnSkill( int /*nSkillId*/, int /*nSkillLevel*/ )
{
	return TRUE;
}

void GameServerScriptInterface::DoRide( int /*nID*/ )
{
	//Player* m_pPlayer = (Player*)thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::CanLearnSkill error" );
	//	return;
	//}
	//m_pPlayer->DoRide( nID );
}

int GameServerScriptInterface::GetDromeID()
{
	return -1;
	//Player* m_pPlayer = (Player*)thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::GetDromeID error" );
	//	return -1;
	//}
	//return m_pPlayer->GetDromeID();
}

void GameServerScriptInterface::SetDialogName( LPCSTR /*szName*/ )
{
	//MSG_SETDIALOGNAME_CMD msg;
	//strncpy( msg.szName, szName, MSG_SETDIALOGNAME_CMDStruct::NAME_MAXLENGTH );
}

void GameServerScriptInterface::SetHotkey( int /*nIndex*/, int /*nID*/ )
{
	//MSG_SCRIPT_SETHOTKEY_CMD msg;
	//msg.ustIndex = nIndex;
	//msg.nID = nID;
}

BOOL GameServerScriptInterface::DumpInfoToDB( LPCSTR /*szInfo*/ )
{
   return TRUE;
} 

int GameServerScriptInterface::GetTeamPlayerCount()
{
	return 1;
	//Player* m_pPlayer = thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::GetTeamPlayerCount error" );
	//	return 1;
	//}
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( m_pPlayer->GetTeamID() );
	//if ( pTeam )
	//		return pTeam->GetTeamNumber();
	//	return 1;
}

int GameServerScriptInterface::GetTeamPlayerLevel( int /*nType*/ )
{
	return 0;
	//Player* m_pPlayer = thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::GetTeamPlayerCount error" );
	//	return 0;
	//}
	//int nLevel = m_pPlayer->GetLevel();
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( m_pPlayer->GetTeamID() );
	//if ( pTeam )
	//{
	//	for ( int n=0; n<pTeam->GetTeamNumber(); n++ )
	//	{
	//		DWORD dwID = pTeam->GetTeamMemberID( n );
	//		Player* pTeamPlayer = thePlayerManager.FindPlayerUser( dwID );
	//		assert( pTeamPlayer );
	//		if( !pTeamPlayer )
	//			continue;
	//		switch( nType ) 
	//		{
	//		case 0:
	//			{
	//				if ( pTeamPlayer->GetLevel() < nLevel )
	//					nLevel = pTeamPlayer->GetLevel();
	//			}
	//			break;
	//		case 1:
	//			{
	//				if ( pTeamPlayer->GetLevel() > nLevel )
	//					nLevel = pTeamPlayer->GetLevel();
	//			}
	//			break;
	//		default:
	//			assert(FALSE);
	//			break;
	//		}
	//	}
	//}
	//return nLevel;
}

int GameServerScriptInterface::GetTeamPlayerID( UINT /*nIndex*/ )
{
	//Player* m_pPlayer = thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::GetTeamPlayerID error" );
	//	return -1;
	//}
	//PlayerTeam * pTeam = NULL;
	//pTeam = GetPlayerTeamManager().GetTeam( m_pPlayer->GetTeamID() );
	//if ( pTeam )
	//{
	//	if ( nIndex < (WORD)pTeam->GetTeamNumber() )
	//	{
	//		return pTeam->GetTeamMemberID( nIndex );
	//	}
	//}
	return -1;
	
}


void GameServerScriptInterface::SetTargetPlayer( DWORD /*dwID*/)
{
	//Player* m_pPlayer = (Player*)thePlayerManager.FindPlayerUser( m_dwCharacterID );
	//if( !m_pPlayer )
	//{
	//	LOGINFO( "GameServerScriptInterface::SetTargetPlayer error" );
	//	return;
	//}
	//m_pPlayer->SetTargetPlayer( nID );
}

void GameServerScriptInterface::CreateMonster	(DWORD /*dwMapId*/
                                       ,float /*fPosX*/
													,float /*fPosY*/
													,float /*fRadius*/
													,float /*fBodySize*/
													,int   /*nMonsterIndex*/
													,int   /*nMonsterNum*/)
{
}


int GameServerScriptInterface::GetEmptySlotCount()
{
	
	CHECK_PLAYER2();

	GameServerScriptHandler script(m_pPlayer);
	return script.GetEmptySlotCount();
	
}

void GameServerScriptInterface::CreateGuild()
{
    //Player* m_pPlayer = (Player*)thePlayerManager.FindPlayerUser( m_dwCharacterID );
    //if ( !m_pPlayer )
    //{
    //    LOGINFO("错误:CreateGuild");
    //    return;
    //}
    //m_pPlayer->CreateGuild();
}


void	GameServerScriptInterface::LogTip(LPCSTR      pszText,LPCSTR      szTip2,eTIP_TYPE type)
{
	CHECK_PLAYER();

	StringHandle	sTip;
	sTip.Format(_T("%s`%s"),pszText,szTip2);


	MakeShowTextPacket(type, sTip);
	SEND_PLAYER(m_msgShowText);
}


#include "stdafx.h"
#include "Monster.h"
#include "Field.h"
#include "Map.h"
#include "BaseZone.h"
#include "BaseRoom.h"
#include "Party.h"
#include "PartyManager.h"
#include "StatusManager.h"
#include "FormularManager.h"
#include "RegenLocation.h"
#include "AIParamParser.h"
#include "DropManager.h"
#include "AIStateManager.h"

Monster::Monster() 
{
	SetObjectType( MONSTER_OBJECT );
}

Monster::~Monster()
{

}

BOOL Monster::Init()
{	
	m_nReferencePlayerNum = 0;
	SetRegenPos( Vector3D::ZERO );

	return _SUPER::Init();
}

VOID Monster::Release()
{
	_SUPER::Release();

	m_pRegenLocation	= NULL;
}

VOID Monster::SetPlayerRefCounter	(int nCount )
{
	m_nReferencePlayerNum = nCount;
}

VOID Monster::Update( DWORD dwTick )
{
	/////////////////////////////////////
	AIProgress();

	/////////////////////////////////////
	if( m_nReferencePlayerNum )
	{
		if( m_pAggroTimer->IsExpired() )
		{
			RegisterBattleRecords();

			if( GetTargetChar() != NULL )
			{
				SelectBestTarget();
			}
		}

		m_pAIStateMan->Update( dwTick );
		m_pBattleRecord->Update( dwTick );
	}

	_SUPER::Update( dwTick );
}

VOID Monster::GetRenderInfo( sMONSTERINFO_RENDER * OUT pRenderInfo )
{
	_SUPER::GetRenderInfo(pRenderInfo);
	//pRenderInfo->m_byType = NPC_FUNC_MONSTER;
}


VOID Monster::OnEnterField( Field* pField, Vector3D* pvPos ) 
{
	//Init();

	if( !pField )
		return;

	////////////////////////////////////////
	Map* pMap = pField->GetMap();
	if( !pMap )	
		return;

	////////////////////////////////////////
	BaseZone * pZone = pMap->GetGameZone();
	if( !pZone )
		return;


	////////////////////////////////////////////////////
	_SUPER::OnEnterField(pField, pvPos);


	SetRegenPos( *pvPos );

	if(	pZone->GetZoneType() == ZONETYPE_HUNTING 
		||	pZone->GetZoneType() == ZONETYPE_MISSION )
	{
		BaseRoom * pRoom = (BaseRoom *)pZone;
		Party *pParty = pRoom->GetParty( 0 );
		if( pParty )
		{
			OnPartyNumberSet( pParty->GetMemberNum() );
		}
	}

	////////////////////////////////////////////////////
	if( CanRegenHPMP() )
	{
		sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();
		m_pStatusManager->AllocStatus(CHARSTATE_ETC_AUTO_RECOVER_HPMP
                                   ,BASE_EXPIRE_TIME_INFINITY
											  ,aiParam.m_NpcHPMPRegenPeriod);
	}



	////////////////////////////////////////////////////
	//pField->AddNPC( this );


	////////////////////////////////////////////////////
	//DWORD dwStateID = AISTATE_IDLE;
	//if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_SEARCH_AREA )
	//{
	//	dwStateID	= AISTATE_TRACK_AREA;
	//}
	//m_pAIStateMan->OnEnterField(pField,dwStateID );
}


VOID Monster::OnLeaveField()
{
	////////////////////////////////////////////////////
	if( m_pRegenLocation )
	{
		m_pRegenLocation->OnRemoveNPC( m_pBaseInfo->m_MonsterCode );
	}

	_SUPER::OnLeaveField();
}


VOID Monster::OnPartyNumberSet( DWORD dwCurMemberNum )
{
	if( !dwCurMemberNum )
		return;

	/////////////////////////////
	// MaxHP, CurHP
	float fIncreaseRatio = theFormularManager.CalcHPIncreaseRatioAsParty( dwCurMemberNum );
	DWORD dwMaxHP			= (DWORD)( m_pBaseInfo->m_dwMaxHP * fIncreaseRatio );
	DWORD dwCurHP			= dwMaxHP;

	////////////////////////////////////////
	m_Attr[ATTRIBUTE_MAX_HP][ATTRIBUTE_KIND_BASE] = dwMaxHP;
	m_Attr.Update();
	SetHP( dwCurHP );

	/////////////////////////////
	int	iCount = 0;
	int	iMinAttackPower,
			iMaxAttackPower, 
			iBonusMinAttackPower,
			iBonusMaxAttackPower;

	eATTRIBUTE_TYPE	eMinAttrType,
							eMaxAttrType;


	/////////////////////////////
	iMinAttackPower		= GetAttr().GetBaseMeleeMinAttPower();
	iMaxAttackPower		= GetAttr().GetBaseMeleeMaxAttPower();
	iBonusMinAttackPower = theFormularManager.CalcAttackPowerIncreaseAsParty(dwCurMemberNum
                                                                           ,iMinAttackPower)		;
	iBonusMaxAttackPower = theFormularManager.CalcAttackPowerIncreaseAsParty(dwCurMemberNum
                                                                           ,iMaxAttackPower)		;

	m_Attr[ATTRIBUTE_ADD_MELEE_MIN_ATTACK_POWER][ATTRIBUTE_KIND_BASE] = iBonusMinAttackPower;
	m_Attr[ATTRIBUTE_ADD_MELEE_MAX_ATTACK_POWER][ATTRIBUTE_KIND_BASE] = iBonusMaxAttackPower;


	//////////////////////////////////////////////////
	iMinAttackPower = GetAttr().GetBaseRangeMinAttPower();
	iMaxAttackPower = GetAttr().GetBaseRangeMaxAttPower();
	iBonusMinAttackPower = theFormularManager.CalcAttackPowerIncreaseAsParty(dwCurMemberNum
                                                                           ,iMinAttackPower)		;
	iBonusMaxAttackPower = theFormularManager.CalcAttackPowerIncreaseAsParty(dwCurMemberNum
                                                                           ,iMaxAttackPower)		;
	m_Attr[ATTRIBUTE_ADD_RANGE_MIN_ATTACK_POWER][ATTRIBUTE_KIND_BASE] = iBonusMinAttackPower;
	m_Attr[ATTRIBUTE_ADD_RANGE_MAX_ATTACK_POWER][ATTRIBUTE_KIND_BASE] = iBonusMaxAttackPower;



	/////////////////////////////
	for( BYTE byAttackType = ATTACKKIND_MAGICBASE; byAttackType <ATTACKKIND_MAGICMAX ; ++byAttackType )
	{
		iMinAttackPower		= GetAttr().GetBaseMagicMinAttPower() 
									+ GetAttr().GetMagicalAttackPower( (eATTACK_KIND)byAttackType );
		iMaxAttackPower		= GetAttr().GetBaseMagicMaxAttPower() 
									+ GetAttr().GetMagicalAttackPower( (eATTACK_KIND)byAttackType );

		iBonusMinAttackPower = theFormularManager.CalcAttackPowerIncreaseAsParty( dwCurMemberNum, iMinAttackPower );
		iBonusMaxAttackPower = theFormularManager.CalcAttackPowerIncreaseAsParty( dwCurMemberNum, iMaxAttackPower );

		eMinAttrType = (eATTRIBUTE_TYPE)( ATTRIBUTE_ADD_WATER_MIN_ATTACK_POWER + iCount );
		eMaxAttrType = (eATTRIBUTE_TYPE)( ATTRIBUTE_ADD_WATER_MAX_ATTACK_POWER + iCount );
		m_Attr[eMinAttrType][ATTRIBUTE_KIND_BASE] = iBonusMinAttackPower;
		m_Attr[eMaxAttrType][ATTRIBUTE_KIND_BASE] = iBonusMaxAttackPower;

		++iCount;
	}
	m_Attr.Update();
}



///////////////////////////////////////////////////////
BOOL Monster::IsOutOfRegenRange( Vector3D &vTargetPos ) const
{
	//������Ŀ�����
	float fDistToRegenPos = GetDist2( m_RegenPos );
	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();

	if( fDistToRegenPos > aiParam.m_NpcRegenRange * aiParam.m_NpcRegenRange )
	{
		return TRUE;
	}

	return FALSE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Monster::OnDead()
{
	Player * pOwner(NULL);

	if(	m_KillerType == PLAYER_OBJECT 
		&& m_dwKillerObjectKey != 0 )
	{
		pOwner = thePlayerManager.FindPlayer(m_dwKillerObjectKey);
	}

	///////////////////////////////
	DistributeExp();

	///////////////////////////////
	Vector3C& vDropPos = 	GetPosition();


	////////////////////////////////////////
	theDropManager.Drop	( GetField()
								, GetSectorIndex()
								, &vDropPos
								, pOwner
								, this );


	////////////////////////////////////////
	if(pOwner)
	{
		GetField()->OnMonsterDead(m_pBaseInfo->m_MonsterCode, m_dwKillerObjectKey);
	}

	return _SUPER::OnDead();
}






























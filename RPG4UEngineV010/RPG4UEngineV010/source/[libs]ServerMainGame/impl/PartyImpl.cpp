#include "StdAfx.h"
#include "Field.h"
#include "Player.h"
#include "PartyData.h"
#include "PartyImpl.h"
#include <PacketStruct_ClientGameS.h>
#include "PartyManager.h"


PartyImpl::PartyImpl()
{
	Init( INVALID_PARTY_KEY );
}

PartyImpl::~PartyImpl()
{
}

VOID PartyImpl::Init( DWORD dwMasterUserKey )
{
	_SUPER::Init(dwMasterUserKey);

	m_dwMasterUserKey		= dwMasterUserKey;
	m_dwMasterObjKey		= INVALID_PARTY_KEY;
	m_DistributionType	= ITEM_DISTRIBUTION_DEFAULT;
	m_TotalLevel			= 0;
	m_dwPartyTarget		= 0;
}


VOID PartyImpl::Release(  )
{
	_SUPER::Release();
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PartyImplChangeMasterOpr
{
public:
	Party*	m_pParty;
	DWORD		dwNewMasterUserKey;

	void operator()(Player *pMember)
	{
		BOOL bMaster = FALSE;
		bMaster = pMember->GetUserKey() == dwNewMasterUserKey;

		/////////////////////////////////////////////////
		pMember->GetPartyData().JoinParty( m_pParty->GetPartyKey(), bMaster );

		if( bMaster )
		{
			m_pParty->SetMasterObjKey( pMember->GetObjectKey() );
		}
	}
};

VOID PartyImpl::ChangeMaster( DWORD dwNewMasterUserKey )
{
	PartyImplChangeMasterOpr	opr={this,dwNewMasterUserKey};
	ForEachMember(opr);
	m_dwMasterUserKey = dwNewMasterUserKey;
}

BOOL PartyImpl::_CanJoin( DWORD dwObjKey )
{
	//////////////////////////////////////
	__CHECK( GetMemberNum() < MAX_PARTYMEMBER_NUM );

	//////////////////////////////////////
	__CHECK( NULL == GetMemberInfo( dwObjKey ) )	;

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PartyImplSendPacketAllOpr
{
public:
	MSG_OBJECT_BASE * pMsg;
	WORD					wSize;

	void operator()(Player *pMember)
	{
		pMember->SendPacket( pMsg, wSize );
	}
};



BOOL PartyImpl::SendToAll( MSG_OBJECT_BASE * pMsg, WORD wSize )
{
	PartyImplSendPacketAllOpr	opr={pMsg, wSize};
	ForEachMember(opr);
	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PartyImplSendPacketExceptOpr
{
public:
	DWORD					dwObjKey;
	MSG_OBJECT_BASE * pMsg;
	WORD					wSize;

	void operator()(Player *pMember)
	{
		if( pMember->GetObjectKey() == dwObjKey )	
			return;
		pMember->SendPacket( pMsg, wSize );
	}
};

BOOL PartyImpl::SendExcept( DWORD dwObjKey, MSG_OBJECT_BASE * pMsg, WORD wSize )
{
	PartyImplSendPacketExceptOpr	opr={dwObjKey,pMsg, wSize};
	ForEachMember(opr);

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PartyImplGetMemberInfosOpr
{
public:
	PARTY_MEMBER_STREAM *	pMemberInfoAll;
	DWORD							m_dwMasterUserKey;
	INT							m_nIndex;
	void operator()(Player *pMember)
	{
		lstrcpyn( pMemberInfoAll[m_nIndex].m_szCharName, pMember->GetCharName(), MAX_CHARNAME_LENGTH );

		pMemberInfoAll[m_nIndex].m_dwObjKey = pMember->GetObjectKey();
		pMemberInfoAll[m_nIndex].m_wLevel	= pMember->GetLevel();
		pMemberInfoAll[m_nIndex].m_dwHP		= pMember->GetHP();
		pMemberInfoAll[m_nIndex].m_dwHPMax	= pMember->GetMaxHP();
		pMemberInfoAll[m_nIndex].m_dwMP		= pMember->GetMP();
		pMemberInfoAll[m_nIndex].m_dwMPMax	= pMember->GetMaxMP();
		pMemberInfoAll[m_nIndex].m_bMaster	= (m_dwMasterUserKey == pMember->GetUserKey() );
		m_nIndex++;
	}
};
VOID PartyImpl::GetMemberInfos( PARTY_MEMBER_STREAM *pMemberInfoAll )
{
	PartyImplGetMemberInfosOpr	opr={pMemberInfoAll,m_dwMasterUserKey,0};
	ForEachMember(opr);
}



VOID PartyImpl::OnMemberLevelUp( DWORD dwObjKey )
{
	/////////////////////////////////////////////////
	Player *pMember = GetMemberInfo( dwObjKey );
	if( NULL == pMember )
		return;

	++m_TotalLevel;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class PartyImplUpdateNpcHPInfoOpr
{
public:
	DWORD	m_dwCurMemberNum;

	VOID operator () ( NPC *pNPC )
	{
		pNPC->OnPartyNumberSet( m_dwCurMemberNum );
	}
};

VOID PartyImpl::_UpdateNpcHPInfo()
{
	/////////////////////////////////////////////////
	Character *pMember = GetMemberInfo( m_dwMasterObjKey );
	if( !pMember )
		return;

	Field* pField = pMember->GetField();
	if( !pField )
		return;

	/////////////////////////////////////////////////
	PartyImplUpdateNpcHPInfoOpr opr={ GetMemberNum() };
	pField->ForEachNPC( opr );
}















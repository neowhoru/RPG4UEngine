/*////////////////////////////////////////////////////////////////////////
文 件 名：Summoned.cpp
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "Summoned.h"
#include "Field.h"
#include "StatusManager.h"
#include "SkillInfoParser.h"
#include "SkillSlotContainer.h"
#include "Player.h"
#include "SkillManager.h"
#include "SkillSlot.h"
#include "ObjectSystem.h"
#include "AIStateManager.h"

Summoned::Summoned() : m_pSummoner(NULL)
{
	m_pLifeTimer = new util::Timer;

	SetObjectType( SUMMON_OBJECT );
}

Summoned::~Summoned()
{
	SAFE_DELETE( m_pLifeTimer );
}

BOOL Summoned::Init()
{	
	m_CurrentCommand		= SUMMON_COMMAND_DELEGATE_ATTACK;
	m_bSkillUseDelegate	= FALSE;

	return _SUPER::Init();
}

VOID Summoned::Release()
{
	m_pSummoner = NULL;

	_SUPER::Release();
}

VOID Summoned::Update( DWORD dwTick )
{
	/////////////////////////////////////////
	if( m_pLifeTimer->IsExpired() && IsAlive() )
	{
		SetHP(0);
		OnDead();
		return;
	}

	/////////////////////////////////////////
	AIProgress();

	/////////////////////////////////////////
	if(	m_pAggroTimer->IsExpired() 
		&& GetTargetChar() )
	{
		RegisterBattleRecords();

		/////////////////////////////////////////
		if( IsOffensive() || IsDefensive() )
			SelectBestTarget();
	}

	/////////////////////////////////////////
	m_pAIStateMan->Update( dwTick );

	m_pBattleRecord->Update( dwTick );

	_SUPER::Update( dwTick );
}


VOID Summoned::InitAIStates( DWORD dwStateID, LPARAM param1 )
{
	m_pAIStateMan->Release();

	static DWORD	arStates[AISTATE_MAX]=
	{
		 AISTATE_UNKNOWN

		,AISTATE_SUMMON_IDLE//AISTATE_WANDER
		,AISTATE_SUMMON_IDLE//AISTATE_IDLE
		,AISTATE_TRACK
		,AISTATE_ATTACK
		,0//AISTATE_HELP

		,AISTATE_THRUST
		,AISTATE_DEAD
		,AISTATE_FLYING
		,AISTATE_KNOCKDOWN
		,AISTATE_JUMP

		,AISTATE_FALL_APART
		,AISTATE_SUMMON_IDLE//AISTATE_RETURN
		,AISTATE_RETREAT
		,AISTATE_RUNAWAY
		,AISTATE_CHAOS

		,0,0,0,0,0,0
		//,AISTATE_SUMMON_IDLE
		//,AISTATE_PATROL
		//,AISTATE_SPAWN_IDLE
		//,AISTATE_STOP_IDLE
		//,AISTATE_TRACK_AREA 
		//,AISTATE_MAPNPC_WANDER
	};

	LinkAIStates(arStates);


	m_pAIStateMan->Init();

	///////////////////////////////////////
	GetStatusManager()->AllocStatus(CHARSTATE_ETC_DISABLE_VISION
                                  ,m_pBaseInfo->m_dwSpawnTime
											 ,0);
}


VOID Summoned::GetRenderInfo( sMONSTERINFO_RENDER * OUT pRenderInfo )
{
	_SUPER::GetRenderInfo(pRenderInfo);
	//pRenderInfo->m_byType = NPC_FUNC_SUMMONED;
}


VOID Summoned::OnEnterField( Field* pField, Vector3D* pvPos ) 
{
	Init();

	Character::OnEnterField(pField, pvPos);
	ASSERTV( GetField() != NULL, "GetField() == NULL" );

	pField->AddNPC( this );

	m_pAIStateMan->OnEnterField(pField
                                    ,AISTATE_IDLE
												,m_pBaseInfo->m_dwSpawnTime);
}

VOID Summoned::OnLeaveField()
{
	_SUPER::OnLeaveField();
}


BOOL Summoned::OnDead()
{
	return _SUPER::OnDead();
}

VOID Summoned::AddSkill( SLOTCODE skillCode )
{
	SkillDetailInfo *pNewSkill;
	SkillDetailInfo *pBaseSkill;
	
	/////////////////////////////////////////////
	pNewSkill = theSkillInfoParser.GetSkillInfo( skillCode );
	if( !pNewSkill )
		return;

	/////////////////////////////////////////////
	for( int i = 0; i < _countof(m_pBaseInfo->m_wSkillCode); ++i )
	{
		if( !m_pBaseInfo->m_wSkillCode[i] )	
			continue;
		
		pBaseSkill = theSkillInfoParser.GetSkillInfo( m_pBaseInfo->m_wSkillCode[i] );
		if( !pBaseSkill )
			continue;

		if( pBaseSkill->m_SkillClassCode != pNewSkill->m_SkillClassCode )
			continue;
	
		m_SkillSelector.Add(skillCode
                         ,m_pBaseInfo->m_bySkillRate[i])			;
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL Summoned::DelSkill( SLOTCODE skillCode )
{
	return m_SkillSelector.Remove( skillCode );
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID Summoned::SetSummoner( Character *pSummoner )
{
	m_pSummoner = pSummoner;

	SetSummonerKey( pSummoner->GetObjectKey() );

	if(! pSummoner->IsPlayer())
		return;

	////////////////////////////////////////////////////////////////////////
	Player *					pPlayer = (Player*)pSummoner;
	SkillDetailInfo *		pBaseSkill;
	SkillSlotContainer *	pContainer;
	SkillSlot *				pSkillSlot;

	////////////////////////////////////////////////////////////////////////
	m_SkillSelector.Release();
	for( int i = 0; i < _countof(m_pBaseInfo->m_wSkillCode); ++i )
	{
		if( !m_pBaseInfo->m_wSkillCode[i] )	
			continue;

		pBaseSkill = theSkillInfoParser.GetSkillInfo( m_pBaseInfo->m_wSkillCode[i] );
		if( !pBaseSkill )
			continue;

		pContainer = pPlayer->GetSkillManager()->GetSkillSlotContainer();
		pSkillSlot = pContainer->GetSkillSlotBy( pBaseSkill->m_SkillClassCode );
		if( !pSkillSlot )
			continue;

		m_SkillSelector.Add(pSkillSlot->GetCode()
                         ,m_pBaseInfo->m_bySkillRate[i])			;
	}
}

VOID Summoned::SetExpireTime( DWORD dwInterval )
{
	m_pLifeTimer->SetTimer( dwInterval );

	if( !dwInterval )
		m_pLifeTimer->DisableCheckTime();
}

VOID Summoned::RunCommand( eSUMMON_COMMAND eCommand, DWORD dwTargetKey )
{
	switch(eCommand)
	{
		////////////////////////////////////////////////////////
	case SUMMON_COMMAND_ATTACK:
		{
			AIMSG_FORCE_ATTACK aiMsg;
			aiMsg.dwTargetKey = dwTargetKey;
			AINotice( &aiMsg, sizeof(aiMsg) );
		}
		break;

		////////////////////////////////////////////////////////
	case SUMMON_COMMAND_FOLLOW:
		{
			AIMSG_COMMAND_FOLLOW aiMsg;
			AINotice( &aiMsg, sizeof(aiMsg) );
			m_CurrentCommand = eCommand;
		}
		break;

		////////////////////////////////////////////////////////
	case SUMMON_COMMAND_DELEGATE_ATTACK:	
	case SUMMON_COMMAND_DELEGATE_DEFENSE:
		m_CurrentCommand = eCommand;
		break;

	case SUMMON_COMMAND_SKILL_ATTACK:
		SetSkillUseDelegate( TRUE );
		break;

	default:
		LOGMSG(LOG_CRITICAL
            ,"[RunCommand] Invalid Command[%d] ObjKey[%d]"
				,eCommand
				,GetObjectKey());
	}
}


BOOL Summoned::CheckClassDefine( BYTE byUserType, CODETYPE dwClassDefine )
{
	__CHECK ( byUserType == SKILLUSER_SUMMONED );

	return ( m_pBaseInfo->m_wClass == dwClassDefine );
}






















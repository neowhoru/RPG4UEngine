#include "stdafx.h"
#include "SkillInfoParser.h"
#include "NPC.h"
#include "Field.h"
#include <PacketStruct_CG.h>
#include "Character.h"
#include "Player.h"
#include "Skill.h"
#include "AITypes.h"
#include <Battle.h>
#include <3DTerrain\3DTerrain.h>



VOID Skill::BroadcastAreaThrust()
{
	// 타겟이 필드에 있는지 체크
	NPC *pNPC = m_pOwnerChar->GetField()->FindNPC( m_SkillInfo.dwPrimaryTargetKey );
	if( !pNPC )		return;

	Field *pField = m_pOwnerChar->GetField();
	if( !pField ) return;

	// 밀리는 거리(어택시작점-어택끝점)가 너무 길면 접속을 끊는다.
	if( VectorLength( &( m_SkillInfo.wvCurPos - m_SkillInfo.wvDestPos ) ) > 5.0f )
	{
		DISPMSG( "Warning: 플레이어 공격 좌표 해킹 가능성 있음\n" );
		((Player*)m_pOwnerChar)->ForceDisconnect();
		return;
	}

	// 이제 패킷 데이터를 믿기로 하고 플레이어 위치를 어택 시작점으로 옮겨서 밀리기 처리 시작
	m_pOwnerChar->SetPos( &m_SkillInfo.wvCurPos );
	pField->FindPathThrust( m_pOwnerChar, &( m_SkillInfo.wvDestPos - m_SkillInfo.wvCurPos ) );
	m_pOwnerChar->SetMove( TRUE );
	m_pOwnerChar->SetMoveSpeed( SWIPE_SPEED );

	// 스타일 공격 시작에 대한 브로드 캐스팅은 해준다.
	MSG_CG_STYLE_P2M_ATTACK_BRD brdMsg;
	brdMsg.m_byCategory				= CG_STYLE;
	brdMsg.m_byProtocol				= CG_STYLE_P2M_ATTACK_BRD;
	brdMsg.dwAttackerKey			= m_pOwnerChar->GetObjectKey();
	brdMsg.byAttackType				= m_SkillInfo.byAttackType;
	brdMsg.dwClientSerial			= m_SkillInfo.dwClientSerial;
	brdMsg.dwPrimaryTargetKey		= m_SkillInfo.dwTargetKey[0];
	brdMsg.wvCurPos					= m_SkillInfo.wvCurPos;
	brdMsg.wvDestPos				= m_SkillInfo.wvDestPos;

	pField->SendPacketAround( m_pOwnerChar, (MSG_BASE_FORWARD*)&brdMsg, brdMsg.GetSize() );
}

VOID Skill::ExecuteAreaThrust()
{
	Field *pField = m_pOwnerChar->GetField();
	if( !pField ) return;

	// 브로드 캐스팅 패킷 준비
	MSG_CG_STYLE_P2M_ATTACK_RESULT_BRD brdMsg;
	brdMsg.m_byCategory					= CG_STYLE;
	brdMsg.m_byProtocol					= CG_STYLE_P2M_ATTACK_RESULT_BRD;
	brdMsg.dwClientSerial				= m_SkillInfo.dwClientSerial;
	brdMsg.dwAttackerKey				= m_pOwnerChar->GetObjectKey();

	WzVector vPlayerPos, vNPCPos;
	m_pOwnerChar->GetPos( &vPlayerPos );

	// 받은 리스트 중에서 공격에 유효한 몬스터들만 데미지 처리 후 브로드 캐스트 패킷에 추가
	BOOL bMainTargetFound = FALSE;
	WzVector vDirToMainTarget;
	BYTE byHitObjectCount = 0;
	NPC *pNPC;
	WzVector wvNormalVectorP2M, wvPosAfterThrust;
	for( DWORD i = 0; i < m_SkillInfo.byNumberOfTargets; ++i )
	{
		// 공격 대상 찾기
		pNPC = pField->FindNPC( m_SkillInfo.dwTargetKey[i] );
		if( !pNPC ) continue;

		pNPC->GetPos( &vNPCPos );

		WORD wDamage = pNPC->CalculateDamage( 100 );

		pNPC->DecreaseHP( wDamage );

		// 거리 판별(플레이어와 타겟 NPC간의 거리)
		if( VectorLength( &(vPlayerPos - vNPCPos) ) < m_pOwnerChar->GetAttackRange() + RANGE_DECISION_TOLERANCE )
		{
			// 메인 타겟이 아직 없다면 이 NPC로 설정
			if( !bMainTargetFound )
			{
				vDirToMainTarget = vNPCPos - vPlayerPos;
				bMainTargetFound = TRUE;
			}

			// 부채꼴 체크
			if( IsPositionInSector( &vNPCPos, &vPlayerPos, &vDirToMainTarget, WZ_PI ) )
			{
				// NPC 밀리기 처리.. 추후 공격 타입에 따라 밀리는 거리 다르게 처리해야 함
				VectorNormalize( &wvNormalVectorP2M, &(vNPCPos - vPlayerPos) );
				wvNormalVectorP2M = wvNormalVectorP2M * 3.0f;
				pField->FindPathThrust( pNPC, &wvNormalVectorP2M );
				wvPosAfterThrust = pNPC->GetPathExplorer()->GetTargetPos();
				pNPC->SetMove( TRUE );
				pNPC->SetMoveSpeed( 15.0f * SPEED_MULTIPLIER );

				// NPC에게 밀리기 메세지를 보냄
				AI_MSG_THRUST aiMsg;
				pNPC->SendAIMessage( &aiMsg, sizeof(aiMsg) );

				// 패킷에 추가
				brdMsg.AttackInfo[byHitObjectCount].dwTargetKey		= pNPC->GetObjectKey();
				brdMsg.AttackInfo[byHitObjectCount].dwDamage		= wDamage;
				brdMsg.AttackInfo[byHitObjectCount].dwTargetHP		= pNPC->GetHP();
				brdMsg.AttackInfo[byHitObjectCount].wvCurPos		= vNPCPos;
				brdMsg.AttackInfo[byHitObjectCount].wvDestPos		= wvPosAfterThrust;
				byHitObjectCount++;
			}
		}
	}

	brdMsg.byNumberOfTargets = byHitObjectCount;

	if( byHitObjectCount > 0 )
	{
		pField->SendPacketAround( m_pOwnerChar, (MSG_BASE_FORWARD*)&brdMsg, (WORD)brdMsg.GetSize() );
	}
}

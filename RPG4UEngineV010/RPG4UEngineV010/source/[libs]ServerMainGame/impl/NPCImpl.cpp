/*////////////////////////////////////////////////////////////////////////
文 件 名：NPCImpl.cpp
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "NPCImpl.h"
#include "AIState.h"
#include "Field.h"
#include "AIMsgQueue.h"
#include "AITypes.h"
#include <Timer.h>
#include "ObjectGroup.h"
#include <PacketStruct_ClientGameS.h>
#include <TileWorldInc.h>
#include "FormularManager.h"
#include "Map.h"
#include "SkillInfoParser.h"
#include "SummonedRelation.h"
#include "ServerUtil.h"
#include <MathHelper.h>
#include "Party.h"
#include "PartyManager.h"
#include "ObjectGroupManager.h"
#include "SummonManager.h"
#include "BaseZone.h"
#include "BaseRoom.h"
#include "AIParamParser.h"
#include "StatusManager.h"
#include "Summoned.h"
#include "MoveStateHandle.h"
#include "AIStateManager.h"
#include "BattleUtil.h"
#include "FormularManager.h"
#include "AIInfoParser.h"


//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////
using namespace util;

NPCImpl::NPCImpl()
{
	m_pMoveStateHandle	= &m_MoveStateHandle;
	m_Attr.SetOwner( this );
}

NPCImpl::~NPCImpl()
{
}

VOID NPCImpl::Release()
{
	_SUPER::Release();
	m_Attr.Clear();	
}

BOOL NPCImpl::Init()
{
	m_pMoveStateHandle->Init( this, MOVETYPE_WALK );

	_SUPER::Init();

	return TRUE;
}

VOID NPCImpl::SetBaseInfo( sNPCINFO_BASE* pNPCInfo ) 
{ 
	ASSERTV( pNPCInfo != NULL, "pNPCInfo == NULL" );

	m_pBaseInfo = pNPCInfo;

	/////////////////////////////////////////////////
	int iRecoverHP = 0;	
	int iRecoverMP = 0;

	if( CanRegenHPMP() )
	{
		iRecoverHP = theBattleFormular.CalcNPCHPRecover( m_pBaseInfo->m_dwMaxHP );
		iRecoverMP = theBattleFormular.CalcNPCMPRecover( m_pBaseInfo->m_dwMaxMP );
	}

	m_pSkillTimer->SetTimer( m_pBaseInfo->m_wSkillUpdateTime );

	m_Attr.SetCharInfo(m_pBaseInfo
                     ,iRecoverHP
							,iRecoverMP);
	m_Attr.Update();
}


BOOL NPCImpl::CanRegenHPMP()
{
	if( !m_pBaseInfo )
		return FALSE;

	switch(m_pBaseInfo->m_byGrade)
	{
	case NPC_GRADE_GENERAL:
	case NPC_GRADE_MAPOBJ:
		return FALSE;
	}

	return TRUE;
}

VOID NPCImpl::GetRenderInfo( sMONSTERINFO_RENDER * OUT pRenderInfo )
{
	pRenderInfo->m_dwObjectKey			= GetObjectKey();
	pRenderInfo->m_dwMonsterCode		= GetBaseInfo()->m_MonsterCode;
	pRenderInfo->m_dwNpcFuncCode		= 0;
	pRenderInfo->m_vPos					= GetPosition();
	pRenderInfo->m_dwHP					= GetHP();
	pRenderInfo->m_wMoveSpeedRatio	= m_Attr.GetMoveSpeedRatio();
	pRenderInfo->m_wAttSpeedRatio		= m_Attr.GetAttSpeedRatio();

	///////////////////////////////
	sSTATE_INFO *pStateInfo;
	
	pStateInfo					= (sSTATE_INFO*)( (BYTE*)pRenderInfo + sizeof(sMONSTERINFO_RENDER) );
	pRenderInfo->m_byCount	= GetStatusManager()->GetStatusInfos( pStateInfo );
}


VOID NPCImpl::Update( DWORD dwTick )
{
	_SUPER::Update( dwTick );

	if( !IsMoving() )
		return;

	/////////////////////////////////////////
	float		fMoveSpeed			= m_pMoveStateHandle->GetMoveSpeed();
	DWORD		dwPrevSectorIndex;
	DWORD		dwAfterSectorIndex;


	/////////////////////////////////////////
	dwPrevSectorIndex = GetSectorIndex();

	PathProgress( (float)dwTick * fMoveSpeed );

	Vector3D vCurPos		= GetPosition( );
	dwAfterSectorIndex	= GetField()->GetSectorIndex( &vCurPos );

	/////////////////////////////////////////
	if( dwPrevSectorIndex != dwAfterSectorIndex )
	{
		SetOldSectorIndex	( dwPrevSectorIndex );
		SetSectorIndex		( dwAfterSectorIndex );
		GetField()->UpdateSectorBy( this );
	}
}


VOID NPCImpl::SelectBestTarget()
{
	BattleRecord *	pBattleRecord	= GetBattleRecord();
	Character *		pNewTargetChar = pBattleRecord->SelectBestTarget( this );

	////////////////////////////////////
	if(!pNewTargetChar)
		return;

	if(pNewTargetChar == m_pTargetChar )
		return;

	////////////////////////////////////
	if(IsFriend( pNewTargetChar ) )
		return;

	////////////////////////////////////
	if( util::DrawLots(m_pBaseInfo->m_byChangeTargetRatio ))
	{
		SetTargetChar( pNewTargetChar );
	}
}


Character* NPCImpl::SelectSkillTarget( SLOTCODE skillCode, BOOL bHeal )
{
	SkillDetailInfo *pSkillInfo = theSkillInfoParser.GetSkillInfo( skillCode );
	if( !pSkillInfo )
		return NULL;

	Field *pField = GetField();
	if( !pField )
		return NULL;

	// 技能目标范围类型
	switch( pSkillInfo->m_byTarget )
	{
	case SKILL_TARGET_ENEMY:
	case SKILL_TARGET_REACHABLE_ENEMY:
		return m_pTargetChar;

	case SKILL_TARGET_FRIEND:	
	case SKILL_TARGET_REACHABLE_FRIEND:
		{
		if( bHeal )	
			return pField->SearchTarget(this
                                    ,eTARGET_SEARCH_LOW_HPRATIO
												,SKILL_TARGET_FRIEND);
		return pField->SearchTarget(this
                                 ,eTARGET_SEARCH_LOW_HPRATIO
											,SKILL_TARGET_FRIEND);
		}break;

	case SKILL_TARGET_ME:
	case SKILL_TARGET_REACHABLE_ME:
		return this;

	case SKILL_TARGET_FRIEND_CORPSE:
		return pField->SearchTarget(this
                                 ,eTARGET_SEARCH_CORPSE
											,SKILL_TARGET_FRIEND);

	case SKILL_TARGET_SUMMON:
		return m_pTargetChar;

	default :
		ASSERT( ! "[NPCImpl::SelectSkillTarget] Invalid TargetType!" );
		LOGMSG( LOG_FULL,  "[NPCImpl::SelectSkillTarget] Invalid TargetType[%d] ", pSkillInfo->m_byTarget );
	}
	return m_pTargetChar;
}


VOID NPCImpl::ChangeAIState(DWORD  dwStateID
                          ,LPARAM param1
								  ,LPARAM param2
								  ,LPARAM param3)
{
	m_pAIStateMan->ChangeAIState(dwStateID
                                    ,param1
												,param2
												,param3);
}

BOOL NPCImpl::ProcessAIInfo(eAI_INFOTYPE type)
{
	__BOOL_SUPER(ProcessAIInfo(type));

	////////////////////////////////////////
	DWORD*	pAttitudes	= NULL;
	DWORD*	pMoves		= NULL;

	if(!theAIInfoParser.GetInfo(m_pBaseInfo->m_AIInfoCode
                              ,type
										,m_pBaseInfo->m_wAttitude
										,m_pBaseInfo->m_wMoveAttitude
										,pAttitudes
										,pMoves	)		)
		return FALSE;

	////////////////////////////////////////
	INT		nIndex		= math::Random((INT)MAX_AI_STATE);
	INT		nIndex2		= math::Random((INT)MAX_AI_STATE);

	if(pAttitudes[nIndex] == 0 && pMoves[nIndex2] == 0)
		return FALSE;

	////////////////////////////////////////
	MSG_CG_SYNC_NPC_AIINFO_BRD	msgAI;
	msgAI.dwObjectKey	= GetObjectKey();
	msgAI.byIndex		= (BYTE)nIndex;
	msgAI.byIndex2		= (BYTE)nIndex2;
	msgAI.byAIInfoType= (BYTE)type;

	SendAround(&msgAI,msgAI.GetSize());

	return TRUE;
}


VOID NPCImpl::InitAIStates( DWORD dwStateID, LPARAM param1 )
{
	m_pAIStateMan->Release();

	// 静止型
	if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_ETERNAL_STOP )
	{
		static DWORD	arStates[AISTATE_MAX]=
		{
			 AISTATE_UNKNOWN

			,AISTATE_STOP_IDLE //,AISTATE_WANDER
			,AISTATE_STOP_IDLE //,AISTATE_IDLE
			,AISTATE_STOP_IDLE //,AISTATE_TRACK
			,AISTATE_ATTACK
			,AISTATE_STOP_IDLE//,AISTATE_HELP
			
			,0//,AISTATE_THRUST
			,AISTATE_DEAD
			,0//,AISTATE_FLYING
			,AISTATE_KNOCKDOWN
			,0//,AISTATE_JUMP
			
			,0//,AISTATE_FALL_APART
			,0//,AISTATE_RETURN
			,0//,AISTATE_RETREAT
			,0//,AISTATE_RUNAWAY
			,AISTATE_CHAOS

			,0,0,0,0,0,0
		};
		LinkAIStates(arStates);
	}

	//归家型
	else if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_BACK_SPAWN )
	{
		static DWORD	arStates[AISTATE_MAX]=
		{
			 AISTATE_UNKNOWN

			,AISTATE_RETURN		//,AISTATE_WANDER
			,AISTATE_SPAWN_IDLE	//,AISTATE_IDLE
			,AISTATE_TRACK
			,AISTATE_ATTACK
			,AISTATE_HELP
			
			,AISTATE_THRUST
			,AISTATE_DEAD
			,AISTATE_FLYING
			,AISTATE_KNOCKDOWN
			,AISTATE_JUMP
			
			,AISTATE_FALL_APART
			,AISTATE_RETURN
			,AISTATE_RETREAT
			,AISTATE_RUNAWAY
			,AISTATE_CHAOS

			,0,0,0,0,0,0
		};
		LinkAIStates(arStates);
	}
	// 巡逻型
	else if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_PATROL )
	{
		static DWORD	arStates[AISTATE_MAX]=
		{
			 AISTATE_UNKNOWN

			,AISTATE_PATROL	//,AISTATE_WANDER
			,AISTATE_PATROL	//,AISTATE_IDLE
			,AISTATE_TRACK
			,AISTATE_ATTACK
			,AISTATE_HELP
			
			,AISTATE_THRUST
			,AISTATE_DEAD
			,AISTATE_FLYING
			,AISTATE_KNOCKDOWN
			,AISTATE_JUMP
			
			,AISTATE_FALL_APART
			,AISTATE_RETURN
			,AISTATE_RETREAT
			,AISTATE_RUNAWAY
			,AISTATE_CHAOS

			,0,0,0,0,0,0
		};
		LinkAIStates(arStates);
	}
	// 否则，默认处理
	else
	{
		_SUPER::InitAIStates(dwStateID, param1);
	}

	m_pAIStateMan->Init();// dwStateID, param1 );

}


VOID NPCImpl::AINotice(AIMSG* pMsg
                      ,WORD    wSize
							 ,DWORD   dwDelay)
{
	m_pAIMsgQueue->AINotice( pMsg, wSize, dwDelay );
}


VOID NPCImpl::AINoticeAround(AIMSG* pMsg
                            ,WORD    wSize
									 ,DWORD   dwDelay)
{
	Field *pField = GetField();
	if( !pField )
		return;

	pField->AINoticeAround(this
                         ,pMsg
								 ,wSize
								 ,dwDelay);
}


BOOL NPCImpl::HasEnemy() const
{
	return m_pTargetChar ? TRUE : FALSE;
	//return !m_pBattleRecord.IsEmpty();
}

VOID NPCImpl::RecordAttackPoint( Character* pAttacker, int attackPoint )
{
	if( !pAttacker )		
		return;

	if(GetObjectKey() == pAttacker->GetObjectKey())
		return;

	if(pAttacker->IsMapNpc())
		return;

	if( IsFriend( pAttacker ) )	
		return;

	BattleRecordInfo* pRecord;
	
	////////////////////////////////////////////////
	pRecord = m_pBattleRecord->Get( pAttacker->GetObjectKey() );
	if( pRecord == NULL )
	{
		pRecord = m_pBattleRecord->Add( pAttacker, m_pBattleRecord->IsEmpty() );
	}

	pRecord->IncAttackPoint( attackPoint );
}


VOID NPCImpl::RecordTotalPoint( Character* pAttacker, int TotalkPoint )
{
	if( !pAttacker )		
		return;

	if(GetObjectKey() == pAttacker->GetObjectKey())
		return;

	if(pAttacker->IsMapNpc())
		return;

	if( IsFriend( pAttacker ) )
		return;

	BattleRecordInfo* pRecord;
	
	pRecord = m_pBattleRecord->Get( pAttacker->GetObjectKey() );
	if (pRecord == NULL)
	{
		pRecord = m_pBattleRecord->Add( pAttacker, m_pBattleRecord->IsEmpty() );
	}

	pRecord->IncTotalPoint(TotalkPoint);
}




// 搜索攻击目标
Character* NPCImpl::SearchTarget()
{
	Field *pField = GetField();
	if( !pField )
		return NULL;

	eTARGET_SEARCH_TYPE eSearchType = eTARGET_SEARCH_NEAREST;

	switch( m_pBaseInfo->m_wAttitude )
	{
	case ATTACK_ATTITUDE_PASSIVE:
		return NULL;

	case ATTACK_ATTITUDE_ONE_TARGET:
		if( m_pTargetChar )	
			return NULL;

	case ATTACK_ATTITUDE_LOW_HP_FIRST:
		eSearchType = eTARGET_SEARCH_LOW_HP;
		break;

	case ATTACK_ATTITUDE_LOW_LEVEL_FIRST:
		eSearchType = eTARGET_SEARCH_LOW_LEVEL;
		break;

	case ATTACK_ATTITUDE_HIGH_MP_FIRST:
		eSearchType = eTARGET_SEARCH_HIGH_MP;
		break;
	}

	return pField->SearchTarget(this
                              ,eSearchType
										,SKILL_TARGET_ENEMY);
}




VOID NPCImpl::RemoveEnemy( DWORD leaveEnemyKey )
{	
	if (	GetTargetChar() != NULL
		&& GetTargetChar()->GetObjectKey() == leaveEnemyKey )
	{
		SetTargetChar( NULL );
		SetTrackSlot( -1 );
	}

	m_pBattleRecord->Remove( leaveEnemyKey );
}

VOID NPCImpl::SetTargetChar( Character *pTarget, BOOL bProcessObserver )
{
	if (m_pTargetChar == pTarget)
		return;

	if (m_pTargetChar != NULL)
	{

		if(bProcessObserver)
		{
			Character* pValidChar = dynamic_cast<Character*>(m_pTargetChar);
			if(pValidChar)
				pValidChar->RemoveObserver( GetObjectKey() );
		}
	}

	m_pTargetChar = pTarget;

	///////////////////////////////////////////
	if (m_pTargetChar != NULL)
	{
		if(bProcessObserver)
			m_pTargetChar->AddObserver( this );
		RecordAttackPoint( pTarget, 0 );
	}
}


/////////////////////////////////////////////
VOID NPCImpl::OnEnterField(Field* pField, Vector3D* pvPos)
{
	LOGINFO	("NPCImpl::OnEnterField %s at (%2.1f,%2.1f,%2.1f) Tile(%d,%d)\n"
				,(LPCTSTR)m_pBaseInfo->m_NpcName
				,pvPos->x,pvPos->y,pvPos->z
				,(INT)(pvPos->x/TILE_3DSIZE),(INT)(pvPos->y/TILE_3DSIZE));

	Init();

	_SUPER::OnEnterField(pField, pvPos);
	ASSERTV	( GetField() != NULL
				, "GetField() == NULL" );

	BOOL bAdded = pField->AddNPC( this );
	ASSERTV( bAdded, "bAdded" );


	DWORD dwStateID = AISTATE_IDLE;
	if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_SEARCH_AREA )
	{
		dwStateID	= AISTATE_TRACK_AREA;
	}
	m_pAIStateMan->OnEnterField(pField,dwStateID );

}

VOID NPCImpl::OnLeaveField()
{
	LOGINFO("NPCImpl::OnLeaveField %s\n",(LPCTSTR)m_pBaseInfo->m_NpcName);

	ASSERTV( GetField() != NULL, "GetField() == NULL" ); 
	m_pAIStateMan->OnLeaveField(GetField() );

	LeaveGroup();

	_SUPER::OnLeaveField();

	Release();
}


BOOL NPCImpl::IsSameTargetPos()
{
	Vector3D vTargetPos	= m_pTargetChar->GetPosition();
	Vector3D vPos			= GetPosition( );

	return( vPos == vTargetPos );
}

VOID NPCImpl::AllocTrackPos( Vector3D &vDestPos )
{
	Vector3D vPos;	

	m_pTargetChar->FreeEnemySlot( m_iTrackSlot );

	vPos = GetPosition();
	m_iTrackSlot = m_pTargetChar->AllocEnemySlot(&vPos
                                               ,vDestPos
															  ,GetAttackRange() * 0.9f);
}

VOID NPCImpl::GroupCmdBeginAttack( Character* pTargetChar )
{
	if(! IsLeaderOfGroup() )
		return;

	AIMSG_GROUP_COMMAND aiMsg;
	aiMsg.byType		= GROUP_CMD_ATTACK;
	aiMsg.dwTargetKey = pTargetChar->GetObjectKey();

	AINoticeGroup(&aiMsg
                ,sizeof(AIMSG_GROUP_COMMAND))		;
}

VOID NPCImpl::GroupCmdStopAttack()
{
	if(! IsLeaderOfGroup() )
		return;

	AIMSG_GROUP_COMMAND aiMsg;
	aiMsg.byType = GROUP_CMD_STOP_ATTACK;
	AINoticeGroup(&aiMsg
                ,sizeof(AIMSG_GROUP_COMMAND))		;
}


eSEND_RESULT NPCImpl::SendAround(MSG_OBJECT_BASE * pMsg
                                ,WORD              wSize
										  ,BOOL              bSendToMe)
{
	return Character::SendAround(pMsg
                               ,wSize
										 ,bSendToMe);
}


BOOL NPCImpl::OnResurrection(float fRecoverExp
                            ,float fRecoverHP
									 ,float fRecoverMP)
{
	__BOOL_SUPER( OnResurrection(fRecoverExp
                                ,fRecoverHP
										  ,fRecoverMP));

	ChangeAIState( AISTATE_IDLE );
	return TRUE;
}


BOOL NPCImpl::OnDead()
{
	ASSERTV( GetHP() == 0, "GetHP() != 0" );

	ChangeAIState( AISTATE_DEAD );

	SetTargetChar( NULL );
	m_pAIMsgQueue->Clear();

	return _SUPER::OnDead();
}




BOOL NPCImpl::IsNearTargetPos()
{
	__CHECK(m_pTargetChar != this);

	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();

	Vector3D		vCurPos		= GetPosition();
	Vector3D		vTargetPos	= m_pTargetChar->GetPosition();

	float fDistToTarget2 = (vCurPos-vTargetPos).Length2();

	return ( fDistToTarget2 < aiParam.m_FallapartMinAttackRange * aiParam.m_FallapartMinAttackRange );
}

BOOL NPCImpl::CanFallApartTarget()
{
	Vector3D vTargetPos;
	AllocTrackPos( vTargetPos );

	return GetField()->FindPath( this, &vTargetPos );
}


BOOL NPCImpl::CanRetreatFromTarget()
{
	__CHECK(GetSelectedSkill() == SKILLCODE_NORMAL_ATTACK);

	__CHECK_PTR(m_pTargetChar);


	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();
	__CHECK(GetAttackRange() >= aiParam.m_RetreatDistanceMin);

	float fDistToTarget = GetDistToTarget();
	float fMinAttackDist = GetAttackRange() * aiParam.m_RetreatDistanceLimitRatio;

	return (fDistToTarget < fMinAttackDist);
}

////////////////////////////////////////////////
BOOL NPCImpl::IsPositionCanJump()
{
	ASSERTV( GetTargetChar(), "GetTargetChar() == NULL" );

	Vector3D		vCurPos		= GetPosition();
	Vector3D		vTargetPos	= m_pTargetChar->GetPosition();

	////////////////////////////////////////////////
	float fHeight = vCurPos.z - vTargetPos.z;
	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();

	__CHECK(fHeight >= aiParam.m_JumpHeightMin);
	__CHECK(fHeight <= aiParam.m_JumpHeightMax);


	////////////////////////////////////////////////
	vCurPos.z		= 0;
	vTargetPos.z	= 0;

	float fDistance = (vTargetPos - vCurPos).Length();

	__CHECK(fDistance >= aiParam.m_JumpLengthMin);
	__CHECK(fDistance <= aiParam.m_JumpLengthMax);

	return TRUE;
}

BOOL NPCImpl::JoinGroup( ObjectGroup* pGroup )
{
	ASSERT( pGroup != NULL );
	ASSERT( m_pGroup == NULL );

	m_pGroup = pGroup;

	return TRUE;
}

BOOL NPCImpl::LeaveGroup()
{
	__CHECK_PTR(m_pGroup);


	BOOL bRet;
	
	bRet = m_pGroup->RemoveObject( GetObjectKey(), GetBaseInfo()->m_MonsterCode );

	if( m_pGroup->GetObjectAmount() == 0 )
	{
		Field *pField = GetField();
		if( pField )
		{
			pField->RemoveGroup( m_pGroup );
		}
	}

	m_pGroup = NULL;

	return bRet;
}

BOOL NPCImpl::IsMemberOfGroup()
{
	return m_pGroup != NULL;
}

BOOL NPCImpl::IsLeaderOfGroup()
{
	__CHECK(IsMemberOfGroup());

	return ( m_pGroup->GetLeaderObjKey() == GetObjectKey() );
}


BOOL NPCImpl::IsFollowerOfGroup()
{
	__CHECK(IsMemberOfGroup());

	return ( m_pGroup->GetLeaderObjKey() != GetObjectKey() );
}


BOOL NPCImpl::IsLeaderAlive()
{
	__CHECK_PTR(GetField());
	__CHECK_PTR(m_pGroup);

	__CHECK( m_pGroup->GetLeaderObjKey() );

	return TRUE;
}


BOOL NPCImpl::MoveBRD( Vector3D *pvDestPos, eMOVE_TYPE moveState )
{
	Field *pField = GetField();

	__CHECK_PTR(pField);

	///////////////////////////////////
	__CHECK(pField->FindPath( this, pvDestPos ) ) ;

	SetMoveState( moveState );

	//LOGINFO	("%s Move (%2.1f,%2.1f,%2.1f) -> (%2.1f,%2.1f,%2.1f)\n"
	//			,(LPCTSTR)m_pBaseInfo->m_NpcName
	//			,vPos.x,vPos.y,vPos.z
	//			,pvDestPos->x,pvDestPos->y,pvDestPos->z);

	MSG_CG_SYNC_MOVE_BRD sendMsg;
	sendMsg.m_byCategory			= CG_SYNC;
	sendMsg.m_byProtocol			= CG_SYNC_MOVE_BRD;
	sendMsg.m_dwObjectKey		= GetObjectKey();
	sendMsg.m_byState				= (BYTE)moveState;	// RUN or WALK
	sendMsg.vCurPos				= GetPosition();
	sendMsg.vDestPos				= *pvDestPos;

	SendAround( &sendMsg, sizeof(sendMsg) );

	return TRUE;
}



float NPCImpl::GetAttackRange() const
{
	if( GetSelectedSkill() == INVALID_SKILLCODE )
		return 2.5f;

	////////////////////////////////////////
	if( GetSelectedSkill() == SKILLCODE_NORMAL_ATTACK )
		return m_pBaseInfo->m_fAttRange;

	////////////////////////////////////////
	SkillDetailInfo *pBaseSkillInfo;
	
	pBaseSkillInfo = theSkillInfoParser.GetSkillInfo( GetSelectedSkill() );
	if(NULL == pBaseSkillInfo)
		return 2.5f;

	return pBaseSkillInfo->m_wSkillRange/10 + GetSkillRangeBonus();
}

eATTACK_KIND NPCImpl::GetAttackKind(eATTACK_KIND /*defaultKind*/) const
{
	return (eATTACK_KIND)m_pBaseInfo->m_bySeries;
}

BOOL NPCImpl::IsFriend( CharacterCookie *pTargetRef )
{
	Character *pTarget = (Character *)pTargetRef;

	__CHECK_PTR(pTarget);

	if(pTarget->IsMapNpc())
		return TRUE;

	/////////////////////////////////////////
	if(GetObjectKey() == pTarget->GetObjectKey())
		return TRUE;

	/////////////////////////////////////////
	Character *				pSummoner		= NULL; 
	Character *				pTargetSummoner= NULL;
	SummonedRelation *	pRelation		= theSummonManager.FindRelation( m_dwSummonerKey );
	SummonedRelation *	pRelation2		= theSummonManager.FindRelation( pTarget->GetSummonerKey() );

	/////////////////////////////////////////
	if( pRelation )			
	{
		pSummoner = pRelation->GetSummoner();
		if( pSummoner == this )	
			pSummoner = NULL;
	}

	/////////////////////////////////////////
	if( pRelation2 )		
	{
		pTargetSummoner = pRelation2->GetSummoner();
		if( pTargetSummoner == pTarget )
			pTargetSummoner = NULL;
	}


	///////////////////////////////////////////////
	if( pSummoner && pTargetSummoner )
		return pSummoner->IsFriend( pTargetSummoner );

	///////////////////////////////////////////////
	else if( pSummoner )
		return pSummoner->IsFriend( pTarget );

	///////////////////////////////////////////////
	else if( pTargetSummoner )
		return IsFriend( pTargetSummoner );

	///////////////////////////////////////////////
	return ( pTarget->IsNpc() );
}

VOID NPCImpl::OnAttack( Character *pTarget, CODETYPE skillcode, DAMAGETYPE damage )
{
	_SUPER::OnAttack( pTarget, skillcode, damage );

	RecordAttackPoint( pTarget, 0 );
}

DAMAGETYPE NPCImpl::OnDamaged( Character *pAttacker, eATTACK_KIND attackType, DAMAGETYPE damage )
{
	if( pAttacker )
	{
		RecordAttackPoint( pAttacker, damage );
	}
	return _SUPER::OnDamaged( pAttacker, attackType, damage );
}



BOOL NPCImpl::IsOutOfWanderRange() const
{
	float fDistToRegenPos = GetDist2( m_RegenPos );

	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();

	return ( fDistToRegenPos	>aiParam.m_NpcWanderRange
										*aiParam.m_NpcWanderRange );
}




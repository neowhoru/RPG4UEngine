#include "stdafx.h"
#include "MoveStateHandleImpl.h"
#include "NumericValueInfo.h"
#include "Player.h"



VOID PlayerMoveStateHandle::SetMoveState( eMOVE_TYPE eMoveState )
{
	_SUPER::SetMoveState(eMoveState);
	m_fBaseMoveSpeed = GetBaseMoveSpeedAsState( m_MoveState );
}

VOID NpcMoveStateHandle::SetMoveState( eMOVE_TYPE eMoveState )
{
	if(!m_pOwner->IsNpc())
		return;

	_SUPER::SetMoveState(eMoveState);

	NPC *pNPC = (NPC*)m_pOwner;

	switch( m_MoveState )
	{
	case MOVETYPE_WALK:
		m_fBaseMoveSpeed = pNPC->GetBaseInfo()->m_fWalkSpeed * SCALE_SPEED_FACTOR;
		//LOGINFO("Monster Walk Speed : %f\n",m_fBaseMoveSpeed);
		break;
	case MOVETYPE_RUN:
		m_fBaseMoveSpeed = pNPC->GetBaseInfo()->m_fRunSpeed * SCALE_SPEED_FACTOR;
		//LOGINFO("Monster Walk Speed : %f\n",m_fBaseMoveSpeed);
		break;
	default:
		m_fBaseMoveSpeed = GetBaseMoveSpeedAsState( m_MoveState );
	}
}

float PlayerMoveStateHandle::GetMoveSpeed()
{
	float fMoveSpeed = _SUPER::GetMoveSpeed();
	if(!m_pOwner->IsPlayer())
		return fMoveSpeed;

	Player *pPlayer = (Player*)m_pOwner;

	float fAddMoveSpeedRatio = pPlayer->GetAddMoveSpeedRatio();
	if( fAddMoveSpeedRatio )
		fMoveSpeed *= fAddMoveSpeedRatio;

	return fMoveSpeed;
}




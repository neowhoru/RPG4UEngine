#include "stdafx.h"
#include "SummonedRelation.h"
#include "Summoned.h"


SummonedRelation::SummonedRelation() 
: m_pSummoner(NULL)
, m_dwSummonerKey(0)
{
	m_NpcAmounts.clear();

	m_pSummoneds = new SummonedNpcTable;
	m_pSummoneds->Initialize( MAX_SUMMONED_NUM );
}

SummonedRelation::~SummonedRelation()
{
	Release();
	SAFE_DELETE( m_pSummoneds );
}

VOID SummonedRelation::Init(Character*      pSummoner
                           ,eSUMMON_COMMAND eCommand)
{
	m_pSummoner			= pSummoner;
	m_dwSummonerKey	= pSummoner->GetObjectKey();
	m_CurrentCommand	= eCommand;
}

VOID SummonedRelation::Release()
{
	RemoveAll( );

	if( m_pSummoner )
		m_pSummoner->SetSummonerKey( 0 );

	m_pSummoner			= NULL;
	m_dwSummonerKey	= 0;

	m_NpcAmounts.clear();
	m_pSummoneds->clear();
}

DWORD SummonedRelation::GetAliveAmount( DWORD dwNPCCode )
{
	NpcAmountMapIt it = m_NpcAmounts.find( dwNPCCode );
	if(it != m_NpcAmounts.end())
		return it->second;
	return 0;
}

VOID SummonedRelation::IncreaseSummoned( DWORD dwNPCCode, NPC *pNPC )
{
	NpcAmountMapIt it;

	it = m_NpcAmounts.find( dwNPCCode );
	if ( it != m_NpcAmounts.end() )
	{
		DWORD&	dwValue = it->second;
		dwValue++;
	}
	else
	{
		m_NpcAmounts.insert( NpcAmountMapPair(dwNPCCode, 1) ); 

		m_pSummoneds->Add( pNPC, pNPC->GetObjectKey() );
	}
}

VOID SummonedRelation::DecreaseSummoned( DWORD dwNPCCode, DWORD dwObjectKey )
{
	NpcAmountMapIt it = m_NpcAmounts.find( dwNPCCode );
	if ( it == m_NpcAmounts.end() )
		return;

	DWORD&	dwValue = it->second;
	if(dwValue > 1)
	{
		--dwValue;
	}
	else
	{
		m_NpcAmounts.erase(it); 
		m_pSummoneds->Remove( dwObjectKey );
	}
}


VOID SummonedRelation::Remove( DWORD dwObjectKey )
{
	///////////////////////////////////////////////
	if(!dwObjectKey )
		return;

	Summoned *pSummoned;
	
	pSummoned = (Summoned*)GetSummoned( dwObjectKey );
	if( !pSummoned )
		return;

	Field *pField = pSummoned->GetField();
	if( !pField )
		return;

	pField->PushNpcDeadList( pSummoned );
}


VOID SummonedRelation::RemoveAll( )
{
	///////////////////////////////////////////////
	NPC *						pNPC = NULL;
	SummonedNpcTableIt	it;

	for(it = m_pSummoneds->begin(); it != m_pSummoneds->end(); ++it )
	{
		pNPC = *it;
		if( !pNPC->IsSummoned() )	
			continue;

		Summoned *pSummoned = (Summoned*)pNPC;

		if( !pSummoned->GetField() )	
			continue;

		pSummoned->GetField()->PushNpcDeadList( pSummoned );
	}
}


eSUMMON_RESULT SummonedRelation::RunCommand(eSUMMON_COMMAND eCommand
                                           ,DWORD           dwObjectKey
														 ,DWORD           dwTargetKey)
{
	//////////////////////////////////////
	if( m_CurrentCommand == eCommand )
		return RC::RC_SUMMON_SAME_COMMAND;

	//////////////////////////////////////
	switch(eCommand)
	{
	case SUMMON_COMMAND_DESTROY:
		{
			Remove( dwObjectKey );
			return RC_SUMMON_SUCCESS;
		}break;

	case SUMMON_COMMAND_FOLLOW				:
	case SUMMON_COMMAND_DELEGATE_ATTACK	:
	case SUMMON_COMMAND_DELEGATE_DEFENSE:
		{
			m_CurrentCommand = eCommand;
		}break;
	}


	////////////////////////////////////////////
	NPC *						pNPC = NULL;
	SummonedNpcTableIt	it;

	for( it = m_pSummoneds->begin(); it != m_pSummoneds->end(); ++it )
	{
		pNPC = *it;

		if( dwObjectKey && dwObjectKey != pNPC->GetObjectKey() )	
			continue;

		if( !pNPC->IsSummoned() )	
			continue;

		Summoned *	pSummoned = (Summoned*)pNPC;
		pSummoned->RunCommand( eCommand, dwTargetKey );
	}

	return RC_SUMMON_SUCCESS;
}













/*////////////////////////////////////////////////////////////////////////
文 件 名：GMCommandManager.cpp
创建日期：2009年4月8日
最后更新：2009年4月8日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "GMCommandManager.h"
#include "Field.h"
#include "Player.h"
#include "PlayerManager.h"
#include "ServerSession.h"
#include "DropManager.h"
#include "GameShell.h"
#include "ServerUtil.h"
#include "Map.h"
#include "Item.h"
#include "CommonDefine.h"
#include "ObjectSystem.h"
#include <ItemInfoParser.h>
#include "BaseZone.h"
#include "ZoneManager.h"
#include "BaseRoom.h"
#include "StatusManager.h"
#include "ratiomanager.h"
#include "ItemManager.h"
#include <ItemRank.h>
#include <ServerShell.h>
#include "ScriptWord.h"
#include "TargetFinder.h"

#define	MAX_RANK_OPTION	49
#define	MAX_ITEM_GRADE		1	
using namespace RC;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define GM_CMD_NAK()			MSG_CG_GM_STRING_CMD_NAK msgNak
#define GM_CMD_CHECK(x,c)	if(x)\
									{\
										msgNak.m_byErrorCode	=  c;\
										goto laFailed;\
									}


#define GM_CMD_FAILED()\
									return TRUE;\
							laFailed:\
									pPlayer->SendPacket( &msgNak, sizeof(msgNak) );\
									return FALSE


#define GM_CMD_COUNT(N)		GM_CMD_CHECK(arCmds.GetWordCount() != N, RC_GM_INVALID_ARGUMENT)
#define GM_CMD_PARAM(x)		GM_CMD_CHECK(x, RC_GM_INVALID_ARGUMENT)


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GMCommandManager, ()  , gamemain::eInstPrioGameFunc);


GMCommandManager::GMCommandManager(void)
{
	m_dwPlayerKey	=	0;


}

GMCommandManager::~GMCommandManager(void)
{

}


BOOL GMCommandManager::ParseCommand( DWORD dwPlayerKeyFrom, LPCTSTR szCmd )
{
	__CHECK(GetGMPlayer(dwPlayerKeyFrom));

	m_dwPlayerKey	= dwPlayerKeyFrom;

	return theServerShell.ProcessConsole(szCmd);
}




Player*	 GMCommandManager::GetGMPlayer( DWORD dwPlayerKeyFrom )
{
	Player * pPlayer = thePlayerManager.FindPlayerUser(dwPlayerKeyFrom  );

	if( !pPlayer )
		return NULL;

	if( !pPlayer->IsGMPlayer() )
	{
		MSG_CG_GM_STRING_CMD_NAK  msgNak;
		msgNak.m_byErrorCode =  RC_GM_ISNOT_GM;	
		pPlayer->SendPacket( &msgNak, sizeof(msgNak) );
		return NULL;
	}

	return pPlayer;
}


//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessEntrance( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	GM_CMD_NAK();
	GM_CMD_COUNT(1);

	//////////////////////////////////////////////////////
	BOOL bTransparency = pPlayer->ExistStatus(CHARSTATE_ETC_OBSERVER);

	BOOL bSuccess = FALSE;
	if (!bTransparency)
	{
		pPlayer->GetStatusManager()->AllocStatus( CHARSTATE_ETC_OBSERVER );
		bSuccess = TRUE;
	}

	//////////////////////////////////////////////////////
	GM_CMD_CHECK(!bSuccess, RC_GM_ISNOT_GM);

	{
		//////////////////////////////////////////////////////
		MSG_CG_GM_OBSERVER_BRD BrdMsg;
		BrdMsg.m_dwObjectKey		= pPlayer->GetObjectKey();
		BrdMsg.m_byObserverOn	= 1;		
		pPlayer->SendAround( &BrdMsg, sizeof(BrdMsg) );
	}


	//////////////////////////////////////////////////////
	BOOL bUndead = pPlayer->ExistStatus(CHARSTATE_ETC_UNDEAD);

	bSuccess = FALSE;
	if (!bUndead)
	{
		pPlayer->GetStatusManager()->AllocStatus( CHARSTATE_ETC_UNDEAD );
		bSuccess = TRUE;
	}

	//////////////////////////////////////////////////////
	GM_CMD_CHECK(!bSuccess, 0);

	{
		//////////////////////////////////////////////////////
		MSG_CG_GM_UNDEAD_MODE_ACK AckMsg;
		AckMsg.m_bUndeadOn = TRUE;
		pPlayer->SendPacket( &AckMsg, sizeof(AckMsg) );
	}


	//////////////////////////////////////////////////////
	float fRatio = 2.5f;
	pPlayer->SetAddMoveSpeedRatio(fRatio);

	//////////////////////////////////////////////////////
	{
	MSG_CG_GM_SET_SPEED_BRD brdMsg;
	brdMsg.m_fSpeedRatio = fRatio;
	brdMsg.m_byMoveLevel = 3;
	brdMsg.m_dwObjectKey = pPlayer->GetObjectKey();
	pPlayer->SendAround( &brdMsg, sizeof(brdMsg) );
	}

	GM_CMD_FAILED();
}


//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessSpeed( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	GM_CMD_NAK();

	//////////////////////////////////////////////////////
	GM_CMD_COUNT(2);

	int nLevel = arCmds.GetNumber(1 );

	//////////////////////////////////////////////////////
	GM_CMD_CHECK(nLevel > 5 || nLevel < 0, RC_GM_INVALID_ARGUMENT);


	//////////////////////////////////////////////////////
	float fRatio = 1.0f + 0.5f * nLevel;

	pPlayer->SetAddMoveSpeedRatio( fRatio );

	//////////////////////////////////////////////////////
	{
	MSG_CG_GM_SET_SPEED_BRD brdMsg;
	brdMsg.m_dwObjectKey = pPlayer->GetObjectKey();
	brdMsg.m_byMoveLevel = nLevel;
	brdMsg.m_fSpeedRatio = fRatio;
	pPlayer->SendAround( &brdMsg, sizeof(brdMsg) );
	}

	GM_CMD_FAILED();
}

	//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessWhisper( ScriptWord& arCmds )
{
	return TRUE;
}

//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessTransparent( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	//////////////////////////////////////////////////////
	BOOL bObserver		= pPlayer->ExistStatus(CHARSTATE_ETC_OBSERVER);
	BOOL bSuccess		= FALSE;
	BOOL bSet = FALSE;

	bSet	= (arCmds.GetNumber(1) != 0);


	//////////////////////////////////////////////////////
	// ON --> OFF
	if (bObserver)
	{
		if ( !bSet )
		{
			pPlayer->GetStatusManager()->Remove( CHARSTATE_ETC_OBSERVER );
			bSuccess = TRUE;
		}		
	}
	//////////////////////////////////////////////////////
	// OFF --> ON
	else
	{
		if ( bSet )
		{
			pPlayer->GetStatusManager()->AllocStatus( CHARSTATE_ETC_OBSERVER );
			bSuccess = TRUE;
		}	
	}

	//////////////////////////////////////////////////////
	GM_CMD_CHECK(!bSuccess, RC_GM_ISNOT_GM);


	//////////////////////////////////////////////////////
	{
	MSG_CG_GM_OBSERVER_BRD BrdMsg;
	BrdMsg.m_dwObjectKey		= pPlayer->GetObjectKey();
	BrdMsg.m_byObserverOn	= bSet;
	pPlayer->SendAround( &BrdMsg, sizeof(BrdMsg) );
	}

	GM_CMD_FAILED();
}


	//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessInvincibility( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	GM_CMD_NAK();

	//////////////////////////////////////////////////////
	BOOL								bUndead;
	BOOL 								bSuccess		= FALSE;
	bool 								bSet			= FALSE;
	

	bUndead = pPlayer->ExistStatus(CHARSTATE_ETC_UNDEAD);

	GM_CMD_COUNT(2);


	bSet	= (arCmds.GetNumber(1) != 0);

	//////////////////////////////////////////////////////
	if( bUndead )
	{
		if ( !bSet )
		{
			pPlayer->GetStatusManager()->Remove( CHARSTATE_ETC_UNDEAD );
			bSuccess = TRUE;
		}		
	}
	//////////////////////////////////////////////////////
	else
	{
		if( bSet )
		{
			pPlayer->GetStatusManager()->AllocStatus( CHARSTATE_ETC_UNDEAD );
			bSuccess = TRUE;
		}	
	}

	//////////////////////////////////////////////////////
	GM_CMD_CHECK(! bSuccess, 0);

	{
	MSG_CG_GM_UNDEAD_MODE_ACK AckMsg;
	AckMsg.m_bUndeadOn = bSet;
	pPlayer->SendPacket( &AckMsg, sizeof(AckMsg) );
	}

	GM_CMD_FAILED();
}

	//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessAccount( ScriptWord& /*arCmds*/ )
{
	return TRUE;
}

//////////////////////////////////////////////////////
class GMKillMonsterAroundOpr
{
public:
	Vector3D		m_vOriginPos;
	float			m_fRange;
	Player *		m_pAttacker;

	//////////////////////////////////////////////////////
	void operator() ( NPC * pNPC)
	{
		Field *pField = pNPC->GetField();
		if( !pField )
			return;

		Vector3D		vNPCPos = pNPC->GetPosition();
		float			fDist;

		fDist = (vNPCPos - m_vOriginPos).Length2();

		if( fDist > m_fRange * m_fRange )
			return;

		//////////////////////////////////////////////////////
		DAMAGETYPE wDamage = (DAMAGETYPE)pNPC->GetMaxHP();

		//pNPC->OnDamaged( m_pAttacker, ATTACKKIND_MELEE, wDamage );
		pField->KillNPC( m_pAttacker, pNPC, TRUE );

		//////////////////////////////////////////////////////
		MSG_CG_BATTLE_PLAYER_ATTACK_BRD msgSend;
		msgSend.byEffect			= 0;
		msgSend.dwClientSerial	= 0;
		msgSend.vCurPos			= m_vOriginPos;
		msgSend.vDestPos			= vNPCPos;
		msgSend.dwAttackerKey	= m_pAttacker->GetObjectKey();
		msgSend.dwTargetKey		= pNPC->GetObjectKey();
		msgSend.wDamage			= wDamage;
		msgSend.dwTargetHP		= pNPC->GetHP();
		msgSend.byAttackType		= ATTACK_SEQUENCE_FIRST;
		m_pAttacker->SendAround(&msgSend, sizeof(msgSend) );
	}
};

//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessRemovalMonster( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();

	GM_CMD_COUNT(1);

	//////////////////////////////////////////////////////
	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	{
	Vector3D vPlayerPos = pPlayer->GetPosition();

	//////////////////////////////////////////////////////
	GMKillMonsterAroundOpr	killOpr = { vPlayerPos, 15.f, pPlayer };
	pField->ForEachNpcAround( pPlayer->GetSectorIndex(), killOpr );
	}

	GM_CMD_FAILED();
}


//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessRemoval( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	//////////////////////////////////////////////////////
	DWORD dwMonsterKey = arCmds.GetNumber(1 );

	//////////////////////////////////////////////////////
	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	//////////////////////////////////////////////////////
	NPC* pMonster = (NPC*)pField->FindCharacter( dwMonsterKey );
	GM_CMD_CHECK(!pMonster, RC_GM_NOTEXIST_USER);

	//////////////////////////////////////////////////////
	pField->KillNPC(pPlayer, pMonster, FALSE);

	GM_CMD_FAILED();
}


//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessKill( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );

	__CHECK_PTR(pPlayer);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	//////////////////////////////////////////////////////
	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	//////////////////////////////////////////////////////
	DWORD dwMonsterKey = arCmds.GetNumber(1 );
	NPC* pMonster = (NPC*)pField->FindCharacter( dwMonsterKey );

	GM_CMD_CHECK(!pMonster, RC_GM_NOTEXIST_USER);

	//////////////////////////////////////////////////////
	pField->KillNPC(pPlayer, pMonster, TRUE);

	GM_CMD_FAILED();
}

	//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessCreateItem( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	GM_CMD_NAK();

	//////////////////////////////////////////////////////
	GM_CMD_COUNT(3);

	int nItemCode	= arCmds.GetNumber(1 );
	int nItemCount = arCmds.GetNumber(2 );

	//////////////////////////////////////////////////////
	GM_CMD_CHECK(nItemCount > GM_MAX_CREATE_ITEM_NUM || nItemCount <= 0, RC_GM_INVALID_ARGUMENT);


	//////////////////////////////////////////////////////
	GM_CMD_CHECK(!theItemInfoParser.GetItemInfo( nItemCode ), RC_GM_INVALID_ARGUMENT);

	//////////////////////////////////////////////////////
	for( int i = 0; i < nItemCount; ++i )
	{
		Vector3D vDropPos = pPlayer->GetRandPosAround(2);
		ItemSlot slot;
		slot.SetCode( nItemCode );
		theDropManager.DropItemAt(pField
                                    ,&vDropPos
												,NULL
												,0
												,slot);
	}


	GM_CMD_FAILED();
}

//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessCreateMoney( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	int nMoney = arCmds.GetNumber(1 );

	GM_CMD_CHECK(nMoney > GM_MAX_CREATE_MONEY || nMoney <= 0, RC_GM_INVALID_ARGUMENT);

	//////////////////////////////////////////////////////
	{
	Vector3D vDropPos = pPlayer->GetRandPosAround(2);

	MONEY money = (MONEY)nMoney;
	theDropManager.DropMoneyAt(pField
                                  ,&vDropPos
											 ,NULL
											 ,0
											 ,money);
	}


	GM_CMD_FAILED();
}


//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessLevelUp( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	//////////////////////////////////////////////////////
	int nPlusLevel = arCmds.GetNumber(1 );
	int nLevel		= pPlayer->GetLevel() + nPlusLevel;

	//////////////////////////////////////////////////////
	GM_CMD_PARAM(nLevel > MAX_LEVEL || nLevel < 0);

	pPlayer->LevelUp( nPlusLevel );

	GM_CMD_FAILED();
}


//////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessDisConnect( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	//////////////////////////////////////////////////////
	GM_CMD_NAK();

	GM_CMD_COUNT(2);

	//////////////////////////////////////////////////////
	Field* pField = pPlayer->GetField();
	GM_CMD_PARAM(!pField);

	//////////////////////////////////////////////////////
	DWORD dwUserKey = arCmds.GetNumber(1 );

	//////////////////////////////////////////////////////
	Player* pUser = (Player*)pField->FindCharacter( dwUserKey );
	GM_CMD_CHECK(!pUser, RC_GM_NOTEXIST_USER);

	//////////////////////////////////////////////////////
	pUser->ForceDisconnect(DISCONNECT_GM_FORCE_DISCONNECT);

	return TRUE;
	GM_CMD_FAILED();
}


/////////////////////////////////////////////////////
BOOL GMCommandManager::ProcessRebirth( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	GM_CMD_NAK();
	GM_CMD_COUNT(1);

	////////////////////////////////////////////////
	GM_CMD_CHECK(!pPlayer->OnResurrection( 0, 1.0f, 1.0f ), RC_GM_CANNOT_RESURRECTION);

	{
		Vector3D curPos;
		pPlayer->GetPos(curPos);

		MSG_CG_GM_RESURRECTION_BRD msgSend;
		msgSend.m_dwObjectKey	= pPlayer->GetObjectKey();
		msgSend.m_dwHP			= pPlayer->GetHP();
		msgSend.m_dwMP			= pPlayer->GetMP();
		msgSend.m_vCurPos		= curPos;

		pPlayer->SendAround( &msgSend, sizeof(msgSend) );
	}

	GM_CMD_FAILED();
}

////////////////////////////////////////////////
BOOL GMCommandManager::ProcessRecovery( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(1);

	DWORD dwMaxHP = pPlayer->GetMaxHP();
	DWORD dwMaxMP = pPlayer->GetMaxMP();

	GM_CMD_CHECK( !pPlayer->OnLifeRecover( dwMaxHP, dwMaxMP ),  RC_GM_CANNOT_RESURRECTION);

	GM_CMD_FAILED();
		 
}

////////////////////////////////////////////////
BOOL GMCommandManager::ProcessCreateMonster( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(3);


	Field* pField = pPlayer->GetField();

	GM_CMD_PARAM(NULL == pField );

	////////////////////////////////////////////////
	int nMonsterCode	= arCmds.GetNumber(1 );
	int nMonsterCount = arCmds.GetNumber(2 );

	GM_CMD_PARAM(nMonsterCount <= 0 );

	////////////////////////////////////////////////
	{
	Vector3D vPlayerPos;
	pPlayer->GetPos(vPlayerPos );	

	////////////////////////////////////////////////
	int iTileIdx = pPlayer->GetPathHandler()->GetTile();
	GM_CMD_PARAM(-1 == iTileIdx);


	////////////////////////////////////////////////
	vPlayerPos = pField->GetFieldInfo()->GetTileLand()->GetRandomPosInTile(iTileIdx);

	GM_CMD_PARAM( !pField->CreateNPC(MONSTER_OBJECT
                                   ,nMonsterCode
											  ,nMonsterCount
											  ,&vPlayerPos));
	}

	GM_CMD_FAILED();
}


//////////////////////////////////////////////
class GMKillItemAroundOpr
{
public:
	Vector3D	m_vOriginPos;
	float		m_fRange;

	void operator() ( Item * pItem )
	{
		Vector3D vItemPos;
		pItem->GetPos(vItemPos );
		float fDist = (vItemPos - m_vOriginPos).Length( );

		if( fDist <= m_fRange )
		{
			pItem->DestroyFromField();
		}
	}
};



////////////////////////////////////////////////
BOOL GMCommandManager::ProcessRemovalItem( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(1);

	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	{
	Vector3D vPlayerPos;
	pPlayer->GetPos(vPlayerPos );

	////////////////////////////////////////////////
	GMKillItemAroundOpr DestroyItemOpr={ vPlayerPos, 15.f };
	pField->ForEachItemAround( pPlayer->GetSectorIndex(), DestroyItemOpr );
	}

	GM_CMD_FAILED();
}

////////////////////////////////////////////////
BOOL GMCommandManager::ProcessStatPointUp( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	int nPlusPoint = arCmds.GetNumber(1 );	
	pPlayer->StatPointUp( nPlusPoint );

	GM_CMD_FAILED();
		 
}

////////////////////////////////////////////////
BOOL GMCommandManager::ProcessSkillPointUp( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);

	int nPlusPoint = arCmds.GetNumber(1 );	

	pPlayer->SkillPointUp( nPlusPoint );

	return TRUE;
	GM_CMD_FAILED();
}


////////////////////////////////////////////////
BOOL GMCommandManager::ProcessCharInfo( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);



	////////////////////////////////////////////////
	Player* pFindChar = NULL;
	pFindChar = thePlayerManager.FindPlayer( arCmds[1] );

	GM_CMD_PARAM(!pFindChar);

	////////////////////////////////////////////////
	{
	MSG_CG_GM_CHARINFO_ACK msgSend;
	msgSend.m_eCharType	= pFindChar->GetCharType();
	msgSend.m_wLevel		= pFindChar->GetLevel();
	msgSend.m_Money		= pFindChar->GetMoney();

	pFindChar->GetPos( msgSend.m_vCurPos );

	if( pFindChar->GetField() && pFindChar->GetField()->GetMap() )
	{
		BaseZone *pZone = pFindChar->GetField()->GetMap()->GetGameZone();
		msgSend.m_RoomKey = pZone->GetKey();
		msgSend.m_MapCode = pFindChar->GetField()->GetMap()->GetMapCode();
	}

	pPlayer->SendPacket( &msgSend, sizeof(msgSend) );
	}

	GM_CMD_FAILED();
}

/////////////////////////////////////////////////////////////
class GMGetPlayerInfoOpr
{
public:
	sCHARINFO_GM*	m_pCharInfo;
	BYTE				m_byPlayerNum;

public:
	BYTE			GetPlayerNum()	{ return m_byPlayerNum; }

	VOID operator () ( Player *pPlayer )
	{
		if( m_byPlayerNum >= MSG_CG_GM_ROOMINFO_ACK::_MAX_PLAYER_INFO_SIZE )
			return;

		lstrcpyn( m_pCharInfo[m_byPlayerNum].m_szCharName, pPlayer->GetCharName(), MAX_CHARNAME_LENGTH );
		m_pCharInfo[m_byPlayerNum].m_byClass = pPlayer->GetCharType();
		m_pCharInfo[m_byPlayerNum].m_LV = pPlayer->GetLevel();
		pPlayer->GetPos(m_pCharInfo[m_byPlayerNum].m_vCurPos );

		m_byPlayerNum++;
	}
};

////////////////////////////////////////////////
BOOL GMCommandManager::ProcessRoomInfo( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(2);



	BaseZone *pZone = NULL;
	if( arCmds.GetWordCount() > 1 )
	{
		DWORD dwRoomKey = arCmds.GetNumber(1 );
		pZone = theZoneManager.GetZone( dwRoomKey );
	}
	else if( pPlayer->GetField() 
			&& pPlayer->GetField()->GetMap() )
	{
		pZone = pPlayer->GetField()->GetMap()->GetGameZone();
	}
	else
	{
		GM_CMD_PARAM(1);
	}

	////////////////////////////////////////
	GM_CMD_PARAM(!pZone);

	////////////////////////////////////////
	{
	MSG_CG_GM_ROOMINFO_ACK AckMsg;

	memset( AckMsg.m_szMasterName, 0, sizeof(TCHAR) *MAX_CHARNAME_LENGTH );
	if( pZone->GetZoneType() != ZONETYPE_VILLAGE )
	{
		BaseRoom *pRoom = (BaseRoom*)pZone;
		Player * pMaster = thePlayerManager.FindPlayerUser( pRoom->GetMasterUserKey() );
		if( pMaster )
		{
			strncpy( AckMsg.m_szMasterName, pMaster->GetCharName(), MAX_CHARNAME_LENGTH );
		}
	}

	////////////////////////////////////////
	GMGetPlayerInfoOpr GetInfoOpr={ AckMsg.m_pPlayerInfo ,0};
	pZone->ForEachPlayer( GetInfoOpr );
	AckMsg.m_byCount = GetInfoOpr.GetPlayerNum();

	pPlayer->SendPacket( &AckMsg, sizeof(AckMsg) );
	}

	GM_CMD_FAILED();
}

////////////////////////////////////////////////
BOOL GMCommandManager::ProcessCreateEnchantItem( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();
	GM_CMD_COUNT(4);


	////////////////////////////////////////////////
	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	int nItemCode = arCmds.GetNumber(1 );
	int nItemCount = arCmds.GetNumber(2 );
	int nItemLevel = arCmds.GetNumber(3 );

	////////////////////////////////////////////////
	GM_CMD_PARAM(nItemCount > GM_MAX_CREATE_ITEM_NUM || nItemCount <= 0);

	////////////////////////////////////////////////
	GM_CMD_PARAM( !theItemInfoParser.GetItemInfo( nItemCode ) );

	////////////////////////////////////////////////
	GM_CMD_PARAM( nItemLevel < 0 || nItemLevel > MAX_ENCHANT_GRADE );

	////////////////////////////////////////////////
	for( int i = 0; i < nItemCount; ++i )
	{
		Vector3D vDropPos = pPlayer->GetRandPosAround(2);
		ItemSlot slot;
		slot.SetSerial( TEMP_SERIALTYPE_VALUE );
		slot.SetCode( nItemCode );
		slot.SetEnchant( nItemLevel);
		theDropManager.DropItemAt( pField, &vDropPos, NULL, 0, slot );
	}

	GM_CMD_FAILED();
}


BOOL GMCommandManager::ProcessCreateItemEx( ScriptWord& arCmds )
{
	ItemSlot itemSlot;
	itemSlot.SetSerial( TEMP_SERIALTYPE_VALUE );

	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	////////////////////////////////////////////////
	GM_CMD_NAK();

	////////////////////////////////////////////////
	int nArgSize = (int)arCmds.GetWordCount();
	GM_CMD_PARAM( nArgSize < 3 || arCmds.GetWordCount() > 15);

	////////////////////////////////////////////////
	Field* pField = pPlayer->GetField();
	__CHECK_PTR(pField);

	////////////////////////////////////////////////
	int nItemCode = arCmds.GetNumber(1 );		
	sITEMINFO_BASE* pItemInfo = theItemInfoParser.GetItemInfo( nItemCode );
	GM_CMD_PARAM( !pItemInfo );

	////////////////////////////////////////////////
	itemSlot.SetCode( nItemCode );

	int nItemCount = arCmds.GetNumber(2 );
	GM_CMD_PARAM( nItemCount <= 0 || nItemCount > GM_MAX_CREATE_ITEM_NUM );


	////////////////////////////////////////////////
	if( nArgSize > 3 )
	{
		int nItemGrade = arCmds.GetNumber(3 );
		GM_CMD_PARAM( nItemGrade < 0 || nItemGrade > MAX_ITEM_GRADE );

		itemSlot.SetSet( nItemGrade );
	}
	
	////////////////////////////////////////////////
	int nEnchantLevel = 0;
	if( nArgSize > 4 )		
	{
		nEnchantLevel = arCmds.GetNumber(4 );

		GM_CMD_PARAM( nEnchantLevel < 0 || nEnchantLevel > MAX_ENCHANT_GRADE );

		itemSlot.SetEnchant( nEnchantLevel );
	}

	////////////////////////////////////////////////
	int	nRank = 0;
	itemSlot.SetRank( (eRANK)nRank );


	////////////////////////////////////////////////
	if( nArgSize > 5 )
	{
		nRank = arCmds.GetNumber(5 );

		GM_CMD_PARAM( nRank < 0 || nRank > MAX_RANK_OPTION );

		for( int i = 1; i <= nRank; i++ )
		{
			//OptionIndex Check
			int nOptionIndex = 0;
			GM_CMD_PARAM( nArgSize <= 5 + i);

			nOptionIndex =  arCmds.GetNumber(5+i);

			GM_CMD_PARAM( !GetRankItem( &itemSlot, nOptionIndex ) );
		}
	}

	//////////////////////////////////////////////////////
	for( int i = 0; i < nItemCount; ++i )
	{
		Vector3D vDropPos = pPlayer->GetRandPosAround(2);
		theDropManager.DropItemAt( pField, &vDropPos, NULL, 0, itemSlot );
	}

	GM_CMD_FAILED();
}


////////////////////////////////////////////////
BOOL	GMCommandManager::GetRankItem( ItemSlot* pItemSlot, int nOptionIndex )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	BYTE curRank = pItemSlot->GetRank();

	////////////////////////////////////////////////
	GM_CMD_NAK();

	GM_CMD_PARAM( curRank == RANK_0 )

	eRANK_OPTION_ITEM_TYPE nOptionType;
	nOptionType = ItemRank::GetRankOptionItemType	( pItemSlot->GetItemInfo()->m_wType 
																	, pItemSlot->GetItemInfo()->m_byMaterialType);

	sRANK_OPTION* pRankOption = theRankOptionParser.GetRankOption( nOptionType, nOptionIndex );

	//////////////////////////////////////////////
	GM_CMD_PARAM( !pRankOption );

	GM_CMD_PARAM( pRankOption->m_iOptionIndex == 0 );

	GM_CMD_PARAM( pRankOption->m_OptionNCode == 0 );

	GM_CMD_PARAM( pRankOption->m_Value[curRank+1] == 0 );

	//////////////////////////////////////////////
	pItemSlot->SetRank( (eRANK)(curRank + 1) );
	pItemSlot->SetRankAttr( (eRANK)(curRank + 1), pRankOption,0, TRUE );

	ItemManager::CreateItemSocket( pItemSlot, curRank );

	GM_CMD_FAILED();
}


//--------------------------------------------------------------------------------------------------------------
class GMTargetFinder : public TargetFinder
{
public :	
	virtual VOID	SetOwner		( Character* pOwner )
	{
		m_pOwner = pOwner;
		pOwner->GetPos(m_vOwnerPos );
		m_fOwnerSight = MATH_INFINITE;
	}
	virtual BOOL	operator () (Object* pObject)
	{
		Character* pTarget = dynamic_cast<Character*>(pObject);
		m_pTarget = pTarget;
		return FALSE;
	}
};

BOOL GMCommandManager::ProcessMoveToNearMonster( ScriptWord& arCmds )
{
	Player* pPlayer = GetGMPlayer( m_dwPlayerKey );
	__CHECK_PTR(pPlayer);

	DWORD		dwFromID = arCmds.GetNumber(1);

	Character*	pCharFrom	(NULL);
	Field*		pField		;

	pField	= pPlayer->GetField();
	if(dwFromID)
	{
		pCharFrom = thePlayerManager.FindPlayerUser( dwFromID );
	}
	else
	{
		pCharFrom = thePlayerManager.FindPlayer( (LPCTSTR)arCmds[1] );
	}
	if(pCharFrom == NULL)
		pCharFrom = (Character*)pPlayer;

	//////////////////////////////////////
	if(pCharFrom)
	{
		GMTargetFinder finder;
		finder.SetOwner( pCharFrom );
		finder.SetTargetType( SKILL_TARGET_ENEMY );
		if(pField->SearchNPC(finder))
		{
			pCharFrom->SetPosition(finder.GetTarget()->GetPosition() );
			pCharFrom->StopMoving();
		}
	}


	return TRUE;
}

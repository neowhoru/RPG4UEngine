
#include "StdAfx.h"
#include <Macro.h>
#include "MonsterManager.h"
#include "Monster.h"
#include "ObjectList.h"


MonsterManager::MonsterManager(VOID)
{
	m_pObjectList = new ObjectList;
}

MonsterManager::~MonsterManager(VOID)
{
	if(m_pObjectList) delete m_pObjectList;
}

BOOL MonsterManager::AddMonster(Object *pObject)
{
	return m_pObjectList->Add(pObject);
}

BOOL MonsterManager::RemoveMonster(Object *pObject)
{
	return m_pObjectList->Remove(pObject);
}

Object* MonsterManager::RemoveMonster(const DWORD dwObjKey)
{
	return m_pObjectList->Remove( dwObjKey );
}

Object* MonsterManager::FindMonster(const DWORD dwObjKey)
{
	return m_pObjectList->Find( dwObjKey );
}

VOID MonsterManager::RemoveAllMonster()
{
	m_pObjectList->RemoveAll();
}

VOID MonsterManager::Update(DWORD dwElapsedTime)
{
	m_pObjectList->Update(dwElapsedTime);
}


/*////////////////////////////////////////////////////////////////////////
文 件 名：PlayerImpl.cpp
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PlayerImpl.h"
#include <iostream>
#include <assert.h>
#include <PacketStruct_Gate_Game.h>
#include "Map.h"
#include "Field.h"
#include "PlayerManager.h"
#include "ItemManager.h"
#include "ServerSession.h"
#include "NPC.h"
#include "SkillInfoParser.h"
#include "FormularManager.h"
#include "MathHelper.h"
#include "GameShell.h"
#include "MissionManager.h"
#include "QuestManager.h"
#include "ITradeContainer.h"
#include "TradeManager.h"
#include "VendorManager.h"
#include "SummonManager.h"
#include "SummonedRelation.h"
#include "SlotManager.h"
#include "SkillManager.h"
#include "QuickManager.h"
#include "QuickStyleManager.h"
#include "IMovementChecker.h"
#include "BaseZone.h"
#include <GMInfoParser.h>
#include "ServerInfoParser.h"
#include "TimeHelper.h"
#include "StatusManager.h"
#include "PVPResult.h"
#include "MoveStateHandle.h"
#include "TimeHelper.h"
#include "AIParamParser.h"
#include "IEventInventory.h"
#include "PacketStruct_Server.h"
#include "PacketStruct_Common.h"
#include "FormularManager.h"
#include "IScriptCharacter.h"
#include "IQuestCharacter.h"
#include "GlobalManager.h"
#include "PacketCryptManager.h"
#include "PacketStruct_ClientGameS_Event.h"
#include "SlotSystem.h"
#include "ServerOptionParser.h"
#include "PlayerParameter.h"
#include "DataPumpSystem.h"
#include "ScriptManager.h"

using namespace std;

PlayerImpl::PlayerImpl()
{
	m_pMoveStateHandle	= &m_MoveStateHandle;
	m_pMovementChecker	= &m_MoveChecker;
	m_Attr.SetOwner( this );

}

PlayerImpl::~PlayerImpl()
{
}


BOOL PlayerImpl::Init()
{
	m_pMoveStateHandle->Init( this, MOVETYPE_RUN );

	///////////////////////////////
	__BOOL_SUPER (Init());

	m_pSlotManager				= (SlotManager			*)theDataPumpSystem.AllocDataPump(DATAPUMP_SLOT			);
	m_pSkillManager			= (SkillManager		*)theDataPumpSystem.AllocDataPump(DATAPUMP_SKILL);
	m_pMissionManager			= (MissionManager		*)theDataPumpSystem.AllocDataPump(DATAPUMP_MISSION		);
	m_pStyleManager			= (QuickStyleManager	*)theDataPumpSystem.AllocDataPump(DATAPUMP_QUICKSTYLE	);
	m_pItemManager				= (ItemManager			*)theDataPumpSystem.AllocDataPump(DATAPUMP_ITEM			);
	m_pQuickManager			= (QuickManager		*)theDataPumpSystem.AllocDataPump(DATAPUMP_QUICK		);


	m_pSlotManager	->Init(this,m_pSlotManager );
	m_pItemManager	->Init(this,m_pSlotManager );
	m_pSkillManager->Init(this,m_pSlotManager );
	m_pQuickManager->Init(this,m_pSlotManager );
	m_pStyleManager->Init(this,m_pSlotManager );

	///////////////////////////////
	if(!m_pEventSlotContainer)
	{
		m_pEventSlotContainer = (IEventInventory*)theSlotSystem.CreateBaseContainer(SLOTT_EVENT);
		assert(m_pEventSlotContainer);
	}

	m_pEventSlotContainer->ClearAll();	
	m_pEventSlotContainer->SetPlayer(this);

	return TRUE;
}

VOID PlayerImpl::Release()
{
	if(!m_pSlotManager)
		return;

	Player *				pPlayer;

	switch(GetBehave())
	{
	///////////////////////////////////////////
	case PLAYER_BEHAVE_TRADE:
	{
		pPlayer = GetTradeContainer()->GetCounterPlayer();
		theTradeManager.DestroyTradeContainer( this );

		MSG_CG_TRADE_CANCEL_CMD cmsg;
		pPlayer->SendPacket( &cmsg, sizeof(cmsg) );
	}break;

	///////////////////////////////////////////
	case PLAYER_BEHAVE_VENDOR_ESTABLISHER:
	{
		eVENDOR_RESULT result = theVendorManager.EndVendor( this );
		ASSERT( result == RC_VENDOR_SUCCESS );

		MSG_CG_VENDOR_END_BRD bmsg;
		bmsg.m_dwPlayerKey = GetObjectKey();
		SendAround( &bmsg, sizeof(bmsg), FALSE );
	}break;

	///////////////////////////////////////////
	case PLAYER_BEHAVE_VENDOR_OBSERVER:
	{
		eVENDOR_RESULT result = theVendorManager.EndViewVendor( this );
		ASSERT( result == RC_VENDOR_SUCCESS );
	}break;
	}

	SetBehave(PLAYER_BEHAVE_IDLE);

	///////////////////////////////////////////
	m_pSlotManager->Release();
	m_pQuickManager->Release();
	m_pStyleManager->Release();
	m_pItemManager->Release();
	m_pSkillManager->Release();

	SAFE_RELEASEDATAPUMP( m_pSlotManager		);
	SAFE_RELEASEDATAPUMP( m_pItemManager		);
	SAFE_RELEASEDATAPUMP( m_pMissionManager	);
	SAFE_RELEASEDATAPUMP( m_pQuickManager		);
	SAFE_RELEASEDATAPUMP( m_pStyleManager		);
	SAFE_RELEASEDATAPUMP(m_pSkillManager);

	///////////////////////////////////////////
	if(m_pEventSlotContainer)
	{
		m_pEventSlotContainer->ClearAll();
		theSlotSystem.DestroyBaseContainer(m_pEventSlotContainer);
		m_pEventSlotContainer = NULL;
	}

	///////////////////////////////////////////
	//CharacterCookie::FreeCoolTimer( TRUE );
	//PlayerCookie::FreeItemCoolTime( TRUE );

	////PlayerCookie::Destroy();

	_SUPER::Release();

	m_Attr.Clear();
}

VOID PlayerImpl::Create	(ServerSession *  pServerSession
								 ,KEYTYPE         userKey
								 ,LPCTSTR		   szUserID)
{
	ASSERT( pServerSession );
	ASSERT( pServerSession->GetServerType() == GATE_SHELL );

	SetServerSession( pServerSession );
	SetUserKey( userKey );
	SetState	( PLAYER_CREATED );
	SetUserID( szUserID );

}

VOID PlayerImpl::Destroy()
{
	SetServerSession( NULL );
	SetUserKey( 0 );
	SetState( PLAYER_DESTROYED );
}

VOID PlayerImpl::Update( DWORD dwTick )
{
	_SUPER::Update( dwTick );

	if(m_pScriptChar)
		m_pScriptChar->FrameMove(dwTick);

	/////////////////////////////////
	if( m_DBUpdateTimer.IsExpired() )
	{
		thePlayerParameter.SetPlayer(this);
		thePlayerParameter.UpdateToDBProxy();
	}


	/////////////////////////////////
	if( m_PlayingTimer.IsExpired() )
	{
		GetCharInfo()->m_PlayLimitedTime += m_PlayingTimer.GetIntervalTime() / 60000;

		SYSTEMTIME sysTime;	
		GetLocalTime( &sysTime );
		if(	(sysTime.wHour == 23 && sysTime.wMinute == 59) 
			|| (sysTime.wHour == 0 && sysTime.wMinute == 0) )
		{
			GetCharInfo()->m_PlayLimitedTime = 0;
		}
	}

	/////////////////////////////////
	if( IsDoingAction() && IsActionExpired() )
	{
		SetActionDelay( 0 );
	}


	if( m_ShapShotLogTimer.IsExpired() )
		GAMELOG.LogSnapShot( this, theGameShell.GetServerGUID() );
}


void PlayerImpl::OnSetItemLog(BOOL bSet)
{
	//theItemLogManager.SetEnableLog(bSet);
}

BOOL PlayerImpl::RunScript(DWORD	dwData,DWORD npcID,BOOL bRunNow)
{
	//IScriptCharacter*	pChar;
	KeepQuestScript(SCRIPTCHARACTER_CHAR,QUESTCHARACTER_PLAYER);

	if(m_pScriptChar)
	{
		if(!bRunNow)
		{
			sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();
			m_pScriptChar->SetScriptTimer(aiParam.m_wScriptRunPeriod );
		}
		return m_pScriptChar->RunScript(dwData,npcID,bRunNow);
	}
	return FALSE;
}


BOOL PlayerImpl::RunQuestScript(CODETYPE questID,DWORD	dwData,BOOL bRunNow)//,DWORD npcID)
{
	IScriptCharacter*	pChar;
	Character*			pDestChar;

	KeepQuestScript(SCRIPTCHARACTER_CHAR,QUESTCHARACTER_PLAYER);

	pChar = GetScriptChar();
	__CHECK_PTR(pChar);

	///////////////////////////////////////////
	pDestChar	= (Character*)theGlobalManager.FindMapNPC( pChar->GetTargetNpcID() );
	__CHECK(pDestChar && pDestChar->IsMapNpc());

	///////////////////////////////////////////
	if(m_pScriptChar)
	{
		if(!bRunNow)
		{
			sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();
			m_pScriptChar->SetScriptTimer(aiParam.m_wScriptRunPeriod );
		}

		return pChar->RunQuestScript(questID
											 ,dwData
											 ,pDestChar->GetQuestChar()
											 ,bRunNow);
	}
	return FALSE;
}


DWORD	PlayerImpl::GetQuestState( CODETYPE	questID )
{
	if(m_pQuestChar)
		return m_pQuestChar->GetQuestState (questID);
	return 0;
}

void	PlayerImpl::SetQuestState		( CODETYPE	questID, DWORD state )
{
	if(m_pQuestChar)
		m_pQuestChar->SetQuestState( questID, state);
}
void	PlayerImpl::DiscardQuest		( CODETYPE	questID )
{
	if(m_pQuestChar)
		m_pQuestChar->DiscardQuest( questID);
}
BOOL	PlayerImpl::IsQuestActive( CODETYPE	questID )
{
	if(m_pQuestChar)
		return m_pQuestChar->IsQuestActive (questID);
	return FALSE;
}

BOOL	PlayerImpl::IsQuestDone( CODETYPE	questID )
{
	if(m_pQuestChar)
		return m_pQuestChar->IsQuestDone (questID);
	return FALSE;
}

BOOL	PlayerImpl::CanGetNewQuest		()
{
	if(m_pQuestChar)
		return m_pQuestChar->CanGetNewQuest ();
	return FALSE;
}

BOOL	PlayerImpl::IsNeedDropTaskItem	(CODETYPE		questID
											,DWORD			taskState
											,SLOTCODE		itemCode)
{
	if(m_pQuestChar)
		return m_pQuestChar->IsNeedDropTaskItem (questID
															 ,taskState
															 ,itemCode);
	return FALSE;
}

void PlayerImpl::ChangeQuestData	(CODETYPE questID
                              ,DWORD    state
										,DWORD	 dwAction)
{
	MSG_CG_EVENT_QUESTDATA_CHANGE_CMD	msg;
	msg.m_QuestID	= questID;
	msg.m_Data		= state;
	msg.m_Action	= dwAction;
	SendPacket(&msg,sizeof(msg));
}

BOOL PlayerImpl::KeepQuestScript(DWORD dwScriptType,DWORD dwQuestType)
{
	__BOOL_SUPER(KeepQuestScript(dwScriptType,dwQuestType));
	if(m_pScriptChar)
		m_pScriptChar->SetListener(this);
	return TRUE;
}



//-------------------------------------------------------------------------------------------------
BOOL PlayerImpl::SendPacket(MSG_OBJECT_BASE * pMsg
                           ,WORD              wSize
									,BOOL              bCrypt)
{
	return _SUPER::SendPacket(  pMsg,  wSize, bCrypt );
}

BOOL PlayerImpl::SendPackets(DWORD  dwNumberOfMessages
                            ,BYTE** pMsg
									 ,WORD*  pwSize)
{
	MSG_OBJECT_BASE *pSendMsg = (MSG_OBJECT_BASE*)pMsg[0];

	pSendMsg->m_dwKey = GetUserKey();

	return m_pServerSession->Send(dwNumberOfMessages
                                  ,pMsg
											 ,pwSize);
}


BOOL PlayerImpl::SendToGuildServer(MSG_OBJECT_BASE * pMsg
                                  ,WORD              wSize)
{
	//ASSERT( g_pGameServer );
	pMsg->m_dwKey = GetUserKey();
	return theGameShell.SendToGuild( pMsg, wSize );
}

BOOL PlayerImpl::SendToDBPorxy( MSG_OBJECT_BASE * pMsg, WORD wSize )
{
	//ASSERT( g_pGameServer );
	pMsg->m_dwKey = GetUserKey();
	return theGameShell.SendToGameDBProxy( pMsg, wSize );
}

VOID PlayerImpl::ForceDisconnect( eDISCONNECT_REASON Reason )
{
	MSG_AG_CONNECTION_UNREGISTER_CMD msg;
	SendPacket( &msg, sizeof(msg) );
}


//-------------------------------------------------------------------------------------------------
VOID PlayerImpl::OnEnterField(Field* pField, Vector3D* pvPos)
{
	_SUPER::OnEnterField(pField, pvPos);

	SetState(PLAYER_MAP_ENTER);	

	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();

	switch(GetCharType())
	{
	case PLAYERTYPE_WARRIOR:
	case PLAYERTYPE_POWWOW:
		{
			m_pStatusManager->AllocStatus(CHARSTATE_ETC_AUTO_RECOVER_HP
												  ,BASE_EXPIRE_TIME_INFINITY
												  ,aiParam.m_PlayerHPRegenPeriod);

			m_pStatusManager->AllocStatus(CHARSTATE_ETC_AUTO_RECOVER_MP
												  ,BASE_EXPIRE_TIME_INFINITY
												  ,aiParam.m_PlayerMPRegenPeriod);
		}break;
	default:
		{
			m_pStatusManager->AllocStatus(CHARSTATE_ETC_AUTO_RECOVER_HPMP
                                      ,BASE_EXPIRE_TIME_INFINITY
												  ,aiParam.m_PlayerHPRegenPeriod);
		}break;
	}


	if(m_nEnterCounter == 0)
		theScriptManager.StartupVM(SCRIPTTYPE_PLAYERONLINE,0,m_pScriptChar);

	m_nEnterCounter++;
}

VOID PlayerImpl::OnLeaveField()
{
	GetField()->OnDisconnected(GetUserKey());

	_SUPER::OnLeaveField();

	m_pStatusManager->Release();
}


//-------------------------------------------------------------------------------------------------
BOOL PlayerImpl::OnDead()
{
	ASSERTV( GetHP() == 0, "[PlayerImpl::OnDead] GetHP() != 0" );

	__BOOL_SUPER(OnDead());

	StopMoving();

	/////////////////////////////////////////
	Vector3D vCurPos = GetPosition();

	MSG_CG_STATUS_DEAD_BRD sendMsg;
	sendMsg.m_dwObjectKey	= GetObjectKey();
	sendMsg.m_vCurPos			= vCurPos;
	sendMsg.m_dwExp			= GetExp();

	SendAround( &sendMsg, sizeof(sendMsg) );

	/////////////////////////////////////////
	theSummonManager.CancelSummonRelation( this );

	/////////////////////////////////////////
	CODETYPE dwMapCode = GetField()->GetMap()->GetMapCode();
	GAMELOG.LogPlayerDead(this
                          ,theGameShell.GetServerGUID()
								  ,GetExp()
								  ,dwMapCode
								  ,vCurPos);


	/////////////////////////////////////////
	GetGameZone()->OnPlayerDead(this
                              ,m_dwKillerObjectKey
										,m_KillerType);


	theScriptManager.StartupVM(SCRIPTTYPE_PLAYERDEAD, GetLevel(),m_pScriptChar);

	return TRUE;
}


VOID PlayerImpl::OnAttack(Character* pTarget
                         ,SLOTCODE   skillcode
								 ,DAMAGETYPE damage)
{
	_SUPER::OnAttack( pTarget, skillcode, damage );

	DWORD skillRecoverPercent	= 100;
	DWORD skillRequireMP			= 0;

	SkillDetailInfo* pSkillInfo;
	
	///////////////////////////////////////////
	pSkillInfo = theSkillInfoParser.GetSkillInfo( skillcode );
	if( pSkillInfo )
		skillRequireMP = pSkillInfo->m_wMPSpend;

	///////////////////////////////////////////
	DWORD recoverMP;
	
	recoverMP = theBattleFormular.CalcMPRecoverByAttack(GetCharType()
                                                      ,m_Attr.GetSPR()
																		,skillRecoverPercent
																		,skillRequireMP);
	OnLifeRecover( 0, recoverMP );
}


DAMAGETYPE PlayerImpl::OnDamaged(Character*   pAttacker
                                ,eATTACK_KIND attackType
										  ,DAMAGETYPE   damage)
{
	DWORD recoverMP;
	
	recoverMP = theBattleFormular.CalcMPRecoverByAttacked(GetCharType()
                                                        ,m_Attr.GetSPR()
																		  ,damage);

	OnLifeRecover( 0, recoverMP );

	return _SUPER::OnDamaged( pAttacker, attackType, damage );
}


//-------------------------------------------------------------------------------------------------
eITEM_RESULT PlayerImpl::PrepareUseItem( sITEMINFO_BASE * pItemInfo )
{
	if( GetField()==NULL )
		return RC_ITEM_INVALIDSTATEOFPLAYER;

	if( IsDead() )
		return RC_ITEM_INVALIDSTATEOFPLAYER;


	INT  nHPMPFulled = 0;

	//////////////////////////////////////////////////
	switch(pItemInfo->m_byWasteType)
	{
	case ITEMWASTE_HPPOTION:
	case ITEMWASTE_HPRATE_POTION:
		if(GetHP() >= GetMaxHP()) 
			return  RC_ITEM_PLAYER_HP_FULL;
		break;
	case ITEMWASTE_HPMPPOTION:
	case ITEMWASTE_HPMPRATE_POTION:
		if(GetHP() >= GetMaxHP()) nHPMPFulled++;
		break;
	}

	switch(pItemInfo->m_byWasteType)
	{
	case ITEMWASTE_MPPOTION:
	case ITEMWASTE_MPRATE_POTION:
		if(GetMP() >= GetMaxMP())
			return  RC_ITEM_PLAYER_MP_FULL;
		break;
	case ITEMWASTE_HPMPPOTION:
	case ITEMWASTE_HPMPRATE_POTION:
		if(GetMP() >= GetMaxMP())
			nHPMPFulled++;
		break;
	}

	if( nHPMPFulled > 1)
		return  RC_ITEM_PLAYER_HPMP_FULL;

	//if( GetHP()==GetMaxHP() )
	//	return RC_ITEM_INVALIDSTATEOFPLAYER;

	///////////////////////////////////////////
	util::Timer * pTimer = GetItemCoolTimer( pItemInfo->m_byWasteType );
	if( pTimer )
	{
		if( !pTimer->IsExpired( FALSE ) )
			return RC_ITEM_COOLTIME_ERROR;

		pTimer->Reset();
	}

	return RC_ITEM_SUCCESS;
}


VOID PlayerImpl::SetActionDelay( DWORD dwDelay )
{
	m_ActionTimer.SetTimer( dwDelay );

	if( dwDelay )
	{
		m_bDoingAction = TRUE;
	}
	else if( m_bDoingAction )
	{ 
		m_bDoingAction = FALSE;

		MSG_CG_SYNC_ACTION_EXPIRED_CMD msg;
		SendPacket( &msg, sizeof(msg) );
	}
}

DWORD PlayerImpl::GetActionDelay()
{
	return m_ActionTimer.GetIntervalTime();
}


BOOL PlayerImpl::IsActionExpired()
{
	return ( m_ActionTimer.IsExpired() );
}

VOID PlayerImpl::OnPunish()
{
	if( GetLevel() <= 5 )
		return;

	if(	ZONESTATE_ATPVP == GetGameZoneState() )
		return;

	/////////////////////////////////////////////////
	DWORD dwExpNextAccum = theFormularManager.GetExpAccumlate( GetLevel() + 1 );
	DWORD dwExpCurAccum	= theFormularManager.GetExpAccumlate( GetLevel() );
	DWORD dwExpResult		= dwExpCurAccum;

	float			fRatio	= 0;
	LEVELTYPE	level		= GetLevel();
	INT64			nDiff;

	/////////////////////////////////////////////////
	if(level > 10 && level <= 80)
		fRatio = 0.03f;
	else if(level >= 81  && level <= MAX_LEVEL)
		fRatio = 0.01f;
	else
		fRatio = 0;

	nDiff = (INT64)( ( dwExpNextAccum - dwExpCurAccum ) * fRatio );

	/////////////////////////////////////////////////
	if( nDiff < 0 )
	{
		LOGMSG	(LOG_CRITICAL
					,"[Player::OnPunish] nDiff[%I64d] dwExpCurAccum[%d] dwExpNextAccum[%d] CurExp[%d] "
					,nDiff
					,dwExpCurAccum
					,dwExpNextAccum
					,GetExp());
		return;
	}

	/////////////////////////////////////////////////
	if( dwExpCurAccum < (GetExp() - nDiff) )
	{
		dwExpResult = (DWORD)(GetExp() - nDiff);
		m_dwDeadExp = (DWORD)nDiff;
	}
	else
	{
		if( GetExp() > dwExpCurAccum )
			m_dwDeadExp = GetExp() - dwExpCurAccum;
		else		
			m_dwDeadExp = 0;
	}

	/////////////////////////////////
	GAMELOG.LogCutExp(this
                       ,theGameShell.GetServerGUID()
							  ,dwExpResult
							  ,m_dwDeadExp
							  ,level);
	SetExp( dwExpResult );
}


sPLAYERINFO_BASE* PlayerImpl::GetCharInfo()
{
	m_Attr.GetCharInfo( PlayerCookie::GetCharInfo() );
	return PlayerCookie::GetCharInfo();
}



BOOL PlayerImpl::IsFriend( CharacterCookie *pTargetRef )
{
	Character *pTarget = (Character *)pTargetRef;

	if( !pTarget )	
		return FALSE;

	if( GetObjectKey() == pTarget->GetObjectKey() )
		return TRUE;

	SummonedRelation *pRelation;
	
	//////////////////////////////////////////////
	pRelation = theSummonManager.FindRelation( pTarget->GetSummonerKey() );
	if( pRelation )
	{
		Character *pSummoner = pRelation->GetSummoner();
		if( pSummoner && pTarget != pSummoner )
			return IsFriend( pSummoner );
	}

	//////////////////////////////////////////////
	if( m_pPVPResult->GetPVPState() == PVPSTATE_MATCH )
	{
		if( m_PartyData.IsMember() )
		{
			if( m_PartyData.GetPartyKey() == pTarget->GetPartyData().GetPartyKey() )
				return TRUE;
		}

		return FALSE;
	}

	return  pTarget->IsPlayer();
}

BOOL PlayerImpl::IsGMPlayer()
{
	sSERVER_INFOC * pServerEnv = theServerInfoParser.GetServerInfo();
	if( pServerEnv->bEnableGMFile )
	{
		sGM_INFO *pGMInfo = theGMInfoParser.FindGM( GetUserID() );
		return ( pGMInfo != NULL );
	}
	return TRUE;
}

BOOL PlayerImpl::IsSuperPlayer()
{
	sGM_INFO *pGMInfo = theGMInfoParser.FindSuperUser( GetUserID() );
	return ( pGMInfo != NULL );
}

BOOL PlayerImpl::CanChat()
{
	if( PlayerCookie::GetCharInfo()->m_byCharState == DBCHAR_STATE_CHAT_BLOCK )
		return FALSE;
	return TRUE;
}

BOOL PlayerImpl::CanBehave()
{
	if( PlayerCookie::GetCharInfo()->m_byCharState == DBCHAR_STATE_BEHAVE_BLOCK )
		return FALSE;
	return TRUE;
}

VOID PlayerImpl::SetDBCharState( eDBCHAR_STATE stat, WORD /*wMinutes*/ )
{
	PlayerCookie::GetCharInfo()->m_byCharState = stat;
}


IPlayerWarehouse * PlayerImpl::GetWarehouseContainer()
{ 
	return (IPlayerWarehouse *)GetItemManager()->GetItemSlotContainer(SI_WAREHOUSE);	
}

BOOL PlayerImpl::OnResurrection(float fRecoverExp
                               ,float fRecoverHP
										 ,float fRecoverMP)
{
	__BOOL_SUPER(OnResurrection( fRecoverExp, fRecoverHP, fRecoverMP ));



	if( GetField() && GetField()->GetMap() )
	{
		Vector3D vPos		= GetPosition();
		CODETYPE dwMapCode= GetField()->GetMap()->GetMapCode();
		GAMELOG.LogPlayerResurrection	(this
                                    ,theGameShell.GetServerGUID()
												,GetExp()
												,dwMapCode
												,vPos);
	}
	return TRUE;
}


BOOL PlayerImpl::IsOverPlayingTime()
{
	WORD wHour = GetCharInfo()->m_PlayLimitedTime / 60;

	sAIPARAM_INFO& aiParam = theAIParamParser.GetInfo();
	if(	aiParam.m_PlayingOverTime 
		&& wHour >= aiParam.m_PlayingOverTime )
		return TRUE;

	return FALSE;
}














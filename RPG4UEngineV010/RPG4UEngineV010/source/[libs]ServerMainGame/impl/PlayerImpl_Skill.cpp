/*////////////////////////////////////////////////////////////////////////
文 件 名：PlayerImpl_Skill.cpp
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PlayerImpl.h"
#include "Skill.h"
#include "SkillInfoParser.h"
#include "ResultCode.h"
#include "BaseContainer.h"
#include "ItemManager.h"
#include "SkillManager.h"
#include "SkillSlotContainer.h"
#include "ItemInfoParser.h"
#include "GameShell.h"





//-------------------------------------------------------------------------------------------------
eSKILL_RESULT PlayerImpl::CanLevelUpSkill(BOOL     bSkill
                                             ,SLOTCODE curSkillCode
															,SLOTCODE &nextSkillCode)
{
	SkillSlotContainer *	pContainer = GetSkillManager()->GetSkillSlotContainer();
	SkillSlot *				pSkillSlot = pContainer->GetSkillSlot( curSkillCode );

	////////////////////////////////////////
	if( pSkillSlot )
	{
		////////////////////////////////////////
		eSKILL_RESULT rcResult;
		if( bSkill )
			rcResult = m_pSkillManager->CanLevelUpSkill( curSkillCode, curSkillCode + 1 );
		else		
			rcResult = m_pSkillManager->CanLevelUpStyle( curSkillCode, curSkillCode + 1 );

		if( rcResult != RC_SKILL_SUCCESS )	
			return rcResult;

		////////////////////////////////////////
		nextSkillCode = curSkillCode + 1;
	}

	////////////////////////////////////////
	else
	{
		////////////////////////////////////////
		eSKILL_RESULT rcResult;
		if( bSkill )
			rcResult = m_pSkillManager->CanLearnSkill( curSkillCode );
		else	
			rcResult = m_pSkillManager->CanLearnStyle( curSkillCode );

		if( rcResult != RC_SKILL_SUCCESS )	
			return rcResult;

		////////////////////////////////////////
		nextSkillCode = curSkillCode;
	}

	return RC_SKILL_SUCCESS;
}


DWORD PlayerImpl::LevelUpSkill(BOOL        bSkill
                          ,SLOTCODE    curSkillCode
								  ,SLOTCODE    nextSkillCode
								  ,sSKILL_SLOT &skillSlot)
{
	///////////////////////////////////////////////
	sPLAYERINFO_BASE*	pPlayerInfo;
	sSTYLEINFO_BASE *	pStylenfo;
	SkillDetailInfo *	pSkillInfo;

	pPlayerInfo = PlayerCookie::GetCharInfo();

	if( bSkill )
	{
		pSkillInfo = theSkillInfoParser.GetSkillInfo( nextSkillCode );
		pPlayerInfo->m_dwRemainSkill -= pSkillInfo->m_byRequireSkillPoint;
	}
	else
	{
		pStylenfo = theSkillInfoParser.GetStyleInfo( nextSkillCode );
		pPlayerInfo->m_dwRemainSkill -= pStylenfo->m_byRequireSkillPoint;
	}

	///////////////////////////////////////////////
	Vector3D vPos = GetPosition();

	///////////////////////////////////////////////
	if( curSkillCode == nextSkillCode )
	{
		if( bSkill )	
			m_pSkillManager->LearnSkill( curSkillCode, skillSlot );		
		else	
			m_pSkillManager->LearnStyle( curSkillCode, skillSlot );

		GAMELOG.LogSkillLevelUp(this
                               ,theGameShell.GetServerGUID()
										 ,curSkillCode
										 ,FALSE);

	}
	else
	{
		if( bSkill )
			m_pSkillManager->LevelUpSkill( curSkillCode, nextSkillCode, skillSlot );
		else	
			m_pSkillManager->LevelUpStyle( curSkillCode, nextSkillCode, skillSlot );

		///////////////////////////////////////////////
		GAMELOG.LogSkillLevelUp(this
                             ,theGameShell.GetServerGUID()
									  ,nextSkillCode
									  ,TRUE);
	}

	return PlayerCookie::GetCharInfo()->m_dwRemainSkill;
}


/////////////////////////////////////
SLOTCODE PlayerImpl::GetDefaultStyle( DWORD dwWeaponCode )
{
	if( !dwWeaponCode )
		return GetRangeStyle();


	sITEMINFO_BASE *pItemInfo;
	
	/////////////////////////////////////
	pItemInfo = theItemInfoParser.GetItemInfo( dwWeaponCode );
	if( !pItemInfo )
		return GetRangeStyle();

	/////////////////////////////////////
	switch( pItemInfo->m_wType )
	{	
	case ITEMTYPE_TWOHANDSWORD:		return STYLECODE_TWOHANDSWORD_NORMAL;	
	case ITEMTYPE_TWOHANDAXE:			return STYLECODE_TWOHANDAXE_NORMAL;		
	case ITEMTYPE_ONEHANDSWORD:		return STYLECODE_ONEHANDSWORD_NORMAL;	
	case ITEMTYPE_SPEAR:					return STYLECODE_SPEAR_NORMAL;			
	case ITEMTYPE_STAFF:					return STYLECODE_STAFF_NORMAL;			
	case ITEMTYPE_ORB:					return STYLECODE_ORB_NORMAL;				
	case ITEMTYPE_CROSSBOW:				return STYLECODE_CROSSBOW_NORMAL;		
	case ITEMTYPE_ETHERWEAPON:			return STYLECODE_ETHER_NORMAL;			
	}
	return GetRangeStyle();						
}

SLOTCODE PlayerImpl::GetRangeStyle()
{
	switch( GetCharType() )
	{
	case PLAYERTYPE_WARRIOR:		return STYLECODE_WARRIOR_BOW;
	case PLAYERTYPE_PALADIN:		return STYLECODE_PALADIN_BOW;
	case PLAYERTYPE_POWWOW:			return STYLECODE_POWWOW_BOW;
	case PLAYERTYPE_STABBER:		return STYLECODE_STABBER_BOW;
	case PLAYERTYPE_NECROMANCER:	return STYLECODE_NECROMANCER_BOW;
	}
	return STYLECODE_WARRIOR_BOW;
}


BOOL PlayerImpl::CheckClassDefine(BYTE     byUserType
                                 ,CODETYPE dwClassDefine)
{
	__CHECK(byUserType == SKILLUSER_PLAYER);

	return ( GetCharType() == dwClassDefine );
}




















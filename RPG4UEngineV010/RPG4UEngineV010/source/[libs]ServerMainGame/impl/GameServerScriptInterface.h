/*////////////////////////////////////////////////////////////////////////
文 件 名：GameServerScriptInterface.h
创建日期：2007年5月20日
最后更新：2007年5月20日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：
		脚本变量接口
		脚本变量使用方法
			对服务器系统变量的控制，使用下面的方法：
			void	SetVar( int varIndex, int nValue );
			int	GetVar( int varIndex );

			当SerVar和GetVar的参数：
				varIndex >=0 	访问是变量数组
				varIndex < 0  访问是属性

			属性分为：
				a.当前触发脚本的char属性(金钱，等级，职业等等)
				b.系统内部属性(未定义)


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __GAMESERVERSCRIPTINTERFACE_H__
#define __GAMESERVERSCRIPTINTERFACE_H__
#pragma once



#include "IScriptInterface.h"
#include "PacketStruct_ClientGameS_Event.h"

class Player;
struct MSG_CG_EVENT_SHOWTEXT_CMD;

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class   GameServerScriptInterface:public IScriptInterface
{
public:
	GameServerScriptInterface();

public:
	BOOL	HasVarTable				(){ return TRUE; }
	void	HighLightSourceLine	( int /*nLine*/ ){}
	void	MarkSourceLine			( int /*nLine*/ ){}


	BOOL	CheckPlayer				(LPCTSTR szErrorText);
	BOOL	SendPacket				(MSG_OBJECT_BASE * pMsg, WORD wSize, BOOL bCrypt = FALSE );
	void	MakeShowTextPacket	(BYTE byType, LPCTSTR szText);

public:
	void		SetVar			(int               varIndex
									,int               oprType
									,void*             pParams
									,int					 nParamCount = 1);

	void		SetTeamVar		(int               varIndex
									,int               oprType
									,void*             pParams
									,int					 nParamCount = 1);

	int		GetVar			(int               varIndex
									,int					 nPlayerId = -1)		;
	
	LPCSTR	GetLabelOfVar	( int					 varIndex );


public:
	void	  ShowDialog	( LPCSTR pszText , BOOL bEndDialog );

    void   Tell			( LPCSTR pszText );  //私聊
	void    Talk			( DWORD time, DWORD color, LPCSTR pszText );
	void    TalkInShout	( DWORD time, DWORD color, LPCSTR pszText );
	void    TalkInSector	(int         nMapId
                        ,int         nX
								,int         nY
								,DWORD       time
								,DWORD       color
								,const char* pszText);
    void    Broadcast	( LPCSTR pszText );       //广播

	void	JumpToMap( int nMapId,  int x, int y );
	void	OpenShop ( DWORD  npcID );
	void	DoCommand( LPCSTR command );
	void	ShowAsk	( KEYWORD varID, LPCSTR pszText );
	void	Show		( short stImageId, int x, int y, DWORD dwLife, DWORD dwFlag, DWORD dwFlagTime );

	void	RemoveAllEquip	( );
	void	RemoveAllItem	( );
	void	SetBornPoint	( DWORD dwMapID, int iPosX, int iPosY );

	BOOL	ExistItem				( LPCSTR szItemName,  int nItemCount);
	void	AddItem					( LPCSTR pszItemName, int nItemCount );
	void	DeleteItem				( LPCSTR pszItemName, int nItemCount );
	BOOL	IsItemInEquip			( LPCSTR	pItemName  );
	BOOL	IsSkillActived			( WORD ustSkillItemID,WORD ustSkillLevel );

	int	GetMapCodeByName		( LPCSTR pszMapName );

	void	TeashSkill				( const std::vector<int>&vectorSkill );
	void	AddSkill					( int nSkillId );

//	void	Quest_CastQuest( int nQuestID );
//	void	Quest_GetQuest( int nQuestID );
	DWORD	Quest_CancelQuest		( int nQuestID );
	DWORD	Quest_DoneQuest		( int nQuestID );
	void	Quest_SetQuestState	( int nQuestID, int nState );
	void	Quest_GetQuestState	( int nQuestID, int nVarID );
	void	Quest_RelateQuest		( int nQuestID, int nState, int nEnter );
	void	Quest_CanGetNewQuest	( int nVarID );
	void	Printf					( LPCSTR string, ... );

	void	RefreshMonster			(int    nMapId
                              ,int    x
										,int    y
										,LPCSTR szArea);

	void	PlayBGMusic				(LPCSTR	szFileName
										,DWORD	dwParam)		;

	void	MakeItem			(BYTE byCreatNewOrOld
                        ,int  nRate
								,int  nIdBegin[]
								,int  nIdEnd[]
								,int  nCount);

	int	GetCharCount	( int nMapId );
	void	ShowQuestDialog( KEYWORD varID, LPCSTR szInfo );
	void	UseSkill			( int nSkillId, int nSkillLevel );
	void	PopNpcList		();
	int	GetWorldTime	( int nTimeType );
	int	GetBirthday		( int nTimeType );
	void	ShowBank			();

	void	NpcMoveNext		();
	void	NpcStopMove		();
	void	NpcStartMove	();
	void	NpcPlayAnim		(LPCSTR szName
                        ,int    nTimes
								,LPCSTR szEndName
								,BOOL   bForView);

	BOOL	CanLearnSkill	( int nSkillId, int nSkillLevel );
	void	DoRide			( int nID );
	int	GetDromeID		();
   void	SetDialogName	( LPCSTR szName );
	void	SetHotkey		( int nIndex, int nID );
   BOOL	DumpInfoToDB	( LPCSTR szInfo );

	int	GetTeamPlayerCount	();
	int	GetTeamPlayerLevel	(int nType );
	int	GetTeamPlayerID		(UINT nIndex );
	void	SetTargetPlayer		(DWORD dwID );

	void	CreateMonster	(DWORD dwMapId
                        ,float fPosX
								,float fPosY
								,float fRadius
								,float fBodySize
								,int   nMonsterIndex
								,int   nMonsterNum);

	int	GetEmptySlotCount	();
   void  CreateGuild			();
	void	LogTip				(LPCSTR      szTip,LPCSTR      szTip2,eTIP_TYPE type);

protected:
	VG_DWORD_PROPERTY	(CharacterID);
	VG_DWORD_PROPERTY	(TargetNpcID);
	VG_DWORD_PROPERTY	(TempValue);
	Player*				m_pPlayer;
	MSG_CG_EVENT_SHOWTEXT_CMD	m_msgShowText;
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(GameServerScriptInterface ,API_NULL );
//extern GameServerScriptInterface& GetGameServerScriptInterface();
#define theGameServerScriptInterface	GetGameServerScriptInterface()


#endif //__GAMESERVERSCRIPTINTERFACE_H__
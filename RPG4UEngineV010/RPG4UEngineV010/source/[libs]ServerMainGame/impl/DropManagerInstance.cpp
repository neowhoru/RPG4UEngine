#include "StdAfx.h"
#include "DropManagerInstance.h"
#include "ObjectSystem.h"
#include "Item.h"
#include "Map.h"
#include "Field.h"
#include <PacketStruct_DBP_Game.h>
#include <Protocol_DBP_Game.h>
#include "Player.h"
#include <MiscUtil.h>
#include "ServerShell.h"
#include "ItemManager.h"
#include "NPC.h"
#include "QuestItemDropInfoParser.h"
#include "QuestManager.h"
#include "BattleUtil.h"
#include "PacketStruct_ClientGameS_Event.h"
#include "PlayerLVSumOpr.h"
#include "BaseZone.h"
#include "DropResultOpr.h"
#include "DropItemGradeParser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(DropManagerInstance, ()  , gamemain::eInstPrioGameFunc);



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DropManagerInstance::DropManagerInstance()
{
}

DropManagerInstance::~DropManagerInstance()
{
	Release();
}

VOID DropManagerInstance::Release()
{
	_SUPER::Release();
}

BOOL DropManagerInstance::Init()
{
	return _SUPER::Init	();
}




BOOL DropManagerInstance::Drop	(Field *    pField
                                 ,DWORD      SectorIndex
											,Vector3C * pOrgPos
											,Player *   pOwner
											,NPC*       pMonster)
{

	DWORD					dwMonsterKey;
	sNPCINFO_BASE *	pMonsterInfo;
	DWORD					dwPlayerKey		= 0;
	FLOAT					fPlusBonusPer	= 1.0f;
	BaseZone *			pZone;

	__CHECK_PTR(pMonster);

	dwMonsterKey		= pMonster->GetObjectKey();
	pMonsterInfo		= pMonster->GetBaseInfo();


	if( pOwner )
		dwPlayerKey = pOwner->GetObjectKey();

	//////////////////////////////////////
	DWORD					dwPlayerNum;
	PlayerLVSumOpr		lvSum;
	LEVELTYPE			nLevelSum;
	DWORD					dwLevelAvg;

	pZone = pField->GetMap()->GetGameZone();
	pZone->OnDropItem	(lvSum
							,fPlusBonusPer
							,pOrgPos
							,pField
							,SectorIndex);


	/////////////////////////////////
	dwPlayerNum = lvSum.GetPlayerNum();
	__CHECK(dwPlayerNum > 0);


	nLevelSum	= lvSum.GetLevelSum();
	dwLevelAvg	= (nLevelSum / dwPlayerNum);

	__CHECK(dwLevelAvg > 0);

	/////////////////////////////////
	//分发掉落种类
	DispatchDrop	(pMonsterInfo
                  ,dwPlayerNum
						,fPlusBonusPer
						,(LEVELTYPE)dwLevelAvg);
	

	/////////////////////////////////
	//附加物品掉落
	AdditionalItemDrop(pMonsterInfo->m_byItemDropRate
							,sNPCINFO_BASE::MAX_DROPITEMCODE_NUM
							,pMonsterInfo->m_wDropItemRate
							,pMonsterInfo->m_ItemCode);


	/////////////////////////////////
	//任务物品掉落
	DropQuest(pOwner,pMonster);


	/////////////////////////////////
	//处理所有掉落结果
	DropResultOpr	opr=	{this
								,pMonsterInfo
								,pField
								,pOrgPos
								,dwPlayerKey
								,dwMonsterKey};
	ForEachResult(opr);


	return TRUE;
}

BOOL DropManagerInstance::DispatchDrop	(sNPCINFO_BASE*	pNpcInfo
													,DWORD            PlayerNum
													,FLOAT            fBonusPercent
													,LEVELTYPE        LVAverage)
{
	__CHECK_PTR(pNpcInfo);

   eNPC_GRADE			monsterGrade	= (eNPC_GRADE)pNpcInfo->m_byGrade;
	LEVELTYPE			monsterLV		= pNpcInfo->m_DisplayLevel;
	eDROP_TYPE			dropType;

	ClearDropResult();

	//////////////////////////////////////
	dropType = (eDROP_TYPE)m_DropRatioSelecter[monsterGrade][PlayerNum].RandomizedKey();
	switch(dropType)
	{
	case DROP_MONEY_1:
	case DROP_MONEY_40:
	case DROP_MONEY_400:
		return ProcessMoneyDrop	(fBonusPercent
                           ,LVAverage
									,dropType
									,monsterGrade
									,monsterLV);
	case DROP_ITEMGRADE_0:
	case DROP_ITEMGRADE_1:
	case DROP_ITEMGRADE_2:
	case DROP_ITEMGRADE_3:
	case DROP_ITEMGRADE_4:
	case DROP_ITEMGRADE_5:
	case DROP_ITEMGRADE_6:
	case DROP_ITEMGRADE_7:
	case DROP_ITEMGRADE_8:
	case DROP_ITEMGRADE_9:
	case DROP_ITEMGRADE_10:
		return ProcessGeneralItemDrop	(dropType
                           ,monsterLV);

	case DROP_ITEM_MONSTER:
		return ProcessMonsterItemDrop	(pNpcInfo->m_MonsterCode);

	case DROP_ITEM_RARE:
		return ProcessRareItemDrop	(PlayerNum
                              ,monsterGrade
										,monsterLV);
	}

	return FALSE;
}



BOOL DropManagerInstance::AdditionalItemDrop(BOOL           bItemDrop
                                        ,INT            nArrayNum
													 ,WORD *         pDropItemRate
													 ,WORD *         pDropItemCode)
{
	if( bItemDrop )
	{
		return ProcessMonsterPrivateDrop	(nArrayNum
                                 ,pDropItemRate
											,pDropItemCode);
	}
	return FALSE;
}

//---------------------------------------------------------------------------------------------------------------------------------
BOOL DropManagerInstance::ProcessGeneralItemDrop(eDROP_TYPE     dropType
                                  ,LEVELTYPE      monsterDisplayLV)
{
	///////////////////////////////////////
	// -5 <= nLevelDiff <= 5
	// DROP_ITEMGRADE_0 ~ DROP_ITEMGRADE_11 :
	INT nLevelDiff		= dropType - DROP_ITEMGRADE_0 - 5;
	INT nDropItemLV	= monsterDisplayLV + nLevelDiff;

	///////////////////////////////////////
	if( nDropItemLV <= 1 )
	{
		nDropItemLV = 2;
	}

	if( nDropItemLV >= MAX_LEVEL )
		return FALSE;

	///////////////////////////////////////
	DropItemInfo * pDropItem;
	
	pDropItem = theDropItemGradeParser.DropItemAround( (LEVELTYPE)nDropItemLV );
	if( !pDropItem )
		return FALSE;


	///////////////////////////////////////
	sDROP_RESULT	result;
	result.m_ItemInfo	= *pDropItem;
	result.m_DropType	= dropType;
	AddDropResult(result);

	return TRUE;
}

BOOL DropManagerInstance::ProcessMonsterItemDrop	(CODETYPE		 dwMonsterCode)
{

	__CHECK(theMonsterItemDrop.DropMonsterItem( dwMonsterCode) );


	return TRUE;
}

//---------------------------------------------------------------------------------------------------------------------------------
BOOL DropManagerInstance::ProcessMonsterPrivateDrop(INT            nArrayNum
                                         ,WORD *         pDropItemRate
													  ,WORD *         pDropItemCode)
{
	__CHECK(nArrayNum > 0);
	__CHECK_PTR(pDropItemRate);
	__CHECK_PTR(pDropItemCode);

	//////////////////////////////////////////
	INT nPercent	= 0;
	INT nPercent2	= 0;
	INT nSeed		= math::Random( (INT)MAXTOP_ITEM_DROP_RATE );

	///////////////////////////////////////////
	for(INT i  = 0 ; i < nArrayNum ; ++i )
	{
		nPercent		=  nPercent2;
		nPercent2	+= pDropItemRate[i];
		if(nPercent <= nSeed && nSeed < nPercent2)
		{
			AddItemResult(pDropItemCode[i]);
			return TRUE;
		}
	}

	return FALSE;
}



BOOL DropManagerInstance::AddItemResult	(DWORD dwItemCode,eDROP_TYPE dropType)
{
	//ItemSlot			slot;
	sDROP_RESULT	result;
	//slot.SetCode(dwItemCode);

	//if(slot.GetItemInfo() == NULL)
	//	return FALSE;
	result.m_ItemInfo.SetCode	(dwItemCode);
	result.m_DropType	= dropType;
	AddDropResult(result);
	return TRUE;
}


BOOL DropManagerInstance::DropItemAt(Field *    pField
                                 ,Vector3C * pvPos
											,DWORD      dwPlayerKey
											,DWORD      dwFromMonsterKey
											,ItemSlot & slot)
{
	Item * pFieldItem;
	
	pFieldItem = (Item *)theObjectSystem.AllocObject( ITEM_OBJECT );
	pFieldItem->CreateItem(dwPlayerKey
                         ,dwFromMonsterKey
								 ,slot);
	return DropItemAt(pFieldItem, pField, pvPos);;
}


BOOL DropManagerInstance::DropMoneyAt(Field *    pField
                                  ,Vector3C * pvPos
											 ,DWORD      dwPlayerKey
											 ,DWORD      dwFromMonsterKey
											 ,MONEY &    Money
											 ,DWORD      DelayTime)
{
	Item * pFieldMoneyItem;
	
	pFieldMoneyItem = (Item *)theObjectSystem.AllocObject( MONEY_OBJECT );

	pFieldMoneyItem->CreateMoney(dwPlayerKey
                               ,dwFromMonsterKey
										 ,Money);

	if( 0 == DelayTime )
		return DropItemAt(pFieldMoneyItem, pField, pvPos);

	pFieldMoneyItem->SetDropDelay(pField
                                ,(Vector3D*)pvPos
										  ,DelayTime);
	_AddToDelayList	(pFieldMoneyItem
                     ,pFieldMoneyItem->GetObjectKey())		;
	return TRUE;
}


BOOL DropManagerInstance::DropItemAt(Item *     pDropItem
                                    ,Field *    pField
												,Vector3C*  pvPos)
{
	__CHECK_PTR(pField);

	if(!pField->EnterField( pDropItem, (Vector3D*)pvPos ) )
	{
		theObjectSystem.FreeObject( pDropItem );
		return FALSE;
	}
	return TRUE;
}



BOOL DropManagerInstance::CanDropQuestItem	(Player*  pPlayer
                                    ,CODETYPE questID
												,DWORD    questStep
												,SLOTCODE itemCode)
{
	if(!pPlayer)
		return TRUE;

	DWORD dwDropMax;

	// 如果不是此任务状态则不掉
	if ( pPlayer->GetQuestState( questID ) != questStep )
	{
		return FALSE;
	}

	dwDropMax = theQuestItemDropInfoParser.GetItemDropMax(itemCode);

	// 物品已经满了
	return !pPlayer->GetItemManager()->IsExistItem(SI_INVENTORY
                                                 ,itemCode
																 ,(DURATYPE)dwDropMax
																 ,NULL
																 ,NULL);
}


BOOL DropManagerInstance::DropQuest(Player* pPlayer
                                   ,NPC*    pMonster)
{
	sQUESTITEM_DROPINFO*		pDropInfo;
	sQUESTITEM_DROPUNIT*		pDropUnit;

	__CHECK(pMonster && pMonster->IsMonster());
	__CHECK(pPlayer  && pPlayer->IsPlayer());


	pDropInfo = theQuestItemDropInfoParser.GetQuestItemDropInfo(pMonster->GetBaseInfo()->m_MonsterCode);
	__CHECK_PTR(pDropInfo);

	for ( UINT i=0; i<pDropInfo->m_DropUnits.size(); i++ )
	{
		pDropUnit = &pDropInfo->m_DropUnits[i];

		// 更新记数
		_UpdateQuestVarInfo( pDropUnit, pMonster, pPlayer, i );

		// 有任务物品
		if ( pDropUnit->m_ItemCode != INVALID_DWORD_ID )
		{
			// 是否需要掉落此任务物品
			if (! CanDropQuestItem	(pPlayer
                                 ,pDropUnit->m_QuestID
											,pDropUnit->m_QuestStep
											,(SLOTCODE)pDropUnit->m_ItemCode) )
			{
				continue;
			}

			// 掉落物品
			if(!DrawLots(pDropUnit->m_DropRate))
				continue;
			AddItemResult(pDropUnit->m_ItemCode);
		}
	}
	return TRUE;
}



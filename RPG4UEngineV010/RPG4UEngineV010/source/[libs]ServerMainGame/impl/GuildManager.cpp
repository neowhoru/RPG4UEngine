#include "StdAfx.h"
#include "GuildManager.h"
#include "Guild.h"
#include "Player.h"
#include <PacketStruct_Game_Guild.h>
#include <GuildMemberFactoryManager.h>
#include <GuildFactoryManager.h>
#include <GuildMemberImpl.h>
#include <GuildImpl.h>

enum { MAX_GUILD_POOL_NUM = 512, };


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GuildManager, ()  , gamemain::eInstPrioGameFunc);



GuildManager::GuildManager(void)
{
	m_Guilds.Initialize(MAX_GUILD_POOL_NUM);
}

GuildManager::~GuildManager(void)
{
	//Release();
}

VOID GuildManager::Init	(DWORD dwGuildSize, DWORD dwMemberSize)
{
	REG_GUILDMEMBERFACTORY	(GuildMemberImpl	, GUILD_TYPE_DEFAULT, dwMemberSize);
	REG_GUILDFACTORY			(GuildImpl			, GUILD_TYPE_DEFAULT, dwGuildSize);

	theGuildMemberFactoryManager.InitAllFactories();
	theGuildFactoryManager.InitAllFactories();
}

VOID GuildManager::Release()
{
	for( GameGuildTableIt it = m_Guilds.begin() ; it != m_Guilds.end() ; ++it )
	{
		(*it)->Destroy();
		theGuildFactoryManager.DestroyInstance( (*it) );
	}

	theGuildFactoryManager.ReleaseAllFactories();
	theGuildMemberFactoryManager.ReleaseAllFactories();
}


BOOL GuildManager::ValidPlayer( Player * /*pPlayer*/ )
{
	return TRUE;
}


eGUILD_RESULT GuildManager::GetGuildInfo(Player *                 pPlayer
                                           ,sGUILD_INFO_PACKET & rGuildInfo)
{
	if( !ValidPlayer(pPlayer) ) 
		return _RC_GID(INVALID_STATE);

	GUILDGUID guildID = pPlayer->GetCharInfo()->m_GuildGuid;
	if( 0 == guildID )
		return _RC_GID(CANNOT_SELECT_GUILD_STATE);


	///////////////////////////////////////////
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
	{
		MSG_GZ_GUILD_SELECT_SYN msgGuild;
		msgGuild.m_GuildGuid = guildID;
		msgGuild.m_CharGuid	= pPlayer->GetCharGuid();

		if( !pPlayer->SendToGuildServer( &msgGuild, sizeof(msgGuild) ) )
		{
			return _RC_GID(DISCONNECTED_GUILD_SERVER);
		}
		return _RC_GID(NEED_SELECT);
	}

	pGuild->GetGuildInfo( rGuildInfo );

	return _RC_GID(SUCCESS);
}

///////////////////////////////////////////
eGUILD_RESULT GuildManager::CreateGuild(Player * pPlayer
                                       ,LPCTSTR  szGuildName)
{
	if( !ValidPlayer(pPlayer) )
		return _RC_GID(INVALID_STATE);

	///////////////////////////////////////////
	GUILDGUID guildID = pPlayer->GetCharInfo()->m_GuildGuid;
	if( 0 != guildID )
		return _RC_GID(CANNOT_CREAT_GUILD_STATE);

	///////////////////////////////////////////
	if( pPlayer->GetMoney() < CREATE_GUILD_LIMIT_MONEY )
		return _RC_GID(CANNOT_CREAT_MONEY_STATE);

	///////////////////////////////////////////
	if( pPlayer->GetLevel() < CREATE_GUILD_LIMIT_LEVEL )
		return _RC_GID(CANNOT_CREAT_LEVEL_STATE);


	MSG_GZ_GUILD_CREATE_SYN msg;
	lstrcpyn( msg.m_szGuildName		, szGuildName, MAX_GUILDNAME_LENGTH );
	lstrcpyn( msg.m_szMasterCharName	, pPlayer->GetCharName(), MAX_CHARNAME_LENGTH );

	msg.m_MasterCharGuid	= pPlayer->GetCharGuid();
	msg.m_byMasterClass	= pPlayer->GetCharType();
	msg.m_MasterLV			= pPlayer->GetLevel();

	if( ! pPlayer->SendToGuildServer( &msg, sizeof(msg) ) )
		return _RC_GID(DISCONNECTED_GUILD_SERVER);

	pPlayer->MinusMoney( CREATE_GUILD_LIMIT_MONEY );

	return _RC_GID(SUCCESS);
}


//////////////////////////////////////////////////////////
eGUILD_RESULT GuildManager::DestroyGuild( Player * pPlayer )
{
	if( !ValidPlayer(pPlayer) ) 
		return _RC_GID(INVALID_STATE);

	//////////////////////////////////////////
	GUILDGUID guildID = pPlayer->GetCharInfo()->m_GuildGuid;
	if( 0 == guildID )
		return _RC_GID(CANNOT_DESTROY_STATE);

	//////////////////////////////////////////
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
		return _RC_GID(INVALID_STATE);

	//////////////////////////////////////////
	if(  pGuild->GetMemberNum() > 1 )
		return _RC_GID(CANNOT_DESTROY_EXIST_MEMBER_STATE);

	//////////////////////////////////////////
	if( pGuild->GetMasterCharGuid() != pPlayer->GetCharGuid() )
		return _RC_GID(CANNOT_DESTROY_NOT_MASTER_STATE);


	MSG_GZ_GUILD_DESTROY_SYN msg;
	msg.m_GuildGuid		= guildID;
	msg.m_MasterCharGuid	= pPlayer->GetCharGuid();
	if( ! pPlayer->SendToGuildServer( &msg, sizeof(msg) ) )
		return _RC_GID(DISCONNECTED_GUILD_SERVER);

	return _RC_GID(SUCCESS);
}

//////////////////////////////////////////
eGUILD_RESULT GuildManager::CanJoin(Player * pPlayerFrom
                                   ,Player * pPlayerTo)
{
	GUILDGUID guildID = pPlayerFrom->GetCharInfo()->m_GuildGuid;
	if( 0 == guildID )
		return _RC_GID(CANNOT_INVITE_STATE);

	//////////////////////////////////////////
	GUILDGUID guildTo = pPlayerTo->GetCharInfo()->m_GuildGuid;
	if( 0 != guildTo )
		return _RC_GID(CANNOT_INVITE_STATE);

	//////////////////////////////////////////
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
		return _RC_GID(INVALID_STATE);

	//////////////////////////////////////////
	if( !pGuild->CheckPlayerRight( pPlayerFrom, eGUILD_RIGHTS_INVITE ) )
		return _RC_GID(DONT_HAVE_RIGHTS_STATE);

	if( pGuild->IsFullMember() )
		return _RC_GID(FULL_MEMBER_STATE);

	return _RC_GID(SUCCESS);
}


//////////////////////////////////////////
eGUILD_RESULT GuildManager::InviteMember( Player * pPlayerFrom, Player * pPlayerTo )
{
	eGUILD_RESULT result;
	result = CanJoin( pPlayerFrom, pPlayerTo );
	if( result != _RC_GID(SUCCESS) )
		return result;

	pPlayerTo->SetInvitationHostKey( pPlayerFrom->GetObjectKey() );

	return _RC_GID(SUCCESS);
}

//////////////////////////////////////////
eGUILD_RESULT GuildManager::AnswerMember( Player * pPlayerFrom, Player * pPlayerTo, BOOL bAccept )
{
	eGUILD_RESULT result;
	
	result = CanJoin( pPlayerFrom, pPlayerTo );
	if( result != _RC_GID(SUCCESS) ) 
		return result;

	if( bAccept )
	{
		GUILDGUID guildID = pPlayerFrom->GetCharInfo()->m_GuildGuid;

		MSG_GZ_GUILD_JOIN_SYN msg;
		lstrcpyn( msg.m_szCharName, pPlayerTo->GetCharName(), MAX_CHARNAME_LENGTH );
		msg.m_GuildGuid		= guildID;
		msg.m_MasterCharGuid	= pPlayerFrom->GetCharGuid();
		msg.m_CharGuid			= pPlayerTo->GetCharGuid();
		msg.m_byClass			= pPlayerTo->GetCharType();
		msg.m_CharLV			= pPlayerTo->GetLevel();

		if( ! pPlayerTo->SendToGuildServer( &msg, sizeof(msg) ) )
		{
			return _RC_GID(DISCONNECTED_GUILD_SERVER);
		}
	}
	else
	{
		pPlayerTo->SetInvitationHostKey( 0 );
	}

	return _RC_GID(SUCCESS);
}


//////////////////////////////////////////
eGUILD_RESULT GuildManager::WithdrawMember( Player * pPlayer )
{
	GUILDGUID guildID = pPlayer->PlayerCookie::GetCharInfo()->m_GuildGuid;
	if( 0 == guildID )
		return _RC_GID(CANNOT_WITHDRAW_STATE);

	//////////////////////////////////////////
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
		return _RC_GID(INVALID_STATE);

	//////////////////////////////////////////
	if( !pGuild->CheckPlayerRight( pPlayer, eGUILD_RIGHTS_WITHDRAW ) )
		return _RC_GID(DONT_HAVE_RIGHTS_STATE);

	//////////////////////////////////////////
	MSG_GZ_GUILD_WITHDRAW_SYN msg;
	msg.m_GuildGuid = pGuild->GetGuildGuid();
	msg.m_CharGuid	= pPlayer->GetCharGuid();

	if( ! pPlayer->SendToGuildServer( &msg, sizeof(msg) ) )
	{
		return _RC_GID(DISCONNECTED_GUILD_SERVER);
	}

	return _RC_GID(SUCCESS);
}


//////////////////////////////////////////
VOID GuildManager::OnAddGuild( const sGUILD_INFO_BASE & IN rInfo, const sGUILD_MEMBER_INFO * pMemberInfo, BYTE MemberCount )
{
	Guild * pGuild = NULL;
	if( NULL != (pGuild = FindGuild(rInfo.m_GuildGuid)) )
	{
		RemoveGuild( pGuild->GetGuildGuid() );
		pGuild->Destroy();
		theGuildFactoryManager.DestroyInstance( pGuild );
		pGuild = NULL;
	}

	pGuild = theGuildFactoryManager.CreateInstance(GUILD_TYPE_DEFAULT);
	pGuild->Create( rInfo, pMemberInfo, MemberCount );
	AddGuild( pGuild, pGuild->GetGuildGuid() );
}

	//////////////////////////////////////////
VOID GuildManager::OnDestroyGuild( GUILDGUID guildID )
{
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
		return;

	RemoveGuild( pGuild->GetGuildGuid() );
	pGuild->Destroy();

	theGuildFactoryManager.DestroyInstance( pGuild );
	pGuild = NULL;
}

VOID GuildManager::OnWithdrawGuild( GUILDGUID guildID, CHARGUID CharGuid )
{
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
		return;
	pGuild->Withdraw(CharGuid);
}

VOID GuildManager::OnJoinGuild(GUILDGUID                     guildID
                              ,const sGUILD_MEMBER_INFO & IN info)
{
	Guild * pGuild = FindGuild( guildID );
	if( !pGuild )
		return;
	pGuild->Join(info);
}


VOID GuildManager::DisplayerGuildInfo()
{
	DWORD dwNum = 0;
	for( GameGuildTableIt it = m_Guilds.begin() ; it != m_Guilds.end() ; ++it )
	{
		dwNum += (*it)->GetMemberNum();
	}
	DISPMSG	( "Guild : %u, GuildMember : %u\n"
				, m_Guilds.GetDataNum()
				, dwNum );
}

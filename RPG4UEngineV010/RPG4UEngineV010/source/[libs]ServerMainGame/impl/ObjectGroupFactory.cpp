#include "StdAfx.h"
#include "ObjectGroup.h"
#include "ObjectGroupFactory.h"



ObjectGroupFactory::ObjectGroupFactory()
{
	m_pGroupPool = new CMemoryPoolFactory<ObjectGroup>;
}

ObjectGroupFactory::~ObjectGroupFactory()
{
	SAFE_DELETE( m_pGroupPool );	
}

VOID ObjectGroupFactory::Release()
{
	if( m_pGroupPool )
	{
		m_pGroupPool->Release();
	}
}

VOID ObjectGroupFactory::Init( DWORD dwMaxPoolSize )
{
	m_pGroupPool->Initialize( dwMaxPoolSize, dwMaxPoolSize/2+1 );
}

ObjectGroup * ObjectGroupFactory::AllocGroup()
{
	return m_pGroupPool->Alloc();
}


VOID ObjectGroupFactory::FreeGroup( ObjectGroup* pGroup )
{
	m_pGroupPool->Free( pGroup );
}
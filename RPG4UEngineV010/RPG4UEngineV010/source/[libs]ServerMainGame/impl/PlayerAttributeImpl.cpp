#include "stdafx.h"
#include "PlayerAttributeImpl.h"
#include "Player.h"



PlayerAttributeImpl::PlayerAttributeImpl()
:m_pPlayer(NULL)
{
}

PlayerAttributeImpl::~PlayerAttributeImpl()
{
}

VOID PlayerAttributeImpl::SetOwner( PlayerCookie* pPlayer )
{
	_SUPER::SetOwner( pPlayer );
	m_pPlayer = (Player*)pPlayer;
}


////////////////////////////////////////////////////////////////////////
VOID PlayerAttributeImpl::UpdateEx()
{
	int	iAttSpeedRatio		;
	int	iMoveSpeedRatio	;
	DWORD dwMaxHP				;
	DWORD dwMaxMP				;


	////////////////////////////////////////////////////////////////////////
	iAttSpeedRatio		= m_pPlayer->GetAttSpeedRatio();
	iMoveSpeedRatio	= m_pPlayer->GetMoveSpeedRatio();
	dwMaxHP				= m_pPlayer->GetMaxHP();
	dwMaxMP				= m_pPlayer->GetMaxMP();

	_SUPER::Update();

	////////////////////////////////////////////////////////////////////////
	if( iAttSpeedRatio != m_pPlayer->GetAttSpeedRatio() )
	{
 		m_pPlayer->SendAttrChanging(ATTRIBUTE_ATTACK_SPEED
                                 ,m_pPlayer->GetAttSpeedRatio())			;
	}
	
	////////////////////////////////////////////////////////////////////////
	if( iMoveSpeedRatio != m_pPlayer->GetMoveSpeedRatio() )
	{
		m_pPlayer->SendAttrChanging(ATTRIBUTE_MOVE_SPEED
                                 ,m_pPlayer->GetMoveSpeedRatio())			;
	}
	
	////////////////////////////////////////////////////////////////////////
	if( dwMaxHP != m_pPlayer->GetMaxHP() )
	{
		m_pPlayer->SendAttrChanging(ATTRIBUTE_MAX_HP
                                 ,m_pPlayer->GetMaxHP())			;
	}
	
	////////////////////////////////////////////////////////////////////////
	if( dwMaxMP != m_pPlayer->GetMaxMP() )
	{
		m_pPlayer->SendAttrChanging(ATTRIBUTE_MAX_MP
                                 ,m_pPlayer->GetMaxMP())			;
	}
}




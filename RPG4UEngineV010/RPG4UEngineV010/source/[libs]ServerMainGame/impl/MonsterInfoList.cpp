#include "StdAfx.h"
#include ".\monsterinfolist.h"
#include <Struct.h>
#include <ScriptRead.h>


MonsterInfoList::MonsterInfoList()
	:	m_pMonsterInfoHashTable ( NULL )
{
}

MonsterInfoList::~MonsterInfoList()
{
	ASSERT( m_pMonsterInfoHashTable == NULL );
}

VOID MonsterInfoList::Release()
{
	Unload();
	delete m_pMonsterInfoHashTable;
	m_pMonsterInfoHashTable = NULL;
}

VOID MonsterInfoList::Init( DWORD dwMonsterInfoPoolSize )
{
	ASSERT( m_pMonsterInfoHashTable == NULL );
	m_pMonsterInfoHashTable = new util::SolarHashTable<BASE_MONSTERINFO *>;
	m_pMonsterInfoHashTable->Initialize( dwMonsterInfoPoolSize );
}


VOID MonsterInfoList::Unload()
{

	BASE_MONSTERINFO * pInfo = NULL ;
	m_pMonsterInfoHashTable->SetFirst();
	while( pInfo = m_pMonsterInfoHashTable->GetNext() )
	{
		delete pInfo;
	}
	m_pMonsterInfoHashTable->RemoveAll();
}

#pragma warning ( push )
#pragma warning ( disable : 4244)

BOOL MonsterInfoList::Load( char * pszFileName )
{
	ASSERT( m_pMonsterInfoHashTable != NULL );
	Unload();
	
	// 파일 열기
	CScriptRead sr;
	if ( !sr.Create( pszFileName ) )
	{
		DISPMSG("File Open Error : MonsterInfoList.txt");
		return (FALSE);
	}
	BASE_MONSTERINFO * pInfo = new BASE_MONSTERINFO;

	sr.GetToken(TT_STRING); 
	strncpy( pInfo->m_pszName, sr.GetTokenStringPtr(), MAX_MONSTERNAME_LENGTH );
	sr.GetToken(TT_NUMBER); pInfo->m_MonsterCode = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_NCode = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_byGrade = sr.GetTokenNumber();								// 보스/일반
	sr.GetToken(TT_NUMBER); pInfo->m_Level = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMaxHP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwRecoverHP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMaxMP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwRecoverMP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wSize = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttProperty = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMinDamage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMaxDamage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwPhyDef = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwMagicDef = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttitude = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAvoid = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wHelp = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wClass = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wWaterResist = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wFireResist = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wWindResist = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wEarthResist = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_wPhyAttRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wPhyAttEvade = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttType = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttRange = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wViewRange = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMoveRange = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMoveSpeed = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttSpeed = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_byItemDropRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_byMoneyDropGrade = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_ItemCode1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_byDropItem1Rate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_ItemCode2 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_byDropItem2Rate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_ItemCode3 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_byDropItem3Rate = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_dwOptionID1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwOptionParam1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwOptionID2 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_dwOptionParam2 = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_SkillCode1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_bySkill1Rate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_SkillCode2 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_bySkill2Rate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_SkillCode3 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_bySkill3Rate = sr.GetTokenNumber();
	
	sr.Destroy();

	ASSERT( NULL == m_pMonsterInfoHashTable->GetData( pInfo->m_MonsterCode ) );
	m_pMonsterInfoHashTable->Add( pInfo, pInfo->m_MonsterCode );

	return TRUE;

}
#pragma warning ( pop )
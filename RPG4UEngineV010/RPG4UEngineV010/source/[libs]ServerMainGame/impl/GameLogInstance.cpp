/*////////////////////////////////////////////////////////////////////////
文 件 名：GameLogInstance.cpp
创建日期：2009年4月7日
最后更新：2009年4月7日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "GameLogInstance.h"
#include "ItemManager.h"
#include "SkillManager.h"
#include "MissionManager.h"
#include "ItemSlotContainer.h"
#include "SkillSlotContainer.h"
#include "SkillInfoParser.h"
#include "Map.h"
#include "LogWriteManager.h"


#define _GAMELOG_INIT(DATA)\
						if(!pPlayer || !IsRun() )	\
							return;\
						DATA.Init();\
						LogTitle(&DATA\
								  ,pPlayer->GetUserID()\
								  ,pPlayer->GetCharName())

#define _GAMELOG_ACTION(code)		_GAMELOG_INIT(m_ActionData );\
												m_ActionData.dwServerCode		= dwServerCode;\
												m_ActionData.dwActionLogCode	= (code)


#define _GAMELOG_ITEM0(code)		_GAMELOG_INIT( m_ItemData);\
						m_ItemData.dwServerCode = dwServerCode;\
						m_ItemData.dwItemCode	= (code)

#define _GAMELOG_ITEM(code)		if(pItemSlot)\
												_SetItemInfo( &m_ItemData, pItemSlot );\
											_GAMELOG_ITEM0(code)



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GameLogInstance, ()  , gamemain::eInstPrioGameFunc);



GameLogInstance::GameLogInstance(void)
{
}

GameLogInstance::~GameLogInstance(void)
{
}


//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogLevelUp(Player* pPlayer
                                  ,DWORD dwServerCode
											 ,DWORD   dwOldExp
											 ,DWORD   dwNewExp
											 ,WORD    wOldLevel
											 ,WORD    wNewLevel)
{
	_GAMELOG_INIT(m_ExpData);

	m_ExpData.dwExpLogCode	= LOGEXP_LEVELUP;
	m_ExpData.dwServerCode	= dwServerCode;
	m_ExpData.dwPrevExp		= dwOldExp;
	m_ExpData.dwNewExp		= dwNewExp;
	m_ExpData.dwPrevLevel	= wOldLevel;
	m_ExpData.dwNewLevel		= wNewLevel;

	m_pLogWrite->LogExp( m_ExpData, m_bWriteText, NULL );
}


//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogExtraExp	(Player* pPlayer
                                    ,DWORD dwServerCode
												,DWORD   dwOldExp
												,DWORD   dwNewExp
												,WORD    wOldLevel
												,WORD    wNewLevel
												,WORD    wDivideCount)
{
	_GAMELOG_INIT(m_ExpData);

	m_ExpData.dwExpLogCode		= LOGEXP_DIVIDECNT;
	m_ExpData.dwServerCode		= dwServerCode;
	m_ExpData.dwLevelUpAmount	= wDivideCount;
	m_ExpData.dwPrevExp			= dwOldExp;
	m_ExpData.dwNewExp			= dwNewExp;
	m_ExpData.dwPrevLevel		= wOldLevel;
	m_ExpData.dwNewLevel			= wNewLevel;

	m_pLogWrite->LogExp( m_ExpData, m_bWriteText, NULL );
}


//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogCutExp(Player* pPlayer
                                ,DWORD dwServerCode
										  ,DWORD   dwTotalExp
										  ,DWORD   dwChangeExp
										  ,WORD    wLevel)
{
	_GAMELOG_INIT(m_ExpData);

	m_ExpData.dwExpLogCode = LOGEXP_DOWN;
	m_ExpData.dwServerCode = dwServerCode;
	m_ExpData.dwNewExp = dwTotalExp;
	m_ExpData.dwNewLevel = wLevel;

	m_pLogWrite->LogExp(m_ExpData
                      ,m_bWriteText
							 ,"Exp = %d"
							 ,dwChangeExp);
}


//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogItemDrop(Player*   pPlayer
                                  ,DWORD dwServerCode
											 ,BOOL      bPickUp
											 ,ItemSlot* pItemSlot)
{
	if(!pItemSlot )
		return;

	_GAMELOG_ITEM( ( bPickUp == TRUE ) ? LOGITEM_PICKUP : LOGITEM_CAST );

	_SetPlayerInfo( &m_ItemData, pPlayer );

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"%d,%d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant());
}

//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogEnchant(Player*                   pPlayer
                                      ,DWORD dwServerCode
												  ,BOOL                      bSuccess
												  ,ItemSlot*                 pItemSlot
												  ,vector<sLOG_COMPOSITE_MATERIAL> arMatInfos)
{
	_GAMELOG_ITEM(bSuccess?LOGITEM_ENCHANTSUCCESS : LOGITEM_ENCHANTFAIL);

	//////////////////////////////////////////
	StringHandle strInfo;
	StringHandle szText;
	int nSize = (int)arMatInfos.size();
	for(int i = 0; i < nSize; i++)
	{
		sLOG_COMPOSITE_MATERIAL& info = arMatInfos[i];

		szText.Format("MAT= %d,NUM= %d ",  info.nMaterialCode, info.wCntMaterial );
		strInfo += szText;
	}

	m_pLogWrite->LogItem( m_ItemData, m_bWriteText, strInfo );
}


//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogItemRepair(Player*   pPlayer
                                          ,DWORD dwServerCode
														,ItemSlot* pItemSlot
														,DWORD     dwMoney)
{
	_GAMELOG_ITEM(LOGITEM_REPAIR);

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ," %d ,%d ,Money = %d "
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant()
							  ,dwMoney);
}

void GameLogInstance::LogNPCTrade(Player*   pPlayer
                                         ,DWORD dwServerCode
													  ,BOOL      bSell
													  ,ItemSlot* pItemSlot
													  ,DWORD     dwMoney)
{
	_GAMELOG_ITEM(bSell?LOGITEM_SELLNPC : LOGITEM_BUYNPC);

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ," %d, %d, Money = %d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant()
							  ,dwMoney);
}

void GameLogInstance::LogPlayerTrade(Player*   pPlayer
                                            ,DWORD dwServerCode
														  ,BOOL      bGive
														  ,ItemSlot* pItemSlot
														  ,Player*   pPlayer2)
{
	_GAMELOG_ITEM(bGive?LOGITEM_GIVE : LOGITEM_RECEIVE);

	_SetOtherInfo( &m_ItemData, pPlayer2 );

	m_pLogWrite->LogItem(m_ItemData
                            ,m_bWriteText
									 ,"%d, %d"
									 ,pItemSlot->GetRank()
									 ,pItemSlot->GetEnchant());
}


void GameLogInstance::LogMoneyTrade(Player* pPlayer
                                             ,DWORD dwServerCode
															,BOOL    bGive
															,DWORD   dwMoney
															,Player* pPlayer2)
{
	_GAMELOG_ITEM0(bGive?LOGITEM_MONEY:LOGITEM_MONEY);

	_SetOtherInfo( &m_ItemData, pPlayer2 );

	m_pLogWrite->LogItem(m_ItemData
                              ,m_bWriteText
										,bGive?"Give Money  = %d":"Receive Money  = %d"
										,dwMoney);

}


void GameLogInstance::LogItemVendor(Player*   pPlayer
                                             ,DWORD dwServerCode
															,BOOL      bSell
															,ItemSlot* pItemSlot
															,DWORD     dwMoney
															,Player*   pPlayer2)
{
	_GAMELOG_ITEM(bSell?LOGITEM_SELLPC:LOGITEM_BUYPC);

	_SetOtherInfo( &m_ItemData, pPlayer2 ); 

	m_pLogWrite->LogItem(m_ItemData
                     ,m_bWriteText
							,bSell ? "%d, %d, RECEIVE MONEY = %d" : "%d, %d, PAY MONEY = %d"
							,pItemSlot->GetRank()
							,pItemSlot->GetEnchant()
							,dwMoney);
}


void GameLogInstance::LogItemRankUp(Player*   pPlayer
                                         ,DWORD dwServerCode
													  ,ItemSlot* pItemSlot
													  ,DWORD     dwMoney
													  ,BOOL      bCreateSocket)
{
	_GAMELOG_ITEM(LOGITEM_RANKUPSUCCESS);

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"%d, %d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant());
}


void GameLogInstance::LogItemWarehouse(Player*   pPlayer
                                            ,DWORD dwServerCode
														  ,BOOL      bKeep
														  ,ItemSlot* pItemSlot)
{
	_GAMELOG_ITEM(bKeep?LOGITEM_PUTSTORAGE:LOGITEM_GETSTORAGE);

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"%d, %d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant());
}



void GameLogInstance::LogEnchantOpr(Player*                         pPlayer
                                     ,DWORD dwServerCode
												 ,vector<sLOG_COMPOSITE_MATERIAL> arMatInfos)
{

	_GAMELOG_ITEM0(LOGITEM_ENCHANTTRY);

	//////////////////////////////////////////
	StringHandle strInfo;
	StringHandle szText;
	int nSize = (int)arMatInfos.size();
	for(int i = 0; i < nSize; i++)
	{
		sLOG_COMPOSITE_MATERIAL& info = arMatInfos[i];

		szText.Format("MAT= %d,NUM= %d ",  info.nMaterialCode, info.wCntMaterial );
		strInfo += szText;
	}

	m_pLogWrite->LogItem( m_ItemData, m_bWriteText, strInfo );
}


void GameLogInstance::LogMonsterDrop(DWORD dwServerCode
                                        ,ItemSlot* pItemSlot
													 ,DWORD     dwMonsterCode
													 ,DWORD     dwMonsterLevel
													 ,int       nMapCode
													,Vector3C&   vPos)
{
	if( !IsRun() )	
		return;

	m_ItemData.Init();

	m_ItemData.dwItemCode	= LOGITEM_MONSTER_ITEMDROP;
	m_ItemData.dwServerCode	= dwServerCode;

	_SetItemInfo( &m_ItemData, pItemSlot );

	m_ItemData.dwMapCode = nMapCode;
	m_ItemData.fMapX		= vPos.x;
	m_ItemData.fMapY		= vPos.y;
	m_ItemData.fMapZ		= vPos.z;

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"%d, %d, MONSTER CODE = %d, LEVEL = %d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant()
							  ,dwMonsterCode
							  ,dwMonsterLevel);
}

////////////////////////////////////////////////////////
void GameLogInstance::LogTradeStatus(Player* pPlayer
                                        ,DWORD dwServerCode
													 ,Player* pPlayer2
													 ,int     nType)
{
	INT nServerType;

	if( nType == 0 )
		nServerType = LOGITEM_REQUEST;
	else if( nType == 1 )
		nServerType = LOGITEM_ACCEPT;
	else if( nType == 2 )
		nServerType = LOGITEM_REJECT;
	else
		return;

	_GAMELOG_ITEM0(nServerType);


	_SetOtherInfo( &m_ItemData, pPlayer2 );
	m_pLogWrite->LogItem( m_ItemData, m_bWriteText, NULL );

}

void GameLogInstance::LogVendorStatus( Player* pPlayer, DWORD dwServerCode, BOOL bOpen )
{
	_GAMELOG_ITEM0(bOpen? LOGITEM_NEWPC: LOGITEM_DELPC);

	_SetPlayerInfo( &m_ItemData, pPlayer );
	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"Money = %d"
							  ,pPlayer->GetMoney());
}


void GameLogInstance::LogViewVendor(Player* pPlayer
                                       ,DWORD dwServerCode
													,Player* pPlayer2)
{
	_GAMELOG_ITEM0(LOGITEM_WATCHPC);

	_SetOtherInfo( &m_ItemData, pPlayer2 );
	m_pLogWrite->LogItem( m_ItemData, m_bWriteText, NULL );
}


void GameLogInstance::LogWarehouseInfo( Player* pPlayer, DWORD dwServerCode, ItemSlot* pItemSlot, DWORD dwMoney )
{
	_GAMELOG_ITEM(LOGITEM_WATCHPC);

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"%d,%d ,Money = %d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant()
							  ,dwMoney);
}


void GameLogInstance::LogVendorInfo( Player* pPlayer, DWORD dwServerCode, ItemSlot* pItemSlot, DWORD dwMoney )
{
	_GAMELOG_ITEM(LOGITEM_SETPRICE_PC);

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"%d, %d, money = %d"
							  ,pItemSlot->GetRank()
							  ,pItemSlot->GetEnchant()
							  ,dwMoney);
}


void GameLogInstance::LogItemRankUpOpr( Player* pPlayer, DWORD dwServerCode, SLOTPOS pos1, SLOTPOS pos2 )
{
	_GAMELOG_ITEM0(LOGITEM_RANKUPTRY);

	BaseContainer* pInventory;
	
	pInventory = (BaseContainer*)pPlayer->GetItemManager()->GetItemSlotContainer( SI_INVENTORY );
	ItemSlot& slotTarget  = (ItemSlot &)pInventory->GetSlot(pos1);
	ItemSlot& slotTarget2 = (ItemSlot &)pInventory->GetSlot(pos2);


	_SetItemInfo( &m_ItemData, &slotTarget );

	m_pLogWrite->LogItem(m_ItemData
                       ,m_bWriteText
							  ,"Second Item : %d, %d, %d, %d, %d"
							  ,slotTarget2.GetCode()
							  ,slotTarget2.GetSerial()
							  ,slotTarget2.GetNum()
							  ,slotTarget2.GetRank()
							  ,slotTarget2.GetEnchant());

}



//////////////////////////////////////////////////////////////////////////
void GameLogInstance::LogPlayerResurrection(Player* pPlayer
                                           ,DWORD dwServerCode
														 ,DWORD   dwExp
														 ,int     nMapCode
										 ,Vector3C&   vPos)
{
	_GAMELOG_ACTION(LOGACTION_REBIRTH_CHAR);

	m_ActionData.dwMapCode = nMapCode;
	m_ItemData.fMapX = vPos.x;
	m_ItemData.fMapY = vPos.y;
	m_ItemData.fMapZ = vPos.z;
	m_pLogWrite->LogUserAction(m_ActionData
                             ,m_bWriteText
									  ,NULL);
}

void GameLogInstance::LogPlayerDead(Player* pPlayer
                                   ,DWORD dwServerCode
											  ,DWORD   dwExp
											  ,int     nMapCode
										 ,Vector3C&   vPos)
{

	_GAMELOG_ACTION(LOGACTION_DEAD_CHAR);

	m_ActionData.dwMapCode = nMapCode;
	m_ItemData.fMapX = vPos.x;
	m_ItemData.fMapY = vPos.y;
	m_ItemData.fMapZ = vPos.z;

	m_pLogWrite->LogUserAction( m_ActionData, m_bWriteText, NULL );
}

void GameLogInstance::LogInvenInfoOnLogin( Player* pPlayer, DWORD dwServerCode )
{
	if( !IsRun() )		return;

	LogSnapShot( pPlayer, dwServerCode );
}


void GameLogInstance::LogUseStat( Player* pPlayer, DWORD dwServerCode, byte byStatType )
{
	_GAMELOG_ACTION(LOGACTION_USE_STAT);

	m_pLogWrite->LogUserAction(m_ActionData
                             ,m_bWriteText
									  ," STATCODE = %d"
									  ,byStatType);
}

void GameLogInstance::LogCharacter( Player* pPlayer, DWORD dwServerCode, BOOL bCreate )
{
	_GAMELOG_ACTION(bCreate?LOGACTION_CREATE_CHAR : LOGACTION_DELETE_CHAR);

	m_pLogWrite->LogUserAction(m_ActionData
                             ,m_bWriteText
									  ,NULL);
}

void GameLogInstance::LogMoveVillage( Player* pPlayer, DWORD dwServerCode, int nMapCode )
{
	_GAMELOG_ACTION(LOGACTION_MOVE_TOWN);

	m_ActionData.dwMapCode = nMapCode;
	m_pLogWrite->LogUserAction( m_ActionData, m_bWriteText, NULL );
}


void GameLogInstance::LogSkillLevelUp( Player* pPlayer, DWORD dwServerCode, int nSkillCode, BOOL bLevelUp )
{
	_GAMELOG_ACTION(bLevelUp? LOGACTION_SKILLLV_UP: LOGACTION_ACQUIRE_SKILL);

	m_pLogWrite->LogUserAction(m_ActionData
                             ,m_bWriteText
									  ,"SKILL CODE = %d"
									  ,nSkillCode);
}


void GameLogInstance::LogCharConnect( Player* pPlayer, DWORD dwServerCode )
{
	_GAMELOG_INIT(m_SessionData);

	m_SessionData.nConnectType = LOGCONN_CHAR_DISCONNECT;
	m_SessionData.dwServerCode = dwServerCode;

	m_pLogWrite->LogSession( m_SessionData, m_bWriteText, NULL );
}


void GameLogInstance::LogSnapShot( Player* pPlayer, DWORD dwServerCode )
{
	_GAMELOG_INIT(m_SnapShotData);

	int nSize = 0;

	////////////////////////////////////////////////////////////////////////
	sPLAYERINFO_BASE* pCharInfo = NULL;
	pCharInfo = pPlayer->GetCharInfo();
	if( pCharInfo )
	{
		nSize = sizeof(sPLAYERINFO_BASE);
		if( nSize <= sizeof(m_SnapShotData.arCharStats) )
			CopyMemory( m_SnapShotData.arCharStats, pCharInfo, nSize );	
	}

	////////////////////////////////////////////////////////////////////////
	nSize = sizeof( m_SnapShotData.arMissions );
	pPlayer->GetMissionManager()->SerializeStream(m_SnapShotData.arMissions
                                                ,nSize
																,SERIALIZE_LOAD);

	////////////////////////////////////////////////////////////////////////
	//Inventory
	nSize = sizeof(m_SnapShotData.arEquipItem);
	ITEMOPT_STREAM*		pEquipItemStrem = (ITEMOPT_STREAM*)m_SnapShotData.arEquipItem;
	ItemSlotContainer*	pEquipContainer = (ItemSlotContainer*)pPlayer->GetItemManager()->GetItemSlotContainer( SI_EQUIPMENT );
	pEquipContainer->SerializeItemStreamAll(pEquipItemStrem
                                          ,nSize
														,SERIALIZE_LOAD);

	////////////////////////////////////////////////////////////////////////
	nSize = sizeof(m_SnapShotData.arSkills);
	SKILL_STREAM* pSkillStrem = (SKILL_STREAM*)m_SnapShotData.arSkills;
	SkillSlotContainer* pSkillContainer = (SkillSlotContainer*)pPlayer->GetSkillManager()->GetSkillSlotContainer();
	pSkillContainer->SerializeSkillStreamAll(pSkillStrem
                                           ,nSize
														 ,SERIALIZE_LOAD);

	////////////////////////////////////////////////////////////////////////
	nSize = sizeof(m_SnapShotData.arInventory);
	ITEMOPT_STREAM* pItemStrem = (ITEMOPT_STREAM*)m_SnapShotData.arInventory;
	ItemSlotContainer* pItemContainer = (ItemSlotContainer*)pPlayer->GetItemManager()->GetItemSlotContainer( SI_INVENTORY );
	pItemContainer->SerializeItemStreamAll(pItemStrem
                                         ,nSize
													  ,SERIALIZE_LOAD);

	////////////////////////////////////////////////////////////////////////
	nSize = sizeof(m_SnapShotData.arTmpInventory);
	ITEMOPT_STREAM* pTempItemStrem = (ITEMOPT_STREAM*)m_SnapShotData.arTmpInventory;
	ItemSlotContainer* pTempItemContainer = (ItemSlotContainer*)pPlayer->GetItemManager()->GetItemSlotContainer( SI_INVENTORY2 );
	pTempItemContainer->SerializeItemStreamAll(pTempItemStrem
                                             ,nSize
															,SERIALIZE_LOAD);

	m_pLogWrite->LogCharSnap( m_SnapShotData, NULL );
}


////////////////////////////////////////////////////////////////////////
bool GameLogInstance::_SetItemInfo( ItemLogData* pData, ItemSlot* pItemSlot )  
{
	if( !pData || !pItemSlot )
		return false;

	pData->dwItemInfoCode	= pItemSlot->GetCode();
	pData->dwItemSerial		= pItemSlot->GetSerial();
	pData->dwItemAmount		= pItemSlot->GetNum();
	return true;
}

////////////////////////////////////////////////////////////////////////
bool GameLogInstance::_SetPlayerInfo( ItemLogData* pData, Player* pPlayer)
{
	if( !pData || !pPlayer )
		return false;

	Vector3D vecPos;
	pPlayer->GetPos(vecPos );

	pData->dwMapCode = pPlayer->GetField()->GetMap()->GetMapCode();
	pData->fMapX = vecPos.x;
	pData->fMapY = vecPos.y;
	pData->fMapZ = vecPos.z;

	return true;
}

	////////////////////////////////////////////////////////////////////////
void GameLogInstance::_SetOtherInfo( ItemLogData* pData, Player* pPlayer )
{
	if( pPlayer->GetUserID() == NULL )	
		lstrcpyn( pData->szToUserID, "", MAX_ID_LENGTH);
	else					
		lstrcpyn( pData->szToUserID, pPlayer->GetUserID(), MAX_ID_LENGTH );


	if( pPlayer->GetCharName() == NULL )
		lstrcpyn( pData->szToCharName, "", MAX_CHARNAME_LENGTH );
	else 				
		lstrcpyn( pData->szToCharName, pPlayer->GetCharName(), MAX_CHARNAME_LENGTH );
}






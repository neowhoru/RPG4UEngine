/*////////////////////////////////////////////////////////////////////////
文 件 名：MapNpc.cpp
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "MapNPC.h"
#include "Field.h"
#include "Map.h"
#include "BaseZone.h"
#include "BaseRoom.h"
#include "AIParamParser.h"
#include "AIStateManager.h"
#include "MapNPCMovement.h"
#include "IQuestCharacter.h"
#include "NPCInfoParser.h"
#include "QuestDefine.h"
#include "QuestManager.h"
#include "Player.h"

MapNPC::MapNPC()
{
	SetObjectType( MAPNPC_OBJECT );
	m_pMapObjectInfo	= NULL;
	m_pFuncInfo			= NULL;
	m_pMovement			= NULL;
}

MapNPC::~MapNPC()
{
	SAFE_DELETE(m_pMovement);
}

BOOL MapNPC::Init()
{	
	return _SUPER::Init();
}

VOID MapNPC::Release()
{
	_SUPER::Release();
}




BOOL MapNPC::AddRelateQuestInfo( sQUEST_RELATEINFO* pInfo )
{
	if(m_pQuestChar)
		return m_pQuestChar->AddRelateQuestInfo(pInfo );
	return FALSE;
}


sQUEST_RELATEINFO* MapNPC::GetRelateQuestInfo( int n )
{
	if(m_pQuestChar)
		return m_pQuestChar->GetRelateQuestInfo(n );
	return NULL;
}


UINT MapNPC::GetRelateQuestInfoCount()
{
	if(m_pQuestChar)
		return m_pQuestChar->GetRelateQuestInfoCount( );
	return 0;
}


void MapNPC::GetAroundQuestsTo(Player* pPlayer)
{
	if(!m_pQuestChar || !pPlayer)
		return;

	INT						nNum;
	sQUEST_RELATEINFO*	pInfo;
	sQUEST_INFO*			pQuest;

	nNum = m_pQuestChar->GetRelateQuestInfoCount();
	for(int n=0; n<nNum; n++)
	{
		pInfo = m_pQuestChar->GetRelateQuestInfo(n);
		if(!pInfo)
			continue;
		pPlayer->PushAroundQuest(pInfo->m_QuestID);

		////////////////////////////////////
		//检测依赖任务
		pQuest = theQuestManager.LoadQuest(pInfo->m_QuestID);
		if(!pQuest)
			continue;
		for(INT m=0; m<QUEST_MAX; m++)
		{
			if(pQuest->m_QuestLimits[m] == -1)
				continue;
			pPlayer->PushAroundQuest(pQuest->m_QuestLimits[m]);
		}
	}

}

VOID MapNPC::Update( DWORD dwTick )
{
	///////////////////////////////////////
	AIProgress();

	///////////////////////////////////////
	//if( m_pAggroTimer->IsExpired() )
	//{
	/////////////////////////////////////////
	//	RegisterBattleRecords();

	//	if( GetTargetChar() != NULL )
	//	{
	//		SelectBestTarget();
	//	}
	//}

	///////////////////////////////////////
	m_pAIStateMan->Update( dwTick );

	//m_pBattleRecord->Update( dwTick );

	_SUPER::Update( dwTick );
}


VOID MapNPC::GetRenderInfo( sMONSTERINFO_RENDER * OUT pRenderInfo )
{
	_SUPER::GetRenderInfo(pRenderInfo);
	//pRenderInfo->m_byType = (BYTE)m_pFuncInfo->m_eNPCTYPE;
	pRenderInfo->m_dwNpcFuncCode	= (MONSTERCODE)m_pFuncInfo->m_ExtraCode;
}

VOID MapNPC::InitAIStates( DWORD dwStateID, LPARAM param1 )
{
	///////////////////////////////////////
	if(m_pMovement == NULL)
	{
		_SUPER::InitAIStates(  dwStateID,  param1 );

		if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_ETERNAL_STOP )
		{
			m_pAIStateMan->RelinkAIState( AISTATE_ATTACK,		AISTATE_STOP_IDLE );
		}
		//归家型
		else if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_BACK_SPAWN )
		{
			m_pAIStateMan->RelinkAIState( AISTATE_ATTACK,		AISTATE_SPAWN_IDLE );
		}
		// 巡逻型
		else if( m_pBaseInfo->m_wMoveAttitude == eMOVE_ATTITUDE_PATROL )
		{
			m_pAIStateMan->RelinkAIState( AISTATE_ATTACK,		AISTATE_PATROL );
		}
		// 否则，默认处理
		else
		{
			m_pAIStateMan->RelinkAIState( AISTATE_ATTACK,		AISTATE_IDLE );
		}
		return;
	}

	/////////////////////////////////////////////////
	m_pAIStateMan->Release();

	static DWORD	arStates[AISTATE_MAX]=
	{
		 AISTATE_UNKNOWN

		,AISTATE_MAPNPC_WANDER	//,AISTATE_WANDER
		,AISTATE_IDLE				//,AISTATE_IDLE
		,AISTATE_MAPNPC_WANDER	//,AISTATE_TRACK
		,AISTATE_IDLE				//,AISTATE_ATTACK
		,AISTATE_IDLE				//,AISTATE_HELP
		
		,0//,AISTATE_THRUST
		,AISTATE_IDLE//,AISTATE_DEAD
		,0//,AISTATE_FLYING
		,AISTATE_KNOCKDOWN
		,0//,AISTATE_JUMP
		
		,0//,AISTATE_FALL_APART
		,0//,AISTATE_RETURN
		,0//,AISTATE_RETREAT
		,0//,AISTATE_RUNAWAY
		,AISTATE_CHAOS

		,0,0,0,0,0,0
	};
	LinkAIStates(arStates);


	m_pAIStateMan->Init();

}

VOID MapNPC::OnEnterField( Field* pField, Vector3D* pvPos ) 
{
	//Init();

	_SUPER::OnEnterField(pField, pvPos);

	//BOOL bAdded = pField->AddNPC( this );
}

VOID MapNPC::OnLeaveField()
{
	_SUPER::OnLeaveField();
}

Character* MapNPC::SearchTarget()
{
	return NULL;
}


BOOL MapNPC::OnDead()
{
	// DistributeExp();

	return _SUPER::OnDead();
}



BOOL MapNPC::IsFriend( CharacterCookie * /*pTarget*/ )
{
	return TRUE;
}

VOID MapNPC::StopMoving(BOOL bSendToPlayers,WORD* pwAngle)
{
	_SUPER::StopMoving(bSendToPlayers,pwAngle);
	if(m_pMovement)
		m_pMovement->StopMove();
}

BOOL MapNPC::SetFuncInfo(sNPCINFO_FUNC& funInfo)
{
	m_pFuncInfo = &funInfo;

	/////////////////////////////////////////
	if(m_pFuncInfo->m_iMoveType != 0 && m_pMovement == NULL)
	{
		m_pMovement		= new	MapNPCMovement(this);
		m_pMovement->InitMoveInfo();
	}

	if(m_pMovement)
		m_pMovement->SetFuncInfo(m_pFuncInfo);


	/////////////////////////////////////////
	KeepQuestScript(0,QUESTCHARACTER_NPC);

	/////////////////////////////////////////
	if(m_pQuestChar)
	{
		NPC_QUESTINFO*		pQuestInfo;

		pQuestInfo	= theNPCInfoParser.GetQuestInfo(funInfo.m_NPCCODE);

		m_pQuestChar->AddQuestScript	(funInfo.m_ServerScriptCode);
		m_pQuestChar->InitQuestInfo	(pQuestInfo);
	}

	return TRUE;
}


void MapNPC::OnNextRoutePos()
{
}






























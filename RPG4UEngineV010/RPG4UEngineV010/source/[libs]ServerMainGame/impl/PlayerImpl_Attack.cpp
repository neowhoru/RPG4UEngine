#include "stdafx.h"
#include "PlayerImpl.h"
#include "float.h"
#include "MathHelper.h"
#include "SkillInfoParser.h"
#include "PVPResult.h"


BOOL PlayerImpl::CanAttack() const
{
	if(	m_pPVPResult->GetPVPState() != PVPSTATE_NONE 
		&& m_pPVPResult->GetPVPState() != PVPSTATE_MATCH )
		return FALSE;

	return _SUPER::CanAttack();
}

BOOL PlayerImpl::CanBeAttacked() const
{
	if(	m_pPVPResult->GetPVPState() != PVPSTATE_NONE 
		&& m_pPVPResult->GetPVPState() != PVPSTATE_MATCH )
		return FALSE;

	return _SUPER::CanBeAttacked();
}

eBATTLE_RESULT PlayerImpl::CanNormalAttack(BYTE     byAttackType
                                              ,VECTOR3D &vCurPos
															 ,VECTOR3D &vDestPos)
{
	/////////////////////////////////////
	if( byAttackType == ATTACK_SEQUENCE_SECOND )
	{
		if( GetLastAttackSeq() != ATTACK_SEQUENCE_FIRST )
		{
			LOGMSG( LOG_FULL,  "[CanNormalAttack] Attack Seq error ObjectKey[%d]", GetObjectKey() );
		}
	}
	else if( byAttackType == ATTACK_SEQUENCE_THIRD )
	{
		if( GetLastAttackSeq() != ATTACK_SEQUENCE_SECOND )
		{
			LOGMSG( LOG_FULL,  "[CanNormalAttack] Attack Seq error ObjectKey[%d] ", GetObjectKey() );
		}
	}

	/////////////////////////////////////
	SetLastAttackSeq( byAttackType );

	Field *pField = GetField();
	if( !pField )
		return RC_BATTLE_PLAYER_NOTEXIST_TO_FIELD;


	///////////////////////////////////////


	///////////////////////////////////////
	if( !CanAttack() )	
		return RC_BATTLE_PLAYER_STATE_WHERE_CANNOT_ATTACK_ENEMY;

	///////////////////////////////////////
	if( ( vCurPos- vDestPos ).LengthSquared() > 9.0f )
		return RC_BATTLE_THRUST_DIST_OVER;

	///////////////////////////////////////
	if( IsDoingAction() )
		return RC_BATTLE_ALREADY_DOING_ACTION;

	return RC_BATTLE_SUCCESS;
}


eBATTLE_RESULT PlayerImpl::CanStyleAttack(BYTE     byAttackType
                                         ,SLOTCODE StyleCode
													  ,VECTOR3D &vCurPos
													  ,VECTOR3D &vDestPos)
{
	///////////////////////////////////////
	eBATTLE_RESULT		rcResult;
	sSTYLEINFO_BASE *	pStyleInfo;
	
	rcResult = CanNormalAttack( byAttackType, vCurPos, vDestPos );
	if( rcResult != RC_BATTLE_SUCCESS )
		return rcResult;

	///////////////////////////////////////
	if( GetSelectedStyle() != StyleCode )
		return RC_BATTLE_STYLECODE_WHERE_DONOT_SELECT;

	
	pStyleInfo = theSkillInfoParser.GetStyleInfo( StyleCode );
	if( !pStyleInfo )	
		return RC_BATTLE_BASEINFO_NOTEXIST;	

	///////////////////////////////////////
	if( pStyleInfo->m_dwClassDefine != -1 && GetCharType() != pStyleInfo->m_dwClassDefine )
		return RC_BATTLE_CHAR_CLASS_LIMIT;

	///////////////////////////////////////
	if(pStyleInfo->m_WeaponDefines != -1 && GetWeaponKind() != pStyleInfo->m_WeaponDefines )
		return RC_BATTLE_WEAPON_LIMIT;

	return RC_BATTLE_SUCCESS;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
VOID PlayerImpl::SetAttackDelay( BYTE byAttackType, SLOTCODE StyleCode )
{
	sSTYLEINFO_BASE *pStyleInfo;
	
	pStyleInfo = theSkillInfoParser.GetStyleInfo( StyleCode );

	DWORD dwActionDelay;
	if(byAttackType >= ATTACK_SEQUENCE_MAX)
	{
		LOGMSG	(LOG_CRITICAL
					,"[PlayerImpl::SetAttackDelay] Invalid attack sequence! Style[%d] byAttackType[%d]"
					,StyleCode
					,byAttackType);
		ForceDisconnect( DISCONNECT_INVALID_ATTACK_SEQUENCE );
		return;
	}
	dwActionDelay = pStyleInfo->m_dwAttackTime[byAttackType];



	dwActionDelay = (DWORD)(dwActionDelay * 0.9f);

	if( 0 != GetPhysicalAttackSpeed() )
	{
		SetActionDelay( (DWORD)( dwActionDelay / GetPhysicalAttackSpeed() ) );
	}
}
















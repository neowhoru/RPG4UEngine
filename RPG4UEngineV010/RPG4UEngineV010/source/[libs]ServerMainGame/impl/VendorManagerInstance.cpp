#include "StdAfx.h"
#include "VendorManagerInstance.h"
#include "IVendorContainer.h"
#include "Player.h"
#include "IVendorSlot.h"
#include "ItemManager.h"
#include "ItemSlotContainer.h"
#include "ServerShell.h"
#include "SlotSystem.h"
#include "SlotHandle.h"

using namespace RC;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(VendorManagerInstance, ()  , gamemain::eInstPrioGameFunc);



VendorManagerInstance::VendorManagerInstance(void)
{

}

VendorManagerInstance::~VendorManagerInstance(void)
{
}

IVendorContainer *	VendorManagerInstance::_Alloc	()
{
	return (IVendorContainer*)theSlotSystem.CreateBaseContainer(SLOTT_VENDOR);
}

VOID VendorManagerInstance::_Free	( IVendorContainer * pUnit )
{
	theSlotSystem.DestroyBaseContainer(pUnit);
}


BOOL VendorManagerInstance::CheckEstablisher( Player * pPlayer )
{
	__CHECK_PTR(pPlayer->GetField());

	__CHECK(pPlayer->GetBehave() == PLAYER_BEHAVE_VENDOR_ESTABLISHER);

	return TRUE;
}

BOOL VendorManagerInstance::CheckObserver( Player * pPlayer )
{
	__CHECK_PTR(pPlayer->GetField());

	__CHECK(pPlayer->GetBehave() == PLAYER_BEHAVE_VENDOR_OBSERVER);

	return TRUE;
}


eVENDOR_RESULT VendorManagerInstance::StartVendor(Player *                           pEstablishedPlayer
                                             ,LPCTSTR                            pTitle
															,sTOTALINFO_ESTABLISHER_VENDOR & IN TotalInfo)
{
	__CHECK2(pEstablishedPlayer->GetBehave() == PLAYER_BEHAVE_IDLE ,_RC_VEN(INVALID_STATE));
	__CHECK2(pEstablishedPlayer->GetField() != NULL	,_RC_VEN(INVALID_STATE));
	

	////////////////////////////////////////////
	IVendorContainer * pContainer = _Alloc();
	__CHECK2_PTR(pContainer, _RC_VEN(FAILED));


	////////////////////////////////////////////
	if( !pContainer->CreateVendor( pEstablishedPlayer, pTitle, TotalInfo ) )
	{
		_Free( pContainer );

		return _RC_VEN(FAILED);
	}


	pEstablishedPlayer->SetVendorContainer	(pContainer);
	pEstablishedPlayer->SetBehave				(PLAYER_BEHAVE_VENDOR_ESTABLISHER);

	return _RC_VEN(SUCCESS);
}


eVENDOR_RESULT VendorManagerInstance::EndVendor( Player * pEstablishedPlayer )
{
	__CHECK2	(pEstablishedPlayer->GetBehave() == PLAYER_BEHAVE_VENDOR_ESTABLISHER
				,_RC_VEN(INVALID_STATE));


	IVendorContainer * pContainer = pEstablishedPlayer->GetVendorContainer();

	__CHECK2_PTR(pContainer, _RC_VEN(INVALID_STATE));

	pContainer->DestroyVendor();
	_Free( pContainer );

	//////////////////////////////////////
	pEstablishedPlayer->SetVendorContainer(NULL);
	pEstablishedPlayer->SetBehave(PLAYER_BEHAVE_IDLE);

	return _RC_VEN(SUCCESS);
}



eVENDOR_RESULT VendorManagerInstance::Modify(Player *                  pEstablishedPlayer
                                            ,sVENDOR_ITEM_SLOT & IN    slotInfo
														  ,sVENDOR_ITEM_SLOTEX & OUT retInfo)
{
	__CHECK2	(CheckEstablisher(pEstablishedPlayer)
				,_RC_VEN(INVALID_STATE));

	IVendorContainer * pContainer = pEstablishedPlayer->GetVendorContainer();
	__CHECK2_PTR(pContainer, _RC_VEN(INVALID_STATE));

	/////////////////////////////////////////////
	__CHECK2	(pContainer->ValidPos(slotInfo.m_VendorPos)
				,_RC_VEN(INVALID_VENDORSLOT));

	if( !pContainer->ModifySlot( slotInfo.m_VendorPos, slotInfo ) )
	{
		return _RC_VEN(INVALID_VENDORSLOT);
	}

	IVendorSlot & rSlot = (IVendorSlot &)pContainer->GetSlot(slotInfo.m_VendorPos);
	rSlot.CopyOut( retInfo );

	return _RC_VEN(SUCCESS);
}


eVENDOR_RESULT VendorManagerInstance::Insert(Player *              pEstablishedPlayer
                                        ,sVENDOR_ITEM_SLOT & IN    slotInfo
													 ,sVENDOR_ITEM_SLOTEX & OUT retInfo)
{
	__CHECK2	(CheckEstablisher(pEstablishedPlayer)
				,_RC_VEN(INVALID_STATE));

	IVendorContainer * pContainer = pEstablishedPlayer->GetVendorContainer();
	__CHECK2_PTR(pContainer, _RC_VEN(INVALID_STATE));

	////////////////////////////////////////////////////
	__CHECK2	(pContainer->ValidPos(slotInfo.m_VendorPos) 
				,_RC_VEN(INVALID_VENDORSLOT));
	__CHECK2	(pContainer->IsEmpty(slotInfo.m_VendorPos)
				,_RC_VEN(INVALID_VENDORSLOT));


	if( !pContainer->ModifySlot( slotInfo.m_VendorPos, slotInfo ) )
	{
		return _RC_VEN(INVALID_VENDORSLOT);
	}

	IVendorSlot & rSlot = (IVendorSlot &)pContainer->GetSlot(slotInfo.m_VendorPos);
	rSlot.CopyOut( retInfo );

	return _RC_VEN(SUCCESS);
}


eVENDOR_RESULT VendorManagerInstance::Delete(Player * pEstablishedPlayer
                                            ,SLOTPOS  posVendor)
{
	__CHECK2	(CheckEstablisher(pEstablishedPlayer)
				,_RC_VEN(INVALID_STATE));


	IVendorContainer * pContainer = pEstablishedPlayer->GetVendorContainer();
	__CHECK2_PTR(pContainer, _RC_VEN(INVALID_STATE));

	//////////////////////////////////////
	__CHECK2	(pContainer->ValidPos(posVendor) 
				,_RC_VEN(INVALID_VENDORSLOT));
	__CHECK2	(!pContainer->IsEmpty(posVendor)
				,_RC_VEN(INVALID_VENDORSLOT));


	pContainer->DeleteSlot( posVendor, NULL );

	return _RC_VEN(SUCCESS);
}


eVENDOR_RESULT VendorManagerInstance::Rename(Player * pEstablishedPlayer
                                            ,LPCTSTR  szVendorTitle)
{
	__CHECK2	(CheckEstablisher(pEstablishedPlayer)
				,_RC_VEN(INVALID_STATE));


	IVendorContainer * pContainer = pEstablishedPlayer->GetVendorContainer();
	__CHECK2_PTR(pContainer, _RC_VEN(INVALID_STATE));

	pContainer->SetTitle(szVendorTitle);

	return _RC_VEN(SUCCESS);
}







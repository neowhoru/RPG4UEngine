/*////////////////////////////////////////////////////////////////////////
文 件 名：SummonManagerInstance.cpp
创建日期：2009年6月9日
最后更新：2009年6月9日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "SummonManagerInstance.h"
#include "SummonedRelation.h"
#include "SkillInfoParser.h"
#include "Summoned.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(SummonManagerInstance, ()  , gamemain::eInstPrioGameFunc);


SummonManagerInstance::SummonManagerInstance()
{
	m_pRelationPool	= new TMemoryPoolFactory<SummonedRelation>;
	m_pRelations		= new util::THashTable<SummonedRelation *, DWORD>;
}

SummonManagerInstance::~SummonManagerInstance()
{
	SAFE_DELETE( m_pRelationPool );
	SAFE_DELETE( m_pRelations );
}

VOID SummonManagerInstance::Init( DWORD dwMaxPoolSize )
{
	m_pRelationPool->Initialize( dwMaxPoolSize, dwMaxPoolSize/2+1 );
	m_pRelations	->Initialize( dwMaxPoolSize );
}

VOID SummonManagerInstance::Release()
{
	if( m_pRelationPool )
	{
		m_pRelationPool->Release();
	}
}


SummonedRelation* SummonManagerInstance::AllocRelation(Character* pSummoner
                                                      ,BOOL       bFromPlayer)
{
	SummonedRelation *pRelation = FindRelation( pSummoner->GetObjectKey() );

	////////////////////////////////////////////////
	if( pRelation )
	{
		if(!bFromPlayer )
			return pRelation;

		FreeRelation( pRelation );
	}

	////////////////////////////////////////////////
	pRelation = (SummonedRelation*)m_pRelationPool->Alloc();

	if( !pRelation )
		return NULL;

	////////////////////////////////////////////////
	pRelation->Init( pSummoner, SUMMON_COMMAND_DELEGATE_ATTACK );

	////////////////////////////////////////////////
	if( m_pRelations->Add( pRelation, pSummoner->GetObjectKey() ) )
		return pRelation;

	////////////////////////////////////////////////
	m_pRelationPool->Free( pRelation );
	return NULL;
}

BOOL SummonManagerInstance::FreeRelation( SummonedRelation *pRelation )
{
	if( NULL == FindRelation( pRelation->GetSummonerKey() ) )
		return FALSE;

	m_pRelations	->Remove( pRelation->GetSummonerKey() );
	m_pRelationPool->Free( pRelation );

	pRelation->Release();

	return TRUE;
}


VOID SummonManagerInstance::AddSummonedSkill(DWORD    dwSummonKey
                                            ,SLOTCODE skillCode)
{
	SummonedRelation *pRelation;
	SkillDetailInfo *	pSkillInfo;
	NPC *					pNPC;
	
	pRelation = FindRelation( dwSummonKey );
	if( !pRelation )
		return;

	//////////////////////////////////////////////
	pSkillInfo = theSkillInfoParser.GetSkillInfo( skillCode );
	if( pSkillInfo->m_bySkillUserType != SKILLUSER_SUMMONED )	
		return;

	//////////////////////////////////////////////
	for( SummonedNpcTableIt it = pRelation->Begin(); it != pRelation->End(); ++it )
	{
		pNPC = *it;
		if( ! pNPC->IsSummoned() )
			continue;

		Summoned *pSummoned = (Summoned*)pNPC;
		pSummoned->AddSkill( skillCode );
	}
}

VOID SummonManagerInstance::DelSummonedSkill( DWORD dwSummonKey, SLOTCODE skillCode )
{
	SummonedRelation *pRelation;
	SkillDetailInfo *	pSkillInfo;
	NPC *					pNPC;
	
	////////////////////////////////
	pRelation = FindRelation( dwSummonKey );
	if( !pRelation )
		return;

	////////////////////////////////
	pSkillInfo = theSkillInfoParser.GetSkillInfo( skillCode );
	if( pSkillInfo->m_bySkillUserType != SKILLUSER_SUMMONED )
		return;

	////////////////////////////////
	SummonedNpcTableIt it;

	for(it = pRelation->Begin(); it != pRelation->End(); ++it )
	{
		pNPC = *it;
		if( ! pNPC->IsSummoned() )
			continue;

		Summoned *pSummoned = (Summoned*)pNPC;
		pSummoned->DelSkill( skillCode );
	}
}

eSUMMON_RESULT SummonManagerInstance::RunCommand(DWORD           dwSummonKey
                                                ,eSUMMON_COMMAND eCommand
																,DWORD           dwObjectKey
																,DWORD           dwTargetKey)
{
	SummonedRelation *pRelation;
	
	//////////////////////////////////////
	pRelation = FindRelation( dwSummonKey );
	if( !pRelation )
		return RC_SUMMON_SUMMONED_NPC_NOTEXIST;

	//////////////////////////////////////
	if( eCommand == SUMMON_COMMAND_DESTROY )
	{
		pRelation->Remove( dwObjectKey );
		return RC::RC_SUMMON_SUCCESS;
	}

	return pRelation->RunCommand(eCommand
                               ,dwObjectKey
										 ,dwTargetKey);
}

//////////////////////////////////////
VOID SummonManagerInstance::CancelSummonRelation( Character *pCharCancel )
{
	SummonedRelation *pRelation;
	
	//////////////////////////////////////
	pRelation = FindRelation( pCharCancel->GetSummonerKey() );
	if( !pRelation )	
		return;

	//////////////////////////////////////
	DWORD dwObjectKey = pCharCancel->GetObjectKey();

	//////////////////////////////////////
	if( pCharCancel->IsPlayer() )
	{
		FreeRelation( pRelation );
		return;
	}


	//////////////////////////////////////
	NPC *pNPCCancel = (NPC*)pCharCancel;

	if( !pRelation->IsSummoner( dwObjectKey ) )
	{
		pRelation->DecreaseSummoned	(pNPCCancel->GetBaseInfo()->m_MonsterCode
                                    ,dwObjectKey)			;
	}
	else
	{
		NPC *pNPC = NULL;
		for( SummonedNpcTableIt it = pRelation->Begin(); it != pRelation->End(); ++it )
		{
			pNPC = *it;
			pNPC->SetSummonerKey(0);
		}
		pRelation->SetSummoner( NULL );
	}


	//////////////////////////////////////
	if( !pRelation->GetTotalAmount() )
	{
		FreeRelation( pRelation );
	}

	pNPCCancel->SetSummonerKey(0);
}











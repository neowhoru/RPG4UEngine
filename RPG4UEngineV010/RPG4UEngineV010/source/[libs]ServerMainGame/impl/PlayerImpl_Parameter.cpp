/*////////////////////////////////////////////////////////////////////////
文 件 名：PlayerImpl_Parameter.cpp
创建日期：2009年6月10日
最后更新：2009年6月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "PlayerImpl.h"
#include "ItemManager.h"
#include "SKillManager.h"
#include <SkillSlotContainer.h>
#include <PacketStruct_DBP_Game.h>
#include <PacketStruct_Gate_Game.h>
#include <BaseContainer.h>
#include <ItemInfoParser.h>
#include "FormularManager.h"
#include "Party.h"
#include "PartyManager.h"
#include "MissionManager.h"
#include "QuestManager.h"
#include "QuickManager.h"
#include "IScriptCharacter.h"
#include "QuickStyleManager.h"
#include <ItemSlotContainer.h>
#include <QuickSlotContainer.h>
#include <StyleQuickSlotContainer.h>
#include "IPlayerWarehouse.h"
#include "GMInfoParser.h"
#include "GameShell.h"
#include "Map.h"
#include "StatusManager.h"
#include "GuildManager.h"
#include "Guild.h"
#include "SkillDefaultParser.h"
#include "FormularManager.h"
#include "IVendorContainer.h"
#include "DropManager.h"
#include "ScriptManager.h"


VOID PlayerImpl::OnSetCharInfo()
{	
	/////////////////////////////////////
	// HP, MP
	sPLAYERINFO_BASE *	pInfo			= PlayerCookie::GetCharInfo();
	int						iRecoverHP;
	int						iRecoverMP;

	/////////////////////////////////////
	if(pInfo->m_wSelectStyleCode == 0)
		pInfo->m_wSelectStyleCode = theSkillDefaultParser.GetSkill((ePLAYER_TYPE)pInfo->m_byClassCode);

	/////////////////////////////////////
	iRecoverHP = theBattleFormular.CalcHPRecover(GetCharType()
                             ,pInfo->m_sVitality
									  ,GetStatusManager()->GetCondition()
									  ,pInfo->m_LV);
	iRecoverMP = theBattleFormular.CalcMPRecover(GetCharType()
                             ,pInfo->m_sSpirit
									  ,GetStatusManager()->GetCondition());


	m_Attr.SetCharInfo( pInfo, iRecoverHP, iRecoverMP );
	m_Attr.Update();

	/////////////////////////////////////
	// HP/MP max check
	SetHP( GetHP() );
	SetMP( GetMP() );



	/////////////////////////////////////
	ReadyNextExp();	

	/////////////////////////////////////
	AutoFitLevel();
}


VOID PlayerImpl::GetRenderInfo( sPLAYERINFO_RENDER * OUT pRenderInfo )
{
	Vector3D					vPos = GetPosition();
	sPLAYERINFO_BASE *	pInfo = PlayerCookie::GetCharInfo();
	sGM_INFO *				pGMInfo;
	sSTATE_INFO *			pStateInfo;

	pGMInfo								= theGMInfoParser.FindGM( GetUserID() );

	pRenderInfo->m_wPlayerKey		= (WORD)GetObjectKey();
	pRenderInfo->m_byClass			= pInfo->m_byClassCode;
	pRenderInfo->m_LV					= pInfo->m_LV;
	pRenderInfo->m_HP					= (HPTYPE)GetHP();
	pRenderInfo->m_MaxHP				= (HPTYPE)GetMaxHP();
	pRenderInfo->m_fPos[0]				= vPos.x;
	pRenderInfo->m_fPos[1]				= vPos.y;
	pRenderInfo->m_fPos[2]				= vPos.z;
	pRenderInfo->m_wAttSpeedRatio		= GetAttr().GetAttSpeedRatio();
	pRenderInfo->m_wMoveSpeedRatio	= GetAttr().GetMoveSpeedRatio();
	pRenderInfo->m_BehaveState			= GetBehave();
	pRenderInfo->m_SelectStyleCode	= pInfo->m_wSelectStyleCode;
	lstrcpyn(pRenderInfo->m_szName, pInfo->m_szCharName, MAX_CHARNAME_LENGTH);

	pRenderInfo->m_byHair				= pInfo->m_byHair			;	// 
	pRenderInfo->m_byHairColor			= pInfo->m_byHairColor	;	// 
	pRenderInfo->m_bySex					= pInfo->m_bySex;				// 
	pRenderInfo->m_byHeight				= pInfo->m_byHeight;			// 
	pRenderInfo->m_byFace				= pInfo->m_byFace;			// 

	pRenderInfo->m_byHelmetOption		= GetHelmetOption();
	pRenderInfo->m_byGMGrade			= pGMInfo ? pGMInfo->m_byGrade : 0;
	pRenderInfo->m_byCondition			= GetStatusManager()->GetCondition();

	///////////////////////////////
	pRenderInfo->m_byPCBangUser		= GetReservedValue() > 0 ? 1 : 0;

	///////////////////////////////
	
	pStateInfo					= (sSTATE_INFO*)( (BYTE*)pRenderInfo + sizeof(sPLAYERINFO_RENDER) );
	pRenderInfo->m_byCount	= GetStatusManager()->GetStatusInfos( pStateInfo );
}


VOID PlayerImpl::GetVillageInfo( sPLAYERINFO_VILLAGE * OUT pVillageInfo )
{
	Vector3D					vPos		= GetPosition();
	sPLAYERINFO_BASE *	pInfo		= PlayerCookie::GetCharInfo();
	sGM_INFO *				pGMInfo;

	pGMInfo									= theGMInfoParser.FindGM( GetUserID() );

	pVillageInfo->m_wPlayerKey			= (WORD)GetObjectKey();
	lstrcpyn(pVillageInfo->m_szName, pInfo->m_szCharName, MAX_CHARNAME_LENGTH);
	pVillageInfo->m_fPos[0]				= vPos.x;
	pVillageInfo->m_fPos[1]				= vPos.y;
	pVillageInfo->m_fPos[2]				= vPos.z;
	pVillageInfo->m_wMoveSpeedRatio	= GetAttr().GetMoveSpeedRatio();
	pVillageInfo->m_byHair				= pInfo->m_byHair;	
	pVillageInfo->m_byHairColor		= pInfo->m_byHairColor;
	pVillageInfo->m_bySex				= pInfo->m_bySex;	
	pVillageInfo->m_byHeight			= pInfo->m_byHeight;
	pVillageInfo->m_byFace				= pInfo->m_byFace;

	pVillageInfo->m_byGMGrade			= pGMInfo ? pGMInfo->m_byGrade : 0;

	///////////////////////////////
	pVillageInfo->m_byPCBangUser = GetReservedValue() > 0 ? 1 : 0;

	///////////////////////////////
	pVillageInfo->m_byCondition = GetStatusManager()->GetCondition();

	pVillageInfo->m_byClass				= pInfo->m_byClassCode;
	pVillageInfo->m_BehaveState		= GetBehave();
}

VOID PlayerImpl::GetGuildRenderInfo( sGUILDINFO_RENDER * OUT pRenderInfo )
{
	sPLAYERINFO_BASE * pInfo = PlayerCookie::GetCharInfo();
	if( 0 != pInfo->m_GuildGuid )
	{
		pRenderInfo->m_byExistGuild = 1;
		pRenderInfo->m_GuildMarkIdx = 0;
		lstrcpyn( pRenderInfo->m_szGuildName, pInfo->m_szGuildName, MAX_GUILDNAME_LENGTH );
	}
	else
	{
		pRenderInfo->m_byExistGuild = 0;
	}	
}

VOID PlayerImpl::GetVendorRenderInfo	( sVENDORINFO_RENDER * OUT pRenderInfo )
{
	if(	GetBehave() == PLAYER_BEHAVE_VENDOR_ESTABLISHER 
		&& GetVendorContainer())
	{
		LPCTSTR szText = GetVendorContainer()->GetTitle();
		lstrcpy	(pRenderInfo->m_pszTitle
					,szText );
		pRenderInfo->m_byTitleLen = lstrlen(szText) + 1;
	}
	else
	{
		pRenderInfo->m_byTitleLen	= 0;
		pRenderInfo->m_pszTitle[0]	= 0;
	}	
}


sITEMINFO_BASEC* PlayerImpl::GetEquipItemInfo(eEQUIP_POS_INDEX pos) const
{
	BaseContainer* pContainer;
	
	pContainer = m_pItemManager->GetItemSlotContainer(SI_EQUIPMENT);

	if (!pContainer->IsEmpty( pos ))
	{
		const BaseSlot &			rSlot = pContainer->GetSlot( pos );
		sITEMINFO_BASEC *	pInfo = theItemInfoParser.GetItemInfo( rSlot.GetCode() );

		return pInfo;
	}

	return NULL;
}

eARMOR_TYPE PlayerImpl::GetArmorType() const
{
	sITEMINFO_BASEC* pInfo = GetEquipItemInfo( EQUIPPOS_SHIRTS );

	if (pInfo != NULL)
		return (eARMOR_TYPE)pInfo->m_wDefType;

	return eARMOR_UNARMOR;
}

eMELEE_TYPE PlayerImpl::GetMeleeType() const
{
	sITEMINFO_BASEC* pInfo = GetEquipItemInfo( EQUIPPOS_WEAPON );

	if (pInfo != NULL)
		return (eMELEE_TYPE)pInfo->m_wAttType;

	return MELEETYPE_HIT;
}


eATTACK_KIND PlayerImpl::GetAttackKind(eATTACK_KIND defaultKind) const
{
	sITEMINFO_BASEC* pInfo = GetEquipItemInfo( EQUIPPOS_WEAPON );

	if (pInfo != NULL)
		return (eATTACK_KIND)pInfo->m_bySeries;

	return defaultKind;
}


float PlayerImpl::GetAttackRange() const
{
	float fAttackRange(2.5f);

	SLOTPOS wPos = EQUIPPOS_WEAPON;
	if(!m_pItemManager->GetItemSlotContainer(SI_EQUIPMENT)->IsEmpty( wPos ) )
	{
		const BaseSlot & rSlot = m_pItemManager->GetItemSlotContainer(SI_EQUIPMENT)->GetSlot( wPos );
		sITEMINFO_BASE *pInfo = theItemInfoParser.GetItemInfo( rSlot.GetCode() );
		if(pInfo)
			fAttackRange = pInfo->m_wAttRange;
	}

	return fAttackRange;
}




DWORD PlayerImpl::GetPhysicalAvoidValue() const 
{
	short nPhysicalVoidRate		= m_Attr.GetPhysicalAvoidRate();
	short nPhysicalVoidRatePer = m_Attr.GetPhysicalAvoidRatePer();

	nPhysicalVoidRate				+= (short)((float)nPhysicalVoidRatePer/100.f * nPhysicalVoidRate);
	return (DWORD)( GetLevel()/5.f + nPhysicalVoidRate ); 
}



DWORD PlayerImpl::GetWeaponKind()
{
	const BaseSlot & rSlot = m_pItemManager->GetItemSlotContainer(SI_EQUIPMENT)->GetSlot( EQUIPPOS_WEAPON );
	if( !rSlot.GetCode() )
		return 0;

	sITEMINFO_BASE *pItemInfo = theItemInfoParser.GetItemInfo( rSlot.GetCode() );
	if( !pItemInfo )
		return 0;
	return pItemInfo->m_wType;
}

VOID PlayerImpl::UpdateLifeRecover( BOOL bHPUpdate, BOOL bMPUpdate )
{
	int iRecoverHP;
	int iRecoverMP;

	//////////////////////////////////////////////
	if( bHPUpdate )
	{
		iRecoverHP = theBattleFormular.CalcHPRecover	(GetCharType()
                                       ,m_Attr.GetVIT()
													,GetStatusManager()->GetCondition()
													,GetLevel());
		SetAttr(ATTRIBUTE_RECOVERY_HP
             ,ATTRIBUTE_KIND_BASE
				 ,iRecoverHP);
	}
	
	//////////////////////////////////////////////
	if( bMPUpdate )
	{
		iRecoverMP = theBattleFormular.CalcMPRecover	(GetCharType()
                                       ,m_Attr.GetSPR()
													,GetStatusManager()->GetCondition());
		SetAttr(ATTRIBUTE_RECOVERY_MP
             ,ATTRIBUTE_KIND_BASE
				 ,iRecoverMP);
	}
}


//-------------------------------------------------------------------------------------------------
BOOL	PlayerImpl::CanChangeStatAttr(eATTRIBUTE_TYPE attrType) const
{
	__BOOL_SUPER(CanChangeStatAttr(attrType));

	switch (attrType)
	{
	case ATTRIBUTE_STR:
	case ATTRIBUTE_DEX:
	case ATTRIBUTE_VIT:
	case ATTRIBUTE_INT:
	case ATTRIBUTE_SPR:
	case ATTRIBUTE_LRN:
	case ATTRIBUTE_CRE:
	case ATTRIBUTE_EXPERTY1:
	case ATTRIBUTE_EXPERTY2:
		break;

	default :
		return FALSE;
	}

	return TRUE;
}

DWORD	PlayerImpl::PrepareStatAttr(eATTRIBUTE_TYPE attrType)
{
	//DWORD		resultValue;
	DWORD&	dwRemainStat = PlayerCookie::GetCharInfo()->m_dwRemainStat;
	if(dwRemainStat <= 0)
		return 0;


	//////////////////////////////////
	switch (attrType)
	{
	case ATTRIBUTE_STR:
	case ATTRIBUTE_DEX:
	case ATTRIBUTE_VIT:
	case ATTRIBUTE_INT:
	case ATTRIBUTE_SPR:
	case ATTRIBUTE_LRN:
	case ATTRIBUTE_CRE:
	case ATTRIBUTE_EXPERTY1:
	case ATTRIBUTE_EXPERTY2:
		break;

	default :
		return 0;
	}


	return _SUPER::PrepareStatAttr(attrType);
}




VOID PlayerImpl::StatPointUp( DWORD dwBonusStat )
{
	PlayerCookie::GetCharInfo()->m_dwRemainStat += dwBonusStat;

	MSG_CG_GM_STAT_UP_ACK msgSend;
	msgSend.m_dwRemainStat		= (WORD)PlayerCookie::GetCharInfo()->m_dwRemainStat;	// 巢篮 胶湃器牢飘

	SendPacket( &msgSend, sizeof(msgSend) );
}

VOID PlayerImpl::SkillPointUp( DWORD dwBonusSkill )
{
	PlayerCookie::GetCharInfo()->m_dwRemainSkill += dwBonusSkill;

	MSG_CG_GM_SKILLPOINT_UP_ACK msgSend;
	msgSend.m_dwRemainSkill		= (WORD)PlayerCookie::GetCharInfo()->m_dwRemainSkill;	// 巢篮 胶懦器牢飘

	SendPacket( &msgSend, sizeof(msgSend) );
}

VOID PlayerImpl::ModifyPlayerTitle( ePLAYER_TITLE removeFlag, ePLAYER_TITLE addFlag ) 
{ 
	_BIT_REMOVE	(m_dwTitleFlag, removeFlag);
	_BIT_ADD		(m_dwTitleFlag, addFlag);

	//////////////////////////////////////
	MSG_CG_STATUS_TITLE_CHANGED_CMD cmsg;
	cmsg.m_dwTitleFlag	= GetTitleFlag();
	SendPacket( &cmsg, sizeof(cmsg) );

	//////////////////////////////////////
	MSG_CG_STATUS_TITLE_CHANGED_BRD bmsg;
	bmsg.m_dwPlayerKey	= GetObjectKey();
	bmsg.m_dwTitleFlag	= GetTitleFlag();
	SendAround( &bmsg, sizeof(bmsg), FALSE );
}





VOID PlayerImpl::OnExpChanged( DWORD dwTargetObjKey )
{
	MSG_CG_STATUS_EXP_CMD msgSend;
	msgSend.m_dwTargetObjKey	= dwTargetObjKey;
	msgSend.m_dwExp				= GetExp();

	SendPacket( &msgSend, sizeof(msgSend) );
}

VOID PlayerImpl::OnLevelUp()
{
	DWORD dwStatBonus;
	DWORD dwSkillBonus;

	//////////////////////////////////////////////
	dwStatBonus = theFormularManager.GetStatPointPerLevel();
	PlayerCookie::GetCharInfo()->m_dwRemainStat = PlayerCookie::GetCharInfo()->m_dwRemainStat + dwStatBonus;


	//////////////////////////////////////////////
	dwSkillBonus = theFormularManager.GetSkillPoint( GetLevel() );
	PlayerCookie::GetCharInfo()->m_dwRemainSkill = PlayerCookie::GetCharInfo()->m_dwRemainSkill + dwSkillBonus;

	//////////////////////////////////////////////
	ReadyNextExp();


	//////////////////////////////////////////////
	SetHP( GetMaxHP() );

	SetMP( GetMaxMP() );


	//////////////////////////////////////////////
	MSG_AG_STATUS_LEVEL_UP_CMD cmsg;
	cmsg.m_Level				= GetLevel();
	cmsg.m_wRemainStat		= (WORD)PlayerCookie::GetCharInfo()->m_dwRemainStat;	
	cmsg.m_wRemainSkill		= (WORD)PlayerCookie::GetCharInfo()->m_dwRemainSkill;	
	cmsg.m_dwCurHP				= GetHP();
	cmsg.m_dwCurMP				= GetMP();
	SendPacket( &cmsg, sizeof(cmsg) );

	//////////////////////////////////////////////
	MSG_CG_STATUS_LEVEL_UP_BRD msgSend;
	msgSend.m_dwObjectKey		= GetObjectKey();
	msgSend.m_Level				= GetLevel();
	msgSend.m_wRemainStat		= (WORD)PlayerCookie::GetCharInfo()->m_dwRemainStat;	
	msgSend.m_wRemainSkill		= (WORD)PlayerCookie::GetCharInfo()->m_dwRemainSkill;	
	msgSend.m_dwCurHP				= GetHP();
	msgSend.m_dwCurMP				= GetMP();

	SendAround( &msgSend, sizeof(msgSend), FALSE );


	//////////////////////////////////////////////
	Party *pParty = thePartyManager.FindParty( GetPartyData().GetPartyKey() );
	if( pParty )
	{
		pParty->OnMemberLevelUp( GetObjectKey() );
	}

	theScriptManager.StartupVM(SCRIPTTYPE_LEVELUP, GetLevel(),m_pScriptChar);

}


DWORD PlayerImpl::IncreaseMP( DWORD dwIncreament )
{
	dwIncreament = _SUPER::IncreaseMP( dwIncreament );

	m_pSkillManager->UpdatePassiveSkills();

	return dwIncreament;
}

DWORD PlayerImpl::DecreaseMP( DWORD dwDecrement )
{
	dwDecrement = _SUPER::DecreaseMP( dwDecrement );

	m_pSkillManager->UpdatePassiveSkills();

	return dwDecrement;
}


VOID PlayerImpl::SaveStartPos()
{
	if( !GetField() ) 
		return ;

	Map * pMap = GetField()->GetMap();
	if( pMap->GetZoneType() == ZONETYPE_VILLAGE )
	{
		Vector3D vPos = GetPosition();

		PlayerCookie::GetCharInfo()->m_dwRegion		= GetField()->GetMap()->GetMapCode();

		PlayerCookie::GetCharInfo()->m_sLocationX	= (SHORT)(vPos.x / TILE_3DSIZE + 0.5f);
		PlayerCookie::GetCharInfo()->m_sLocationY	= (SHORT)(vPos.y / TILE_3DSIZE + 0.5f);
		PlayerCookie::GetCharInfo()->m_sLocationZ	= (SHORT)vPos.z;
	}
	else
	{
		PlayerCookie::GetCharInfo()->m_sLocationX	= 0;
		PlayerCookie::GetCharInfo()->m_sLocationY	= 0;
		PlayerCookie::GetCharInfo()->m_sLocationZ	= 0;
	}
}


Vector3D PlayerImpl::LoadStartPos( Field * pField )
{
	Vector3D vPosition(0.f);
	sPLAYERINFO_BASE&	info = *PlayerCookie::GetCharInfo();

	if(	info.m_dwRegion	== pField->GetMap()->GetMapCode()
		&&	info.m_sLocationX != 0 
		&&	info.m_sLocationY != 0 
		&&	info.m_sLocationZ != 0 )
	{
		vPosition.x = info.m_sLocationX * TILE_3DSIZE;
		vPosition.y = info.m_sLocationY * TILE_3DSIZE;
		vPosition.z = info.m_sLocationZ ;

		//if( -1 == pField->GetTileLand()->GetTileToStand( vPosition ) )
	}

	if( !pField->IsMovableAt( vPosition ) )
	{
		vPosition = pField->GetRandomStartPos();
	}

	return vPosition;
}


BOOL PlayerImpl::CanPlusMoney( MONEY plus_money )
{
	if( GetMoney() + plus_money > ULLONG_MAX )
		return FALSE;
	return TRUE;
}

BOOL PlayerImpl::CanMinusMoney( MONEY minus_money )
{
	if( GetMoney() < minus_money )
		return FALSE;
	return TRUE;
}

BOOL PlayerImpl::PlusMoney( MONEY plus_money )
{
	if( !CanPlusMoney(plus_money) )
		return FALSE;

	SetMoney( GetMoney() + plus_money );
	theDropManager.PlusUserMoney( (DWORD)plus_money );

	return TRUE;
}

BOOL PlayerImpl::MinusMoney( MONEY minus_money )
{
	if( !CanMinusMoney(minus_money) )
		return FALSE;

	SetMoney( GetMoney() - minus_money );

	theDropManager.MinusUserMoney			((DWORD) minus_money );
	theDropManager.MinusTotalMinusMoney	((DWORD) minus_money );

	return TRUE;
}


//-------------------------------------------------------------------------------------------------
VOID PlayerImpl::SetName	(LPCTSTR /*szName*/)
{
	//lstrcpy(m_pCharInfo->m_szCharName,szName);
}

VOID PlayerImpl::SetAttr(eATTRIBUTE_TYPE attrType
                        ,ATTRIBUTE_KIND  attrKind
								,int             iValue)
{
	m_Attr[attrType][attrKind] = iValue;
	m_Attr.UpdateEx(); 
}

VOID PlayerImpl::SetHP( DWORD dwHP )		
{
	m_pCharInfo->m_dwHP = math::Clamp(dwHP, (DWORD)0,  GetMaxHP() );
}

VOID PlayerImpl::SetMP( DWORD dwMP )		
{
	m_pCharInfo->m_dwMP = math::Clamp( dwMP, (DWORD)0, GetMaxMP() );
}


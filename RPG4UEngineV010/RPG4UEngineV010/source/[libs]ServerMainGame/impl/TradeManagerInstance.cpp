/*////////////////////////////////////////////////////////////////////////
文 件 名：TradeManagerInstance.cpp
创建日期：2009年6月10日
最后更新：2009年6月10日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "TradeManagerInstance.h"
#include "ITradeSlot.h"
#include "Player.h"
#include "ItemManager.h"
#include "ItemSlotContainer.h"
#include <ItemSlot.h>
#include "ITradeContainer.h"
#include "ServerShell.h"
#include "SlotSystem.h"

using namespace RC;



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(TradeManagerInstance, ()  , gamemain::eInstPrioGameFunc);


TradeManagerInstance::TradeManagerInstance(void)
{
}

TradeManagerInstance::~TradeManagerInstance(void)
{
	Release();
}

VOID TradeManagerInstance::Init( DWORD maxPoolSize )
{
	_SUPER::Init(maxPoolSize);
}

VOID TradeManagerInstance::Release()
{
	_SUPER::Release();
}




BOOL TradeManagerInstance::ValidState( Player * pPlayer )
{
	//////////////////////////////////////
	__CHECK		(!pPlayer->IsExpiredPlayer() );
	__CHECK		(pPlayer->GetBehave() == PLAYER_BEHAVE_TRADE);
	__CHECK_PTR	(pPlayer->GetTradeContainer());


	Player * pPlayer2 = pPlayer->GetTradeContainer()->GetCounterPlayer();

	//////////////////////////////////////
	__CHECK		(!pPlayer2->IsExpiredPlayer() );
	__CHECK		(pPlayer2->GetBehave() == PLAYER_BEHAVE_TRADE);
	__CHECK_PTR	(pPlayer2->GetTradeContainer());


	__CHECK( pPlayer->GetTradeContainer()->GetKey() == pPlayer2->GetTradeContainer()->GetKey() );

	return TRUE;
}


eTRADE_RESULT TradeManagerInstance::PutItem(Player *             pPlayer
                                       ,SLOTPOS              posOrgin
													,SLOTPOS & OUT        posTrade
													,ITEMOPT_STREAM & OUT tradeStream)
{
	ITradeContainer * pContainer;
	BaseContainer *	pInventoryContainer;
	
	///////////////////////////////////////
	pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer)	,_RC_TRADE(CANCEL));

	__CHECK2(pContainer->GetState() == READY,	_RC_TRADE(FAILED));

	///////////////////////////////////////
	pInventoryContainer = pPlayer->GetItemManager()->GetItemSlotContainer(SI_INVENTORY);

	__CHECK2(pInventoryContainer->ValidPos( posOrgin ) , _RC_TRADE(FAILED));
	__CHECK2(!pInventoryContainer->IsEmpty( posOrgin )	,_RC_TRADE(FAILED));


	///////////////////////////////////////
	BaseSlot & rSlot = pInventoryContainer->GetSlot( posOrgin );

	///////////////////////////////////
	if( !pContainer->PutItem( (ItemSlot &)rSlot, posTrade ) )
		return _RC_TRADE(FAILED);

	((ItemSlot &)rSlot).CopyOut( tradeStream );

	return _RC_TRADE(SUCCESS);
}


eTRADE_RESULT TradeManagerInstance::SwapItem(Player * pPlayer
                                        ,SLOTPOS  fromPos
													 ,SLOTPOS  toPos)
{
	ITradeContainer * pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer),_RC_TRADE(CANCEL));
	__CHECK2(pContainer->GetState() == READY,_RC_TRADE(FAILED));

	__CHECK2(pContainer->ValidPos( fromPos )	,_RC_TRADE(FAILED));
	__CHECK2(!pContainer->IsEmpty( fromPos )	,_RC_TRADE(FAILED));


	///////////////////////////////////
	if( !pContainer->SwapItem( fromPos, toPos ) )
		return _RC_TRADE(FAILED);

	return _RC_TRADE(SUCCESS);
}

eTRADE_RESULT TradeManagerInstance::TakeItem( Player * pPlayer, SLOTPOS posTrade )
{
	ITradeContainer * pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer),_RC_TRADE(CANCEL));
	__CHECK2(pContainer->GetState() == READY,_RC_TRADE(FAILED));


	__CHECK2(pContainer->ValidPos(posTrade  )	,_RC_TRADE(FAILED));
	__CHECK2(!pContainer->IsEmpty(posTrade  )	,_RC_TRADE(FAILED));

	pContainer->TakeItem( posTrade );

	return _RC_TRADE(SUCCESS);
}

eTRADE_RESULT TradeManagerInstance::PutMoney( Player * pPlayer, MONEY & IN money )
{
	ITradeContainer * pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer),_RC_TRADE(CANCEL));
	__CHECK2(pContainer->GetState() == READY,_RC_TRADE(FAILED));

	if( !pContainer->PutMoney( money ) )
		return _RC_TRADE(PLAYER1_HAVENOTMONEY);

	return _RC_TRADE(SUCCESS);
}

eTRADE_RESULT TradeManagerInstance::TakeMoney( Player * pPlayer, MONEY & IN money )
{
	ITradeContainer * pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer),_RC_TRADE(CANCEL));
	__CHECK2(pContainer->GetState() == READY,_RC_TRADE(FAILED));

	if( !pContainer->TakeMoney( money ) )
		return _RC_TRADE(FAILED);

	return _RC_TRADE(SUCCESS);
}

eTRADE_RESULT TradeManagerInstance::Proposal( Player * pPlayer )
{
	ITradeContainer * pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer),_RC_TRADE(CANCEL));
	__CHECK2(pContainer->GetState() == READY,_RC_TRADE(FAILED));

	pContainer->SetState( PROPOSAL );

	return _RC_TRADE(SUCCESS);
}

eTRADE_RESULT TradeManagerInstance::Modify( Player * pPlayer )
{
	ITradeContainer * pContainer = pPlayer->GetTradeContainer();

	__CHECK2(ValidState(pPlayer),_RC_TRADE(CANCEL));
	__CHECK2(pContainer->GetState() == PROPOSAL,_RC_TRADE(FAILED));


	Player *				pPlayer2		= pContainer->GetCounterPlayer();
	ITradeContainer * pContainer2 = pPlayer2->GetTradeContainer();


	pContainer->SetState( READY );
	pContainer2->SetState( READY );

	return _RC_TRADE(SUCCESS);
}




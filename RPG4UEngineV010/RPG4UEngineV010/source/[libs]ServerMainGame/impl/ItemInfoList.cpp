#include "stdafx.h"
#include "ItemInfoList.h"
#include <Struct.h>
#include <ScriptRead.h>

ItemInfoList::ItemInfoList()
	:	m_pItemInfoHashTable ( NULL )
{
}

ItemInfoList::~ItemInfoList()
{
	ASSERT( m_pItemInfoHashTable == NULL );
}

VOID ItemInfoList::Release()
{
	if( m_pItemInfoHashTable )
	{
		Unload();
		delete m_pItemInfoHashTable;
		m_pItemInfoHashTable = NULL;
	}
}

VOID ItemInfoList::Init( DWORD dwItemInfoPoolSize )
{
	ASSERT( m_pItemInfoHashTable == NULL );
	m_pItemInfoHashTable = new util::SolarHashTable<BASE_ITEMINFO *>;
	m_pItemInfoHashTable->Initialize( dwItemInfoPoolSize );
}


VOID ItemInfoList::Unload()
{
	if( m_pItemInfoHashTable )
	{
		BASE_ITEMINFO * pInfo = NULL ;
		m_pItemInfoHashTable->SetFirst();
		while( pInfo = m_pItemInfoHashTable->GetNext() )
		{
			delete pInfo;
		}
		m_pItemInfoHashTable->RemoveAll();
	}
}

#pragma warning ( push )
#pragma warning ( disable : 4244)

BOOL ItemInfoList::Load( char * pszFileName )
{
	ASSERT( m_pItemInfoHashTable != NULL );
	Unload();
	
	// ���� ����
	CScriptRead sr;
	if ( !sr.Create( pszFileName ) )
	{
		DISPMSG("File Open Error : ItemList.txt");
		return (FALSE);
	}

	BASE_ITEMINFO * pInfo = new BASE_ITEMINFO;
	sr.GetToken(TT_STRING);
	strncpy( pInfo->m_pszName, sr.GetTokenStringPtr(), MAX_ITEMNAME_LENGTH );
	sr.GetToken(TT_NUMBER); pInfo->m_Code = sr.GetTokenNumber();										
	sr.GetToken(TT_NUMBER); pInfo->m_NCode = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_DCode = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_byType = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_Level = sr.GetTokenNumber();	                                
	sr.GetToken(TT_NUMBER); pInfo->m_Dura = sr.GetTokenNumber();										
	sr.GetToken(TT_NUMBER); pInfo->m_wMass = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_byDrop = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_byUse = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_wMaterial1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMaQa1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMaterial2 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMaQa2 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wSound = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_Cost = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wLimitEqLevel = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wLimitStr = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wLimitDex = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wLimitInt = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMinDamage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMaxDamage = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_wPhyAttRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wPhyAttSpeed = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wPhyDef = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wPhyAvoid = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttType = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wAttRange = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wDefType = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMagicAttDamgage = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMagicAttRate = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMagicAttSpeed = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMagicDef = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wMagicAvoid = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_wSpeed = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_wEqClass1 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wEqClass2 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wEqClass3 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wEqClass4 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wEqClass5 = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wEqPos = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_wMaxRank = sr.GetTokenNumber();									

	sr.GetToken(TT_NUMBER); pInfo->m_wOption1 = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_wOption2 = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_wOptionKind = sr.GetTokenNumber();									
	sr.GetToken(TT_NUMBER); pInfo->m_SocketNum = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_wHealHP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wHealHPTime = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wHealMP = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wHealMPTime = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wSpecialEffect = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wHeelSpecialQa = sr.GetTokenNumber();
	sr.GetToken(TT_NUMBER); pInfo->m_wHeelSpecialTime = sr.GetTokenNumber();

	sr.GetToken(TT_NUMBER); pInfo->m_MaterialCode = sr.GetTokenNumber();


	ASSERT( NULL == m_pItemInfoHashTable->GetData( pInfo->m_Code ) );
	m_pItemInfoHashTable->Add( pInfo, pInfo->m_Code );


	sr.Destroy();

	return TRUE;

}
#pragma warning ( pop )


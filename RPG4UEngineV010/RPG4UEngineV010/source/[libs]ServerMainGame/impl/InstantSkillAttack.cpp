#include "stdafx.h"
#include "SkillInfoParser.h"
#include "NPC.h"
#include "Field.h"
#include "Character.h"
#include "Player.h"
#include "Skill.h"
#include "AITypes.h"
#include <PacketStruct_CG.h>
#include <battle.h>
#include "CharacterFormula.h"



VOID Skill::BroadcastInstantSkill()
{
	Field *pField = m_pOwnerChar->GetField();
	if( !pField ) return;

	// 스킬 구사중 맞지 않도록 해야 하는 경우 비젼을 꺼준다.
	if( IsInvisibleSkill() )
	{
		m_pOwnerChar->SetVision( FALSE );
	}

	// 플레이어 위치를 클라가 보내준 어택 시작점으로 옮긴다.
	WzVector vPlayerPos, vNPCPos;
	vPlayerPos = m_SkillInfo.wvCurPos;
	m_pOwnerChar->SetPos( &vPlayerPos );

	// 플레이어를 DestPos으로 이동시킨다.
	WzVector wvPosAfterThrust;
	m_pOwnerChar->ExecuteThrust( &( m_SkillInfo.wvDestPos - m_SkillInfo.wvCurPos ), wvPosAfterThrust );
	m_pOwnerChar->OnAttack( m_SkillInfo.dwSkillCode );

	// 공격범위에 들어가는 NPC들을 찾아서 Result 처리를 한다.
	NPC *pNPC;	WORD wDamage = 0;	BOOL bMainTargetFound = FALSE;	BYTE byHitObjectCount = 0;
	WzVector wvNormalVectorP2M, vMainTargetPos;

	SKILL_RESULT_POS_INFO PosResultMsg[MAX_TARGET_NUM];
	SKILL_RESULT_NPOS_INFO NPosResultMsg[MAX_TARGET_NUM];

	// Player좌표를 MainTarget으로 지정하는 스킬일 경우 vMainTargetPos을 PlayerPos으로 지정한다. ( [m_uiTarget] 1:적, 2:아군, 3:자신, 4:범위, 5:시체)
	if( m_pBaseSkillInfo->m_uiTarget == 3 )
	{
		vMainTargetPos = vPlayerPos;
		bMainTargetFound = TRUE;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	// 전체 NPC에게 적용되는 확률 계산!
	///////////////////////////////////////////////////////////////////////////////////////////
	BYTE byCommonEffect = 0;

	// 밀리게 할 것인가?
	BOOL bKnockBack = FALSE;
	if( m_pBaseSkillInfo->m_fKnockBackRate && GetRandProp() <= m_pBaseSkillInfo->m_fKnockBackRate * 100 )
	{
		bKnockBack = TRUE;
		byCommonEffect |= SKILL_EFFECT_KNOCKBACK;
	}

	///////////////////////////////////////////////////////////////////////////////////////////
	// 확률 계산이 끝났으면 처리
	///////////////////////////////////////////////////////////////////////////////////////////

	for( DWORD i = 0; i < m_SkillInfo.byNumberOfTargets; ++i )
	{
		// 공격 대상 찾기
		pNPC = pField->FindNPC( m_SkillInfo.dwTargetKey[i] );
		if( !pNPC ) continue;
		pNPC->GetPos( &vNPCPos );
		wvPosAfterThrust = vNPCPos;

		// 메인 타겟이 아직 없다면 이 NPC로 설정
		if( !bMainTargetFound )
		{
			vMainTargetPos = vNPCPos;
			bMainTargetFound = TRUE;
		}

		// 스킬 공격범위에 들어가는지 체크
		if( !SkillAreaCheck( pNPC, vMainTargetPos ) )
		{
//			DISPMSG( "Skill : [BroadcastInstantSkill] Out Of SkillArea! \n" );
			continue;
		}

		// 데미지를 적용한다.
		if (pNPC->CanBeAttacked())
		{
			WORD	armorDamageRatio = 100;	// TODO: 값옷 종류별로 결정
			BOOL	bRangeAttack = FALSE;
			wDamage = (WORD)CalcPhysicalDamage(m_pOwnerChar, pNPC, armorDamageRatio, bRangeAttack);
			wDamage += m_pBaseSkillInfo->m_uiDamageValue1;

			wDamage = (WORD) ( wDamage * ( 1 + m_pBaseSkillInfo->m_fDamagePercent1 ) );
		}
		pNPC->DecreaseHP( wDamage );
		pNPC->OnDamaged( wDamage );

		// NPC에게 공격받았다고 알려준다.
		{
			// 공격당한 NPC에게 AIMessage를 날린다.
			AI_MSG_ATTACK AIAttackMsg;
			AIAttackMsg.dwAttackerKey = m_pOwnerChar->GetObjectKey();
			AIAttackMsg.dwTargetKey = pNPC->GetObjectKey();
			AIAttackMsg.wDamage = wDamage;
			AIAttackMsg.dwTargetHP = pNPC->GetHP();
			pNPC->SendAIMessage( &AIAttackMsg, sizeof(AI_MSG_ATTACK) );
		}

		// NPC가 공중으로 뜨는 스킬일 경우 Flying Message를 날린다.
		if( IsTargetFlyingSkill() )
		{
			AI_MSG_FLYING flyMsg;
			flyMsg.dwFlyingTick = GetExecuteInterval();
			pNPC->SendAIMessage( &flyMsg, sizeof(flyMsg) );
		}

		// 타겟을 멈춰놓는 스킬일 경우
		if( IsTargetStopSkill() )
		{
			AI_MSG_KNOCKDOWN downMsg;
			downMsg.dwKnockDownTick = GetExecuteInterval();
			pNPC->SendAIMessage( &downMsg, sizeof(downMsg) );
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		// NPC 마다 확률이 적용되는 경우
		///////////////////////////////////////////////////////////////////////////////////////////
		BYTE bySkillEffect = 0;
		bySkillEffect |= byCommonEffect;

		// 스턴을 먹일 것인가?
		BOOL bStun = FALSE;
		if( m_pBaseSkillInfo->m_fStunRate && GetRandProp() <= m_pBaseSkillInfo->m_fStunRate * 100 )
		{
			bStun = TRUE;
			bySkillEffect |= SKILL_EFFECT_STUN;
		}

		// 다운시킬 것인가?
		BOOL bKnockDown = FALSE;				
		if( m_pBaseSkillInfo->m_fDownRate && GetRandProp() <= m_pBaseSkillInfo->m_fDownRate * 100 )
		{
			bKnockDown = TRUE;
			bySkillEffect |= SKILL_EFFECT_KNOCKDOWN;
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		// 확률 계산이 끝났으면 처리
		///////////////////////////////////////////////////////////////////////////////////////////

		// 밀려야 하는 경우의 처리(그냥 밀리기와 다운되는 밀리기는 속도가 다르므로 다운 여부도 넣어서 같이 처리)
		if( bKnockBack )
		{
			VectorNormalize( &wvNormalVectorP2M, &(vNPCPos - vPlayerPos) );
			wvNormalVectorP2M = wvNormalVectorP2M * m_pBaseSkillInfo->m_fKnockBackRange;
			pNPC->ExecuteThrust( &wvNormalVectorP2M, wvPosAfterThrust, bKnockDown );
		}
		// 밀리지는 않고 다운만 되는 경우
		else if( bKnockDown )
		{
			// NPC에게 다운 메세지를 보냄
			AI_MSG_KNOCKDOWN downMsg;
			pNPC->SendAIMessage( &downMsg, sizeof(downMsg) );
		}

		// 스턴 처리
		if( bStun )
		{
			// NPC에게 스턴 메세지를 보냄
			AI_MSG_STUN stunMsg;
			pNPC->SendAIMessage( &stunMsg, sizeof(stunMsg) );
		}

		// Result Packet 정보를 설정한다.
		if( m_pBaseSkillInfo->m_fKnockBackRate )
		{
			PosResultMsg[byHitObjectCount].dwTargetKey		= pNPC->GetObjectKey();
			PosResultMsg[byHitObjectCount].dwDamage			= wDamage;
			PosResultMsg[byHitObjectCount].dwTargetHP		= pNPC->GetHP();
			PosResultMsg[byHitObjectCount].bySkillEffect	= bySkillEffect;
			PosResultMsg[byHitObjectCount].wvCurPos			= vNPCPos;
			PosResultMsg[byHitObjectCount].wvDestPos		= wvPosAfterThrust;
		}
		else
		{
			NPosResultMsg[byHitObjectCount].dwTargetKey		= pNPC->GetObjectKey();
			NPosResultMsg[byHitObjectCount].dwDamage		= wDamage;
			NPosResultMsg[byHitObjectCount].dwTargetHP		= pNPC->GetHP();
			NPosResultMsg[byHitObjectCount].bySkillEffect	= bySkillEffect;
		}

		byHitObjectCount++;
	}

	///////////////////////////////////////////
	// 기본 정보
	MSG_BASE_FORWARD baseMsg;
	baseMsg.m_byCategory		= CG_SKILL;
	baseMsg.m_byProtocol		= CG_SKILL_P2M_INSTANT_BRD;

	// 액션 정보
	SKILL_ACTION_POS_INFO actionMsg;
	actionMsg.dwSkillCode			= m_SkillInfo.dwSkillCode;
	actionMsg.dwClientSerial		= m_SkillInfo.dwClientSerial;
	actionMsg.dwAttackerKey			= m_pOwnerChar->GetObjectKey();
	actionMsg.dwPrimaryTargetKey	= m_SkillInfo.dwPrimaryTargetKey;
	actionMsg.wvCurPos				= m_SkillInfo.wvCurPos;
	actionMsg.wvDestPos				= m_SkillInfo.wvDestPos;
	actionMsg.byNumberOfTargets		= byHitObjectCount;

	// 정보들을 하나로 합쳐서 보낸다.
	BYTE *pMsg[ 2 + MAX_TARGET_NUM ];
	WORD wSize[ 2 + MAX_TARGET_NUM ];

	for( i = 0; i < byHitObjectCount; i++ )
	{
		if( m_pBaseSkillInfo->m_fKnockBackRate )
		{
			pMsg[2+i] = (BYTE*)&PosResultMsg[i];
			wSize[2+i] = PosResultMsg[i].GetSize();
		}
		else
		{
			pMsg[2+i] = (BYTE*)&NPosResultMsg[i];
			wSize[2+i] = NPosResultMsg[i].GetSize();
		}
	}

	pMsg[0] = (BYTE*)&baseMsg;		pMsg[1] = (BYTE*)&actionMsg;
	wSize[0] = sizeof(MSG_BASE_FORWARD);
	wSize[1] = actionMsg.GetSize();

	pField->SendExPacketAround( m_pOwnerChar, 2 + byHitObjectCount, pMsg, wSize );
}

VOID Skill::ExecuteInstantSkill()
{
	// 플레이어가 스킬 구사중 몬스터의 시야에서 사라지는 스킬일 경우 Vision 속성을 켜준다.
	if( IsInvisibleSkill() )
	{
		m_pOwnerChar->SetVision( TRUE );	// 켜줘야 되는거 아닌가? --; 담덕\죄송함다 ^^;;
	}
}

#include "stdafx.h"
#include "SkillInfoParser.h"
#include "NPC.h"
#include "Field.h"
#include "Character.h"
#include "Player.h"
#include "Skill.h"
#include "AITypes.h"
#include "CharacterFormula.h"
#include <PacketStruct_CG.h>
#include <Battle.h>
#include <Global.h>


VOID Skill::Broadcast_PlayerStyleAttack()
{
	// 타겟이 필드에 있는지 체크
	NPC *pNPC = m_pOwnerChar->GetField()->FindNPC( m_SkillInfo.dwPrimaryTargetKey );
	if( !pNPC )		return;

	Field *pField = m_pOwnerChar->GetField();
	if( !pField ) return;

	// 스타일 공격 시작에 대한 브로드 캐스팅은 해준다.
	MSG_CG_STYLE_P2M_ATTACK_BRD brdMsg;
	brdMsg.m_byCategory				= CG_STYLE;
	brdMsg.m_byProtocol				= CG_STYLE_P2M_ATTACK_BRD;
	brdMsg.dwAttackerKey			= m_pOwnerChar->GetObjectKey();
	brdMsg.byAttackType				= m_SkillInfo.byAttackType;
	brdMsg.dwStyleCode				= m_SkillInfo.dwSkillCode;
	brdMsg.dwClientSerial			= m_SkillInfo.dwClientSerial;
	brdMsg.dwPrimaryTargetKey		= m_SkillInfo.dwTargetKey[0];
	brdMsg.wvCurPos					= m_SkillInfo.wvCurPos;
	brdMsg.wvDestPos				= m_SkillInfo.wvDestPos;

	//DISPMSG("Broadcast_PlayerStyleAttack: %s --> %s\n", ToString(brdMsg.wvCurPos).c_str(), ToString(brdMsg.wvDestPos).c_str());

	pField->SendPacketAround( m_pOwnerChar, (MSG_BASE_FORWARD*)&brdMsg, (WORD)brdMsg.GetSize() );
}

VOID Skill::Execute_PlayerStyleAttack()
{
	Field *pField = m_pOwnerChar->GetField();
	if( !pField ) return;

	// 브로드 캐스팅 패킷 준비
	MSG_CG_STYLE_P2M_ATTACK_RESULT_BRD brdMsg;
	brdMsg.m_byCategory					= CG_STYLE;
	brdMsg.m_byProtocol					= CG_STYLE_P2M_ATTACK_RESULT_BRD;
	brdMsg.dwClientSerial				= m_SkillInfo.dwClientSerial;
	brdMsg.dwAttackerKey				= m_pOwnerChar->GetObjectKey();

	WzVector vPlayerPos, vNPCPos;
	m_pOwnerChar->GetPos( &vPlayerPos );

	// TODO: StyleAttack에서의 타격계 MP회복처리가 필요하다? StyleInfo에 MP소모량이 없다.;
//	m_pOwnerChar->OnAttack( m_SkillInfo.dwSkillCode );

	// 받은 리스트 중에서 공격에 유효한 몬스터들만 데미지 처리 후 브로드 캐스트 패킷에 추가
	NPC *pNPC;
	BOOL bMainTargetFound = FALSE;
	BYTE byHitObjectCount = 0;
	WzVector wvNormalVectorP2M, wvPosAfterThrust, vMainTargetPos;
	DWORD dwDamage = 0;

	///////////////////////////////////////////////////////////////////////////////////////////
	// 일단 녹백과 다운은 타겟 NPC 전체에 같은 확률로 적용하기 위해 미리 계산
	///////////////////////////////////////////////////////////////////////////////////////////
	BYTE byCommonEffect = 0;

	// 밀리게 할 것인가?
	BOOL bKnockBack = FALSE;
	if( m_pBaseStyleInfo->m_fKnockBackRate && GetRandProp() <= m_pBaseStyleInfo->m_fKnockBackRate * 100 )
	{
		bKnockBack = TRUE;
		byCommonEffect |= SKILL_EFFECT_KNOCKBACK;
	}


	// 타겟 NPC 개별적 처리
	for( DWORD i = 0; i < m_SkillInfo.byNumberOfTargets; ++i )
	{
		BYTE bySkillEffect = 0;
		bySkillEffect |= byCommonEffect;

		// 공격 대상 찾기
		pNPC = pField->FindNPC( m_SkillInfo.dwTargetKey[i] );
		if( !pNPC ) 
		{
			DISPMSG( "Skill : [ExecuteStyleAttack] Can't Find NPC!\n" );
			continue;
		}

		pNPC->GetPos( &vNPCPos );
		wvPosAfterThrust = vNPCPos;

		// 메인 타겟이 아직 없다면 이 NPC로 설정
		if( !bMainTargetFound )
		{
			vMainTargetPos = vNPCPos;
			bMainTargetFound = TRUE;
		}

		// 스타일 공격범위에 들어가는지 체크
		BOOL bAreaCheckFlag = pNPC->IsWithinRange( m_pOwnerChar, vMainTargetPos, m_pBaseStyleInfo->m_uiAttRangeform, 
			m_pBaseStyleInfo->m_fAttRange, 0 );

		if( !bAreaCheckFlag )
		{
//			DISPMSG( "Skill : [ExecuteStyleAttack] Out Of StyleArea!\n" );
			continue;
		}

		// 데미지를 적용한다.
		DWORD dwDamage = 0;
		if (pNPC->CanBeAttacked())
		{
			BOOL bSkillAttack = FALSE;
			BOOL bRangeAttack = FALSE;
			WORD wCriticalBonus = m_pBaseStyleInfo->m_wCriticalBonus;
			float fCriticalRatioBonus = 0;
			BOOL bCritical = FALSE;
			dwDamage = CalcPhysicalDamage( m_pOwnerChar, pNPC, bSkillAttack, bRangeAttack, wCriticalBonus, fCriticalRatioBonus, &bCritical);

			// 크리티컬이 터지면
			if (bCritical)
			{
				bySkillEffect |= SKILL_EFFECT_CRITICAL;
			}

			dwDamage = (DWORD) ( dwDamage * ( 1 + m_pBaseStyleInfo->m_fDamagePercent3 ) ) + m_pBaseStyleInfo->m_uiAddDamage3;
		}		
		
		pNPC->DecreaseHP( dwDamage );
		pNPC->OnDamaged( dwDamage );

		// NPC에게 공격받았다고 알려준다.
		{
			// 공격당한 NPC에게 AIMessage를 날린다.
			AI_MSG_ATTACK AIAttackMsg;
			AIAttackMsg.dwAttackerKey = m_pOwnerChar->GetObjectKey();
			AIAttackMsg.dwTargetKey = pNPC->GetObjectKey();
			AIAttackMsg.dwDamage = dwDamage;
			AIAttackMsg.dwTargetHP = pNPC->GetHP();
			pNPC->SendAIMessage( &AIAttackMsg, sizeof(AI_MSG_ATTACK) );
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		// 성공 확률에 따라 이펙트 효과가 적용된다.
		///////////////////////////////////////////////////////////////////////////////////////////

		// 관통될 것인가?
		BOOL bPierce = FALSE;
		if( m_pBaseStyleInfo->m_fPierceRate && GetRandProp() <= m_pBaseStyleInfo->m_fPierceRate * 100 )
		{
			bPierce = TRUE;
			bySkillEffect |= SKILL_EFFECT_PIERCE;
		}

		// 스턴을 먹일 것인가?
		BOOL bStun = FALSE;
		if( m_pBaseStyleInfo->m_fStunRate && GetRandProp() <= m_pBaseStyleInfo->m_fStunRate * 100 )
		{
			bStun = TRUE;
			bySkillEffect |= SKILL_EFFECT_STUN;
		}

		// 다운시킬 것인가?
		BOOL bKnockDown = FALSE;				
		if( m_pBaseStyleInfo->m_fDownRate && GetRandProp() <= m_pBaseStyleInfo->m_fDownRate * 100 )
		{
			bKnockDown = TRUE;
			bySkillEffect |= SKILL_EFFECT_KNOCKDOWN;
		}

		///////////////////////////////////////////////////////////////////////////////////////////
		// 확률 계산이 끝났으면 처리
		///////////////////////////////////////////////////////////////////////////////////////////

		// 밀려야 하는 경우의 처리(밀린 후 다운 될 것인가의 정보도 포함)
		if( bKnockBack )
		{
			WzVector diffVec = vNPCPos - vPlayerPos;
			VectorNormalize( &wvNormalVectorP2M, &diffVec );
			wvNormalVectorP2M = wvNormalVectorP2M * m_pBaseStyleInfo->m_fKnockBackRange;
			pNPC->ExecuteThrust( &wvNormalVectorP2M, wvPosAfterThrust, bKnockDown );	// 메세지까지 보냄
		}
		// 밀리지는 않고 다운만 되는 경우
		else if( bKnockDown )
		{
			// NPC에게 다운 메세지를 보냄
			AI_MSG_KNOCKDOWN downMsg;
			pNPC->SendAIMessage( &downMsg, sizeof(downMsg) );
		}

		// 관통시켜야 하는 경우의 처리
		if( bPierce )
		{
		}

		// 스턴 이펙트를 적용해야 하는 경우의 처리
		if( bStun )
		{
			// NPC에게 스턴 메세지를 보냄
			AI_MSG_STUN stunMsg;
			stunMsg.dwStunTick = 1500;
			pNPC->SendAIMessage( &stunMsg, sizeof(stunMsg) );
		}

		// 패킷에 추가
		brdMsg.AttackInfo[byHitObjectCount].dwTargetKey		= pNPC->GetObjectKey();
		brdMsg.AttackInfo[byHitObjectCount].dwDamage		= dwDamage;
		brdMsg.AttackInfo[byHitObjectCount].dwTargetHP		= pNPC->GetHP();
		brdMsg.AttackInfo[byHitObjectCount].wvCurPos		= vNPCPos;
		brdMsg.AttackInfo[byHitObjectCount].wvDestPos		= wvPosAfterThrust;
		brdMsg.AttackInfo[byHitObjectCount].byEffect		= bySkillEffect;
		
		//DISPMSG("Execute_PlayerStyleAttack: %s --> %s\n", ToString(vNPCPos).c_str(), ToString(wvPosAfterThrust).c_str());

		byHitObjectCount++;
	}

	brdMsg.byNumberOfTargets = byHitObjectCount;
	if( byHitObjectCount == 0 )
	{
		DISPMSG( "byHitObjectCount==0\n" );
	}
	

	pField->SendPacketAround( m_pOwnerChar, (MSG_BASE_FORWARD*)&brdMsg, (WORD)brdMsg.GetSize() );
}

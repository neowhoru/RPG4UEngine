#include "StdAfx.h"
#include "Guild.h"
#include "GuildMember.h"
#include "StructBase.h"
#include "GuildMemberFactoryManager.h"



enum { MAX_GUILDMEMBER_NUM = 64 };

Guild::Guild(void)
{
	m_Members.Initialize(MAX_GUILDMEMBER_NUM);
}

Guild::~Guild(void)
{
	Destroy();
}

BOOL Guild::CheckPlayerRight( Player * /*pPlayer*/, eGUILD_RIGHTS /*right*/ )
{

	return TRUE;
}

VOID Guild::Destroy()
{
	for( GuildMemberTableIt it = m_Members.begin() ; it != m_Members.end() ; ++it )
	{
		(*it)->Release();
		theGuildMemberFactoryManager.DestroyInstance((*it));
	}
	m_Members.RemoveAll();
}

VOID Guild::GetGuildInfo( sGUILD_INFO_PACKET & OUT guildInfo )
{
	lstrcpyn( guildInfo.m_szGuildName		, m_Info.m_szGuildName		, MAX_GUILDNAME_LENGTH );
	lstrcpyn( guildInfo.m_szMasterCharName	, m_Info.m_szMasterCharName, MAX_CHARNAME_LENGTH);

	guildInfo.m_GuildGrade			= m_Info.m_GuildGrade;
	guildInfo.m_MasterCharGuid		= m_Info.m_MasterCharGuid;
	guildInfo.m_GuildPoint			= m_Info.m_GuildPoint;
	guildInfo.m_UserPoint			= m_Info.m_UserPoint;
	guildInfo.m_RestrictedRight	= m_Info.m_RestrictedRight;
	guildInfo.m_GuildMoney			= m_Info.m_GuildMoney;
	guildInfo.m_GuildMemberNum		= (WORD)GetMemberNum();
}

VOID Guild::Create(const sGUILD_INFO_BASE & IN rInfo
                  ,const sGUILD_MEMBER_INFO *  pMemberInfo
						,BYTE                        nMemberCount)
{
	memcpy( &m_Info, &rInfo, sizeof(sGUILD_INFO_BASE) );

	//////////////////////////////////////////
	GuildMember * pMember;
	for( int i = 0 ; i < nMemberCount ; ++i )
	{
		ASSERT( !FindMember( pMemberInfo[i].m_CharGuid ) );
		pMember = theGuildMemberFactoryManager.CreateInstance(GUILD_TYPE_DEFAULT);
		pMember->Init(*pMemberInfo);
		AddMember( pMember, pMember->GetCharGuid() );
	}
}

VOID Guild::Withdraw( CHARGUID CharGuid )
{
	GuildMember * pMember = FindMember(CharGuid);
	ASSERT(pMember);
	RemoveMember( CharGuid );
	theGuildMemberFactoryManager.DestroyInstance(pMember);
}

VOID Guild::Join( const sGUILD_MEMBER_INFO & IN memberInfo )
{
	GuildMember * pMember;
	
	pMember = theGuildMemberFactoryManager.CreateInstance(GUILD_TYPE_DEFAULT);
	pMember->Init(memberInfo);
	AddMember( pMember, pMember->GetCharGuid() );
}



// MapObject.cpp: implementation of the MapObject class.
//
//////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "MapObject.h"

//////////////////////////////////////////////////////////////////////
// Construction/Destruction
//////////////////////////////////////////////////////////////////////

MapObject::MapObject()
{
	SetObjectType( MAP_OBJECT );
}

MapObject::~MapObject()
{

}

BOOL MapObject::Init()
{
	m_BaseInfo.m_dwMaxHP = 100;
	m_BaseInfo.m_dwHP		= 100;

	return _SUPER::Init();
}

VOID MapObject::Release()
{
	_SUPER::Release();
}

VOID MapObject::Update( DWORD dwTick )
{
	_SUPER::Update( dwTick );
}

VOID MapObject::DecHP( DWORD dwHP )
{
	if( m_BaseInfo.m_dwHP <= dwHP )
		m_BaseInfo.m_dwHP = 0;
	else
		m_BaseInfo.m_dwHP -= dwHP;
}
/*////////////////////////////////////////////////////////////////////////
文 件 名：GameServerScriptHandler.cpp
创建日期：2007年8月2日
最后更新：2007年8月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "GameServerScriptHandler.h"
#include "Player.h"
#include "ItemSlotContainer.h"
#include "ItemInfoParser.h"
#include "IScriptInterface.h"
#include "PlayerAttributes.h"
//#include "SkillStorageManager.h"
#include "ItemManager.h"
#include "PacketInclude.h"
#include "QuestManager.h"
#include "QuestDefine.h"
#include "ResultCode.h"

#ifndef NONE_NEWGAMEINSCRIPT
//#include "ApplicationSetting.h"
//#include "DropItemAt.h"
//#include "QuestManager.h"

#pragma message(__FILE__  "(27) GameServerScriptHandler做人物类型检测  " )

using namespace RC;


int	GameServerScriptHandler::GetProfession(void)
{
	return m_pPlayer->GetCharInfo()->m_byClassCode;
}

int GameServerScriptHandler::GetSex()
{
	return m_pPlayer->GetCharInfo()->m_bySex;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::Money( int nMoney, int oprType )
{
	if(!m_pPlayer)
		return;

	switch( oprType ) 
	{
	case KEYW_SETADD:
		{
			m_pPlayer->SetMoney( m_pPlayer->GetMoney() + nMoney );
		}break;

	case KEYW_SETSUB:
		{
			if ( nMoney > (int)m_pPlayer->GetMoney() )
				m_pPlayer->SetMoney( 0 );
			else
				m_pPlayer->SetMoney( m_pPlayer->GetMoney() - nMoney );
		}break;

	case KEYW_SET:
		{
			m_pPlayer->SetMoney( nMoney );
		}break;

	default:
		LOGINFO("Error:Script_Money\n");
		return;
	}

	//MsgMoneyChange change;
	//change.dwNumber = m_pPlayer->GetMoney();
	//theServer.SendMsgToSingle( &change, pPlayer );
}




/*////////////////////////////////////////////////////////////////////////
//检测是否升级
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::SetExp( int nExp, int oprType )		// 经验
{
	if(!m_pPlayer)
		return;

	nExp = math::Clamp(nExp,0,nExp);
	switch( oprType ) 
	{
	case KEYW_SETADD:
		{
			m_pPlayer->AddExp( nExp );
		}break;

	case KEYW_SETSUB:
		{
			m_pPlayer->AddExp( -nExp );
		}break;

	case KEYW_SET:
		{
			m_pPlayer->SetExp( nExp );
			m_pPlayer->AddExp( 0 );
		}break;

	default:
		LOGINFO("Error:Script_SetExp");
		return;
	}
}


/*////////////////////////////////////////////////////////////////////////
//检测是否升级
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::SetSkillExp( int /*nExp*/, int /*oprType*/ )		// 经验
{
	//switch( oprType ) 
	//{
	//case KEYW_SETADD:
	//	m_pCharacter-> IncreaseExp( m_pCharacter, 0, nExp );
	//	break;
	//case KEYW_SETSUB:
	//	if ( nExp > (int)m_pCharacter->GetSkillExpForServer() )
	//		m_pCharacter->SetSkillExp( 0 );
	//	else
	//		m_pCharacter->SetSkillExp(m_pCharacter->GetSkillExpForServer()-nExp);
	//	break;
	//case KEYW_SET:
	//	m_pCharacter->SetSkillExp( nExp );
	//	break;
	//default:
	//	LOGINFO("Error:Script_SetSkillExp");
	//	return;
	//}
}

void GameServerScriptHandler::SetReputation( int /*attrVal*/, int /*oprType*/ )
{
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL	GameServerScriptHandler::IsSkillActived( SLOTCODE skillCode,LEVELTYPE skillLevel )
{
	return FALSE;
	//SkillSlot* pSkillSlot;
	//pSkillSlot = theSkillStorageManager.GetSkillSlot(skillCode + skillLevel);
	//return pSkillSlot != NULL;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define BASE_ATTR_IMPL(STR)\
int GameServerScriptHandler::Get##STR(  )\
{\
	if(!m_pPlayer)\
		return 0;\
	Attributes*  pAttr = &m_pPlayer->GetAttr();\
	return (int)pAttr->Get##STR();\
}\
void GameServerScriptHandler::Set##STR( int attrVal, int oprType )\
{\
	if(!m_pPlayer)\
		return;\
	Attributes*  pAttr = &m_pPlayer->GetAttr();\
\
	switch( oprType ) \
	{\
	case KEYW_SETADD:		\
		pAttr->m_##STR[ATTRIBUTE_KIND_BASE] += attrVal;\
		break;\
	case KEYW_SETSUB:	\
		pAttr->m_##STR[ATTRIBUTE_KIND_BASE] -= attrVal;\
		break;\
	case KEYW_SET:		\
		pAttr->m_##STR[ATTRIBUTE_KIND_BASE] = attrVal;		\
		break;\
	default:\
		LOGINFO("Error:Script_SetInt\n");\
		return;\
	}\
\
	pAttr->m_##STR.Update();\
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BASE_ATTR_IMPL(VIT);
BASE_ATTR_IMPL(DEX);
BASE_ATTR_IMPL(STR);
BASE_ATTR_IMPL(INT);
BASE_ATTR_IMPL(SPR);




void GameServerScriptHandler::SetHP( int attrVal, int oprType )
{
	switch( oprType ) 
	{
	case KEYW_SETADD:
		m_pCharacter-> SetHP( m_pCharacter-> GetHP() + attrVal );
		break;
	case KEYW_SETSUB:
		m_pCharacter-> SetHP( m_pCharacter-> GetHP() - attrVal );
		break;

	case KEYW_SET:
		m_pCharacter-> SetHP( attrVal );
		break;
	default:
		LOGINFO("Error:Script_SetHp\n");
		return;
	}
	//m_pCharacter-> SendCharArrtChange( CharAttr_HP, (DWORD)m_pCharacter-> GetHP() );
}


void GameServerScriptHandler::SetMP( int attrVal, int oprType )
{ 
	switch( oprType ) 
	{
	case KEYW_SETADD:
		m_pCharacter-> SetMP( m_pCharacter-> GetMP()+attrVal );
		break;
	case KEYW_SETSUB:
		m_pCharacter-> SetMP( m_pCharacter-> GetMP()-attrVal );
		break;
	case KEYW_SET:
		m_pCharacter-> SetMP( attrVal );
		break;
	default:
		LOGINFO("Error:Script_SetMp\n");
		return;
	}
	//m_pCharacter-> SendCharArrtChange( CharAttr_MP, (DWORD)m_pCharacter-> GetMP() );
}







//设置出生点
void GameServerScriptHandler::SetBornPoint( DWORD /*dwMapID*/, int /*iPosX*/, int /*iPosY*/ )
{
	//m_pCharacter->SetBornPosMapID(dwMapID);
	//m_pCharacter->SetBornPosX(iPosX);
	//m_pCharacter->SetBornPosY(iPosY);
}

//去除所有装备
void GameServerScriptHandler::RemoveAllEquip( )
{
	//m_pCharacter->UnequipAll( );
	//m_pCharacter->UpdatePlayerVisualEquip();
	//m_pCharacter->UpdateEquipEffCharDataToClient();
}


//去除所有道具
void GameServerScriptHandler::RemoveAllItem( )
{
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void	GameServerScriptHandler::LearnSkill	( LPCSTR /*szSkillName*/, INT /*nLevel*/ )
{
	//if(m_pPlayer)
	//	m_pPlayer->LearnSkill( szSkillName, nLevel );
}




/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::AddMoney( DWORD dwMoney, int /*nType*/ )
{
	MSG_CG_ITEM_PICK_MONEY_ACK msg;
	msg.m_Money		= m_pPlayer->GetMoney() + dwMoney;
	m_pPlayer->SendPacket(&msg,sizeof(msg));
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::AddItem( const char* szItemName, DWORD stCount )
{
	if(!m_pPlayer)
		return;

	sITEMINFO_BASE*	pItemInfo;	
	pItemInfo = theItemInfoParser.GetItemInfo(szItemName);
	if(!pItemInfo)
	{
		LOGINFO("物品信息[%s]不存在\n",szItemName);
		return;
	}
	ItemManager*	pItemManger;

	pItemManger = m_pPlayer->GetItemManager();

	{
		MSG_CG_ITEM_PICK_ACK msg;

		pItemManger->Obtain(pItemInfo->m_Code,(SLOTPOS)stCount, &msg.m_ItemInfo);
		m_pPlayer->SendPacket(&msg,msg.GetSize());
	}
}



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
DWORD GameServerScriptHandler::CancelQuest( CODETYPE questID, BOOL bDiscard )
{
	quest::sQUEST_INFO*			pQuest;
	quest::sQUEST_STEPINFO*		pState;
	quest::sQUEST_REWARD*		pReward;

	if(!m_pPlayer)
		return _RC_QUE(PLAYER_NOT_EXISTED);
	
	pQuest = theQuestManager.LoadQuest( questID );
	if( !pQuest )
	{
		LOGINFO( "sQUEST_INFO %ld load failed", questID );
		return _RC_QUE(INFO_NOT_EXISTED);
	}

	////////////////////////////////////////
	if(bDiscard)
	{
		if ( pQuest->m_QuestLV >= 100 )
		{
			LOGINFO( "Cancel sQUEST_INFO, sQUEST_INFO %ld is cant be abandon", questID );
			return _RC_QUE(CANNT_DISCARD);
		}

		// 如果任务不是出于活动状态
		if( !m_pPlayer-> IsQuestActive( questID ) )
		{
			LOGINFO( "Cancel sQUEST_INFO, sQUEST_INFO %ld is not active", questID );
			return _RC_QUE(NOT_ACTIVATED);
		}
	}

	// 清除此任务相应的记数变量
	for( UINT n=0; n<pQuest->m_StepNum; n++ )
	{
		pState	= &pQuest->m_StepInfos[n];

		for( UINT m=0; m<pState->m_StepRequireNum; m++ )
		{
			pReward = &pState->m_StepRequires[m];

			if ( pReward->m_RewardType == quest::eQUESTREWARD_VAR )
			{
				if( m_pPlayer-> GetVar(pReward->m_VarID ) != 0 )
				{
					m_pPlayer-> SetVar(pReward->m_VarID, 0 );
				}
			}
		}
	}

	// 取消任务后，任务系统变量置0
	if(bDiscard)
		m_pPlayer->DiscardQuest( questID);

	return _RC_QUE(SUCCESS);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameServerScriptHandler::CheckQuestDone( CODETYPE questID )
{
	quest::sQUEST_INFO* pQuest = theQuestManager.LoadQuest( questID );
	if( !pQuest )
	{
		//m_pCharacter-> TellClient( "CheckQuestDone, sQUEST_INFO %ld load failed", questID );
		return FALSE;
	}

	// 如果任务不是出于活动状态
	if( !QuestManager:: IsQuestActive( questID ) )
	{
		//m_pCharacter-> TellClient( "CheckQuestDone, sQUEST_INFO %ld is not active", questID );
		return FALSE;
	}
	return FALSE;
}

DWORD GameServerScriptHandler::DoneQuest( CODETYPE questID )
{
	quest::sQUEST_INFO* pQuest;

	if(!m_pPlayer)
		return 0;
	
	pQuest = theQuestManager.LoadQuest( questID );
	if( !pQuest )
	{
		LOGINFO( "DoneQuest, sQUEST_INFO %ld load failed", questID );
		return 0;
	}

	// 如果任务不是出于活动状态
	if( !m_pPlayer-> IsQuestActive( questID ) )
	{
		LOGINFO( "DoneQuest, sQUEST_INFO %ld is not active", questID );
		return 0;
	}

	if ( pQuest->m_CanRepeate )
		m_pPlayer-> SetQuestState( questID, 0 );
	else
		m_pPlayer-> SetQuestState( questID, INVALID_DWORD_ID );

	//sprintf( szInfo, "[%s]任务完成", pQuest->szName );

	////////////////////////////////////////////////////////
	for( UINT i = 0; i < pQuest->m_RewardNum; i++ )
	{
		quest::sQUEST_REWARD* r = &pQuest->m_Rewards[i];
		switch( r->m_RewardType )
		{
		case quest::eQUESTREWARD_MONEY:
			{
				AddMoney( r->m_Value, eSCRIPTMATH_PLUS );
			}break;

		case quest::eQUESTREWARD_EXP:
			{
				SetExp( r->m_Value, KEYW_SETADD );
			}break;

		case quest::eQUESTREWARD_SKILLEXP:
			{
				SetSkillExp( r->m_Value, KEYW_SETADD );
			}break;

		case quest::eQUESTREWARD_ITEM:
			{
				AddItem((LPCTSTR) r->m_ItemName, r->m_Value );
			}break;

		case quest::eQUESTREWARD_SKILL:
			{
				LearnSkill((LPCTSTR) r->m_ItemName, r->m_Value );
			}break;

		}
	}

	CancelQuest(questID,FALSE);

	return 1;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameServerScriptHandler::ExistItem(LPCSTR szItemName, DWORD dwItemCount)
{
	if(!m_pPlayer)
		return FALSE;

	sITEMINFO_BASE*	pItemInfo;	
	ItemManager*	pItemManger;

	pItemInfo = theItemInfoParser.GetItemInfo(szItemName);
	if(!pItemInfo)
	{
		LOGINFO("物品信息[%s]不存在\n",szItemName);
		return FALSE;
	}

	pItemManger = m_pPlayer->GetItemManager();

	__CHECK(pItemManger->IsExistItem(SI_INVENTORY, pItemInfo->m_Code,(DURATYPE)dwItemCount,NULL,NULL) );

	return TRUE;
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
BOOL GameServerScriptHandler::DeleteItem(LPCSTR szItemName, DWORD dwItemCount)
{
	if(!m_pPlayer)
		return FALSE;

	sITEMINFO_BASE*	pItemInfo;	
	ItemManager*		pItemManger;
	BaseContainer*		pContainer;

	//assert(dwItemCount < 255);

	pItemInfo = theItemInfoParser.GetItemInfo(szItemName);
	if(!pItemInfo)
	{
		LOGINFO("物品信息[%s]不存在\n",szItemName);
		return FALSE;
	}

	pItemManger = m_pPlayer->GetItemManager();

	SLOTPOS*	pPosArray	= new SLOTPOS[dwItemCount];
	DWORD		posNum		= dwItemCount;

	if(pItemManger->IsExistItem(SI_INVENTORY
                              ,pItemInfo->m_Code
										,dwItemCount
										,pPosArray
										,&posNum))
	{
		pContainer	= pItemManger->GetItemSlotContainer(SI_INVENTORY);
		for(UINT n=0; n<posNum; n++)
			pItemManger->Lose(pContainer,pPosArray[n],1, TRUE);
	}

	SAFE_DELETE_ARRAY(pPosArray);

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
int GameServerScriptHandler::GetEmptySlotCount()
{
	return 0;
}


void GameServerScriptHandler::SetProfession( int /*nProfession*/ )			// 职业
{
	//m_pCharacter->SetProfession( nProfession );
	//MsgProfessionChangeInfo profession;
	//profession.stProfession = nProfession;
	//theServer.SendMsgToSingle( &profession, m_pCharacter );
}






/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::SetQuestState( CODETYPE	questID, DWORD		state )
{
	if(m_pPlayer)
	{
		m_pPlayer->SetQuestState( questID, state );
	}
}

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::GetQuestState( CODETYPE	questID, KEYWORD varID)
{
	if(m_pPlayer)
	{
		DWORD dwStep = m_pPlayer->GetQuestState( questID );

		if(varID == INVALID_KEYWORD)
			theScriptInterface.SetTempValue(dwStep);
		else
			m_pPlayer->SetVar( varID, dwStep );
	}
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
void GameServerScriptHandler::CanGetNewQuest( KEYWORD varID )
{
	if(m_pPlayer)
	{
		BOOL bCan = m_pPlayer->CanGetNewQuest();
		if(varID == INVALID_KEYWORD)
			theScriptInterface.SetTempValue(bCan);
		else
			m_pPlayer->SetVar( varID, bCan ); 
	}
}






#endif
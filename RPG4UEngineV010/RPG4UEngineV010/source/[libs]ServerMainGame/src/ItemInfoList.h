#ifndef __ITEM_INFO_LIST_H__
#define __ITEM_INFO_LIST_H__

#include <SolarHashTable.h>
#include <Singleton.h>

struct BASE_ITEMINFO;

class ItemInfoList	: public Singleton<ItemInfoList>
{
public:
	ItemInfoList();
	~ItemInfoList();

	VOID									Init( DWORD dwItemInfoPoolSize );
	VOID									Release();
	BOOL									Load( char * pszFileName );

	BASE_ITEMINFO *							GetItemInfo( CODETYPE ItemCode ) { return m_pItemInfoHashTable->GetData( ItemCode ); }
private:
	VOID									Unload();
	util::SolarHashTable<BASE_ITEMINFO *> *	m_pItemInfoHashTable;
};

#endif // __ITEM_INFO_LIST_H__
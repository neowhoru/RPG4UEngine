/*////////////////////////////////////////////////////////////////////////
文 件 名：PlayerImpl.inl
创建日期：2009年6月3日
最后更新：2009年6月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PLAYERIMPL_INL__
#define __PLAYERIMPL_INL__
#pragma once


inline Attributes&			PlayerImpl::GetAttr() {return m_Attr;}
inline const Attributes&	PlayerImpl::GetAttr() const  {return m_Attr;}

inline LEVELTYPE			PlayerImpl::GetLevel() const				{ return PlayerCookie::GetCharLevel(); }
inline float				PlayerImpl::GetSkillRangeBonus() const	{ return (float)m_Attr.GetSkillRangeBonus()/10.0f; }
inline float				PlayerImpl::GetSightRange() const		{ return (float)m_Attr.GetSightRange()/10.0f; }

inline DWORD		PlayerImpl::GetExp() const				{ return m_pCharInfo->m_dwExp; }
inline DWORD		PlayerImpl::GetHP() const				{ return m_pCharInfo->m_dwHP; }
inline DWORD		PlayerImpl::GetMP() const				{ return m_pCharInfo->m_dwMP; }
inline DWORD		PlayerImpl::GetMaxHP() const			{ return (DWORD)(m_Attr.GetMaxHP() * (1.f + (float)m_Attr.GetMaxHPPer()/100.f)) ; }
inline DWORD		PlayerImpl::GetMaxMP() const			{ return (DWORD)(m_Attr.GetMaxMP() * (1.f + (float)m_Attr.GetMaxMPPer()/100.f)); }
inline LPCTSTR		PlayerImpl::GetCharName() const		{ return (LPCTSTR)m_pCharInfo->m_szCharName; }
inline int			PlayerImpl::GetRegenHP() const			{ return (DWORD)(m_Attr.GetRecoverHP() * (1.f + (float)m_Attr.GetRecoverHPPer()/100.f)); }
inline int			PlayerImpl::GetRegenMP() const			{ return (DWORD)(m_Attr.GetRecoverMP() * (1.f + (float)m_Attr.GetRecoverMPPer()/100.f)); }
inline DWORD		PlayerImpl::GetRemainStatPoint() const	{ return m_pCharInfo->m_dwRemainStat; }
inline DWORD		PlayerImpl::GetRemainSkillPoint() const{ return m_pCharInfo->m_dwRemainSkill; }
inline MONEY		PlayerImpl::GetMoney()	const				{ return m_pCharInfo->m_Money; }

inline float		PlayerImpl::GetPhysicalAttackSpeed() const { return m_Attr.GetAttSpeedRatio()/100.0f; }
inline int			PlayerImpl::GetMoveSpeedRatio		() const	{ return m_Attr.GetMoveSpeedRatio(); }
inline int			PlayerImpl::GetAttSpeedRatio		() const	{ return m_Attr.GetAttSpeedRatio(); }

inline VOID			PlayerImpl::SetMoney				( MONEY money )		{ m_pCharInfo->m_Money = money; }

inline VOID			PlayerImpl::SetExp	(DWORD exp)			{ m_pCharInfo->m_dwExp = exp; }
inline VOID			PlayerImpl::SetLevel	(LEVELTYPE level)	{ m_pCharInfo->m_LV = level; m_Attr.UpdateEx(); }
inline LPCTSTR		PlayerImpl::GetName	()						{ return m_pCharInfo->m_szCharName; }


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
//inline VOID			PlayerImpl::SetMoney				( MONEY money )		{ m_pCharInfo->m_Money = money; }



#endif //__PLAYERIMPL_INL__
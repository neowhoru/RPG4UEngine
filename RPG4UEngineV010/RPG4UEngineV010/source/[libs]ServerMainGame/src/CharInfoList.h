#ifndef __CHAR_INFO_LIST_H__
#define __CHAR_INFO_LIST_H__

#pragma once

#include <SolarHashTable.h>
#include <Singleton.h>

struct INIT_CHARINFO;
struct CALC_CHARINFO;

class CharInfoList : public Singleton<CharInfoList>
{
public:
	CharInfoList();
	~CharInfoList();

	VOID										Init( DWORD dwCharInfoPoolSize );
	VOID										Release();
	BOOL										Load( char * pszInitCharFileName, char * pszCalcCharFileName );

	INIT_CHARINFO *								GetInitCharInfo( CODETYPE ClassCode ) { return m_pInitCharInfoHashTable->GetData( ClassCode ); }
	CALC_CHARINFO *								GetCalcCharInfo( CODETYPE ClassCode ) { return m_pCalcCharInfoHashTable->GetData( ClassCode ); }
private:
	VOID										Unload();

	util::SolarHashTable<INIT_CHARINFO *> *		m_pInitCharInfoHashTable;
	util::SolarHashTable<CALC_CHARINFO *> *		m_pCalcCharInfoHashTable;
};



#endif // __CHAR_INFO_LIST_H__
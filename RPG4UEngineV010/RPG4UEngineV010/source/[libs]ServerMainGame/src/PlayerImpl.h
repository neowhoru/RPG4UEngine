/*////////////////////////////////////////////////////////////////////////
文 件 名：PlayerImpl.h
创建日期：2009年6月3日
最后更新：2009年6月3日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __PLAYERIMPL_H__
#define __PLAYERIMPL_H__
#pragma once



#include "CommonDefine.h"
#include <Player.h>
#include "PlayerAttributeImpl.h"
#include "MoveStateHandleImpl.h"
#include "MovementChecker.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERMAINGAME_API PlayerImpl	: public Player
{
public:
	PlayerImpl();
	virtual ~PlayerImpl();

public:
	virtual BOOL	Init		();
	virtual VOID	Release	();
	virtual VOID	Create	(ServerSession * pServerSession
                           ,KEYTYPE         userKey
									,LPCTSTR			  szUserID);
	virtual VOID	Destroy	();
	virtual VOID	Update	( DWORD dwTick );

	/////////////////////////////////////////////
	virtual VOID	OnEnterField(Field* pField, Vector3D* pvPos);
	virtual VOID	OnLeaveField();

public:
	////////////////////////////////////////////////////////////////////////
	//脚本与任务操作
	virtual BOOL	KeepQuestScript	(DWORD dwScriptType,DWORD dwQuestType);
	virtual BOOL	RunScript			(DWORD dwData, DWORD	npcID,BOOL bRunNow=FALSE );
	virtual BOOL	RunQuestScript		(CODETYPE questID, DWORD dwData,BOOL bRunNow=FALSE);//, DWORD	npcID );

	virtual DWORD	GetQuestState		( CODETYPE	questID );
	virtual void	SetQuestState		( CODETYPE	questID, DWORD state );
	virtual void	DiscardQuest		( CODETYPE	questID);
	virtual BOOL	IsQuestActive		( CODETYPE	questID );
	virtual BOOL	IsQuestDone			( CODETYPE	questID );
	virtual BOOL	CanGetNewQuest		();
	virtual BOOL	IsNeedDropTaskItem(CODETYPE		questID
												,DWORD			taskState
												,SLOTCODE		itemCode);

	virtual void	ChangeQuestData	(CODETYPE questID
                                    ,DWORD    state
												,DWORD    action);
	virtual void	OnSetItemLog		(BOOL bSet);

public:
	/////////////////////////////////////////////
	// 基本操作
	virtual BOOL		IsGMPlayer		();
	virtual BOOL		IsSuperPlayer	();
	virtual BOOL		CanChat			();
	virtual BOOL		CanBehave		();
	virtual VOID		SetDBCharState	(eDBCHAR_STATE stat
                                    ,WORD          wMinutes=0)		;

	/////////////////////////////////////////////
	virtual sPLAYERINFO_BASE *	GetCharInfo	();
	virtual VOID		GetRenderInfo			( sPLAYERINFO_RENDER		* OUT pRenderInfo );
	virtual VOID		GetVillageInfo			( sPLAYERINFO_VILLAGE	* OUT pVillageInfo );
	virtual VOID		GetGuildRenderInfo	( sGUILDINFO_RENDER		* OUT pRenderInfo );
	virtual VOID		GetVendorRenderInfo	( sVENDORINFO_RENDER		* OUT pRenderInfo );

	/////////////////////////////////////////////
	virtual VOID		ModifyPlayerTitle		( ePLAYER_TITLE removeFlag, ePLAYER_TITLE addFlag );


	/////////////////////////////////////////////
	virtual BOOL		SendPacket			(MSG_OBJECT_BASE * pMsg
                                       ,WORD              wSize
													,BOOL bCrypt =     FALSE);
	virtual BOOL		SendPackets			(DWORD dwNumberOfMessages
                                       ,BYTE** pMsg
													,WORD* pwSize);
	virtual BOOL		SendToDBPorxy		( MSG_OBJECT_BASE * pMsg, WORD wSize );
	virtual BOOL		SendToGuildServer	( MSG_OBJECT_BASE * pMsg, WORD wSize );
	virtual VOID		ForceDisconnect	( eDISCONNECT_REASON reason = DISCONNECT_UNKNOWN_REASON );


	/////////////////////////////////////////////
	virtual eBATTLE_RESULT		CanNormalAttack(BYTE     byAttackType
															,VECTOR3D &vCurPos
															,VECTOR3D &vDestPos);
	virtual eBATTLE_RESULT		CanStyleAttack	(BYTE     byAttackType
															,SLOTCODE StyleCode
															,VECTOR3D &vCurPos
															,VECTOR3D &vDestPos);
	virtual BOOL					CanAttack		() const;
	virtual BOOL					CanBeAttacked	() const;
	VOID								SetAttackDelay	(BYTE     byAttackType
                                             ,SLOTCODE StyleCode)		;
	virtual BOOL					OnResurrection	(float fRecoverExp
                                             ,float fRecoverHP
															,float fRecoverMP);
	virtual BOOL					OnDead			();
	virtual VOID					OnAttack			(Character* pTarget
                                             ,SLOTCODE   skillcode
															,DAMAGETYPE damage);
	virtual DAMAGETYPE			OnDamaged		(Character*   pAttacker
                                             ,eATTACK_KIND attackType
															,DAMAGETYPE   damage);
	virtual VOID					OnPunish			();
	virtual BOOL					IsFriend			( CharacterCookie *pTarget );
	

	/////////////////////////////////////////////
	virtual VOID					SetActionDelay	( DWORD delay );
	virtual DWORD					GetActionDelay	();
	virtual BOOL					IsActionExpired();
	virtual BOOL					IsOverPlayingTime();


	/////////////////////////////////////////////
	eITEM_RESULT					PrepareUseItem	( sITEMINFO_BASE * pItemInfo );

protected :
	//////////////////////////////////
	virtual VOID			OnLevelUp	();
	virtual VOID			OnExpChanged( DWORD dwTargetObjKey );


public:
	//////////////////////////////////
	virtual VOID					OnSetCharInfo		();
	virtual sITEMINFO_BASEC*	GetEquipItemInfo	(eEQUIP_POS_INDEX pos) const;


public:
	//////////////////////////////////

	//////////////////////////////////
	virtual eARMOR_TYPE			GetArmorType		() const;
	virtual eMELEE_TYPE			GetMeleeType		() const;
	virtual eATTACK_KIND			GetAttackKind		(eATTACK_KIND defaultKind) const;
	virtual float					GetAttackRange		() const;

	/////////////////////////////////////////////
	virtual Attributes&			GetAttr() ;
	virtual const Attributes&	GetAttr() const  ;

	virtual LEVELTYPE				GetLevel				() const			;
	virtual float					GetSkillRangeBonus() const	;
	virtual float					GetSightRange		() const		;

	virtual DWORD				GetExp		() const			;
	virtual DWORD				GetHP			() const			;
	virtual DWORD				GetMP			() const			;
	virtual DWORD				GetMaxHP		() const			;
	virtual DWORD				GetMaxMP		() const			;
	virtual LPCTSTR			GetCharName	() const			;
	virtual int					GetRegenHP	() const			;
	virtual int					GetRegenMP	() const			;
	virtual MONEY				GetMoney		()	const				;
	DWORD							GetRemainStatPoint	() const	;
	DWORD							GetRemainSkillPoint	() const	;



	/////////////////////////////////////////////
	virtual DWORD				GetPhysicalAvoidValue	() const ;
	virtual float				GetPhysicalAttackSpeed	() const ;
	virtual int					GetMoveSpeedRatio			() const	;
	virtual int					GetAttSpeedRatio			() const	;
	virtual DWORD				GetWeaponKind				();

	/////////////////////////////////////////////
	PLAYER_PTY_DECL(Strength			,STR );
	PLAYER_PTY_DECL(Dexterity			,DEX );
	PLAYER_PTY_DECL(Vitality			,VIT );
	PLAYER_PTY_DECL(Spirit				,SPR );
	PLAYER_PTY_DECL(Inteligence		,INT );
	PLAYER_PTY_DECL(Experty1			,Experty1 );
	PLAYER_PTY_DECL(Experty2			,Experty2 );



	/////////////////////////////////////////////
	virtual VOID			SetExp		(DWORD exp)			;
	virtual VOID			SetLevel		(LEVELTYPE level)	;
	virtual LPCTSTR		GetName		()	;
	virtual VOID			SetName		(LPCTSTR szName);


	/////////////////////////////////////////////
	virtual VOID				SetAttr		(eATTRIBUTE_TYPE eAttrType
													,ATTRIBUTE_KIND  eAttrKind
													,int             iValue);
	virtual VOID				SetHP			( DWORD dwHP )			;
	virtual VOID				SetMP			( DWORD dwMP )			;
	virtual DWORD				IncreaseMP	( DWORD dwIncrement );
	virtual DWORD				DecreaseMP	( DWORD dwDecrement );
	virtual VOID				UpdateLifeRecover( BOOL bHPUpdate, BOOL bMPUpdate );

	/////////////////////////////////////////////
	virtual BOOL				CanPlusMoney	( MONEY plus_money );
	virtual BOOL				CanMinusMoney	( MONEY minus_money );
	virtual BOOL				PlusMoney		( MONEY plus_money );
	virtual BOOL				MinusMoney		( MONEY minus_money );
	virtual VOID				SetMoney			( MONEY money )		;

	/////////////////////////////////////////////
	//开始位置
	virtual VOID				SaveStartPos	();
	virtual Vector3D			LoadStartPos	( Field * pField );


	/////////////////////////////////////////////
	//属性分配处理
	virtual BOOL				CanChangeStatAttr	(eATTRIBUTE_TYPE attrType) const;
	virtual DWORD				PrepareStatAttr	(eATTRIBUTE_TYPE attrType);

	/////////////////////////////////////////////
	//virtual VOID				LevelUp		( DWORD dwPlusLevel );
	virtual VOID				StatPointUp	( DWORD dwBonusStat );
	virtual VOID				SkillPointUp( DWORD dwBonusSkill );

	/////////////////////////////////////////////
	//技能处理
	virtual SLOTCODE			GetDefaultStyle	( DWORD dwWeaponCode );
	virtual SLOTCODE			GetRangeStyle		();//远程攻击心法

	virtual eSKILL_RESULT	CanLevelUpSkill	(BOOL     bSkill
                                             ,SLOTCODE CurSkillCode
															,SLOTCODE &NextSkillCode);
	virtual DWORD				LevelUpSkill		(BOOL        bSkill
                                             ,SLOTCODE    CurSkillCode
															,SLOTCODE    NextSkillCode
															,sSKILL_SLOT &skillSlot);
	virtual BOOL				CheckClassDefine	(BYTE     byUserType
                                             ,CODETYPE dwClassDefine)		;



	//----------------------------------------------------------------------
	virtual IPlayerWarehouse*	GetWarehouseContainer();

protected:
	PlayerAttributeImpl		m_Attr;
	PlayerMoveStateHandle	m_MoveStateHandle;
	MovementChecker			m_MoveChecker;
};

OBJECT_FACTORY_DECLARE(PlayerImpl);


#include "PlayerImpl.inl"



#endif //__PLAYERIMPL_H__
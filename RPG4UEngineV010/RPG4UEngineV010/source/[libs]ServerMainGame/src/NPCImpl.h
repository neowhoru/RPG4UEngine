/*////////////////////////////////////////////////////////////////////////
文 件 名：NPCImpl.h
创建日期：2009年6月2日
最后更新：2009年6月2日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#ifndef __NPCIMPL_H__
#define __NPCIMPL_H__
#pragma once


#include "CommonDefine.h"
#include "NPC.h"
#include "NPCAttributeImpl.h"
#include "MoveStateHandleImpl.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERMAINGAME_API NPCImpl : public NPC
{
public:
	NPCImpl();
	virtual ~NPCImpl();

public:
	virtual VOID					Update				( DWORD dwTick );
	virtual VOID					SetBaseInfo			( sNPCINFO_BASE* pNPCInfo );

	virtual VOID					GetRenderInfo		( sMONSTERINFO_RENDER * OUT pRenderInfo );
	virtual void					GetAroundQuestsTo	(Player* /*pPlayer*/){};

	//////////////////////////////////////////////////////////
	virtual VOID				SetTargetChar		(Character *pChar, BOOL bProcessObserver=TRUE );
	virtual VOID				SelectBestTarget	();
	virtual VOID				RemoveEnemy			(DWORD leaveEnemyKey);
	virtual BOOL				IsFriend				(CharacterCookie *pTarget );
	virtual Character*		SelectSkillTarget	(SLOTCODE skillCode, BOOL bHeal=FALSE );




	////////////////////////////////////////////////
public:
	////////////////////////////////////////////////
	virtual Attributes&			GetAttr		()					;
	virtual const Attributes&	GetAttr		() const			;

	virtual DWORD				GetExp				() const;
	virtual LEVELTYPE			GetLevel				() const				;
	virtual LEVELTYPE			GetDisplayLevel	() const		;
	virtual BYTE				GetGrade				() const				;
	virtual float				GetAttackRange		() const;
	virtual eATTACK_KIND		GetAttackKind		(eATTACK_KIND defaultKind) const;
	virtual float				GetSkillRangeBonus() const	;
	virtual float				GetSightRange		()	const		;
	virtual eARMOR_TYPE		GetArmorType		()	const		;
	virtual eMELEE_TYPE		GetMeleeType		()	const		;
	virtual DWORD				GetHP					() const		;
	virtual DWORD				GetMP					() const		;
	virtual DWORD				GetMaxHP				() const		;
	virtual DWORD				GetMaxMP				() const		;
	virtual LPCTSTR			GetCharName			() const		;
	virtual int					GetRegenHP			() const		;
	virtual int					GetRegenMP			() const		;

	virtual DWORD				GetPhysicalAvoidValue	() const ;
	virtual float				GetPhysicalAttackSpeed	() const ;
	virtual int					GetMoveSpeedRatio			() const	;
	virtual int					GetAttSpeedRatio			() const	;

	virtual VOID				SetAttr	(eATTRIBUTE_TYPE eAttrType
                                    ,ATTRIBUTE_KIND  eAttrKind
												,int             iValue);
	virtual VOID				SetHP		( DWORD dwHP )	;
	virtual VOID				SetMP		( DWORD dwMP )	;

	////////////////////////////////////////////////


	////////////////////////////////////////////////
public:
	virtual BOOL				CanRegenHPMP();

	virtual BOOL				UseSkill		(SLOTCODE skillCode );
	virtual eSKILL_RESULT	PrepareUseSkill	(SLOTCODE skillCode, BOOL bCoolTimerReset = TRUE );
	virtual VOID				SelectSkill	(Character *pTarget, SLOTCODE skillCode );

	virtual BOOL				IsSameTargetPos		();
	virtual VOID				AllocTrackPos			( Vector3D &vDestPos );

	////////////////////////////////////////////////
	virtual BOOL				IsNearTargetPos		();
	virtual BOOL				CanFallApartTarget	();

	////////////////////////////////////////////////
	virtual BOOL				CanRetreatFromTarget	();

	////////////////////////////////////////////////
	virtual BOOL			IsPositionCanJump		();

	// SendPacket
	virtual eSEND_RESULT	SendAround	(MSG_OBJECT_BASE *	pMsg
                                    ,WORD						wSize
												,BOOL						bSendToMe =  TRUE);

	virtual BOOL			MoveBRD				(Vector3D*  pvDestPos
                                          ,eMOVE_TYPE moveState)		;

	/////////////////////////////////////////////////////
	virtual	VOID			ChangeAIState		(DWORD  dwStateID
                                          ,LPARAM param1=0
														,LPARAM param2=0
														,LPARAM param3=0);
	virtual BOOL			ProcessAIInfo		(eAI_INFOTYPE type);

	virtual void			AINotice				(AIMSG* pMsg
                                          ,WORD    wSize
														,DWORD   dwDelay=0);
	virtual void			AINoticeAround		(AIMSG* pMsg			//除自身外的邻区伙伴
                                          ,WORD    wSize
														,DWORD   dwDelay=0);

	virtual void		GroupCmdBeginAttack	( Character* pTargetChar );
	virtual void		GroupCmdStopAttack	();

	/////////////////////////////////////////////////////
	virtual VOID		OnEnterField			(Field* pField, Vector3D* pvPos);
	virtual VOID		OnLeaveField			();

	/////////////////////////////////////////////////////
	virtual VOID			OnAttack	(Character* pTarget
                                 ,CODETYPE   skillcode
											,DAMAGETYPE damage);
	virtual DAMAGETYPE	OnDamaged(Character*   pAttacker
                                 ,eATTACK_KIND attackType
											,DAMAGETYPE   damage);


	/////////////////////////////////////////////////////
	virtual BOOL			JoinGroup			( ObjectGroup* pGroup );
	virtual BOOL			LeaveGroup			();
	virtual BOOL			IsMemberOfGroup	();
	virtual BOOL			IsLeaderOfGroup	();
	virtual BOOL			IsFollowerOfGroup	();
	virtual BOOL			IsLeaderAlive		();


	/////////////////////////////////////////////////////
	// BattleRecord
	virtual BOOL			HasEnemy			() const;
	virtual VOID			RecordAttackPoint	( Character* pAttacker, int attackPoint );
	virtual VOID			RecordTotalPoint	( Character* pAttacker, int TotalPoint );
	virtual Character*	SearchTarget	();


	/////////////////////////////////////////////////////
	// Skill

	virtual BOOL			OnResurrection	(float fRecoverExp
                                       ,float fRecoverHP
													,float fRecoverMP);
	virtual BOOL			OnDead			();
	VOID						DistributeExp	();

	virtual VOID			SetPlayerRefCounter	(int iCount );
	virtual BOOL			IsOutOfWanderRange	() const;
	virtual BOOL			IsOutOfRegenRange		( Vector3D &vTargetPos ) const ;

protected:
	virtual BOOL			Init			();
	virtual VOID			Release		();
	virtual VOID			InitAIStates	( DWORD dwStateID, LPARAM param1=NULL );

protected:
	virtual VOID			InitDefaultSkill();

protected:
	NPCAttributeImpl			m_Attr;
	NpcMoveStateHandle		m_MoveStateHandle;

};


#include "NPCImpl.inl"





#endif //__NPCIMPL_H__
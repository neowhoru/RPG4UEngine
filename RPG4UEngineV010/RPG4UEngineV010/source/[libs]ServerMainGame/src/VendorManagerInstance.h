#pragma once

#include <CommonDefine.h>
#include "VendorManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class  VendorManagerInstance : public VendorManager
{
public:
	VendorManagerInstance(void);
	virtual ~VendorManagerInstance(void);

public:
	///////////////////////////////////////////
	//��̯��̯
	virtual eVENDOR_RESULT StartVendor	(Player *                           pEstablishedPlayer
                                       ,LPCTSTR                            pTitle
													,sTOTALINFO_ESTABLISHER_VENDOR & IN TotalInfo);
	virtual eVENDOR_RESULT EndVendor		( Player * pEstablishedPlayer );


	///////////////////////////////////////////
	//��������
	virtual eVENDOR_RESULT Rename	(Player * pEstablishedPlayer
                                 ,LPCTSTR  szVendorTitle)		;
	virtual eVENDOR_RESULT Modify	(Player *                  pEstablishedPlayer
                                 ,sVENDOR_ITEM_SLOT & IN    slotInfo
											,sVENDOR_ITEM_SLOTEX & OUT retInfo);
	virtual eVENDOR_RESULT Insert	(Player *                  pEstablishedPlayer
                                 ,sVENDOR_ITEM_SLOT & IN    slotInfo
											,sVENDOR_ITEM_SLOTEX & OUT retInfo);
	virtual eVENDOR_RESULT Delete	(Player * pEstablishedPlayer
                                 ,SLOTPOS  posVendor)		;


	///////////////////////////////////////////
	virtual BOOL CheckEstablisher		( Player * pPlayer );
	virtual BOOL CheckObserver			( Player * pPlayer );

protected:
	IVendorContainer *	_Alloc	();
	VOID						_Free		( IVendorContainer * pUnit );
};


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(VendorManagerInstance ,  );
//extern API_NULL VendorManagerInstance& singleton::GetVendorManagerInstance();
#define theVendorManagerInstance  singleton::GetVendorManagerInstance()


#pragma once
//=============================================================================================================================

#include "THashTable.h"

class Guild;
using namespace RC;
using namespace util;


typedef THashTable<Guild *>					GameGuildTable;
typedef THashTable<Guild *>::iterator		GameGuildTableIt;

struct sGUILD_INFO_PACKET;


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class _SERVERMAINGAME_API GuildManager
{
public:
	GuildManager(void);
	virtual ~GuildManager(void);

public:
	virtual VOID Init		(DWORD dwGuildSize, DWORD dwMemberSize);
	virtual VOID Release	();

public:
	virtual eGUILD_RESULT GetGuildInfo	(Player *                 pPlayer
                                          ,sGUILD_INFO_PACKET & OUT rGuildInfo)		;
	virtual eGUILD_RESULT CreateGuild		(Player *      pPlayer
                                          ,LPCTSTR szGuildName)		;
	virtual eGUILD_RESULT DestroyGuild		(Player * pPlayer );
	virtual eGUILD_RESULT InviteMember		(Player * pPlayer
                                          ,Player * pReceiver)		;
	virtual eGUILD_RESULT AnswerMember		(Player * pPlayer
                                          ,Player * pReceiver
														,BOOL     bAccept);
	virtual eGUILD_RESULT WithdrawMember	(Player * pPlayer );
	
public:
	virtual eGUILD_RESULT	CanJoin		( Player * pPlayer, Player * pReceiver );
	virtual BOOL				ValidPlayer	( Player * pPlayer );

	virtual VOID				OnAddGuild	(const sGUILD_INFO_BASE & IN rInfo
                                       ,const sGUILD_MEMBER_INFO *  pMemberInfo
													,BYTE                        MemberCount);

	virtual VOID				OnDestroyGuild	(GUILDGUID GuildGuid );
	virtual VOID				OnWithdrawGuild(GUILDGUID GuildGuid
                                          ,CHARGUID  CharGuid)		;
	virtual VOID				OnJoinGuild		(GUILDGUID                     GuildGuid
                                          ,const sGUILD_MEMBER_INFO & IN rMemberInfo)		;
	
	virtual VOID DisplayerGuildInfo();


public:
	Guild * FindGuild( GUILDGUID GuildGuid )					;

protected:

	VOID AddGuild		( Guild * pGuild, GUILDGUID GuildGuid )		;
	VOID RemoveGuild	( GUILDGUID GuildGuid )						;

private:
	GameGuildTable		m_Guilds;
};

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_DECL(GuildManager , _SERVERMAINGAME_API );
//extern _SERVERMAINGAME_API  GuildManager& singleton::GetGameGuildManager();
#define theGuildManager  singleton::GetGuildManager()


#include "GuildManager.inl"
#define		GM_ENTRANCE				"입장"
#define		GM_SPEED				"속도"
#define		GM_WHISPER				"귓말"
#define		GM_TRANSPARENT			"투명"
#define		GM_INVINCIBILITY		"무적"
#define		GM_ACCOUNT				"어카운트"
#define		GM_REMOVAL_MONSTER		"몬스터제거"
#define		GM_REMOVAL				"제거"
#define		GM_KILL					"킬"
#define		GM_CREATE_ITEM			"생성"			//아이템생성
#define		GM_HIME					"하임"			//하임생성
#define		GM_LEVELUP				"레벨업"		
#define		GM_DISCONNECT			"접속종료"
#define		GM_REBIRTH				"부활"
#define		GM_RECOVERY				"회복"
#define		GM_CREATE_MONSTER		"몬스터소환"
#define		GM_REMOVAL_ITEM			"아이템제거"
#define		GM_STATPOINT_UP			"스텟업"		//스텟포인트 업
#define		GM_SKILLPOINT_UP		"스킬포인트업"	//스킬포인트 업
#define		GM_CHAR_INFO			"정보"			//캐릭터정보
#define		GM_ROOM_INFO			"방정보"		//방정보
#define		GM_CREATE_ENCHANT_ITEM	"인첸트만들기"	//인첸트아이템 생성

#define		GM_TURN_ON				"켬"
#define		GM_TURN_OFF				"끔"

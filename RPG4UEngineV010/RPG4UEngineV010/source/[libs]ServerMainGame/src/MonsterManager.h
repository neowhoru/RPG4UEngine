#ifndef __MONSTER_MANAGER_H__
#define __MONSTER_MANAGER_H__

#pragma once

#include <CriticalSection.h>
#include <Singleton.h>

#include <map>

using namespace std;
using namespace util;

class Object;
class ObjectList;

class MonsterManager
{
public:
	MonsterManager();
	~MonsterManager(void);

public:

	BOOL		AddMonster(Object *pMonster);
	BOOL		RemoveMonster(Object *pMonster);
	Object*		RemoveMonster(const DWORD dwObjKey);
	Object*		FindMonster(const DWORD dwObjKey);
	VOID		RemoveAllMonster();

	VOID		Update(DWORD dwElapsedTime);

private:
	ObjectList*				m_pObjectList;
	CCriticalSection		m_cs;

};

#endif // __MONSTER_MANAGER_H__

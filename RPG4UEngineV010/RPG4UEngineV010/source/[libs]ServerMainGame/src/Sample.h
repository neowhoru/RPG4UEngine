#include "NetworkGroup.h"
#include "ISessionHandle.h"

//-------------------------------------------------------------------------------------------------
// Sample Echo Server
//-------------------------------------------------------------------------------------------------

#ifdef _DEBUG
#pragma comment( lib, "NetworkGroup_d.lib" )
#else
#pragma comment( lib, "NetworkGroup.lib" )
#endif

//-------------------------------------------------------------------------------------------------
// User Class
//-------------------------------------------------------------------------------------------------
class _NETWORKGROUP_API User : public ISessionHandle
{
protected:
	virtual VOID	OnAccept( DWORD dwNetworkIndex ) {};
	virtual VOID	OnDisconnect() {};
	virtual	VOID	OnRecv( BYTE *pMsg, WORD wSize ) { Send( pMsg, wSize ); }
	virtual VOID	OnConnect( BOOL bSuccess, DWORD dwNetworkIndex ) {}    
};

//-------------------------------------------------------------------------------------------------
// Callback Functions
//-------------------------------------------------------------------------------------------------
ISessionHandle* CreateAcceptedObject() {	return new User; }
VOID DestroyAcceptedObject( ISessionHandle *pSessionHandle ) { delete pSessionHandle; }
VOID DestroyConnectedObject( ISessionHandle *pSessionHandle ) {delete pSessionHandle; }


//-------------------------------------------------------------------------------------------------
// Main
//-------------------------------------------------------------------------------------------------
int _tmain(int argc, _TCHAR* argv[])
{
	const DWORD CLIENT_IOHANDLER_KEY = 0;

	// IOCP 서버 인스턴스 생성
	NetworkGroup *pIOCPServer = new NetworkGroup;

	// 디스크립션 세팅
	sNETWORK_INFO desc;

	desc.dwNetworkKey				= CLIENT_IOHANDLER_KEY;
	desc.dwMaxAcceptSession			= 1000;
	desc.dwMaxConnectSession		= 0;
	desc.dwSendBufferSize			= 60000;
	desc.dwRecvBufferSize			= 60000;
	desc.dwTimeOut					= 30000;
	desc.dwMaxPacketSize			= 4096;
	desc.dwMaxIOThreads		= 1;
	desc.dwMaxAcceptThreads	= 1;
	desc.dwMaxConnectThreads	= 0;
	desc.fnCreateAcceptedSession		= CreateAcceptedObject;
	desc.fnDestroyAcceptedSession	= DestroyAcceptedObject;
	desc.fnDestroyWorkingSession	= DestroyConnectedObject;

	// IOCP 서버 초기화
	if( !pIOCPServer->Init( &desc, 1 ) )
	{
		printf( "IOCP 서버 초기화 실패" );
		return 0;
	}

	// 리슨 시작
	if( !pIOCPServer->StartListen( CLIENT_IOHANDLER_KEY, "10.1.28.134", 6000 ) )
	{
		printf( "리슨 실패" );
		return 0;
	}

	// assert( pIOCPServer->IsListening( CLIENT_IOHANDLER_KEY ) == TRUE );

	printf( "Server started!\n" );

	// 메인 루프에서 IOCP 서버 업데이트
    while( TRUE )
	{
		Sleep( 10 );
		pIOCPServer->Update();
	}

	printf( "Server is terminated...\n" );

	// IOCP 서버 셧다운
	pIOCPServer->Shutdown();

	// IOCP 서버 인스턴스 파괴
	delete pIOCPServer;

	return 0;
}

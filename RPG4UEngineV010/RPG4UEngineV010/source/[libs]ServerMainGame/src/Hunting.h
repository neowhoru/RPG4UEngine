#pragma once

#include "room.h"

class HUNTING_DESC;

class Hunting :
	public Room
{
	friend class ZoneManager;
public:
	Hunting(void);
	virtual ~Hunting(void);

	BOOL				Init( KEYTYPE Key, CODETYPE MapCode, KEYTYPE LeaderPlayerKey, HUNTING_DESC & IN desc );
	virtual BOOL		Update( DWORD dwDeltaTick );
	const HUNTING_DESC *GetHuntingDesc() { return &m_RoomDesc;	}
	virtual eROOMTYPE	GetRoomType()	{ return m_RoomDesc.m_eRoomType; }

	virtual CODETYPE	GetEntryFieldCode() { return m_RoomDesc.m_FieldCode; }
protected:
	virtual BOOL	readyPlayer( Player * pPlayer, CODETYPE FieldCode  );
	virtual BOOL	joinPlayer( Player * pPlayer, CODETYPE FieldCode );
	virtual BOOL	leavePlayer( Player * pPlayer );
	
private:
	HUNTING_DESC	m_RoomDesc;

	__DECL_ZONE_POOL( Hunting )
};

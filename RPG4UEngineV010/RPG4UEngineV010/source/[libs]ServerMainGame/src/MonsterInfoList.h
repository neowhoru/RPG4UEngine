#ifndef __MONSTER_INFO_LIST_H__
#define __MONSTER_INFO_LIST_H__


#pragma once


#include <SolarHashTable.h>
#include <Singleton.h>

struct BASE_MONSTERINFO;

class MonsterInfoList	: public Singleton<MonsterInfoList>
{
public:
	MonsterInfoList();
	~MonsterInfoList();

	VOID										Init( DWORD dwMonsterInfoPoolSize );
	VOID										Release();
	BOOL										Load( char * pszFileName );

	BASE_MONSTERINFO *							GetMonsterInfo( CODETYPE MonsterCode ) { return m_pMonsterInfoHashTable->GetData( MonsterCode ); }
private:
	VOID										Unload();
	util::SolarHashTable<BASE_MONSTERINFO *> *	m_pMonsterInfoHashTable;

};


#endif // __MONSTER_INFO_LIST_H__
/*////////////////////////////////////////////////////////////////////////
文 件 名：ApplicationSetting.h
创建日期：2006年8月16日
最后更新：2006年8月16日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：


版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#ifndef __APPLICATIONSETTING_H__
#define __APPLICATIONSETTING_H__
#pragma once

#include "BaseApplicationSetting.h"


// 配置信息
class  _SERVERMAINGAME_API ApplicationSetting : public gamemain::BaseApplicationSetting
{
public:

public:
	ApplicationSetting();
	// 初始化信息
	bool InitCfg();
	void Save();
};

/*////////////////////////////////////////////////////////////////////////
// 全局配置文件信息
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_DECL(ApplicationSetting , _SERVERMAINGAME_API );


#endif //__APPLICATIONSETTING_H__
#ifndef __OBJECTGROUP_FACTORY_H__
#define __OBJECTGROUP_FACTORY_H__

#pragma once

#include <Singleton.h>
#include <MemoryPoolFactory.h>

using namespace util;

class ObjectGroup;

class ObjectGroupFactory : public Singleton<ObjectGroupFactory>
{
public:
	ObjectGroupFactory();
	~ObjectGroupFactory();

	VOID			Init( DWORD dwMaxPoolSize );
	VOID			Release();
	ObjectGroup*	AllocGroup();
	VOID			FreeGroup( ObjectGroup* pGruop );

private:
	CMemoryPoolFactory<ObjectGroup> *	m_pGroupPool;
};

#endif

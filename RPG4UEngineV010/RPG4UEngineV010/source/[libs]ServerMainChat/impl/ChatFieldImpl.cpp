/*////////////////////////////////////////////////////////////////////////
文 件 名：ChatFieldImpl.cpp
创建日期：2007年2月11日
最后更新：2007年2月11日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ChatFieldImpl.h"
#include "User.h"
#include "ViewPortImpl.h"

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
/////////////////////////////////////////
// ChatFieldImpl
ChatFieldImpl::ChatFieldImpl()
: m_pZonePool(NULL)
{
	//m_Zones.clear();
}

ChatFieldImpl::~ChatFieldImpl()
{
	Release();
}


ViewPort*	ChatFieldImpl::AllocViewPort	()
{
	ViewPort* pObject = m_pZonePool->Alloc();
	
	if(pObject)
		pObject->Init();

	return pObject;
}

void ChatFieldImpl::FreeViewPort	(ViewPort* pObject)
{
	if(!pObject)
		return;
	pObject->Release();

	m_pZonePool->Free((ViewPortImpl*)pObject);
}


BOOL ChatFieldImpl::Init(FieldInfo *pFieldInfo)
{
	m_pFieldInfo = pFieldInfo;

	m_pZonePool = new TMemoryPoolFactory<ViewPortImpl>;
	m_pZonePool->Initialize(m_pFieldInfo->GetTotalSectorNum());

	return _SUPER::Init(pFieldInfo);
}

VOID ChatFieldImpl::Release()
{
	_SUPER::Release();
	SAFE_DELETE(m_pZonePool);
}


BOOL ChatFieldImpl::MoveViewPort(User *pUser, DWORD dwFieldCode, DWORD dwZoneID)
{
	__CHECK_PTR(pUser);

	//////////////////////////////////////////////////////////////////////
	//在不同的Field时
	if(pUser->GetFieldCode() != dwFieldCode)
	{
		if(!GetViewPort(dwZoneID) )
			goto laFailed;


		AddUser(pUser, dwZoneID);
		return TRUE;
	}


	//////////////////////////////////////////////
	//在不同的Zone时
	if(pUser->GetSectorIndex() != dwZoneID)
	{
		ViewPort	*	pViewPortNew ;
		ViewPort *	pViewPort;
		
		pViewPortNew = GetViewPort(dwZoneID);
		if(!pViewPortNew )
			goto laFailed;

		/////////////////////////////////////////////////////////
		pViewPort = GetViewPort(pUser->GetSectorIndex());
		if(!pViewPort )
			goto laFailed;




		pViewPort	->RemoveUser( pUser );
		pViewPortNew->AddUser	( pUser );
	}

	return TRUE;

	/////////////////////////////////////////
laFailed:
	MessageOut( LOG_CRITICAL, "MoveViewPort SectorIndex error - [%s][f:%u][s:%u]", (LPCTSTR)pUser->GetCharName(), 
																					dwFieldCode, 
																					dwZoneID );
	return FALSE;
}


BOOL ChatFieldImpl::SendToNeighbors(DWORD dwZoneID, BYTE *pMsg, WORD wSize, DWORD dwExceptUserID)
{

	ViewPort *pViewPort = GetViewPort(dwZoneID);

	if(pViewPort == NULL)
	{
		MessageOut(LOG_CRITICAL, "No Neighbor Zone Error - [F:%u][Z:%u]", GetFieldCode(), dwZoneID);
		return FALSE;
	}

	pViewPort->SendToNeighbors( pMsg, wSize, dwExceptUserID );

	return TRUE;
}

class ChatFieldImplSendToAllOpr
{
public:
	BYTE* pMsg;
	WORD	wSize;
	DWORD dwExceptUserID;
	void operator()(ViewPort *pViewPort)
	{
		pViewPort->SendToZone( pMsg, wSize,dwExceptUserID );
	}
};

BOOL ChatFieldImpl::SendToAll( BYTE* pMsg, WORD wSize, DWORD dwExceptUserID )
{
	ChatFieldImplSendToAllOpr opr={pMsg, wSize,  dwExceptUserID};
	ForEach(opr);

	return TRUE;
}

DWORD ChatFieldImpl::GetFieldCode()
{
	if(!m_pFieldInfo)
	{
		return 0;
	}

	return m_pFieldInfo->GetFieldCode();
}

BOOL ChatFieldImpl::AddUser( User *pUser, DWORD dwZoneID )
{
	ViewPort *pViewPort = GetViewPort(dwZoneID);

	if(pViewPort == NULL)
	{
		MessageOut(LOG_CRITICAL, "No Neighbor Zone Error - [F:%u][Z:%u]", GetFieldCode(), dwZoneID);
		return FALSE;
	}

	pViewPort->AddUser( pUser );
	pUser->SetFieldCode( m_pFieldInfo->GetFieldCode() );

	MessageOut( LOG_FULL, "AddUser %s[Guid:%u][CharGuid:%u][ZT:%u][F:%u][Z:%u] Complete",(LPCTSTR) (LPCTSTR)pUser->GetCharName(), 
																				pUser->GetGUID(), 
																				pUser->GetCharGuid(), 
																				pUser->GetStatus(), 
																				pUser->GetFieldCode(), 
																				dwZoneID );

	return TRUE;
}
VOID ChatFieldImpl::RemoveUser( User *pUser )
{
	DWORD dwZoneID = pUser->GetSectorIndex();

	ViewPort *pViewPort = GetViewPort(dwZoneID);

	if(pViewPort == NULL)
	{
		MessageOut(LOG_CRITICAL, "No Neighbor Zone Error - [F:%u][Z:%u]", GetFieldCode(), dwZoneID);
		return;
	}

	pViewPort->RemoveUser( pUser );

	//pUser->SetFieldCode( 0 );
	pUser->SetSectorIndex( 0 );

	MessageOut( LOG_FULL, "RemoveUser %s[Guid:%u][CharGuid;%u][ZT:%u][F:%u][S:%u] Complete", (LPCTSTR)pUser->GetCharName(), 
															pUser->GetGUID(), 
															pUser->GetCharGuid(), 
															pUser->GetStatus(), 
															m_pFieldInfo->GetFieldCode(), 
															pViewPort->GetKey() );
}


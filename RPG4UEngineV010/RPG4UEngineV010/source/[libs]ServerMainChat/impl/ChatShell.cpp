/*////////////////////////////////////////////////////////////////////////
文 件 名：ChatShell.cpp
创建日期：2009年6月19日
最后更新：2009年6月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdafx.h"
#include "ChatShell.h"
#include "UserDBProxySession.h"
#include "ServerSessionManagerInstance.h"
//#include "ChatShellInfoParser.h"
#include "User.h"
#include "WaitingUserManager.h"
#include "UserManager.h"
#include "ZoneManager.h"
#include "ChannelManager.h"
#include <conio.h>
#include <NetworkGroup.h>
#include <iostream>
#include <process.h>
#include <PacketStruct_ClientWorldS.h>
#include <GMInfoParser.h>
#include "ViewPortWork.h"
#include <Version.h>
#include <BuildVersion.h>
#include <ServerSetting.h>
#include <mapinfoparser.h>

#include "ChatShell.inc.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ChatShell, ()  , gamemain::eInstPrioGameFunc);


ChatShell::ChatShell()
{

	m_dwSeverEnableFlags		= _BIT(PLAYER_DBPROXY)
									;
	m_pCountLog					= new ServerGroupLog;
}

ChatShell::~ChatShell()
{
	SAFE_DELETE(m_pCountLog);
}


VOID ChatShell::Release()
{
	/////////////////////////////
	//UserFactory::Instance()->Release();
	theChannelManager.Release();
	theGMInfoParser.Release();

	theZoneManager.Release();
	theUserManager.Release();
	theWaitingUserManager.Release();

	/////////////////////////////




//	ViewPortWork::Instance()->Release();



	_SUPER::Release();
}




BOOL ChatShell::_InitLog	(sSERVERLOG_INFO*	pSvrLogInfo)
{
	__BOOL_SUPER(_InitLog	(pSvrLogInfo));

	__INIT(_InitGameLog(GAMELOG, theServerSetting.m_szServerName));

	DISPMSG( "Creating CountLog File...\n" );
	StringHandle	sLogFile;
	sLogFile.Format(_T("Counter_%s_%d")
						,theServerSetting.m_szServerName
						,theServerSetting.m_dwServerID);

	m_pCountLog->Init	(theServerSetting.m_LogInfo.byLogOption
							,theServerSetting.m_LogInfo.byLogFileLevel
							,theServerSetting.m_LogInfo.szLogFilePath 
							,sLogFile);

	return TRUE;
}


BOOL ChatShell::_InitNetwork()
{
	__BOOL_SUPER(_InitNetwork());

	//_GAMELOGINIT(GameLogAgent)
	sNETWORK_INFO ioDesc[IOHANDLER_MAX];

	ioDesc[0].fnCreateAcceptedSession		= CreateClientSideAcceptedObject;
	ioDesc[0].fnDestroyAcceptedSession	= DestroyClientSideAcceptedObject;
	ioDesc[0].fnDestroyWorkingSession	= DestroyClientSideConnectedObject;

	ioDesc[1].fnCreateAcceptedSession		= CreateServerSideAcceptedObject;
	ioDesc[1].fnDestroyAcceptedSession	= DestroyServerSideAcceptedObject;
	ioDesc[1].fnDestroyWorkingSession	= DestroyServerSideConnectedObject;

	return _InitNetworkCS(ioDesc);


}

BOOL ChatShell::Init()
{
	__BOOL_SUPER(Init());







	theZoneManager		.Init(1000);
	theUserManager		.Init();
	theWaitingUserManager.Init();
	theChannelManager	.Init(10);


	////////////////////////////////////////////






	return TRUE;
}





VOID ChatShell::Run(DWORD dwDelay)
{

	if( theServerSetting.m_bServerInfoFromFile )
	{

		StartListen		();
	}

	_SUPER::Run(dwDelay);

}

void ChatShell::OnRunEnd		()
{
	if(m_pCountLog)
	{
		m_pCountLog->SetDisplayRunning(FALSE);
		m_pCountLog->SetFileRunning	(FALSE);
	}
	_SUPER::OnRunEnd();
}


BOOL ChatShell::FrameMove(DWORD dwTick)
{
	__BOOL_SUPER(FrameMove(dwTick));



	//////////////////////////////////////////
	theWaitingUserManager.Update();

	theChannelManager.Update();




	return TRUE;
}


VOID ChatShell::MaintainConnection()
{
	if(m_RunState != RUNSTATE_RUNNING)
		return;

	_SUPER::MaintainConnection();




	////////////////////////////////////////////

}





VOID ChatShell::ReadAndSendNotice()
{
	char szNotice[MAX_NOTICE_LENGTH+1];
	ZeroMemory( szNotice, sizeof(szNotice) );

	///////////////////////////////////////
	FILE *fp = fopen( "Notice.txt", "rt" );
	if( !fp )
	{
		MessageOut(LOG_CRITICAL,   "Notice.txt invalid." );
		return;
	}
	fread( szNotice, sizeof(char), MAX_NOTICE_LENGTH, fp );
	fclose( fp );

	MSG_CW_NOTICE_BRD noticeMsg;
	noticeMsg.m_byCategory	= CW_CHAT;
	noticeMsg.m_byProtocol	= CW_NOTICE_BRD;
	noticeMsg.wLen			= (WORD)strlen(szNotice);
	strncpy( noticeMsg.szMsg, szNotice, noticeMsg.wLen ); 

	//////////////////////////////////
	theUserManager.SendToAll( (BYTE*)&noticeMsg, noticeMsg.GetSize() );

	MessageOut(LOG_CRITICAL,   ">> notice all : %s", szNotice );
}	









VOID ChatShell::DisplayServerInfo()
{


	_SUPER::DisplayServerInfo();

	DISPMSG(" Client Connection: GuidList((( %d )))  CharNameList(%d)\n"
			 ,theUserManager.GetUserAmount()
			 ,theUserManager.GetNameUserAmount());

	///////////////////////////////////////////////
	theChannelManager.DisplayChannels();
}




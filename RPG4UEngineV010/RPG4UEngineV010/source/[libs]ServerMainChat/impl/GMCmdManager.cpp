/*////////////////////////////////////////////////////////////////////////
文 件 名：GMCmdManager.cpp
创建日期：2009年6月19日
最后更新：2009年6月19日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "stdAfx.h"
#include "GMCmdManager.h"
#include "User.h"
#include "UserManager.h"
#include <GMInfoParser.h>
#include <PacketStruct_ClientWorldS.h>
#include <ResultCode.h>
#include <ServerShell.h>
#include <ScriptWord.h>
#include "Zone.h"
#include "Channel.h"
#include "ChannelManager.h"


using namespace RC;
/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(GMCmdManager, ()  , gamemain::eInstPrioGameFunc);



GMCmdManager::GMCmdManager(void)
:m_pUser(NULL)
{

}

GMCmdManager::~GMCmdManager(void)
{

}


BOOL GMCmdManager::ParseCommand( User* pUser, LPCTSTR szCmd )
{
	m_pUser	= NULL;

	if( !IsGMUser( pUser ) )
	{
		LOGINFO( "Invalid GM !!!(%s)", (LPCTSTR)pUser->GetCharName());
		return false;
	}

	m_pUser = pUser;

	return theServerShell.ProcessConsole(szCmd);



}





BOOL GMCmdManager::IsGMUser( User *pUser )
{
	sGM_INFO* pGMInfo = theGMInfoParser.FindGM( (LPCTSTR)pUser->GetCharName() );

	if( !pGMInfo )
	{
		MSG_CW_GM_STRING_CMD_NAK msgNAK;
		msgNAK.m_byErrorCode =  RC_GM_ISNOT_GM;
		pUser->Send( (BYTE*)&msgNAK, sizeof(msgNAK) );
		return FALSE;
	}

	return TRUE;
}


BOOL GMCmdManager::PrevProcessCommand(ScriptWord& arCmds )
{
	m_arParams.resize(arCmds.GetWordCount());
	for(INT i = 0; i<arCmds.GetWordCount(); i++)
	{
		m_arParams[i] = arCmds[i];
	}
	return TRUE;
}

void GMCmdManager::_ConcatParams()
{

	m_szParamList = "";
	for(UINT i = 1; i<m_arParams.size(); i++)
	{
		if(m_szParamList.Length() + m_arParams[i].Length() > MAX_NOTICE_LENGTH)
			break;

		m_szParamList	+= m_arParams[i];
		m_szParamList	+= " ";
	}
}


BOOL GMCmdManager::ProcessWorldNotice( ScriptWord& arCmds )
{
	PrevProcessCommand(arCmds);



	_ConcatParams();

	////////////////////////////////////////////////////
	MSG_CW_NOTICE_BRD			msgNotice;
	msgNotice.wLen				= m_szParamList.Length();
	__ZERO(msgNotice.szMsg);
	lstrcpyn( msgNotice.szMsg, m_szParamList, msgNotice.wLen ); 

	///////////////////////////////////////////////
	theUserManager.SendToAll( (BYTE*)&msgNotice, msgNotice.GetSize() );



	LOGINFO(_T(">>>>>> World Notice -----------------------------\n") );
	LOGINFO(_T("%s(%u)\n"), msgNotice.szMsg, msgNotice.wLen );
	LOGINFO(_T("-------------------------------------------------\n") );

	return true;
}


BOOL GMCmdManager::ProcessChannelNotice(ScriptWord& arCmds )
{
	PrevProcessCommand(arCmds);




	///////////////////////////////////////////////////////
	MSG_CW_NOTICE_BRD msgNotice;
	msgNotice.wLen				= m_szParamList.Length();
	__ZERO(msgNotice.szMsg);
	lstrcpyn( msgNotice.szMsg, m_szParamList, msgNotice.wLen ); 

	/////////////////////////////////////////
	Channel *pChannel = theChannelManager.GetChannel( m_pUser->GetChannelID() );
	if(!pChannel)
	{
		LOGMSG(LOG_CRITICAL
            ," %s at Invalid Channel %u!!"
				,(LPCTSTR)m_pUser->GetCharName()
				,m_pUser->GetChannelID());
		return false;
	}

	//////////////////////////////////////////
	pChannel->SendToAll( (BYTE*)&msgNotice, msgNotice.GetSize() );


	LOGINFO(  ">>>>>>>>>>>>>>>> Channel[%d] >>----------------------------\n", m_pUser->GetChannelID() );
	LOGINFO(  "%s(%u)\n", msgNotice.szMsg, msgNotice.wLen );
	LOGINFO(  "------------------------------------------------------------------------\n" );

	return true;
}


BOOL GMCmdManager::_ProcessZoneNotice	()
{
	_ConcatParams();

	/////////////////////////////////////////
	MSG_CW_NOTICE_BRD msgNotice;
	msgNotice.wLen				= m_szParamList.Length();
	__ZERO(msgNotice.szMsg);
	lstrcpyn( msgNotice.szMsg, m_szParamList, msgNotice.wLen ); 


	/////////////////////////////////////////
	Channel *	pChannel;
	Zone *		pZone = NULL;
	

	/////////////////////////////////////////
	pChannel = theChannelManager.GetChannel( m_pUser->GetChannelID() );
	if(!pChannel)
	{
		LOGMSG(LOG_CRITICAL
            ,"Invalid channel %s %u!!"
				,(LPCTSTR)m_pUser->GetCharName()
				,m_pUser->GetChannelID());
		return false;
	}

	/////////////////////////////////////////
	pZone = pChannel->_FindZone(m_pUser->GetZoneKey(), m_pUser->GetStatus());
	if(!pZone)
	{
		LOGMSG(LOG_CRITICAL
            ,"invalid zone %s %u!!"
				,(LPCTSTR)m_pUser->GetCharName()
				,m_pUser->GetZoneKey());
		return false;
	}


	/////////////////////////////////////////
	pZone->SendToAll( (BYTE*)&msgNotice, msgNotice.GetSize() );


	/////////////////////////////////////////
	LOGINFO(  "---------------------------<< Channel[%u] Zone[%u] >>-------------------------\n", 
									m_pUser->GetChannelID(), 
									m_pUser->GetZoneKey() );
	LOGINFO(  "%s(%u)\n", msgNotice.szMsg, msgNotice.wLen );
	LOGINFO(  "------------------------------------------------------------------------\n" );

	return true;
}

BOOL GMCmdManager::ProcessZoneNotice(ScriptWord& arCmds )
{
	PrevProcessCommand(arCmds);



	/////////////////////////////////////////////////////
	switch(m_pUser->GetStatus())
	{
	case ZONETYPE_CHARSELECT:
		{
			LOGMSG( LOG_CRITICAL, "invalid cmd(%s)", (LPCTSTR)m_pUser->GetCharName() );
			MSG_CW_GM_STRING_CMD_NAK	msgNAK;
			msgNAK.m_byErrorCode = 1;
			m_pUser->Send( (BYTE*)&msgNAK, sizeof(MSG_CW_GM_STRING_CMD_NAK) );
			return false;
		}
	default:
		return _ProcessZoneNotice();
	}



}

/*////////////////////////////////////////////////////////////////////////
文 件 名：ChatShellLogInstance.cpp
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "ChatShellLogInstance.h"
#include "LogWriteManager.h"


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#define _GAMELOG_INIT(DATA)\
						if(!IsRun() )	\
							return;\
						DATA.Init();\
						LogTitle(&DATA,szAccountID, szCharName)


#define _GAMELOG_CHAT2(code)		_GAMELOG_INIT(m_ChatData );\
											m_ChatData.nChannelIndex	= dwServerCode;\
											m_ChatData.dwChatLogCode	= (code);\
											lstrcpyn(m_ChatData.szChatContext,szChatMsg,MAX_CHAT_LENGTH);\
											m_ChatData.szChatContext[MAX_CHAT_LENGTH-1] =0

#define _GAMELOG_CHAT(code)		_GAMELOG_CHAT2(code );\
											m_ChatData.dwMapCode = mapCode

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(ChatShellLogInstance, ()  , gamemain::eInstPrioGameFunc);


ChatShellLogInstance::ChatShellLogInstance(void)
{
}

ChatShellLogInstance::~ChatShellLogInstance(void)
{
}


//////////////////////////////////////////////////////////////////////////
void ChatShellLogInstance::LogVillageChat	(LPCTSTR szAccountID
                                 ,LPCTSTR szCharName
											,DWORD dwServerCode
											,MAPCODE  mapCode
											,LPCTSTR szChatMsg)
{
	_GAMELOG_CHAT(LOGCHAT_EYERANGE);

	m_pLogWrite->LogChat( m_ChatData, m_bWriteText, NULL );
}


void ChatShellLogInstance::LogShoutChat	(LPCTSTR szAccountID
                              ,LPCTSTR szCharName
										,DWORD dwServerCode
										,MAPCODE  mapCode
										,LPCTSTR szChatMsg)
{
	_GAMELOG_CHAT(LOGCHAT_SHOUT);

	m_pLogWrite->LogChat( m_ChatData, m_bWriteText, NULL );
}


void ChatShellLogInstance::LogBattleZoneChat(LPCTSTR szAccountID
                                  ,LPCTSTR szCharName
											 ,int     nRoomNo
											 ,DWORD dwServerCode
											 ,MAPCODE  mapCode
											 ,LPCTSTR szChatMsg)
{
	_GAMELOG_CHAT(LOGCHAT_BATTLEZONE);

	m_ChatData.dwRoomCode = nRoomNo;

	m_pLogWrite->LogChat( m_ChatData, m_bWriteText, NULL );
}


void ChatShellLogInstance::LogGuildChat(LPCTSTR szAccountID
                             ,LPCTSTR szCharName
									  ,LPCTSTR szGuildName
									  ,DWORD dwServerCode
									  ,MAPCODE  mapCode
									  ,LPCTSTR szChatMsg)
{
	_GAMELOG_CHAT(LOGCHAT_GUILD);

	lstrcpyn( m_ChatData.szToCharName, szGuildName,  MAX_CHARNAME_LENGTH );

	m_pLogWrite->LogChat( m_ChatData, m_bWriteText, NULL );
}


void ChatShellLogInstance::LogWhisperChat(LPCTSTR szAccountID
                               ,LPCTSTR szCharName
										 ,LPCTSTR szRecvCharName
										 ,DWORD dwServerCode
										 ,LPCTSTR szChatMsg)
{

	_GAMELOG_CHAT2(LOGCHAT_PRIVATE);

	lstrcpyn( m_ChatData.szToCharName, szRecvCharName,  MAX_CHARNAME_LENGTH );

	m_pLogWrite->LogChat( m_ChatData, m_bWriteText, NULL );
}

// 6. 
void ChatShellLogInstance::LogFriendChat(LPCTSTR szAccountID
                              ,LPCTSTR szCharName
										,LPCTSTR szRecvCharName
										,DWORD dwServerCode
										,LPCTSTR szChatMsg)
{
	_GAMELOG_CHAT2(LOGCHAT_FRIEND);

	lstrcpyn( m_ChatData.szToCharName, szRecvCharName,  MAX_CHARNAME_LENGTH );

	m_pLogWrite->LogChat( m_ChatData, m_bWriteText, NULL );
}


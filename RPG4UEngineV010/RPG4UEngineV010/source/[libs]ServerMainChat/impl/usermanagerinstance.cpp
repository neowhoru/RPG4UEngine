/*////////////////////////////////////////////////////////////////////////
文 件 名：UserManagerInstance.cpp
创建日期：2009年4月5日
最后更新：2009年4月5日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "UserManagerInstance.h"
#include "UserFactoryManager.h"
#include "User.h"
#include "KeyUserList.h"
#include "NameUserList.h"
#include "StructBase.h"
#include "ServerInfoParser.h"

#include "ActiveUser.h"
#include "TempUser.h"



/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
GLOBALINST_SINGLETON_IMPL(UserManagerInstance, ()  , gamemain::eInstPrioGameFunc);


UserManagerInstance::UserManagerInstance(void)
{
	m_pUserList		= new KeyUserList;
	m_pKeyUserList		= new KeyUserList;
	m_pNameUserList = new NameUserList;
}

UserManagerInstance::~UserManagerInstance(void)
{
	SAFE_DELETE( m_pUserList );
	SAFE_DELETE( m_pKeyUserList );
	SAFE_DELETE( m_pNameUserList );

	Release();
}

BOOL UserManagerInstance::Init()
{
	//@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
	sIOHANDLER_INFO *pDesc;
	pDesc = theServerInfoParser.GetClientIoDesc();

	REG_USERFACTORY(ActiveUser,	ACTIVE_USER , pDesc->dwMaxAcceptSession);
	REG_USERFACTORY(TempUser,		TEMP_USER	, pDesc->dwMaxAcceptSession);

	m_pUserList->Init();
	m_pKeyUserList->Init();
	m_pNameUserList->Init();

	return _SUPER::Init();
}

VOID UserManagerInstance::Release()
{
	if(!m_pUserList)
		return;

	m_pUserList->Release();
	m_pKeyUserList->Release();
	m_pNameUserList->Release();

	ClearFriends();

	_SUPER::Release();
}





VOID UserManagerInstance::AddUser( DWORD dwGuid, User *pUser )
{
	m_pUserList->Add( dwGuid, pUser );

		////////////////////////////////////////////
	if( !(LPCTSTR)pUser->GetCharName().IsBlank() )
	{
		m_pNameUserList->Add( (LPCTSTR)pUser->GetCharName(), pUser );
	}

		////////////////////////////////////////////
	if( pUser->GetCharGuid() )
	{
		m_pKeyUserList->Add( pUser->GetCharGuid(), pUser );
	}
}

VOID UserManagerInstance::RemoveUser( DWORD dwGuid )
{
	User *pUser = m_pUserList->Find( dwGuid );
	if( pUser )
	{
		m_pUserList->Remove( dwGuid );

		////////////////////////////////////////////
		if( !(LPCTSTR)pUser->GetCharName().IsBlank() )
		{
			m_pNameUserList->Remove( (LPCTSTR)pUser->GetCharName() );
		}

		if( pUser->GetCharGuid() )
		{
			m_pKeyUserList->Remove( pUser->GetCharGuid() );
		}
	}
}


BOOL UserManagerInstance::SetUserCharGuid( DWORD dwGuid, DWORD dwCharGuid )
{
	User *pUser = m_pUserList->Find( dwGuid );
	if( !pUser )
		return FALSE;

	////////////////////////////////////////////
	if( m_pKeyUserList->Find( pUser->GetCharGuid() ) )
	{
		m_pKeyUserList->Remove( pUser->GetCharGuid() );
		MessageOut( LOG_FULL,  "user existed(%u)  ", pUser->GetCharGuid() );
	}

	////////////////////////////////////////////
	pUser->SetCharGuid( dwCharGuid );
	
	////////////////////////////////////////////
	m_pKeyUserList->Add( dwCharGuid, pUser );

	MessageOut( LOG_FULL, "CharGuid(%u)", dwCharGuid );

	return TRUE;
}

BOOL UserManagerInstance::SetUserCharName( DWORD dwGuid, LPCSTR strCharName )
{
	User *pUser = m_pUserList->Find( dwGuid );
	if( !pUser ) return FALSE;

	MessageOut( LOG_CRITICAL,  "Guid(%u)CharGuid(%u)Name(%s)", dwGuid, pUser->GetCharGuid(), (LPCTSTR)pUser->GetCharName() );

	////////////////////////////////////////////
	if( m_pNameUserList->Find( (LPCTSTR)pUser->GetCharName() ) )
	{
		m_pNameUserList->Remove( (LPCTSTR)pUser->GetCharName() );
		MessageOut( LOG_FULL,  "SetUserCharName(%s)  ", (LPCTSTR)pUser->GetCharName() );
	}

	////////////////////////////////////////////
	pUser->SetCharName( strCharName );
	
	////////////////////////////////////////////
	m_pNameUserList->Add( strCharName, pUser );

	MessageOut(LOG_FULL,  "SetUserCharName(%s)", strCharName );

	return TRUE;
}

BOOL UserManagerInstance::UnSetCharGuid( DWORD dwGuid )
{
	User *pUser = m_pKeyUserList->Find( dwGuid );
	if( !pUser ) return FALSE;

	////////////////////////////////////////////
	m_pKeyUserList->Remove( pUser->GetCharGuid() );

	return TRUE;
}

BOOL UserManagerInstance::UnsetUserCharName( DWORD dwGuid )
{
	User *pUser = m_pUserList->Find( dwGuid );
	if( !pUser ) return FALSE;

	////////////////////////////////////////////
	m_pNameUserList->Remove( (LPCTSTR)pUser->GetCharName() );
	
	return TRUE;
}

User* UserManagerInstance::FindUser( DWORD dwGuid )
{
	return m_pUserList->Find( dwGuid );
}

User* UserManagerInstance::FindUserBy( DWORD CharGuid )
{
	return m_pKeyUserList->Find( CharGuid );
}

User* UserManagerInstance::FindUser( LPCSTR strCharName )
{
	return m_pNameUserList->Find( strCharName );
}

VOID UserManagerInstance::SendToAll( BYTE *pMsg, WORD wSize, DWORD dwExceptUserID )
{
	m_pUserList->SendToAll( pMsg, wSize, dwExceptUserID );
}

DWORD UserManagerInstance::GetUserAmount()
{
	return m_pUserList->GetUserAmount();
}

DWORD UserManagerInstance::GetNameUserAmount()
{
	return m_pNameUserList->GetUserAmount();
}
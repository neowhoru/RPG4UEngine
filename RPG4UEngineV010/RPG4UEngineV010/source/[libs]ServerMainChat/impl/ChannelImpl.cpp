/*////////////////////////////////////////////////////////////////////////
文 件 名：ChannelImpl.cpp
创建日期：2009年4月4日
最后更新：2009年4月4日
编 写 者：亦哥(Leo/李亦)
          liease@163.com
			 qq:4040719
功能描述：



版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
		http://www.rpg4u.com 下载。
		
		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。
		
	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。
				
						2.公益使用授权
				如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。
			
		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#include "StdAfx.h"
#include "ChannelImpl.h"
#include "ZoneManager.h"
#include "ZoneImpl.h"
#include "User.h"
#include "UserManager.h"
#include "ViewPortWork.h"
#include "ViewPortSystem.h"

#include "ChannelManager.h"

ChannelImpl::ChannelImpl(void)
{
	m_RoomKeyGenerator.Create( 1, 10000 );
}

ChannelImpl::~ChannelImpl(void)
{
	//SAFE_DELETE(m_pCharSceneZone);
}


BOOL ChannelImpl::Init()
{
	m_pCharSceneZone	= theZoneManager.AllocZone();

	m_pCharSceneZone->Init();
	m_pCharSceneZone->SetKey( 0 );
	m_pViewPortWork = theViewPortSystem.AllocWork();
	m_pViewPortWork->LoadFieldMaps();
	return TRUE;
}

VOID ChannelImpl::Release()
{

	RemoveUsers();

	///////////////////////////////////////////
	if(m_pCharSceneZone)
	{
		theZoneManager.FreeZone(m_pCharSceneZone);
		//m_pCharSceneZone->Release();
		m_pCharSceneZone = NULL;
	}


	RemoveZones();

	if(m_pViewPortWork)
	{
		//m_pViewPortWork->Release();
		theViewPortSystem.FreeWork(m_pViewPortWork);
		m_pViewPortWork = NULL;
	}

	////////////////////////////////////////
	m_dwVillageUserNum = 0;
	m_dwLobbyUserNum = 0;
	m_dwMissionUserNum = 0;
	m_dwHuntingUserNum = 0;
	m_dwQuestUserNum = 0;
	m_dwPVPUserNum = 0;
	m_dwEventUserNum = 0;
	m_dwGuildUserNum = 0;
	m_dwSiegeUserNum = 0;

}

VOID ChannelImpl::Update()
{
}

LPCTSTR ChannelImpl::GetChannelName	()
{
	if(m_pGroupInfo)
		return (LPCTSTR)m_pGroupInfo->m_Name;
	return _T("");
}







/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChannelImpl_SendToZonesOpr
{
public:
	BYTE *pMsg;
	WORD wSize;
	DWORD dwExceptUserID;
	void operator()(Zone* pZone)
	{
		pZone->SendToAll( pMsg, wSize,dwExceptUserID );
	}
};
BOOL ChannelImpl::_SendToZones	( BYTE *pMsg, WORD wSize, eZONE_TYPE	type, DWORD dwExceptUserID )
{
	__CHECK_RANGE(type,MAX_ZONE_TYPE);

	ChannelImpl_SendToZonesOpr opr={pMsg, wSize, dwExceptUserID};
	ForEachZone(type,opr);

	return TRUE;
}

BOOL ChannelImpl::_IncZoneUserNum( eZONE_TYPE	type)
{
	__CHECK_RANGE(type,MAX_ZONE_TYPE);
	m_arUserNums[type] ++;
	return TRUE;
}

BOOL ChannelImpl::_DecZoneUserNum( eZONE_TYPE	type)
{
	__CHECK_RANGE(type,MAX_ZONE_TYPE);

	if( m_arUserNums[type] > 0 ) 
		m_arUserNums[type]--; 

	return TRUE;
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
Zone* ChannelImpl::FindZone( eZONE_TYPE eZoneType, DWORD dwZoneKey )
{
	switch( eZoneType )
	{
	case ZONETYPE_CHARSELECT:
		return m_pCharSceneZone;
	default:
		return _FindZone(dwZoneKey,eZoneType);
	}
}




VOID ChannelImpl::AddUser( User *pUser, eZONE_TYPE userStatus, DWORD dwZoneKey )
{
//	pUser->SetStatus( userStatus );

	switch( userStatus )
	{
	case ZONETYPE_CHARSELECT:
		UserEnterCharScene( pUser );
		break;
	case ZONETYPE_VILLAGE:
		UserEnterVillage( pUser, dwZoneKey );
		break;
	case ZONETYPE_LOBBY:
		UserEnterLobby( pUser, dwZoneKey );
		break;
	case ZONETYPE_MISSION:
		UserEnterMissionZone( pUser, dwZoneKey );
		break;
	case ZONETYPE_HUNTING:
		UserEnterHuntingZone( pUser, dwZoneKey );
		break;
	case ZONETYPE_QUEST:
		UserEnterQuestZone( pUser, dwZoneKey );
		break;
	case ZONETYPE_PVP:
		UserEnterPVPZone( pUser, dwZoneKey );
		break;
	case ZONETYPE_EVENT:
		UserEnterEventZone( pUser, dwZoneKey );
		break;
	case ZONETYPE_GUILD:
		UserEnterGuildZone( pUser, dwZoneKey );
		break;
	case ZONETYPE_SIEGE:
		UserEnterSiegeZone( pUser, dwZoneKey );
		break;
	default:

		LOGINFO	("[Guid:%u] ChannelImpl::AddUser Error - No ZoneType[%u][ZK:%u]\n"
					,userStatus
					,dwZoneKey);
#ifndef _DEBUG
		pUser->DoLogout();
		return;
#endif
		break;
	}

	/////////////////////////////////
	_AddUser( pUser->GetGUID(), pUser  );

	/////////////////////////////////
	theUserManager.AddUser( pUser->GetGUID(), pUser );


	LOGMSG	(LOG_FULL
				,"ChannelImpl::AddUser(%d) ! [Guid:%u][CharName:%s][Status:%d]"
				,GetChannelID()
				,pUser->GetGUID()
				,(LPCTSTR)pUser->GetCharName()
				,userStatus);
}


VOID ChannelImpl::RemoveUser( User *pUser )
{
	LOGMSG( LOG_FULL
			, "ChannelImpl::RemoveUser(%d) [Guid:%u][CharName:%s][Status:%d]"
			,GetChannelID()
			, pUser->GetGUID()
			, (LPCTSTR)pUser->GetCharName()
			, pUser->GetStatus() );

	/////////////////////////////////////////////////
	if(	pUser->GetStatus() >= ZONETYPE_LOBBY 
		&& pUser->GetStatus() < ZONETYPE_MAX )
	{
		UserLeavePrevZone( pUser );
	}

	m_pViewPortWork->RemoveUser( pUser );

	_EraseUser(pUser->GetGUID());

	theUserManager.RemoveUser( pUser->GetGUID() );
}


BOOL ChannelImpl::AddViewPortUser( User *pUser, DWORD dwField, DWORD dwSector )
{
	return m_pViewPortWork->AddUser( pUser, dwField, dwSector );
}

BOOL ChannelImpl::MoveViewPortUser( User *pUser, DWORD dwField, DWORD dwSector )
{
	return m_pViewPortWork->MoveUser( pUser, dwField, dwSector );
}

VOID ChannelImpl::RemoveViewPortUser( User *pUser )
{
	m_pViewPortWork->RemoveUser( pUser );
}

VOID ChannelImpl::UserLeavePrevZone( User *pUser )
{
	LOGMSG( LOG_MIDDLE
			, "UserLeaveZone[ZoneType:%u][Key:%u][Guid:%u][CharGuid:%u]"
			, pUser->GetStatus()
			, pUser->GetZoneKey()
			, pUser->GetGUID() 
			, pUser->GetCharGuid() );

	//////////////////////////////////////////////////
	switch( pUser->GetStatus() )
	{
	case ZONETYPE_CHARSELECT:
		{
			m_pCharSceneZone->RemoveUser( pUser->GetGUID() );
		}
		break;
	default:
		{
			_UserLeaveZone(pUser );
		}break;
	}
}

VOID ChannelImpl::UserEnterCharScene( User *pUser )
{
	if( pUser->GetStatus() >= ZONETYPE_LOBBY && pUser->GetStatus() < ZONETYPE_MAX )
	{
		LOGMSG	(LOG_CRITICAL
						,"User Leave CharSceneEnter Error[%s][Guid:%u][CharGuid:%u]"
						,(LPCTSTR)pUser->GetCharName()
						,pUser->GetGUID()
						,pUser->GetCharGuid());
		UserLeavePrevZone( pUser );
	}

	pUser->SetStatus( ZONETYPE_CHARSELECT );

	m_pCharSceneZone->AddUser( pUser->GetGUID(), pUser );
	LOGMSG( LOG_FULL
			, "UserEnterCharScene(%s:%u)"
			, (LPCTSTR)pUser->GetCharName()
			, pUser->GetGUID() );
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChannelImplSendToAllOpr
{
public:
	BYTE *pMsg;
	WORD wSize;
	DWORD dwExceptUserID;
	void operator()(User *pUser)
	{
		if(dwExceptUserID != 0 && pUser->GetLoginUserID() == dwExceptUserID)
			return;
		pUser->Send( pMsg, wSize );
	}
};

VOID ChannelImpl::SendToAll( BYTE *pMsg, WORD wSize, DWORD dwExceptUserID )
{
	ChannelImplSendToAllOpr opr={pMsg, wSize, dwExceptUserID};
	ForEachUser(opr);
}

BOOL ChannelImpl::SendToOneVillage(DWORD dwFieldCode
                                  ,BYTE* pMsg
											 ,WORD  wSize
											 , DWORD dwExceptUserID)
{
	return m_pViewPortWork->SendToAll( dwFieldCode, pMsg, wSize,dwExceptUserID );
}


BOOL ChannelImpl::SendToViewPort(User* pUser
                                ,BYTE* pMsg
										  ,WORD  wSize
										  , DWORD dwExceptUserID)
{
	return m_pViewPortWork->SendViewPort( pUser, pMsg, wSize , dwExceptUserID);
}


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
class ChannelImplDisplayInfoVillageOpr
{
public:
	DWORD dwNumberOfVillageUsers;
	void operator()(Zone			*pZone)
	{
		dwNumberOfVillageUsers += pZone->GetUserAmount();
	}
};

class ChannelImplDisplayInfoBattleOpr
{
public:
	DWORD dwNumberOfBattleUsers;
	void operator()(Zone			*pZone)
	{
		dwNumberOfBattleUsers += pZone->GetUserAmount();
	}
};


VOID ChannelImpl::DisplayInfo()
{
	ChannelImplDisplayInfoVillageOpr	villageOpr={0};
	ChannelImplDisplayInfoBattleOpr	battleOpr={0};

	DWORD& dwNumberOfVillageUsers	= villageOpr.dwNumberOfVillageUsers;
	DWORD& dwNumberOfBattleUsers	= battleOpr.dwNumberOfBattleUsers;

	//////////////////////////////////////////////
	ZoneHashMapIt	it;
	//Zone			*pZone;


	//////////////////////////////////////////////
	ForEachZone(ZONETYPE_VILLAGE,villageOpr);

	//////////////////////////////////////////////
	for(INT n=0; n<ZONETYPE_MAX; n++)
	{
		if(	n == ZONETYPE_VILLAGE 
			||	n == ZONETYPE_CHARSELECT )
			continue;

		ForEachZone((eZONE_TYPE)n,battleOpr);
	}


	//////////////////////////////////////////////
	LOGMSG(LOG_CRITICAL,   "ChannelImpl No %d: ", GetChannelID() );
	LOGMSG(LOG_CRITICAL
             ,"Total(%d) CharScene(%d) Village(%d) Battle(%d)"
				 ,GetTotalUserNum()
				 ,m_pCharSceneZone->GetUserAmount()
				 ,dwNumberOfVillageUsers
				 ,dwNumberOfBattleUsers);//
}
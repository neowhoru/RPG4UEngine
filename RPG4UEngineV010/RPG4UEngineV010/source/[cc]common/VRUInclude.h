/*////////////////////////////////////////////////////////////////////////
文 件 名：	winInc.h
创建日期：	2007.10.22
最后更新：	2007.10.2
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：
	本文件只包括公用的头文件
		SystemLib
		Define
		Namespace
		winInc
	经常修改的，请别包进来

版权所有：rpg4u.com
/*////////////////////////////////////////////////////////////////////////
#pragma once


/////////////////////////////////////////////////////////////
//#pragma push_macro("new")
//#pragma push_macro("delete")
//#undef new
//#undef delete
//
//#include <windows.h>
//
//#pragma pop_macro("delete")
//#pragma pop_macro("new")
/////////////////////////////////////////////////////////////



//#include "VRUSystemLibInc.h"
//
//#include "VRUExceptionDefine.h"
#include "VRUDefine.h"
//#include "VRUFileSystemDefine.h"
#include "VRUAssert.h"
#include "VRUNamespace.h"
//#include "winInc.h"
#include "stdInc.h"

//这里不要加内存拦截处理，可在工程的stdafx.h加，如需要的话
//#include "VRUMemoryHandler.h"

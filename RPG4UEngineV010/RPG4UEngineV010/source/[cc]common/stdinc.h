/*////////////////////////////////////////////////////////////////////////
文 件 名：	stdInc.h
创建日期：	2007.10.16
最后更新：	2007.10.16
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：


版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#pragma once

#include "DataTypeDefine.h"
//#include "VRUMemoryHandler.h"
//#include "tIterator.h"


//////////////////////////////////////////////////////////////////////
#ifdef _VRUMEMORYHANDLER_H_
#	pragma message(__FILE__ "(21) 使用VRUMemoryHandler.h，禁止使用std[new]系列内存操作")
	#define	_NEW_			///> 不使用std内存分配，避免内存问题
	#define _FSTREAM_		///> 不使用std::fstream，避免内存问题
	#define _SSTREAM_		///> 不使用std::sstream，避免内存问题



//取消之前的任何new定义，还原原始的new函数
///////////////////////////////////////////////////////////
#pragma push_macro("new")
#pragma push_macro("delete")
#undef new
#undef delete

#include "stdxmemory.h"
#include "stdxdebug.h"
#include <xlocale>
#include <xiosbase>
#include <xtree>

#pragma pop_macro("delete")
#pragma pop_macro("new")
/////////////////////////////////////////////////////////////
//#undef _XMEMORY_

/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#else
	#pragma message(__FILE__ "(47) 未使用VRUMemoryHandler.h，默认情况下，允许使用std[new]系列内存操作...")
#endif


/*////////////////////////////////////////////////////////////////////////
/*////////////////////////////////////////////////////////////////////////
#include <xutility>
#include <string>
#include <vector>
#include <map>
#include <hash_map>
#include <algorithm>
#include <exception>

#include <deque>
#include <list>
#include <functional>
#include <stack>
#include <fstream>
#include <queue>
#include <set>
#include <ctime>
#include <utility>
#include <xstring>
#include <cctype>


//////////////////////////////////////////////////////////////////////
//VRU_BEGIN
//
////////////////////////////////////////////////////////////////////////
//class DataStream;
//class FileDataStream;
//class IPackage;
//class PackageFactory;
//
//struct FileInfo;

#define Vector													std::vector
//////////////////////////////////////////////////////////////////////
///StdString
typedef std::string											StdString;
typedef StdString::size_type								StringSize;
#define STRING_NPOS											StdString::npos
//typedef std::vector<StdString>							StringVector;
typedef std::multimap<StdString, StdString>			StrStrMultiMap;

typedef std::vector<void*>									VoidPtrVector;
typedef VoidPtrVector::iterator							VoidPtrVectorIt;
typedef std::vector<char*>									LPSTRVector;

typedef std::map<StdString,int>							StringIntMap;
typedef StringIntMap::iterator							StringIntMapIt;

typedef std::map<int,LPCSTR>								IntLPCSTRMap;
typedef IntLPCSTRMap::iterator							IntLPCSTRMapIt;

typedef std::map<void*,void*>								VoidPtrSelfMap;
typedef std::pair<void*,void*>							VoidPtrSelfMapPair;
typedef VoidPtrSelfMap::iterator							VoidPtrSelfMapIt;

/// VRSTR
typedef std::map<VRSTR,int>								VRSTRIntMap;
typedef VRSTRIntMap::iterator								VRSTRIntMapIt;
typedef std::map<VRSTR,void*>								VRSTRVoidPtrMap;
typedef VRSTRVoidPtrMap::iterator						VRSTRVoidPtrMapIt;


///StdString, HANDLE
typedef stdext::hash_map<StdString, HANDLE>			StrHandleMap;
typedef std::pair<StdString, HANDLE>					StrHandleMapPair;
typedef StrHandleMap::iterator							StrHandleMapIt;

///DWORD, void*
typedef stdext::hash_map<DWORD, void*>					DwordPtrMap;
typedef std::pair<DWORD, void*>							DwordPtrMapPair;
typedef DwordPtrMap::iterator								DwordPtrMapIt;


///Settings
//typedef StrStrMultiMap										SettingsMultiMap;
//typedef MapIterator<SettingsMultiMap>					SettingsIterator;
//typedef std::map<StdString, SettingsMultiMap*>		SettingsBySection;
//typedef MapIterator<SettingsBySection>					SectionIterator;

///ostringstream
//typedef std::ostringstream									OSStrStream;

///Exception
//typedef std::exception										ExceptionBase;

//FileInfo
//typedef std::vector<FileInfo>								FileInfoList;

//PackageMap
//typedef std::map<StdString, PackageFactory*>				PackageFactoryMap;
//typedef std::map<StdString, IPackage*>						PackageMap;
//typedef std::vector<IPackage*>							PackageVector;
//typedef PackageVector::iterator							PackageVectorIt;
//typedef PackageVector::reverse_iterator				PackageVectorItR;


//////////////////////////////////////////////////////////////////////
#define	AutoPtr							std::auto_ptr

//typedef AutoPtr<DataStream>			DataStreamPtr;
//typedef AutoPtr<FileDataStream>		FileDataStreamPtr;
typedef AutoPtr<char>					TextBufferPtr;
typedef AutoPtr<BYTE>					DataBufferPtr;

//typedef AutoPtr<FileInfoList>			FileInfoListPtr;
//typedef AutoPtr<StringVector>			StringVectorPtr;


//////////////////////////////////////////////////////////////////////
//VRU_END

/*////////////////////////////////////////////////////////////////////////
文 件 名：	VRUMemory.h
创建日期：	2007.10.16
最后更新：	2007.10.16
编 写 者：	李亦
				liease@163.com	
				qq:4040719
功能描述：
	掌管operator new系列的内存操作
	拦截malloc realloc free之类的内存操作




版权所有：RPG4U(尔游)工作室 

许可协议：
		本引擎遵照<RPG4U引擎使用许可协议书>约定。
		当你取得本套源码包时，记得要认真地阅读<RPG4U引擎使用许可协议书>，可到
	http://www.rpg4u.com 下载。

		如果你不接受这些约定，请删除你手上所有相关的资料、源码包等数据。
		不可使用本引擎做任何违法事情，由此带来的任何责任你须自担。
		本引擎本身存在相关侵权问题，则由我们来承担责任。

	以下有三类授权许可约定：
		1.个人使用授权
			在你未得到商业使用授权之前，间接或直接获得源码包时，将默认为你是贡
		献者，则视为自动接受个人使用授权。
			你可以把自己的创作、作品等作各类非商业性合法传播、演示、发布等。

		注：	在本源码包上所做修改、拓展等源码，也须用同样的个人使用许可约定，
				共享或授权于其他任何贡献者使用。
				当然，不可用于商业用途。

		2.公益使用授权
			如果你是在个人使用授权情况下得到源码包，同时将贡献者版本、应用源码等
		用于公益性活动、普通教育等公益事业中，则视为接受公益使用授权。
			可参考个人使用授权。

		3. 商业使用授权
			你须得到我们商业使用授权认可后，无论你之前是间接或直接获得源码包，都可
		以在授权约定范畴内，对源码包做修改、创作、部署等学术性、营利性等各类合法活动。
			不可做引擎二次商业授权。
			不可使用本引擎或相关源码包等做营利性宣传。
/*////////////////////////////////////////////////////////////////////////
#pragma once
#ifdef _VRUMEMORYHANDLER_H_


#ifndef _VRUMEMORYHANDLER_H_
#define _VRUMEMORYHANDLER_H_

//#include "stdInc.h"
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>

#include "VRUNamespace.h"
#include "BaseLibExport.h"
#include "SystemUtil.h"

//#include "VRUExceptionDefine.h"
//#include "VRUMemoryManager.h"

#include "VRUDebugNew.h"

#define __UNUSED(x)		(x);
/////////////////////////////////////////////////////////////
//以下为free Allocate realloc处理


//赋予新定义
//**
//#pragma message(__FILE__ "(34) 此处 Allocate宏定义，可能会引起xdebug中free内存问题，若发生问题，可去取消Allocate系列的宏定义")
#ifdef Allocate
#undef Allocate
#endif
#ifdef free
#undef free
#endif
#ifdef realloc
#undef realloc
#endif


////////////////////////////////////////////////////////////////////
/// System系列的内存函数
#ifdef _DEBUG
#define malloc(size)			VRU::SystemUtil::SysMemoryAlloc(size,__FILE__,__LINE__)
#define realloc(ptr,size)	VRU::SystemUtil::SysMemoryRealloc(ptr,size,__FILE__,__LINE__)
#else
#define malloc(size)			VRU::SystemUtil::SysMemoryAlloc(size)
#define realloc(ptr,size)	VRU::SystemUtil::SysMemoryRealloc(ptr,size)
#endif
#define free(ptr)				VRU::SystemUtil::SysMemoryFree(ptr)


////////////////////////////////////////////////////////////////////
/// VRU系列的内存函数
#ifdef _DEBUG
#define VRUMalloc(size)			VRU::SystemUtil::SysMemoryAlloc(size,__FILE__,__LINE__)
#define VRURealloc(ptr,size)	VRU::SystemUtil::SysMemoryRealloc(ptr,size,__FILE__,__LINE__)
#else
#define VRUMalloc(size)			VRU::SystemUtil::SysMemoryAlloc(size)
#define VRURealloc(ptr,size)	VRU::SystemUtil::SysMemoryRealloc(ptr,size)
#endif
#define VRUFree(ptr)				VRU::SystemUtil::SysMemoryFree(ptr)


//*/


/////////////////////////////////////////////////////////////
#pragma push_macro("new")
#pragma push_macro("delete")
#undef new
#undef delete

/////////////////////////////////////////////////////////////
//以下为operator new
inline void*__cdecl operator new(size_t size) 
{ 
	return VRUMalloc(size); 
}

inline void*__cdecl operator new(size_t size, void* p) 
{ 
	__UNUSED(size);
	return p;
}

inline void *__cdecl operator new[](size_t size) 
{	
	return VRUMalloc(size); 
}


inline void __cdecl operator delete(void *p) 
{ 
	VRU::SystemUtil::SysMemoryFree(p); 
}

inline void __cdecl operator delete(void *p, void *p2) 
{ 
	//__UNUSED(p);
	__UNUSED(p2);
	VRU::SystemUtil::SysMemoryFree(p); 
}

inline void __cdecl operator delete[](void *p) 
{ 
	VRU::SystemUtil::SysMemoryFree(p); 
}


#ifdef _DEBUG

inline void* __cdecl operator new(size_t size,const char *pFile,unsigned nLine) 
{ 
	return VRU::SystemUtil::SysMemoryAlloc(size,pFile,nLine);
}

//inline void* __cdecl operator new(size_t size,void* p, const char *pFile,unsigned nLine) VET_BAD_ALLOC
//{ 
//	__UNUSED(size);
//	__UNUSED(pFile);
//	__UNUSED(nLine);
//	return p;
//}

inline void* __cdecl operator new[](size_t size,const char *pFile,unsigned nLine) 
{ 
	return VRU::SystemUtil::SysMemoryAlloc(size,pFile,nLine);
}

inline void __cdecl operator delete(void *p,const char *pFile,unsigned nLine) 
{
	__UNUSED(pFile);
	__UNUSED(nLine);
	VRU::SystemUtil::SysMemoryFree(p);
}

inline void __cdecl operator delete[](void *p,const char *pFile,unsigned nLine) 
{
	__UNUSED(pFile);
	__UNUSED(nLine);
	VRU::SystemUtil::SysMemoryFree(p);
}


/***************************************************************
	以下为拦截std的debug版本内存处理
	详情可参阅 stdxdebug.h
*/
namespace std
{
struct _DebugHeapTag_t;
};

inline void * __cdecl operator new(size_t _Size,
	const std::_DebugHeapTag_t&,  char *pFile, int nLine, int nReserved=0/*避免与库中new冲突*/)
{
	__UNUSED(nReserved);
	return VRU::SystemUtil::SysMemoryAlloc(_Size,pFile,nLine);
}

inline  void * __cdecl operator new[](size_t _Size,
	const std::_DebugHeapTag_t&,  char *pFile, int nLine, int nReserved=0/*避免与库中new冲突*/)
{
	__UNUSED(nReserved);
	return VRU::SystemUtil::SysMemoryAlloc(_Size,pFile,nLine);
}

inline  void __cdecl operator delete(void * ptr,
	const std::_DebugHeapTag_t&,  char *, int, int nReserved)
{
	__UNUSED(nReserved);
	VRU::SystemUtil::SysMemoryFree(ptr);
}

inline  void __cdecl operator delete[](void *ptr,
	const std::_DebugHeapTag_t&,  char *, int, int nReserved)
{
	__UNUSED(nReserved);
	VRU::SystemUtil::SysMemoryFree(ptr);
}
#endif




//////////////////////////////////////////////////////////////////////////
template <class T>
inline T* ConstructInPlace(T* p)
{
	(p);
   return new(p) T;
}

//template <class T>
//inline T* ConstructInPlace(T* p, const T* copy)
//{
//	(p);
//   return new(p) T(*copy);
//}

template <class T,class P1>
inline T* ConstructInPlace1(T* p, P1 p1)
{
	(p);
   return new(p) T(p1);
}

template <class T,class P1, class P2>
inline T* ConstructInPlace2(T* p, P1 p1, P2 p2)
{
	(p);
   return new(p) T(p1,p2);
}


template <class T>
inline void DestructInPlace(T* p)
{
	(p);
   p->~T();
}

/////////////////////////////////////////////////////////////
#pragma pop_macro("delete")
#pragma pop_macro("new")




#endif//#ifndef _VRUMEMORYHANDLER_H_

#endif//#ifdef _VRUMEMORYHANDLER_H_

//	数值配置表																	
//																		
//																		
//																		
//																		
//																		
//																		
//																		
//	类型	注释	数量	默认	默认2	0	1	2	3	4	5	6	7	8	9	10	11	12
	PriceWeightForEnchant	强化价格级权重	13	0.3	0.3	0.3	0.31	0.33	0.37	0.45	0.61	0.93	1.57	2.85	5.41	10.53	20.77	41.25
	WeaponEnchantWeightForEnchant	武器强化级权重	13	0	0	0	0	0	1	2	3	5	7	9	12	15	18	22
	ArmorEnchantWeightForEnchant	防具强化级权重	13	0	0	0	0	0	1	2	3	5	7	9	12	15	18	22
	DuraEnchantWeightForEnchant	耐久度强化级权重	13	0	0	0	0	0	1	2	3	5	7	9	12	15	18	22




//																								
//						rank_F	rank_E	rank_D	rank_C	rank_B	rank_A1	rank_A2	rank_A3	rank_S1	rank_S2	rank_S3								
//						普通	九品	八品	七品	六品	五品	四品	三品	二品	一品	极品								
	PriceWeightForRank	品质价格级权重	11	1	1	1	1	1.1	1.3	1.7	2.5	4.1	7.3	13.7	26.5	52.1								
	PriceWeightForRareRank		11	1	1	1	1.2	1.4	1.6	1.8		2												
	LimitValueForRank	品质限制	11	0	0		0	1	2	3	6	9	12	17	22	27								


//						1级	2级	3级	4级	5级	6级	7级	8级	9级	10级	11级	12级	13级	14级	15级	16级	17级	18级	19级
	WeaponEnchantWeightForLV	武器对人物级别强化权重	15	2	2	2	2	3	3	3	4	4	4	4	5	5	5	5	5	6				
	ArmorEnchantWeightForLV	防具对人物级别强化权重	10	1	1	1	1	1	1	2	2	2	2	3	3									
//				级差<0	级差>19																			
	ExpLevelRatio	级别差距影响经验获取率	19	1	0	0.95	0.9	0.85	0.8	0.75	0.7	0.65	0.6	0.55	0.5	0.45	0.4	0.35	0.3	0.25	0.2	0.15	0.1	0.05

//						1人	2人	3人	4人	5人	6人	7人	8人	9人	10人									
	AdditionalExpRatio	队员数量经验加成	10	1	1	1	1.15	1.3	1.45	1.55	1.65	1.75	1.85	1.9	1.95				
	BonusAttackPower	队员数量攻击加成				0	2	4	6	8	10	12	14	16	18				

//						1 : 普通怪物	2 : 精锐怪物	3 : 怪物头目	4 : 中等BOSS	5 : BOSS									
	ExpRankRatio	Npc级别影响经验加成	5	1	1	1	1.3	1.5	10	15									
	StatusRatioAsNPCGrade	Npc级别受状态机率	5	1	1	1	0.8	0.7	0.5	0.2									

//						0普通村庄	1副本												
	RoomBonusTypeRatio	区域奖励加成	2	1	1	1	1.1												

//						步走	跑步	猛冲	被推	推倒	侧走	后退	前翻	左翻	右翻	后翻	抖肩	滑行	瞬移
//						WALK	RUN	SWIPE	KNOCKBACK	KNOCKBACK_DOWN	SIDESTEP	BACKSTEP	TUMBLING_FRONT	TUMBLING_LEFT	TUMBLING_RIGHT	TUMBLING_BACK	SHOULDER_CHARGE	SLIDING	TELEPORT
	BaseMoveSpeedAsState	各类动作移动速度	14	5	5	0.8	2.4	8	4.5	9	1.8	1.2	10	10	10	10	10	10	10
	MoveTimeAsState	各类动作移动时间	14	433	433	433	433	433	433	433	433	433	300	300	300	300	300	300	300

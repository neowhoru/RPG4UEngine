//	物品提示模板
//	$[@,clr1,clr2]
//	$[@]
//  文本对齐：left right center 
//  图标位置：iconL iconR orgin
//
$[ItemName,0xffff00ff,0xffff0000,iconL,orgin]%s %s   $[ItemRankLvl,right]%s+%d
$[ItemUserName,0xffff00ff,0xffff0000](%s)
$[ItemVendorPrice,0xff1fef4f,0xffaf1f1f]商店  标价：%I64u
$[ItemEqClass,0xff7f7fff,0xffff7f1f]职业  要求：%s %s %s %s %s
$[ItemLimitLevel,0xff7f7fff,0xffaf1f1f]等级  要求：%d
$[ItemCoolTime,0xff7f7f7f]冷却  时间：%d分%g秒
$[ItemAttackSpeed]攻击  加速：+%d%%
$[ItemAttack]基本攻击力：+%ld ~ +%ld
$[ItemMeleeAttack]近身攻击力：+%ld ~ +%ld
$[ItemRangeAttack]远距攻击力：+%ld ~ +%ld
$[ItemMagicAttack]魔法攻击力：+%ld ~ +%ld
$[ItemElementAttack]%s：%c%ld
$[ItemPhyDefense]物理防御力：+%ld
$[ItemMagicDefense]魔法防御力：+%ld
$[ItemElementDefense]%s：%c%ld
$[ItemWasteType]%s%d[分%d次,时间%d分%g秒]
$[ItemLimitStr,0xff7f7fff,0xffaf1f1f]力量  要求：%ld
$[ItemLimitDex,0xff7f7fff,0xffaf1f1f]敏捷  要求：%ld
$[ItemLimitInt,0xff7f7fff,0xffaf1f1f]智力  要求：%ld
$[ItemLimitSkill1,0xff7f7fff,0xffaf1f1f]%s要求：%ld
$[ItemLimitSkill2,0xff7f7fff,0xffaf1f1f]%s要求：%ld
$[ItemOptions,0xff1f7fff]%s   +%d
$[ItemRanks,0xff1f7fff]%s (+%d%s)
$[ItemPotentials,0xff1fafff,0xff01378d]%s (+%d%s)
$[ItemSocketNum]可嵌镶宝石：%d/%d
$[ItemSockets,0xff1f7fff]%d#[%s %s%s]
$[ItemDesc,0xffafafaf]%s
$[ItemOverlap]重叠  数量:%d
$[ItemDura,0xffafafaf,0xff7f7f7f]耐  久  度：%.0f/%d


rem	启动用户DB代理
start UserDBProxyD.exe -no:7000

rem 启动登录服务器
start LoginShellD.exe -no:8000

rem 启动玩家DB代理
start PlayerDBProxyD.exe -no:3000

rem 启动门头服务器
start GateShellD.exe -no:1

rem 启动游戏战区服务器
start GameShellD.exe -no:1000

rem 启动聊天服务器
start ChatShellD.exe -no:4000



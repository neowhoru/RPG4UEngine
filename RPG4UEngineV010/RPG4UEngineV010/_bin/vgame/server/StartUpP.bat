rem	启动用户DB代理
start UserDBProxyP.exe -no:7000

rem 启动登录服务器
start LoginShellP.exe -no:8000

rem 启动玩家DB代理
start PlayerDBProxyP.exe -no:3000

rem 启动门头服务器
start GateShellP.exe -no:1

rem 启动游戏战区服务器
start GameShellP.exe -no:1000

rem 启动聊天服务器
start ChatShellP.exe -no:4000


